<%-- File name: /trescal/core/thirdparty/viewtpquote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="tpcosts.thirdpartyquotation"/> - ${command.tpQuotation.qno}</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/tpcosts/tpquote/ViewTPQuote.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js' ></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${command.tpQuotation.qno}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<!-- error section displayed when form submission fails -->
		<form:errors path="command.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="tpcosts.errorviewthirdpartyquote"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
		<!-- end error section -->
		<!-- infobox div containing third party quote details -->	
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="tpcosts.thirdpartyquotation"/> - ${command.tpQuotation.qno}</legend>
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="company"/>:</label>
							<span>
								<links:companyLinkDWRInfo company="${command.tpQuotation.contact.sub.comp}" rowcount="0" copy="true"/>
							</span>
						</li>
						<li>
							<label><spring:message code="subdiv"/>:</label>
							<span>
								<links:subdivLinkDWRInfo rowcount="0" subdiv="${command.tpQuotation.contact.sub}"/>
							</span>
						</li>
						<li>
							<label><spring:message code="contact"/>:</label>
							<span>
								<links:contactLinkDWRInfo contact="${command.tpQuotation.contact}" rowcount="0"/>
							</span>
						</li>
						<li>
							<label><spring:message code="tpcosts.validity"/>:</label>
							<span><c:out value="${command.tpQuotation.duration} "/><spring:message code="days"/></span>
						</li>
						<li>
							<label><spring:message code="issuedate"/>:</label>
							<span><fmt:formatDate value="${command.tpQuotation.issuedate}" type="date" dateStyle="MEDIUM"/></span>
						</li>
					</ol>
				</div>
				<!-- end of first column div -->
				<!-- displays content in column 49% wide (right) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="tpcosts.tpquotenumber"/>:</label>
							<span>${command.tpQuotation.tpqno}</span>
						</li>
						<li>
							<label><spring:message code="status"/>:</label>
							<span>
								<cwms:besttranslation translations="${command.tpQuotation.status.nametranslations}"/>
							</span>
						</li>
						<li>
							<label><spring:message code="tpcosts.registeredby"/>:</label>
							<span>${command.tpQuotation.createdBy.name}</span>
						</li>
						<li>
							<label><spring:message code="tpcosts.registeredon"/>:</label>
							<span><fmt:formatDate value="${command.tpQuotation.regdate}" dateStyle="medium" type="date"/></span>
						</li>
						<li>
							<label><spring:message code="tpcosts.originaltpqrequest"/>:</label>
							<span>
								<a href="viewtpquoterequest.htm?id=${command.tpQuotation.fromRequest.id}" class="mainlink">
									${command.tpQuotation.fromRequest.requestNo}
								</a>
								<c:choose>
									<c:when test="${command.tpQuotation.fromRequest.requestFromQuote == null}">
										<spring:message code="tpcosts.originatingfromjob"/>
										<a href="viewjob.htm?jobid=${command.tpQuotation.fromRequest.requestFromJob.jobid}" class="mainlink" target="_blank">
											${command.tpQuotation.fromRequest.requestFromJob.jobno}
										</a>
									</c:when>
									<c:otherwise>
										<spring:message code="tpcosts.originatingfromquote"/>
										<a href="viewquotation.htm?id=${command.tpQuotation.fromRequest.requestFromQuote.id}" class="mainlink" target="_blank">
											${command.tpQuotation.fromRequest.requestFromQuote.qno} ver ${command.tpQuotation.fromRequest.requestFromQuote.ver}
										</a>
									</c:otherwise>
								</c:choose>
							</span>
						</li>
					</ol>
				</div>
				<!-- end of second column div -->
			</fieldset>
		</div>
		<!-- end of infobox div containing third party quote details -->
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->		
		<div id="subnav">
			<dl>
				<dt>
					<a href="" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'items-tab', false);" id="items-link" title="<spring:message code='tpcosts.viewthirdpartyquoteitems'/>" class="selected">
						<spring:message code="items"/>
					</a>
				</dt>
				<dt>
					<c:set var="QUOTE_MODEL_ADD_TP_VAR">
						<spring:message code="tpcosts.additemtothirdpartyquote"/>
					</c:set>
					<cwms:securedLink permission="QUOTE_MODEL_ADD_TP" parameter="?id=${command.tpQuotation.id}" collapse="True" title="${QUOTE_MODEL_ADD_TP_VAR}">
						<spring:message code="tpcosts.newitem"/>
					</cwms:securedLink>
				</dt>
				<c:if test="${command.tpQuotation.fromRequest != null}">
					<dt>
						<c:set var="addnewitemfromthirdpartyquoterequest">
							<spring:message code="tpcosts.addnewitemfromthirdpartyquoterequest"/>
						</c:set>
						<cwms:securedLink permission="QUOTE_MODEL_ADD_TP_FROM_REQUEST"  parameter="?tpqid=${command.tpQuotation.id}&tpqrid=${command.tpQuotation.fromRequest.id}" collapse="True" title="${addnewitemfromthirdpartyquoterequest}">
							<spring:message code="tpcosts.newitemfromrequest"/>
						</cwms:securedLink>
					</dt>
				</c:if>
				<dt>
					<a href="" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'edit-tab', false);" id="edit-link" title="<spring:message code='tpcosts.editthirdpartyquotedetails'/>">
						<spring:message code="edit"/>
					</a>
				</dt>
				<dt>
					<a href="" onclick="event.preventDefault(); genDocPopup(${command.tpQuotation.id}, 'tpquotebirtdocs.htm?id=', 'Generating Third Party Quotation Document');" title="<spring:message code='tpcosts.generatethirdpartyquotedocument'/>">
						<spring:message code="generate"/>
					</a>
				</dt>
			</dl>
		</div>
		<div id="subnav2">
			<dl>
				<dt>
					<a href="" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'docs-tab', false);" id="docs-link" title="<spring:message code='tpcosts.viewdocumentsinthirdpartyquotedirectory'/>">
						<spring:message code="documents"/>
					</a>
				</dt>
				<dt>
					<a href="" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'email-tab', false);" id="email-link" title="<spring:message code='tpcosts.viewsendemailsforthirdpartyquote'/>">
						<spring:message code="emails"/>
						(<span class="entityEmailDisplaySize">${command.tpQuotation.sentEmails.size()}</span>)
					</a>
				</dt>
				<dt>
					<a href="" onclick="event.preventDefault(); preparePurchaseOrderFromTPQuotation(${command.tpQuotation.id});">
						<spring:message code="tpcosts.createpurchaseorder"/>
					</a>
				</dt>
				<dt>
					<a href="" onclick="event.preventDefault(); deleteTPQuote(${command.tpQuotation.id});" title="<spring:message code='tpcosts.deletethirdpartyquote'/>">
						<spring:message code="delete"/>
					</a>
				</dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		<div class="infobox" id="viewtpquote">
			<div id="items-tab">
				<div class="float-right">
					<a href="" class="mainlink" id="showall" onclick="event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, 0);">
						<spring:message code="tpcosts.expandall"/>
					</a>
					|
					<a href="" class="mainlink" id="hideall" onclick="event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, 0);">
						<spring:message code="tpcosts.collapseall"/>
					</a>
				</div>
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
				<table class="default4 itemstable" summary="<spring:message code='tpcosts.tablequoteitemheaders'/>">
					<thead>
						<tr>
							<td colspan="10">
								&nbsp;													
								<a href="" class="QIDel float-right pad-right hid" onclick="event.preventDefault(); compileAndSubmitQisToDelete(${command.tpQuotation.id});">
									<img src="img/icons/delete.png" height="16" width="16" alt="<spring:message code='tpcosts.deleteselectedqis'/>" title="<spring:message code='tpcosts.deleteselectedqis'/>"/>
								</a>
							</td>
						</tr>											
						<tr>
							<th scope="col" colspan="2"><spring:message code="tpcosts.itemunit"/></th>
							<th scope="col" class="notes">														
								<a href="" onclick="event.preventDefault(); toggleNoteDisplay(this, 'TPQUOTEITEMNOTE', 'vis', null);" class="toggleNoteDisplay">
									<img src="img/icons/note_expand.png" width="20" height="16" alt="<spring:message code='tpcosts.expandallnotes'/>" title="<spring:message code='tpcosts.expandallnotes'/>"/>
								</a>
							</th> 
							<th scope="col" class="caltype"><spring:message code="servicetype"/></th>
							<th scope="col" class="qty"><spring:message code="qty"/></th>
							<th scope="col" class="insp"><spring:message code="tpcosts.inspection"/></th>
							<th scope="col" class="disc cost"><spring:message code="tpcosts.itemdisc"/> (&#37;)</th>
							<th scope="col" class="disc cost"><spring:message code="tpcosts.itemdisc"/> (${command.tpQuotation.currency.currencyERSymbol})</th>
							<th scope="col" class="finalcost cost"><spring:message code="finalcost"/></th>
							<th scope="col" class="del">
								<input type="checkbox" name="qiDeleteAll" onclick="toggleQIDeleteItems(this.checked);" title="<spring:message code='tpcosts.selectalltpquoteitems'/>"/>
							</th>
						</tr>
					</thead>
				</table>
				<c:choose>
					<c:when test="${command.tpQuotation.items.size() > 0}">
						<c:set var="itemCount" value="0"/>
						<c:forEach var="item" items="${command.tpQuotation.items}">
							<table class="default4 itemstable" summary="<spring:message code='tpcosts.tablethirdpartyquoteitemssubsequentcosts'/>">
								<tbody>
									<c:set var="qinotecount" value="${item.publicActiveNoteCount + item.privateActiveNoteCount}"/>
									<c:if test="${item.partOfBaseUnit != null}">
										<c:set var="itemCount" value="${itemCount + 1}"/>
										<tr id="item${item.id}" style="background-color: ${item.caltype.serviceType.displayColour};">
											<td class="item">
												${itemCount}
												<a href="" id="hide${item.id}" onclick="event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, ${item.id});">
													<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="<spring:message code='tpcosts.hidecostbreakdown'/>" title="<spring:message code='tpcosts.hidecostbreakdown'/>"/>
												</a>
											</td>
											<td class="model">
												<cwms:showmodel instrumentmodel="${item.model}"/>
												<c:forEach var="link" items="${item.linkedTo}">
													<br/>
													(<spring:message code="item"/>
													<a class="mainlink" href="jithirdparty.htm?jobitemid=${link.jobItem.jobItemId}">
														${link.jobItem.itemNo}
													</a>
													<spring:message code="tpcosts.onjob"/>
													<a class="mainlink" href="viewjob.htm?jobid=${link.jobItem.job.jobid}">
														${link.jobItem.job.jobno}
													</a>
													)
												</c:forEach>
											</td>
											<td class="notes" scope="col">
												<links:showNotesLinks noteTypeId="${item.id}" noteType="TPQUOTEITEMNOTE" colspan="10" notecounter="${qinotecount}"/>
											</td>
											<td class="caltype">
												<cwms:besttranslation translations="${item.caltype.serviceType.shortnameTranslation}"/>
											</td>
											<td class="qty">${item.quantity}</td>
											<td class="insp cost">${command.tpQuotation.currency.currencyERSymbol}${item.inspection}</td>
											<c:choose>
												<c:when test="${item.generalDiscountRate > 0}">
													<td class="disc cost">${item.generalDiscountRate}&#37;</td>
													<td class="disc cost">${command.tpQuotation.currency.currencyERSymbol}${item.generalDiscountValue}</td>
												</c:when>
												<c:otherwise>
													<td class="disc cost"><spring:message code="tpcosts.notapplied"/></td>
													<td class="disc cost"><spring:message code="tpcosts.notapplied"/></td>
												</c:otherwise>
											</c:choose>
											<td class="finalcost cost">
												<a href="edittpquoteitem.htm?id=${item.id}" class="mainlink">
													${command.tpQuotation.currency.currencyERSymbol}${item.finalCost}
												</a>
											</td>
											<td class="del">
												<input type="checkbox" name="qiDelete" value="${item.id}" onclick="monitorCheckedItems();" title="<spring:message code='tpcosts.deletequoteitem'/>"/>
											</td>
										</tr>
										<c:set var="rowspan" value="2"/>
										<c:set var="activeCosts" value="false"/>
										<c:forEach var="cost" items="${item.costs}">
											<c:if test="${cost.active}">
												<c:set var="rowspan" value="${rowspan + 1}"/>
												<c:set var="activeCosts" value="true"/>
											</c:if>
										</c:forEach>
										<c:if test="${item.carriageIn > 0}">
											<c:set var="rowspan" value="${rowspan + 1}"/>
											<c:set var="activeCosts" value="true"/>
										</c:if>
										<c:if test="${item.carriageOut > 0}">
											<c:set var="rowspan" value="${rowspan + 1}"/>
											<c:set var="activeCosts" value="true"/>
										</c:if>
										<c:if test="${activeCosts || item.carriageIn > 0 || item.carriageOut > 0}">
											<tr class="costsummary${item.id} vis">
												<td class="bold" rowspan="${rowspan}">&nbsp;</td>
												<td class="bold" colspan="3"><spring:message code="costtype"/></td>
												<td class="cost bold" colspan="2"><spring:message code="cost"/></td>
												<td class="cost bold"><spring:message code="tpcosts.disc"/> (&#37;)</td>
												<td class="cost bold"><spring:message code="tpcosts.disc"/> (${command.tpQuotation.currency.currencyERSymbol})</td>
												<td class="cost bold"><spring:message code="finalcost"/></td>
												<td rowspan="${rowspan}">&nbsp;</td>
											</tr>
											<c:forEach var="cost" items="${item.costs}">
												<c:if test="${cost.active}">									
													<tr class="costsummary${item.id} vis nobord">																											
														<td colspan="3"><spring:message code="${cost.costType.messageCode}"/></td>
														<td class="cost" colspan="2">${command.tpQuotation.currency.currencyERSymbol}${cost.totalCost}</td>
														<td class="cost">${cost.discountRate}&#37;</td>
														<td class="cost">${command.tpQuotation.currency.currencyERSymbol}${cost.discountValue}</td>
														<td class="cost">${command.tpQuotation.currency.currencyERSymbol}${cost.finalCost}</td>
													</tr>																									
												</c:if>
											</c:forEach>
											<!-- TODO make this a macro -->
											<c:if test="${item.carriageIn > 0}">
												<tr class="vis nobord">
													<td colspan="3"><spring:message code="tpcosts.carriagein"/></td>
													<td class="cost" colspan="2">${command.tpQuotation.currency.currencyERSymbol}${item.carriageIn}</td>
													<td class="cost">N/A</td>
													<td class="cost">N/A</td>
													<td class="cost">${command.tpQuotation.currency.currencyERSymbol}${item.carriageIn}</td>
												</tr>
											</c:if>
											<c:if test="${item.carriageOut > 0}">
												<tr class="vis nobord">
													<td colspan="3"><spring:message code="tpcosts.carriageout"/></td>
													<td class="cost" colspan="2">${command.tpQuotation.currency.currencyERSymbol}${item.carriageOut}</td>
													<td class="cost">N/A</td>
													<td class="cost">N/A</td>
													<td class="cost">${command.tpQuotation.currency.currencyERSymbol}${item.carriageOut}</td>
												</tr>
											</c:if>
										</c:if>
										<tr class="costsummary${item.id} vis nobord">
											<c:if test="${!activeCosts}">
												<td>&nbsp;</td>
											</c:if>
											<td colspan="6">&nbsp;</td>
											<td class="cost bold"><spring:message code="total"/></td>
											<td class="cost">
												<a href="edittpquoteitem.htm?id=${item.id}" class="mainlink">
													${command.tpQuotation.currency.currencyERSymbol}${item.finalCost}
												</a>
											</td>
											<c:if test="${!activeCosts}">
												<td>&nbsp;</td>
											</c:if>
										</tr>
										<c:if test="${item.publicActiveNoteCount + item.privateActiveNoteCount > 0}">
											<tr id="TPQUOTEITEMNOTE${item.id}" class="costsummary${item.id} hid">
												<td colspan="10" class="nopadding">
													<t:showActiveNotes contact="" links="true" entity="${item}" privateOnlyNotes="false" noteTypeId="${item.id}" noteType="TPQUOTEITEMNOTE"/>
												</td>
											</tr>
										</c:if>															
										<!-- code left in case module functionality needed?
										#foreach($module in $item.modules)
											<div id="item${module.id}" style="border-left: 1px solid black; padding: 3px; margin-left: 20px;">
												#showModel($module.model)
												[
												#foreach($costtype in $module.costTypes)
													#springMessage(${costtype.messageCode})
												#end
												]		
												<a href="#" onclick="deleteTPQItem($module.id); return false;">delete</a>
												##showActiveNotes($module.notes $module.id true "tpquotereqitemnote" "itemnote" "tpquoteformitemnote" "tpquotereqitemid" "")
											</div>
										#end
										-->
									</c:if>
								</tbody>
							</table>
						</c:forEach>
						<table class="default4 hid QIDel" summary="<spring:message code='tpcosts.tybleselecttodelete'/>">
								<thead>
									<tr>
										<td>
											<a href="" class="float-right pad-right" onclick="event.preventDefault(); compileAndSubmitQisToDelete(${command.tpQuotation.id});">
												<img src="img/icons/delete.png" height="16" width="16" alt="<spring:message code='tpcosts.deleteselectedqis'/>" title="<spring:message code='tpcosts.deleteselectedqis'/>"/>
											</a>
										</td>
									</tr>
								</thead>
							</table>
							<table class="default4" summary="<spring:message code='tpcosts.tabletotalcostsalltpqitems'/>">
								<tbody>
									<tr class="nobord">												
										<td class="totalsum cost2pcTotal bold"><spring:message code="totalcost"/>:</td>
										<td class="totalamount cost2pcTotal bold">${command.tpQuotation.currency.currencyERSymbol}${command.tpQuotation.finalCost}</td>
										<td class="filler">&nbsp;</td>
									</tr>
								</tbody>
							</table>
					</c:when>
					<c:otherwise>
						<table class="default4" summary="<spring:message code='tpcosts.tablenotpqitems'/>">																					
							<tbody>
								<tr class="nobord">
									<td class="center bold" colspan="9"><spring:message code="tpcosts.nothirdpartyquoteitemshavebeenadded"/></td>
								</tr>
							</tbody>
						</table>
					</c:otherwise>
				</c:choose>
			</div>
			<!-- this div contains all edit quote item form elements -->
			<div id="edit-tab" class="hid">
				<form:form modelAttribute="command" method="post">
					<fieldset>
						<legend><spring:message code="tpcosts.editthirdpartyquotation"/></legend>
						<ol>
							<li>
								<form:label path="tpQuotation.tpqno"><spring:message code="tpcosts.tpqno"/>:</form:label>
								<form:input path="tpQuotation.tpqno" type="text"/>
								<span class="attention"><form:errors path="tpQuotation.tpqno"/></span>
							</li>
							<li>
								<form:label path="statusId"><spring:message code="status"/>:</form:label>
								<form:select path="statusId">
									<c:forEach var="sta" items="${statusList}">
										<form:option value="${sta.statusid}">
											<cwms:besttranslation translations="${sta.nametranslations}"/>
										</form:option>
									</c:forEach>
								</form:select>
								<span class="attention"><form:errors path="statusId"/></span>
							</li>
							<li>
								<form:label path="personId"><spring:message code="contact"/>:</form:label>
								<form:select path="personId" items="${clientConList}" itemValue="personid" itemLabel="name"/>
								<span class="attention"><form:errors path="personId"/></span>
							</li>
							<li>
								<form:label path="tpQuotation.duration"><spring:message code="tpcosts.validity"/>:</form:label>
								<form:input path="tpQuotation.duration" type="text"/> <spring:message code="days"/>
								<span class="attention"><form:errors path="tpQuotation.duration"/></span>
							</li>
							<li>
								<form:label path="tpQuotation.issuedate"><spring:message code="issuedate"/>:</form:label>
								<form:input path="tpQuotation.issuedate" type="date" />
								<span class="attention"><form:errors path="tpQuotation.issuedate"/></span>
							</li>
							<li>
								<form:label path="currencyCode"><spring:message code="currency"/>:</form:label>
								<form:select path="currencyCode">
									<c:forEach var="curopt" items="${currencyopts}">
										<form:option value="${curopt.optionValue}">
											${curopt.currency.currencyCode}
											@ ${curopt.defaultCurrencySymbol}1 = ${curopt.currency.currencySymbol}${curopt.rate}
											<c:choose>
												<c:when test="${curopt.companyDefault}">
													[<spring:message code="viewjob.compdefrate"/>]
												</c:when>
												<c:when test="${curopt.systemDefault}">
													[<spring:message code="viewjob.systdefrate"/>]
												</c:when>
											</c:choose>
										</form:option>
									</c:forEach>
								</form:select>
								<span class="attention"><form:errors path="currencyCode"/></span>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" value="<spring:message code='save'/>"/>
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
			<!-- end of div containing all edit quote item form elements -->
			<!-- this div contains all documents relating to third party quote item -->
			<div id="docs-tab" class="hid">
				<files:showFilesForSC rootFiles="${scRootFiles}" id="${command.tpQuotation.id}" allowEmail="true" entity="${command.tpQuotation}" isEmailPlugin="false" ver="" identifier="${command.tpQuotation.qno}" deleteFiles="true" sc="${sc}" rootTitle="<spring:message code='tpcosts.filesforthirdpartyquotation'/>"/>
			</div>
			<!-- end of documents div -->
			<!-- this div contains all emails relating to third party quote item -->
			<div id="email-tab" class="hid">
				<t:showEmails entity="${command.tpQuotation}" entityId="${command.tpQuotation.id}" entityName="<spring:message code='tpcosts.thirdpartyquotation'/>" sc="${sc}"/>
			</div>
			<!-- end of email div -->
		</div>
		<!-- this section contains all third party quotation notes -->
		<t:showTabbedNotes entity="${command.tpQuotation}" noteType="TPQUOTENOTE" noteTypeId="${command.tpQuotation.id}" privateOnlyNotes="${privateOnlyNotes}"/>
		<!-- end of third party quotation notes section -->
	</jsp:body>
</t:crocodileTemplate>