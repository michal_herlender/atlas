<%--
  Created by IntelliJ IDEA.
  User: Soufiane.AMANKACHI
  Date: 21/12/2021
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:crocodileTemplate>

    <jsp:attribute name="scriptPart">
		<script src='script/trescal/core/workallocation/workallocation.js'></script>
	</jsp:attribute>

    <jsp:attribute name="header">
		<span class="headtext"><spring:message code="workflow.allocateworktoengineers" /></span>
	</jsp:attribute>

    <jsp:body>
        <div id="subnav">
            <dl>
                <dt>
                    <a href="#"
                       onclick="event.preventDefault(); switchMenuFocus(menuElements, 'unallocated-tab', false);"
                       id="unallocated-link" class="selected"><spring:message
                            code="workflow.unallocated" /></a>
                </dt>
                <dt>
                    <a href="#"
                       onclick="event.preventDefault(); switchMenuFocus(menuElements, 'allocated-tab', true);"
                       id="allocated-link"> <spring:message code="workflow.allocated" /> </a>
                </dt>
            </dl>
        </div>

        <div id="workallocation">
            <form:form modelAttribute="form" method="post" id="myForm">
                 <div id="unallocated-tab">
                        <div class="infobox" >
                            <fieldset id="searchfieldset">
                                <div class="displaycolumn-60">
                                    <ol>
                                        <li>
                                            <label><spring:message code="workallocation.filterby.jobtype"/> :</label>
                                            <form:select path="searchForm.jobtype" items="${jobtypes}" itemValue="name" itemLabel="description"/>
                                        </li>

                                        <li>
                                            <label><spring:message code="workallocation.filterby.lab"/> :</label>
                                            <form:select id="laboratoryId" path="searchForm.laboratoryId" onchange="populateSubLabSelect()">
                                                <form:option value=""> ---- ALL ----</form:option>
                                                <c:forEach items="${labs}" var="lab">
                                                    <form:option  value="${lab.deptid}">
                                                        ${lab.name}
                                                    </form:option>
                                                </c:forEach>
                                            </form:select>
                                        </li>

                                        <li>
                                            <label><spring:message code="workallocation.filterby.sublab"/> :</label>
                                            <form:select id="subLabId" path="searchForm.subLabId">
                                                <form:option value=""> ---- ALL ----</form:option>
                                                <c:choose>
                                                    <c:when test="${not empty sublabs}">
                                                        <c:forEach items="${sublabs}" var="sublab">
                                                            <form:option value="${sublab.id}"> ${sublab.description} </form:option>
                                                        </c:forEach>
                                                    </c:when>
                                                    <c:otherwise>

                                                    </c:otherwise>
                                                </c:choose>
                                            </form:select>

                                        </li>

                                        <li>
                                            <label><spring:message code="workallocation.filterby.subfamily"/> :</label>
                                            <form:input path="searchForm.desc" id="desc" tabindex="6"/>
                                            <form:hidden path="searchForm.descId" />
                                        </li>
                                        <li>
                                            <input type="submit" name="submit" id="submit" class="float-left" value="<spring:message code='workallocation.submit' />" tabindex="24" />
                                            <input type="button" onclick="resetForm();" value="<spring:message code='workalloaction.reset' />" tabindex="0" />
                                            <c:if test="${not empty items}">
                                                <input type="submit" name="addtobasket" id="addtobasket" class="centre" value="Add to Basket" tabindex="24" />
                                            </c:if>

                                        </li>
                                    </ol>
                                </div>
                                <div class="displaycolumn-40">
                                    <ol>
                                        <li>
                                            <label><spring:message code="workallocation.filterby.duedate"/> :</label>
                                            <form:select path="searchForm.rangeDueDate" id="rangeDueDate" tabindex="7" onchange=" changeDateOption(this.value, 'dueDateSpan'); return false; ">
                                                <option value="false" <c:if test="${form.searchForm.rangeDueDate == false}"> selected </c:if>><spring:message code='certsearch.on'/></option>
                                                <option value="true" <c:if test="${form.searchForm.rangeDueDate == true}"> selected </c:if>><spring:message code='certsearch.between'/></option>
                                            </form:select>

                                            <form:input  path="searchForm.aweDueDate1" id="aweDueDate1" type="date" />


                                            <span id="dueDateSpan" <c:choose><c:when test="${form.searchForm.rangeDueDate == true}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>><spring:message code='certsearch.and'/> &nbsp;
						                        	<form:input type="date"  path="searchForm.awedueDate2" id="awedueDate2" />
						                    </span>
                                        </li>

                                        <li>
                                            <label><spring:message code="workallocation.filterby.clientCompany"/>: </label>
                                            <form:select  id="clientCompanyId" path="searchForm.clientCompanyId" onchange="populatClientSubdivSelect()">
                                                <form:option value=""> ---- ALL ----</form:option>
                                                <c:forEach items="${clientCompanies}" var="cltComp">
                                                    <form:option value="${cltComp.coid}"> ${cltComp.coname} </form:option>
                                                </c:forEach>
                                            </form:select>

                                        </li>

                                        <li>
                                            <label><spring:message code="workallocation.filterby.clientsubdiv"/> :</label>
                                            <form:select id="clientSubdivId" path="searchForm.clientSubdivId">
                                                <form:option value=""> ---- ALL ----</form:option>
                                                <c:choose>
                                                    <c:when test="${not empty subdivs}">
                                                        <c:forEach items="${subdivs}" var="subdiv">
                                                            <form:option value="${subdiv.subdivid}"> ${subdiv.subname} </form:option>
                                                        </c:forEach>
                                                    </c:when>
                                                    <c:otherwise>

                                                    </c:otherwise>
                                                </c:choose>
                                            </form:select>
                                        </li>

                                        <li>
                                            <label><spring:message code="workallocation.filterby.ServiceType"/> :</label>
                                            <form:select path="searchForm.serviceTypeId">
                                                <form:option value=""> ---- ALL ----</form:option>
                                                <c:forEach items="${serviceTypes}" var="st">
                                                    <form:option value="${st.serviceTypeId}"> <cwms:besttranslation translations="${st.shortnameTranslation}" /> </form:option>
                                                </c:forEach>
                                            </form:select>
                                        </li>
                                    </ol>
                                </div>
                            </fieldset>
                            <fieldset id="basketfieldset" class="hid" >
                                <div class="displaycolumn-60">
                                    <ol>
                                        <li>
                                            <label>Subdivision Engineer : </label>
                                            <form:select path="basketForm.SubdivEngineerI">
                                                <c:forEach items="${subdivEngineer.keySet()}" var="engineer">
                                                    <c:forEach var="name" items="${subdivEngineer.get(engineer)}">
                                                        <form:option value="${engineer}">  ${name} </form:option>
                                                    </c:forEach>
                                                </c:forEach>
                                            </form:select>
                                        </li>
                                        <li>
                                            <label> Automatic sheduling : </label>
                                            <form:select path="basketForm.autoSheduling" onchange="addRequiredToProdDate();">
                                                <c:forEach items="${AutoSheduling}" var="auto">
                                                    <form:option value="${auto}">${auto}</form:option>
                                                </c:forEach>
                                            </form:select>
                                        </li>
                                        <li>
                                            <input type="submit" name="allocate" id="allocate" class="float-left" value="Allocate" tabindex="24" />
                                            <input type="button" onclick="resetForm();" value="<spring:message code='workalloaction.reset' />" id="clear"  tabindex="0" />
                                        </li>
                                    </ol>
                                </div>
                                <div class="displaycolumn-40">
                                    <ol>
                                        <li>
                                            <label>BC Engineer</label>
                                            <form:select path="basketForm.busnissCompEngineerId">
                                                <form:option value="" selected="selected">--------------</form:option>
                                                <c:forEach var="subdiv" items="${businessCompany.keySet()}">
                                                    <optgroup label="${subdiv}">
                                                        <form:options items="${businessCompany.get(subdiv)}" itemLabel="name" itemValue="personid" />
                                                    </optgroup>
                                                </c:forEach>
                                            </form:select>

                                        </li>
                                    </ol>
                                </div>
                            </fieldset>
                        </div>
                         <div id="subnav">
                             <dl>
                                 <dt>
                                     <a href="#"
                                        onclick="event.preventDefault(); switchMenuFocus(menuElements2, 'searchresult-tab', false); switchfielset('searchfieldset');"
                                        id="searchresult-link" class="selected"><spring:message
                                             code="workalloaction.searchresult" /></a>
                                 </dt>
                                 <dt>
                                     <a href="#"
                                        onclick="event.preventDefault(); switchMenuFocus(menuElements2, 'basket-tab', true); switchfielset('basketfieldset');"
                                        id="basket-link"> <spring:message code="workalloaction.basket" /> (items : ${basketsize} - TMC : ${sumOfTmcs} )</a>
                                 </dt>
                             </dl>
                         </div>
                        <div id="search-basket-tab">
                            <div id="searchresult-tab">
                                <c:if test="${not empty items}">
                                 <fieldset>
                                    <div class="infobox">
                                        <input type="checkbox" id="allItemsCheck" onclick="checkAllItems('itemsClass','allItemsCheck')" />
                                        <c:set var="counter" value="0"/>
                                        <c:forEach var="job" varStatus="jobloop" items="${items.keySet()}">
                                            <table id="searchResults-${job.jobId}" class="default2" >
                                                <tbody>
                                                    <tr>
                                                        <td class="wfjobno">
                                                            <input type="checkbox" id="jobCheckedForAllocation" onclick="selectAllItemsOfJob(${job.jobId})" />
                                                            <links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${job.jobNo}" jobid="${job.jobId}" copy="${false}"/>

                                                        </td>
                                                        <td class="wfconame">
                                                            <links:companyLinkInfo companyId="${job.clientCompanyId}" onStop="${job.clientCompanyOnStop}" rowcount="${rowcount}" companyName="${job.clientCompanyName}" active="${job.clientCompanyActive}" copy="${false}"/>

                                                        </td>
                                                        <td class="wfcontact">
                                                            <links:contactLinkInfo contactName="${job.clientContactName}" personId="${job.clientContactId}" active="${job.clientContactActive}" rowcount="${rowcount}" />

                                                        </td>
                                                        <td class="wfstate">
                                                                ${job.jobStatus}
                                                        </td>
                                                        <td class="wfclientref">
                                                                ${job.transportOutName}
                                                        </td>
                                                        <td class="wfdatein">
                                                            <fmt:formatDate value="${job.regDate}" type="date" dateStyle="SHORT" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6" style="padding: 0;" >
                                                            <table class="default4 child-table" style="border: 0; cellpadding: 0; cellspacing: 0;" id="itemsOfJob-${job.jobId}">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="wfitem" scope="col"><spring:message code="item"/></th>
                                                                        <th class="wfinstr" scope="col"><spring:message code="instmodelname"/></th>
                                                                        <th class="wfserial" scope="col"><spring:message code="serialno"/></th>
                                                                        <th class="wfplant" scope="col"><spring:message code="plantno"/></th>
                                                                        <th class="wfcaltype" scope="col"><spring:message code="servicetype"/></th>
                                                                        <th class="wfproc" scope="col"><spring:message code="capability"/></th>
                                                                        <th class="wfcaltype" scope="col">TMC</th>
                                                                        <th class="wfstatus" scope="col"><spring:message code="status"/></th>
                                                                        <th class="wfduedate" scope="col"><spring:message code="duedate"/></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <c:forEach var="jobItem" varStatus="loop" items="${items.get(job)}">
                                                                        <tr  id="${jobItem.jobItemId}">
                                                                            <td>

                                                                                <form:checkbox path="dto.row[${counter}].check" class="itemsClass"/>
                                                                                <links:jobitemLinkInfo jobItemNo="${jobItem.jobItemNo}" jobItemCount="${jobItem.jobItemCount}" jobItemId="${jobItem.jobItemId}" defaultPage="jiactions.htm"/>

                                                                                <form:hidden path="dto.row[${counter}].jobItemNo" value="${jobItem.jobItemNo}"/>
                                                                                <form:hidden path="dto.row[${counter}].jobItemCount" value="${jobItem.jobItemCount}"/>
                                                                                <form:hidden path="dto.row[${counter}].jobItemId" value="${jobItem.jobItemId}"/>

                                                                                <form:hidden path="dto.row[${counter}].jobNo" value="${job.jobNo}"/>
                                                                                <form:hidden path="dto.row[${counter}].jobId" value="${job.jobId}"/>

                                                                                <form:hidden path="dto.row[${counter}].clientCompanyId" value="${job.clientCompanyId}"/>
                                                                                <form:hidden path="dto.row[${counter}].clientCompanyOnStop" value="${job.clientCompanyOnStop}"/>
                                                                                <form:hidden path="dto.row[${counter}].clientCompanyName" value="${job.clientCompanyName}"/>
                                                                                <form:hidden path="dto.row[${counter}].clientCompanyActive" value="${job.clientCompanyActive}"/>

                                                                                <form:hidden path="dto.row[${counter}].clientContactFirstName" value="${jobItem.clientContactFirstName}"/>
                                                                                <form:hidden path="dto.row[${counter}].clientContactLastName" value="${jobItem.clientContactLastName}"/>

                                                                                <form:hidden path="dto.row[${counter}].turn" value="${jobItem.turn}"/>
                                                                                <form:hidden path="dto.row[${counter}].jobType" value="${jobItem.jobType}"/>

                                                                                <form:hidden path="dto.row[${counter}].clientSubdivId" value="${jobItem.clientSubdivId}"/>
                                                                                <form:hidden path="dto.row[${counter}].clientSubdivName" value="${jobItem.clientSubdivName}"/>

                                                                                <form:hidden path="dto.row[${counter}].clientCompanyOnStop" value="${jobItem.clientCompanyOnStop}"/>
                                                                                <form:hidden path="dto.row[${counter}].transportOutId" value="${jobItem.transportOutId}"/>

                                                                                <form:hidden path="dto.row[${counter}].transportOutSubdiv" value="${jobItem.transportOutSubdiv}"/>

                                                                                <form:hidden path="dto.row[${counter}].allocatedSubdivId" value="${jobItem.allocatedSubdivId}"/>
                                                                                <form:hidden path="dto.row[${counter}].allocatedSubdivCode" value="${jobItem.allocatedSubdivCode}"/>

                                                                                <form:hidden path="dto.row[${counter}].clientSubdivActive" value="${jobItem.clientSubdivActive}"/>

                                                                                <form:hidden path="dto.row[${counter}].clientContactId" value="${job.clientContactId}"/>
                                                                                <form:hidden path="dto.row[${counter}].clientContactActive" value="${job.clientContactActive}"/>
                                                                                <form:hidden path="dto.row[${counter}].jobStatus" value="${job.jobStatus}"/>
                                                                                <form:hidden path="dto.row[${counter}].jobRegDate" value="${jobItem.jobRegDate}"/>
                                                                                <form:hidden path="dto.row[${counter}].transportOutName" value="${job.transportOutName}"/>


                                                                            </td>
                                                                            <td>
                                                                                    ${jobItem.modelName}
                                                                                <form:hidden path="dto.row[${counter}].modelName" value="${jobItem.modelName}"/>
                                                                            </td>
                                                                            <td>
                                                                                    ${jobItem.serialNo}
                                                                                <form:hidden path="dto.row[${counter}].serialNo" value="${jobItem.serialNo}"/>
                                                                                <form:hidden path="dto.row[${counter}].plantId" value="${jobItem.plantId}"/>
                                                                            </td>
                                                                            <td>
                                                                                    ${jobItem.plantNo}
                                                                                <form:hidden path="dto.row[${counter}].plantNo" value="${jobItem.plantNo}"/>
                                                                            </td>
                                                                            <td class="center">
                                                                                    ${jobItem.serviceTypeName}
                                                                                <form:hidden path="dto.row[${counter}].serviceTypeName" value="${jobItem.serviceTypeName}"/>
                                                                            </td>
                                                                            <td class="center">
                                                                                    ${jobItem.procedureReference}
                                                                                <form:hidden path="dto.row[${counter}].procedureReference" value="${jobItem.procedureReference}"/>
                                                                                <form:hidden path="dto.row[${counter}].procedureId" value="${jobItem.procedureId}"/>
                                                                            </td>
                                                                            <td>
                                                                                    ${jobItem.tmc}
                                                                                <form:hidden path="dto.row[${counter}].tmc" value="${jobItem.tmc}"/>
                                                                            </td>
                                                                            <td>
                                                                                <c:if test="${jobItem.lastActionRemark != null}">
                                                                                    <fmt:formatDate var="date" value="${jobItem.lastActionEndStamp}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
                                                                                    <img src="img/icons/information.png" width="16" height="16" class="img_marg_bot" title="${date} - <c:out value='${jobItem.lastActionRemark}'/>"/>
                                                                                    <form:hidden path="dto.row[${counter}].lastActionRemark" value="${jobItem.lastActionRemark}"/>
                                                                                </c:if>
                                                                                    ${jobItem.stateName}
                                                                                <form:hidden path="dto.row[${counter}].stateName" value="${jobItem.stateName}"/>
                                                                            </td>
                                                                            <td class="center">
                                                                                <fmt:formatDate value="${jobItem.dueDate}" type="date" dateStyle="SHORT"/>
                                                                                <form:hidden path="dto.row[${counter}].dueDate" value="${jobItem.dueDate}" />
                                                                            </td>
                                                                        </tr>
                                                                        <c:set var="counter" value="${counter + 1}"/>
                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </c:forEach>
                                    </div>
                                </fieldset>
                                </c:if>
                            </div>
                            <div id="basket-tab" class="hid">
                                <c:if test="${not empty basketItems}">
                                    <fieldset>
                                        <div class="infobox">
                                            <c:set var="basketcounter" value="0"/>
                                            <c:forEach var="basketJob"  items="${basketItems.keySet()}">
                                                <table id="basketItems-${basketJob.jobId}" class="default2" >
                                                    <tbody>
                                                        <tr>
                                                            <td class="wfjobno">
                                                                <input type="checkbox" checked="true" />
                                                                <links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${basketJob.jobNo}" jobid="${basketJob.jobId}" copy="${false}"/>
                                                            </td>
                                                            <td class="wfconame">
                                                                <links:companyLinkInfo companyId="${basketJob.clientCompanyId}" onStop="${basketJob.clientCompanyOnStop}" rowcount="${rowcount}" companyName="${basketJob.clientCompanyName}" active="${basketJob.clientCompanyActive}" copy="${false}"/>
                                                            </td>
                                                            <td class="wfcontact">
                                                                <links:contactLinkInfo contactName="${basketJob.clientContactName}" personId="${basketJob.clientContactId}" active="${basketJob.clientContactActive}" rowcount="${rowcount}" />
                                                            </td>
                                                            <td class="wfstate">
                                                                    ${basketJob.jobStatus}
                                                            </td>
                                                            <td class="wfclientref">
                                                                    ${basketJob.transportOutName}
                                                            </td>
                                                            <td class="wfdatein">
                                                                <fmt:formatDate value="${basketJob.regDate}" type="date" dateStyle="SHORT" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="6" style="padding: 0;" >
                                                                <table class="default4 child-table" style="border: 0; cellpadding: 0; cellspacing: 0;" id="BasketitemsOfJob-${basketJob.jobId}">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="wfitem" scope="col"><spring:message code="item"/></th>
                                                                            <th class="wfinstr" scope="col"><spring:message code="instmodelname"/></th>
                                                                            <th class="wfserial" scope="col"><spring:message code="serialno"/></th>
                                                                            <th class="wfplant" scope="col"><spring:message code="plantno"/></th>
                                                                            <th class="wfcaltype" scope="col"><spring:message code="servicetype"/></th>
                                                                            <th class="wfproc" scope="col"><spring:message code="capability"/></th>
                                                                            <th class="wfproc" scope="col"> <spring:message code="workallocation.tmc"/> </th>
                                                                            <th class="wfstatus" scope="col"> <spring:message code="workallocation.proudction_date"/> </th>
                                                                            <th class="wfstatus" scope="col"> <spring:message code="workallocation.comment"/> </th>
                                                                            <th class="wfduedate" scope="col"><spring:message code="duedate"/></th>
                                                                            <th class="wfduedate" scope="col"><spring:message code="remove"/></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <c:forEach var="item" varStatus="loop" items="${basketItems.get(basketJob)}">
                                                                            <tr  id="${item.jobItemId}">
                                                                                <td>
                                                                                    <links:jobitemLinkInfo jobItemNo="${item.jobItemNo}" jobItemCount="${item.jobItemCount}" jobItemId="${item.jobItemId}" defaultPage="jiactions.htm"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].jobItemNo" value="${item.jobItemNo}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].jobItemCount" value="${item.jobItemCount}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].jobItemId" value="${item.jobItemId}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].jobNo" value="${basketJob.jobNo}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].jobId" value="${basketJob.jobId}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientCompanyId" value="${basketJob.clientCompanyId}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientCompanyOnStop" value="${basketJob.clientCompanyOnStop}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientCompanyName" value="${basketJob.clientCompanyName}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientCompanyActive" value="${basketJob.clientCompanyActive}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientContactFirstName" value="${item.clientContactFirstName}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientContactLastName" value="${item.clientContactLastName}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].turn" value="${item.turn}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].jobType" value="${item.jobType}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientSubdivId" value="${item.clientSubdivId}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientSubdivName" value="${item.clientSubdivName}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientCompanyOnStop" value="${item.clientCompanyOnStop}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].transportOutId" value="${item.transportOutId}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].transportOutSubdiv" value="${item.transportOutSubdiv}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].allocatedSubdivId" value="${item.allocatedSubdivId}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].allocatedSubdivCode" value="${item.allocatedSubdivCode}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientSubdivActive" value="${item.clientSubdivActive}"/>

                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientContactId" value="${basketJob.clientContactId}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].clientContactActive" value="${basketJob.clientContactActive}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].jobStatus" value="${basketJob.jobStatus}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].jobRegDate" value="${item.jobRegDate}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].transportOutName" value="${basketJob.transportOutName}"/>
                                                                                </td>
                                                                                <td>
                                                                                        ${item.modelName}
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].modelName" value="${item.modelName}"/>
                                                                                </td>
                                                                                <td>
                                                                                        ${item.serialNo}
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].serialNo" value="${item.serialNo}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].plantId" value="${item.plantId}"/>
                                                                                </td>
                                                                                <td>
                                                                                        ${item.plantNo}
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].plantNo" value="${item.plantNo}"/>
                                                                                </td>
                                                                                <td class="center">
                                                                                        ${item.serviceTypeName}
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].serviceTypeName" value="${item.serviceTypeName}"/>
                                                                                </td>
                                                                                <td class="center">
                                                                                        ${item.procedureReference}
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].procedureReference" value="${item.procedureReference}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].procedureId" value="${item.procedureId}"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].stateName" value="${item.stateName}"/>
                                                                                </td>
                                                                                <td class="center">
                                                                                        ${item.tmc}
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].tmc" value="${item.tmc}"/>
                                                                                </td>
                                                                                <td class="center">
                                                                                    <form:input class="itemDate" id="date-${basketcounter}" path="basketForm.basketItems[${basketcounter}].production_date" type="date"/>
                                                                                    <a role="button" class="imagelink" onclick="copyDown('date-${basketcounter}',${basketcounter},'date','itemDate');" >
                                                                                        <img src="img/icons/arrow_down.png" width="10" height="16" alt="Arrow down" title="Arrow down">
                                                                                    </a>
                                                                                    <a role="button" class="imagelink" onclick="copyUp('date-${basketcounter}',${basketcounter},'date','itemDate');">
                                                                                        <img src="img/icons/arrow_up.png" width="10" height="16" alt="Arrow Up" title="Arrow Up">
                                                                                    </a>
                                                                                </td>
                                                                                <td class="center">
                                                                                    <form:input  class="itemCmt" type="text" size="30" id="cmt-${basketcounter}" path="basketForm.basketItems[${basketcounter}].item_comment"/>
                                                                                    <a role="button" class="imagelink" onclick="copyDown('cmt-${basketcounter}',${basketcounter},'cmt','itemCmt');">
                                                                                        <img src="img/icons/arrow_down.png" width="10" height="16" alt="Arrow down" title="Arrow down">
                                                                                    </a>
                                                                                    <a role="button" class="imagelink" onclick="copyUp('cmt-${basketcounter}',${basketcounter},'cmt','itemCmt');">
                                                                                        <img src="img/icons/arrow_up.png" width="10" height="16" alt="Arrow Up" title="Arrow Up">
                                                                                    </a>
                                                                                </td>
                                                                                <td class="center">
                                                                                    <fmt:formatDate value="${item.dueDate}" type="date" dateStyle="SHORT"/>
                                                                                    <form:hidden path="basketForm.basketItems[${basketcounter}].dueDate" value="${item.dueDate}" />
                                                                                </td>
                                                                                <td class="center">
                                                                                    <img src="img/icons/delete.png" width="16" height="16" alt=""title="" onclick="removeItemFromBasket(${basketJob.jobId},${item.jobItemId});"  />
                                                                                </td>
                                                                            </tr>
                                                                            <c:set var="basketcounter" value="${basketcounter + 1}"/>
                                                                        </c:forEach>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </c:forEach>
                                        </div>
                                    </fieldset>
                                </c:if>
                            </div>
                        </div>
                 </div>
                <div id="allocated-tab" class="hid">
                    <div class="infobox" >
                        <fieldset>
                            <div class="displaycolumn-60">
                                <ol>
                                    <li>
                                        <label><spring:message code="workallocation.filterby.jobtype"/> :</label>
                                        <form:select path="allocatedItemsSearchForm.jobtype" items="${jobtypes}" itemValue="name" itemLabel="description"/>
                                    </li>

                                    <li>
                                        <label><spring:message code="workallocation.filterby.lab"/> :</label>
                                        <form:select id="laboratoryId_2" path="allocatedItemsSearchForm.laboratoryId" onchange="populateAllocatedSubLabSelect()">
                                            <form:option value=""> ---- ALL ----</form:option>
                                            <c:forEach items="${labs}" var="lab">
                                                <form:option  value="${lab.deptid}">
                                                    ${lab.name}
                                                </form:option>
                                            </c:forEach>
                                        </form:select>
                                    </li>

                                    <li>
                                        <label><spring:message code="workallocation.filterby.sublab"/> :</label>
                                        <form:select id="subLabId_2" path="allocatedItemsSearchForm.subLabId">
                                            <form:option value=""> ---- ALL ----</form:option>
                                        </form:select>

                                    </li>

                                    <li>
                                        <label><spring:message code="workallocation.filterby.subfamily"/> :</label>
                                        <form:input path="allocatedItemsSearchForm.desc" id="desc" tabindex="6"/>
                                        <form:hidden path="allocatedItemsSearchForm.descId" />
                                    </li>
                                    <li>
                                        <label>Subdivision Engineer : </label>
                                        <form:select path="allocatedItemsSearchForm.SubdivEngineerId">
                                            <form:option value=""></form:option>
                                            <c:forEach items="${subEngineers}" var="engineer">
                                                    <form:option value="${engineer.personid}">  ${engineer.getName()} </form:option>
                                            </c:forEach>
                                        </form:select>
                                    </li>
                                    <li>

                                    </li>
                                    <li>
                                        <input type="submit" name="allocated" id="allocated" class="float-left" value="<spring:message code='workallocation.submit' />" tabindex="24" />
                                        <input type="button" onclick="resetForm();" value="<spring:message code='workalloaction.reset' />" tabindex="0" />
                                        <c:if test="${not empty allocatedItems}">
                                            <input type="submit" name="save" id="save" value="<spring:message code='workallocation.save' />" tabindex="0" />
                                        </c:if>

                                    </li>
                                </ol>
                            </div>
                            <div class="displaycolumn-40">
                                <ol>
                                    <li>
                                        <label><spring:message code="workallocation.filterby.duedate"/> :</label>
                                        <form:select path="allocatedItemsSearchForm.rangeDueDate" id="rangeDueDate" tabindex="7" onchange=" changeDateOption(this.value, 'allocacteddueDateSpan'); return false; ">
                                            <option value="false" <c:if test="${form.allocatedItemsSearchForm.rangeDueDate == false}"> selected </c:if>><spring:message code='certsearch.on'/></option>
                                            <option value="true" <c:if test="${form.allocatedItemsSearchForm.rangeDueDate == true}"> selected </c:if>><spring:message code='certsearch.between'/></option>
                                        </form:select>

                                        <form:input  path="allocatedItemsSearchForm.aweDueDate1" id="aweDueDate1" type="date" />


                                        <span id="allocacteddueDateSpan" <c:choose><c:when test="${form.allocatedItemsSearchForm.rangeDueDate == true}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>><spring:message code='certsearch.and'/> &nbsp;
						                        	<form:input type="date"  path="allocatedItemsSearchForm.awedueDate2" id="awedueDate2" />
						                    </span>
                                    </li>
                                    <li>
                                        <label><spring:message code="workallocation.filterby.production_date"/> :</label>
                                        <form:select path="allocatedItemsSearchForm.rangeProdDate" id="rangeProdDate" tabindex="7" onchange=" changeDateOption(this.value, 'prodDateSpan'); return false; ">
                                            <option value="false" <c:if test="${form.allocatedItemsSearchForm.rangeProdDate == false}"> selected </c:if>><spring:message code='certsearch.on'/></option>
                                            <option value="true" <c:if test="${form.allocatedItemsSearchForm.rangeProdDate == true}"> selected </c:if>><spring:message code='certsearch.between'/></option>
                                        </form:select>

                                        <form:input  path="allocatedItemsSearchForm.aweProdDate1" id="aweProdDate1" type="date" />


                                        <span id="prodDateSpan" <c:choose><c:when test="${form.allocatedItemsSearchForm.rangeProdDate == true}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>><spring:message code='certsearch.and'/> &nbsp;
						                        	<form:input type="date"  path="allocatedItemsSearchForm.aweProdDate1" id="aweProdDate1" />
						                    </span>
                                    </li>

                                    <li>
                                        <label><spring:message code="workallocation.filterby.clientCompany"/>: </label>
                                        <form:select  id="clientCompanyId_2" path="allocatedItemsSearchForm.clientCompanyId" onchange="populatAllocatedClientSubdivSelect()">
                                            <form:option value=""> ---- ALL ----</form:option>
                                            <c:forEach items="${clientCompanies}" var="cltComp">
                                                <form:option value="${cltComp.coid}"> ${cltComp.coname} </form:option>
                                            </c:forEach>
                                        </form:select>

                                    </li>

                                    <li>
                                        <label><spring:message code="workallocation.filterby.clientsubdiv"/> :</label>
                                        <form:select id="clientSubdivId_2" path="allocatedItemsSearchForm.clientSubdivId">
                                            <form:option value=""> ---- ALL ----</form:option>

                                        </form:select>
                                    </li>

                                    <li>
                                        <label><spring:message code="workallocation.filterby.ServiceType"/> :</label>
                                        <form:select path="allocatedItemsSearchForm.serviceTypeId">
                                            <form:option value=""> ---- ALL ----</form:option>
                                            <c:forEach items="${serviceTypes}" var="st">
                                                <form:option value="${st.serviceTypeId}"> <cwms:besttranslation translations="${st.shortnameTranslation}" /> </form:option>
                                            </c:forEach>
                                        </form:select>
                                    </li>
                                    <li>
                                        <label> Barcode :</label>
                                        <form:input type="text" path="allocatedItemsSearchForm.barcode" />
                                    </li>
                                </ol>
                            </div>
                        </fieldset>
                        <div>
                            <c:if test="${not empty allocatedItems}">
                                <fieldset>
                                    <div class="infobox">
                                        <c:set var="allocatedcounter" value="0"/>
                                        <c:forEach  items="${allocatedItems.keySet()}" var="date" >
                                                <table  class="default2" >
                                                    <tbody>
                                                        <tr>
                                                            <td class="wfjobno center">
                                                                ${date.getDayOfWeek()}
                                                            </td>
                                                            <td class="wfjobno center">
                                                                    <fmt:formatDate value="${date}" type="date" dateStyle="Short"/>
                                                            </td>
                                                            <td class="wfjobno center">
                                                                <a role="button" class="imagelink" onclick="showAlloctedItems('${date}');">
                                                                    <img src="img/icons/items.png" width="16" height="16" class="image_inline">
                                                                </a>
                                                            </td>
                                                            <td class="wfjobno center">
                                                                <c:forEach items="${tottalItemsPerdate.keySet()}" var="itemsSum">
                                                                    <c:if test="${date eq itemsSum}">
                                                                        ${tottalItemsPerdate.get(itemsSum)} Items
                                                                    </c:if>
                                                                </c:forEach>
                                                            </td>
                                                            <td class="wfjobno center">
                                                                <c:forEach items="${SumOfTmcsByDate.keySet()}" var="tmcsDate">
                                                                    <c:if test="${date eq tmcsDate}">
                                                                        ${SumOfTmcsByDate.get(tmcsDate)}
                                                                    </c:if>
                                                                </c:forEach>
                                                            </td>
                                                        </tr>
                                                        <tr>

                                                            <c:forEach items="${allocatedItems.get(date).keySet()}" var="allocatedJob">
                                                                    <table class="default2 hid" id="${date}">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="wfjobno">
                                                                                <input type="checkbox" checked="true" />
                                                                                <links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${allocatedJob.jobNo}" jobid="${allocatedJob.jobId}" copy="${false}"/>
                                                                            </td>
                                                                            <td class="wfconame">
                                                                                <links:companyLinkInfo companyId="${allocatedJob.clientCompanyId}" onStop="${allocatedJob.clientCompanyOnStop}" rowcount="${rowcount}" companyName="${allocatedJob.clientCompanyName}" active="${allocatedJob.clientCompanyActive}" copy="${false}"/>
                                                                            </td>
                                                                            <td class="wfcontact">
                                                                                <links:contactLinkInfo contactName="${allocatedJob.clientContactName}" personId="${allocatedJob.clientContactId}" active="${allocatedJob.clientContactActive}" rowcount="${rowcount}" />
                                                                            </td>
                                                                            <td class="wfstate">
                                                                                    ${allocatedJob.jobStatus}
                                                                            </td>
                                                                            <td class="wfclientref">
                                                                                    ${allocatedJob.transportOutName}
                                                                            </td>
                                                                            <td class="wfdatein">
                                                                                <fmt:formatDate value="${allocatedJob.regDate}" type="date" dateStyle="SHORT" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="6" style="padding: 0;" >
                                                                                <table class="default4 child-table" style="border: 0; cellpadding: 0; cellspacing: 0;">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th class="wfitem" scope="col"> Trescal ID </th>
                                                                                            <th class="wfitem" scope="col"><spring:message code="item"/></th>
                                                                                            <th class="wfinstr" scope="col"><spring:message code="instmodelname"/></th>
                                                                                            <th class="wfserial" scope="col"><spring:message code="serialno"/></th>
                                                                                            <th class="wfplant" scope="col"><spring:message code="plantno"/></th>
                                                                                            <th class="wfcaltype" scope="col"><spring:message code="servicetype"/></th>
                                                                                            <th class="wfproc" scope="col"><spring:message code="capability"/></th>
                                                                                            <th class="wfproc" scope="col"> <spring:message code="workallocation.tmc"/></th>
                                                                                            <th class="wfproc" scope="col"> <spring:message code="workflow.alloc"/></th>
                                                                                            <th class="wfstatus" scope="col"> <spring:message code="workallocation.proudction_date"/> </th>
                                                                                            <th class="wfstatus" scope="col"> <spring:message code="workallocation.comment"/> </th>
                                                                                            <th class="wfduedate" scope="col"><spring:message code="duedate"/></th>
                                                                                            <th class="wfduedate" scope="col"><spring:message code="aliasgroup.action"/></th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <c:forEach items="${allocatedItems.get(date).get(allocatedJob)}" var="allocatedItem" varStatus="loop">
                                                                                            <tr>
                                                                                                <td>
                                                                                                        ${allocatedItem.barcode}
                                                                                                </td>
                                                                                                <td>
                                                                                                    <form:checkbox path="allocatedItems.row[${allocatedcounter}].check"/>
                                                                                                    <form:hidden path="allocatedItems.row[${allocatedcounter}].allocationId" value="${allocatedItem.allocationId}"/>
                                                                                                    <form:hidden path="allocatedItems.row[${allocatedcounter}].allocationMessageId" value="${allocatedItem.allocationMessageId}"/>
                                                                                                    <links:jobitemLinkInfo jobItemNo="${allocatedItem.jobItemNo}" jobItemCount="${allocatedItem.jobItemCount}" jobItemId="${allocatedItem.jobItemId}" defaultPage="jiactions.htm"/>
                                                                                                </td>
                                                                                                <td>
                                                                                                        ${allocatedItem.modelName}
                                                                                                </td>
                                                                                                <td>
                                                                                                        ${allocatedItem.serialNo}

                                                                                                </td>
                                                                                                <td>
                                                                                                        ${allocatedItem.plantNo}
                                                                                                </td>
                                                                                                <td class="center">
                                                                                                        ${allocatedItem.serviceTypeName}
                                                                                                </td>
                                                                                                <td class="center">
                                                                                                        ${allocatedItem.procedureReference}
                                                                                                </td>
                                                                                                <td class="center">
                                                                                                        ${allocatedItem.tmc}
                                                                                                </td>
                                                                                                <td>
                                                                                                    <links:contactLinkInfo contactName="${allocatedItem.allocatedForName}" personId="${allocatedItem.allocatedForId}" active="${allocatedItem.clientContactActive}" rowcount="${rowcount}" />
                                                                                                </td>
                                                                                                <td class="center">
                                                                                                    <form:input class="${allocatedJob.jobId}-allitemDate" id="${allocatedJob.jobId}-alldate-${loop.index}" path="allocatedItems.row[${allocatedcounter}].production_date" type="date" value="${allocatedItem.production_date}"/>
                                                                                                    <a role="button" class="imagelink" onclick="copyDownForAllocateItems('${allocatedJob.jobId}-alldate-${loop.index}',${loop.index},'${allocatedJob.jobId}-alldate','${allocatedJob.jobId}-allitemDate');" >
                                                                                                        <img src="img/icons/arrow_down.png" width="10" height="16" alt="Arrow down" title="Arrow down">
                                                                                                    </a>
                                                                                                    <a role="button" class="imagelink" onclick="copyUpForAllocateItems('${allocatedJob.jobId}-alldate-${loop.index}',${loop.index},'${allocatedJob.jobId}-alldate','${allocatedJob.jobId}-allitemDate');">
                                                                                                        <img src="img/icons/arrow_up.png" width="10" height="16" alt="Arrow Up" title="Arrow Up">
                                                                                                    </a>
                                                                                                </td>
                                                                                                <td class="center">
                                                                                                    <form:input  class="${allocatedJob.jobId}-allitemCmt" type="text" size="30" id="${allocatedJob.jobId}-allcmt-${loop.index}" path="allocatedItems.row[${allocatedcounter}].item_comment" value="${allocatedItem.item_comment}"/>
                                                                                                    <a role="button" class="imagelink" onclick="copyDownForAllocateItems('${allocatedJob.jobId}-allcmt-${loop.index}',${loop.index},'${allocatedJob.jobId}-allcmt','${allocatedJob.jobId}-allitemCmt');">
                                                                                                        <img src="img/icons/arrow_down.png" width="10" height="16" alt="Arrow down" title="Arrow down">
                                                                                                    </a>
                                                                                                    <a role="button" class="imagelink" onclick="copyUpForAllocateItems('${allocatedJob.jobId}-allcmt-${loop.index}',${loop.index},'${allocatedJob.jobId}-allcmt','${allocatedJob.jobId}-allitemCmt');">
                                                                                                        <img src="img/icons/arrow_up.png" width="10" height="16" alt="Arrow Up" title="Arrow Up">
                                                                                                    </a>
                                                                                                </td>
                                                                                                <td class="center">
                                                                                                    <fmt:formatDate value="${allocatedItem.dueDate}" type="date" dateStyle="SHORT"/>
                                                                                                </td>
                                                                                                <td class="center">
                                                                                                    <img src="img/icons/form_edit.png" width="16" height="16" alt=""title="" onclick="reallocateitem(${allocatedItem.allocationId},${allocatedItem.allocatedForId},${allocatedItem.procedureId},${allocatedItem.allocationMessageId})"  />

                                                                                                    <img src="img/icons/delete.png" width="16" height="16" alt=""title="" onclick="deallocatitem(${allocatedItem.allocationId});"  />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <c:set var="allocatedcounter" value="${allocatedcounter + 1}"/>
                                                                                        </c:forEach>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                            </c:forEach>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                        </c:forEach>
                                    </div>
                                </fieldset>
                            </c:if>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>

    </jsp:body>
</t:crocodileTemplate>