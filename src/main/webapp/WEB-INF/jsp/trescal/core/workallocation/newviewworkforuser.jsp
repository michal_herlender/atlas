<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<t:crocodileTemplate>

    <jsp:attribute name="scriptPart">
		<script src='script/trescal/core/workallocation/workallocation.js'></script>
	</jsp:attribute>

    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="workflow.viewworkforuser"/></span>
    </jsp:attribute>

    <jsp:body>

    <div id="workallocation">
        <form:form modelAttribute="form" method="post">
        <div class="infobox" >
            <fieldset>
                <div class="displaycolumn-60">
                    <ol>
                        <li>
                            <label><spring:message code="workallocation.filterby.jobtype"/> :</label>
                            <form:select path="jobtype" items="${jobtypes}" itemValue="name" itemLabel="description"/>
                        </li>

                        <li>
                            <label><spring:message code="workallocation.filterby.lab"/> :</label>
                            <form:select id="laboratoryId_2" path="laboratoryId" onchange="populateAllocatedSubLabSelect()">
                                <form:option value=""> ---- ALL ----</form:option>
                                <c:forEach items="${labs}" var="lab">
                                    <form:option  value="${lab.deptid}">
                                        ${lab.name}
                                    </form:option>
                                </c:forEach>
                            </form:select>
                        </li>

                        <li>
                            <label><spring:message code="workallocation.filterby.sublab"/> :</label>
                            <form:select id="subLabId_2" path="subLabId">
                                <form:option value=""> ---- ALL ----</form:option>
                            </form:select>

                        </li>

                        <li>
                            <label><spring:message code="workallocation.filterby.subfamily"/> :</label>
                            <form:input path="desc" id="desc" tabindex="6"/>
                            <form:hidden path="descId" />
                        </li>
                        <li>

                        </li>
                        <li>
                            <input type="submit" name="submit" id="submit" class="float-left" value="<spring:message code='workallocation.submit' />" tabindex="24" />
                        </li>
                    </ol>
                </div>
                <div class="displaycolumn-40">
                    <ol>
                        <li>
                            <label><spring:message code="workallocation.filterby.duedate"/> :</label>
                            <form:select path="rangeDueDate" id="rangeDueDate" tabindex="7" onchange=" changeDateOption(this.value, 'allocacteddueDateSpan'); return false; ">
                                <option value="false" <c:if test="${form.rangeDueDate == false}"> selected </c:if>><spring:message code='certsearch.on'/></option>
                                <option value="true" <c:if test="${form.rangeDueDate == true}"> selected </c:if>><spring:message code='certsearch.between'/></option>
                            </form:select>

                            <form:input  path="aweDueDate1" id="aweDueDate1" type="date" />


                            <span id="allocacteddueDateSpan" <c:choose><c:when test="${form.rangeDueDate == true}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>><spring:message code='certsearch.and'/> &nbsp;
						                        	<form:input type="date"  path="awedueDate2" id="awedueDate2" />
						                    </span>
                        </li>
                        <li>
                            <label><spring:message code="workallocation.filterby.production_date"/> :</label>
                            <form:select path="rangeProdDate" id="rangeProdDate" tabindex="7" onchange=" changeDateOption(this.value, 'prodDateSpan'); return false; ">
                                <option value="false" <c:if test="${form.rangeProdDate == false}"> selected </c:if>><spring:message code='certsearch.on'/></option>
                                <option value="true" <c:if test="${form.rangeProdDate == true}"> selected </c:if>><spring:message code='certsearch.between'/></option>
                            </form:select>

                            <form:input  path="aweProdDate1" id="aweProdDate1" type="date" />


                            <span id="prodDateSpan" <c:choose><c:when test="${form.rangeProdDate == true}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>><spring:message code='certsearch.and'/> &nbsp;
						                        	<form:input type="date"  path="aweProdDate1" id="aweProdDate1" />
						                    </span>
                        </li>

                        <li>
                            <label><spring:message code="workallocation.filterby.clientCompany"/>: </label>
                            <form:select  id="clientCompanyId_2" path="clientCompanyId" onchange="populatClientSubdivSelect()">
                                <form:option value=""> ---- ALL ----</form:option>
                                <c:forEach items="${clientCompanies}" var="cltComp">
                                    <form:option value="${cltComp.coid}"> ${cltComp.coname} </form:option>
                                </c:forEach>
                            </form:select>

                        </li>

                        <li>
                            <label><spring:message code="workallocation.filterby.clientsubdiv"/> :</label>
                            <form:select id="clientSubdivId_2" path="clientSubdivId">
                                <form:option value=""> ---- ALL ----</form:option>

                            </form:select>
                        </li>

                        <li>
                            <label><spring:message code="workallocation.filterby.ServiceType"/> :</label>
                            <form:select path="serviceTypeId">
                                <form:option value=""> ---- ALL ----</form:option>
                                <c:forEach items="${serviceTypes}" var="st">
                                    <form:option value="${st.serviceTypeId}"> <cwms:besttranslation translations="${st.shortnameTranslation}" /> </form:option>
                                </c:forEach>
                            </form:select>
                        </li>
                    </ol>
                </div>
            </fieldset>
            <div>
                <fieldset>
                    <div class="infobox">
                        <c:forEach  items="${allocatedItems.keySet()}" var="date" >
                            <table  class="default2" >
                                <tbody>
                                    <tr>
                                        <td class="wfjobno">
                                                ${date.getDayOfWeek()}
                                        </td>
                                        <td class="wfjobno">
                                            <fmt:formatDate value="${date}" type="date" dateStyle="Short"/>
                                        </td>
                                        <td class="wfjobno">
                                            <a role="button" class="imagelink" onclick="showAlloctedItems('${date}');">
                                                <img src="img/icons/items.png" width="16" height="16" class="image_inline">
                                            </a>
                                        </td>
                                        <td class="wfjobno">
                                            <c:forEach items="${tottalItemsPerdate.keySet()}" var="itemsSum">
                                                <c:if test="${date eq itemsSum}">
                                                    ${tottalItemsPerdate.get(itemsSum)} Items
                                                </c:if>
                                            </c:forEach>
                                        </td>
                                    </tr>
                                    <tr>
                                        <c:forEach items="${allocatedItems.get(date).keySet()}" var="allocatedJob">
                                            <table class="default2 hid" id="${date}">
                                                <tbody>
                                                    <tr>
                                                        <td class="wfjobno">
                                                            <links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${allocatedJob.jobNo}" jobid="${allocatedJob.jobId}" copy="${false}"/>
                                                        </td>
                                                        <td class="wfconame">
                                                            <links:companyLinkInfo companyId="${allocatedJob.clientCompanyId}" onStop="${allocatedJob.clientCompanyOnStop}" rowcount="${rowcount}" companyName="${allocatedJob.clientCompanyName}" active="${allocatedJob.clientCompanyActive}" copy="${false}"/>
                                                        </td>
                                                        <td class="wfcontact">
                                                            <links:contactLinkInfo contactName="${allocatedJob.clientContactName}" personId="${allocatedJob.clientContactId}" active="${allocatedJob.clientContactActive}" rowcount="${rowcount}" />
                                                        </td>
                                                        <td class="wfstate">
                                                                ${allocatedJob.jobStatus}
                                                        </td>
                                                        <td class="wfclientref">
                                                                ${allocatedJob.transportOutName}
                                                        </td>
                                                        <td class="wfdatein">
                                                            <fmt:formatDate value="${allocatedJob.regDate}" type="date" dateStyle="SHORT" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6" style="padding: 0;" >
                                                            <table class="default4 child-table" style="border: 0; cellpadding: 0; cellspacing: 0;">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="wfitem" scope="col"> Trescal ID </th>
                                                                        <th class="wfitem" scope="col"><spring:message code="item"/></th>
                                                                        <th class="wfinstr" scope="col"><spring:message code="instmodelname"/></th>
                                                                        <th class="wfserial" scope="col"><spring:message code="serialno"/></th>
                                                                        <th class="wfplant" scope="col"><spring:message code="plantno"/></th>
                                                                        <th class="wfcaltype" scope="col"><spring:message code="servicetype"/></th>
                                                                        <th class="wfproc" scope="col"><spring:message code="capability"/></th>
                                                                        <th class="wfstatus" scope="col"> <spring:message code="workallocation.proudction_date"/> </th>
                                                                        <th class="wfstatus" scope="col"> <spring:message code="workallocation.comment"/> </th>
                                                                        <th class="wfduedate" scope="col"><spring:message code="duedate"/></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <c:forEach items="${allocatedItems.get(date).get(allocatedJob)}" var="allocatedItem" varStatus="loop">
                                                                        <tr>
                                                                            <td>
                                                                                    ${allocatedItem.barcode}
                                                                            </td>
                                                                            <td>
                                                                                <links:jobitemLinkInfo jobItemNo="${allocatedItem.jobItemNo}" jobItemCount="${allocatedItem.jobItemCount}" jobItemId="${allocatedItem.jobItemId}" defaultPage="jiactions.htm"/>
                                                                            </td>
                                                                            <td>
                                                                                    ${allocatedItem.modelName}
                                                                            </td>
                                                                            <td>
                                                                                    ${allocatedItem.serialNo}

                                                                            </td>
                                                                            <td>
                                                                                    ${allocatedItem.plantNo}
                                                                            </td>
                                                                            <td class="center">
                                                                                    ${allocatedItem.serviceTypeName}
                                                                            </td>
                                                                            <td class="center">
                                                                                    ${allocatedItem.procedureReference}
                                                                            </td>
                                                                            <td class="center">
                                                                                <fmt:formatDate value="${allocatedItem.production_date}" type="date" dateStyle="SHORT"/>
                                                                            </td>
                                                                            <td class="center">
                                                                                    ${allocatedItem.item_comment}
                                                                            </td>
                                                                            <td class="center">
                                                                                <fmt:formatDate value="${allocatedItem.dueDate}" type="date" dateStyle="SHORT"/>
                                                                            </td>
                                                                        </tr>
                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </c:forEach>
                                    </tr>
                                </tbody>
                            </table>
                        </c:forEach>
                    </div>
                </fieldset>
            </div>
        </form:form>
    </div>

    </jsp:body>
</t:crocodileTemplate>