<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:overlayTemplate bodywidth="480">

    <jsp:attribute name="scriptPart">
        <script src='script/trescal/core/workallocation/workallocation.js'></script>
    </jsp:attribute>
    <jsp:body>
        <div id="allocateItemOverlay">
                <fieldset>
                    <ol>
                        <li>
                            <label>Subdivision Engineer : </label>
                            <select id="engineer_overlay">
                                <c:forEach items="${subdivEngineer.keySet()}" var="engineer">
                                    <c:forEach var="name" items="${subdivEngineer.get(engineer)}">
                                        <option value="${engineer}"> ${name} </option>
                                    </c:forEach>
                                </c:forEach>
                            </select>
                        </li>
                        <li>
                            <label>commments : </label>
                            <input type="text" size="50"  id="cmt_overlay">
                        </li>
                        <li>
                            <input type="button"  onclick="submitoverlay(${allocationId},${allocationMessageId});" class="float-left" value="<spring:message code='workallocation.submit' />" tabindex="24" />
                        </li>
                    </ol>
                </fieldset>
        </div>
    </jsp:body>

</t:overlayTemplate>

