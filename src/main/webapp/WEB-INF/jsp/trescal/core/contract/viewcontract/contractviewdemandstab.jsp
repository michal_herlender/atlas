<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="script/trescal/core/contract/viewcontract/contractviewdemandstab.js"></script>
<script src="script/thirdparty/DataTables/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="script/thirdparty/DataTables/datatables.min.css"/>

<div class="infobox" id="viewdemands">
	<table class="default2" id="demandTable">
		<thead>
			<tr>
		        <td colspan="6">
		            <div class="float-right">
		                <button id="adddemandbutton">
		                    <spring:message var="addTechReq" code="contract.addtechreq"/>
		                    ${addTechReq}
		                </button>
		            </div>
		        </td>
		    </tr>
			<tr>
				<th><spring:message code="domain"/></th>
				<th><spring:message code="sub-family"/></th>
				<th><spring:message code="additionalDemand.cleaning"/></th>
				<th><spring:message code="additionalDemand.sealing"/></th>
				<th><spring:message code="additionalDemand.engraving"/></th>
				<th></th>
			</tr>
		</thead>
	</table>
</div>

<div id="adddemanddialog" style="display: none; overflow: auto" title="${addTechReq}">
	<fieldset>
	    <ol>
	    	<li>
	        	<label><spring:message code="domain"/></label>
	        	<input type="hidden" id="domainId"/>
	        	<input type="text" id="domain"/>
	        </li>
	        <li>
	        	<label><spring:message code="sub-family"/></label>
	        	<input type="hidden" id="subfamilyId"/>
	        	<input type="text" id="subfamily"/>
	        </li>
	        <li>
	        	<label><spring:message code="additionalDemand.cleaning"/></label>
	        	<input type="checkbox" id="cleaning"/>
	        </li>
	        <li>
	        	<label><spring:message code="additionalDemand.sealing"/></label>
	        	<input type="checkbox" id="sealing"/>
	        </li>
	        <li>
	        	<label><spring:message code="additionalDemand.engraving"/></label>
	        	<input type="checkbox" id="engraving"/>
	        </li>
	    </ol>
	</fieldset>
</div>