<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="script/trescal/core/contract/viewcontract/contractviewinstructionstab.js"></script>

<div class="infobox" id="viewinstructions">

    <fieldset>
        <ol>
            <li>
                <button id="addinstruction" style="float: right"><spring:message code="add"/></button>
            </li>

            <li id="instructionsEmpty" class="${instructions.isEmpty() ? "vis" : "hid"}">
                <div class="center bold"><spring:message code="company.noinstructionstodisplay"/></div>
            </li>

            <c:forEach var="instruction" items="${instructions}">
                <li id="instruction${instruction.instructionid}">
                    <!-- this div is floated left and displays group name and description -->
                    <div class="notes_note instructions_note">
                        <strong>
                            <spring:message code="${instruction.instructiontype.messageCode}"/>:
                        </strong><br />
                        <c:out value="${instruction.instruction}"/>
                    </div>
                    <!-- end float left div -->
                    <div class="notes_editor">
                        <c:if test="${not empty instruction.lastModifiedBy}">
                            ${instruction.lastModifiedBy.name}
                        </c:if>
                        <br/>
                    </div>
                    <!-- this div is floated right and displays the edit and delete links -->
                    <div class="notes_edit instructions_edit">
                        <a href="#" id="editinstruction" data-instructionid="${instruction.instructionid}">
                            <img src="img/icons/note_edit.png" width="16" height="16" alt="<spring:message code="company.editinstruction"/>" title="<spring:message code="company.editinstruction"/>" />
                        </a>
                        <a href="#" id="deleteinstruction"  data-instructionid="${instruction.instructionid}">
                            <img src="img/icons/note_delete.png" width="16" height="16" alt="<spring:message code="company.deleteinstruction"/>" title="<spring:message code="company.deleteinstruction"/>" />
                        </a>
                    </div>
                    <!--  end of float right  div -->

                    <!--  clear floats and restore page flow  -->
                    <div class="clear"></div>
                </li>
            </c:forEach>
        </ol>
    </fieldset>
</div>

<div id="addeditinstructiondialog"  style="display: none;">
    <fieldset>
        <ol>
            <li>
                <label>Instruction Type</label>
                <select id="instructiontype">
                    <c:forEach items="${instructiontypes}" var="instructionType">
                        <option value="${instructionType.key}"><spring:message code="${instructionType.value}"/></option>
                    </c:forEach>
                </select>
            </li>
            <li>
                <label><spring:message code="system.includeonclientdeliverynotes"/></label>
                <input type="checkbox" id="clientdelivery" />
                <spring:message var="instructionTypeCarriage" code="instructiontype.carriage" />  
                (<spring:message code="system.usedforinstructiontype" arguments="${instructionTypeCarriage}" />)
            </li>
            <li>
                <label><spring:message code="system.includeonsupplierdeliverynotes"/></label>
                <input type="checkbox" id="supplierdelivery" />
                (<spring:message code="system.usedforinstructiontype" arguments="${instructionTypeCarriage}" />)
            </li>
            <li>
                <label>Instruction</label>
                <textarea id="instructiontext" cols="70" rows="8"></textarea>
            </li>
        </ol>
    </fieldset>

</div>
