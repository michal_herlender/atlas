<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
        <script src='script\trescal\core\contract\viewcontract\contractview.js'></script>
	</jsp:attribute>
    <jsp:attribute name="header"><span class="headtext"><spring:message code="contract.view"/> : <c:out value="${contractdetails.contractNumber}"/></span></jsp:attribute>
    <jsp:body>
        <div id="viewcontract" class="infobox">
            <fieldset id="viewcontractpage">
                <legend><spring:message code="contract.view"/></legend>
                <div class="displaycolumn">
                    <ol class="viewInstrumentTechnicalList">
                        <li>
                            <label><spring:message code="viewjob.valid"/></label>
                            <span>
							<fmt:formatDate type="date" dateStyle="medium" value="${contractdetails.startDate}"/>
                            &nbsp<spring:message code="to"/>&nbsp
                            <fmt:formatDate type="date" dateStyle="medium" value="${contractdetails.endDate}"/>
                            </span>
                        </li>
                        <li>
                            <label><spring:message code="company"/>:</label>
                            <span id="clientCompanyName"><c:out value="${contractdetails.clientCompanyName}"/></span>
                            <input type="hidden" id="clientCompanyId" value="${contractdetails.clientCompanyId}"/>
                        </li>
                        <li>
                            <label><spring:message code="contact"/>:</label>
                            <span><c:out value="${contractdetails.clientContactName}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="contract.invoicingmethod"/>:</label>
                            <span><c:out value="${contractdetails.invoicingMethodName}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="contract.trescalmanager"/>:</label>
                            <span><c:out value="${contractdetails.trescalManagerName}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="quotation"/>:</label>
                            <span>
                                <c:if test="${contractdetails.quotationId > 0}">
                                    <a href="viewquotation.htm?id=${contractdetails.quotationId}"><c:out value="${contractdetails.quotationNumber}"/></a>
                                </c:if>
                            </span>
                        </li>
                        <li>
                            <label><spring:message code="viewjob.bponum"/></label>
                            <span>
                                <c:if test="${contractdetails.bpoId > 0}">
                                    <a href="viewbpousage.htm?poid=${contractdetails.bpoId}"><c:out value="${contractdetails.bpoNumber}"/></a>
                                </c:if>
                            </span>
                        </li>
                        <li>
                            <label><spring:message code="contract.software"/>:</label>
                            <span><c:out value="${contractdetails.softwareName}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="contract.dbmanagement"/>:</label>
                            <span><c:out value="${contractdetails.dbManagement}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="contract.transfertype"/>:</label>
                            <span><c:out value="${contractdetails.transferTypeName}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="contract.legalContractNumber"/>:</label>
                            <span><c:out value="${contractdetails.legalContractNo}"/></span>
                        </li>

                    </ol>
                </div>
                <div class="displaycolumn">
                    <ol class="viewInstrumentTechnicalList">
                        <div class="periodicInvoicing" hidden>
                            <li>
                                <label><spring:message code="contract.premium"/>:</label>
                                <span>${contractdetails.premiumWithCurrency}</span>
                            </li>
                            <li>
                                <label><spring:message code="contract.outstandingpremium"/>:</label>
                                <span>${contractdetails.outstandingPremiumWithCurrency}</span>
                            </li>
                            <li>
                                <label><spring:message code="invoicefrequency"/></label>
                                <span><c:out value="${contractdetails.invoiceFrequency}"/></span>
                            </li>
                        </div>
                        <li>
                            <label><spring:message code="paymentmode"/>:</label>
                            <span><c:out value="${contractdetails.paymentModeDescription}"/></span>
                        </li>
                        
                        <li>
                            <label><spring:message code="paymentterms"/>:</label>
                            <span><c:out value="${contractdetails.paymentTerm}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="contract.invoicingmanager"/>:</label>
                            <span><c:out value="${contractdetails.invoicingManagerName}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="description"/>:</label>
                            <span><c:out value="${contractdetails.description}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="contract.instrumentlisttype"/>:</label>
                            <span><c:out value="${contractdetails.instrumentListType}"/></span>
                        </li>
                        <li>
                            <label><spring:message code="file"/></label>
                            <c:choose>
                                <c:when test="${contractdetails.hasDocument}">
                                    <a href="contractfile/${contractdetails.contractId}" target="_blank">
                                        <spring:message code="viewprebooking.downloadfile"/>
                                    </a>
                                </c:when>
                                <c:otherwise>
                                    <spring:message code="recall.nofile"/>
                                </c:otherwise>
                            </c:choose>

                        </li>
                         <li>
                         	<label><spring:message code="systemdefault.calibrationwarranty"/> (<spring:message code="days"/>):</label>
                         	<span><c:out value="${contractdetails.warrantyCalibration}"/></span>
                         </li>
                         <li>
                         	<label><spring:message code="systemdefault.repairwarranty"/> (<spring:message code="days"/>) :</label>
                         	<span><c:out value="${contractdetails.warrantyRepair}"/></span>
                         </li>
                    </ol>
                </div>
            </fieldset>
        </div>


        <div id="subnav">
            <input type="hidden" value="${contractdetails.contractId}" id="contractid"/>
            <dl>
                <dt>
                    <a href="contractaddedit.htm?id=${contractdetails.contractId}">
                        <spring:message code="edit"/>
                    </a>
                </dt>
                <dt>
                    <a href="#" id="subdivs" class="menubutton">
                        <spring:message code="include.subdivisions"/>
                    </a>
                </dt>
                <dt>
                    <a href="#" id="instruments" class="menubutton">
                        <spring:message code="instruments"/>
                    </a>
                </dt>
                <dt>
                    <a href="#" id="servicetypes" class="menubutton">
                        <spring:message code="contract.validservicetypes"/>
                    </a>
                </dt>
                <dt>
                    <a href="#" id="instructions" class="menubutton">
                        <spring:message code="instructions"/>
                    </a>
                </dt>
               <%-- <dt>
                    <a href="#" id="markups" class="menubutton">
                        <spring:message code="contract.markup"/>
                    </a>
                </dt>
                <dt>
                    <a href="#" id="turnaround" class="menubutton">
                        <spring:message code="jicontractreview.turnaround"/>
                    </a>
                </dt>
                <dt>
                    <a href="#" id="history" class="menubutton">
                        <spring:message code="company.history"/>
                    </a>
                </dt>--%>
                <dt>
                    <a href="#" id="technicalrequirements" class="menubutton">
                        <spring:message code="contract.techreqs"/>
                    </a>
                </dt>
                <%--<dt>
                    <a href="#" id="invoices" class="menubutton">
                        <spring:message code="invoice.invoices"/>
                    </a>
                </dt>--%>
            </dl>
        </div>

        <div id="tabarea"></div>

    </jsp:body>
</t:crocodileTemplate>