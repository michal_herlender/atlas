<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>

	<jsp:attribute name="header">
		<span class="headtext">
            <c:choose>
                <c:when test="${command.editMode}">
                    <spring:message code="contract.edit"/>
                </c:when>
                <c:otherwise>
                    <spring:message code="contract.add"/>
                </c:otherwise>
            </c:choose>
        </span>

	</jsp:attribute>
    <jsp:attribute name="scriptPart">
		<script src='script\trescal\core\contract\contractaddedit.js'></script>
	</jsp:attribute>
    <jsp:body>
        <t:showErrors path="command.*" showFieldErrors="false"/>
        <div id="addcontract" class="infobox">

            <form:form id="addcontractform" enctype="multipart/form-data">
                <form:hidden path="currencyId" id="currencyId"/>
                <fieldset id="addcontractpage">
                    <c:choose>
                        <c:when test="${command.editMode}">
                            <legend><spring:message code="contract.edit"/> - ${command.contractNumber}</legend>
                        </c:when>
                        <c:otherwise>
                            <legend><spring:message code="contract.add"/></legend>
                        </c:otherwise>
                    </c:choose>

                        <%-- Left Column --%>
                    <div class="displaycolumn">
                        <ol>
                            <li>
                                <label><spring:message code="company"/>:</label>
                                <c:choose>
                                    <c:when test="${command.editMode}">
                                        <span>${command.clientCompanyName}</span>
                                        <form:hidden path="clientCompanyId"/>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="float-left extendPluginInput">
                                            <div class="compSearchJQPlugin">
                                                <input type="hidden" name="field" value="clientCompanyId" id="coid"/>
                                                <input type="hidden" name="compCoroles" value="client"/>
                                                <input type="hidden" name="tabIndex" value="1"/>
                                                <input type="hidden" name="triggerMethod" value="true"/>
                                                <!-- company results listed here -->
                                            </div>
                                        </div>
                                        <!-- clear floats and restore page flow -->
                                        <div class="clear"></div>
                                        <form:errors path="clientCompanyId" cssClass="attention"/>
                                    </c:otherwise>
                                </c:choose>
                            </li>
                            <li>
                                <label><spring:message code="contract.invoicingmethod"/>:</label>
                                <c:choose>
                                    <c:when test="${command.editMode}">
                                        <span>${command.invoicingMethodName}</span>
                                    </c:when>
                                    <c:otherwise>
                                        <%-- Select currently disabled 'Individual' is the only invoicing method currently supported
                                        <form:select path="invoicingMethod" items="${invoicingmethods}" itemLabel="value"
                                                     itemValue="key"/>
                                                     --%>
                                        <span><c:out value="${invoicingmethods.get(0).value}"/></span>
                                        <form:hidden path="invoicingMethod" value="${invoicingmethods.get(0).key}"/>
                                    </c:otherwise>
                                </c:choose>

                            </li>
                            <li>
                                <label for="contact"><spring:message code="contact"/>:</label>
                                <form:select path="clientContactId" id="contact" items="${clientcompanycontacts}" itemLabel="value" itemValue="key"/>
                                <form:errors path="clientContactId" cssClass="attention"/>
                            </li>

                            <li>
                                <label><spring:message code="bpo.startdate"/>:</label>
                                <form:input path="startDate" autocomplete="off" type="date"/>
                                <form:errors path="startDate" cssClass="attention"/>
                            </li>
                            <li>
                                <label><spring:message code="jicertificates.duration"/>:</label>
                                <form:input path="duration"/>
                                <form:select path="durationUnit" id="unit" items="${units}" itemLabel="value"
                                             itemValue="key"/>
                                <form:errors path="duration" cssClass="attention"/>
                            </li>
                            <c:if test="${command.editMode}">
                                <li>
                                    <label><spring:message code="bpo.enddate"/>:</label>
                                    <form:input path="endDate" autocomplete="off" type="date"/>
                                </li>
                            </c:if>
                            <li>
                                <label><spring:message code="contract.trescalmanager"/>:</label>
                                <form:select path="trescalManagerId" items="${businesscompanycontacts}"
                                             itemLabel="value" itemValue="key"/>
                            </li>
                            <li>
                                <label><spring:message code="quotation"/>:</label>
                                <div class="float-left">
                                    <form:input path="quoteNumber" size="30" disabled="true" cssClass="thickbox"/>
                                    <img id="removeQuote" src="img/icons/delete.png" width="10" height="10" alt="<spring:message code='contract.removequote'/>" title="<spring:message code='contract.removequote'/>" onclick="removeQuotation();"/>
                                    <button id="linkQuote">
                                        <spring:message code="viewjob.linkquotat"/>
                                    </button>
                                    <form:hidden path="quoteId" />
                                </div>
                            </li>
                            <li>
                                <label><spring:message code="invoice.bpo"/></label>
                                <form:input path="bpoNumber" size="30" disabled="true"/>
                                <img id="removeBPOimg" src="img/icons/delete.png" width="10" height="10" alt="<spring:message code='contract.removebpo'/>" title="<spring:message code='contract.removebpo'/>" onclick="removeBPO();"/>
                                <button id="linkBpo">
                                    <spring:message code="contract.linkbpo"/>
                                </button>
                                <form:hidden path="bpoId"/>
                            </li>
                            <li>
                                <label><spring:message code="file"/>:</label>
                                <input type="file" name="file" />
                            </li>
                            <li>
                                <label><spring:message code="contract.software"/>:</label>
                                <form:select path="software" items="${softwaretypes}" itemLabel="value"
                                             itemValue="key"/>
                            </li>
                            <li>
                                <label><spring:message code="contract.dbmanagement"/>:</label>
                                <form:checkbox path="dbManagement"/>
                            </li>
                            <li>
                                <label><spring:message code="contract.transfertype"/>:</label>
                                <form:select path="transferType" items="${transfertypes}" itemLabel="value"
                                             itemValue="key"/>
                            </li>
                            <li>
                                <label><spring:message code="contract.legalContractNumber"/>:</label>
                                <form:input path="legalContractNo"/>
                            </li>
                            <li>
                                <label><spring:message code="description"/>:</label>
                                <form:textarea path="description"/>
                            </li>
                        </ol>
                    </div>


                        <%-- Right Column --%>
                    <div class="displaycolumn">
                        <ol>
                            <li>
                                <label><spring:message code="contract.instrumentlisttype"/>:</label>
                                <form:select path="instrumentListType" items="${listTypes}" itemLabel="value" itemValue="key"/>
                            </li>

                            <div id="periodicinvoicing" hidden>
                                <li>
                                    <label><spring:message code="contract.premium"/>:</label>
                                    <form:input path="premium"/> <span
                                        class="currencysymbol">${command.currencySymbol}</span>
                                </li>
                                <li>
                                    <label><spring:message code="contract.outstandingpremium"/>:</label>
                                    <form:input path="outstandingPremium"/> <span
                                        class="currencysymbol">${command.currencySymbol}</span>
                                </li>
                                <li>
                                    <label><spring:message code="invoicefrequency"/></label>
                                    <form:input path="invoiceFrequency"/>
                                    <span><spring:message code="months"/></span>
                                </li>
                            </div>
                            <li>
                                <label><spring:message code="paymentmode"/>:</label>
                                <form:select path="paymentModeId" items="${paymentmodes}" itemLabel="value"
                                             itemValue="key"/>
                            </li>
                            <li>
                                <label><spring:message code="paymentterms"/>:</label>
                                <form:select
                                        cssClass="float-left"
                                        id="paymentTerm"
                                        items="${paymentterms}"
                                        path="paymentTerm" />
                            </li>
                            <li>
                                <label><spring:message code="contract.invoicingmanager"/>:</label>
                                <form:select path="invoicingManagerId" items="${businesscompanycontacts}"
                                             itemLabel="value" itemValue="key"/>
                            </li>
                            <li>
                                <label><spring:message code="systemdefault.calibrationwarranty"/> (<spring:message code="days"/>):</label>
                                <form:input path="warrantyCalibration"/>
                            </li>
                            <li>
                                <label><spring:message code="systemdefault.repairwarranty"/> (<spring:message code="days"/>) :</label>
                                <form:input path="warrantyRepair"/>
                            </li>
                        </ol>

                    </div>
                    <c:choose>
                        <c:when test="${command.editMode}">
                            <spring:message code="update" var="buttonword"/>
                        </c:when>
                        <c:otherwise>
                            <spring:message code="add" var="buttonword"/>
                        </c:otherwise>
                    </c:choose>
                    <label>&nbsp</label>
                    <input type="submit" name="Submit" id="submit" value="${buttonword}"/>
                </fieldset>

            </form:form>
        </div>

        <div id="choosebpodialog"  style="display: none;">
            <table class="default2">
                <thead>
                <tr>
                    <th class="bponumber">
                        <spring:message code="viewjob.bponum"/>
                    </th>
                    <th class="date">
                        <spring:message code="bpo.startdate"/>
                    </th>
                    <th class="date">
                        <spring:message code="bpo.enddate"/>
                    </th>
                    <th class="value">
                        <spring:message code="value"/>
                    </th>
                    <th class="value">
                        <spring:message code="bpo.spentamount" />
                    </th>
                    <th class="action">
                    </th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <td colspan="6"></td>
                </tr>
                </tfoot>
                <tbody>
                <tr>
                    <td colspan="6"><spring:message code="contract.nobpos"/></td>
                </tr>
                </tbody>
            </table>
        </div>

    </jsp:body>
</t:crocodileTemplate>