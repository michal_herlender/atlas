<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script
	src="script/trescal/core/contract/viewcontract/contractviewinstrumentstab.js"></script>
<script src="script/thirdparty/DataTables/datatables.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="script/thirdparty/DataTables/datatables.min.css" />

<c:choose>
	<c:when test="${listtype eq 'ADDITIONAL'}">
		<spring:message var="headertext" code="contract.instrumentsaddition" />
		<spring:message var="noinstrumentstext"
			code="contract.noadditionalinstruments" />
	</c:when>
	<c:when test="${listtype eq 'EXCLUSIVE'}">
		<spring:message var="headertext" code="contract.instrumentsexclusive" />
		<spring:message var="noinstrumentstext" code="contract.noinstruments" />
	</c:when>
</c:choose>

<table class="default2" id="instrumentListTable">
	<thead>
		<tr>
			<td colspan="5">${headertext}
				<div class="float-right">
					<button id="addinstrumentbutton">
						<spring:message code="newjobitemsearch.addinstr" />
					</button>
					<button id="bulkaddbutton">
						<spring:message code="contract.bulkadd" />
					</button>
				</div>
			</td>
		</tr>
		<tr>
			<th><spring:message code="barcode" /></th>
			<th><spring:message code="viewinstrument.name" /></th>
			<th><spring:message code="serialnumber" /></th>
			<th><spring:message code="subdivision" /></th>
			<th>&nbsp</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
	</tfoot>
</table>


<%-- *****************  Dialogs ************************ --%>

<div id="instsearchdialog" style="display: none; overflow: auto">

	<fieldset>
		<ol>
			<li>
				<label><spring:message code="company" /></label> 
				<input	type="text" id="company" name="company" value="${companyName}" class="wideinput" /> 
				<input type="hidden" id="companyid" name="companyid" value="${companyId}" />
			</li>
			<li>
				<label><spring:message code="subdivision" /></label> 
				<select name="subdivid" id="subdivid">
					<option value="0"><spring:message code="contract.selectacomp" /></option>
				</select>
			</li>
		</ol>
	</fieldset>

</div>

<div id="instsearchresultsdialog" style="display: none;">
	<table class="default2" id="instSearchResultsTable">
		<thead>
			<tr>
				<th class="barcode" scope="col"><spring:message code="barcode" /></th>
				<th class="instrumentname" scope="col"><spring:message
						code="viewinstrument.name" /></th>
				<th class="serialno" scope="col"><spring:message
						code="serialnumber" /></th>
				<th class="subdiv" scope="col"><spring:message
						code="subdivision" /></th>
				<th class="action" scope="col">&nbsp</th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<td colspan="5">&nbsp</td>
			</tr>
		</tfoot>
	</table>
</div>

