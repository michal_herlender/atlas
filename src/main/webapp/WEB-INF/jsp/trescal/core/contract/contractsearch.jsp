<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
	<jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/contract/contractsearch.js'></script>
	</jsp:attribute>
    <jsp:attribute name="header"><span class="headtext"><spring:message code="contract.search" /></span></jsp:attribute>
    <jsp:body>
        <div class="infobox">
            <form:form>
                <t:showErrors path="command.*" showFieldErrors="true" />
                <fieldset >
                    <legend><spring:message code="contract.search" /></legend>

                    <div>
                        <ol>
                            <li>
                                <label><spring:message code="company" />:</label>
                                <spring:bind path="command.clientCompanyId">
                                    <div class="float-left extendPluginInput">
                                        <div class="compSearchJQPlugin">
                                            <input type="hidden" name="field" value="clientCompanyId" />
                                            <input type="hidden" name="compCoroles" value="client,business" />
                                            <input type="hidden" name="tabIndex" value="1" />
                                            <input type="hidden" name="triggerMethod" value="true" />
                                            <!-- company results listed here -->
                                        </div>
                                    </div>
                                </spring:bind>
                            </li>
                            <li><label><spring:message code="subdiv" />:</label>
                                <select name="clientSubdivId" id="subdiv">
                                    <option value="0"><spring:message code="contract.selectacomp"/></option>
                                </select>
                            </li>
                            <li>
                                <label><spring:message code="contract.legalContractNumber"/>:</label>
                                <form:input path="legalContractNumber" />
                            </li>
                            <li>
                                <label><spring:message code="contract.excludeexpired"/>:</label>
                                <form:checkbox path="excludeExpired"  />
                            </li>
                        </ol>
                    </div>
                    <label for="submit">&nbsp;</label>
                    <input type="submit" name="Submit" id="submit" value="<spring:message code='submit' />" />
                </fieldset>
            </form:form>
        </div>
        <!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>