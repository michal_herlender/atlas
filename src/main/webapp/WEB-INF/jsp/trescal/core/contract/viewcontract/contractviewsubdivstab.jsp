<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="script/trescal/core/contract/viewcontract/contractviewsubdivstab.js"></script>

<table class="default2">
    <thead>
        <tr>
            <td colspan="2">
                <spring:message code="contract.associatedsubdivs"/>
                <c:if test="${listtype eq 'ADDITIONAL'}">
                    <button class="float-right" id="addsubdivbutton">
                        <spring:message code="company.addsubdivision"/>
                    </button>
                </c:if>
            </td>
        </tr>
        <tr>
            <th class="subdiv" scope="col"><spring:message code="subdivision"/></th>
            <th class="action" scope="col">&nbsp</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    </tfoot>
    <tbody id="subdivtablebody">
        <c:choose>
            <c:when test="${listtype eq 'ADDITIONAL'}">
                <c:choose>
                    <c:when test="${linkedsubdivs.size() < 1 }">
                        <tr>
                            <td colspan="2" class="center bold">
                                <spring:message code="contract.nosubdivs"/>
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="subdiv" items="${linkedsubdivs}">
                            <tr>
                                <td class="subdiv">${subdiv.value}</td>
                                <td class="action">
                                    <button data-subdivid="${subdiv.key}" class="remove"><spring:message code="remove"/></button>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <tr>
                    <td colspan="2" class="center bold">
                        <spring:message code="contract.onlyequipmentinlist"/>
                    </td>
                </tr>
            </c:otherwise>
        </c:choose>

    </tbody>
</table>

<div id="subdivchoicedialog" title='<spring:message code="contract.choosesubdiv"/>' style="display: none;" >
    <table class="default2 subdivchooser">
        <c:forEach items="${unlinkedsubdivs}" var="subdiv">
            <tr>
                <td class="name"><c:out value="${subdiv.value}"/></td>
                <td class="action"><input type="checkbox" value="${subdiv.key}"/></td>
            </tr>
        </c:forEach>
    </table>
</div>