<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="contract.searchresults"/></span>
	</jsp:attribute>
    <jsp:attribute name="scriptPart">

	</jsp:attribute>
    <jsp:body>
        <!-- infobox contains all search form elements and is styled with nifty corners -->
        <div class="infobox">
            <c:set var="results" value="${resultSet.results}"/>
            <form:form modelAttribute="command">
                <form:hidden path="resultsPerPage"/>
                <form:hidden path="pageNo"/>
                <form:hidden path="excludeExpired"/>
                <form:hidden path="clientCompanyId"/>
                <form:hidden path="clientSubdivId"/>
                <form:hidden path="legalContractNumber"/>
                <form:hidden path="plantId"/>

                <t:showResultsPagination rs="${resultSet}" pageNoId=""/>
                <table class="default4 contractsearchresults">
                    <thead>
                        <tr>
                            <td colspan="10"><spring:message code="contract.matching"/> (${results.size()})</td>
                        </tr>
                        <tr>
                            <th class="contractnumber" scope="col"><spring:message code="contract.number"/></th>
                            <th class="company" scope="col"><spring:message code="company"/></th>
                            <th class="legalcontractnumber"><spring:message code="contract.legalContractNumber"/></th>
                            <th class="startdate" scope="col"><spring:message code="bpo.startdate"/></th>
                            <th class="enddate" scope="col"><spring:message code="bpo.enddate"/></th>
                        </tr>
                    </thead>
                </table>
                <table class="default4 contractsearchresults">
                    <tbody>
                        <c:set var="rowcount" value="0"/>
                        <c:choose>
                            <c:when test="${results.size() == 0}">
                                <tr>
                                    <td colspan="5" class="bold center">
                                        <spring:message code="noresults"/>
                                    </td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="dto" items="${results}">
                                    <tr>
                                        <td class="contractnumber">
                                            <a href="contractview.htm?id=${dto.contractId}" class="mainlink">${dto.contractNumber}</a>
                                        </td>
                                        <td class="company">${dto.clientCompanyName}</td>
                                        <td class="legalcontractnumber">${dto.legalContractNumber}</td>
                                        <td class="startdate"><fmt:formatDate value="${dto.startDate}" dateStyle="MEDIUM" type="both" timeStyle="short"/></td>
                                        <td class="enddate"><fmt:formatDate value="${dto.endDate}" dateStyle="medium" type="both" timeStyle="short"/></td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
                <t:showResultsPagination rs="${resultSet}" pageNoId=""/>
            </form:form>
        </div>
    </jsp:body>
</t:crocodileTemplate>

