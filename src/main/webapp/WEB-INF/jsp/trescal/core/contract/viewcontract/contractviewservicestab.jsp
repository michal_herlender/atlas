<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="script/trescal/core/contract/viewcontract/contractviewservicestab.js"></script>

<div class="infobox">
	<table class="default2" id="contractserviceslist">
		<thead>
			<tr>
				<th colspan="2"><spring:message code="servicetype"/></th>
				<th><spring:message code="contract.valid"/></th>
				<th><spring:message code="contract.turnaround"/></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan=4></td>
			</tr>
		</tfoot>
		<tbody>
			<c:forEach items="${services}" var="service">
				<tr>
					<td class="name"><c:out value="${service.serviceTypeName}"/></td>
					<td class="code"><c:out value="${service.serviceTypeCode}"/></td>
					<td class="code">
						<input type="checkbox" class="servicevalid" value="${service.serviceTypeId}" ${service.serviceTypePermitted ? 'checked' : ''}/>
					</td>
					<td class="code">
						<input type="number" min="0" class="turnaround" value="${service.turnaround}" ${service.serviceTypePermitted ? '' : 'disabled'}/>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
