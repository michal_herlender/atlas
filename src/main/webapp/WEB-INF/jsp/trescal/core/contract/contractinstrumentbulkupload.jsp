<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="contract.instbulkadd"/>
        </span>
	</jsp:attribute>
    <jsp:body>
        <form:form enctype="multipart/form-data">
            <fieldset>
                <ul>
                    <li>
                        <label><spring:message code="contract.number"/></label>
                        <span><c:out value="${contractNumber}"/></span>
                    </li>
                    <li>
                        <label><spring:message code="client"/></label>
                        <span><c:out value="${clientName}"/></span>
                    </li>
                </ul>
                <p><spring:message code="contract.uploadexcel"/><br>
                        <spring:message code="contract.excelshape"/><br>
                        <spring:message code="contract.excelsuggestsource"/><br><br>
                <h5 class="center-0 attention">
                    <spring:message code="contract.uploadwarning"/><br><br>
                </h5>
                <input type="file" name="importfile"/><br><br>
                <input type="hidden" name="contractid" value="${contractId}"/>
                <input type="submit"/>
            </fieldset>
        </form:form>
    </jsp:body>
</t:crocodileTemplate>