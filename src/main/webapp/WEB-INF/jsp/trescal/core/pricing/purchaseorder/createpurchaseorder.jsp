<%-- File name: /trescal/core/pricing/purchaseorder/createpurchaseorder.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="purchaseorder" tagdir="/WEB-INF/tags/purchaseorder" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="createpurchaseorder.createpurchaseorder"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/pricing/purchaseorder/CreatePurchaseOrder.js'></script>
    </jsp:attribute>
    <jsp:body>
    	<t:showErrors path="command.*" showFieldErrors="false" />
		<!-- main div which contains all create purchase order form elements and has nifty corners applied -->
		<div class="infobox">
			
			<form:form modelAttribute="command" action="" id="createpurchaseorderform" method="post">
				
				<fieldset>
					
					<legend><spring:message code="createpurchaseorder.createpurchaseorder"/></legend>
					
					<ol>
						<li>
							<label for="orgid"><spring:message code="fromcompany"/>:</label>
							${allocatedCompany.value}				
						</li>
						<li>
							<label><spring:message code="createpurchaseorder.contactaddress"/>:</label>
																				
							<!-- this div creates a new cascading search plugin. The default search contains an input 
								 field and results box for companies, when results are returned this cascades down to
								 display the subdivisions within the selected company. This default behavoir can be obtained
								 by including the hidden input field with no value or removing the input field. You can also
								 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
								 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
								 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
							 -->									
							<div id="cascadeSearchPlugin">
								<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
								<input type="hidden" id="compCoroles" value="business,supplier,utility" />
								<c:if test="${(not empty command.coid) && (command.coid != 0)}">	
									<input type="hidden" id="prefillIds" value="${command.coid},${command.subdivid},${command.personid},${command.addrid}" />
								</c:if>
								<input type="hidden" id="loadCurrency" value="true" />
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
							<form:errors path="addrid" class="attention" />
						</li>
						<li>
							<label for="jobno"><spring:message code="createpurchaseorder.jobno"/>:</label>
							<form:input path="jobno" size="50" />
							<form:errors path="jobno" class="attention" />
						</li>
						<li>
							<label for="returnAddressId"><spring:message code="createpurchaseorder.returnaddress"/>:</label>
							<form:select path="returnAddressId" items="${businessaddresslist}" itemLabel="value" itemValue="key" />
							<form:errors path="returnAddressId" class="attention" />
						</li>
						<li>
 							<label for="businessPersonid"><spring:message code="createpurchaseorder.businesscontact"/>:</label>
							<form:select path="businessPersonid" items="${businesscontacts}" itemLabel="value" itemValue="key" />
							<form:errors path="businessPersonid" class="attention" />
						</li>
						<li>
							<label for="clientref"><spring:message code="createpurchaseorder.supplierref"/>:</label>
							<form:input path="clientref" size="100" />
							<form:errors path="clientref" class="attention" />
						</li>
						<li>
							<label for="currencyCode"><spring:message code="currency"/>:</label>
							<form:select path="currencyCode" items="${currencyList}" itemLabel="value" itemValue="key" />
							<form:errors path="currencyCode" class="attention" />
						</li>
						<li <c:if test="${!useTaxableOption}">class="hid"</c:if>>
                            <form:label path="taxableOption"><spring:message code="createpurchaseorder.taxable"/></form:label>
                            <form:select path="taxableOption" items="${taxableOptions}" itemValue="key" itemLabel="value"/>
                        </li>
						<c:if test="${not empty quickPO}">
							<li>
								<label class="labelhead width80 bold"><spring:message code="createpurchaseorder.fromquickpurchaseorder"/></label>
								<span>&nbsp;</span>
							</li>
							<li>
								<label><spring:message code="createpurchaseorder.purchaseorder"/>:</label>
								<span>${quickPO.pono}</span>
							</li>
							<li>
								<label><spring:message code="jobno"/>:</label>
								<span>${quickPO.jobno}</span>
							</li>
							<li>
								<label><spring:message code="createdby"/>:</label>
								<span>${quickPO.createdBy.name}</span>
							</li>
							<li>
								<label><spring:message code="createdon"/>:</label>
								<span><fmt:formatDate value="${quickPO.createdOn}" type="date" dateStyle="SHORT" /></span>
							</li>
							<li>
								<label><spring:message code="description"/>:</label>
								<div class="float-left width80 padtop">
									${quickPO.description}
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
						</c:if>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code="create"/>" />
						</li>	
					</ol>																	
					
				</fieldset>
				
			</form:form>
		
		</div>
		<!-- end of create purchase order div -->
    </jsp:body>
</t:crocodileTemplate>