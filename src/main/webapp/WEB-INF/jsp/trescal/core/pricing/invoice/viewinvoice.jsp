<%-- File name: /trescal/core/pricing/invoice/viewinvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.trescal.cwms.core.account.entity.AccountStatus" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="invoice" tagdir="/WEB-INF/tags/invoice" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="invoice.viewinvoice" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/pricing/invoice/ViewInvoice.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
		<script type="module" src="script/components/cwms-invoicing-po/cwms-invoicing-po.js"></script>
		<script>
			var invoiceBrokered = false;
			/* Mark if invoice is locked for editing */
			<c:if test="${invoice.accountsStatus.equals(AccountStatus.S) || invoice.accountsStatus.equals(AccountStatus.P)}">
				invoiceBrokered = true;
			</c:if>
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>

	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${invoice.invno}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<c:set var="isIssued" value="${invoice.accountsStatus.equals(AccountStatus.S) || invoice.accountsStatus.equals(AccountStatus.P)}"/>
		<c:set var="isSynchronised" value="${invoice.accountsStatus.equals(AccountStatus.S)}"/>
		<c:if test="${not empty itemsnotlinked}" >
		<spring:message code="error.notlinkeditems" />
		</c:if>
		<c:if test="${isIssued}">
			<div id="lockedmessage" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<spring:message code="invoice.notebrokered" />.										
						<cwms:securedJsLink permission="INVOICE_UNLOCK" collapse="True" classAttr="mainlink" onClick="event.preventDefault(); unlockInvoice(); ">
							<spring:message code="invoice.unlocktemporarily" />
						</cwms:securedJsLink>
					</div>
				</div>
			</div>	
		</c:if>
				<form:errors path="*">
                        <div class="warningBox1">
                            <div class="warningBox2">
                                <div class="warningBox3">
                                    <h5 class="center-0 attention">
                                        <spring:message code="error.save"/>
                                    </h5>
                                </div>
                            </div>
                        </div>
                </form:errors>
		<form:errors path="">
  			 <script>
     			 /* If an error is in form, then display edit tab by default */
     				var LOADTAB = "edit-tab"; 
   		 	</script>
		</form:errors>
		
		<div class="infobox">
			<fieldset>
				<legend><spring:message code="invoice.invoice" />: ${invoice.invno}</legend>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="businesscompany" />:</label>
							<span><c:out value="${invoice.organisation.coname}" /></span>
						</li>
						<li>
							<label><spring:message code="clientcompany" />:</label>
							<span><links:companyLinkDWRInfo company="${invoice.address.sub.comp}" rowcount="0" copy="true" /></span>
						</li>
						<li>
							<label><spring:message code="subdivision" />:</label>
							<span><links:subdivLinkDWRInfo rowcount="0" subdiv="${invoice.address.sub}" /></span>
						</li>
						<li>
							<label><spring:message code="address" />:</label>
							<div class="float-left padtop">
								<address:showAddress address="${invoice.address}" vis="true" separator="<br/>" copy="true" />
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="invoice.coveringjobs" />:</label>
							<div class="float-left" >
								<span id="currentjoblinks">
									<c:forEach var="joblink" items="${invoice.jobLinks}" varStatus="forEachStatus">
										<c:choose>
											<c:when test="${joblink.job != null}">
												<span id="invoicejoblinkspan${joblink.id}">
													<links:jobnoLinkDWRInfo rowcount="0" jobno="${joblink.job.jobno}" jobid="${joblink.job.jobid}" copy="true" />
												</span>
											</c:when>
											<c:otherwise>
												<span id="invoicejoblinkspan${joblink.id}">${joblink.jobno}</span>
											</c:otherwise>
										</c:choose>
										<c:if test="${!forEachStatus.last}">
											,
											<c:if test="${!(forEachStatus.index % 3 == 0)}">
												<br />
											</c:if>
										</c:if>
									</c:forEach>
								</span>
								<c:set var="classEditNoJob">
									mainlink limitAccess <c:if test="${isIssued}"> notvis </c:if>
								</c:set>
								<cwms:securedJsLink permission="COVERING_EDIT" onClick=" event.preventDefault(); getInvoiceJobLinks(${invoice.id}); "  id="editjobnoslink" classAttr="${classEditNoJob}">
									<spring:message code="edit"/>
								</cwms:securedJsLink>
								<div id="invoicejoblinkediting" class="hid">
									<div id="jobnoslist" >
										
									</div>
									<div class="padtop">														
										<a href="#" onclick=" event.preventDefault(); addNewJobNoField(); ">
											<img src="img/icons/add.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code="invoice.addjobnumber" />" title="<spring:message code="invoice.addjobnumber" />" />
										</a>
									</div>
									<div class="padtop">
										<a href="#" onclick=" event.preventDefault(); saveJobNos(${invoice.id}); " class="mainlink-float"><spring:message code="save" /></a>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="invoice.invoicetype" /></label>
							<span>
								<t:showTranslationOrDefault translations="${invoice.type.nametranslation}" defaultLocale="${defaultlocale}" /> (<spring:message code="${invoice.pricingType.messageCode}" />)
							</span>
						</li>
						<li>
							<label><spring:message code="invoice.paymentterm" /></label>
							
							<div class="float-left">
 								<div>${invoice.paymentTerm.message}</div> 
							</div>

						</li>
						<li>
							<label><spring:message code="invoice.paymentdate" /></label>
							<span>
								<c:choose>
									<c:when test="${invoice.duedate != null}">
										<fmt:formatDate value="${invoice.duedate}" type="date" dateStyle="SHORT"/>
									</c:when>
									<c:otherwise>
										-
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="invoice.paymentmode" /></label>
							<span>
								<t:showTranslationOrDefault translations="${invoice.paymentMode.translations}" defaultLocale="${defaultlocale}" /> 
							</span>
						</li>
					</ol>
																																						
				</div>

				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="invoice.invoicedatetaxpoint" />:</label>
							<span><fmt:formatDate value="${invoice.invoiceDate}" type="date" dateStyle="SHORT"/></span>
						</li>
						<li>
							<label><spring:message code="purchaseorder.businesscontact" />:</label>
							<span>${invoice.businessContact.name}</span>
						</li>
						<li>
							<label><spring:message code="createdby" />:</label>
							<span><c:out value="${invoice.createdBy.name} "/><fmt:formatDate value="${invoice.regdate}" type="date" dateStyle="SHORT" /></span>
						</li>
						<li>
							<label><spring:message code="currency" />:</label>
							<span>
								<c:set var="currency" value="${invoice.currency}" />
								<c:out value="${currency.currencyCode} (${currency.currencySymbol} ${currency.currencyName} - "/> 
								<fmt:formatNumber value="1" type="currency" currencySymbol="${defaultCurrency.currencyERSymbol}"/>
								<c:out value=" : "/>
								<fmt:formatNumber value="${invoice.rate}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>)
							</span>
						</li>
						<li>
							<label><spring:message code="company.vatrate"/>:</label>
							<!-- display VAT information -->
							<span>
								<c:out value="${formattedVatRate}" />
							</span>
						</li>
						<li>
							<label><spring:message code="issuedate" /></label>
							<span>
								<c:choose>
									<c:when test="${invoice.issuedate != null}">
										<fmt:formatDate value="${invoice.issuedate}" type="date" dateStyle="SHORT" />
									</c:when>
									<c:otherwise>
										-
									</c:otherwise>
								</c:choose>
							</span>
						</li>
						<li>
							<label><spring:message code="invoice.statusinvoice" />:</label>
							<div class="float-left padtop">
								<t:showTranslationOrDefault translations="${invoice.status.descriptiontranslations}" defaultLocale="${defaultlocale}" />
								<c:if test="${invoice.status.requiresAttention}">
									<br/>
									<c:choose>
										<c:when test="${nominaltotalvalue.equals(invoice.totalCost)}">
											<cwms:securedLink permission="INVOICE_VIEW" parameter="?id=${invoice.id}&submission=submit" classAttr="mainlink">
												<spring:message code="invoice.updatestatusto" />
												'<cwms:besttranslation translations="${invoice.status.next.nametranslations}"/>'
											</cwms:securedLink>
										</c:when>
										<c:otherwise>
											<img src="img/icons/flag_red.png"/>&nbsp;&nbsp;<i><spring:message code="invoice.fixnominaldiscrepancybeforeupdatingstatus" /></i>
										</c:otherwise>
									</c:choose>
								</c:if>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="invoice.accountsstatus" />:</label>
							<div class="float-left padtop" id="accountstatus">
								<c:choose>
									<c:when test="${invoice.type.addToAccount}">
										<c:choose>
											<c:when test="${accountssoftwaresupported && (invoice.comp.account == null)}">
												<spring:message code="invoice.noaccountsprofilehasbeensetupfor" /> ${invoice.comp.coname}.
												<spring:message code="invoice.oncea" />
													<cwms:securedLink classAttr="mainlink" permission="COMPANY_VIEW" parameter="?coid=${invoice.comp.coid}&loadtab=finance-tab">
														<spring:message code="invoice.profilehasbeencreated" /> 
													</cwms:securedLink>
												<spring:message code="invoice.brokeringcanbeprocessed" />.
											</c:when>
											<c:when test="${!accountssoftwaresupported && ((companySettings == null) || (companySettings.status != 'VERIFIED'))}">
												<spring:message code="companystatusnotverified" />.
												<cwms:securedLink classAttr="mainlink" permission="COMPANY_VIEW" parameter="?coid=${invoice.comp.coid}&loadtab=finance-tab">
													<spring:message code="verifycompanytocontinue"/>
												</cwms:securedLink>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${invoice.accountsStatus.equals(AccountStatus.P)}">
														<img src="img/icons/sync_pending.png" width="14" height="14" alt="<spring:message code="invoice.pendingsync" />" title="<spring:message code="invoice.pendingsync" />" class="image_inl" />
														<spring:message code="invoice.pendingsynchronisationwithaccountingsoftware" />
													</c:when>
													<c:when test="${invoice.accountsStatus.equals(AccountStatus.E)}">
														<img src="img/icons/sync_error.png" width="14" height="14" alt="<spring:message code="invoice.errorsynchronising" />" title="<spring:message code="invoice.errorsynchronising" />" class="image_inl" />
														<spring:message code="invoice.errorssynchronisingwithaccountingsoftware" /> 
														<cwms:securedJsLink classAttr="mainlink" permission="INVOICE_UNLOCK" onClick=" event.preventDefault(); resetInvoiceStatus(${invoice.id}, '${accountssoftware}'); ">
															<spring:message code="invoice.reset"/>
														</cwms:securedJsLink>
													</c:when>
													<c:when test="${invoice.accountsStatus.equals(AccountStatus.S) }">
														<img src="img/icons/sync.png" width="14" height="14" alt="<spring:message code="invoice.accountsynchronised" />" title="<spring:message code="invoice.accountsynchronised" />" class="image_inl" />
														<spring:message code="invoice.invoicesynchronisedwithaccountingsoftware" />
													</c:when>
													<c:otherwise>
														<spring:message code="invoice.nobrokerstatusdataavailable" />
													</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<c:out value="${invoice.type.name} "/><spring:message code="invoice.invoice" /> - <spring:message code="invoice.willnotbebrokered" />
									</c:otherwise>
								</c:choose>
							</div>
							<div class="clear"></div>
						</li>
					</ol>
				</div>
			</fieldset>
		</div>

		<div id="subnav">
			<dl>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'items-tab', false); " id="items-link" title="<spring:message code="invoice.viewinvoiceitems" />" class="selected"><spring:message code="items" /> (<span class="invItemSize">${invoice.items.size()}</span>)</a></dt> 
				<dt class="<c:if test="${isIssued}">notvis</c:if>"><a href="editinvoiceitem.htm?invoiceid=${invoice.id}" title="<spring:message code="invoice.addnewitemtoinvoice" />" class="limitAccess"><spring:message code="invoice.newitem" /></a></dt>
				<dt class="<c:if test="${isIssued}">notvis</c:if>"><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'edit-tab', false); " id="edit-link" title="<spring:message code="invoice.editinvoicedetails" />" class="limitAccess"><spring:message code="edit" /></a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'actions-tab', false); " id="actions-link" title="<spring:message code="invoice.viewinvoiceactionhistory" />"><spring:message code="invoice.actions" /> (${invoice.actions.size()})</a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'email-tab', false); " id="email-link" title="<spring:message code="invoice.viewsendemailsforinvoice" />"><spring:message code="emails" /> (<span class="entityEmailDisplaySize">${invoice.sentEmails.size()}</span>)</a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'docs-tab', false); " id="docs-link" title="<spring:message code="invoice.viewdocumentoptions" />" ><spring:message code="invoice.documentoptions" /></a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false); " id="files-link" title="<spring:message code="invoice.viewdocumentsininvoicedirectory" />"><spring:message code="invoice.documents" /> (<span class="fileResourceCount">${scRootFiles.numberOfFiles}</span>)</a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'generate-tab', false); initInvoiceType();" id="generate-link" title="<spring:message code="invoice.generateinvoicedocument" />"><spring:message code="generate" /></a></dt> 
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'pos-tab', false); " id="pos-link" title="<spring:message code="invoice.viewinvoicepurchaseorders" />"><spring:message code="invoice.pos" /></a></dt>
				<dt class="<c:if test="${isIssued}">notvis</c:if>"><a href="#" onclick=" event.preventDefault(); loadScript.createOverlay('dynamic', 'Delete Invoice', null, createDeleteInvoiceContent(${invoice.id}), null, 36, 640, 'deletereason'); " title="<spring:message code="invoice.deletethisinvoice" />" class="limitAccess"><spring:message code="delete" /></a></dt>
			</dl>
		</div>

		<div id="subnav2">
			<dl>
				<dt>
					<c:set var="code_copyinvoice">
						<spring:message code="invoice.copyinvoice" />
					</c:set>
					<cwms:securedLink permission="INVOICE_COPY"  classAttr="mainlink"  parameter="?id=${invoice.id}" collapse="True" title="${code_copyinvoice}" >
						<spring:message code="jicalloffitem.copy"/>
					</cwms:securedLink>
				</dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'nominals-tab', false); " id="nominals-link" title="<spring:message code="invoice.showsbreakdownofcostsbynominals" />"><spring:message code="invoice.nominals" /> (${nominals.size()} <c:if test="${!nominaltotalvalue.equals(invoice.totalCost)}"> <img src="img/icons/flag_red.png"/> </c:if>)</a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'delivery-tab', false); " id="delivery-link" title="<spring:message code="invoice.showanyinvoicedeliveries" />"><spring:message code="invoice.invoicedeliveries" /> (<span class="invDelSize1">${invoice.deliveries.size()}</span>)</a></dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'creditnotes-tab', false); " id="creditnotes-link" title="<spring:message code="invoice.vieworaddcreditnotesforinvoice" />">
					<c:choose>
						<c:when test="${invoice.creditNotes.size() > 0}">
							<span class="attention"><spring:message code="invoice.creditnotes" /> (${invoice.creditNotes.size()})</span>
						</c:when>
						<c:otherwise>
							<spring:message code="invoice.creditnotes" /> (${invoice.creditNotes.size()})
						</c:otherwise>
					</c:choose>
				</a></dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'periodicLinking-tab', false); " id="periodicLinking-link" title="<spring:message code="invoice.periodicinvoice.manage" />" class="limitAccess" > 
						<spring:message code="invoice.periodicinvoice.manage" />
					</a>
				</dt>
				<dt><a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'instructions-tab', false); " id="instructions-link" title="<spring:message code="instructiontype.invoice" />"><span class="instructionSize <c:if test="${instructionsCount>0}">attention</c:if>"><spring:message code="instructions" />( ${instructionsCount})</span></a></dt>
			</dl>
		</div>

		<c:set var="currency" value="${invoice.currency}" />

		<div class="infobox">
			<div id="items-tab">
				<div class="float-right">
					<a href="#" class="mainlink" id="showall" onclick=" event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, 0); " title="<spring:message code="invoice.showallcostbreakdowns" />"><spring:message code="invoice.expandall" /></a> | <a href="#" class="mainlink" id="hideall" onclick=" event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, 0); " title="<spring:message code="invoice.showallcostbreakdowns" />"><spring:message code="invoice.collapseall" /></a>
				</div>
				<div class="clear"></div>

				<table class="default4 invoiceItemList" summary="<spring:message code="invoice.tableheadingsforallinitems" />">
					<thead>
						<tr>
							<td colspan="11"><spring:message code="invoice.invoiceitems" /> (<span class="invItemSize">${invoice.items.size()}</span>)</td>
						</tr>
						<tr>
							<th class="item" scope="col"><spring:message code="item" /></th>
							<th class="desc" scope="col"><spring:message code="description" /></th>
							<th class="inst" scope="col"><spring:message code="instmodelname" /></th>											
							<th class="caltype" scope="col"><spring:message code="servicetype" /></th>
							<th class="plantno" scope="col"><spring:message code="plantno" /></th>
							<th class="serial" scope="col"><spring:message code="serialno" /></th>
							<th class="subdiv" scope="col"><spring:message code="subdiv" /></th>
							<th class="cost cost2pc" scope="col"><spring:message code="cost" /></th>
							<th class="disc cost2pc" scope="col"><spring:message code="invoice.disc" /></th>
							<th class="final cost2pc" scope="col"><spring:message code="finalcost" /></th>
							<th class="del" scope="col"><spring:message code="delete" /></th>
						</tr>
					</thead>
				</table>
				
				<c:set var="rowcount" value="0" />
				<c:forEach var="item" items="${invoice.items}" varStatus="loop">
					<table class="default4 invoiceItemList" id="invitem${item.id}" summary="<spring:message code="invoice.tableinvoiceitems" />">
						<tbody>
							<c:choose>
								<c:when test="${item.jobItem != null}">
									<c:set var="bgcol" value="${item.jobItem.calType.serviceType.displayColourFastTrack}" />
									<c:if test="${item.jobItem.turn > fastTrackTurn}">
										<c:set var="bgcol" value="${item.jobItem.calType.serviceType.displayColour}" />
									</c:if>
								
									<tr style=" background-color: ${bgcol}; ${item.jobItem.calType.serviceType.displayColour}; ">
										<td class="item center">
											<span class="itemNumber">${item.itemno}</span>
											<a href="#" id="hide${item.id}" onclick=" event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, ${item.id}); ">
												<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="<spring:message code="invoice.hidecostbreakdown" />" title="<spring:message code="invoice.hidecostbreakdown" />" />
											</a>
										</td>
										<td class="desc">
											<spring:message code="item" />
											<links:jobitemLinkDWRInfo jobitem="${item.jobItem}" jicount="false" rowcount="${rowcount}" />
											<spring:message code="invoice.onjob" />
											<cwms:securedLink permission="JOB_VIEW" classAttr="mainlink" parameter="?jobid=${item.jobItem.job.jobid}">
												${item.jobItem.job.jobno}
											</cwms:securedLink>
											<br>
											<links:jobitemDeliveryLink jobItem="${item.jobItem}" showClientDeliveries="true" />
										</td>
										<td class="inst"><links:instrumentLinkDWRInfo instrument="${item.jobItem.inst}" rowcount="${rowcount}" displayBarcode="false" displayCalTimescale="false" displayName="true" caltypeid="${item.jobItem.calType.calTypeId}"  /></td>
										<td class="caltype center"><t:showTranslationOrDefault translations="${item.jobItem.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}" /></td>
										<td class="plantno">${item.jobItem.inst.plantno}</td>
										<td class="serial">${item.jobItem.inst.serialno}</td>	
										<td class="subdiv">${item.businessSubdiv.subdivCode}</td>												
										<td class="cost cost2pc">
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:otherwise>
													<fmt:formatNumber value="${item.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="disc cost2pc">
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:otherwise>
													<fmt:formatNumber value="${item.generalDiscountRate}" type="NUMBER" pattern="0.00"/>%
												</c:otherwise>
											</c:choose>
										</td>
										<td class="final cost2pc">
											<c:set var="url" value="editinvoiceitem.htm?itemid=${item.id}" />
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE' && not isSynchronised}">
													<a href="${url}" class="mainlink">N/A</a>
												</c:when>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:when test="${isSynchronised}">
													<fmt:formatNumber value="${item.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
												</c:when>
												<c:otherwise>
													<a href="${url}" class="mainlink"><fmt:formatNumber value="${item.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></a>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="del center">
											<c:set var="delItemInvoiceClass">limitAccess <c:if test="${isIssued}"> notvis </c:if></c:set>
											<c:if test="${not isSynchronised}">
												<cwms:securedJsLink permission="INVOICE_ITEM_DELETE" classAttr="${delItemInvoiceClass}" collapse="True" onClick="event.preventDefault(); deleteItem(${item.id}); ">
													<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code="invoice.deleteinvoiceitem" />" title="<spring:message code="invoice.deleteinvoiceitem" />" />
												</cwms:securedJsLink>
											</c:if>
										</td>													
									</tr>
								</c:when>
								<c:when test="${item.expenseItem != null}">
									<c:set var="bgcol" value="${item.expenseItem.serviceType.displayColour}"/>
									<tr style=" background-color: ${bgcol}; ${item.expenseItem.serviceType.displayColour}; ">
										<td class="item center">
											${loop.index + 1 }
											<a href="#" id="hide${item.id}" onclick=" event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, ${item.id}); ">
												<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="<spring:message code="invoice.hidecostbreakdown" />" title="<spring:message code="invoice.hidecostbreakdown" />" />
											</a>
										</td>
										<td class="desc">
											<spring:message code="invoice.serviceonjob" arguments="${item.expenseItem.itemNo}, <spring:message code='invoice.onjob'/>" text="Expense item {0} on job {1}"/><br/>
											<a href="viewjob.htm?jobid=${item.expenseItem.job.jobid}" class="mainlink">${item.expenseItem.job.jobno}</a>
										</td>
										<td class="inst"><cwms:showmodel instrumentmodel="${item.expenseItem.model}"/></td>
										<td class="caltype center"><t:showTranslationOrDefault translations="${item.expenseItem.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}" /></td>
										<td class="expcomment" colspan="2">${item.expenseItem.comment}</td>
										<td class="subdiv">${item.businessSubdiv.subdivCode}</td>
										<td class="cost cost2pc">
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:otherwise>
													<fmt:formatNumber value="${item.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="disc cost2pc">
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:otherwise>
													<fmt:formatNumber value="${item.generalDiscountRate}" type="NUMBER" pattern="0.00"/>%
												</c:otherwise>
											</c:choose>
										</td>
										<td class="final cost2pc">
											<c:set var="url" value="editinvoiceitem.htm?itemid=${item.id}" />
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE' && not isSynchronised}">
													<a href="${url}" class="mainlink">N/A</a>
												</c:when>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:when test="${isSynchronised}">
													<fmt:formatNumber value="${item.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
												</c:when>
												<c:otherwise>
													<a href="${url}" class="mainlink"><fmt:formatNumber value="${item.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></a>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="del center">
										<c:set var="deleteInvoiceItemCl">limitAccess <c:if test="${isIssued}"> notvis </c:if></c:set>
										<c:if test="${not isSynchronised}">
											<cwms:securedJsLink permission="INVOICE_ITEM_DELETE" collapse="True" classAttr="${deleteInvoiceItemCl}" onClick="event.preventDefault(); deleteItem(${item.id});">
												<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code="invoice.deleteinvoiceitem" />" title="<spring:message code="invoice.deleteinvoiceitem" />" />
											</cwms:securedJsLink>
										</c:if>
										</td>										
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<td class="item">
											${loop.index + 1 }
											<a href="#" id="hide${item.id}" onclick=" event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, ${item.id}); ">
												<img src="img/icons/hideinfo.png" width="16" height="16" class="img_marg_bot" alt="<spring:message code="invoice.hidecostbreakdown" />" title="<spring:message code="invoice.hidecostbreakdown" />" />
											</a>
										</td>
										<td colspan="3">${item.description.replaceAll("\\n", "")}</td>
										<td class="plantno"></td>
										<td class="serial">${item.serialno}</td>
										<td class="subdiv">${item.businessSubdiv.subdivCode}</td>
										<td class="cost cost2pc">
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:otherwise>
													<fmt:formatNumber value="${item.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="disc cost2pc">
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:otherwise>
													<fmt:formatNumber value="${item.generalDiscountRate}" type="NUMBER" pattern="0.00"/>%
												</c:otherwise>
											</c:choose>
										</td>
										<td class="final cost2pc">
											<c:set var="url" value="editinvoiceitem.htm?itemid=${item.id}" />
											<c:choose>
												<c:when test="${invoice.pricingType == 'SITE' && not isSynchronised}">
													<a href="${url}" class="mainlink">N/A</a>
												</c:when>
												<c:when test="${invoice.pricingType == 'SITE'}">
													N/A
												</c:when>
												<c:when test="${isSynchronised}">
													<fmt:formatNumber value="${item.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
												</c:when>
												<c:otherwise>
													<a href="${url}" class="mainlink"><fmt:formatNumber value="${item.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></a>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="del">
											<c:set var="deleteInvoiceItemCl">limitAccess <c:if test="${isIssued}"> notvis </c:if></c:set>
											<c:if test="${not isSynchronised}">
												<cwms:securedJsLink permission="INVOICE_ITEM_DELETE" collapse="True" classAttr="${deleteInvoiceItemCl}" onClick="event.preventDefault(); deleteItem(${item.id});">
													<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code="invoice.deleteinvoiceitem" />" title="<spring:message code="invoice.deleteinvoiceitem" />" />
												</cwms:securedJsLink>
											</c:if>

										</td>
									</tr>
								</c:otherwise>
							</c:choose>
							
							<!-- don't show cost break downs for site invoices -->
							<c:if test="${invoice.pricingType != 'SITE'}">			
								<c:set var="rowspan" value="1" />
								<c:choose>
									<c:when test="${item.breakUpCosts}">
										<c:if test="${item.calibrationCost.active}"> <c:set var="rowspan" value="${rowspan + 1}" /></c:if>
										<c:if test="${item.adjustmentCost.active}"> <c:set var="rowspan" value="${rowspan + 1}" /></c:if>
										<c:if test="${item.purchaseCost.active}"> <c:set var="rowspan" value="${rowspan + 1}" /></c:if>
										<c:if test="${item.repairCost.active}"> <c:set var="rowspan" value="${rowspan + 1}" /></c:if>
										<c:if test="${item.serviceCost.active}"> <c:set var="rowspan" value="${rowspan + 1}" /></c:if>
									</c:when>
									<c:otherwise>
										<c:set var="rowspan" value="${rowspan + 1}" />
									</c:otherwise>
								</c:choose>
								
								<tr class="costsummary${item.id}">
									<td rowspan="${rowspan}">&nbsp;</td>
									<td class="bold"><spring:message code="invoice.costtype" /></td>
									<td class="bold" colspan="5"><spring:message code="invoice.nominal" /></td>
									<td class="cost bold"><spring:message code="cost" /></td>
									<td class="cost bold"><spring:message code="invoice.disc" /></td>												
									<td class="cost bold"><spring:message code="finalcost" /></td>
									<td rowspan="${rowspan}">&nbsp;</td>
								</tr>
								
								<c:choose>
									<c:when test="${item.breakUpCosts}">
										<c:if test="${item.calibrationCost.active}">
											<tr class="costsummary${item.id} nobord">
												<td class="desc"><c:if test="${item.calibrationCost.costType != null}"> <spring:message code="${item.calibrationCost.costType.messageCode}"/></c:if></td>
												<td colspan="5"><c:if test="${item.calibrationCost.nominal != null}">${item.calibrationCost.nominal.code} - <t:showTranslationOrDefault translations="${item.calibrationCost.nominal.titleTranslations}" defaultLocale="${defaultlocale}" /></c:if></td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.calibrationCost.totalCost == null || item.calibrationCost.totalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.calibrationCost.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.calibrationCost.discountRate == null || item.calibrationCost.discountRate == 0}">
															<fmt:formatNumber value="0" type="NUMBER" pattern="0.00"/>%
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.calibrationCost.discountRate}" type="NUMBER" pattern="0.00"/>%
														</c:otherwise>
													</c:choose>
												</td>														
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.calibrationCost.finalCost == null || item.calibrationCost.finalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.calibrationCost.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>														
											</tr>
										</c:if>
										<c:if test="${item.adjustmentCost.active}">
											<tr class="costsummary${item.id} nobord">														
												<td class="desc"><c:if test="${item.adjustmentCost.costType != null}"> <spring:message code="${item.adjustmentCost.costType.messageCode}" /> </c:if></td>
												<td colspan="5"><c:if test="${item.adjustmentCost.nominal != null}">${item.adjustmentCost.nominal.code} - <t:showTranslationOrDefault translations="${item.adjustmentCost.nominal.titleTranslations}" defaultLocale="${defaultlocale}" /></c:if></td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.adjustmentCost.totalCost == null || item.adjustmentCost.totalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.adjustmentCost.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.adjustmentCost.discountRate == null || item.adjustmentCost.discountRate == 0}">
															<fmt:formatNumber value="0" type="NUMBER" pattern="0.00"/>%
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.adjustmentCost.discountRate}" type="NUMBER" pattern="0.00"/>%
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.adjustmentCost.finalCost == null || item.adjustmentCost.finalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.adjustmentCost.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>														
											</tr>
										</c:if>
										<c:if test="${item.purchaseCost.active}">
											<tr class="costsummary${item.id} nobord">														
												<td class="desc"><c:if test="${item.purchaseCost.costType != null}"> <spring:message code="${item.purchaseCost.costType.messageCode}" /></c:if></td>
												<td colspan="5"><c:if test="${item.purchaseCost.nominal != null}">${item.purchaseCost.nominal.code} - <t:showTranslationOrDefault translations="${item.purchaseCost.nominal.titleTranslations}" defaultLocale="${defaultlocale}" /></c:if></td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.purchaseCost.totalCost == null || item.purchaseCost.totalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.purchaseCost.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.purchaseCost.discountRate == null || item.purchaseCost.discountRate == 0}">
															<fmt:formatNumber value="0" type="NUMBER" pattern="0.00"/>%
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.purchaseCost.discountRate}" type="NUMBER" pattern="0.00"/>%
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.purchaseCost.finalCost == null || item.purchaseCost.finalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.purchaseCost.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>														
											</tr>
										</c:if>
										<c:if test="${item.repairCost.active}">
											<tr class="costsummary${item.id} nobord">														
												<td class="desc"><c:if test="${item.repairCost.costType != null}"> <spring:message code="${item.repairCost.costType.messageCode}" /> </c:if></td>
												<td colspan="5"><c:if test="${item.repairCost.nominal != null}">${item.repairCost.nominal.code} - <t:showTranslationOrDefault translations="${item.repairCost.nominal.titleTranslations}" defaultLocale="${defaultlocale}" /></c:if></td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.repairCost.totalCost == null || item.repairCost.totalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.repairCost.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.repairCost.discountRate == null || item.repairCost.discountRate == 0}">
															<fmt:formatNumber value="0" type="NUMBER" pattern="0.00"/>%
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.repairCost.discountRate}" type="NUMBER" pattern="0.00"/>%
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.repairCost.finalCost == null || item.repairCost.finalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.repairCost.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>														
											</tr>
										</c:if>
										<c:if test="${item.serviceCost.active}">
											<tr class="costsummary${item.id} nobord">														
												<td class="desc"><c:if test="${item.serviceCost.costType != null}"> <spring:message code="${item.serviceCost.costType.messageCode}" /> </c:if></td>
												<td colspan="5"><c:if test="${item.serviceCost.nominal != null}">${item.serviceCost.nominal.code} - <t:showTranslationOrDefault translations="${item.serviceCost.nominal.titleTranslations}" defaultLocale="${defaultlocale}" /></c:if></td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.serviceCost.totalCost == null || item.serviceCost.totalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.serviceCost.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.serviceCost.discountRate == null || item.serviceCost.discountRate == 0}">
															<fmt:formatNumber value="0" type="NUMBER" pattern="0.00"/>%
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.serviceCost.discountRate}" type="NUMBER" pattern="0.00"/>%
														</c:otherwise>
													</c:choose>
												</td>
												<td class="cost2pc">
													<c:choose>
														<c:when test="${item.serviceCost.finalCost == null || item.serviceCost.finalCost == 0}">
															<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:when>
														<c:otherwise>
															<fmt:formatNumber value="${item.serviceCost.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
														</c:otherwise>
													</c:choose>
												</td>														
											</tr>
										</c:if>
									</c:when>
									<c:otherwise>
										<tr class="costsummary${item.id} nobord">													
											<td class="desc"><spring:message code="invoice.singlecost" /></td>
											<td colspan="5"><c:if test="${item.nominal != null}">${item.nominal.code} - <t:showTranslationOrDefault translations="${item.nominal.titleTranslations}" defaultLocale="${defaultlocale}" /></c:if></td>
											<td class="cost2pc">
												<c:choose>
													<c:when test="${item.totalCost == null || item.totalCost == 0}">
														<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
													</c:when>
													<c:otherwise>
														<fmt:formatNumber value="${item.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
													</c:otherwise>
												</c:choose>
											</td>
											<td class="cost2pc">
												<c:choose>
													<c:when test="${item.generalDiscountRate == null || item.generalDiscountRate == 0}">
														<fmt:formatNumber value="0" type="NUMBER" pattern="0.00"/>%
													</c:when>
													<c:otherwise>
														<fmt:formatNumber value="${item.generalDiscountRate}" type="NUMBER" pattern="0.00"/>% 
														(<fmt:formatNumber value="${item.generalDiscountValue}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>)
													</c:otherwise>
												</c:choose>
											</td>
											<td class="cost2pc">
												<c:choose>
													<c:when test="${item.finalCost == null || item.finalCost == 0}">
														<fmt:formatNumber value="0" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
													</c:when>
													<c:otherwise>
														<fmt:formatNumber value="${item.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
													</c:otherwise>
												</c:choose>
											</td>													
										</tr>
									</c:otherwise>
								</c:choose>														
							</c:if>		
						</tbody>
					</table>
					<c:set var="rowcount" value="${rowcount + 1}" />
				</c:forEach>
				
				<!--  show any site costing notes -->
				<c:if test="${invoice.pricingType == 'SITE'}">
					<div>
						<h3><spring:message code="invoice.siteinvoicenotes" />:</h3>
						<div>
							<c:choose>
								<c:when test="${invoice.siteCostingNote == ''}">
									<spring:message code="invoice.nonotes" />.
								</c:when>
								<c:otherwise>
									${invoice.siteCostingNote}
								</c:otherwise>
							</c:choose>
							<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'edit-tab', false); $j('#siteCostingNoteTextArea').focus();"><spring:message code="edit" /></a>
						</div>
					</div>
				</c:if>
				
				<table class="default4 invoiceItemList" summary="<spring:message code="invoice.tablecostingtotalvalue" />">
					<tbody>
						<tr class="nobord">											
							<td class="totalsum cost2pcTotal bold"><spring:message code="totalcost" />:</td>
							<td class="totalamount cost2pcTotal bold">
								<span id="invTotalCost"><fmt:formatNumber value="${invoice.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
							</td>
							<td class="filler">&nbsp;</td>
						</tr>																				
					</tbody>
				</table>
				<c:choose>
					<c:when test="${not empty invoice.taxes}">
						<table class="default4 invoiceItemList" summary="<spring:message code="invoice.tableinvoicevatvalue" />">
							<tbody>
								<c:forEach var="tax" items="${invoice.taxes}">
									<tr class="nobord">											
										<td class="totalsum cost2pcTotal bold"><spring:message code="tax"/>&nbsp;${tax.description} (<fmt:formatNumber value="${tax.rate.multiply(100)}" type="NUMBER" pattern="0.00"/>%):</td>
										<td class="totalamount cost2pcTotal bold">
											<span id="invVat"><fmt:formatNumber value="${tax.tax}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
										</td>
										<td class="filler">&nbsp;</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<table class="default4 invoiceItemList" summary="<spring:message code="invoice.tableinvoicevatvalue" />">
							<tbody>
								<tr class="nobord">											
									<td class="totalsum cost2pcTotal bold">(<fmt:formatNumber value="${invoice.vatRate}" type="NUMBER" pattern="0.00"/>%) <spring:message code="vat" />:</td>
									<td class="totalamount cost2pcTotal bold">
										<span id="invVat"><fmt:formatNumber value="${invoice.vatValue}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
									</td>
									<td class="filler">&nbsp;</td>
								</tr>
							</tbody>
						</table>
					</c:otherwise>
				</c:choose>
				<table class="default4 invoiceItemList" summary="<spring:message code="invoice.tableinvoicetotalvalue" />">
					<tbody>
						<tr class="nobord">											
							<td class="totalsum cost2pcTotal bold"><spring:message code="invoice.nettotal" />:</td>
							<td class="totalamount cost2pcTotal bold">
								<span id="invFinalCost">
									<fmt:formatNumber value="${invoice.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>
								</span>
						</td>
							<td class="filler">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				
				<c:if test="${invoice.creditNotes.size() > 0}">
					<c:set var="creditTotal" value="0.00" />
					<c:forEach var="cn" items="${invoice.creditNotes}">
						<c:set var="creditTotal" value="${creditTotal + cn.finalCost}" />
					</c:forEach>
					<c:set var="totalToPay" value="${invoice.finalCost - creditTotal}" />
					<table class="default4 invoiceItemList" summary="<spring:message code="invoice.tableinvoicetotalvalue" />">
						<tbody>
							<tr class="nobord" style=" background-color: pink; ">											
								<td class="totalsum cost2pcTotal bold"><spring:message code="invoice.creditnotesapplied" />:</td>
								<td class="totalamount cost2pcTotal bold">
									-&nbsp;<span id="invFinalCost"><fmt:formatNumber value="${creditTotal}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
								</td>
								<td class="filler">&nbsp;</td>
							</tr>
						</tbody>
					</table>
					<table class="default4 invoiceItemList" summary="<spring:message code="invoice.tableinvoicetotalvalue" />">
						<tbody>
							<tr class="nobord" style=" background-color: pink; ">											
								<td class="totalsum cost2pcTotal bold"><spring:message code="invoice.nettotaltopay" />:</td>
								<td class="totalamount cost2pcTotal bold">
									<span id="invFinalCost"><fmt:formatNumber value="${totalToPay}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
								</td>
								<td class="filler">&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</c:if>
			</div>

			<div id="edit-tab" class="hid">
				<form:form action="" class="editInvoiceForm" method="post" modelAttribute="invoiceform">
					<fieldset>
						<legend><spring:message code="invoice.editinvoice" /></legend>
							<ol>
								<li>
									<label><spring:message code="address" />:</label>
									<div class="symbolbox"></div>
									<div id="cascadeSearchPlugin">
										<input type="hidden" id="cascadeRules" value="subdiv,address" />
										<input type="hidden" id="compCoroles" value="client,business" />
										<input type="hidden" id="addressType" value="Invoice" />
										<input type="hidden" id="prefillIds" value="${invoice.address.sub.comp.coid},${invoice.address.sub.subdivid},${invoice.address.addrid}" />
									</div>
									<!-- clear floats and restore page flow -->
									<div class="clear"></div>
								</li>
								<li>
									<label for="businessContact">
										<spring:message code="purchaseorder.businesscontact"/>:
									</label>
									<div class="symbolbox"></div>
									<div class="float-left">
										<form:select path="businessContactId" items="${businessContacts}" itemLabel="name" itemValue="id"/>
									</div>
									<form:errors path="businessContactId" cssClass="attention"/>
									<div class="clear"></div>
								</li>
								<li>
									<label for="vatcode">
										<spring:message code="company.vatrate"/>:
									</label>
									<div class="symbolbox"></div>
									<div class="float-left">
										<form:select path="vatCode" id="vatCode">
											<c:forEach var="vatGroup" items="${vatRates.keySet()}">
												<optgroup label="${vatGroup}">
													<form:options items="${vatRates.get(vatGroup)}" itemLabel="value" itemValue="key" /> 
												</optgroup>
											</c:forEach>
										</form:select>
									</div>
									<form:errors path="vatCode" cssClass="attention"/>
									<div class="clear"></div>
								</li>
								<li>
										<form:label path="typeid"><spring:message code="invoice.invoicetype" />:</form:label>
										<div class="symbolbox"></div>
										<div class="float-left">
											<form:select path="typeid">
												<c:forEach var="invt" items="${invoicetypes}">
													<option value="${invt.id}" ${invoice.type.id == invt.id ? "selected" : ""}><t:showTranslationOrDefault translations="${invt.nametranslation}" defaultLocale="${defaultlocale}" /></option>
												</c:forEach>
											</form:select>
										</div>
									<form:errors path="typeid" element="div" delimiter="; " cssClass="float-left; error"/>
									<div class="clear"></div>													
								</li>
								<li>
									<label for=""><spring:message code="invoice.invoicedatetaxpoint" />:</label>
									<div class="symbolbox"></div>
									<div class="float-left">
											<form:input type="date" path="invoiceDate" id="invoicedate"  />
									</div>
									<form:errors path="invoiceDate" element="div" delimiter="; " cssClass="float-left; error"/>
									<div class="clear"></div>													
								</li>
								<li>
									<label><spring:message code="invoice.financialperiod" />:</label>
									<div class="symbolbox"></div>
									<div class="float-left">
										${invoice.financialPeriod} (<spring:message code="invoice.thisfieldisautomaticallycalculatedusingtheinvoicedate" />)
									</div>
									<div class="clear"></div>													
								</li>
								
								<!-- make the total cost editable for site costing -->
								<c:if test="${invoice.pricingType == 'SITE'}">
									<li><strong><spring:message code="invoice.siteinvoiceoptions" /></strong></li>
									<li>
											<form:label path="totalCost"><spring:message code="totalcost" />:</form:label>
											${currency.currencyERSymbol}
											<form:input type="text" id="totalCost" path="totalCost"/>
											<form:errors path="invoiceform.totalCost" class="attention"/>
									</li>
									<li>
											<form:label path="siteCostingNote"><spring:message code="invoice.sitecostingnotes" />:</form:label>
											<form:textarea id="siteCostingNoteTextArea" path="siteCostingNote" rows="3" cols="80" />
											<form:errors path="invoiceform.siteCostingNote" class="attention"/>
									</li>
								</c:if>
								<li>
									<label>&nbsp;</label>
									<div class="symbolbox"></div>
									<input type="submit" name="update" value="<spring:message code="update" />" />
								</li>
							</ol>
					</fieldset>
				</form:form>
			</div>

			<div id="nominals-tab" class="hid">
				<table class="default4 invNominalCheck" summary="<spring:message code="invoice.tableheadingsnominals" />">
					<thead>
						<tr>
							<td colspan="3"><spring:message code="invoice.nominalsfound" /> (${nominals.size()})</td>
						</tr>
						<tr>
							<th class="nominal" scope="col"><spring:message code="invoice.nominalcode" /></th>
							<th class="descrip" scope="col"><spring:message code="description" /></th>
							<th class="amount" scope="col"><spring:message code="invoice.amount" /></th>
						</tr>
					</thead>
				</table>
					
				<c:choose>							
					<c:when test="${accountssoftwaresupported && (invoice.comp.account == null)}">
						<table class="default4" summary="<spring:message code="invoice.tablenominalcode" />">
							<tbody>											
								<tr>
									<td>
										<spring:message code="invoice.noaccountsprofilehasbeensetupfor" /> ${invoice.comp.coname}.
										<spring:message code="invoice.oncea" /> 
										<cwms:securedLink classAttr="mainlink" permission="COMPANY_VIEW"  parameter="?coid=${invoice.comp.coid}&loadtab=finance-tab">
											<spring:message code="invoice.profilehasbeencreated"/>
										</cwms:securedLink>
										<spring:message code="invoice.brokeringcanbeprocessed" />.
									</td>
								</tr>
							</tbody>
						</table>
					</c:when>
					<c:when test="${!accountssoftwaresupported && ((companySettings == null) || (companySettings.status != 'VERIFIED'))}">
						<spring:message code="companystatusnotverified" />.
						<cwms:securedLink classAttr="mainlink" permission="COMPANY_VIEW" parameter="?coid=${invoice.comp.coid}&loadtab=finance-tab">
							<spring:message code="verifycompanytocontinue"/>
						</cwms:securedLink>
					</c:when>
					<c:otherwise>
						<c:if test="${nominals.size() > 0}">
							<c:forEach var="nom" items="${nominals}">
								<table class="default4 invNominalCheck" summary="<spring:message code="invoice.tablenominalcode" />">
									<tbody>											
										<tr>
											<td class="nominal">${nom.nominalCode}</td>
											<td class="descrip">${nom.nominalTitle}</td>
											<td class="amount"><fmt:formatNumber value="${nom.cost}" type="currency" currencySymbol="${invoice.currency.currencyERSymbol}"/></td>
										</tr>
									</tbody>
								</table>
							</c:forEach>
								
							<table class="default4 invNominalCheck" summary="<spring:message code="invoice.tablenominalcode" />">
								<tbody>											
									<tr class="nobord">																				
										<td colspan="2" class="totalsum cost2pcTotal bold"><spring:message code="totalcost" />:</td>
										<td class="amount totalamount cost2pcTotal bold">
											<fmt:formatNumber value="${invoice.totalCost}" type="currency" currencySymbol="${invoice.currency.currencyERSymbol}"/>
										</td>													
									</tr>
								</tbody>
							</table>
						</c:if>
						<c:if test="${!nominaltotalvalue.equals(invoice.totalCost)}"> 
							<p>
								<img src="img/icons/flag_red.png"/> 
								<spring:message code="invoice.pleasenotethetotalnominalcosts" /> (<fmt:formatNumber value="${nominaltotalvalue}" type="currency" currencySymbol="${invoice.currency.currencyERSymbol}"/>) <spring:message code="invoice.donotmatchwiththetotalinvoiceandcarriagecosts" /> (<fmt:formatNumber value="${invoice.totalCost}" type="currency" currencySymbol="${invoice.currency.currencyERSymbol}"/>). <spring:message code="invoice.thismayindicatethatpotentiallysomecostsdonothavenominalscorrectlyassigned" />. 
							</p>
						</c:if>
					</c:otherwise>
				</c:choose>					
			</div>							

			<div id="files-tab" class="hid">
				<files:showFilesForSC entity="${invoice}" sc="${sc}" id="${invoice.id}" 
								identifier="${invoice.invno}" rootFiles="${scRootFiles}" 
								rootTitle="<spring:message code='invoice.filesforinvoice' /> ${invoice.invno}" 
								deleteFiles="true" allowEmail="true" isEmailPlugin="false" ver="" />
			</div>

			<div id="email-tab" class="hid">
				<t:showEmails entity="${invoice}" entityId="${invoice.id}" entityName="<spring:message code='invoice.invoice' />" sc="${sc}" coid="${invoice.comp.coid}"/>
			</div>

			<div id="docs-tab" class="hid">
				<fieldset>
					<legend><spring:message code="invoice.advanceddocumentoptions" /></legend>
					<ul>
						<li>
							<label><spring:message code="createexcelexport" />:</label>
							<span>
								<c:choose>
									<c:when test="${showDocumentOptions}">
										<a class="mainlink"  href="invoiceexport.xlsx?invoiceId=${invoice.id}" >
											<img src="img/doctypes/excel.gif" class="img_marg_bot" width="16" height="16" alt="<spring:message code="createexcelexport" />" title="<spring:message code="createexcelexport" />" />
											<spring:message code="viewprebooking.downloadfile" /> 
										</a>
									</c:when>
									<c:otherwise>
										<spring:message code="adminarea.legend.development" />
									</c:otherwise>
								</c:choose>
							</span>
						</li>
					</ul>
				</fieldset>			
			</div>

			<div id="generate-tab" class="hid">
				<invoice:invoiceGenerateOptions invoiceId="${invoice.id}" detailedInvoiceType="${detailedInvoiceType}" printDiscount="${printDiscount}" 
					printUnitaryPrice="${printUnitaryPrice}" printQuantity="${printQuantity}" showAppendix="${showAppendix}" isCalibration="${isCalibration}" 
					description="${invoice.siteCostingNote}" languageTags="${languageTags}" defaultLanguageTag="${defaultLanguageTag}"
					 />
			</div>

			<div id="actions-tab" class="hid">
				<table class="default4" title="<spring:message code="invoice.tableactionscompleted" />">
					<thead>
						<tr>
							<td colspan="4"><spring:message code="invoice.actionscompletedoninvoice" /> (${invoice.actions.size()})</td>
						</tr>
						<tr>
							<th scope="col"><spring:message code="invoice.activity" /></th>
							<th scope="col"><spring:message code="invoice.outcome" /></th>
							<th scope="col"><spring:message code="invoice.peformedby" /></th>
							<th scope="col"><spring:message code="invoice.performedon" /></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="action" items="${invoice.actions}">
							<tr>
								<td><t:showTranslationOrDefault translations="${action.activity.descriptiontranslations}" defaultLocale="${defaultlocale}" /></td>
								<td><t:showTranslationOrDefault translations="${action.outcomeStatus.descriptiontranslations}" defaultLocale="${defaultlocale}" /></td>
								<td>${action.contact.name}</td>
								<td><fmt:formatDate value="${action.date}" type="both" dateStyle="SHORT" timeStyle="SHORT"/></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

			<div id="pos-tab" class="hid">
				<div class="displaycolumn">							
					<c:if test="${invoicedJobPONums.size() > 0}">
						<c:forEach var="jobLink" items="${invoice.jobLinks}">
							<!-- build up a list of all purchase orders available from invoice items -->
							<table class="default4 invJobPurchaseOrders" summary="">
								<thead>
									<tr>
										<td colspan="3"><spring:message code="invoice.purchaseordersonjob" /> (<span class="bold">${jobLink.jobno}</span>)</td>
									</tr>
									<tr>
										<th class="type"><spring:message code="type" /></th>
										<th class="ponum"><spring:message code="invoice.ponumber" /></th>
										<th class="link"><spring:message code="invoice.linktoitems" /></th>
									</tr>
								</thead>													
								<tbody>
									<c:forEach var="ijpos" items="${invoicedJobPONums.get(jobLink.id)}">
										<c:if test="${not empty ijpos.bpos}">
											<c:forEach var="bpo" items="${ijpos.bpos}">
												<tr>
													<td class="type"><spring:message code="invoice.bpo" /></td>
													<td class="ponum">${bpo.poNumber}</td>
													<c:set var="escapedPoNumber"><spring:escapeBody javaScriptEscape="true">${bpo.poNumber}</spring:escapeBody></c:set>
													<td class="link"><a href="#" class="mainlink limitAccess <c:if test="${isIssued}"> notvis </c:if>" onclick=" event.preventDefault(); linkInvoicePOContent(${invoice.id}, ${bpo.poId}, '${escapedPoNumber}', 'jobbpo', ${ijpos.job.jobid}, '${ijpos.job.jobno}'); "><spring:message code="invoice.linktoitems" /></a></td>
												</tr>
											</c:forEach>
										</c:if>
										<c:if test="${ijpos.pos != null && ijpos.pos.size() > 0}">
											<c:forEach var="po" items="${ijpos.pos}">
												<tr>
													<td class="type"><spring:message code="invoice.po" /></td>
													<td class="ponum">${po.poNumber}</td>
													<c:set var="escapedPoNumber"><spring:escapeBody javaScriptEscape="true">${po.poNumber}</spring:escapeBody></c:set>
													<td class="link">
													<a href="#" class="mainlink limitAccess <c:if test="${isIssued}"> notvis </c:if>" onclick=" event.preventDefault(); linkInvoicePOContent(${invoice.id}, ${po.poId}, '${escapedPoNumber}', 'jobpo', ${po.job.jobid}, '${ijpos.job.jobno}'); "><spring:message code="invoice.linktoitems" /></a>
													</td>
												</tr>
											</c:forEach>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</c:forEach>
					</c:if>
				</div>
				<div class="displaycolumn">
					<cwms-invoice-po pos="${cwms:objectToJson(invoicepos)}" invoiceId="${invoice.id}"></cwms-invoice-po>
				<%-- Normally hide "old" invoice PO feature (targeted for removal soon) --%>
				<c:if test="${showOldInvoicePOFeature}" >
					<table class="default4 width80 invPurchaseOrders" summary="">
						<thead>
						<tr>
							<td colspan="3"><spring:message code="invoice.invoicepurchaseorders"/> (<span
									class="invPOSize">${invoice.pos.size()}</span>)
							</td>
						</tr>
						<tr>
							<th class="ponum"><spring:message code="invoice.ponumber"/></th>
							<th class="link"><spring:message code="invoice.linktoitems"/></th>
							<th class="remove"><spring:message code="remove"/></th>
						</tr>
						</thead>
						<tfoot>
						<tr>
							<td colspan="3">
								<spring:message var="overlayTitle" code="invoice.createinvoicepurchaseorder"
												javaScriptEscape="true"/>
								<c:set var="command" value="event.preventDefault(); 
								loadScript.createOverlay('dynamic', '${overlayTitle}',
								 null, newInvoicePOContent(${invoice.id}), null, '180px', 480, 'newInvoicePOValue'); "/>
								
								<c:if test="${isIssued}"> 
									<c:set var="classAtrb"  value="notvis" />
								</c:if>
									<cwms:securedJsLink permission="PURCHASE_ORDER_ADD_INVOICE" 
									onClick="${command}" classAttr="mainlink limitAccess ${classAtrb}" >
										<spring:message code="invoice.newinvoicepo"/>
									</cwms:securedJsLink>
								</td>
							</tr>
						</tfoot>
						<tbody>
							<c:choose>
								<c:when test="${invoice.pos.size() > 0}">
									<c:forEach var="po" items="${invoice.pos}">
										<tr>
											<td class="ponum">${po.poNumber}</td>
											<td class="link">
												<c:set var="escapedPoNumber"><spring:escapeBody htmlEscape="false" javaScriptEscape="true">${po.poNumber}</spring:escapeBody></c:set>
												<a href="#" class="mainlink limitAccess <c:if test="${isIssued}"> notvis </c:if>" 
													onclick=" event.preventDefault(); linkInvoicePOContent(${invoice.id}, ${po.poId}, '${escapedPoNumber}', 'invpo', null, null); ">
													<spring:message code="invoice.linktoitems" />
												</a>
											</td>
											<td class="remove">
												<a href="#" class="deleteAnchor limitAccess <c:if test="${isIssued}"> notvis </c:if>" 
													onclick=" event.preventDefault(); deleteInvoicePO(this, ${po.poId}); ">
													&nbsp;
												</a>
											</td>
										</tr>
									</c:forEach>									
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="3" class="bold text-center"><spring:message code="invoice.noinvoicepurchaseorders" /></td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</c:if>
				<%-- End of "old" invoice PO feature (targeted for removal soon) --%>
				</div>
				<div class="clear"></div>							

				<c:set var="rowcount" value="1" />
				<c:forEach var="jobLink" items="${invoice.jobLinks}">
					<table class="default4 displayCurrentInvoiceItemLinks">
						<thead>
							<tr>
								<td colspan="5"><spring:message code="invoice.invoiceitemsforjob" /> (<span class="bold">${jobLink.jobno}</span>)</td>
							</tr>
							<tr>
								<th class="itemno"><spring:message code="itemno" /></th>
								<th class="desc"><spring:message code="description" /></th>
								<th class="inst"><spring:message code="instmodelname" /></th>
								<th class="jobpos"><spring:message code="invoice.linkedjobpos" /></th>
								<th class="invpos"><spring:message code="invoice.linkedinvoicepos" /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="ii" items="${invoiceItemsWithPos}">
								<c:choose>
									<c:when test="${not empty ii.jobItem}" >
										<c:set var="jobid" value="${ii.jobItem.job.jobid}"/>
										<c:set var="jobno" value="${ii.jobItem.job.jobno}"/>
									</c:when>
									<c:when test="${not empty ii.expenseItem}" >
										<c:set var="jobid" value="${ii.expenseItem.job.jobid}"/>
										<c:set var="jobno" value="${ii.expenseItem.job.jobno}"/>
									</c:when>
									<c:otherwise>
										<c:set var="jobid" value="${null}"/>
										<c:set var="jobno" value="${null}"/>
									</c:otherwise>
								</c:choose>
								<c:if test="${not empty jobid}">
									<c:if test="${jobid == jobLink.job.jobid}">
										<tr class="invItemPORow${ii.id}">
											<td class="itemno">${ii.itemno}</td>	
											<td class="desc">
												<c:choose>
													<c:when test="${not empty ii.jobItem}" >
														<spring:message code="item" />
														<links:jobitemLinkDWRInfo jobitem="${ii.jobItem}" jicount="false" rowcount="${rowcount}"  />
													</c:when>
													<c:when test="${not empty ii.expenseItem}" >
														<spring:message code="invoice.service" />
														<c:out value=" ${ii.expenseItem.itemNo} " />
													</c:when>
												</c:choose>
												<spring:message code="invoice.onjob" />
												<cwms:securedLink permission="JOB_VIEW" classAttr="mainlink" parameter="?jobid=${jobid}">
													${jobno}
												</cwms:securedLink>
											</td>
											<td class="inst">
												<c:choose>
													<c:when test="${not empty ii.jobItem}" >
														<links:instrumentLinkDWRInfo instrument="${ii.jobItem.inst}" rowcount="${rowcount}" displayBarcode="false" displayName="true" displayCalTimescale="false" caltypeid="${ii.jobItem.calType.calTypeId}" />
													</c:when>
													<c:when test="${not empty ii.expenseItem}" >
														<cwms:showmodel instrumentmodel="${ii.expenseItem.model}"/><br/>
														<c:out value="${ii.expenseItem.comment}" />
													</c:when>
												</c:choose>													
											</td>												
											<td class="jobpos">
												<%-- Previously this would display links just for current job item PO links, now show just based on invoice --%>
												<c:forEach var="iipo" items="${ii.invItemPOs}">
													<c:if test="${not empty iipo.jobPO}">
														<div class="jobpo${jobid}${iipo.jobPO.poId} padding2">
															${iipo.jobPO.poNumber} (<spring:message code="invoice.jobpo" />)
														</div>
													</c:if>
													<%-- 
														Previously, we would display the line-through if the PO is also linked, as the PO takes precedence over the BPO 
														<div ${ poAlreadyOnII? 'style="text-decoration: line-through;" ':'' }
														However, changed to display all PO/BPO links and let the user interpret GB 2019-04-19 as some links were being hidden  
													--%>
													<c:if test="${not empty iipo.bpo}">
														<div class="jobpo${jobid}${iipo.bpo.poId} padding2">
															${iipo.bpo.poNumber} (<spring:message code="invoice.jobbpo" />)
														</div>
													</c:if>
												</c:forEach>
											</td>
											<td class="invpos">
												<invoice:linkInvoiceItemToPO invoiceitem="${ii}" />										
											</td>
										</tr>
										<c:set var="rowcount" value="${rowcount + 1}" />
									</c:if>
								</c:if>											
							</c:forEach>
						</tbody>
					</table>
				</c:forEach>
				<!-- End of jobLink forEach -->		
				<c:set var="unlinkedinvoiceitems" value="false" />
				<c:forEach var="ii" items="${invoice.items}">
					<c:if test="${(ii.jobItem == null) && (ii.expenseItem == null)}">
						<c:set var="unlinkedinvoiceitems" value="true" />
					</c:if>
				</c:forEach>
				<c:if test="${unlinkedinvoiceitems}">
					<table class="default4 displayCurrentInvoiceItemLinks">
						<thead>
							<tr>
								<td colspan="3"><spring:message code="invoice.invoiceitems" /></td>
							</tr>
							<tr>
								<th class="itemno"><spring:message code="itemno" /></th>
								<th class="descNoJob"><spring:message code="description" /></th>
								<th class="invpos"><spring:message code="invoice.linkedinvoicepos" /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="ii" items="${invoice.items}">
								<c:if test="${(empty ii.jobItem) && (empty ii.expenseItem)}">
									<tr class="invItemPORow${ii.id}">
										<td class="itemno">${ii.itemno}</td>	
										<td class="descNoJob">${ii.description.replaceAll("\\n", "")}</td>
										<td class="invpos">
											<invoice:linkInvoiceItemToPO invoiceitem="${ii}" />								
										</td>
									</tr>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
			<div id="delivery-tab" class="hid">
				<div class="displaycolumn">
					<table class="default4 width80 invDeliveries" summary="">
						<thead>
							<tr>
								<td colspan="2"><spring:message code="invoice.manualinvoicedeliveries" /> (<span class="invDelSize">${invoice.deliveries.size()}</span>)</td>
							</tr>
							<tr>
								<th class="delno"><spring:message code="invoice.deliveryno" /></th>
								<th class="remove"><spring:message code="remove" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="2"> 
									<a href="#" class="mainlink limitAccess <c:if test="${isIssued}"> notvis </c:if>" onclick=" event.preventDefault(); loadScript.createOverlay('dynamic', '<spring:message code="invoice.createinvoicedelivery" />', null, newInvoiceDeliveryContent(${invoice.id}), null, '180px', 480, 'newInvoiceDelValue'); "><spring:message code="invoice.newinvoicedelivery" /></a>
								</td>
							</tr>
						</tfoot>
						<tbody>
							<c:choose>
								<c:when test="${invoice.deliveries.size() > 0}">
									<c:forEach var="del" items="${invoice.deliveries}">
										<tr>
											<td class="delno">${del.deliveryNo}</td>
											<td class="remove"><a href="#" class="deleteAnchor limitAccess <c:if test="${!isIssued}"> notvis </c:if>" onclick=" event.preventDefault(); deleteInvoiceDelivery(this, ${del.id}); ">&nbsp;</a></td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="3" class="bold text-center"><spring:message code="invoice.noinvoicedeliveries" /></td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
			<div id="creditnotes-tab" class="hid">
				<div class="displaycolumn">
					<table class="default4 width80 invoiceCreditNotes" summary="<spring:message code="invoice.tableinvoicecreditnotes" />">
						<thead>
							<tr>
								<th class="creditno"><spring:message code="invoice.creditnoteno" /></th>
								<th class="amount"><spring:message code="invoice.amount" /></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="2">
									<!-- bolck the user to create a credit note if the invoice type is pro-forma -->
									<c:if test="${invoice.type.addToAccount}">
										<!-- Remove link if invoice is yet to be syncronised -->
										<c:choose>
											<c:when test="${isIssued}">
												<cwms:securedLink permission="CREDIT_NOTE_CREATE" classAttr="mainlink" parameter="?invid=${invoice.id}">
													<spring:message code="invoice.addcreditnote"/>
												</cwms:securedLink>
											</c:when>
											<c:otherwise>
												<strong><spring:message code="invoice.invoicenotbrokered" /></strong>
											</c:otherwise>
										</c:choose>
									</c:if>
								</td>
							</tr>
						</tfoot>
						<tbody>
							<c:choose>
								<c:when test="${invoice.creditNotes.size() > 0}">
									<c:forEach var="cn" items="${invoice.creditNotes}">
										<tr>
											<td class="creditno">
											<cwms:securedLink permission="CREDIT_NOTE_VIEW" classAttr="mainlink" parameter="?id=${cn.id}">
												${cn.creditNoteNo}
											</cwms:securedLink>
											</td>
											<td class="amount">${cn.finalCost}</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="2" class="bold text-center"><spring:message code="invoice.nocreditnotes" /></td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
			</div>
			<div id="periodicLinking-tab" class="hid">
			  <fieldset>
					<legend><spring:message code="invoice.periodicinvoice.manage" /></legend>
					<ul>
						<li>
							<span>
								<a class="mainlink"  href="viewPeriodicLinking.htm?id=${invoice.id}" > <spring:message code="invoice.periodicinvoice.view" /> </a>
							</span>
						</li>
					</ul>
				</fieldset>		
			</div>
			
			<div id="instructions-tab" class="hid">
				<cwms-instructions link-coid ="${ linkCoid }" link-subdivid ="${ linkSubdiv }" job-ids="${ jobids }" jobitem-ids="${ jobitemids }" instruction-types="INVOICE,DISCOUNT" show-mode="RELATED"></cwms-instructions>
			</div>
		</div>
		<div class="clear"></div>
		<t:showTabbedNotes entity="${invoice}" privateOnlyNotes="${privateOnlyNotes}" noteTypeId="${invoice.id}" noteType="INVOICENOTE"/>
	</jsp:body>
</t:crocodileTemplate>