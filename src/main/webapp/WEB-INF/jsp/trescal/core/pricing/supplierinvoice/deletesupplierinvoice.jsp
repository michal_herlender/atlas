<%-- File name: /trescal/core/pricing/supplierinvoice/deletesupplierinvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="supplierinvoice.deletesupplierinvoice" /></span>
	</jsp:attribute>
	<jsp:body>
		<%-- Binding to username (no form object) --%>
		<t:showErrors path="username"/>
		<div class="listbox">
			<form:form modelAttribute="form" method="post">
				<fieldset>
					<legend>
						<c:url var="urlPurchaseOrder" value='/viewpurchaseorder.htm?id=${supplierInvoice.purchaseOrder.id}' />
						<spring:message code="supplierinvoice.deletesupplierinvoiceforpurchaseorder" /> <a href="${urlPurchaseOrder}" class="mainlink" >${supplierInvoice.purchaseOrder.pono}</a>
					</legend>
					<ol>
						<li>
							<label for="supplierCompany" class="usage">
								<spring:message code="companyrole.supplier"/> <spring:message code="company"/>:
							</label>
							<div class="float-left">
								<links:companyLinkDWRInfo company="${supplierInvoice.purchaseOrder.contact.sub.comp}" rowcount="0" copy="true" />
							</div>
							<div class="clear"></div>									
						</li>
						<li>
							<label for="invoiceNumber" class="usage">
								<spring:message code="supplierinvoice.invoicenumber"/>:
							</label>
							${supplierInvoice.invoiceNumber}
							<div class="clear"></div>									
						</li>
						<li>
							<label for="submit" class="usage">&nbsp;</label>
							<input type="submit" id="submit" value="<spring:message code='delete'/>" />
							<div class="clear"></div>															
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>