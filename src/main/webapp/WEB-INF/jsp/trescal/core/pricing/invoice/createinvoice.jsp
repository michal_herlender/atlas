<%-- File name: /trescal/core/pricing/invoice/createinvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<script type='text/javascript' src='script/trescal/core/pricing/invoice/CreateInvoice.js'></script>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="invoice.preparedinvoice" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
		<script type="module" src="${pageContext.request.contextPath}/script/components/cwms-invoice/cwms-create-invoice/index.js"></script>
	</jsp:attribute>
	<jsp:body>
		<c:if test="${createinvoiceform.countItemsAlreadyInvoiced > 0}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="invoice.itemsalreadyinvoiced" arguments="${createinvoiceform.countItemsAlreadyInvoiced}" /></h5>
					</div>
				</div>
			</div>
		</c:if>
		<div class="infobox">
			<fieldset>
				<ol>
					<li>
						<label class="bold width30">
							<spring:message code="invoice.invoicepreparationfor" />:
						</label>
						<span>
							<c:choose>
								<c:when test="${createinvoiceform.companyWide}">
									<spring:message code="businesscompany" />
								</c:when>
								<c:otherwise>
									<spring:message code="businesssubdivision" />
								</c:otherwise>
							</c:choose>
						</span>
					</li>
					<li>
						<label class="bold width30">
							<spring:message code="businesscompany" />:
						</label>
						<span>${allocatedCompany.value}</span>
					</li>
					<li>
						<label class="bold width30">
							<spring:message code="businesssubdivision" />:
						</label>
						<span>
							<c:choose>
								<c:when test="${createinvoiceform.companyWide}"><spring:message code="all" /></c:when>
								<c:otherwise>${allocatedSubdiv.value}</c:otherwise>
							</c:choose>
						</span>
					</li>
					<li>
						<t:showCompanyInvoiceRequirements company="${company}" monthlyInvoice="${monthlyinvoicesystemdefault}" separateCostsInvoice="${separatecostsinvoicesystemdefault}" poInvoice="${poinvoicesystemdefault}"/>
					</li>
					<li>
						<t:showInstructions instructions="${instructions}"/>
					</li>
					<li>
						<label class="labelhead width80">
							<spring:message code="invoice.proposedinvoicesweresuggested" arguments="${createinvoiceform.proposedInvoice.size()}"/>
						</label>
						<span>&nbsp;</span>
					</li>
				</ol>
			</fieldset>
		</div>
		
		<form:form id="createinvoiceform" method="post" modelAttribute="createinvoiceform">
			<form:errors path="*">
        		<div class="warningBox1">
                	 <div class="warningBox2">
                    	   <div class="warningBox3">
                        	    <h5 class="center-0 attention">
                            	     <spring:message code='error.save' />
                            	</h5>
                            	<c:forEach var="e" items="${messages}">
                               		<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	 </div>
                 	</div>
           		</div>
       	    </form:errors>
			<div class="infobox" id="prepInvoice">
				<form:checkbox path="approveGoodsOnAllPOs" id="approveGoodsOnAllPOs" class="hid"/>
				<c:set var="count" value="0" />
				<c:forEach var="invoice" items="${createinvoiceform.proposedInvoice}" varStatus="invoiceLoopStatus">
	                <cwms-create-invoice 
						listJobSubdivs="${cwms:objectToJson(listJobSubdivs)}"
						listJobNos="${cwms:objectToJson(listJobNos)}"
						listJobStatus="${cwms:objectToJson(listJobStatus)}"
						listJobPOs="${cwms:objectToJson(listJobPOs)}"
						listJobBPOs="${cwms:objectToJson(listJobBPOs)}"
						listJobClientRefs="${cwms:objectToJson(listJobClientRefs)}"
						listJobItemClientRefs="${cwms:objectToJson(listJobItemClientRefs)}"
						listExpenseItemClientRefs="${cwms:objectToJson(listExpenseItemClientRefs)}"
						notInvoicedReasons="${cwms:objectToJson(notInvoicedReasons)}"
						periodicInvoices="${cwms:objectToJson(periodicInvoices)}"
						invoiceTypes="${cwms:objectToJson(invoiceTypes)}"
						invoiceNumber="${invoiceLoopStatus.index+1}" invoice="${cwms:objectToJson(invoice)}"></cwms-create-invoice>
				</c:forEach>
			</div>
			<div class="infobox center">
				<input id="submitButton" class="center" type="button" disabled="disabled" value="<spring:message code='invoice.createinvoicesupdatejobitems' />" onClick="beforeSubmit(this);"/>
			</div>
		</form:form>
		<div id="beforeSubmitDialog" class="dialog"></div>
	</jsp:body>						
</t:crocodileTemplate>