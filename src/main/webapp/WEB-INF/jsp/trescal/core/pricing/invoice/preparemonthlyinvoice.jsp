<%-- File name: /trescal/core/pricing/invoice/preparemonthlyinvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="invoice.preparejobiteminvoice" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/pricing/invoice/PrepareMonthlyInvoice.js'></script>
     
		<script>
			
			function updateInstructionSize(instructionSize) {}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
    </jsp:attribute>
	<jsp:body>
		<c:choose>
			<c:when test="${not empty preparemonthlyinvoiceform.company}">
				<!-- infobox div which contains all prepare invoice form elements and has nifty corners applied -->
				<div class="infobox" id="prepMonthlyInvoice">
					<form:form action="" id="createinvoiceform" method="post" modelAttribute="preparemonthlyinvoiceform">
						<form:errors path="*">
        					<div class="warningBox1">
                	 			<div class="warningBox2">
                    	   			<div class="warningBox3">
                        	    		<h5 class="center-0 attention">
                            	    		 <spring:message code='error.save' />
                            			</h5>
                            			<c:forEach var="e" items="${messages}">
                               				<div class="center attention"><c:out value="${e}"/></div>
                            			</c:forEach>
                     	 			</div>
                 				</div>
           					</div>
       	    			</form:errors>
       	    			
						<fieldset>
							<legend><spring:message code="invoice.invoicepreparationfor" />&nbsp;<c:out value="${preparemonthlyinvoiceform.company.coname}"/></legend>
							<ol>
								<li>
									<t:showCompanyInvoiceRequirements company="${preparemonthlyinvoiceform.company}" monthlyInvoice="${monthlyinvoicesystemdefault}" separateCostsInvoice="${separatecostsinvoicesystemdefault}" poInvoice="${poinvoicesystemdefault}"/>
								</li>
								<li>
									<cwms-instructions link-coid="${preparemonthlyinvoiceform.company.coid}" instruction-types="INVOICE,DISCOUNT" show-mode="TABLE"></cwms-instructions>
								</li>
								<li>
									<c:set var="currentSource" value=""/>
									<c:set var="checked" value=""/>
									<label><spring:message code="invoice.invoicesource" />:</label>
									<c:forEach var="source" items="${invoicesource}" varStatus="forEachStatus">
										<c:if test="${status.value == source || forEachStatus.index == 1}">
											<c:set var="currentSource" value="${source}"/>
											<c:set var="checked" value="checked"/>
										</c:if>
										<form:radiobutton path="source" onclick=" changeInvoiceSource(this.value); " value="${source}"
											checked= "${checked}"/>
										<spring:message code="${source.messageCode}" />
									</c:forEach>
								</li>
								<li>
									<label><spring:message code="invoice.invoicetype" />:</label>
									<form:select path="typeid">
										<c:forEach var="invt" items="${invoicetypes}">
											<option value="${invt.id}"><t:showTranslationOrDefault translations="${invt.nametranslation}" defaultLocale="${defaultlocale}"/></option>
										</c:forEach>
									</form:select>
								</li>
							</ol>
						</fieldset>
						
						<c:choose>
							<c:when test="${currentSource == 'SYSTEM'}">
								<c:set var="sysdisplay" value="block;" />
								<c:set var="nonsysdisplay" value="none;" />
							</c:when>
							<c:otherwise>
								<c:set var="sysdisplay" value="none;" />
								<c:set var="nonsysdisplay" value="block;" />
							</c:otherwise>
						</c:choose>
						<fieldset style=" display: ${nonsysdisplay}" id="nonsystem">
							<legend><spring:message code="invoice.enteroneormorejobnumbersforthisinvoicetocover" />:</legend>
							<ol>
								<li>
									<label><spring:message code="currency" />:</label>
									<form:select path="currencyId" items="${currencies}" itemLabel="value" itemValue="key" />
								</li>
								<li>
									<label><spring:message code="invoice.jobnos" />:</label>
									<div class="float-left" id="jobnoslist">
										<c:choose>
											<c:when test="${not empty preparemonthlyinvoiceform.jobNos}">
												<c:forEach var="jobn" items="${preparemonthlyinvoiceform.jobNos}" varStatus="jobnstatus">
													<div>
														<c:set var="count" value="${jobnstatus - 1}"/>
														<form:input path="jobNos[${count}]"/>
														<a href="#" onclick=" addJobNoField('jobNos', this); ">
															<img src="img/icons/add.png" class="img_marg_bot" width="16"
																 height="16" alt="<spring:message code='invoice.addjobnumber' />" title="<spring:message code='invoice.addjobnumber' />" />
														</a>
													</div>
												</c:forEach>
											</c:when>
											<c:otherwise>
													<div>
														<form:input path="jobNos"/>
														<a href="#"
														   onclick=" addJobNoField('jobNos', this);return false; ">
															<img src="img/icons/add.png" class="img_marg_bot" width="16"
																 height="16" alt="<spring:message code='invoice.addjobnumber' />" title="<spring:message code='invoice.addjobnumber'/>" />
														</a>
													</div>
											</c:otherwise>
										</c:choose>
									</div>
									<!-- clear floats and restore page flow -->
									<div class="clear"></div>
								</li>
								<li>
			
									<label><spring:message code="address" /></label>
									<c:if test="${addresses.isEmpty()}">
										<span class="error"><spring:message code="error.invoice.noaddresses" /></span>
									</c:if>	
									<span>
										<a target="_blank" href="<spring:url value='viewcomp.htm?coid=${preparemonthlyinvoiceform.company.coid}'/>" class="mainlink"><spring:message code="invoice.addaddress" /></a>
									</span>
								</li>
								<c:forEach var="address" items="${addresses}">
									<c:if test="${address.sub.active eq true}">
										<li>
											<c:set var="types" value=""/>
											<c:forEach var="func" items="${address.addressType}" varStatus="adresstatus" >
												<c:choose>
													<c:when test="${types == ''}">
														<c:set var="types" value="[${func.type}]"/>
													</c:when>
													<c:otherwise>
														<c:set var="types" value="${types},${func.type}"/>
													</c:otherwise>
												</c:choose>
													<c:if test="${func == 'INVOICE'}">
														<c:set var="checked" value="checked='checked'"/>
													</c:if>	
											</c:forEach>
											<c:set var="types" value="${types}"/>
											<label class="width80">
												<form:radiobutton path="addressid" value="${address.addrid}" onchange="onAddressChange(${address.sub.subdivid});"/>
													<a href="<spring:url value='viewaddress.htm?addrid=${address.addrid}'/>" class="mainlink"><address:showShortAddress address="${address}" /></a>&nbsp;&nbsp;
													-&nbsp;&nbsp;<a href="<spring:url value='viewsub.htm?subdivid=${address.sub.subdivid}'/>"  class="mainlink">${address.sub.subname}</a>&nbsp;${types}
											</label>
											<span>&nbsp;</span>
										</li>
									</c:if>
								</c:forEach>
								<li>
									<div class="clear"></div>
									<span>
										<input type="submit" value="<spring:message code='invoice.createinvoices' />" name="submit" />	
									</span>
								</li>										
							</ol>
																	
						</fieldset>
						
						<div id="system" style="display: ${sysdisplay}">
							<table class="default4" summary="<spring:message code='invoice.tablecompjobsreadyinvoice' />">
								<thead>
									<tr>
			
										<td colspan="7"><spring:message code="invoice.invoicepreparationfor" /> <strong>${preparemonthlyinvoiceform.company.coname}</strong></td>
			
									</tr>
									<tr>
										<th class="select" scope="col">
											<input type="checkbox" onclick=" selectAllItems(this.checked, 'prepMonthlyInvoice'); " />
											<spring:message code="all" />
										</th>
										<th class="jobno" scope="col"><spring:message code="jobno" /></th>
										<th class="po" scope="col"><spring:message code="invoice.ponos" /></th>
										<th class="contact" scope="col"><spring:message code="contact" /></th>
										<th class="datein" scope="col"><spring:message code="invoice.datein" /></th>
										<th class="clientref" scope="col"><spring:message code="clientref" /></th>
										<th class="items" scope="col"><spring:message code="items" /></th>
									</tr>
								</thead>
							</table>
							
							<c:choose> 
								<c:when test="${preparemonthlyinvoiceform.jobs.size() < 1}">
							
									<table class="default4" summary="<spring:message code='invoice.tablenojobsready' />">
										<tbody>
											<tr>
												<td colspan="7" class="bold center"><spring:message code="invoice.thiscompanyhasnojobsreadyforinvoicing" /></td>
											</tr>
										</tbody>
									</table>
									
								</c:when>
								<c:otherwise>
							
									<c:set var="subdivid" value="0"/>
									
									<table class="default4" summary="<spring:message code='invoice.tablecompsubdivjobs' />">
										<c:forEach var="job" items="${preparemonthlyinvoiceform.jobs}" varStatus="jobStatus">
			
											<c:if test="${job.con.sub.subdivid != subdivid}">
												<c:set var="subdivid" value="${job.con.sub.subdivid}"/>
												<tbody>
													<tr>
														<th colspan="7">
															<input type="checkbox" onclick=" selectAllItems(this.checked, 'subdiv${subdivid}'); " />
															<spring:message code="invoice.jobsfordivision" />: <strong>${job.con.sub.subname}</strong>
														</th>
													</tr>
												</tbody>
											</c:if>
											
											<tbody id="subdiv${subdivid}">														
												<tr>
													<td class="select">	
														<c:set var="checked" value=""/>
														<c:if test="${preparemonthlyinvoiceform.jobid == job.jobid}"> checked="checked" 
															<c:set var="checked" value="checked"/>
														</c:if>
														<form:checkbox path="jobids" value="${job.jobid}" checked="${checked}"/> 
													</td>
													<td class="jobno">
														<links:jobnoLinkDWRInfo rowcount="${rowcount}" jobno="${job.jobno}" jobid="${job.jobid}" copy="${true}"/>
													</td>
													<td>
														<c:choose>
															<c:when test="${job.POs.size() < 1 && (empty job.bpo)}">
																<div><spring:message code="invoice.nopurchaseorder" /></div>
													  		</c:when>
													  		<c:otherwise>
																<t:showPurchaseOrder job="${job}"/>											  		
													 		</c:otherwise>
													 	</c:choose>	
													</td>
													<td class="contact">${job.con.name}</td>
													<td class="datein"><fmt:formatDate value="${job.regDate}" type="date" dateStyle="SHORT"/></td>
													<td class="clientref">${job.clientRef}</td>
													<td class="items">${job.items.size()} items</td>
												</tr>											
											</tbody>
										</c:forEach>
									</table>
								</c:otherwise>
							</c:choose>
							<div class="center">
								<input type="submit" value="<spring:message code='viewjob.previnvoi' />" name="submit" />	
							</div>
						
						</div>
																	
					</form:form>
				
				</div>
				<!-- end of infobox div -->			
			</c:when>
			<c:otherwise>
				<!-- infobox div which contains all prepare invoice form elements and has nifty corners applied -->
				<div class="infobox">
					<fieldset>
						<legend><spring:message code="invoice.specifycompanytoinvoice" /></legend>
						<ol>
							<li>
								<label><spring:message code="invoice.choosecompany" />:</label>
								<div class="float-left extendPluginInput">
									<div class="compSearchJQPlugin">
										<!-- company search results displayed here -->
										<input type="hidden" name="field" value="coid" />
										<input type="hidden" name="compCoroles" value="client,business" />
										<input type="hidden" name="tabIndex" value="1" />
									</div>
								</div>									
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="button" name="button" value="<spring:message code='invoice.prepareinvoice' />" onclick=" window.location='<spring:url value="/preparemonthlyinvoice.htm?coid="/>' + $j('input[name=coid]').val(); " />			
							</li>
						</ol>
					</fieldset>
				</div>
				<!-- end of infobox div -->
			<!-- infobox div which contains all prepare invoice form elements and has nifty corners applied -->
			<div class="infobox">
				<table class="default2" id="readyToInvoiceTable" summary="<spring:message code='invoice.tablealljobsreadyinvoice' />">
					<thead>
						<tr>
							<td colspan="4">
								<spring:message code="invoice.jobsreadytoinvoice" /> (${readytoinv.size()})
							</td>
						</tr>
						<tr>
							<th scope="col" class="comp"><spring:message code="company" /></th>
							<th scope="col" class="comprole"><spring:message code="company.role" /></th>
							<th scope="col" class="jobs"><spring:message code="invoice.completejobs" /></th>
							<th scope="col" class="invoice"><spring:message code="viewjob.previnvoi" /></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${readytoinv.size() < 1}">
								<tr class="odd">
									<td colspan="4" class="center bold"><spring:message code="invoice.therearenojobsreadytoinvoice" /></td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set var="rowcount" value="0"/>
								<c:forEach var="co" items="${readytoinv}">
									<tr <c:choose><c:when test="${rowcount % 2 == 0}">  class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose>>
										<td>${co.company.coname}</td>
										<td class="center">${co.company.companyRole.message}</td>
										<td class="center">${co.completeJobs}</td>
										<td class="center">
											<a href="<spring:url value='preparemonthlyinvoice.htm?coid=${co.company.coid}'/>">
												<img src="img/icons/page.png" width="16" height="16" alt="Invoice ${co.company.coname}" title="<spring:message code='viewjob.previnvoi' /> ${co.company.coname}" />
											</a>
										</td>
									</tr>
									<c:set var="rowcount" value="${rowcount + 1}"/>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<!-- end of infobox div -->
			</c:otherwise>
		</c:choose>
	</jsp:body>
</t:crocodileTemplate>