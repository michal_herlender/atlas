<%-- File name: /trescal/core/pricing/jobcost/createjobcosting.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="jobcost.createjobcosting" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			
			function updateInstructionSize(instructionSize) {
				$j('span.instructionSize').text(instructionSize);
			}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
		<script src='script/trescal/core/pricing/jobcost/CreateJobCosting.js'></script>
	</jsp:attribute>
	<jsp:body>
		<form:errors path="jobCostingForm.*">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention"><spring:message code="jobcost.errorcreatecosting"/>:</h5>
						<c:forEach var="e" items="${messages}">
							<div class="center attention"><c:out value="${e}"/></div>
						</c:forEach>
					</div>
				</div>
			</div>
		</form:errors>
		<c:set var="showInstructions" value="false"/>
		<c:forEach var="userInstructionType" items="${userInstructionTypes}">
			<c:if test="${userInstructionType.instructionType == 'COSTING' || userInstructionType.instructionType == 'DISCOUNT'}">
				<c:forEach var="instructionLink" items="${relatedInstructionLinks}">
					<c:if test="${userInstructionType.instructionType == instructionLink.instruction.instructiontype}">
						<c:set var="showInstructions" value="true"/>
					</c:if>
				</c:forEach>
			</c:if>
		</c:forEach>
		<!-- main div which contains all create costing form elements and has nifty corners applied -->
		<div class="infobox">
			<form:form modelAttribute="jobCostingForm" method="post" onsubmit="$j('#submit').attr('disabled', 'disabled');">
				<form:hidden path="jobid"/>
				<fieldset>
					<legend>
						<spring:message code="jobcost.createcostingforjob"/>&nbsp;
						<links:jobnoLinkDWRInfo rowcount="0" jobno="${job.jobno}" jobid="${job.jobid}" copy="true"/>
					</legend>
					<ol>
						<c:if test="${showInstructions}">
<%-- 						<li><t:showTableOfInstructionTypesToUser instructionLinks="${job.relatedInstructionLinks}" userInstructionTypes="${userInstructionTypes}" stringType1="Costing" stringType2="Discount"/></li> --%>
							<li>
								<cwms-instructions link-jobid="${job.jobid}" instruction-types="DISCOUNT,COSTING" show-mode="TABLE"></cwms-instructions>
							</li>
						</c:if>
						<li>
							<label><spring:message code="company"/>:</label>
							<span>${job.con.sub.comp.coname}</span>
						</li>
						<li>
							<label for="personid"><spring:message code="jobcost.primarycontact"/>:</label>
							<form:select path="personid" items="${contacts}" itemValue="personid" itemLabel="name"/>
						</li>
						<li>
							<label><spring:message code="jobcost.additionalcontacts"/>:</label>
							<span class="float-left"><form:select path="additionalPersonids" multiple="true" items="${contacts}" itemValue="personid" itemLabel="name"/></span>
							<span class="float-left"><spring:message code="jobcost.additionalcontactsnote"/></span>
						</li>
						<li>
							<form:label path="clientCosting"><spring:message code="jobcost.costingtype"/>:</form:label>
							<form:select path="clientCosting" items="${clientcost}" itemLabel="localizedName" />
							<%-- Previously onchange="toggleContactSelection(this); return false;" --%>
						</li>
						<li>
							<form:label path="jobCostingTypeId"><spring:message code="jobcost.costingstatus"/>:</form:label>
							<form:select path="jobCostingTypeId" items="${jobcosttypelist}" itemValue="id" itemLabel="type"/>
						</li>
						<li>
							<form:label path="clientref"><spring:message code="clientref"/>:</form:label>
							<form:input type="text" path="clientref" size="100" />
						</li>
						<li>																							
							<table class="default2" id="jobitems">
								<thead>
									<tr>
										<td colspan="9"><spring:message code="jobcost.jobitems"/> (${job.items.size()})</td>
									</tr>
									<tr>
										<th id="select" scope="col">
											<span>
												<input type="checkbox" onclick="selectAllItems(this.checked, 'jobitems');"/>
												<spring:message code="all"/>
											</span>
										</th>
										<th id="item" scope="col"><spring:message code="instrument"/></th>
										<th id="po" scope="col"><spring:message code="purchaseorder"/></th>
										<th id="serial" scope="col"><spring:message code="serialno"/></th>
										<th id="plant" scope="col"><spring:message code="plantno"/></th>
										<th id="jicontractreview.jobitemclientref" scope="col"><spring:message code="jicontractreview.jobitemclientref"/></th>
										<th id="caltype" scope="col"><spring:message code="viewinstrument.customerdescription"/></th>
										<th id="caltype" scope="col"><spring:message code="caltype"/></th>
										<th id="status" scope="col"><spring:message code="status"/></th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="9">&nbsp;</td>
									</tr>
								</tfoot>
								<tbody>
									<c:forEach var="item" items="${job.items}">
										<!-- auto select items if they are already awaiting costing -->
										<c:set var="checked" value=""/>
										<c:forEach var="link" items="${item.state.groupLinks}">
											<c:if test="${link.group == 'CSJOBCOSTING'}">
												<c:set var="checked" value="checked"/>
											</c:if>
										</c:forEach>
										<tr class="even">
											<td><input type="checkbox" value="${item.jobItemId}" name="jobItemIds" ${checked}/></td>
											<td><links:showInstrumentLink instrument="${item.inst}"/></td>
											<td>
												<c:forEach var="jobItemPO" items="${item.jobItemPOs}" varStatus="loopStatus">
													${jobItemPO.po.poNumber.concat(" ")}${jobItemPO.bpo.poNumber}
													<c:if test="${loopStatus.first}">
														<br>
													</c:if>
												</c:forEach>
											</td>
											<td>${item.inst.serialno}</td>
											<td>${item.inst.plantno}</td>
											<td>${item.clientRef}</td>
											<td>${item.inst.customerDescription}</td>
											<td class="center"><cwms:besttranslation translations="${item.calType.serviceType.shortnameTranslation}"/></td>
											<td><cwms:besttranslation translations="${item.state.translations}"/></td>																														
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</li>
						<li>
							<table class="default2" id="expenseItems">
								<thead>
									<tr>
										<th id="select" scope="col">
											<span>
												<input type="checkbox" onclick="selectAllItems(this.checked, 'expenseItems');"/>
												<spring:message code="all"/>
											</span>
										</th>
										<th id="expitype" scope="col"><spring:message code="servicetype"/></th>
										<th id="expicomment" scope="col"><spring:message code="comments"/></th>
										<th id="expidate" scope="col"><spring:message code="date"/></th>
										<th id="expisingleprice" scope="col"><spring:message code="singleprice"/></th>
										<th id="expiquantity" scope="col"><spring:message code="quantity"/></th>
										<th id="expiprice" scope="col"><spring:message code="totalcost"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="expi" items="${job.expenseItems}">
										<tr>
											<td><input type="checkbox" value="${expi.id}" name="expenseItemIds"/></td>
											<td><cwms:besttranslation translations="${expi.serviceType.shortnameTranslation}"/></td>
											<td>${expi.comment}</td>
											<td><fmt:formatDate value="${expi.date}"/></td>
											<td>${expi.singlePrice}</td>
											<td>${expi.quantity}</td>
											<td>${expi.totalPrice}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" id="submit" value="<spring:message code='create'/>"/>
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
		<!-- end of main job div -->
	</jsp:body>
</t:crocodileTemplate>