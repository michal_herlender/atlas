<%-- File name: /trescal/core/pricing/creditnote/createcreditnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
      <jsp:attribute name="header">
            <span class="headtext"><spring:message code="creditnote.createcreditnoteforinvoice" />&nbsp;${invoice.invno}</span>
      </jsp:attribute>
      <jsp:attribute name="scriptPart">
      </jsp:attribute>
      <jsp:body>
      <div class="infobox">

	  <fieldset>
		
		<legend><spring:message code="creditnote.createcreditnoteselectstartingpoint" /></legend>
		
		<form:form action="" method="post" modelAttribute="createcreditnoteform">
		
			<ol>
		
				<li>
					<form:radiobutton id="creditall" path="creditWholeInvoice" value="true" checked="checked"/>
					<label for="creditall" style=" display: inline; float: none; width: auto; "><spring:message code="creditnote.startbycreditingtheentireinvoiceusingthesamenominalspread" /></label>
					<div class="clear">&nbsp;</div>
					<form:radiobutton id="creditnone" path="creditWholeInvoice" value="false"/>
					<label for="creditnone" style=" display: inline; float: none; width: auto; "><spring:message code="creditnote.startfromscratchandadditemsmanuallytothecreditnote" /></label>
					<div class="clear">&nbsp;</div>
				</li>	
				<li>
					<input type="submit" value="<spring:message code="submit"/>" />

				</li>
				
			</ol>
		
		</form:form>
		
		</fieldset>
	</div>
	</jsp:body>
</t:crocodileTemplate>