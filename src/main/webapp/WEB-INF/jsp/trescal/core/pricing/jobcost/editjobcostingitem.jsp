<%-- File name: /trescal/core/pricing/jobcost/editjobcostingitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="link" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="jobcost.editjobcostingitem"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/pricing/jobcost/EditJobCostingItem.js'></script>
    </jsp:attribute>
    <jsp:body>
						
			<c:set var="item" value="${command.item}"/>
			<c:set var="jobItem" value="${item.jobItem}"/>
			<c:set var="currency" value="${item.jobCosting.currency}"/>
			<c:set var="totalDiscount" value="${item.calibrationCost.discountValue + item.repairCost.discountValue
			+ item.adjustmentCost.discountValue + item.purchaseCost.discountValue }"></c:set>		
			
			<form:form action="" method="post" id="editjobcostingitem" modelAttribute="command">
			<form:errors path="*">
       				 <div class="warningBox1">
              		  <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="jobcost.errorssavingversionofcosting"/></h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                   </div>
                  </div>
        	 </form:errors>
																		
				<div class="infobox">
				
					<jobs:inlineJobCostingItemNavigation item="${item}" /> 								
																															
					<table class="default2" summary="<spring:message code='jobcost.tablecurrentjob'/>">
						<thead>
							<tr>
								<td colspan="7">
									<spring:message code="jobcost.editcostingitem"/> 
									<link:jobcostingItemLinkDWRInfo jobcostingitem="${item}" rowcount="0" />
									<spring:message code="jobcost.oncosting"/>
									<a href="<spring:url value='viewjobcosting.htm?id=${item.jobCosting.id}'/>" class="mainlink">&nbsp;<c:out value="${item.jobCosting.qno}"/> version ${item.jobCosting.ver}</a>
								</td>
							</tr>
							<tr>
								<th class="inst" scope="col"><spring:message code="instrument"/></th>
								<th class="serial" scope="col"><spring:message code="serialno"/></th>
								<th class="plant" scope="col"><spring:message code="plantno"/></th>
								<th class="caltype" scope="col"><spring:message code="caltype"/></th>
								<th class="status" scope="col"><spring:message code="status"/></th>
								<th class="disc" scope="col"><spring:message code="jobcost.discount"/></th>
								<th class="finalcost" scope="col"><spring:message code="finalcost"/></th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="7">&nbsp;</td>
							</tr>
						</tfoot>
						<tbody>
							<tr>
								<td> <links:instrumentLinkDWRInfo instrument="${jobItem.inst}" rowcount="0" displayBarcode="false" displayName="true" displayCalTimescale="false" caltypeid="${jobItem.calType.calTypeId}" /></td>
								<td>${jobItem.inst.serialno}</td>
								<td>${jobItem.inst.plantno}</td>
								<td class="center"><t:showTranslationOrDefault translations="${jobItem.calType.serviceType.shortnameTranslation}" defaultLocale="${defaultlocale}"/></td>
								<td>${jobItem.state.description}</td>
								<td class="right">${currency.currencyERSymbol}${item.generalDiscountValue + totalDiscount}</td>
								<td class="right">${currency.currencyERSymbol}${item.finalCost}</td>
							</tr>
						</tbody>
					</table>
																									
				</div>
				
				<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
				<div id="subnav">
					<dl>
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'calibrationcosts-tab', false); $j('input#openTab').val('calibrationcosts-tab'); return false;" id="calibrationcosts-link" class="selected" title="<spring:message code='jobcost.editcalibrationcostsforjobcostingitem'/>"><spring:message code="jobcost.calibrationcosts"/></a></dt>
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'repaircosts-tab', false); $j('input#openTab').val('repaircosts-tab'); return false;" id="repaircosts-link" title="<spring:message code='jobcost.editrepaircostsforjobcostingitem'/>"><spring:message code="jobcost.repaircosts"/></a></dt>
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'adjustmentcosts-tab', false); $j('input#openTab').val('adjustmentcosts-tab'); return false; " id="adjustmentcosts-link" title="<spring:message code='jobcost.editadjustmentcostsforjobcostingitem'/>"><spring:message code="jobcost.adjustmentcosts"/></a></dt>
						<dt><a href="#" onclick="switchMenuFocus(menuElements, 'purchasecosts-tab', false); $j('input#openTab').val('purchasecosts-tab'); return false;" id="purchasecosts-link" title="<spring:message code='jobcost.editpurchasecostsforjobcostingitem'/>"><spring:message code="jobcost.purchasecosts"/></a></dt>
					</dl>
				</div>
				<!-- end of sub navigation menu -->
																		
				<div class="infobox" id="costs">
					
					<!-- div containing calibration cost form elements -->
					<div id="calibrationcosts-tab">
						
						<form:hidden path="item.calibrationCost.active" />
						<c:set var="calibrationCost" value="${item.calibrationCost}"/>
						
						<fieldset>
							
							<legend>
								<spring:message code="jobcost.calibrationcharges"/>
								<c:set var="display" value="hid" />
								<c:choose>
									<c:when test="${item.calibrationCost.active == true}">
									- (<a href="#" class="mainlink" onclick=" toggleIncludeCost('calibration', this); return false; "><spring:message code="remove"/></a>)
									<c:set var="display" value="vis" />
									</c:when>
									<c:otherwise>	
									- (<a href="#" class="mainlink" onclick=" toggleIncludeCost('calibration', this); return false; "><spring:message code="add"/></a>)
									</c:otherwise>
								</c:choose>
							</legend>
								
							<c:set var="displayTPCosts" value="hid" />
							<c:if test="${item.calibrationCost.thirdCostTotal > 0}">
								<c:set var="displayTPCosts" value="vis" />
							</c:if>	
							
							<div id="calibrationMessage" <c:choose><c:when test="${display == 'hid'}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>>
								
								<ol>												
									<li>
										<spring:message code="jobcost.nocalibrationcostsset"/> 
									</li>												
								</ol>
								
							</div>
							
							<div id="calibrationMain" class="${display}">
																														
								<ol>
									
									<li>
											<form:label path="item.calibrationCost.houseCost"><spring:message code="jobcost.housecalibrationprice"/>:</form:label>
											<div class="symbolbox">${currency.currencyERSymbol}</div>
											<form:input type="text" path="item.calibrationCost.houseCost" id="item.calibrationCost.houseCost" onkeyup=" calculateNewInspectionCost(this.value); return false; " class="right" value="${command.item.calibrationCost.houseCost}" />
											<span id="manualCalCostError" class="attention hid"><img src="img/icons/error.png" width="16" height="16" class="image_inl" alt="<spring:message code='jobcost.calibrationcostinvalid'/>" title="<spring:message code='jobcost.calibrationcostinvalid'/>"/> <spring:message code="jobcost.thispriceisinvalid"/></span>
											<c:if test="${displayTPCosts == 'hid'}">
												<a href="#" class="mainlink" onclick=" displayTPCosts('calibration'); return false;"><spring:message code="jobcost.thirdpartycosts"/>&nbsp;(${item.jobItem.linkedTPQuotes.size()}&nbsp;<spring:message code="jobcost.linkedquot"/>)</a>
											</c:if>
											<form:errors path="item.calibrationCost.houseCost"	class="attention calibrationcosts-erro" />
									</li>
									
								</ol>
								
								<ol id="calibrationTP" class="${displayTPCosts}">
									
									<c:set var="costName" value="calibration" />
									<costs:showJobCostingTPCost item="${item}" costName="${costName}" cost="${item.calibrationCost}"/>
								</ol>
								
								<ol class="suborderedlist">
									
									<li>
										<label><spring:message code="jobcost.beforediscount"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${calibrationCost.totalCost}
											</span>
										
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>												
									<li>
										<label><spring:message code="jobcost.discount"/>:</label>
										<div class="symbolbox"></div>
										<form:input path="item.calibrationCost.discountRate" class="right" tabindex="" /> %
										<c:if test="${not empty calibrationCost.discountValue}">
											[${currency.currencyERSymbol} ${calibrationCost.discountValue}] 
										</c:if>
										<form:errors path="item.calibrationCost.discountRate" element="span" cssClass="attention calibrationcosts-error" />
									</li>												
									<li>
										<label><spring:message code="jobcost.total"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${calibrationCost.finalCost}
											</span>
											
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
									
								</ol>
								
							</div>
							
						</fieldset>
						
						<div class="center">
							<div class="warningBox1 hid">
								<div class="warningBox2">
									<div class="warningBox3">
										<img src="img/icons/error.png" width="16" height="16" class="image_inl" alt="<spring:message code='jobcost.calibrationcostinvalid'/>" title="<spring:message code='jobcost.calibrationcostinvalid'/>" />
										&nbsp;<spring:message code="jobcost.warningcostscannotberecalculatedorsaveduntilinvaliddataentryisfixed"/>
									</div>
								</div>
							</div>
							
								<form:input type="hidden" id="openTab" path="openTabId" value="${command.openTabId}" />
								
								<input type="submit" name="submit" value="<spring:message code='save'/>" />
						</div>
						
					</div>
					<!-- end of calibration cost div -->
					
					<!-- div containing repair cost form elements -->
					<div id="repaircosts-tab" class="hid">
						
						<form:hidden path="item.repairCost.active" />
						<c:set var="repairCost" value="${item.repairCost}"/>
						
						<fieldset>
							
							<legend>
								<spring:message code="jobcost.repaircharges"/> 
								<c:set var="display" value="hid" />
								<c:choose>
									<c:when test="${item.repairCost.active == true}">
									- (<a href="#" class="mainlink" onclick=" toggleIncludeCost('repair', this); return false; "><spring:message code="remove"/></a>)
									<c:set var="display" value="vis" />
									</c:when>
									<c:otherwise>
									- (<a href="#" class="mainlink" onclick=" toggleIncludeCost('repair', this); return false; "><spring:message code="add"/></a>)
									</c:otherwise>
								</c:choose>
							</legend>
							
							<c:set var="displayTPCosts" value="hid" />
							<c:if test="${item.repairCost.thirdCostTotal > 0}">
								<c:set var="displayTPCosts" value="vis" />
							</c:if>
							
							<div id="repairMessage" <c:choose><c:when test="${display == 'hid'}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>>
								
								<ol>												
									<li>
										<spring:message code="jobcost.norepaircostsset"/> 
									</li>												
								</ol>
								
							</div>
							
							<div id="repairMain" class="${display}">
								
								<ol>
									
									<li>
										<label><spring:message code="jobcost.labourtime"/>:</label>
										<div class="symbolbox"></div>
										<form:input path="item.repairCost.labourTime"  class="right" tabindex="" /> <spring:message code="jobcost.mins"/>
										<form:errors path="item.repairCost.labourTime" element="span" cssClass="attention repaircosts-error" />
									</li>
									<li>
										<label><spring:message code="jobcost.hourlyrate"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<form:input path="item.repairCost.hourlyRate"  class="right" tabindex="" />
										<form:errors path="item.repairCost.hourlyRate" element="span" cssClass="attention repaircosts-error" />
									</li>
									<li>
										<label><spring:message code="jobcost.labourcost"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${item.repairCost.labourCostTotal}
											</span>
											
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>									
									</li>
									<li>
										<label><spring:message code="jobcost.partscost"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<form:input path="item.repairCost.partsCost"  class="right" tabindex="" value="${repairPartsPrice}" />
										<form:errors path="item.repairCost.partsCost" element="span" cssClass="attention repaircosts-error" />
									</li>
									<li>
										<label><spring:message code="jobcost.partsmarkupsource"/>:</label>
										<form:select path="item.repairCost.partsMarkupSource" onchange=" toggleRepMarkUpDisplay(this.value); return false; ">
											<c:forEach var="pm" items="${partsmarkupsources}">
												<option value="${pm}" <c:if test="${item.repairCost.partsMarkupSource == pm}"> selected="selected" </c:if>>${pm.name}</option>
											</c:forEach>
										</form:select>
										<form:errors path="item.repairCost.partsMarkupSource" element="span" cssClass="attention repaircosts-error" />
									</li>
									<li>
										<label><spring:message code="jobcost.partsmarkup"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${item.repairCost.partsMarkupValue}
											</span>
										
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
									<c:choose>
										<c:when test="${item.repairCost.partsMarkupSource == 'SYSTEM_DERIVED'}">
											<c:set var="fixedDisplay" value="vis" />
											<c:set var="editableDisplay" value="hid" />
										</c:when>
										<c:otherwise>
											<c:set var="fixedDisplay" value="hid" />
											<c:set var="editableDisplay" value="vis" />
										</c:otherwise>
									</c:choose>
									<li id="fixedrepairmarkup" class="${fixedDisplay}">
										<label><spring:message code="jobcost.partsmarkuprate"/>:</label>
										<c:choose>
											<c:when test="${not empty item.repairCost.partsMarkupRate}">
												<div class="symbolbox"></div>
												<div class="valuebox">
												<span class="valuetext">
													${item.repairCost.partsMarkupRate}
												</span>
												<span class="symbolbox2"> %</span>
												</div>		
											<!-- clear floats and restore page flow -->
												<div class="clear"></div>
											</c:when>
											<c:otherwise>
												N/A&nbsp;&nbsp;(fixed value markup)
											</c:otherwise>
										</c:choose>
									</li>
									<li id="editablerepairmarkup" class="${editableDisplay}">
										<label><spring:message code="jobcost.partsmarkuprate"/>:</label>
										<div class="symbolbox"></div>
										<form:input path="item.repairCost.partsMarkupRate"  class="right" tabindex="" /> %
										<form:errors path="item.repairCost.partsMarkupRate" element="span" cssClass="attention repaircosts-error" />
									</li>
									<li>
										<label><spring:message code="jobcost.partstotal"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${item.repairCost.partsTotal}
											</span>
											<span class="symbolbox2">
												<c:if test="${displayTPCosts == 'hid'}">
													<a href="#" class="mainlink" onclick=" displayTPCosts('repair'); return false; "><spring:message code="jobcost.thirdpartycosts"/>&nbsp;(${item.jobItem.linkedTPQuotes.size()}&nbsp;<spring:message code="jobcost.linkedquot"/>)</a>
												</c:if>
											</span>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>											
									</li>
								</ol>	
																			
								<ol id="repairTP" class="${displayTPCosts}">												
									<c:set var="costName" value="repair" />
									<costs:showJobCostingTPCost item="${item}" costName="${costName}" cost="${item.repairCost}"/>
								</ol>
																			
								<ol class="suborderedlist">
									
									<li>
										<label><spring:message code="jobcost.beforediscount"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${repairCost.totalCost}
											</span>
										
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>												
									<li>
										<label><spring:message code="jobcost.discount"/>:</label>
										<div class="symbolbox"></div>
										<form:input path="item.repairCost.discountRate"  class="right" tabindex="" /> %
										<c:if test="${not empty repairCost.discountValue}">
											[${currency.currencyERSymbol} ${repairCost.discountValue}] 
										</c:if>
										<form:errors path="item.repairCost.discountRate" element="span" cssClass="attention repaircosts-error" />
									</li>												
									<li>
										<label><spring:message code="jobcost.total"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${repairCost.finalCost}
											</span>
										
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
									
								</ol>
								
							</div>
							
						</fieldset>
						
						<div class="center">
							<div class="warningBox1 hid">
								<div class="warningBox2">
									<div class="warningBox3">
										<img src="img/icons/error.png" width="16" height="16" class="image_inl" alt="<spring:message code='jobcost.calibrationcostinvalid'/>" title="<spring:message code='jobcost.calibrationcostinvalid'/>" />
										&nbsp;<spring:message code="jobcost.warningcostscannotberecalculatedorsaveduntilinvaliddataentryisfixed"/> 
									</div>
								</div>
							</div>
							<input type="submit" name="submit" value="<spring:message code='save'/>" />
						</div>
						
					</div>
					<!-- end of repair cost div -->
					
					<!-- div containing adjustment cost form elements -->
					<div id="adjustmentcosts-tab" class="hid">
						
						<form:hidden path="item.adjustmentCost.active" />
						<c:set var="adjustmentCost" value="${item.adjustmentCost}"/>
						
						<fieldset>
							
							<legend>
								<spring:message code="jobcost.adjustmentcharges"/>  
								<c:set var="display" value="hid" />
								<c:choose>
									<c:when test="${item.adjustmentCost.active == true}">
									- (<a href="#" class="mainlink" onclick=" toggleIncludeCost('adjustment', this); return false; "><spring:message code="remove"/></a>)
									<c:set var="display" value="vis" />
									 </c:when>
									 <c:otherwise>
									- (<a href="#" class="mainlink" onclick=" toggleIncludeCost('adjustment', this); return false; "><spring:message code="add"/></a>)
									</c:otherwise>
								</c:choose>
							</legend>
							
							<c:set var="displayTPCosts" value="hid" />
							<c:if test="${item.adjustmentCost.thirdCostTotal > 0}">
								<c:set var="displayTPCosts" value="vis" />
							</c:if>
							
							<div id="adjustmentMessage" <c:choose><c:when test="${display == 'hid'}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>>
								
								<ol>												
									<li>
										<spring:message code="jobcost.noadjustmentcostsset"/>
									</li>												
								</ol>
								
							</div>
							
							<div id="adjustmentMain" class="${display}">
								
								<ol>
									<li>
										<label><spring:message code="fixedprice"/>:</label>
										<div class="symbolbox"></div>
										<form:input path="item.adjustmentCost.fixedPrice"  class="right" tabindex="" />
										<form:errors path="item.adjustmentCost.fixedPrice" element="span" cssClass="attention adjustmentcosts-error" />
									</li>
									<li>
										<label><spring:message code="jobcost.labourtime"/>:</label>
										<div class="symbolbox"></div>
										<form:input path="item.adjustmentCost.labourTime"  class="right" tabindex="" /> <spring:message code="jobcost.mins"/>
										<form:errors path="item.adjustmentCost.labourTime" element="span" cssClass="attention adjustmentcosts-error" />
									</li>
									<li>
										<label><spring:message code="jobcost.hourlyrate"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<form:input path="item.adjustmentCost.hourlyRate"  class="right" tabindex="" />
										<form:errors path="item.adjustmentCost.hourlyRate" element="span" cssClass="attention adjustmentcosts-error" />
									</li>
									<li>
										<label><spring:message code="jobcost.labourcost"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${item.adjustmentCost.labourCostTotal}
											</span>
											<span class="symbolbox2">
												<c:if test="${displayTPCosts == 'hid'}">
													<a href="#" class="mainlink" onclick=" displayTPCosts('adjustment'); return false;"><spring:message code="jobcost.thirdpartycosts"/>&nbsp;(${item.jobItem.linkedTPQuotes.size()}&nbsp;<spring:message code="jobcost.linkedquot"/>)</a>
												</c:if>
											</span>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>													
									</li>
									
								</ol>
																			
								<ol id="adjustmentTP" class="${displayTPCosts}">												
									<c:set var="costName" value="adjustment" />
									<costs:showJobCostingTPCost item="${item}" costName="${costName}" cost="${item.adjustmentCost}"/>
								</ol>
																			
								<ol class="suborderedlist">
												
									<li>
										<label><spring:message code="jobcost.beforediscount"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${adjustmentCost.totalCost}
											</span>
										
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>												
									<li>
										<label><spring:message code="jobcost.discount"/>:</label>
										<div class="symbolbox"></div>
										<form:input path="item.adjustmentCost.discountRate"  class="right" tabindex="" /> %
										<c:if test="${not empty adjustmentCost.discountValue}">  
										</c:if>	
										<form:errors path="item.adjustmentCost.discountRate" element="span" cssClass="attention adjustmentcosts-error" />
									</li>
									
									<li>
										<label><spring:message code="jobcost.total"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${adjustmentCost.finalCost}
											</span>
											<span class="symbolbox2">
											</span>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
									
								</ol>
								
							</div>
								
						</fieldset>
						
						<div class="center">
							<div class="warningBox1 hid">
								<div class="warningBox2">
									<div class="warningBox3">
										<img src="img/icons/error.png" width="16" height="16" class="image_inl" alt="<spring:message code='jobcost.calibrationcostinvalid'/>" title="<spring:message code='jobcost.calibrationcostinvalid'/>" />
										&nbsp;<spring:message code="jobcost.warningcostscannotberecalculatedorsaveduntilinvaliddataentryisfixed"/>
									</div>
								</div>
							</div>
							<input type="submit" name="submit" value="<spring:message code='save'/>" />
						</div>
						
					</div>
					<!-- end of adjustment cost div -->
					
					<!-- div containing purchase cost form elements -->
					<div id="purchasecosts-tab" class="hid">
						
						<form:hidden path="item.purchaseCost.active" />
						<c:set var="purchaseCost" value="${item.purchaseCost}"/>
						
						<fieldset>
							
							<legend>
								<spring:message code="jobcost.purchasecharges"/>
								<c:set var="display" value="hid" />
								 <c:choose>
									 <c:when test="${item.purchaseCost.active == true}">
										- (<a href="#" class="mainlink" onclick=" toggleIncludeCost('purchase', this); return false; "><spring:message code="remove"/></a>)
									<c:set var="display" value="vis" />
									</c:when>
									<c:otherwise>
										- (<a href="#" class="mainlink" onclick=" toggleIncludeCost('purchase', this); return false; "><spring:message code="add"/></a>)
									</c:otherwise>
								</c:choose>
							</legend>
								
							<c:set var="displayTPCosts" value="hid" />
							<c:if test="${item.purchaseCost.thirdCostTotal > 0}">
								<c:set var="displayTPCosts" value="vis" />
							</c:if>	
							
							<div id="purchaseMessage" <c:choose><c:when test="${display == 'hid'}"> class="vis" </c:when><c:otherwise> class="hid" </c:otherwise></c:choose>>
								
								<ol>												
									<li>
										<spring:message code="jobcost.nopurchasecostsset"/>
									</li>												
								</ol>
								
							</div>
							
							<div id="purchaseMain" class="${display}">
								
								<ol>
									
									<li>
										<label><spring:message code="jobcost.housepurchaseprice"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<form:input path="item.purchaseCost.houseCost"  class="right" tabindex="" />
										<c:if test="${displayTPCosts == 'hid'}">
											<a href="#" class="mainlink" onclick=" displayTPCosts('purchase'); return false; "><spring:message code="jobcost.thirdpartycosts"/>&nbsp;(${item.jobItem.linkedTPQuotes.size()}&nbsp;<spring:message code="jobcost.linkedquot"/>)</a>
										</c:if>	
										<form:errors path="item.purchaseCost.houseCost" element="span" cssClass="attention purchasecosts-error" />
									</li>
									
								</ol>
																			
								<ol id="purchaseTP" class="${displayTPCosts}">												
									<c:set var="costName" value="purchase" />
									<costs:showJobCostingTPCost item="${item}" costName="${costName}" cost="${item.purchaseCost}"/>
								</ol>
																			
								<ol class="suborderedlist">
									
									<li>
										<label><spring:message code="jobcost.beforediscount"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${purchaseCost.totalCost}
											</span>
											<span class="symbolbox2">
											</span>
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>												
									<li>
										<label><spring:message code="jobcost.discount"/>:</label>
										<div class="symbolbox"></div>
										<form:input path="item.purchaseCost.discountRate"  class="right" tabindex="" /> %
										<c:if test="${not empty purchaseCost.discountValue}">
											[${currency.currencyERSymbol} ${purchaseCost.discountValue}] 
										</c:if>
										<form:errors path="item.purchaseCost.discountRate" element="span" cssClass="attention purchasecosts-error" />
									</li>												
									<li>
										<label><spring:message code="jobcost.total"/>:</label>
										<div class="symbolbox">${currency.currencyERSymbol}</div>
										<div class="valuebox">
											<span class="valuetext">
												${purchaseCost.finalCost}
											</span>
											
										</div>
										<!-- clear floats and restore page flow -->
										<div class="clear"></div>
									</li>
									
								</ol>
								
							</div>
								
						</fieldset>
						
						<div class="center">
							<div class="warningBox1 hid">
								<div class="warningBox2">
									<div class="warningBox3">
										<img src="img/icons/error.png" width="16" height="16" class="image_inl" alt="<spring:message code='jobcost.calibrationcostinvalid'/>" title="<spring:message code='jobcost.calibrationcostinvalid'/>" />
										&nbsp;<spring:message code="jobcost.warningcostscannotberecalculatedorsaveduntilinvaliddataentryisfixed"/>
									</div>
								</div>
							</div>
							<input type="submit" name="submit" value="<spring:message code='save'/>" />
						</div>
						
					</div>
					<!-- end of purchase costs div -->								
					
				</div>
				<!-- end of infobox div containing cost types -->
				
				<!-- infobox div containing cost totals -->
				<div class="infobox">
												
					<fieldset>
						
						<legend><spring:message code="jobcost.totalitemcharges"/></legend>
						
						<ol>
							
							<li>
								<label><spring:message code="jobcost.totalcost"/></label>
								<div class="symbolbox">${currency.currencyERSymbol}</div>
								<div class="valuebox">
									<span class="valuetext">
										${item.totalCost}													
									</span>
								
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<c:if test="${item.generalDiscountValue.signum() != 0}">
								<li>
									<label><spring:message code="jobcost.globaldiscount"/></label>
									<div class="symbolbox"></div>
									<div class="valuebox">
										<span class="valuetext"> ${item.generalDiscountRate}</span>
										<span class="symbolbox2">
											% [${currency.currencyERSymbol} ${item.generalDiscountValue}] 
										</span>
									</div>
								</li>
							</c:if>
							<li>
								<label><spring:message code="jobcost.finalcost"/></label>
								<div class="symbolbox">${currency.currencyERSymbol}</div>
								<div class="valuebox">
									<span class="valuetext">
										${item.finalCost}
									</span>
									
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<form:label path="item.inspectionCost.finalCost"><spring:message code="jobcost.inspectioncost"/></form:label>
								<div class="symbolbox">${currency.currencyERSymbol}</div>
								<form:input type="text" path="item.inspectionCost.finalCost" id="jciInspectionCost" class="right" value="${command.item.inspectionCost.finalCost}" />										
																		
								<c:if test="${item.jobCosting.costingType.includeInspectionCharges == false}">
									- <spring:message code="jobcost.notethiscostingtype"/> (${item.jobCosting.costingType.type}) <spring:message code="jobcost.willnotdisplayinspectioncharges"/>
								</c:if>		
							</li>
							
						</ol>
							
					</fieldset>
					
					<jobs:inlineJobCostingItemNavigation item="${item}" />
													
				</div>
				<!-- end of infobox div containing totals -->
				
			</form:form>
    </jsp:body>
</t:crocodileTemplate>