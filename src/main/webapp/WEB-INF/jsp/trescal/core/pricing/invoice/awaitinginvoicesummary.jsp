<%-- File name: /trescal/core/pricing/invoice/awaitinginvoicesummary.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="invoice.awaitinginvoicesummary" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="text/javascript" src="script/trescal/core/pricing/invoice/AwaitingInvoiceSummary.js"></script>
		<style>
			#subdivSelectorDiv ,#monthSelectorDiv {
				display: flex;
			}
		</style>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox div containing results -->
		<div class="infobox">
			<div id="invoiceScopeDiv">
				<span class="bold"></span><spring:message code="invoice.invoicepreparationfor" />:</span>
				<span class="bold">
					<c:choose>
						<c:when test="${companyWide}">
							<spring:message code="businesscompany" />
							<c:out value=" - ${allocatedCompany.value} " />
							<a href="awaitinginvoicesummary.htm?companyWide=false" class="mainlink">
								(<spring:message code="purchaseorder.switch" />)
							</a>
						</c:when>
						<c:otherwise>
							<spring:message code="businesssubdivision" />
							<c:out value=" - ${allocatedSubdiv.value} " />
							<a href="awaitinginvoicesummary.htm?companyWide=true" class="mainlink">
								(<spring:message code="purchaseorder.switch" />)
							</a>
						</c:otherwise>
					</c:choose>
				</span>
			</div>
			<div class="float-right">
				<cwms:securedLink permission="JOB_ITEM_INVOICE_PREPARE" collapse="True" classAttr="mainlink" parameter="?companyWide=${companyWide}">
					<spring:message code="invoice.preparejobiteminvoice"/>
				</cwms:securedLink>
				|
				<cwms:securedLink permission="INVOICE_HOME" collapse="True" classAttr="mainlink" parameter="?companyWide=${companyWide}">
					<spring:message code="menu.invoicing.home"/>
				</cwms:securedLink>
			</div>
			<div class="clear"></div>
			<div>	<!-- style=" width: 200px; " -->
				<span>
					<spring:message code="invoice.jobcompleted" />:
					<select class="jobCompleteFilter" onchange="filter('${wrapper.id}-table')">
						<option value="ALL"><spring:message code="all" /></option>
						<option value="YES"><spring:message code="yes" /></option>
						<option value="NO"><spring:message code="no" /></option>
					</select>
				</span>
			</div>
			<div class="clear"></div>
			<table id="${wrapper.id}-table" class="default2 tablesorter" >									
				<thead>
					<tr>
						<td colspan="6">
							${wrapper.description} (<span class="${wrapper.id}Size" id="jobitemsCount" >${wrapper.quantity}</span>)
						</td>
					</tr>
					<tr>
						<th class="summarytoinvoiceBusinessSubdiv"><spring:message code="businesssubdivision" /></th>
						<th class="summarytoinvoiceClientCompany"><spring:message code="clientcompany" /></th>
						<th class="summarytoinvoiceYearMonth">
							<spring:message code="invoice.jobitemscompleted" />
							<c:out value=" / " />
							<spring:message code="periodiclinking.clientdelivery" />
						</th>
						<th class="summarytoinvoiceJobItems"><spring:message code="jobitems" /></th>
						<th class="summarytoinvoiceJobItemsComplete">(<spring:message code="invoice.jobcompleted" />)</th>
						<th class="summarytoinvoiceJobItemsIncomplete">()<spring:message code="invoice.jobongoing" />)</th>
				<%--	<th class="summarytoinvoiceProforma"><spring:message code="invoice.proformatoinvoice" /></th> --%>
				<%--	<th class="summarytoinvoiceExpense"><spring:message code="viewjob.jobservices" /></th> --%>
					</tr>
				</thead>												
				<tfoot>
					<tr>
						<td colspan="6">&nbsp;</td>
					</tr>
				</tfoot>	
				
				<tbody>
					<c:choose>
						<c:when test="${empty wrapper.contents}">
							<tr class="odd">
								<td colspan="6" class="center bold"><spring:message code="invoice.therearenojobsawaitinginvoice" /></td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach var="summaryDto" items="${wrapper.contents}" varStatus="loop">
								<c:set var="lineclass" value="${loop.index % 2 == 0 ? 'even' : 'odd'}" />
								<tr class="${lineclass}" jobcomplete="${summaryDto.completeJob}" jitotal="${summaryDto.jobItemCount}" 
								jijobcomplete="${summaryDto.jiWithCompleteJob}" jijobnocomplete="${summaryDto.jiWithIncompleteJob}">
									<td><c:out value="${summaryDto.businessSubdivName}" /></td>
									<td><c:out value="${summaryDto.clientCompanyName}" /></td>
									<td class="center"><c:out value="${summaryDto.completionYearMonth}" /></td>

									<td class="totalji center"><c:if test="${summaryDto.jobItemCount > 0}"><c:out value="${summaryDto.jobItemCount}" /></c:if></td>
									<td class="jijobcomplete center"><c:if test="${summaryDto.jiWithCompleteJob > 0}"><c:out value="${summaryDto.jiWithCompleteJob}" /></c:if></td>
									<td class="jijobnocomplete center"><c:if test="${summaryDto.jiWithIncompleteJob > 0}"><c:out value="${summaryDto.jiWithIncompleteJob}" /></c:if></td>
<%-- <td><c:if test="${summaryDto.proformaCount > 0}"><c:out value="${summaryDto.proformaCount}" /></c:if></td> --%>
<%-- <td><c:if test="${summaryDto.expenseItemCount > 0}"><c:out value="${summaryDto.expenseItemCount}" /></c:if></td> --%>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
	</jsp:body>
</t:crocodileTemplate>
