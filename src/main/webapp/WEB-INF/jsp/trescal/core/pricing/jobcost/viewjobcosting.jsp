<%-- File name: /trescal/core/pricing/jobcost/viewjobcosting.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="link" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<jsp:useBean id="now" class="java.util.Date" />
<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="jobcost.viewjobcosting" />
			<c:out
				value=" ${command.jobCosting.qno} ver ${command.jobCosting.ver}" /></span>
		<style>
			.linkdisabled {
			        pointer-events: none;
			        cursor: default;
			}
		</style>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="module" src="${pageContext.request.contextPath}/script/components/cwms-datetime/cwms-datetime.js"></script>
		<script>
			
			function updateInstructionSize(instructionSize) {
				$j('span.instructionSize').text(instructionSize);
			}
		</script>
		<script type='text/javascript'
			src='script/trescal/core/pricing/jobcost/ViewJobCosting.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${command.jobCosting.job.jobno}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<c:set var="costing" value="${command.jobCosting}" />
		<c:set var="job" value="${costing.job}" />
		<form:errors path="*">
       				 <div class="warningBox1">
              		  <div class="warningBox2">
              	       <div class="warningBox3">
                    	<h5 class="center-0 attention"><spring:message code="jobcost.errorcreatecosting"/></h5>
                      <c:forEach var="e" items="${messages}">
                        <div class="center attention"><c:out value="${e}"/></div>
                      </c:forEach>
                     </div>
                   </div>
                  </div>
       </form:errors>
		
		<c:if test="${costing.costingType.includeInspectionCharges == false}">
			<div class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<h5 class="center-0 attention">
							<spring:message code="jobcost.notethiscostingtype" /> (${costing.costingType.type}) <spring:message
								code="jobcost.willnotdisplayinspectioncharges" />
						</h5>
					</div>
				</div>
			</div>								
		</c:if>
		
		<!-- Mark if quotation is locked for editing -->
		<c:if test="${requiresIssue == false}">
			<div id="lockedmessage" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<spring:message
							code="jobcost.pleasenoteallcostsarewithinclientapprovallimitsthiscostingdoesnotrequireissuing" />
						<br />
						<%-- TODO: Consider implementing override in future (doesn't exist currently) --%>
						<!-- <a href="#" class="mainlink" onclick=" event.preventDefault(); alert('implement this'); "><spring:message code="jobcost.clickheretopushitemsthroughwithoutissue" /></a>  --> 
					</div>
				</div>
			</div>	
		</c:if>
		
		<!-- infobox contains all job costing information and is styled with nifty corners -->
		<div class="infobox">
			<fieldset>
				<legend>
					<spring:message code="jobcost.costing" />
					<c:out value=": ${costing.qno} vers ${costing.ver} (" />
					<spring:message code="jobcost.forjob" />
					<c:out value=" ${job.jobno})" />
				</legend>
				
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="company" />:</label>
							<span><link:companyLinkDWRInfo
	 									company="${costing.contact.sub.comp}" rowcount="0" copy="true" /></span>
						</li>
						<li>
							<label><spring:message code="subdivision" />:</label>
							<span><link:subdivLinkDWRInfo rowcount="0"
									subdiv="${costing.contact.sub}" /></span>
						</li>
						<li>
							<label><spring:message code="jobcost.jobcontact" />:</label>
							<span><link:contactLinkDWRInfo
									contact="${costing.contact}" rowcount="0" /></span>
						</li>
						<c:if test="${costing.additionalContacts.size() > 0}">	
							<li>
								<label><spring:message code="jobcost.additionalcontacts" />:</label>
								<div class="float-left">																								
									<c:forEach var="additionalContact"
										items="${costing.additionalContacts}">
										<div class="marg-top-small">
											<link:contactLinkDWRInfo
												contact="${additionalContact.contact}" rowcount="0" />
										</div>
									</c:forEach>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
						</c:if>
						<li>
							<label><spring:message code="job" />:</label>
							<span><link:jobnoLinkDWRInfo rowcount="0"
									jobno="${job.jobno}" jobid="${job.jobid}" copy="true" /></span>
						</li>
						<li>
							<label><spring:message code="createdon" />:</label>
							<div class="float-left">
								${cwms:dateDifference(costing.regdate, date)} -
								<fmt:formatDate value="${costing.regdate}" type="date" dateStyle="SHORT" /> 
								<br />
								${costing.createdBy.name}
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="jobcost.lastupdate" />:</label>
							<div class="float-left">
								${cwms:dateDifference(costing.lastUpdateOn, date)} -
								<fmt:formatDate value="${costing.lastUpdateOn}"
									type="date" dateStyle="SHORT"  /> - <fmt:formatDate
									value="${costing.lastUpdateOn}" type="time"  timeStyle="SHORT" />
								<br />
								${costing.lastUpdateBy.name}
							</div>
							<div class="clear"></div>
						</li>
						<c:set var="showInstructions" value="false" />
						 <c:forEach var="i" items="${instLinks}">
							<c:forEach var="uit" items="${userInstructionTypes}">
								<c:if test="${uit.instructionType == i.instruction.instructiontype}">
									<c:set var="showInstructions" value="true" />
								</c:if>
							</c:forEach>
						</c:forEach>
						<li>
							<label><spring:message code="jobcost.relatedinstructions" />:</label>
 							<cwms-instructions link-jobid="${job.jobid}" instruction-types="DISCOUNT,COSTING" show-mode="BASIC_IN_HEAD"></cwms-instructions>
						</li>																				
					</ol>
				</div>
				
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="jobcost.version" />:</label>
							<span>${costing.ver}</span>
							<c:if test="${othercostings.size() > 1}">
								<span style="padding-left: 30px;">
									(													
									<c:forEach var="c" items="${othercostings}">
										<c:if test="${c.ver != costing.ver}">
											<link:jobcostingLinkDWRInfo jobcosting="${c}" rowcount="0"
 												showno="false" />				
										</c:if>
									</c:forEach>
									)
								</span>
							</c:if>
						</li>
						<li>
							<label><spring:message code="type" />:</label>
							<span>${costing.type}</span>
						</li>
						<li>
							<label><spring:message code="status" />:</label>
							<span><t:showTranslationOrDefault
									translations="${costing.status.nametranslations}"
									defaultLocale="${defaultlocale}" /></span>
							<c:if test="${!costing.status.issued and !costing.status.accepted and !costing.status.rejected }">
								<div class="float-right padtop">
									<!-- beware id of link used when closing email thickbox to update status and visibility of link -->
									<c:if
										test="${not empty costing.issueMethod and costing.issueMethod eq 'ADVESO'}">
									<a href="#" class="mainlink-float"
											id="makeCostingAvailableToAdveso"
											onclick=" event.preventDefault(); displayAvailableFileToAdveso(${costing.id}, ${costing.ver}, ${job.jobid}); "><spring:message
												code="stategroup.displayname.makecostingavailabletoadveso" /></a>
											</c:if>
											<c:if
										test="${empty costing.issueMethod or costing.issueMethod ne 'ADVESO'}">
									<a href="#" class="mainlink-float" id="issueCostingLink"
											onclick=" event.preventDefault(); issueCosting(this, ${costing.id}); "><spring:message
												code="jobcost.setcostingasissued" /></a>
											</c:if>
								</div>
							</c:if>
						</li>
						<c:if test="${ not empty costing.lastClientApproval and not empty costing.lastClientApproval.clientApproval }">
							<li>
								<label><spring:message code="jobcost.clientapprovaldate" />:</label>
								<fmt:formatDate value="${costing.lastClientApproval.clientApprovalOn}" type="both" dateStyle="SHORT" timeStyle="SHORT" />
							</li>
						</c:if>
						<li>
							<label><spring:message code="jobcost.costingtype" />:</label>
							<span>${costing.costingType.type}</span>
						</li>
						<li>
							<label><spring:message code="clientref" />:</label>
							<span>${costing.clientref} &nbsp;</span>
						</li>
						<li>
							<label><spring:message code="currency" />:</label>
							<span>
								<c:set var="currency" value="${costing.currency}" />
								${currency.currencyCode}
								(${currency.currencySymbol} ${currency.currencyName}
								- <fmt:formatNumber value="1" type="currency"
									currencySymbol="${defaultCurrency.currencyERSymbol}" /> : <fmt:formatNumber
									value="${costing.rate}" type="currency"
									currencySymbol="${currency.currencyERSymbol}" />)
							</span>
						</li>										
					</ol>
				</div>
			</fieldset>
		</div>
		
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt>
					<a href="#"
						onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'costingitems-tab', false); "
						id="costingitems-link"
						title="<spring:message code='jobcost.viewjobcostingitems' />"
						class="selected">
						<spring:message code="items" /> (${command.jobCosting.items.size()})
					</a>
				</dt> 
				<dt>
					<a href="#"
						onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'editcosting-tab', false); "
						id="editcosting-link"
						title="<spring:message code='jobcost.editjobcostingdetails' />">
						<spring:message code="edit" />
					</a>
				</dt>
				<dt>
					<a href="#"
						onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'costingfiles-tab', false);  "
						id="costingfiles-link"
						title="<spring:message code='jobcost.viewdocumentsinjobcostingdirectory' />">
						<spring:message code="jobcost.documents" /> (<span
						class="fileResourceCount">${scRootFiles.numberOfFiles}</span>)
					</a>
				</dt>
				<c:if test="${not empty costing.job.returnTo.costCentre}">
					<dt>
						<a href="#"
							onclick=" event.preventDefault(); genDocPopup(${costing.id}, 'costcentrebirtdocument.htm?id=', 'Generating Cost Centre Document'); "
							title="<spring:message code='jobcost.generatecostcentredocument' />">
							<spring:message code="jobcost.generateccpor" />
						</a>
					</dt>
				</c:if>
				<dt>
					<a href="#"
						onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'email-tab', false); "
						id="email-link"
						title="<spring:message code='jobcost.viewsendemailsforjobcosting' />">
						<spring:message code="emails" /> (<span
						class="entityEmailDisplaySize">${costing.sentEmails.size()}</span>)
					</a>
				</dt>
				<dt disabled="true">
					<a href="#"
						onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'clientdecision-tab', false); "
						id="clientresponse-link"
						title="<spring:message code='jobcost.clientdecision' />" ${ !costing.status.issued?'class="disabled linkdisabled"':'' }>
						<spring:message code="jobcost.clientdecision.decision" /> 
					</a>
				</dt>
				<dt>
					<a href="#"
						onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'instruction-tab', false); "
						id="instruction-link"
						title="<spring:message code='jobcost.viewaddinstructionsforjobcosting' />">
						<spring:message code="jobcost.instructions" /> (<span
						class="instructionSize">${instLinks.size()}</span>)
					</a>
				</dt>
				<dt>
					<a href="#"
						onclick=" event.preventDefault(); deleteCosting(${costing.id}); "
						title="<spring:message code="jobcost.deletejobcosting" />">
						<spring:message code="delete" />
					</a>
				</dt>
				<dt>
					<a href="#"
						onclick=" event.preventDefault(); genDocPopup(${costing.id}, 'birtjobcostingdocument.htm?id=', '<spring:message code="jobcost.generatingjobcostingdocument"/>'); "
						title="<spring:message code="jobcost.generatejobcostingdocument" />">
						<spring:message code="generate" />
					</a>
				</dt>
			</dl>
		</div>
		
		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<!-- this section lists all items on the costing --> 
			<div id="costingitems-tab">
				<div id="jobcostingitems">
					<div class="float-right">
						<a href="#" class="mainlink" id="showall"
							onclick=" event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, 0); "
							title="<spring:message code='jobcost.showallcostbreakdowns' />"><spring:message
								code="jobcost.expandall" /></a>
						|
						<a href="#" class="mainlink" id="hideall"
							onclick="event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, 0);"
							title="<spring:message code="jobcost.showallcostbreakdowns" />"><spring:message
								code="jobcost.collapseall" /></a>
					</div>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>

					<table class="default4" id="jci${jobCostingItem.id}"
						summary="<spring:message code='jobcost.thistabledisplaysalljobcostingitems' />">
						<thead>
							<tr>
								<td colspan="9"><spring:message
										code="jobcost.jobcostingitems" /> (<span id="costItemSize">${command.jobCosting.items.size()}</span>)</td>
							</tr>
						</thead>
						<thead>	
							<tr>
								<th class="item" scope="col"><spring:message code="item" /></th>
								<th class="model" scope="col"><spring:message code="model" /></th>
								<th class="notes" scope="col">
									<a href="#"
									onclick=" event.preventDefault(); toggleNoteDisplay(this, 'JOBCOSTINGITEMNOTE', 'vis', null); "
									class="toggleNoteDisplay">
										<img src="img/icons/note_expand.png" width="20" height="16"
										alt="<spring:message code='jobcost.expandallnotes' />"
										title="<spring:message code='jobcost.expandallnotes' />" />
									</a>
								</th>
								<th class="trescalid" scope="col"><spring:message
										code="trescalid" /></th>
								<th class="serial" scope="col"><spring:message
										code="serialno" /></th>
								<th class="plant" scope="col"><spring:message
										code="plantno" /></th>
								<th class="caltype" scope="col" class="center"><spring:message
										code="caltype" /></th>
								<th class="del" scope="col" class="center"><spring:message
										code="delete" /></th>
							</tr>
						</thead>
					</table>
					<c:set var="items" value="${command.jobCosting.items.toArray() }" />
					<c:forEach var="jobCostingItemDto" items="${JobCostingItemsDto}"
						varStatus="i">
						<c:set var="jcinotecount" 
							value="${jobCostingItemDto.publicActiveNoteCount + jobCostingItemDto.privateActiveNoteCount}" />
						<table class="default4" id="jci${jobCostingItemDto.id}"
							summary="<spring:message code='jobcost.thistabledisplaysalljobcostingitems' />">
							
							<tbody>
								<c:set var="color"
									value="${jobCostingItemDto.serviceTypeDisplayColor}" />
								<c:if test="${jobCostingItemDto.jobItemTurn <= fastTrackTurn}">
									<c:set var="color"
										value="${jobCostingItemDto.serviceTypeDisplayColorFastTrack}" />
								</c:if>
								<tr id="item${jobCostingItemDto.id}"
									style=" background-color: ${color}; ">
									<td class="item">
										<link:jobcostingItemDtoLinkDWRInfo
											jobitemdefaultpage="${ jobCostingItemDto.jobItemDefaultPage }"
											jobitemid="${ jobCostingItemDto.jobItemId }"
											jobitemno="${ jobCostingItemDto.jobItemNo }"
											jobcostingitemid="${ jobCostingItemDto.id }" rowcount="0" />
										<a href="#" id="hide${jobCostingItemDto.id}"
										onclick=" event.preventDefault(); toggleCostSummaryDisplay('costsummary', this.id, ${jobCostingItemDto.id}); ">
											<img src="img/icons/hideinfo.png" width="16" height="16"
											class="img_marg_bot"
											alt="<spring:message code='jobcost.hidecostbreakdown' />"
											title="<spring:message code='jobcost.hidecostbreakdown' />" />
										</a>
									</td>												
									<td class="model">
										<instmodel:jobCostingViewShowInstrumentModelLink
											instrumentcalibrationstandard="${ jobCostingItemDto.instrumentCalibrationStandard }"
											instrumentscrapped="${ jobCostingItemDto.instrumentScrapped }"
											instrumentmodelid="${ jobCostingItemDto.instrumentModelId }"
											modelnametranslation="${ jobCostingItemDto.modelName }"
											instrumentmodelname="${ jobCostingItemDto.instrumentModelName }"
											instrumentsubfamilytypology="${ jobCostingItemDto.typology }"
											instrumentmodelmfrtype="${ jobCostingItemDto.modelMfrType }"
											instrumentmfrname="${ jobCostingItemDto.instrumentMfrName }"
											instrumentgenericmfr="${ jobCostingItemDto.genericMfr }" />
									</td>
									<td class="notes"><link:showNotesLinks
											noteTypeId="${jobCostingItemDto.id}"
											noteType="JOBCOSTINGITEMNOTE" colspan="9"
											notecounter="${jcinotecount}" /></td>
									<td class="trescalid">${jobCostingItemDto.instrumentId}</td>
									<td class="serial">${jobCostingItemDto.instrumentSerialNo}</td>
									<td class="plant">${jobCostingItemDto.instrumentPlantNo}</td>
									<td class="caltype">${jobCostingItemDto.serviceTypeShortNameTranslation}</td>
									<td class="del">
										<a href="#"
										onclick=" event.preventDefault(); deleteItem(${jobCostingItemDto.id}); ">
											<img src="img/icons/delete.png" width="16" height="16"
											alt="<spring:message code='jobcost.deletejobcostingitem' />"
											title="<spring:message code='jobcost.deletejobcostingitem' />" />
										</a>
									</td>																					
								</tr>												
								<c:set var="rowspan" value="2" />
								<c:forEach var="cost" items="${jobCostingItemDto.costs}">
									<c:if test="${cost.active == true}">
										<c:if
											test="${cost.costTypeName != 'INSPECTION' || (cost.costTypeName == 'INSPECTION' && jobCostingItemDto.showInspectionCost)}">
											<c:set var="rowspan" value="${rowspan + 1}" />
										</c:if>
										
									</c:if>
								</c:forEach>
								<!-- Don't show a cost breakdown for site costings -->
								<c:if test="${jobCostingItemDto.jobCostingType != 'SITE'}">										
									<tr class="costsummary${jobCostingItemDto.id}">
										<td rowspan="${rowspan}">&nbsp;</td>
										<td class="bold"><spring:message
												code="jobcost.costtype" /></td>
										<td colspan="2" class="bold"><spring:message
												code="jobcost.costsource" /></td>
										<td class="bold center"><spring:message
												code="jobcost.base.amount" /></td>
										<td class="bold center"><spring:message
												code="jobcost.discount" /></td>
										<td class="bold center"><spring:message
												code="jobcost.discount" /></td>
										<td class="bold center"><spring:message 
												code="finalcost" /></td>
									</tr>
									<c:set var="discountValueTotal" value="0.00" />
									<c:set var="finalCostInspection" value="0.00" />
									<c:set var="inspectionTypeMessageCode" value=""/>
									<c:set var="BaseAmountTotal" value="0.00"/>
									<c:forEach var="cost" items="${jobCostingItemDto.costs}"> 
										<c:if test="${cost.active == true && cost.costTypeName == 'INSPECTION' && jobCostingItemDto.showInspectionCost}">
												<c:set var="finalCostInspection" value="${cost.finalCost}"/>
												<c:set var="inspectionTypeMessageCode" value="${cost.costTypeMessageCode}"/>
										</c:if>
										<c:if test="${cost.active == true && cost.costTypeName != 'INSPECTION'}">												
												<tr class="costsummary${jobCostingItemDto.id} nobord">
													<td>
														<spring:message code="${cost.costTypeMessageCode}" />
													</td>
													<td colspan="2">
														<c:if test="${not empty cost.costSrc}">
															<c:choose>
																<c:when test="${cost.costSrc == CostSource.QUOTATION}">
																	${cost.costSrc}
																	<link:jobCostingViewShowQuotationLink
																		quotationid="${jobCostingItemDto.quotationId}"
																		quotationno="${jobCostingItemDto.quotationNo}"
																		quotationver="${jobCostingItemDto.quotationVer}" />
																	<c:choose>
																		<c:when test="${cost.quotationIssued != true}">
																			[<span class="attention"
																				title="<spring:message code='jobcost.thisquotationhasnotbeenissued' />"><spring:message
																					code="jobcost.unissued" /></span>]
																		</c:when>
																		<c:when test="${cost.quotationExpired == true}">
																			[<span class="attention"
																				title="<spring:message code='jobcost.thisquotationhasexpired' />"><spring:message
																					code="jobcost.expired" /></span>]
																		</c:when>
																		<c:otherwise>
																			[<span class="attention"
																				title="<spring:message code='jobcost.quotationissuedon' />"><fmt:formatDate
																					value="${cost.quotationIssuedate}"
																					type="date" dateStyle="SHORT" /></span>]
																		</c:otherwise>
																	</c:choose>
																	<spring:message code="item" /> <a
																		href="editquotationitem.htm?id=${cost.quotationItemId}"
																		class="mainlink">${cost.quotationItemNo}</a>
																</c:when>
																<c:otherwise>
																	${cost.costSrc}
																</c:otherwise>
															</c:choose>
														</c:if>
													</td>
													<td class="center">
													   	<fmt:formatNumber value="${cost.totalCost}" type="currency"
															currencySymbol="${currency.currencyERSymbol}" />
														<c:set var="BaseAmountTotal" value="${ BaseAmountTotal + cost.totalCost}"/>
													</td>
													<td class="center">
													   ${cost.discountRate}%
													</td>
													<td class="center">
													   	<fmt:formatNumber value="${cost.discountValue}" type="currency"
															currencySymbol="${currency.currencyERSymbol}" />
													</td>	
													<td class="center">
														<fmt:formatNumber value="${cost.finalCost}"
															type="currency"
															currencySymbol="${currency.currencyERSymbol}" />
													</td>																													 
												</tr>	
												<c:if test="${cost.costTypeName != 'INSPECTION'}">
													<c:set var="discountValueTotal" value="${ discountValueTotal + cost.discountValue}" />
												</c:if>									
										</c:if>
									</c:forEach>
									<tr class="nobord"> 
										<td colspan="2" class="bold"><spring:message code="jobcost.item.total" /></td>
										<td></td>
										<td class="center" > <fmt:formatNumber value="${BaseAmountTotal}" type="currency"
											currencySymbol="${currency.currencyERSymbol}" /> </td>
										<td class="center"></td>
										<td class="center"><fmt:formatNumber value="${discountValueTotal + jobCostingItemDto.generalDiscountValue}" type="currency"
											currencySymbol="${currency.currencyERSymbol}" /></td>
										<td>
											<a href="editjobcostingitem.htm?id=${jobCostingItemDto.id}"
															class="mainlink" ${ onholdstatus?'class="disabled linkdisabled"':'' }>
															<fmt:formatNumber value="${jobCostingItemDto.finalCost}"
																type="currency"
																currencySymbol="${currency.currencyERSymbol}" />
											</a>
										</td>
									</tr>
									<c:if test="${inspectionTypeMessageCode != '' && finalCostInspection.signum() != 0}">
										<tr class="nobord" style="border-top: thin solid gray">
											<td colspan="2"> <spring:message code="${inspectionTypeMessageCode}" />
												(<spring:message code="jobcost.inspectionchargeappliesifoverallcostoftheinstrumentisrejected" />)
											</td>
										<td></td>
										<td class="center"></td>
										<td class="center"></td>
										<td class="center"></td>
										<td>
											<fmt:formatNumber value="${finalCostInspection}" type="currency"
											currencySymbol="${currency.currencyERSymbol}" /></td>
										</tr>
									</c:if>
								</c:if>
								<c:if test="${jcinotecount > 0}">
									<tr id="JOBCOSTINGITEMNOTE${jobCostingItemDto.id}" class="hid">
										<td colspan="9" class="nopadding">
											<t:showActiveNotes contact="${currentContact}" links="true"
													entity="${ items[i.index] }" privateOnlyNotes="false"
    												noteTypeId="${jobCostingItemDto.id}"
    												noteType="JOBCOSTINGITEMNOTE" /> 
										</td>
									</tr>
								</c:if>
							</tbody>
						</table>
					</c:forEach>
					<table class="default4"
						summary="<spring:message code='jobcost.thistabledisplaysalljobcostingitems'/>">
						<thead>
							<tr>
								<td id="expenseQuantityRow" colspan="8">
									Job Expense Items (${jobCostingExpenseItemsDto.size()})
								</td>
							</tr>
							<tr>
								<th class="item" scope="col"><spring:message code="item" /></th>
								<th class="model" scope="col"><spring:message code="model" /></th>
								<th class="expcomment" scope="col"><spring:message
										code="comment" /></th>
								<th class="plant" scope="col"><spring:message code="date" /></th>
								<th class="caltype" scope="col" class="center"><spring:message
										code="type" /></th>
								<th class="finalcost" scope="col" class="cost2pc"><spring:message
										code="jobcost.finalcost" /></th>
								<th class="del" scope="col" class="center"><spring:message
										code="delete" /></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="expenseItem"
								items="${jobCostingExpenseItemsDto}">
								<tr id="jcei${expenseItem.costingExpenseItemId}" class="expenseItemRow">
									<td class="item">${expenseItem.itemNo}</td>
									<td class="model"><c:if
											test="${not empty expenseItem.modelNameTranslations}">
											${expenseItem.modelNameTranslations}
										</c:if></td>
									<td class="expcomment">${expenseItem.comment}</td>
									<td class="plant"><fmt:formatDate dateStyle="medium" type="both" timeStyle="short"
											value="${expenseItem.date}" /></td>
									<td class="caltype">${expenseItem.serviceTypeShortnameTranslation}</td>
									<td class="finalcost"><fmt:formatNumber
											value="${expenseItem.totalPrice}" type="currency"
											currencySymbol="${job.currency.currencySymbol}"
											currencyCode="${job.currency.currencyCode}" /></td>
									<td class="del">
										<a href=""
										onclick="event.preventDefault(); deleteExpenseItem(${expenseItem.costingExpenseItemId});">
											<img src="img/icons/delete.png" width="16" height="16"
											alt="<spring:message code='jobcost.deletejobcostingexpenseitem'/>"
											title="<spring:message code='jobcost.deletejobcostingexpenseitem'/>" />
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!--  show any site costing notes  -->
					<c:if test="${command.jobCosting.type == 'SITE'}">
						<div>
							<h3>
								<spring:message code="jobcost.sitecostingnotes" />: (<a
									href="#"
									onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'editcosting-tab', false); $j('#siteCostingNoteTextArea').focus(); "
									class="mainlink"><spring:message code="edit" /></a>)</h3>
							<div>
								<c:choose>
									<c:when test="${empty costing.siteCostingNote}">
										<spring:message code="jobcost.nonotes" />.
									</c:when>
									<c:otherwise>
										${costing.siteCostingNote.replaceAll("\\n", "")}
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</c:if>
					
					<table class="default4"
						summary="This table displays the costing total value">
						<tbody>
							<tr class="nobord">											
								<td class="totalsum cost2pcTotal bold"><spring:message
										code="totalcost" />:</td>
								<td class="totalamount cost2pcTotal bold">
									<span id="jciTotalCost"><fmt:formatNumber
											value="${costing.totalCost}" type="currency"
											currencySymbol="${currency.currencyERSymbol}" /></span>
								
								</td>
								<td class="filler">&nbsp;</td>
							</tr>																				
						</tbody>
					</table>
					<c:if test="${showVAT}">
						<table class="default4"
							summary="<spring:message code='jobcost.tablecostingvat' />">
							<tbody>
								<tr class="nobord">											
									<td class="totalsum cost2pcTotal bold">(${costing.vatRate}%) <spring:message
											code="jobcost.vat" />:</td>
									<td class="totalamount cost2pcTotal bold">
										<span id="jciVatCost"><fmt:formatNumber
												value="${costing.vatValue}" type="currency"
												currencySymbol="${currency.currencyERSymbol}" /></span>
						
									</td>
									<td class="filler">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table class="default4"
							summary="<spring:message code='jobcost.tablecostingnettotal' />">
							<tbody>
								<tr class="nobord">											
									<td class="totalsum cost2pcTotal bold"><spring:message
											code="jobcost.grosstotal" />:</td>
									<td class="totalamount cost2pcTotal bold">
										<span id="jciFinalCost"><fmt:formatNumber
												value="${costing.finalCost}" type="currency"
												currencySymbol="${currency.currencyERSymbol}" /></span>
									</td>
									<td class="filler">&nbsp;</td>
								</tr>
							</tbody>
						</table>
					</c:if>
					<table class="default4"
						summary="<spring:message code='jobcost.tablecostingdiscount' />">
						<tbody>
							<c:if test="${discounts.discounts > 0}">
								<tr class="nobord">												
									<td class="totalsum cost2pcTotal bold"><spring:message
											code="jobcost.discountsapplied" /> (${discounts.discounts}):</td>
									<td class="totalamount cost2pcTotal bold">
										<fmt:formatNumber value="${discounts.discountValue}"
											type="currency" currencySymbol="${currency.currencyERSymbol}" />
								
									</td>
									<td class="filler">&nbsp;</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
			
			<!-- this section contains form elements for editing job costing -->
			<div id="editcosting-tab" class="hid">
				<form:form action="" method="post" modelAttribute="command">
					<fieldset>
						<legend>
							<spring:message code="jobcost.editcosting" />
						</legend>
						
						<ol>
							<li>
								<label><spring:message code="jobcost.jobcontact" />:</label>
								<form:select path="personid">
									<form:options items="${conlist}" itemValue="personid"
										itemLabel="name" />
								</form:select>
								<span class="attention"><form:errors
										path="personid" /></span>
							</li>
							<li>
								<label><spring:message code="jobcost.additionalcontacts" />:</label>
								<div class="float-left" id="additionalcontactlist">
									<c:choose>
										<c:when test="${costing.additionalContacts.size() > 0}">
											<c:forEach var="con" items="${costing.additionalContacts}"
												varStatus="Fcount">																
												<select name="additionalPersonids">
													<option value="">N/A</option>
													<c:forEach var="c" items="${conlist}">
														<option value="${c.personid}"
															${con.contact.personid == c.personid ? "selected" : ""}>${c.name}</option>
													</c:forEach>
												</select>
												<c:if test="${Fcount.last}">
													<a href="#"
														onclick=" event.preventDefault(); addAdditionalContacts(this); "
														class="mainlink"><spring:message
															code="jobcost.another" /></a>
												</c:if>
												<br />																
											</c:forEach>
										</c:when>
										<c:otherwise>
											<select name="additionalPersonids">
												<option value="" selected="selected">N/A</option>
												<c:forEach var="c" items="${conlist}">
													<option value="${c.personid}">${c.name}</option>
												</c:forEach>
											</select>
											<a href="#"
												onclick=" event.preventDefault(); addAdditionalContacts(this); "
												class="mainlink"><spring:message code="jobcost.another" /></a>
										</c:otherwise>
									</c:choose>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>	
							<li>
								<label><spring:message code="jobcost.costingstatus" />:</label>
								<form:select path="jobCostingTypeId">
									<form:options items="${jobcosttypelist}" itemValue="id"
										itemLabel="type" />
								</form:select>
								<span class="attention"><form:errors
										path="jobCostingTypeId" /></span>
							</li>
							<li>
								<label><spring:message code="clientref" />:</label>
								<form:input path="jobCosting.clientref" />
								<span class="attention"><form:errors
										path="jobCosting.clientref" /> </span>
							</li>
							<li>
								<label><spring:message code="jobcost.applyadiscount" />:</label>
								<div class="float-left">
									<form:input path="discountRate" />%
									<span class="attention"><form:errors
											path="discountRate" /></span>
									
									<div>
										<form:checkbox path="applyDiscountToCalibration"
											value="true" id="disccal"
											onclick="if(this.checked == true){$j('#discall').attr('checked', false);}" />
										<spring:message
											code="jobcost.applydiscounttoallitemcalcostsonly" />
									</div>
									
									<div>
										<form:checkbox path="applyDiscount" value="true"
											id="discall"
											onclick="if(this.checked == true){$j('#disccal').attr('checked', false);}" />
										<spring:message code="jobcost.applythisdiscounttoallcosts" />
									</div>
								</div>
								<div class="clear"></div>
							</li>
							
							<!--  make the total cost editable for site costing  -->
							<c:if test="${command.jobCosting.type == 'SITE'}">
								<li><strong><spring:message
											code="jobcost.sitecostingoptions" /></strong></li>
								<li>
									<label><spring:message code="totalcost" />:</label>
									${currency.currencyERSymbol}
									<form:input path="jobCosting.totalCost" />
									<span class="attention"><form:errors
											path="jobCosting.totalCost" /></span>
								</li>
								<li>
									<label><spring:message code="jobcost.sitecostingnotes" />:</label>
									<form:textarea id="siteCostingNoteTextArea"
										path="jobCosting.siteCostingNote" cols="80" rows="3" />
									<span class="attention"><form:errors
											path="jobCosting.siteCostingNote" /></span>
								</li>
							</c:if>
							
							<li>
								<label>&nbsp;</label>
								<input type="submit" name="update"
								value="<spring:message code="update" />" />
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
								
			<!-- this section displays all costing files created -->
			<div id="costingfiles-tab" class="hid">
				<files:showFilesForSC rootFiles="${scRootFiles}" id="${costing.id}"
					allowEmail="true" entity="${costing}" isEmailPlugin="false" ver=""
					identifier="${job.jobno}" deleteFiles="true" sc="${sc}"
					rootTitle="Files for Costing ${costing.qno} ${costing.ver}" />
			</div>

			<!-- this section displays all emails for the selected job -->
			<div id="email-tab" class="hid">
				<t:showEmails entity="${costing}" entityId="${costing.id}"
					entityName="Job Costing" sc="${sc}" />
			</div>

			<!-- this section displays any instructions for the costing -->
			<div id="instruction-tab" class="hid">
				<div class="infobox">
					<h5><spring:message code="viewjob.jobinstr"/> <a href="addinstruction.htm?jobid=${job.jobid}" class="mainlink-float"><spring:message code="jifaultreport.addinstr"/></a></h5>
					<!-- clears floats and restores page flow -->
					<div class="clear"></div>
					<cwms-instructions link-jobid="${job.jobid}" instruction-types="DISCOUNT,COSTING" show-mode="RELATED"></cwms-instructions>
				</div>
			</div>
			
			<!-- this section displays client response for the costing -->
			<div id="clientdecision-tab" class="hid">
				<form:form action="" method="post" modelAttribute="command">
				
					<fieldset>
						<legend>
							<spring:message code="jobcost.clientdecision" />
						</legend>
						<ol>
							<li>
								<label><spring:message code="jobcost.clientdecision.decision" />:</label>
								<form:select path="clientDecision">
									<form:option value="">&nbsp;</form:option>
									<form:options items="${clientApprovals}" itemLabel="localizedName" />
								</form:select>
								<span class="attention"><form:errors
										path="clientDecision" /></span>
							</li>
							<li>
								<label><spring:message code="jobcost.clientdecision.date" />:</label>
								<form:input type="datetime-local" path="clientApprovalOn"
										step="60" max="${cwms:isoDatetime(now)}"
										/>
								<span class="attention"><form:errors
										path="clientApprovalOn" /></span>
							</li>
						
								<li>
									<label><spring:message code="comment" />:</label>
									<form:textarea path="clientApprovalComment" cols="80" rows="3" />
									<span class="attention"><form:errors
											path="clientApprovalComment" /></span>
								</li>
							
							
							<li>
								<label>&nbsp;</label>
								<input type="hidden" name="setclientdecision" value="1"/>
								<input type="submit" name="update"
								value="<spring:message code="update" />" />
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
		</div>
		
		<!-- this section contains all Invoice notes -->
		<t:showTabbedNotes entity="${costing}"
			privateOnlyNotes="${privateOnlyNotes}" noteTypeId="${costing.id}"
			noteType="JOBCOSTINGNOTE" />
	</jsp:body>
</t:crocodileTemplate>