<%-- File name: /trescal/core/pricing/purchaseorder/editpurchaseorderitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="purchaseorder.createeditpurchaseorderitem" /></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
       <script type='text/javascript' src='script/trescal/core/pricing/purchaseorder/EditPurchaseOrderItem.js'></script>

		<script type='text/javascript'>
	
			var ledgers = new Array();
			<c:forEach var="ledgerItem" items="${ledgers}">
			ledgers.push('${ledgerItem}');
			</c:forEach>
	
		</script>
    </jsp:attribute>
    <jsp:body>
    <form:errors path="command.*">
  		<div class="warningBox1">
   			 <div class="warningBox2">
     		 <div class="warningBox3">
      		   <h5 class="center-0 attention">
        	   <spring:message code="error.save"/>
        	  </h5>
     		 </div>
  		 </div>
 	 </div>
	</form:errors>

<!-- main div which contains all create costing form elements and has nifty corners applied -->
<div class="infobox">
	
	<form:form action="" id="editpurchaseorderitemform" method="post" >
		
		<c:set var="currency" value="${order.currency}"/>
		
		<fieldset>
			
			<legend>
				<spring:message code="purchaseorder.createeditpurchaseorderitemforpurchaseorder" /> 
				<a href="<spring:url value='viewpurchaseorder.htm?id=${order.id}'/>"><c:out value=" ${order.pono} "/></a> 
			</legend>
			
			<ol>
				<li>
					<form:label path="businessSubdivId"><spring:message code="businesssubdivision" />:</form:label>											
					<form:select path="businessSubdivId" id="businessSubdivId" >
						<option value="0">N/A</option>
						<c:forEach var="subdiv" items="${businessSubdivs}">
							<option value="${subdiv.subdivid}" <c:if test="${command.businessSubdivId == subdiv.subdivid}"> selected="selected" </c:if>>
								${subdiv.subdivCode} - ${subdiv.subname} 
							</option>
						</c:forEach>
					</form:select>					
					<form:errors path="businessSubdivId" class="error"/>
				</li>
				<li>											
					<!-- this div creates a new preset comments plugin. -->		
					<label for="item.description"><spring:message code="description" />:</label>
					<div class="float-left width80">
						<form:textarea path="description" rows="4" class="width80 presetComments PURCHASE_ORDER_ITEM" value="${command.description}" />
						<form:errors path="description" class="error"/>
						<br />	
						<form:checkbox value="true" path="saveComments" id="saveComments" />
						<spring:message code="purchaseorder.savecurrentcommentforfutureuse" /> 
						<form:errors path="saveComments" class="attention"/>
					</div>
					<!-- clear floats and restore page flow -->
					<div class="clear"></div>
				</li>
				<li>
					<form:label path="costTypeId"><spring:message code="purchaseorder.costtype" />:</form:label>
					<form:select path="costTypeId" id="costTypeId" onchange=" event.preventDefault(); findNominal(this.value, $j('#jobItemId').val()); ">
						<option value="null">N/A</option>
						<c:forEach var="costType" items="${costTypeList}">
							<option value="${costType.typeid}" 
								<c:if test="${not empty command.costTypeId && (costType.typeid == command.costTypeId)}" > selected="selected" &nbsp;</c:if>>
								 <spring:message code="${costType.messageCode}" />&nbsp;
							 </option>
						</c:forEach>
					</form:select>
				</li>
				<li>
					<form:label path="deliveryDate"><spring:message code="purchaseorder.requireddate" />:</form:label>
					<form:input type="date" path="deliveryDate" id="deliveryDate"/>
					<form:errors path="deliveryDate" class="attention"/>
				</li>
				
				<c:if test="${not empty jobitems}">
					<li>
						<form:label path="jobItemId"><spring:message code="purchaseorder.linktojobitem" />:</form:label>
						<form:select path="jobItemId" id="jobItemId" onchange=" event.preventDefault(); findNominal($j('#costTypeId').val(), this.value); ">
							<option value="0">N/A</option>
							<c:forEach var="ji" items="${jobitems}">
								<option value="${ji.jobItemId}" <c:if test="${command.jobItemId == ji.jobItemId}"> selected="selected" </c:if> >
									<spring:message code="item" /><c:out value=" ${ji.itemNo} "/>
									<instmodel:showInstrumentModelName instrument="${ji.inst}" caltypeid="${ji.calType.calTypeId}" relativePathToImages=""/>
								</option>
							</c:forEach>
						</form:select>
					</li>
				</c:if>
				<li>
					<form:label path="nominalId"><spring:message code="purchaseorder.nominal" />:</form:label>
					<div class="float-left padtop">
						<div id="suggestednominal" class="marg-bot-small">
							<spring:message code="purchaseorder.suggestednominal" /> : 
							<c:choose>
								<c:when test="${not empty suggestednominal}">
									${suggestednominal.code}: 
									<t:showTranslationOrDefault translations="${suggestednominal.titleTranslations}" defaultLocale="${defaultlocale}"/> &nbsp;<a href="#" class="mainlink" onclick="$j('#${status.expression}').val(${suggestednominal.id}); return false;">
									<spring:message code="purchaseorder.switch" /></a>
								</c:when>
								<c:otherwise> 
									<spring:message code="purchaseorder.nomatchesfound" />
								</c:otherwise>
							</c:choose>	
						</div>
						<form:select path="nominalId" id="nominalId">
							<option value="">N/A / <spring:message code="purchaseorder.unknown" /></option>
							<c:forEach var="nominal" items="${nominalList}">
								<option value="${nominal.id}" <c:if test="${not empty command.nominalId}"></c:if>
															<c:if test="${nominal.id == command.nominalId}">  selected="selected"  </c:if>>
														    ${nominal.code} - <t:showTranslationOrDefault translations="${nominal.titleTranslations}" defaultLocale="${defaultlocale}"/></option>
							</c:forEach>
						</form:select>
						<form:errors path="nominalId" class="attention"/>
					</div>
					<!-- clear floats and restore page flow -->										
					<div class="clear"></div>
				</li>
				
				<c:if test="${not empty command.newPoItem && item.order.status.issued == true}">
					
					<!-- 
					#*
					<li>
						<label for="$!{status.expression}">Recevied:</label>
						#springBind("command.received")
						<input type="radio" name="$status.expression" value="true" #if($command.received == true) checked="checked" #end/> Received
						<input type="radio" name="$status.expression" value="false" #if($command.received == false) checked="checked" #end/> Not-Received
						${status.errorMessage}
					</li>
					*#
					 -->											 
					 
					<li>
						<form:label path="progress.receiptStatus"><spring:message code="purchaseorder.receiptstatus" />:</form:label>
						<div <c:if test="${order.accountsStatus == 'S'}"> class="hid" </c:if>>
							<form:select path="progress.receiptStatus" onchange=" updateOrderItemCompleteStatus(this.value, ${accountsAuthenticated}); return false; " >
								<c:forEach var="rs" items="${receiptstatuslist}">
									<option value="${rs}" <c:if test="${rs == command.progress.receiptStatus}"> selected="selected" </c:if>>${rs}</option>
								</c:forEach>
							</form:select>
						</div>
						<c:if test="${order.accountsStatus == 'S'}">
							<span><spring:message code="purchaseorder.editingrestrictedpurchaseorderhasbeenbrokered" /></span>
						</c:if>
						<form:errors path="progress.receiptStatus" class="attention"/>
					</li>											
					<li>
						<c:set var="goodsDisabled" value=""/>
						<c:if test="${command.progress.receiptStatus != 'COMPLETE'}">
							<c:set var="goodsDisabled" value="disabled"/> 
						</c:if>
					
						<form:label path="progress.goodsApproved"><spring:message code="purchaseorder.goodsapproved" />:</form:label>
						<div <c:if test="${order.accountsStatus == 'S'}"> class="hid" </c:if>>
							<form:radiobutton disabled="${goodsDisabled}" path="progress.goodsApproved" value="false" /> <spring:message code="purchaseorder.awaitingapproval" />
							<form:radiobutton disabled="${goodsDisabled}" path="progress.goodsApproved" value="true" /> <spring:message code="purchaseorder.goodsapproved" />
							<c:if test="${command.progress.receiptStatus != 'COMPLETE'}">
								- <spring:message code="purchaseorder.settingdisableduntilgoodsreceived" />
							</c:if>												
						</div>
						<c:if test="${order.accountsStatus == 'S'}">
							<span><spring:message code="purchaseorder.editingrestrictedpurchaseorderhasbeenbrokered" /></span>
						</c:if>
						<form:errors path="progress.goodsApproved" class="attention"/>
					</li>										 
					<li>
						<c:set var="disabled" value=""/>
						<c:if test="${accountsAuthenticated == false || command.progress.receiptStatus != 'COMPLETE'}">
							<c:set var="disabled" value="disabled"/> 
						</c:if>
					
						<form:label path="progress.accountsApproved"><spring:message code="purchaseorder.accountsapproval" />:</form:label>
						<div <c:if test="${order.accountsStatus == 'S'}"> class="hid" </c:if>>
							<form:radiobutton disabled="${disabled}" path="progress.accountsApproved" value="false" /> <spring:message code="purchaseorder.awaitingaccountsapproval" />
							<form:radiobutton disabled="${disabled}" path="progress.accountsApproved" value="true" /> <spring:message code="purchaseorder.accountsapproved" />
							<c:choose>
								<c:when test="${command.progress.receiptStatus != 'COMPLETE'}">
									- <spring:message code="purchaseorder.settingdisableduntilgoodsreceived" />
								</c:when>
								<c:when test="${accountsAuthenticated == false}">
									- <spring:message code="purchaseorder.youarenotaccreditedtochangethissetting" />
								</c:when>
							</c:choose>												
						</div>
						<c:if test="${order.accountsStatus == 'S'}">
							<span><spring:message code="purchaseorder.editingrestrictedpurchaseorderhasbeenbrokered" /></span>
						</c:if>
						<form:errors path="progress.accountsApproved" class="attention"/>
					</li>										 
					<li>
						<form:label path="progress.cancelled"><spring:message code="purchaseorder.active" />:</form:label>
						<div <c:if test="${order.accountsStatus == 'S'}"> class="hid" </c:if>>
							<form:radiobutton  path="progress.cancelled" value="false" /> <spring:message code="purchaseorder.active" />
							<form:radiobutton  path="progress.cancelled" value="true"/> <spring:message code="purchaseorder.cancellednolongerrequired" />
						</div>
						<c:if test="${order.accountsStatus == 'S'}">
							<span><spring:message code="purchaseorder.editingrestrictedpurchaseorderhasbeenbrokered" /></span>
						</c:if>
						<form:errors path="progress.cancelled" class="attention"/>
					</li>
				</c:if>
				
				<li>
					<form:label path="quantity"><spring:message code="purchaseorder.quantity" />:</form:label>
					<form:input path="quantity" tabindex="" />
					<form:errors path="quantity" class="attention"/>
				</li>
				<li>
					<form:label path="totalCost"><spring:message code="purchaseorder.peritemcost" />:</form:label>
					<form:input path="totalCost" tabindex="" />
					<form:errors path="totalCost" class="attention"/> (${currency.currencyERSymbol})
				</li>
				<li>
					<form:label path="generalDiscountRate"><spring:message code="purchaseorder.discountrate" />(%):</form:label>
					<form:input type="text" path="generalDiscountRate"/>
					<form:errors path="generalDiscountRate" class="attention"/>
				</li>
				<c:if test="${not empty command.newPoItem}">					
					<li>
						<label for="finalcost"><spring:message code="purchaseorder.finalcost" />:</label>
						<span class="bold">
							${currency.currencyERSymbol}${item.finalCost}
						</span>
					</li>
				</c:if>
		 		<li>
					<label>&nbsp;</label>
					<input type="submit" name="submit" value="<spring:message code='save' />" />
				</li>	
			</ol>																	
			
		</fieldset>
		
	</form:form>

</div>
<!-- end of main job div -->	
    </jsp:body>
</t:crocodileTemplate>