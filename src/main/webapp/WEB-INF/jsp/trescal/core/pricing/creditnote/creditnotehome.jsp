<%-- File name: /trescal/core/pricing/creditnote/creditnotehome.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<style>
   .widthLabel{
    width : 260px;
   }
</style>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="creditnote.searchcreditnotes"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type="text/javascript" src="script/trescal/core/pricing/creditnote/CreditNoteHome.js"></script>
		<script type='text/javascript'>
			/** 
			 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
			 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
			 */
		 	var menuElements = [];
			<c:forEach var="ii" items="${interestingItems.keySet()}" varStatus="itemStatus">
				menuElements.push({ anchor: 'menu-${itemStatus.index}-link', block: 'menu-${itemStatus.index}-tab' });
			</c:forEach>
		</script>
    </jsp:attribute>
    <jsp:body>
		<div class="infobox">
													
			<form:form action="" id="searchcreditnoteform" method="post" modelAttribute="creditnotehomeform">
						
				<fieldset style=" margin: 0px auto; width: 80%; ">
					
					<legend><spring:message code="creditnote.searchcreditnotes"/></legend>
					
					<ol>
						<li>
							<label for="orgid"><spring:message code="businesscompany"/>:</label>
							${allocatedCompany.value}
						</li>
						
						<li>
							<label for="coid"><spring:message code="company"/>:</label>
							<div class="float-left extendPluginInput">
								<div class="compSearchJQPlugin">
									<input type="hidden" name="field" value="compId" />
									<input type="hidden" name="compCoroles" value="client,business" />
									<input type="hidden" name="tabIndex" value="1" />
									<!-- company results listed here -->
								</div>
							</div>									
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
		
							<label for="creditNoteNo"><spring:message code="creditnote.creditnoteno"/>:</label>
							<form:input path="creditNoteNo" tabindex="4" class="widthLabel"/> 
						</li>
						<li>
		
							<label for="invNo"><spring:message code="creditnote.invoiceno"/>:</label>
							<form:input path="invNo" tabindex="5" class="widthLabel"/>
						</li>
						<li>
						
							<label for="date"><spring:message code="date"/>:</label>
							<form:input path="date" tabindex="6"  type="date" class="widthLabel"/>
						<li>
		
							<label for="dateFrom"><spring:message code="creditnote.daterange"/>:</label>
		                    <form:input path="dateFrom" tabindex="7" type="date" />
		                    
							<spring:message code= "to" />&nbsp;
							 <form:input path="dateTo" tabindex="8" type="date" />
						</li>
						<li>
						
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code="search"/>" tabindex="9" />
						</li>
									
					</ol>
				</fieldset>
				
			</form:form>
							
		</div>
		<!-- end of infobox -->
								
		<!-- infobox div containing invoice search form fields -->
		<div class="infobox">
		
			<div id="tabmenu">
				
				<!-- solves bug in IE where bar under subnav disappears -->	
				<div>&nbsp;</div>
				
				<ul class="subnavtab">
					<c:set var="count" value="0"/>
					
					<c:forEach var="ii" items="${interestingItems.keySet()}" varStatus="itemStatus">
						<li>
						
							<a href="" id="menu-${itemStatus.index}-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, 'menu-${itemStatus.index}-tab', false); " 
							<c:if test="${count == 0}"> class="selected" 
							</c:if>>
							${ii} 
							(<span class="size-${itemStatus.index}">${interestingItems.get(ii).size()}</span>)</a>
						</li>
						<c:set var="count" value="${count + 1}"/>
					</c:forEach>
				</ul>
			
				<div class="tab-box">
					
					<c:set var="count" value="0"/>
					<c:forEach var="ii" items="${interestingItems.keySet()}" varStatus="itemStatus"> 
						<!-- div displays a purchase order heading -->
						<div id="menu-${itemStatus.index}-tab" 
						<c:if test="${count > 0}">  class="hid" 
						</c:if>>
							
							<table class="default2" summary="<spring:message code="creditnote.tablecreditnotesspecificheading"/>">										
								<thead>
									<tr>
										<td colspan="4">
		
											<span><c:out value="${ii} "/><spring:message code="creditnote.creditnotes"/></span> (<span class="size-${itemStatus.index}">${interestingItems.get(ii).size()}</span>)
		
										</td>
									</tr>
									<tr>
										<th class="poNum"><spring:message code="creditnote.creditnoteno"/></th>
										<th class="poComp"><spring:message code="company"/></th>
										<th class="poCont"><spring:message code="invoice"/></th>
										<th class="poRegDate"><spring:message code="regdate"/></th>														
									</tr>
								</thead>												
								<tfoot>
									<tr>
										 <td colspan="4">&nbsp;</td>
									</tr>
								</tfoot>												
								<tbody>
								<c:choose>
		                            <c:when test="${interestingItems.get(ii).size() < 1}">
									<tr class="odd">
											<td colspan="6" class="center bold"><spring:message code="creditnote.thereareno"/><c:out value=" ${ii} " /><spring:message code="creditnote.creditnotes"/></td>
									</tr>
									</c:when>
									<c:otherwise>
										<c:set var="rowcount" value="1"/>
										<c:forEach var="item" items="${interestingItems.get(ii)}">
											<tr 
											<c:choose>
			                                      <c:when test="${rowcount % 2 == 0}"> class="even" </c:when>
											   <c:otherwise> class="odd" </c:otherwise>
											</c:choose>>
												<td><a href="viewcreditnote.htm?id=${item.id}" class="mainlink">${item.creditNoteNo}</a></td>
												<td>${item.invoice.comp.coname}</td>
												<td>${item.invoice.invno}</td>
												<td><fmt:formatDate type="date" dateStyle="SHORT" value="${item.regdate}"/></td>
											</tr>
											<c:set var="rowcount" value="${rowcount + 1}"/>
										</c:forEach>
									</c:otherwise>
								</c:choose>
								</tbody>
							</table>
							
						</div>
						<!-- end of div to display purchase order heading -->
						
						<c:set var="count" value="${count + 1}"/>
					</c:forEach>
				
				</div>
				<!-- end of div to hold all tabbed submenu content -->
					
			</div>
			<!-- end of div to hold tabbed submenu -->
			<!-- this div clears floats and restores page flow -->
			<div class="clear"></div>
		</div>
		<!-- end of infobox -->
    </jsp:body>
</t:crocodileTemplate>