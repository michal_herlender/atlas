<%-- File name: /trescal/core/pricing/invoice/invoicehome.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="invoice.searchinvoice" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="text/javascript" src="script/trescal/core/pricing/invoice/InvoiceHome.js"></script>
		<script type='text/javascript'>
			/** 
			 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
			 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
			 */
			var menuElements = [];
			// get a list of headings
			<c:forEach var="wrapper" items="${interestingItems}">
				menuElements.push({ anchor: '${wrapper.id}-link', block: '${wrapper.id}-tab' });
			</c:forEach>
		</script>
	</jsp:attribute>
	<jsp:body>
		<!-- infobox div containing invoice search form fields -->
		<div class="infobox">
			<div class="float-right">
				<cwms:securedLink permission="JOB_ITEM_INVOICE_PREPARE" collapse="True" classAttr="mainlink" parameter="?companyWide=${companyWide}">
					<spring:message code="invoice.preparejobiteminvoice"/>
				</cwms:securedLink>
				|
				<cwms:securedLink permission="INVOICE_MONTHLY_PREPARE" collapse="True" classAttr="mainlink">
					<spring:message code="invoice.preparecustominvoice"/>
				</cwms:securedLink>
				|
				<cwms:securedLink permission="INVOICE_CREATE_INTERNAL" collapse="True" classAttr="mainlink">
					<spring:message code="invoice.createinternalinvoices"/>
				</cwms:securedLink>				
				|
				<cwms:securedLink permission="INVOICE_AWAITING_INVOICE_SUMMARY" collapse="True" classAttr="mainlink" parameter="?companyWide=${companyWide}">
					<spring:message code="invoice.awaitinginvoicesummary"/>
				</cwms:securedLink>
				|
				<cwms:securedLink permission="COMPONENT_DELETED" collapse="True" classAttr="mainlink">
					<spring:message code="invoice.searchdeletedinvoices"/>
				</cwms:securedLink>				
			</div>
			<!-- clear float and restore page flow -->
			<div class="clear-height"></div>
													
			<form:form action="" id="searchinvoiceform" method="post" modelAttribute="invoicehomeform">
				<fieldset>
					<legend><spring:message code="invoice.searchinvoicelabel" /></legend>
					<ol>
						<li>
							<label for="orgid"><spring:message code="businesscompany" />:</label> 
							<span>${allocatedCompany.value}</span>
						</li>
						<li>
							<label for="subdivId"><spring:message code="exchangeformat.bussubdivlabel" />:</label>
							<form:select path="subdivId"  style=" width: 267px;">
								<option value="0">-- <spring:message code="certmanagement.selall"/> --</option>
								<c:forEach var="subdiv" items="${activSubdivs}">
									<option value="${subdiv.key}" ${(subdiv.key == defaultSubdivId )? 'selected=true':''}> ${subdiv.value}</option>
								</c:forEach>
							</form:select>
						</li>
						<li>
							<label for="coid"><spring:message code="clientcompany" />:</label>
								<input type="checkbox" onclick="includeDeactivatedCompanies(this)"/> 
								<spring:message code="invoice.includedeactivatedcompanies" /> <br>
								<div class="float-left extendPluginInput">
									<div class="compSearchJQPlugin">
										<input type="hidden" name="field" value="coid" />
										<input type="hidden" name="compCoroles" value="client,business" />
										<input type="hidden" name="deactivated" value="" />
										<input type="hidden" name="tabIndex" value="1" />
										<!-- company results listed here -->
									</div>
								</div>			
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label for="jobno"><spring:message code="jobno" />:</label>
							<form:input path="jobno" tabindex="4" size="39"/>
						</li>
						<li>
							<label for="pono"><spring:message code="invoice.invoicenumber" />:</label>
							<form:input path="invno" tabindex="5" size="39"/>
						</li>
						<li>
							<label for="clientpo"><spring:message code="clientpo" />:</label>
							<form:input path="clientPO" tabindex="5" size="39"/>
						</li>
						<li>
							<label for="date"><spring:message code="invoice.invoicedate" />:</label>
							<form:input path="date" type="date" tabindex="6" style="width: 261px;" />
						</li>
						<li>
							<label for="issuedate"><spring:message code="invoice.invoiceissuedate" />:</label>
							<form:input path="issuedate" type="date" tabindex="6" style="width: 261px;"/>
						</li>
						<li>
							<label for="dateFrom"><spring:message code="invoice.daterange" />:</label>
							<form:input path="dateFrom" type="date" tabindex="7" style="width: 118px;" />
							<spring:message code="to" />&nbsp;
							<form:input path="dateTo" type="date" tabindex="8"  style="width: 118px;"/>
						</li>
						<li>
								<form:label path="typeid"><spring:message code="type" /></form:label>
								<form:select path="typeid" style=" width: 268px;">
									<option value=""><spring:message code="all" /></option>
									<c:forEach var="type" items="${invtypes}">
										<option value="${type.id}">${type.name}</option>
									</c:forEach>
								</form:select>
						</li>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code='search' />" tabindex="9" />
						</li>				
					</ol>
				</fieldset>
			</form:form>
		</div>
								
		<div class="infobox">
			<div class="align-left">
				<span class="bold">
					<spring:message code="invoices" />
					<c:out value=" - " />
					<c:choose>
						<c:when test="${companyWide}">
							<spring:message code="businesscompany" />
							<c:out value=" - ${allocatedCompany.value} " />
							<a href="invoicehome.htm?companyWide=false" class="mainlink">
								(<spring:message code="purchaseorder.switch" />)
							</a>
						</c:when>
						<c:otherwise>
							<spring:message code="businesssubdivision" />
							<c:out value=" - ${allocatedSubdiv.value} " />
							<a href="invoicehome.htm?companyWide=true" class="mainlink">
								(<spring:message code="purchaseorder.switch" />)
							</a>
						</c:otherwise>
					</c:choose>
					|
					<cwms:securedLink permission="INVOICE_AWAITING_INVOICE_SUMMARY" collapse="True" classAttr="mainlink" parameter="?companyWide=${companyWide}">
						<spring:message code="invoice.awaitinginvoicesummary"/>
					</cwms:securedLink>
				</span>
			<div id="tabmenu">

				<!-- solves bug in IE where bar under subnav dissappears -->	
				<div>&nbsp;</div>
				
				<ul class="subnavtab">
					<!-- get a list of headings -->
					<c:forEach var="wrapper" items="${interestingItems}" varStatus="loop">
						<li>
							<a href="#" id="${wrapper.id}-link" onclick=" event.preventDefault(); switchMenuFocus(menuElements, '${wrapper.id}-tab', false); " <c:if test="${loop.first}"> class="selected"</c:if>>
								${wrapper.name} (<span class="${wrapper.id}Size">${wrapper.quantity}</span>)
							</a>
						</li>
					</c:forEach>
				</ul>
			
				<div class="tab-box">
					<!-- now display a list of items for each key -->
					<c:forEach var="wrapper" items="${interestingItems}" varStatus="loop">
						<!-- div displays each specific wrapper heading -->
						<div id="${wrapper.id}-tab" <c:if test="${!loop.first}"> class="hid" </c:if>>
				
							<!-- Job Item DTO (Projection) formatting only expected -->
							<c:choose>
								<%--  New invoice projection display --%>
								<c:when test="${wrapper.contentsInvoiceProjection}">
									<table id="${wrapper.id}-table" class="default2 tablesorter" >
										<thead>
											<tr>
													<td colspan="4">
														${wrapper.description} (<span class="${wrapper.id}Size">${wrapper.quantity}</span>)
													</td>
											</tr>
											<tr>
												<th class="poNum"><spring:message code="invoice.invoicenumber" /></th>
												<th class="poComp"><spring:message code="company" /></th>
												<th class="poCont"><spring:message code="invoice.jobs" /></th>
												<th class="poRegDate"><spring:message code="invoice.invoicedate" /></th>														
											</tr>
										</thead>												
										<tfoot>
											<tr>
												<td colspan="4">&nbsp;</td>
											</tr>
										</tfoot>												
										<tbody>
											<c:choose>
												<c:when test="${empty wrapper.contents}">
													<tr class="odd">
														<td colspan="6" class="center bold">
														<spring:message code="viewcapabilities.noresults" />&nbsp;
														 </td>
													</tr>  
												</c:when>
												<c:otherwise>
													<c:forEach var="item" items="${wrapper.contents}" varStatus="loop">
														<c:set var="lineclass" value="odd" />
														<c:if test="${loop.index % 2 == 0}">
															<c:set var="lineclass" value="even" />
														</c:if>
														<tr class="${lineclass}">
															<td><a href="viewinvoice.htm?id=${item.id}" class="mainlink">${item.invoiceNumber}</a></td>
															<td>${item.company.coname}</td>
															<td>
																<c:forEach var="jobLink" items="${item.invoiceJobLinks}" varStatus="linkStatus">
																	<c:if test="${not linkStatus.first}">
																		<c:out value=", " />
																	</c:if>
																	<c:out value="${jobLink.jobno}" />
																</c:forEach>
															</td>
															<td><fmt:formatDate value="${item.invoiceDate}" type="date" dateStyle="SHORT"/></td>
														</tr>
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
								</c:when>
							</c:choose>
						</div>
					</c:forEach>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</jsp:body>
</t:crocodileTemplate>