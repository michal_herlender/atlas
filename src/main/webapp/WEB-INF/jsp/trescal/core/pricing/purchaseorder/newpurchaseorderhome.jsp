<%-- File name: /trescal/core/pricing/purchaseorder/purchaseorderhome.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<style>
   .widthLabel{
    width : 260px;
   }
</style>
<t:crocodileTemplate>

	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="purchaseorder.searchpurchaseorders"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/pricing/purchaseorder/PurchaseOrderHome.js'></script>
		<script type='text/javascript'>
			/** 
			 * this variable holds an array of id objects, one for the navigation link and one for the content area the link refers to.
			 * this array is passed to the Menu.js file to display the corresponding content area of the link clicked.
			 */
			var menuElements = [];
			<%-- get a list of headings --%> 
			<c:forEach var="wrapper" items="${interestingItems}">
				menuElements.push({ anchor: '${wrapper.id}-link', block: '${wrapper.id}-tab' });
			</c:forEach>
		</script>
	</jsp:attribute>
	<jsp:body>
		
		<t:showErrors showFieldErrors="true" path="command.*"/>
		
		<!-- infobox div containing purchase order search form fields -->
		<div class="infobox">
		
			<div class="float-right">
				<cwms:securedLink permission="PURCHASE_ORDER_CREATE" collapse="True" classAttr="mainlink">
					<spring:message code="purchaseorder.createpurchaseorder" />				
				</cwms:securedLink>
				|
				<cwms:securedJsLink permission="PURCHASE_ORDER_ISSUE" collapse="True" classAttr="mainlink" onClick=" quickPOContent(); return false;">
					<spring:message code="purchaseorder.issuequickpurchaseorder" />
				</cwms:securedJsLink>
				<a href="#" class="mainlink" onclick=" " title="Issue quick purchase order">
				</a>
				|
				<cwms:securedLink permission="COMPONENT_DELETED" collapse="True" classAttr="mainlink">
					<spring:message code="purchaseorder.searchdeletedpurchaseorders" />				
				</cwms:securedLink>
			</div>
			<!-- clear float and restore page flow -->
			<div class="clear-0"></div>
			
			<form:form action="" method="post" >
				<fieldset>
					<legend>
						<spring:message code="purchaseorder.searchpurchaseorders" />
					</legend>
					
					<ol>
						<li>
							<label><spring:message code="businesscompany" />:</label>
							<c:out value="${allocatedCompany.value}" />
						</li>
						<li>
							<label for="coid"><spring:message code="company" />:</label>											
							<div class="float-left extendPluginInput">
								<spring:bind path="coid">
									<div class="compSearchJQPlugin">
										<input type="hidden" name="field" value="coid" />
										<input type="hidden" name="compCoroles" value="supplier,utility,business" />
										<input type="hidden" name="tabIndex" value="1" />
										<!-- company results listed here -->
									</div>
								</spring:bind>
							</div>											
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label for="personid"><spring:message code="contact" />:</label>
							<spring:bind path="personid">
								<div id="contSearchPlugin">
									<input type="hidden" id="contCoroles" value="supplier,utility,business" />
									<input type="hidden" id="contindex" value="2" />
									<!-- contact results listed here -->
								</div>
							</spring:bind>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label for="jobno"><spring:message code="jobno" />:</label>
							<form:input path="jobno" tabindex="4" class="widthLabel"/>
						</li>
						<li>
							<label for="pono"><spring:message code="purchaseorder.ponumber" />:</label>
							<form:input path="pono" tabindex="5" class="widthLabel" />
						</li>
						<li>
							<label for="date"><spring:message code="date" />:</label>
							<form:input path="date" type="date" tabindex="6" class="widthLabel" />
						</li>
						<li>
							<label for="dateFrom"><spring:message code="purchaseorder.daterange" />:</label>
							<form:input path="dateFrom" type="date" tabindex="7"/>
							<spring:message code="to" />&nbsp;
							<form:input path="dateTo" type="date" tabindex="8"/>
						</li>
						<li>
							<label>&nbsp;</label>
							<spring:message var="searchText" code="search" />
							<input type="submit" name="submit" value="${searchText}" tabindex="9" />
						</li>				
					</ol>					
				
				</fieldset>
			</form:form>
					
		</div>
		<!-- end of purchase order search form fields -->
		
		<div class="infobox" id="POHomeStatusHeadings">
			<div id="tabmenu">
				<ul class="subnavtab">
					<!-- get a list of headings -->
					<c:forEach var="wrapper" items="${interestingItems}" varStatus="loopTagStatus">
						<c:choose>
							<c:when test="${loopTagStatus.first}">
								<c:set var="selectedClass" value="class='selected'" />
							</c:when>
							<c:otherwise>
								<c:set var="selectedClass" value="" />
							</c:otherwise>
						</c:choose>
						<li>
							<a href="#" id="${wrapper.id}-link" onclick="event.preventDefault(); switchMenuFocus(menuElements, '${wrapper.id}-tab', false); return false; " ${selectedClass}>
								<c:out value="${wrapper.name}" />	 (<span class="${wrapper.id}Size"><c:out value="${wrapper.dtos.size()}" /></span>)
							</a>
						</li>
						</c:forEach>
				</ul>
				<div class="tab-box">
					<c:set var="count" value="0"/>
					<!-- now display a list of items for each key --> 
					<c:forEach var="wrapper" items="${interestingItems}" varStatus="wrapperLoopStatus">
							<!-- div displays a purchase order heading -->
							<c:choose>
								<c:when test="${wrapper.contentsQuickPurchaseOrder}">
									<c:choose>
										<c:when test="${wrapperLoopStatus.first}">
											<c:set var="divClass" value="vis" />
										</c:when>
										<c:otherwise>
											<c:set var="divClass" value="hid" />
										</c:otherwise>
									</c:choose>
									<div id="${wrapper.id}-tab" class="${divClass}">
										<table id="" class="default2 QPOtable" summary="This table all purchase orders under a specific purchase order heading">										
											<thead>
												<tr>
													<td colspan="7">
														<c:out value="${wrapper.description}" /> (<span class="${wrapper.id}Size"><c:out value="${wrapper.dtos.size()}" /></span>)
													</td>
												</tr>
												<tr>
													<th class="qpoNum"><spring:message code="purchaseorder.ponumber" /></th>
													<th class="qpoDesc"><spring:message code="description" /></th>
													<th class="qpoCreateBy"><spring:message code="createdby" /></th>
													<th class="qpoCreateOn"><spring:message code="createdon" /></th>
													<th class="qpoJobno"><spring:message code="purchaseorder.joblink" /></th>
													<th class="qpoApprove"><spring:message code="purchaseorder.approve" /></th>
													<th class="qpoDel"><spring:message code="delete" /></th>												
												</tr>
											</thead>												
											<tfoot>
												<tr>
													<td colspan="7">&nbsp;</td>
												</tr>
											</tfoot>												
											<tbody>
												<c:choose>
													<c:when test="${empty wrapper.dtos}">
														<tr class="odd">
															<td colspan="7" class="center bold">
																<spring:message code="purchaseorder.therearenoquickpurchaseorders" />
															</td>
														</tr>
													</c:when>
													<c:otherwise>
														<c:forEach var="item" items="${wrapper.dtos}" varStatus="loopTagStatus">
															<c:choose>
																<c:when test="${loopTagStatus.index % 2 == 0}">
																	<c:set var="rowClass" value="even" />
																</c:when>
																<c:otherwise>
																	<c:set var="rowClass" value="odd" />
																</c:otherwise>
															</c:choose>
															
															<tr id="qpo${item.id}" class="${rowClass}" >
																<td class="qpoNum"><c:out value="${item.pono}" /></td>
																<td class="qpoDesc"><c:out value="${item.desc}" /></td>
																<td class="qpoCreateBy"><c:out value="${item.createdBy}" /></td>
																<td class="qpoCreateOn"><c:out value="${item.createdOn}" /></td>
																<td class="qpoJobno"><t:fill field="${item.jobNo}" /></td>
																<td class="qpoApprove">
																	<c:url var="createUrl" value="/createpurchaseorder.htm?quickpoid=${item.id}" />
																	<a href="${createUrl}" class="mainlink">
																		<spring:message code="purchaseorder.approve" />
																	</a>
																</td>
																<td class="qpoDel">
																	<a href="#" onclick=" event.preventDefault(); showDeleteQPOContent(${item.id}); ">
																		<spring:message var="deleteText" code="purchaseorder.deletequickpurchaseorder" />
																		<img src="img/icons/delete.png" width="16" height="16" alt="${deleteText}" title="${deleteText}" />		
																	</a>
																</td>
															</tr>
														</c:forEach>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
								</c:when>
								<c:otherwise>
									<c:if test="${count > 0}">
										<c:set var="classText" value="class='hid'"/>
									</c:if>
									<div id="${wrapper.id}-tab" ${classText}>
										<table id="" class="default2" summary="This table all purchase orders under a specific purchase order heading">										
											<thead>
												<tr>
													<td colspan="4">
														<spring:message code="purchaseorder.purchaseorders" /> - <c:out value="${wrapper.description}" />
														(<span class="${wrapper.id}Size"><c:out value="${wrapper.dtos.size()}" /></span>)
													</td>
												</tr>
									
												<tr>
													<th class="poNum"><spring:message code="purchaseorder.ponumber" /></th>
													<th class="poComp"><spring:message code="company" /></th>
													<th class="poCont"><spring:message code="contact" /></th>
													<th class="poRegDate"><spring:message code="purchaseorder.regdate" /></th>														
												</tr>
											</thead>												
											<tfoot>
												<tr>
													<td colspan="4">&nbsp;</td>
												</tr>
											</tfoot>												
											<tbody>
												<c:choose>
													<c:when test="${empty wrapper.dtos}">
														<tr class="odd">
															<td colspan="6" class="center bold"><spring:message code="purchaseorder.therearenopurchaseorders" /></td>
														</tr>
													</c:when>
													<c:otherwise>
														<c:forEach var="item" items="${wrapper.dtos}" varStatus="loopTagStatus">
															<c:choose>
																<c:when test="${posUpperLimit.contains(item.id)}" >
																	<c:set var="isGreen" value="style=\"background-color: #99ff99;\""></c:set>
																</c:when>
																<c:otherwise>
																	<c:set var="isGreen" value=""></c:set>
																</c:otherwise>
															</c:choose>
															<c:choose>
																<c:when test="${loopTagStatus.index % 2 == 0}" >
																	<c:set var="rowClass" value="even" />
																</c:when>
																<c:otherwise>
																	<c:set var="rowClass" value="odd" />
																</c:otherwise>
															</c:choose>
															<tr class="${rowClass}"  ${isGreen} >
																<c:url var="purchaseOrderUrl" value="/viewpurchaseorder.htm?id=${item.id}" />
																<td><a href="${purchaseOrderUrl}" class="mainlink"><c:out value="${item.pono}" /></a></td>
																<td><c:out value="${item.compName}" /></td>
																<td><c:out value="${item.contactName}" /></td>
																<td><fmt:formatDate value="${item.regdate}" type="date" dateStyle="SHORT" /></td>
															</tr>
														</c:forEach>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
									<!-- end of div to display purchase order heading -->
								</c:otherwise>
							</c:choose>
							<c:set var="count" value="${count + 1}"/>  										
					</c:forEach>
						
				</div>
			</div>
			<div class="clear"></div>	
		</div>
	
		
	</jsp:body>

</t:crocodileTemplate>