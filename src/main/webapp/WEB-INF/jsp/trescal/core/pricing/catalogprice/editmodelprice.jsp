<%-- File name: /trescal/core/pricing/catalogprice/editmodelprice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="instmodel" tagdir="/WEB-INF/tags/instmodel"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<cwms:showmodel instrumentmodel="${price.instrumentModel}"/>
		</span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="price.*" showFieldErrors="true"/>
        <c:choose>
         <c:when test="${price.id == 0 && servicetypes.size() == 0}">
       		<div class="warningBox1">
            	<div class="warningBox2">
                	<div class="warningBox3">
                    	<h5 class="center-0 attention">
							<spring:message code="editmodelprice.noservicetypes" />
						</h5>
                    </div>
                </div>
           </div>
           <div class="infobox">
           		<form:form>
               		<fieldset>
                       	<ol>
                           	<li>
                           		<input name="modelid" type="hidden"
									value="${price.instrumentModel.modelid}" />
                               	<input type="submit" name="return"
									id="submit" value="Return" />
                           	</li>
                       	</ol>
               		</fieldset>
               	</form:form>
               </div>
        </c:when>
        <c:otherwise>
        <div class="infobox">
            <form:form modelAttribute="price">
                <fieldset>
                        <ol>
                            <li>
                                <label><spring:message code="model"/>:</label>
                                <cwms:showmodel instrumentmodel="${price.instrumentModel}"/>
                            </li>
                            <li>
                                <label><spring:message code="costtype" />:</label>
                                <spring:message code="${price.costType.messageCode}"/>
                            </li>
                            <li>
                                <label><spring:message code="servicetype" />:</label>
                                <c:choose>
                                    <c:when test="${price.id == 0}">
                                        <select name="servicetypeid">
                                            <c:forEach var="serviceType" items="${servicetypes}">
                                                <option value="${serviceType.serviceTypeId}">
                                                	<cwms:besttranslation translations="${serviceType.longnameTranslation}"/>
												</option>
                                            </c:forEach>
                                        </select>
                                    </c:when>
                                    <c:otherwise>
                                        <cwms:besttranslation translations="${price.serviceType.longnameTranslation}"/>
                                    </c:otherwise>
                                </c:choose>
                            </li>
                            <li>
                                <label><spring:message code="fixedprice"/>(${businessCurrency.currencyCode}):</label>
                                <form:input path="fixedPrice" />
                            </li>
                            <li>
                                <label><spring:message
											code="variableprice" /> (${businessCurrency.currencyCode}):</label>
                                <form:input
										path="variablePrice.unitPrice" />
                            </li>
                            <li>
                                <label><spring:message
											code="per" />:</label>
                                <form:select path="variablePrice.unit">
                                    <form:option value="" />
                                    <form:options items="${units}"
											itemLabel="name" />
                                </form:select>
                            </li>
                            <li>
                            	<label><spring:message code="editproc.estimatedtime"/></label>
                            	<form:input path="standardTime"/>
                            </li>
                            <li>
                                <label><spring:message
											code="comments" />:</label>
                                <form:textarea path="comments" cols="80" rows="6"/>
                            </li>
                            <li>
                                <label>&nbsp;</label>
                                <form:hidden path="id" />
                                <input name="modelid" type="hidden"
									value="${price.instrumentModel.modelid}" />
                                <input type="submit" name="save"
									id="submit" value="<spring:message code="editproc.save" />" />
                                <c:if test="${price.id > 0}">
                                    <input type="submit" name="delete"
											id="delete" value="<spring:message code="editproc.delete" />" /> 
                                </c:if>
                            </li>
                        </ol>
                </fieldset>
            </form:form>
        </div>
        </c:otherwise>
        </c:choose>
    </jsp:body>
</t:crocodileTemplate>
