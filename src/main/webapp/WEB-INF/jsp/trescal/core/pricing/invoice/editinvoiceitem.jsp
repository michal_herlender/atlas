<%-- File name: /trescal/core/pricing/invoice/editinvoiceitem.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">
       	 	<spring:message code="invoice.createeditinvoiceitem"/><c:out value=" ${editinvoiceitemform.item.itemno} " /> 
        	<spring:message code="on"/><c:out value=" ${editinvoiceitemform.invoice.invno}" />
        </span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src="script/trescal/core/pricing/invoice/EditInvoiceItem.js"/></script>
		<script type='text/javascript'>
			var invoiceBrokered = false;
			/* Mark if invoice is locked for editing */
			<c:if test="${editinvoiceitemform.invoice.accountsStatus == 'S' || editinvoiceitemform.invoice.accountsStatus == 'P'}">
				invoiceBrokered = true;
			</c:if>
		</script>
		<script type='module' src='script/components/cwms-expenseitem-selector/cwms-expenseitem-selector.js'></script>
    </jsp:attribute>
    <jsp:body>
		<!-- Mark if item is locked for editing -->
		<c:if test="${editinvoiceitemform.invoice.accountsStatus  == 'S' || editinvoiceitemform.invoice.accountsStatus  == 'P'}">
			<div id="lockedmessage" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<spring:message code="invoice.notebrokered"/>.					
						<cwms:securedJsLink permission="INVOICE_UNLOCK" classAttr="mainlink" collapse="True"  onClick=" unlockInvoiceItem(); return false;" >
							<spring:message code="invoice.unlocktemporarily"/>
						</cwms:securedJsLink>					
					</div>
				</div>
			</div>							
		</c:if>
						
		<form:form action="" id="editinvoiceitemform" method="post" modelAttribute="editinvoiceitemform">
			<form:errors path="*">
        		<div class="warningBox1">
                	 <div class="warningBox2">
                    	   <div class="warningBox3">
                        	    <h5 class="center-0 attention">
                            	     <spring:message code='error.save' />
                            	</h5>
                            	<c:forEach var="e" items="${messages}">
                               		<div class="center attention"><c:out value="${e}"/></div>
                            	</c:forEach>
                     	 </div>
                 	</div>
           		</div>
       	   </form:errors>
		  <c:set var="currency" value="${editinvoiceitemform.invoice.currency}"/>
				
			<!-- div with nifty corners applied -->
			<div class="listbox">
				
				<fieldset>
					<legend>
						<span><spring:message code="invoice.createeditinvoiceitem"/><c:out value=" ${editinvoiceitemform.item.itemno} "/></span>
						<spring:message code="on"/> 
						<a href="<spring:url value='viewinvoice.htm?id=${editinvoiceitemform.invoice.id}'/>"><c:out value=" ${editinvoiceitemform.invoice.invno}"/></a> 
					</legend>
					<c:set var="disabled" value="false"/>
					<c:if test="${not empty editinvoiceitemform.item.jobItem}">
						<c:set var="disabled" value="true"/>
					</c:if>
					<ol>
						<li>
							<form:label path="businessSubdivId"><spring:message code="businesssubdivision"/>:</form:label>											
							<form:select path="businessSubdivId" disabled="${disabled}">
								<option value="0">N/A</option>
								<c:forEach var="subdiv" items="${businessSubdivs}">
									<option value="${subdiv.subdivid}" <c:if test="${editinvoiceitemform.businessSubdivId == subdiv.subdivid}"> selected="selected" </c:if>>
										${subdiv.subdivCode} - ${subdiv.subname} 
									</option>
								</c:forEach>
							</form:select>
							<form:errors path="businessSubdivId" class="error"/>										
						</li>
						<li>
							<label for="item.description"><spring:message code="description"/>:</label>											
							<form:textarea path="item.description" rows="5" class="addSpellCheck"></form:textarea>
							<form:errors path="item.description" class="error"/>									
						</li>
						<li>
							<form:label path="item.serialno"><spring:message code="serialno"/>:</form:label>
							<form:input path="item.serialno"/>
						</li>
						<li>
							<label class="width80"><spring:message code="invoice.noteblankforjobitemlink"/></label>
							<span>&nbsp;</span>
						</li>
						<c:if test="${jobitems.size() > 0}">
							<li>
								<form:label path="jobItemId"><spring:message code="invoice.linktojobitem"/>:</form:label>
								<form:select id="jobItemSelector" path="jobItemId" onchange="enableDisableItemSelectors();">
									<option value="">N/A</option>
									<c:forEach var="ji" items="${jobitems}">
										<option value="${ji.jobItemId}" <c:if test="${editinvoiceitemform.item.jobItem != null && editinvoiceitemform.item.jobItem.jobItemId == ji.jobItemId}"> selected="selected" </c:if>>
										Job ${ji.jobno} Item ${ji.itemno} -
										<cwms:securedLink permission="INSTRUMENT_VIEW" classAttr="mainlink" parameter="?plantid=${ji.instrument.plantid}">
											${ji.instrument.instrumentModelTranslation} (${ji.instrument.subfamilyTypology})
											<c:if test="${ not ji.instrument.instrumentMfrGeneric }">
												${ji.instrument.instrumentMfrName}&nbsp;
											</c:if>
											${ji.instrument.instrumentModelName}
										</cwms:securedLink>
										</option> 
									</c:forEach>
								</form:select>
							</li>
						</c:if>
						<c:if test="${expenseItems.size() > 0}">
							<li>
								<form:label path="expenseItemId">Link to job service</form:label>
								<form:select id="expenseItemSelector" path="expenseItemId" onchange="enableDisableItemSelectors();">
									<form:option value="">N/A</form:option>
									<c:forEach var="item" items="${expenseItems}">
										<form:option value="${item.id}">
		            						<spring:message code="job"/>
		            						${item.jobNo}
		            						<spring:message code="item"/>
		            						${item.itemNo}
		            						-
		            						${item.modelName}
		            						<c:if test="${not empty item.comment}">
		            						- ${item.comment}
		            						</c:if>
		        						</form:option>
		        					</c:forEach>
	        					</form:select>
							</li>
						</c:if>
		<!-- 				## remove cost editing for items on site invoices -->
						<c:if test="${editinvoiceitemform.invoice.pricingType != 'SITE' && (allowSplitCosts || editinvoiceitemform.item.breakUpCosts)}">
							<li>
								<c:set var="multipleChecked" value=""/>
								<c:set var="singleChecked" value=""/>
								<c:if test="${editinvoiceitemform.item.breakUpCosts == true}"> 
									<c:set var="multipleChecked" value="checked"/>
								</c:if>
								<c:if test="${editinvoiceitemform.item.breakUpCosts == false}">
									<c:set var="singleChecked" value="checked"/>
								</c:if>
								<form:label path="item.breakUpCosts"><spring:message code="invoice.splitcosts"/>:</form:label>
								<div class="float-left">
									<form:radiobutton path="item.breakUpCosts" value="true" checked="${multipleChecked}" onclick="switchSplitAndSingleCost('multiple'); return true; " /> <spring:message code="invoice.split"/><br />
									<form:radiobutton path="item.breakUpCosts" value="false" checked="${singleChecked}" onclick="switchSplitAndSingleCost('single'); return true; " /> <spring:message code="invoice.single"/>
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
						</c:if>
						
					</ol>
					
				</fieldset>
				
			</div>
			<!-- end of nifty corners div -->
			
		<!-- 	## don't allow cost editing for site invoice items -->
			<c:if test="${editinvoiceitemform.invoice.pricingType != 'SITE'}">
		<!-- 		## set up the displaying / hiding of single / broken up costs -->
				<c:set var="showSplitCosts" value="block" />
				<c:set var="showSingleCosts" value="none" />
				<c:set var="showSingleNominal" value="none" />
				<c:if test="${editinvoiceitemform.item.breakUpCosts != true}"> 
					<c:set var="showSplitCosts" value="none" />
					<c:set var="showSingleCosts" value="block" />
					<c:set var="showSingleNominal" value="block" />
				</c:if>
			
				<!-- this div contains all calibration split costs -->									
				<div id="splitcosts" style=" display: ${showSplitCosts}; ">	
																												
					<c:forEach var="cost" items="${editinvoiceitemform.item.costs}">
					
						<c:set var="costsStyle" value="block" />
						<c:set var="addLinkStyle" value="inline" />
						<c:set var="removeLinkStyle" value="inline" />
						<c:choose>
							<c:when test="${cost.active == false}"> 
							<c:set var="costsStyle" value="none" />
							<c:set var="removeLinkStyle" value="none" />
							</c:when>
							<c:otherwise>
							<c:set var="addLinkStyle" value="none" />
							</c:otherwise>
						</c:choose>
						
						<c:set var="costname" value="${cost.costType.name}"/>
						<c:set var="name" value="${costname.toLowerCase()}"/>
						<c:set var="costid" value="${cost.costType.typeid}"/>
						<c:set var="includeCosts" value="${cost.active}"/>
						
						<!-- div with nifty corners applied -->
						<div class="listbox">	
									
							<fieldset>
								
								<legend>
									<span><spring:message code="${cost.costType.messageCode}"/> </span> <spring:message code="costs"/>
									<a href="#" style=" display: ${addLinkStyle}; " onclick=" switchCostTypeDisplay('${name}', true); return false;" id="${name}add">
										<img src="img/icons/add.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code="add"/> <spring:message code="${cost.costType.messageCode}"/> <spring:message code="cost"/>" title="<spring:message code="add"/> <spring:message code="${cost.costType.messageCode}"/> <spring:message code="cost"/>" />
									</a>
									<a href="#" style=" display: ${removeLinkStyle}; " onclick=" switchCostTypeDisplay('${name}', false); return false;" id="${name}remove">
										<img src="img/icons/delete.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code="remove"/> <spring:message code="${cost.costType.messageCode}"/> <spring:message code="cost"/>" title="<spring:message code="remove"/> <spring:message code="${cost.costType.messageCode}"/> <spring:message code="cost"/>" />
									</a>
								</legend>
								
								<ol id="${name}costs" style=" display: ${costsStyle}; ">
										
									<li class="nopadding nobord relative">
																						
										 <form:hidden path="item.${name}Cost.active" />
										<ol class="editinvoiceitem">
													
											<li>
												<form:label path="${name}NominalId"><spring:message code="invoice.nominal"/>:</form:label>
												<div class="symbolbox"></div>
												<div class="valuebox">
													<form:select path="${name}NominalId">
														<option value="">N/A / <spring:message code="unknown"/></option>
														<c:forEach var="nominal" items="${nominalList}">
															<option value="${nominal.id}" <c:if test="${not empty cost.nominal}"></c:if> <c:if test="${nominal.id == cost.nominal.id}"> selected="selected" </c:if>>${nominal.code} - <t:showTranslationOrDefault translations="${nominal.titleTranslations}" defaultLocale="${defaultlocale}" /></option>
														</c:forEach>
													</form:select>
												</div>
												<form:errors path="${name}NominalId" class="error"/>
												<!-- clear floats and restore page flow -->
												<div class="clear"></div>
											</li>
											<li>
												<label><spring:message code="totalcost"/>:</label>
												<div class="symbolbox">${currency.currencyERSymbol}</div>
												<div class="valuebox">
													<form:input path="item.${name}Cost.totalCost" cssClass="right" /> 
												</div>
												<span class="error">${status.errorMessage}</span>
												<!-- clear floats and restore page flow -->
												<div class="clear"></div>																	
											</li>
											<li>
												<label><spring:message code="invoice.discountrate"/>:</label>
												<div class="symbolbox"></div>
												<div class="valuebox">
													<form:input path="item.${name}Cost.discountRate" cssClass="right" /> 
												</div>
												<div class="float-left padtop">&nbsp;%</div>
												<form:errors path="item.${name}Cost.discountRate" class="error"/>
												<!-- clear floats and restore page flow -->
												<div class="clear"></div>			
											</li>
											<li>
												<label><spring:message code="invoice.discountvalue"/>:</label>
												<div class="symbolbox">${currency.currencyERSymbol}</div>
												<div class="valuebox">
													<span class="valuetext">
														${cost.discountValue}
													</span>
																												
												</div>
												<!-- clear floats and restore page flow -->
												<div class="clear"></div>	
											</li>
											<li>
												<label><spring:message code="total"/>:</label>
												<div class="symbolbox">${currency.currencyERSymbol}</div>
												<div class="valuebox">
													<span class="valuetext">
														${cost.finalCost}
													</span>
																												
												</div>
												<!-- clear floats and restore page flow -->
												<div class="clear"></div>																	
											</li>
											
										</ol>													
																
									</li>
										
								</ol>
									
							</fieldset>
									
						</div>
						<!-- end of nifty corners div -->																						
										
					</c:forEach>
								
				</div>
				<!-- end of calibration split costs -->
			</c:if>										
						
			<!-- div with nifty corners applied -->
			<div class="listbox">	
							
				<fieldset>
				
					<legend><spring:message code="total"/></legend>
					
					<ol>
						<li id="singlenominal" style=" display: ${showSingleNominal}; ">
							<form:label path="singleNominalId"><spring:message code="invoice.nominal"/>:</form:label>
							<div class="symbolbox">&nbsp;</div>
							<div class="valuebox">
								<form:select path="singleNominalId">
									<option value="">N/A / <spring:message code="unknown"/></option>
									<c:forEach var="nominal" items="${nominalList}">
										<option value="${nominal.id}" <c:if test="${not empty editinvoiceitemform.item.nominal}"></c:if> <c:if test="${nominal.id == editinvoiceitemform.item.nominal.id}"> selected="selected" </c:if>>${nominal.code} - <t:showTranslationOrDefault translations="${nominal.titleTranslations}" defaultLocale="${defaultlocale}" /></option>
									</c:forEach>
								</form:select>
							</div>
							<form:errors path="singleNominalId" class="error"/>								
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
		<!-- 				## don't allow cost editing for site invoice items -->
						<c:if test="${editinvoiceitemform.invoice.pricingType != 'SITE'}">
							<li id="singlecosttotal" class="editinvoiceitem" style=" display: ${showSingleCosts}; ">
								<label><spring:message code="itemcost"/>:</label>
								<div class="symbolbox">${currency.currencyERSymbol}</div>
								<div class="valuebox">
									<form:input path="item.totalCost" cssClass="right" />
								</div>
								<div class="float-left padtop">
									<span class="error">${status.errorMessage}</span>						
								</div>											
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>									
							</li>
							<li id="multiplecosttotal" style=" display: ${showSplitCosts}; ">
								<label><spring:message code="itemcost"/>:</label>
								<div class="symbolbox">${currency.currencyERSymbol}</div>
								<div class="valuebox">
									<span class="valuetext">
										${editinvoiceitemform.item.totalCost}
									</span>
									
								</div>
								<div class="float-left padtop"></div>
								<span class="error">${status.errorMessage}</span>						
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li id="generaldiscountrate" class="editinvoiceitem">
								<form:label path="item.generalDiscountRate"><spring:message code="invoice.discountrate"/>:</form:label>
								<div class="symbolbox"></div>
								<div class="valuebox">
									<form:input type="text" path="item.generalDiscountRate" class="right" />
								</div>
								<div class="float-left padtop">
									&nbsp;%&nbsp;						
								</div>
								<form:errors path="item.generalDiscountRate" class="error"/>				
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>					
							</li>						
							<li>
								<label for="finalcost"><spring:message code="finalcost"/>:</label>
								<div class="symbolbox">${currency.currencyERSymbol}</div>
								<div class="valuebox">
									<span class="valuetext">
										${editinvoiceitemform.item.finalCost}
									</span>
								
								</div>
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>				
						</c:if>
						<li>
							<label>&nbsp;</label>
							<input type="submit" name="submit" value="<spring:message code='save'/>">
						</li>	
					</ol>																	
			 	</fieldset>
												
			</div>
			<!-- end of nifty corners div -->
				
		</form:form>	
    </jsp:body>
</t:crocodileTemplate>