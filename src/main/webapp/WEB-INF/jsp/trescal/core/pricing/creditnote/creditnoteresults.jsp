<%-- File name: /trescal/core/pricing/creditnote/creditnoteresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
          <span class="headtext"><spring:message code="creditnote.creditnotesearchresults"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/pricing/creditnote/CreditNoteResults.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox div containing invoice results -->						
		<div class="infobox" id="InvSearchResults">
			
			<form:form action="" name="searchform" id="searchform" method="post" modelAttribute="creditnotehomeform">

				<form:hidden path="compId"/>
				<form:hidden path="invNo"/>
				<form:hidden path="creditNoteNo"/> 
				<%-- Note: Spring data binding automatically converts dates to text via property editor --%>
				<form:hidden path="date"/> 
				<form:hidden path="dateFrom"/> 
				<form:hidden path="dateTo"/> 
				<form:hidden path="pageNo"/>
				<form:hidden path="resultsPerPage"/> 
				
				<t:showResultsPagination rs="${creditnotehomeform.rs}" pageNoId=""/>
				
				<table class="default2" summary="This table lists all credit note search results">
					<thead>
						<tr>
							<td colspan="5"><spring:message code="creditnote.creditnoteresults"/> (${creditnotehomeform.rs.results.size()})</td>
						</tr>
						<tr>
							<th class="pono" scope="col"><spring:message code="creditnote.creditnoteno"/></th>  
							<th class="coname" scope="col"> <spring:message code="company"/></th>  
							<th	class="jobno" scope="col"><spring:message code="creditnote.invoiceno"/></th>
							<th	class="regdate" scope="col"><spring:message code="regdate"/></th>
							<th class="value" scope="col"><spring:message code="creditnote.value"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="keynavResults">
						<c:set var="rowcount" value="0"/>
					<c:choose>
                                    <c:when test="${creditnotehomeform.rs.results.size() < 1}">
							<tr>
								<td colspan="5" class="bold center">
									<spring:message code="noresults"/>
								</td>
							</tr>
						</c:when>
						<c:otherwise>												
							<c:forEach var="cn" items="${creditnotehomeform.rs.results}">
								<tr onclick="window.location.href='viewcreditnote.htm?id=${cn.id}'" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
									<td>
										<a href="viewcreditnote.htm?id=${cn.id}" class="mainlink">${cn.creditNoteNo}</a>
									</td>
									<td>
										<links:companyLinkInfo companyId="${cn.coid}" onStop="${cn.companyOnStop}" rowcount="0" companyName="${cn.coname}" active="${cn.companyActive}" copy="true" />
									</td>
									<td>${cn.invno}</td>
									<td class="regdate"><fmt:formatDate value="${cn.regDate}" type="date" dateStyle="SHORT"/></td>
									<td class="right">${cn.currencyERSymbol}${cn.totalCost}</td>
									
							  </tr>
								<c:set var="rowcount" value="${rowcount + 1}"/>
							</c:forEach>
						</c:otherwise>
				    </c:choose>
					</tbody>
				</table>
				
				<t:showResultsPagination rs="${creditnotehomeform.rs}" pageNoId=""/>
				
			</form:form>
			
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>