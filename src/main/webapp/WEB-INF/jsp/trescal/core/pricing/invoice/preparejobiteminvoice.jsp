<%-- File name: /trescal/core/pricing/invoice/preparejobiteminvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="invoice.preparejobiteminvoice" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type="text/javascript" src="script/trescal/core/pricing/invoice/PrepareJobItemInvoice.js"></script>
	</jsp:attribute>
	<jsp:body>
		<div class="infobox">
			<fieldset>
				<ol>
					<li>
						<label class="bold width30">
							<spring:message code="invoice.invoicepreparationfor" />:
						</label>
						<span>
							<c:choose>
								<c:when test="${companyWide}">
									<spring:message code="businesscompany" />
								</c:when>
								<c:otherwise>
									<spring:message code="businesssubdivision" />
								</c:otherwise>
							</c:choose>
						</span>
					</li>
					<li>
						<label class="bold width30">
							<spring:message code="businesscompany" />:
						</label>
						<span>${allocatedCompany.value}</span>
					</li>
					<li>
						<label class="bold width30">
							<spring:message code="businesssubdivision" />:
						</label>
						<span>
							<c:choose>
								<c:when test="${companyWide}"><spring:message code="all" /></c:when>
								<c:otherwise>${allocatedSubdiv.value}</c:otherwise>
							</c:choose>
						</span>
					</li>
				</ol>
			</fieldset>
		</div>
	
		<!-- infobox div which contains all prepare invoice elements -->
		<div class="infobox">
			<div>
				<label ><spring:message code="invoice.jobcompleted" />:
					<select class="jobCompleteFilter" onchange="filter('readyToInvoiceTable')">
							<option value="ALL"><spring:message code="all" /></option>
							<option value="YES"><spring:message code="yes" /></option>
							<option value="NO"><spring:message code="no" /></option>
					</select>
			 	</label>
			</div> <br> <br>
			<table class="default2" id="readyToInvoiceTable">
				<thead>
					<tr>
						<td colspan="6">
							<spring:message code="invoice.jobitemsawaitinginvoice"/>&nbsp;(${readyToInvoice.size()}&nbsp;<spring:message code="company.companiesfound"/>)
						</td>
					</tr>
					<tr>
						<th scope="col" class="comp"><spring:message code="company"/></th>
						<th scope="col" class="comprole"><spring:message code="company.role"/></th>
						<th scope="col" class="jobs"><spring:message code="invoice.jobitemscompleted"/></th>
						<th scope="col" class="invoice"><spring:message code="invoice.invoiceableservices"/></th>
						<th scope="col" class="invoice"><spring:message code="viewjob.previnvoi"/></th>
						<th scope="col" class="invoice"><spring:message code="viewjob.periodiclinking"/></th>
					</tr>
				</thead>												
				<tbody>
					<c:choose>
						<c:when test="${empty readyToInvoice}">
							<tr class="odd">
								<td colspan="6" class="center bold"><spring:message code="invoice.therearenojobitemsreadytoinvoice" /></td>
							</tr>
						</c:when>
						<c:otherwise>
						    <c:forEach var="coid" items="${readyToInvoice.keySet()}" varStatus="loopStatus">
						    	<c:choose>
									<c:when test="${loopStatus.index % 2 == 0}">
										<c:set var="rowClass" value="even" />
									</c:when>
									<c:otherwise>
										<c:set var="rowClass" value="odd" />
									</c:otherwise>
								</c:choose>
								<c:set var="completejob" value="YES"></c:set>
								<c:if test="${!companyResult.completeJob}" >
									<c:set var="completejob" value="NO"></c:set>
								</c:if>
								<tr class="${rowClass}" jobcomplete="${completejob}" jiwithcompletejob="${readyToInvoice.get(coid).jicompletedCountJobComplete}"
								jiwithnocompletejob="${readyToInvoice.get(coid).jicompletedCountJobnoComplete}" jitotal="${readyToInvoice.get(coid).jicompletedTotal}"
								jewithcompletejob="${readyToInvoice.get(coid).jecompletedCountJobComplete}" jewithnocompletejob="${readyToInvoice.get(coid).jecompletedCountJobnoComplete}" 
								jetotal="${readyToInvoice.get(coid).jecompletedTotal}">
									<td>${readyToInvoice.get(coid).companyName}</td>
									<td class="center">
										<spring:message var="companyRole" code="${readyToInvoice.get(coid).companyRole.messageCode}"/>
										${companyRole}
									</td>
									<td class="totalji center" >${readyToInvoice.get(coid).jicompletedTotal}</td>
									<td class="jijobcomplete center" style="display: none">${readyToInvoice.get(coid).jicompletedCountJobComplete}</td>
									<td class="jijobnocomplete center" style="display: none">${readyToInvoice.get(coid).jicompletedCountJobnoComplete}</td>
									
									<td class="totalje center" >${readyToInvoice.get(coid).jecompletedTotal}</td>
									<td class="jejobcomplete center" style="display: none">${readyToInvoice.get(coid).jecompletedCountJobComplete}</td>
									<td class="jejobnocomplete center" style="display: none">${readyToInvoice.get(coid).jecompletedCountJobnoComplete}</td>
									<td class="invoicecreate center">
										<cwms:securedLink permission="INVOICE_CREATE" parameter="?coidforjobitems=${coid}&companyWide=${companyWide}&complete=ALL" collapse="True" >
											<img src="img/icons/page.png" width="16" height="16" alt="<spring:message code='viewjob.previnvoi' />" title="<spring:message code='viewjob.previnvoi' />" />
										</cwms:securedLink>
									</td>
									<td class="periodiclinking center">
										<cwms:securedLink permission="PERIODIC_LINKING"  parameter="?coid=${coid}&companyWide=${companyWide}&complete=ALL" collapse="True" >
											<img src="img/icons/link.png" width="16" height="16" alt="<spring:message code='viewjob.periodiclinking' />" title="<spring:message code='viewjob.periodiclinking' />" />
										</cwms:securedLink>
									</td>
								</tr>
						    </c:forEach>
							
						</c:otherwise>
					</c:choose>
				</tbody>
				<tfoot>
					<tr>
						<td><spring:message code="total"/> - <spring:message code="invoice.jobitemscompleted"/></td>
						<td class="jobs"></td>
						<td class="jobs" id="countJobItems">${countJobItems}</td>
						<td class="jobs" id="countServiceItems" >${countServiceItems}</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</tfoot>												
			</table>
		</div>
		<!-- end of infobox div -->
	</jsp:body>
</t:crocodileTemplate>