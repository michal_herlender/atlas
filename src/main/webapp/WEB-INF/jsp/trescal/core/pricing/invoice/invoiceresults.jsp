<%-- File name: /trescal/core/pricing/invoice/invoiceresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="quotation" tagdir="/WEB-INF/tags/quotation" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="invoice.invoicesearchresults"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/pricing/invoice/InvoiceResults.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox div containing invoice results -->						
		<div class="infobox" id="InvSearchResults">
			
			<form:form action="" name="searchform" id="searchform" method="post" modelAttribute="invoicehomeform">
				
				<form:hidden path="subdivId" />
				<form:hidden path="coid" />
				<form:hidden path="personid" />
				<form:hidden path="coname" />
				<form:hidden path="contact" />
				<form:hidden path="invno" />
				<form:hidden path="jobno" />
				<form:hidden path="typeid" />

				<form:hidden path="date" />				
				<form:hidden path="dateFrom" />				
				<form:hidden path="dateTo" />				
																				
				<form:hidden path="pageNo" />
				<form:hidden path="resultsPerPage" />
				<form:hidden path="resultsTotal" />
				
				<t:showResultsPagination rs="${invoicehomeform.rs}" pageNoId="" />
				
				<table class="default2" summary="This table lists all invoice search results">
					<thead>
						<tr>

							<td colspan="9"><spring:message code="invoice.invoiceresults"/> (${invoicehomeform.rs.results.size()})
								<spring:message code="web.export.excel" var="submitText"/>  
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>

						</tr>
						<tr>
							<th class="pono" scope="col"><spring:message code="invoice.invoicenumber"/></th> 
							<th class="pono" scope="col"><spring:message code="invoice.invoicebpopo"/></th> 
							<th class="coname" scope="col"><spring:message code="company"/></th>
							<th class="subdiv" scope="col"><spring:message code="subdiv"/></th>  
							<th	class="jobno" scope="col"><spring:message code="jobno"/></th>
							<th	class="type" scope="col"><spring:message code="type"/></th>
							<th	class="cnref" scope="col"><spring:message code="creditnote.ref"/></th>
							<th	class="regdate" scope="col"><spring:message code="invoice.invoicedate"/></th>
							<th class="value" scope="col"><spring:message code="invoice.value"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="9">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="keynavResults">
						<c:set var="rowcount" value="0"/>
						<c:choose>
						 	<c:when test="${invoicehomeform.rs.results.size() < 1}">
							<tr>
								<td colspan="9" class="bold center">
									<spring:message code="noresults"/>
								</td>
							</tr>
							</c:when> 
							<c:otherwise> 												
							 <c:forEach var="invoice" items="${invoicehomeform.rs.results}"> 
								<tr onclick="window.location.href='<spring:url value="viewinvoice.htm?id=${invoice.id}"/>'" 
								
								 onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); ">
									<td>
										<a href="<spring:url value='viewinvoice.htm?id=${invoice.id}'/>" class="mainlink">${invoice.invno}</a>
									</td>
									<td>
									<c:forEach var="PO" items="${invoice.invoiceItemPoLinkDTO}" varStatus="loop">
									<c:if test="${loop.index != 0}"> <br></c:if>
									<c:choose>
									<c:when test="${not empty PO.poId}">
									<spring:message code="invoice.po"/> : ${PO.poNo}
									</c:when>
									
									<c:when test="${not empty PO.bpoId}">
									<spring:message code="invoice.bpo"/> : ${PO.bpoNo}
									</c:when>
									
									<c:when test="${not empty PO.invoicePOId}">
									<spring:message code="invoice.invoicepo"/> : ${PO.invoicePONo}
									</c:when>
									
									</c:choose>
									</c:forEach>
									</td>
									<td>
									    <links:companyLinkInfo companyId="${invoice.coid}" onStop="${invoice.companyOnStop}" rowcount="0" companyName="${invoice.coname}" active="${invoice.companyActive}" copy="true" />
									</td>
									<td>
										<links:subdivLinkInfo rowcount="${rowcount}"
										subdivId="${invoice.subdivid}"
										subdivName="${invoice.subname}"
										companyActive="${invoice.companyActive}"
										companyOnStop="${invoice.companyOnStop}"
										subdivActive="${invoice.subdivActive}" />
									</td>
									<td>
										<c:forEach var="jl" items="${invoice.invoiceJobLinksDto}" varStatus="loop">
											<c:if test="${loop.index == 0}">
												<c:if test="${not empty jl.jobid}">
													${jl.jobno}
												</c:if>
												<c:if test="${invoice.invoiceJobLinksDto.size() > 1}">
													<c:set var="more" value="${invoice.invoiceJobLinksDto.size() - 1}"/> 
													... (+ ${more} more)
												</c:if>		
											</c:if>
										</c:forEach>
									</td>
									<td class="center">${invoice.invoiceType}</td>
									
									<td>
									<c:forEach var="CN" items="${invoice.creditNoteNumbers}" varStatus="loop">
										<c:if test="${loop.index != 0}"> <br> </c:if>
										${CN}
									</c:forEach>
									</td>
									
									<td class="center"><fmt:formatDate value="${invoice.invoiceDate}" type="date" dateStyle="SHORT"/></td>
									<td class="right">
									${invoice.currencyERSymbol}
									${invoice.totalCost}</td>
								</tr>
								<c:set var="rowcount" value="${rowcount + 1}"/>
							</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<table class="default tlast" summary="">
					<tfoot>
						<tr>
							<td>
								<input type="submit" name="export" value="${submitText}" class="float-right">
							</td>
							
						</tr>
					</tfoot>
				</table>
				<t:showResultsPagination rs="${invoicehomeform.rs}" pageNoId="" />
				
			</form:form>
			
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>