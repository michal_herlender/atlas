<%-- File name: /trescal/core/pricing/creditnote/viewcreditnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.trescal.cwms.core.account.entity.AccountStatus" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="creditnote.creditnote" />
			<c:out value=" ${creditNote.creditNoteNo} "/>
			<spring:message code="creditnote.forinvoice" />
			<c:out value=" ${creditNote.invoice.invno}"/>
		</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript'>
			var nominalArray = [];
			<c:forEach var="nominal" items="${nominals}">
				nominalArray.push({ nominalId: ${nominal.id}, nominalCode: '<spring:escapeBody javaScriptEscape="true">${nominal.code}</spring:escapeBody>', nominalTitle: '<spring:escapeBody javaScriptEscape="true">${nominal.translatedTitle}</spring:escapeBody>' });
			</c:forEach>
		
			var subdivArray = [];
			<c:forEach var="subdiv" items="${businessSubdivs}">
				subdivArray.push({ id: ${subdiv.subdivid}, title: '<spring:escapeBody javaScriptEscape="true">${subdiv.subdivCode} - ${subdiv.subname}</spring:escapeBody>' });
			</c:forEach>
		
			var invoiceBrokered = false;
			/* Mark if credit note is locked for editing */
			<c:if test="${creditNote.accountsStatus == 'S' || creditNote.accountsStatus == 'P'}">
			invoiceBrokered = true;
			</c:if>
		</script>
		
		<script type='text/javascript' src='script/trescal/core/pricing/creditnote/ViewCreditNote.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${creditNote.creditNoteNo}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<!-- Mark if credit note is locked for editing -->
		<c:if test="${creditNote.accountsStatus == 'S' || creditNote.accountsStatus == 'P'}">
			<div id="lockedmessage" class="warningBox1">
				<div class="warningBox2">
					<div class="warningBox3">
						<spring:message code="creditnote.pleasenotethiscreditnotehasbeenbrokeredithasbeenlockedforediting" />
							<cwms:securedJsLink classAttr="mainlink" collapse="True" onClick=" unlockCreditNote(); return false; "  permission="CREDIT_NOTE_UNLOCK">
								<spring:message code="creditnote.unlockthiscreditnoteandtemporarilyenableediting" />
							</cwms:securedJsLink>
					</div>
				</div>
			</div>	
		</c:if>
		
		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<fieldset>
				<legend>
					<spring:message code="creditnote.viewcreditnote" />
				</legend>
				<div class="displaycolumn">
					<ol>
						<li>
							<input id="creditNoteId" type="hidden" value="${creditNote.id}">
							<label><spring:message code="creditnote.creditnoteno" />:</label>
							<span>${creditNote.creditNoteNo}</span>
						</li>
						<li>
							<label><spring:message code="createdby" />:</label>
							<span>${creditNote.createdBy.name} &nbsp; <fmt:formatDate value="${creditNote.regdate}" type="date" dateStyle="SHORT"/></span>
						</li>
						<li>
							<label><spring:message code="currency" />:</label>
							<span>
								<c:set var="currency" value="${creditNote.currency}" />
								<c:out value="${currency.currencyCode} (${currency.currencySymbol} ${currency.currencyName} - " />
								<fmt:formatNumber value="1" type="currency" currencySymbol="${defaultCurrency.currencyERSymbol}"/> : <fmt:formatNumber value="${creditNote.rate}" type="currency" currencySymbol="${currency.currencyERSymbol}"/>)
							</span>
						</li>
						<li>
							<label><spring:message code="company.vatrate"/>:</label>
							<!-- display VAT information -->
							<span>
								<c:out value="${formattedVatRate}" />
							</span>
						</li>
						<li>
							<label><spring:message code="creditnote.issuedate" />:</label>
							<span>
								<c:choose>
									<c:when test="${not empty creditNote.issuedate}">
										<fmt:formatDate value="${creditNote.issuedate}" type="date" dateStyle="SHORT"/>
									</c:when>
									<c:otherwise>
										-
									</c:otherwise>
								</c:choose>
							</span>
						</li>
					</ol>
				</div>
				<div class="displaycolumn">
					<ol>
						<li>
							<label><spring:message code="creditnote.coveringinvoice" />:</label>
							<span>
								<a class="mainlink" href="<c:url value="viewinvoice.htm?id=${creditNote.invoice.id}"/>">
									${creditNote.invoice.invno}
								</a>
							</span>
						</li>
						<li>
							<label><spring:message code="status" />:</label>
							<div class="float-left padtop">
								<t:showTranslationOrDefault translations="${creditNote.status.descriptiontranslations}" defaultLocale="${defaultlocale}"/>
								<c:if test="${creditNote.status.requiresAttention}">
									<br/>
									<a href="<c:url value="viewcreditnote.htm?id=${creditNote.id}&submission=submit"/>" class="mainlink">
										<spring:message code="creditnote.updatestatusto" />
										<c:out value=" " />
										<t:showTranslationOrDefault translations="${creditNote.status.next.nametranslations}" defaultLocale="${defaultlocale}"/>
									</a>
								</c:if>
							</div>
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="creditnote.accountsstatus" />:</label>
							<div class="float-left padtop" id="accountstatus">
								<c:choose>
									<c:when test="${accountssoftwaresupported && (empty creditNote.invoice.comp.account)}">
										<spring:message code="creditnote.noaccountsprofilehasbeensetupfor" /> ${invoice.comp.coname}.
										<spring:message code="creditnote.oncea" />
											<a href="viewcomp.htm?coid=${creditNote.invoice.comp.coid}&loadtab=finance-tab">
												<spring:message code="creditnote.profilehasbeencreated" /> 
											</a>
										<spring:message code="creditnote.brokeringcanbeprocessed" />.
									</c:when>
									<c:when test="${!accountssoftwaresupported && ((empty companySettings) || (companySettings.status != 'VERIFIED'))}">
										<spring:message code="companystatusnotverified" />.
										<a href="viewcomp.htm?coid=${creditNote.invoice.comp.coid}&loadtab=finance-tab">
											<spring:message code="verifycompanytocontinue" />
										</a>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${creditNote.accountsStatus == 'P'}">
												<img src="img/icons/sync_pending.png" width="14" height="14" alt="<spring:message code="creditnote.pendingsynchronisation" />" title="<spring:message code="creditnote.pendingsynchronisation" />" class="image_inl" />
												<spring:message code="creditnote.pendingsynchronisationwithaccountingsoftware" />
											</c:when>
											<c:when test="${creditNote.accountsStatus == 'E'}">
												<img src="img/icons/sync_error.png" width="14" height="14" alt="<spring:message code="creditnote.errorsynchronising" />" title="<spring:message code="creditnote.errorsynchronising" />" class="image_inl" />
												<spring:message code="creditnote.errorssynchronisingwithaccountingsoftware" />
											</c:when>
											<c:when test="${creditNote.accountsStatus == 'S'}">
												<img src="img/icons/sync.png" width="14" height="14" alt="<spring:message code="creditnote.accountsynchronised" />" title="<spring:message code="creditnote.accountsynchronised" />" class="image_inl" />
												<spring:message code="creditnote.creditnotesynchronisedwithaccountingsoftware" />
											</c:when>
											<c:otherwise>
												<spring:message code="creditnote.nobrokerstatusdataavailable" />
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</div>
							<div class="clear"></div>
						</li>
					</ol>
				</div>
			</fieldset>
		</div>
		<!-- end infobox div -->
		<c:set var="notvisSynchronized" value=""/>
		<c:if test="${creditNote.accountsStatus == 'S' || creditNote.accountsStatus == 'P'}">
			<c:set var="notvisSynchronized" value="notvis"/>
		</c:if>
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'items-tab', false); " id="items-link" title="<spring:message code="creditnote.viewcreditnoteitems" />" class="selected">
						<spring:message code="items" /> (<span class="cnItemSize">${creditNote.items.size()}</span>)
					</a>
				</dt> 
				<dt>
					<a href="editcreditnote.htm?id=${creditNote.id}" id="edit-link" title="<spring:message code="creditnote.editcreditnote" />" class="limitAccess ${notvisSynchronized}">
						<spring:message code="edit" />
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'actions-tab', false); " id="actions-link" title="<spring:message code="creditnote.viewcreditnoteactionhistory" />">
						<spring:message code="creditnote.actions" /> (${creditNote.actions.size()})
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'email-tab', false); " id="email-link" title="<spring:message code="creditnote.viewsendemailsforcreditnote" />">
						<spring:message code="emails" /> (<span class="entityEmailDisplaySize">${creditNote.sentEmails.size()}</span>)
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'cndocs-tab', false); " id="cndocs-link" title="<spring:message code="creditnote.viewdocumentsincreditnotedirectory" />">
						<spring:message code="documents" /> (<span class="fileResourceCount">${scRootFiles.numberOfFiles}</span>)
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); genDocPopup(${creditNote.id}, 'birtcreditnote.htm?id=', '<spring:message code="creditnote.generatingcreditnotedocument" />'); " title="<spring:message code="creditnote.generatecreditnotedocument" />">
						<spring:message code="generate" />
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); loadScript.createOverlay('dynamic', '<spring:message code="creditnote.deletethiscreditnote" />', null, createDeleteCreditNoteContent(${creditNote.id}), null, 36, 640, 'deletereason'); " title="<spring:message code="creditnote.deletethiscreditnote" />" class="limitAccess ${notvisSynchronized}">
						<spring:message code="delete" />
					</a>
				</dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		<c:set var="currency" value="${creditNote.currency}"/>
		<!-- infobox div with nifty corners applied -->
		<div class="infobox">
			<!-- div containing all credit note items -->
			<div id="items-tab">				
				<table id="creditNoteItems" class="default4 creditNoteItems" summary="<spring:message code="creditnote.tableallcreditnoteitems" />">
					<thead>
						<tr>
							<td colspan="6">
								<div class="float-right limitAccess ${notvisSynchronized}">
									<span class="bold"><spring:message code="creditnote.addnewcreditnoteitem" /></span>
									<!-- Note 'Add' is a command to system and is not translated -->
									<a href="" onclick="event.preventDefault(); addCreditNoteItem();" class="addAnchor" title="<spring:message code="creditnote.addnewcreditnoteitem" />">&nbsp;</a>
								</div>
							</td>
						</tr>
						<tr>
							<th class="item" scope="col"><spring:message code="item"/></th>
							<th class="desc" scope="col"><spring:message code="description"/></th>
							<th class="nominal" scope="col"><spring:message code="invoice.nominal"/></th>
							<th class="subdiv" scope="col"><spring:message code="businesssubdivision"/></th>
							<th class="amount" scope="col"><spring:message code="creditnote.amount"/></th>
							<th class="del" scope="col"><spring:message code="delete"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="6">
								<span class="bold limitAccess ${notvisSynchronized}">
									<!-- Note 'Add' is a command to system and is not translated -->
									<a href="" onclick="event.preventDefault(); addCreditNoteItem();" class="addAnchor" title="<spring:message code="creditnote.addnewcreditnoteitem" />">&nbsp;</a>
									<spring:message code="creditnote.addnewcreditnoteitem" />
								</span>
							</td>
						</tr>
					</tfoot>
					<tbody>
						<c:choose>
							<c:when test="${creditNote.items.size() > 0}">
								<c:forEach var="cnItem" items="${creditNote.items}">
									<tr id="cnItem${cnItem.id}">
										<td class="item"><input type="hidden" value="${cnItem.id}">${cnItem.itemno}</td>
										<td class="desc">${cnItem.description}</td>
										<td class="nominal">
											<input type="hidden" value="${cnItem.nominal.id}" />
											<span class="nominalCode">
												${cnItem.nominal.code} -
												<t:showTranslationOrDefault translations="${cnItem.nominal.titleTranslations}" defaultLocale="${defaultlocale}" />
											</span>
										</td>
										<td class="subdiv">
											<input type="hidden" value="${cnItem.businessSubdiv.subdivid}" />
											<span class="subdivName">
												${cnItem.businessSubdiv.subdivCode} - ${cnItem.businessSubdiv.subname}
											</span>
										</td>
										<td class="amount">														
											<input type="hidden" value="${cnItem.finalCost}" />
											<a href="#" class="mainlink limitAccess ${notvisSynchronized}" onclick="event.preventDefault(); editCreditNoteItem(this);">
												<span class="finalCost">
													<fmt:formatNumber value="${cnItem.finalCost}" minIntegerDigits="1" minFractionDigits="2"/>
												</span>
											</a>
											<c:if test="${creditNote.accountsStatus == 'S' || creditNote.accountsStatus == 'P'}">
												<span class="lockedDisplay finalCost">
													<fmt:formatNumber value="${cnItem.finalCost}" minIntegerDigits="1" minFractionDigits="2"/>
												</span>
											</c:if>
										</td>
										<td class="del">
											<a href="#" onclick=" deleteItemFromCreditNote(${cnItem.id}); return false; " class="deleteAnchor limitAccess ${notvisSynchronized}">&nbsp;</a>
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
							<tr>
								<td colspan="6" class="bold text-center">
									<spring:message code="creditnote.therearenoitemsonthiscreditnote" />
								</td>
							</tr>
							</c:otherwise>
						</c:choose>
						<tr id="newItemRow" class="hid">
									<td class="item"><input type="hidden" value=""></td>
									<td class="desc"></td>
									<td class="nominal">
										<input type="hidden" value=""/>
										<span class="nominalCode"></span>
									</td>
									<td class="subdiv">
										<input type="hidden" value=""/>
										<span class="subdivName"></span>
									</td>
									<td class="amount">														
										<input type="hidden" value=""/>
										<a href="" class="mainlink limitAccess" onclick="event.preventDefault(); editCreditNoteItem(this);">
											<span class="finalCost"></span>
										</a>
									</td>
									<td class="del"></td>
						</tr>
					</tbody>
				</table>
				<table class="default4 invoiceItemList" summary="<spring:message code="creditnote.tablecostingtotalvalue" />">
					<tbody>
						<tr class="nobord">											
							<td class="totalsum cost2pcTotal bold"><spring:message code="creditnote.value" />:</td>
							<td class="totalamount cost2pcTotal bold">
								<span id="creditNoteTotalCost"><fmt:formatNumber value="${creditNote.totalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
							</td>
							<td class="filler">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<table id="taxtable" class="default4 invoiceItemList" summary="<spring:message code="creditnote.tableinvoicevatvalue" />">
					<tbody>
						<c:choose>
							<c:when test="${not empty creditNote.taxes}">
								<c:forEach var="tax" items="${creditNote.taxes}">
									<tr class="nobord">											
										<td class="totalsum cost2pcTotal bold"><spring:message code="tax"/>&nbsp;${tax.description} (<fmt:formatNumber value="${tax.rate.multiply(100)}" type="NUMBER" pattern="0.00"/>%):</td>
										<td class="totalamount cost2pcTotal bold">
											<span id="creditNoteVatValue"><fmt:formatNumber value="${tax.tax}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
										</td>
										<td class="filler">&nbsp;</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr class="nobord">											
									<td class="totalsum cost2pcTotal bold">(<fmt:formatNumber value="${creditNote.vatRate}" type="NUMBER" pattern="0.00"/>%) <spring:message code="vat" />:</td>
									<td class="totalamount cost2pcTotal bold">
										<span id="creditNoteVatValue"><fmt:formatNumber value="${creditNote.vatValue}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
									</td>
									<td class="filler">&nbsp;</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<table class="default4 invoiceItemList" summary="<spring:message code="creditnote.tableinvoicetotalvalue" />">
					<tbody>
						<tr class="nobord">											
							<td class="totalsum cost2pcTotal bold"><spring:message code="creditnote.nettotal" />:</td>
							<td class="totalamount cost2pcTotal bold">
								<span id="creditNoteFinalCost"><fmt:formatNumber value="${creditNote.finalCost}" type="currency" currencySymbol="${currency.currencyERSymbol}"/></span>
							</td>
							<td class="filler">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<div id="addEditCreditNoteItemOverlay" style="display: none;">
					<form:form id="creditNoteItemForm" modelAttribute="creditNote">
						<input id="creditNoteId" type="hidden" value="${creditNote.id}"/>
						<input id="itemId" type="hidden"/>
						<fieldset>
							<ol>
								<li>
									<label><spring:message code="createdelnote.coldesc"/></label>
									<input id="description"/>
								</li>
								<li>
									<label><spring:message code="creditnote.nominal"/></label>
									<select id="nominal">
										<c:forEach var="nominal" items="${nominals}">
											<option value="${nominal.id}">${nominal.code} - <cwms:besttranslation translations="${nominal.titleTranslations}"/></option>
										</c:forEach>
									</select>
								</li>
								<li>
									<label><spring:message code="businesssubdivision"/></label>
									<select id="itemSubdiv">
										<c:forEach var="subdiv" items="${businessSubdivs}">
											<option value="${subdiv.subdivid}">${subdiv.subdivCode} - ${subdiv.subname}</option>
										</c:forEach>
									</select>
								</li>
								<li>
									<label><spring:message code="creditnote.amount"/></label>
									<input type="number" step="0.01" id="totalCost" />
								</li>
							</ol>
						</fieldset>
					</form:form>
				</div>
			</div>
			<!-- end of div containing all credit note items -->
			<!-- begin the actions history tab -->
			<div id="actions-tab" class="hid">
				<table class="default4" title="this table displays all actions that have been completed on a credit note">
					<thead>
						<tr>
							<td colspan="4"><spring:message code="creditnote.actionscompletedoncreditnote" /> (${creditNote.actions.size()})</td>
						</tr>
						<tr>
							<th scope="col"><spring:message code="creditnote.activity" /></th>
							<th scope="col"><spring:message code="creditnote.outcome" /></th>
							<th scope="col"><spring:message code="creditnote.peformedby" /></th>
							<th scope="col"><spring:message code="creditnote.performedon" /></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="action" items="${creditNote.actions}">
							<tr>
								<td><t:showTranslationOrDefault translations="${action.activity.descriptiontranslations}" defaultLocale="${defaultlocale}"/></td>
								<td><t:showTranslationOrDefault translations="${action.outcomeStatus.descriptiontranslations}" defaultLocale="${defaultlocale}"/></td>
								<td>${action.contact.name}</td>
								<td><fmt:formatDate type="date" dateStyle="SHORT" value="${action.date}"/></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<!-- end the actions history tab -->
			<!-- this section displays all emails for the selected job -->
			<div id="email-tab" class="hid">
				<t:showEmails entity="${creditNote}" entityId="${creditNote.id}" entityName="Credit Note" sc="${sc}"/>
			</div>
			<!-- end of email section -->
			<!-- this section displays all order files created -->
			<div id="cndocs-tab" class="hid">
				<spring:message var="filesText" code="creditnote.filesforcreditnote" />
				<files:showFilesForSC rootFiles="${scRootFiles}" id="${creditNote.id}" 
						allowEmail="true" entity="${creditNote}" isEmailPlugin="false" 
						ver="" identifier="${creditNote.creditNoteNo}" 
						deleteFiles="true" sc="${sc}" 
						rootTitle="${filesText} ${creditNote.creditNoteNo}" />
			</div>
		</div>
		<!-- this div clears floats and restores page flow -->
		<div class="clear">&nbsp;</div>
		<!-- this section contains all credit note notes -->									
		<t:showTabbedNotes entity="${creditNote}" privateOnlyNotes="${privateOnlyNotes}" noteTypeId="${creditNote.id}" noteType="CREDITNOTENOTE"/>
		<!-- end of credit note notes section -->
	</jsp:body>
</t:crocodileTemplate>