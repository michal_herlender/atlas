<%-- File name: /trescal/core/pricing/invoice/newcreateinvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="invoice.preparedinvoice" /></span>
	</jsp:attribute>
		<jsp:attribute name="scriptPart">
		<script>
			function updateInstructionSize(instructionSize) {}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
	</jsp:attribute>
	<jsp:body>
	
		<div class="infobox">
			<cwms-instructions link-coid="${company.coid}" instruction-types="INVOICE,DISCOUNT" show-mode="TABLE"></cwms-instructions>
		</div>
		
		<div class="infobox">
			<fieldset>
				<ol>
					<li>
						<t:showCompanyInvoiceRequirements companyname="${company.coname}"
							monthlyInvoice="${monthlyinvoicesystemdefault}"
							separateCostsInvoice="${separatecostsinvoicesystemdefault}"
							poInvoice="${poinvoicesystemdefault}" />
					</li>
					<li>
						<label class="labelhead width80">
							<spring:message code="invoice.proposedinvoicesweresuggested" arguments="1" />
						</label>
						<span>&nbsp;</span>
					</li>
				</ol>
			</fieldset>
		</div>
		
		<form:form modelAttribute="createinvoiceform" method="post">
			<div class="infobox" id="prepInvoice">
				<fieldset>
					<legend><spring:message code="invoice.invoice"/>: 1</legend>
					<ol>
						<li>
							<label><spring:message code="company"/>:</label>
							<links:companyLinkInfo companyId="${company.coid}" companyName="${company.coname}" active="${company.active}" onStop="${company.onStop}" rowcount="0" copy="true"/>
						</li>
						<li>
							<label><spring:message code="jobcost.costingtype"/>:</label>
							<spring:message code="${pricingType.messageCode}" />
						</li>
						<li>
							<label><spring:message code="viewjob.invoicetype"/>:</label>
							<form:select path="invoiceTypeId" items="${invoiceTypes}" itemValue="key" itemLabel="value"/>
						</li>
						<li>
							<label><spring:message code="address"/>:</label>
							<form:select path="addressId" items="${addresses}" itemValue="key" itemLabel="value"/>
						</li>
						<li>
							<table id="invoice-1" class="default4 proposedInvoice" summary="<spring:message code='invoice.tableinvdetails' />">
								<thead>
									<tr>										
										<th rowspan="2" class="include" scope="col"><input type="checkbox" checked="checked" onClick=" toggleAllForInvoice(this.checked, '${invoiceLoopStatus.index}'); "></th>
										<th rowspan="1" class="job" scope="col"><spring:message code="job" /></th>
										<th rowspan="1" class="item" scope="col"><spring:message code="item" /></th>
										<th rowspan="1" class="jobstatus" scope="col"><spring:message code="jobstatus" /></th>
										<th rowspan="1" class="ponos" scope="col"><spring:message code="invoice.ponos" /></th>
										<th rowspan="1" class="cal cost2pc" scope="col"><spring:message code="calibration" /></th>
										<th rowspan="1" class="rep cost2pc" scope="col"><spring:message code="invoice.repair" /></th>
										<th rowspan="1" class="adj cost2pc" scope="col"><spring:message code="invoice.adjustment" /></th>
										<th rowspan="1" class="sales cost2pc" scope="col"><spring:message code="invoice.sales" /></th>
										<th rowspan="1" class="inv" scope="col"><spring:message code="invoice.invoiced" /></th>
									</tr>
									<tr>
										<th colspan="3"><spring:message code="docs.deliverynote" /></th>
										<th><spring:message code="plantno" /></th>
										<th colspan="4">
											<spring:message code="costsource"/>
											<span>
												(
												<a href="" class="mainlink" onclick="event.preventDefault(); showCostSources(); "><spring:message code="invoice.showsources" /></a> |
												<a href="" class="mainlink" onclick="event.preventDefault(); hideCostSources(); "><spring:message code="invoice.hidesources" /></a>
												)
											</span>
										</th>
										<th><spring:message code="invoice.unsettledpos" /></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="item" items="${createinvoiceform.jobItems}" varStatus="itemCounter">
										<c:set var="itemId" value="invoice${1}item${itemCounter.index}" />
										<tr subdivid="${item.subdivId}" firstRow>
											<td rowspan="2">
												<form:checkbox path="ids" value="${item.id}"/>
											</td>
											<td>
												<links:jobnoLinkDWRInfo rowcount="0" jobno="${item.jobNo}" jobid="${item.jobId}" copy="true" />
											</td>
											<td>
												<links:jobitemLinkInfo defaultPage="jiactions.htm" jobItemNo="${item.itemNo}" jobItemCount="${item.itemCount}" jobItemId="${item.id}"/>
											</td>
											<td>
												${item.jobStatus}
											</td>
											<td>
												${item.poNumbers}
											</td>
											<td class="cost2pc">
												<fmt:formatNumber type="currency" value="${item.costingCalibrationCost}" currencySymbol="${item.costingCurrencySymbol}"/>
											</td>
											<td class="cost2pc">
												<fmt:formatNumber type="currency" value="${item.costingRepairCost}" currencySymbol="${item.costingCurrencySymbol}"/>
											</td>
											<td class="cost2pc">
												<fmt:formatNumber type="currency" value="${item.costingAdjustmentCost}" currencySymbol="${item.costingCurrencySymbol}"/>
											</td>
											<td class="cost2pc">
												<fmt:formatNumber type="currency" value="${item.costingPurchaseCost}" currencySymbol="${item.costingCurrencySymbol}"/>
											</td>
											<td>
												<c:choose>
													<c:when test="${item.invoiceCount > 0}">
														<c:set var="classExistingInvoices" value="attention bold" />
													</c:when>
													<c:otherwise>
														<c:set var="classExistingInvoices" value="" />
													</c:otherwise>
												</c:choose>
												<span class="${classExistingInvoices}">
													<spring:message code="invoice.invoices1" arguments="${item.invoiceCount}" />
												</span>
												
											</td>
										</tr>
										<tr subdivid="${item.subdivId}" secondRow>
											<td colspan="3">
												Blabla
											</td>
											<td>
												${item.plantNo}
											</td>
											<td colspan="4"></td>
											<td>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</li>
					</ol>
				</fieldset>
			</div>
			<input type="submit"/>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>