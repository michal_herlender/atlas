<%-- File name: /trescal/core/pricing/creditnote/viewcreditnote.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">
			<spring:message code="creditnote.editcreditnote" />
			${creditNote.creditNoteNo}
		</span>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="true"/>
	
		<form:form modelAttribute="command" action="" method="post">
			<fieldset>
				<legend>
					<spring:message code="creditnote.editcreditnote" />
					<a href="viewcreditnote.htm?id=${creditNote.id}" class="mainlink">${creditNote.creditNoteNo}</a>
				</legend>
				<ol>
					<li>
						<label for="vatcode">
							<spring:message code="company.vatrate"/>:
						</label>
						<div class="symbolbox"></div>
						<div class="float-left">
							<form:select path="vatCode" id="vatCode">
								<c:forEach var="vatGroup" items="${vatRates.keySet()}">
									<optgroup label="${vatGroup}">
										<form:options items="${vatRates.get(vatGroup)}" itemLabel="value" itemValue="key" /> 
									</optgroup>
								</c:forEach>
							</form:select>
						</div>
						<form:errors path="vatCode" cssClass="attention"/>
						<div class="clear"></div>
					</li>
					<li>
						<label>&nbsp;</label>
						<div class="symbolbox"></div>
						<input type="submit" name="update" value="<spring:message code="update" />" />
					</li>
				</ol>
			</fieldset>
		</form:form>	
	</jsp:body>
</t:crocodileTemplate>	
