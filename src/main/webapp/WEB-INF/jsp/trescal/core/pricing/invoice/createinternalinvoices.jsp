<%-- File name: /trescal/core/pricing/invoice/createinternalinvoices.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="invoice.createinternalinvoices" /></span>
	</jsp:attribute>
	<jsp:body>
		<form:form modelAttribute="dates">
			<spring:message code='from'/>&nbsp;&nbsp;<form:input type="date" path="begin"/>&nbsp;
			<spring:message code='to'/>&nbsp;&nbsp;<form:input type="date" path="end"/>
			<input type="submit" value="<spring:message code='submit'/>"/>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>