<%-- File name: /trescal/core/pricing/purchaseorder/viewpurchaseorder.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.trescal.cwms.core.account.entity.AccountStatus" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="files" tagdir="/WEB-INF/tags/files" %>
<%@ taglib prefix="purchaseorder" tagdir="/WEB-INF/tags/purchaseorder" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="purchaseorder.viewpurchaseorder" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			/* translations used int the js script */
			var popupText1 = String("<spring:message code='purchaseorder.copycancel.popup1.text' />").split(',');
			var popupText2 =String("<spring:message code='purchaseorder.copycancel.popup2.text' />").split(',');
			
			function updateInstructionSize(instructionSize) {
				$j("span.instructionSize").text(instructionSize);
			}
		</script>
		<script type="module" src="script/components/cwms-instructions/cwms-instructions.js"></script>
		<script type='text/javascript' src='script/trescal/core/pricing/purchaseorder/ViewPurchaseOrder.js'></script>
		<script type='text/javascript' src='script/thirdparty/jQuery/jquery.js'></script>
		<script type='module' src='script/components/cwms-upload-file/cwms-upload-file.js'></script>
	</jsp:attribute>
	<jsp:attribute name="globalElements">
		<cwms-upload-file system-component-id="${sc.componentId}" entity-id1="${order.pono}" entity-id2=""
 			files-name="${filesName}" rename-to="" web="false"></cwms-upload-file>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="command.*" showFieldErrors="false" />	
		<c:choose>
			<c:when test="${not empty itemNos && itemNos.toArray()[0] == 0}">
				<div class="successBox1">
					<div class="successBox2">
						<div class="successBox3">
							<div class="text-center"><spring:message code="purchaseorder.infomessage"/></div>
						</div>
					</div>
				</div>
			</c:when>
			<c:when test ="${not empty itemNos}">
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<h5 class="center-0 attention"><spring:message code="purchaseorder.warningmessage"/>
								<c:forEach var="itemno" items="${itemNos}">
									<strong> ${itemno} </strong>,
								</c:forEach>
							</h5>
						</div>
					</div>
				</div>
			</c:when>
		</c:choose>
		
		<!-- infobox contains all purchase order information and is styled with nifty corners -->
		<div class="infobox" id="viewPOInfo">
		 
			<fieldset>
			
				<legend><spring:message code="createpurchaseorder.purchaseorder"/>: ${order.pono}</legend>
				
				<!-- displays content in column 49% wide (left) -->
				<div class="displaycolumn">
				
					<ol>
						<li>
							<label><spring:message code="fromcompany"/>:</label>
							<span>${order.organisation.coname}</span>
						</li>
						<li>
							<label><spring:message code="tocompany"/>:</label>
							<span><links:companyLinkDWRInfo company="${order.contact.sub.comp}" rowcount="0" copy="true" /></span>
						</li>
						<li>
							<label><spring:message code="subdivision"/>:</label>
							<span><links:subdivLinkDWRInfo rowcount="0" subdiv="${order.contact.sub}"/></span>
						</li>
						<li>
							<label><spring:message code="contact"/>:</label>
							<span><links:contactLinkDWRInfo contact="${order.contact}" rowcount="0"/></span>
						</li>
						<li>
							<label><spring:message code="address"/>:</label>
							<div class="float-left padtop">
								<t:showFullAddress address="${order.address}" separator="<br/>" copy="true"/>
							</div>
							<div class="float-left padtop">
								&nbsp;&nbsp;-&nbsp;&nbsp;<a class="mainlink" href="<c:url value='/viewaddress.htm?addrid=${order.address.addrid}'/>"><spring:message code="address.viewdetails"/></a>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="job"/>:</label>
							<span>
								<c:choose>
									<c:when test="${not empty order.job}">
										<links:jobnoLinkDWRInfo rowcount="0" jobno="${order.job.jobno}" jobid="${order.job.jobid}" copy="true"/>
									</c:when>
									<c:otherwise>
										${order.jobno}
									</c:otherwise>
								</c:choose>
							</span>
                        </li>
                        <li>
                            <label><spring:message code="currency"/>:</label>
                            <span>
								<c:set var="currency" value="${order.currency}"/>
								${currency.currencyCode}
								(${currency.currencySymbol} ${currency.currencyName}
								- ${defaultCurrency.currencyERSymbol}1 : ${currency.currencyERSymbol}${order.rate})
							</span>
                        </li>
                        <li>
                            <label><spring:message code="purchaseorder.poFrom" text="PO From"/></label>
                            <cwms:render-PO-origin origin="${order.poOrigin}"/>
                        </li>
						<c:if test="${useTaxableOption}">
							<li>
								<label><spring:message code="createpurchaseorder.taxable"/></label>
								${order.taxable.message}
							</li>
						</c:if>
                    </ol>

                </div>
                <!-- end of left column -->

                <!-- displays content in column 49% wide (right) -->
                <div class="displaycolumn">

                    <ol>
                        <li>
							<label><spring:message code="createdon"/>:</label>
							<span><fmt:formatDate type="date" dateStyle="SHORT" value="${order.regdate}"/></span>
						</li>
						<li>
							<label><spring:message code="createdby"/>:</label>
							<span>${order.createdBy.name}</span>
						</li>
						<li>
							<label><spring:message code="purchaseorder.issuedate"/>:</label>
							<span>
								<fmt:formatDate type="date" dateStyle="SHORT" value="${order.issuedate}"/>
							</span>
						</li>
						<li>
							<label><spring:message code="purchaseorder.businesscontact"/>:</label>
							<span>
								<links:contactLinkDWRInfo contact="${order.businessContact}" rowcount="0"/>
							</span>
						</li>
						<li>
							<label><spring:message code="createpurchaseorder.returnaddress"/>:</label>
							<div class="float-left padtop">
								<t:showFullAddress address="${order.returnToAddress}" separator="<br/>" copy="true"/>
							</div>
							<div class="float-left padtop">
								&nbsp;&nbsp;-&nbsp;&nbsp;<a class="mainlink" href="<c:url value='/viewaddress.htm?addrid=${order.returnToAddress.addrid}'/>"><spring:message code="address.viewdetails"/></a>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label><spring:message code="purchaseorder.supplierref"/>:</label>
							<span><t:fill field="${order.clientref}" /></span>
						</li>
						<li>
							<label><spring:message code="status"/>:</label>
							<div class="fleftTooltip width60">
								<cwms:besttranslation translations="${order.status.descriptiontranslations}" />
								<c:if test="${not empty order.status.next}">
									<%-- this should only ever be an accounts department --%>													
									<c:choose>
										<c:when test="${!isWellAllocated}">
													<br />
													<strong>
														<spring:message code="purchaseorder.statuserrorcomp"/>
													</strong>
												</c:when>
										<c:when test="${!isAllowToPerformStatusChange}">
										<br />
											<strong>
												<spring:message code="purchaseorder.statuspermissionneeded"/> 
											</strong>
										</c:when>
										<c:when test='${order.status.next.name=="pre-issued" && !isUnderLimit}'>
													<br />
													<strong>
														<spring:message code="purchaseorder.isunderlimitorhasnopermission"/> 
													</strong>
												</c:when>
										<c:when test='${empty order.status.next.deptMembershipRequired }'>
											<a href="<c:url value='/viewpurchaseorder.htm?id=${order.id}&submission=submit'/>" class="mainlink ">
												<spring:message code="purchaseorder.updatestatusto"/> '<cwms:besttranslation translations="${order.status.next.nametranslations}" />'
											</a>
										</c:when>
										
										<c:otherwise>
											<c:choose>
												<c:when test='${(order.status.next.deptMembershipRequired == "ACCOUNTS") && accountsAuthenticated }'>
													<br />
													<c:choose>
														<c:when test="${missingNominals.size() > 0}">
															<img src="img/icons/flag_red.png" />
															&nbsp;&nbsp;
															<i><spring:message code="purchaseorder.fixnominaldiscrepancybeforeupdatingstatus"/></i>
														</c:when>
														<c:otherwise>
															<a id="hrefStatusChange" href="<c:url value='/viewpurchaseorder.htm?id=${order.id}&submission=submit' />" class="mainlink">
																<spring:message code="purchaseorder.updatestatusto"/> '<cwms:besttranslation translations="${order.status.next.nametranslations}" />'
															</a>
															
														</c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>
													<br />
													<strong><spring:message code="purchaseorder.youmustbeamemberoftheaccountsdepartmenttoperformthisactivity"/></strong>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:if>
							</div>
						
							<!-- clear floats and restore page flow -->
							<div class="clear"></div>
						</li>
							<li>
								<label><spring:message code="purchaseorder.accountsstatus"/>:</label>
								<div class="float-left padtop width60" id="accountstatus">
									<c:choose>
										<c:when test="${accountssoftwaresupported && empty order.contact.sub.comp.account}">
											<spring:message code="purchaseorder.noaccountsprofilehasbeensetupfor"/> ${order.contact.sub.comp.coname}.
											<spring:message code="purchaseorder.oncea"/> 
											<a href="<c:url value='/viewcomp.htm?coid=${order.contact.sub.comp.coid}&loadtab=accounts-tab' />" class="mainlink">
												<spring:message code="purchaseorder.profilehasbeencreated"/>
											</a>
											<spring:message code="purchaseorder.brokeringcanbeprocessed"/>.
										</c:when>
										<c:when test="${accountssoftwaresupported && ((companySettings == null) || (companySettings.status != 'VERIFIED'))}">
											<spring:message code="companystatusnotverified" />.
											<a href="viewcomp.htm?coid=${invoice.comp.coid}&loadtab=finance-tab">
												<spring:message code="verifycompanytocontinue" />
											</a>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${order.accountsStatus == 'P'}">
													<img src="img/icons/sync_pending.png" width="14" height="14" alt="<spring:message code='purchaseorder.pendingsynchronisation' />" title="<spring:message code='purchaseorder.pendingsynchronisation' />" class="image_inl" />
													<spring:message code="purchaseorder.pendingsynchronisationwithaccountingsoftware"/>
												</c:when>
												<c:when test="${order.accountsStatus == 'E'}">
													<img src="img/icons/sync_error.png" width="14" height="14" alt="<spring:message code='purchaseorder.errorsynchronising' />" title="<spring:message code='purchaseorder.errorsynchronising' />" class="image_inl" />
													<spring:message code="purchaseorder.errorssynchronisingwithaccountingsoftware"/> 
													<a href="#" onclick="event.preventDefault(); resetOrderStatus(${order.id}, '${accountssoftware}'); " class="mainlink"><spring:message code="purchaseorder.reset"/></a>
												</c:when>
												<c:when test="${order.accountsStatus == 'S'}">
													<img src="img/icons/sync.png" width="14" height="14" alt="<spring:message code='purchaseorder.accountsynchronised' />" title="<spring:message code='purchaseorder.accountsynchronised' />" class="image_inl" />
													<spring:message code="purchaseorder.ordersynchronisedwithaccountingsoftware"/>
												</c:when>
												<c:when test="${order.status.onCancel}">
													<spring:message code="purchaseorder.cancellednosynchronisation"/>
												</c:when>
												<c:otherwise>
													<spring:message code="purchaseorder.nobrokerstatusdataavailableordermaynotbecomplete"/>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="clear"></div>
							</li>
						</ol>														
				</div>
				<!-- end of right column -->
																
			</fieldset>
			
		</div>
		<!-- end of purchase order information div -->
		<c:if test='${order.status.next.name=="pre-issued"}'>

		<div class="infobox">
			<table class="default4" >
				<thead>
					<tr>
						<td colspan="3">
						<spring:message code="purchaseorder.userLimitType"/> :  
					</td>
					</tr>
					<tr>
						<th>
							CAPEX
						</th>
						<th>
							OPEX
						</th>
						<th>
							GENEX
						</th>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<c:choose >
								<c:when test="${limitPo.poLimitConfig.capexLimitless}">
									<spring:message code="purchaseorder.limitless" />
								</c:when>
								<c:otherwise>
									${limitPo.currency }${limitPo.poLimitConfig.capexRightMax}
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose >
								<c:when test="${limitPo.poLimitConfig.opexLimitless}">
									<spring:message code="purchaseorder.limitless" />
								</c:when>
								<c:otherwise>
									${limitPo.currency } ${limitPo.poLimitConfig.opexRightMax}
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose >
								<c:when test="${limitPo.poLimitConfig.genexLimitless}">
									<spring:message code="purchaseorder.limitless" />
								</c:when>
								<c:otherwise>
									${limitPo.currency } ${limitPo.poLimitConfig.genexRightMax}
								</c:otherwise>
							</c:choose>
						</td>	
					</tr>
				</tbody>
			</table>
		
		</div>
	
		</c:if>
		
		<spring:message var="alertOperationPrevented" code="purchaseorder.alertnotallowed" javaScriptEscape="true" />
		<spring:message var="alertNotIssued" code="purchaseorder.alertnotissued" javaScriptEscape="true" />
		<!-- sub navigation menu which displays and hides sections of page as well as linking to external pages -->
		<div id="subnav">
			<dl>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'items-tab', false); " id="items-link"  title="<spring:message code='purchaseorder.viewpurchaseorderitems' />" class="selected">
						<spring:message code="purchaseorder.poitems"/> (<span class="poItemSize">${order.items.size()}</span>)
					</a>
				</dt> 
				<dt>
					<c:choose>
						<c:when test="${editable}">
							<a href="<c:url value='/editpurchaseorderitem.htm?poid=${order.id}' />" title="<spring:message code='purchaseorder.additemtopurchaseorder' />">
								<spring:message code="purchaseorder.newitem"/>
							</a>
						</c:when>
						<c:otherwise>
							<a href="#" onclick=" event.preventDefault(); $j.prompt('${alertOperationPrevented}');" class="disabled" title="<spring:message code='purchaseorder.additemtopurchaseorder' />">
								<spring:message code="purchaseorder.newitem"/>
							</a>
						</c:otherwise>
					</c:choose>
				</dt>
				<dt>
					<c:choose>
						<c:when test="${editable}">
							<c:set var="linkScript" value="switchMenuFocus(menuElements, 'edit-tab', false);" />
							<c:set var="linkClass" value="" />
						</c:when>
						<c:otherwise>
							<c:set var="linkScript" value="$j.prompt('${alertOperationPrevented}');" />
							<c:set var="linkClass" value="disabled" />
						</c:otherwise>
					</c:choose>
					<a href="#" onclick=" event.preventDefault(); ${linkScript}" class="${linkClass}" id="edit-link" title="<spring:message code='purchaseorder.editpurchaseorderdetails' />">
						<spring:message code="edit"/>
					</a>
				</dt>
				<dt>
					<c:choose>
						<c:when test="${!order.status.issued}">
							<c:set var="accountsScript" value="$j.prompt('${alertNotIssued}'); " />
							<c:set var="goodsScript" value="$j.prompt('${alertNotIssued}'); " />
							<c:set var="linkClass" value="disabled" />
							<c:set var="goodslink" value="goods-link" />
							<c:set var="accountslink" value="accounts-link" />
						</c:when>
						<c:when test="${order.status.issued && !order.status.onComplete && !order.status.onCancel}">
							<c:set var="accountsScript" value="switchMenuFocus(menuElements, 'accounts-tab', false); " />
							<c:set var="goodsScript" value="switchMenuFocus(menuElements, 'goods-tab', false); " />
							<c:set var="linkClass" value="" />
							<c:set var="goodslink" value="goods-link" />
							<c:set var="accountslink" value="accounts-link" />
						</c:when>
						<c:when test="${order.status.onComplete}">
							<c:set var="accountsScript" value="switchMenuFocus(menuElements, 'accountsread-tab', false); " />
							<c:set var="goodsScript" value="switchMenuFocus(menuElements, 'goodsread-tab', false); " />
							<c:set var="linkClass" value="" />
							<c:set var="goodslink" value="goodsread-link" />
							<c:set var="accountslink" value="accountsread-link" />
						</c:when>						
						<c:otherwise>
							<c:set var="accountsScript" value="$j.prompt('${alertOperationPrevented}');" />
							<c:set var="goodsScript" value="$j.prompt('${alertOperationPrevented}');" />
							<c:set var="linkClass" value="disabled" />
							<c:set var="goodslink" value="goods-link" />
							<c:set var="accountslink" value="accounts-link" />
						</c:otherwise>
					</c:choose>
					<a href="#" onclick=" event.preventDefault(); ${goodsScript}" class="${linkClass}" id="${goodslink}" title="<spring:message code='purchaseorder.goodsapproval' />">
						<spring:message code="purchaseorder.goodsapproval"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); ${accountsScript}" class="${linkClass}" id="accounts-link" title="<spring:message code='purchaseorder.accountsapproval' />">
						<spring:message code="purchaseorder.accountsapproval"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'nominals-tab', false); " id="nominals-link" title="<spring:message code='purchaseorder.displayspurchaseorderitemswithmissingnominals' />">
						<spring:message code="purchaseorder.nominals" />
						 <c:if test="${missingNominals.size() > 0}">
						 	<img src="img/icons/flag_red.png" />
						</c:if>
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'files-tab', false); " id="files-link" title="<spring:message code='purchaseorder.viewdocumentsinpurchaseorderdirectory' />">
						<spring:message code="purchaseorder.documents" /> (<span class="fileResourceCount">${scRootFiles.numberOfFiles}</span>)
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'email-tab', false); " id="email-link" title="<spring:message code='purchaseorder.viewsendemailsforpurchaseorder' />">
						<spring:message code="purchaseorder.emails" /> (<span class="entityEmailDisplaySize">${order.sentEmails.size()}</span>)
					</a>
				</dt>
				<dt>
					<c:choose>
						<c:when test="${deletable}">
							<c:set var="linkScript" value="showDeletePOContent(${order.id}); " />
							<c:set var="linkClass" value="" />
						</c:when>						
						<c:otherwise>
							<c:set var="linkScript" value="$j.prompt('${alertOperationPrevented}');" />
							<c:set var="linkClass" value="disabled" />
						</c:otherwise>
					</c:choose>
					<a href="#" onclick=" event.preventDefault(); ${linkScript}" class="${linkClass}" title="<spring:message code='purchaseorder.deletepurchaseorder' />">
						<spring:message code="delete"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); genDocPopup(${order.id}, 'purchaseorderbirtdocument.htm?id=', 'Generating Purchase Order Document'); "  title="<spring:message code='purchaseorder.generatepurchaseorderdocument' />" >
						<spring:message code="generate"/>
					</a>
				</dt>
			</dl>
		</div>
		<div id="subnav">
			<dl>
				<dt>
					<a href="#" onclick=" event.preventDefault(); genDocPopup(${order.id}, 'purchaseorderdocument.htm?id=', 'Generating Purchase Order Document'); "  title="<spring:message code='purchaseorder.generatepurchaseorderdocument' />" >
						<spring:message code="generateold"/>
					</a>
				</dt>
				<dt>
					<c:set var="copyCancelScript" value="event.preventDefault();" />
					<c:if test="${order.status.issued}">
						<c:set var="copyCancelScript" value=" event.preventDefault(); copyCancelPopUp(popupText1,popupText2,${order.id});" />
					</c:if>
					<a href="#" onclick=" ${copyCancelScript} " class="${!order.status.issued?'disabled':''}"
						title="<spring:message code='purchaseorder.copycancel.title' />">
						<spring:message code="purchaseorder.copycancel"/>
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'actions-tab', false); " id="actions-link" title="<spring:message code='purchaseorder.viewpurchaseorderactionhistory' />">
						<spring:message code="purchaseorder.actions"/> (${order.actions.size()})
					</a>
				</dt>
				<dt>
					<a href="#" onclick=" event.preventDefault(); switchMenuFocus(menuElements, 'instructions-tab', false); " id="instructions-link" title="<spring:message code="instructions" />">
						<spring:message code="instructions" />(<span class="instructionSize">${instructionSize}</span>)
					</a>
				</dt>
			</dl>
		</div>
		<!-- end of sub navigation menu -->
		
		<!-- infobox contains all purchase order functionality and is styled with nifty corners -->
		<div class="infobox">
												
			<c:set var="currency" value="${order.currency}"/>
			<div id="items-tab" class="viewPOItems">
				
				<table class="default4" summary="This table displays all purchase order items">
					<thead>
						<tr>
							<td colspan="10"><spring:message code="purchaseorder.purchaseorderitems" /> (<span class="poItemSize">${order.items.size()}</span>)</td>
						</tr>
						<tr>
							<th class="poitem" scope="col"><spring:message code="item"/></th>
							<th class="podesc" scope="col"><spring:message code="description"/></th>
							<th class="notes" scope="col">														
								<a href="#" onclick=" event.preventDefault(); toggleNoteDisplay(this, 'PURCHASEORDERITEMNOTE', 'vis', null); " class="toggleNoteDisplay">
									<img src="img/icons/note_expand.png" width="20" height="16" alt="<spring:message code='notes.expandallnotes' />" title="<spring:message code='notes.expandallnotes' />" />
								</a>													
							</th> 
							<th class="podeldate" scope="col"><spring:message code="purchaseorder.requireddate"/></th>
							<th class="poqty" scope="col"><spring:message code="purchaseorder.qty"/></th>
							<th class="pocostfor" scope="col"><spring:message code="purchaseorder.costfor"/></th>
							<th class="pocost cost" scope="col"><spring:message code="cost"/></th>
							<th class="podisc cost" scope="col"><spring:message code="purchaseorder.discount"/></th>
							<th class="pofcost cost" scope="col"><spring:message code="purchaseorder.finalcost"/></th>
							<th class="podel" scope="col"><spring:message code="delete"/></th>
						</tr>
					</thead>
				</table>
				
				<c:choose>
					<c:when test="${order.items.size() > 0}">
					
						<%-- TODO refactor using varStatus --%>
						<c:set var="rowcount" value="0"/>
						<c:forEach var="item" items="${order.items}">
							<c:set var="poinotecount" value="${item.publicActiveNoteCount + item.privateActiveNoteCount}" />
							<table class="default4" id="poitem${item.id}">
								<tbody>
									<c:set var="rowclass" value="odd"/>
									<c:if test="${rowcount % 2 == 0}">
										<c:set var="rowclass" value="even"/>	
									</c:if>
									
									<tr id="item${item.id}" class="${rowclass}">
										<td class="poitem">
											<c:choose>
												<c:when test="${editable}">
													<a href="<c:url value='/editpurchaseorderitem.htm?poiid=${item.id}' />" class="mainlink">${item.itemno}</a>
												</c:when>
												<c:otherwise>
													${item.itemno}
												</c:otherwise>
											</c:choose>
										</td>
										<td class="podesc">${item.description}</td>
										<td class="notes"><links:showNotesLinks notecounter="${poinotecount}" noteType="PURCHASEORDERITEMNOTE" noteTypeId="${item.id}" colspan="10" /></td>
										<td class="podeldate"><fmt:formatDate type="date" dateStyle="SHORT" value="${item.deliveryDate}" /></td>
										<td class="poqty">${item.quantity}</td>
										<td class="pocostfor">
											<c:if test="${not empty item.costType}">
												<spring:message code="${item.costType.messageCode}" />
											</c:if>
										</td>
										<td class="pocost cost">
											${currency.currencyERSymbol}${item.totalCost}
										</td>
										<td class="podisc cost">${item.generalDiscountRate}%</td>
										<td class="pofcost cost">
											<c:choose>
												<c:when test="${editable}">
													<a href="<c:url value='/editpurchaseorderitem.htm?poiid=${item.id}' />" class="mainlink">
														${currency.currencyERSymbol}${item.finalCost}
													</a>
												</c:when>
												<c:otherwise>
													${currency.currencyERSymbol}${item.finalCost}
												</c:otherwise>
											</c:choose>
										</td>
										<td class="podel">
											<c:if test="${editable}">
												<a href="#" onclick=" deleteItem(${item.id}); return false; ">
													<img src="img/icons/delete.png" width="16" height="16" alt="<spring:message code='purchaseorder.deletepurchaseorderitem' />" title="<spring:message code='purchaseorder.deletepurchaseorderitem' />" />
												</a>
											</c:if>
										</td>
									</tr>
									<c:if test="${poinotecount > 0}">
										<tr id="PURCHASEORDERITEMNOTE${item.id}" class="hid">
											<td colspan="10" class="nopadding">
												<t:showActiveNotes entity="${item}" links="true" noteType="PURCHASEORDERITEMNOTE" noteTypeId="${item.id}" privateOnlyNotes="${privateOnlyNotes}" contact="${currentContact}"/>										
											</td>
										</tr>
									</c:if>
									<c:if test="${item.linkedJobItems != null && item.linkedJobItems.size() > 0}">
										<c:forEach var="lj" items="${item.linkedJobItems}">
											<c:set var="ji" value="${lj.ji}"/>
											<tr class="${rowclass}">
												<td class="center">
													<img src="img/icons/link.png" width="16" height="16" alt="<spring:message code='purchaseorder.linkedjobitem' />" title="<spring:message code='purchaseorder.linkedjobitem' />" />
												</td>
												<td colspan="4">
													<spring:message code="item"/> <links:jobitemLinkDWRInfo jobitem="${ji}" jicount="false" rowcount="0" /> 
													<spring:message code="purchaseorder.onjob"/> 
													<links:jobnoLinkDWRInfo rowcount="0" jobno="${ji.job.jobno}" jobid="${ji.job.jobid}" copy="false" />
													<links:instrumentLinkDWRInfo instrument="${ji.inst}" rowcount="0" displayBarcode="true" displayName="true" displayCalTimescale="false" caltypeid="${ji.calType.calTypeId}" />
												</td>
												<td colspan="5">
													<strong><spring:message code="businesssubdivision" />:</strong>
													<c:choose>
														<c:when test="${not empty item.businessSubdiv}">
															${item.businessSubdiv.subdivCode} - ${item.businessSubdiv.subname}
														</c:when>
														<c:otherwise>
															<spring:message code="purchaseorder.noneset" />
														</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</c:forEach>
									</c:if>
									
									<tr id="nominals${item.id}" class="${rowclass}">
										<td colspan="5">										
											<strong><spring:message code="purchaseorder.receiptstatus" />: </strong>
											<c:choose>
												<c:when test="${item.receiptStatus == 'NOT_RECEIVED'}">
												&nbsp;<img src="img/icons/lorry_faded.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.notreceived' />" title="<spring:message code='purchaseorder.notreceived' />" />
												</c:when>
												<c:when test="${item.receiptStatus == 'PART_RECEIVED'}">
												&nbsp;<img src="img/icons/lorry_flatbed.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.partreceived' />" title="<spring:message code='purchaseorder.partreceived' />" />
												</c:when>
												<c:when test="${item.receiptStatus == 'COMPLETE'}">
												&nbsp;<img src="img/icons/lorry.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.complete' />" title="<spring:message code='purchaseorder.complete' />" />
												</c:when>
											</c:choose>
											&nbsp;<strong><spring:message code="purchaseorder.approvedstatus"/>:</strong>&nbsp;
											<c:choose>
												<c:when test="${item.goodsApproved}">
													<img src="img/icons/package.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.goodsapprovedb' />" title="<spring:message code='purchaseorder.goodsapprovedb' />" /> &nbsp;
												</c:when>
												<c:otherwise>
													<img src="img/icons/package_faded.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.goodsnotapproved' />" title="<spring:message code='purchaseorder.goodsnotapproved' />" /> &nbsp;
												</c:otherwise>
											</c:choose>
											<c:choose>
												<c:when test="${item.accountsApproved}">
													<img src="img/icons/pound.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.accountsapprovedb' />" title="<spring:message code='purchaseorder.accountsapprovedb' />" /> &nbsp;
												</c:when>
												<c:otherwise>
													<img src="img/icons/pound_faded.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.accountsnotapproved' />" title="<spring:message code='purchaseorder.accountsnotapproved' />" /> &nbsp;
												</c:otherwise>
											</c:choose>
											<c:choose>
												<c:when test="${item.cancelled}">
													<img src="img/icons/redcross.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.cancelledb' />" title="<spring:message code='purchaseorder.cancelledb' />" /> &nbsp;
												</c:when>
												<c:otherwise>
													<img src="img/icons/redcross_faded.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.notcancelled' />" title="<spring:message code='purchaseorder.notcancelled' />" /> &nbsp;
												</c:otherwise>
											</c:choose>
											<a href="#" id="actLink${rowcount}" onclick=" toggleProgressActions(${rowcount}, this.id); return false; ">
												<img src="img/icons/viewactions.png" width="16" height="16" alt="<spring:message code='purchaseorder.viewprogressactions' />" title="<spring:message code='purchaseorder.viewprogressactions' />" />
											</a>
											&nbsp;<strong><spring:message code="supplierinvoice.linkedtoinvoice" /></strong>:
											<c:choose>
												<c:when test="${not empty item.supplierInvoice}" >
													<spring:message code="true" /> - ${item.supplierInvoice.invoiceNumber}
												</c:when>
												<c:when test="${not empty item.internalInvoice}">
													<spring:message code="true" /> - ${item.internalInvoice.invno}
												</c:when>
												<c:otherwise>
													<spring:message code="false" />
												</c:otherwise>
											</c:choose>
										</td>
										<td colspan="5">										
											<strong><spring:message code="purchaseorder.nominalcode" />:</strong>
											<c:choose>
												<c:when test="${not empty item.nominal}">
													${item.nominal.code} - <cwms:besttranslation translations="${item.nominal.titleTranslations}" />
												</c:when>
												<c:otherwise>
													<spring:message code="purchaseorder.noneset" />
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr id="itemprog${rowcount}" class="${rowclass} hid">
										<td colspan="10">
											<div style=" display: none; ">
												<table class="child_table" id="actionlist" summary="<spring:message code='purchaseorder.tableactions' />">
													<thead>
														<tr>
															<td colspan="7">
																<spring:message code="purchaseorder.thisitemhas"/> ${item.progressActions.size()} <spring:message code="purchaseorder.progressactions" />
															</td>
														</tr>
														<tr>
															<th class="comment" scope="col"><spring:message code="comment"/></th>
															<th class="status" scope="col"><spring:message code="purchaseorder.receiptstatus"/></th>
															<th class="goodsinapp" scope="col"><spring:message code="purchaseorder.goodsapp"/></th>
															<th class="accountsapp" scope="col"><spring:message code="purchaseorder.accountsapp"/></th>																		
															<th class="cancelled" scope="col"><spring:message code="purchaseorder.cancelled"/></th>
															<th class="actionby" scope="col"><spring:message code="purchaseorder.actionby"/></th>
															<th class="actionon" scope="col"><spring:message code="purchaseorder.actionon"/></th>
														</tr>
													</thead>
													<tfoot>
														<tr>
															<td colspan="7">&nbsp;</td>
														</tr>
													</tfoot>
													<tbody>
														<c:choose>
															<c:when test="${item.progressActions.size() > 0}">
																<c:forEach var="pa" items="${item.progressActions}">
																	<tr>
																		<td>
																			<t:fill field="${pa.description}"/> 
																		</td>
																		<td class="text-center">
																			<c:if test="${not empty pa.receiptStatus}">
																				<c:choose>
																					<c:when test="${pa.receiptStatus == 'PART_RECEIVED'}">
																						<img src="img/icons/lorry_flatbed.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.partreceived' />" title="<spring:message code='purchaseorder.partreceived' />" />
																					</c:when>
																					<c:when test="${pa.receiptStatus == 'COMPLETE'}">
																						<img src="img/icons/lorry.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.complete' />" title="<spring:message code='purchaseorder.complete' />" />
																					</c:when>
																					<c:otherwise>
																						<img src="img/icons/lorry_faded.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.notreceived' />" title="<spring:message code='purchaseorder.notreceived' />" />
																					</c:otherwise>
																				</c:choose>
																			</c:if>
																		</td>
																		<td class="text-center">
																			<c:choose>
																				<c:when test="${pa.goodsApproved}">
																					<img src="img/icons/package.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.goodsapprovedb' />" title="<spring:message code='purchaseorder.goodsapprovedb' />" />
																				</c:when>
																				<c:otherwise>
																					<img src="img/icons/package_faded.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.goodsnotapproved' />" title="<spring:message code='purchaseorder.goodsnotapproved' />" />
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td class="text-center">
																			<c:choose>
																				<c:when test="${pa.accountsApproved}">
																					<img src="img/icons/pound.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.accountsapprovedb' />" title="<spring:message code='purchaseorder.accountsapprovedb' />" />
																				</c:when>
																				<c:otherwise>
																					<img src="img/icons/pound_faded.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.accountsnotapproved' />" title="<spring:message code='purchaseorder.accountsnotapproved' />" />
																				</c:otherwise>
																			</c:choose>
																		</td>																				
																		<td class="text-center">
																			<c:choose>
																				<c:when test="${pa.cancelled}">
																					<img src="img/icons/redcross.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.cancelledb' />" title="<spring:message code='purchaseorder.cancelledb' />" />
																				</c:when>
																				<c:otherwise>
																					<img src="img/icons/redcross_faded.png" class="img_marg_bot" width="16" height="16" alt="<spring:message code='purchaseorder.notcancelled' />" title="<spring:message code='purchaseorder.notcancelled' />" />
																				</c:otherwise>
																			</c:choose>
																		</td>
																		<td>${pa.contact.name}</td>
																		<td>
																			<fmt:formatDate type="date" dateStyle="SHORT" value="${pa.date}"/>
																			<br />
																			<fmt:formatDate type="time" timeStyle="SHORT" value="${pa.date}"/>
																		</td>																			
																	</tr>
																</c:forEach>
															</c:when>
															<c:otherwise>
																<tr>
																	<td colspan="7" class="text-center bold"><spring:message code="purchaseorder.noprogressactionshavebeenrecordedforthisitem"/></td>
																</tr>
															</c:otherwise>
														</c:choose>
													</tbody>
												</table>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
							<c:set var="rowcount" value="${rowcount + 1}"/>
						</c:forEach>
						
						
						<!-- 									
						<table class="default4" summary="This table displays the purchase order carriage value">
							<tbody>
								<tr class="nobord">											
									<td class="totalsum cost2pcTotal bold">Carriage:</td>
									<td class="totalamount cost2pcTotal bold">
										${currency.currencyERSymbol} TBI
									</td>
									<td class="filler">&nbsp;</td>
								</tr>																				
							</tbody>
						</table>
						 -->
						 														
						<table class="default4">
							<tbody>
								<tr class="nobord">											
									<td class="totalsum cost2pcTotal bold"><spring:message code="purchaseorder.totalcost"/>:</td>
									<td class="totalamount cost2pcTotal bold">
										${currency.currencyERSymbol}<span id="pototal">${order.finalCost}</span>										
									</td>
									<td class="filler">&nbsp;</td>
								</tr>																				
							</tbody>
						</table>
					</c:when>
					<c:otherwise>
						<table class="default4" summary="<spring:message code='purchaseorder.tablenopoitems' />">
							<tbody>
								<tr class="nobord">											
									<td class="center bold"><spring:message code="purchaseorder.nopotodisplay" /></td>
								</tr>																				
							</tbody>
						</table>	
					</c:otherwise>
				</c:choose>
			</div>					
			
			<!-- this section contains all edit purchase order form elements -->
			<div id="edit-tab" class="hid">
																	
				<form:form  action="" method="post" modelAttribute="command">
					<input type="hidden" name="action" value="edit" />
					
					<fieldset>
						
						<legend><spring:message code="purchaseorder.editpurchaseorder"/></legend>
															
						<ol>													
							<li>
								<label><spring:message code="createpurchaseorder.contactaddress"/>:</label>
																					
								<!-- this div creates a new cascading search plugin. The default search contains an input 
									 field and results box for companies, when results are returned this cascades down to
									 display the subdivisions within the selected company. This default behavoir can be obtained
									 by including the hidden input field with no value or removing the input field. You can also
									 add address and/or contact searches to the cascade by including the values 'contact' and/or 'address'
									 in the hidden inputs value attribute separated by a comma (e.g value="contact", value="address",
									 value="address,contact").  You can also swap these values around (e.g. value="contact,address").  
								 -->									
								<div id="cascadeSearchPlugin">
									<input type="hidden" id="cascadeRules" value="subdiv,contact,address" />
									<input type="hidden" id="compCoroles" value="supplier,business" />																													
									<input type="hidden" id="prefillIds" value="${command.coid},${command.subdivid},${command.personid},${command.addrid}" />
									<input type="hidden" id="loadCurrency" value="true" />
								</div>
								
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
									<form:label path="jobno"><spring:message code="createpurchaseorder.jobno"/>:</form:label>
									<form:input type="text" path="jobno" value="${command.jobno}" size="30" />
									<form:errors path="jobno" class="attention" />
							</li>
							<li>
								<label for="returnAddressId"><spring:message code="createpurchaseorder.returnaddress"/>:</label>
								<form:select path="returnAddressId" items="${businessaddresslist}" itemLabel="value" itemValue="key" />
								<form:errors path="returnAddressId" class="attention" />
							</li>
							<li>
									<form:label path="businessPersonid"><spring:message code="purchaseorder.businesscontact"/>:</form:label>
									<form:select path="businessPersonid">
										<option value="">N/A</option>
										<c:forEach var="bcontact" items="${businesscontacts}">
											<c:set var="selectedText" value=""/>
											<c:if test="${bcontact.personid == order.businessContact.personid}">
												<c:set var="selectedText" value="selected='selected' "/>
											</c:if>
											<option value="${bcontact.personid}" ${selectedText}>${bcontact.name}</option>
										</c:forEach>
									</form:select>
									<form:errors path="businessPersonid" class="attention" />
							</li>
							<li>
									<form:label path="clientref"><spring:message code="purchaseorder.supplierref"/>:</form:label>
									<form:input type="text" path="clientref" value="${command.clientref}" />
									<form:errors path="clientref" class="attention" />
							</li>
							<li>
									<form:label path="currencyCode"><spring:message code="currency"/>:</form:label>
									<form:select path="currencyCode" id="currencyCode">
										<c:forEach var="curopt" items="${currencyopts}">
											<c:set var="selectedText" value=""/>
											<c:if test="${(curopt.currency.currencyCode == order.currency.currencyCode) && (curopt.rate == order.rate)}">
												<c:set var="selectedText" value="selected='selected'"/>
											</c:if>
											<option value="${curopt.optionValue}" ${selectedText}>
											${curopt.currency.currencyCode}
											@ ${curopt.defaultCurrencySymbol}1 = ${curopt.currency.currencySymbol}${curopt.rate}
											<c:choose>
												<c:when test="${curopt.companyDefault}">
													[Company Default Rate]
												</c:when>
												<c:when test="${curopt.systemDefault}">
													[System Default Rate]
												</c:when>
											</c:choose>
											</option>
										</c:forEach>
									</form:select>
								<form:errors path="currencyCode" class="attention" />
							</li>
							<li <c:if test="${!useTaxableOption}">class="hid"</c:if>>
								<form:label path="taxableOption"><spring:message code="createpurchaseorder.taxable"/></form:label>
								<form:select path="taxableOption" items="${taxableOptions}" itemValue="key" itemLabel="value"/>
							</li>
							<li>
								<label>&nbsp;</label>
								<input type="submit" name="update" value="<spring:message code='update' />">
							</li>
						</ol>
					</fieldset>
				</form:form>
			</div>
			<!-- end of edit purchase order section -->							
			
			<!-- displays nominals information -->
			<div id="nominals-tab" class="hid">
				<c:choose>
					<c:when test="${missingNominals.size() > 0}">
						<spring:message code="purchaseorder.thefollowing"/> ${missingNominals.size()} <spring:message code="purchaseorder.nocodes"/>
						<br/>
						<c:forEach var="item" items="${missingNominals}">
							<div><a href="<c:url value='/editpurchaseorderitem.htm?poiid=${item.id}' />" class="mainlink">${item.itemno}</a> ${item.description}</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<spring:message code="purchaseorder.nominalcodesassigned"/>
					</c:otherwise>
				</c:choose>
			</div>							
			<!-- End nominals information -->	
								
			<!-- this section displays all purchase order files -->
			<div id="files-tab" class="hid">
				<files:showFilesForSC entity="${order}" sc="${sc}" id="${order.id}" identifier="${order.pono}" ver="" rootFiles="${scRootFiles}" allowEmail="true" isEmailPlugin="false" rootTitle="<spring:message code='purchaseorder.filesfororder' /> ${order.pono}" deleteFiles="true" />
			</div>
			<!-- end of file div -->							
			
			<!-- this section displays all emails for the selected purchase order -->
			<div id="email-tab" class="hid">
				<t:showEmails entity="${order}" entityId="${order.id}" entityName="Purchase Order" sc="${sc}"/>
			</div>
			<!-- end of email section -->
			
			<!-- this section allows goods receipt for the purchase order -->
			<div id="goods-tab" class="hid">
				<purchaseorder:goodsReceipt purchaseOrder="${order}" />
			</div>
			
			<!-- this section allows goods receipt for the purchase order only for read -->
			<div id="goodsread-tab" class="hid">
				<purchaseorder:goodsReceiptOnlyRead purchaseOrder="${order}" />
			</div>
			
			<!-- this section displays all supplier invoices / accounts approval connected to the purchase order -->
			<div id="accounts-tab" class="hid">
				<c:choose>
					<c:when test="${accountsAuthenticated}">
						<purchaseorder:showSupplierInvoices purchaseOrder="${order}" />
						<purchaseorder:accountsApproval purchaseOrder="${order}" />
					</c:when>	
					<c:otherwise>
						<spring:message code="purchaseorder.youmustbeamemberoftheaccountsdepartmenttoperformthisactivity"/>
					</c:otherwise>
				</c:choose>
			</div>
			
			<!-- this section displays all supplier invoices / accounts approval connected to the purchase order for only read-->
			<div id="accountsread-tab" class="hid">
				<c:choose>
					<c:when test="${accountsAuthenticated}">
						<purchaseorder:showSupplierInvoices purchaseOrder="${order}" />
						<purchaseorder:accountsApprovalOnlyRead purchaseOrder="${order}" />
					</c:when>	
					<c:otherwise>
						<spring:message code="purchaseorder.youmustbeamemberoftheaccountsdepartmenttoperformthisactivity"/>
					</c:otherwise>
				</c:choose>
			</div>
		
			<!-- begin the actions history table -->
			<div id="actions-tab" class="hid">
				
				<table class="default4">
					<tr>
						<th align="left"><spring:message code="purchaseorder.activity"/></th>
						<th align="left"><spring:message code="purchaseorder.outcome"/></th>
						<th align="left"><spring:message code="purchaseorder.peformedby"/></th>
						<th align="left"><spring:message code="purchaseorder.performedon"/></th>
					</tr>
					<c:forEach var="action" items="${order.actions}">
						<tr>
							<td>
								<cwms:besttranslation translations="${action.activity.descriptiontranslations}" />
							</td>
							<td>
								<cwms:besttranslation translations="${action.outcomeStatus.descriptiontranslations}" />
							</td>
							<td>
								${action.contact.name}
							</td>
							<td>
								<fmt:formatDate type="date" dateStyle="SHORT" value="${action.date}"/>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<!-- end the actions history table -->
		
			<!-- begin the purchase order instructions -->
			<div id="instructions-tab" class="hid">
				<cwms-instructions
						link-coid="${order.contact.sub.comp.coid}"
						link-subdivid="${order.contact.sub.subdivid}"
						link-contactid="${order.contact.personid}"
						instruction-types="CALIBRATION,REPAIRANDADJUSTMENT,PURCHASE"
						link-jobid="${not empty order.job ? order.job.jobid:''}"
						jobitem-ids="${ jobitemIds }"
						show-mode="RELATED">
				</cwms-instructions>
			</div>
			<!-- end the purchase order instructions -->
			
		</div>
		<!-- end of infobox -->
		
		<!-- this section contains all purchase order notes -->
		<t:showTabbedNotes entity="${order}" noteType="PURCHASEORDERNOTE" noteTypeId="${order.id}" privateOnlyNotes="${privateOnlyNotes}"/>
		<!-- end of purchase order notes section -->
	</jsp:body>
</t:crocodileTemplate>	