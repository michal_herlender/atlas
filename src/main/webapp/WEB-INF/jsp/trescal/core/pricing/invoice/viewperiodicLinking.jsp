<%-- File name: /trescal/core/pricing/invoice/viewperiodicLinking.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
.addperiodicLink{
	float: right;
	margin-right: 10px;
	margin-bottom :20px;
	text-decoration: underline;
	cursor: pointer;
	color: #0063be;
}

.addperiodicLink:hover{
	color:  #990033;
}
</style>

<t:crocodileTemplate>
		<jsp:attribute name="header">
			<span class="headtext"><spring:message code="invoice.periodicinvoice.title" /> (<spring:message code="invoice.invoice" />: ${dto.invoiceNumber} ) </span>
		</jsp:attribute>
		<jsp:attribute name="scriptPart">
					<script type="text/javascript" src="script/trescal/core/pricing/invoice/viewPeriodicLinking.js"></script>
		</jsp:attribute>
		<jsp:body>
			
		<div class="infobox" >
			<div>
				<a href="viewinvoice.htm?id=${dto.id}">
					<spring:message code="periodiclinking.backtoinvoice" arguments="${dto.invoiceNumber}"/>
				</a>
				(<fmt:formatDate value="${dto.invoiceDate}" type="date" dateStyle="SHORT"/>)
			</div>
				<div class="addperiodicLink"> 
					<a href="editPeriodicLinking.htm?id=${dto.id}"> 
						<span> 
							<spring:message code="invoice.periodicinvoice.edit"/> 
						</span> 
					</a> 
				</div>
				
				<div class="addperiodicLink"> 
					<a href="periodiclinking.htm?coid=${orgId}&companyWide=false&complete=ALL&periodicInvoiceId=${dto.id}"> 
						<span> 
							<spring:message code="invoice.periodicinvoice.add" /> 
						</span> 
					</a> 
				</div>
				
			<table class="default4" id="jobitems">
				<thead>
					<tr>
						<td colspan="11"> <spring:message code="jobcost.jobitems" /> (${dto.periodicJobItems.size()}) </td>
					</tr>
					<tr>
						<th id="itemno" scope="col"> <spring:message code="viewjob.itemno" /> </th>
						<th id="instrument" scope="col"><spring:message code="instmodelname" /></th>
						<th id="plant" scope="col"><spring:message code="barcode" /></th>
						<th id="plant" scope="col"><spring:message code="plantno" /></th>
						<th id="serial" scope="col"><spring:message code="serialno" /></th>
						<th id="delno" scope="col"><spring:message code="viewjob.delno" /></th>
						<th id="deldate" scope="col"><spring:message code="viewjob.deldate" /></th> 
						<th id="jiclientRef" scope="col"><spring:message code="jicontractreview.jobitemclientref" /></th> 
						<th id="jobclientRef" scope="col"><spring:message code="addjob.clientref" /></th>
						<th id="amount" scope="col"><spring:message code="creditnote.amount" /></th> 
					</tr>
				</thead>
				<tbody>
					
					<c:forEach var="ji" items="${dto.periodicJobItems}" varStatus="count">
						
						<tr>
							<td> <links:jobnoLinkDWRInfo jobno=" ${ji.jobItem.job.jobno}" jobid="${ji.jobItem.job.jobid}" copy="false" rowcount="${count}"/>
								 / <links:jobitemLinkInfo jobItemNo="${ji.jobItem.itemno}" jobItemId="${ji.jobItem.jobItemId}"/>
							</td>
							<td> ${ji.jobItem.instrument.instrumentModelName} </td>
							<td> 
								<cwms:securedLink permission="INSTRUMENT_VIEW" classAttr="mainlink" 
								parameter="?plantid=${ji.jobItem.instrument.plantid}">   ${ji.jobItem.instrument.plantid} </cwms:securedLink>
							</td>
							<td> ${ji.jobItem.instrument.plantno} </td>
							<td> ${ji.jobItem.instrument.serialno} </td>
							<td> 
								<c:forEach items="${ji.jobItem.deliveryItems}" var="delItem">
									${delItem.delivery.deliveryNumber} </br>
								</c:forEach>
							 </td>
							<td>  
								<c:forEach items="${ji.jobItem.deliveryItems}" var="delItem">
									${delItem.delivery.deliveryDate} </br>
								</c:forEach>
							</td>
							<td> ${ji.jobItem.clientRef}  </td>
							<td> ${ji.jobItem.job.clientRef} </td>
							<td> ${ji.jobItem.estimatedPrice.price} ${ji.jobItem.estimatedPrice.currency.getValue()} </td>
						</tr>
					
					</c:forEach>
				
				</tbody>			
			</table>
	</div>
	
		<div class="infobox">
			<table class="default4" id="jobServices">
				<thead>
					<tr>
						<td colspan="4"> <spring:message code="viewjob.jobservices" /> (${dto.periodicExpenseItems.size()}) </td>
					</tr>
					<tr>
						<th id="itemno" scope="col"> <spring:message code="viewjob.itemno" /> </th>
						<th id="instrument" scope="col"><spring:message code="instmodelname" /></th>
						<th id="jobclientRef" scope="col"><spring:message code="addjob.clientref" /></th>
						<th id="amount" scope="col"><spring:message code="creditnote.amount" /></th>  
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${dto.periodicExpenseItems}" var="jbServices">
						<tr>
							<td> 
								<links:jobnoLinkDWRInfo jobno=" ${jbServices.expenseItem.job.jobno}" jobid="${jbServices.expenseItem.job.jobid}" copy="false" rowcount="${count}"/>
							 / ${jbServices.expenseItem.itemNo} </td>
							<td> ${jbServices.expenseItem.instrumentModelTranslation} </td>
							<td> ${jbServices.expenseItem.job.clientRef} </td>
							<td> ${jbServices.expenseItem.totalPrice}  ${jbServices.expenseItem.job.currency.getValue()}</td>
						</tr>
					</c:forEach>
				</tbody>			
			</table>
		</div>
	</jsp:body>

</t:crocodileTemplate>


