<%-- File name: /trescal/core/pricing/invoice/copyinvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="invoice.copyinvoice" /></span>
	</jsp:attribute>
	<jsp:body>
		<form:form action="" id="createinvoiceform" method="post" modelAttribute="form">
			<div class="infobox" id="copyInvoice">
				<fieldset>
					<legend><spring:message code="invoice.copyinvoice" /></legend>
					<ol>
						<li>
							<label class="width30">
								<spring:message code="invoice"/>
							</label>
							<a class="mainlink" href="viewinvoice.htm?id=${invoice.id}">${invoice.invno}</a>
						</li>
						<li>
							<label class="width30">
								<spring:message code="comments"/>
							</label>
							<div class="float-left">
								<div><spring:message code="invoice.copyinvoicenote1"/></div>
								<div><spring:message code="invoice.copyinvoicenote2"/></div>
								<div><spring:message code="invoice.copyinvoicenote3"/></div>
								<div><spring:message code="invoice.copyinvoicenote4"/></div>
							</div>
							<div class="clear"></div>
						</li>
						<li>
							<label class="width30">
								<spring:message code="viewjob.invoicetype" />
							</label>
								<form:select path="invoiceTypeId" items="${invoiceTypes}" itemLabel="value" itemValue="key" />
						</li>
						<li>
							<label class="width30" >
								<spring:message code="address" />
							</label>
								<form:select path="invoiceAddressId">
									<c:forEach var="address" items="${invoiceAddresses}" >
										<form:option value="${address.addrid}" >${address.addressLine}</form:option>
									</c:forEach>
								</form:select>
						</li>
						<li>
							<label class="width30">&nbsp;</label>
							<input type="submit" value="<spring:message code='invoice.copyinvoice' />" name="submit" />
						</li>
					</ol>
				</fieldset>
			</div>
		</form:form>
	</jsp:body>
</t:crocodileTemplate>
