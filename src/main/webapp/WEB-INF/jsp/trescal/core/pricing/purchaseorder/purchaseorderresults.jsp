<%-- File name: /trescal/core/pricing/purchaseorder/purchaseorderresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/address" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="purchaseorder" tagdir="/WEB-INF/tags/purchaseorder" %>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext"><spring:message code="purchaseorder.posearchresults"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/pricing/purchaseorder/PurchaseOrderResults.js'></script>
    </jsp:attribute>
    <jsp:body>
        <!-- infobox div containing purchase order results -->						
		<div class="infobox" id="POSearchResults">
			
			<form:form action="" name="searchform" id="searchform" method="post"  modelAttribute="command">
				
				<form:hidden path="coid"/>
				<form:hidden path="personid"/>
				<form:hidden path="coname"/>
				<form:hidden path="contact"/>
				<form:hidden path="pono"/>
				<form:hidden path="jobno"/>
				<form:hidden path="addressid"/>
				<%-- Note: Spring data binding automatically converts dates to text via property editor --%>
				<form:hidden path="date" /> 
				<form:hidden path="dateFrom" /> 
				<form:hidden path="dateTo" /> 
												
				<form:hidden path="pageNo"/>
				<form:hidden path="resultsPerPage"/>
													
				<t:showResultsPagination rs="${command.rs}" pageNoId=""/>						
										
				<table class="default2" id="POResults" summary="<spring:message code="purchaseorder.tableposearchresults"/>">
					<thead>
						<tr>
							<td colspan="5">
								<spring:message code="purchaseorder.poresults"/> (${command.rs.results.size()})
							</td>
						</tr>
						<tr>
							<th class="pono" scope="col"><spring:message code="purchaseorder.ponumber"/></th>  
							<th class="coname" scope="col"><spring:message code="company"/></th>  
							<th class="contact" scope="col"><spring:message code="contact"/></th>  
							<th	class="jobno" scope="col"><spring:message code="jobno"/></th>
							<th	class="regdate" scope="col"><spring:message code="purchaseorder.regdate"/></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
					</tfoot>
					<tbody id="keynavResults">
						<c:set var="rowcount" value="0"/>
						<c:choose>
				           <c:when test="${command.rs.results.size() < 1}">
							  <tr>
								 <td colspan="5" class="bold center">
									<spring:message code="noresults"/>
								 </td>
							 </tr>
						   </c:when>
						   <c:otherwise>												
							 <c:forEach var="order" items="${command.rs.results}"> 
								<tr onclick="window.location.href='<spring:url value="viewpurchaseorder.htm?id=${order.id}"/>'" onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" if (this.className != 'hoverrow') $j(this).removeClass('hoverrow'); ">
									<td>
										<a href="<spring:url value='viewpurchaseorder.htm?id=${order.id}'/>" class="mainlink">${order.pono}</a>
									</td>
									<td><links:companyLinkDWRInfo company="${order.contact.sub.comp}" rowcount="0" copy="true"/></td>
									<td><links:contactLinkDWRInfo contact="${order.contact}" rowcount="0"/></td>
									<td>${order.jobno}</td>
									<td class="regdate"><fmt:formatDate value="${order.regdate}" type="date" dateStyle="SHORT"/></td>
									
								</tr>
								<c:set var="rowcount" value="${rowcount + 1}"/>
							 </c:forEach>
						   </c:otherwise>
						</c:choose>
					</tbody>
				</table>
				
				<t:showResultsPagination rs="${command.rs}" pageNoId=""/>						
				
			</form:form>
			
		</div>
		<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>