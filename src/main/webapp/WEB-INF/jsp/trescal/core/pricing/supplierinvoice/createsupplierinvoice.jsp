<%-- File name: /trescal/core/pricing/supplierinvoice/createsupplierinvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="image" tagdir="/WEB-INF/tags/image" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="supplierinvoice.createsupplierinvoice" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script type='text/javascript' src='script/trescal/core/pricing/supplierinvoice/CreateSupplierInvoice.js'></script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="true"/>
		<div class="listbox">
			<form:form modelAttribute="form" method="post">
				<fieldset>
					<legend>
						<c:url var="urlPurchaseOrder" value='/viewpurchaseorder.htm?id=${order.id}' />
						<spring:message code="supplierinvoice.createsupplierinvoiceforpurchaseorder" />&nbsp;<a href="${urlPurchaseOrder}" class="mainlink" >${order.pono}</a>
					</legend>
					<ol>
						<li>
							<label for="supplierCompany" class="usage">
								<spring:message code="companyrole.supplier"/> <spring:message code="company"/>:
							</label>
							<div class="float-left">
								<links:companyLinkDWRInfo company="${order.contact.sub.comp}" rowcount="0" copy="true" />
							</div>
							<div class="clear"></div>									
						</li>
						<li>
							<label for="invoiceNumber" class="usage">
								<spring:message code="supplierinvoice.invoicenumber"/> *:
							</label>
							<div class="float-left">
								<form:input id="invoiceNumber" path="invoiceNumber" size="50" tabindex="1" />
							</div>
							<form:errors path="invoiceNumber" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>									
						</li>
						<li>
							<label for="invoiceDate" class="usage">
								<spring:message code="supplierinvoice.invoicedate"/> *:
							</label>
							<div class="float-left">
								<form:input id="invoiceDate" path="invoiceDate" tabindex="2" type="date" />
							</div>
							<form:errors path="invoiceDate" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>									
						</li>
						<li>
							<label for="paymentDate" class="usage">
								<spring:message code="supplierinvoice.paymentdate"/>:
							</label>
							<div class="float-left">
								<form:input id="paymentDate" path="paymentDate" tabindex="3" type="date" />
							</div>
							<form:errors path="paymentDate" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>									
						</li>
                        <li>
                            <label for="currencyCode" class="usage"><spring:message code="currency"/>:</label>
                            <div class="float-left">
                                <form:select
                                        path="currencyCode"
                                        cssClass="float-left"
                                        items="${currencyList}"
                                        itemValue="currencyCode"
                                        itemLabel="currencyName"
                                        tabindex="4"/>
                            </div>
                            <form:errors path="currencyCode"/>
                            <div class="clear"></div>
                        </li>
                        <li>
							<label for="totalWithoutTax" class="usage">
								<spring:message code="supplierinvoice.totalwithouttax"/>:
							</label>
							<div class="float-left">
								<form:input id="totalWithoutTax" path="totalWithoutTax" tabindex="5" />
							</div>
                            <div class="float-left">
                                (<spring:message code="supplierinvoice.pototalcost"/>
                                ${order.getCurrency().getCurrencyERSymbol()}&nbsp;${order.getBaseFinalCostFormatted()})
                            </div>
							<form:errors path="totalWithoutTax" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>
						</li>
						<li>
							<label for="totalWithTax" class="usage">
								<spring:message code="supplierinvoice.totalwithtax"/>:
							</label>
							<div class="float-left">
								<form:input id="totalWithTax" path="totalWithTax" tabindex="6" />
							</div>
							<form:errors path="totalWithTax" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>
						</li>
						<li>
							<label for="paymentTerms" class="usage">
								<spring:message code="paymentterms"/>:
							</label>
							<div class="float-left">
								<form:select 
										cssClass="float-left"
										id="paymentTerm" 
										items="${paymentterms}" 
										path="paymentTerm"
                                        tabindex="7"/>
								<form:errors path="paymentTerm" delimiter="; " cssClass="error"/>
							</div>
							<div class="clear"></div>									
						</li>
						<li>
							<label for="paymentMode" class="usage">
								<spring:message code="paymentmode"/>:
							</label>
							<div class="float-left">
								<form:select 
										cssClass="float-left"
										id="paymentModeId" 
										items="${paymentModes}" 
										itemValue="key" 
										itemLabel="value"
										path="paymentModeId"
                                        tabindex="8"/>
								<form:errors path="paymentModeId" delimiter="; " cssClass="error"/>
							</div>
							<div class="clear"></div>									
						</li>
						<li>
							<label for="linkAllItems" class="usage">
								<spring:message code="supplierinvoice.linkallitems"/>:
							</label>
							<div class="float-left">
								<form:radiobutton path="linkAllItems" value="true" />
								<spring:message code="yes"/>
								-
								<spring:message code="supplierinvoice.linkautomatically"/>
								<br/>
								<form:radiobutton path="linkAllItems" value="false" />
								<spring:message code="no"/>
								-
								<spring:message code="supplierinvoice.linkmanually"/>
							</div>
							<form:errors path="linkAllItems" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>									
						</li>
						<li>
							<label for="submit" class="usage">&nbsp;</label>
							<input type="submit" id="submit" value="<spring:message code='create'/>" />
							<div class="clear"></div>															
						</li>
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>