<%-- File name: /trescal/core/pricing/invoice/preparejobiteminvoice.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>


<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message
				code="invoice.periodicinvoice.title" /></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
        <script type="module" src="${pageContext.request.contextPath}/script/components/cwms-invoice/cwms-periodic-linking/index.js"></script>
	</jsp:attribute>
	<jsp:body>
	
	<form:form modelAttribute="form" method="post" >
	<div class="infobox">
	
		<t:showErrors path="*" showFieldErrors="true" showFieldName="false"/>
		<c:set var="totalPriceJobService" value="${0}"></c:set>
		<t:showCompanyInvoiceRequirements company="${company}"
			monthlyInvoice="${monthlyinvoicesystemdefault}"
			separateCostsInvoice="${separatecostsinvoicesystemdefault}"
			poInvoice="${poinvoicesystemdefault}" />

		<t:showInstructions instructions="${instructions}"/>

		<cwms-periodic-linking-global-filers
			periodicInvoices="${cwms:objectToJson(periodicInvoices)}"
			allocatedCompanyName="${allocatedCompany.value}"
			allocatedSubdivName="${allocatedSubdiv.value}"
			clientCompanyId="${company.coid}"
			clientCompanyName="${company.coname}"
			periodicInvoiceId=${periodicInvoiceId}
					></cwms-periodic-linking-global-filers>
			
	</div>
	
	<div class="infobox" id="periodiclink">
        <cwms-periodic-linking
				listJobSubdivs="${cwms:objectToJson(listJobSubdivs)}"
				listJobNos="${cwms:objectToJson(listJobNos)}"
				listJobTypes="${cwms:objectToJson(listJobTypes)}"
				listJobPOs="${cwms:objectToJson(listJobPOs)}"
				listJobBPOs="${cwms:objectToJson(listJobBPOs)}"
				listJobClientRefs="${cwms:objectToJson(listJobClientRefs)}"
				listJobItemClientRefs="${cwms:objectToJson(listJobItemClientRefs)}"
				listExpenseItemClientRefs="${cwms:objectToJson(listExpenseItemClientRefs)}"
				proposedInvoiceItems="${cwms:objectToJson(proposedInvoiceItems)}"
				proposedInvoiceExpenseItems="${cwms:objectToJson(proposedInvoiceExpenseItems)}"
				currencyCode="${company.currency.currencyCode}"

		></cwms-periodic-linking>
	
		</br>
		<input type="submit" value="<spring:message code="periodiclinking.updateselecteditems" />" name="submit" />
	</div>
	</form:form>
	</jsp:body>
</t:crocodileTemplate>