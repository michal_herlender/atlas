<%-- File name: /trescal/core/pricing/invoice/createinvoiceresults.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="costs" tagdir="/WEB-INF/tags/costs" %>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld" %>
<%@ taglib prefix="jobs" tagdir="/WEB-INF/tags/jobs" %>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:crocodileTemplate>
	<jsp:attribute name="header">
        <span class="headtext"><spring:message code="invoice.createinvoices"/></span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/core/pricing/jobcost/CreateJobCosting.js'></script>
    </jsp:attribute>
    <jsp:body>
        <div class="infobox" id="createInvResults">
            <table class="default4"
                   summary="<spring:message code="invoice.tablecreatedinvoices"/>">
                <thead>
                <tr>
                    <td colspan="4"><spring:message code="invoice.thefollowinginvoiceswerecreated"/>:</td>
                </tr>
                <tr>
                    <th class="invno" scope="col"><spring:message code="invoice.invoicenumber"/></th>
                    <th class="comp" scope="col"><spring:message code="company"/></th>
                    <th class="invstate" scope="col"><spring:message code="invoice.invoicestatus"/></th>
                    <th class="final cost2pc" scope="col"><spring:message code="finalcost"/></th>
                </tr>
                </thead>
            </table>
            <c:choose>
                <c:when test="${not empty invs}">
                    <c:set var="rowcount" value="0"/>
                    <c:forEach var="invoice" items="${invs}">
                        <table class="default4"
                               summary="<spring:message code='invoice.tableinvoicecreated'/>">
                            <tbody>
                            <tr>
                                <td class="invno">
                                    <a href="<spring:url value='viewinvoice.htm?id=${invoice.id}'/>"
                                       class="mainlink">${invoice.invno}</a>
                                </td>
                                <td class="comp">
                                <td><links:companyLinkDWRInfo company="${invoice.comp}" rowcount="0" copy="false"/></td>
                                <td class="invstate">
                                    <t:showTranslationOrDefault translations="${invoice.status.descriptiontranslations}"
                                                                defaultLocale="${defaultlocale}"/>
                                </td>
                                <td class="final cost2pc">
                                        ${invoice.currency.currencyERSymbol}${invoice.finalCost}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    [<spring:message code="invoice.coveringjobs"/>:
                                    <c:forEach var="jobLink" items="${invoice.jobLinks}">
                                        <c:if test="${velocityCount > 1}">
                                            ,
                                        </c:if>
                                        <c:choose>
                                            <c:when
                                                    test="${jobLink.job0}">
                                                <links:jobnoLinkDWRInfo rowcount="0" jobno="${jobLink.job.jobno}"
                                                                        jobid="${jobLink.job.jobid}" copy="false"/>
                                            </c:when>
                                            <c:otherwise>
                                                ${jobLink.jobno}
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                    ]
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <c:set var="rowcount" value="${rowcount + 1}"/>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <table class="default4"
                           summary="<spring:message code="invoice.tablemessagenoinvoice"/>">
                        <tbody>
                        <tr>
                            <td colspan="4" class="bold center">
                                <spring:message code="invoice.noinvoiceswerecreated"/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </jsp:body>
</t:crocodileTemplate>