<%-- File name: /trescal/core/login/login.jsp --%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
		<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
		<!-- <meta http-equiv="Refresh" content="0; url=login.htm?js=false" />  -->
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Trescal Group ERP</title>
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="default" />
		<link rel="icon" href="img/favicon.png" />
		<c:if test="${systemType == 'development'}">
			<link rel="stylesheet" href="styles/theme/test-theme.css" type="text/css" />
		</c:if>
		<script src="script/thirdparty/jQuery/jquery-3.1.1.min.js"></script>
		<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script> -->
		<!-- Provides credentials to local Alligator service if present, otherwise is silent -->
	</head>
	<body class="center" onload="document.getElementById('j_username').focus();">
		<div class="login">
			<div class="loginhead"></div>
			<div class="loginsubhead">
				<span class="loginrequest"><spring:message code="login.request"/></span>
			</div>
			<div class="loginbox">
				<fieldset>
					<legend><spring:message code="login.login"/></legend>
					<form:form id="f1" method="post" action="forgot-my-password-form.json" modelAttribute="resetForm" >
						<p>
							<label for="j_username"><spring:message code="login.username"/></label>
							<form:input id="j_username" path="username" size="20" autocomplete="off"/>
							<!--  <input type="text" id="j_username" name="username" size="20" autocomplete="off"/>-->
						</p>
						<p>
							<label for="submit"></label>
							<button name="submit" value="passwordReset" type="submit" ><spring:message code="login.resetPassword"/></button>
						</p>
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					</form:form>
				</fieldset>
			</div>
			<div class="loginfooter">
				<p>Trescal Inc. &copy;<fmt:formatDate type="date" dateStyle="SHORT" value="<%=new java.util.Date()%>" /></p>
			</div>
		</div> 
	</body>
		<script src='script/trescal/core/login/ResetMyPassword.js'></script>
</html>