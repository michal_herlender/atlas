<%-- File name: /trescal/core/login/login.jsp --%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
		<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
		<!-- <meta http-equiv="Refresh" content="0; url=login.htm?js=false" />  -->
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Trescal Group ERP</title>
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="default" />
		<link rel="icon" href="img/favicon.png" />
		<c:if test="${systemType == 'development'}">
			<link rel="stylesheet" href="styles/theme/test-theme.css" type="text/css" />
		</c:if>
		<script src="script/thirdparty/jQuery/jquery-3.1.1.min.js"></script>
	</head>
	<body class="center" onload="document.getElementById('j_username').focus();">
		<div class="login">
			<div class="loginhead"></div>
			<div class="loginbox">
			<c:choose>
				<c:when test="${error }">
				<spring:message code="login.passwordResetedError"/>
				</c:when>
				<c:otherwise>
				<spring:message code="login.passwordReseted"/>
				</c:otherwise>
			</c:choose>
			</div>
			<div class="loginfooter">
				<p>Trescal Inc. &copy;<fmt:formatDate type="date" dateStyle="SHORT" value="<%=new java.util.Date()%>" /></p>
			</div>
		</div> 
	</body>
</html>