<%-- File name: /trescal/core/login/accessdenied.jsp --%>
<%-- Note, this page does NOT use the crocodile template intentionally, 
     e.g. when multiple logout attempts are made the user has no authentication... so menu creation will fail
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Trescal Group ERP</title>
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="default" />
		<link rel="icon" href="img/favicon.png" />
		<c:if test="${systemType == 'development'}">
			<link rel="stylesheet" href="styles/theme/test-theme.css" type="text/css" />
		</c:if>
	</head>
	<body>
		<div class="login">
			<div class="loginhead"></div>
			<div class="loginsubhead">
				<span class="headtext"><spring:message code="accessdenied.accessdenied"/></span>
			</div>
		<div class="login, center">
			<div class="loginbox">
				<%-- User name may be unknown if no session, e.g after logout etc... --%>
				<c:choose>
					<c:when test="${not empty currentContact}">
						<c:set var="currentContactName" value="${currentContact.name}" />					
					</c:when>
					<c:otherwise>
						<spring:message var="currentContactName" code="unknown" />
					</c:otherwise>
				</c:choose>
				<div>
					<spring:message code="accessdenied.firstline" arguments="${currentContactName}"/>
					<br/><br/>
					<spring:message code="accessdenied.secondline"/>
				</div>
				<c:if test="${not empty message}">
					<div style="padding: 10px;">
						<spring:message code="reason"/>: ${message}
					</div>
				</c:if>
			</div>	
		</div>
	</body>
</html>