<%-- File name: /trescal/core/login/login.jsp --%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
		<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
		<!-- <meta http-equiv="Refresh" content="0; url=login.htm?js=false" />  -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Trescal Group ERP</title>
		<link rel="stylesheet" href="styles/global.css" type="text/css" title="default" />
		<link rel="stylesheet" href="styles/login.css" type="text/css" title="default" />
		<link rel="icon" href="img/favicon.png" />
		<c:if test="${systemType == 'development'}">
			<link rel="stylesheet" href="styles/theme/test-theme.css" type="text/css" />
		</c:if>
		<script src="script/thirdparty/jQuery/jquery-3.1.1.min.js"></script>
		<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script> -->
		<!-- Provides credentials to local Alligator service if present, otherwise is silent -->
		<script src='script/trescal/core/login/Login.js'></script>
		<script type="module" src="script/components/cwms-local-timezone/cwms-local-timezone.js"></script>
	</head>
	<body class="center" onload="document.getElementById('j_username').focus();">
		<div class="login">
			<div class="loginhead"></div>
			<div class="loginsubhead">
				<span class="loginrequest"><spring:message code="login.request"/></span>
			</div>
			<div class="loginbox">
				<fieldset>
					<c:if test="${not empty error}">
						<c:choose>
							<c:when test="${error.equals('1')}">
								<p><font color="red"><spring:message code="login.error1"/></font></p>
							</c:when>
							<c:when test="${error.equals('2')}">
								<p><font color="red"><spring:message code="login.error2"/></font></p>
							</c:when>
							<c:when test="${error.equals('3')}">
								<p><font color="red"><spring:message code="login.error3"/></font></p>
							</c:when>
							<c:when test="${error.equals('4')}">
								<p><font color="red"><spring:message code="login.error4"/></font></p>
							</c:when>
							<c:when test="${error.equals('5')}">
								<p><font color="red"><spring:message code="login.error5"/></font></p>
							</c:when>
							<c:when test="${error.equals('6')}">
								<p><font color="red"><spring:message code="login.error6"/></font></p>
							</c:when>
							<c:otherwise>
								<p><font color="red"><spring:message code="login.errorDefault"/></font></p>
							</c:otherwise>
						</c:choose>
					</c:if>
					<legend><spring:message code="login.login"/></legend>
					<form:form id="f1" method="post" action="login">
							<cwms-local-timezone name="timezone"></cwms-local-timezone>
						<p>
							<label for="j_username"><spring:message code="login.username"/></label>
							<input type="text" id="j_username" name="username" size="20" autocomplete="off"/>
						</p>
						<p>
							<label for="j_password"><spring:message code="login.password"/></label>
							<input type="password" id="j_password" name="password" size="24" autocomplete="off"/>
						</p>
						<p>
							<label for="j_target">
							<spring:message code="login.application"/></label>
							<input type="radio"  name="targetUrl" value="/home.htm" checked/>
							<spring:message code="login.employee"/>
							<input type="radio"  name="targetUrl" value="/web/home.htm"/>
							<spring:message code="login.customer"/>
							<c:out value="${targetUrl}"/>
						</p>
						<p>
							<label for="submit"></label>
							<button name="submit" value="login" type="submit" onclick="Login(); return true;"><spring:message code="login.login"/></button>
						</p>
					</form:form>
					<a class="forgotten-password" href="${forgottenPasswordUrl}"><spring:message code="login.forgottenPassword"/></a>
				</fieldset>
			</div>
			<div class="loginfooter">
				<p>Trescal Inc. &copy;<fmt:formatDate type="date" dateStyle="SHORT" value="<%=new java.util.Date()%>" /></p>
			</div>
		</div> 
	</body>
</html>