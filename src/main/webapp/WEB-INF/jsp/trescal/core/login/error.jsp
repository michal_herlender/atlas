<%-- File name: /trescal/core/login/error.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>


<t:crocodileTemplate>
	<jsp:body>
		<div class="warningBox1">
			<div class="warningBox2">
				<div class="warningBox3">
					<h5 class="center-0 attention"><spring:message code="error"/>:</h5>
					<div class="center attention">
						<c:choose>
							<c:when test="${not empty error.localizedMessage}">
								<c:out value="${error.localizedMessage}"/>
							</c:when>
							<c:otherwise>
								<spring:message code="genericerrormessage"/>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
		
		<c:if test="${(sessionScope.systemType eq 'development') or (sessionScope.systemType eq 'debug')}">
			<div style="text-align: center">
				<button onclick="$j('#stacktrace').removeClass('hid'); $j(this).addClass('hid');">Show stack trace</button>
			</div>
			<div class="hid" id="stacktrace">
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<div class="attention"><c:out value="${message}"/></div>
						</div>
					</div>
				</div>
				<pre><c:out value="${stackTrace}"/></pre>
			</div>
		</c:if>
	</jsp:body>
</t:crocodileTemplate>