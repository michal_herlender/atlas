<%-- File name: /trescal/core/login/addlogin.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="cwms" uri="/WEB-INF/cwms.tld"%>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/company"%>
<%@ taglib prefix="links" tagdir="/WEB-INF/tags/links"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext"><spring:message code="company.addweblogin"/></span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<script>
			var focusElement = "password";
		</script>
	</jsp:attribute>
	<jsp:body>
		<t:showErrors path="form.*" showFieldErrors="false"/>
		
		<div class="listbox">
			<form:form modelAttribute="form" method="post">
				<fieldset>
					<legend>
						<spring:message code="company.addweblogin"/>
					</legend>
					
					<ol>
						<li>
							<label for="contact" class="usage">
								<spring:message code="contact"/>
							</label>
							<div class="float-left">
								<links:contactLinkDWRInfo contact="${contact}" rowcount="0"/>
							</div>
							<div class="clear"></div>															
						</li>
						<li>
							<label for="email" class="usage">
								<spring:message code="email"/>
							</label>
							<div class="float-left">
								<a href="mailto:${contact.email}" class="mainlink">${contact.email}</a>
							</div>
							<div class="clear"></div>															
						</li>
						<li>
							<label for="note" class="usage"><spring:message code="company.note"/>:</label>
							<span><spring:message code="contactadd.auto.assigned"/></span>
						</li>
						<li>
							<label for="username" class="usage">
								<spring:message code="contactadd.userloginid"/> *:
							</label>
							<div class="float-left">
								<form:input id="username" path="username" size="50"/>
							</div>
							<form:errors path="username" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label for="password" class="usage">
								<spring:message code="contactadd.password"/> *:
							</label>
							<div class="float-left">
								<form:input id="password" path="password" size="50"/>
							</div>
							<form:errors path="password" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label for="sendEmailToUser" class="usage">
								<spring:message code="contactadd.sendloginemail"/> *:
							</label>
							<div class="float-left">
								<form:radiobutton path="sendEmailToUser" value="true"/><spring:message code="true"/><br>
								<form:radiobutton path="sendEmailToUser" value="false"/><spring:message code="false"/>
							</div>
							<form:errors path="sendEmailToUser" element="div" delimiter="; " cssClass="float-left; error"/>
							<div class="clear"></div>															
						</li>
						<li>
							<label for="submit" class="usage">
								&nbsp;
							</label>
							<div class="float-left">
								<input type="submit" name="submit" id="submit" value="<spring:message code="submit"/>"/>		
							</div>
							<div class="clear"></div>
						</li>	
					</ol>
				</fieldset>
			</form:form>
		</div>
	</jsp:body>
</t:crocodileTemplate>
