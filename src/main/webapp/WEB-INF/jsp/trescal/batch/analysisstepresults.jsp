<%-- File name: /trescal/core/admin/businessEmails.jsp --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
	<jsp:attribute name="header">
		<span class="headtext">Analysis Steps results</span>
	</jsp:attribute>
	<jsp:attribute name="scriptPart">
		<style>
			vaadin-dialog-overlay {
				width: 60%;
			}
		</style>
		<script type='module' src='script/components/cwms-translate/cwms-translate.js' ></script>
		<script type='module'
			src='script/components/cwms-analysis-results/cwms-analysis-results.js'></script>
	</jsp:attribute>
	<jsp:body>
		
		<cwms-analysis-results step-execution-id="${ stepExecutionId }" first-columns-to-show="9"
			 ef-start-index="${ efStartIndex }" job-name="${ jobName }" page-size="25" >
		</cwms-analysis-results>
			
	</jsp:body>
</t:crocodileTemplate>