<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        Section between main_2 and main_3
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script language="JavaScript">

	function link(key) {		
		window.location.href="<c:url value='/runproc.htm?runfile=" + key + "'/>";"/>
	}
	
</script>

<script type='text/javascript' src='script/trescal/calibration/Procedures.js'></script>
    </jsp:attribute>
    <jsp:body>
        <button onclick="window.location.href='<c:url value='/searchprocedures.htm'/>'" style=" margin-bottom: 10px; ">New Search</button>
						
						<div class="infobox">
						
							<table id="proceduresearchresults" class="tablesorter"  summary="This table lists all procedure search results">
								<thead>
									<tr>
										<td colspan="4">
											<c:choose>
												<c:when test="${procedures.size() > 1}">
													Your procedures search returned ${procedures.size()} results
												</c:when>
												<c:otherwise>
													Your procedures search returned ${procedures.size()} result
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
									<tr>		
										<th id="cert" scope="col">Certificate</th>  																
										<th id="title" scope="col">Title</th>  
										<th id="dept" scope="col">department</th>  
										##<th id="heading" scope="col">heading</th>  
										##<th	id="qresclientref" scope="col">equipment</th>																			
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="4">&nbsp;</td>
									</tr>
								</tfoot>
								<tbody>
									<c:set var="rowcount" value="1"/>
									<c:forEach var="p" items="${procedures}">
										<tr id="" <c:choose><c:when test="${rowcount % 2 == 0}"> class="even" </c:when><c:otherwise> class="odd" </c:otherwise></c:choose>> onclick="link('${p.xindiceKey}')" onmouseover="this.id = this.className; this.className = 'hoverrow';" onmouseout="if (this.id == 'even') {this.className = 'even'; this.id = '';} else {this.className = 'odd'; this.id = ''}">
											<td>${p.comment}</td>
											<td scope="row">${p.title}</td>
										##	<td>${p.heading}</td>		
											<td>${p.department}</td>
										##	<td>${p.equipment}</td>																														
										</tr>
										<c:set var="rowcount" value="${rowcount + 1}"/>
									</c:forEach>
								</tbody>
							</table>
							
						</div>
    </jsp:body>
</t:crocodileTemplate>