<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">Procedure Browser</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/calibration/SearchProcedures.js'></script>
    </jsp:attribute>
    <jsp:body>
			<!-- infobox contains all search form elements and is styled with nifty corners -->
			<div class="infobox">
			
				<!-- link to create a new procedure -->
				<a href="#" class="mainlink-float" onclick=" window.location.href = springUrl + '/editproc.htm?file=NEW'; return false; ">New Procedure</a>
				
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
			
				<form method="post" action="" id="searchproceduresform">
			
				<fieldset>
				
					<legend>Search Procedures</legend>
					
					<ol>
						<li>
							<spring:bind path="searchprocedures.comment">
								<label for="${status.expression}">Certificate Number:</label>												
								<input name="${status.expression}" id="${status.expression}" type="text" value="${status.value}" tabindex="1" />
							</spring:bind>
						</li>									
						<li>
							<spring:bind path="searchprocedures.title">
								<label for="${status.expression}">Title:</label>												
								<input name="${status.expression}" id="${status.expression}" type="text" value="${status.value}" tabindex="2" />
							</spring:bind>
						</li>
						<li>
							<spring:bind path="searchprocedures.department">
								<label for="${status.expression}">Department:</label>												
								<input name="${status.expression}" id="${status.expression}" type="text" value="${status.value}" tabindex="3" />
							</spring:bind>
						</li>
						<li>
							<spring:bind path="searchprocedures.heading">
								<label for="${status.expression}">Heading:</label>												
								<input name="${status.expression}" id="${status.expression}" type="text" value="${status.value}" tabindex="4" />
							</spring:bind>
						</li>
						<li>
							<spring:bind path="searchprocedures.equipment">
								<label for="${status.expression}">Equipment:</label>
								<input name="${status.expression}" id="${status.expression}" type="text" value="${status.value}" tabindex="5" />
							</spring:bind>
						</li>
						<li>
							<spring:bind path="searchprocedures.procedureNumber">
								<label for="${status.expression}">Number:</label>
								<input name="${status.expression}" id="${status.expression}" type="text" value="${status.value}" tabindex="6" />
							</spring:bind>
						</li>
						<li>
							<spring:bind path="searchprocedures.user">
								<label for="${status.expression}">Owner:</label>											
								<input name="${status.expression}" id="${status.expression}" type="text" value="${status.value}" tabindex="7" />
							</spring:bind>
						</li>
						<li>
							<spring:bind path="searchprocedures.completed">
								<label for="${status.expression}">Completed:</label>
								<input name="${status.expression}" id="${status.expression}" type="checkbox" value="true" tabindex="8" />
							</spring:bind>
						</li>
						<li>
							<spring:bind path="searchprocedures.XPathQuery">
								<label for="${status.expression}">XPath:</label>
								<input name="${status.expression}" type="text" id="${status.expression}" size="80" value="${status.value}" tabindex="9" />																		
							</spring:bind>
						</li>
						<li>
							
							<spring:bind path="searchprocedures.collectionType">
								<label for="collection">Collection:</label>									
								<select name="${status.expression}" id="collection" tabindex="10">							
									<option value="jobs">jobs</option>
									<option value="procedures">procedures</option>
								</select>
							</spring:bind>
						</li>
						<li>
							<label for="submit">&nbsp;</label>
							<input type="submit" value="Submit" name="submit" id="submit" tabindex="11" />
						</li>									
					</ol>
				
				</fieldset>
				
				</form>
			
			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>