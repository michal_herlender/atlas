<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">Certificate Template Editor - Work Instruction Browser</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
        <script type='text/javascript' src='script/trescal/calibration/SearchWorkResults.js'></script>
    </jsp:attribute>
    <jsp:body>
    				<!-- error section displayed when form submission fails -->
			<spring:bind path="form.*">
			<c:if test="${status.error}">
				<div class="warningBox1">
					<div class="warningBox2">
						<div class="warningBox3">
							<h5 class="center-0 attention">One or more errors occured searching for procedures</h5>
							<div class="center attention">${status.errorMessage}</div>
						</div>
					</div>
				</div>
			</c:if>
			</spring:bind>
			<!-- end error section -->
			
			<!-- infobox contains all form elements for procedure searching and is styled with nifty corners -->				
			<div class="infobox">
				
				<a href="<c:url value ="/editworkinstruction.htm"/>" class="mainlink-float">New Work Instruction</a>
				
				<!-- clear floats and restore page flow -->
				<div class="clear"></div>
				
				<form method="post" action="" id="searchwiform">
					
					<fieldset id="instrumentSearch">
						
						<legend>Search Procedure</legend>
						Select a work instruction to base a templates test data on<p>
						<ol>
							<li>
								<label>Contact:</label>
								<spring:bind path="form.personid">
									<div id="contSearchPlugin">
										<input type="hidden" id="contCoroles" value="client,business" />
										<input type="hidden" id="contindex" value="1" />
										<!-- contact results listed here -->
									</div>
								</spring:bind>
								
								<!-- clear floats and restore page flow -->
								<div class="clear"></div>
							</li>
							<li>
								<label>Lab:</label>
								<span>
									<form:input path="form.lab" tabindex="3" />
								</span>
							</li>
							<li>
								<label>Name:</label>
								<span>
									<form:input path="form.name" tabindex="4" />
								</span>
							</li>
							<li>
								<label>Title:</label>
								<span>
									<form:input path="form.title" tabindex="5" />
								</span>
							</li>											
							<li>
								<label>&nbsp;</label>
								<span>
									<input type="submit" name="search" value="Search" tabindex="6" />
								</span>
							</li>
						</ol>
						
					
					</fieldset>
													
				</form>
				
			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>