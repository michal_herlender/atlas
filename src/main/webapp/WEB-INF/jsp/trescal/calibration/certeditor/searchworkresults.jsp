<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<t:crocodileTemplate>
    <jsp:attribute name="header">
        <span class="headtext">Certificate Template Editor - Work Instruction Results</span>
    </jsp:attribute>
    <jsp:attribute name="scriptPart">
       <script type='text/javascript' src='script/trescal/calibration/SearchWorkResults.js'></script>
    </jsp:attribute>
    <jsp:body>
			<!-- infobox contains all search results and is styled with nifty corners -->
			<div class="infobox">								
				
				<form action="" name="searchform" id="searchform" method="post">
					
					<form:hidden path="form.pageNo" />
					<form:hidden path="form.resultsPerPage" />
					<form:hidden path="form.personid" />
					<form:hidden path="form.lab" />
					<form:hidden path="form.name" />
					<form:hidden path="form.title" />
					
					<t:showResultsPagination rs="${form.rs}" pageNoId="" />
										
					<table id="wisearchresults" class="tablesorter" summary="This table lists all procedure search results">
						<thead>
							<tr>
								<td colspan="4">
									Procedure Results
								</td>
							</tr>
							<tr>
								<th class="title" scope="col">Title</th>
								<th class="name" scope="col">Name</th>	
								<th class="lab" scope="col">Lab</th>	
								<th class="owner" scope="col">Owner</th>												
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
						</tfoot>
						<tbody id="keynavResults">
							<c:set var="rowcount" value="0"/>
							<c:choose>
								<c:when test="${form.rs.results.size() < 1}">
									<tr>
										<td colspan="4" class="bold center">
											There are no results to display
										</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach var="WorkInstruction" items="${form.rs.results}">
										<tr onmouseover=" $j(this).addClass('hoverrow'); " onmouseout=" $j(this).removeClass('hoverrow'); " >
											<td>${WorkInstruction.title}</td>
											<td>${WorkInstruction.name}</td>
											<td>${WorkInstruction.lab}</td>
											<td>${WorkInstruction.owner.name}</td>
										</tr>
										<c:set var="rowcount" value="${rowcount + 1}"/>	
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					
					<t:paginationControl resultset="${form.rs}"/>
					
				</form>
				
			</div>
			<!-- end of infobox div -->
    </jsp:body>
</t:crocodileTemplate>