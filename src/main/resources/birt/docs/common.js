function countryName(countryCode, lang) {
	importPackage(Packages.java.util);
	if(countryCode) {
		return new Locale("", countryCode).getDisplayCountry(new Locale(lang));
	} else {
		return "";
	}
}