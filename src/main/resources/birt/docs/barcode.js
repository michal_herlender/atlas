function generateCode128(value, height) {
	importPackage(Packages.java.awt.image);
	importPackage(Packages.java.io);

	importPackage(Packages.org.krysalis.barcode4j.impl.code128);
	importPackage(Packages.org.krysalis.barcode4j.output.bitmap);
	importPackage(Packages.org.krysalis.barcode4j.tools);
	importPackage(Packages.org.krysalis.barcode4j);
	
	baos = null;

	// Create the barcode bean
	bean = new Code128Bean();

	dpi = 150;

	// Configure the barcode generator
	bean.setModuleWidth(UnitConv.in2mm(1.0 / dpi));
	bean.doQuietZone(false);
	bean.setMsgPosition(HumanReadablePlacement.HRP_NONE);

	// Open output file
	baos = new ByteArrayOutputStream();

	try {
		// Set up the canvas provider for monochrome JPEG output
		canvas = new BitmapCanvasProvider(baos, "image/jpeg", dpi,
				BufferedImage.TYPE_BYTE_BINARY, false, 0);

		// Generate the barcode
		bean.generateBarcode(canvas, value);
		dim = bean.calcDimensions(value);
		width = dim.getWidth() + "mm";
		height = dim.getHeight() + "mm";
		// Signal end of generation
		canvas.finish();
	} finally {
		baos.close();
	}

	return {"width": width, "height": height, "baos":baos};
}

function generateDatamatrix(value, height) {
	importPackage(Packages.java.awt.image);
	importPackage(Packages.java.awt)
	importPackage(Packages.java.io);

	importPackage(Packages.org.krysalis.barcode4j.impl.datamatrix);
	importPackage(Packages.org.krysalis.barcode4j.output.bitmap);
	importPackage(Packages.org.krysalis.barcode4j.tools);
	importPackage(Packages.org.krysalis.barcode4j);
	
	baos = null;
	
	
	// Create the barcode bean
	bean = new DataMatrixBean();

	dpi = 150;
	side = height / 2.54 * 150;
	dimension = new Dimension(side, side);
	//bean.setMinSize(dimension);
	//bean.setMaxSize(dimension);

	// Configure the barcode generator
	bean.setModuleWidth(UnitConv.in2mm(5.0 / dpi));
	bean.doQuietZone(false);
	bean.setMsgPosition(HumanReadablePlacement.HRP_NONE);

	// Open output file
	baos = new ByteArrayOutputStream();

	try {
		// Set up the canvas provider for monochrome JPEG output
		canvas = new BitmapCanvasProvider(baos, "image/jpeg", dpi,
				BufferedImage.TYPE_BYTE_BINARY, false, 0);

		// Generate the barcode
		bean.generateBarcode(canvas, value);
		width = bean.calcDimensions(value).getWidth() + "mm";
		// Signal end of generation
		canvas.finish();
	} finally {
		baos.close();
	}

	return {"width": width, "baos":baos};
}


function generateCode39(value) {
	importPackage(Packages.java.awt.image);
	importPackage(Packages.java.io);

	importPackage(Packages.org.krysalis.barcode4j.impl.code39);
	importPackage(Packages.org.krysalis.barcode4j.output.bitmap);
	importPackage(Packages.org.krysalis.barcode4j.tools);
	importPackage(Packages.org.krysalis.barcode4j);
	baos = null;

	// Create the barcode bean
	bean = new Code39Bean();

	dpi = 240;

	// Configure the barcode generator
	bean.setModuleWidth(UnitConv.in2mm(1.0 / dpi));
	bean.setMsgPosition(HumanReadablePlacement.HRP_NONE);
	bean.setWideFactor(3);
	bean.doQuietZone(false);
	bean.setHeight(10);

	// Open output file
	baos = new ByteArrayOutputStream();

	try {
		// Set up the canvas provider for monochrome JPEG output
		canvas = new BitmapCanvasProvider(baos, "image/jpeg", dpi,
				BufferedImage.TYPE_BYTE_BINARY, false, 0);

		// Generate the barcode
		bean.generateBarcode(canvas, value);

		// Signal end of generation
		canvas.finish();
	} finally {
		baos.close();
	}

	return baos;
}

function countryName(countryCode, lang) {
	importPackage(Packages.java.util);
	if(countryCode) {
		return new Locale("", countryCode).getDisplayCountry(new Locale(lang));
	} else {
		return "";
	}
}