USE [msdb]
GO

/****** Object:  Job [Sales Company Table Creation]    Script Date: 05/09/2018 09:53:47 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 05/09/2018 09:53:47 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Sales Company Table Creation', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Sales Company Table Creation', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'TRESCAL\admin.adrian', 
		@notify_email_operator_name=N'Administrator', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Drop]    Script Date: 05/09/2018 09:53:48 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Drop', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'drop table SALES_COMPANY;', 
		@database_name=N'spain', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create]    Script Date: 05/09/2018 09:53:48 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'create table SALES_COMPANY
(ORGID INT
,COMPANY VARCHAR(100)
,COID VARCHAR(10)
,CODE VARCHAR(26)
,NAME VARCHAR(67)
,AREA VARCHAR(67)
,NETT FLOAT
,VAT FLOAT
,GROSS FLOAT
,DATE DATETIME
,CATEGORY VARCHAR(50)
,DETPRIMARY FLOAT
,LASTMODIFIED DATETIME
); 
INSERT INTO SALES_COMPANY 
select T1.orgid 
, T2.Coname Company
, T1.coid
, T1.code
, {fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})} Name
, case  when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%SALES%'' then ''Sales'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%HIRE%'' then ''Hire'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%PRESSURE%'' then ''Pressure'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%ELECTRICAL%'' then ''Electrical'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%TEMPERATURE%'' then ''Temperature'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%ORIFICE%'' then ''Orifice'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%VALVE%'' then ''Valve'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%HUMIDITY%'' then ''Humidity'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%DEWPOINT%'' then ''Dewpoint'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%FLOW%'' then ''Flow'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%GAS%'' then ''Gas Detection'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%DIMENSIONAL%'' then ''Dimensional'' 
else T1.TITLE end  Area
, ISNULL(T1.cost,(T1.altcost / T1.rate)) Nett
, 0 Vat
, 0 Gross
, DATEADD(dd, 0, DATEDIFF(dd, 0, T1.invoicedate)) Date
, case  when {fn LEFT(T1.CODE,1)} = ''9'' then ''Grimsby'' when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%VALVE%'' then ''Valves'' else ''Calibration'' end Category
, T1.ID DetPrimary
, CURRENT_TIMESTAMP Lastmodified
from INVACCS T1
, COMPANY T2
where T1.COID = T2.COID
UNION ALL
select T1.orgid
, T2.Coname Company
, T1.coid
, T1.code
, {fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})} Name
, case  when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%SALES%'' then ''Sales'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%HIRE%'' then ''Hire'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%PRESSURE%'' then ''Pressure'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%ELECTRICAL%'' then ''Electrical'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%TEMPERATURE%'' then ''Temperature'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%ORIFICE%'' then ''Orifice'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%VALVE%'' then ''Valve'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%HUMIDITY%'' then ''Humidity'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%DEWPOINT%'' then ''Dewpoint'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%FLOW%'' then ''Flow'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%GAS%'' then ''Gas Detection'' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%DIMENSIONAL%'' then ''Dimensional'' 
else T1.TITLE end  Area
, ISNULL(-T1.totalcost,(-T1.altcost / T1.rate)) Nett
, 0 Vat
, 0 Gross
, DATEADD(dd, 0, DATEDIFF(dd, 0, T1.invoicedate)) Date
, case  when {fn LEFT(T1.CODE,1)} = ''9'' then ''Grimsby'' when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,'' '')},{fn RIGHT(T1.CODE,5)})})} like ''%VALVE%'' then ''Valves'' else ''Calibration'' end Category
, T1.ID DetPrimary
, CURRENT_TIMESTAMP Lastmodified
from CREDNOTEACCS T1
, COMPANY T2
where T1.COID = T2.COID;', 
		@database_name=N'spain', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Hourly', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20160504, 
		@active_end_date=99991231, 
		@active_start_time=60000, 
		@active_end_time=225959, 
		@schedule_uid=N'2d65e926-aec1-44a6-9583-836db150f52a'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

