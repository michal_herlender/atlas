/* Certificats pour autres subdivisions */

SELECT 
	S1.COMPANY "Empresa",
	S1.DESCRIPTION "Nombre de modelo del instrumento",
	S1.SERIALNO "Número de Serie",
	S1.PLANTNO "Número de equipo",
	S1.JOBNO "Número de trabajo",
	S1.SUBNAME "Job Subdivision",
	S1.ITEMNO "Elemento",
	CONVERT(VARCHAR(10), S1.DATEIN, 103)  "Fecha admisión",
	S1.DATEIN,
	S1.ITEMSTATUS "Estado",
	S1.COST "Precio",
	ISNULL(CONVERT(VARCHAR(10), S1.WORKCOMPLETED, 103),'') "Trabajo completado",
	S1.WORKCOMPLETED,
	S1.TPNAME "Nombre del tercero",
	ISNULL(S2.certno,'') "Número de certificado",
	S4.subname "Subdivision fournissant",
	S3.Technician "Técnico"
FROM
(
SELECT distinct
	T1.ORGID
	,T7.SUBNAME
	,T1.COMPANY
	,T1.MFR
	,T1.INSTMODEL MODEL
	,T1.DESCRIPTION
	,T1.FAMILY
	,T1.DOMAIN
	,T1.SERIALNO
	,T1.PLANTID BARCODE
	,T1.PLANTNO
	,T1.JOBNO
	,T1.ITEMNO
	,T1.DATEIN
	,T1.JOBITEMID
	,T1.ITEMSTATUS
	,T1.INSTRUMENTSTATUS
	,case when T1.INVfinalcost = 0 
	then 
		case when (T1.IHCAL + T1.TPCAL + T1.IHREP + T1.TPREP + T1.SER) = 0 
			then cast(round((T1.CRCAL + T1.CRREP + T1.CRADJ + T1.CRPUR) * ((100 - T6.interbranchdiscountrate)/100),2) as decimal(12,2)) 
			else cast(round((T1.IHCAL + T1.TPCAL + T1.IHREP + T1.TPREP + T1.SER) * ((100 - T6.interbranchdiscountrate)/100),2) as decimal(12,2))
		end
	else cast(round(T1.INVfinalcost * ((100 - T6.interbranchdiscountrate)/100),2) as decimal(12,2))
	end COST
	,T1.FIRSTACTIVITY
	,T1.LASTACTIVITY
	,T1.WORKCOMPLETED
	,T1.ONBEHALFOF
	,T1.TPNAME
FROM 
	dbo.KPI T1
	inner join dbo.jobitemaction T2 on T1.jobitemid = T2.jobitemid and T2.deleted = 0 and T2.activityid <> 53 
	inner join dbo.address T3 on T2.toaddrid = T3.addrid
	inner join dbo.subdiv T4 on T3.subdivid = T4.subdivid
	inner join dbo.company T5 on T4.coid = T5.coid
	inner join dbo.businesscompanysettings T6 on T5.coid = T6.companyid
	inner join dbo.subdiv T7 on T1.orgid = T7.subdivid
WHERE 
	T1.COID <> 6690
	and T1.jobitemid in 
		(
		SELECT 
			distinct jobitemid
		FROM
			dbo.jobitemaction 
		where 
			toaddrid in (
						select addrid FROM dbo.address WHERE subdivid = (@Subdivision)
						) 
			and fromaddrid in (
								SELECT 
									T1.addrid 
								FROM 
									dbo.address T1 
									inner join dbo.subdiv T2 on T1.subdivid = T2.subdivid and T2.coid = 6690 
								WHERE 
									T1.addrid not in (
													 select addrid FROM dbo.address WHERE subdivid in (
																									  select subdivid from subdiv where analyticalcenter = (
																																						   select analyticalcenter from subdiv where subdivid = (@Subdivision)
																																						   )
																									  )
													 )
								)
		)
UNION
SELECT 
	T1.ORGID
	,T7.SUBNAME
	,T1.COMPANY
	,T1.MFR
	,T1.INSTMODEL MODEL
	,T1.DESCRIPTION
	,T1.FAMILY
	,T1.DOMAIN
	,T1.SERIALNO
	,T1.PLANTID BARCODE
	,T1.PLANTNO
	,T1.JOBNO
	,T1.ITEMNO
	,T1.DATEIN
	,T1.JOBITEMID
	,T1.ITEMSTATUS
	,T1.INSTRUMENTSTATUS
	,case when T1.INVfinalcost = 0 
		then 
			case when (T1.IHCAL + T1.TPCAL + T1.IHREP + T1.TPREP + T1.SER) = 0 
				then cast(round((T1.CRCAL + T1.CRREP + T1.CRADJ + T1.CRPUR) * ((100 - T6.interbranchdiscountrate)/100),2) as decimal(12,2)) 
				else cast(round((T1.IHCAL + T1.TPCAL + T1.IHREP + T1.TPREP + T1.SER) * ((100 - T6.interbranchdiscountrate)/100),2) as decimal(12,2))
			end
		else cast(round(T1.INVfinalcost * ((100 - T6.interbranchdiscountrate)/100),2) as decimal(12,2))
	end COST
	,T1.FIRSTACTIVITY
	,T1.LASTACTIVITY
	,T1.WORKCOMPLETED
	,T1.ONBEHALFOF
	,T1.TPNAME
FROM 
	dbo.KPI T1
	inner join dbo.subdiv T4 on T1.orgid = T4.subdivid
	inner join dbo.company T5 on T4.coid = T5.coid
	inner join dbo.businesscompanysettings T6 on T5.coid = T6.companyid
	inner join dbo.subdiv T7 on t1.orgid = T7.subdivid
where 
	T1.COID <> 6690
	and T1.ORGID in (
					SELECT subdivid FROM dbo.subdiv WHERE coid = 6690 and subdivid not in (
																							select subdivid from subdiv where analyticalcenter = (
																																				  select analyticalcenter from subdiv where subdivid = (@Subdivision)
																																				 )
																						  )
					)
) as S1 
inner join
-- certificate numbers
(
SELECT 
	T1.jobitemid,
	STUFF((SELECT ';' + T2.certno
			FROM
			(SELECT
				T1.jobitemid
				,T2.certid
				,T2.certno
			FROM 
				dbo.certlink T1
				inner join dbo.certificate T2 on T1.certid = T2.certid
				inner join dbo.jobitem T3 on T1.jobitemid = T3.jobitemid
			) T2
			WHERE 
				T1.jobitemid = T2.jobitemid
			ORDER BY
				T2.certid
			FOR XML PATH('')), 1, 1, '') [certno]
FROM
	dbo.certlink T1
	inner join dbo.certificate T2 on T1.certid = T2.certid
	inner join dbo.jobitem T3 on T1.jobitemid = T3.jobitemid
WHERE
	T2.certno like 'FR005-' + (SELECT subdivcode FROM  dbo.subdiv WHERE subdivid = (@Subdivision)) + '-CI%' or T2.certno like 'FR011-' + (SELECT subdivcode FROM dbo.subdiv WHERE subdivid = (@Subdivision)) + '-CI%'
GROUP BY
	T1.jobitemid
) S2 on S1.JOBITEMID = S2.jobitemid
inner join 
-- certificate technicians
(
SELECT 
	T1.jobitemid,
	STUFF((SELECT ';' + T2.technician
			FROM
			(SELECT
				T1.jobitemid
				,T2.certid
				,CONCAT(T4.firstname,CONCAT(' ',T4.lastname)) technician
			FROM 
				dbo.certlink T1
				inner join dbo.certificate T2 on T1.certid = T2.certid
				inner join dbo.jobitem T3 on T1.jobitemid = T3.jobitemid
				inner join dbo.contact T4 on T2.registeredby = T4.personid
			) T2
			WHERE 
				T1.jobitemid = T2.jobitemid
			ORDER BY
				T2.certid
			FOR XML PATH('')), 1, 1, '') [technician]
FROM
	dbo.certlink T1
	inner join dbo.certificate T2 on T1.certid = T2.certid
	inner join dbo.jobitem T3 on T1.jobitemid = T3.jobitemid
	inner join dbo.contact T4 on T2.registeredby = T4.personid
WHERE
	T2.certno like 'FR005-' + (SELECT subdivcode FROM  dbo.subdiv WHERE subdivid = (@Subdivision)) + '-CI%' or T2.certno like 'FR011-' + (SELECT subdivcode FROM dbo.subdiv WHERE subdivid = (@Subdivision)) + '-CI%'
GROUP BY
	T1.jobitemid
) S3 on S1.JOBITEMID = S3.jobitemid
inner join
-- supplying subdivision(s)
(
SELECT
	T1.jobitemid,
	STUFF((SELECT '; ' + T2.subname
			FROM
			(SELECT distinct
				T1.jobitemid
				,T5.subname
			FROM 
				dbo.certlink T1
				inner join dbo.certificate T2 on T1.certid = T2.certid
				inner join dbo.jobitem T3 on T1.jobitemid = T3.jobitemid
				inner join dbo.contact T4 on T2.registeredby = T4.personid
				inner join dbo.subdiv T5 on T4.subdivid = T5.subdivid
			) T2
			WHERE 
				T1.jobitemid = T2.jobitemid
			ORDER BY
				T2.jobitemid
			FOR XML PATH('')), 1, 1, '') [subname]
FROM
	dbo.certlink T1
	inner join dbo.certificate T2 on T1.certid = T2.certid
	inner join dbo.jobitem T3 on T1.jobitemid = T3.jobitemid
	inner join dbo.contact T4 on T2.registeredby = T4.personid
	inner join dbo.subdiv T5 on T4.subdivid = T5.subdivid
WHERE
	T2.certno like 'FR005-' + (SELECT subdivcode FROM  dbo.subdiv WHERE subdivid = (@Subdivision)) + '-CI%' or T2.certno like 'FR011-' + (SELECT subdivcode FROM dbo.subdiv WHERE subdivid = (@Subdivision)) + '-CI%'
GROUP BY
	T1.jobitemid
) S4 on S1.JOBITEMID = S4.jobitemid
ORDER BY 
	S1.DATEIN,
	S1.JOBNO,
	S1.ITEMNO