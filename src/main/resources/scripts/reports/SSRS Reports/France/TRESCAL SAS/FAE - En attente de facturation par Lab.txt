/* FAE - En attente de facturation par Lab */

select 
	T1.subname as Sub,
	ISNULL(ISNULL(d.name,d2.name),'UNKNOWN') Lab,
	sum(T1.cal) Cal, 
	sum(T1.rep) Rep, 
	sum(T1.adj) Adj, 
	sum(T1.pur) Pur,
	sum(T1.ser) Ser,
	sum(T1.total) Total 
from 
	AWAITINGINVOICE T1 
	inner join jobitem ji on ji.jobitemid = T1.jobitemid 
	left join jobitemworkrequirement jiwr on jiwr.id = ji.nextworkreqid 
	left join workrequirement wr on jiwr.reqid = wr.id 
	left join department d on d.deptid = wr.deptid
	left join categorisedprocedure cp on wr.procedureid = cp.procid
	left join procedurecategory pc on cp.categoryid = pc.id
	left join department d2 on pc.departmentid = d2.deptid
where 
	T1.coid <>  T1.orgid
	and T1.orgid = 6690
group by 
	T1.subname, 
	d.name,d2.name
order by 
	T1.subname, 
	d.name