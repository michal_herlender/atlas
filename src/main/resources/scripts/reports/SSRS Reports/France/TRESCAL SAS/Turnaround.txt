/* Turnaround */

select
	D1.[Grupo Trescal Subdivisión],
	D1.[Empresa Cliente],
	D1.[Interno/Subcontratar],
	D1.[Sous traité en interne],
	D1.[Capabilité],
	D1.[Subdivision fournissant],
	D1.Servicio,
	D1.Laboratorio,
	D1.[Número de trabajo],
	D1.Elemento,
	D1.[Número de equipo],
	D1.Tipo,
	D1.[Informe del fallo],
	D1.[Revisión del contrato por],
	D2.deliveryno "Número de entrega del cliente",
	D1.Año,
	D1.Mes,
	D1.[Objectivo Turnaround],
	D1.[Días de trabajo Turnaround],
	D1.[Retardo de la revisión del contrato],
	D1.[Retardo de calibración],
	D1.[Duración de calibración],
	D1.[Retardo de certificado firmado],
	D1.[Retardo de coste del trabajo],
	D1.[Retardo de despacho],
	D1.JOBITEMID,
	D1.[Sub-familia],
	D1.[Nombre del subcontratista],
	D1.[Costo subcontratista]
from
(
select 
	T12.CONAME + ' - ' + T11.SUBNAME "Grupo Trescal Subdivisión",
	T2.COMPANY "Empresa Cliente",
	case when T2.category like 'TP%' then 'Sous-traité' else 
		case when T2.category like 'IH%' then 'Interne' else
			'err'
		end
	end "Interno/Subcontratar",
	case when T2.ORGID = T16.subdivid then 'Non' else 'Oui' end "Sous traité en interne",
	case when T2.category like '%CAL' then 'Vérification' else 
		case when T2.category like '%REP' then 'éparation' else
			case when T2.category like '%ADJ' then 'Ajustage' else
				case when T2.category like '%INV' then 'Enquête' else
					'err'
				end
	end
		end
	end "Servicio",
	T2.REFERENCE "Capabilité",
	T17.subname "Subdivision fournissant",
	T1.name "Laboratorio", 
	T2.JOBNO "Número de trabajo",
	T2.ITEMNO "Elemento",
	T2.PLANTNO "Número de equipo",
	T2.caltype "Tipo",
	case when LEN(T2.FAULTREPDESC) > 0 then 'Oui' else 'Non' end "Informe del fallo",
	T2.conrevby "Revisión del contrato por",
	YEAR(DATEADD(month, DATEDIFF(month, 0, case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end), 0)) "Año",
	MONTH(DATEADD(month, DATEDIFF(month, 0, case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end), 0)) "Mes" ,
	T15.TURN - ((T15.TURN / 7)*2) "Objectivo Turnaround",
	(T7.COUNT - T4.COUNT) "Días de trabajo Turnaround", 
	(T3.COUNT - T4.COUNT) "Retardo de la revisión del contrato", 
	(T5.COUNT - T3.COUNT) "Retardo de calibración", 
	(T6.COUNT - T5.COUNT) "Duración de calibración", 
	(T14.COUNT - T6.COUNT) "Retardo de certificado firmado",
	case when (T13.COUNT - T14.COUNT) <0 then 0 else (T13.COUNT - T14.COUNT) end  "Retardo de coste del trabajo ",
	(T7.COUNT - T6.COUNT) "Retardo de despacho", 
	T2.JOBITEMID,
	T2.DESCRIPTION "Sub-familia",
	T2.CATEGORY,
	TPNAME "Nombre del subcontratista",
	TPCOST "Costo subcontratista"
from 
	dbo.procedurecategory T8, 
	dbo.department T1, 
	dbo.categorisedprocedure T9, 
	dbo.procs T10,
	dbo.address T16,
	dbo.subdiv T17,
	dbo.CALENDAR2 T5, 
	dbo.CALENDAR2 T6,
	dbo.COMPANY T12,
	dbo.KPI T2 
	LEFT JOIN dbo.CALENDAR2 T3 on T2.CONREV = T3.DATE
	LEFT JOIN dbo.CALENDAR2 T4 on T2.FIRSTACTIVITY = T4.DATE
	LEFT JOIN dbo.CALENDAR2 T7 on case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end  = T7.DATE
	LEFT JOIN dbo.CALENDAR2 T13 on case when T2.ISSUEJOBCOSTING is null then case when T2.CERTIFICATESIGNED is null then T2.PRECALEND else T2.CERTIFICATESIGNED  end else T2.ISSUEJOBCOSTING end = T13.DATE
	LEFT JOIN dbo.CALENDAR2 T14 on case when T2.CERTIFICATESIGNED is null then T2.PRECALEND else T2.CERTIFICATESIGNED end = T14.DATE
	LEFT JOIN dbo.SUBDIV T11 on T2.ORGID = T11.SUBDIVID
	LEFT JOIN dbo.JOBITEM T15 on T2.JOBITEMID = T15.JOBITEMID
where 
	T8.departmentid = T1.deptid 
	and T9.categoryid = T8.id 
	and T10.id = T9.procid
	and T10.defaddressid = T16.addrid
	and T16.subdivid = T17.subdivid
	and T2.PROCID = T10.id 
	and T2.PRECAL = T5.DATE 
	and T2.PRECALEND = T6.DATE 
	and T11.COID = T12.COID 
	and T2.CATEGORY like 'IH%'
	and T2.STATUSID in (54,4051)
	and T2.ORGID in (select T1.subdivid from dbo.subdiv T1 inner join dbo.company T2 on T1.coid = T2.coid where T1.coid = 6690)
	and T2.THIRDPARTYGROUP is null
	and T2.COID in (@Empressa_Cliente)
	and YEAR(DATEADD(month, DATEDIFF(month, 0, case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end), 0)) in (@Año)
	and MONTH(DATEADD(month, DATEDIFF(month, 0, case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end), 0)) in (@Mes)
UNION
select 
	T12.CONAME + ' - ' + T11.SUBNAME "Grupo Trescal Subdivisión",
	T2.COMPANY "Empresa Cliente",
	case when T2.category like 'TP%' then 'Sous-traité' else 
		case when T2.category like 'IH%' then 'Interne' else
			'err'
		end
	end "Interno/Subcontratar",
	'' "Sous traité en interne",
	case when T2.category like '%CAL' then 'Vérification' else 
		case when T2.category like '%REP' then 'éparation' else
			case when T2.category like '%ADJ' then 'Ajustage' else
				case when T2.category like '%INV' then 'Enquête' else
					'err'
				end
			end
		end
	end "Servicio",
	T2.REFERENCE "Capabilité",
	'' "Subdivision fournissant",
	'' "Laboratorio" ,
	T2.JOBNO "Número de trabajo",
	T2.ITEMNO "Elemento",
	T2.PLANTNO "Número de equipo",
	T2.caltype "Tipo",
	case when LEN(T2.FAULTREPDESC) > 0 then 'Oui' else 'Non' end "Informe del fallo",
	T2.conrevby "Revisión del contrato por",
	YEAR(DATEADD(month, DATEDIFF(month, 0, case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end), 0)) "Año",
	MONTH(DATEADD(month, DATEDIFF(month, 0, case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end), 0)) "Mes" ,
	T15.TURN - ((T15.TURN / 7)*2) "Objectivo Turnaround",
	(T7.COUNT - T4.COUNT) "Días de trabajo Turnaround", 
	(T3.COUNT - T4.COUNT) "Retardo de la revisión del contrato", 
	0 "Retardo de calibración", 
	0 "Duración de calibración", 
	0 "Retardo de certificado firmado",
	0 "Retardo de coste del trabajo ",
	0 "Retardo de despacho", 
	T2.JOBITEMID,
	T2.DESCRIPTION "Sub-familia",
	T2.CATEGORY,
	T2.TPNAME "Nombre del subcontratista",
	T2.TPCOST "Costo subcontratista"
from 
	dbo.COMPANY T12,
	dbo.KPI T2 
	LEFT JOIN dbo.CALENDAR2 T3 on T2.CONREV = T3.DATE
	LEFT JOIN dbo.CALENDAR2 T4 on T2.FIRSTACTIVITY = T4.DATE
	LEFT JOIN dbo.CALENDAR2 T7 on case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end  = T7.DATE
	LEFT JOIN dbo.SUBDIV T11 on T2.ORGID = T11.SUBDIVID
	LEFT JOIN dbo.JOBITEM T15 on T2.JOBITEMID = T15.JOBITEMID
where  
	T11.COID = T12.COID 
	and T2.CATEGORY like 'TP%'
	and T2.STATUSID in (54,4051)
	and T2.ORGID in (select T1.subdivid from dbo.subdiv T1 inner join dbo.company T2 on T1.coid = T2.coid where T1.coid = 6690)
	and T2.COID in (@Empressa_Cliente)
	and YEAR(DATEADD(month, DATEDIFF(month, 0, case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end), 0)) in (@Año)
	and MONTH(DATEADD(month, DATEDIFF(month, 0, case  when T2.LASTDISPATCHED is null then case  when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end  else T2.LASTDISPATCHED end), 0)) in(@Mes)
) as D1
left join
(
SELECT 
	T1.jobitemid,
	STUFF((SELECT ';' + T2.deliveryno
			FROM
			(SELECT
				T1.jobitemid
				,T2.deliveryno
			FROM 
				dbo.deliveryitem T1
				inner join dbo.delivery T2 on T1.deliveryid = T2.deliveryid
			WHERE
				T2.deliverytype = 'client'
			) T2
			WHERE 
				T1.jobitemid = T2.jobitemid
			ORDER BY
				T2.deliveryno desc
			FOR XML PATH('')), 1, 1, '') [deliveryno]
FROM
	dbo.deliveryitem T1
	inner join dbo.delivery T2 on T1.deliveryid = T2.deliveryid
WHERE
	T2.deliverytype = 'client'
GROUP BY
	T1.jobitemid
) as D2 on D1.jobitemid = D2.jobitemid
order by 
	9,10