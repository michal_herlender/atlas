/* Contrôle du temps - en laboratoire - détail */

SELECT
	S1.subname Subdivisión,
	case S1.Retrasar
		when 0 then 'À temps'
		when 1 then '1 jour de retard'
		when 2 then '2 jours de retard'
		when 3 then '3 jours de retard'
		when 4 then '4 jours de retard'
		when 5 then '5 jours de retard'
		when 6 then '6 jours de retard'
		when 7 then '7 jours de retard'
		when 8 then '8 jours de retard'
		when 9 then '9 jours de retard'
		when 10 then '10 jours de retard'
		when 11 then '+10 jours de retard'
	end Retrasar,
	S1.jobitemid,
	S1.COMPANY "Empressa Cliente",
	S1.JOBNO "Número de trabajo",
	S1.ITEMNO "Elemento",
	S1.PLANTID "Código de barras",
	S1.INSTRUMENTSTATUS "Estado",
	S1.SERIALNO "Número de Serie",
	S1.PLANTNO "Número de equipo",
	convert(varchar, S1.DATEIN, 103) "Entrega",
	S1.DATEIN,
	convert(varchar, S1.DISPATCHED, 103) "Enviado",
	S1.DISPATCHED,
	S1.TARGET "Objetivo de retorno",
	S1.TURN "Retorno real",
	CONCAT(CONCAT(CONCAT(CONCAT(S1.DESCRIPTION,' '),S1.MFR),' '),S1.INSTMODEL) "Instrumento nombre del modelo",
	S1.LAB "Laboratorio",
	convert(varchar, S1.PRECALEND, 103) "Fecha de calibración",
	S1.PRECALEND,
	S1.FAULTREPDESC "Informe del fallo"
FROM
(
SELECT  
	T2.subname,
	(((T7.COUNT - T6.COUNT) - T1.AWAITINGPAYMENTDELAY)) TURN,
	(T5.TURN - ((T5.TURN / 7)*2)) TARGET,
	case 
		when (((T7.COUNT - T6.COUNT) - T1.AWAITINGPAYMENTDELAY)) - (T5.TURN - ((T5.TURN / 7)*2)) <= 0 then 0 
		when (((T7.COUNT - T6.COUNT) - T1.AWAITINGPAYMENTDELAY)) - (T5.TURN - ((T5.TURN / 7)*2)) >10 then 11
		else (((T7.COUNT - T6.COUNT) - T1.AWAITINGPAYMENTDELAY)) - (T5.TURN - ((T5.TURN / 7)*2))
	end "Retrasar",
	T1.JOBITEMID,
	T1.COMPANY,
	T1.JOBNO,
	T1.ITEMNO,
	T1.PLANTID,
	T1.INSTRUMENTSTATUS,
	T1.SERIALNO,
	T1.PLANTNO,
	T1.DATEIN,
	case  when T1.LASTDISPATCHED is null then case  when T1.FIRSTDISPATCHED is null then T1.LASTACTIVITY else T1.FIRSTDISPATCHED end  else T1.LASTDISPATCHED end DISPATCHED,
	T1.MFR,
	T1.INSTMODEL,
	T1.DESCRIPTION,
	T1.LAB,
	T1.PRECALEND,
	T1.FAULTREPDESC
FROM 
	dbo.KPI T1
	inner join dbo.subdiv T2 on T1.orgid = T2.subdivid
	inner join dbo.jobitem T5 on T1.jobitemid = T5.jobitemid
	inner join dbo.CALENDAR2 T6 on T1.FIRSTACTIVITY = T6.DATE
	inner join dbo.CALENDAR2 T7 on case  when T1.LASTDISPATCHED is null then case  when T1.FIRSTDISPATCHED is null then T1.LASTACTIVITY else T1.FIRSTDISPATCHED end  else T1.LASTDISPATCHED end  = T7.DATE
WHERE 
	T1.ORGID = (@Subdivisión)
	and T1.STATUSID = 54
	and T1.PRECALEND is not NULL
	and T1.DELNOTEFORTHIRDPARTY is NULL
	and T1.DELNOTEFORINTERNAL is NULL
	and T1.FAULTREPDESC = ''
	and case  when T1.LASTDISPATCHED is null then case  when T1.FIRSTDISPATCHED is null then T1.LASTACTIVITY else T1.FIRSTDISPATCHED end  else T1.LASTDISPATCHED end between (@From) and (@To)
	and T1.coid in (@EmpressaCliente)
) S1
WHERE
	S1.Retrasar in (@Retrasar)
ORDER BY
	S1.subname,
	S1.Retrasar,
	S1.JOBITEMID