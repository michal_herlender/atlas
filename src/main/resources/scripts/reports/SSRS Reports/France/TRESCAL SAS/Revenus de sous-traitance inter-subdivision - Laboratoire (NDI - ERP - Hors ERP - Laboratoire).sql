/* Revenus de sous-traitance inter-subdivision - Laboratoire (NDI - ERP - Hors ERP - Laboratoire) */
/* ONLY for Trescal business subdivision using the ERP booking in items for a subdivision from the same Trescal business company that is NOT using the ERP */

select
	D1.[Business Company],
	D1.[Supplying Subdivision],
	D1.[Supplying analyticalcenter],
	D1.[Requesting Subdivision],
	D1.[Requesting analyticalcenter],
	CONCAT(CONCAT(CONCAT(CONCAT(D1.[sub-family],' '),D1.[Manufacturer]),' '),D1.[Instrument model]) "Instrument Model Name",
	D1.Barcode,
	D1.[Serial No.],
	D1.[Plant No.],
	D1.[Job No.],
	D1.[Item No.],
	D1.ONBEHALFOF,
	ISNULL(D4.ponos,'') [PO Number],
	CONVERT(VARCHAR(10), D1.[Date Booked In], 103)  "Date Booked In",
	D1.[Date Booked In] DATEBOOKEDIN,
	ISNULL(CONVERT(VARCHAR(10), S1.DESPATCHEDTORETURNINGBUSINESSSUBDIVISION, 103),'') "Enviado a la subdivision de negocios de retorno",
	S1.DESPATCHEDTORETURNINGBUSINESSSUBDIVISION,
	D1.[Job Item Status],
	D1.[Revenue Type],
	D1.Revenue,
	YEAR(isnull(D2.[Last Signed],D1.DELNOTEFORRTNTOCLIENT)) "Year Cert Signed",
	MONTH(isnull(D2.[Last Signed],D1.DELNOTEFORRTNTOCLIENT)) "Month Cert Signed",
	convert(varchar(10), isnull(D2.[Last Signed],D1.DELNOTEFORRTNTOCLIENT), 103) "Cert Signed",
	isnull(D2.[Last Signed],D1.DELNOTEFORRTNTOCLIENT) CERTSIGNED,
	case when D2.[Last Signed] is null then 'No' else 'Oui' end "Certificate",
	case when D1.INSTRUMENTSTATUSID = 1 then 'Oui' else 'No' end "Instrument B.E.R.",
	ISNULL(D3.deliveryno,'') "Delivery No.",
	CONVERT(VARCHAR(10), D3.creationdate, 103)  "Delivery Note Created",
	D3.creationdate DELIVERYNOTECREATED
from
	(
	SELECT 
			T3.coname "Business Company"
			,T3.coid
			,T2.subname "Supplying Subdivision"
			,T2.analyticalcenter "Supplying analyticalcenter"
			,T1.SUBNAME "Requesting Subdivision"
			,T7.analyticalcenter "Requesting analyticalcenter"
			,T1.DESCRIPTION "Sub-family"
			,T1.MFR "Manufacturer"
			,T1.INSTMODEL "Instrument model"
			,T1.PLANTID "Barcode"
			,T1.SERIALNO "Serial No."
			,T1.PLANTNO "Plant No."
			,T1.JOBNO "Job No."
			,T1.ITEMNO "Item No."
			,YEAR(T1.DATEIN) "Year Booked In"
			,MONTH(T1.DATEIN) "Month Booked In"
			,T1.DATEIN "Date Booked In"
			,T1.DESPATCHEDTORETURNINGBUSINESSSUBDIVISION
			,T1.JOBITEMID
			,T1.ITEMSTATUS "Job Item Status"
			,T1.INSTRUMENTSTATUSID
			,case when (T5.id is null and T6.invoiceid is null) 
				then 'WIP' 
				else 
					case when (T5.reason = 1 or T6.invoiceid is not null) 
						then 'Facturé' 
						else
							case when T5.reason = 4
								then 'Garantie'
								else
									case when T5.reason = 5
										then 'Autre'
										else
											case when T5.reason = 6 
												then 'Job inter-subdivision' 
												else
													case when T5.reason = 7
														then 'Travail refait pour des raisons de qualité'
														else
															case when T5.reason = 0
																then 'Indéfini'
																else
																	case when T5.reason = 3 
																			then 'Aucun travail effectué' 
																			else 
																				case when T5.reason = 2 
																					then 'Job interne' 
																				end 
																	end
															end
													end
											end
									end
							end
					end 
			end "Revenue Type"
			,cast(
				round(		
					case when T1.INVOICED = 1
						then
							case when (T1.CALfinalcost + T1.REPfinalcost + T1.ADJfinalcost + T1.PURfinalcost + T1.SERfinalcost) = 0
								then
									T1.INVfinalcost
								else 
									(T1.CALfinalcost + T1.REPfinalcost + T1.ADJfinalcost + T1.PURfinalcost + T1.SERfinalcost)
							end
						else
							case when (T1.IHCAL + T1.TPCAL + T1.IHREP + T1.TPREP + T1.IHADJ + T1.TPADJ + T1.SER) = 0
								then 
									(T1.CRCAL + T1.CRREP + T1.CRADJ + T1.CRPUR)
								else 
									(T1.IHCAL + T1.TPCAL + T1.IHREP + T1.TPREP + T1.IHADJ + T1.TPADJ + T1.SER)
							end
					end * ((100 - T4.interbranchdiscountrate)/100)
				,2) 
			as decimal(12,2)) Revenue
			,T1.DELNOTEFORRTNTOCLIENT
			,T1.ONBEHALFOF
	FROM 
			dbo.KPI T1 
			inner join dbo.subdiv T2 on  T1.orgid = T2.subdivid
			inner join dbo.company T3 on T2.coid = T3.coid
			inner join dbo.businesscompanysettings T4 on T1.COID = T4.companyid
			left join dbo.jobitemnotinvoiced T5 on T1.JOBITEMID = T5.jobitemid
			left join dbo.invoiceitem T6 on T1.JOBITEMID = T6.jobitemid
			inner join dbo.subdiv T7 on T1.subdivid = T7.subdivid
	WHERE 
			T1.orgid in (select subdivid from subdiv where coid = 6690)
			and T1.JOBTYPEID = 1
			and	T3.coid = T1.COID
			and T1.SUBNAME <> T2.subname
			--and T1.statusid in (50,52,54,143,1001,1002,1025,4051,4058)
			and T1.DATEIN between (@From) and (@To)
	) as D1 
/* last signed certificate */	
left join
	(
	SELECT 
		T1.jobitemid,
		max(T1.endstamp) "Last Signed"
	FROM 
		dbo.jobitemaction T1
	where 
		T1.activitydesc = 'Certificate signed' and T1.complete = 1 and T1.deleted = 0
	group by 
		T1.jobitemid
	) as D2 on D1.jobitemid = D2.jobitemid
/* last client delivery note */	
left join
	(
	select 
		E1.jobitemid, 
		E2.deliveryno, 
		E2.creationdate
	FROM
		(
		SELECT 	
			T2.jobitemid, 
			max(T1.deliveryid) Lastclientdeliveryid
		FROM 
			dbo.delivery T1, 
			dbo.deliveryitem T2
		WHERE 
			T1.deliveryid = T2.deliveryid	
			and T1.deliverytype = 'client'
		GROUP BY 
			T2.jobitemid
		) as E1
		left join
		(
		SELECT 
			T2.jobitemid,
			T1.deliveryid,
			T1.deliveryno,
			T1.creationdate 
		FROM 
			dbo.delivery T1, 
			dbo.deliveryitem T2
		WHERE 
			T1.deliveryid = T2.deliveryid
		) as E2 on E1.jobitemid = E2.jobitemid and E1.Lastclientdeliveryid = E2.deliveryid
	) as D3 on D1.JOBITEMID = D3.jobitemid
/* job item purchase order(s) */	
left join
	(
	SELECT 
	T1.jobitemid,
	STUFF((SELECT '; ' + T2.ponos
		FROM
			(SELECT
				T1.jobitemid,
				isnull(isnull(T2.ponumber,T3.ponumber),'') ponos
			FROM 
				dbo.jobitempo T1
				left join dbo.po T2 on T1.poid = T2.poid
				left join dbo.bpo T3 on T1.bpoid = T3.poid
			) T2
		WHERE 
			T1.jobitemid = T2.jobitemid
		ORDER BY
			T2.ponos
		FOR XML PATH('')), 1, 1, '') ponos
	FROM
		dbo.jobitempo T1
		left join dbo.po T2 on T1.poid = T2.poid
		left join dbo.bpo T3 on T1.bpoid = T3.poid
	GROUP BY
		T1.jobitemid
	) as D4 on D1.jobitemid = D4.jobitemid
order by
	D1.jobitemid