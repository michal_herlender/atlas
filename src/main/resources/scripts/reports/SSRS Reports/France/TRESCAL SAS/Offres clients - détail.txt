/* Offres clients - détail */

SELECT
	D1.[Grupo Trescal Subdivisión],
	D1.[Año],
	D1.[Mes],
	D1.qno + ' ver ' + RTRIM(CONVERT(CHAR,D1.ver)) "Nº Presupuesto" , 
	case when D1.clientref = '' then D1.coname else D1.coname + ' (' + D1.clientref + ')' end "Empresa Cliente (referencia de cliente)" , 
	D1.[Contacto],
	D1.[Estado], 
	convert(varchar(10),D1.regdate,103) "Fecha Reg" ,
	D1.[Conseguido por], 
	ISNULL(convert(varchar(10),D1.issuedate,103),'') "Fecha de envío" , 
	ISNULL(D1.issuedby,'') "Envío por",
	ISNULL(convert(varchar(10),D1.expirydate,103),'') "Fecha de caducidad" ,  
	ISNULL(convert(varchar(10),D1.acceptedon,103),'') "Fecha de aceptación",
	ISNULL(D1.acceptedby,'') "Aceptación por",
	D1.itemno "Elemento",
	D1.[Sub-familia],
	ISNULL(D1.Marca,'') Marca,
	ISNULL(D1.Modelo,'') Modelo,
	ISNULL(D1.serialno,'') "Número de Serie",
	ISNULL(D1.plantno,'') "Número de equipo",
	ISNULL(D1.plantid,'') "Código de barras",
	ISNULL(D3.note,'') Nota,
	{fn CONCAT(
		{fn CONCAT(
			{fn CONCAT(
				{fn CONCAT(
					{fn CONCAT(
					ISNULL(D4.instructions,''),ISNULL(D4.publicinstructions,''))},
				ISNULL(D5.instructions,''))},
			ISNULL(D5.publicinstructions,''))},
		ISNULL(D4.range,''))},
	ISNULL(D5.range,''))}
	"Instrucciones de calibración",
	ISNULL(ISNULL(D6.points,D7.points),'') "Puntos de calibración",
	D1.Servicio,
	D1.currencycode "Moneda",
	D1.Descuento,
	D1.Cantidad,
	D1.[Precio por unidad],
	D1.[Precio total],
	ISNULL(D8.jno,'') "Trabajos vinculados"
FROM
	(
	select 
		T8.CONAME + ' - ' + T7.SUBNAME "Grupo Trescal Subdivisión",
		T7.subdivid,
		YEAR(T1.regdate) "Año",
		MONTH(T1.regdate) "Mes",
		T1.id qid,
		T1.qno,
		T1.ver,
		T12.itemno,
		T12.id,
		CONVERT (DATE,T1.acceptedon) acceptedon, 
		T3.translation "Estado", 
		CONVERT (DATE,T1.expirydate) expirydate,
		CONVERT (DATE,T1.issuedate) issuedate, 
		T6.firstname + ' ' + T6.lastname "Conseguido por",
		ISNULL(T1.clientref,'') clientref, 
		CONVERT (DATE,T1.regdate) regdate, 
		T2.coname,
		T2.coid,
		T4.firstname + ' ' + T4.lastname "Contacto",
		T9.firstname + ' ' + T9.lastname issuedby,
		T10.firstname + ' ' + T10.lastname acceptedby,
		T12.plantid,
		T12.modelid,
		T14.serialno,
		T14.plantno,
		ISNULL(T20.translation,T17.translation) "Sub-familia",	 
		ISNULL(T21.name,T16.name) "Marca",
		ISNULL(T14.modelname,T15.model) "Modelo",
		T22.translation "Servicio",
		T23.currencycode,
		T12.discountvalue "Descuento",
		T12.quantity "Cantidad",
		T12.totalcost "Precio por unidad",
		T12.finalcost "Precio total"
	from 
		dbo.quotation T1
		inner join dbo.contact T4 on T1.personid = T4.personid
		inner join dbo.subdiv T5 on T4.subdivid = T5.subdivid
		inner join dbo.company T2 on T5.coid = T2.coid
		inner join dbo.basestatusnametranslation T3 on T1.statusid = T3.statusid and T3.locale = 'fr_FR'
		inner join dbo.supportedcurrency T11 on T1.currencyid = T11.currencyid
		inner join dbo.quotationitem T12 on T1.id = T12.quoteid
		inner join dbo.servicetypeshortnametranslation T22 on T12.servicetypeid = T22.servicetypeid and T22.locale = 'fr_FR'
		inner join dbo.supportedcurrency T23 on T1.currencyid = T23.currencyid
		left join dbo.contact T6 on T1.createdby = T6.personid
		left join dbo.subdiv T7 on T6.subdivid = T7.subdivid
		left join dbo.company T8 on T7.coid = T8.coid
		left join dbo.contact T9 on T1.issueby = T9.personid
		left join dbo.contact T10 on T1.acceptedby = T10.personid
		left join dbo.instrument T14 on T12.plantid = T14.plantid
		left join dbo.instmodel T15 on T12.modelid = T15.modelid
		left join dbo.mfr T16 on T15.mfrid = T16.mfrid
		left join dbo.descriptiontranslation T17 on T15.descriptionid = T17.descriptionid and T17.locale = 'fr_FR'
		left join dbo.mfr T21 on T14.mfrid = T21.mfrid
		left join dbo.instmodel T18 on T14.modelid = T18.modelid
		left join dbo.mfr T19 on T18.mfrid = T19.mfrid
		left join dbo.descriptiontranslation T20 on T18.descriptionid = T20.descriptionid and T20.locale = 'fr_FR'
	where 
		T1.orgid = 6690
	) as D1
inner join
-- Max quote version
	(
	select 
		T1.qno qno , 
		max(T1.ver) maxver 
	from 
		dbo.quotation T1 
	where 
		T1.orgid = 6690
	group by 
		T1.qno
	) as D2 on D1.qno = D2.qno and D1.ver = D2.maxver
left join
-- Get active quoteitem notes	
	(
	SELECT
	D0.id,
	case when (ISNULL(D1.publiclabel,'') + ISNULL(D1.publicnote,'')) <> '' then 'Público: ' + ISNULL(D1.publiclabel,'') + ISNULL(D1.publicnote,'') + case when (ISNULL(D2.privatelabel,'') + ISNULL(D2.privatenote,'')) <> '' then char(13) else '' end else '' end + 
	case when (ISNULL(D2.privatelabel,'') + ISNULL(D2.privatenote,'')) <> '' then 'Privado: ' + ISNULL(D2.privatelabel,'') + ISNULL(D2.privatenote,'') else '' end note
FROM
(
SELECT distinct
	T2.id
FROM 
	dbo.quoteitemnote T1
	inner join dbo.quotationitem T2 on T1.id = T2.id
	inner join dbo.quotation T3 on T2.quoteid = T3.id
WHERE
	T3.orgid = 6690
	and T1.active = 1
) as D0
left join
(
SELECT
		T2.id,
		T1.label publiclabel,
		replace(replace(dbo.udf_StripHTML(T1.note),CHAR(10),' '),CHAR(13),' ') publicnote
	FROM 
		dbo.quoteitemnote T1
		inner join dbo.quotationitem T2 on T1.id = T2.id
		inner join dbo.quotation T3 on T2.quoteid = T3.id
	WHERE
		T3.orgid = 6690
		and T1.active = 1
		and T1.publish = 1
) as D1 on D0.id = D1.id
left join
(
SELECT
		T2.id,
		T1.label privatelabel,
		replace(replace(dbo.udf_StripHTML(T1.note),CHAR(10),' '),CHAR(13),' ') privatenote
	FROM 
		dbo.quoteitemnote T1
		inner join dbo.quotationitem T2 on T1.id = T2.id
		inner join dbo.quotation T3 on T2.quoteid = T3.id
	WHERE
		T3.orgid = 6690
		and T1.active = 1
		and T1.publish = 0
) as D2 on D0.id = D2.id
	) as D3 on D1.id = D3.id
left join
-- Get plantid calreq instructions and any range
	(
	select
		T11.plantid,
		replace(replace(T11.instructions,CHAR(10),' '),CHAR(13),' ') instructions,
		replace(replace(T11.publicinstructions,CHAR(10),' '),CHAR(13),' ') publicinstructions,
		case when T11.rangeid is not NULL then {fn CONCAT({fn CONCAT({fn CONCAT(cast(T12.start as CHAR),' a ')},cast(T12.endd as CHAR))},T13.translation)} end range
	FROM
		dbo.calreq T11 
		left outer join dbo.modelrange T12 on T11.rangeid = T12.id 
		left outer join dbo.uomnametranslation T13 on T12.uomid = T13.id
	WHERE
		T11.active = 1
		and (T13.locale = 'fr_FR' or T13.locale is NULL)
	) as D4 on D1.plantid = D4.plantid
left join
-- Get model company calreq instructions and any range
	(
	select
		T11.modelid,
		T11.compid,
		replace(replace(T11.instructions,CHAR(10),' '),CHAR(13),' ') instructions,
		replace(replace(T11.publicinstructions,CHAR(10),' '),CHAR(13),' ') publicinstructions,
		case when T11.rangeid is not NULL then {fn CONCAT({fn CONCAT({fn CONCAT(cast(T12.start as CHAR),' a ')},cast(T12.endd as CHAR))},T13.translation)} end range
	FROM
		dbo.calreq T11 
		left outer join dbo.modelrange T12 on T11.rangeid = T12.id 
		left outer join dbo.uomnametranslation T13 on T12.uomid = T13.id
	WHERE
		T11.active = 1
		and (T13.locale = 'fr_FR' or T13.locale is NULL)
	) as D5 on D1.modelid = D5.modelid and D1.coid = D5.compid
left join
-- Get instrument points
	(
	SELECT 
		T1.plantid,
		STUFF((SELECT '; ' + T2.points
				FROM
				(SELECT 
					T1.plantid,
							{fn CONCAT({fn CONCAT(CONVERT(VARCHAR,CAST(T2.point as FLOAT)),'')},ISNULL(T3.symbol,''))} points
				FROM
					dbo.calreq T1
					inner join dbo.calibrationpoint T2 on T1.pointsetid = T2.psid
					left outer join dbo.uom T3 on T2.uomid = T3.id 
				WHERE
					T1.active = 1
					and T1.plantid is not null
				) T2
				WHERE 
					T1.plantid = T2.plantid
				ORDER BY
					T2.plantid
				FOR XML PATH('')), 1, 1, '') [points]
	FROM
		dbo.calreq T1
		inner join dbo.calibrationpoint T2 on T1.pointsetid = T2.psid
		inner join dbo.uom T3 on T2.uomid = T3.id
	WHERE
		T1.active = 1
		and T1.plantid is not null
	GROUP BY
		T1.plantid
	) as D6 on D1.plantid = D6.plantid
left join
-- Get company model points
	(
	SELECT 
		T1.compid,
		T1.modelid,
		STUFF((SELECT '; ' + T2.points
				FROM
				(SELECT 
					T1.compid,
					T1.modelid,
							{fn CONCAT({fn CONCAT(CONVERT(VARCHAR,CAST(T2.point as FLOAT)),'')},ISNULL(T3.symbol,''))} points
				FROM
					dbo.calreq T1
					inner join dbo.calibrationpoint T2 on T1.pointsetid = T2.psid
					left outer join dbo.uom T3 on T2.uomid = T3.id 
				WHERE
					T1.active = 1
					and T1.compid is not null

				) T2
				WHERE 
					T1.compid = T2.compid and T1.modelid = T2.modelid
				ORDER BY
					T1.compid,
					T1.modelid
				FOR XML PATH('')), 1, 1, '') [points]
	FROM
		dbo.calreq T1
		inner join dbo.calibrationpoint T2 on T1.pointsetid = T2.psid
		inner join dbo.uom T3 on T2.uomid = T3.id
	WHERE
		T1.active = 1
		and T1.compid is not null
	GROUP BY
		T1.compid,
		T1.modelid
	) as D7 on D1.modelid = D7.modelid and D1.coid = D7.compid
left join
	-- Get all jobs linked to a quote
	(
	SELECT 
		T1.quoteid,
		STUFF((SELECT '; ' + T2.jobno
				FROM
				(SELECT 
						T1.quoteid,
						T2.jobno
				FROM
					dbo.jobquotelink T1
					inner join dbo.job T2 on T1.jobid = T2.jobid
				) T2
				WHERE 
					T1.quoteid = T2.quoteid
				ORDER BY
					T2.jobno
				FOR XML PATH('')), 1, 1, '') [jno]
	FROM
		dbo.jobquotelink T1
		inner join dbo.job T2 on T1.jobid = T2.jobid
	GROUP BY
		T1.quoteid
	) as D8 on D1.qid = D8.quoteid
WHERE
	D1.[Conseguido por] <> 'ERP Auto User'
	and (D1.issuedate between (@Rango_fecha_de_envio) and (@A) or D1.issuedate is NULL) 
	and D1.Estado in (@Estado)
	and (D1.issuedby in (@Envío_por) or D1.issuedby is NULL)
	and D1.subdivid in (@Subdivision)
ORDER BY
	D1.regdate,
	D1.qno,
	D1.servicio,
	D1.itemno