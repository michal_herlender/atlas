/* Étalonnages avec et sans référence à des étalons */

SELECT
	O1.orgid,
	O1.caldate,
	O1.standards,
	ISNULL(O2.cals,0) cals
FROM
(
SELECT
	T2.orgid
	,YEAR(T1.caldate) caldate
	,'Con referencia a cualquier patrones' standards
FROM  
	dbo.calibration T1
	inner join dbo.procs T2 on T1.procedureid = T2.id
where 
	T1.calibrationstatus = 32
UNION 
SELECT distinct
	T2.orgid
	,YEAR(T1.caldate) caldate
	,'Sin referencia a ninguna patrones' standards
FROM  
	dbo.calibration T1
	inner join dbo.procs T2 on T1.procedureid = T2.id
where 
	T1.calibrationstatus = 32
) as O1
left join
(
SELECT
      T3.orgid
	  ,year(T1.caldate) caldate
	  ,T2.standards
	  ,count(T2.id) cals
FROM 
	dbo.calibration T1
	inner join dbo.procs T3 on T1.procedureid = T3.id
	left join
	(
	SELECT
		T1.id
		,case when count(T2.inst) > 0 then 'Con referencia a cualquier patrones' else 'Sin referencia a ninguna patrones' end standards
	FROM
		dbo.calibration T1
		left join dbo.standardused T2 on T1.id = T2.calibration
	WHERE
		T1.calibrationstatus = 32
	GROUP BY
		T1.id
	) T2 on T1.id = T2.id
WHERE
	T1.calibrationstatus = 32
GROUP BY
	T3.orgid
	,year(T1.caldate)
	,T2.standards
) as O2 on O1.orgid = O2.orgid and O1.caldate = O2.caldate and O1.standards = O2.standards
WHERE
	O1.orgid in (@Subdivisión)
ORDER BY
	O1.orgid,
	O1.caldate,
	O1.standards