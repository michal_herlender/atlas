/* Factures périodiques - Détail - avec prix */

SELECT 
	S1.[Número Factura Periódico],
	S1.[Creado por],
	S1.[Centro analítico],
	S1.[Fecha de facturación],
	S1.[Estado de la factura],
	S1.[Estado de cuentas],
	S1.[Valor de Factura Periódico],
	S1.[Empresa cliente],
	S1.[Empresa facturada],
	S1.[Sub-familia],
	S1.Marca,
	S1.Modelo,
	S1.[Código de barras],
	S1.[Número de Serie],
	S1.[Número de equipo],
	S1.[Número de trabajo],
	S1.[Tipo de trabajo],
	S1.[Número de elemento],
	S1.[Fecha admisión],
	S1.[Elemento completado],
	S1.JOBITEMID,
	S1.[Estado de elemento],
	S1.Valor
FROM
(
select distinct
	T1.invno "Número Factura Periódico"
	,T7.subname "Creado por"
	,T7.analyticalCenter "Centro analítico"
	,convert(varchar,T1.invoicedate,103) "Fecha de facturación"
	,T4.translation "Estado de la factura"
	,case when isnull(T1.accountstatus,'') ='P' then 'En attente de synchronisation' else case when isnull(T1.accountstatus,'') ='S' then 'Facture synchronisée' else '' end end "Estado de cuentas"
	,T1.totalcost "Valor de Factura Periódico"
	,ISNULL(T2.COMPANY,'') "Empresa cliente"
	,T3.coname "Empresa facturada"
	,ISNULL(T2.DESCRIPTION,'') "Sub-familia"
	,ISNULL(T2.MFR,'') "Marca"
	,ISNULL(T2.INSTMODEL,'') "Modelo"
	,T2.PLANTID "Código de barras"
	,ISNULL(T2.SERIALNO,'') "Número de Serie"
	,ISNULL(T2.PLANTNO,'') "Número de equipo"
	,ISNULL(T2.JOBNO,'') "Número de trabajo"
	,ISNULL(T2.jobtype,'') "Tipo de trabajo"
	,ISNULL(T2.ITEMNO,'') "Número de elemento"
	,convert(varchar,T2.DATEIN,103) "Fecha admisión"
	,T2.JOBITEMID JOBITEMID
	,ISNULL(T2.ITEMSTATUS,'') "Estado de elemento"
	,convert(varchar,T2.WORKCOMPLETED,103) "Elemento completado"
	,ISNULL(case when T2.INVOICED = 1
		then
			case when (T2.CALfinalcost + T2.REPfinalcost + T2.ADJfinalcost + T2.PURfinalcost + T2.SERfinalcost) = 0
				then
					T2.INVfinalcost
				else 
					(T2.CALfinalcost + T2.REPfinalcost + T2.ADJfinalcost + T2.PURfinalcost + T2.SERfinalcost)
			end
		else
			case when (T2.IHCAL + T2.TPCAL + T2.IHREP + T2.TPREP + T2.IHADJ + T2.TPADJ + T2.SER) = 0
				then 
					(T2.CRCAL + T2.CRREP + T2.CRADJ + T2.CRPUR)
				else 
					(T2.IHCAL + T2.TPCAL + T2.IHREP + T2.TPREP + T2.IHADJ + T2.TPADJ + T2.SER)
			end
	end,0) "Valor"
FROM
	dbo.invoice T1 
	left join dbo.jobitemnotinvoiced T5 on T1.id = T5.periodicinvoiceid
	inner join dbo.invoicestatusdescriptiontranslation T4 on T1.statusid = T4.statusid
	left join dbo.KPI T2 on T5.jobitemid = T2.JOBITEMID
	left join dbo.company T3 on T1.coid = T3.coid
	inner join dbo.contact T6 on T1.createdby = T6.personid
	inner join dbo.subdiv T7 on T6.subdivid = T7.subdivid
WHERE
	T1.typeid = 6
	and (T5.reason = 1 or T5.reason is null)
	and T1.orgid = 6690
	and T4.locale = 'fr_FR'
	and T1.coid in (@Invoiced_Company)
	and T1.invoicedate between (@From) and (@To)
	and T1.id in (@Invoice)
) S1
order by 
	S1.[Número Factura Periódico]