/* Jobs sans offres liées */

SELECT 
	  T7.coname "Empresa"
	  ,T1.jobno "Número de trabajo"
	  ,convert(varchar,T1.regdate,103) "Fecha Reg"
	  ,T8.translation "Estado"
	  ,ISNULL({fn CONCAT({fn CONCAT(T9.firstname,' ')},T9.lastname)},'') "Contracto por"
	  ,count(T3.jobitemid) "Elementos de trabajo"
FROM 
	dbo.job T1 left outer join dbo.jobquotelink T2 on T1.jobid = T2.jobid
	left outer join dbo.jobitem T3 on T1.jobid = T3.jobid
	left outer join dbo.calibrationtype T4 on T3.caltypeid = T4.caltypeid
	left outer join dbo.contact T9 on T3.lastContractReviewBy_personid = T9.personid
	inner join dbo.contact T5 on T1.personid = T5.personid
	inner join dbo.subdiv T6 on T5.subdivid = T6.subdivid
	inner join dbo.company T7 on T6.coid = T7.coid
	inner join dbo.jobstatusnametranslation T8 on T1.statusid = T8.statusid
WHERE 	
	T4.accreditationspecific = 1
	and T1.orgid in (select subdivid from dbo.subdiv where coid = 6690)
	and T8.locale = 'fr_FR'
	and T2.quoteid is null
GROUP BY 
	T7.coname,
	T1.jobno,
	convert(varchar,T1.regdate,103),
	T8.translation,
	{fn CONCAT({fn CONCAT(T9.firstname,' ')},T9.lastname)}
ORDER BY
	T1.jobno