/* Lista de patrones */			
			
SELECT 			
	T1.plantid "Código de barras",
	T1.calfrequency "Intervalo de calibración",
	case T1.calintervalunit
		when 'DAY' then 'Día'
		when 'MONTH' then 'Mes'
		when 'YEAR' then 'Año'
	end "Unidad de intervalo de calibración",
	convert(varchar(10),T1.recalldate,103) "Fecha próxima calibración",
	T1.recalldate,	
	T1.plantno "Número de Equipo",
	T1.serialno "Número de Serie",	
	T3.subname "Instrumento propiedad de",
	concat(concat(T2.firstname,' '),T2.lastname) "Dueño",
	ISNULL(T4.location,'Sin ubicación') "Ubicación",
	case T1.statusid 
		when 0 then 'En circulación'
		when 1 then 'B.E.R.'
		when 2 then 'No recuerdar'
		when 3 then 'Transferido'
	end "Estado"
FROM			
	dbo.instrument T1		
	inner join dbo.contact T2 on T1.personid = T2.personid		
	inner join dbo.subdiv T3 on T2.subdivid = T3.subdivid		
	left join dbo.location T4 on T1.locationid = T4.locationid			
WHERE 			
	T1.calibrationstandard = 1
	and T3.subdivid in (@Subdivisión)
	and ISNULL(T4.location,'Sin ubicación') in (@Ubicación)
	and T1.statusid in (@Estado)
ORDER BY
	T1.plantid