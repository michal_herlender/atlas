/* WIP - En curso no terminado */

SELECT 
	T1.jobno JobNo
	,T1.businesssubname
	,T1.analyticalcenter
	,T1.jobtype
	,T1.itemno ItemNo
	,T1.itemstate
	,T1.jobitemid JobItemID
	,instmodeltranslation InstModelName
	,CASE WHEN d.name IS NOT NULL THEN d.name ELSE 'UNKNOWN' END as Lab
	,ISNULL(convert(varchar,T1.caldate,103),'') CalDate
	,ISNULL(convert(varchar,T1.datecomplete,103),'') DateComplete
	,T1.cal Cal
	,T1.rep Rep
	,T1.adj Adj
	,T1.pur Pur
	,T1.ser Ser
	,T1.total Total
	,T1.coname Company
	,T1.legalidentifier LegalIdentifier
FROM 
	dbo.ONGOINGNOTCOMPLETE T1
	inner join jobitem ji on ji.jobitemid = T1.jobitemid 
	left join jobitemworkrequirement jiwr on jiwr.id = ji.nextworkreqid 
	left join workrequirement wr on jiwr.reqid = wr.id 
	left join department d on d.deptid = wr.deptid 
where 
	T1.coid <> T1.orgid
	and T1.orgid = 6578
order by 
	T1.jobno, 
	T1.jobitemid