/**** Summary ****/

select
	T1.invno "Número Factura Periódico"
	,convert(varchar,T1.invoicedate,103) "Fecha de facturación"
	,T4.translation "Estado"
	,case when isnull(T1.accountstatus,'') ='P' then 'Pendiente de sincronización' else case when isnull(T1.accountstatus,'') ='S' then 'Sincronizada' else '' end end "Estado de cuentas"
	,T1.totalcost "Valor de Factura Periódico"
	,T3.coname "Empresa facturada"
	,ISNULL(T2.COMPANY,'') "Empresa cliente"
	,COUNT(T2.JOBITEMID) "Elementos de trabajo cubiertos por factura periódica"
	,SUM(ISNULL(case when (T2.IHCAL + T2.TPCAL + T2.IHREP + T2.TPREP + T2.IHADJ + T2.TPADJ + T2.PURCHASE + T2.SER) = 0 then (T2.CRCAL + T2.CRREP + T2.CRADJ + T2.CRPUR) else (T2.IHCAL + T2.TPCAL + T2.IHREP + T2.TPREP + T2.IHADJ + T2.TPADJ + T2.PURCHASE + T2.SER) end - T2.INVfinalcost,0)) "Ingresos Totales Corrientes de los Trabajos"
FROM
	dbo.invoice T1 
	left outer join dbo.jobitemnotinvoiced T5 on T1.id = T5.periodicinvoiceid
	left outer join dbo.KPI T2 on T5.jobitemid = T2.JOBITEMID
	left outer join dbo.company T3 on T1.coid = T3.coid
	inner join dbo.invoicestatusdescriptiontranslation T4 on T1.statusid = T4.statusid
WHERE
	T1.typeid = 6
	and (T5.reason = 1 or T5.reason is null)
	and T1.orgid = 6579
	and T4.locale = 'es_ES'
group by 
	T1.invno,
	T1.invoicedate,
	T4.translation,
	T1.accountstatus,
	T1.totalcost,
	ISNULL(T2.COMPANY,''),
	T3.coname