/**** Elementos de trabajo - Activos por laboratorio ****/

select distinct
	D1.[Número de trabajo],
	D1.[Fecha Reg],
	D1.Empresa,
	D1.Subdivisión,
	D1.Contacto,
	D1.[Elemento de trabajo],
	D1.[Contracto por],
	ISNULL(CONVERT(VARCHAR(10), D8.[Fecha revisada], 103),'') "Fecha revisada",
	D1.jobitemid,
	D1.Estado,
	D7.[Departamento / Procedimiento],
	D1.[Tiempo estándar (en min)],
	D1.[Sub-familia],
	D1.Marca,
	D1.Modelo,
	D1.[Número de Serie],
	D1.[Número de equipo],
	{fn CONCAT(
	{fn CONCAT(
	{fn CONCAT(
	{fn CONCAT(
		{fn CONCAT(
			{fn CONCAT(
				{fn CONCAT(
					{fn CONCAT(
						{fn CONCAT(
							ISNULL(D2.instructions,''),	ISNULL(D2.publicinstructions,''))},
								ISNULL(D4.instructions,''))},
									ISNULL(D4.publicinstructions,''))},
										ISNULL(D5.instructions,''))},
											ISNULL(D5.publicinstructions,''))},
												ISNULL(D3.requirement,''))},
													ISNULL(D2.range,''))},
														ISNULL(D4.range,''))},
															ISNULL(D5.range,''))}
	"Instrucciones de calibración",
	D1.currencycode "Moneda",
	D1.finalcost "Precio final",
	D1.costsource "Fuente del precio",
	ISNULL(D6.qno,'') "Presupuestos vinculados"
from
(
select 
	T1.jobno "Número de trabajo",
	T1.jobid,
	convert(varchar,T1.regdate,103) "Fecha Reg",
	T4.coname "Empresa",
	T4.coid,
	T3.subname "Subdivisión",
	{fn CONCAT({fn CONCAT(T2.firstname,' ')},T2.lastname)} "Contacto",
	T5.itemno "Elemento de trabajo",
	ISNULL({fn CONCAT({fn CONCAT(T16.firstname,' ')},T16.lastname)},'') "Contracto por",
	T5.jobitemid,
	T6.translation "Estado",
	T10.description "Sub-familia",
	T9.name Marca,
	T8.model Modelo,
	T8.modelid,
	T7.serialno "Número de Serie",
	T7.plantno "Número de equipo",
	T7.plantid,
	ISNULL(T15.estimatedTime,0) "Tiempo estándar (en min)",
	T18.currencycode,
	T17.finalcost,
	T17.costsource
FROM
	dbo.job T1
	inner join dbo.contact T2 on T1.personid = T2.personid
	inner join dbo.subdiv T3 on T2.subdivid = T3.subdivid
	inner join dbo.company T4 on T3.coid = T4.coid
	inner join dbo.jobitem T5 on T1.jobid = T5.jobid
	inner join dbo.itemstatetranslation T6 on T5.stateid = T6.stateid
	inner join dbo.itemstate T66 on T5.stateid = T66.stateid
	inner join dbo.instrument T7 on T5.plantid = T7.plantid
	inner join dbo.instmodel T8 on T7.modelid = T8.modelid
	inner join dbo.mfr T9 on T8.mfrid = T9.mfrid
	inner join dbo.description T10 on T8.descriptionid = T10.descriptionid
	inner join dbo.jobitemworkrequirement T11 on T5.jobitemid = T11.jobitemid
	inner join dbo.workrequirement T12 on T11.reqid = T12.id
	inner join dbo.department T13 on T12.deptid = T13.deptid
	inner join dbo.procs T15 on T12.procedureid = T15.id
	left outer join dbo.contact T16 on T5.lastContractReviewBy_personid = T16.personid
	left join dbo.costs_calibration T17 on T5.calcost_id = T17.costid
	inner join dbo.supportedcurrency T18 on T1.currencyid = T18.currencyid
WHERE
	T6.locale = 'es_ES'
	and T66.stateid not in (SELECT stateid FROM dbo.itemstate where active = 0 and type = 'workstatus')
	and T13.subdivid in (select T1.subdivid from dbo.subdiv T1 inner join dbo.company T2 on T1.coid = T2.coid where T1.coid = 6579)
	and T12.complete = 0
	and T1.typeid = 1
) as D1 
left outer join
-- Get jobitem calreq instructions and any range
	(
	select
		T11.jobitemid,
		T11.instructions,
		T11.publicinstructions,
		case when T11.rangeid is not NULL then {fn CONCAT({fn CONCAT({fn CONCAT(cast(T12.start as CHAR),' a ')},cast(T12.endd as CHAR))},T13.translation)} end range
	FROM
		dbo.calreq T11 
		left outer join dbo.modelrange T12 on T11.rangeid = T12.id 
		left outer join dbo.uomnametranslation T13 on T12.uomid = T13.id
	WHERE
		T11.active = 1
		and (T13.locale = 'es_ES' or T13.locale is NULL)
	) as D2 on D1.jobitemid = D2.jobitemid
	left outer join
-- Get jobitem requirements
	(
	select
		T15.jobitemid,
		T15.requirement
	FROM
		dbo.requirement T15
	WHERE
		T15.deleted = 0
	) as D3 on D1.jobitemid = D3.jobitemid
	left outer join
-- Get plantid calreq instructions and any range
	(
	select
		T11.plantid,
		T11.instructions,
		T11.publicinstructions,
		case when T11.rangeid is not NULL then {fn CONCAT({fn CONCAT({fn CONCAT(cast(T12.start as CHAR),' a ')},cast(T12.endd as CHAR))},T13.translation)} end range
	FROM
		dbo.calreq T11 
		left outer join dbo.modelrange T12 on T11.rangeid = T12.id 
		left outer join dbo.uomnametranslation T13 on T12.uomid = T13.id
	WHERE
		T11.active = 1
		and (T13.locale = 'es_ES' or T13.locale is NULL)
	) as D4 on D1.plantid = D4.plantid
	left outer join
-- Get model company calreq instructions and any range
	(
	select
		T11.modelid,
		T11.compid,
		T11.instructions,
		T11.publicinstructions,
		case when T11.rangeid is not NULL then {fn CONCAT({fn CONCAT({fn CONCAT(cast(T12.start as CHAR),' a ')},cast(T12.endd as CHAR))},T13.translation)} end range
	FROM
		dbo.calreq T11 
		left outer join dbo.modelrange T12 on T11.rangeid = T12.id 
		left outer join dbo.uomnametranslation T13 on T12.uomid = T13.id
	WHERE
		T11.active = 1
		and (T13.locale = 'es_ES' or T13.locale is NULL)
	) as D5 on D1.modelid = D5.modelid and D1.coid = D5.compid
	left join
-- Get all Quotes linked to a Job
	(
	SELECT 
		T1.jobid,
		STUFF((SELECT '; ' + T2.qno
				FROM
				(SELECT 
					  T1.jobid,
					  T2.qno
				FROM
					dbo.jobquotelink T1
					inner join dbo.quotation T2 on T1.quoteid = T2.id
				) T2
				WHERE 
					T1.jobid = T2.jobid
				ORDER BY
					T2.qno
				FOR XML PATH('')), 1, 1, '') [qno]
	FROM
		dbo.jobquotelink T1
		inner join dbo.quotation T2 on T1.quoteid = T2.id
	GROUP BY
		T1.jobid
	) as D6 on D1.jobid = D6.jobid
	left join
-- Get all job item work requirement Department / Procedures
	(
	SELECT 
		T1.jobitemid,
		STUFF((SELECT '; ' + T2.[Departamento / Procedimiento]
				FROM
				(SELECT
					T1.jobitemid
					,T3.name + ' / ' + T5.reference "Departamento / Procedimiento"
				FROM 
					dbo.jobitemworkrequirement T1
					inner join dbo.workrequirement T2 on T1.reqid = T2.id
					inner join dbo.department T3 on T2.deptid = T3.deptid
					inner join dbo.procs T5 on T2.procedureid = T5.id
				) T2
				WHERE 
					T1.jobitemid = T2.jobitemid
				ORDER BY
					T2.[Departamento / Procedimiento]
				FOR XML PATH('')), 1, 1, '') [Departamento / Procedimiento]
	FROM
		dbo.jobitemworkrequirement T1
		inner join dbo.workrequirement T2 on T1.reqid = T2.id
		inner join dbo.department T3 on T2.deptid = T3.deptid
		inner join dbo.procs T5 on T2.procedureid = T5.id
	GROUP BY
		T1.jobitemid
	) D7 on D1.jobitemid = D7.jobitemid
	left join 
-- Get last Contract Review date
	(
	SELECT
		T1.jobitemid
			 ,convert(date,max(T2.reviewdate)) "Fecha revisada"
	FROM
		dbo.contractreviewitem T1
		inner join dbo.contractreview T2 on T1.reviewid = T2.id
	GROUP BY
		T1.jobitemid
	) as D8 on D1.jobitemid = D8.jobitemid
WHERE
	D7.[Departamento / Procedimiento] like '%' + @Laboratorio +'%'
ORDER BY
	1,6