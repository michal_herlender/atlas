/****** Instrumentos con instrucciones a nivel de c�digo de barras y modelo de instrumento  ******/

SELECT
	S1.coid "Empresa ID",
	S1.coname "Empresa",
	S1.subname "Subdivisi�n",
	S1.plantid "C�digo de barras",
	S1.description + ' ' + S1.mfr + ' ' + S1.model "Nombre de modelo del instrumento",
	S1.customerdescription "Descripci�n cliente",
	S1.instrumentinstructions "Instrucciones del instrumento",
	S1.instrumentpublicinstructions "Instrumento de instrucciones p�blicas",
	S2.modelinstructions "Instrucciones del modelo de instrumento",
	S2.modelpublicinstructions "Instrucciones del modelo de instrumento p�blicas",
	ISNULL(S3.points,'') "Puntos de instrumentos",
	ISNULL(S4.points,'') "Puntos de modelo de instrumento"
FROM
(
SELECT 
		T5.coid
		,T2.pointsetid
		,T5.coname
		,T4.subname
		,T1.plantid
		,T1.modelid
		,T1.serialno
		,T1.plantno
		,T9.translation description
		,T6.model
		,case when T8.name = '/' then T7.name else T8.name end mfr
		,ISNULL(T1.customerdescription,'') customerdescription
		,ISNULL(replace(replace(T2.instructions,CHAR(10),' '),CHAR(13),' '),'') instrumentinstructions
		,ISNULL(replace(replace(T2.publicinstructions,CHAR(10),' '),CHAR(13),' '),'') instrumentpublicinstructions
  FROM 
	dbo.instrument T1
	inner join dbo.calreq T2 on T1.plantid = T2.plantid and T2.active = 1
	inner join dbo.contact T3 on T1.personid = T3.personid
	inner join dbo.subdiv T4 on T3.subdivid = T4.subdivid
	inner join dbo.company T5 on T4.coid = T5.coid
	inner join dbo.instmodel T6 on T1.modelid = T6.modelid
	inner join dbo.mfr T7 on T1.mfrid = T7.mfrid
	inner join dbo.mfr T8 on T6.mfrid = T8.mfrid
	inner join dbo.descriptiontranslation T9 on T6.descriptionid = T9.descriptionid and T9.locale = 'es_ES'
	inner join dbo.companysettingsforallocatedcompany T10 on T5.coid = T10.companyid and T10.orgid = 6579 and T10.active = 1
  where 
	T1.statusid <> 1
) S1
inner join 
(
SELECT  
	T1.modelid,
	T2.pointsetid,
	T2.compid,
	ISNULL(replace(replace(T2.instructions,CHAR(10),' '),CHAR(13),' '),'') modelinstructions,
	ISNULL(replace(replace(T2.publicinstructions,CHAR(10),' '),CHAR(13),' '),'') modelpublicinstructions   
  FROM 
  dbo.instmodel T1
  inner join dbo.calreq T2 on T1.modelid = T2.modelid and T2.active = 1
  inner join dbo.company T3 on T2.compid = T3.coid
  inner join dbo.companysettingsforallocatedcompany T4 on T3.coid = T4.companyid and T4.orgid = 6579 and T4.active = 1
) S2 on S1.coid = S2.compid and S1.modelid = S2.modelid
left outer join
(
SELECT 
	   T1.psid,
	   STUFF((SELECT ', ' + cast(T2.point as varchar) + ' ' + T2.shortname
			  FROM 
			  (SELECT T3.psid
					,T3.point
					,T4.shortname
				FROM dbo.calibrationpoint T3 inner join dbo.uom T4 on T3.uomid = T4.id
			  ) T2
			  WHERE T1.psid = T2.psid
			  FOR XML PATH('')), 1, 2, '') [points]
FROM 
	dbo.calibrationpoint T1
GROUP BY 
	T1.psid
) S3 on S1.pointsetid = S3.psid and S1.plantid is NOT NULL
left outer join
(
SELECT 
	   T1.psid,
	   STUFF((SELECT ', ' + cast(T2.point as varchar) + ' ' + T2.shortname
			  FROM 
			  (SELECT T3.psid
					,T3.point
					,T4.shortname
				FROM dbo.calibrationpoint T3 inner join dbo.uom T4 on T3.uomid = T4.id
			  ) T2
			  WHERE T1.psid = T2.psid
			  FOR XML PATH('')), 1, 2, '') [points]
FROM 
	dbo.calibrationpoint T1
GROUP BY 
	T1.psid
) S4 on S2.pointsetid = S4.psid
ORDER BY 
	1