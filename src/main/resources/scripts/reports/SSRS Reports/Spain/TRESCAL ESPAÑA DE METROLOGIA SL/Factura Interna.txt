/* Factura interna */

select 
	T1.invno "Número de factura interna"
	,T9.subname "Emitido por"
	,T2.coname "Trabajo realizado por la empresa"
	,T13.subname "y subdivisión"
	,convert(varchar,T1.invoicedate,103) "Fecha de facturación"
	,T1.invoicedate dte
	,convert(varchar,T1.issuedate,103) "Fecha de Emisión"
	,T1.issuedate issdte
	,T3.translation "Estado de la factura"
	,case when isnull(T1.accountstatus,'') ='P' then 'Pendiente de sincronización' else case when isnull(T1.accountstatus,'') ='S' then 'Sincronizada' else 'N/A' end end "Estado de cuentas"
	,T16.coname "Empresa cliente"
	,T12.jobno "Número de trabajo"
	,T11.itemno "Elemento de trabajo"
	,T7.currencycode "Moneda"
	,T10.finalcost "Precio final"
from 
	dbo.invoice T1
	inner join dbo.company T2 on T1.coid = T2.coid
	inner join dbo.invoicestatusdescriptiontranslation T3 on T1.statusid = T3.statusid
	inner join dbo.invoicetypetranslation T4 on T1.typeid = T4.id
	inner join dbo.company T5 on T1.orgid = T5.coid
	inner join dbo.supportedcurrency T6 on T5.currencyid = T6.currencyid
	inner join dbo.supportedcurrency T7 on T1.currencyid = T7.currencyid
	inner join dbo.contact T8 on T1.issueby = T8.personid
	inner join dbo.subdiv T9 on T8.subdivid = T9.subdivid
	inner join dbo.invoiceitem T10 on T1.id = T10.invoiceid
	inner join dbo.jobitem T11 on T10.jobitemid = T11.jobitemid
	inner join dbo.job T12 on T11.jobid = T12.jobid
	inner join dbo.subdiv T13 on T12.orgid = T13.subdivid
	inner join dbo.contact T14 on T12.personid = T14.personid
	inner join dbo.subdiv T15 on T14.subdivid = T15.subdivid
	inner join dbo.company T16 on T15.coid = T16.coid
where
	T1.typeid = 7
	and T1.orgid = 6579
	and T1.invoicedate between (@From) and (@To)
	and T9.subdivid in (@Subdivision)
	and T3.locale = 'es_ES'
	and T4.locale = 'es_ES'
order by
	T1.id,
	T13.subdivid,
	T10.jobitemid