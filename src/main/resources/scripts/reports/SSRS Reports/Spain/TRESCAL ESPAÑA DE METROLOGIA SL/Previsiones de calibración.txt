/****** Previsiones de calibraci�n - sin contracto  ******/

SELECT
	T14.orgid
	,T2.coname + ' - ' + T15.subname coname
	,T1.coid
	,T12.firstname + ' ' + T12.lastname defbusinesscontact
	,T13.subname defbusinesscontactsubname
	,T13.subdivid defbusinesscontactsubdivid
	,T1.plantid
	,T1.calfrequency
	,case T1.calintervalunit
		when 'DAY' then 'D�as'
		when 'WEEK' then 'Semanas'
		when 'MONTH' then 'Meses'
	end calintervalunit
	,CONVERT(VARCHAR(10), T1.recalldate, 103)  "Fecha de pr�xima calibraci�n"
	,T1.recalldate
	,YEAR(T1.recalldate) recalldateyear
	,MONTH(T1.recalldate) recalldatemonth
	,T1.serialno
	,T1.plantno
	,T1.customermanaged
	,case when T1.customerdescription <> '' then T1.customerdescription else T7.translation end description
	,case when T1.modelname <> '' then T1.modelname else T5.model end model
	,ISNULL(T10.name,T6.name) mfr
	,T8.firstname + ' ' + T8.lastname owner
	,T8.email
	,T9.addr1
	,ISNULL(T11.location,'') location
	,case when T16.shortname is NULL then '' else case when T16.shortname like '%OS%' then 'Si' else 'No' end end "on-site"
FROM 
	dbo.instrument T1
	inner join dbo.company T2 on T1.coid = T2.coid
	inner join dbo.instrumentstoragetype T3 on T1.storageid = T3.typeid
	inner join dbo.instrumentusagetype T4 on T1.usageid = T4.typeid
	inner join dbo.instmodel T5 on T1.modelid = T5.modelid
	inner join dbo.mfr T6 on T5.mfrid = T6.mfrid
	inner join dbo.descriptiontranslation T7 on T5.descriptionid = T7.descriptionid and T7.locale = 'es_ES'
	inner join dbo.contact T8 on T1.personid = T8.personid
	inner join dbo.address T9 on T1.addressid = T9.addrid
	inner join dbo.subdiv T15 on T9.subdivid = T15.subdivid
	inner join dbo.contact T12 on T2.defaultbusinesscontact = T12.personid
    inner join dbo.subdiv T13 on T12.subdivid = T13.subdivid
	inner join dbo.companysettingsforallocatedcompany T14 on T1.coid = T14.companyid
	left join dbo.addressplantillassettings T17 on T9.addrid = T17.addressId
	left join dbo.servicetype T16 on T1.defaultservicetype = T16.servicetypeid
	left join dbo.mfr T10 on T1.mfrid = T10.mfrid
	left join dbo.location T11 on T1.locationid = T11.locationid
WHERE
	T14.orgid = 6579
	and T17.contract = 0
	and T1.coid in (10153)
	and T14.active = 1
	and T1.statusid = 0
	and T3.recall = 1
	and T4.recall = 1
	and T1.plantid not in (SELECT T1.plantid FROM dbo.jobitem T1 inner join dbo.itemstate T2 on T1.stateid = T2.stateid and T2.active = 1)
	and recalldate between '2017-01-01' and '2017-12-31'
	/****** and recalldate between DATEADD(DAY,1,EOMONTH(Getdate(),-1)) and EOMONTH(Getdate(), 1 )  ******/
ORDER BY
	T2.coname + ' - ' + T15.subname,
	T1.recalldate