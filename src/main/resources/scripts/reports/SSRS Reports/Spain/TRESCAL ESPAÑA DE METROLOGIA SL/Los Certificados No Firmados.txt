/* Los Certificados No Firmados  */

select
	T1.certno "Certificate No" , 
	convert(varchar(10),T1.certdate,103) "Issue Date", 
	isnull(T11.certno,'') "Supplementary To", 
	T17.SERIALNO "Serial No", 
	T17.PLANTNO "Plant No", 
	T17.PLANTID "Barcode", 
	T4.translation "Status",
	T12.process "Cal Process",
	T5.reference "Procedure", 
	T6.name "Laboratory", 
	{fn CONCAT({fn CONCAT(T7.firstname,' ')},T7.lastname)} "Registered By",
	case when T14.shortname is NULL then '' else case when T14.shortname like '%OS%' then 'Si' else 'No' end end "In situ"
from 
	dbo.certificate T1 
	left join dbo.certificate T11 on T1.supplementaryforid = T11.certid
	inner join dbo.certlink T2 on T2.certid = T1.certid
	inner join dbo.jobitem T16 on T2.jobitemid = T16.jobitemid
	inner join dbo.instrument T17 on T16.plantid = T17.plantid
	inner join dbo.basestatusnametranslation T4 on T1.status = T4.statusid and T4.locale = 'es_ES' and T4.statusid NOT in (38,39)
	inner join dbo.calibration T8 on T1.calid = T8.id and T8.calibrationstatus NOT in (30,34)
	inner join dbo.categorisedprocedure T9 on T8.procedureid = T9.procid 
	inner join dbo.procs T5 on T5.id = T9.procid 
	inner join dbo.procedurecategory T10 on T9.categoryid = T10.id 
	inner join dbo.department T6 on T6.deptid = T10.departmentid 
	inner join dbo.contact T7 on T1.registeredby = T7.personid 
	inner join dbo.calibrationprocess T12 on T8.calprocessid = T12.id
	inner join dbo.calibrationtype T13 on T1.caltype = T13.caltypeid
	inner join dbo.servicetype T14 on T13.servicetypeid = T14.servicetypeid
	inner join dbo.subdiv T15 on T7.subdivid = T15.subdivid
where 
	T15.subdivid in (select subdivid from dbo.subdiv where coid = 6579)
order by 
	T1.certdate, 
	T2.jobitemid