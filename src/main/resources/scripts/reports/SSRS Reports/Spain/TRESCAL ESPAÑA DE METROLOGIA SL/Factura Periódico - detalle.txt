/* Factura Peri�dico - detalle */

SELECT 
	S1.[N�mero Factura Peri�dico],
	S1.Nominal,
	S1.[Fecha de facturaci�n],
	S1.[Estado de la factura],
	S1.[Estado de cuentas],
	S1.[Valor de Factura Peri�dico],
	S1.[Empresa cliente],
	S1.[Subdivisi�n cliente],
	S1.[Empresa facturada],
	S1.[Sub-familia],
	S1.Marca,
	S1.Modelo,
	S1.[C�digo de barras],
	S1.[N�mero de Serie],
	S1.[N�mero de equipo],
	S1.[N�mero de trabajo],
	S1.[N�mero de elemento],
	S1.[A�o admisi�n],
	S1.[Mes admisi�n],
	S1.[Fecha admisi�n],
	S1.[Fecha de envio],
	S1.JOBITEMID,
	S1.[Estado de elemento],
	S1.Valor,
	ISNULL(S2.comments,'') "Notas de revisi�n de contrato"
FROM
(
select distinct
	T1.invno "N�mero Factura Peri�dico"
	,{fn CONCAT({fn CONCAT(T7.code,' - ')},T8.translation)} "Nominal"
	,convert(varchar,T1.invoicedate,103) "Fecha de facturaci�n"
	,T4.translation "Estado de la factura"
	,case when isnull(T1.accountstatus,'') ='P' then 'Pendiente de sincronizaci�n' else case when isnull(T1.accountstatus,'') ='S' then 'Sincronizada' else '' end end "Estado de cuentas"
	,T1.totalcost "Valor de Factura Peri�dico"
	,ISNULL(T2.COMPANY,'') "Empresa cliente"
	,T2.subname "Subdivisi�n cliente"
	,T3.coname "Empresa facturada"
	,ISNULL(T2.DESCRIPTION,'') "Sub-familia"
	,ISNULL(T2.MFR,'') "Marca"
	,ISNULL(T2.INSTMODEL,'') "Modelo"
	,T2.PLANTID "C�digo de barras"
	,ISNULL(T2.SERIALNO,'') "N�mero de Serie"
	,ISNULL(T2.PLANTNO,'') "N�mero de equipo"
	,ISNULL(T2.JOBNO,'') "N�mero de trabajo"
	,ISNULL(T2.ITEMNO,'') "N�mero de elemento"
	,YEAR(T2.DATEIN) "A�o admisi�n"
	,MONTH(T2.DATEIN) "Mes admisi�n"
	,convert(varchar,T2.DATEIN,103) "Fecha admisi�n"
	,convert(varchar,case  when T2.LASTDISPATCHED is null then case when T2.FIRSTDISPATCHED is null then T2.LASTACTIVITY else T2.FIRSTDISPATCHED end else T2.LASTDISPATCHED end,103) "Fecha de envio"
	,T2.JOBITEMID JOBITEMID
	,ISNULL(T2.ITEMSTATUS,'') "Estado de elemento"
	,ISNULL(case when (T2.IHCAL + T2.TPCAL + T2.IHREP + T2.TPREP + T2.IHADJ + T2.TPADJ + T2.PURCHASE + T2.SER) = 0 then (T2.CRCAL + T2.CRREP + T2.CRADJ + T2.CRPUR) else (T2.IHCAL + T2.TPCAL + T2.IHREP + T2.TPREP + T2.IHADJ + T2.TPADJ + T2.PURCHASE + T2.SER) end - T2.INVfinalcost,0) "Valor"
FROM
	dbo.invoice T1 
	left join dbo.jobitemnotinvoiced T5 on T1.id = T5.periodicinvoiceid
	inner join dbo.invoiceitem T6 on T1.id = T6.invoiceid
	inner join dbo.nominalcode T7 on T6.nominalid = T7.id
	inner join dbo.nominalcodetitletranslation T8 on T6.nominalid = T8.id
	inner join dbo.invoicestatusdescriptiontranslation T4 on T1.statusid = T4.statusid
	left join dbo.KPI T2 on T5.jobitemid = T2.JOBITEMID
	left join dbo.company T3 on T1.coid = T3.coid
WHERE
	T1.typeid = 6
	and (T5.reason = 1 or T5.reason is null)
	and T6.nominalid <> 164
	and T1.orgid = 6579
	and T8.locale = 'es_ES'
	and T4.locale = 'es_ES'
) S1
left join
	(
	SELECT 
	T1.jobitemid,
	STUFF((SELECT '; ' + T2.comments
			FROM
			(SELECT 
			T3.reviewid,
			T3.jobitemid,
			T4.comments 
			FROM dbo.contractreview T4
			inner join dbo.contractreviewitem T3 on T4.id = T3.reviewid
			WHERE T4.comments <> ''
			) T2
			WHERE T1.jobitemid = T2.jobitemid
			FOR XML PATH('')), 1, 2, '') [comments]
	FROM
		dbo.contractreviewitem T1
	GROUP BY
		T1.jobitemid
) S2 on S1.jobitemid = S2.jobitemid
order by S1.[N�mero Factura Peri�dico]