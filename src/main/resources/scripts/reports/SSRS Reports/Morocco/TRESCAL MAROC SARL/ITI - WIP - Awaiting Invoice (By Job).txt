/**** Work In Progress - Awaiting Invoice (By Job) ****/

select distinct 
	D1.JobNo , 
	D1.DateComplete , 
	c14 Cal , 
	c13 Rep , 
	c12 Adj , 
	c11 Pur , 
	c10 Total , 
	c16 Company 
from 
	(select 
		T1.jobno, 
		sum(T1.total) c10 , 
		sum(T1.pur) c11 , 
		sum(T1.adj) c12 , 
		sum(T1.rep) c13 , 
		sum(T1.cal) c14 
	from 
		AWAITINGINVOICE T1 
	where 
		T1.coid <> T1.orgid
		and T1.orgid = 6685
	group by 
		T1.jobno
	) D2 inner join
	(select 
		T1.jobno, 
		T1.coname c16 , 
		convert(varchar,T1.datecomplete,103) DateComplete 
	from 
		AWAITINGINVOICE T1 
	where 
		T1.coid <> T1.orgid
		and T1.orgid = 6685
	) D1 on D1.jobno = D2.jobno 
order by
	1 asc , 
	8 asc , 
	2 asc