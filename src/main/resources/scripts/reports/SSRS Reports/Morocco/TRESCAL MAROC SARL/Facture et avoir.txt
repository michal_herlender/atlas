/**** Facture et avoir ****/

select 
	T1.invno "Invoice / Credit No."
	,T9.subname "Issued By"
	,T2.coname "Company"
	,T4.translation "Invoice Type"
	,convert(varchar,T1.invoicedate,103) "Invoice / Credit Date"
	,T1.invoicedate dte
	,{fn ROUND(CAST(T1.totalcost AS FLOAT) / (T7.exchangerate / T6.exchangerate),2)} + {fn ROUND(CAST(T1.carriage AS FLOAT) / (T7.exchangerate / T6.exchangerate),2)} "Value"
	,'Facture' "Invoice / Credit"
	,T3.translation "Invoice / Credit Status"
	,case when isnull(T1.accountstatus,'') ='P' then 'En attente de synchronisation' else case when isnull(T1.accountstatus,'') ='S' then 'Facture synchronisée' else 'N/A' end end "Synchronisation  Status"
from 
	dbo.invoice T1
	inner join dbo.company T2 on T1.coid = T2.coid
	inner join dbo.invoicestatusdescriptiontranslation T3 on T1.statusid = T3.statusid
	inner join dbo.invoicetypetranslation T4 on T1.typeid = T4.id
	inner join dbo.company T5 on T1.orgid = T5.coid
	inner join dbo.supportedcurrency T6 on T5.currencyid = T6.currencyid
	inner join dbo.supportedcurrency T7 on T1.currencyid = T7.currencyid
	inner join dbo.contact T8 on T1.issueby = T8.personid
	inner join dbo.subdiv T9 on T8.subdivid = T9.subdivid
where
	T1.typeid <> 5
	and T1.orgid = 6685
	and T3.locale = 'fr_FR'
	and T4.locale = 'fr_FR'
	and T1.invoicedate between (@Rango_de_fechas) and (@A)
	and T3.translation in (@Estado)
	and (case when isnull(T1.accountstatus,'') ='P' then 'En attente de synchronisation' else case when isnull(T1.accountstatus,'') ='S' then 'Facture synchronisée' else 'N/A' end end) in (@Estado_de_cuentas)
	and 'Facture' in (@Factura_abono)
UNION ALL
select 
	T1.creditnoteno "Invoice / Credit No."
	,T9.subname "Issued By"
	,T3.coname "Company"
	,'' " Invoice Type" 
	,convert(varchar,T1.regdate,103) "Invoice / Credit Date" 
	,T1.regdate dte
	, -{fn ROUND((CAST(T1.totalcost AS FLOAT) / (T7.exchangerate / T6.exchangerate)),2)} "Value" 
	,'Crédit' "Invoice / Credit"
	,T4.translation "Invoice / Credit Status"
	,case when isnull(T1.accountstatus,'') ='P' then 'En attente de synchronisation' else case when isnull(T1.accountstatus,'') ='S' then 'Facture synchronisée' else 'N/A' end end "Synchronisation  Status"
from 
	dbo.creditnote T1
	inner join dbo.invoice T2 on T1.invid = T2.id 
	inner join dbo.company T3 on T2.coid = T3.coid 
	inner join dbo.creditnotestatusdescriptiontranslation T4 on T1.statusid = T4.statusid 
	inner join dbo.company T5 on T1.orgid = T5.coid
	inner join dbo.supportedcurrency T6 on T5.currencyid = T6.currencyid
	inner join dbo.supportedcurrency T7 on T1.currencyid = T7.currencyid
	inner join dbo.contact T8 on T1.issueby = T8.personid
	inner join dbo.subdiv T9 on T8.subdivid = T9.subdivid
where
	T2.typeid <> 5
	and T1.orgid = 6685
	and T4.locale = 'fr_FR'
	and T1.regdate between (@Rango_de_fechas) and (@A)
	and T4.translation in (@Estado)
	and (case when isnull(T1.accountstatus,'') ='P' then 'En attente de synchronisation' else case when isnull(T1.accountstatus,'') ='S' then 'Facture synchronisée' else 'N/A' end end) in (@Estado_de_cuentas)
	and 'Crédit' in (@Factura_abono)
order by 
	1