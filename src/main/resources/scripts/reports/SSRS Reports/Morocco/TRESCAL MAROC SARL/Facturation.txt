/* Facturation */

SELECT 
	i.id AS invId
	,i.invno AS invNo
	-- Ship To
	,inv_comp.coid AS sellTo_id	
	-- Bill To 
	,inv_comp.coid AS billTo_id
	,inv_comp.coname AS billTo_name
	,inv_comp.fiscalidentifier AS billTo_fiscalId
	,(ISNULL(inv_addr.addr1, '') + CHAR(32) + ISNULL(inv_addr.addr2, '') + CHAR(32) + ISNULL(inv_addr.addr3, '')) as billTo_address
	,inv_addr.town AS billTo_city
	,inv_addr.postcode AS billTo_postCode
	,inv_addr.county AS billTo_county
	,inv_country.countrycode AS billTo_countryCode
	,(inv_defcont.firstname + CHAR(32) + inv_defcont.lastname) AS billTo_contact
	,inv_defcont.email AS billTo_email
	-- Ship To
	,del_comp.coid AS shipTo_id
	,del_comp.coname AS shipTo_name	
	,(ISNULL(del_addr.addr1, '') + CHAR(32) + ISNULL(del_addr.addr2, '') + CHAR(32) + ISNULL(del_addr.addr3, '')) as shipTo_address
	,del_addr.town AS shipTo_city
	,del_addr.postcode AS shipTo_postCode
	,del_addr.county AS shipTo_county
	,del_country.countrycode AS shipTo_countryCode
	,(ISNULL(del_cont.firstname, '') + CHAR(32) + ISNULL(del_cont.lastname, '')) AS shipTo_contact
	,del_cont.email AS shipTo_email		
	-- Others data on invoice
	,i.accountstatus AS accountStatus
	,CONVERT(VARCHAR(10), GETDATE(), 104) AS currentDate
	,i.regdate AS regDate
	,i.issuedate AS issueDate
	,i.invoicedate AS invoiceDate
	,i.duedate AS dueDate
	,i.paymentterms AS paymentTermsCode
	--,pt.code AS paymentTermsCode
	,pm.code AS paymentModeCode
	,i.period AS period
	,sc.currencycode AS currencyCode
	,i.rate AS currencyRate
	,i.totalcost AS totalAmountExclTax
	,i.finalcost AS totalAmountInclTax
	-- Invoice Lines
	,ii.itemno AS itemNo
	,'Item' AS type
	,nc.code AS nominalCode
	,nc.title AS nominalDesc
	,ii.quantity AS quantity
	,ii.totalcost AS unitPrice
	,i.vatrate AS vatRate
	,(ii.finalcost * i.vatrate / 100) AS vatValue
	,CASE 
		WHEN vr.type = 0 THEN 'NATIONAL'
		WHEN vr.type = 1 AND vr.countryid = 162 THEN 'EXPORT'
		WHEN vr.type = 1 AND vr.countryid != 162 THEN vr.description
		WHEN vr.type = 2 THEN 'UE'
		WHEN vr.type = 3 THEN 'EXPORT'
		WHEN vr.type = 4 THEN 'EXPORT'
		ELSE NULL
		END AS vatZone
	,ii.discountrate AS discountRate
	,ii.discountvalue AS discountValue
	,ii.finalcost AS amountExclTax
	,CASE 
		WHEN i.vatrate IS NULL
			THEN ii.finalcost
		ELSE CAST(ii.finalcost + (ii.finalcost * i.vatrate / 100) AS NUMERIC(10,2))
		END AS amountInclTax	
	,CASE 
		WHEN ii.itemno = 0 -- !!! Specific case for carriage !!! 
			THEN 
			-- Use the analytical center of the first item where the subdivision is filled, else use the analytical center of the subdiv of the creator of the invoice
			COALESCE(
					(SELECT TOP 1 subdiv.analyticalCenter
					FROM dbo.invoiceitem
						INNER JOIN dbo.subdiv ON subdiv.subdivid = invoiceitem.orgid
					WHERE invoiceitem.invoiceid = ii.invid
						AND invoiceitem.orgid IS NOT NULL
					ORDER BY invoiceitem.itemno),
					inv_subdivcreator.analyticalcenter)
		ELSE 
			-- Use, by priority, the analytical center of: 
				-- 1. subdiv defined at invoice item
				-- 2. subdiv that created the job
				-- 3. subdiv of the creator of the invoice
			COALESCE(invitem_subdiv.analyticalCenter, job_subdiv.analyticalCenter, inv_subdivcreator.analyticalcenter)
		END AS analyticalCenter
	,job.jobno AS jobNo
	,STUFF((SELECT ';' + del.deliveryno	
		FROM deliveryitem AS deli
			INNER JOIN delivery AS del ON del.deliveryid = deli.deliveryid
		WHERE deli.jobitemid = ii.jobitemid
			AND deliveryitemtype = 'job' AND deli.deliveryid IN (
				SELECT tmp.deliveryid
				FROM delivery tmp
				WHERE tmp.deliverytype = 'client')
		FOR XML PATH('')),1,1,'') AS deliveryNo	
	,STUFF((SELECT ';' + ISNULL(po.ponumber, bpo.ponumber)	
		FROM jobitempo AS jipo
			LEFT JOIN po AS po ON po.poid = jipo.poid
			LEFT JOIN bpo AS bpo ON bpo.poid = jipo.bpoid
		WHERE jipo.jobitemid = ii.jobitemid
		FOR XML PATH('')),1,1,'') AS purchaseOrderNo
	,bus.coid AS busId
	,bus.coname AS busName	
FROM 
	invoice AS i
	INNER JOIN company AS inv_comp ON inv_comp.coid = i.coid
	INNER JOIN address AS inv_addr ON inv_addr.addrid = i.addressid
	INNER JOIN subdiv AS inv_subdiv ON inv_subdiv.subdivid = inv_addr.subdivid
	INNER JOIN country AS inv_country ON inv_country.countryid = inv_addr.countryid
	INNER JOIN supportedcurrency AS sc ON sc.currencyid = i.currencyid
	INNER JOIN company AS bus ON bus.coid = i.orgid
	--LEFT JOIN paymentterms AS pt ON pt.id = i.paymenttermsid
	LEFT JOIN paymentmode AS pm ON pm.id = i.paymentmodeid
	LEFT JOIN vatrate AS vr ON vr.vatcode = i.vatcode
	LEFT JOIN new_invoiceaccounts_costssummary AS ii ON ii.invid = i.id
	LEFT JOIN nominalcode AS nc ON nc.id = ii.nominalid
	LEFT JOIN contact AS inv_defcont ON inv_defcont.personid = inv_subdiv.defpersonid
	-- Get subdivision that created the job
	LEFT JOIN jobitem ON jobitem.jobitemid = ii.jobitemid
	LEFT JOIN job ON job.jobid = jobitem.jobid
	LEFT JOIN subdiv AS job_subdiv ON job_subdiv.subdivid = job.orgid
	-- Get subdivision that created the invoice
	LEFT join contact AS inv_creator ON inv_creator.personid = i.createdby
	LEFT JOIN subdiv AS inv_subdivcreator ON inv_subdivcreator.subdivid = inv_creator.subdivid
	-- Get subdivision of invoiceitem
	LEFT JOIN subdiv AS invitem_subdiv ON invitem_subdiv.subdivid = ii.subdivid
	-- Get unique delivery address for all the job
	LEFT JOIN invoicejoblink AS jlsingleaddr ON jlsingleaddr.invoiceid = i.id
		AND jlsingleaddr.jobid = (
			SELECT MAX (tmp.jobid)
			FROM invoicejoblink tmp
				,delivery tmp2
			WHERE tmp.invoiceid = jlsingleaddr.invoiceid
				AND tmp2.jobid = tmp.jobid
				AND deliverytype = 'client'
			HAVING count(distinct(tmp2.addressid)) <= 1 
				)
	LEFT JOIN delivery AS delsingleaddr ON delsingleaddr.jobid = jlsingleaddr.jobid
		AND delsingleaddr.deliveryid = (
			SELECT MAX(tmp.deliveryid)
			FROM delivery tmp 
			WHERE tmp.jobid = jlsingleaddr.jobid
			AND delsingleaddr.deliverytype = 'client'
			)
	LEFT JOIN address AS del_addr ON del_addr.addrid = delsingleaddr.addressid
	LEFT JOIN subdiv AS del_subdiv ON del_subdiv.subdivid = del_addr.subdivid
	LEFT JOIN company AS del_comp ON del_comp.coid = del_subdiv.coid
	LEFT JOIN country AS del_country ON del_country.countryid = del_addr.countryid
	-- Get unique delivery contact for the all the job
	LEFT JOIN invoicejoblink AS jlsinglecon ON jlsinglecon.invoiceid = i.id
		AND jlsinglecon.jobid = (
			SELECT MAX (tmp.jobid)
			FROM invoicejoblink tmp
				,delivery tmp2
			WHERE tmp.invoiceid = jlsinglecon.invoiceid
				AND tmp2.jobid = tmp.jobid
				AND deliverytype = 'client'
			HAVING count(distinct(tmp2.contactid)) <= 1
				)
	LEFT JOIN delivery AS delsinglecon ON delsinglecon.jobid = jlsinglecon.jobid
		AND delsinglecon.deliveryid = (
			SELECT MAX(tmp.deliveryid)
			FROM delivery tmp 
			WHERE tmp.jobid = jlsinglecon.jobid
			AND delsinglecon.deliverytype = 'client'
			)
	LEFT JOIN contact AS del_cont ON del_cont.personid = delsinglecon.contactid
WHERE
	bus.coid = 6685
	and i.accountstatus in (@AccountsStatus)
	and i.issueedate between (@From) and (@To)