/**** WIP - Production non comptabilisée ****/

SELECT 
	T1.jobno JobNo
	,T1.jobtype
	,T1.itemno ItemNo
	,CONCAT(CONCAT(CONCAT(CONCAT(T1.subfamily,' '),T1.mfr),' '),T1.model) InstModelName
	,CASE WHEN d.name IS NOT NULL THEN d.name ELSE 'UNKNOWN' END as Lab
	,ISNULL(convert(varchar,T1.caldate,103),'') CalDate
	,ISNULL(convert(varchar,T1.datecomplete,103),'') DateComplete
	,T1.cal Cal
	,T1.rep Rep
	,T1.adj Adj
	,T1.pur Pur
	,T1.ser Ser
	,T1.total Total
	,T1.coname Company
	,T1.legalidentifier LegalIdentifier
FROM dbo.NOTACCOUNTEDPRODUCTION T1
	inner join jobitem ji on ji.jobitemid = T1.jobitemid 
	left outer join jobitemworkrequirement jiwr on jiwr.id = ji.nextworkreqid 
	left outer join workrequirement wr on jiwr.reqid = wr.id 
	left outer join department d on d.deptid = wr.deptid 
where 
	T1.coid <> T1.orgid
	and T1.orgid = 6685
order by T1.jobno, T1.jobitemid