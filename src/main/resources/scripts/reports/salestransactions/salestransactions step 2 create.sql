USE [spain]

BEGIN TRAN

CREATE TABLE SALES_TRANSACTIONS
(ORGID INT NULL,
CODE VARCHAR(26) NULL,
NAME VARCHAR(67) NULL,
AREA VARCHAR(67) NULL,
NETT FLOAT NULL,
VAT FLOAT NULL,
GROSS FLOAT NULL,
DATE DATETIME NULL,
CATEGORY VARCHAR(50) NULL,
DETPRIMARY FLOAT NULL,
LASTMODIFIED DATETIME NULL
);
INSERT into SALES_TRANSACTIONS
select T1.orgid 
,T1.code
, {fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})} Name
, case  when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%SALES%' then 'Sales' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%HIRE%' then 'Hire' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%PRESSURE%' then 'Pressure' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%ELECTRICAL%' then 'Electrical' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%TEMPERATURE%' then 'Temperature' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%ORIFICE%' then 'Orifice' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%VALVE%' then 'Valve' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%HUMIDITY%' then 'Humidity' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%DEWPOINT%' then 'Dewpoint' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%FLOW%' then 'Flow' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%GAS%' then 'Gas Detection' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%DIMENSIONAL%' then 'Dimensional' 
else T1.TITLE end  Area
, ISNULL(T1.cost,(T1.altcost / T1.rate)) Nett
, 0 Vat
, 0 Gross
, DATEADD(dd, 0, DATEDIFF(dd, 0, T1.invoicedate)) Date
, case  when {fn LEFT(T1.CODE,1)} = '9' then 'Grimsby' when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%VALVE%' then 'Valves' else 'Calibration' end Category
, T1.ID DetPrimary
, CURRENT_TIMESTAMP Lastmodified
from INVACCS T1
UNION ALL
select T1.orgid 
,T1.code
, {fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})} Name
, case  when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%SALES%' then 'Sales' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%HIRE%' then 'Hire' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%PRESSURE%' then 'Pressure' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%ELECTRICAL%' then 'Electrical' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%TEMPERATURE%' then 'Temperature' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%ORIFICE%' then 'Orifice' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%VALVE%' then 'Valve' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%HUMIDITY%' then 'Humidity' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%DEWPOINT%' then 'Dewpoint' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%FLOW%' then 'Flow' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%GAS%' then 'Gas Detection' 
when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%DIMENSIONAL%' then 'Dimensional' 
else T1.TITLE end  Area
, ISNULL(-T1.totalcost,(-T1.altcost / T1.rate)) Nett
, 0 Vat
, 0 Gross
, DATEADD(dd, 0, DATEDIFF(dd, 0, T1.invoicedate)) Date
, case  when {fn LEFT(T1.CODE,1)} = '9' then 'Grimsby' when {fn UCASE({fn CONCAT({fn CONCAT(T1.TITLE,' ')},{fn RIGHT(T1.CODE,5)})})} like '%VALVE%' then 'Valves' else 'Calibration' end Category
, T1.ID DetPrimary
, CURRENT_TIMESTAMP Lastmodified
from CREDNOTEACCS T1;

COMMIT TRAN