USE [spain]

BEGIN TRAN

drop table JOBS;
drop table QUOTATIONS_TMP1;
drop table QUOTATIONS_TMP2;
drop table QUOTATIONS;
drop table CERTIFICATES;
drop table CALIBRATIONS;
drop table TPQUOTES;
drop table INSTRUMENTS;
drop table COMPANIES;
drop table MAXJOBCOSTINGFORJOBITEM;
drop table MAXQUOTATIONVER;

COMMIT TRAN