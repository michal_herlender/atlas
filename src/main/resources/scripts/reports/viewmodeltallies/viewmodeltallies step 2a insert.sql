USE [spain]

BEGIN TRAN

INSERT INTO JOBS
select
T4.orgid,
T1.modelid, 
count(T2.jobitemid) jobs
from dbo.instrument T3, dbo.instmodel T1, dbo.jobitem T2, dbo.job T4
where T3.modelid = T1.modelid 
and T3.plantid = T2.plantid
and T2.jobid = T4.jobid
group by T4.orgid, T1.modelid 
;
INSERT INTO QUOTATIONS_TMP1
select T2.orgid, T1.modelid, case  when T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-2,convert(datetime,CURRENT_TIMESTAMP))} then 'Last 2 years' else 'Older' end regdate, count(distinct T3.quoteid) quotes 
from dbo.quotationitem T3, dbo.instmodel T1, dbo.quotation T2
where T3.modelid = T1.modelid and T2.id = T3.quoteid
group by T2.orgid, T1.modelid, case  when T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-2,convert(datetime,CURRENT_TIMESTAMP))} then 'Last 2 years' else 'Older' end 
;
INSERT INTO QUOTATIONS_TMP2
select T2.orgid, T1.modelid, case  when T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-2,convert(datetime,CURRENT_TIMESTAMP))} then 'Last 2 years' else 'Older' end regdate, count(distinct T3.quoteid) quotes 
from dbo.instrument T4, dbo.instmodel T1, dbo.quotationitem T3, dbo.quotation T2
where T4.modelid = T1.modelid and T3.plantid = T4.plantid and T2.id = T3.quoteid
group by T2.orgid, T1.modelid, case  when T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-2,convert(datetime,CURRENT_TIMESTAMP))} then 'Last 2 years' else 'Older' end 
;
INSERT INTO QUOTATIONS
select case when T1.ORGID is null then T2.ORGID else T1.ORGID end  orgid, case  when T1.MODELID is null then T2.MODELID else T1.MODELID end  modelid , sum(case  when (case  when T1.REGDATE is null then T2.REGDATE else T1.REGDATE end  = 'Older') then (case  when T1.QUOTES is null then 0 else T1.QUOTES end  + case  when T2.QUOTES is null then 0 else T2.QUOTES end ) else 0 end ) + sum(case  when (case  when T1.REGDATE is null then T2.REGDATE else T1.REGDATE end  = 'Last 2 years') then (case  when T1.QUOTES is null then 0 else T1.QUOTES end  + case  when T2.QUOTES is null then 0 else T2.QUOTES end ) else 0 end ) Quotes , sum(case  when case  when T1.REGDATE is null then T2.REGDATE else T1.REGDATE end  = 'Last 2 years' then case  when T1.QUOTES is null then 0 else T1.QUOTES end  + case  when T2.QUOTES is null then 0 else T2.QUOTES end  else 0 end ) Last2years 
from (dbo.QUOTATIONS_TMP1 T1 FULL OUTER JOIN dbo.QUOTATIONS_TMP2 T2 on T1.MODELID = T2.MODELID and T1.REGDATE = T2.REGDATE and T1.ORGID = T2.ORGID)
group by case when T1.ORGID is null then T2.ORGID else T1.ORGID end, case  when T1.MODELID is null then T2.MODELID else T1.MODELID end 
;
INSERT INTO CERTIFICATES
select T5.orgid, T1.modelid , count(T2.certid) certs
from dbo.instrument T3, dbo.instmodel T1, dbo.jobitem T4, dbo.certlink T2, dbo.job T5
where T3.modelid = T1.modelid and T3.plantid = T4.plantid and T2.jobitemid = T4.jobitemid and T4.jobid = T5.jobid
group by T5.orgid, T1.modelid
;
INSERT INTO CALIBRATIONS
select T5.orgid, T1.modelid, count(T2.calid) cals 
from dbo.instrument T3, dbo.instmodel T1, dbo.jobitem T4, dbo.callink T2, dbo.job T5
where T3.modelid = T1.modelid and T3.plantid = T4.plantid and T2.jobitemid = T4.jobitemid and T4.jobid = T5.jobid
group by T5.orgid, T1.modelid
;
INSERT INTO TPQUOTES
select T3.orgid, T1.modelid, count(distinct T2.tpquoteid) tpquotes 
from dbo.instmodel T1, dbo.tpquotationitem T2, dbo.tpquotation T3
where T1.modelid = T2.modelid and T2.tpquoteid = T3.id
group by T3.orgid, T1.modelid
;
INSERT INTO INSTRUMENTS
select T1.modelid, count(T2.plantid) instruments 
from dbo.instrument T2, dbo.instmodel T1
where T2.modelid = T1.modelid
group by T1.modelid
;
INSERT INTO COMPANIES
select T1.modelid, count(distinct T2.coid) companies 
from dbo.instrument T3, dbo.instmodel T1, dbo.company T2
where T3.modelid = T1.modelid and T3.coid = T2.coid
group by T1.modelid
;
INSERT INTO VIEWMODELTALLIES
select distinct T1.orgid, T1.modelid,
0,
0,
0,
0,
0,
0,
0,
0,
CURRENT_TIMESTAMP LASTMODIFIED
from (
select orgid, modelid from dbo.jobs
union
select orgid, modelid from dbo.quotations
union
select orgid, modelid from dbo.certificates
union
select orgid, modelid from dbo.calibrations
union
select orgid, modelid from dbo.quotations
union
select orgid, modelid from dbo.tpquotes
union
select null, modelid from dbo.instruments
union
select null, modelid from dbo.companies
) T1 order by 2 asc
;

UPDATE dbo.VIEWMODELTALLIES
	SET JOBS=T1.JOBS
	FROM
	dbo.JOBS T1
	WHERE dbo.VIEWMODELTALLIES.modelid = T1.modelid and dbo.VIEWMODELTALLIES.orgid = T1.orgid
;
GO

UPDATE dbo.VIEWMODELTALLIES
	SET QUOTATIONS=T1.QUOTES,LAST2YEARSQUOTATIONS=T1.LAST2YEARS
	FROM
	dbo.QUOTATIONS T1
	WHERE dbo.VIEWMODELTALLIES.modelid = T1.modelid and dbo.VIEWMODELTALLIES.orgid = T1.orgid
;
GO

UPDATE dbo.VIEWMODELTALLIES
	SET CERTIFICATES=T1.CERTS
	FROM
	dbo.CERTIFICATES T1
	WHERE dbo.VIEWMODELTALLIES.modelid = T1.modelid and dbo.VIEWMODELTALLIES.orgid = T1.orgid
;
GO

UPDATE dbo.VIEWMODELTALLIES
	SET CALIBRATIONS=T1.CALS
	FROM
	dbo.CALIBRATIONS T1
	WHERE dbo.VIEWMODELTALLIES.modelid = T1.modelid and dbo.VIEWMODELTALLIES.orgid = T1.orgid
;
GO

UPDATE dbo.VIEWMODELTALLIES
	SET TPQUOTATIONS=T1.TPQUOTES
	FROM
	dbo.TPQUOTES T1
	WHERE dbo.VIEWMODELTALLIES.modelid = T1.modelid and dbo.VIEWMODELTALLIES.orgid = T1.orgid
;
GO

UPDATE dbo.VIEWMODELTALLIES
	SET INSTRUMENTS=T1.INSTRUMENTS
	FROM
	dbo.INSTRUMENTS T1
	WHERE dbo.VIEWMODELTALLIES.modelid = T1.modelid
;
GO

UPDATE dbo.VIEWMODELTALLIES
	SET COMPANIES=T1.COMPANIES
	FROM
	dbo.COMPANIES T1
	WHERE dbo.VIEWMODELTALLIES.modelid = T1.modelid
;
GO

INSERT INTO MAXJOBCOSTINGFORJOBITEM
select jobitemid,max(costingid) costingid
from
(
select T1.jobitemid , T2.costingid
from jobitem T1, jobcostingitem T2
where T1.jobitemid = T2.jobitemid
) D1
group by jobitemid
;

INSERT INTO VIEWMODELJOBCOSTINGS
select 
	T2.orgid
	,T1.modelid 
	,T2.id jobcostingid
	,T3.id jobcostingitemid
	,T4.coid
	,T5.personid
	,T9.jobitemid
	,T10.subdivid
	, {fn CONCAT({fn CONCAT(T14.jobno ,' ver ')},CAST(T2.ver AS CHAR(254)))} "Cost No"
	, T9.itemno Item
	, T4.coname Company
	, {fn CONCAT({fn CONCAT(T5.firstname,' ')},T5.lastname)} Contact
	, T13.shortname "Cal Type"
	, CAST(T2.regdate AS DATE) "Reg Date"
	, T3.discountrate Discount
	, T12.finalcost "Final Cost"
	, T11.currencyersymbol
	, T9.turn
	, T13.displaycolour
	, T13.displaycolourfasttrack
	, 0 onstop
	, 0 companyactive
	, 0 contactactive
	,CURRENT_TIMESTAMP LASTMODIFIED
from 
	dbo.instrument T8, 
	dbo.instmodel T1, 
	dbo.jobitem T9, 
	dbo.jobcostingitem T3, 
	dbo.jobcosting T2, 
	dbo.contact T5, 
	dbo.subdiv T10, 
	dbo.company T4, 
	dbo.calibrationtype T6, 
	dbo.MAXJOBCOSTINGFORJOBITEM T7,
	dbo.supportedcurrency T11,
	dbo.costs_calibration T12,
	dbo.servicetype T13,
	dbo.job T14
	where T8.modelid = T1.modelid 
	and T8.plantid = T9.plantid 
	and T9.jobitemid = T3.jobitemid 
	and T2.id = T3.costingid 
	and T5.personid = T2.personid 
	and T5.subdivid = T10.subdivid 
	and T10.coid = T4.coid 
	and T9.caltypeid = T6.caltypeid 
	and T9.jobid = T14.jobid
	and T6.servicetypeid = T13.servicetypeid
	and T7.JOBITEMID = T3.jobitemid 
	and T7.COSTINGID = T3.costingid 
	and T11.currencyid = T2.currencyid
	and T3.calcost_id = T12.costid
	and NOT T7.JOBITEMID is null 
	and NOT T7.COSTINGID is null 
	and T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-5,convert(datetime,CURRENT_TIMESTAMP))}
order by 
	1 asc , 7 desc, 3 asc
;

UPDATE dbo.VIEWMODELJOBCOSTINGS
	SET COMPANYACTIVE=T1.ACTIVE,CONTACTACTIVE=T1.ACTIVE
	FROM
	dbo.companysettingsforallocatedcompany T1
	WHERE dbo.VIEWMODELJOBCOSTINGS.orgid = T1.orgid and dbo.VIEWMODELJOBCOSTINGS.COID = T1.COMPANYID
;
GO

INSERT INTO MAXQUOTATIONVER
select qno,max(id) id
from quotation T1
group by qno
;

INSERT INTO VIEWMODELQUOTATIONS
select 
	T2.orgid 
	,T1.modelid
	,T2.id quotationid
	,T3.id quotationitemid
	,T4.coid
	,T5.personid
	,T9.subdivid
	, {fn CONCAT({fn CONCAT(T2.qno,' rev ')},CAST(CAST(T2.ver AS INTEGER) AS CHAR(254)))} "Quote No"
	, T3.itemno Item
	, T4.coname Company
	, {fn CONCAT({fn CONCAT(T5.firstname,' ')},T5.lastname)} Contact
	, T12.shortname "Cal Type"
	, CAST(T2.regdate AS DATE) "Reg Date"
	, T3.discountrate Discount
	, T11.finalcost "Final Cost"
	, T10.currencyersymbol
	, T12.displaycolour
	, 0 onstop
	, 0 companyactive
	, 0 contactactive
	,CURRENT_TIMESTAMP LASTMODIFIED
from 
	dbo.instrument T8, 
	dbo.instmodel T1, 
	dbo.quotationitem T3, 
	dbo.quotation T2, 
	dbo.contact T5, 
	dbo.subdiv T9, 
	dbo.company T4, 
	dbo.calibrationtype T6, 
	dbo.MAXQUOTATIONVER T7,
	dbo.supportedcurrency T10,
	dbo.costs_calibration T11,
	dbo.servicetype T12
where 
	T8.modelid = T1.modelid 
	and T8.plantid = T3.plantid 
	and T2.id = T3.quoteid 
	and T5.personid = T2.personid 
	and T9.subdivid = T5.subdivid 
	and T4.coid = T9.coid 
	and T3.caltype_caltypeid = T6.caltypeid 
	and T6.servicetypeid = T12.servicetypeid
	and T2.id = T7.ID 
	and T10.currencyid = T2.currencyid
	and T3.calcost_id = T11.costid
	and T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-5,convert(datetime,CURRENT_TIMESTAMP))}
	and NOT T7.ID is null
UNION
select 
	T2.orgid
	,T1.modelid
	,T2.id quotationid
	,T3.id quotationitemid
	,T4.coid
	,T5.personid
	,T8.subdivid
	, {fn CONCAT({fn CONCAT(T2.qno,' rev ')},CAST(CAST(T2.ver AS INTEGER) AS CHAR(254)))} "Quote No" 
	, T3.itemno Item
	, T4.coname Company
	, {fn CONCAT({fn CONCAT(T5.firstname,' ')},T5.lastname)} Contact
	, T12.shortname "Cal Type"
	, CAST(T2.regdate AS DATE) "Reg Date"
	, T3.discountrate Discount
	, T11.finalcost "Final Cost"
	, T9.currencyersymbol
	, T12.displaycolour
	, 0 onstop
	, 0 companyactive
	, 0 contactactive
	,CURRENT_TIMESTAMP LASTMODIFIED
from 
	dbo.instmodel T1, 
	dbo.quotationitem T3, 
	dbo.quotation T2, 
	dbo.contact T5, 
	dbo.subdiv T8, 
	dbo.company T4, 
	dbo.calibrationtype T6, 
	dbo.MAXQUOTATIONVER T7,
	dbo.supportedcurrency T9,
	dbo.costs_calibration T11,
	dbo.servicetype T12
	where T1.modelid = T3.modelid 
	and T2.id = T3.quoteid 
	and T5.personid = T2.personid 
	and T8.subdivid = T5.subdivid 
	and T4.coid = T8.coid 
	and T3.caltype_caltypeid = T6.caltypeid 
	and T6.servicetypeid = T12.servicetypeid
	and T2.id = T7.ID 
	and T9.currencyid = T2.currencyid
	and T3.calcost_id = T11.costid
	and T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-5,convert(datetime,CURRENT_TIMESTAMP))}
	and NOT T7.ID is null
order by 
	1 asc , 7 desc
;

UPDATE dbo.VIEWMODELQUOTATIONS
	SET COMPANYACTIVE=T1.ACTIVE,CONTACTACTIVE=T1.ACTIVE
	FROM
	dbo.companysettingsforallocatedcompany T1
	WHERE dbo.VIEWMODELQUOTATIONS.orgid = T1.orgid and dbo.VIEWMODELQUOTATIONS.COID = T1.COMPANYID
;
GO

COMMIT TRAN