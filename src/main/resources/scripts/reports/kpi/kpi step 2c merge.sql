USE [spain]

BEGIN TRAN

PRINT 'UPDATE KPI 5'

UPDATE dbo.KPI
	SET ihcal=T1.ihcal,tpcal=T1.tpcal,ihrep=T1.ihrep,tprep=T1.tprep,ihadj=T1.ihadj,tpadj=T1.tpadj,trescaltpcal=T1.trescaltpcal,trescaltprep=T1.trescaltprep,trescaltpadj=T1.trescaltpadj,purchase=T1.purchase,inspect=T1.inspect,tpcarr=T1.tpcarr
	FROM 
	IHTPCR_TMP T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 5b'

UPDATE dbo.KPI
	SET INVfinalcost=S1.INVfinalcost, INVOICED = 1
	FROM
	(
	SELECT
		T2.jobitemid, 
		sum(T2.finalcost) INVfinalcost 
	from 
		dbo.invoice T1	
		inner join dbo.invoiceitem T2 on T1.id = T2.invoiceid
		left outer join dbo.creditnote T3 on T1.id = T3.invid
	where 
		T2.jobitemid is NOT NULL 
		and T1.typeid not in (5,7)
		and T3.id is NULL
	group by 
		T2.jobitemid
	) S1
	WHERE dbo.KPI.JOBITEMID = S1.jobitemid
;
GO
	

PRINT 'UPDATE KPI 6'

UPDATE dbo.KPI
	SET CALfinalcost=S1.CALfinalcost
	FROM
	(
	select 
		T1.jobitemid , 
		sum(T2.finalcost) CALfinalcost 
	from 
		dbo.invoice T6
		INNER JOIN dbo.invoiceitem T1 on T6.id = T1.invoiceid
		LEFT JOIN dbo.costs_calibration T2 on T1.calcost_id = T2.costid
		LEFT OUTER JOIN dbo.creditnote T3 on T6.id = T3.invid
	where 
		NOT T1.jobitemid is null 
		and T6.typeid not in (5,7)
		and T2.costtype = 'invoice' 
		and T2.active = 1 
		and T3.id is NULL
	group by 
		T1.jobitemid
	) S1
	WHERE dbo.KPI.JOBITEMID = S1.jobitemid
;
GO


PRINT 'UPDATE KPI 7'

UPDATE dbo.KPI
	SET REPfinalcost=S1.REPfinalcost
	FROM
	(
	select 
		T1.jobitemid , 
		sum(T3.finalcost) REPfinalcost
	from 
		dbo.invoice T6
		INNER JOIN dbo.invoiceitem T1 on T6.id = T1.invoiceid
		LEFT JOIN dbo.costs_repair T3 on T1.repaircost_id = T3.costid
		LEFT OUTER JOIN dbo.creditnote T4 on T6.id = T4.invid
	where 
		NOT T1.jobitemid is null 
		and T6.typeid not in (5,7)
		and T3.costtype = 'invoice' 
		and T3.active = 1 
		and T4.id is NULL
	group by 
		T1.jobitemid
	) S1
	WHERE dbo.KPI.JOBITEMID = S1.jobitemid
;
GO


PRINT 'UPDATE KPI 8'

UPDATE dbo.KPI
	SET ADJfinalcost=S1.ADJfinalcost
	FROM
	(
	select
		 T1.jobitemid,
		 sum(T4.finalcost) ADJfinalcost 
	from 
		dbo.invoice T6
		INNER JOIN dbo.invoiceitem T1 on T6.id = T1.invoiceid 
		LEFT JOIN dbo.costs_adjustment T4 on T1.adjustmentcost_id = T4.costid
		LEFT OUTER JOIN dbo.creditnote T3 on T1.id = T3.invid
	where 
		NOT T1.jobitemid is null 
		and T6.typeid not in (5,7) 
		and T4.costtype = 'invoice'
		and T4.active = 1
		and T3.id is NULL
	group by
		T1.jobitemid
	) S1
	WHERE dbo.KPI.JOBITEMID = S1.jobitemid
;
GO


PRINT 'UPDATE KPI 9'

UPDATE dbo.KPI
	SET PURfinalcost=S1.PURfinalcost
	FROM
	(
	select 
		T1.jobitemid,
		sum(T5.finalcost) PURfinalcost 
	from 
		dbo.invoice T6
		INNER JOIN dbo.invoiceitem T1 on T6.id = T1.invoiceid
		LEFT JOIN dbo.costs_purchase T5 on T1.purchasecost_id = T5.costid
		LEFT OUTER JOIN dbo.creditnote T3 on T6.id = T3.invid
	where 
		T6.id = T1.invoiceid 
		and  NOT T1.jobitemid is null 
		and T6.typeid not in (5,7) 
		and T5.costtype = 'invoice' 
		and T5.active = 1
		and T3.id is NULL
	group by 
		T1.jobitemid
	) S1
	WHERE dbo.KPI.JOBITEMID = S1.jobitemid
;
GO


PRINT 'UPDATE KPI 10'

UPDATE dbo.KPI
	SET JOBSER=T1.SER
	FROM
	dbo.SERJOBCOSTS T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 11'

UPDATE dbo.KPI
	SET SER=T1.SER
	FROM
	dbo.SERJCCOSTS T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 12'

UPDATE dbo.KPI
	SET SERfinalcost=T1.SER
	FROM
	dbo.SERINVCOSTS T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 13'

UPDATE dbo.KPI SET INVfinalcost = INVfinalcost + SERfinalcost
;
GO

PRINT 'UPDATE KPI 14'

UPDATE dbo.KPI
	SET CRCAL=T1.cal,CRREP=T1.rep,CRADJ=T1.adj,CRPUR=T1.pur
	FROM 
	CRCOSTS T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 15'

UPDATE dbo.KPI
	SET firstactivity=T1.startstamp
	FROM 
	firstactivity T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 16'

UPDATE dbo.KPI
	SET receivedintransit=T1.startstamp
	FROM 
	receivedintransit T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 17'

UPDATE dbo.KPI
	SET conrev=T1.startstamp, conrevby=T1.startedby
	FROM 
	conrev T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 18'

UPDATE dbo.KPI
	SET precal=T1.startstamp
	FROM 
	precal T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 19'

UPDATE dbo.KPI
	SET precalend=T1.endstamp
	FROM 
	precalend T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 20'

UPDATE dbo.KPI
	SET certificatesigned=T1.endstamp
	FROM 
	certificatesigned T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 21'

UPDATE dbo.KPI
	SET issuejobcosting=T1.startstamp
	FROM 
	issuejobcosting T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 22'

UPDATE dbo.KPI
	SET CUSTAPPROVESJOBCOSTING=T1.startstamp
	FROM 
	CUSTAPPROVESJOBCOSTING T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 23'

UPDATE dbo.KPI
	SET AWAITINGDELNOTETOCLIENT=T1.startstamp
	FROM 
	AWAITINGDELNOTETOCLIENT T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 24'

UPDATE dbo.KPI
	SET DELNOTEFORRTNTOCLIENT=T1.startstamp
	FROM 
	DELNOTEFORRTNTOCLIENT T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 24b'

UPDATE dbo.KPI
	SET DELNOTENOFORRTNTOCLIENT=T1.deliveryno
	FROM 
	DELNOTENOFORRTNTOCLIENT T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 25'

UPDATE dbo.KPI
	SET DELNOTEFORTHIRDPARTY=T1.startstamp
	FROM 
	DELNOTEFORTHIRDPARTY T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 26'

UPDATE dbo.KPI
	SET DELNOTEFORINTERNAL=T1.startstamp
	FROM 
	DELNOTEFORINTERNAL T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 27'

UPDATE dbo.KPI
	SET DELNOTEFORINTERNALRETURN=T1.startstamp
	FROM 
	DELNOTEFORINTERNALRETURN T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 28'

UPDATE dbo.KPI
	SET DESPATCHEDTHIRDPARTY=T1.startstamp
	FROM 
	DESPATCHEDTHIRDPARTY T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 29'

UPDATE dbo.KPI
	SET RECEIVEDBACKGOODSIN=T1.startstamp
	FROM 
	RECEIVEDBACKGOODSIN T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 30'

UPDATE dbo.KPI
	SET DISPTACHEDINTERNALTRANSIT=T1.startstamp
	FROM 
	DISPTACHEDINTERNALTRANSIT T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 31'

UPDATE dbo.KPI
	SET FIRSTDISPATCHED=T1.startstamp
	FROM 
	FIRSTDISPATCHED T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 32'

UPDATE dbo.KPI
	SET RECEIVEDINTERNALTRANSIT=T1.startstamp
	FROM 
	RECEIVEDINTERNALTRANSIT T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 33'

UPDATE dbo.KPI
	SET LASTDISPATCHED=T1.startstamp
	FROM 
	LASTDISPATCHED T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 33b'

UPDATE dbo.KPI
	SET DESPATCHEDTORETURNINGBUSINESSSUBDIVISION=T1.startstamp
	FROM 
	DESPATCHEDTORETURNINGBUSINESSSUBDIVISION T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 34'

UPDATE dbo.KPI
	SET DISPATCHEDTOCLIENTBY=T1.dispatchedtoclientby
	FROM 
	DISPATCHEDTOCLIENTBY T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 34b'

UPDATE dbo.KPI
	SET CLIENTPURCHASEORDER=T1.clientpurchaseorder
	FROM 
	CLIENTPURCHASEORDER T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 34c'

UPDATE dbo.KPI	
	SET CALIBRATIONVERIFICATIONSTATUS=T1.calibrationverificationstatus,OPTIMIZATION=T1.optimization,REPAIR=T1.repair,RESTRICTION=T1.restriction,ADJUSTMENT=T1.adjustment	
	FROM 	
	CALIBRATIONWITHJUDGEMENTCERTIFICATE	T1		
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 35'

UPDATE dbo.KPI
	SET LASTACTIVITY=T1.startstamp
	FROM 
	LASTACTIVITY T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 36'

UPDATE dbo.KPI
	SET WORKCOMPLETED=T1.startstamp
	FROM 
	WORKCOMPLETED T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 37'

UPDATE dbo.KPI
	SET itemsheld=S1.itemsheld,itemscal=S1.itemscal
	FROM 
	(select T1.COMPANY , T1.SUBNAME , count(T1.PLANTID) ITEMSHELD , count(T1.JOBITEMID) ITEMSCAL
	from ITEMSHELDANDCAL T1
	group by T1.COMPANY, T1.SUBNAME) S1
	WHERE dbo.KPI.company = S1.company and dbo.KPI.subname = S1.subname
;
GO


PRINT 'UPDATE KPI 38'

UPDATE dbo.KPI
	SET TPNAME=S1.tpname,TPCOST=S1.tpcost
	FROM 
	(select c13 JOBITEMID , c17 TPNAME , c16 TPCOST 
	from (select T2.JOBITEMID c8 , max(T2.TPNAME) c9 
	from TRESCALPO_TMP T1, TRESCALPO_TMP T2
	where T1.JOBITEMID = T2.JOBITEMID
	group by T2.JOBITEMID) D4, (select T1.JOBITEMID c10 , min(T1.TPNAME) c11 
	from TRESCALPO_TMP T1, TRESCALPO_TMP T2
	where T1.JOBITEMID = T2.JOBITEMID
	group by T1.JOBITEMID) D3, (select T2.JOBITEMID c12 , T1.JOBITEMID c13 , T2.TPNAME c14 , T1.TPNAME c15 , 
	case  when T2.TPNAME <> T1.TPNAME then T1.TOTALCOST + T2.TOTALCOST else T1.TOTALCOST end  c16 , 
	case  when T2.TPNAME <> T1.TPNAME then T1.TPNAME + ' and ' + T2.TPNAME else T1.TPNAME end  c17 
	from TRESCALPO_TMP T1, TRESCALPO_TMP T2
	where T1.JOBITEMID = T2.JOBITEMID) D2
	where (c12 = c8 or c12 is null and c8 is null) and (c13 = c10 or c13 is null and c10 is null) and c11 = c15 and c9 = c14
	) S1
	WHERE dbo.KPI.JOBITEMID = S1.jobitemid
;
GO


PRINT 'UPDATE KPI 39'

UPDATE dbo.KPI
	SET THIRDPARTYGROUP=S1.groupid
	FROM 
	(select distinct T1.jobitemid , T1.groupid GROUPID
	from jobitem T1, jobitem T2, deliveryitem T4, delivery T3
	where T1.groupid = T2.groupid and T2.jobitemid = T4.jobitemid and T3.deliveryid = T4.deliveryid and T3.deliverytype = 'thirdparty' and  NOT T1.groupid is null
	) S1
	WHERE dbo.KPI.JOBITEMID = S1.jobitemid
;
GO


PRINT 'UPDATE KPI 40'

UPDATE dbo.KPI
	SET CLIPRODELSCH=T1.startstamp
	FROM 
	CLIPRODELSCH T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 41'

UPDATE dbo.KPI
	SET RESRECDELDTEREQ=T1.startstamp
	FROM 
	RESRECDELDTEREQ T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 42'

UPDATE dbo.KPI
	SET CLIPROCOSDEC=T1.startstamp
	FROM 
	CLIPROCOSDEC T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 43'

UPDATE dbo.KPI
	SET CLIPRODECFAUUNI=T1.startstamp
	FROM 
	CLIPRODECFAUUNI T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 44'

UPDATE dbo.KPI
	SET FAULTREPDESC=T1.faultdesc,FAULTREPISSUED=T1.faultrepissued,FAULTREPAPPROVED=T1.faultrepapproved
	FROM 
	FAULTREP T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO

PRINT 'UPDATE KPI 45'

INSERT INTO CATEGORY2
select
	D1.jobitemid,
	D1.CATEGORY
from
(
select
	T1.jobitemid,
	T1.id,
	case when T1.repair = 1 then 'TPREP' else
		case when T1.calibration = 1 then 'TPCAL' else
			case when T1.investigation = 1 then 'TPINV' else
				case when T1.adjustment = 1 then 'TPADJ' else 'other' 
				end
			end
		end
	end CATEGORY
from 
	tprequirement T1,
	jobitem T3, 
	job T2
where 
	T1.jobitemid = T3.jobitemid 
	and T2.jobid = T3.jobid
) as D1,
(
SELECT
	T4.jobitemid,
	max(T4.id) id
FROM 
	tprequirement T4
group by 
	T4.jobitemid
) as D2
where D1.ID = D2.ID
order by D1.jobitemid asc
;
GO


PRINT 'UPDATE KPI 45b'

UPDATE dbo.KPI
	SET CATEGORY=T1.category
	FROM 
	CATEGORY2 T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 46'

INSERT INTO CATEGORY3
SELECT 
	JOBITEMID  
	,case when IHREP <> 0 then 'IHREP' else 'IHCAL' end CATEGORY
FROM 
	dbo.KPI
where 
	CATEGORY = ''
;
GO

PRINT 'UPDATE KPI 47'

UPDATE dbo.KPI
	SET CATEGORY=T1.category
	FROM 
	CATEGORY3 T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 48'

UPDATE dbo.KPI
	SET PROCID=T1.procid,LAB=T1.shortname,REFERENCE=T1.reference
	FROM 
	JOBITEMPROCEDURE T1
	WHERE dbo.KPI.JOBITEMID = T1.jobitemid
;
GO


PRINT 'UPDATE KPI 49'

UPDATE dbo.KPI
	SET STDCALTIME=T1.stdcaltime
	FROM 
	STANDARDCALIBRATIONTIME T1
	WHERE dbo.KPI.modelid=T1.modelid and dbo.KPI.caltype=T1.caltype and dbo.KPI.lab=T1.lab
;
GO


PRINT 'UPDATE KPI 50'

UPDATE dbo.KPI
	SET ONBEHALFOF=T1.coname
	FROM 
	ONBEHALFOF T1
	WHERE dbo.KPI.jobitemid=T1.jobitemid
;
GO


PRINT 'UPDATE KPI 51'

UPDATE dbo.KPI
	SET CALIBRATIONENGINEER=T1.caleng
	FROM 
	CALIBRATIONENGINEER T1
	WHERE dbo.KPI.jobitemid=T1.jobitemid
;
GO


PRINT 'UPDATE KPI 52'

UPDATE dbo.KPI
	SET tppocaldepartment=S1.department,tppocalnominal=S1.nominal,tppocalcost=S1.totalcost
	FROM 
	(
	select JOBITEMID, min(DEPARTMENT) DEPARTMENT, NOMINAL, SUM(TOTALCOST) TOTALCOST from
	(select T1.JOBITEMID, T1.DEPARTMENT, {fn RIGHT(T1.NOMINAL,(({fn LENGTH({fn CONCAT(T1.NOMINAL,'Z')})} - 1) - {fn LOCATE('-',T1.NOMINAL)}) - 1)} NOMINAL , sum(T1.TOTALCOST) TOTALCOST
	from dbo.TRESCALPO T1 where T1.TOTALCOST <> 0 and {fn RIGHT(T1.NOMINAL,(({fn LENGTH({fn CONCAT(T1.NOMINAL,'Z')})} - 1) - {fn LOCATE('-',T1.NOMINAL)}) - 1)} in ('Calibration & adjustment', 'Standards Certification')
	group by T1.JOBITEMID, T1.DEPARTMENT, {fn RIGHT(T1.NOMINAL,(({fn LENGTH({fn CONCAT(T1.NOMINAL,'Z')})} - 1) - {fn LOCATE('-',T1.NOMINAL)}) - 1)}
	) as D group by JOBITEMID, NOMINAL) S1
	WHERE dbo.KPI.jobitemid=S1.jobitemid
;
GO


PRINT 'UPDATE KPI 53'

UPDATE dbo.KPI
	SET tpporepdepartment=S1.department,tpporepnominal=S1.nominal,tpporepcost=S1.totalcost
	FROM 
	(
	select T1.JOBITEMID, {fn RIGHT(T1.NOMINAL,(({fn LENGTH({fn CONCAT(T1.NOMINAL,'Z')})} - 1) - {fn LOCATE('-',T1.NOMINAL)}) - 1)} NOMINAL , min(T1.DEPARTMENT) DEPARTMENT , sum(T1.TOTALCOST) TOTALCOST 
	from dbo.TRESCALPO T1 where T1.TOTALCOST <> 0 and {fn RIGHT(T1.NOMINAL,(({fn LENGTH({fn CONCAT(T1.NOMINAL,'Z')})} - 1) - {fn LOCATE('-',T1.NOMINAL)}) - 1)} in ('3rd Party Repair Costs')
	group by T1.JOBITEMID, {fn RIGHT(T1.NOMINAL,(({fn LENGTH({fn CONCAT(T1.NOMINAL,'Z')})} - 1) - {fn LOCATE('-',T1.NOMINAL)}) - 1)}) S1
	WHERE dbo.KPI.jobitemid=S1.jobitemid
;
GO


PRINT 'UPDATE KPI 54'

UPDATE dbo.KPI
	SET RETURNOPTION=S1.returnoption
	FROM 
	(
	select
		T9.jobitemid,
		T25.translation RETURNOPTION
	from
		dbo.job T8 
		inner join dbo.jobitem T9 on T8.jobid = T9.jobid
		left join dbo.transportoption T24 on T9.returnoptionid = T24.id
		left join dbo.transportmethodtranslation T25 on T24.transportmethodid = T25.id   
		inner join dbo.emailtemplate T22 on T8.orgid = T22.subdivid and T22.component = 0
	where
		T25.locale = T22.locale) S1
	WHERE dbo.KPI.jobitemid=S1.jobitemid
;
GO


PRINT 'COMPANYINSTRUMENTFIRSTSEEN'

INSERT INTO COMPANYINSTRUMENTFIRSTSEEN
select 
	T1.ORGID, 
	T1.COMPANY , 
	T1.MFR , 
	T1.INSTMODEL , 
	T1.DESCRIPTION , 
	T1.PLANTID ,
	min(T1.DATEIN) MINDATEIN , 
	sum(case T1.CALfinalcost when 0 then T1.IHCAL ELSE T1.CALfinalcost END  + T1.TPCAL + case T1.REPfinalcost when 0 then T1.IHREP else T1.REPfinalcost END + T1.TPREP + T1.IHADJ + T1.TPADJ) SALES ,
	CURRENT_TIMESTAMP LASTMODIFIED
from 
	dbo.KPI T1
group by 
	T1.ORGID, 
	T1.COMPANY, 
	T1.MFR, 
	T1.INSTMODEL, 
	T1.DESCRIPTION, 
	T1.PLANTID
order by 
	1 asc , 
	2 asc , 
	3 asc , 
	4 asc , 
	5 asc
;

GO

PRINT 'KPI_CALIBRATION_TURNOVER'

INSERT INTO KPI_CALIBRATION_TURNOVER
select 
	T1.JOBITEMID,  
	cast(
	case when sum(T1.INVfinalcost) = 0 
		then 
			case when sum(T1.IHCAL + T1.TPCAL + T1.IHREP + T1.TPREP + T1.IHADJ + T1.TPADJ + T1.SER) = 0
				then
					sum(T1.CRCAL + T1.CRREP + T1.CRADJ + T1.CRPUR + T1.JOBSER)
				else
					sum(T1.IHCAL + T1.TPCAL + T1.IHREP + T1.TPREP + T1.IHADJ + T1.TPADJ + T1.SER) 
			end
		else 
			sum(T1.INVfinalcost) 
	end / (count(T2.JOBITEMID)*count(T2.JOBITEMID)) as DECIMAL(8,2)
	)
from 
	dbo.KPI T1
	inner join dbo.KPI_CALIBRATION T2 on T1.JOBITEMID = T2.jobitemid
group by 
	T1.JOBITEMID
;
GO

PRINT 'UPDATE KPI_CALIBRATION '

UPDATE dbo.KPI_CALIBRATION
	SET TURNOVER=T1.turnover
	FROM 
	KPI_CALIBRATION_TURNOVER T1
	WHERE dbo.KPI_CALIBRATION.jobitemid=T1.jobitemid
;
GO

COMMIT TRAN