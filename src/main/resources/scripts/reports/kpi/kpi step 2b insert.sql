USE [spain]

BEGIN TRAN

PRINT 'FAULTREP'

INSERT INTO FAULTREP 
/* Old fault report data */
SELECT
	S1.jobitemid,
	ISNULL(S3.description,'') FAULTDESC,
	S1.issuedate,
	S1.clientapprovalon
	FROM
	(
	SELECT
		jobitemid
		,faultrepid
		,issuedate
	    ,clientapprovalon
	FROM
		dbo.faultreport
	WHERE 
		version = 'FAULT_REPORT'
		and jobitemid not in (SELECT distinct jobitemid FROM dbo.faultreport WHERE version = 'FAILURE_REPORT')
	) as S1
inner join
	(
	SELECT 
		jobitemid
		,max(faultrepid) lastfaultrepid
	FROM 
		dbo.faultreport
	WHERE 
		version = 'FAULT_REPORT'
	GROUP BY
		jobitemid
	) as S2 on S1.faultrepid = S2.lastfaultrepid
left join
	(
	SELECT 
		T1.faultrepid,
		STUFF((SELECT ' ' + T2.description
				FROM
					(
					select faultrepid, description, id from faultreportdescription
					) T2
				WHERE 
					T1.faultrepid = T2.faultrepid
				ORDER BY
					T2.id desc
				FOR XML PATH('')), 1, 1, '') [description]		
	FROM
		dbo.faultreportdescription T1 
	WHERE
		T1.faultrepid is not NULL
	GROUP BY
		T1.faultrepid
	) as S3 on S2.lastfaultrepid = S3.faultrepid
UNION
/* New fault report data */
SELECT
	S1.jobitemid,
	CASE WHEN S1.techniciancomments <> '' 
		then 
			CONCAT(CONCAT(S1.techniciancomments,CASE WHEN S1.managercomments <> '' then CONCAT(' ',S1.managercomments) end), case when S1.otherstate <> '' then CONCAT(' ',S1.otherstate) end)
		else
			CASE WHEN S1.managercomments <> '' 
				then
					CONCAT(S1.managercomments, case when S1.otherstate <> '' then CONCAT(' ',S1.otherstate) else '' end)
				else
					CASE WHEN S1.otherstate <> ''
						then
							S1.otherstate
						else
							''
						end
				end
		end FAULTDESC,
		S1.issuedate,
		S1.clientapprovalon
FROM
	(
	SELECT
		jobitemid
		,faultrepid
		,techniciancomments
		,ISNULL(managercomments,'') managercomments
		,otherstate
		,issuedate,
		clientapprovalon
	FROM
		dbo.faultreport
	WHERE 
		version = 'FAILURE_REPORT'
	) as S1
inner join
	(
	SELECT 
		jobitemid
		,max(faultrepid) lastfaultrepid
	FROM 
		dbo.faultreport
	WHERE 
		version = 'FAILURE_REPORT'
	GROUP BY
		jobitemid
	) as S2 on S1.faultrepid = S2.lastfaultrepid
;

GO

PRINT 'FIRSTACTIVITY'

INSERT INTO FIRSTACTIVITY
select 
	D1.JOBITEMID, 
	D1.DATEIN, 
	D1.ACTIVITY, 
	D1.STARTSTAMP
from (
	select distinct 
		T1.JOBITEMID , 
		T2.REGDATE DATEIN, 
		T3.ACTIVITYDESC ACTIVITY, 
		cast(T3.STARTSTAMP as DATE) STARTSTAMP, 
		T3.ID ID 
	from 
		JOB T2, 
		JOBITEM T1, 
		JOBITEMACTION T3 
	where 
		T2.JOBID = T1.JOBID 
		and T1.JOBITEMID = T3.JOBITEMID 
		and T3.DELETED = 0
	) as D1,
	(
	select distinct 
		T1.JOBITEMID , 
		min(T3.ID) ID 
	from 
		JOB T2, 
		JOBITEM T1, 
		JOBITEMACTION T3 
	where 
		T2.JOBID = T1.JOBID 
		and T1.JOBITEMID = T3.JOBITEMID 
		and T3.DELETED = 0
	GROUP BY  
		T1.JOBITEMID
	) as D2
where 
	D1.ID = D2.ID
;

GO

PRINT 'LASTACTIVITY'

INSERT INTO LASTACTIVITY
select 
	D1.JOBITEMID, 
	D1.DATEIN, 
	D1.ACTIVITY, 
	D1.STARTSTAMP
from (
	select distinct 
		T1.JOBITEMID , 
		T2.REGDATE DATEIN, 
		T3.ACTIVITYDESC ACTIVITY, 
		cast(T3.STARTSTAMP as DATE) STARTSTAMP, 
		T3.ID ID 
	from 
		JOB T2, 
		JOBITEM T1, 
		JOBITEMACTION T3 
	where 
		T2.JOBID = T1.JOBID 
		and T1.JOBITEMID = T3.JOBITEMID 
		and T3.DELETED = 0
	) as D1,
	(
	select distinct 
		T1.JOBITEMID , 
		max(T3.ID) ID 
	from 
		JOB T2, 
		JOBITEM T1, 
		JOBITEMACTION T3 
	where 
		T2.JOBID = T1.JOBID 
		and T1.JOBITEMID = T3.JOBITEMID 
		and T3.DELETED = 0
	GROUP BY  
		T1.JOBITEMID
	) as D2
where 
	D1.ID = D2.ID
;

GO

PRINT 'CONREV'

INSERT INTO CONREV 
select distinct 
	D1.jobitemid, 
	D2.activity, 
	CAST(D1.startstamp as Date) STARTSTAMP, 
	D2.STARTEDBY 
from
	(
	select 
		T1.JOBITEMID , 
		min(T2.STARTSTAMP) STARTSTAMP 
	from 
		JOBITEM T1, 
		JOBITEMACTION T2 
	where 
		T1.JOBITEMID = T2.JOBITEMID 
		and UPPER(T2.ACTIVITYDESC) in ('CONTRACT REVIEW','ONSITE CONTRACT REVIEW')
		and T2.DELETED = 0 
	group by 
		T1.JOBITEMID
	) as D1,
	(
	select 
		T3.JOBITEMID, 
		T4.ACTIVITYDESC ACTIVITY, 
		T4.STARTSTAMP, 
		T5.FIRSTNAME + ' ' + T5.LASTNAME STARTEDBY 
	from 
		JOBITEM T3, 
		JOBITEMACTION T4, 
		CONTACT T5 
	where 
		T3.JOBITEMID = T4.JOBITEMID 
		and T4.startedbyid = T5.personid 
		and UPPER(T4.ACTIVITYDESC) in ('CONTRACT REVIEW','ONSITE CONTRACT REVIEW')
		and T4.DELETED = 0
	) as D2
where 
	D1.jobitemid = D2.jobitemid 
	and D1.startstamp = D2.startstamp
;

GO

PRINT 'PRECAL'

INSERT INTO PRECAL 
select 
	T1.JOBITEMID, 
	'Pre-calibration/Pre-onsite calibration' ACTIVITY, 
	cast(min(T2.STARTSTAMP) as DATE) STARTSTAMP 
from 
	JOBITEM T1, 
	JOBITEMACTION T2 
where 
	T1.JOBITEMID = T2.JOBITEMID 
	and UPPER(T2.ACTIVITYDESC) in ('PRE-CALIBRATION','PRE-ONSITE CALIBRATION')
	and T2.DELETED = 0 
group by 
	T1.JOBITEMID
;

GO

PRINT 'PRECALEND'

INSERT INTO PRECALEND 
select 
	T1.JOBITEMID , 
	'Pre-calibration/Pre-onsite calibration' ACTIVITY, 
	cast(max(T2.ENDSTAMP) as DATE) ENDSTAMP 
from 
	JOBITEM T1, 
	JOBITEMACTION T2 
where 
	T1.JOBITEMID = T2.JOBITEMID 
	and UPPER(T2.ACTIVITYDESC) in ('PRE-CALIBRATION','PRE-ONSITE CALIBRATION')
	and T2.DELETED = 0 
group by 
	T1.JOBITEMID
;

GO

PRINT 'CERTIFICATESIGNED'

INSERT INTO CERTIFICATESIGNED 
select T1.JOBITEMID, min(T2.ACTIVITYDESC) ACTIVITY, cast(max(T2.ENDSTAMP) as DATE) ENDSTAMP from JOBITEM T1, JOBITEMACTION T2 where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('CERTIFICATE SIGNED','Onsite calibration - certificate signed') and T2.DELETED = 0 group by T1.JOBITEMID
;

GO

PRINT 'ISSUEJOBCOSTING'

INSERT INTO ISSUEJOBCOSTING 
select T1.JOBITEMID , 'Issue Job Costing', cast(max(T2.STARTSTAMP) as DATE) STARTSTAMP from JOBITEM T1, JOBITEMACTION T2 where T1.JOBITEMID = T2.JOBITEMID and (UPPER(T2.ACTIVITYDESC) like '%ISSUE JOB COSTING%' or UPPER(T2.ACTIVITYDESC) like '%COSTING ISSUED TO CLIENT') and T2.DELETED = 0 group by T1.JOBITEMID
;
GO

PRINT 'CUSTAPPROVESJOBCOSTING'

INSERT INTO CUSTAPPROVESJOBCOSTING 
select T1.JOBITEMID , cast(max(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('RECEIVED CUSTOMER DECISION ON JOB COSTING','CLIENT DECISION ON REPAIR COSTING RECEIVED','CLIENT RESPONSE RECEIVED','CLIENT DECISION RECEIVED','RESPONSE TO PROGRESS REQUEST RECEIVED FROM CLIENT') and T2.DELETED = 0 
group by T1.JOBITEMID
;
GO

PRINT 'RECEIVEDINTRANSIT'

INSERT INTO RECEIVEDINTRANSIT 
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , cast(min(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) = 'UNIT RECEIVED IN TRANSIT'
group by T1.JOBITEMID, T2.ACTIVITYDESC
;

GO

PRINT 'AWAITINGDELNOTETOCLIENT'

INSERT INTO AWAITINGDELNOTETOCLIENT 
select T1.JOBITEMID , 'Awaiting delivery note to client' STATUS ,cast(min(T1.STARTSTAMP) as DATE) STARTSTAMP
from jobitemaction T1
where T1.endstatusid = 50 and T1.DELETED = 0 
group by T1.JOBITEMID
;
GO

PRINT 'DELNOTEFORRTNTOCLIENT'

INSERT INTO DELNOTEFORRTNTOCLIENT 
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , cast(min(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('DELIVERY NOTE CREATED FOR RETURN TO CLIENT','ADDED TO DELIVERY NOTE') and T2.DELETED = 0 
group by T1.JOBITEMID, T2.ACTIVITYDESC
;
GO

PRINT 'DELNOTENOFORRTNTOCLIENT'

insert into DELNOTENOFORRTNTOCLIENT
SELECT
	T1.jobitemid
	,T2.deliveryno
FROM 
	dbo.deliveryitem T1
	inner join dbo.delivery T2 on T1.deliveryid = T2.deliveryid
WHERE
	T2.deliverytype = 'client'
	and T2.deliveryid = (SELECT max(A2.deliveryid) FROM dbo.deliveryitem A1 inner join dbo.delivery A2 on A1.deliveryid = A2.deliveryid WHERE A2.deliverytype = 'client' and A1.jobitemid = T1.jobitemid GROUP BY A1.jobitemid)
;
GO

PRINT 'DELNOTEFORINTERNAL'

INSERT INTO DELNOTEFORINTERNAL 
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , cast(min(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('ADDED TO INTERNAL DELIVERY NOTE') and T2.DELETED = 0 
group by T1.JOBITEMID, T2.ACTIVITYDESC
;
GO

PRINT 'DELNOTEFORINTERNALRETURN'

INSERT INTO DELNOTEFORINTERNALRETURN 
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , cast(max(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('ADDED TO INTERNAL RETURN DELIVERY NOTE') and T2.DELETED = 0 
group by T1.JOBITEMID, T2.ACTIVITYDESC
;
GO

PRINT 'DELNOTEFORTHIRDPARTY '

INSERT INTO DELNOTEFORTHIRDPARTY 
select distinct D1.jobitemid, D2.activity, D1.startstamp from
(
select T1.JOBITEMID , max(T2.STARTSTAMP) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('DELIVERY NOTE CREATED FOR DISPATCH TO THIRD PARTY','ADDED TO THIRD PARTY DELIVERY NOTE AFTER PO','ADDED TO THIRD PARTY DELIVERY NOTE') and T2.DELETED = 0
group by T1.JOBITEMID
) D1,
(
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , T2.STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('DELIVERY NOTE CREATED FOR DISPATCH TO THIRD PARTY','ADDED TO THIRD PARTY DELIVERY NOTE AFTER PO','ADDED TO THIRD PARTY DELIVERY NOTE') and T2.DELETED = 0
) D2
where D1.jobitemid = D2.jobitemid
and D1.startstamp = D2.startstamp
;
GO

PRINT 'DISPTACHEDINTERNALTRANSIT'

INSERT INTO DISPTACHEDINTERNALTRANSIT 
select T1.JOBITEMID , min(T2.ACTIVITYDESC) ACTIVITY , cast(min(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('UNIT DISPATCHED ON INTERNAL TRANSIT','UNIT DESPATCHED TO BUSINESS COMPANY','DESPATCHED TO BUSINESS COMPANY FOR CALIBRATION','DESPATCHED TO RETURNING BUSINESS SUBDIVISION') and T2.DELETED = 0
group by T1.JOBITEMID
;
GO

PRINT 'RECEIVEDINTERNALTRANSIT'

INSERT INTO RECEIVEDINTERNALTRANSIT
select T1.JOBITEMID , max(T2.ACTIVITYDESC) ACTIVITY, cast(max(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('UNIT RECEIVED ON INTERNAL TRANSIT','UNIT RECEIVED AT BUSINESS COMPANY','RECEIVED AT BUSINESS COMPANY FOR CALIBRATION','RECEIVED AT RETURNING BUSINESS SUBDIVISION') and T2.DELETED = 0
group by T1.JOBITEMID
;
GO

PRINT 'DESPATCHEDTORETURNINGBUSINESSSUBDIVISION'
--jobitemaction.activityid = 4072
--jobitemaction.activitydesc = 'Despatched to returning business subdivision'
INSERT INTO DESPATCHEDTORETURNINGBUSINESSSUBDIVISION
select T1.JOBITEMID , max(T2.ACTIVITYDESC) ACTIVITY, cast(max(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and T2.ACTIVITYID = 4072 and T2.DELETED = 0
group by T1.JOBITEMID
;
GO

PRINT 'LASTDISPATCHED'

INSERT INTO LASTDISPATCHED
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , cast(max(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, (JOBITEMACTION T2 LEFT OUTER JOIN JOBITEMACTION T3 on T2.JOBITEMID = T3.JOBITEMID)
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) = 'UNIT DISPATCHED' and T2.DELETED = 0 and UPPER(T3.ACTIVITYDESC) = 'UNIT RECEIVED IN TRANSIT' and T3.DELETED = 0
group by T1.JOBITEMID, T2.ACTIVITYDESC
union
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , cast(max(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, (JOBITEMACTION T2 LEFT OUTER JOIN JOBITEMACTION T3 on T2.JOBITEMID = T3.JOBITEMID)
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) = 'UNIT DESPATCHED TO CLIENT' and T2.DELETED = 0 and UPPER(T3.ACTIVITYDESC) = 'UNIT RECEIVED AT BUSINESS COMPANY' and T3.DELETED = 0
group by T1.JOBITEMID, T2.ACTIVITYDESC
;

INSERT INTO FIRSTDISPATCHED 
select T1.JOBITEMID , min(T2.ACTIVITYDESC) ACTIVITY, cast(min(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('UNIT DISPATCHED','UNIT DESPATCHED TO BUSINESS COMPANY','UNIT DESPATCHED TO CLIENT') and T2.DELETED = 0
group by T1.JOBITEMID
;
DELETE FROM FIRSTDISPATCHED where JOBITEMID in
(select jobitemid from LASTDISPATCHED)
and UPPER(ACTIVITY) = 'UNIT DESPATCHED TO CLIENT'
;
GO

PRINT 'DESPATCHEDTHIRDPARTY'

INSERT INTO DESPATCHEDTHIRDPARTY
select T1.JOBITEMID , max(T2.ACTIVITYDESC) ACTIVITY , cast(min(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('DESPATCHED TO 3RD PARTY', 'DESPATCHED TO THIRD PARTY','ITEM DESPATCHED TO THIRD PARTY AFTER PO','ITEM DESPATCHED TO THIRD PARTY') and T2.DELETED = 0
group by T1.JOBITEMID
;
GO

PRINT 'RECEIVEDBACKGOODSIN'

INSERT INTO RECEIVEDBACKGOODSIN
select T1.JOBITEMID , min(T2.ACTIVITYDESC) ACTIVITY , cast(max(T2.STARTSTAMP) as DATE) STARTSTAMP
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('UNIT RECEIVED BACK AT GOODS IN','UNIT RECEIVED FROM THIRD PARTY') and T2.DELETED = 0
group by T1.JOBITEMID
;
GO

PRINT 'WORKCOMPLETED '

INSERT INTO WORKCOMPLETED 
select 
	T1.JOBITEMID , 
	'All item states that are not retired and not active' STATUS ,
	cast(min(T1.STARTSTAMP) as DATE) STARTSTAMP
from 
	jobitemaction T1
where 
	T1.endstatusid in (SELECT stateid FROM dbo.itemstate where retired = 0 and active = 0) 
	and T1.DELETED = 0
group by 
	T1.JOBITEMID
;
GO

PRINT 'ITEMSHELDANDCAL'

INSERT INTO ITEMSHELDANDCAL
select c13 COMPANY , c12 SUBNAME , c11 DESCRIPTION , c9 PLANTID , c10 JOBITEMID 
from 
(
select T4.PLANTID c7 , min(T5.JOBITEMID) c8 
from COMPANY T1, 
CONTACT T6, 
SUBDIV T2, 
BASESTATUS T3, 
(INSTRUMENT T4 LEFT JOIN JOBITEM T5 on T4.PLANTID = T5.PLANTID)
where T4.COID = T1.COID and T6.PERSONID = T4.PERSONID and T2.SUBDIVID = T6.SUBDIVID and T4.STATUSID = T3.STATUSID 
group by T4.PLANTID
) D3,
(
select T4.PLANTID c9 , T5.JOBITEMID c10 , T3.NAME c11 , T2.SUBNAME c12 , T1.CONAME c13 
from COMPANY T1, CONTACT T6, SUBDIV T2, BASESTATUS T3, (INSTRUMENT T4 LEFT JOIN JOBITEM T5 on T4.PLANTID = T5.PLANTID)
where T4.COID = T1.COID and T6.PERSONID = T4.PERSONID and T2.SUBDIVID = T6.SUBDIVID and T4.STATUSID = T3.STATUSID 
) D2
where (c9 = c7 or c9 is null and c7 is null) and (c10 = c8 or c10 is null)
;
GO

PRINT 'MAXJOBCOSTINGFORJOBITEM'

INSERT INTO MAXJOBCOSTINGFORJOBITEM
select jobitemid,max(costingid) costingid
from
(
select T1.jobitemid , T2.costingid
from jobitem T1, jobcostingitem T2
where T1.jobitemid = T2.jobitemid
) D1
group by jobitemid
;
GO

PRINT 'INHOUSETHIRDPARTYCALREPAIR_TMP'

INSERT INTO INHOUSETHIRDPARTYCALREPAIR_TMP
select 
	T1.coname COMPANY ,
	T2.subname ,
	T3.name MFR ,
	T4.model INSTMODEL ,
	T5.description  ,
	T6.jobno ,
	T6.regdate DATEIN ,
	T7.jobitemid ,
	T7.itemno ,
	T8.finalcost CALtotalcost ,
	T9.finalcost REPtotalcost ,
	case when T8.housecost = 0 
		then 0 
		else round((T8.housecost - T8.discountvalue),2) 
	end IHCAL ,
	case when round(((T8.TPMARKUPRATE * T8.TPMANUALPRICE) / 100) + T8.TPMANUALPRICE /*+ T8.thirdcosttotal*/, 2) = 0 
		then 0 
		else round((((T8.TPMARKUPRATE * T8.TPMANUALPRICE) / 100) + T8.TPMANUALPRICE /*+ T8.thirdcosttotal*/ - T8.tpcartotal),2) 
	end TPCAL,
	case when round(((T9.LABOURTIME / 60.00) * T9.HOURLYRATE) + T9.PARTSCOST + round((T9.PARTSCOST * T9.PARTSMARKUPRATE) / 100, 2), 2) = 0 
		then 0 
		else round((((T9.LABOURTIME / 60.00) * T9.HOURLYRATE) + T9.PARTSCOST + round((T9.PARTSCOST * T9.PARTSMARKUPRATE) / 100, 2)), 2) 
	end IHREP,
	case when round(((T9.TPMARKUPRATE * T9.TPMANUALPRICE) / 100) + T9.TPMANUALPRICE /*+ T9.thirdcosttotal*/, 2) = 0 
		then 0 
		else round((((T9.TPMARKUPRATE * T9.TPMANUALPRICE) / 100) + T9.TPMANUALPRICE /*+ T9.thirdcosttotal*/ - T9.tpcartotal), 2) 
	end TPREP,
	case when round(((T13.LABOURTIME / 60.00) * T13.HOURLYRATE) , 2) = 0 
		then 0 
		else round((((T13.LABOURTIME / 60.00) * T13.HOURLYRATE)), 2) 
	end + ISNULL(T13.fixedprice,0) IHADJ,
	case when round(((T13.TPMARKUPRATE * T13.TPMANUALPRICE) / 100) + T13.TPMANUALPRICE /*+ T13.thirdcosttotal*/, 2) = 0 
		then 0 
		else round((((T13.TPMARKUPRATE * T13.TPMANUALPRICE) / 100) + T13.TPMANUALPRICE /*+ T13.thirdcosttotal*/), 2) 
	end TPADJ,
	case when round(T8.TPMANUALPRICE /*+ T8.thirdcosttotal*/ - T8.tpmarkupvalue,2) = 0 
		then 0 
		else round((T8.TPMANUALPRICE /*+ T8.thirdcosttotal*/ - T8.tpmarkupvalue - T8.tpcartotal), 2) 
	end TRESCALTPCAL ,
	case when round(T9.TPMANUALPRICE + T10.TPCARIN + T10.TPCAROUT /*+ T9.thirdcosttotal*/ - T9.tpmarkupvalue, 2) = 0
		then 0 
		else round((T9.TPMANUALPRICE + T10.TPCARIN + T10.TPCAROUT /*+ T9.thirdcosttotal*/ - T9.tpmarkupvalue - T9.tpcartotal), 2) 
	end TRESCALTPREP ,
	case when round(T13.TPMANUALPRICE /*+ T13.thirdcosttotal*/, 2) = 0 
		then 0 
		else round((T13.TPMANUALPRICE /*+ T13.thirdcosttotal*/) , 2) 
	end TRESCALTPADJ,
	case when T15.FINALCOST = 0 
		then 0 
		else round(T15.FINALCOST,2) 
	end PURCHASE,
	case when T14.FINALCOST = 0 
		then 0 
		else round(T14.FINALCOST,2) 
	end INSPECT,
	case when (T8.TPCARTOTAL + T9.TPCARTOTAL) = 0 
		then 0 
		else round((T8.TPCARTOTAL + T9.TPCARTOTAL) , 2) 
	end TPCARR,
	T10.COSTINGID
from
	company T1
	inner join subdiv T2 on T2.coid = T1.COID 
	inner join contact T11 on T11.subdivid = T2.subdivid
	inner join job T6 on T6.personid = T11.personid 
	inner join jobitem T7 on T6.jobid = T7.jobid 
	inner join instrument T12 on T7.plantid = T12.plantid 
	inner join instmodel T4 on T4.modelid = T12.modelid 
	inner join mfr T3 on T4.mfrid = T3.mfrid 
	inner join description T5 on T4.descriptionid = T5.descriptionid 
	inner join jobcostingitem T10 on T7.jobitemid = T10.jobitemid 
	left join costs_calibration T8 on T10.calcost_id = T8.costid and T8.active = 1
	left join costs_repair T9 on T10.repaircost_id = T9.costid and T9.active = 1
	left join costs_adjustment T13 on T10.adjustmentcost_id = T13.costid and T13.active = 1
	left join costs_inspection T14 on T10.inspectioncost_id = T14.costid and T14.active = 1
	left join costs_purchase T15 on T10.purchasecost_id = T15.costid and T15.active = 1
;
GO

PRINT 'INHOUSETHIRDPARTYCALREPAIR'

INSERT INTO INHOUSETHIRDPARTYCALREPAIR
select 
	T1.COMPANY, 
	T1.SUBNAME,
	T1.MFR,
	T1.INSTMODEL,
	T1.DESCRIPTION,
	T1.JOBNO,
	T1.DATEIN,
	T1.JOBITEMID,
	T1.ITEMNO,
	ISNULL(T1.CALtotalcost,0) CALtotalcost,
	ISNULL(T1.REPtotalcost,0) REPtotalcost,
	ISNULL(T1.IHCAL,0) IHCAL,
	ISNULL(T1.TPCAL,0) TPCAL,
	ISNULL(T1.IHREP,0) IHREP,
	ISNULL(T1.TPREP,0) TPREP,
	ISNULL(T1.IHADJ,0) IHADJ,
	ISNULL(T1.TPADJ,0) TPADJ,
	ISNULL(T1.TRESCALTPCAL,0) TRESCALTPCAL,
	ISNULL(T1.TRESCALTPREP,0) TRESCALTPREP,
	ISNULL(T1.TRESCALTPADJ,0) TRESCALTPADJ,
	ISNULL(T1.PURCHASE,0) PURCHASE,
	ISNULL(T1.INSPECT,0) INSPECT,
	ISNULL(T1.TPCARR,0) TPCARR,
	CURRENT_TIMESTAMP
from 
	INHOUSETHIRDPARTYCALREPAIR_TMP T1, 
	MAXJOBCOSTINGFORJOBITEM T2
where 
	T1.JOBITEMID = T2.JOBITEMID
	and T1.COSTINGID = T2.COSTINGID
;
GO

PRINT 'IHTPCR_TMP '

INSERT INTO IHTPCR_TMP 
SELECT 
	JOBITEMID ,
	sum(CALtotalcost) CALtotalcost, 
	sum(REPtotalcost) REPtotalcost, 
	sum(IHCAL) IHCAL, 
	sum(TPCAL) TPCAL, 
	sum(IHREP) IHREP, 
	sum(TPREP) TPREP,
	sum(IHADJ) IHADJ,
	sum(TPADJ) TPADJ,
	sum(TRESCALTPCAL) TRESCALTPCAL,
	sum(TRESCALTPREP) TRESCALTPREP,
	sum(TRESCALTPADJ) TRESCALTPADJ,
	sum(PURCHASE) PURCHASE,
	sum(INSPECT) INSPECT,
	sum(TPCARR) TPCARR
FROM 
	INHOUSETHIRDPARTYCALREPAIR
group by 
	JOBITEMID
;
GO

PRINT 'TRESCALPO'

INSERT INTO TRESCALPO
select T1.jobitemid , T1.poitemid, T2.title Nominal, T3.name Costtype, T4.name Department, sum(T5.totalcost * T5.quantity - (T5.discountrate * 1.0e0 / 100) * (T5.totalcost * T5.quantity)) Totalcost 
from dbo.purchaseorderjobitem T1, dbo.purchaseorderitem T5, ((dbo.nominalcode T2 LEFT OUTER JOIN dbo.costtype T3 on T2.costtypeid = T3.typeid) LEFT OUTER JOIN dbo.department T4 on T2.deptid = T4.deptid)
where T1.poitemid = T5.id and T5.nominalid = T2.id
group by T1.jobitemid, T1.poitemid, T2.title, T3.name, T4.name
order by 1 asc
;
GO

PRINT 'TRESCALPO_TMP1'

INSERT INTO TRESCALPO_TMP1
select T1.jobitemid , T3.orderid,  T2.coname TPNAME , sum(T3.totalcost * T3.quantity - ((T3.discountrate * 1.0e0) / 100) * (T3.totalcost * T3.quantity)) TOTALCOST
from jobitem T1, purchaseorderjobitem T4, purchaseorderitem T3, purchaseorder T5, contact T6, subdiv T7, company T2
where T1.jobitemid = T4.jobitemid and T3.id = T4.poitemid and T5.id = T3.orderid and T5.personid = T6.personid and T6.subdivid = T7.subdivid and T7.coid = T2.COID and T3.cancelled = 0
group by T1.jobitemid, T3.orderid, T2.coname
;
GO

PRINT 'TRESCALPO_TMP2'

INSERT INTO TRESCALPO_TMP2 
select T1.orderid, sum(T1.totalcost * T1.quantity - ((T1.discountrate * 1.0e0) / 100) * (T1.totalcost * T1.quantity)) TOTALCOST
from  {oj purchaseorderitem T1 LEFT OUTER JOIN purchaseorderjobitem T2 on T1.id = T2.poitemid }
where T2.jobitemid is null and T1.cancelled = 0
group by T1.orderid
;
GO

PRINT 'TRESCALPO_TMP'

INSERT INTO TRESCALPO_TMP 
select T1.JOBITEMID , T1.TPNAME , sum(case  when T2.TOTALCOST is null then T1.TOTALCOST else T1.TOTALCOST + T2.TOTALCOST end)  TOTALCOST
from (dbo.TRESCALPO_TMP1 T1 LEFT JOIN dbo.TRESCALPO_TMP2 T2 on T1.ORDERID = T2.ORDERID)
group by T1.jobitemid, T1.TPNAME
;
GO

PRINT 'CLIPRODELSCH'

INSERT INTO CLIPRODELSCH 
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , min(T2.STARTSTAMP) STARTSTAMP 
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) = 'CLIENT PROGRESSED FOR DELIVERY SCHEDULING' and T2.DELETED = 0
group by T1.JOBITEMID, T2.ACTIVITYDESC
;
GO

PRINT 'CLIPROCOSDEC'

INSERT INTO CLIPROCOSDEC 
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , min(T2.STARTSTAMP) STARTSTAMP 
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) = 'CLIENT CONTACT PROGRESSED FOR COSTING DECISION' and T2.DELETED = 0
group by T1.JOBITEMID, T2.ACTIVITYDESC
;
GO

PRINT 'CLIPRODECFAUUNI'

INSERT INTO CLIPRODECFAUUNI 
select T1.JOBITEMID , T2.ACTIVITYDESC ACTIVITY , min(T2.STARTSTAMP) STARTSTAMP 
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) = 'CLIENT CONTACT PROGRESSED FOR DECISION ON FAULTY UNIT' and T2.DELETED = 0
group by T1.JOBITEMID, T2.ACTIVITYDESC
;
GO

PRINT 'RESRECDELDTEREQ'

INSERT INTO RESRECDELDTEREQ
select distinct D1.jobitemid, D2.activity, D1.startstamp from
(
select T1.JOBITEMID , max(T2.STARTSTAMP) STARTSTAMP 
from JOBITEM T1, JOBITEMACTION T2
where T1.JOBITEMID = T2.JOBITEMID and UPPER(T2.ACTIVITYDESC) in ('RESPONSE RECEIVED FOR DELIVERY DATE REQUEST','DELIVERY DATE AGREED WITH CLIENT') and T2.DELETED = 0
group by T1.JOBITEMID
) D1,
(
select T3.JOBITEMID, T4.ACTIVITYDESC ACTIVITY, T4.STARTSTAMP
from JOBITEM T3, JOBITEMACTION T4
where T3.JOBITEMID = T4.JOBITEMID and UPPER(T4.ACTIVITYDESC) in ('RESPONSE RECEIVED FOR DELIVERY DATE REQUEST','DELIVERY DATE AGREED WITH CLIENT') and T4.DELETED = 0
) D2
where D1.jobitemid = D2.jobitemid
and D1.startstamp = D2.startstamp
;
GO

PRINT 'JOBITEMPROCEDURE'

INSERT INTO JOBITEMPROCEDURE
select
	S1.jobitemid,
	S1.ProcId,
	S1.reference,
	S2.shortname
from
(
select 
	T1.jobitemid,
	T2.id ProcId,
	T2.reference
from
	dbo.jobitem T1
	inner join dbo.jobitemworkrequirement T4 on T1.nextworkreqid = T4.id
	inner join dbo.workrequirement T5 on T4.reqid = T5.id
	inner join dbo.procs T2 on T5.procedureid = T2.id
) S1
inner join
(
SELECT 
	T1.jobitemid,
	STUFF((SELECT ',' + T2.shortname
			FROM
			(select 
				T1.jobitemid,
				T8.shortname
			from
				dbo.jobitem T1
				inner join dbo.jobitemworkrequirement T4 on T1.nextworkreqid = T4.id
				inner join dbo.workrequirement T5 on T4.reqid = T5.id
				inner join dbo.procs T2 on T5.procedureid = T2.id
				inner join dbo.categorisedprocedure T3 on T2.id = T3.procid
				inner join dbo.procedurecategory T7 on T3.categoryid = T7.id
				inner join dbo.department T8 on T7.departmentid = T8.deptid
			) T2
			WHERE 
				T1.jobitemid = T2.jobitemid
			ORDER BY
				T2.shortname
			FOR XML PATH('')), 1, 1, '') [shortname]
FROM
	dbo.jobitem T1
	inner join dbo.jobitemworkrequirement T4 on T1.nextworkreqid = T4.id
	inner join dbo.workrequirement T5 on T4.reqid = T5.id
	inner join dbo.procs T2 on T5.procedureid = T2.id
	inner join dbo.categorisedprocedure T3 on T2.id = T3.procid
	inner join dbo.procedurecategory T7 on T3.categoryid = T7.id
	inner join dbo.department T8 on T7.departmentid = T8.deptid
GROUP BY
	T1.jobitemid
) S2 on S1.jobitemid = S2.jobitemid
order by 1 asc
;
GO

PRINT 'ONBEHALFOF'

INSERT INTO ONBEHALFOF
select 
	T1.jobitemid,
	T2.coname
from 
	dbo.onbehalfitem T3,
	dbo.jobitem T1, 
	dbo.company T2
where 
	T3.jobitemid = T1.jobitemid 
	and T3.coid = T2.coid 
	and  NOT T3.coid is null
;
GO

PRINT 'CALIBRATIONENGINEER'

INSERT INTO CALIBRATIONENGINEER
select c9 Jobitemid , c13 Completetime , c12 Procedureid , c11 Timespent , c10 CalEng 
from (select T1.jobitemid c7 , max(T2.completetime) c8 
from dbo.calibration T2, dbo.callink T1, dbo.contact T3
where T2.id = T1.calid and T2.completedby = T3.personid and T2.calibrationstatus = 32
group by T1.jobitemid) D3, (select T1.jobitemid c9 , {fn CONCAT({fn CONCAT(T3.firstname,' ')},T3.lastname)} c10 , T1.timespent c11 , T2.procedureid c12 , T2.completetime c13 
from dbo.calibration T2, dbo.callink T1, dbo.contact T3
where T2.id = T1.calid and T2.completedby = T3.personid and T2.calibrationstatus = 32) D2
where (c9 = c7 or c9 is null and c7 is null) and c8 = c13
order by 1 asc
;
GO

PRINT 'STANDARDCALIBRATIONTIME'

INSERT INTO STANDARDCALIBRATIONTIME
select 
	T1.modelid,
	T10.translation Caltype,
	T3.shortname Lab,
	sum(T4.timespent) / count(T4.jobitemid)
from 
	dbo.jobitem T6
	inner join dbo.instrument T1 on T6.plantid = T1.plantid
	inner join dbo.callink T4 on T4.jobitemid = T6.jobitemid
	inner join dbo.calibration T5 on T5.id = T4.calid 
	inner join dbo.calibrationtype T2 on T5.caltypeid = T2.caltypeid 
	inner join dbo.categorisedprocedure T7 on T5.procedureid = T7.procid 
	inner join dbo.procedurecategory T8 on T7.categoryid = T8.id 
	inner join dbo.department T3 on T3.deptid = T8.departmentid 
	inner join dbo.servicetype T9 on T2.servicetypeid = T9.servicetypeid
	inner join dbo.servicetypeshortnametranslation T10 on T9.servicetypeid = T10.servicetypeid
	inner join dbo.job T11 on T6.jobid = T11.jobid
	inner join dbo.subdiv T12 on T11.orgid = T12.subdivid
	inner join dbo.company T14 on T12.coid = T14.coid
where 
	T5.calibrationstatus = 32
	and T10.locale = T14.documentlanguage
group by 
	T1.modelid, 
	T10.translation, 
	T3.shortname
order by 1 asc , 2 asc , 3 asc
;
GO

PRINT 'CRCOSTS'

insert into CRCOSTS
select 
	T1.jobitemid,
	T2.finalcost cal,
	T3.finalcost rep,
	T4.finalcost adj,
	T5.finalcost pur 
from 
	dbo.jobitem T1 
	LEFT JOIN dbo.costs_calibration T2 on T1.calcost_id = T2.costid 
	LEFT JOIN dbo.costs_repair T3 on T1.repcost_id = T3.costid 
	LEFT JOIN dbo.costs_adjustment T4 on T1.adjust_id = T4.costid 
	LEFT JOIN dbo.costs_purchase T5 on T1.salescost_id = T5.costid
where T2.finalcost is not null
;
GO

PRINT 'SERJOBCOSTS'

insert into SERJOBCOSTS
SELECT 
	D3.jobitemid,
	((D1.sumcost * (D3.jobownbuscomexcahngerate / D3.jobexchangerate)) / D2.items) ser
FROM
	(
	SELECT
		T2.jobid,
		sum(T1.cost) sumcost
	FROM 
		dbo.jobexpenseitem T1
		inner join dbo.job T2 on T1.job = T2.jobid
	group by
		T2.jobid
	) D1
inner join 
	(
	SELECT
		T1.jobid
		,count(T1.jobitemid) items     
	FROM 
		dbo.jobitem T1
		inner join dbo.jobexpenseitem T2 on T1.jobid = T2.job
	GROUP BY
		T1.jobid
	) D2 on D1.jobid = D2.jobid
inner join
	(
	SELECT distinct
		T1.jobid,
		T1.jobitemid,
		T6.exchangerate jobexchangerate,
		T5.exchangerate jobownbuscomexcahngerate
	FROM 
		dbo.jobitem T1
		inner join dbo.job T2 on T1.jobid = T2.jobid
		inner join dbo.subdiv T3 on T2.orgid = T3.subdivid
		inner join dbo.company T4 on T3.coid = T4.coid
		inner join dbo.supportedcurrency T5 on T5.currencyid = T4.currencyid
		inner join dbo.supportedcurrency T6 on T6.currencyid = T2.currencyid
		inner join dbo.jobexpenseitem T7 on T2.jobid = T7.job
	) D3 on D1.jobid = D3.jobid
;
GO

PRINT 'SERJCCOSTS'

insert into SERJCCOSTS
SELECT
	D3.jobitemid,
	((D1.sumfinalcost * (D3.jobownbuscomexcahngerate / D3.jcexchangerate)) / D2.items) ser
FROM
	(
	SELECT 
		T1.jobCosting,
		sum(T1.finalcost) sumfinalcost   
	FROM 
		dbo.jobcostingexpenseitem T1
	GROUP BY
		T1.jobCosting
	) D1
inner join 
	(
	select 
		T1.costingid, 
		COUNT(T1.id) items
	FROM
		dbo.jobcostingitem T1
	GROUP BY
		T1.costingid
	) D2 on D1.jobCosting = D2.costingid
inner join
	(
	select distinct
		T1.costingid, 
		T1.jobitemid,
		T5.exchangerate jcexchangerate,
		T4.exchangerate jobownbuscomexcahngerate
	FROM
		dbo.jobcostingitem T1
		inner join dbo.jobcosting T2 on T1.costingid = T2.id
		inner join dbo.company T3 on T2.orgid = T3.coid
		inner join dbo.supportedcurrency T4 on T4.currencyid = T3.currencyid
		inner join dbo.supportedcurrency T5 on T5.currencyid = T2.currencyid
		inner join dbo.jobexpenseitem T6 on T2.jobid = T6.job
	) D3 on D1.jobCosting = D3.costingid
inner join 
(
SELECT
	T1.COSTINGID,
	T1.JOBITEMID
FROM
	MAXJOBCOSTINGFORJOBITEM T1
) D4 on D3.costingid = D4.costingid and D3.jobitemid = D4.JOBITEMID
;
GO

PRINT 'SERINVCOSTS'

insert into SERINVCOSTS
SELECT 
	D3.jobitemid,
	SUM((D1.sumfinalcost * (D3.jobownbuscomexcahngerate / D3.invexchangerate)) / D2.items) ser
FROM
	(
	SELECT
		T1.invoiceid,
		  sum(T2.finalcost) sumfinalcost
	FROM 
		dbo.invoiceitem T1
		inner join dbo.costs_service T2 on T1.servicecost = T2.costid
		inner join dbo.jobexpenseitem T3 on T1.expenseitem = T3.id
		inner join dbo.job T4 on T3.job = T4.jobid
		left join dbo.creditnote T5 on T1.invoiceid = T5.invid
	where 
		T2.costtype = 'invoice' 
		and T2.active = 1
		and T5.id is NULL
	group by
		T1.invoiceid
	) D1
inner join 
	(
	SELECT  
		T1.invoiceid,
		count(T1.jobitemid) items 
	FROM 
		dbo.invoiceitem T1
	GROUP BY 
		T1.invoiceid
	) D2 on D1.invoiceid = D2.invoiceid
inner join
	(
	SELECT distinct
		T1.invoiceid,
		T1.jobitemid,
		T5.exchangerate invexchangerate,
		T4.exchangerate jobownbuscomexcahngerate
	FROM 
		dbo.invoiceitem T1
		inner join dbo.invoice T2 on T1.invoiceid = T2.id
		inner join dbo.company T3 on T2.orgid = T3.coid
		inner join dbo.supportedcurrency T4 on T4.currencyid = T3.currencyid
		inner join dbo.supportedcurrency T5 on T5.currencyid = T2.currencyid
		inner join dbo.jobitem T6 on T1.jobitemid = T6.jobitemid
		inner join dbo.job T7 on T6.jobid = T7.jobid
		inner join dbo.jobexpenseitem T8 on T7.jobid = T8.job
	) D3 on D1.invoiceid = D3.invoiceid
GROUP BY
	D3.jobitemid
;
GO

PRINT 'DISPATCHEDTOCLIENTBY'

insert into DISPATCHEDTOCLIENTBY
SELECT
	S1.jobitemid,
	S1.scannedoutby
FROM
(
SELECT
	T1.jobitemid
	,T1.id
	,{fn CONCAT({fn CONCAT(T2.firstname,' ')},T2.lastname)} scannedoutby
FROM 
	dbo.jobitemaction T1
	inner join dbo.contact T2 on T1.startedbyid = T2.personid
where 
	T1.activityid = 53
	and T1.deleted = 0
) as S1
inner join
(
SELECT
	T1.jobitemid
	,max(T1.id) lastclientdespatchid
FROM 
	dbo.jobitemaction T1
	inner join dbo.contact T2 on T1.startedbyid = T2.personid
where 
	T1.activityid = 53
	and T1.deleted = 0
group by
	T1.jobitemid
) as S2 on S1.jobitemid = S2.jobitemid and S1.id = S2.lastclientdespatchid
;
GO

PRINT 'CLIENTPURCHASEORDER'

insert into CLIENTPURCHASEORDER
SELECT 
	T1.jobitemid,
	STUFF((SELECT '; ' + T2.ponos
			FROM
			(SELECT distinct
				T1.jobitemid
				,CONCAT(CASE WHEN T2.ponumber IS NOT NULL THEN CONCAT(T2.ponumber,' [PO]') ELSE '' END,CASE WHEN T3.ponumber IS NOT NULL THEN CONCAT(T3.ponumber,' [BPO]') ELSE '' END) ponos
			FROM 
				dbo.jobitempo T1
				left join dbo.po T2 on T1.poid = T2.poid
				left join dbo.bpo T3 on T1.bpoid = T3.poid
			) T2
			WHERE 
				T1.jobitemid = T2.jobitemid
			ORDER BY
				T2.ponos
			FOR XML PATH('')), 1, 1, '') ponos
FROM
	dbo.jobitempo T1
	left join dbo.po T2 on T1.poid = T2.poid
	left join dbo.bpo T3 on T1.bpoid = T3.poid
GROUP BY
	T1.jobitemid
;
GO

PRINT 'CALIBRATIONWITHJUDGEMENTCERTIFICATE'

insert into CALIBRATIONWITHJUDGEMENTCERTIFICATE
select	
	ocee.jobitemid,	
	ISNULL(oce.calibrationverificationstatus,'') CALIBRATIONVERIFICATIONSTATUS,	
	ISNULL(oce.optimization,0) OPTIMIZATION,	
	ISNULL(oce.repair,0) REPAIR,	
	ISNULL(oce.restriction,0) RESTRICTION,	
	ISNULL(oce.adjustment,0) ADJUSTMENT
from	
	dbo.certlink ocee	
	inner join dbo.certificate oce on oce.certid = ocee.certid	
	inner join (select 
					cee.jobitemid,
					max(ce.certid) maxcertid 
				FROM 
					dbo.certlink cee					
					inner join dbo.certificate ce on ce.certid = cee.certid and ce.caltype in (select caltypeid from dbo.calibrationtype where calibrationWithJudgement = 1)					
					inner join dbo.calibration caa on caa.id = ce.calid and caa.calibrationstatus = 32
				GROUP BY					
					cee.jobitemid
				) ftr on ftr.jobitemid = ocee.jobitemid and ftr.maxcertid = oce.certid
;
GO

PRINT 'INSERT KPI '

INSERT INTO KPI
select
	T8.orgid,
	T1.coname,
	T1.coid,
	T2.subname,
	T2.subdivid,
	T3.addr1, 
	T3.postcode,
	0 ITEMSHELD,
	0 ITEMSCAL,
	case when T4.name = '/' then ISNULL(T23.name,'/') else T4.name end MFR,
	case when T5.model = '/' then ISNULL(T7.modelname,'/') else T5.model end INSTMODEL,
	T5.descriptionid,
	T6.translation DESCRIPTION,
	T17.translation FAMILY,
	T19.translation DOMAIN,
	T7.serialno,
	T7.plantid,
	T7.modelid,
	ISNULL(T7.customerdescription,'') CUSTOMERDESCRIPTION,
	0 STDCALTIME,
	T7.plantno, 
	'NOT_APPLICABLE' CALIBRATIONVERIFICATIONSTATUS, 	
	NULL OPTIMIZATION,	
	NULL REPAIR,	
	NULL RESTRICTION,	
	NULL ADJUSTEMENT,
	T8.jobno, 
	T8.regdate,
	T8.typeid,
	case T24.documentlanguage
		when 'es_ES' then
			case T8.typeid 
				when 0 then 'Undefined'
				when 1 then 'Trabajo sobre equipos'
				when 2 then 'Trabajo precio único'
				when 3 then 'Valve Job'
				when 4 then 'Trabajo in situ'
				when 5 then 'Reservado'
			end 
		when 'fr_FR' then
			case T8.typeid 
				when 0 then 'Undefined'
				when 1 then 'Job standard'
				when 2 then 'Job prix unique'
				when 3 then 'Valve Job'
				when 4 then 'Job sur site'
				when 5 then 'Pre-reserva'
			end
		when 'de_DE' then
			case T8.typeid 
				when 0 then 'Undefined'
				when 1 then 'Standard-Auftrag'
				when 2 then 'Festpreis-Auftrag'
				when 3 then 'Valve Job'
				when 4 then 'Vor-Ort-Auftrag'
				when 5 then 'Reserviert'
			end
		when 'en_GB' then
			case T8.typeid 
				when 0 then 'Undefined'
				when 1 then 'Standard Job'
				when 2 then 'Single Price Job'
				when 3 then 'Valve Job'
				when 4 then 'Site Job'
				when 5 then 'Reserved'
			end
		else T24.documentlanguage
	end JOBTYPE,
	T9.jobitemid,
	'' CLIENTPURCHASEORDER,
	'' RETURNOPTION,
	NULL PROCID,
	'' REFERENCE,
	'' LAB,
	T13.servicetypeid,
	T14.translation CALTYPE,
	T9.stateid,
	T10.translation ITEMSTATUS,
	T7.statusid,
	case T24.documentlanguage
		when 'es_ES' then
			case T7.statusid 
				when 0 then 'En circulación'
				when 1 then 'B.E.R.'
				when 2 then 'No recuerdar'
				when 3 then 'Transferido'
			end
		when 'fr_FR' then
			case T7.statusid 
				when 0 then 'In circulation'
				when 1 then 'B.E.R.'
				when 2 then 'Do not recall'
				when 3 then 'Transferred'
			end
		when 'de_DE' then
			case T7.statusid 
				when 0 then 'Im Umlauf'
				when 1 then 'Reparatur unwirtschaftlich (B.E.R.)'
				when 2 then 'Nicht abrufen'
				when 3 then 'Übertragen'
			end
		when 'en_GB' then
			case T7.statusid 
				when 0 then 'In circulation'
				when 1 then 'B.E.R.'
				when 2 then 'Do not recall'
				when 3 then 'Transferred'
			end
		else T24.documentlanguage
	end	INSTRUMENTSTATUS,
	T9.itemno,
	'' CATEGORY,
	0 INVOICED,
	0.00 INVfinalcost,
	0.00 CALtotalcost,
	0.00 REPtotalcost,
	0.00 ADJtotalcost,
	0.00 PURtotalcost,
	0.00 SERfinalcost,
	0.00 IHCAL,
	0.00 TPCAL,
	0.00 IHREP,
	0.00 TPREP,
	0.00 IHADJ,
	0.00 TPADJ,
	0.00 SER,
	0.00 TRESCALTPCAL,
	0.00 TRESCALTPREP,
	0.00 TRESCALTPADJ,
	0.00 PURCHASE,
	0.00 INSPECT,
	0.00 TPCARR,
	0.00 CRCAL,
	0.00 CRREP,
	0.00 CRADJ,
	0.00 CRPUR,
	0.00 JOBSER,
	NULL FIRSTACTIVITY,
	NULL RECEIVEDINTRANSIT,
	NULL CONREV,
	'' CONREVBY,
	NULL PRECAL,
	NULL PRECALEND,
	NULL CERTIFICATESIGNED,
	'' CALIBRATIONENGINEER,
	NULL ISSUEJOBCOSTING,
	NULL CUSTAPPROVESJOBCOSTING,
	NULL AWAITINGDELNOTETOCLIENT,
	NULL DELNOTEFORRTNTOCLIENT,
	'' DELNOTENOFORRTNTOCLIENT,
	NULL DELNOTEFORTHIRDPARTY,
	NULL DELNOTEFORINTERNAL,
	NULL DELNOTEFORINTERNALRETURN,
	NULL DESPATCHEDTHIRDPARTY,
	NULL RECEIVEDBACKGOODSIN,
	NULL DISPTACHEDINTERNALTRANSIT,
	NULL FIRSTDISPATCHED,
	NULL RECEIVEDINTERNALTRANSIT,
	NULL LASTDISPATCHED,
	NULL DESPATCHEDTORETURNINGBUSINESSSUBDIVISION,
	'' DISPATCHEDTOCLIENTBY,
	NULL LASTACTIVITY,
	NULL WORKCOMPLETED,
	'' ONBEHALFOF,
	'' TPNAME,
	0.00 TPCOST,
	NULL THIRDPARTYGROUP,
	NULL TPPOCALDEPARTMENT,
	NULL TPPOCALNOMINAL,
	0.00 TPPOCALCOST,
	NULL TPPOREPDEPARTMENT,
	NULL TPPOREPNOMINAL,
	0.00 TPPOREPCOST,
	NULL CLIPRODELSCH,
	NULL RESRECDELDTEREQ,
	NULL CLIPROCOSDEC,
	NULL CLIPRODECFAUUNI,
	'' FAULTREPDESC,
	NULL FAULTREPISSUED,
	NULL FAULTREPAPPROVED,
	--0 AWAITINGPAYMENTDELAY,
	CURRENT_TIMESTAMP LASTMODIFIED
from
	dbo.company T1
	inner join dbo.subdiv T2 on T2.coid = T1.COID
	inner join dbo.contact T11 on T11.subdivid = T2.subdivid
	inner join dbo.job T8 on T8.personid = T11.personid 
	inner join dbo.jobitem T9 on T8.jobid = T9.jobid 
	inner join dbo.instrument T7 on T9.plantid = T7.plantid
	inner join dbo.address T3 on T7.addressid = T3.addrid 
	inner join dbo.instmodel T5 on T5.modelid = T7.modelid
	inner join dbo.mfr T4 on T5.mfrid = T4.mfrid
	left join dbo.mfr T23 on T7.mfrid = T23.mfrid
	inner join dbo.descriptiontranslation T6 on T5.descriptionid = T6.descriptionid 
	inner join dbo.itemstatetranslation T10 on T9.stateid = T10.stateid
	inner join dbo.calibrationtype T13 on T9.caltypeid = T13.caltypeid
	inner join dbo.servicetypeshortnametranslation T14 on T13.servicetypeid = T14.servicetypeid
	inner join dbo.description T15 on T5.descriptionid = T15.descriptionid
	inner join dbo.instmodelfamily T16 on T15.familyid = T16.familyid
	inner join dbo.instmodelfamilytranslation T17 on T16.familyid = T17.familyid
	inner join dbo.instmodeldomain T18 on T16.domainid = T18.domainid
	inner join dbo.instmodeldomaintranslation T19 on T18.domainid = T19.domainid
	inner join dbo.subdiv T22 on T8.orgid = T22.subdivid
	inner join dbo.company T24 on T22.coid = T24.coid
where
	T6.locale = T24.documentlanguage
	and T14.locale = T24.documentlanguage
	and T10.locale = T24.documentlanguage
	and T17.locale = T24.documentlanguage
	and T19.locale = T24.documentlanguage
order by
	T9.jobitemid
;
GO

PRINT 'INSERT KPI_CALIBRATION'

INSERT INTO KPI_CALIBRATION
select 
	T11.orgid,
	T1.jobitemid, 
	T2.caldate,
	datediff(MI,T2.starttime,T2.completetime) "Calculated Time",
	T13.subname,
	T9.subdivid,
	T9.name Lab,
	T4.reference "Procedure",
	T6.translation "Cal Type",
	T1.timespent "Entered Time",
	T4.estimatedTime "Standard Time",
	T3.firstname + ' ' + T3.lastname "Engineer",
	T3.personid,
	0 Turnover,
	CURRENT_TIMESTAMP LASTMODIFIED
from 
	dbo.calibration T2
	inner join dbo.callink T1 on T2.id = T1.calid
	inner join dbo.contact T3 on T2.completedby = T3.personid
	inner join dbo.procs T4 on T2.procedureid = T4.id
	inner join dbo.calibrationtype T5 on T2.caltypeid = T5.caltypeid
	inner join dbo.servicetypeshortnametranslation T6 on T5.servicetypeid = T6.servicetypeid
	inner join dbo.categorisedprocedure T7 on T4.id = T7.procid
	inner join dbo.procedurecategory T8 on T7.categoryid =T8.id
	inner join dbo.department T9 on T8.departmentid = T9.deptid
	inner join dbo.jobitem T10 on T1.jobitemid = T10.jobitemid
	inner join dbo.job T11 on T10.jobid = T11.jobid
	inner join dbo.subdiv T12 on T11.orgid = T12.subdivid
	inner join dbo.company T14 on T12.coid = T14.coid
	inner join dbo.subdiv T13 on T9.subdivid = T13.subdivid
where 
	T2.calprocessid <> 6
	and T2.calibrationstatus = 32
	and T6.locale = T14.documentlanguage
UNION ALL
SELECT
	S1.orgid,
	S1.jobitemid,
	S1.caldate,
	S2.[Calculated Time],
	S1.subname,
	S1.subdivid,
	S1.[Lab],
	S1.[Procedure],
	S1.[Cal Type],
	S2.[Entered Time],
	S1.[Standard Time],
	S2.[Engineer],
	S2.personid,
	S1.Turnover,
	S1.LASTMODIFIED
FROM
(
SELECT
	ROW_NUMBER() OVER (Partition by T1.jobitemid order by T1.jobitemid,T2.id) RN,
	T11.orgid,
    T1.jobitemid, 
	T2.caldate,
	T13.subname,
	T9.subdivid,
	T9.name Lab,
	T4.reference "Procedure",
	T6.translation "Cal Type",
	T4.estimatedTime "Standard Time",
	0 Turnover,
	CURRENT_TIMESTAMP LASTMODIFIED
FROM 
	dbo.callink T1
	inner join dbo.calibration T2 on T1.calid = T2.id
	inner join dbo.contact T3 on T2.completedby = T3.personid
	inner join dbo.procs T4 on T2.procedureid = T4.id
	inner join dbo.calibrationtype T5 on T2.caltypeid = T5.caltypeid
	inner join dbo.servicetypeshortnametranslation T6 on T5.servicetypeid = T6.servicetypeid
	inner join dbo.categorisedprocedure T7 on T4.id = T7.procid
	inner join dbo.procedurecategory T8 on T7.categoryid =T8.id
	inner join dbo.department T9 on T8.departmentid = T9.deptid
	inner join dbo.jobitem T10 on T1.jobitemid = T10.jobitemid
	inner join dbo.job T11 on T10.jobid = T11.jobid
	inner join dbo.subdiv T12 on T11.orgid = T12.subdivid
	inner join dbo.company T14 on T12.coid = T14.coid
	inner join dbo.subdiv T13 on T9.subdivid = T13.subdivid
where  
	T2.calprocessid = 6
	and T2.calibrationstatus = 32
	and T6.locale = T14.documentlanguage
) as S1
inner join
(
SELECT 
      ROW_NUMBER() OVER (Partition by T1.startstatusid,T1.jobitemid order by T1.jobitemid,T1.id) RN,
	  T1.jobitemid
	  ,datediff(MI,T1.startstamp,T1.endstamp) "Calculated Time"
	  ,T1.timespent "Entered Time"
	  ,T2.firstname + ' ' + T2.lastname "Engineer"
	  ,T2.personid
FROM 
	dbo.jobitemaction T1
	inner join dbo.contact T2 on T1.completedbyid = T2.personid 
WHERE  
	T1.deleted = 0
	and (
		--Awaiting calibration - Pre-calibration on hold activity
		(T1.activityid = 26 and T1.startstatusid in (19,176) and T1.endstatusid = 176)
		--Awaiting post-calibration - Post-calibration on hold activity
		or (T1.activityid = 61 and T1.startstatusid in (60,177) and T1.endstatusid = 177)
		--Awaiting onsite calibration - Onsite calibration on hold activity
		or (T1.activityid = 4048 and T1.startstatusid in (4047,4060) and T1.endstatusid = 4060) 
		--Onsite - awaiting post-calibration - Recorded onsite service - awaiting job costing activity
		or (T1.activityid = 4079 and T1.startstatusid in (4080,4049) and T1.endstatusid = 4049) 
	)
) as S2 on S1.RN = S2.RN and S1.jobitemid = S2.jobitemid
;
GO

COMMIT TRAN