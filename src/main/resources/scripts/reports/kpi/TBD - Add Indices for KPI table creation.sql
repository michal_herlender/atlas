-- Note ROLLBACK TRAN at end

USE [spain]
GO

BEGIN TRAN

CREATE NONCLUSTERED INDEX [IDX_jobitemaction_KPI_1]
ON [dbo].[jobitemaction] ([deleted])
INCLUDE ([jobitemid])
GO

CREATE NONCLUSTERED INDEX [IDX_jobitemaction_KPI_2]
ON [dbo].[jobitemaction] ([deleted])
INCLUDE ([activitydesc],[startstamp],[jobitemid],[startedbyid])
GO

CREATE NONCLUSTERED INDEX [IDX_jobitemaction_KPI_3]
ON [dbo].[jobitemaction] ([deleted])
INCLUDE ([activitydesc],[startstamp],[jobitemid])
GO

CREATE NONCLUSTERED INDEX [IDX_jobitemaction_KPI_4]
ON [dbo].[jobitemaction] ([deleted])
INCLUDE ([activitydesc],[endstamp],[jobitemid])
GO

CREATE NONCLUSTERED INDEX [IDX_jobitemaction_KPI_5]
ON [dbo].[jobitemaction] ([deleted],[endstatusid])
INCLUDE ([startstamp],[jobitemid])
GO

CREATE NONCLUSTERED INDEX [IDX_jobitemaction_KPI_6]
ON [dbo].[jobitemaction] ([deleted])
INCLUDE ([activitydesc],[jobitemid])
GO

CREATE NONCLUSTERED INDEX [IDX_jobitemaction_KPI_7]
ON [dbo].[jobitemaction] ([deleted],[activityid],[jobitemid])
INCLUDE ([startedbyid])
GO

CREATE NONCLUSTERED INDEX [IDX_purchaseorderitem_KPI_1]
ON [dbo].[purchaseorderitem] ([cancelled])
INCLUDE ([discountrate],[quantity],[totalcost],[orderid])
GO

CREATE NONCLUSTERED INDEX [IDX_calibration_KPI_1]
ON [dbo].[calibration] ([calibrationstatus],[completetime])
INCLUDE ([completedby],[procedureid])
GO

CREATE NONCLUSTERED INDEX [IDX_calibration_KPI_2]
ON [dbo].[calibration] ([calibrationstatus])
INCLUDE ([caltypeid],[procedureid])
GO

CREATE NONCLUSTERED INDEX [IDX_jobcosting_KPI_1]
ON [dbo].[jobcosting] ([orgid])
INCLUDE ([currencyid],[jobid])
GO

CREATE NONCLUSTERED INDEX [IDX_jobcostingitem_KPI_1]
ON [dbo].[jobcostingitem] ([costingid])
INCLUDE ([jobitemid])
GO

CREATE NONCLUSTERED INDEX [IDX_invoiceitem_KPI_1]
ON [dbo].[invoiceitem] ([expenseitem])
INCLUDE ([invoiceid],[servicecost])
GO

CREATE NONCLUSTERED INDEX [IDX_invoiceitem_KPI_2]
ON [dbo].[invoiceitem] ([servicecost])
INCLUDE ([invoiceid],[expenseitem])
GO

CREATE NONCLUSTERED INDEX [IDX_invoiceitem_KPI_3]
ON [dbo].[invoiceitem] ([jobitemid])
INCLUDE ([invoiceid],[repaircost_id])
GO

CREATE NONCLUSTERED INDEX [IDX_emailtemplate_KPI_1]
ON [dbo].[emailtemplate] ([component])

CREATE NONCLUSTERED INDEX [IDX_costs_adjustment_KPI_1]
ON [dbo].[costs_adjustment] ([costtype],[active])
INCLUDE ([finalcost])
GO

CREATE NONCLUSTERED INDEX [IDX_delivery_KPI_1]
ON [dbo].[delivery] ([deliverytype])
GO

CREATE NONCLUSTERED INDEX [IDX_costs_repair_KPI_1]
ON [dbo].[costs_repair] ([costtype],[active])
INCLUDE ([finalcost])
GO

ROLLBACK TRAN