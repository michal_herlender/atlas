/* INVACCS_COSTSSUMMARY VIEW */

CREATE VIEW INVACCS_COSTSSUMMARY AS
-- calibration costs
SELECT ii.id as iid, n.code      AS code,
                n.title,
				c.finalcost AS cost,
                ii.invoiceid AS invid,
				i.orgid
         FROM   invoice i,
         		invoiceitem ii,
                nominalcode n ,
                costs_calibration c
         WHERE  ii.calcost_id   = c.costid
         	AND ii.invoiceid = i.id
            AND i.pricingtype = 'CLIENT'
            AND c.nominalid     = n.id
            AND c.active        = 1
            AND ii.breakupcosts = 1	
         UNION ALL
-- repair costs		
         SELECT ii.id as iid, n.code       AS code,
                n.title,
         		c.finalcost AS cost,
                ii.invoiceid AS invid,
				i.orgid
         FROM   invoice i,
         		invoiceitem ii,
                nominalcode n ,
                costs_repair c
         WHERE  ii.repaircost_id = c.costid
         	AND ii.invoiceid = i.id
            AND i.pricingtype = 'CLIENT'
            AND c.nominalid      = n.id
            AND c.active         = 1
            AND ii.breakupcosts  = 1		
         UNION ALL
-- purchase costs	
         SELECT ii.id as iid, n.code       AS code,
                n.title,
         		c.finalcost AS cost,
                ii.invoiceid AS invid,
				i.orgid
         FROM   invoice i,
         		invoiceitem ii,
                nominalcode n ,
                costs_purchase c
         WHERE  ii.purchasecost_id = c.costid
         	AND ii.invoiceid = i.id
            AND i.pricingtype = 'CLIENT'
            AND c.nominalid        = n.id
            AND c.active           = 1
            AND ii.breakupcosts    = 1          
         UNION ALL
-- adjustment costs  
         SELECT ii.id as iid, n.code       AS code,
                n.title,
         		c.finalcost AS cost,
                ii.invoiceid AS invid,
				i.orgid
         FROM   invoice i,
         		invoiceitem ii,
                nominalcode n ,
                costs_adjustment c
         WHERE  ii.adjustmentcost_id = c.costid
         	AND ii.invoiceid = i.id
            AND i.pricingtype = 'CLIENT'
            AND c.nominalid          = n.id
            AND c.active             = 1
            AND ii.breakupcosts      = 1            
         UNION ALL
-- service costs  
         SELECT ii.id as iid, n.code       AS code,
                n.title,
         		c.finalcost AS cost,
                ii.invoiceid AS invid,
				i.orgid
         FROM   invoice i,
         		invoiceitem ii,
                nominalcode n ,
                costs_service c
         WHERE  ii.servicecost = c.costid
         	AND ii.invoiceid = i.id
            AND i.pricingtype = 'CLIENT'
            AND c.nominal          = n.id
            AND c.active             = 1
            AND ii.breakupcosts      = 1     
		UNION ALL
-- single item costs
         SELECT ii.id as iid, n.code       AS code,
                n.title,
         		ii.finalcost AS cost,
                ii.invoiceid AS invid,
				i.orgid
         FROM   invoice i,
         		invoiceitem ii,
                nominalcode n
         WHERE  ii.nominalid    = n.id
         	AND ii.invoiceid = i.id
            AND i.pricingtype = 'CLIENT'
            AND ii.breakupcosts = 0		
        UNION ALL
-- site costs		
        SELECT ii.id as iid, n.code       AS code,
               n.title,
        	   i.totalcost AS cost,
               i.id AS invid,
			   i.orgid
        FROM   invoice i,
               invoiceitem ii,
               nominalcode n
        WHERE  ii.nominalid    = n.id
           AND ii.invoiceid = i.id
           AND i.pricingtype = 'SITE'
           AND ii.itemno = 1  	    
        UNION ALL
-- carriage costs		
		SELECT 0 as iid, n.code  AS code,
                n.title,
				i.carriage AS cost,
                i.id AS invid,
				i.orgid
         FROM   invoice i,
                nominalcode n,
                nominalinvoicelookup nl
         WHERE nl.nominalid     = n.id
         	   and nl.nominaltype = 'carriage'
               and i.carriage != 0.00;
