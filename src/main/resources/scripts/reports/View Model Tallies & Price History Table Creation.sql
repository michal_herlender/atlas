USE [msdb]
GO

/****** Object:  Job [View Model Tallies & Price History Table Creation]    Script Date: 20/02/2019 10:17:29 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 20/02/2019 10:17:30 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'View Model Tallies & Price History Table Creation', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'View Model Tallies & Price History Table Creation', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'ITMZARCOM\SupportERP', 
		@notify_email_operator_name=N'Administrator', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Drop]    Script Date: 20/02/2019 10:17:31 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Drop', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''VIEWMODELTALLIES'') drop table VIEWMODELTALLIES
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''VIEWMODELJOBCOSTINGS'') drop table VIEWMODELJOBCOSTINGS
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''VIEWMODELQUOTATIONS'') drop table VIEWMODELQUOTATIONS
--
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''JOBS'') drop table JOBS
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''QUOTATIONS_TMP1'') drop table QUOTATIONS_TMP1
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''QUOTATIONS_TMP2'') drop table QUOTATIONS_TMP2
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''QUOTATIONS'') drop table QUOTATIONS
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''CERTIFICATES'') drop table CERTIFICATES
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''CALIBRATIONS'') drop table CALIBRATIONS
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''TPQUOTES'') drop table TPQUOTES
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''INSTRUMENTS'') drop table INSTRUMENTS
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''COMPANIES'') drop table COMPANIES
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''MAXJOBCOSTINGFORJOBITEM'') drop table MAXJOBCOSTINGFORJOBITEM
IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''MAXQUOTATIONVER'') drop table MAXQUOTATIONVER', 
		@database_name=N'spain', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create]    Script Date: 20/02/2019 10:17:32 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'create table VIEWMODELTALLIES
(ORGID INT
,MODELID INT NOT NULL
,JOBS INT
,QUOTATIONS INT
,LAST2YEARSQUOTATIONS INT
,CERTIFICATES INT
,CALIBRATIONS INT
,TPQUOTATIONS INT
,INSTRUMENTS INT
,COMPANIES INT
,LASTMODIFIED DATETIME NOT NULL
);
create table JOBS
(ORGID INT
,MODELID INT NOT NULL
,JOBS INT
);
INSERT INTO JOBS
select
T4.orgid,
T1.modelid, 
count(T2.jobitemid) jobs
from dbo.instrument T3, dbo.instmodel T1, dbo.jobitem T2, dbo.job T4
where T3.modelid = T1.modelid 
and T3.plantid = T2.plantid
and T2.jobid = T4.jobid
group by T4.orgid, T1.modelid 
;
create table QUOTATIONS_TMP1
(ORGID INT
,MODELID INT NOT NULL
,REGDATE VARCHAR(15)
,QUOTES INT
);
INSERT INTO QUOTATIONS_TMP1
select T2.orgid, T1.modelid, case  when T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-2,convert(datetime,CURRENT_TIMESTAMP))} then ''Last 2 years'' else ''Older'' end regdate, count(distinct T3.quoteid) quotes 
from dbo.quotationitem T3, dbo.instmodel T1, dbo.quotation T2
where T3.modelid = T1.modelid and T2.id = T3.quoteid
group by T2.orgid, T1.modelid, case  when T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-2,convert(datetime,CURRENT_TIMESTAMP))} then ''Last 2 years'' else ''Older'' end 
;
create table QUOTATIONS_TMP2
(ORGID INT
,MODELID INT NOT NULL
,REGDATE VARCHAR(15)
,QUOTES INT
);
INSERT INTO QUOTATIONS_TMP2
select T2.orgid, T1.modelid, case  when T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-2,convert(datetime,CURRENT_TIMESTAMP))} then ''Last 2 years'' else ''Older'' end regdate, count(distinct T3.quoteid) quotes 
from dbo.instrument T4, dbo.instmodel T1, dbo.quotationitem T3, dbo.quotation T2
where T4.modelid = T1.modelid and T3.plantid = T4.plantid and T2.id = T3.quoteid
group by T2.orgid, T1.modelid, case  when T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-2,convert(datetime,CURRENT_TIMESTAMP))} then ''Last 2 years'' else ''Older'' end 
;
create table QUOTATIONS
(ORGID INT
,MODELID INT NOT NULL
,QUOTES INT
,LAST2YEARS INT
);
INSERT INTO QUOTATIONS
select case when T1.ORGID is null then T2.ORGID else T1.ORGID end  orgid, case  when T1.MODELID is null then T2.MODELID else T1.MODELID end  modelid , sum(case  when (case  when T1.REGDATE is null then T2.REGDATE else T1.REGDATE end  = ''Older'') then (case  when T1.QUOTES is null then 0 else T1.QUOTES end  + case  when T2.QUOTES is null then 0 else T2.QUOTES end ) else 0 end ) + sum(case  when (case  when T1.REGDATE is null then T2.REGDATE else T1.REGDATE end  = ''Last 2 years'') then (case  when T1.QUOTES is null then 0 else T1.QUOTES end  + case  when T2.QUOTES is null then 0 else T2.QUOTES end ) else 0 end ) Quotes , sum(case  when case  when T1.REGDATE is null then T2.REGDATE else T1.REGDATE end  = ''Last 2 years'' then case  when T1.QUOTES is null then 0 else T1.QUOTES end  + case  when T2.QUOTES is null then 0 else T2.QUOTES end  else 0 end ) Last2years 
from (dbo.QUOTATIONS_TMP1 T1 FULL OUTER JOIN dbo.QUOTATIONS_TMP2 T2 on T1.MODELID = T2.MODELID and T1.REGDATE = T2.REGDATE and T1.ORGID = T2.ORGID)
group by case when T1.ORGID is null then T2.ORGID else T1.ORGID end, case  when T1.MODELID is null then T2.MODELID else T1.MODELID end 
;
create table CERTIFICATES
(ORGID INT
,MODELID INT NOT NULL
,CERTS INT
);
INSERT INTO CERTIFICATES
select T5.orgid, T1.modelid , count(T2.certid) certs
from dbo.instrument T3, dbo.instmodel T1, dbo.jobitem T4, dbo.certlink T2, dbo.job T5
where T3.modelid = T1.modelid and T3.plantid = T4.plantid and T2.jobitemid = T4.jobitemid and T4.jobid = T5.jobid
group by T5.orgid, T1.modelid
;
create table CALIBRATIONS
(ORGID INT
,MODELID INT NOT NULL
,CALS INT
);
INSERT INTO CALIBRATIONS
select T5.orgid, T1.modelid, count(T2.calid) cals 
from dbo.instrument T3, dbo.instmodel T1, dbo.jobitem T4, dbo.callink T2, dbo.job T5
where T3.modelid = T1.modelid and T3.plantid = T4.plantid and T2.jobitemid = T4.jobitemid and T4.jobid = T5.jobid
group by T5.orgid, T1.modelid
;
create table TPQUOTES
(ORGID INT
,MODELID INT NOT NULL
,TPQUOTES INT
);
INSERT INTO TPQUOTES
select T3.orgid, T1.modelid, count(distinct T2.tpquoteid) tpquotes 
from dbo.instmodel T1, dbo.tpquotationitem T2, dbo.tpquotation T3
where T1.modelid = T2.modelid and T2.tpquoteid = T3.id
group by T3.orgid, T1.modelid
;
create table INSTRUMENTS
(MODELID INT NOT NULL
,INSTRUMENTS INT
);
INSERT INTO INSTRUMENTS
select T1.modelid, count(T2.plantid) instruments 
from dbo.instrument T2, dbo.instmodel T1
where T2.modelid = T1.modelid
group by T1.modelid
;
create table COMPANIES
(MODELID INT NOT NULL
,COMPANIES INT
);
INSERT INTO COMPANIES
select T1.modelid, count(distinct T2.coid) companies 
from dbo.instrument T3, dbo.instmodel T1, dbo.company T2
where T3.modelid = T1.modelid and T3.coid = T2.coid
group by T1.modelid
;
INSERT INTO VIEWMODELTALLIES
select distinct T1.orgid, T1.modelid,
0,
0,
0,
0,
0,
0,
0,
0,
CURRENT_TIMESTAMP LASTMODIFIED
from (
select orgid, modelid from dbo.jobs
union
select orgid, modelid from dbo.quotations
union
select orgid, modelid from dbo.certificates
union
select orgid, modelid from dbo.calibrations
union
select orgid, modelid from dbo.quotations
union
select orgid, modelid from dbo.tpquotes
union
select null, modelid from dbo.instruments
union
select null, modelid from dbo.companies
) T1 order by 2 asc
;
MERGE INTO VIEWMODELTALLIES AS e
USING (SELECT 
ORGID, MODELID, JOBS
FROM JOBS) AS et (ORGID, MODELID, JOBS)
ON (e.modelid = et.modelid and e.orgid = et.orgid)
WHEN MATCHED THEN 
UPDATE SET JOBS=et.JOBS
;
MERGE INTO VIEWMODELTALLIES AS e
USING (SELECT 
ORGID, MODELID, QUOTES, LAST2YEARS 
FROM QUOTATIONS) AS et (ORGID, MODELID, QUOTES, LAST2YEARS)
ON (e.modelid = et.modelid and e.orgid = et.orgid)
WHEN MATCHED THEN 
UPDATE SET QUOTATIONS=et.QUOTES,LAST2YEARSQUOTATIONS=et.LAST2YEARS
;
MERGE INTO VIEWMODELTALLIES AS e
USING (SELECT 
ORGID, MODELID, CERTS 
FROM CERTIFICATES) AS et (ORGID, MODELID, CERTS)
ON (e.modelid = et.modelid and e.orgid = et.orgid)
WHEN MATCHED THEN 
UPDATE SET CERTIFICATES=et.CERTS
;
MERGE INTO VIEWMODELTALLIES AS e
USING (SELECT 
ORGID, MODELID, CALS 
FROM CALIBRATIONS) AS et (ORGID, MODELID, CALS)
ON (e.modelid = et.modelid and e.orgid = et.orgid)
WHEN MATCHED THEN 
UPDATE SET CALIBRATIONS=et.CALS
; 
MERGE INTO VIEWMODELTALLIES AS e
USING (SELECT 
ORGID, MODELID, TPQUOTES 
FROM TPQUOTES) AS et (ORGID, MODELID, TPQUOTES)
ON (e.modelid = et.modelid and e.orgid = et.orgid)
WHEN MATCHED THEN 
UPDATE SET TPQUOTATIONS=et.TPQUOTES
;
MERGE INTO VIEWMODELTALLIES AS e
USING (SELECT 
MODELID, INSTRUMENTS 
FROM INSTRUMENTS) AS et (MODELID, INSTRUMENTS)
ON (e.modelid = et.modelid)
WHEN MATCHED THEN 
UPDATE SET INSTRUMENTS=et.INSTRUMENTS
; 
MERGE INTO VIEWMODELTALLIES AS e
USING (SELECT 
MODELID, COMPANIES 
FROM COMPANIES) AS et (MODELID, COMPANIES)
ON (e.modelid = et.modelid)
WHEN MATCHED THEN 
UPDATE SET COMPANIES=et.COMPANIES
;
create table MAXJOBCOSTINGFORJOBITEM
(JOBITEMID INT NOT NULL
,COSTINGID INT NOT NULL
);
INSERT INTO MAXJOBCOSTINGFORJOBITEM
select jobitemid,max(costingid) costingid
from
(
select T1.jobitemid , T2.costingid
from jobitem T1, jobcostingitem T2
where T1.jobitemid = T2.jobitemid
) D1
group by jobitemid
;
create table VIEWMODELJOBCOSTINGS
(ORGID INT
,MODELID INT NOT NULL
,JOBCOSTINGID INT
,JOBCOSTINGITEMID INT
,COID INT
,PERSONID INT
,JOBITEMID INT
,SUBDIVID INT
,COSTNO VARCHAR(40)
,ITEM INT
,COMPANY VARCHAR(100)
,CONTACT VARCHAR(61)
,CALTYPE VARCHAR(10)
,REGDATE DATE
,DISCOUNT NUMERIC(10,2)
,FINALCOST NUMERIC(10,2)
,CURRENCYERSYMBOL VARCHAR(10)
,TURN INT
,DISPLAYCOLOUR VARCHAR(10)
,DISPLAYCOLOURFASTTRACK VARCHAR(10)
,ONSTOP TINYINT
,COMPANYACTIVE TINYINT
,CONTACTACTIVE TINYINT
,LASTMODIFIED DATETIME NOT NULL
);
INSERT INTO VIEWMODELJOBCOSTINGS
select 
T2.orgid
,T1.modelid 
,T2.id jobcostingid
,T3.id jobcostingitemid
,T4.coid
,T5.personid
,T9.jobitemid
,T10.subdivid
, {fn CONCAT({fn CONCAT(T14.jobno ,'' ver '')},CAST(T2.ver AS CHAR(254)))} "Cost No"
, T9.itemno Item
, T4.coname Company
, {fn CONCAT({fn CONCAT(T5.firstname,'' '')},T5.lastname)} Contact
, T13.shortname "Cal Type"
, CAST(T2.regdate AS DATE) "Reg Date"
, T3.discountrate Discount
, T12.finalcost "Final Cost"
, T11.currencyersymbol
, T9.turn
, T13.displaycolour
, T13.displaycolourfasttrack
, 0 onstop
, 0 companyactive
, 0 contactactive
,CURRENT_TIMESTAMP LASTMODIFIED
from 
dbo.instrument T8, 
dbo.instmodel T1, 
dbo.jobitem T9, 
dbo.jobcostingitem T3, 
dbo.jobcosting T2, 
dbo.contact T5, 
dbo.subdiv T10, 
dbo.company T4, 
dbo.calibrationtype T6, 
dbo.MAXJOBCOSTINGFORJOBITEM T7,
dbo.supportedcurrency T11,
dbo.costs_calibration T12,
dbo.servicetype T13,
dbo.job T14
where T8.modelid = T1.modelid 
and T8.plantid = T9.plantid 
and T9.jobitemid = T3.jobitemid 
and T2.id = T3.costingid 
and T5.personid = T2.personid 
and T5.subdivid = T10.subdivid 
and T10.coid = T4.coid 
and T9.caltypeid = T6.caltypeid 
and T9.jobid = T14.jobid
and T6.servicetypeid = T13.servicetypeid
and T7.JOBITEMID = T3.jobitemid 
and T7.COSTINGID = T3.costingid 
and T11.currencyid = T2.currencyid
and T3.calcost_id = T12.costid
and NOT T7.JOBITEMID is null 
and NOT T7.COSTINGID is null 
and T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-5,convert(datetime,CURRENT_TIMESTAMP))}
order by 1 asc , 7 desc, 3 asc
;
MERGE INTO VIEWMODELJOBCOSTINGS AS e
USING (SELECT 
ORGID, COMPANYID, ACTIVE 
FROM companysettingsforallocatedcompany) AS et (ORGID, COMPANYID, ACTIVE)
ON (e.ORGID = et.ORGID and e.COID = et.COMPANYID)
WHEN MATCHED THEN 
UPDATE SET COMPANYACTIVE=et.ACTIVE, CONTACTACTIVE=et.ACTIVE
;
create table MAXQUOTATIONVER
(QNO VARCHAR(30) NOT NULL
,ID INT NOT NULL
);
INSERT INTO MAXQUOTATIONVER
select qno,max(id) id
from quotation T1
group by qno
;
create table VIEWMODELQUOTATIONS
(ORGID INT
,MODELID INT NOT NULL
,QUOTATIONID INT
,QUOTATIONITEMID INT
,COID INT
,PERSONID INT
,SUBDIVID INT
,QUOTENO VARCHAR(30)
,ITEM INT
,COMPANY VARCHAR(100)
,CONTACT VARCHAR(61)
,CALTYPE VARCHAR(10)
,REGDATE DATE
,DISCOUNT NUMERIC(10,2)
,FINALCOST NUMERIC(10,2)
,CURRENCYERSYMBOL VARCHAR(10)
,DISPLAYCOLOUR VARCHAR(10)
,ONSTOP TINYINT
,COMPANYACTIVE TINYINT
,CONTACTACTIVE TINYINT
,LASTMODIFIED DATETIME NOT NULL
);
INSERT INTO VIEWMODELQUOTATIONS
select T2.orgid 
,T1.modelid
,T2.id quotationid
,T3.id quotationitemid
,T4.coid
,T5.personid
,T9.subdivid
, {fn CONCAT({fn CONCAT(T2.qno,'' rev '')},CAST(CAST(T2.ver AS INTEGER) AS CHAR(254)))} "Quote No"
, T3.itemno Item
, T4.coname Company
, {fn CONCAT({fn CONCAT(T5.firstname,'' '')},T5.lastname)} Contact
, T12.shortname "Cal Type"
, CAST(T2.regdate AS DATE) "Reg Date"
, T3.discountrate Discount
, T11.finalcost "Final Cost"
, T10.currencyersymbol
, T12.displaycolour
, 0 onstop
, 0 companyactive
, 0 contactactive
,CURRENT_TIMESTAMP LASTMODIFIED
from 
dbo.instrument T8, 
dbo.instmodel T1, 
dbo.quotationitem T3, 
dbo.quotation T2, 
dbo.contact T5, 
dbo.subdiv T9, 
dbo.company T4, 
dbo.calibrationtype T6, 
dbo.MAXQUOTATIONVER T7,
dbo.supportedcurrency T10,
dbo.costs_calibration T11,
dbo.servicetype T12
where T8.modelid = T1.modelid 
and T8.plantid = T3.plantid 
and T2.id = T3.quoteid 
and T5.personid = T2.personid 
and T9.subdivid = T5.subdivid 
and T4.coid = T9.coid 
and T3.caltype_caltypeid = T6.caltypeid 
and T6.servicetypeid = T12.servicetypeid
and T2.id = T7.ID 
and T10.currencyid = T2.currencyid
and T3.calcost_id = T11.costid
and T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-5,convert(datetime,CURRENT_TIMESTAMP))}
and NOT T7.ID is null
UNION
select T2.orgid
,T1.modelid
,T2.id quotationid
,T3.id quotationitemid
,T4.coid
,T5.personid
,T8.subdivid
, {fn CONCAT({fn CONCAT(T2.qno,'' rev '')},CAST(CAST(T2.ver AS INTEGER) AS CHAR(254)))} "Quote No" 
, T3.itemno Item
, T4.coname Company
, {fn CONCAT({fn CONCAT(T5.firstname,'' '')},T5.lastname)} Contact
, T12.shortname "Cal Type"
, CAST(T2.regdate AS DATE) "Reg Date"
, T3.discountrate Discount
, T11.finalcost "Final Cost"
, T9.currencyersymbol
, T12.displaycolour
, 0 onstop
, 0 companyactive
, 0 contactactive
,CURRENT_TIMESTAMP LASTMODIFIED
from dbo.instmodel T1, 
dbo.quotationitem T3, 
dbo.quotation T2, 
dbo.contact T5, 
dbo.subdiv T8, 
dbo.company T4, 
dbo.calibrationtype T6, 
dbo.MAXQUOTATIONVER T7,
dbo.supportedcurrency T9,
dbo.costs_calibration T11,
dbo.servicetype T12
where T1.modelid = T3.modelid 
and T2.id = T3.quoteid 
and T5.personid = T2.personid 
and T8.subdivid = T5.subdivid 
and T4.coid = T8.coid 
and T3.caltype_caltypeid = T6.caltypeid 
and T6.servicetypeid = T12.servicetypeid
and T2.id = T7.ID 
and T9.currencyid = T2.currencyid
and T3.calcost_id = T11.costid
and T2.regdate >= {fn TIMESTAMPADD(SQL_TSI_YEAR,-5,convert(datetime,CURRENT_TIMESTAMP))}
and NOT T7.ID is null
order by 1 asc , 7 desc
;
MERGE INTO VIEWMODELQUOTATIONS AS e
USING (SELECT 
ORGID, COMPANYID, ACTIVE 
FROM companysettingsforallocatedcompany) AS et (ORGID, COMPANYID, ACTIVE)
ON (e.ORGID = et.ORGID and e.COID = et.COMPANYID)
WHEN MATCHED THEN 
UPDATE SET COMPANYACTIVE=et.ACTIVE, CONTACTACTIVE=et.ACTIVE
;', 
		@database_name=N'spain', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Clean Up]    Script Date: 20/02/2019 10:17:32 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Clean Up', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'drop table JOBS;
drop table QUOTATIONS_TMP1;
drop table QUOTATIONS_TMP2;
drop table QUOTATIONS;
drop table CERTIFICATES;
drop table CALIBRATIONS;
drop table TPQUOTES;
drop table INSTRUMENTS;
drop table COMPANIES;
drop table MAXJOBCOSTINGFORJOBITEM;
drop table MAXQUOTATIONVER;', 
		@database_name=N'spain', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Houly', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=127, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20160410, 
		@active_end_date=99991231, 
		@active_start_time=45000, 
		@active_end_time=210000, 
		@schedule_uid=N'7d52b0ac-e8fd-442b-b163-d56ded5639b6'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

