USE [spain]

BEGIN TRAN

declare @PCMAXJC table
(JOBITEMID INT NOT NULL PRIMARY KEY
,COSTINGID INT NOT NULL
);
INSERT INTO @PCMAXJC
select D1.jobitemid, D2.costingid
from
(
SELECT  a1.jobitemid,
   MAX(a2.regdate) AS maxregdate
FROM     jobcosting a2
   INNER JOIN jobcostingitem a1
   ON       a1.costingid = a2.id
WHERE    a1.jobitemid IN
    (SELECT DISTINCT ji.jobitemid
    FROM             job j
		     INNER JOIN jobitem ji
		     ON               ji.jobid = j.jobid
			 INNER JOIN itemstate its
			 ON				ji.stateid = its.stateid
		     INNER JOIN jobstatus js
		     ON               j.statusid = js.statusid
    WHERE            js.statusid = 1 and its.active = 0
    )
GROUP BY jobitemid
)  D1
INNER JOIN
(
SELECT distinct a1.jobitemid,
   a2.regdate, a2.id costingid
FROM     jobcosting a2
   INNER JOIN jobcostingitem a1
   ON       a1.costingid = a2.id
WHERE    a1.jobitemid IN
    (SELECT DISTINCT ji.jobitemid
    FROM             job j
		     INNER JOIN jobitem ji
		     ON               ji.jobid = j.jobid
			 INNER JOIN itemstate its
			 ON				ji.stateid = its.stateid
		     INNER JOIN jobstatus js
		     ON               j.statusid = js.statusid
    WHERE            js.statusid = 1 and its.active = 0
    )
) D2
ON D1.jobitemid = D2.jobitemid and D1.maxregdate = D2.regdate
;
declare @PCAIJI table
(orgid INT,
jobid INT,
datecomplete DATE,
estcarout DECIMAL(8,2),
jobno VARCHAR(30),
rate DECIMAL(8,2),
regdate DATE,
name VARCHAR(100),
description VARCHAR(50),
coname VARCHAR(100),
coid INT,
subname VARCHAR(75),
jobitemid INT NOT NULL PRIMARY KEY,
itemno CHAR(3)
);
insert into @PCAIJI
select distinct
	T6.coid , 
	T1.jobid ,
	T1.datecomplete ,
	T1.estcarout , 
	T1.jobno , 
	T1.rate ,
	T1.regdate , 
	T2.name ,
	case T1.typeid 
		when 0 then 'Undefined'
		when 1 then 'Standard Job'
		when 2 then 'Single Price Job'
		when 3 then 'Valve Job'
		when 4 then 'Onsite Job'
	end description,
	T4.coname , 
	T4.coid , 
	T6.subname,
	T5.jobitemid, 
	T5.itemno
from 
	job T1,
	jobstatus T2,
	contact T11,
	subdiv T12,
	company T4,
	subdiv T6,
	jobitem T5 left join invoiceitem T7 on T5.jobitemid = T7.jobitemid
	inner join itemstate T8 on T5.stateid = T8.stateid
where 
	T1.statusid = T2.statusid
	and T1.personid = T11.personid
	and T11.subdivid = T12.subdivid
	and T12.coid = T4.COID
	and T1.jobid = T5.jobid
	and T1.orgid = T6.subdivid
	and (T7.jobitemid is null or T7.jobitemid in 
												(select 
													distinct jobitemid 
												from 
													(SELECT 
														jobitemid, 
														max(cast(addtoaccount as tinyint)) maxaddtoaccount 
													FROM 
														dbo.invoiceitem ii, 
														dbo.invoice i, 
														dbo.invoicetype it 
													where 
														ii.invoiceid = i.id 
														and i.typeid = it.id 
													group by jobitemid) D1 
												where 
													D1.maxaddtoaccount = 0)
												)
	and T2.statusid = 1
	and T8.active = 0
;
declare @PCAIJC table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @PCAIJC
select
	D1.jobitemid,
	D1.cal,
	D1.rep,
	D1.adj,
	D1.pur,
	D1.total
from
(
select 
	T1.jobitemid  ,
	T1.costingid  , 
	((T2.finalcost * T2.active) / (T9.exchangerate / T8.exchangerate)) cal ,
	((T3.finalcost * T3.active) / (T9.exchangerate / T8.exchangerate)) rep ,
	((T4.finalcost * T4.active) / (T9.exchangerate / T8.exchangerate)) adj ,
	((T5.finalcost * T5.active) / (T9.exchangerate / T8.exchangerate)) pur ,
	(((T2.finalcost * T2.active) + (T3.finalcost * T3.active) + (T4.finalcost * T4.active) + (T5.finalcost * T5.active)) / (T9.exchangerate / T8.exchangerate)) total
from 
	jobcostingitem T1
	inner join costs_calibration T2 on T1.calcost_id = T2.costid
	inner join costs_repair T3 on T1.repaircost_id = T3.costid
	inner join costs_adjustment T4 on T1.adjustmentcost_id = T4.costid
	inner join costs_purchase T5 on T1.purchasecost_id = T5.costid
	inner join jobcosting T6 on T1.costingid = T6.id
	inner join company T7 on T6.orgid = T7.coid
	inner join supportedcurrency T8 on T8.currencyid = T7.currencyid
	inner join supportedcurrency T9 on T9.currencyid = T6.currencyid
where 
	T1.jobitemid IN
		(SELECT DISTINCT ji.jobitemid
		FROM             job j
				 INNER JOIN jobitem ji
				 ON               ji.jobid = j.jobid
				 INNER JOIN itemstate its
				 ON				ji.stateid = its.stateid
				 INNER JOIN jobstatus js
				 ON               j.statusid = js.statusid
		WHERE            js.statusid = 1 and its.active = 0
		)
) D1
inner join 
(
SELECT
	T1.COSTINGID,
	T1.JOBITEMID
FROM
	@PCMAXJC T1
) D2 on D1.costingid = D2.costingid and D1.jobitemid = D2.JOBITEMID
;
declare @PCAICR table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @PCAICR
select 
	T1.jobitemid,
	((T2.finalcost * T2.active) / (T10.exchangerate / T7.exchangerate)) cal ,
	((T3.finalcost * T3.active) / (T10.exchangerate / T7.exchangerate)) rep ,
	((T4.finalcost * T4.active) / (T10.exchangerate / T7.exchangerate)) adj ,
	((T5.finalcost * T5.active) / (T10.exchangerate / T7.exchangerate)) pur ,
	((case when T2.finalcost is null
		 then 0 
		 else (T2.finalcost * T2.active) 
	  end + 
	  case when T3.finalcost is null 
		then 0 
		else (T3.finalcost * T3.active)
	  end + 
	  case when T4.finalcost is null 
		then 0 
		else (T4.finalcost * T4.active)
	  end + 
	  case when T5.finalcost is null 
		then 0 
		else (T5.finalcost * T5.active)
	   end) / (T10.exchangerate / T7.exchangerate)) total
from 
	jobitem T1 
	inner join job T6 on T1.jobid = T6.jobid 
	inner join supportedcurrency T7 on T7.currencyid = T6.currencyid
	inner join subdiv T8 on T6.orgid = T8.subdivid
	inner join company T9 on T8.coid = T9.coid
	inner join supportedcurrency T10 on T9.currencyid = T10.currencyid
	left join costs_calibration T2 on T1.calcost_id = T2.costid 
	left join costs_repair T3 on T1.repcost_id = T3.costid 
	left join costs_adjustment T4 on T1.adjust_id = T4.costid 
	left join costs_purchase T5 on T1.salescost_id = T5.costid
WHERE    
	T1.jobitemid IN
    (SELECT DISTINCT ji.jobitemid
    FROM             job j
		     INNER JOIN jobitem ji
		     ON               ji.jobid = j.jobid
			 INNER JOIN itemstate its
			 ON				ji.stateid = its.stateid
		     INNER JOIN jobstatus js
		     ON               j.statusid = js.statusid
    WHERE            js.statusid = 1 and its.active = 0
    )
;
declare @PCINV table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @PCINV
select 
	T1.jobitemid,
	sum(((T2.finalcost * T2.active) / (T9.exchangerate / T8.exchangerate))) cal ,
	sum(((T3.finalcost * T3.active) / (T9.exchangerate / T8.exchangerate))) rep ,
	sum(((T4.finalcost * T4.active) / (T9.exchangerate / T8.exchangerate))) adj ,
	sum(((T5.finalcost * T5.active) / (T9.exchangerate / T8.exchangerate))) pur ,
	sum((((T2.finalcost * T2.active) + (T3.finalcost * T3.active)  + (T4.finalcost * T4.active)  + (T5.finalcost * T5.active)) / (T9.exchangerate / T8.exchangerate))) total
from 
	invoiceitem T1
	inner join costs_calibration T2 on T1.calcost_id = T2.costid
	inner join costs_repair T3 on T1.repaircost_id = T3.costid
	inner join costs_adjustment T4 on T1.adjustmentcost_id = T4.costid
	inner join costs_purchase T5 on T1.purchasecost_id = T5.costid
	inner join invoice T6 on T1.invoiceid = T6.id
	inner join company T7 on T6.orgid = T7.coid
	inner join supportedcurrency T8 on T8.currencyid = T7.currencyid
	inner join supportedcurrency T9 on T9.currencyid = T6.currencyid
where 
	T6.typeid <> 5
	and T1.jobitemid IN
		(SELECT DISTINCT ji.jobitemid
		FROM             job j
				 INNER JOIN jobitem ji
				 ON               ji.jobid = j.jobid
				 INNER JOIN itemstate its
				 ON				ji.stateid = its.stateid
				 INNER JOIN jobstatus js
				 ON               j.statusid = js.statusid
		WHERE            js.statusid = 1 and its.active = 0
		)
GROUP BY 
	T1.jobitemid
;
declare @PCSERJOBCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @PCSERJOBCOSTS
SELECT 
	D3.jobitemid,
	((D1.sumcost * (D3.jobownbuscomexcahngerate / D3.jobexchangerate)) / D2.items) ser
FROM
	(
	SELECT
		T2.jobid,
		sum(T1.cost) sumcost
	FROM 
		dbo.jobexpenseitem T1
		inner join dbo.job T2 on T1.job = T2.jobid
	group by
		T2.jobid
	) D1
inner join 
	(
	SELECT
		T1.jobid
		,count(T1.jobitemid) items     
	FROM 
		dbo.jobitem T1
		inner join dbo.jobexpenseitem T2 on T1.jobid = T2.job
	GROUP BY
		T1.jobid
	) D2 on D1.jobid = D2.jobid
inner join
	(
	SELECT distinct
		T1.jobid,
		T1.jobitemid,
		T6.exchangerate jobexchangerate,
		T5.exchangerate jobownbuscomexcahngerate
	FROM 
		dbo.jobitem T1
		inner join dbo.job T2 on T1.jobid = T2.jobid
		inner join dbo.subdiv T3 on T2.orgid = T3.subdivid
		inner join dbo.company T4 on T3.coid = T4.coid
		inner join dbo.supportedcurrency T5 on T5.currencyid = T4.currencyid
		inner join dbo.supportedcurrency T6 on T6.currencyid = T2.currencyid
		inner join dbo.jobexpenseitem T7 on T2.jobid = T7.job
	) D3 on D1.jobid = D3.jobid
;
declare @NAPSERJCCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
declare @PCSERJCCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @PCSERJCCOSTS
SELECT
	D3.jobitemid,
	((D1.sumfinalcost * (D3.jobownbuscomexcahngerate / D3.jcexchangerate)) / D2.items) ser
FROM
	(
	SELECT 
		T1.jobCosting,
		sum(T1.finalcost) sumfinalcost   
	FROM 
		dbo.jobcostingexpenseitem T1
	GROUP BY
		T1.jobCosting
	) D1
inner join 
	(
	select 
		T1.costingid, 
		COUNT(T1.id) items
	FROM
		dbo.jobcostingitem T1
	GROUP BY
		T1.costingid
	) D2 on D1.jobCosting = D2.costingid
inner join
	(
	select 
		T1.costingid, 
		T1.jobitemid,
		T5.exchangerate jcexchangerate,
		T4.exchangerate jobownbuscomexcahngerate
	FROM
		dbo.jobcostingitem T1
		inner join jobcosting T2 on T1.costingid = T2.id
		inner join company T3 on T2.orgid = T3.coid
		inner join supportedcurrency T4 on T4.currencyid = T3.currencyid
		inner join supportedcurrency T5 on T5.currencyid = T2.currencyid
	) D3 on D1.jobCosting = D3.costingid
inner join 
(
SELECT
	T1.COSTINGID,
	T1.JOBITEMID
FROM
	@PCMAXJC T1
) D4 on D3.costingid = D4.costingid and D3.jobitemid = D4.JOBITEMID
;
declare @PCSERINVCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @PCSERINVCOSTS
SELECT 
	D3.jobitemid,
	SUM((D1.sumfinalcost * (D3.jobownbuscomexcahngerate / D3.invexchangerate)) / D2.items) ser
FROM
	(
	SELECT
		T1.invoiceid,
		  sum(T2.finalcost) sumfinalcost
	FROM 
		dbo.invoiceitem T1
		inner join dbo.costs_service T2 on T1.servicecost = T2.costid
		inner join dbo.jobexpenseitem T3 on T1.expenseitem = T3.id
		inner join dbo.job T4 on T3.job = T4.jobid
		left join dbo.creditnote T5 on T1.invoiceid = T5.invid
	where 
		T2.costtype = 'invoice' 
		and T2.active = 1
		and T5.id is NULL
	group by
		T1.invoiceid
	) D1
inner join 
	(
	SELECT  
		T1.invoiceid,
		count(T1.jobitemid) items 
	FROM 
		dbo.invoiceitem T1
	GROUP BY 
		T1.invoiceid
	) D2 on D1.invoiceid = D2.invoiceid
inner join
	(
	SELECT  
		T1.invoiceid,
		T1.jobitemid,
		T5.exchangerate invexchangerate,
		T4.exchangerate jobownbuscomexcahngerate
	FROM 
		dbo.invoiceitem T1
		inner join invoice T2 on T1.invoiceid = T2.id
		inner join company T3 on T2.orgid = T3.coid
		inner join supportedcurrency T4 on T4.currencyid = T3.currencyid
		inner join supportedcurrency T5 on T5.currencyid = T2.currencyid
	WHERE
		T1.jobitemid is not NULL
	) D3 on D1.invoiceid = D3.invoiceid
GROUP BY
	D3.jobitemid
;
create table PARTCOMPLETED
(orgid INT,
coname VARCHAR(100),
coid INT,
subname VARCHAR(75),
jobno VARCHAR(30),
itemno CHAR(3),
datecomplete DATE,
jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
ser DECIMAL(8,2),
total DECIMAL(8,2),
LASTMODIFIED DATETIME
);
insert into PARTCOMPLETED
select 
	T1.orgid ,  
	T1.coname  ,
	T1.coid ,
	T1.subname,
	T1.jobno  , 
	T1.itemno  , 
	T1.datecomplete  , 
	T1.JOBITEMID , 
	ISNULL(T5.cal,ISNULL(T4.cal,T3.cal)) cal ,
	ISNULL(T5.rep,ISNULL(T4.rep,T3.rep)) rep ,
	ISNULL(T5.adj,ISNULL(T4.adj,T3.adj)) adj ,
	ISNULL(T5.pur,ISNULL(T4.pur,T3.pur)) pur ,
	ISNULL(ISNULL(T8.ser,ISNULL(T7.ser,T6.ser)),0) ser ,
	ISNULL(T5.total,ISNULL(T4.total,T3.total)) + ISNULL(ISNULL(T8.ser,ISNULL(T7.ser,T6.ser)),0) total ,
	CURRENT_TIMESTAMP
from 
	@PCAIJI T1 
	LEFT JOIN @PCAICR T3 on T1.jobitemid = T3.jobitemid
	LEFT JOIN @PCAIJC T4 on T1.jobitemid = T4.jobitemid
	LEFT JOIN @PCINV T5 on T1.jobitemid = T5.jobitemid
	LEFT JOIN @PCSERJOBCOSTS T6 on T1.jobitemid = T6.jobitemid
	LEFT JOIN @PCSERJCCOSTS T7 on T1.jobitemid = T7.jobitemid
	LEFT JOIN @PCSERINVCOSTS T8 on T1.jobitemid = T8.jobitemid
;
declare @PCALLJOBITEMS table
(orgid INT,
jobno VARCHAR(30),
coname VARCHAR(100),
coid INT,
subname VARCHAR(75),
jobitemid INT NOT NULL PRIMARY KEY,
LASTMODIFIED DATETIME
);
insert into @PCALLJOBITEMS
select distinct
	T1.orgid, 
	T1.jobno , 
	T1.coname, 
	T1.coid,
	T1.subname,
	T2.jobitemid, 
	CURRENT_TIMESTAMP
from 
	PARTCOMPLETED T1
	inner join job T3 on T1.jobno = T3.jobno 
	inner join jobitem T2 on T3.jobid = T2.jobid
;
create table PARTUNCOMPLETED
(orgid INT,
Lab VARCHAR (40),
jobno VARCHAR(30),
itemno CHAR(3),
coname VARCHAR(100),
coid INT,
subname VARCHAR(75),
jobitemid INT NOT NULL PRIMARY KEY,
LASTMODIFIED DATETIME
);
insert into PARTUNCOMPLETED
select 
	T1.orgid,
	CASE WHEN d.name IS NOT NULL THEN d.name ELSE 'Unknown' END as Lab, 
	j.jobno "Job No", 
	ji.itemno "Item No", 
	T1.coname, 
	T1.coid,
	T1.subname,
	T1.jobitemid, 
	CURRENT_TIMESTAMP
from 
	@PCALLJOBITEMS T1 
	left join dbo.PARTCOMPLETED T2 on T1.jobitemid = T2.jobitemid
	left join dbo.jobitem ji on ji.jobitemid = T1.jobitemid 
	left join dbo.job j on ji.jobid  = j.jobid
	left join dbo.jobitemworkrequirement jiwr on jiwr.id = ji.nextworkreqid 
	left join dbo.workrequirement wr on jiwr.reqid = wr.id 
	left join dbo.department d on d.deptid = wr.deptid
where 
	T2.jobitemid is null 
	and ji.stateid not in (48)
group by 
	T1.orgid,
	d.name,
	j.jobno, 
	ji.itemno, 
	T1.coname, 
	T1.coid, 
	T1.subname, 
	T1.jobitemid
;

COMMIT TRAN