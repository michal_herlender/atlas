-- OLD SSRS report for WIP report
-- 2021-07-31 : Updated to remove states 4160,4144 
-- (Service continued on new job item, old job item completed, Job item completed at same address as instrument address)
-- Note ROLLBACK TRAN at end, change after testing

USE [atlas]

BEGIN TRAN

declare @OGMAXJC table
(JOBITEMID INT NOT NULL PRIMARY KEY
,COSTINGID INT NOT NULL
);
INSERT INTO @OGMAXJC
select 
	D1.jobitemid, 
	D2.costingid
from
(
SELECT  
	a1.jobitemid,
	MAX(a2.regdate) AS maxregdate
FROM
	jobcosting a2
	INNER JOIN jobcostingitem a1 ON a1.costingid = a2.id
WHERE    
	a1.jobitemid IN
					(SELECT DISTINCT 
						ji.jobitemid
					FROM
						job j
						INNER JOIN jobitem ji ON ji.jobid = j.jobid
						INNER JOIN jobstatus js ON j.statusid = js.statusid
					WHERE
						js.statusid = 1 and
						ji.stateid in (27,31,33,35,50,52,85,167,4049,4061,4063,4070,4160,4144)
					)
GROUP BY 
	jobitemid
)  D1
INNER JOIN
(
SELECT distinct 
	a1.jobitemid,
	a2.regdate, 
	a2.id costingid
FROM
	jobcosting a2
	INNER JOIN jobcostingitem a1 ON a1.costingid = a2.id
WHERE
	a1.jobitemid IN
					(SELECT DISTINCT 
						ji.jobitemid
					FROM 
						job j
						INNER JOIN jobitem ji ON ji.jobid = j.jobid
						INNER JOIN jobstatus js ON j.statusid = js.statusid
					WHERE
						js.statusid = 1 and
						ji.stateid in (27,31,33,35,50,52,85,167,4049,4061,4063,4070,4160,4144)
					)
) D2
ON D1.jobitemid = D2.jobitemid and D1.maxregdate = D2.regdate
;
declare @OGAIJI table
(jobid INT,
datecomplete DATE,
estcarout DECIMAL(8,2),
jobno VARCHAR(30),
lastContractReviewBy VARCHAR(61),
businesssubname VARCHAR(75),
analyticalcenter VARCHAR(255),
rate DECIMAL(8,2),
regdate DATE,
name VARCHAR(100),
description VARCHAR(50),
coname VARCHAR(100),
legalidentifier VARCHAR(255),
coid INT,
subname VARCHAR(75),
jobitemid INT NOT NULL PRIMARY KEY,
itemno CHAR(3),
itemstate VARCHAR(200),
instmodeltranslation VARCHAR(1000),
caldate DATE,
orgid INT
);
insert into @OGAIJI
select 
	D2.jobid,
	D2.datecomplete,
	D2.estcarout,
	D2.jobno,
	D2.lastContractReviewBy,
	D2.businesssubname,
	D2.analyticalcenter,
	D2.rate,
	D2.regdate,
	D2.name,
	D2.Description,
	D2.coname,
	D2.legalidentifier,
	D2.coid,
	D2.subname,
	D2.jobitemid,
	D2.itemno,
	D2.itemstate,
	D2.instmodeltranslation,
	D3.caldate,
	D2.ORGID
from
(
select distinct
	T1.jobid ,
	T1.datecomplete ,
	T1.estcarout , 
	T1.jobno ,
	CONCAT(T27.firstname,' ',T27.lastname) lastContractReviewBy,
	T6.subname businesssubname ,
	T6.analyticalcenter ,
	T1.rate ,
	T1.regdate , 
	T2.name ,
	case T24.locale
		when 'es_ES' then
			case T1.typeid 
				when 0 then 'Undefined'
				when 1 then 'Trabajo sobre equipos'
				when 2 then 'Trabajo precio único'
				when 3 then 'Valve Job'
				when 4 then 'Trabajo in situ'
			end
		when 'fr_FR' then
			case T1.typeid 
				when 0 then 'Undefined'
				when 1 then 'Job standard'
				when 2 then 'Job prix unique'
				when 3 then 'Valve Job'
				when 4 then 'Job sur site'
			end
		when 'en_GB' then
			case T1.typeid 
				when 0 then 'Undefined'
				when 1 then 'Standard Job'
				when 2 then 'Single Price Job'
				when 3 then 'Valve Job'
				when 4 then 'Onsite Job'
			end
		when 'en_US' then
			case T1.typeid 
				when 0 then 'Undefined'
				when 1 then 'Standard Job'
				when 2 then 'Single Price Job'
				when 3 then 'Valve Job'
				when 4 then 'Onsite Job'
			end
	end description,
	T4.coname ,
	T4.legalidentifier,
	T4.coid , 
	T12.subname,
	T5.jobitemid,
	T5.itemno,
	T25.translation itemstate,
	T26.translation instmodeltranslation,
	T6.coid ORGID
from 
	job T1
	inner join jobstatus T2 on T1.statusid = T2.statusid
	inner join contact T11 on T1.personid = T11.personid
	inner join subdiv T12 on T11.subdivid = T12.subdivid
	inner join company T4 on T12.coid = T4.COID
	inner join subdiv T6 on T1.orgid = T6.subdivid
	inner join company T23 on T6.coid = T23.coid
	inner join contact T24 on T23.defaultbusinesscontact = T24.personid
	inner join jobitem T5 on T1.jobid = T5.jobid
	inner join itemstate T13 on T5.stateid = T13.stateid
	left join jobitemnotinvoiced T14 on T5.jobitemid = T14.jobitemid
	left join invoiceitem T7 on T5.jobitemid = T7.jobitemid
	inner join instrument T17 on T5.plantid = T17.plantid
	inner join instmodeltranslation T26 on T17.modelid = T26.modelid
	inner join itemstatetranslation T25 on T13.stateid = T25.stateid
	left join contact T27 on T5.lastContractReviewBy_personid = T27.personid 
where 
	T25.locale = T24.locale
	and T26.locale = T24.locale
	and (T7.jobitemid is null or T7.jobitemid in 
												(select distinct 
													jobitemid 
												from 
													(SELECT 
														jobitemid, 
														max(cast(addtoaccount as tinyint)) maxaddtoaccount 
													FROM 
														dbo.invoiceitem ii, 
														dbo.invoice i, 
														dbo.invoicetype it 
													where 
														ii.invoiceid = i.id 
														and i.typeid = it.id 
													group by 
														jobitemid
													) D1 
												where D1.maxaddtoaccount = 0)
												)
	and T14.jobitemid is null
	and T2.statusid = 1
	and T5.stateid in (27,31,33,35,50,52,85,167,4049,4061,4063,4070,4160,4144)
) as D2 left join
(
select 
	T1.jobitemid,
	max(T2.caldate) caldate
from 
	dbo.callink T1
	inner join dbo.calibration T2 on T1.calid = T2.id
group by 
	T1.jobitemid
) as D3 on D2.jobitemid = D3.jobitemid
;
declare @OGAIJC table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @OGAIJC
select
	D1.jobitemid,
	D1.cal,
	D1.rep,
	D1.adj,
	D1.pur,
	D1.total
from
(
select 
	T1.jobitemid  ,
	T1.costingid  , 
	((T2.finalcost * T2.active) / (T9.exchangerate / T8.exchangerate)) cal ,
	((T3.finalcost * T3.active) / (T9.exchangerate / T8.exchangerate)) rep ,
	((T4.finalcost * T4.active) / (T9.exchangerate / T8.exchangerate)) adj ,
	((T5.finalcost * T5.active) / (T9.exchangerate / T8.exchangerate)) pur ,
	(((T2.finalcost * T2.active) + (T3.finalcost * T3.active) + (T4.finalcost * T4.active) + (T5.finalcost * T5.active)) / (T9.exchangerate / T8.exchangerate)) total
from 
	jobcostingitem T1
	inner join costs_calibration T2 on T1.calcost_id = T2.costid
	inner join costs_repair T3 on T1.repaircost_id = T3.costid
	inner join costs_adjustment T4 on T1.adjustmentcost_id = T4.costid
	inner join costs_purchase T5 on T1.purchasecost_id = T5.costid
	inner join jobcosting T6 on T1.costingid = T6.id
	inner join company T7 on T6.orgid = T7.coid
	inner join supportedcurrency T8 on T8.currencyid = T7.currencyid
	inner join supportedcurrency T9 on T9.currencyid = T6.currencyid
where 
	T1.jobitemid IN
					(SELECT DISTINCT 
						ji.jobitemid
					FROM
						job j
						INNER JOIN jobitem ji ON ji.jobid = j.jobid
						INNER JOIN jobstatus js ON j.statusid = js.statusid
					WHERE
						js.statusid = 1 and
						ji.stateid in (27,31,33,35,50,52,85,167,4049,4061,4063,4070,4160,4144)
					)
) D1
inner join 
(
SELECT
	T1.COSTINGID,
	T1.JOBITEMID
FROM
	@OGMAXJC T1
) D2 on D1.costingid = D2.costingid and D1.jobitemid = D2.JOBITEMID
;
declare @OGAICR table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @OGAICR
select 
	T1.jobitemid,
	((T2.finalcost * T2.active) / (T10.exchangerate / T7.exchangerate)) cal ,
	((T3.finalcost * T3.active) / (T10.exchangerate / T7.exchangerate)) rep ,
	((T4.finalcost * T4.active) / (T10.exchangerate / T7.exchangerate)) adj ,
	((T5.finalcost * T5.active) / (T10.exchangerate / T7.exchangerate)) pur ,
	((case when T2.finalcost is null
		 then 0 
		 else (T2.finalcost * T2.active) 
	  end + 
	  case when T3.finalcost is null 
		then 0 
		else (T3.finalcost * T3.active)
	  end + 
	  case when T4.finalcost is null 
		then 0 
		else (T4.finalcost * T4.active)
	  end + 
	  case when T5.finalcost is null 
		then 0 
		else (T5.finalcost * T5.active)
	   end) / (T10.exchangerate / T7.exchangerate)) total
from 
	jobitem T1 
	inner join job T6 on T1.jobid = T6.jobid 
	inner join supportedcurrency T7 on T7.currencyid = T6.currencyid
	inner join subdiv T8 on T6.orgid = T8.subdivid
	inner join company T9 on T8.coid = T9.coid
	inner join supportedcurrency T10 on T9.currencyid = T10.currencyid
	left join costs_calibration T2 on T1.calcost_id = T2.costid 
	left join costs_repair T3 on T1.repcost_id = T3.costid 
	left join costs_adjustment T4 on T1.adjust_id = T4.costid 
	left join costs_purchase T5 on T1.salescost_id = T5.costid
WHERE    
	T1.jobitemid IN
					(SELECT DISTINCT 
						ji.jobitemid
					FROM 
						job j
						INNER JOIN jobitem ji ON ji.jobid = j.jobid
						INNER JOIN jobstatus js ON j.statusid = js.statusid
					WHERE
						js.statusid = 1 and
						ji.stateid in (27,31,33,35,50,52,85,167,4049,4061,4063,4070,4160,4144)
					)
;
declare @OGINV table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @OGINV
select 
	T1.jobitemid,
	sum(((T2.finalcost * T2.active) / (T9.exchangerate / T8.exchangerate))) cal ,
	sum(((T3.finalcost * T3.active) / (T9.exchangerate / T8.exchangerate))) rep ,
	sum(((T4.finalcost * T4.active) / (T9.exchangerate / T8.exchangerate))) adj ,
	sum(((T5.finalcost * T5.active) / (T9.exchangerate / T8.exchangerate))) pur ,
	sum((((T2.finalcost * T2.active) + (T3.finalcost * T3.active)  + (T4.finalcost * T4.active)  + (T5.finalcost * T5.active)) / (T9.exchangerate / T8.exchangerate))) total
from 
	invoiceitem T1
	inner join costs_calibration T2 on T1.calcost_id = T2.costid
	inner join costs_repair T3 on T1.repaircost_id = T3.costid
	inner join costs_adjustment T4 on T1.adjustmentcost_id = T4.costid
	inner join costs_purchase T5 on T1.purchasecost_id = T5.costid
	inner join invoice T6 on T1.invoiceid = T6.id
	inner join company T7 on T6.orgid = T7.coid
	inner join supportedcurrency T8 on T8.currencyid = T7.currencyid
	inner join supportedcurrency T9 on T9.currencyid = T6.currencyid
where 
	T6.typeid <> 5
	and T1.jobitemid IN
						(SELECT DISTINCT 
							ji.jobitemid
						FROM
							job j
							INNER JOIN jobitem ji ON ji.jobid = j.jobid
							INNER JOIN jobstatus js ON j.statusid = js.statusid
						WHERE
							js.statusid = 1 and
							ji.stateid in (27,31,33,35,50,52,85,167,4049,4061,4063,4070)
						)
group by
	T1.jobitemid
;
declare @OGSERJOBCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @OGSERJOBCOSTS
SELECT 
	D3.jobitemid,
	((D1.sumcost * (D3.jobownbuscomexcahngerate / D3.jobexchangerate)) / D2.items) ser
FROM
	(
	SELECT
		T2.jobid,
		sum(T1.cost) sumcost
	FROM 
		dbo.jobexpenseitem T1
		inner join dbo.job T2 on T1.job = T2.jobid
	group by
		T2.jobid
	) D1
inner join 
	(
	SELECT
		T1.jobid
		,count(T1.jobitemid) items     
	FROM 
		dbo.jobitem T1
		inner join dbo.jobexpenseitem T2 on T1.jobid = T2.job
	GROUP BY
		T1.jobid
	) D2 on D1.jobid = D2.jobid
inner join
	(
	SELECT distinct
		T1.jobid,
		T1.jobitemid,
		T6.exchangerate jobexchangerate,
		T5.exchangerate jobownbuscomexcahngerate
	FROM 
		dbo.jobitem T1
		inner join dbo.job T2 on T1.jobid = T2.jobid
		inner join dbo.subdiv T3 on T2.orgid = T3.subdivid
		inner join dbo.company T4 on T3.coid = T4.coid
		inner join dbo.supportedcurrency T5 on T5.currencyid = T4.currencyid
		inner join dbo.supportedcurrency T6 on T6.currencyid = T2.currencyid
		inner join dbo.jobexpenseitem T7 on T2.jobid = T7.job
	) D3 on D1.jobid = D3.jobid
;
declare @OGSERJCCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @OGSERJCCOSTS
SELECT
	D3.jobitemid,
	((D1.sumfinalcost * (D3.jobownbuscomexcahngerate / D3.jcexchangerate)) / D2.items) ser
FROM
	(
	SELECT 
		T1.jobCosting,
		sum(T1.finalcost) sumfinalcost   
	FROM 
		dbo.jobcostingexpenseitem T1
	GROUP BY
		T1.jobCosting
	) D1
inner join 
	(
	select 
		T1.costingid, 
		COUNT(T1.id) items
	FROM
		dbo.jobcostingitem T1
	GROUP BY
		T1.costingid
	) D2 on D1.jobCosting = D2.costingid
inner join
	(
	select 
		T1.costingid, 
		T1.jobitemid,
		T5.exchangerate jcexchangerate,
		T4.exchangerate jobownbuscomexcahngerate
	FROM
		dbo.jobcostingitem T1
		inner join jobcosting T2 on T1.costingid = T2.id
		inner join company T3 on T2.orgid = T3.coid
		inner join supportedcurrency T4 on T4.currencyid = T3.currencyid
		inner join supportedcurrency T5 on T5.currencyid = T2.currencyid
	) D3 on D1.jobCosting = D3.costingid
inner join 
(
SELECT
	T1.COSTINGID,
	T1.JOBITEMID
FROM
	@OGMAXJC T1
) D4 on D3.costingid = D4.costingid and D3.jobitemid = D4.JOBITEMID
;
declare @OGSERINVCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @OGSERINVCOSTS
SELECT 
	D3.jobitemid,
	SUM((D1.sumfinalcost * (D3.jobownbuscomexcahngerate / D3.invexchangerate)) / D2.items) ser
FROM
	(
	SELECT
		T1.invoiceid,
		  sum(T2.finalcost) sumfinalcost
	FROM 
		dbo.invoiceitem T1
		inner join dbo.costs_service T2 on T1.servicecost = T2.costid
		inner join dbo.jobexpenseitem T3 on T1.expenseitem = T3.id
		inner join dbo.job T4 on T3.job = T4.jobid
		left join dbo.creditnote T5 on T1.invoiceid = T5.invid
	where 
		T2.costtype = 'invoice' 
		and T2.active = 1
		and T5.id is NULL
	group by
		T1.invoiceid
	) D1
inner join 
	(
	SELECT  
		T1.invoiceid,
		count(T1.jobitemid) items 
	FROM 
		dbo.invoiceitem T1
	GROUP BY 
		T1.invoiceid
	) D2 on D1.invoiceid = D2.invoiceid
inner join
	(
	SELECT  
		T1.invoiceid,
		T1.jobitemid,
		T5.exchangerate invexchangerate,
		T4.exchangerate jobownbuscomexcahngerate
	FROM 
		dbo.invoiceitem T1
		inner join invoice T2 on T1.invoiceid = T2.id
		inner join company T3 on T2.orgid = T3.coid
		inner join supportedcurrency T4 on T4.currencyid = T3.currencyid
		inner join supportedcurrency T5 on T5.currencyid = T2.currencyid
	WHERE
		T1.jobitemid is not NULL
	) D3 on D1.invoiceid = D3.invoiceid
GROUP BY
	D3.jobitemid
;
create table ONGOINGNOTCOMPLETE
(orgid INT,
coname VARCHAR(100),
legalidentifier VARCHAR(255),
coid INT,
subname VARCHAR(75),
jobno VARCHAR(30),
lastContractReviewBy VARCHAR(61),
businesssubname VARCHAR(75),
analyticalcenter VARCHAR(255),
jobtype VARCHAR(100),
itemno CHAR(3),
itemstate VARCHAR(200),
instmodeltranslation VARCHAR(1000),
datecomplete DATE,
caldate DATE,
jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
ser DECIMAL(8,2),
total DECIMAL(8,2),
LASTMODIFIED DATETIME
);
insert into ONGOINGNOTCOMPLETE
select 
	T1.orgid,  
	T1.coname,
	T1.legalidentifier,
	T1.coid,
	T1.subname,
	T1.jobno,
	T1.lastContractReviewBy,
	T1.businesssubname,
	T1.analyticalcenter,
	T1.Description,
	T1.itemno,
	T1.itemstate,
	T1.instmodeltranslation,
	T1.datecomplete,
	T1.caldate,
	T1.JOBITEMID,
	ISNULL(T5.cal,ISNULL(T4.cal,T3.cal)) * 0.8 cal ,
	ISNULL(T5.rep,ISNULL(T4.rep,T3.rep)) * 0.8 rep ,
	ISNULL(T5.adj,ISNULL(T4.adj,T3.adj)) * 0.8 adj ,
	ISNULL(T5.pur,ISNULL(T4.pur,T3.pur)) * 0.8 pur ,
	ISNULL(ISNULL(T8.ser,ISNULL(T7.ser,T6.ser)),0) * 0.8 ser ,
	(ISNULL(T5.total,ISNULL(T4.total,T3.total)) + ISNULL(ISNULL(T8.ser,ISNULL(T7.ser,T6.ser)),0)) * 0.8 total ,
	CURRENT_TIMESTAMP
from 
	@OGAIJI T1 
	LEFT JOIN @OGAICR T3 on T1.jobitemid = T3.jobitemid 
	LEFT JOIN @OGAIJC T4 on T1.jobitemid = T4.jobitemid 
	LEFT JOIN @OGINV T5 on T1.jobitemid = T5.jobitemid
	LEFT JOIN @OGSERJOBCOSTS T6 on T1.jobitemid = T6.jobitemid
	LEFT JOIN @OGSERJCCOSTS T7 on T1.jobitemid = T7.jobitemid
	LEFT JOIN @OGSERINVCOSTS T8 on T1.jobitemid = T8.jobitemid
;

ROLLBACK TRAN