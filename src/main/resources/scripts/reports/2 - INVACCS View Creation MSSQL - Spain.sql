/**** INVACCS TABLE ****/

CREATE VIEW INVACCS AS
SELECT   i.id   ,
         i.invno,
         dt.code   ,
         dt.title, 
         case when cur.currencycode = 'GBP' then SUM(cost) else null end as cost ,
         case when cur.currencycode <> 'GBP' then SUM(cost) else null end as altcost ,
         co.coid                                            ,
         i.regdate                                          ,
         CONVERT(VARCHAR(10), GETDATE(), 104)  as currentdate,
         co.vatcode                                         ,
         (a.addr1 + CHAR(13) + a.addr2 + CHAR(13) + a.addr3) as addr,
         a.town                                       AS town     ,
         a.county                                     AS county   ,
         a.postcode                                   AS postcode ,
         a.country                                    AS country  ,
         i.rate                                                   ,
         cur.accountscode as currencycode,
         i.accountstatus, i.period, 
         -- below is no longer a view - too slow
         (select case when
            (select count(distinct jobno) from invoicejoblink jl where jl.invoiceid = i.id) > 1
          then ''
          else
            (select distinct jobno from invoicejoblink where invoiceid = i.id)
          end ) as linedetail, 
         i.invoicedate as invoicedate   ,
		 dt.orgid      
FROM     invoice i             ,
		 invoicetype t		   ,
         INVACCS_COSTSSUMMARY dt               ,
         subdiv s              ,
         company co            ,
         address a             ,
         supportedcurrency cur ,
         companyaccount ac
WHERE    i.id             = dt.invid
	 AND a.subdivid       = s.subdivid
     AND s.coid           = co.coid
     AND a.addrid         = i.addressid
     AND cur.currencyid   = i.currencyid
     AND co.accountid     = ac.id
	 AND i.typeid		  = t.id
     AND t.addtoaccount	  = 1
GROUP BY i.id,
         code, 
		 i.invno, 
		 dt.title, 
		 co.coid, 
		 i.regdate, 
		 co.vatcode, 
		 a.addr1, 
		 a.addr2, 
		 a.addr3, 
		 a.town,
		 a.county, 
		 a.postcode, 
		 a.country, 
		 i.rate, 
		 i.accountstatus,
		 cur.currencycode,
		 cur.accountscode, 
		 i.period,
		 cur.currencysymbol, 
		 i.invoicedate,
		 dt.orgid;