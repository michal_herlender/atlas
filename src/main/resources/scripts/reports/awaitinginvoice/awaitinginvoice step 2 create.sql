USE [spain]

BEGIN TRAN

declare @MAXJC table
(JOBITEMID INT NOT NULL PRIMARY KEY 
,COSTINGID INT NOT NULL
);
INSERT INTO @MAXJC
select 
	D1.jobitemid, 
	D2.costingid
from
	(
	SELECT  
		a1.jobitemid,
	    MAX(a2.regdate) maxregdate
	FROM     
		jobcosting a2
	    INNER JOIN jobcostingitem a1 ON a1.costingid = a2.id
	WHERE    
		a1.jobitemid IN
		(select distinct
			T5.jobitemid
		from 
			jobitem T5 
			left join jobitemnotinvoiced T14 on T5.jobitemid = T14.jobitemid
			left join invoiceitem T15 on T5.jobitemid = T15.jobitemid
			left join invoice T16 on T15.invoiceid = T16.id
			inner join itemstate T13 on T5.stateid = T13.stateid
		where 
			T13.active = 0
			and (T14.jobitemid is null or T14.jobitemid in (
															SELECT 
																jini.jobitemid
															FROM 
																dbo.jobitemnotinvoiced jini
																inner join dbo.invoice i on jini.periodicinvoiceid = i.id
															WHERE
																i.statusid in (2,4)
															)
			)
			and (T15.jobitemid is null or T15.jobitemid in (
															select distinct 
																D1.jobitemid 
															from 
																(
																SELECT 
																	ii.jobitemid, 
																	max(cast(it.addtoaccount as tinyint)) maxaddtoaccount  
																FROM 
																	dbo.invoiceitem ii, 
																	dbo.invoice i, 
																	dbo.invoicetype it 
																where 
																	ii.invoiceid = i.id 
																	and i.typeid = it.id 
																group by 
																	ii.jobitemid
																) D1 
															where 
																D1.maxaddtoaccount = 0
															)
										or T15.jobitemid in (
															SELECT 
																ii.jobitemid	
															FROM 
																dbo.invoiceitem ii, 
																dbo.invoice i
															where 
																ii.invoiceid = i.id 
																and i.statusid in (2,4)
															)
				)
		)
GROUP BY 
	jobitemid
) D1
INNER JOIN
(
SELECT distinct 
	a1.jobitemid,
    a2.regdate, 
	a2.id costingid
FROM     
	jobcosting a2
    INNER JOIN jobcostingitem a1 ON a1.costingid = a2.id
WHERE    
	a1.jobitemid IN
    (select distinct
		T5.jobitemid
	from 
		jobitem T5 
		left join jobitemnotinvoiced T14 on T5.jobitemid = T14.jobitemid
		left join invoiceitem T15 on T5.jobitemid = T15.jobitemid
		left join invoice T16 on T15.invoiceid = T16.id
		inner join itemstate T13 on T5.stateid = T13.stateid
	where 
		T13.active = 0
		and (T14.jobitemid is null or T14.jobitemid in (
														SELECT 
															jini.jobitemid
														FROM 
															dbo.jobitemnotinvoiced jini
															inner join dbo.invoice i on jini.periodicinvoiceid = i.id
														WHERE
															i.statusid in (2,4)
														)
			)
		and (T15.jobitemid is null or T15.jobitemid in (
														select distinct 
															jobitemid
														from 
															(SELECT 
																jobitemid, 
																max(cast(addtoaccount as tinyint)) maxaddtoaccount 
															FROM 
																dbo.invoiceitem ii, 
																dbo.invoice i, 
																dbo.invoicetype it 
															where 
																ii.invoiceid = i.id 
																and i.typeid = it.id 
															group by 
																jobitemid
															) D1 
														where 
															D1.maxaddtoaccount = 0)
														)
    )
) D2
ON D1.jobitemid = D2.jobitemid and D1.maxregdate = D2.regdate
;
declare @AIJI table
(jobid INT,
datecomplete DATE,
estcarout DECIMAL(8,2),
jobno VARCHAR(30),
clientsubname VARCHAR(75),
clientcontact VARCHAR(61),
lastContractReviewBy VARCHAR(61),
businesssubname VARCHAR(75),
analyticalcenter VARCHAR(255),
rate DECIMAL(8,2),
regdate DATE,
name VARCHAR(100),
description VARCHAR(50),
coname VARCHAR(100),
legalidentifier VARCHAR(255),
coid INT,
subdivid INT,
subname VARCHAR(75),
jobitemid INT NOT NULL PRIMARY KEY,
jobitemdatecomplete DATE,
itemno CHAR(3),
stateid INT,
itemstate VARCHAR(200),
instmodeltranslation VARCHAR(1000),
serialno VARCHAR(50),
plantno VARCHAR(50),
ponos VARCHAR(255),
proforma BIT,
deliveryno VARCHAR(30),
deliverydate DATE,
caldate DATE,
orgid INT
);
insert into @AIJI
select
	D2.jobid,
	D2.datecomplete,
	D2.estcarout,
	D2.jobno,
	D2.clientsubname,
	D2.clientcontact,
	D2.lastContractReviewBy,
	D2.businesssubname,
	D2.analyticalcenter,
	D2.rate,
	D2.regdate,
	D2.name,
	D2.Description,
	D2.coname,
	D2.legalidentifier,
	D2.coid,
	D2.subdivid,
	D2.subname,
	D2.jobitemid,
	D2.jobitemdatecomplete,
	D2.itemno,
	D2.stateid,
	D2.itemstate,
	D2.instmodeltranslation,
	D2.serialno,
	D2.plantno,
	ISNULL(D5.ponos,'') ponos,
	CASE WHEN D6.jobitemid is NULL THEN 0 ELSE 1 END proforma,
	D4.deliveryno,
	D4.deliverydate,
	D3.caldate,
	D2.ORGID
from
(
select distinct
	T1.jobid,
	T1.datecomplete, 
	T1.estcarout, 
	T1.jobno,
	T12.subname clientsubname,
	CONCAT(T11.firstname,' ',T11.lastname) clientcontact,
	CONCAT(T27.firstname,' ',T27.lastname) lastContractReviewBy,	
	T6.subname businesssubname, 
	T6.analyticalcenter, 
	T1.rate, 
	T1.regdate, 
	T2.name,
	case T24.locale
		when 'es_ES' then
			case T1.typeid 
				when 0 then 'Undefined'
				when 1 then 'Trabajo sobre equipos'
				when 2 then 'Trabajo precio único'
				when 3 then 'Valve Job'
				when 4 then 'Trabajo in situ'
			end
		when 'fr_FR' then
			case T1.typeid 
				when 0 then 'Undefined'
				when 1 then 'Job standard'
				when 2 then 'Job prix unique'
				when 3 then 'Valve Job'
				when 4 then 'Job sur site'
			end
		when 'en_GB' then
			case T1.typeid 
				when 0 then 'Undefined'
				when 1 then 'Standard Job'
				when 2 then 'Single Price Job'
				when 3 then 'Valve Job'
				when 4 then 'Onsite Job'
			end
	end Description,
	T4.coname,
	T4.legalidentifier,
	T4.coid ,
	T6.subdivid,
	T6.subname,
	T5.jobitemid,
	T5.datecomplete jobitemdatecomplete,
	T5.itemno,
	T5.stateid,
	T25.translation itemstate,
	T26.translation instmodeltranslation,
	T17.serialno,
	T17.plantno,
	T6.coid ORGID
from 
	job T1
	inner join jobstatus T2 on T1.statusid = T2.statusid
	inner join contact T11 on T1.personid = T11.personid 
	inner join subdiv T12 on T11.subdivid = T12.subdivid 
	inner join company T4 on T12.coid = T4.COID 
	inner join subdiv T6 on T1.orgid = T6.subdivid
	inner join subdiv T21 on T1.orgid = T21.subdivid
	inner join company T23 on T21.coid = T23.coid
	inner join contact T24 on T23.defaultbusinesscontact = T24.personid
	inner join jobitem T5 on T1.jobid = T5.jobid 
	inner join itemstate T13 on T5.stateid = T13.stateid
	left join jobitemnotinvoiced T14 on T5.jobitemid = T14.jobitemid
	left join invoiceitem T15 on T5.jobitemid = T15.jobitemid
	inner join instrument T17 on T5.plantid = T17.plantid
	inner join instmodeltranslation T26 on T17.modelid = T26.modelid
	inner join itemstatetranslation T25 on T13.stateid = T25.stateid
	left join contact T27 on T5.lastContractReviewBy_personid = T27.personid 
where 
	T25.locale = T24.locale
	and T26.locale = T24.locale
	and T13.active = 0
	and (T14.jobitemid is null or T14.jobitemid in (
													SELECT 
														jini.jobitemid
													FROM 
														dbo.jobitemnotinvoiced jini
														inner join dbo.invoice i on jini.periodicinvoiceid = i.id
													WHERE
														i.statusid in (2,4)
													)
		)
	and (T15.jobitemid is null or T15.jobitemid in (
													select distinct 
														D1.jobitemid 
													from 
														(
														SELECT 
															ii.jobitemid, 
															max(cast(it.addtoaccount as tinyint)) maxaddtoaccount 
														FROM 
															dbo.invoiceitem ii, 
															dbo.invoice i, 
															dbo.invoicetype it 
														where 
															ii.invoiceid = i.id 
															and i.typeid = it.id 
														group by 
															ii.jobitemid
														) D1 
													where 
														D1.maxaddtoaccount = 0
													)
								or T15.jobitemid in (
												SELECT 
													ii.jobitemid	
												FROM 
													dbo.invoiceitem ii, 
													dbo.invoice i
												where 
													ii.invoiceid = i.id 
													and i.statusid in (2,4)
												)
		)
) as D2 left join
(
-- Get Cal Date
select 
	T1.jobitemid,
	max(T2.caldate) caldate
from 
	dbo.certlink T1 
	inner join dbo.certificate T2 on T1.certid = T2.certid
	inner join dbo.basestatus T3 on T2.status = T3.statusid
where
	T3.type = 'certificate'
	and T3.issued = 1
group by 
	T1.jobitemid
) as D3 on D2.jobitemid = D3.jobitemid
left join
(
-- Get Delivery date and number
select 
	T1.jobitemid,
	max(T2.deliveryno) deliveryno,
	max(T2.deliverydate) deliverydate
from 
	dbo.deliveryitem T1 
	inner join dbo.delivery T2 on T1.deliveryid = T2.deliveryid
where 
	T2.deliverytype = 'client'
group by 
	T1.jobitemid
) as D4 on D2.jobitemid = D4.jobitemid
left join
(
-- Get client po and bpo
SELECT 
	T1.jobitemid,
	STUFF((SELECT '; ' + T2.ponos
			FROM
			(SELECT distinct
				T1.jobitemid
				,CONCAT(CASE WHEN T2.ponumber IS NOT NULL THEN CONCAT(T2.ponumber,' [PO]') ELSE '' END,CASE WHEN T3.ponumber IS NOT NULL THEN CONCAT(T3.ponumber,' [BPO]') ELSE '' END) ponos
			FROM 
				dbo.jobitempo T1
				left join dbo.po T2 on T1.poid = T2.poid
				left join dbo.bpo T3 on T1.bpoid = T3.poid
			) T2
			WHERE 
				T1.jobitemid = T2.jobitemid
			ORDER BY
				T2.ponos
			FOR XML PATH('')), 1, 1, '') ponos
FROM
	dbo.jobitempo T1
	left join dbo.po T2 on T1.poid = T2.poid
	left join dbo.bpo T3 on T1.bpoid = T3.poid
GROUP BY
	T1.jobitemid
) as D5 on D2.jobitemid = D5.jobitemid
left join
(
-- Pro-forma invoice items
select
	jobitemid
from
	dbo.jobitem 
where
	jobitemid in (
				select distinct 
					D1.jobitemid 
				from 
					(
					SELECT 
						ii.jobitemid, 
						max(cast(it.addtoaccount as tinyint)) maxaddtoaccount  
					FROM 
						dbo.invoiceitem ii, 
						dbo.invoice i, 
						dbo.invoicetype it 
					where 
						ii.invoiceid = i.id 
						and i.typeid = it.id 
					group by 
						ii.jobitemid
					) D1 
				where 
					D1.maxaddtoaccount = 0
				)
) as D6 on D2.jobitemid = D6.jobitemid
;
declare @AIJC table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @AIJC
select
	D1.jobitemid,
	D1.cal,
	D1.rep,
	D1.adj,
	D1.pur,
	D1.total
from
(
select 
	T1.jobitemid,
	T1.costingid, 
	(T2.finalcost * T2.active / (T9.exchangerate / T8.exchangerate)) cal,
	(T3.finalcost * T3.active / (T9.exchangerate / T8.exchangerate)) rep,
	(T4.finalcost * T4.active / (T9.exchangerate / T8.exchangerate)) adj,
	(T5.finalcost * T5.active / (T9.exchangerate / T8.exchangerate)) pur,
	(T1.finalcost / (T9.exchangerate / T8.exchangerate)) total
from 
	jobcostingitem T1
	inner join costs_calibration T2 on T1.calcost_id = T2.costid
	inner join costs_repair T3 on T1.repaircost_id = T3.costid
	inner join costs_adjustment T4 on T1.adjustmentcost_id = T4.costid
	inner join costs_purchase T5 on T1.purchasecost_id = T5.costid
	inner join jobcosting T6 on T1.costingid = T6.id
	inner join company T7 on T6.orgid = T7.coid
	inner join supportedcurrency T8 on T8.currencyid = T7.currencyid
	inner join supportedcurrency T9 on T9.currencyid = T6.currencyid
where  
	T1.jobitemid IN
		(select distinct
			T5.jobitemid
		from 
			jobitem T5 
			left join jobitemnotinvoiced T14 on T5.jobitemid = T14.jobitemid
			left join invoiceitem T15 on T5.jobitemid = T15.jobitemid
			left join invoice T16 on T15.invoiceid = T16.id
			inner join itemstate T13 on T5.stateid = T13.stateid
		where 
			T13.active = 0
			and (T14.jobitemid is null or T14.jobitemid in (
															SELECT 
																jini.jobitemid
															FROM 
																dbo.jobitemnotinvoiced jini
																inner join dbo.invoice i on jini.periodicinvoiceid = i.id
															WHERE
																i.statusid in (2,4)
															)
			)
			and (T15.jobitemid is null or T15.jobitemid in (
															select distinct 
																D1.jobitemid 
															from 
																(
																SELECT 
																	ii.jobitemid, 
																	max(cast(it.addtoaccount as tinyint)) maxaddtoaccount
																FROM 
																	dbo.invoiceitem ii, 
																	dbo.invoice i, 
																	dbo.invoicetype it 
																where 
																	ii.invoiceid = i.id 
																	and i.typeid = it.id 
																group by 
																	ii.jobitemid
																) D1 
															where 
																D1.maxaddtoaccount = 0
															)
										or T15.jobitemid in (
														SELECT 
															ii.jobitemid	
														FROM 
															dbo.invoiceitem ii, 
															dbo.invoice i
														where 
															ii.invoiceid = i.id 
															and i.statusid in (2,4)
														)
				)
		)
) as D1
inner join 
(
SELECT
	T1.COSTINGID,
	T1.JOBITEMID
FROM
	@MAXJC T1
) D2 on D1.costingid = D2.costingid and D1.jobitemid = D2.JOBITEMID
;
declare @AICR table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @AICR
select 
	T1.jobitemid,
	(T2.finalcost * T2.active / (T10.exchangerate / T7.exchangerate)) cal,
	(T3.finalcost * T3.active / (T10.exchangerate / T7.exchangerate)) rep,
	(T4.finalcost * T4.active / (T10.exchangerate / T7.exchangerate)) adj,
	(T5.finalcost * T5.active / (T10.exchangerate / T7.exchangerate)) pur,
	((case when T2.finalcost is null 
		then 0 
		else T2.finalcost * T2.active 
	 end + 
	 case when T3.finalcost is null 
		then 0 
		else T3.finalcost * T3.active 
	 end + 
	 case when T4.finalcost is null 
		then 0 
		else T4.finalcost * T4.active 
	 end + 
	 case when T5.finalcost is null 
		then 0 
		else T5.finalcost * T5.active 
		end) / (T10.exchangerate / T7.exchangerate)) total
FROM 
	jobitem T1 
	inner join job T6 on T1.jobid = T6.jobid 
	inner join supportedcurrency T7 on T7.currencyid = T6.currencyid
	inner join subdiv T8 on T6.orgid = T8.subdivid
	inner join company T9 on T8.coid = T9.coid
	inner join supportedcurrency T10 on T9.currencyid = T10.currencyid
	left join costs_calibration T2 on T1.calcost_id = T2.costid 
	left join costs_repair T3 on T1.repcost_id = T3.costid 
	left join costs_adjustment T4 on T1.adjust_id = T4.costid 
	left join costs_purchase T5 on T1.salescost_id = T5.costid
WHERE 
	T1.jobitemid IN
    (select distinct
		T5.jobitemid
		from 
			jobitem T5 
			left join jobitemnotinvoiced T14 on T5.jobitemid = T14.jobitemid
			left join invoiceitem T15 on T5.jobitemid = T15.jobitemid
			left join invoice T16 on T15.invoiceid = T16.id
			inner join itemstate T13 on T5.stateid = T13.stateid
		where 
			T13.active = 0
			and (T14.jobitemid is null or T14.jobitemid in (
															SELECT 
																jini.jobitemid
															FROM 
																dbo.jobitemnotinvoiced jini
																inner join dbo.invoice i on jini.periodicinvoiceid = i.id
															WHERE
																i.statusid in (2,4)
															)
			)
			and (T15.jobitemid is null or T15.jobitemid in (
															select distinct 
																jobitemid 
															from 
																(
																SELECT 
																	jobitemid, 
																	max(cast(addtoaccount as tinyint)) maxaddtoaccount 
																FROM 
																	dbo.invoiceitem ii, 
																	dbo.invoice i, 
																	dbo.invoicetype it 
																where 
																	ii.invoiceid = i.id 
																	and i.typeid = it.id 
																group by 
																jobitemid
																) D1 
															where D1.maxaddtoaccount = 0
															)
										or T15.jobitemid in (
															SELECT 
																ii.jobitemid	
															FROM 
																dbo.invoiceitem ii, 
																dbo.invoice i
															where 
																ii.invoiceid = i.id 
																and i.statusid in (2,4)
															)
				)											
    )
;
declare @AIINV table
(jobitemid INT NOT NULL PRIMARY KEY,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @AIINV
SELECT 
	T1.jobitemid,
	sum(((T2.finalcost * T2.active) / (T9.exchangerate / T8.exchangerate))) cal,
	sum(((T3.finalcost * T3.active) / (T9.exchangerate / T8.exchangerate))) rep,
	sum(((T4.finalcost * T4.active) / (T9.exchangerate / T8.exchangerate))) adj,
	sum(((T5.finalcost * T5.active) / (T9.exchangerate / T8.exchangerate))) pur,
	sum(Case when (((T2.finalcost * T2.active) + (T3.finalcost * T3.active)  + (T4.finalcost * T4.active)  + (T5.finalcost * T5.active)) / (T9.exchangerate / T8.exchangerate)) = 0 then T1.finalcost / (T9.exchangerate / T8.exchangerate) else (((T2.finalcost * T2.active) + (T3.finalcost * T3.active)  + (T4.finalcost * T4.active)  + (T5.finalcost * T5.active)) / (T9.exchangerate / T8.exchangerate)) end) total
FROM 
	invoiceitem T1
	inner join costs_calibration T2 on T1.calcost_id = T2.costid
	inner join costs_repair T3 on T1.repaircost_id = T3.costid
	inner join costs_adjustment T4 on T1.adjustmentcost_id = T4.costid
	inner join costs_purchase T5 on T1.purchasecost_id = T5.costid
	inner join invoice T6 on T1.invoiceid = T6.id
	inner join company T7 on T6.orgid = T7.coid
	inner join supportedcurrency T8 on T8.currencyid = T7.currencyid
	inner join supportedcurrency T9 on T9.currencyid = T6.currencyid
WHERE 
	T6.typeid <> 5
	and T1.jobitemid IN
	(select distinct
			T5.jobitemid
			from 
				jobitem T5
				inner join itemstate T13 on T5.stateid = T13.stateid
				left join jobitemnotinvoiced T14 on T5.jobitemid = T14.jobitemid
				left join invoiceitem T15 on T5.jobitemid = T15.jobitemid
				left join invoice T16 on T15.invoiceid = T16.id
			where 
				T13.active = 0
				and (T14.jobitemid is null or T14.jobitemid in (
																SELECT 
																	jini.jobitemid
																FROM 
																	dbo.jobitemnotinvoiced jini
																	inner join dbo.invoice i on jini.periodicinvoiceid = i.id
																WHERE
																	i.statusid in (2,4)
																)
				)
				and (T15.jobitemid is null or T15.jobitemid in (
																select distinct 
																	D1.jobitemid 
																from 
																	(
																	SELECT 
																		ii.jobitemid, 
																		max(cast(it.addtoaccount as tinyint)) maxaddtoaccount
																	FROM 
																		dbo.invoiceitem ii, 
																		dbo.invoice i, 
																		dbo.invoicetype it 
																	where 
																		ii.invoiceid = i.id 
																		and i.typeid = it.id 
																	group by 
																		ii.jobitemid
																	) D1 
																where 
																	D1.maxaddtoaccount = 0
																)
											or T15.jobitemid in (
																SELECT 
																	ii.jobitemid	
																FROM 
																	dbo.invoiceitem ii, 
																	dbo.invoice i
																where 
																	ii.invoiceid = i.id 
																	and i.statusid in (2,4)
																)
					)
	)
GROUP BY
	T1.jobitemid
;
/*
declare @SERJOBCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @SERJOBCOSTS
SELECT 
	D3.jobitemid,
	((D1.sumcost * (D3.jobownbuscomexcahngerate / D3.jobexchangerate)) / D2.items) ser
FROM
	(
	SELECT
		T2.jobid,
		sum(T1.cost) sumcost
	FROM 
		dbo.jobexpenseitem T1
		inner join dbo.job T2 on T1.job = T2.jobid
	group by
		T2.jobid
	) D1
inner join 
	(
	SELECT
		T1.jobid
		,count(distinct T1.jobitemid) items     
	FROM 
		dbo.jobitem T1
		inner join dbo.jobexpenseitem T2 on T1.jobid = T2.job
	GROUP BY
		T1.jobid
	) D2 on D1.jobid = D2.jobid
inner join
	(
	SELECT distinct
		T1.jobid,
		T1.jobitemid,
		T6.exchangerate jobexchangerate,
		T5.exchangerate jobownbuscomexcahngerate
	FROM 
		dbo.jobitem T1
		inner join dbo.job T2 on T1.jobid = T2.jobid
		inner join dbo.subdiv T3 on T2.orgid = T3.subdivid
		inner join dbo.company T4 on T3.coid = T4.coid
		inner join dbo.supportedcurrency T5 on T5.currencyid = T4.currencyid
		inner join dbo.supportedcurrency T6 on T6.currencyid = T2.currencyid
		inner join dbo.jobexpenseitem T7 on T2.jobid = T7.job
	) D3 on D1.jobid = D3.jobid
;
declare @SERJCCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @SERJCCOSTS
SELECT
	D3.jobitemid,
	((D1.sumfinalcost * (D3.jobownbuscomexcahngerate / D3.jcexchangerate)) / D2.items) ser
FROM
	(
	SELECT 
		T1.jobCosting,
		sum(T1.finalcost) sumfinalcost   
	FROM 
		dbo.jobcostingexpenseitem T1
	GROUP BY
		T1.jobCosting
	) D1
inner join 
	(
	select 
		T1.costingid, 
		COUNT(T1.id) items
	FROM
		dbo.jobcostingitem T1
	GROUP BY
		T1.costingid
	) D2 on D1.jobCosting = D2.costingid
inner join
	(
	select distinct
		T1.costingid, 
		T1.jobitemid,
		T5.exchangerate jcexchangerate,
		T4.exchangerate jobownbuscomexcahngerate
	FROM
		dbo.jobcostingitem T1
		inner join dbo.jobcosting T2 on T1.costingid = T2.id
		inner join dbo.company T3 on T2.orgid = T3.coid
		inner join dbo.supportedcurrency T4 on T4.currencyid = T3.currencyid
		inner join dbo.supportedcurrency T5 on T5.currencyid = T2.currencyid
		inner join dbo.jobexpenseitem T6 on T2.jobid = T6.job
	) D3 on D1.jobCosting = D3.costingid
inner join 
(
SELECT
	T1.COSTINGID,
	T1.JOBITEMID
FROM
	@MAXJC T1
) D4 on D3.costingid = D4.costingid and D3.jobitemid = D4.JOBITEMID
;
declare @SERINVCOSTS table
(jobitemid INT NOT NULL PRIMARY KEY,
ser DECIMAL(8,2)
);
insert into @SERINVCOSTS
SELECT 
	D3.jobitemid,
	SUM((D1.sumfinalcost * (D3.jobownbuscomexcahngerate / D3.invexchangerate)) / D2.items) ser
FROM
	(
	SELECT
		T1.invoiceid,
		sum(T2.finalcost) sumfinalcost
	FROM 
		dbo.invoiceitem T1
		inner join dbo.costs_service T2 on T1.servicecost = T2.costid
		inner join dbo.jobexpenseitem T3 on T1.expenseitem = T3.id
		inner join dbo.job T4 on T3.job = T4.jobid
		left join dbo.creditnote T5 on T1.invoiceid = T5.invid
	where 
		T2.costtype = 'invoice' 
		and T2.active = 1
		and T5.id is NULL
	group by
		T1.invoiceid
	) D1
inner join 
	(
	SELECT  
		T1.invoiceid,
		count(T1.jobitemid) items 
	FROM 
		dbo.invoiceitem T1
	GROUP BY 
		T1.invoiceid
	) D2 on D1.invoiceid = D2.invoiceid
inner join
	(
	SELECT distinct
		T1.invoiceid,
		T1.jobitemid,
		T5.exchangerate invexchangerate,
		T4.exchangerate jobownbuscomexcahngerate
	FROM 
		dbo.invoiceitem T1
		inner join dbo.invoice T2 on T1.invoiceid = T2.id
		inner join dbo.company T3 on T2.orgid = T3.coid
		inner join dbo.supportedcurrency T4 on T4.currencyid = T3.currencyid
		inner join dbo.supportedcurrency T5 on T5.currencyid = T2.currencyid
		inner join dbo.jobitem T6 on T1.jobitemid = T6.jobitemid
		inner join dbo.job T7 on T6.jobid = T7.jobid
		inner join dbo.jobexpenseitem T8 on T7.jobid = T8.job
	) D3 on D1.invoiceid = D3.invoiceid
GROUP BY
	D3.jobitemid
;
*/
declare @AIJS table
(jobid INT,
datecomplete DATE,
estcarout DECIMAL(8,2),
jobno VARCHAR(30),
clientsubname VARCHAR(75),
clientcontact VARCHAR(61),
lastContractReviewBy VARCHAR(61),
businesssubname VARCHAR(75),
analyticalcenter VARCHAR(255),
rate DECIMAL(8,2),
regdate DATE,
name VARCHAR(100),
description VARCHAR(50),
coname VARCHAR(100),
legalidentifier VARCHAR(255),
coid INT,
subdivid INT,
subname VARCHAR(75),
jobitemid INT,
expenseitemid INT NOT NULL PRIMARY KEY,
jobitemdatecomplete DATE,
itemno CHAR(3),
stateid INT,
itemstate VARCHAR(200),
instmodeltranslation VARCHAR(1000),
serialno VARCHAR(50),
plantno VARCHAR(50),
ponos VARCHAR(255),
proforma BIT,
deliveryno VARCHAR(30),
deliverydate DATE,
caldate DATE,
orgid INT,
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
ser DECIMAL(8,2),
total DECIMAL(8,2)
);
insert into @AIJS
SELECT
	T1.job jobid,
	T2.datecomplete, 
	T2.estcarout, 
	T2.jobno,
	T12.subname clientsubname,
	CONCAT(T11.firstname,' ',T11.lastname) clientcontact,
	'' lastContractReviewBy,
	T6.subname businesssubname, 
	T6.analyticalcenter, 
	T2.rate, 
	T2.regdate,
	T7.name,
	case T24.locale
		when 'es_ES' then
			case T2.typeid 
				when 0 then 'Undefined'
				when 1 then 'Trabajo sobre equipos'
				when 2 then 'Trabajo precio único'
				when 3 then 'Valve Job'
				when 4 then 'Trabajo in situ'
			end
		when 'fr_FR' then
			case T2.typeid 
				when 0 then 'Undefined'
				when 1 then 'Job standard'
				when 2 then 'Job prix unique'
				when 3 then 'Valve Job'
				when 4 then 'Job sur site'
			end
		when 'en_GB' then
			case T2.typeid 
				when 0 then 'Undefined'
				when 1 then 'Standard Job'
				when 2 then 'Single Price Job'
				when 3 then 'Valve Job'
				when 4 then 'Onsite Job'
		end
	end Description,
	T8.coname,
	T8.legalidentifier,
	T8.coid,
	T6.subdivid,
	T6.subname,
	-T1.id jobitemid,
	T1.id expenseitemid,
	NULL jobitemdatecomplete,
	T1.itemNo,
	NULL stateid,
	'' itemstate,
	T26.translation instmodeltranslation,
	'' serialno,
	'' plantno,
	'' ponos,
	0 proforma,
	'' deliveryno,
	NULL deliverydate,
	T1.date caldate,
	T6.coid ORGID,
	0 cal,
	0 rep,
	0 adj,
	0 pur,
	T1.cost,
	T1.cost
FROM 
	dbo.jobexpenseitem T1
	inner join dbo.job T2 on T1.job = T2.jobid
	left join jobexpenseitemnotinvoiced T3 on T1.id = T3.expenseitemid
	left join invoiceitem T4 on T1.id = T4.expenseitem and T4.id = (select min(id) from invoiceitem where invoiceitem.expenseitem=T1.id group by invoiceitem.expenseitem)
	left join dbo.creditnote T5 on T4.invoiceid = T5.invid
	inner join subdiv T6 on T2.orgid = T6.subdivid
	inner join jobstatus T7 on T2.statusid = T7.statusid
	inner join contact T11 on T2.personid = T11.personid 
	inner join subdiv T12 on T11.subdivid = T12.subdivid 
	inner join company T8 on T12.coid = T8.COID
	inner join subdiv T20 on T2.orgid = T20.subdivid
	inner join subdiv T21 on T2.orgid = T21.subdivid
	inner join company T23 on T21.coid = T23.coid
	inner join contact T24 on T23.defaultbusinesscontact = T24.personid
	inner join instmodeltranslation T26 on T1.model = T26.modelid
/*
	inner join
	(
	SELECT
		S1.jobid
	FROM
		(
		SELECT
			T1.jobid,
			COUNT(T2.jobitemid) JobItems
		FROM 
			dbo.job T1
			left join dbo.jobitem T2 on T1.jobid = T2.jobid
			left join dbo.jobexpenseitem T3 on T1.jobid = T3.job
		WHERE
			T1.statusid <> 3
			and T3.job is not NULL
		GROUP BY
			T1.jobid
		) as S1
	WHERE
		S1.JobItems = 0
	) T9 on T1.job = T9.jobid
*/
WHERE
	T1.invoiceable = 1
	and T26.locale = T24.locale
	and (T3.expenseitemid is NULL or T3.expenseitemid in (
														SELECT 
															jeini.expenseitemid
														FROM 
															dbo.jobexpenseitemnotinvoiced jeini
															inner join dbo.invoice i on jeini.periodicinvoiceid = i.id
														WHERE
															i.statusid in (2,4)					
														)
		)
	and T5.id is NULL
	and (T4.id is NULL or T4.id in (
									SELECT 
										ii.id	
									FROM 
										dbo.invoiceitem ii, 
										dbo.invoice i
									where 
										ii.invoiceid = i.id 
										and i.statusid in (2,4)
									)
		)
;
create table AWAITINGINVOICE
(orgid INT,
coname VARCHAR(100),
legalidentifier VARCHAR(255),
coid INT,
subdivid INT,
subname VARCHAR(75),
jobno VARCHAR(30),
clientsubname VARCHAR(75),
clientcontact VARCHAR(61),
lastContractReviewBy VARCHAR(61),
jobid INT,
businesssubname VARCHAR(75),
analyticalcenter VARCHAR(255),
jobtype VARCHAR(100),
itemno CHAR(3),
stateid INT,
itemstate VARCHAR(200),
instmodeltranslation VARCHAR(1000),
serialno VARCHAR(50),
plantno VARCHAR(50),
ponos VARCHAR(255),
proforma BIT,
datecomplete DATE,
deliveryno VARCHAR(30),
deliverydate DATE,
caldate DATE,
jobitemid INT NOT NULL PRIMARY KEY,
jobitemdatecomplete DATE,
source CHAR(3),
cal DECIMAL(8,2),
rep DECIMAL(8,2),
adj DECIMAL(8,2),
pur DECIMAL(8,2),
ser DECIMAL(8,2),
total DECIMAL(8,2),
LASTMODIFIED DATETIME
);
insert into AWAITINGINVOICE
select 
	T1.orgid ,  
	T1.coname  ,
	T1.legalidentifier,
	T1.coid ,
	T1.subdivid ,
	T1.subname,
	T1.jobno,
	T1.clientsubname,
	T1.clientcontact,
	T1.lastContractReviewBy,
	T1.jobid,
	T1.businesssubname,
	T1.analyticalcenter,
	T1.Description,
	T1.itemno,
	T1.stateid,
	T1.itemstate,
	T1.instmodeltranslation,
	T1.serialno,
	T1.plantno,
	T1.ponos,
	T1.proforma,
	T1.datecomplete,
	T1.deliveryno,
	T1.deliverydate,
	T1.caldate,
	T1.jobitemid,
	T1.jobitemdatecomplete,
	case when T5.total is NULL then case when T4.total is NULL then 'CR' else 'JP' end else 'IN' end source, 
	ISNULL(T5.cal,ISNULL(T4.cal,T3.cal)) cal ,
	ISNULL(T5.rep,ISNULL(T4.rep,T3.rep)) rep ,
	ISNULL(T5.adj,ISNULL(T4.adj,T3.adj)) adj ,
	ISNULL(T5.pur,ISNULL(T4.pur,T3.pur)) pur ,
	--ISNULL(ISNULL(T8.ser,ISNULL(T7.ser,T6.ser)),0) ser ,
	0 ser,
	--ISNULL(T5.total,ISNULL(T4.total,T3.total)) + ISNULL(ISNULL(T8.ser,ISNULL(T7.ser,T6.ser)),0) total ,
	ISNULL(T5.total,ISNULL(T4.total,T3.total)) total ,
	CURRENT_TIMESTAMP
from 
	@AIJI T1 
	LEFT JOIN @AICR T3 on T1.jobitemid = T3.jobitemid
	LEFT JOIN @AIJC T4 on T1.jobitemid = T4.jobitemid
	LEFT JOIN @AIINV T5 on T1.jobitemid = T5.jobitemid
	--LEFT JOIN @SERJOBCOSTS T6 on T1.jobitemid = T6.jobitemid
	--LEFT JOIN @SERJCCOSTS T7 on T1.jobitemid = T7.jobitemid
	--LEFT JOIN @SERINVCOSTS T8 on T1.jobitemid = T8.jobitemid
UNION ALL
select
	T1.orgid,  
	T1.coname ,
	T1.legalidentifier,
	T1.coid ,
	T1.subdivid,
	T1.subname,
	T1.jobno,
	T1.clientsubname,
	T1.clientcontact,
	T1.lastContractReviewBy,
	T1.jobid,
	T1.businesssubname,
	T1.analyticalcenter,
	T1.Description,
	T1.itemno,
	T1.stateid,
	T1.itemstate,
	T1.instmodeltranslation,
	T1.serialno,
	T1.plantno,
	T1.ponos,
	T1.proforma,
	T1.datecomplete,
	T1.deliveryno,
	T1.deliverydate,
	T1.caldate,
	T1.jobitemid,
	T1.jobitemdatecomplete,
	'JS' source, 
	T1.cal,
	T1.rep,
	T1.adj,
	T1.pur,
	T1.ser,
	T1.total,
	CURRENT_TIMESTAMP
from
	@AIJS T1
;

COMMIT TRAN