/**** CREDNOTEACCS TABLE ****/

CREATE VIEW CREDNOTEACCS AS
        SELECT co.coid                                            ,
               cn.id AS id                                         ,
               cn.creditnoteno                                     ,
               n.title						,
               i.invoicedate                                         ,
               cni.description as linedetail					  ,
               n.code                                             ,
			   case when cur.currencycode = 'GBP' then cni.totalcost else null end as totalcost ,
               case when cur.currencycode <> 'GBP' then cni.totalcost else null end as altcost ,
               cni.id AS itemid                                    ,
               cn.accountstatus                                   ,
               co.vatcode                                         ,
               cni.discountrate                                    ,
               CONVERT(VARCHAR(10), GETDATE(), 104) as currentdate ,
               a.addr1 + CHAR(13) + a.addr2 + CHAR(13) + a.addr3 AS creditaddr     ,
               a.town                                       AS credittown     ,
               a.county                                     AS creditcounty   ,
               a.postcode                                   AS creditpostcode ,
               a.country                                    AS creditcountry  ,
               cn.rate                                                   ,
               cur.accountscode as currencycode,
               i.period,
               'Crediting Inv. ' + i.invno as ref,
			   i.orgid
        FROM   creditnote cn,
               (creditnoteitem cni
               LEFT JOIN nominalcode n
               ON     cni.nominalid = n.id),
               invoice i				   ,
               subdiv s                   ,
               company co                 ,
               address a                  ,
               supportedcurrency cur      ,
               companyaccount ac
        WHERE  cni.creditnoteid       = cn.id
				AND i.id = cn.invid
           AND a.subdivid       = s.subdivid
           AND s.coid           = co.coid
           AND a.addrid         = i.addressid
           AND cur.currencyid   = cn.currencyid
           AND co.accountid     = ac.id;