/* New view for credit note accounts - Created by TProvost the 2016-05-11 */
/* DO NOT RENAME COLUMNS defined in this view - or if you do make sure you update the Transaction Broker file accordingly */
/* 
 * Last Modification: 
 * 		2016-11-18 TProvost: Fixed bug on existing field vatValue and add field to get the vatRate
 *  	2018-01-03 TPRovost: Replaced CRLF and TAB with a space in the item description to prevent the Excel issue
 */

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'new_creditnoteaccounts')
    DROP VIEW new_creditnoteaccounts
GO

CREATE VIEW new_creditnoteaccounts 
AS
SELECT cn.id AS cnId
    -- Header
	,cn.creditnoteno AS cnNo
	,i.id AS invId
	,i.invno AS invNo
	,i.invoicedate AS invDate
	,inv_comp.coid AS billTo_id
	,inv_comp.coname AS billTo_name
	,inv_comp.fiscalidentifier AS billTo_fiscalId
    ,(ISNULL(inv_addr.addr1, '') + CHAR(32) + ISNULL(inv_addr.addr2, '') + CHAR(32) + ISNULL(inv_addr.addr3, '')) as billTo_address
	,inv_addr.town AS billTo_city
	,inv_addr.postcode AS billTo_postCode
	,inv_addr.county AS billTo_county
	,inv_country.countrycode AS billTo_countryCode
	,(inv_defcont.firstname + CHAR(32) + inv_defcont.lastname) AS billTo_contact
	,inv_defcont.email AS billTo_email
	,cn.accountstatus AS accountStatus	
	,CONVERT(VARCHAR(10), GETDATE(), 104) AS currentDate
	,cn.regdate AS regDate
	,cn.issuedate AS issueDate
	,sc.currencycode AS currencyCode
	,cn.rate AS currencyRate
	,cn.totalcost AS totalAmountExclTax
	,cn.finalcost AS totalAmountInclTax
	-- Items
	,cni.itemno AS itemNo
	,'Item' AS type
	,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cni.description, CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ') AS description	
	,nc.code AS nominalCode
	,nc.title AS nominalTitle
	,cni.quantity AS quantity
	,cni.totalcost AS unitPrice			
	,cn.vatRate
	,(cni.finalcost * cn.vatrate / 100) AS vatValue
	,CASE 
		WHEN vr.type = 0 THEN 'NATIONAL'
		WHEN vr.type = 1 THEN vr.description
		WHEN vr.type = 2 THEN 'UE'
		WHEN vr.type = 3 THEN 'EXPORT'
		WHEN vr.type = 4 THEN 'EXPORT'
		ELSE NULL
		END AS vatZone
	,cni.discountrate AS discountRate
	,cni.discountvalue AS discountValue
	,cni.finalcost AS amountExclTax
	,CASE 
		WHEN cn.vatrate IS NULL
			THEN cni.finalcost
		ELSE CAST(cni.finalcost + (cni.finalcost * cn.vatrate / 100) AS NUMERIC(10,2))
		END AS amountInclTax	
	,cni_subdiv.analyticalCenter AS analyticalCenter
 	,bus.coid AS busId
	,bus.coname AS busName  
FROM creditnote cn
	INNER JOIN creditnoteitem cni ON cni.creditnoteid = cn.id
	INNER JOIN invoice i ON i.id = cn.invid
	INNER JOIN company AS inv_comp ON inv_comp.coid = i.coid
	INNER JOIN address AS inv_addr ON inv_addr.addrid = i.addressid
	INNER JOIN subdiv AS inv_subdiv ON inv_subdiv.subdivid = inv_addr.subdivid
	INNER JOIN country AS inv_country ON inv_country.countryid = inv_addr.countryid
	INNER JOIN supportedcurrency AS sc ON sc.currencyid = cn.currencyid
	INNER JOIN company AS bus ON bus.coid = cn.orgid
	LEFT OUTER JOIN vatrate AS vr ON vr.vatcode = cn.vatcode
	LEFT OUTER JOIN nominalcode AS nc ON nc.id = cni.nominalid
	LEFT OUTER JOIN subdiv AS cni_subdiv ON cni_subdiv.subdivid = cni.orgid
	LEFT OUTER JOIN contact AS inv_defcont ON inv_defcont.personid = inv_subdiv.defpersonid
WHERE cn.accountstatus = 'P'
GO

/* Redefine rights for supplier to access and update view */
BEGIN

DECLARE @userName sysname = 'COMUNICA'
DECLARE @roleName sysname = 'db_accountinginterface'
DECLARE @cmd varchar(4000)

---> Define securables for database role
SET @CMD = 'GRANT SELECT ON [dbo].[new_creditnoteaccounts] TO [@roleName]'
SET @CMD = REPLACE(@CMD, '@roleName', @roleName)
PRINT (@CMD)
EXEC (@CMD)

SET @CMD = 'GRANT UPDATE ON [dbo].[new_creditnoteaccounts] ([accountstatus]) TO [@roleName]'
SET @CMD = REPLACE(@CMD, '@roleName', @roleName)
PRINT (@CMD)
EXEC (@CMD)

---> Add role to the user
EXECUTE sp_AddRoleMember @roleName, @userName

END