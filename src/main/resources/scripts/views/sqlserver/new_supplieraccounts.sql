/* New view for supplier Accounts - TProvost 2016-03-11 */
/* DO NOT RENAME COLUMNS AND DO NOT REORDER COLUMNS defined in this view - or if you do make sure you update the Transaction Broker file accordingly */
/* Last Modification: 
 * 		2016-06-30 TProvost : Added column hasIssuedPO (boolean) to indicate if the supplier has issued purchase orders (specific per business company)
 */

IF NOT EXISTS (
		SELECT TABLE_NAME
		FROM INFORMATION_SCHEMA.VIEWS
		WHERE TABLE_NAME = 'new_supplieraccounts'
		)
	EXECUTE('CREATE VIEW new_supplieraccounts AS SELECT * FROM company WHERE coid = 0')
GO

ALTER VIEW new_supplieraccounts
AS
SELECT supp.coid AS id
	,supp.coname AS NAME
	,(a.addr1 + CHAR(32) + a.addr2 + CHAR(32) + a.addr3) AS address
	,a.town AS city
	,a.postcode AS postCode
	,a.county AS county
	,CASE 
		WHEN supp.legaladdressid IS NOT NULL
			THEN countrya.countrycode
		ELSE countrysupp.countrycode
		END AS countryCode
	,(con.firstname + CHAR(32) + con.lastname) AS contact
	,con.telephone AS telephoneNo
	,con.fax AS faxNo
	,con.email AS email
	,CASE 
		WHEN supp.corole = 5
			THEN 1
		ELSE 0
		END AS postingGroup
	,supp.fiscalidentifier AS fiscalId
	,CASE 
		WHEN supp.corole = 5
			THEN 'GROUP '
		ELSE ''
		END + CASE 
		WHEN supp.countryid = bus.countryid
			THEN 'NATIONAL'
		ELSE CASE 
				WHEN countrysupp.memberOfUE = 1
					THEN 'UE'
				ELSE 'EXPORT'
				END
		END AS genBusPostingGroup
	,pt.code AS paymentTermsCode
	,pm.code AS paymentModeCode
	,i.code AS invoiceFrequencyCode
	,ba.bankname AS bankName
	,ba.iban AS iban
	,ba.swiftbic AS swift
	,ba.accountno AS accountNo
	,CASE 
		WHEN ba.currencyid IS NOT NULL
			THEN scba.currencycode
		ELSE scsupp.currencycode
		END AS currencyCode
	,b.NAME AS sector
	,(
		SELECT MAX(MaxCol)
		FROM (
			VALUES (supp.lastModified) -- Date of last modified of the supplier
				,(suppset.lastModified) -- Date of last modified of the settings of the supplier
				,(ba.lastModified) -- Date of last modified of the default bank account of the supplier
				,(s.lastModified) -- Date of last modified of the default subdivision of the supplier
				,(a.lastmodified) -- Date of last modified of the legal address of the supplier
				,(con.lastmodified) -- Date of last modified of the default contact of default subdivision of the supplier
			) AS MaxTable(MaxCol)
		) AS lastModified
	,CASE WHEN
			(SELECT count(*) 
			FROM purchaseorder
				INNER JOIN dbo.contact ON contact.personid = purchaseorder.personid
				INNER JOIN dbo.subdiv ON subdiv.subdivid = contact.subdivid
			WHERE subdiv.coid = supp.coid
				AND purchaseorder.orgid = bus.coid
				AND purchaseorder.issued = 1) > 0 
			THEN 1
		ELSE 0
		END AS hasIssuedPO
	,bus.coid AS busId
	,bus.coname AS busName
FROM company supp
INNER JOIN country AS countrysupp ON countrysupp.countryid = supp.countryid -- Country of the customer
INNER JOIN companysettingsforallocatedcompany AS suppset ON suppset.companyid = supp.coid -- Settings of the customer (for each Business/Trescal Company)
INNER JOIN company AS bus ON bus.coid = suppset.orgid -- Business/Trescal Company
LEFT OUTER JOIN address AS a ON a.addrid = supp.legaladdressid -- Legal Address of the supplier
LEFT OUTER JOIN country AS countrya ON countrya.countryid = a.countryid -- Country of the legal address of the supplier
LEFT OUTER JOIN subdiv AS s ON s.subdivid = supp.defsubdivid -- Default subdivision of the supplier
LEFT OUTER JOIN contact AS con ON con.personid = s.defpersonid -- Default Contact of the default subdivision of the supplier
LEFT OUTER JOIN businessarea AS b ON b.businessareaid = supp.businessareaid
LEFT OUTER JOIN paymentterms AS pt ON pt.id = suppset.paymenttermsid
LEFT OUTER JOIN paymentmode AS pm ON pm.id = suppset.paymentmodeid
LEFT OUTER JOIN invoicefrequency AS i ON i.id = suppset.invoicefrequencyid
LEFT OUTER JOIN bankaccount AS ba ON ba.id = suppset.bankaccountid -- Default Bank Account of the supplier
LEFT OUTER JOIN supportedcurrency AS scsupp ON scsupp.currencyid = supp.currencyid -- Supported Currency of the supplier
LEFT OUTER JOIN supportedcurrency AS scba ON scba.currencyid = ba.currencyid --- Supported Currency of the default Bank Account of the supplier
WHERE supp.corole IN (
		2
		,4
		,5
		) -- Note CompanyRole for Supplier is 2, Utility is 4, Business is 5 
GO

/* Redefine rights for supplier to access view */
BEGIN

DECLARE @userName sysname = 'COMUNICA'
DECLARE @roleName sysname = 'db_accountinginterface'
DECLARE @cmd varchar(4000)

---> Define securables for database role
SET @CMD = 'GRANT SELECT ON [dbo].[new_supplieraccounts] TO [@roleName]'
SET @CMD = REPLACE(@CMD, '@roleName', @roleName)
PRINT (@CMD)
EXEC (@CMD)

---> Add role to the user
EXECUTE sp_AddRoleMember @roleName, @userName

END
