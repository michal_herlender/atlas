-- The following views are used to aid calculations on the job progress page


-- view that lists all job items currently active on the system for business companies (i.e. Antech or MD)

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'activebusinessjobitems')
    DROP VIEW activebusinessjobitems
GO

-- Note corole (CompanyRole) 5 is Business 
CREATE VIEW activebusinessjobitems as
select ji.jobitemid, ji.duedate
from job j, contact con, subdiv sub, company comp, jobitem ji, itemstate i
where ji.jobid = j.jobid and j.personid = con.personid
and con.subdivid = sub.subdivid
and sub.coid = comp.coid
and comp.corole = 5
and i.stateid = ji.stateid
and i.active = 1;


-- view that lists all job items currently active on the system for client companies

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'activeclientjobitems')
    DROP VIEW activeclientjobitems
GO

-- Note corole (CompanyRole) 1 is Client
CREATE VIEW activeclientjobitems as
select ji.jobitemid, ji.duedate
from job j, contact con, subdiv sub, company comp, jobitem ji, itemstate i
where ji.jobid = j.jobid and j.personid = con.personid
and con.subdivid = sub.subdivid
and sub.coid = comp.coid
and comp.corole = 1
and i.stateid = ji.stateid
and i.active = 1;