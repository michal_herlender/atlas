/* New view for customer Accounts - TProvost 2016-03-11 */
/* DO NOT RENAME COLUMNS AND DO NOT REORDER COLUMNS defined in this view - or if you do make sure you update the Transaction Broker file accordingly */
/* Last Modification: 
 * 		2016-06-29 TProvost : Added column isTaxable (boolean) to indicate if the customer is taxable or not (specific per business company)
 * 		2016-06-30 TProvost : Added column hasIssuedInvoices (boolean) to indicate if the customer has already been invoiced (specific per business company)
*/

IF NOT EXISTS (
		SELECT TABLE_NAME
		FROM INFORMATION_SCHEMA.VIEWS
		WHERE TABLE_NAME = 'new_customeraccounts'
		)
	EXECUTE('CREATE VIEW new_customeraccounts AS SELECT * FROM company WHERE coid = 0')
GO

ALTER VIEW new_customeraccounts
AS
SELECT cust.coid AS id
	,cust.coname AS name
	,(a.addr1 + CHAR(32) + a.addr2 + CHAR(32) + a.addr3) AS address
	,a.town AS city
	,a.postcode AS postCode
	,a.county AS county
	,CASE 
		WHEN cust.legaladdressid IS NOT NULL
			THEN countrya.countrycode
		ELSE countrycust.countrycode
		END AS countryCode
	,(con.firstname + CHAR(32) + con.lastname) AS contact
	,con.telephone AS telephoneNo
	,con.fax AS faxNo
	,con.email AS email
	,CASE 
		WHEN cust.corole = 5
			THEN 1
		ELSE 0
		END AS postingGroup
	,cust.fiscalidentifier AS fiscalId
	,CASE 
		WHEN cust.corole = 5
			THEN 'GROUP '
		ELSE ''
		END + CASE 
		WHEN cust.countryid = bus.countryid
			THEN 'NATIONAL'
		ELSE CASE 
				WHEN countrycust.memberOfUE = 1
					THEN 'UE'
				ELSE 'EXPORT'
				END
		END AS genBusPostingGroup
	,pt.code AS paymentTermsCode
	,pm.code AS paymentModeCode
	,i.code AS invoiceFrequencyCode
	,ba.bankname AS bankName
	,ba.iban AS iban
	,ba.swiftbic AS swift
	,ba.accountno AS accountNo
	,CASE 
		WHEN ba.currencyid IS NOT NULL
			THEN scba.currencycode
		ELSE sccust.currencycode
		END AS currencyCode
	,b.NAME AS sector
	,(
		SELECT MAX(MaxCol)
		FROM (
			VALUES (cust.lastModified) -- Date of last modified of the customer
				,(custset.lastModified) -- Date of last modified of the settings of the customer
				,(ba.lastModified) -- Date of last modified of the default bank account of the customer
				,(s.lastModified) -- Date of last modified of the default subdivision of the customer
				,(a.lastmodified) -- Date of last modified of the legal address of the customer
				,(con.lastmodified) -- Date of last modified of the default contact of default subdivision of the customer
			) AS MaxTable(MaxCol)
		) AS lastModified
	,CASE WHEN
			(SELECT count(*) 
			FROM invoice 
			WHERE invoice.coid = cust.coid 
				AND invoice.orgid = bus.coid
				AND invoice.issued = 1) > 0 
			THEN 1
		ELSE 0
		END AS hasIssuedInvoices
	,bus.coid AS busId
	,bus.coname AS busName
	,custset.taxable AS isTaxable	
FROM company cust
INNER JOIN country AS countrycust ON countrycust.countryid = cust.countryid -- Country of the customer
INNER JOIN companysettingsforallocatedcompany AS custset ON custset.companyid = cust.coid -- Settings of the customer (for each Business/Trescal Company)
INNER JOIN company AS bus ON bus.coid = custset.orgid -- Business/Trescal Company
LEFT OUTER JOIN address AS a ON a.addrid = cust.legaladdressid -- Legal Address of the customer
LEFT OUTER JOIN country AS countrya ON countrya.countryid = a.countryid -- Country of the legal address of the customer
LEFT OUTER JOIN subdiv AS s ON s.subdivid = cust.defsubdivid -- Default subdivision of the customer
LEFT OUTER JOIN contact AS con ON con.personid = s.defpersonid -- Default Contact of the default subdivision of the customer
LEFT OUTER JOIN businessarea AS b ON b.businessareaid = cust.businessareaid
LEFT OUTER JOIN paymentterms AS pt ON pt.id = custset.paymenttermsid
LEFT OUTER JOIN paymentmode AS pm ON pm.id = custset.paymentmodeid
LEFT OUTER JOIN invoicefrequency AS i ON i.id = custset.invoicefrequencyid
LEFT OUTER JOIN bankaccount AS ba ON ba.id = custset.bankaccountid -- Default Bank Account of the customer
LEFT OUTER JOIN supportedcurrency AS sccust ON sccust.currencyid = cust.currencyid -- Supported Currency of the customer
LEFT OUTER JOIN supportedcurrency AS scba ON scba.currencyid = ba.currencyid -- Supported Currency of the default Bank Account of the customer
WHERE cust.corole IN (
		1
		,5
		) -- Note: CompanyRole for Client is 1, Business is 5
GO


/* Redefine rights for supplier to access view */
BEGIN

DECLARE @userName sysname = 'COMUNICA'
DECLARE @roleName sysname = 'db_accountinginterface'
DECLARE @cmd varchar(4000)

---> Define securables for database role
SET @CMD = 'GRANT SELECT ON [dbo].[new_customeraccounts] TO [@roleName]'
SET @CMD = REPLACE(@CMD, '@roleName', @roleName)
PRINT (@CMD)
EXEC (@CMD)

---> Add role to the user
EXECUTE sp_AddRoleMember @roleName, @userName

END