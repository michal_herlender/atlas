/* New view for invoice Accounts - TProvost 2016-03-11 */
/* DO NOT RENAME COLUMNS AND DO NOT REORDER COLUMNS defined in this view - or if you do make sure you update the Transaction Broker file accordingly */
/* 
 * Last Modification: 
 * 		2016-05-12 TProvost : Renamed column nominalDesc to nominalTitle + Added column issueDate  
 * 		2016-06-15 TProvost : Change rule to deduce VAT Zone - For vatrate of type 1, if countryid = 162, we use "EXPORT" and not the description of vatrate
 * 		2016-06-30 TProvost : Change the mechanism to get client purchase order no (use STUFF and XML PATH function to concatenate PO number and BPO number with separator ;)
 * 		2016-07-05 TProvost : Change the mechanism to get delivery no (use STUFF and XML PATH function to delivery Number with separator ;)
 * 		2016-11-18 TProvost : Fixed bug on existing field vatValue and add field to get the vatRate
 */

/* Lines of invoice */
IF NOT EXISTS (
		SELECT TABLE_NAME
		FROM INFORMATION_SCHEMA.VIEWS
		WHERE TABLE_NAME = 'new_invoiceaccounts_costssummary'
		)
	EXECUTE('CREATE VIEW new_invoiceaccounts_costssummary AS SELECT * FROM invoiceitem WHERE invoiceitemid = 0')
GO

ALTER VIEW new_invoiceaccounts_costssummary
AS
/* get any active cal costs*/
SELECT ii.id AS invitemid
	,ii.invoiceid AS invid
	,'CAL_COST' AS costtype
	,ii.itemno AS itemno
	,ii.quantity AS quantity
	,c.nominalid AS nominalid
	,c.discountrate AS discountrate
	,c.discountvalue AS discountvalue
	,c.finalcost AS finalcost
	,c.totalcost AS totalcost
	,ii.jobitemid AS jobitemid
	,ii.orgid AS subdivid
FROM invoice i
	,invoiceitem ii
	,costs_calibration c
WHERE ii.calcost_id = c.costid
	AND ii.invoiceid = i.id
	AND i.pricingtype = 'CLIENT'
	AND c.active = 1
	AND ii.breakupcosts = 1
	AND ii.finalcost != 0.00

UNION ALL

/* get any active repair costs*/
SELECT ii.id AS invitemid
	,ii.invoiceid AS invid
	,'REP_COST' AS costtype
	,ii.itemno AS itemno
	,ii.quantity AS quantity
	,c.nominalid AS nominalid
	,c.discountrate AS discountrate
	,c.discountvalue AS discountvalue
	,c.finalcost AS finalcost
	,c.totalcost AS totalcost
	,ii.jobitemid AS jobitemid
	,ii.orgid AS subdivid	
FROM invoice i
	,invoiceitem ii
	,costs_repair c
WHERE ii.repaircost_id = c.costid
	AND ii.invoiceid = i.id
	AND i.pricingtype = 'CLIENT'
	AND c.active = 1
	AND ii.breakupcosts = 1
	AND ii.finalcost != 0.00
	
UNION ALL

/* get any active sales costs*/
SELECT ii.id AS invitemid
	,ii.invoiceid AS invid
	,'SAL_COST' AS costtype
	,ii.itemno AS itemno
	,ii.quantity AS quantity
	,c.nominalid AS nominalid
	,c.discountrate AS discountrate
	,c.discountvalue AS discountvalue
	,c.finalcost AS finalcost
	,c.totalcost AS totalcost
	,ii.jobitemid AS jobitemid
	,ii.orgid AS subdivid
FROM invoice i
	,invoiceitem ii
	,costs_purchase c
WHERE ii.purchasecost_id = c.costid
	AND ii.invoiceid = i.id
	AND i.pricingtype = 'CLIENT'
	AND c.active = 1
	AND ii.breakupcosts = 1
	AND ii.finalcost != 0.00	

UNION ALL

/* get any active adjustment costs*/
SELECT ii.id AS invitemid
	,ii.invoiceid AS invid
	,'ADJ_COST' AS costtype
	,ii.itemno AS itemno
	,ii.quantity AS quantity
	,c.nominalid AS nominalid
	,c.discountrate AS discountrate
	,c.discountvalue AS discountvalue
	,c.finalcost AS finalcost
	,c.totalcost AS totalcost
	,ii.jobitemid AS jobitemid
	,ii.orgid AS subdivid	
FROM invoice i
	,invoiceitem ii
	,costs_adjustment c
WHERE ii.adjustmentcost_id = c.costid
	AND ii.invoiceid = i.id
	AND i.pricingtype = 'CLIENT'
	AND c.active = 1
	AND ii.breakupcosts = 1
	AND ii.finalcost != 0.00

UNION ALL

/* get any general costs*/
SELECT ii.id AS invitemid
	,ii.invoiceid AS invid
	,'SGL_COST' AS costtype
	,ii.itemno AS itemno
	,ii.quantity AS quantity
	,ii.nominalid AS nominalid
	,ii.discountrate AS discountrate
	,ii.discountvalue AS discountvalue
	,ii.finalcost AS finalcost
	,ii.totalcost AS totalcost
	,ii.jobitemid AS jobitemid
	,ii.orgid AS subdivid	
FROM invoice i
	,invoiceitem ii
WHERE ii.invoiceid = i.id
	AND i.pricingtype = 'CLIENT'
	AND ii.breakupcosts = 0
	AND ii.finalcost != 0.00	

UNION ALL

-- get site invoice costs: use the nominal code defined on first item of the invoice and use the totalcost defined on the invoice    
SELECT ii.id AS invitemid
	,ii.invoiceid AS invid
	,'SIT_COST' AS costtype
	,ii.itemno AS itemno
	,ii.quantity AS quantity
	,ii.nominalid AS nominalid
	,ii.discountrate AS discountrate
	,ii.discountvalue AS discountvalue
	,i.totalcost AS finalcost --,ii.finalcost AS finalcost
	,i.totalcost AS totalcost -- ,ii.totalcost AS totalcost
	,ii.jobitemid AS jobitemid
	,ii.orgid AS subdivid	
FROM invoice i
	,invoiceitem ii
WHERE ii.invoiceid = i.id
	AND i.pricingtype = 'SITE'
	AND ii.itemno = 1
	
UNION ALL

/* get any carriage costs*/
SELECT 0 AS invitemid
	,i.id AS invid
	,'CARR_COST' AS costtype
	,0 AS itemno
	,1 AS quantity
	,nl.nominalid AS nominalid
	,0 AS discountrate
	,0 AS discountvalue
	,i.carriage AS finalcost
	,i.carriage AS totalcost
	,NULL AS jobitemid
	,NULL AS subdivid	
FROM invoice i
	LEFT OUTER JOIN nominalinvoicelookup nl ON nl.nominaltype = 'carriage'
WHERE i.carriage != 0.00

UNION ALL

/* get any active services costs*/
SELECT ii.id AS invitemid
	,ii.invoiceid AS invid
	,'SER_COST' AS costtype
	,ii.itemno AS itemno
	,ii.quantity AS quantity
	,c.nominal AS nominalid
	,c.discountrate AS discountrate
	,c.discountvalue AS discountvalue
	,c.finalcost AS finalcost
	,c.totalcost AS totalcost
	,ii.jobitemid AS jobitemid
	,ii.orgid AS subdivid	
FROM invoice i
	,invoiceitem ii
	,costs_service c
WHERE ii.servicecost = c.costid
	AND ii.invoiceid = i.id
	AND i.pricingtype = 'CLIENT'
	AND c.active = 1
	AND ii.breakupcosts = 1
	AND ii.finalcost != 0.00

GO


/* Header + Lines of Invoice */
IF EXISTS (
		SELECT TABLE_NAME
		FROM INFORMATION_SCHEMA.VIEWS
		WHERE TABLE_NAME = 'new_invoiceaccounts'
		)
	DROP VIEW new_invoiceaccounts
	EXECUTE('CREATE VIEW new_invoiceaccounts AS SELECT * FROM invoice WHERE id = 0')
GO

ALTER VIEW new_invoiceaccounts
AS
SELECT i.id AS invId
	,i.invno AS invNo
	-- Ship To
	,inv_comp.coid AS sellTo_id	
	-- Bill To 
	,inv_comp.coid AS billTo_id
	,inv_comp.coname AS billTo_name
	,inv_comp.fiscalidentifier AS billTo_fiscalId
    ,(ISNULL(inv_addr.addr1, '') + CHAR(32) + ISNULL(inv_addr.addr2, '') + CHAR(32) + ISNULL(inv_addr.addr3, '')) as billTo_address
	,inv_addr.town AS billTo_city
	,inv_addr.postcode AS billTo_postCode
	,inv_addr.county AS billTo_county
	,inv_country.countrycode AS billTo_countryCode
	,(inv_defcont.firstname + CHAR(32) + inv_defcont.lastname) AS billTo_contact
	,inv_defcont.email AS billTo_email
	-- Ship To
	,del_comp.coid AS shipTo_id
	,del_comp.coname AS shipTo_name	
	,(ISNULL(del_addr.addr1, '') + CHAR(32) + ISNULL(del_addr.addr2, '') + CHAR(32) + ISNULL(del_addr.addr3, '')) as shipTo_address
	,del_addr.town AS shipTo_city
	,del_addr.postcode AS shipTo_postCode
	,del_addr.county AS shipTo_county
	,del_country.countrycode AS shipTo_countryCode
	,(ISNULL(del_cont.firstname, '') + CHAR(32) + ISNULL(del_cont.lastname, '')) AS shipTo_contact
	,del_cont.email AS shipTo_email		
	-- Others data on invoice
	,i.accountstatus AS accountStatus
	,CONVERT(VARCHAR(10), GETDATE(), 104) AS currentDate
	,i.regdate AS regDate
	,i.issuedate AS issueDate
	,i.invoicedate AS invoiceDate
	,pt.code AS paymentTermsCode
	,pm.code AS paymentModeCode
	,i.period AS period
	,sc.currencycode AS currencyCode
	,i.rate AS currencyRate
	,i.totalcost AS totalAmountExclTax
	,i.finalcost AS totalAmountInclTax
	-- Invoice Lines
	,ii.itemno AS itemNo
	,'Item' AS type
	,nc.code AS nominalCode
	,nc.title AS nominalDesc
	,ii.quantity AS quantity
	,ii.totalcost AS unitPrice
	,i.vatrate AS vatRate
	,(ii.finalcost * i.vatrate / 100) AS vatValue
	,CASE 
		WHEN vr.type = 0 THEN 'NATIONAL'
		WHEN vr.type = 1 AND vr.countryid = 162 THEN 'EXPORT'
		WHEN vr.type = 1 AND vr.countryid != 162 THEN vr.description
		WHEN vr.type = 2 THEN 'UE'
		WHEN vr.type = 3 THEN 'EXPORT'
		WHEN vr.type = 4 THEN 'EXPORT'
		ELSE NULL
		END AS vatZone
	,ii.discountrate AS discountRate
	,ii.discountvalue AS discountValue
	,ii.finalcost AS amountExclTax
	,CASE 
		WHEN i.vatrate IS NULL
			THEN ii.finalcost
		ELSE CAST(ii.finalcost + (ii.finalcost * i.vatrate / 100) AS NUMERIC(10,2))
		END AS amountInclTax	
	,CASE 
		WHEN ii.itemno = 0 -- !!! Specific case for carriage !!! 
			THEN 
			-- Use the analytical center of the first item where the subdivision is filled, else use the analytical center of the subdiv of the creator of the invoice
			COALESCE(
						(SELECT TOP 1 subdiv.analyticalCenter
						FROM dbo.invoiceitem
							INNER JOIN dbo.subdiv ON subdiv.subdivid = invoiceitem.orgid
						WHERE invoiceitem.invoiceid = ii.invid
							AND invoiceitem.orgid IS NOT NULL
						ORDER BY invoiceitem.itemno),
						inv_subdivcreator.analyticalcenter)
		ELSE 
			-- Use, by priority, the analytical center of: 
				-- 1. subdiv defined at invoice item
				-- 2. subdiv that created the job
				-- 3. subdiv of the creator of the invoice
			COALESCE(invitem_subdiv.analyticalCenter, job_subdiv.analyticalCenter, inv_subdivcreator.analyticalcenter)
		END AS analyticalCenter
	,job.jobno AS jobNo
	,STUFF((SELECT ';' + del.deliveryno	
		FROM deliveryitem AS deli
			INNER JOIN delivery AS del ON del.deliveryid = deli.deliveryid
		WHERE deli.jobitemid = ii.jobitemid
			AND deliveryitemtype = 'job' AND deli.deliveryid IN (
				SELECT tmp.deliveryid
				FROM delivery tmp
				WHERE tmp.deliverytype = 'client')
		FOR XML PATH('')),1,1,'') AS deliveryNo	
	,STUFF((SELECT ';' + ISNULL(po.ponumber, bpo.ponumber)	
		FROM jobitempo AS jipo
			LEFT OUTER JOIN po AS po ON po.poid = jipo.poid
			LEFT OUTER JOIN bpo AS bpo ON bpo.poid = jipo.bpoid
		WHERE jipo.jobitemid = ii.jobitemid
		FOR XML PATH('')),1,1,'') AS purchaseOrderNo
	,bus.coid AS busId
	,bus.coname AS busName	
FROM invoice AS i
	INNER JOIN company AS inv_comp ON inv_comp.coid = i.coid
	INNER JOIN address AS inv_addr ON inv_addr.addrid = i.addressid
	INNER JOIN subdiv AS inv_subdiv ON inv_subdiv.subdivid = inv_addr.subdivid
	INNER JOIN country AS inv_country ON inv_country.countryid = inv_addr.countryid
	INNER JOIN supportedcurrency AS sc ON sc.currencyid = i.currencyid
	INNER JOIN company AS bus ON bus.coid = i.orgid
	LEFT OUTER JOIN paymentterms AS pt ON pt.id = i.paymenttermsid
	LEFT OUTER JOIN paymentmode AS pm ON pm.id = i.paymentmodeid
	LEFT OUTER JOIN vatrate AS vr ON vr.vatcode = i.vatcode
	LEFT OUTER JOIN new_invoiceaccounts_costssummary AS ii ON ii.invid = i.id
	LEFT OUTER JOIN nominalcode AS nc ON nc.id = ii.nominalid
	LEFT OUTER JOIN contact AS inv_defcont ON inv_defcont.personid = inv_subdiv.defpersonid

	-- Get subdivision that created the job
	LEFT OUTER JOIN jobitem ON jobitem.jobitemid = ii.jobitemid
	LEFT OUTER JOIN job ON job.jobid = jobitem.jobid
	LEFT OUTER JOIN subdiv AS job_subdiv ON job_subdiv.subdivid = job.orgid

	-- Get subdivision that created the invoice
	LEFT OUTER join contact AS inv_creator ON inv_creator.personid = i.createdby
	LEFT OUTER JOIN subdiv AS inv_subdivcreator ON inv_subdivcreator.subdivid = inv_creator.subdivid
	
	-- Get subdivision of invoiceitem
	LEFT OUTER JOIN subdiv AS invitem_subdiv ON invitem_subdiv.subdivid = ii.subdivid
	
	-- Get unique delivery address for all the job
	LEFT OUTER JOIN invoicejoblink AS jlsingleaddr ON jlsingleaddr.invoiceid = i.id
		AND jlsingleaddr.jobid = (
			SELECT MAX (tmp.jobid)
			FROM invoicejoblink tmp
				,delivery tmp2
			WHERE tmp.invoiceid = jlsingleaddr.invoiceid
				AND tmp2.jobid = tmp.jobid
				AND deliverytype = 'client'
			HAVING count(distinct(tmp2.addressid)) <= 1 
				)
	LEFT OUTER JOIN delivery AS delsingleaddr ON delsingleaddr.jobid = jlsingleaddr.jobid
		AND delsingleaddr.deliveryid = (
			SELECT MAX(tmp.deliveryid)
			FROM delivery tmp 
			WHERE tmp.jobid = jlsingleaddr.jobid
			AND delsingleaddr.deliverytype = 'client'
			)
	LEFT OUTER JOIN address AS del_addr ON del_addr.addrid = delsingleaddr.addressid
	LEFT OUTER JOIN subdiv AS del_subdiv ON del_subdiv.subdivid = del_addr.subdivid
	LEFT OUTER JOIN company AS del_comp ON del_comp.coid = del_subdiv.coid
	LEFT OUTER JOIN country AS del_country ON del_country.countryid = del_addr.countryid

	-- Get unique delivery contact for the all the job
	LEFT OUTER JOIN invoicejoblink AS jlsinglecon ON jlsinglecon.invoiceid = i.id
		AND jlsinglecon.jobid = (
			SELECT MAX (tmp.jobid)
			FROM invoicejoblink tmp
				,delivery tmp2
			WHERE tmp.invoiceid = jlsinglecon.invoiceid
				AND tmp2.jobid = tmp.jobid
				AND deliverytype = 'client'
			HAVING count(distinct(tmp2.contactid)) <= 1
				)
	LEFT OUTER JOIN delivery AS delsinglecon ON delsinglecon.jobid = jlsinglecon.jobid
		AND delsinglecon.deliveryid = (
			SELECT MAX(tmp.deliveryid)
			FROM delivery tmp 
			WHERE tmp.jobid = jlsinglecon.jobid
			AND delsinglecon.deliverytype = 'client'
			)
	LEFT OUTER JOIN contact AS del_cont ON del_cont.personid = delsinglecon.contactid
WHERE i.accountstatus = 'P'	
GO


/* Redefine rights for supplier to access and update view */
BEGIN

DECLARE @userName sysname = 'COMUNICA'
DECLARE @roleName sysname = 'db_accountinginterface'
DECLARE @cmd varchar(4000)

---> Define securables for database role
SET @CMD = 'GRANT SELECT ON [dbo].[new_invoiceaccounts] TO [@roleName]'
SET @CMD = REPLACE(@CMD, '@roleName', @roleName)
PRINT (@CMD)
EXEC (@CMD)

SET @CMD = 'GRANT UPDATE ON [dbo].[new_invoiceaccounts] ([accountstatus]) TO [@roleName]'
SET @CMD = REPLACE(@CMD, '@roleName', @roleName)
PRINT (@CMD)
EXEC (@CMD)

---> Add role to the user
EXECUTE sp_AddRoleMember @roleName, @userName

END
