/* New view for purchase order Accounts - TProvost 2016-07-01 */
/* DO NOT RENAME COLUMNS AND DO NOT REORDER COLUMNS defined in this view - or if you do make sure you update the Transaction Broker file accordingly */
/* 
 * Last Modification: 
 *  	2018-01-03 TProvost: Replaced CRLF and TAB with a space in the item description to prevent the Excel issue
 */

IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'new_purchaseorderaccounts')
	EXECUTE('CREATE VIEW new_purchaseorderaccounts AS SELECT * FROM purchaseorder WHERE id = 0')
GO

ALTER VIEW new_purchaseorderaccounts 
AS
SELECT p.id AS poId
	,p.pono AS poNo
	,p.clientref AS supplierRef
	-- Company payTo
	,c.coid AS payTo_id
	,c.coname AS payTo_name
	,c.fiscalidentifier AS payTo_fiscalId
    ,(ISNULL(a.addr1, '') + CHAR(32) + ISNULL(a.addr2, '') + CHAR(32) + ISNULL(a.addr3, '')) as payTo_address
	,a.town AS payTo_city
	,a.postcode AS payTo_postCode
	,a.county AS payTo_county
	,act.countrycode AS payTo_countryCode
	,(con.firstname + CHAR(32) + con.lastname) AS payTo_contact
	,con.email AS payTo_email
	-- Address delTo
	,(ISNULL(ra.addr1, '') + CHAR(32) + ISNULL(ra.addr2, '') + CHAR(32) + ISNULL(ra.addr3, '')) as delTo_address
    ,ra.town AS delTo_city
    ,ra.postcode AS delTo_postCode
    ,ra.county AS delTo_county
	,ract.countrycode AS delTo_countryCode
	,(bcon.firstname + CHAR(32) + bcon.lastname) AS delTo_contact
	,bcon.email AS delTo_email
	-- Others data on pruchase order
	,p.accountstatus AS accountStatus
	,CONVERT(VARCHAR(10), GETDATE(), 104) AS currentDate
	,p.regdate AS regDate
	,p.issuedate AS issueDate
	,sc.currencycode AS currencyCode
	,p.rate AS currencyRate
	,p.totalcost AS totalAmountExclTax
	,p.finalcost AS totalAmountInclTax
	-- Purchase order Lines
	,pi.itemno AS itemNo
	,IIF(ltrim(rtrim(pi.description)) = '' OR pi.description = NULL, 
		CASE
			WHEN pi.costtypeid = 1 THEN 'For Calibration of '
			WHEN pi.costtypeid = 2 THEN 'For Adjustment of '
			WHEN pi.costtypeid = 3 THEN 'For Repair of '
			WHEN pi.costtypeid = 4 THEN 'For Purchase of '
			WHEN pi.costtypeid = 5 THEN 'For Inspection of '
			ELSE ' '
			END + 'Item ' + CAST(ji.itemno AS VARCHAR) +  ' on job ' + j.jobno
			,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(pi.description, CHAR(13) + CHAR(10), ' '), CHAR(10) + CHAR(13), ' '), CHAR(13), ' '), CHAR(10), ' '), CHAR(9), ' ')
			) AS description
	,'Item' AS type
	,nc.code AS nominalCode
	,nc.title AS nominalDesc	
	,pi.quantity AS quantity
	,pi.totalcost AS unitPrice
	,p.vatrate AS vatRate
	,'' AS vatZone
	,pi.discountrate AS discountRate
	,pi.discountvalue AS discountValue
	,pi.finalcost AS amountExclTax
	,CASE 
		WHEN p.vatrate IS NULL
			THEN pi.finalcost
		ELSE CAST(pi.finalcost + (pi.finalcost * p.vatrate / 100) AS NUMERIC(10,2))
		END AS amountInclTax
	,pi_subdiv.analyticalcenter	
	,pi.deliverydate AS deliveryDate
	,j.jobno AS jobNo
	,si.invoicenumber AS invoiceNo
	,si.invoicedate AS invoiceDate
	,si.paymentdate AS paymentDate
	,pt.code AS paymentTermsCode
	,pm.code AS paymentModeCode
	,bus.coid AS busId
	,bus.coname AS busName	
FROM purchaseorder p 
	INNER JOIN contact AS con ON con.personid = p.personid
	INNER JOIN subdiv AS s ON s.subdivid = con.subdivid
	INNER JOIN company AS c ON c.coid = s.coid
	INNER JOIN supportedcurrency AS sc ON sc.currencyid = p.currencyid
	INNER JOIN company AS bus ON bus.coid = p.orgid
	INNER JOIN dbo.address AS a ON a.addrid = p.addressid
	INNER JOIN dbo.country AS act ON act.countryid = a.countryid
	INNER JOIN dbo.address AS ra ON ra.addrid = p.returnaddressid
    INNER JOIN country AS ract ON ract.countryid = ra.countryid
	LEFT OUTER JOIN contact AS bcon on bcon.personid = p.buspersonid
	LEFT OUTER JOIN purchaseorderitem AS pi ON pi.orderid = p.id
	LEFT OUTER JOIN subdiv AS pi_subdiv ON pi_subdiv.subdivid = pi.orgid	
	LEFT OUTER JOIN purchaseorderjobitem AS pji ON pji.poitemid = pi.id
	LEFT OUTER JOIN jobitem AS ji ON ji.jobitemid = pji.jobitemid
	LEFT OUTER JOIN job AS j ON j.jobid = ji.jobid
	LEFT OUTER JOIN nominalcode AS nc ON nc.id = pi.nominalid
	LEFT OUTER JOIN supplierinvoice AS si ON si.id = pi.supplierinvoiceid
	LEFT OUTER JOIN paymentterms AS pt ON pt.id = si.paymenttermsid
	LEFT OUTER JOIN companysettingsforallocatedcompany AS csfac ON csfac.companyid = c.coid AND csfac.orgid = p.orgid
	LEFT OUTER JOIN paymentmode AS pm ON pm.id = csfac.paymentmodeid
WHERE p.accountstatus = 'P'
    AND pi.cancelled = 0
GO

/* Redefine rights for supplier to access and update view */
BEGIN

DECLARE @userName sysname = 'COMUNICA'
DECLARE @roleName sysname = 'db_accountinginterface'
DECLARE @cmd varchar(4000)

---> Define securables for database role
SET @CMD = 'GRANT SELECT ON [dbo].[new_purchaseorderaccounts] TO [@roleName]'
SET @CMD = REPLACE(@CMD, '@roleName', @roleName)
PRINT (@CMD)
EXEC (@CMD)

SET @CMD = 'GRANT UPDATE ON [dbo].[new_purchaseorderaccounts] ([accountstatus]) TO [@roleName]'
SET @CMD = REPLACE(@CMD, '@roleName', @roleName)
PRINT (@CMD)
EXEC (@CMD)

---> Add role to the user
EXECUTE sp_AddRoleMember @roleName, @userName

END
