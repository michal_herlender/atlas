-- The following views are used to aid calculations on the job progress page


-- view that lists all job items currently active on the system for business companies (i.e. Antech or MD)

CREATE OR REPLACE VIEW activebusinessjobitems as
select ji.jobitemid, ji.duedate
from job j, contact con, subdiv sub, company comp, corole, jobitem ji, itemstate i
where ji.jobid = j.jobid and j.personid = con.personid
and con.subdivid = sub.subdivid
and sub.coid = comp.coid
and comp.corole = corole.coroleid
and i.stateid = ji.stateid
and i.active = true
and corole.corole = 'Business';


-- view that lists all job items currently active on the system for client companies

CREATE OR REPLACE VIEW activeclientjobitems as
select ji.jobitemid, ji.duedate
from job j, contact con, subdiv sub, company comp, corole, jobitem ji, itemstate i
where ji.jobid = j.jobid and j.personid = con.personid
and con.subdivid = sub.subdivid
and sub.coid = comp.coid
and comp.corole = corole.coroleid
and i.stateid = ji.stateid
and i.active = true
and corole.corole = 'Client';