/* Add Sample Nominal Codes for Spain (demo systems only) - Author Galen Beck 2016-03-09 */
/* Meant to run just once */

BEGIN TRAN 

USE [SPAIN];
GO

INSERT INTO [Spain].[dbo].[nominalcode]
           ([code]
           ,[description]
           ,[ledger]
           ,[shortdesc]
           ,[title])
     VALUES
           ('CD_SALES', 'Generic Sales Nominal Code', 'SALES_LEDGER', 'Generic Sales Nominal Code', 'Generic Sales Nominal Code'),
           ('CD_PURCHASE', 'Generic Purchase Nominal Code', 'PURCHASE_LEDGER', 'Generic Purchase Nominal Code', 'Generic Purchase Nominal Code'),
           ('CD_UTILITY', 'Generic Utility Nominal Code', 'UTILITY', 'Generic Utility Nominal Code', 'Generic Utility Nominal Code'),
           ('CD_CAPITAL', 'Generic Capital Nominal Code', 'CAPITAL', 'Generic Capital Nominal Code', 'Generic Capital Nominal Code');

GO

COMMIT TRAN