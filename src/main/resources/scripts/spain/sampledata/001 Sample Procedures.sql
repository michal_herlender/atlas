/* Add Sample Procedures for Spain (demo systems only) - Author Galen Beck 2016-03-03 */
/* Meant to run just once, will throw key constraint violations if duplicates attempted on multiple runs */

BEGIN TRAN 

USE [SPAIN];
GO

SET IDENTITY_INSERT [procedureheading] ON
GO

INSERT INTO [procedureheading] ([id], [heading])
VALUES 
	(102, 'Bilbao Flow')
	,(103, 'Vitoria Mass' )
	,(104, 'Zaragoza Dimensional' )
	,(105, 'Madrid Temperature' )
	,(106, 'Bilbao Electrical' )
	,(107, 'Vitoria Electrical' )
	,(108, 'Zaragoza Electrical' )
	,(109, 'Madrid Electrical' )
GO

SET IDENTITY_INSERT [procedureheading] OFF
GO

INSERT INTO [Spain].[dbo].[procs]
           ([active]
           ,[calprocxmlid]
           ,[description]
           ,[name]
           ,[reference]
           ,[headingid]
           ,[accreditedlabid]
           ,[orgid])
VALUES
	(1,'','Procedure for Flow in Bilbao','Bilbao Flow','10000',102,1,6642),
	(1,'','Mass procedure for Vitoria','Vitoria Mass','10010',103,1,6643),    
	(1,'','Dimensional procedure for Zaragoza','Zaragoza Dimensional','10020',104,1,6644),    
	(1,'','Temperature procedure for Madrid','Madrid Temperature','10030',105,1,6645),    
	(1,'','Electrical procedure for Bilbao','Bilbao Electrical','10001',106,1,6642),    
	(1,'','Electrical procedure for Vitoria','Vitoria Electrical','10011',107,1,6643),    
	(1,'','Electrical procedure for Zaragoza','Zaragoza Electrical','10021',108,1,6644),    
	(1,'','Electrical procedure for Madrid','Madrid Electrical','10031',109,1,6645)
GO
	
ROLLBACK TRAN