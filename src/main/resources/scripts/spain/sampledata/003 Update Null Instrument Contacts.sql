/*
 * Script to update null instrument contacts as found on spain database 2016-03-26 GB
 * 6578 - Basque - Fran Manas - 17281
 * 6579 - ITM Madrid - Jose Antonio Canadas - 17372
 * 6690 - France - DBD - 17286
 * 6692 - Italy - Standard - 17634 
 */
BEGIN TRAN;

USE [spain]
GO

UPDATE [instrument] SET [personid] = 17281 WHERE personid IS NULL AND coid = 6578;
UPDATE [instrument] SET [personid] = 17372 WHERE personid IS NULL AND coid = 6579;
UPDATE [instrument] SET [personid] = 17286 WHERE personid IS NULL AND coid = 6690;
UPDATE [instrument] SET [personid] = 17634 WHERE personid IS NULL AND coid = 6692;
GO

ROLLBACK TRAN;