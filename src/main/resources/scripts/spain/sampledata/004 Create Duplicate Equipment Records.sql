/*
 * Script to create 2000 duplicate recrods under GE Heathcare; used for recall report testing
 * GB 2016-04-29
 */

USE [spain]
GO

INSERT INTO [dbo].[instrument]
           ([addedon]
           ,[calfrequency]
           ,[calibrationStandard]
           ,[calsintervalon]
           ,[firmwareaccesscode]
           ,[recalldate]
           ,[plantno]
           ,[recallednotreceived]
           ,[requirement]
           ,[serialno]
           ,[addressid]
           ,[addedby]
           ,[coid]
           ,[personid]
           ,[mfrid]
           ,[modelid]
           ,[statusid]
           ,[storageid]
           ,[usageid]
           ,[customerdescription]
           ,[formerbarcode]
           ,[formerlocaldescription]
           ,[formername]
           ,[measurementtype])
     VALUES
           ('2016-04-26'
           ,12
           ,0
           ,0
           ,''
           ,'2015-04-26'
           ,'DUPLICATEPN'
           ,0
           ,''
           ,'DUPLICATESN'
           ,13026
           ,8740
           ,7150
           ,18892
           ,1
           ,43551
           ,20
           ,1
           ,3
           ,'DOSIMETRO'
           ,'DUPLICATEFBC'
           ,'DOSIMETRO'
           ,'SGestion'
           ,'DIRECT')
GO
