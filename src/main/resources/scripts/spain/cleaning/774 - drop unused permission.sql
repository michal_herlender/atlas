USE [atlas]

begin TRAN

delete from permission where permission = 'MODEL_IMPORT';
GO

insert into dbversion(version) values(774);

commit TRAN
