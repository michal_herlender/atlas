USE [atlas]

BEGIN TRAN

    declare @groupId int;
    declare @settingId int;
    
    select @groupId = g.id
    	from systemdefaultgroup g
    	where g.dname = 'Link - Adveso';
    	
	INSERT INTO systemdefault (defaultdatatype,defaultdescription,defaultname,defaultvalue,endd,[start],groupid,groupwide)
		VALUES ('date','Link to Adveso after date','Link to Adveso after','01.01.2020',0,0,@groupId,1);
		
	select @settingId = max(defaultid) from systemdefault;
    
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@settingId,'en_GB','Link to Adveso after date');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@settingId,'es_ES','Enlace a Adveso despu�s de la fecha');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@settingId,'fr_FR','Lier � Adveso apr�s la date');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@settingId,'de_DE','Link zu Adveso nach Datum');
		
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@settingId,'en_GB','Send notifications and activities to Adveso only after the specified date') 
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@settingId,'es_ES','Enviar notificaciones y actividades a Adveso solo despu�s de la fecha especificada') 
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@settingId,'fr_FR','Envoyer les notifications et les activit�s � Adveso uniquement apr�s la date sp�cifi�e');
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@settingId,'de_DE','Senden Sie Benachrichtigungen und Aktivit�ten erst nach dem angegebenen Datum an Adveso');
		
	INSERT INTO systemdefaultcorole (coroleid,defaultid)
		VALUES (1,@settingId);
		
	INSERT INTO systemdefaultscope ([scope],defaultid)
		VALUES (2,@settingId);
	

	INSERT INTO dbversion(version) VALUES(665);

COMMIT TRAN