-- Workflow update to manage costing creation / closeout on the "old job item" after a "new job item created for repair" situation.
-- This is to fix the issue where currently, the "old job item" is treated as if it is inhouse. (workflow v21).

USE [atlas]
GO

BEGIN TRAN

DECLARE @lookupinstance1 int;
DECLARE @lookupinstance2 int;
DECLARE @itemstate1 int;
DECLARE @itemstate2 int;
DECLARE @itemstate3 int;
DECLARE @itemstate4 int;
DECLARE @os1 int;
DECLARE @os2 int;
DECLARE @os3 int;
DECLARE @os4 int;

-- create statues and activities
INSERT INTO dbo.itemstate ([type],active,description,retired)
	VALUES ('workstatus',1,'Service continued on new job item, awaiting confirmation costing on old job item',0);
select @itemstate1 = max(stateid) from dbo.itemstate;

INSERT INTO dbo.itemstate ([type],active,description,retired)
	VALUES ('workactivity',1,'Confirmation costing issued to client for old job item',0);
select @itemstate2 = max(stateid) from dbo.itemstate;

INSERT INTO dbo.itemstate ([type],active,description,retired)
	VALUES ('workactivity',1,'Final job costing issued on old job item',0);
select @itemstate3 = max(stateid) from dbo.itemstate;

INSERT INTO dbo.itemstate ([type],active,description,retired)
	VALUES ('workstatus',0,'Service continued on new job item, old job item completed',0);
select @itemstate4 = max(stateid) from dbo.itemstate;

-- rename status 4122
UPDATE dbo.itemstate SET description='Service continued on new job item, awaiting final costing on old job item'
	WHERE stateid=4122;
UPDATE dbo.itemstatetranslation
	SET [translation]='Der Service wurde für neue Auftragsposition fortgesetzt und wartet auf die endgültige Kalkulation der alten Auftragsposition'
	WHERE stateid=4122 AND locale='de_DE';
UPDATE dbo.itemstatetranslation
	SET [translation]='Service continued on new job item, awaiting final costing on old job item'
	WHERE stateid=4122 AND locale='en_GB';
UPDATE dbo.itemstatetranslation
	SET [translation]='El servicio continuó en el nuevo elemento de trabajo, a la espera de un costo final en el elemento de trabajo anterior'
	WHERE stateid=4122 AND locale='es_ES';
UPDATE dbo.itemstatetranslation
	SET [translation]='Le service a continué sur le nouvel jobitem, en attendant la détermination du pricing final sur l''ancien jobitem'
	WHERE stateid=4122 AND locale='fr_FR';


-- create lookup 'Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting (repair after failure report)'
INSERT INTO dbo.lookupinstance (lookup,lookupfunction)
	VALUES ('Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting (repair after failure report)',25);
select @lookupinstance1 = max(id) from dbo.lookupinstance;

-- create lookup 'Lookup_ConfirmationCostRequired (repair after failure report)'
INSERT INTO dbo.lookupinstance (lookup,lookupfunction)
	VALUES ('Lookup_ConfirmationCostRequired (repair after failure report)',9);
select @lookupinstance2 = max(id) from dbo.lookupinstance;


-- create LR and OS for lookup 1
-- yes
INSERT INTO dbo.outcomestatus (defaultoutcome,lookupid)
	VALUES (1,@lookupinstance2);
select @os1 = max(id) from dbo.outcomestatus;
-- no
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)
	VALUES (0,4122);
select @os2 = max(id) from dbo.outcomestatus;

-- yes
INSERT INTO atlas.dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lookupinstance1,@os1,0);
-- no
INSERT INTO atlas.dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lookupinstance1,@os2,1);

-- create LR and OS for lookup 2
-- yes
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)
	VALUES (1,@itemstate1);
select @os3 = max(id) from dbo.outcomestatus;
-- no
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)
	VALUES (0,@itemstate4);
select @os4 = max(id) from dbo.outcomestatus;

-- yes
INSERT INTO atlas.dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lookupinstance2,@os3,0);
-- no
INSERT INTO atlas.dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lookupinstance2,@os4,1);

	
-- link statues to activities
INSERT INTO dbo.nextactivity (manualentryallowed,activityid,statusid)
	VALUES (0,@itemstate2,@itemstate1);
INSERT INTO dbo.nextactivity (manualentryallowed,activityid,statusid)
	VALUES (0,@itemstate3,4122);


-- connect the 'NO' LR of the lookup isOnRepair to new lookup instance
UPDATE dbo.outcomestatus SET lookupid=@lookupinstance1,statusid=NULL
	WHERE id=356;
 

-- connect activities to complete status
INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,statusid)
	VALUES (0,@itemstate3,@itemstate4);
INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,statusid)
	VALUES (0,@itemstate2 ,@itemstate4);
	
-- delete old one
DELETE FROM dbo.hookactivity
	WHERE id=108;	
	
-- connect to the activities for the hook
INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@itemstate3,4,4122);
INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@itemstate2,4,@itemstate1);

-- new statues translations
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate1,'de_DE','Der Service wurde für neue Auftragsposition fortgesetzt und die Bestätigungskosten für alte Auftragsposition wurden abgewartet');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate1,'en_GB','Service continued on new job item, awaiting confirmation costing on old job item') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate1,'es_ES','El servicio continuó en el nuevo elemento de trabajo, a la espera de la confirmación del costo del elemento de trabajo anterior') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate1,'fr_FR','Le service a continué sur le nouvel jobitem, en attente de confirmation du coût de l''ancien jobitem') ;

INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate2,'de_DE','Bestätigungskalkulation für alte Auftragsposition an Mandanten ausgestellt') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate2,'en_GB','Confirmation costing issued to client for old job item') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate2,'es_ES','Coste de confirmación emitido al cliente para el artículo de trabajo anterior') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate2,'fr_FR','Calcul du coût de confirmation envoyé au client pour l''ancien jobitem') ;
	
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate3,'de_DE','Endgültige Auftragskalkulation für alte Auftragsposition') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate3,'en_GB','Final job costing issued on old job item') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate3,'es_ES','Costeo final del trabajo emitido en el artículo de trabajo anterior') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate3,'fr_FR','Calcul final du coût du travail émis sur l''ancien jobitem') ;
	
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate4,'de_DE','Die Wartung wurde für neue Auftragsposition fortgesetzt, die alte Auftragsposition wurde abgeschlossen') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate4,'en_GB','Service continued on new job item, old job item completed') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate4,'es_ES','El servicio continuó en el nuevo elemento de trabajo, el elemento de trabajo antiguo completado') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstate4,'fr_FR','Service continu sur le nouvel jobitem, ancien jobitem terminé') ;

INSERT INTO dbversion(version) VALUES(492);

COMMIT TRAN