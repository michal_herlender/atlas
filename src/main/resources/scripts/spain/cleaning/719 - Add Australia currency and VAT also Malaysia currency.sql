-- Creates VAT code 'V' for Australia
-- Adds currency for Australia and Malaysia
-- Note, Malaysia VAT already exists at 6%, SG at 7%

USE [atlas]
GO

BEGIN TRAN

INSERT INTO [dbo].[supportedcurrency]
           ([accountscode]
           ,[currencycode]
           ,[currencyersymbol]
           ,[currencyname]
           ,[currencysymbol]
           ,[exchangerate]
           ,[orderby]
           ,[ratelastset]
           ,[rateset]
           ,[ratelastsetby]
           ,[majorpluralname]
           ,[majorsingularname]
           ,[minorexponent]
           ,[minorpluralname]
           ,[minorsingularname]
           ,[minorunit]
           ,[symbolposition])
     VALUES
           ('AUD'
           ,'AUD'
           ,'A$'
           ,'Australian dollar'
           ,'A$'
           ,1.62
           ,14
           ,'2020-11-02'
           ,1
           ,17280
           ,'dollars'
           ,'dollar'
           ,2
           ,'cents'
           ,'cent'
           ,1
           ,'PREFIX_NO_SPACE'),
		   ('MYR'
           ,'MYR'
           ,'RM'
           ,'Malaysian ringgit'
           ,'RM'
           ,4.83
           ,15
           ,'2020-11-02'
           ,1
           ,17280
           ,'ringgit'
           ,'ringgit'
           ,2
           ,'sen'
           ,'sen'
           ,1
           ,'PREFIX_NO_SPACE')
GO

DECLARE @countryIdAU int;
select @countryIdAU = country.countryid from country where countrycode = 'AU';

INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid],[lastModified],[lastModifiedBy])
     VALUES
           ('V','Australia',10.00,0,@countryIdAU,'2020-11-02',17280);

INSERT INTO dbversion(version) VALUES(719);

GO

COMMIT TRAN