UPDATE lookupresult SET outcomestatusid = 34
WHERE lookupresult.lookupid = 65 AND resultmessage = 0

INSERT INTO lookupresult (lookupid, outcomestatusid, resultmessage)
SELECT lookupinstance.id, outcomestatus.id, 2
FROM lookupinstance, outcomestatus
LEFT JOIN itemstate ON outcomestatus.statusid = itemstate.stateid
WHERE lookupfunction = 39 AND itemstate.description='Awaiting internal delivery note'