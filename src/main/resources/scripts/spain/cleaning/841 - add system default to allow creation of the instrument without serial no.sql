BEGIN TRAN
set identity_insert systemdefault ON;
INSERT  into systemdefault (defaultid,defaultdatatype,defaultdescription,defaultname,defaultvalue,groupid,groupwide,endd,start)
values(70,'boolean','Defines if a business company can create an instrument without Serial Number','Create Instrument without Serial Number',
                     'true',14,1,0,0);

set identity_insert systemdefault OFF;

insert into systemdefaultcorole (coroleid,defaultid)
  select  5,sd.defaultid from systemdefault sd where sd.defaultid = 70 ;

insert into systemdefaultscope (scope,defaultid)
  select  0,sd.defaultid from systemdefault sd where sd.defaultid = 70 ;
insert into systemdefaultnametranslation
  select sd.defaultid,'en_GB','Create Instrument without Serial Number'
  from systemdefault sd where sd.defaultid = 70;
insert into systemdefaultdescriptiontranslation
  select sd.defaultid,'en_GB','Defines if a business company can create an instrument without serial number '
  from systemdefault sd where sd.defaultid = 70;



INSERT INTO dbversion(version) VALUES(841);

COMMIT TRAN
