USE atlas
GO

BEGIN TRAN

	ALTER TABLE dbo.instrument ADD lastgsoid INT NULL;
	
	ALTER TABLE dbo.instrument ADD CONSTRAINT FK_instrument_last_gso 
	FOREIGN KEY (lastgsoid) REFERENCES dbo.generalserviceoperation(id);
	
	insert into dbversion values(740)

COMMIT TRAN