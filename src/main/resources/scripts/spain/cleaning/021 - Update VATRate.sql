/* Updates VATRates to use new country allocation GB 2016-03-09 */

/*
	VatRateType 'type' field:
	0 = COUNTRY_DEFAULT		// Indicates the default internal rate for a country
	1 = COUNTRY_SPECIAL		// Indicates an optional special rate for a country (subregion) 
	2 = EUROZONE_INTERNAL	// Indicates a generic rate used from an EU country to another EU country
	3 = EUROZONE_EXTERNAL	// Indicates a generic rate used from an EU country to outside the EU
	4 = NON_EUROZONE		// Indicates a rate used for business companies outside the EU
	
	162 = Spain country
*/

BEGIN TRAN 

USE [SPAIN];
GO

UPDATE [dbo].[vatrate]
   SET [description] = 'Canarias' , [rate] = 0.00, [type] = 1, [countryid] = 162
 WHERE vatcode='0'
GO

UPDATE [dbo].[vatrate]
   SET [description] = 'Spain' ,[rate] = 21.00 ,[type] = 0 ,[countryid] = 162
 WHERE vatcode='1'
GO

UPDATE [dbo].[vatrate]
   SET [description] = 'Eurozone Internal' ,[rate] = 0.00 ,[type] = 2 ,[countryid] = null
 WHERE vatcode='2'
GO

UPDATE [dbo].[vatrate]
   SET [description] = 'Eurozone Export' ,[rate] = 0.00 ,[type] = 3 ,[countryid] = null
 WHERE vatcode='3'
GO

INSERT INTO [Spain].[dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid])
     VALUES
           ('4','Non-Eurozone',0.00,4,null)
GO

ROLLBACK TRAN