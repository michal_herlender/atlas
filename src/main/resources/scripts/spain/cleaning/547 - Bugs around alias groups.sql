USE [atlas]
GO

BEGIN TRAN 
   
ALTER TABLE dbo.aliasgroup ADD defaultBusinessCompany bit null;
ALTER TABLE dbo.aliasgroup ADD orgid int null;

GO

UPDATE dbo.aliasgroup SET orgid =(SELECT c.coid from dbo.company c where c.coname='TRESCAL SAS');
UPDATE dbo.aliasgroup SET defaultBusinessCompany = 0;

ALTER TABLE dbo.aliasgroup ALTER COLUMN orgid int NOT NULL;
ALTER TABLE dbo.aliasgroup ADD CONSTRAINT FK_Organisation_AliasGroup foreign key (orgid) references dbo.company;

ALTER TABLE dbo.aliasgroup DROP CONSTRAINT FK_aliasgroup_defaultBuisnessCompany;
ALTER TABLE dbo.aliasgroup DROP column defaultBuisnessCompanyid;

INSERT INTO dbversion(version) VALUES(547);

COMMIT TRAN
  