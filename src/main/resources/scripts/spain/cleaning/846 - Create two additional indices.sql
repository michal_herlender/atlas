-- From query regression / resource analyzer on production 2021-10-13

BEGIN TRAN

CREATE NONCLUSTERED INDEX [IDX_images_type_jobitemid]
ON [dbo].[images] ([type],[jobitemid])
INCLUDE ([lastModified],[addedon],[description],[fileName],[rfilepath],[lastModifiedBy],[personid])

CREATE NONCLUSTERED INDEX [IDX_email_entityid_component]
ON [dbo].[email] ([entityid],[component])

INSERT INTO dbversion values (846);

COMMIT TRAN