-- Creation of a "general service operation" that would handle cases such as the functional verification (VBF for Thales) 
USE [atlas]
GO

BEGIN TRAN

DECLARE @itemstatus1 int;
DECLARE @itemstatus2 int;
DECLARE @itemactivity1 int;
DECLARE @itemactivity2 int;
DECLARE @itemactivity3 int;
DECLARE @os1 int;
DECLARE @os2 int;
DECLARE @os3 int;
DECLARE @os4 int;
DECLARE @os5 int;
DECLARE @os6 int;
DECLARE @hk1 int;
DECLARE @hk2 int;
DECLARE @hk3 int;
DECLARE @lu1 int;
DECLARE @lu2 int;
DECLARE @lu_104 int;
DECLARE @lu_90 int;

-- select lookup 90 
SELECT @lu_90 = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_NextWorkRequirementByJobCompany (before lab calibration)';

-- create statues and activities
INSERT INTO dbo.itemstate ([type],active,description,retired)
	VALUES ('workstatus',1,'Awaiting general service operation',0);
SELECT @itemstatus1 = MAX(stateid) FROM dbo.itemstate;

INSERT INTO dbo.itemstate ([type],active,description,retired)
	VALUES ('workactivity',1,'General service operation',0);
SELECT @itemactivity1 = MAX(stateid) FROM dbo.itemstate;

INSERT INTO dbo.itemstate ([type],active,description,retired)
	VALUES ('workstatus',1,'Awaiting document upload for general operation',0);
SELECT @itemstatus2 = MAX(stateid) FROM dbo.itemstate;

INSERT INTO dbo.itemstate ([type],active,description,retired,lookupid)
	VALUES ('workactivity',1,'Document uploaded for general operation',0,@lu_90);
SELECT @itemactivity2 = MAX(stateid) FROM dbo.itemstate;

INSERT INTO dbo.itemstate ([type],active,description,retired,lookupid)
	VALUES ('workactivity',1,'Document not required for general operation',0,@lu_90);
SELECT @itemactivity3 = MAX(stateid) FROM dbo.itemstate;

-- add new LR and OS for Lookup 104 (type of next wr) for the new type : GENERAL_SERVICE_OPERATION
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)
	VALUES (0,@itemstatus1);
SELECT @os1 = MAX(id) FROM dbo.outcomestatus;
SELECT @lu_104 = id FROM lookupinstance WHERE lookup = 'Lookup_TypeOfNextWorkRequirement';

INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lu_104, @os1, 5);

-- link itemstatus1 and itemactivity1
INSERT INTO dbo.nextactivity (manualentryallowed,activityid,statusid)
	VALUES (0, @itemactivity1, @itemstatus1);
	
-- link itemstatus2 with itemactivity2 and itemactivity3
INSERT INTO dbo.nextactivity (manualentryallowed,activityid,statusid)
	VALUES (0, @itemactivity2, @itemstatus2);
	
INSERT INTO dbo.nextactivity (manualentryallowed,activityid,statusid)
	VALUES (1, @itemactivity3, @itemstatus2);


-- create lookups
INSERT INTO dbo.lookupinstance (lookup,lookupfunction)
	VALUES ('Lookup_GeneralServiceOperationResult',54);
SELECT @lu1 = MAX(id) FROM dbo.lookupinstance;
INSERT INTO dbo.lookupinstance (lookup,lookupfunction)
	VALUES ('Lookup_HasDocumentForMostRecentGeneralOperation',55);
SELECT @lu2 = MAX(id) FROM dbo.lookupinstance;


-- link GSO activity to result lookup
UPDATE dbo.itemstate SET lookupid=@lu1 WHERE stateid=@itemactivity1;

---- create LR and OS for lookup 1
-- COMPLETED
INSERT INTO dbo.outcomestatus (defaultoutcome, activityid, lookupid)
	VALUES (1, @itemactivity1, @lu2);
SELECT @os1 = MAX(id) FROM dbo.outcomestatus;
-- CANCELLED

INSERT INTO dbo.outcomestatus (defaultoutcome, activityid, lookupid)
	VALUES (0, @itemactivity1, @lu_90);
SELECT @os2 = MAX(id) FROM dbo.outcomestatus;

-- COMPLETED
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lu1,@os1,0);
-- CANCELLED
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lu1,@os2,1);
	
	
---- create LR and OS for lookup 2
-- yes
INSERT INTO dbo.outcomestatus (defaultoutcome,lookupid)
	VALUES (1,@lu_90);
SELECT @os3 = MAX(id) FROM dbo.outcomestatus;
-- no
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)
	VALUES (0,@itemstatus2);
SELECT @os4 = MAX(id) FROM dbo.outcomestatus;

-- yes
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lu2,@os3,0);
-- no
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lu2,@os4,1);

	

-- create "start-general-operation" hook
INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,0,1,0,'start-general-operation',@itemactivity1);
SELECT @hk1 = MAX(id) FROM dbo.hook;

-- create "complete-general-operation" hook
INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,0,0,1,'complete-general-operation',@itemactivity1);
SELECT @hk2 = MAX(id) FROM dbo.hook;

-- create "upload-general-document" hook
INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,1,1,1,'upload-general-document',@itemactivity2);
SELECT @hk3 = MAX(id) FROM dbo.hook;

-- hook activities
INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,0,@itemactivity1,@hk1,@itemstatus1);
INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (0,1,@itemactivity1,@hk2,@itemactivity1);
	

-- translations
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus1,'de_DE','Warten auf allgemeinen Servicebetrieb');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus1,'en_GB','Awaiting general service operation');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus1,'es_ES','En espera de operación de servicio general');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus1,'fr_FR','En attente d''operation de service general');
	
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity1,'de_DE','Allgemeiner Servicebetrieb');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity1,'en_GB','General service operation');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity1,'es_ES','Operación de servicio general');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity1,'fr_FR','Opération de service général');

INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus2,'de_DE','Warten auf das Hochladen des Dokuments für den allgemeinen Betrieb');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus2,'en_GB','Awaiting document upload for general operation');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus2,'es_ES','En espera de carga de documentos para operación general');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus2,'fr_FR','En attente de téléchargement du document pour l''operation de service général');
		
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity2,'de_DE','Dokument zur allgemeinen Verwendung hochgeladen');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity2,'en_GB','Document uploaded for general operation');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity2,'es_ES','Documento cargado para operación general');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity2,'fr_FR','Document chargé pour opération générale');
		
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity3,'de_DE','Dokument für den allgemeinen Betrieb nicht erforderlich');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity3,'en_GB','Document not required for general operation');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity3,'es_ES','Documento no requerido para operación general');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemactivity3,'fr_FR','Document non requis pour pour l''operation de service général');


INSERT INTO dbversion(version) VALUES(515);

COMMIT TRAN