-- Script to populate WorkRequirementType field
-- Should be run after application startup (which creates new field)
-- Author Galen Beck - 2016-10-24
BEGIN TRAN

USE [spain]
GO

UPDATE [dbo].[procs]
   SET [reqtype] = 'THIRD_PARTY'
 WHERE ([reqtype] IS null)
   AND ([name] LIKE '%EXT%');

GO

UPDATE [dbo].[procs]
   SET [reqtype] = 'CALIBRATION'
 WHERE ([reqtype] IS null)
   AND ([name] NOT LIKE '%EXT%')
   AND (name LIKE '%en laboratorio%');

GO

UPDATE [dbo].[procs]
   SET [reqtype] = 'ONSITE'
 WHERE ([reqtype] IS null)
   AND ([name] NOT LIKE '%EXT%')
   AND ([name] NOT LIKE '%EXT%')
   AND (name LIKE '%in situ%');

GO

UPDATE [dbo].[procs]
   SET [reqtype] = 'CALIBRATION'
 WHERE ([reqtype] IS null)
   AND ([name] NOT LIKE '%EXT%')
   AND ([name] NOT LIKE '%EXT%') 
   AND (name LIKE '%Generico%' OR name LIKE '%Genercio%');

GO

UPDATE [dbo].[procs]
   SET [reqtype] = ''
 WHERE ([reqtype] IS null)
   AND ([name] NOT LIKE '%EXT%')

GO

SELECT [id]
      ,[name]
      ,[reference]
      ,[orgid]
      ,[reqtype]
  FROM [dbo].[procs] WHERE reqtype IS NULL;

GO
-- Final results should be null (all assigned) on production, or update sample data as needed
 
COMMIT TRAN;