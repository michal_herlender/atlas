﻿-- 

USE [atlas]
GO

BEGIN TRAN

-- Remove obsolete, unused fields

ALTER TABLE [supportedcurrency] DROP COLUMN [bankaccountno];
ALTER TABLE [supportedcurrency] DROP COLUMN [iban];
ALTER TABLE [supportedcurrency] DROP COLUMN [banksortcode];

-- Add new currency fields

alter table [supportedcurrency]
    add majorpluralname nvarchar(50);

alter table [supportedcurrency]
    add majorsingularname nvarchar(50);

alter table [supportedcurrency]
    add minorexponent int;

alter table [supportedcurrency]
    add minorpluralname nvarchar(50);

alter table [supportedcurrency]
    add minorsingularname nvarchar(50);

alter table [supportedcurrency]
    add minorunit bit;

GO

-- Only Tunisia Dinar is different for existing currencies
UPDATE [dbo].[supportedcurrency]
   SET [minorexponent] = '2',
       [minorunit] = 1;

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'pounds',
       [majorsingularname] = 'pound',
	   [minorpluralname] = 'pence',
	   [minorsingularname] = 'pence',
	   [currencyname] = 'Pound Sterling'
   WHERE currencycode = 'GBP'

UPDATE [dbo].[supportedcurrency]
   SET [currencyname] = 'United States Dollar'
   WHERE currencycode = 'USD'

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'dollars',
       [majorsingularname] = 'dollar',
	   [minorpluralname] = 'cents',
	   [minorsingularname] = 'cent'
   WHERE currencycode in ('USD', 'CAD', 'SGD')

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'euros',
       [majorsingularname] = 'euro',
	   [minorpluralname] = 'cents',
	   [minorsingularname] = 'cent'
   WHERE currencycode in ('EUR')

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'dinars',
       [majorsingularname] = 'dinar',
	   [minorpluralname] = 'millimes',
	   [minorsingularname] = 'millime',
	   [minorexponent] = '3'
   WHERE currencycode = 'TND'

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'dirhams',
       [majorsingularname] = 'dirham',
	   [minorpluralname] = 'centimes',
	   [minorsingularname] = 'centime'
   WHERE currencycode = 'MAD'

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'francs',
       [majorsingularname] = 'franc',
	   [minorpluralname] = 'centimes',
	   [minorsingularname] = 'centime'
   WHERE currencycode = 'CHF'

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'leu',
       [majorsingularname] = 'lei',
	   [minorpluralname] = 'bani',
	   [minorsingularname] = 'ban'
   WHERE currencycode = 'RON'

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'reals',
       [majorsingularname] = 'real',
	   [minorpluralname] = 'centavos',
	   [minorsingularname] = 'centavo'
   WHERE currencycode = 'BRL'

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'koruna',
       [majorsingularname] = 'koruna',
	   [minorpluralname] = 'haléřů',
	   [minorsingularname] = 'haléř'
   WHERE currencycode = 'CZK'

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'kroner',
       [majorsingularname] = 'krone',
	   [minorpluralname] = 'øre',
	   [minorsingularname] = 'øre',
	   [currencysymbol] = 'DKK',
	   [currencyersymbol] = 'DKK'
   WHERE currencycode = 'DKK'

UPDATE [dbo].[supportedcurrency]
   SET [majorpluralname] = 'krona',
       [majorsingularname] = 'krona',
	   [minorpluralname] = 'öre',
	   [minorsingularname] = 'öre',
	   [currencysymbol] = 'SEK',
	   [currencyersymbol] = 'SEK'
   WHERE currencycode = 'SEK'

GO

-- Populate currency units

alter table [supportedcurrency]
    alter column majorpluralname nvarchar(50) not null;

alter table [supportedcurrency]
    alter column majorsingularname nvarchar(50) not null;

alter table [supportedcurrency]
    alter column minorexponent int not null;

alter table [supportedcurrency]
    alter column minorpluralname nvarchar(50) not null;

alter table [supportedcurrency]
    alter column minorsingularname nvarchar(50) not null;

alter table [supportedcurrency]
    alter column minorunit bit not null;

INSERT INTO [dbversion] ([version]) VALUES (555);

COMMIT TRAN