-- (a) Adds a new Roissy accredited lab entry (for Sothy), and 
-- (b) additional payment terms requested by Finance department (via Tony)

-- Galen Beck 2018-12-17
-- Already run on Spain/France test servers as well as production, as it was needed for migration activities

USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[accreditedlab]
           ([description]
           ,[labno]
           ,[log_lastmodified]
           ,[lastModified]
           ,[log_userid]
           ,[orgid]
           ,[accreditationbodyid]
           ,[lastModifiedBy]
           ,[watermark])
     VALUES
           ('Roissy - Etalonnage - Elec-Mag & Temps-Fr�q'
           ,'2-1284 & 2-1357'
           ,null
           ,'2018-12-17'
           ,null
           ,18475
           ,2
           ,17280
           ,0)
GO

SET IDENTITY_INSERT paymentterms ON;

INSERT INTO [dbo].[paymentterms]
           ([id]
		   ,[code]
           ,[description])
     VALUES
           (16,
		   '30EOM15'
           ,'30 days end of month the 15th')
GO

SET IDENTITY_INSERT paymentterms OFF; 

INSERT INTO [dbo].[paymenttermstranslation]
           ([paymenttermsid]
           ,[locale]
           ,[translation])
     VALUES
           (16
           ,'en_GB'
           ,'30 days end of month the 15th'),
           (16
           ,'fr_FR'
           ,'30 jours fin de mois le 15'),
           (16
           ,'es_ES'
           ,'30 d�as fin de mes pago el d�a 15')
GO

INSERT INTO dbversion(version) VALUES(370);

COMMIT TRAN