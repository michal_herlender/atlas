-- Author: Galen Beck 2016-11-02

USE [spain]
GO

BEGIN TRAN;

-- Add a new lookup function which will be used immediately after goods-in, 38 corresponds to the lookup values

SET IDENTITY_INSERT [lookupinstance] ON;

IF NOT EXISTS (SELECT 1 FROM [lookupinstance] WHERE [id] = 72)
   INSERT INTO [dbo].[lookupinstance] ([id],[lookup],[lookupfunction])
   VALUES (72,'Lookup_JobIsOnsite',38);
GO

SET IDENTITY_INSERT [lookupinstance] OFF;

-- Update the goods-in lookup to replace initial "Lookup_ItemAtFirstLocation" to "Lookup_JobIsOnsite"
-- YES - resultmessage 0 - connect to outcomestatus 2 - site job, update from status 4 (transit) to 4047 (onsite service) for now
-- NO - resultmessage 1 - connect to outcomestatus 3 - regular standard job

UPDATE [dbo].[lookupresult]
   SET [lookupid] = 72 ,[outcomestatusid] = 2
 WHERE [lookupid] = 1 AND [activityid] = 3 AND [resultmessage] = 0;

UPDATE [dbo].[lookupresult]
   SET [lookupid] = 72 ,[outcomestatusid] = 3
 WHERE [lookupid] = 1 AND [activityid] = 3 AND [resultmessage] = 1;

GO

-- Change item state 3 (goods in activity) to use new onsite lookup 72 rather than old 1

UPDATE [dbo].[itemstate]
   SET [lookupid] = 72
 WHERE [stateid] = 3;
GO

-- Change outcome status 2 to connect to onsite service 4047 (retain activity of goods in receipt for now)

UPDATE [dbo].[outcomestatus]
   SET [statusid] = 4047
 WHERE [id] = 2
GO

-- Retire unused item activity / item status values to prevent use in overrides
-- Status 4 - Unit held in transit to calibration location - awaiting onward despatch
-- Activity 5 - Unit despatched to calibration location
-- Status 6 - Unit en-route to calibration location

UPDATE [dbo].[itemstate]
   SET [retired] = 1
   WHERE [stateid] IN (4, 5, 6)

COMMIT TRAN;