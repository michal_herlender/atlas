--Add servicetypeid column in asnitem table
USE [atlas]
GO

BEGIN TRAN

    ALTER TABLE dbo.asnitem ADD servicetypeid int
    
	INSERT INTO dbversion(version) VALUES(508);

COMMIT TRAN