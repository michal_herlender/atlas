use [spain]

BEGIN TRAN

alter table dbo.capabilityfilter 
    add servicetypeverification bit default 0;

GO

update dbo.capabilityfilter 
    set servicetypeverification = servicetypecalibration;

GO

alter table dbo.capabilityfilter 
    alter column servicetypeverification bit not null;

GO

INSERT INTO dbversion(version) values(297);

COMMIT TRAN