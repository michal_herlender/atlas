INSERT [spain].[dbo].[actionoutcome](defaultoutcome,description,positiveoutcome,activityid,active)
VALUES(0,'Awaiting quotation to client',0,8,1)

INSERT INTO [spain].[dbo].[actionoutcometranslation] (id,locale,translation)
SELECT id,'en_GB','Awaiting quotation to client' FROM [spain].[dbo].[actionoutcome] WHERE [description] = 'Awaiting quotation to client'

INSERT INTO [spain].[dbo].[actionoutcometranslation] (id,locale,translation)
SELECT id,'es_ES','En espera de la cita con el cliente' FROM [spain].[dbo].[actionoutcome] WHERE [description] = 'Awaiting quotation to client'

INSERT INTO [spain].[dbo].[itemstate] ([type],[active],[description],[retired],[lookupid])
VALUES('workstatus',1,'Contract review - awaiting quotation to client',0,NULL);

INSERT INTO [spain].[dbo].[itemstatetranslation] (stateid,locale,translation)
SELECT stateid,'en_GB','Contract review - awaiting quotation to client' FROM [spain].[dbo].[itemstate] WHERE [description] = 'Contract review - awaiting quotation to client'

INSERT INTO [spain].[dbo].[itemstatetranslation] (stateid,locale,translation)
SELECT stateid,'es_ES','Revisi�n de contrato - esperando de la cita con el cliente' FROM [spain].[dbo].[itemstate] WHERE [description] = 'Contract review - awaiting quotation to client'

INSERT INTO [spain].[dbo].[stategrouplink](groupid,stateid,type)
SELECT 29,stateid,NULL FROM [spain].[dbo].[itemstate] WHERE [description] = 'Contract review - awaiting quotation to client';

INSERT INTO [spain].[dbo].[outcomestatus](activityid,defaultoutcome,lookupid,statusid)
SELECT 8,0,5,stateid FROM [spain].[dbo].[itemstate] WHERE [description] = 'Contract review - awaiting quotation to client'

INSERT INTO [spain].[dbo].[lookupresult](activityid,lookupid,outcomestatusid,result,resultmessage)
SELECT 8,4,outcomestatus.id,'',9
FROM [spain].[dbo].[outcomestatus]
LEFT JOIN [spain].[dbo].[itemstate] ON [outcomestatus].[statusid] = [itemstate].[stateid]
WHERE [description] = 'Contract review - awaiting quotation to client'

INSERT INTO [spain].[dbo].[hookactivity](activityid,beginactivity,completeactivity,hookid,stateid)
SELECT 8,1,1,2,stateid FROM [spain].[dbo].[itemstate] WHERE [description] = 'Contract review - awaiting quotation to client'