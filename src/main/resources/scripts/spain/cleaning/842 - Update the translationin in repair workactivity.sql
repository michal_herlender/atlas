-- Translation:  in the repair workflow, an activity is not correctly translated in french

BEGIN TRAN

UPDATE [dbo].[itemstatetranslation]
SET [translation] = 'Pas de job costing requis pour la réparation'
where stateid = (select stateid from [dbo].[itemstate] where [description] = 'No job costing required for repair') and locale='fr_FR'

INSERT INTO dbversion(version) VALUES(842);

COMMIT TRAN