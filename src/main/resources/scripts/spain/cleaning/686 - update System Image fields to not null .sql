USE [atlas]
BEGIN TRAN    
	
ALTER TABLE systemimage  ALTER COLUMN type varchar(255) NOT NULL
ALTER TABLE systemimage  ALTER COLUMN filedata varbinary(max) NOT NULL
ALTER TABLE systemimage  ALTER COLUMN fileName varchar(255) NOT NULL

ALTER TABLE systemimage  ALTER COLUMN description varchar(255)  NULL

INSERT INTO dbversion(version) VALUES(686);
COMMIT TRAN	
