use spain

BEGIN TRAN

	Insert into dbo.permission(groupId,permission) values 
	(1, 'IMPORT_INSTRUMENTS')
	
	--remove unused permission
	DELETE FROM spain.dbo.permission WHERE permission='INSTRUMENT_IMPORT'
	
	INSERT INTO dbversion(version) VALUES(405);

COMMIT TRAN 