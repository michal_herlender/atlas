-- GB 2021-11-11 Note, the original script contained a copy of script 852; updated to drop categorised_capability_filter
BEGIN TRAN

DROP TABLE IF EXISTS categorised_capability_filter;

INSERT INTO dbversion(version) VALUES(853);

COMMIT TRAN
