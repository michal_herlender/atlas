-- Author Tony Provost 2016-11-18
-- Add new record in table invoicefrequency : IA - Invoicing in advance

BEGIN TRAN 

USE [spain];
GO

SET IDENTITY_INSERT [invoicefrequency] ON
GO
 
IF NOT EXISTS (SELECT 1 FROM [invoicefrequency] WHERE [id] = 9)
	INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (9, 'IA', 'Invoicing in advance')
GO

-- English Translation
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 9 AND locale = 'en_GB') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (9, 'en_GB', 'Invoicing in advance')											
GO
-- Spanish Translation
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 9 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (9, 'es_ES', 'Facturaci�n por adelantado')											
GO

-- French Translation
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 9 AND locale = 'fr_FR') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (9, 'fr_FR', 'Facturation � l''avance')											
GO

SET IDENTITY_INSERT [invoicefrequency] OFF
GO

COMMIT TRAN