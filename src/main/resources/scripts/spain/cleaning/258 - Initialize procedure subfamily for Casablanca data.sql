-- Script 258
-- Initializes subfamilies on procedures for CAS Casablanca data subdiv 6951
-- Author Galen Beck - 2018-05-29

USE [spain]
GO

BEGIN TRAN

-- Updates ~294 procedures to have subfamily
-- Exclude description id 227 (tachometer), same procedure 1919 as 185 (timer)
UPDATE [dbo].[procs]
   SET [procs].[subfamilyid] = 
		(SELECT instmodel.descriptionid 
			FROM [dbo].[servicecapability]
			LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
			WHERE instmodel.mfrid = 1 AND servicecapability.procedureid = procs.id AND
			instmodel.model = '/' AND instmodel.modeltypeid = 1 AND 
			servicecapability.orgid = 6951 AND instmodel.quarantined = 1 and instmodel.descriptionid <> 227)   
 WHERE procs.id in (SELECT 
      [procedureid] 
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  WHERE instmodel.mfrid = 1 AND
  instmodel.model = '/' AND instmodel.modeltypeid = 1 AND 
  servicecapability.orgid = 6951 AND instmodel.quarantined = 1 and instmodel.descriptionid <> 227);
GO

-- Clear out any existing capability filters from previous runs of script
DELETE FROM [dbo].[capabilityfilter]
      WHERE capabilityfilter.id IN 
	  (SELECT capabilityfilter.id FROM [capabilityfilter] 
	  LEFT JOIN procs on procs.id = capabilityfilter.procedureid
	  WHERE orgid = 6951)

GO

-- service types are properly set for CAS service capabilities - onsite
-- (135 rows affected)

INSERT INTO [dbo].[capabilityfilter]
           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
           ,[lastModifiedBy],[procedureid])
     SELECT
           '2018-05-29',0,'','MAYBE',0,1
           ,1,0,calibrationtype.accreditationspecific,(~calibrationtype.accreditationspecific)
           ,17280,[servicecapability].[procedureid]
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  LEFT JOIN procs on [procedureid] = [procs].id 
  LEFT JOIN calibrationtype on [calibrationtypeid] = calibrationtype.caltypeid
  LEFT JOIN servicetype on calibrationtype.[servicetypeid] = servicetype.servicetypeid
  WHERE instmodel.mfrid = 1 AND instmodel.model = '/' AND instmodel.modeltypeid = 1 AND 
  servicecapability.orgid = 6951 AND instmodel.quarantined = 1 and instmodel.descriptionid <> 227
  AND servicetype.shortname IN ('AC-OS','ST-OS')


-- service types are properly set for CAS service capabilities - laboratory
-- (159 rows affected)

INSERT INTO [dbo].[capabilityfilter]
           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
           ,[lastModifiedBy],[procedureid])
     SELECT
           '2018-05-29',0,'','MAYBE',1,0
           ,1,0,calibrationtype.accreditationspecific,(~calibrationtype.accreditationspecific)
           ,17280,[servicecapability].[procedureid]
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  LEFT JOIN procs on [procedureid] = [procs].id 
  LEFT JOIN calibrationtype on [calibrationtypeid] = calibrationtype.caltypeid
  LEFT JOIN servicetype on calibrationtype.[servicetypeid] = servicetype.servicetypeid
  WHERE instmodel.mfrid = 1 AND instmodel.model = '/' AND instmodel.modeltypeid = 1 AND 
  servicecapability.orgid = 6951 AND instmodel.quarantined = 1 and instmodel.descriptionid <> 227
  AND servicetype.shortname IN ('AC-IH','ST-IH')
GO

insert into dbversion(version) values(258)
go

COMMIT TRAN