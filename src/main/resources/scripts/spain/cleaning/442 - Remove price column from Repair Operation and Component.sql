USE spain;

BEGIN TRAN

ALTER TABLE dbo.freerepairoperation DROP COLUMN price;

ALTER TABLE dbo.freerepaircomponent DROP COLUMN price;

INSERT INTO dbversion (version) VALUES (442);

COMMIT TRAN