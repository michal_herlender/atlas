USE [atlas]
GO

BEGIN TRAN

	DECLARE @newStatus INT,
			@newActivity INT,
			@new_lookup INT,
			@work_completed_status INT,
			@unit_despatched_client_activity INT,
			@new_hook INT,
			@os0 INT,
			@os1 INT;

	-- add new status 'Awaiting confirmation of client receipt'
	INSERT INTO itemstate ([type], active, description, retired)
		VALUES ('workstatus', 1, 'Awaiting confirmation of client receipt', 0);
	SELECT @newStatus = MAX(stateid) from itemstate;
	INSERT INTO itemstatetranslation (stateid, locale, [translation])
		VALUES (@newStatus,'de_DE','Warten auf Bestätigung des Kundenempfangs'),
			   (@newStatus,'en_GB','Awaiting confirmation of client receipt'),
		       (@newStatus,'es_ES','Esperando la confirmación del recibo del cliente'),
		       (@newStatus,'fr_FR','En attente de confirmation de la réception du client');

	-- add new activity 'Client receipt confirmed'
	INSERT INTO itemstate ([type], active, description, retired)
		VALUES ('workactivity', 1, 'Client receipt confirmed', 0);
	SELECT @newActivity = MAX(stateid) from itemstate;
	INSERT INTO itemstatetranslation (stateid, locale, [translation])
		VALUES (@newActivity,'de_DE','Kundenbeleg bestätigt'),
			   (@newActivity,'en_GB','Client receipt confirmed'),
		       (@newActivity,'es_ES','Recibo del cliente confirmado'),
		       (@newActivity,'fr_FR','Reçu client confirmé');

	-- create new lookup 'Lookup_ConfirmationClientReceiptRequired'
	INSERT INTO dbo.lookupinstance(lookup, lookupfunction)
		VALUES ('Lookup_ConfirmationClientReceiptRequired', 62);
	SELECT @new_lookup = MAX(id) FROM dbo.lookupinstance;

	-- add outcomestatues
	SELECT @unit_despatched_client_activity = stateid FROM dbo.itemstate WHERE description LIKE 'Unit despatched to client';
	SELECT @os1 = id FROM outcomestatus WHERE activityid = @unit_despatched_client_activity;
	INSERT INTO dbo.outcomestatus (defaultoutcome, activityid, statusid) 
		VALUES (0, @unit_despatched_client_activity, @newStatus);
	SELECT @os0 = MAX(id) from outcomestatus;

	-- add lookupresult
	INSERT INTO lookupresult (lookupid, outcomestatusid, resultmessage)
		VALUES  (@new_lookup, @os0, 0),
				(@new_lookup, @os1, 1);

	-- change the outcomes of 'Unit despatched to client' activity
    UPDATE dbo.itemstate SET lookupid = @new_lookup WHERE stateid = @unit_despatched_client_activity;

	-- create new hook 'destination-receipt-confirmed'
	INSERT INTO dbo.hook (active, alwayspossible, beginactivity, completeactivity, name, activityid)
		VALUES (1,0,1,1,'destination-receipt-confirmed', NULL);
	SELECT @new_hook = MAX(id) FROM dbo.hook;

	-- create hook activities 
	INSERT INTO dbo.hookactivity (beginactivity, completeactivity, activityid, hookid, stateid)
		VALUES  (1, 1, @newActivity, @new_hook, @newStatus);

	-- add the outcome of 'Client receipt confirmed' activity
	SELECT @work_completed_status = stateid FROM dbo.itemstate WHERE description LIKE 'Work completed, despatched to client';
	INSERT INTO outcomestatus(activityid, defaultoutcome, lookupid, statusid) 
		VALUES (@newActivity, 0, NULL, @work_completed_status);

	-- add newStatus to the "AWAITING_CLIENT_RECEIPT_CONFIRMATION" group
	INSERT INTO dbo.stategrouplink(groupid,stateid,[type])
		VALUES (76, @newStatus, 'ALLOCATED_SUBDIV');

	GO

	INSERT INTO dbversion(version) VALUES(716);

COMMIT TRAN