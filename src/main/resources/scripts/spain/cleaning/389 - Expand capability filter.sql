-- Couple database changes:
-- Expand the size of the capability filter comments field to match capability description
-- Create function with similar functionality as mostrecentcertview, allows removal as @OneToOne from Instrument
-- JPA criteria API doesn't currently support table valued functions, so function returns certid (can then use in subquery)
-- Alternately, additional functions could be defined to return specific fields

USE [spain]
GO

BEGIN TRAN

INSERT INTO dbversion(version) VALUES(389);
GO

ALTER TABLE capabilityfilter ALTER COLUMN comments nvarchar(2000) not null
GO

CREATE FUNCTION [dbo].[GetMostRecentCertificate](@PlantId int)
  RETURNS int
AS
  BEGIN
    DECLARE @certId int

    SELECT @certId = 
	  (SELECT topcert.certid FROM
        (SELECT TOP 1 allcerts.certid, allcerts.caldate FROM
          (SELECT c1.certid, c1.caldate
           FROM jobitem
              LEFT JOIN certlink cl ON cl.jobitemid = jobitem.jobitemid
              LEFT JOIN certificate c1 ON c1.certid = cl.certid
           WHERE jobitem.plantid = @PlantId
           UNION
           SELECT c2.certid, c2.caldate
             FROM instcertlink icl
                LEFT JOIN certificate c2 ON c2.certid = icl.certid
           WHERE icl.plantid = @plantId
           ) 
         AS allcerts ORDER BY allcerts.caldate desc, allcerts.certid desc
         )
      AS topcert
      );

    RETURN @certId

  END

GO

COMMIT TRAN