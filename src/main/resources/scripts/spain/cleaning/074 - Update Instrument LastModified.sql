-- Script to set lastModified on all instruments (new field)
-- 2016-10-17 GB
USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[instrument]
   SET [lastModified] = '2016-10-16'
 WHERE [lastModified] IS NULL;

COMMIT TRAN

GO