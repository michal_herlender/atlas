USE [atlas];

BEGIN TRAN

ALTER TABLE calibrationprocess ADD toolcontinuelink VARCHAR(255)
GO

UPDATE calibrationprocess SET toolcontinuelink = 'https://localhost:58159/service/continue?tool=prokal&calibrationId={0}'
WHERE process = 'PROKAL'
GO

INSERT INTO dbversion(version) VALUES(619);

COMMIT TRAN