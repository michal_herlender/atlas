-- Updates approximately ~440 instrumnts with null mfr where instrument model is a MFR_GENERIC mfr.

USE [atlas]
GO

BEGIN TRAN

UPDATE inst 
   SET inst.[mfrid] = 1
   FROM [dbo].[instrument] inst
   LEFT JOIN instmodel im on inst.modelid = im.modelid
 WHERE inst.mfrid is null and im.mfrid = 1;
GO

insert into dbversion(version) values(812);

COMMIT TRAN