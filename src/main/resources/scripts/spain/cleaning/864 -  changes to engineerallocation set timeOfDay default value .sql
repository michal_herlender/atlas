BEGIN TRAN

ALTER TABLE engineerallocation ADD CONSTRAINT df_timeofday DEFAULT 'AM' FOR timeofday;
UPDATE engineerallocation SET timeofday = 'AM' WHERE timeofday is null

INSERT INTO dbversion VALUES (864)

COMMIT TRAN