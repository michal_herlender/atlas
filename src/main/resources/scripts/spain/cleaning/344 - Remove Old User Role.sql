USE [spain]
Begin Tran

IF OBJECT_ID('dbo.user_role', 'U') IS NOT NULL
  DROP TABLE dbo.user_role
IF OBJECT_ID('dbo.role', 'U') IS NOT NULL
  DROP TABLE dbo.role


INSERT INTO dbversion(version) VALUES(344);

COMMIT TRAN