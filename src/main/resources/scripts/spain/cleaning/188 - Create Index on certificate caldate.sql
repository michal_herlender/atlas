-- Creates index on certificate.caldate as results will be ordered by date
-- 2017-11-10

use [spain]

begin tran;

create index IDX_certificate_calDate on dbo.certificate (caldate);

commit tran;