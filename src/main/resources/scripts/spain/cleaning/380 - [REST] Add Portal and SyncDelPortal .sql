use spain;

begin transaction;

--drop table dbo.syncdeliveryportal;
--drop table dbo.portal;

Create table portal(
portalId int identity not null,
portalName varchar(50) not null,
primary key (portalId)
);

Create table syncdeliveryportal(
syncDeliveryId int identity not null,
deliveryId int not null,
lastPull datetime not null, 
portalId int not null,
primary key (syncDeliveryId)
);

ALTER TABLE dbo.syncdeliveryportal
ADD CONSTRAINT FK_DeliverySync 
FOREIGN KEY(deliveryId) REFERENCES dbo.delivery(deliveryid);

ALTER TABLE dbo.syncdeliveryportal
ADD CONSTRAINT FK_DeliveryPortalSync 
FOREIGN KEY(portalId) REFERENCES dbo.portal(portalId);

ALTER TABLE dbo.portal
ADD CONSTRAINT U_PortalName
Unique(portalName);

insert into dbo.portal(portalName) values ('THEMIS_ONLINE');

INSERT INTO dbversion(version) VALUES(380);

commit transaction;