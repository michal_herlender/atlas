-- Updates / inserts permissions for schedule related and check-in / check-out functionality
-- We want these to be in the "Development Features" permission group,
-- as they are under evaluation / acceptance so that they do not appear in the production main menu yet but just for test servers

USE [atlas]
GO

BEGIN TRAN

DECLARE @groupIDNewUserRights INT;
DECLARE @groupIDUnreleaedFeatures INT;

SELECT @groupIDNewUserRights = [id] FROM [dbo].[permissiongroup] WHERE name = 'GROUP_NEW_USER_RIGHTS';
PRINT @groupIDNewUserRights;

SELECT @groupIDUnreleaedFeatures = [id] FROM [dbo].[permissiongroup] WHERE name = 'GROUP_UNRELEASED_FEATURES';
PRINT @groupIDUnreleaedFeatures;

-- Remove the schedule updater from the "new user rights" group

DELETE FROM dbo.permission WHERE
 groupId = @groupIDNewUserRights AND 
 permission ='GOOD_OUT_SCHEDULE_UPDATER';

-- Remove the schedule updater from the "new user rights" group

INSERT INTO dbo.permission(groupId, permission)
VALUES 
 (@groupIDUnreleaedFeatures ,'GOOD_OUT_SCHEDULE_UPDATER'),
 (@groupIDUnreleaedFeatures ,'CHECK_OUT_MAIN'),
 (@groupIDUnreleaedFeatures ,'CHECK_OUT_SEARCH');


INSERT INTO dbversion(version) VALUES(757);

GO

COMMIT TRAN