
USE [atlas]
GO

BEGIN TRAN

    DECLARE @val_servicetype INT,
			@qual_servicetype INT;
	SELECT @val_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'VAL';
	SELECT @qual_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'QUAL';

	DECLARE @val_caltype INT,
			@qual_caltype INT;
	SELECT @val_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @val_servicetype;
	SELECT @qual_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @qual_servicetype;
	
	IF(@val_caltype is NULL)
	BEGIN
	DECLARE @max_orderby INT;
	SELECT @max_orderby = MAX(orderby)+1 FROM dbo.calibrationtype;
	INSERT INTO dbo.calibrationtype
	(accreditationspecific, active, certnoresetyearly, certprefix, firstpagepapertype, labeltemplatepath, nextcertno, orderby, pdfcontinuationwatermark, pdfheaderwatermark, recalldateonlabel, restpagepapertype, log_lastmodified, lastModified, log_userid, servicetypeid, recallRequirementType, calibrationWithJudgement)
	VALUES(0, 1, 1, NULL, 'A4_PLAIN', '/labels/birt/birt_label_standard.rptdesign', NULL, @max_orderby, NULL, NULL, 1, 'A4_PLAIN', NULL, NULL, NULL, @val_servicetype, 'FULL_CALIBRATION', 0);
	SELECT @val_caltype = MAX(caltypeid) FROM dbo.calibrationtype;
	INSERT INTO dbo.jobtypecaltype(caltypeid,jobtypeid,defaultvalue)
	VALUES (@val_caltype,1,1);
	END

	IF(@qual_caltype is NULL)
	BEGIN
	SELECT @max_orderby = MAX(orderby)+1 FROM dbo.calibrationtype;
	INSERT INTO dbo.calibrationtype
	(accreditationspecific, active, certnoresetyearly, certprefix, firstpagepapertype, labeltemplatepath, nextcertno, orderby, pdfcontinuationwatermark, pdfheaderwatermark, recalldateonlabel, restpagepapertype, log_lastmodified, lastModified, log_userid, servicetypeid, recallRequirementType, calibrationWithJudgement)
	VALUES(0, 1, 1, NULL, 'A4_PLAIN', '/labels/birt/birt_label_standard.rptdesign', NULL, @max_orderby, NULL, NULL, 1, 'A4_PLAIN', NULL, NULL, NULL, @qual_servicetype, 'FULL_CALIBRATION', 0);
	SELECT @qual_caltype = MAX(caltypeid) FROM dbo.calibrationtype;
	INSERT INTO dbo.jobtypecaltype(caltypeid,jobtypeid,defaultvalue)
	VALUES (@qual_caltype,1,1);
	END

	INSERT INTO dbo.calibrationaccreditationlevel
	(accredlevel, caltypeid)
	VALUES('APPROVE', @val_caltype),
		  ('PERFORM', @val_caltype),
		  ('REMOVED', @val_caltype),
	      ('SUPERVISED', @val_caltype);

	INSERT INTO dbo.calibrationaccreditationlevel
	(accredlevel, caltypeid)
	VALUES('APPROVE', @qual_caltype),
		  ('PERFORM', @qual_caltype),
		  ('REMOVED', @qual_caltype),
	      ('SUPERVISED', @qual_caltype);

INSERT INTO dbversion(version) VALUES(684);

COMMIT TRAN



