-- Removes old constraints (did not work to two non-global settings at same scope for different business companies)
-- We retain the constraint [UK_ju8nl8nfqp1yo2njus7wjcx9p] on [personid], [subdivid], [coid], [defaultid], [orgid]
-- Author Galen Beck - 2016-11-10

USE [spain]
GO

-- [UQ__systemde__C5C734BF166F3B3A] on [personid], [subdivid], [coid], [defaultid]
/****** Object:  Index [UQ__systemde__C5C734BF166F3B3A]    Script Date: 11/10/2016 11:37:16 AM ******/
ALTER TABLE [dbo].[systemdefaultapplication] DROP CONSTRAINT [UQ__systemde__C5C734BF166F3B3A]
GO

-- [UK_343k8iglu82ckg6u8bx7aonwl] on [personid], [subdivid], [coid], [defaultid]
/****** Object:  Index [UK_343k8iglu82ckg6u8bx7aonwl]    Script Date: 11/10/2016 11:36:48 AM ******/
ALTER TABLE [dbo].[systemdefaultapplication] DROP CONSTRAINT [UK_343k8iglu82ckg6u8bx7aonwl]
GO


