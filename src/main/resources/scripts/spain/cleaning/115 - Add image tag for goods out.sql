-- Inserts a new image tag into database (no translations implemented currently)
-- Author GB - 2017-01-16
USE [spain]
GO

INSERT INTO [dbo].[imagetag]
           ([description]
           ,[tag])
     VALUES
           ('Image saved at goods out', 'goods out');
GO


