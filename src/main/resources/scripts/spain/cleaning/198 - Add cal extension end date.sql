USE SPAIN;

BEGIN TRAN;

ALTER TABLE dbo.permitedcalibrationextension
  ADD extensionEndDate datetime2 NOT NULL;

INSERT INTO dbversion (version) VALUES (198);

COMMIT TRAN;