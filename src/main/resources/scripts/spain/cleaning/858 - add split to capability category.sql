BEGIN TRAN

alter table dbo.procedurecategory
    add splitTraceable bit not null default 0;
exec sp_rename 'procedurecategory', 'capabilitycategory';


INSERT INTO dbversion(version)
VALUES (858);

COMMIT TRAN