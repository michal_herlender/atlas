USE spain;

BEGIN TRAN

UPDATE dbo.instmodeldomain SET domainType = 0
UPDATE dbo.instmodeldomain SET domainType = 1 WHERE name='SERVICES'

UPDATE dbo.servicetype SET domaintype = 0

UPDATE dbo.catalogprice SET servicetype = 
(SELECT calibrationtype.servicetypeid
	FROM catalogprice cp
	LEFT JOIN calibrationtype ON cp.caltypeid = calibrationtype.caltypeid
	WHERE cp.id = catalogprice.id)

DECLARE @ServiceTypeId int

INSERT INTO dbo.servicetype(shortname, longname, displaycolour, displaycolourfasttrack, domaintype)
VALUES ('Hotel', 'Hotel', '#FFFFFF', '#FFFFFF', 1)

SET @ServiceTypeId = SCOPE_IDENTITY()

INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
VALUES (@ServiceTypeId, 'en_GB', 'Hotel')

INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
VALUES (@ServiceTypeId, 'en_GB', 'Hotel')

INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
VALUES (@ServiceTypeId, 'es_ES', 'Hotel')

INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
VALUES (@ServiceTypeId, 'es_ES', 'Hotel')

INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
VALUES (@ServiceTypeId, 'fr_FR', 'H�tel')

INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
VALUES (@ServiceTypeId, 'fr_FR', 'H�tel')

COMMIT TRAN