-- This adds 3 new fields for US instrument data
-- These are not added via customer flexible fields, as expected to be widely used

-- clientbarcode - assigned from client and used on ongoing basis; "EMS barcode" will go in formerbarcode field

-- customer specification - we expect to migrate the curent EMS instrument range data into here;
-- in the future instrument range and/or TML characteristics could be used
-- the customer specification has a dual use for Germany as well

-- freeTextLocation - used as a free text location for inventories, etc... separate from choosing the Location from a list
-- contains data like "Room 123", "Bill's desk", "S1234.293" depending on client but widely used for onsite / equipment location

USE [atlas]
GO

BEGIN TRAN

alter table dbo.instrument 
    add clientbarcode nvarchar(50);

alter table dbo.instrument 
    add customerspecification nvarchar(100);

alter table dbo.instrument 
    add freeTextLocation nvarchar(64);

insert into dbversion(version) values(777);

COMMIT TRAN