USE [atlas]
GO

BEGIN TRAN

	DECLARE @new_prog_activity INT,
			@new_hook INT;
	
	INSERT INTO dbo.itemstate([type], active, description, retired, lookupid, actionoutcometype)
	VALUES('progactivity', 1, 'Production repair time recording', 0, NULL, NULL);
	
	SELECT @new_prog_activity = MAX(stateid) FROM dbo.itemstate;
	
	INSERT INTO dbo.itemstatetranslation(stateid, locale, [translation])
	VALUES(@new_prog_activity, 'fr_FR', 'Enregistrement du temps de réparation de la production'),
		  (@new_prog_activity, 'es_ES', 'Grabación de tiempo de reparación de producción'),
		  (@new_prog_activity, 'en_GB', 'Production repair time recording'),
		  (@new_prog_activity, 'de_DE', 'Aufzeichnung der Produktionsreparaturzeit');
	
	INSERT INTO dbo.stategrouplink(groupid,stateid,[type])
	VALUES (48, @new_prog_activity, '');
	
	INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,0,1,1,'record-production-repair-time',@new_prog_activity);
	
	SELECT @new_hook=id FROM dbo.hook WHERE name = 'record-production-repair-time';
	
	INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@new_prog_activity,@new_hook,NULL);
	
	INSERT INTO dbversion(version) VALUES(671);

COMMIT TRAN
