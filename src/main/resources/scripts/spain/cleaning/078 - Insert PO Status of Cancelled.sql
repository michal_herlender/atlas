-- Author: Galen Beck - 2016-10-20
-- Inserts a new purchase order status of cancelled into the system
-- Can only be run once; will fail if run multiple times due to identity insert
BEGIN TRAN

USE [spain]
GO

SET IDENTITY_INSERT [purchaseorderstatus] ON;

-- Note nextID is updated after first insert happens

INSERT INTO [dbo].[purchaseorderstatus]
           ([statusid]
		   ,[defaultstatus]
           ,[description]
           ,[name]
           ,[issued]
           ,[oncomplete]
           ,[oninsert]
           ,[onissue]
           ,[requiresattention]
           ,[potype]
           ,[deptid]
           ,[nextid]
           ,[depttype]
           ,[approved]
		   ,[oncancel])
     VALUES
           (11
		   ,0
           ,'No goods & services received, order cancelled'
           ,'cancelled'
           ,1
           ,0
           ,0
           ,0
           ,0
           ,'ACTION'
           ,NULL
           ,NULL
           ,NULL
           ,1
		   ,1);

INSERT INTO [dbo].[purchaseorderstatus]
           ([statusid]
		   ,[defaultstatus]
           ,[description]
           ,[name]
           ,[issued]
           ,[oncomplete]
           ,[oninsert]
           ,[onissue]
           ,[requiresattention]
           ,[potype]
           ,[deptid]
           ,[nextid]
           ,[depttype]
           ,[approved]
		   ,[oncancel])
     VALUES
           (12
		   ,0
           ,'Order cancelled'
           ,'cancelled'
           ,1
           ,0
           ,0
           ,0
           ,0
           ,'STATUS'
           ,NULL
           ,NULL
           ,NULL
           ,1
		   ,1);
GO

SET IDENTITY_INSERT [purchaseorderstatus] OFF;

UPDATE [dbo].[purchaseorderstatus]
   SET [oncancel] = 0
 WHERE [oncancel] is NULL;
GO

-- Done after insert
UPDATE [dbo].[purchaseorderstatus]
   SET [nextid] = 12
 WHERE [statusid] = 11;
GO

INSERT INTO [dbo].[purchaseorderstatusdescriptiontranslation]
           ([statusid],[locale],[translation])
     VALUES
           (11, 'en_GB', 'No goods & services received, order cancelled')
           ,(11, 'fr_FR', 'Aucun de biens et services r�ceptionn�s, commande annul�e')
           ,(11, 'es_ES', 'No hay mercanc�as y servicios recibidos, orden cancelada')

           ,(12, 'en_GB', 'Order cancelled')
           ,(12, 'fr_FR', 'Commande annul�e')
           ,(12, 'es_ES', 'Orden cancelada')

INSERT INTO [dbo].[purchaseorderstatusnametranslation]
           ([statusid],[locale],[translation])
     VALUES
           (11, 'en_GB', 'cancelled')
           ,(11, 'fr_FR', 'annul�')
           ,(11, 'es_ES', 'cancelado')
           
		   ,(12, 'en_GB', 'cancelled')
           ,(12, 'fr_FR', 'annul�')
           ,(12, 'es_ES', 'cancelado')

COMMIT TRAN