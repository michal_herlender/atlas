USE [atlas]
BEGIN TRAN

    create table dbo.oauth2accesstoken (
       id int identity not null,
        serializedauthentication varchar(8000) not null,
        serializedtoken varchar(8000) not null,
        tokenid varchar(255) not null,
        authenticationid varchar(255) not null,
        clientid varchar(255) not null,
        refreshtoken varchar(255) not null,
        username varchar(255) not null,
        primary key (id)
    );

    create table dbo.oauth2refreshtoken (
       id int identity not null,
        serializedauthentication varchar(8000) not null,
        serializedtoken varchar(8000) not null,
        tokenid varchar(255) not null,
        primary key (id)
    );

    alter table dbo.oauth2accesstoken 
       add constraint UK_oauth2accesstoken_tokenid unique (tokenid);

    alter table dbo.oauth2accesstoken 
       add constraint UK_oauth2accesstoken_refreshtoken unique (refreshtoken);

    alter table dbo.oauth2refreshtoken 
       add constraint UK_oauth2refreshtoken_tokenid unique (tokenid);

INSERT INTO dbversion(version) VALUES(505);

COMMIT TRAN
