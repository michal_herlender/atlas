-- Drop a number of unused tables which are for migrated entities, past savepoints, etc...
-- This is being done as part of DEV-2142 (US database init)
-- See analysis included in DEV-2142

USE [atlas]

BEGIN TRAN

DROP TABLE dbo.accessory_old
DROP TABLE dbo.archivedcerttemplate
DROP TABLE dbo.bankdetail
DROP TABLE dbo.caltypemodeldefaults
DROP TABLE dbo.capabilityfilter_SAVE20190121
DROP TABLE dbo.catalogprice_SAVE04102017
DROP TABLE dbo.certtemplate
DROP TABLE dbo.connecteddevice
DROP TABLE dbo.departmenttype
DROP TABLE dbo.departmenttypetranslation
DROP TABLE dbo.editablecorole
DROP TABLE dbo.hibernate_sequences
DROP TABLE dbo.hourlyrate
DROP TABLE dbo.instruction
DROP TABLE dbo.instrumentcharacteristic_SAVE
DROP TABLE dbo.jobinstruction
DROP TABLE dbo.message
DROP TABLE dbo.modelrange_SAVE
DROP TABLE dbo.modelrange_SAVE20170202
DROP TABLE dbo.modelrangecharacteristiclibrary_SAVE20170202
DROP TABLE dbo.procs_SAVE20190902
DROP TABLE dbo.queuedautoloadpricelist
DROP TABLE dbo.queuedreport
DROP TABLE dbo.TEMP_CHARACTERISTICS
DROP TABLE dbo.TEMP_DomainTranslations
DROP TABLE dbo.TEMP_FamilyTranslations
DROP TABLE dbo.TEMP_IMToDeactivate
DROP TABLE dbo.TEMP_INSTRUMENTMODELE
DROP TABLE dbo.TEMP_INSTRUMENTMODELE_NEW
DROP TABLE dbo.TEMP_MMSF
DROP TABLE dbo.TEMP_SALESCATEGORY
DROP TABLE dbo.TEMP_SALESCATEGORY_LIBRARY
DROP TABLE dbo.TEMP_SubFamilyTranslations
DROP TABLE dbo.TMP_IMPORT_GENERICINSTRUMENTMODELS
DROP TABLE dbo.webserviceinstlink
DROP TABLE dbo.webservicejobitemlink
DROP TABLE dbo.webservicemodellink
DROP TABLE dbo.trustedwebservice

INSERT INTO dbversion(version) VALUES(754);

COMMIT TRAN