use spain

BEGIN tran

/*Revert Script
delete from spain.dbo.purchaseorderstatusnametranslation  where statusid>12
delete from spain.dbo.purchaseorderstatusdescriptiontranslation  where statusid>12
update dbo.purchaseorder set statusid=4 where statusid > 12
update dbo.purchaseorderstatus set nextid=4 where statusid=3
update dbo.purchaseorderstatus set nextid=6 where statusid=5
delete from dbo.purchaseorderactionlist where purchaseorderactionlist.activityid > 12 or statusid >12
delete from dbo.purchaseorderstatus where statusid > 12
*/

/*Add new status and action */
set identity_insert dbo.purchaseorderstatus on
insert into 
dbo.purchaseorderstatus(statusid,defaultstatus,description,name
,issued,oncomplete,oninsert,onissue,requiresattention,potype, deptid,
nextid,depttype,approved,oncancel) values
(13,0,'awaiting pre-approval before approval','awaiting pre-approval',0,0,0,0,1,'STATUS',NULL,14,NULL,0,0),
(14,0,'purchase order pre-approved','pre-approved',0,0,0,0,0,'ACTION',NULL,4,NULL,0,0),
(15,0,'awaiting pre-issue before issue','awaiting pre-issue',0,0,0,0,1,'STATUS',NULL,16,NULL,0,0),
(16,0,'purchase order pre-issued','pre-issued',0,0,0,0,0,'ACTION',NULL,6,NULL,0,0)
set identity_insert dbo.purchaseorderstatus off

/*Add new status and action Translation */			
insert into spain.dbo.purchaseorderstatusnametranslation values
(13,'en_GB','awaiting pre-approval'),
(14,'en_GB','pre-approved'),
(15,'en_GB','awaiting pre-issue'),
(16,'en_GB','pre-issued')
insert into spain.dbo.purchaseorderstatusdescriptiontranslation values
(13,'en_GB','awaiting pre-approval before approval'),
(14,'en_GB','purchase order pre approved'),
(15,'en_GB','awaiting pre-issue before issue'),
(16,'en_GB','purchase order pre issued')

/*Change status order */
update dbo.purchaseorderstatus set nextid=13 where statusid=3
update dbo.purchaseorderstatus set nextid=15 where statusid=5

INSERT INTO dbversion(version) VALUES(364);

COMMIT TRAN 