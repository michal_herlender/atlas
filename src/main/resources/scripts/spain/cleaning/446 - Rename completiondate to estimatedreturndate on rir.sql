
USE spain;

BEGIN TRAN

	EXEC sp_RENAME 'dbo.repairinspectionreport.completiondate' , 'estimatedreturndate', 'COLUMN';

	INSERT INTO dbversion (version) VALUES (446);

COMMIT TRAN