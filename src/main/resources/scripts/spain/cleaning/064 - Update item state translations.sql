-- 2016-09-21 Galen Beck
-- Updates to Spanish translations for Ticket 848

USE [spain]
GO

UPDATE [dbo].[itemstatetranslation]
 SET [translation] = 'Esperando requisitos el trabajo terceros'
 WHERE [stateid] = 89 and [locale] = 'es_ES';

UPDATE [dbo].[itemstatetranslation]
 SET [translation] = 'Esperando el coste al cliente para el trabajo adicional en terceros'
 WHERE [stateid] = 122 and [locale] = 'es_ES';

UPDATE [dbo].[itemstatetranslation]
 SET [translation] = 'Esperando el coste al cliente para trabajo de terceros'
 WHERE [stateid] = 104 and [locale] = 'es_ES';

UPDATE [dbo].[itemstatetranslation]
 SET [translation] = 'Esperando la aprobaci�n del cliente para el trabajo de terceros despu�s de enviar el coste'
 WHERE [stateid] = 106 and [locale] = 'es_ES';

GO


