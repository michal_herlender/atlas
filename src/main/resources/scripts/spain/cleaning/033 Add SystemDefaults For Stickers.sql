-- Author Galen Beck 2016-04-29
-- Creates new SystemDefault related entities (37 to 40) for Stickers and performs related configuration

BEGIN TRAN 

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
           ([defaultid],[defaultdatatype],[defaultdescription],[defaultname],[defaultvalue],[endd],[start],[groupid],[groupwide])
     VALUES
           (37
		   ,'boolean'
           ,'Defines if this company receives a plant number on standard calibration labels'
           ,'Standard Label Plant Number'
           ,'true',0,0,10,1),
           (38
		   ,'boolean'
           ,'Defines if this company receives a plant number on accredited calibration labels'
           ,'Accredited Label Plant Number'
           ,'true',0,0,10,1),
           (39
		   ,'boolean'
           ,'Defines if this company receives a certificate number on standard calibration labels'
           ,'Standard Label Certificate Number'
           ,'true',0,0,10,1),
           (40
		   ,'boolean'
           ,'Defines if this company receives a certificate number on accredited calibration labels'
           ,'Accredited Label Certificate Number'
           ,'true',0,0,10,1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
   ([defaultid], [locale], [translation])
   VALUES
   (37, 'en_GB', 'Defines if this company receives a plant number on standard calibration labels'),
   (38, 'en_GB','Defines if this company receives a plant number on accredited calibration labels'),
   (39, 'en_GB','Defines if this company receives a certificate number on standard calibration labels'),
   (40, 'en_GB','Defines if this company receives a certificate number on accredited calibration labels'),
   (37, 'es_ES','Define si una compañía quiere número de equipo en las etiquetas de calibración trazable'),
   (38, 'es_ES','Define si una compañía quiere número de equipo en las etiquetas de calibración acreditadas'),
   (39, 'es_ES','Define si una compañía quiere número certificado en las etiquetas de calibración trazable'),
   (40, 'es_ES','Define si una compañía quiere número certificado en las etiquetas de calibración acreditadas');

 -- Enables new system defaults to be configurable at any scope of company, subdiv, contact
 
 INSERT INTO [dbo].[systemdefaultscope]
           ([scope],[defaultid])
     VALUES
           (0,37),
           (1,37),
           (2,37),
           (0,38),
           (1,38),
           (2,38),
           (0,39),
           (1,39),
           (2,39),
           (0,40),
           (1,40),
           (2,40);
GO

-- Enables system defaults (new and old) for specific company roles - Client and Business Company

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid],[defaultid]) VALUES
 (1,37),
 (1,38),
 (1,39),
 (1,40),
 (1,29),
 (5,37),
 (5,38),
 (5,39),
 (5,40),
 (5,29),
 (5,6),
 (5,19);
 
-- Add translations (English appears to be added by default)

INSERT INTO [dbo].[systemdefaultnametranslation]
           ([defaultid],[locale],[translation])
     VALUES
           (37,'es_ES','Número de equipo por etiqueta trazable'),
           (38,'es_ES','Número de equipo por etiqueta acreditado'),
           (39,'es_ES','Número certificado por etiqueta trazable'),
           (40,'es_ES','Número certificado por etiqueta acreditado');
GO

-- Change to COMMIT when successfully tested

ROLLBACK TRAN