BEGIN TRAN

UPDATE jobitem
SET lastModified = datecomplete
WHERE lastModified IS NULL

CREATE TABLE dbo.jobexpenseitemnotinvoiced (
	comments nvarchar(200) NOT NULL,
	date datetime NOT NULL,
	reason INT,
	expenseitemid INT NOT NULL,
	contactid INT,
	periodicinvoiceid INT,
	primary key (expenseitemid));

ALTER TABLE dbo.jobexpenseitemnotinvoiced
	ADD CONSTRAINT FK_jobexpenseitemnotinvoiced_expenseitem
	FOREIGN KEY (expenseitemid)
	REFERENCES dbo.jobexpenseitem;

ALTER TABLE dbo.jobexpenseitemnotinvoiced
	ADD CONSTRAINT FK_jobexpenseitemnotinvoiced_contact
	FOREIGN KEY (contactid)
	REFERENCES dbo.contact;

ALTER TABLE dbo.jobexpenseitemnotinvoiced
	ADD CONSTRAINT FK_jobexpenseitemnotinvoiced_periodicinvoice
	FOREIGN KEY (periodicinvoiceid)
	REFERENCES dbo.invoice;

INSERT INTO dbversion(version) VALUES(456)

COMMIT TRAN