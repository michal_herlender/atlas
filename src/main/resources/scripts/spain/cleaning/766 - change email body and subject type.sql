USE [atlas]

BEGIN TRAN

GO
alter table email alter column body nvarchar(max);
alter table email alter column subject nvarchar(200);

INSERT INTO dbversion(version) VALUES(766);

COMMIT TRAN
