-- Author Galen Beck 2016-07-29
-- Updated JobItemNotInvoiced reason to Inter-Subdivision for all jobs done so far with Zaragoza as a client

USE [spain]
GO

BEGIN TRAN;

UPDATE [dbo].[jobitemnotinvoiced]
   SET [reason] = 6
 WHERE [jobitemid] IN (
	SELECT [jobitemid] FROM [jobitem] WHERE [jobid] IN (
		SELECT [jobid] FROM [dbo].[job] WHERE [personid] IN (
			SELECT [personid] FROM [dbo].[contact] WHERE [subdivid] = 6644)
	)
 );

COMMIT TRAN;

GO


