-- Remove and delete permissions for deleted change subdiv functionality - no longer used
-- Galen Beck 2018-12-11

USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[permission] WHERE permission = 'CHANGE_SUBDIV';


INSERT INTO dbversion(version) VALUES(366);

COMMIT TRAN