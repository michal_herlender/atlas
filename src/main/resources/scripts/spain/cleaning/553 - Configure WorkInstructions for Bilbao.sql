-- Adds a new field to BusinessSubdivisionSettings which indicates to return work instruction information, 
-- when available on certificate web service.
-- Galen Beck 2020-01-10
USE [atlas]
GO

BEGIN TRAN
   	
alter table dbo.businesssubdivisionsettings 
    add workinstructionreference bit;
GO

UPDATE [dbo].[businesssubdivisionsettings]
   SET workinstructionreference = 0;
 
-- Set these only for Bilbao (and the deactivated Vitora subdiv)
UPDATE [dbo].[businesssubdivisionsettings]
   SET workinstructionreference = 1
 WHERE [subdivid] in (6642, 6643);

-- Also set business subdiv turnaround to global default of 7 days, where null, and add constraint.
UPDATE [dbo].[businesssubdivisionsettings]
   SET defaultturnaround = 7
   WHERE defaultturnaround is null;

GO

alter table dbo.businesssubdivisionsettings 
    alter column workinstructionreference bit not null;

alter table dbo.businesssubdivisionsettings 
    alter column defaultturnaround int not null;

-- Also make title and name in WorkInstruction not nullable (essential reference information, no data should exist currently)

alter table dbo.workinstruction
    alter column name varchar(100) not null;

alter table dbo.workinstruction 
    alter column title varchar(100) not null;

INSERT INTO dbversion(version) VALUES(553);

COMMIT TRAN