USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[itemstate]
   SET [description] = 'Service completed, awaiting job costing'
 WHERE stateid = 33 
GO

UPDATE [dbo].[itemstatetranslation]
   SET [translation] = 'Service completed, awaiting job costing'
 WHERE stateid = 33 and locale = 'en_GB' 
GO

UPDATE [dbo].[itemstatetranslation]
   SET [translation] = 'Servicio completado, esperando el Coste del trabajo'
 WHERE stateid = 33 and locale = 'es_ES' 
GO

UPDATE [dbo].[itemstatetranslation]
   SET [translation] = 'Service abgeschlossen, warten auf Auftragskalkulation'
 WHERE stateid = 33 and locale = 'de_DE' 
GO

UPDATE [dbo].[itemstatetranslation]
   SET [translation] = 'Service termin�, en attente de valorisation ("job pricing") pour le client'
 WHERE stateid = 33 and locale = 'fr_FR' 
GO

UPDATE [dbo].[itemstate]
   SET [description] = 'Service completed, awaiting confirmation costing'
 WHERE stateid = 35 
GO

UPDATE [dbo].[itemstatetranslation]
   SET [translation] = 'Service completed, awaiting confirmation costing'
 WHERE stateid = 35 and locale = 'en_GB' 
GO

UPDATE [dbo].[itemstatetranslation]
   SET [translation] = 'Servicio completado, esperando el Coste de la confirmaci�n'
 WHERE stateid = 35 and locale = 'es_ES' 
GO

UPDATE [dbo].[itemstatetranslation]
   SET [translation] = 'Service abgeschlossen, warten auf Best�tigung der Auftragskalkulation'
 WHERE stateid = 35 and locale = 'de_DE' 
GO

UPDATE [dbo].[itemstatetranslation]
   SET [translation] = 'Service termin�, en attente de la valorisation ("job pricing") de confirmation du prix'
 WHERE stateid = 35 and locale = 'fr_FR' 
GO

INSERT INTO dbversion (version) VALUES (449);

COMMIT TRAN