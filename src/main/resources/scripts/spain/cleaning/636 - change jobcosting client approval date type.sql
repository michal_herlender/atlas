USE [atlas]

BEGIN TRAN


    ALTER TABLE dbo.jobcostingclientapproval
	ALTER COLUMN clientapprovalon DATETIME2;

	INSERT INTO dbversion(version) VALUES (636)

COMMIT TRAN