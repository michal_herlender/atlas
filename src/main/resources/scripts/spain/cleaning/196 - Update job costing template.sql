USE [spain]
GO

BEGIN TRAN

UPDATE dbo.template 
SET templatepath = '/birt/docs/jobcosting.rptdesign'
WHERE name = 'Job Costing E-mail'

COMMIT TRAN

INSERT INTO dbversion (version) VALUES (196);

GO