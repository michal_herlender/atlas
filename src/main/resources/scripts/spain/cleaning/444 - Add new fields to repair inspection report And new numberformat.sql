USE spain;

BEGIN TRAN

	ALTER TABLE dbo.repairinspectionreport ADD orgid int;
	ALTER TABLE dbo.repairinspectionreport ADD CONSTRAINT repairinspectionreport_subdiv_FK 
	FOREIGN KEY (orgid) REFERENCES dbo.subdiv(subdivid);
	
	ALTER TABLE dbo.repairinspectionreport ADD rirnumber varchar(30);

	INSERT INTO dbversion (version) VALUES (444);

COMMIT TRAN