USE [atlas]
GO

BEGIN TRAN
set identity_insert systemdefault ON;
INSERT  into systemdefault (defaultid,defaultdatatype,defaultdescription,defaultname,defaultvalue,groupid)
values(56,'boolean','Disallow the same plant numbers for instruments','Disallow same plant number','true',1);

set identity_insert systemdefault OFF;
insert into systemdefaultcorole (coroleid,defaultid)
  select  1,sd.defaultid from systemdefault sd where sd.defaultname = 'Disallow same plant number' ;

insert into systemdefaultscope (scope,defaultid)
  select  0,sd.defaultid from systemdefault sd where sd.defaultname = 'Disallow same plant number' ;
insert into systemdefaultdescriptiontranslation
  select sd.defaultid,'en_GB','Determines whether duplicate checking will cause an error or a warning message during instrument creation'
  from systemdefault sd where sd.defaultName = 'Disallow same plant number';

INSERT INTO dbversion(version) VALUES(616);

COMMIT TRAN
