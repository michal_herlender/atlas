USE [atlas]

BEGIN TRAN

DECLARE @confirmation_rd_client_lookup INT,
		@confirmation_rd_tp_lookup INT,
		@tp_requirement_lookup INT,
		@item_despatched_tp_activity INT,
		@new_os INT,
		@item_despatch_client_activity INT;

SELECT @confirmation_rd_client_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_ConfirmationDestinationReceiptRequired';

UPDATE dbo.lookupinstance SET [lookup] = 'Lookup_ConfirmationDestinationReceiptRequired (after despatching to client)' 
WHERE id = @confirmation_rd_client_lookup;

INSERT INTO dbo.lookupinstance([lookup],lookupfunction)
VALUES ('Lookup_ConfirmationDestinationReceiptRequired (after despatching to thirdparty)', 62);
SELECT @confirmation_rd_tp_lookup = MAX(id) FROM dbo.lookupinstance;

SELECT @tp_requirement_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_TPRequirements (third party after item despatched)';
SELECT @item_despatched_tp_activity = stateid FROM dbo.itemstate WHERE [description] = 'Item despatched to third party';
INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
VALUES (0,@item_despatched_tp_activity,@tp_requirement_lookup,null);
SELECT @new_os = MAX(id) FROM dbo.outcomestatus;

-- to fix issue with previous script 759
UPDATE dbo.lookupresult SET result = NULL, outcomestatusid = @new_os WHERE result = '*' AND activityid = @item_despatched_tp_activity;

SELECT @item_despatch_client_activity = stateid FROM dbo.itemstate WHERE [description] = 'Unit despatched to client';
UPDATE dbo.lookupresult SET activityid = NULL WHERE lookupid = @confirmation_rd_client_lookup AND activityid = @item_despatch_client_activity;
UPDATE dbo.lookupresult SET activityid = NULL,lookupid = @confirmation_rd_tp_lookup WHERE lookupid = @confirmation_rd_client_lookup AND activityid = @item_despatched_tp_activity;

UPDATE dbo.itemstate SET lookupid = @confirmation_rd_tp_lookup WHERE stateid = @item_despatched_tp_activity;

INSERT INTO dbversion(version) VALUES(764);

COMMIT TRAN
