-- Configures system (mainly system defaults) to allow a single type of job pricing

USE [spain]
GO

BEGIN TRAN

-- Removed unused WARRANTY type (not used in production, just a precaution if used locally in testing)
UPDATE [dbo].[invoice]
   SET [pricingtype] = 'CLIENT' WHERE pricingtype = 'WARRANTY'
GO

-- Removed unused WARRANTY type (not used in production, just a precaution if used locally in testing)
UPDATE [dbo].[jobcosting]
   SET [clientcosting] = 'CLIENT' WHERE clientcosting = 'WARRANTY'
GO

-- Repurpose system default 13 (previously used for "Beamex firmware update" at Antech - though in fact not actually used in Atlas)
-- systemdefaultscope is OK (contact, subdiv, company)

-- Setting this up as a per business company default intentionally
UPDATE [dbo].[systemdefault]
   SET [defaultdatatype] = 'boolean'
      ,[defaultdescription] = 'Requires Job Pricing to contain a single job item only'
      ,[defaultname] = 'Pricing Type - Single Job Item'
      ,[defaultvalue] = 'false'
      ,[endd] = 0
      ,[start] = 0
      ,[groupid] = 5
      ,[groupwide] = 0
 WHERE defaultid = 13;
GO

-- None in production, but to be careful
DELETE FROM [dbo].[systemdefaultapplication]
      WHERE defaultid = 13;
GO
DELETE FROM [dbo].[systemdefaultnametranslation]
      WHERE defaultid = 13;
GO
DELETE FROM [dbo].[systemdefaultdescriptiontranslation]
      WHERE defaultid = 13;
GO
DELETE FROM [dbo].[systemdefaultcorole]
      WHERE defaultid = 13;
GO

INSERT INTO [dbo].[systemdefaultnametranslation]
           ([defaultid],[locale],[translation])
     VALUES
           (13,'en_GB','Pricing Type - Single Job Item'),
           (13,'fr_FR','Type de tarification - Seul �l�ment de Job'),
           (13,'es_ES','Tipo de precio - �nico elemento de trabajo'),
           (13,'de_DE','Preistyp - Einzelelement des Jobs')
GO

INSERT INTO [dbo].[systemdefaultdescriptiontranslation]
           ([defaultid],[locale],[translation])
     VALUES
           (13,'en_GB','Requires Job Pricing to contain a single job item only'),
           (13,'fr_FR','La tarification des travaux doit contenir un seul �l�ment de travail.'),
           (13,'es_ES','Requiere que el precio del trabajo contenga un solo elemento de trabajo'),
           (13,'de_DE','Ben�tigt Job Pricing, um nur einen Jobartikel zu enthalten')
GO

INSERT INTO [dbo].[systemdefaultcorole]
           ([coroleid],[defaultid])
     VALUES
           (1,13),(2,13),(5,13)
GO

INSERT INTO dbversion(version) VALUES(479);

COMMIT TRAN