
USE [atlas]
GO

BEGIN TRAN

CREATE TABLE dbo.jobcostingstatushistory (
    id INT IDENTITY(1,1) NOT NULL,
	jobcostingid INT NOT NULL,
	statusid INT NOT NULL,
	statusupdatedon DATETIME2 NOT NULL,
    statusupdatedby  INT NOT NULL,
	clientapproval BIT,
    clientapprovalon DATETIME2,
    clientapprovalcomment nvarchar(1000),
    PRIMARY KEY (id)
);

ALTER TABLE dbo.jobcostingstatushistory 
    ADD CONSTRAINT FK_jcshistory_updatedby_contact
    FOREIGN KEY (statusupdatedby) 
    REFERENCES dbo.contact(personid);

ALTER TABLE dbo.jobcostingstatushistory 
    ADD CONSTRAINT FK_jcshistory_status_jobcostingstatus
    FOREIGN KEY (statusid) 
    REFERENCES dbo.basestatus(statusid);

ALTER TABLE dbo.jobcostingstatushistory 
    ADD CONSTRAINT FK_jcshistory_jobcosting
    FOREIGN KEY (jobcostingid) 
    REFERENCES dbo.jobcosting(id);

-- initiate the new jobcostingstatushistory items
INSERT INTO dbo.jobcostingstatushistory(jobcostingid,statusid,statusupdatedby,statusupdatedon,
clientapproval,clientapprovalon,clientapprovalcomment)
SELECT id,statusid,lastupdateby,lastupdateon,clientapproval,
clientapprovalon,clientapprovalcomment FROM dbo.jobcosting;

-- drop columns and related constraints in jobcosting table
DECLARE @constraint_name VARCHAR(100);

SELECT @constraint_name = CONSTRAINT_NAME FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE 
WHERE TABLE_NAME = 'jobcosting' and COLUMN_NAME = 'statusid';
EXEC ('ALTER TABLE dbo.jobcosting DROP CONSTRAINT ' + @constraint_name);
ALTER TABLE dbo.jobcosting DROP COLUMN statusid;

ALTER TABLE dbo.jobcosting DROP COLUMN statusupdatedon;
ALTER TABLE dbo.jobcosting DROP COLUMN clientapproval;
ALTER TABLE dbo.jobcosting DROP COLUMN clientapprovalon;
ALTER TABLE dbo.jobcosting DROP COLUMN clientapprovalcomment;

-- set the two flags to true to destingesh the ON-Hold jobcosting status
UPDATE dbo.basestatus SET followingissue = 1, followingreceipt = 1 
WHERE [type] = 'jobcosting' AND [name] = 'On-Hold';

INSERT INTO dbversion(version) VALUES (603);

COMMIT TRAN