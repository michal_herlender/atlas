USE spain

BEGIN TRAN

DECLARE @State_AP int, @State_DN int, @Activity_PO int, @Lookup int

INSERT INTO itemstate (active, description, retired, type) VALUES
(1, 'Awaiting PO for inter company work', 0, 'workstatus'),
(1, 'PO created for inter company work', 0, 'workactivity') 

SELECT @State_AP = (SELECT stateid FROM itemstate WHERE description = 'Awaiting PO for inter company work')
SELECT @State_DN = (SELECT stateid FROM itemstate WHERE description = 'Awaiting internal delivery note')
SELECT @Activity_PO = (SELECT stateid FROM itemstate WHERE description = 'PO created for inter company work')
SELECT @Lookup = (SELECT id FROM lookupinstance WHERE lookup='Lookup_PreDeliveryRequirements')

INSERT INTO itemstatetranslation (locale, stateid, translation) VALUES
('en_GB', @State_AP, 'Awaiting PO for inter company work'),
('en_GB', @Activity_PO, 'PO created for inter company work'),
('es_ES', @State_AP, 'Esperando la orden de compra para el trabajo inter empresa'),
('es_ES', @Activity_PO, 'Creada orden de compra para el trabajo inter empresa')

INSERT INTO hookactivity (beginactivity, completeactivity, activityid, hookid, stateid) VALUES
(1, 1, @Activity_PO, 21, @State_AP)

INSERT INTO outcomestatus (defaultoutcome, activityid, statusid, lookupid) VALUES
(1, @Activity_PO, @State_DN, NULL),
(1, NULL, @State_AP, NULL)

UPDATE lookupresult SET outcomestatusid = outcomestatus.id
FROM outcomestatus
WHERE outcomestatus.statusid = @State_AP AND lookupresult.lookupid = @Lookup AND lookupresult.resultmessage = 2

COMMIT TRAN