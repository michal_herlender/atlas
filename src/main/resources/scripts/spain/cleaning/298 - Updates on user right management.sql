use spain;

begin tran

	alter table dbo.perrole
	add name varchar(255) not null;

	create table dbo.perroletranslation (
		perrole int not null,
		locale varchar(255) not null,
		translation nvarchar(1000) not null,
		primary key (perrole, locale, translation)
	);

	alter table dbo.perrole
	add constraint UK_hggak5mcvxxc3gxalv07hy6co unique (name);

	alter table dbo.perroletranslation
	add constraint FK_perroletranslation_perrole
	foreign key (perrole)
	references dbo.perrole;
	
	insert into dbversion(version) values(298);

commit tran