-- Updates to workflow to allow having state set on job item when it's first persisted;
-- this prevents an OpenActivityException when book-in-item hook is then called

USE [atlas]
GO

BEGIN TRAN

UPDATE [dbo].[hook]
   SET [beginactivity] = 0
 WHERE [name] = 'book-in-item';

INSERT INTO dbversion VALUES (804);

COMMIT TRAN
