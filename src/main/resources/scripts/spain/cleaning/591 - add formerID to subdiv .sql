USE [atlas];
GO

BEGIN TRAN

ALTER TABLE company ALTER COLUMN formerid  varchar(50);
ALTER TABLE subdiv ADD formerid varchar(50);

INSERT INTO dbversion(version) VALUES(591);

COMMIT TRAN