USE [atlas]
GO

BEGIN TRAN
set identity_insert systemdefault ON;
INSERT  into systemdefault (defaultid,defaultdatatype,defaultdescription,defaultname,defaultvalue,groupid,groupwide,endd,start)
values(67,'TimeZone','The time zone of the business unit','The Time zone','UTC',11,1,0,0);

set identity_insert systemdefault OFF;

insert into systemdefaultcorole (coroleid,defaultid)
  select  5,sd.defaultid from systemdefault sd where sd.defaultid = 67 ;

insert into systemdefaultscope (scope,defaultid)
  select  0,sd.defaultid from systemdefault sd where sd.defaultid = 67 ;
insert into systemdefaultscope (scope,defaultid)
  select  2,sd.defaultid from systemdefault sd where sd.defaultid = 67 ;
insert into systemdefaultscope (scope,defaultid)
  select  3,sd.defaultid from systemdefault sd where sd.defaultid = 67 ;
insert into systemdefaultnametranslation
  select sd.defaultid,'en_GB','The time zone of the business unit'
  from systemdefault sd where sd.defaultid = 67;
insert into systemdefaultdescriptiontranslation
  select sd.defaultid,'en_GB','The time zone of the time based operation in the business unit'
  from systemdefault sd where sd.defaultid = 67;


  -- default value for Europeans servers
insert into systemdefaultapplication  (defaultvalue,defaultid,lastModified,orgid) values ('Europe/Paris',67,CURRENT_TIMESTAMP,6690)

INSERT INTO dbversion(version) VALUES(785);

COMMIT TRAN
