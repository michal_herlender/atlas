-- Author Tony Provost 2016-11-22
-- Add frenchs tranlsations for default preset comment category

BEGIN TRAN 

USE [spain];
GO

DECLARE @BusinessCompanyID INT = 6685
DECLARE @UserID INT

SELECT @UserID = personid FROM dbo.contact WITH (NOLOCK) WHERE firstname = 'CWMS Auto User'

IF EXISTS (SELECT 1 FROM [dbo].[presetcomment] WHERE [type] = 'EMAIL' AND [orgid] = 6685)
	GOTO endScript

INSERT INTO [dbo].[presetcomment] ([type], [comment], [seton], [categoryid], [setbyid], [lastModified], [orgid])
     VALUES ('EMAIL', 'Madame, Monsieur, veuillez trouver ci-joint notre facture pour les services r�alis�s. Pour toutes pr�cisions, merci de joindre le contact Trescal figurant sur la facture. 
Cordialement.', GETDATE(), NULL, @UserID, GETDATE(), @BusinessCompanyID);

INSERT INTO [dbo].[presetcomment] ([type], [comment], [seton], [categoryid], [setbyid], [lastModified], [orgid])
     VALUES ('EMAIL', 'Madame, Monsieur, veuillez trouver ci-joint notre facture pour les services r�alis�s. Pour toutes pr�cisions, merci de joindre votre contact commercial Trescal.
Note : Nous mettons � jour nos base de donn�es, en cons�quence si vous n''�tes pas le destinataire de ce document, s''il vous pla�t nous fournir l''adresse e-mail et le contact.

Cordialement.', GETDATE(), NULL, @UserID, GETDATE(), @BusinessCompanyID);

INSERT INTO [dbo].[presetcomment] ([type], [comment], [seton], [categoryid], [setbyid], [lastModified], [orgid])
     VALUES ('EMAIL', 'Ce message est � titre informatif, il ne n�cessite pas de r�ponse de votre part.
Veuillez trouver ci-joint les co�ts et d�tail de livraison pour votre instrument de mesure.
Cordialement.', GETDATE(), NULL, @UserID, GETDATE(), @BusinessCompanyID);

INSERT INTO [dbo].[presetcomment] ([type], [comment], [seton], [categoryid], [setbyid], [lastModified], [orgid])
     VALUES ('EMAIL', 'Madame, Monsieur, 
Veuillez trouver ci-joint les co�ts de v�rification/�talonnage/r�paration pour votre instrument de mesure. Nous restons en l''attente de votre approbation et de la r�f�rence de votre commande.
Cordialement.', GETDATE(), NULL, @UserID, GETDATE(), @BusinessCompanyID);

INSERT INTO [dbo].[presetcomment] ([type], [comment], [seton], [categoryid], [setbyid], [lastModified], [orgid])
     VALUES ('EMAIL', 'Madame, Monsieur, 
Veuillez trouver le d�tail de l''anomalie et des co�ts aff�rents (page 2 du document joint). Nous restons en l''attente de votre approbation et de la r�f�rence de votre commande.
Cordialement.', GETDATE(), NULL, @UserID, GETDATE(), @BusinessCompanyID);

INSERT INTO [dbo].[presetcomment] ([type], [comment], [seton], [categoryid], [setbyid], [lastModified], [orgid])
     VALUES ('EMAIL', 'Madame, Monsieur, 
Veuillez trouver ci-joint les co�ts de v�rification/�talonnage pour votre instrument de mesure. Nous restons en l''attente de votre approbation et de la r�f�rence de votre commande.
Cordialement.', GETDATE(), NULL, @UserID, GETDATE(), @BusinessCompanyID);

INSERT INTO [dbo].[presetcomment] ([type], [comment], [seton], [categoryid], [setbyid], [lastModified], [orgid])
     VALUES ('EMAIL', 'Madame, Monsieur, 
Veuillez trouver ci-joint la confirmation des co�ts de v�rification/�talonnage/r�paration pour votre instrument de mesure. Nous restons en l''attente de votre approbation et de la r�f�rence de votre commande.
Cordialement.', GETDATE(), NULL, @UserID, GETDATE(), @BusinessCompanyID);

INSERT INTO [dbo].[presetcomment] ([type], [comment], [seton], [categoryid], [setbyid], [lastModified], [orgid])
     VALUES ('EMAIL', 'Madame, Monsieur, 
Nous avons re�u votre mat�riel pour v�rification/�talonnage. Toutefois, nous ne pouvons engager la prestation sans information compl�mentaire. Merci de remplir le document ci-joint et nous le renvoyer.
Cordialement.', GETDATE(), NULL, @UserID, GETDATE(), @BusinessCompanyID);

endScript:

COMMIT TRAN