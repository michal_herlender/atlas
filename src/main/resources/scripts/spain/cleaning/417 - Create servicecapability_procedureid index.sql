USE [spain]
GO

BEGIN TRAN

CREATE NONCLUSTERED INDEX IDX_servicecapability_procedureid
ON [dbo].[servicecapability] ([procedureid])
GO

CREATE NONCLUSTERED INDEX IDX_capabilityfilter_procedureid
ON [dbo].[capabilityfilter] ([procedureid])
INCLUDE ([id])
GO

CREATE NONCLUSTERED INDEX IDX_engineerallocation_deptid
ON [dbo].[engineerallocation] ([deptid])

CREATE NONCLUSTERED INDEX IDX_workrequirement_procedureid
ON [dbo].[workrequirement] ([procedureid])

INSERT INTO dbversion(version) VALUES(417);

COMMIT TRAN