-- Inserts VAT rate for use in Tunisia by local business company
-- Also updates settings for any existing company relationship with local business company, to use this rate
-- According to Lucie / local country manager, we are exempt from charging VAT in Tunisia
-- (perhaps as currency is set to EUR).  GB 2019-12-18
-- I'm running this script on production today once tested on test, as they need it for Tunisia soon.

USE [atlas]
GO

BEGIN TRAN

DECLARE @countryIdTN int;

select @countryIdTN = country.countryid
from country where countrycode = 'TN'

INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid],[lastModified])
     VALUES
           ('G','Tunisia',0.00,0,@countryIdTN,'2019-12-18');

DECLARE @companyIdBusinessTunisia int;

select @companyIdBusinessTunisia = company.coid
from company where countryid = @countryIdTN and corole = 5;

UPDATE [dbo].[companysettingsforallocatedcompany]
   SET [vatrate] = 'G'
 WHERE orgid = @companyIdBusinessTunisia

INSERT INTO dbversion(version) VALUES(543);

GO

COMMIT TRAN