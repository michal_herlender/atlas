USE [atlas]
GO

BEGIN TRAN


-- takes sometime to execute !

-- update the orgid (allcoated subdiv) from the fault report number (we extract the subdiv and the company code)
UPDATE dbo.faultreport
SET orgid= (

select s.subdivid
from subdiv s
join company c on s.coid = c.coid
where s.subdivcode = (select SUBSTRING(fr.faultreportnumber,
						CHARINDEX('-',fr.faultreportnumber)+1,
						charindex('-', fr.faultreportnumber, (charindex('-', fr.faultreportnumber, 1))+1)  - CHARINDEX('-',fr.faultreportnumber)-1)
						from faultreport fr where fr.faultrepid = dbo.faultreport.faultrepid) 
and (select SUBSTRING(fr.faultreportnumber, 1, CHARINDEX('-',fr.faultreportnumber)-1)
						from faultreport fr where fr.faultrepid = dbo.faultreport.faultrepid)  like CONCAT('%',c.companycode )

)
WHERE faultreportnumber like '%-%-%-%';


go

-- for the rest of cases (subdiv code missing, null/empty fault report number) default that to the orgid of the job
UPDATE dbo.faultreport
SET orgid= (
select j.orgid
from faultreport fr
join jobitem ji on fr.jobitemid = ji.jobitemid
join job j on ji.jobid = j.jobid
where fr.faultrepid = dbo.faultreport.faultrepid
)
where dbo.faultreport.orgid is null;

-- add not null constraits
ALTER TABLE dbo.faultreport ALTER COLUMN orgid int NOT NULL;

INSERT INTO dbversion(version) VALUES(554);

COMMIT TRAN