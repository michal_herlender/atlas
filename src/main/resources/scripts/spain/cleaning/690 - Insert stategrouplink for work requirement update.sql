-- Inserts a state group link for the status 'Awaiting job costing' so that if we add a new work requirement, 
-- the work requirement hook 'select-next-work-requirement' would be enabled to fire.
-- Note, we might expand this to some other statuses reached after WR completion.

USE [atlas]
GO

BEGIN TRAN

declare @stateId int;
select @stateId = stateid from dbo.itemstate where itemstate.description = 'Service completed, awaiting job costing';

-- Group ID is defined in enum as 52 - SELECT_NEXT_WORK_REQUIREMENT
-- type is not relevant but must be set

INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (52, @stateId, 'ALLOCATED_SUBDIV')
GO

INSERT INTO dbversion(version) VALUES(690);

COMMIT TRAN