USE [atlas]
GO

BEGIN TRAN

	EXEC sys.sp_rename 'dbo.job.copyreceiptdatetoitems' , 'overridedateinonjobitems', 'COLUMN';
	GO
	
	update jji set jji.agreeddeldate = null
	from jobitem jji
	where (select j.agreeddeldate from jobitem ji join job j on ji.jobid = j.jobid where ji.jobitemid = jji.jobitemid) is null;
	
	INSERT INTO dbversion(version) VALUES(677);

COMMIT TRAN

