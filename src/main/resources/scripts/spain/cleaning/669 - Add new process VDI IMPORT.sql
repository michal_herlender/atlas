USE [atlas]
GO

BEGIN TRAN

INSERT INTO dbo.calibrationprocess (actionafterissue,applicationcomponent,description,fileextension,issueimmediately,process,quicksign,signonissue,signingsupported,uploadafterissue,uploadbeforeissue)
	VALUES (0,0,'Import VDI2623 xml with the Certificate document.','.xml',0,'VDI IMPORT',0,0,0,0,1);
	
INSERT INTO dbversion(version) VALUES(669);
	


COMMIT TRAN