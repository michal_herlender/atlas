-- Script to configure additional France lab accreditations
-- Author : Galen Beck - 2018-09-20
-- Already run on France-test, Spain-test, Spain (production) to help with data initialization

USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[accreditedlab]
           ([lastModified],[labno],[description],[orgid],[accreditationbodyid])
     VALUES
	('2018-09-18','1-1741','Bordeaux - Mérignac - Etalonnage - Force - Couple',18473,2),
	('2018-09-18','1-1763','Toulouse - Etalonnage - Climatique',6965,2),
	('2018-09-18','1-2023','Rungis - Etalonnage - Climatique',6969,2),
	('2018-09-18','1-2282','Aix en Provence - Etalonnage - Climatique',6973,2),
	('2018-09-18','1-5639','Arras - Etalonnage - Force - Couple',6959,2),
	('2018-09-18','2-1579','Toulon - Etalonnage - Electricité-Magnétisme',6974,2),
	('2018-09-18','2-1668 & 2-2061','Toulouse - Etalonnage - Température - Hygrométrie',6965,2),
	('2018-09-18','2-1696','Aix en Provence - Etalonnage - Dimensionnelle',6973,2),
	('2018-09-18','2-1701','Grenoble - Etalonnage - Pression',6970,2),
	('2018-09-18','2-1703','Châtellerault - Etalonnage - Dimensionnelle',6972,2),
	('2018-09-18','2-1746','La Ciotat - Etalonnage - Dimensionnelle',18471,2),
	('2018-09-18','2-1813','Bievres - Etalonnage - Force - Couple',18467,2),
	('2018-09-18','2-1820','Bordeaux - Etalonnage - Electricité-Magnétisme',6966,2),
	('2018-09-18','2-1821','Rennes - Etalonnage - Electricité-Magnétisme',6963,2),
	('2018-09-18','2-1822','Rennes - Etalonnage - Temps-Fréquence',6963,2),
	('2018-09-18','2-1823 & 2-1824','Rungis - Etalonnage - Electricité-Magnétisme',6969,2),
	('2018-09-18','2-6187 & 2-1736','Rouen - Etalonnage - Température - Hygrométrie',18474,2),
	('2018-09-18','1-2074','Metz - Etalonnage - Climatique',6961,2);
GO

INSERT INTO dbversion(version) values(295);
INSERT INTO dbversion(version) values(296);

COMMIT TRAN