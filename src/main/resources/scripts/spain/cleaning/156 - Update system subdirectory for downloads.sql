-- Updates subdirectory for downloads to accessible Portal Downloads directory
-- GB 2017-06-07 

USE [spain]
GO

UPDATE [dbo].[systemsubdirectory]
   SET [dirname] = 'Portal Downloads'
 WHERE [componentid] = 6 and [dirname] = '/CWMS/downloads'

GO


