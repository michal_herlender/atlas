-- Author - Galen Beck - 2017-05-30 - Note, does ROLLBACK for verification first.
-- Running on test first, confirm with Ana, then on production.

-- Script to update certificate on last job item with APTO/NO APTO data from instrument records
-- This is done just for the clients that actively have a cal verification status 
-- The second part can be run / extracted separately if needed
-- Unix Timestamp - 1496160000000 - Tuesday, May 30, 2017 6:00:00 PM GMT+02:00
-- User ID - 17280 - Galen Beck

USE [spain]
GO

BEGIN TRAN;

UPDATE [dbo].[certificate]
   SET	[certificate].[calibrationverificationstatus] = [instrumentcomplementaryfield].calibrationverificationstatus,
		[certificate].log_lastmodified = '1496160000000',
		[certificate].log_userid = '17280'
   FROM [certificate] 
   LEFT JOIN [certlink] on [certificate].certid = [certlink].certid
   LEFT JOIN [jobitem] AS jitem1 on [certlink].jobitemid = jitem1.jobitemid
   LEFT JOIN [instrument] on jitem1.plantid = [instrument].plantid
   LEFT JOIN [instrumentcomplementaryfield] on [instrumentcomplementaryfield].plantid = [instrument].plantid
 WHERE 
	[certificate].calibrationverificationstatus IS NULL AND 
	[instrument].coid in (12662, 9448, 9661, 9624, 11499, 9646, 9616, 8209, 9648, 8914, 
							9759, 10221, 9628, 9617, 8334, 8949, 8683, 9192, 9670, 9674, 
							10188, 9491, 9485) AND
	jitem1.jobitemid = (SELECT TOP 1 jitem2.jobitemid FROM jobitem as jitem2 WHERE 
						jitem2.plantid = jitem1.plantid ORDER BY jitem2.jobitemid DESC) AND
	[instrumentcomplementaryfield].[calibrationverificationstatus] IS NOT NULL AND
	[instrumentcomplementaryfield].[calibrationverificationstatus] <> 'NOT_APPLICABLE';

GO

-- In testing, 225 rows updated, but more instruments with status (no job item?)

SELECT [certificate].[certid]
      ,[caldate]
      ,[certdate]
      ,[certno]
      ,[duration]
      ,[type]
      ,[caltype]
      ,[status]
      ,[certificate].[remarks]
      ,[intervalunit]
      ,[certificate].[calibrationverificationstatus]
	  ,[job].jobno
	  ,[jitem1].itemno
	  ,[jitem1].datecomplete
	  ,[jitem1].jobitemid
	  ,[instrument].plantid
	  ,(SELECT COUNT(*) FROM jobitem AS jitem3 WHERE jitem3.plantid = instrument.plantid)
  FROM [dbo].[certificate]
   LEFT JOIN [certlink] on [certificate].certid = [certlink].certid
   LEFT JOIN [jobitem] AS jitem1 on [certlink].jobitemid = jitem1.jobitemid
   LEFT JOIN [job] on jitem1.jobid = [job].jobid
   LEFT JOIN [instrument] on jitem1.plantid = [instrument].plantid
   LEFT JOIN [instrumentcomplementaryfield] on [instrumentcomplementaryfield].plantid = [instrument].plantid
  WHERE [certificate].log_lastmodified = '1496160000000' AND
		[certificate].log_userid = '17280'

GO

ROLLBACK TRAN;


