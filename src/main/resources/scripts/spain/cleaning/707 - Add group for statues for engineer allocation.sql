USE [atlas]
GO

BEGIN TRAN

DECLARE @id1 int, @id2 int;

select @id1 = s.stateid
from itemstate s where s.description like 'Awaiting repair completion report update';

select @id2 = s.stateid
from itemstate s where s.description like 'Awaiting Repair Completion Report Validation';

INSERT INTO stategrouplink (groupid,stateid,[type])
	VALUES (73,@id1,'CURRENT_SUBDIV');

INSERT INTO stategrouplink (groupid,stateid,[type])
	VALUES (73,@id2,'CURRENT_SUBDIV');



INSERT INTO dbversion VALUES (707)

COMMIT TRAN