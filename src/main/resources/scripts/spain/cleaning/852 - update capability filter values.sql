BEGIN TRAN

update capability_filter set defworkinstructionid=c.defworkinstructionid ,standardTime=c.estimatedTime, externalComments=c.description
from capability_filter cf join capability c on c.id = cf.capabilityId;

INSERT INTO dbversion(version) VALUES(852);

COMMIT TRAN
