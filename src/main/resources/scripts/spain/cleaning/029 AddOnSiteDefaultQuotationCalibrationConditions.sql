INSERT INTO [dbo].[defaultquotationcalibrationcondition] 
(conditiontext, islatest, lastModified, caltypeid) 
SELECT '',1,'2016-01-01',caltypeid 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname IN ('On-Site Accredited Calibration','On-Site Standard Calibration')