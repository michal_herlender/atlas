-- Add new column in exchangeformat table

USE [atlas]

BEGIN TRAN

ALTER TABLE  dbo.exchangeformat ADD deleted bit NOT NULL DEFAULT 0;

INSERT INTO dbversion(version) VALUES(546);

COMMIT TRAN