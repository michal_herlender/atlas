-- Adds optional fields to address table for latitude and longitude
-- Using 6 decimals after decimal place, max +/-90 for latitude, +/- 180 for longitude

USE [atlas]

BEGIN TRAN

    alter table dbo.address 
       add latitude numeric(9,6);

    alter table dbo.address 
       add longitude numeric(9,6);

	INSERT INTO dbversion(version) VALUES(499);

COMMIT TRAN