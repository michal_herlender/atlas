-- Update script for migration to new JPA auditing settings
-- You'll want to run after startup.
-- There were previously different 169/170 scripts on 2017-08-16 which won't go to production
-- Therefore, run this new script even if the old 169/170 scripts were already run on local DB copy (this should work but not verified)
-- Galen Beck - 2017-08-16
-- 
-- For Allocated entities - this script sets lastModified and lastModifiedBy, first using any old data available from Allocated
-- For Versioned entities - this script sets lastModifiedBy, first using any old data available from Allocated
--
-- Note, the log_lastmodified to lastModified conversion will have an hour offset due to DST but is presumed OK for our purposes
--
-- It's a lot of tables updated so it takes a few minutes to run.

USE [spain]

BEGIN TRAN

PRINT('--- Allocated Entities ---');

PRINT('additionalcostcontact');
UPDATE [dbo].[additionalcostcontact] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[additionalcostcontact] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('asset');
UPDATE [dbo].[asset] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[asset] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('bankdetail');
UPDATE [dbo].[bankdetail] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[bankdetail] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('batchcalibration');
UPDATE [dbo].[batchcalibration] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[batchcalibration] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('businessarea');
UPDATE [dbo].[businessarea] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[businessarea] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('callink');
UPDATE [dbo].[callink] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[callink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('certificate');
UPDATE [dbo].[certificate] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[certificate] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('certificatevalidation');
UPDATE [dbo].[certificatevalidation] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[certificatevalidation] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('certlink');
UPDATE [dbo].[certlink] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[certlink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('characteristicdescription');
UPDATE [dbo].[characteristicdescription] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[characteristicdescription] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('collectedinstrument');
UPDATE [dbo].[collectedinstrument] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[collectedinstrument] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('companyaccount');
UPDATE [dbo].[companyaccount] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[companyaccount] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('contractreview');
UPDATE [dbo].[contractreview] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[contractreview] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('contractreviewitem');
UPDATE [dbo].[contractreviewitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[contractreviewitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('costs_adjustment');
UPDATE [dbo].[costs_adjustment] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[costs_adjustment] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('costs_calibration');
UPDATE [dbo].[costs_calibration] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[costs_calibration] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('costs_inspection');
UPDATE [dbo].[costs_inspection] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[costs_inspection] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('costs_purchase');
UPDATE [dbo].[costs_purchase] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[costs_purchase] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('costs_repair');
UPDATE [dbo].[costs_repair] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[costs_repair] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('courier');
UPDATE [dbo].[courier] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[courier] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('courierdespatch');
UPDATE [dbo].[courierdespatch] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[courierdespatch] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('courierdespatchtype');
UPDATE [dbo].[courierdespatchtype] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[courierdespatchtype] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('creditcard');
UPDATE [dbo].[creditcard] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[creditcard] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('defaultstandard');
UPDATE [dbo].[defaultstandard] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[defaultstandard] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('defect');
UPDATE [dbo].[defect] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[defect] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('deliveryitem');
UPDATE [dbo].[deliveryitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[deliveryitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('departmentmember');
UPDATE [dbo].[departmentmember] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[departmentmember] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('email');
UPDATE [dbo].[email] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[email] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('emailrecipient');
UPDATE [dbo].[emailrecipient] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[emailrecipient] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('faultreport');
UPDATE [dbo].[faultreport] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[faultreport] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('faultreportaction');
UPDATE [dbo].[faultreportaction] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[faultreportaction] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('faultreportdescription');
UPDATE [dbo].[faultreportdescription] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[faultreportdescription] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('faultreportinstruction');
UPDATE [dbo].[faultreportinstruction] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[faultreportinstruction] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('freehandcontact');
UPDATE [dbo].[freehandcontact] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[freehandcontact] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hireaccessory');
UPDATE [dbo].[hireaccessory] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[hireaccessory] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hirecrossitem');
UPDATE [dbo].[hirecrossitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[hirecrossitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hireinstrument');
UPDATE [dbo].[hireinstrument] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[hireinstrument] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hireitemaccessory');
UPDATE [dbo].[hireitemaccessory] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[hireitemaccessory] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hirereplacedinstrument');
UPDATE [dbo].[hirereplacedinstrument] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[hirereplacedinstrument] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hiresuspenditem');
UPDATE [dbo].[hiresuspenditem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[hiresuspenditem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('images');
UPDATE [dbo].[images] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[images] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('imagetag');
UPDATE [dbo].[imagetag] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[imagetag] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instcertlink');
UPDATE [dbo].[instcertlink] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[instcertlink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('modelpartof');
UPDATE [dbo].[modelpartof] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[modelpartof] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instmodelsubfamily');
UPDATE [dbo].[instmodelsubfamily] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[instmodelsubfamily] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instrumentvalidationaction');
UPDATE [dbo].[instrumentvalidationaction] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[instrumentvalidationaction] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instrumentvalidationissue');
UPDATE [dbo].[instrumentvalidationissue] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[instrumentvalidationissue] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('invoiceitempo');
UPDATE [dbo].[invoiceitempo] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[invoiceitempo] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('invoicejoblink');
UPDATE [dbo].[invoicejoblink] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[invoicejoblink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobitem');
UPDATE [dbo].[jobitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobitemaction');
UPDATE [dbo].[jobitemaction] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobitemaction] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobitemgroup');
UPDATE [dbo].[jobitemgroup] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobitemgroup] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobitempo');
UPDATE [dbo].[jobitempo] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobitempo] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobitemworkrequirement');
UPDATE [dbo].[jobitemworkrequirement] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobitemworkrequirement] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobquotelink');
UPDATE [dbo].[jobquotelink] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobquotelink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('location');
UPDATE [dbo].[location] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[location] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('mailgroupmember');
UPDATE [dbo].[mailgroupmember] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[mailgroupmember] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('mfr');
UPDATE [dbo].[mfr] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[mfr] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('nominalcode');
UPDATE [dbo].[nominalcode] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[nominalcode] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('assetnote');
UPDATE [dbo].[assetnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[assetnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('certnote');
UPDATE [dbo].[certnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[certnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('creditnotenote');
UPDATE [dbo].[creditnotenote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[creditnotenote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('deliveryitemnote');
UPDATE [dbo].[deliveryitemnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[deliveryitemnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('deliverynote');
UPDATE [dbo].[deliverynote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[deliverynote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hirenote');
UPDATE [dbo].[hirenote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[hirenote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instrumentnote');
UPDATE [dbo].[instrumentnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[instrumentnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('invoicenote');
UPDATE [dbo].[invoicenote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[invoicenote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobcostingitemnote');
UPDATE [dbo].[jobcostingitemnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobcostingitemnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobcostingnote');
UPDATE [dbo].[jobcostingnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobcostingnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobitemnote');
UPDATE [dbo].[jobitemnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobitemnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobnote');
UPDATE [dbo].[jobnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('modelnote');
UPDATE [dbo].[modelnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[modelnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('purchaseorderitemnote');
UPDATE [dbo].[purchaseorderitemnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[purchaseorderitemnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('purchaseordernote');
UPDATE [dbo].[purchaseordernote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[purchaseordernote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('quoteitemnote');
UPDATE [dbo].[quoteitemnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[quoteitemnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('quotenote');
UPDATE [dbo].[quotenote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[quotenote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('recallnote');
UPDATE [dbo].[recallnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[recallnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('schedulenote');
UPDATE [dbo].[schedulenote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[schedulenote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquoteitemnote');
UPDATE [dbo].[tpquoteitemnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tpquoteitemnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquotenote');
UPDATE [dbo].[tpquotenote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tpquotenote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquoterequestitemnote');
UPDATE [dbo].[tpquoterequestitemnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tpquoterequestitemnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquoterequestnote');
UPDATE [dbo].[tpquoterequestnote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tpquoterequestnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('onbehalfitem');
UPDATE [dbo].[onbehalfitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[onbehalfitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('modeloption');
UPDATE [dbo].[modeloption] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[modeloption] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('pat');
UPDATE [dbo].[pat] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[pat] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('calibrationpoint');
UPDATE [dbo].[calibrationpoint] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[calibrationpoint] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('templatepoint');
UPDATE [dbo].[templatepoint] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[templatepoint] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('calibrationpointset');
UPDATE [dbo].[calibrationpointset] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[calibrationpointset] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('pointsettemplate');
UPDATE [dbo].[pointsettemplate] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[pointsettemplate] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('creditnoteitem');
UPDATE [dbo].[creditnoteitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[creditnoteitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hireitem');
UPDATE [dbo].[hireitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[hireitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('invoiceitem');
UPDATE [dbo].[invoiceitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[invoiceitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('purchaseorderitem');
UPDATE [dbo].[purchaseorderitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[purchaseorderitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobcostingitem');
UPDATE [dbo].[jobcostingitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[jobcostingitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('quotationitem');
UPDATE [dbo].[quotationitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[quotationitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquotationitem');
UPDATE [dbo].[tpquotationitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tpquotationitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('procedureaccreditation');
UPDATE [dbo].[procedureaccreditation] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[procedureaccreditation] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('procedureheading');
UPDATE [dbo].[procedureheading] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[procedureheading] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('purchaseorderjobitem');
UPDATE [dbo].[purchaseorderjobitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[purchaseorderjobitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('quoteheading');
UPDATE [dbo].[quoteheading] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[quoteheading] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('modelrange');
UPDATE [dbo].[modelrange] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[modelrange] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('recalldetail');
UPDATE [dbo].[recalldetail] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[recalldetail] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('recallitem');
UPDATE [dbo].[recallitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[recallitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('recallresponsenote');
UPDATE [dbo].[recallresponsenote] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[recallresponsenote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('recallrule');
UPDATE [dbo].[recallrule] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[recallrule] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('repeatschedule');
UPDATE [dbo].[repeatschedule] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[repeatschedule] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('requirement');
UPDATE [dbo].[requirement] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[requirement] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('schedule');
UPDATE [dbo].[schedule] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[schedule] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('scheduleequipment');
UPDATE [dbo].[scheduleequipment] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[scheduleequipment] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('standardused');
UPDATE [dbo].[standardused] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[standardused] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('taggedimage');
UPDATE [dbo].[taggedimage] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[taggedimage] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('dimensional_thread');
UPDATE [dbo].[dimensional_thread] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[dimensional_thread] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpinstruction');
UPDATE [dbo].[tpinstruction] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tpinstruction] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquotelink');
UPDATE [dbo].[tpquotelink] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tpquotelink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquoterequestitem');
UPDATE [dbo].[tpquoterequestitem] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tpquoterequestitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tprequirement');
DROP TRIGGER [dbo].[log_delete_tprequirement]
DROP TRIGGER [dbo].[log_update_tprequirement]
UPDATE [dbo].[tprequirement] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[tprequirement] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('transportmethod');
UPDATE [dbo].[transportmethod] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[transportmethod] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('transportoption');
UPDATE [dbo].[transportoption] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[transportoption] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('uom');
UPDATE [dbo].[uom] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[uom] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('userpreferences');
UPDATE [dbo].[userpreferences] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[userpreferences] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('vatrate');
UPDATE [dbo].[vatrate] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[vatrate] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('webresource');
UPDATE [dbo].[webresource] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[webresource] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('workinstructionversion');
UPDATE [dbo].[workinstructionversion] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[workinstructionversion] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('workrequirement');
UPDATE [dbo].[workrequirement] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[workrequirement] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('workrequirementprocedure');
UPDATE [dbo].[workrequirementprocedure] SET [lastModified] = dateadd(s, [log_lastmodified]/1000, '1970-01-01T00:00:00.000') WHERE lastModified IS NULL and log_lastmodified IS NOT NULL;
UPDATE [dbo].[workrequirementprocedure] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('--- Versioned Entities ---');

PRINT('accessory');
UPDATE [dbo].[accessory] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('accreditationbody');
UPDATE [dbo].[accreditationbody] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('accreditationbodyresource');
UPDATE [dbo].[accreditationbodyresource] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('address');
UPDATE [dbo].[address] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('bpo');
UPDATE [dbo].[bpo] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('invoicepo');
UPDATE [dbo].[invoicepo] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('po');
UPDATE [dbo].[po] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('accreditedlab');
UPDATE [dbo].[accreditedlab] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('addressplantillassettings');
UPDATE [dbo].[addressplantillassettings] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('addresssettingsforallocatedcompany');
UPDATE [dbo].[addresssettingsforallocatedcompany] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('addresssettingsforallocatedsubdiv');
UPDATE [dbo].[addresssettingsforallocatedsubdiv] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('businessemails');
UPDATE [dbo].[businessemails] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('calibration');
UPDATE [dbo].[calibration] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('calloffitem');
UPDATE [dbo].[calloffitem] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('catalogprice');
UPDATE [dbo].[catalogprice] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('companyaccreditation');
UPDATE [dbo].[companyaccreditation] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('companyinstructionlink');
UPDATE [dbo].[companyinstructionlink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('companysettingsforallocatedcompany');
UPDATE [dbo].[companysettingsforallocatedcompany] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('contactinstructionlink');
UPDATE [dbo].[contactinstructionlink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('defaultquotationcalibrationcondition');
UPDATE [dbo].[defaultquotationcalibrationcondition] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('defaultnote');
UPDATE [dbo].[defaultnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('delivery');
UPDATE [dbo].[delivery] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('emailtemplate');
UPDATE [dbo].[emailtemplate] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('creditnote');
UPDATE [dbo].[creditnote] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hire');
UPDATE [dbo].[hire] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('invoice');
UPDATE [dbo].[invoice] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobcosting');
UPDATE [dbo].[jobcosting] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('purchaseorder');
UPDATE [dbo].[purchaseorder] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('quotation');
UPDATE [dbo].[quotation] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquotation');
UPDATE [dbo].[tpquotation] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('tpquoterequest');
UPDATE [dbo].[tpquoterequest] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('hiremodel');
UPDATE [dbo].[hiremodel] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('job');
UPDATE [dbo].[job] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('numberformat');
UPDATE [dbo].[numberformat] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('numeration');
UPDATE [dbo].[numeration] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('presetcomment');
UPDATE [dbo].[presetcomment] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('procs');
UPDATE [dbo].[procs] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('quickpurchaseorder');
UPDATE [dbo].[quickpurchaseorder] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('customquotationgeneralcondition');
UPDATE [dbo].[customquotationgeneralcondition] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('defaultquotationgeneralcondition');
UPDATE [dbo].[defaultquotationgeneralcondition] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('quotationrequest');
UPDATE [dbo].[quotationrequest] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('recall');
UPDATE [dbo].[recall] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('scheduledquotationrequest');
UPDATE [dbo].[scheduledquotationrequest] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('servicecapability');
UPDATE [dbo].[servicecapability] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('subdivinstructionlink');
UPDATE [dbo].[subdivinstructionlink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('supplierinvoice');
UPDATE [dbo].[supplierinvoice] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('systemdefaultapplication');
UPDATE [dbo].[systemdefaultapplication] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('upcomingwork');
UPDATE [dbo].[upcomingwork] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('user_role');
UPDATE [dbo].[user_role] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('workinstruction');
UPDATE [dbo].[workinstruction] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('bankaccount');
UPDATE [dbo].[bankaccount] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('businesscompanysettings');
UPDATE [dbo].[businesscompanysettings] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('businesssubdivisionsettings');
UPDATE [dbo].[businesssubdivisionsettings] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('calreq');
UPDATE [dbo].[calreq] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('company');
UPDATE [dbo].[company] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('contact');
UPDATE [dbo].[contact] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('customquotationcalibrationcondition');
UPDATE [dbo].[customquotationcalibrationcondition] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('description');
UPDATE [dbo].[description] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('baseinstruction');
UPDATE [dbo].[baseinstruction] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instrument');
UPDATE [dbo].[instrument] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instmodeldomain');
UPDATE [dbo].[instmodeldomain] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instmodelfamily');
UPDATE [dbo].[instmodelfamily] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('jobinstructionlink');
UPDATE [dbo].[jobinstructionlink] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('salescategory');
UPDATE [dbo].[salescategory] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('component');
UPDATE [dbo].[component] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('instmodel');
UPDATE [dbo].[instmodel] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('subdiv');
UPDATE [dbo].[subdiv] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

PRINT('users');
UPDATE [dbo].[users] SET [lastModifiedBy] = [log_userid] WHERE [lastModifiedBy] IS NULL AND [log_userid] is not null;

COMMIT TRAN