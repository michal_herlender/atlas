-- move the state "awaiting internal return delivery note" from StateGroup.INTERNALDELIVERY to StateGroup.INTERNAL_RETURN_DELIVERY

USE [atlas]

BEGIN TRAN

	DECLARE @awaiting_internal_retrun_DN_status int
	SELECT @awaiting_internal_retrun_DN_status = stateid FROM dbo.itemstate WHERE description = 'Awaiting internal return delivery note';
	UPDATE dbo.stategrouplink SET groupid = 72 WHERE stateid = @awaiting_internal_retrun_DN_status AND groupid = 43;

	INSERT INTO dbversion(version) VALUES (695)

COMMIT TRAN