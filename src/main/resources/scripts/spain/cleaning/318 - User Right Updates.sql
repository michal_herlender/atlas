use [spain]

begin tran

alter table dbo.userrole
	add lastModified datetime2 not null;

alter table dbo.userrole
	add lastModifiedBy int;

alter table dbo.userrole
	add orgid int not null;

alter table dbo.userrole
	add constraint FKn750daq861vk3ukel92ay69sp
	foreign key (lastModifiedBy)
	references dbo.contact;

alter table dbo.userrole
	add constraint FKtod65pdsoqedleir1gw3dphfl
	foreign key (orgid)
	references dbo.subdiv;

alter table dbo.userrole
	drop constraint FK_userrole_subdiv

alter table dbo.userrole
	drop column subdiv

alter table dbo.userrole
	add constraint UK_userrole_userName_role_orgid unique (userName, role, orgid);

delete from userrole

delete from dbo.rolegroup

delete from dbo.perrole

delete from dbo.permission

delete from dbo.permissiongroup

set identity_insert dbo.permissiongroup on

insert into dbo.permissiongroup(id, name)
values
(1, 'GROUP_NEW_USER_RIGHTS'),
(2, 'GROUP_ENGINEER_MANAGEMENT'),
(3, 'GROUP_USER_RIGHT_MANAGEMENT')

set identity_insert dbo.permissiongroup off

insert into permission(groupId, permission)
values
(1, 'ADD_CAPABILITY'),
(1, 'ADD_INSTRUMENT_MODEL_SEARCH'),
(1, 'ADD_JOB'),
(1, 'ADD_REQUIREMENTS_MODEL'),
(1, 'CAPABILITY_AUTHORIZATION'),
(1, 'CLEANING_AND_INSPECTION'),
(1, 'CLEANING_LIST'),
(1, 'CREATE_QUOTE'),
(1, 'HOME_CREDIT_NOTE'),
(1, 'HOME_HIRE'),
(1, 'HOME_INVOICE'),
(1, 'HOME_JOB'),
(1, 'INSTRUMENT_DOMAIN_MODEL'),
(1, 'INSTRUMENT_FAMILY_MODEL'),
(1, 'MANAGE_PREBOOKING'),
(1, 'MODEL_EDIT'),	
(1, 'QUICK_DELIVERY_NOTE'),
(1, 'REPORTS'),
(1, 'REQUEST_QUOTE_SCHEDULED'),
(1, 'SALES_CATEGORY_MODEL'),
(1, 'SCAN_OUT'),
(1, 'SCANIN_LOGISTIC'),
(1, 'SEARCH_CALIBRATION'),
(1, 'SEARCH_CAPABILITIES'),
(1, 'SEARCH_CERTIF'),
(1, 'SEARCH_COMPANY'),
(1, 'SEARCH_CONTACT'),
(1, 'SEARCH_COURIER_DESPATCHES'),
(1, 'SEARCH_DELIVERY'),
(1, 'SEARCH_DESCRIPTION_MODEL'),
(1, 'SEARCH_EMAIL'),
(1, 'SEARCH_INSTRUMENT'),
(1, 'SEARCH_JOB'),
(1, 'SEARCH_MODEL'),
(1, 'SEARCH_THREADS_MODEL'),
(1, 'SEARCH_TRANSPORT_SCHEDULES'),
(1, 'SUPPLIER_PURCHASE_ORDER'),
(1, 'SUPPLIER_QUOTATION'),
(1, 'SUPPLIER_QUOTATION_REQUEST'),
(1, 'SUSPENDED_JOB_ITEM'),
(1, 'TIMESHEET'),		
(1, 'TOOLS'),
(1, 'VIEW_CAPABILITIES_BY_DEPARTEMENT'),
(1, 'VIEW_CAPABILITIES_BY_DOMAIN'),
(1, 'VIEW_CLIENT_GOOD_OUT'),
(1, 'VIEW_DASHBOARD_JOB'),
(1, 'VIEW_INVOICE'),
(1, 'VIEW_JOB'),
(1, 'VIEW_PURCHASE_ORDER'),
(1, 'VIEW_RECALL'),
(1, 'VIEW_SUPPLIER_GOOD_OUT'),
(1, 'VIEW_USER_TASK'),
(2, 'ALLOCATE_WORK_ENGINEER'),
(2, 'TIMESHEET_VALIDATION'),
(3, 'ADMIN_AREA'),
(3, 'PERMISSION_GROUPS'),
(3, 'PERMISSION_GROUPS_EDIT'),
(3, 'USER_ROLES'),
-- only for admins, because the link doesn't work --
(3, 'ADD_SUBFAMILY_REQUIREMENTS_MODEL')

set identity_insert dbo.perrole on

insert into dbo.perrole(id, name)
values
(1, 'ROLE_STAFF_USER'),
(2, 'ROLE_MANAGER'),
(3, 'ROLE_DIRECTOR'),
(4, 'ROLE_ADMINISTRATOR')

set identity_insert dbo.perrole off

insert into rolegroup(roleId, groupId)
values
(1, 1),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(4, 3)

GO

-- Initialise roles for existing users --

insert into userrole(userName, orgid, lastModified, role)
select distinct users.username, user_role.orgid, '2018-10-01', 1
from users
left join user_role on users.username = user_role.username
where rolename = 'ROLE_STAFF_USER'
order by username

insert into userrole(userName, orgid, lastModified, role)
select distinct users.username, user_role.orgid, '2018-10-01', 2
from users
left join user_role on users.username = user_role.username
where rolename = 'ROLE_MANAGER'
order by username

insert into userrole(userName, orgid, lastModified, role)
select distinct users.username, user_role.orgid, '2018-10-01', 3
from users
left join user_role on users.username = user_role.username
where rolename = 'ROLE_DIRECTOR'
order by username

insert into userrole(userName, orgid, lastModified, role)
select distinct users.username, user_role.orgid, '2018-10-01', 4
from users
left join user_role on users.username = user_role.username
where rolename = 'ROLE_ADMINISTRATOR'
order by username

insert into dbversion(version) values(318);

commit tran