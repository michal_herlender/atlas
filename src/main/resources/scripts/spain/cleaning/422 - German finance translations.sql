-- Updating German financial translations in database
-- Galen Beck - 2019-03-23
USE [spain]
GO

BEGIN TRAN

PRINT 'Inserting to nominalcodetitletranslation'
-- Note, not all these nominal codes exist in the ERP, it's OK for now

INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Grundst�cke und Geb�ude' FROM [dbo].[nominalcode] WHERE [code] = 'PC001';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Betriebs- und Gesch�ftsausstattung' FROM [dbo].[nominalcode] WHERE [code] = 'PC002';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Sonstige Software' FROM [dbo].[nominalcode] WHERE [code] = 'PC003';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'ERP-Software' FROM [dbo].[nominalcode] WHERE [code] = 'PC004';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - BESCHLEUNIGUNGSMESSER' FROM [dbo].[nominalcode] WHERE [code] = 'PC005';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - AKUSTISCH' FROM [dbo].[nominalcode] WHERE [code] = 'PC006';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - CHEMIE - UMWELT' FROM [dbo].[nominalcode] WHERE [code] = 'PC007';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - KLIMATISCH' FROM [dbo].[nominalcode] WHERE [code] = 'PC008';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - GASSTROM' FROM [dbo].[nominalcode] WHERE [code] = 'PC009';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - WASSERSTROM' FROM [dbo].[nominalcode] WHERE [code] = 'PC010';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - DIMENSIONAL' FROM [dbo].[nominalcode] WHERE [code] = 'PC011';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - SONSTIGE' FROM [dbo].[nominalcode] WHERE [code] = 'PC012';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - ELEKTRISCH' FROM [dbo].[nominalcode] WHERE [code] = 'PC013';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - KRAFT - DREHMOMENT' FROM [dbo].[nominalcode] WHERE [code] = 'PC014';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - INFORMATIK' FROM [dbo].[nominalcode] WHERE [code] = 'PC015';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - MASSE - GEWICHT' FROM [dbo].[nominalcode] WHERE [code] = 'PC016';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - DOM�N�BERGREIFEND' FROM [dbo].[nominalcode] WHERE [code] = 'PC017';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - DRUCK' FROM [dbo].[nominalcode] WHERE [code] = 'PC018';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - RADIOMETRY - PHOTOMETRY' FROM [dbo].[nominalcode] WHERE [code] = 'PC019';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - IONISIERENDE STRAHLUNG' FROM [dbo].[nominalcode] WHERE [code] = 'PC020';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - TEMPERATUR - HYGROMETRIE' FROM [dbo].[nominalcode] WHERE [code] = 'PC021';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Anlagen und Maschinen - VOLUMEN' FROM [dbo].[nominalcode] WHERE [code] = 'PC022';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'IT Ausstattung' FROM [dbo].[nominalcode] WHERE [code] = 'PC023';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Logistikausstattung' FROM [dbo].[nominalcode] WHERE [code] = 'PC024';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fuhrpark' FROM [dbo].[nominalcode] WHERE [code] = 'PC025';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Wasserversorgung' FROM [dbo].[nominalcode] WHERE [code] = 'PG001';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Stromversorgung' FROM [dbo].[nominalcode] WHERE [code] = 'PG002';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Gasversorgung' FROM [dbo].[nominalcode] WHERE [code] = 'PG003';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Verbrauchsmaterial - Genex' FROM [dbo].[nominalcode] WHERE [code] = 'PG004';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Software und IT-Verbrauchsmaterial' FROM [dbo].[nominalcode] WHERE [code] = 'PG005';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'B�romaterial' FROM [dbo].[nominalcode] WHERE [code] = 'PG006';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Verbrauchsmaterial - Kopierer' FROM [dbo].[nominalcode] WHERE [code] = 'PG007';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Kosten ausgelagerte Lohn- und Gehaltsabrechnung' FROM [dbo].[nominalcode] WHERE [code] = 'PG008';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Sicherheit- und �berwachungsdienste' FROM [dbo].[nominalcode] WHERE [code] = 'PG009';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Miete externe Archivierung' FROM [dbo].[nominalcode] WHERE [code] = 'PG010';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Raten Finanzleasing' FROM [dbo].[nominalcode] WHERE [code] = 'PG011';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Objektmieten' FROM [dbo].[nominalcode] WHERE [code] = 'PG012';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mieten - Mitarbeiterunterk�nfte' FROM [dbo].[nominalcode] WHERE [code] = 'PG013';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'KfZ-Leasing (langfristig)' FROM [dbo].[nominalcode] WHERE [code] = 'PG014';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mietwagen (kurzfristig)' FROM [dbo].[nominalcode] WHERE [code] = 'PG015';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mieten - B�roausstattung' FROM [dbo].[nominalcode] WHERE [code] = 'PG016';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mieten - IT Ausstattung' FROM [dbo].[nominalcode] WHERE [code] = 'PG017';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mieten - Frankiermaschinen' FROM [dbo].[nominalcode] WHERE [code] = 'PG018';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Meiten - Kopierer' FROM [dbo].[nominalcode] WHERE [code] = 'PG019';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mieten - Geb�ude' FROM [dbo].[nominalcode] WHERE [code] = 'PG020';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Wartung- und Instandhaltung Geb�ude' FROM [dbo].[nominalcode] WHERE [code] = 'PG021';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Wartung- und Instandhaltung Fuhrpark' FROM [dbo].[nominalcode] WHERE [code] = 'PG022';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Wartung- und Instandhaltung B�ro- und Gesch�ftausstattung' FROM [dbo].[nominalcode] WHERE [code] = 'PG023';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Wartung- und Instandhaltung IT soft- und hardware' FROM [dbo].[nominalcode] WHERE [code] = 'PG024';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'externe IT-Beraterkosten' FROM [dbo].[nominalcode] WHERE [code] = 'PG025';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mieten - Abfallcontainer' FROM [dbo].[nominalcode] WHERE [code] = 'PG026';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'allgemeine Dokumentation' FROM [dbo].[nominalcode] WHERE [code] = 'PG027';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Seminare, Lehrg�nge' FROM [dbo].[nominalcode] WHERE [code] = 'PG028';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Indirekte Genex Lohnkosten - Befristete Anstellung' FROM [dbo].[nominalcode] WHERE [code] = 'PG029';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Indirekte Genex Lohnkosten - Leiharbeit' FROM [dbo].[nominalcode] WHERE [code] = 'PG030';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Anwaltskosten' FROM [dbo].[nominalcode] WHERE [code] = 'PG031';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Wirtschaftspr�ferkosten' FROM [dbo].[nominalcode] WHERE [code] = 'PG032';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Anmelde- und Zulassungsgeb�hren ' FROM [dbo].[nominalcode] WHERE [code] = 'PG033';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Sonstige Geb�hren' FROM [dbo].[nominalcode] WHERE [code] = 'PG034';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Werbung und PR-Kosten' FROM [dbo].[nominalcode] WHERE [code] = 'PG035';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Messekosten' FROM [dbo].[nominalcode] WHERE [code] = 'PG036';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Zuwendungen' FROM [dbo].[nominalcode] WHERE [code] = 'PG037';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Geschenke' FROM [dbo].[nominalcode] WHERE [code] = 'PG038';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Umzugskosten' FROM [dbo].[nominalcode] WHERE [code] = 'PG039';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Kraftstoffe' FROM [dbo].[nominalcode] WHERE [code] = 'PG040';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Taxi, Parken, Mautgeb�hren' FROM [dbo].[nominalcode] WHERE [code] = 'PG041';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Bu�gelder' FROM [dbo].[nominalcode] WHERE [code] = 'PG042';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Hotel- und Bewirtungskosten' FROM [dbo].[nominalcode] WHERE [code] = 'PG043';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Einladungen' FROM [dbo].[nominalcode] WHERE [code] = 'PG044';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Umzugskosten Mitarbeiter' FROM [dbo].[nominalcode] WHERE [code] = 'PG045';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Porto' FROM [dbo].[nominalcode] WHERE [code] = 'PG046';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Telefon' FROM [dbo].[nominalcode] WHERE [code] = 'PG047';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mobilfunk' FROM [dbo].[nominalcode] WHERE [code] = 'PG048';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'IT Network / Data' FROM [dbo].[nominalcode] WHERE [code] = 'PG049';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Kosten des Geldverkehrs' FROM [dbo].[nominalcode] WHERE [code] = 'PG050';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Gewerksschaftsbeitr�ge ' FROM [dbo].[nominalcode] WHERE [code] = 'PG051';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Personalbeschaffung' FROM [dbo].[nominalcode] WHERE [code] = 'PG052';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Management Fees' FROM [dbo].[nominalcode] WHERE [code] = 'PG053';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Weiterbelastung Waren - Beschaffung NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO001';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Weiterbelastung Waren - Beschaffung EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO002';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Weiterbelastung Waren - Beschaffung EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO003';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ersatzteile f�r Bestellung NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO004';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ersatzteile f�r Kundenauftr�ge EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO005';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ersatzteile f�r Kundenauftr�ge EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO006';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'externe Lohnarbeit NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO007';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'externe Lohnarbeit EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO008';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'externe Lohnarbeit EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO009';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'interne Lohnarbeit NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO010';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'interne Fremdvergabe EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO011';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'interne Fremdvergabe EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO012';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Verpackungsmaterial' FROM [dbo].[nominalcode] WHERE [code] = 'PO013';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Weiterbelastung Dienstleistung - Beschaffung NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO014';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Weiterbelastung Dienstleistung - Beschaffung EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO015';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Weiterbelastung Dienstleistung - Beschaffung EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO016';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Equipment rentals ' FROM [dbo].[nominalcode] WHERE [code] = 'PO017';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'externe Fremdvergabe - durchlaufender Posten' FROM [dbo].[nominalcode] WHERE [code] = 'PO018';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Nebenkosten - Wasser/Abwasser' FROM [dbo].[nominalcode] WHERE [code] = 'PO019';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Nebenkosten - Stromversorgung' FROM [dbo].[nominalcode] WHERE [code] = 'PO020';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Nebenkosten - Gasversorgung' FROM [dbo].[nominalcode] WHERE [code] = 'PO021';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Einkauf - Ersatzteile NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO022';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ersatzteile EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO023';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Verbrauchsmaterial NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO024';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Verbrauchsmaterial EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO025';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Kleinteile NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO026';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Kleinteile EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO027';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Kleinteile EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO028';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Verbrauchsmaterial - Genex' FROM [dbo].[nominalcode] WHERE [code] = 'PO029';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Arbeitskleidung' FROM [dbo].[nominalcode] WHERE [code] = 'PO030';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'B�romaterial' FROM [dbo].[nominalcode] WHERE [code] = 'PO031';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Verbrauchsmaterial Kopierer' FROM [dbo].[nominalcode] WHERE [code] = 'PO032';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Sicherheit- und �berwachungsdienste' FROM [dbo].[nominalcode] WHERE [code] = 'PO033';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Miete - Archivierungsst�tte' FROM [dbo].[nominalcode] WHERE [code] = 'PO034';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Miete - Gesch�ftsgeb�ude' FROM [dbo].[nominalcode] WHERE [code] = 'PO035';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Miete - Mitarbeiterunterk�nfte' FROM [dbo].[nominalcode] WHERE [code] = 'PO036';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Miete - Referenzstandards' FROM [dbo].[nominalcode] WHERE [code] = 'PO037';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Miete/Leasing KFZ (langfristig, > 6 Mo)' FROM [dbo].[nominalcode] WHERE [code] = 'PO038';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Miete/Leasing KFZ (kurzfristig, < 6 Mo)' FROM [dbo].[nominalcode] WHERE [code] = 'PO039';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mieten - B�roausstattung' FROM [dbo].[nominalcode] WHERE [code] = 'PO040';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Meiten - Kopierer' FROM [dbo].[nominalcode] WHERE [code] = 'PO041';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Premises rates' FROM [dbo].[nominalcode] WHERE [code] = 'PO042';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Instandhaltung Geb�ude' FROM [dbo].[nominalcode] WHERE [code] = 'PO043';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Referenzstandards Kalibrierung - Wiederkehrend - NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO044';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Referenzstandards Reparturkosten - NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO045';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Referenzstandards Kalibrierung - Wiederkehrend - EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO046';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Referenzstandards Reparturkosten - EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO047';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Instandhaltung KFZ' FROM [dbo].[nominalcode] WHERE [code] = 'PO048';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Instandhaltung B�ro- und IT Austattung' FROM [dbo].[nominalcode] WHERE [code] = 'PO049';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'W�scherei Dienste' FROM [dbo].[nominalcode] WHERE [code] = 'PO050';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Miete -M�llcontainer' FROM [dbo].[nominalcode] WHERE [code] = 'PO051';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Technische Dokumentation ' FROM [dbo].[nominalcode] WHERE [code] = 'PO052';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'allgemeine Dokumentation' FROM [dbo].[nominalcode] WHERE [code] = 'PO053';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Seminare, Lehrg�nge' FROM [dbo].[nominalcode] WHERE [code] = 'PO054';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Zeitarbeiter DOE' FROM [dbo].[nominalcode] WHERE [code] = 'PO055';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Zeitarbeiter IDOE' FROM [dbo].[nominalcode] WHERE [code] = 'PO056';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Leiharbeiter DOE' FROM [dbo].[nominalcode] WHERE [code] = 'PO057';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Leiharbeiter IDOE' FROM [dbo].[nominalcode] WHERE [code] = 'PO058';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Sonstige Geb�hren' FROM [dbo].[nominalcode] WHERE [code] = 'PO059';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'P�rfungsgeb�hren - Cofrac/DAKKS' FROM [dbo].[nominalcode] WHERE [code] = 'PO060';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fracht und Transportkosten Einkauf' FROM [dbo].[nominalcode] WHERE [code] = 'PO061';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fracht und Transportkosten Einkauf' FROM [dbo].[nominalcode] WHERE [code] = 'PO062';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fracht und Transportkosten Verkauf - NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO063';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fracht und Transportkosten innerhalb NL - NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO064';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fracht und Transportkosten Ums�tze - EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO065';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fracht und Transportkosten I/C Ums�tze - EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO066';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fracht und Transportkosten I/C Ums�tze - EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO067';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Fracht und Transportkosten Ums�tze - EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO068';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Umzugskosten' FROM [dbo].[nominalcode] WHERE [code] = 'PO069';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Tankkosten' FROM [dbo].[nominalcode] WHERE [code] = 'PO070';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Taxi, Parkgeb�hren, Strafzettel' FROM [dbo].[nominalcode] WHERE [code] = 'PO071';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Buchungsgeb�hren' FROM [dbo].[nominalcode] WHERE [code] = 'PO072';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Hotel- und Bewirtungskosten' FROM [dbo].[nominalcode] WHERE [code] = 'PO073';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Einladungen - was soll das sein?' FROM [dbo].[nominalcode] WHERE [code] = 'PO074';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Umzugskosten - Mitarbeiter' FROM [dbo].[nominalcode] WHERE [code] = 'PO075';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Portokosten' FROM [dbo].[nominalcode] WHERE [code] = 'PO076';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Telefon/Fax Kosten' FROM [dbo].[nominalcode] WHERE [code] = 'PO077';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Mobilfunk' FROM [dbo].[nominalcode] WHERE [code] = 'PO078';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Gewerksschaftsbeitr�ge ' FROM [dbo].[nominalcode] WHERE [code] = 'PO079';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Recruitment Kosten Mitarbeiter' FROM [dbo].[nominalcode] WHERE [code] = 'PO080';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Einkauf - Produktionsmaterial Inland' FROM [dbo].[nominalcode] WHERE [code] = 'PO081';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Einkauf - Produktionsmaterial EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO082';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Einkauf - Produktionsmaterial EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO083';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze Kalibrierung' FROM [dbo].[nominalcode] WHERE [code] = 'S001';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Wertberichtigung Ums�tze' FROM [dbo].[nominalcode] WHERE [code] = 'S002';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze Reparaturen' FROM [dbo].[nominalcode] WHERE [code] = 'S003';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Sonstige Ums�tze' FROM [dbo].[nominalcode] WHERE [code] = 'S004';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze Ersatzteile' FROM [dbo].[nominalcode] WHERE [code] = 'S005';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze Fracht und Transport' FROM [dbo].[nominalcode] WHERE [code] = 'S006';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze IT-Dienstleistungen' FROM [dbo].[nominalcode] WHERE [code] = 'S007';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze Service Leistungen z.B. management fees' FROM [dbo].[nominalcode] WHERE [code] = 'S008';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze aus Ger�teverk�ufen' FROM [dbo].[nominalcode] WHERE [code] = 'S009';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze Vermietungen' FROM [dbo].[nominalcode] WHERE [code] = 'S010';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze Fremdvergabe' FROM [dbo].[nominalcode] WHERE [code] = 'S011';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Ums�tze eigenproduzierte G�ter' FROM [dbo].[nominalcode] WHERE [code] = 'S012';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Kalibrierung' FROM [dbo].[nominalcode] WHERE [code] = 'S901';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Justage' FROM [dbo].[nominalcode] WHERE [code] = 'S902';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Reparatur' FROM [dbo].[nominalcode] WHERE [code] = 'S903';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus sonstiges' FROM [dbo].[nominalcode] WHERE [code] = 'S904';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Ersatzteile' FROM [dbo].[nominalcode] WHERE [code] = 'S905';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Transport' FROM [dbo].[nominalcode] WHERE [code] = 'S906';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus IT' FROM [dbo].[nominalcode] WHERE [code] = 'S907';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Schulung / Audit / Fuhrpark' FROM [dbo].[nominalcode] WHERE [code] = 'S908';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Wiederverkauf' FROM [dbo].[nominalcode] WHERE [code] = 'S909';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Mieten' FROM [dbo].[nominalcode] WHERE [code] = 'S910';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'de_DE', 'Vorkasse - Ums�tze aus Auslagerung' FROM [dbo].[nominalcode] WHERE [code] = 'S911';

PRINT 'Inserting to invoicefrequencytranslation'

INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (3,'de_DE','Anfang des Monats f�r den laufenden Monat');
INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (4,'de_DE','Anfang des Quartals f�r das laufende Quartal');
INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (7,'de_DE','pro Lieferschein eine Rechnung');
INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (2,'de_DE','Sammelrechnung zum Ende des Monats f�r den abgelaufenen Monat');
INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (8,'de_DE','Sammelrechnung zum Ende des Quartals f�r den abgelaufenen Quartal');
INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (5,'de_DE','J�hrlich zum 01.12.');
INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (9,'de_DE','Vorkasse');
INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (1,'de_DE','laufende Rechnungen nach Fertigstellung der Auftr�ge');
INSERT INTO [dbo].[invoicefrequencytranslation] ([invoicefrequencyid],[locale],[translation]) VALUES (6,'de_DE','pro Bestellung eine Rechnung');

PRINT 'Inserting to paymentmodetranslation'

INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (4,'DE_de','Bankwechsel');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (3,'DE_de','Wechsel');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (1,'DE_de','Kreditkarte');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (7,'DE_de','Scheck');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (9,'DE_de','Bank Avis');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (5,'DE_de','Barzahlung');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (6,'DE_de','Dokumentenakkreditiv');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (2,'DE_de','Schuldschein');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (10,'DE_de','Kundenkarte');

PRINT 'Creating new paymentmode'

SET IDENTITY_INSERT [dbo].[paymentmode] ON
INSERT INTO [dbo].[paymentmode] ([id],[code],[description]) VALUES (12,'PC','Purchase card');
SET IDENTITY_INSERT [dbo].[paymentmode] OFF

INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (12,'en_GB','Purchase card');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (12,'fr_FR','Carte de cr�dit');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (12,'es_ES','Tarjeta de compra');
INSERT INTO [dbo].[paymentmodetranslation] ([paymentmodeid],[locale],[translation]) VALUES (12,'DE_de','Inlands-�berweisung');

INSERT INTO dbversion(version) VALUES (422);

COMMIT TRAN