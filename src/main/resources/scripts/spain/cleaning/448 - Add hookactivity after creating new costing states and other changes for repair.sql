-- GB 2019-06-02 : Moved actionoutcome insert statement before translation insert, 

USE [spain];

BEGIN TRAN

	DECLARE @stateid as int;
	DECLARE @activityid as int;

	select @activityid = iss.stateid
	from itemstate iss
	where iss.description = 'Repair costing issued to client';

	print '@activityid = '
	print @activityid

	select @stateid = iss.stateid
	from itemstate iss
	where iss.description = 'Item requires repair – awaiting costing to client';

	print '@stateid = '
	print @stateid

	INSERT INTO spain.dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES ('1','1',@activityid,4,@stateid) 
	
	DECLARE @activityid2 as int;	

	select @activityid2 = iss.stateid
	from itemstate iss
	where iss.description = 'Client decision received for repair';

	print '@activityid2 = '
	print @activityid2

	UPDATE spain.dbo.nextactivity
	SET manualentryallowed=1
	WHERE activityid = @activityid2;
	
	--Add two ActionOutcome for Activity 'Client decision received for repair'
	DECLARE @activityid3 as int, @status as int, @lookup as int, @positive_ao as int, @negative_ao as int;	

	select @activityid3 = iss.stateid
	from itemstate iss
	where iss.description = 'Client decision received for repair';

	print '@activityid3 = '
	print @activityid3

	select @status = iss.stateid
	from itemstate iss
	where iss.description = 'Awaiting rejection label';

	print '@status = '
	print @status

	select @lookup = l.id
	from lookupinstance l
	where l.[lookup] = 'Lookup_TypeOfNextWorkRequirement';

	print '@lookup = '
	print @lookup
	
	INSERT INTO dbo.stategrouplink (groupid,stateid,[type]) VALUES (3,@activityid3,'ALLOCATED_SUBDIV');

	INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,lookupid) VALUES (0,@activityid3,@lookup);
	INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,statusid) VALUES (0,@activityid3,@status);

	-- GB 2019-06-02 : Moved this insert from the end of the script to this point, as the translation inserts would fail otherwise
	-- Note the 'type' is a Hibernate discriminator value here, so is correct as lower case.

	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (1,'Costing accepted',1,@activityid3,1,'client_decision_costing','COSTS_APPROVED');

	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,'Costing rejected',0,@activityid3,1,'client_decision_costing','COSTS_REJECTED');
	
	select @positive_ao = ao.id
	from actionoutcome ao
	where ao.activityid = @activityid3 and ao.positiveoutcome = 1;

	print '@positive_ao = '
	print @positive_ao

	select @negative_ao = ao.id
	from actionoutcome ao
	where ao.activityid = @activityid3 and ao.positiveoutcome = 0;

	print '@negative_ao = '
	print @negative_ao
	
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@positive_ao,'de_DE','Auftragsbestätigung akzeptiert');
	INSERT INTO spain.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@positive_ao,'en_GB','Costing accepted');
	INSERT INTO spain.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@positive_ao,'es_ES','Coste aceptado');
	INSERT INTO spain.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@positive_ao,'fr_FR','Devis accepté');

	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@negative_ao,'de_DE','Auftragsbestätigung zurückgewiesen');
	INSERT INTO spain.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@negative_ao,'en_GB','Costing rejected');
	INSERT INTO spain.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@negative_ao,'es_ES','Coste rechazado');
	INSERT INTO spain.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@negative_ao,'fr_FR','Devis refusé');
	
	UPDATE itemstate SET actionoutcometype = 'CLIENT_DECISION_COSTING' WHERE description = 'Client decision received for repair';
	
	INSERT INTO dbversion (version) VALUES (448);

COMMIT TRAN