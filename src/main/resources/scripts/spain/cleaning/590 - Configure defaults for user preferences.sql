-- Performs two changes:
-- (a) inserts default user preferences for global vs Trescal SAS (France) users
-- (b) sets default service type usage to 1 for current Trescal SAS users (it's set to 0 for others)

USE [atlas]
GO

BEGIN TRAN

INSERT INTO [dbo].[userpreferences]
           ([jikeynavigation]
           ,[mouseactive]
           ,[mouseoverdelay]
           ,[navbarposition]
           ,[scheme]
           ,[contactid]
           ,[labelprinterid]
           ,[includeselfinreplies]
           ,[defaultJobType]
           ,[lastModified]
           ,[lastModifiedBy]
           ,[autoprintlabel]
           ,[defaultservicetypeusage])
     VALUES
           (1
           ,1
           ,0
           ,0
           ,'Global Default'
           ,null
           ,null
           ,0
           ,1
           ,'2020-02-28'
           ,17280
           ,0
           ,0),

           (1
           ,1
           ,0
           ,0
           ,'Trescal SAS Default'
           ,null
           ,null
           ,0
           ,1
           ,'2020-02-28'
           ,17280
           ,0
           ,1);

DECLARE @userpreferences_global INT;
DECLARE @userpreferences_france INT;

SELECT @userpreferences_global = id FROM dbo.userpreferences WHERE [scheme] = 'Global Default';
SELECT @userpreferences_france = id FROM dbo.userpreferences WHERE [scheme] = 'Trescal SAS Default';

INSERT INTO [dbo].[userpreferencesdefault]
           ([globaldefault]
           ,[addressid]
           ,[coid]
           ,[subdivid]
           ,[userpreferencesid])
     VALUES
           (1
           ,null
           ,null
           ,null
           ,@userpreferences_global),
           (0
           ,null
           ,6690
           ,null
           ,@userpreferences_france);
GO

UPDATE [dbo].[userpreferences] 
   SET [defaultservicetypeusage] = 1
   FROM [userpreferences] up
   INNER JOIN contact on up.contactid = contact.personid
   INNER JOIN subdiv on contact.subdivid = subdiv.subdivid
 WHERE subdiv.coid = 6690
GO

INSERT INTO dbversion(version) VALUES(590);

COMMIT TRAN
