USE [spain]
GO

BEGIN TRAN

/****** Object:  Index [IDX_engineerallocation_allocatedbyid]    Script Date: 2/13/2019 10:03:52 AM ******/
DROP INDEX [IDX_engineerallocation_allocatedbyid] ON [dbo].[engineerallocation]
GO
/****** Object:  Index [IDX_engineerallocation_allocatedtoid]    Script Date: 2/13/2019 10:05:49 AM ******/
DROP INDEX [IDX_engineerallocation_allocatedtoid] ON [dbo].[engineerallocation]
GO
/****** Object:  Index [IDX_engineerallocation_jobitemid]    Script Date: 2/13/2019 10:06:26 AM ******/
DROP INDEX [IDX_engineerallocation_jobitemid] ON [dbo].[engineerallocation]
GO

ALTER TABLE dbo.engineerallocation ALTER COLUMN allocatedbyid int not null;
ALTER TABLE dbo.engineerallocation ALTER COLUMN allocatedtoid int not null;
ALTER TABLE dbo.engineerallocation ALTER COLUMN jobitemid int not null;
ALTER TABLE dbo.engineerallocation ALTER COLUMN deptid int not null;
ALTER TABLE dbo.engineerallocation ALTER COLUMN timeofday varchar(255) not null;
ALTER TABLE dbo.engineerallocation ALTER COLUMN timeofdayuntil varchar(255) not null;
GO

/****** Object:  Index [IDX_engineerallocation_allocatedbyid]    Script Date: 2/13/2019 10:03:52 AM ******/
CREATE NONCLUSTERED INDEX [IDX_engineerallocation_allocatedbyid] ON [dbo].[engineerallocation]
(
	[allocatedbyid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_engineerallocation_allocatedtoid]    Script Date: 2/13/2019 10:05:49 AM ******/
CREATE NONCLUSTERED INDEX [IDX_engineerallocation_allocatedtoid] ON [dbo].[engineerallocation]
(
	[allocatedtoid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_engineerallocation_jobitemid]    Script Date: 2/13/2019 10:06:26 AM ******/
CREATE NONCLUSTERED INDEX [IDX_engineerallocation_jobitemid] ON [dbo].[engineerallocation]
(
	[jobitemid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-- In production, there were 7 messages from 2017 with no setby

UPDATE dbo.engineerallocationmessage SET setbyid = 17280 WHERE setbyid is null;
GO

/****** Object:  Index [IDX_engineerallocationmessage_setbyid]    Script Date: 2/13/2019 10:05:01 AM ******/
DROP INDEX [IDX_engineerallocationmessage_setbyid] ON [dbo].[engineerallocationmessage]
GO

ALTER TABLE dbo.engineerallocationmessage ALTER COLUMN setbyid int not null;

/****** Object:  Index [IDX_engineerallocationmessage_setbyid]    Script Date: 2/13/2019 10:05:01 AM ******/
CREATE NONCLUSTERED INDEX [IDX_engineerallocationmessage_setbyid] ON [dbo].[engineerallocationmessage]
(
	[setbyid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT INTO dbversion(version) VALUES(401);
GO

COMMIT TRAN

