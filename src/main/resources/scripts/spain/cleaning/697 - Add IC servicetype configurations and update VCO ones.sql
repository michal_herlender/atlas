USE [atlas]

BEGIN TRAN

	UPDATE dbo.jobtypeservicetype SET defaultvalue = 0 WHERE jobtypeid = 1 AND
	servicetypeid IN (SELECT servicetypeid FROM dbo.servicetype WHERE shortname IN ('VCO','QUAL','VAL'));
	
	UPDATE dbo.jobtypecaltype SET defaultvalue = 0 WHERE jobtypeid = 1 AND
	caltypeid IN (SELECT ct.caltypeid FROM dbo.servicetype st JOIN dbo.calibrationtype ct 
	ON st.servicetypeid = ct.servicetypeid WHERE st.shortname IN ('VCO','QUAL','VAL'));
	
	DECLARE @ic_servicetype INT,
			@ic_caltype INT;
	
	SELECT @ic_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'IC';
	SELECT @ic_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @ic_servicetype;
	
	INSERT INTO dbo.jobtypeservicetype(defaultvalue,jobtypeid,servicetypeid)
	VALUES (0,1,@ic_servicetype);
	
	IF NOT EXISTS
	(
	    SELECT * FROM dbo.jobtypecaltype WHERE jobtypeid = 1 AND caltypeid = @ic_caltype
	)
	BEGIN
		INSERT INTO dbo.jobtypecaltype(defaultvalue,jobtypeid,caltypeid)
		VALUES (0,1,@ic_caltype);
	END;
	
	INSERT INTO dbo.calibrationaccreditationlevel
	(accredlevel, caltypeid)
	VALUES('APPROVE', @ic_caltype),
		  ('PERFORM', @ic_caltype),
		  ('REMOVED', @ic_caltype),
	      ('SUPERVISED', @ic_caltype);

	INSERT INTO dbversion(version) VALUES (697)

COMMIT TRAN