USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.FreeRepairComponent ADD source varchar(100);

ALTER TABLE dbo.repaircompletionreport ADD internalCompletionComments nvarchar(2000);
ALTER TABLE dbo.repaircompletionreport ADD externalCompletionComments nvarchar(2000);

ALTER TABLE dbo.freerepairoperation ADD repaircompletionreportid int;
ALTER TABLE dbo.freerepairoperation ADD CONSTRAINT freerepairoperation_repaircompletionreport_FK
	FOREIGN KEY (repaircompletionreportid) REFERENCES dbo.repaircompletionreport(id);

INSERT INTO dbversion (version) VALUES (432);

COMMIT TRAN