-- Create a new outcome status for JobItemAtInstrumentAddress loolkup

USE [atlas]
GO
BEGIN TRAN

DECLARE @itemstatus int;
DECLARE @outcomestatus int;
DECLARE @lookup int;

-- create new status 
INSERT INTO dbo.itemstate([type], active, description, retired) 
VALUES ('workstatus', 1, 'Job item completed at same address as instrument address', 0)
SELECT @itemstatus = MAX(stateid) FROM dbo.itemstate;
-- translations
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus,'de_DE','Der Auftrag wurde an derselben Adresse wie die Geräteadresse ausgeführt');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus,'en_GB','Job item completed at same address as instrument address');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus,'es_ES','Elemento de trabajo completado en la misma dirección que la dirección del instrumento');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@itemstatus,'fr_FR','Article de job terminé à la même adresse que l''adresse de l''instrument');

-- create a new outcomes
INSERT INTO dbo.outcomestatus(activityid,defaultoutcome,lookupid,statusid)
VALUES (NULL,0,NULL,@itemstatus);
SELECT @outcomestatus = MAX(id) FROM dbo.outcomestatus;

-- get the id of Lookup_JobItemAtInstrumentAddress
SET @lookup = (SELECT id FROM dbo.lookupinstance WHERE lookup like 'Lookup_JobItemAtInstrumentAddress');

-- change the outcomestatus for lookupresult of lookup 59 
UPDATE dbo.lookupresult 
SET outcomestatusid = @outcomestatus
WHERE result='a' AND lookupid = @lookup

INSERT INTO dbversion(version) VALUES (598);
COMMIT TRAN