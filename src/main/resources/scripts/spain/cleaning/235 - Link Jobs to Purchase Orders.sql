USE spain

BEGIN TRAN

	ALTER TABLE purchaseorder ADD jobId INT

	GO

	ALTER TABLE dbo.purchaseorder 
        ADD CONSTRAINT FK_purchaseorder_job 
        FOREIGN KEY (jobId) 
        REFERENCES dbo.job
	
	GO
	
	UPDATE purchaseorder SET purchaseorder.jobId = job.jobid
	FROM job WHERE job.jobno = purchaseorder.jobno
	
	INSERT INTO dbversion (version) VALUES (235);
	
COMMIT TRAN