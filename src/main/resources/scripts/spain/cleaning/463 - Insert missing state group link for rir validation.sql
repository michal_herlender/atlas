
USE [spain]
GO

BEGIN TRAN

DECLARE @stateid int,
		@stateid_2 int,
		@stateid_3 int,
		@stateid_4 int,
		@stateid_5 int;


SELECT @stateid = stateid FROM dbo.itemstate WHERE description = 'Awaiting Repair Inspection Report Validation by Technician';
SELECT @stateid_2 = stateid FROM dbo.itemstate WHERE description = 'Awaiting Repair Inspection Report Validation by CSR';
SELECT @stateid_3 = stateid FROM dbo.itemstate WHERE description = 'Work completed Awaiting final Costing, switch to Repair Job';
SELECT @stateid_4 = stateid FROM dbo.itemstate WHERE description = 'Costing issued for repair - awaiting client decision for repair';
SELECT @stateid_5 = stateid FROM dbo.itemstate WHERE description = 'Awaiting Repair Completion Report Validation';


INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (36,@stateid,'CURRENT_SUBDIV'),
           (32,@stateid_2,'ALLOCATED_SUBDIV'),
           (32,@stateid_3,'ALLOCATED_SUBDIV'),
           (32,@stateid_4,'ALLOCATED_SUBDIV'),
           (32,@stateid_5,'ALLOCATED_SUBDIV');
           
INSERT INTO dbversion(version) VALUES(463);

COMMIT TRAN