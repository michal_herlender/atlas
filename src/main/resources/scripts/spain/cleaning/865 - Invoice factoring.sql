BEGIN TRAN

ALTER TABLE companysettingsforallocatedcompany ADD invoiceFactoring BIT NOT NULL DEFAULT 0

ALTER TABLE bankaccount ADD includeOnFactoredInvoices BIT NOT NULL DEFAULT 0, includeOnNonFactoredInvoices BIT NOT NULL DEFAULT 1

INSERT INTO dbversion VALUES (865)

COMMIT TRAN