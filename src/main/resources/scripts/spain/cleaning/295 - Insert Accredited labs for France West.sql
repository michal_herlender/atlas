-- Script to configure France lab accreditations for western France locations
-- Author : Galen Beck - 2018-09-19
-- ALready run on France-test, Spain-test, Spain (production) to help with data initialization

USE [spain]
GO

BEGIN TRAN

-- Note, was using ENAC logo for France (as demo) until proper COFRAC logo is obtained
-- This record should be updated to point to the new logo (provided in next commit)

UPDATE [dbo].[accreditationbodyresource]
	SET [resourcePath] = 'labels/birt/callogo_COFRAC.jpg' WHERE accreditationbodyid = 2;
GO

-- Delete 3 bad definitions from france-test only

DELETE FROM [dbo].[accreditedlab] WHERE labno = '2-1317' and orgid = 6969;
DELETE FROM [dbo].[accreditedlab] WHERE labno = '2-1419' and orgid = 6969;
DELETE FROM [dbo].[accreditedlab] WHERE labno = '1-2074' and orgid = 6969;
GO

INSERT INTO [dbo].[accreditedlab]
           ([lastModified],[labno],[description],[orgid],[accreditationbodyid])
     VALUES
	('2018-09-18','2-1775','Belfort - Etalonnage - Pression',18466,2),
	('2018-09-18','2-1777','Belfort - Etalonnage - Métrologie dimensionnelle',18466,2),
	('2018-09-18','2-1698','Arras - Etalonnage - Métrologie Dimensionnelle',6959,2),
	('2018-09-18','2-1736','Rouen - Etalonnage - Température',18474,2),
	('2018-09-18','2-6187','Rouen - Etalonnage - Hygrométrie',18474,2),
	('2018-09-18','2-1284','Roissy - Etalonnage - Electricité-Magnétisme',18475,2),
	('2018-09-18','2-1357','Roissy - Etalonnage - Temps-Fréquence',18475,2),
	('2018-09-18','2-1754','Roissy - Etalonnage - Température',18475,2);
GO

COMMIT TRAN