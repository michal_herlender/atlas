USE [atlas]
GO

BEGIN TRAN

ALTER TABLE quotationdocdefaulttext ALTER COLUMN body NVARCHAR(MAX)
GO

UPDATE quotationdocdefaulttext
SET body = 'Sehr geehrte Damen und Herren,

wir bedanken uns f�r Ihre Anfrage zur Werkskalibrierung Ihrer Pr�fmittel und unterbreiten Ihnen nachfolgend unser Angebot.

Das Angebot wurde auf der Basis der uns vorliegenden Informationen erstellt. Eventuell auftretende Abweichungen preislicher Art k�nnen die Folge von fehlenden bzw. nicht exakten Angaben zum Kalibriergegenstand sein.

Das Angebot beinhaltet die Kalibrierung einschlie�lich Erstellung eines Protokolls in elektronischer Form (PDF-Datei) pro Pr�fmittel. Selbstverst�ndlich �bergeben wir Ihnen auf Wunsch weiterhin die Kalibrierscheine als Papierdokument. F�r diesen Service werden wir jedoch 1,00 � pro Werkskalibrierschein bzw. 3,00 � pro DAkkS-Kalibrierschein in Rechnung stellen.
Auf dem Pr�fprotokoll sind Soll- und Ist-Werte, Ident-Nr., Bezugsnormale (benutzte Messger�te), die Messunsicherheit und ein abschlie�ender Pr�fentscheid ausgewiesen.

Es k�nnen weitere Kosten f�r das Versiegeln von Lehren, bzw. f�r das Aufbringen einer Ident-Nr. (Gravur) entstehen, sofern das gew�nscht wird.

Erforderliche Justagen und Reparaturen werden nach R�cksprache mit dem Auftraggeber durchgef�hrt und nach Zeitaufwand berechnet.

F�r Kooperationsleistungen ist eine Preisbindung der Angebotspreise f�r 3 Monate gew�hrleistet.

Sollte der Auftragswert unter 50,- EUR liegen, wird ein Mindermengenzuschlag berechnet. Bei einer Vor-Ort Kalibrierung liegt der Mindestauftragswert bei 1.100 � (inkl. der Nebenkosten).

Auf Wunsch k�nnen die kalibrierten Pr�fmittel mit einer Pr�fplakette versehen werden (kostenlos), darauf ist der Termin der n�chsten Kalibrierung ersichtlich. 
Pr�fmittel die au�erhalb der Toleranz liegen erhalten ebenfalls eine eindeutige Kennzeichnung mittels Aufkleber.

Unsere Standarddurchlaufzeit richtet sich nach dem Auftragsumfang der angelieferten Pr�fmittel und betr�gt etwa 5-10 Arbeitstage.
Zu den genauen Lieferzeiten oder zur konkreten Terminabstimmung setzen Sie sich bitte mit Ihrer betreuenden Niederlassung in Verbindung. 
Dringend ben�tigte Einzelst�cke k�nnen nach vorheriger Absprache auch innerhalb von 48 Stunden kalibriert und zur R�cklieferung gebracht werden (Expresskalibrierung, gegen Aufpreis).

Bei Abschluss eines Rahmendienstleistungsvertrages gew�hren wir Ihnen einen Rabatt von 3 % auf die Nettokalibrierpreise.
          
Es gelten die Allgemeinen Gesch�ftsbedingungen der Trescal GmbH in Ihrer derzeit g�ltigen Fassung, sofern nichts anders schriftlich vereinbart wurde. Sollten Ihnen die Allgemeinen Gesch�ftsbedingungen nicht vorliegen, k�nnen Sie diese jederzeit anfordern oder einsehen auf der Trescal-Website: http://www.trescal.de

Das Angebot ist 3 Monate g�ltig.

Sollten sich im Zusammenhang mit unserem Angebot detaillierte Fragen f�r Sie ergeben, stehen wir Ihnen gern f�r weitere Ausk�nfte zur Verf�gung.

Wir w�rden uns freuen, wenn Ihnen unser Angebot zusagt und wir f�r Sie arbeiten d�rfen.

Im Auftragsfall bitten wir Sie, in Ihren Bestellungen und Lieferscheinen unbedingt unsere Angebotsnummer anzugeben.

Mit freundlichen Gr��en'

WHERE locale= 'de_DE'

UPDATE actionoutcometranslation SET translation = 'Kunde gibt Angebot f�r Fremdvergabe frei' WHERE translation LIKE 'Kunde best�tigt Angebot des Fremddienstleisters'

INSERT INTO dbversion VALUES (593)

COMMIT TRAN