USE [spain]

BEGIN TRAN

    alter table dbo.instrumentcomplementaryfield
       add customeracceptancecriteria varchar(255);

    GO

    INSERT INTO dbversion(version) VALUES(323);

COMMIT TRAN