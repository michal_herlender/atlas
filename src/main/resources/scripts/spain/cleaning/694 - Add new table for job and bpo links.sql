USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.bpo DROP CONSTRAINT FK_bpo_job;
	ALTER TABLE dbo.bpo DROP COLUMN jobid;

	ALTER TABLE dbo.job DROP CONSTRAINT FK_job_default_bpo;
	ALTER TABLE dbo.job DROP COLUMN defaultbpoid;

	CREATE TABLE dbo.jobbpolink(
		id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		bpoid INT NOT NULL,
		jobid INT NOT NULL,
		defaultforjob BIT NOT NULL,
		lastModifiedBy INT NULL,
		lastModified DATETIME2 NULL,
		CONSTRAINT FK_jobbpolink_bpo FOREIGN KEY (bpoid) REFERENCES dbo.bpo(poid),
		CONSTRAINT FK_jobbpolink_job FOREIGN KEY (jobid) REFERENCES dbo.job(jobid),
		CONSTRAINT FK_jobbpolink_contact FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid)
	);

	-- insert jobbpolink from job.bpoid (default)
	INSERT INTO dbo.jobbpolink(bpoid,jobid,defaultforjob)
	SELECT DISTINCT bpoid,jobid,1 FROM dbo.job 
	WHERE bpoid IS NOT NULL

	-- insert jobbpolink from jobitempo (bpoid IS NOT NULL)
	INSERT INTO dbo.jobbpolink(bpoid,jobid,defaultforjob)
	SELECT DISTINCT jip.bpoid,ji.jobid,0 FROM dbo.jobitempo jip 
	join dbo.jobitem ji ON jip.jobitemid = ji.jobitemid
	WHERE jip.bpoid IS NOT NULL
	AND (SELECT COUNT(*) FROM dbo.jobbpolink WHERE bpoid = jip.bpoid AND jobid = ji.jobid AND defaultforjob = 1) = 0
	
	-- we don't need this part for the moment (delete bpoid on job TABLE)
	--DECLARE @bpoid_fk_constraint_name VARCHAR(100);
	--SELECT @bpoid_fk_constraint_name = CONSTRAINT_NAME
	--FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
	--WHERE TABLE_NAME = 'job' AND COLUMN_NAME = 'bpoid';
	--EXECUTE ('ALTER TABLE dbo.job DROP CONSTRAINT '+@bpoid_fk_constraint_name);
	--ALTER TABLE dbo.job DROP COLUMN bpoid;

	INSERT INTO dbversion(version) VALUES(694);

COMMIT TRAN