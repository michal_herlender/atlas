USE SPAIN;

BEGIN TRAN;

ALTER TABLE dbo.instrumentcomplementaryfield
  ADD certclass VARCHAR(255);


INSERT INTO dbversion (version) VALUES (201);

COMMIT TRAN;