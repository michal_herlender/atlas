USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.certificate ADD supplementarycomments varchar(1000) NULL 
GO

INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
VALUES ('certificate',0,'To be replaced',0,0,0,1,1,0) 


declare @id1 int;
select top 1 @id1 = s.statusid
from dbo.basestatus s
order by s.statusid desc;


INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
VALUES ('certificate',0,'Replaced',0,0,0,1,0,0)


declare @id2 int;
select top 1 @id2 = s.statusid
from dbo.basestatus s
order by s.statusid desc;


-- translations
INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@id1,'en_GB','To be Replaced');

INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@id1,'es_ES','Ser remplazado');

INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@id1,'fr_FR','À remplacer');

INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@id1,'de_DE','Ersetzt werden');

INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@id2,'en_GB','Replaced');

INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@id2,'es_ES','Reemplazado');

INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@id2,'fr_FR','Remplacé');

INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@id2,'de_DE','Ersetzt');



INSERT INTO dbversion(version) VALUES(482);

COMMIT TRAN