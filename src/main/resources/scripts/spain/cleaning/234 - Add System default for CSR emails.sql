BEGIN TRAN

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
  (33
    , 'boolean'
    , 'Defines if default CSR for this client will receive emails relating to customer activity on portal'
    , 'Automatic emails to CSR'
    , 'false', 0, 0, 1, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (33, 'en_GB', 'Defines if default CSR for this client will receive emails relating to customer activity on portal'),
  (33, 'en_ES', 'Define si la CSR predeterminada para este cliente recibirá correos electrónicos relacionados con la actividad del cliente en el portal'),
  (33, 'fr_FR', 'Définit si la RSE par défaut pour ce client recevra des courriels relatifs à l''activité du client sur le portail');

-- Enables system default to be configurable at only company level

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0, 33);
GO

-- Enables for clients only

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (1, 33);

-- Add translations

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (33, 'en_GB', 'Automatic emails to CSR'),
  (33, 'es_ES', 'Correos electrónicos automáticos a CSR'),
  (33, 'fr_FR', 'Courriels automatiques à CSR');
GO

INSERT INTO dbversion (version) VALUES (234);

-- Change to COMMIT when successfully tested

COMMIT TRAN