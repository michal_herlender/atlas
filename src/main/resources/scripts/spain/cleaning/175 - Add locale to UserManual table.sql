﻿-- Script 175 - Galen Beck - 2017-10-04
-- Inserts a not-nullable locale column in the usermanual table
-- After this, on production we'll need to run script 141 (insert new user manuals), 
-- looks like it never ran successfully.

USE [spain]
BEGIN TRAN

    alter table dbo.usermanual 
        add locale varchar(255);

    alter table dbo.usermanual 
        add filename varchar(255);

	GO

	update dbo.usermanual
		set [locale] = 'es_ES' where [locale] is null;

	update dbo.usermanual
		set [filename] = 'usermanual.pdf' where [filename] is null;

	alter table dbo.usermanual 
		alter column locale varchar(255) not null;

	alter table dbo.usermanual 
		alter column filename varchar(255) not null;

COMMIT TRAN;