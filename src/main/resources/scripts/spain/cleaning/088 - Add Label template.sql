DELETE FROM [dbo].[labeltemplateadditionalfieldtranslation]
DELETE FROM [dbo].[labeltemplateadditionalfield]
DELETE FROM [dbo].[labeltemplate]

SET IDENTITY_INSERT [dbo].[labeltemplate] ON 
INSERT [dbo].[labeltemplate] ([id], [labeltemplatetype], [name], [propertyName]) VALUES (1, N'STANDARD', N'STANDARD template', N'label_standard')
INSERT [dbo].[labeltemplate] ([id], [labeltemplatetype], [name], [propertyName]) VALUES (2, N'ALTESIS', N'ALTESIS template', N'label_altesis')
INSERT [dbo].[labeltemplate] ([id], [labeltemplatetype], [name], [propertyName]) VALUES (3, N'ORKLI', N'ORKLI template', N'label_orkli')
INSERT [dbo].[labeltemplate] ([id], [labeltemplatetype], [name], [propertyName]) VALUES (4, N'AERO', N'AERO template', N'label_aero')
INSERT [dbo].[labeltemplate] ([id], [labeltemplatetype], [name], [propertyName]) VALUES (5, N'TALGO', N'TALGO template', N'label_talgo')
SET IDENTITY_INSERT [dbo].[labeltemplate] OFF

SET IDENTITY_INSERT [dbo].[labeltemplateadditionalfield] ON 
INSERT [dbo].[labeltemplateadditionalfield] ([id], [fieldname], [name], [labeltemplateid]) VALUES (1, N'technician', N'Techician', 4)
INSERT [dbo].[labeltemplateadditionalfield] ([id], [fieldname], [name], [labeltemplateid]) VALUES (2, N'correction', N'Correction', 2)
INSERT [dbo].[labeltemplateadditionalfield] ([id], [fieldname], [name], [labeltemplateid]) VALUES (3, N'incertainty', N'Incertainty', 2)
INSERT [dbo].[labeltemplateadditionalfield] ([id], [fieldname], [name], [labeltemplateid]) VALUES (4, N'exptrescal', N'Exp. Trescal', 5)
INSERT [dbo].[labeltemplateadditionalfield] ([id], [fieldname], [name], [labeltemplateid]) VALUES (5, N'icp', N'ICP', 5)
INSERT [dbo].[labeltemplateadditionalfield] ([id], [fieldname], [name], [labeltemplateid]) VALUES (6, N'quality', N'Quality', 5)
SET IDENTITY_INSERT [dbo].[labeltemplateadditionalfield] OFF

INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (1, N'en_GB', N'Technician')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (1, N'es_ES', N'Tecnico')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (1, N'fr_FR', N'Techicien')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (2, N'en_GB', N'Correction')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (2, N'es_ES', N'Correccion')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (2, N'fr_FR', N'Correction')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (3, N'en_GB', N'Incertainty')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (3, N'es_ES', N'Incertidumbre')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (3, N'fr_FR', N'Incertitude')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (4, N'en_GB', N'Exp.Trescal')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (4, N'es_ES', N'Exp.Trescal')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (4, N'fr_FR', N'Exp.Trescal')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (5, N'en_GB', N'ICP')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (5, N'es_ES', N'ICP')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (5, N'fr_FR', N'ICP')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (6, N'en_GB', N'Quality')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (6, N'es_ES', N'Qualidad')
INSERT [dbo].[labeltemplateadditionalfieldtranslation] ([id], [locale], [translation]) VALUES (6, N'fr_FR', N'Qualit�')
