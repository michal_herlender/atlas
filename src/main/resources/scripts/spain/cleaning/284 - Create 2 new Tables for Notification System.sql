-- Script 283
-- Create two Tables for Notification System

USE [spain]
GO

BEGIN TRAN

CREATE TABLE dbo.notificationsystemqueue (
	id int NOT NULL IDENTITY(1,1),
	fieldname varchar(100),
	fieldtype varchar(32),
	fieldvalue varchar(100),
	entityclass varchar(64),
	operationtype varchar(16),
	plantid int,
	issent bit,
	senton datetime,
	isreceived bit,
	receivedon datetime,
	lasterrormessage varchar(200),
	lasterrormessageon datetime
) 
GO

ALTER TABLE dbo.notificationsystemqueue ADD CONSTRAINT notificationsystemqueue_PK PRIMARY KEY (id) 
GO

ALTER TABLE dbo.notificationsystemqueue ADD CONSTRAINT notificationsystemqueue_instrument_FK FOREIGN KEY (plantid) REFERENCES dbo.instrument(plantid) 
GO

CREATE TABLE dbo.notificationsystemfieldchange (
	id int NOT NULL IDENTITY(1,1),
	fieldname varchar(32),
	fieldtype varchar(32),
	oldvalue varchar(32),
	newvalue varchar(32),
	notificationid int NOT NULL,
	CONSTRAINT notificationsystemfieldchange_PK PRIMARY KEY (id),
	CONSTRAINT notificationsystemfieldchange_notificationsystemqueue_FK FOREIGN KEY (notificationid) REFERENCES dbo.notificationsystemqueue(id) ON DELETE CASCADE ON UPDATE CASCADE
) 
GO

INSERT INTO dbversion(version) VALUES(284);

COMMIT TRAN