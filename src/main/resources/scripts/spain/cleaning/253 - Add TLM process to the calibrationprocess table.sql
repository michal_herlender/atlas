USE [spain]
GO

BEGIN TRAN

	SET IDENTITY_INSERT dbo.calibrationprocess ON
	
	go

	INSERT INTO dbo.calibrationprocess (id,actionafterissue,applicationcomponent,description,fileextension,issueimmediately,process,quicksign,signonissue,signingsupported,uploadafterissue,uploadbeforeissue)
	VALUES (6,0,0,'TLM generates the Certificate document.','.pdf',0,'TLM',0,0,0,0,1) 
	
	go
	
	SET IDENTITY_INSERT dbo.calibrationprocess OFF
	
	go

	insert into dbversion(version) values(253);

COMMIT TRAN