USE [spain]
GO

BEGIN TRAN

	ALTER TABLE spain.dbo.jobitem ADD clientRef varchar(100) 
	go
	
	ALTER TABLE spain.dbo.asnitem ADD clientRef varchar(100) 
	go

	INSERT INTO dbversion(version) VALUES(404);
	GO

COMMIT TRAN