-- Add an item deleted in the job delivery note
USE [atlas]

 

BEGIN TRAN

 

DECLARE 
       @new_activity1 INT,
       @new_activity2 INT,
       @new_activity3 INT,
       @new_activity4 INT,
       @new_activity5 INT,
       @hk INT,
       @lookup1 INT,
       @lookup2 INT,
       @lookup3 INT,
       @lookup4 INT,
       @stateid1 INT,
       @stateid2 INT,
       @stateid3 INT,
       @stateid4 INT,
       @stateid5 INT,
       @stateid6 INT,
       @stateid7 INT;

 

SET @lookup1=(SELECT id FROM dbo.lookupinstance WHERE lookup= 'Lookup_PreDeliveryRequirements (before lab calibration)');
SET @lookup2=(SELECT id FROM dbo.lookupinstance WHERE lookup= 'Lookup_PreDeliveryRequirements (after lab calibration - after costing check)');
SET @lookup3=(SELECT id FROM dbo.lookupinstance WHERE lookup= 'Lookup_CompanyIsOnStop (inhouse return to client from this subdivision)');
SET @lookup4=(SELECT id FROM dbo.lookupinstance WHERE lookup= 'Lookup_ItemAtTP (third party after PO creation)');

 

-- create a new activities 
INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Item deleted from internal delivery note',1,0,'workactivity',@lookup1,NULL);
SELECT @new_activity1 = MAX(stateid) FROM itemstate;

 

INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Item deleted from internal return delivery note',1,0,'workactivity',@lookup2,NULL);
SELECT @new_activity2 = MAX(stateid) FROM itemstate;

 

INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Item deleted from delivery note',1,0,'workactivity',@lookup3,NULL);
SELECT @new_activity3 = MAX(stateid) FROM itemstate;

 

INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Item deleted from Third Party delivery note',1,0,'workactivity',NULL,NULL);
SELECT @new_activity4 = MAX(stateid) FROM itemstate;

 

INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Item deleted from Third Party delivery note after PO',1,0,'workactivity',@lookup4,NULL);
SELECT @new_activity5 = MAX(stateid) FROM itemstate;

 

-- add translations
INSERT INTO dbo.itemstatetranslation(stateid,locale,translation)
 VALUES    (@new_activity1, 'fr_FR', 'Article supprim� du bon de livraison interne'),
        (@new_activity1, 'en_GB', 'Item deleted from internal delivery note'),
        (@new_activity1, 'es_ES', 'Art�culo eliminado del albar�n interno'),
        (@new_activity1, 'de_DE', 'Artikel vom internen Lieferschein gel�scht'),

 

        (@new_activity2, 'fr_FR', 'Article supprim� du bordereau de livraison interne'),
        (@new_activity2, 'en_GB', 'Item deleted from internal return delivery note'),
        (@new_activity2, 'es_ES', 'Art�culo eliminado del albar�n de devoluci�n interno'),
        (@new_activity2, 'de_DE', 'Artikel vom internen Lieferschein gel�scht'),

 

        (@new_activity3, 'fr_FR', 'Article supprim� du bon de livraison'),
        (@new_activity3, 'en_GB', 'Item deleted from delivery note'),
        (@new_activity3, 'es_ES', 'Art�culo eliminado de la nota de entrega'),
        (@new_activity3, 'de_DE', 'Artikel vom Lieferschein gel�scht'),

 

        (@new_activity4, 'fr_FR', 'Article supprim� du bon de livraison tiers'),
        (@new_activity4, 'en_GB', 'Item deleted from Third Party delivery note'),
        (@new_activity4, 'es_ES', 'Art�culo eliminado del albar�n de entrega de terceros'),
        (@new_activity4, 'de_DE', 'Artikel vom Lieferschein eines Drittanbieters gel�scht'),

 

        (@new_activity5, 'fr_FR', 'Article supprim� du bon de livraison apr�s le bon de commande'),
        (@new_activity5, 'en_GB', 'Item deleted from Third Party delivery note after PO'),
        (@new_activity5, 'es_ES', 'Art�culo eliminado del albar�n de entrega de terceros despu�s del pedido'),
        (@new_activity5, 'de_DE', 'Artikel vom Lieferschein eines Drittanbieters nach Bestellung gel�scht');

 


-- create "delete-delivery-item" hook
INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
    VALUES (1,0,1,1,'delete-delivery-item',@new_activity1);
SELECT @hk = MAX(id) FROM dbo.hook;

 

SET @stateid1=(SELECT stateid FROM dbo.itemstate WHERE description= 'Awaiting despatch to client')
SET @stateid2=(SELECT stateid FROM dbo.itemstate WHERE description= 'Awaiting despatch to business company')
SET @stateid3=(SELECT stateid FROM dbo.itemstate WHERE description= 'Awaiting despatch to business company for work')
SET @stateid4=(SELECT stateid FROM dbo.itemstate WHERE description= 'Awaiting despatch to third party')
SET @stateid5=(SELECT stateid FROM dbo.itemstate WHERE description= 'Awaiting despatch to third party after PO')
SET @stateid6=(SELECT stateid FROM dbo.itemstate WHERE description= 'Awaiting despatch to returning business subdivision')

 

SET @stateid7=(SELECT TOP 1 stateid FROM dbo.itemstate WHERE description= 'Awaiting delivery note to third party')

 

 


-- create hook activities 
INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
  Values(1,1,@new_activity3, @hk, @stateid1),
        (1,1,@new_activity1, @hk, @stateid2),
        (1,1,@new_activity1, @hk, @stateid3),
        (1,1,@new_activity4, @hk, @stateid4),
        (1,1,@new_activity5, @hk, @stateid5),
        (1,1,@new_activity2, @hk, @stateid6);

 


INSERT INTO outcomestatus(activityid, defaultoutcome, lookupid, statusid) VALUES
(@new_activity4, 0, NULL, @stateid7);

 

INSERT INTO dbversion(version) VALUES(535);

 

GO

 

COMMIT TRAN