-- More suggestions for managing a mandatory client destination receipt date
USE [atlas]
GO

BEGIN TRAN

	DECLARE @systemdefaultId INT,
	        @state_id INT

	-- Update the french description translation of 'Confirmation of receipt of destination' system default
	SELECT @systemdefaultId = defaultid FROM dbo.systemdefault WHERE defaultname LIKE 'Confirmation of receipt of destination';

	UPDATE dbo.systemdefaultdescriptiontranslation SET translation = 'Définir s''il faut enregistrer la date réelle de livraison chez le client'
		WHERE defaultid= @systemdefaultId AND locale = 'fr_FR'

	-- In Jobitem dashboard show status 'Awaiting confirmation of client receipt' in Goods In/Out tab under other
	SELECT @state_id = stateid FROM dbo.itemstate WHERE description = 'Awaiting confirmation of client receipt'

	UPDATE dbo.stategrouplink SET type='CURRENT_SUBDIV' WHERE groupid = 76 

	INSERT INTO dbo.stategrouplink(groupid, stateid, type)
	VALUES (35, @state_id, 'CURRENT_SUBDIV')
	
	GO

	INSERT INTO dbversion(version) VALUES(722);

COMMIT TRAN