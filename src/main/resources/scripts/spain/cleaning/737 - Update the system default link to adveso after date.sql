USE [atlas]
GO

BEGIN TRAN

	DECLARE @link_to_adveso_from_date INT;

	SELECT @link_to_adveso_from_date = defaultid FROM dbo.systemdefault WHERE defaultname = 'Link to Adveso after';
	
	UPDATE dbo.systemdefault SET defaultname = 'Link to Adveso from', defaultdescription = 'Link to Adveso from date' WHERE defaultid = @link_to_adveso_from_date;
	
	UPDATE dbo.systemdefaultdescriptiontranslation SET translation = 'Send notifications and activities to Adveso only from the specified date' WHERE defaultid = @link_to_adveso_from_date AND locale = 'en_GB';
	UPDATE dbo.systemdefaultdescriptiontranslation SET translation = 'Envoyer des notifications et des activit�s � Adveso uniquement � partir de la date sp�cifi�e' WHERE defaultid = @link_to_adveso_from_date AND locale = 'fr_FR';
	UPDATE dbo.systemdefaultdescriptiontranslation SET translation = 'Env�e notificaciones y actividades a Adveso solo a partir de la fecha especificada' WHERE defaultid = @link_to_adveso_from_date AND locale = 'es_ES';
	UPDATE dbo.systemdefaultdescriptiontranslation SET translation = 'Senden Sie Benachrichtigungen und Aktivit�ten nur ab dem angegebenen Datum an Adveso' WHERE defaultid = @link_to_adveso_from_date AND locale = 'de_DE';
	
	UPDATE dbo.systemdefaultnametranslation SET translation = 'Link to Adveso from date' WHERE defaultid = @link_to_adveso_from_date AND locale = 'en_GB';
	UPDATE dbo.systemdefaultnametranslation SET translation = 'Lien vers Adveso � partir de la date' WHERE defaultid = @link_to_adveso_from_date AND locale = 'fr_FR';
	UPDATE dbo.systemdefaultnametranslation SET translation = 'Enlace a Adveso desde la fecha' WHERE defaultid = @link_to_adveso_from_date AND locale = 'es_ES';
	UPDATE dbo.systemdefaultnametranslation SET translation = 'Link zu Adveso ab Datum' WHERE defaultid = @link_to_adveso_from_date AND locale = 'de_DE';

	INSERT INTO dbversion(version) VALUES(737);

COMMIT TRAN


