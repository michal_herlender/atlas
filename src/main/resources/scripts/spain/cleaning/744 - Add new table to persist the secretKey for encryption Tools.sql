
USE [atlas]
GO

BEGIN TRAN

	CREATE TABLE dbo.secretkey(keyvalue binary(16) NOT NULL PRIMARY KEY);

	INSERT INTO dbversion(version) VALUES(744);

COMMIT TRAN