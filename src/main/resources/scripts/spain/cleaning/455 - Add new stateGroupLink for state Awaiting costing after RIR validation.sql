USE [spain];


BEGIN TRAN

	DECLARE @stateid as int;
	
	SELECT @stateid = stateid
	FROM dbo.itemstate WHERE description = 'Item requires repair – awaiting costing to client';
	
	-- add state 'Item requires repair – awaiting costing to client' to stategroup 'csjobcosting'
	INSERT INTO dbo.stategrouplink (groupid,stateid,[type]) VALUES (30,@stateid,'ALLOCATED_SUBDIV');
	
	INSERT INTO dbversion (version) VALUES (455);

COMMIT TRAN