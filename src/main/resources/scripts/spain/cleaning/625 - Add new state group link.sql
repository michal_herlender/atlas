USE [atlas]
GO

BEGIN TRAN

declare @awContractReview int, @awOnsiteContractReview int;

select @awContractReview = iss.stateid from itemstate iss
where iss.description like 'Awaiting contract review';

select @awOnsiteContractReview = iss.stateid from itemstate iss
where iss.description like 'Awaiting onsite contract review';

INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (64,@awContractReview,'ALLOCATED_SUBDIV');
INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (64,@awOnsiteContractReview,'ALLOCATED_SUBDIV');


INSERT INTO dbversion VALUES (625)

COMMIT TRAN