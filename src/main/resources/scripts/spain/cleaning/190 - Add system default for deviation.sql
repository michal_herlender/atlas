BEGIN TRAN

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
  (31
    , 'boolean'
    , 'Defines if certificates for this client can have a deviation figure'
    , 'Deviation'
    , 'false', 0, 0, 9, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (31, 'en_GB', 'Defines if certificates for this client can have a deviation figure'),
  (31, 'en_ES', 'Define si los certificados para este cliente pueden tener una figura de deriva'),
  (31, 'fr_FR', 'Définit si les certificats pour ce client peuvent avoir un chiffre d''écart');

-- Enables system default to be configurable at only company level

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0, 31);
GO

-- Enables system defaults (new and old) for specific company roles - Client and Business Company

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (1, 31),
  (5, 31);

-- Add translations

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (31, 'en_GB', 'Deviation'),
  (31, 'es_ES', 'Deriva'),
  (31, 'fr_FR', 'écart');
GO

-- Change to COMMIT when successfully tested

COMMIT TRAN