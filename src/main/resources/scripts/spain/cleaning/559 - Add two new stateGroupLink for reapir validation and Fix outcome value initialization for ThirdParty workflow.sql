USE [atlas]
GO

BEGIN TRAN
DECLARE @lookup_tpcaloutcome INT,
		@lookup_tprep INT,
		@os INT,
		@rirtechval INT, 
		@rircsrval INT;

SELECT @rirtechval = stateid FROM dbo.itemstate WHERE [description] = 'Repair Inspection Report Validated by Technician';
SELECT @rircsrval = stateid FROM dbo.itemstate WHERE [description] = 'Repair Inspection Report Validated by CSR';

INSERT INTO dbo.stategrouplink(groupid,stateid,[type])
VALUES (60, @rirtechval, 'ALLOCATED_SUBDIV'),
	   (61, @rircsrval, 'ALLOCATED_SUBDIV');

SELECT @lookup_tpcaloutcome = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_TPCalibrationOutcome (inhouse after third party work)';
SELECT @lookup_tprep = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_ItemRepairedAtTP';

SELECT @os = outcomestatusid FROM dbo.lookupresult WHERE lookupid = @lookup_tprep AND resultmessage = 1;

UPDATE dbo.outcomestatus SET lookupid = @lookup_tpcaloutcome WHERE id = @os;

INSERT INTO dbversion(version) VALUES(559);

COMMIT TRAN