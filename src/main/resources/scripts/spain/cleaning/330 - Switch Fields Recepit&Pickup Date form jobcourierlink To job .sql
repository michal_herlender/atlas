-- Delete two fields 'ReceiptDate' and 'PickupDate' from 'jobcourierlink' Table and put it on 'Job' Table

USE [spain]
GO

BEGIN TRAN

	ALTER TABLE dbo.jobcourierlink DROP COLUMN receiptdate;
	ALTER TABLE dbo.jobcourierlink DROP COLUMN pickupdate;

	ALTER TABLE dbo.job ADD receiptdate DATETIME2;
	ALTER TABLE dbo.job ADD pickupdate DATETIME2;

	INSERT INTO dbversion(version) VALUES(330);

COMMIT TRAN
