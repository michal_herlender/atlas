-- As provided by Sothy via email; assumes single user manual file is already in file system to insert

USE [spain]

INSERT INTO dbo.usermanual (filebinary)
VALUES ((SELECT * FROM OPENROWSET(BULK 'F:\MSSQL\ERP_ENTRADAS_SALIDAS.pdf', SINGLE_BLOB) rs))