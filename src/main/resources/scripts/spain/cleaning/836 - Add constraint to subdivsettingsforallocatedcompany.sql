-- This adds a constraint to the only used field in [subdivsettingsforallocatedsubdiv] - businesscontactid
-- We can safely delet null records (~27 in EMEA production) as they are meaningless when the field is not set
-- This new constraint helps prevent incomplete data from being migrated into Atlas from external tools.

USE [atlas]
GO

BEGIN TRAN

DELETE FROM [dbo].[subdivsettingsforallocatedsubdiv] where businesscontactid is null;
GO

ALTER TABLE [dbo].[subdivsettingsforallocatedsubdiv] ALTER COLUMN businesscontactid int not null;

INSERT INTO dbversion(version) VALUES(836);

COMMIT TRAN
