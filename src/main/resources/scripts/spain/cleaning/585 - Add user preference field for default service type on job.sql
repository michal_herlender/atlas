-- Adds a new field to user preferences, 
-- This specifies whether the user would like to add instruments to a job using only the default service type from the job.
-- By default the value is false

USE [atlas];

BEGIN TRAN

alter table dbo.userpreferences 
  add defaultservicetypeusage tinyint;
GO

update dbo.userpreferences 
  set defaultservicetypeusage = 0;
GO

alter table dbo.userpreferences 
  alter column defaultservicetypeusage tinyint not null;

INSERT INTO dbversion(version) VALUES(585);

COMMIT TRAN