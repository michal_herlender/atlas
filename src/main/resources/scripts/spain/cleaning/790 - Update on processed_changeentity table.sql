-- Little update to comment out value 19 as this was inserted in script 788
USE [atlas]
GO

BEGIN TRAN

INSERT INTO atlas.dbo.processed_changeentity
(changeentity, lastprocessedlsn)
VALUES--(19, NULL),
		(20, NULL),
		(21, NULL),
		(22, NULL),
		(23, NULL),
		(24, NULL),
		(25, NULL),
		(26, NULL),
		(27, NULL),
		(28, NULL),
		(29, NULL),
		(30, NULL);

INSERT INTO dbversion(version) VALUES(790);

COMMIT TRAN


