-- rename statues and activities that use to be used for adjustments and repair costing to client, to be used only for adjustments
USE [atlas]
GO

BEGIN TRAN

UPDATE dbo.itemstate SET description='Adjustment costing issued to client' WHERE stateid=41;
GO

UPDATE dbo.itemstatetranslation SET [translation]='Justagekosten an Kunden gesendet'
	WHERE stateid=41 AND locale='de_DE' AND [translation]='Reparatur / Justagekosten an Kunden gesendet' 
GO
UPDATE dbo.itemstatetranslation SET [translation]='Adjustment costing issued to client'
	WHERE stateid=41 AND locale='en_GB' AND [translation]='Repair/adjustment costing issued to client'
GO
UPDATE dbo.itemstatetranslation SET [translation]='Coste de la ajuste comunicado al cliente'
	WHERE stateid=41 AND locale='es_ES' AND [translation]='Coste de la reparación/ajuste comunicado al cliente'
GO
UPDATE dbo.itemstatetranslation SET [translation]='Valorisation des travaux d''ajustage ("job pricing") envoyé au client'
	WHERE stateid=41 AND locale='fr_FR' AND [translation]='Valorisation des travaux de réparation / ajustage ("job pricing") envoyé au client'
GO



UPDATE dbo.itemstate SET description='Costing issued for adjustment - awaiting client decision'
	WHERE stateid=42
GO

UPDATE dbo.itemstatetranslation SET [translation]='Costing issued for adjustment - awaiting client decision'
	WHERE stateid=42 AND locale='en_GB' AND [translation]='Costing issued for repair/adjustment - awaiting client decision' 
GO
UPDATE dbo.itemstatetranslation SET [translation]='Coste de el ajuste comunicado - esperando la decisión del cliente'
	WHERE stateid=42 AND locale='es_ES' AND [translation]='Coste de reparación/el ajuste comunicado - esperando la decisión del cliente'
GO
UPDATE dbo.itemstatetranslation SET [translation]='En attente de la réponse client suite à envoi de la valorisation ("job pricing") des travaux pour ajustage'
	WHERE stateid=42 AND locale='fr_FR' AND [translation]='En attente de la réponse client suite à envoi de la valorisation ("job pricing") des travaux pour réparation / ajustage'
GO



UPDATE dbo.itemstate SET description='Client decision received for adjustment costing'
	WHERE stateid=43 
GO
UPDATE dbo.itemstatetranslation SET [translation]='Kundenentscheidung für Anpassungskalkulation erhalten'
	WHERE stateid=43 AND locale='de_DE' AND [translation]='Kundenentscheid erhalten' 
GO
UPDATE dbo.itemstatetranslation SET [translation]='Client decision received for adjustment costing'
	WHERE stateid=43 AND locale='en_GB' AND [translation]='Client decision received'
GO
UPDATE dbo.itemstatetranslation SET [translation]='Decisión del cliente recibida por costos de ajuste'
	WHERE stateid=43 AND locale='es_ES' AND [translation]='Decisión del cliente recibida'
GO
UPDATE dbo.itemstatetranslation SET [translation]='Réponse client reçue pour la valorisation de l''ajustage'
	WHERE stateid=43 AND locale='fr_FR' AND [translation]='Réponse client reçue' 
GO

UPDATE dbo.lookupinstance
	SET lookup='Lookup_ClientDecision (inhouse after adjustment costing)'
	WHERE id=26 
GO

-- link the positive ouctome to status 'awaiting adjustement directly'
UPDATE dbo.outcomestatus SET lookupid=NULL,statusid=23
	WHERE id=62 
GO

INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,statusid)
	VALUES (0,43,40) 
GO

declare @id int;
select @id= max(os.id)
from dbo.outcomestatus os

INSERT INTO dbo.lookupresult ([result],activityid,lookupid,outcomestatusid,resultmessage)
	VALUES ('c',43,26,@id,2) 

GO

INSERT INTO dbversion(version) VALUES(485);

COMMIT TRAN