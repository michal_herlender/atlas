

USE [spain]

BEGIN TRAN

CREATE INDEX IDX_jobcostingitem_jobitemid on dbo.jobcostingitem (jobitemid);

GO

DROP INDEX [IDX_instrument_wiid] ON [dbo].[instrument]
DROP INDEX [_dta_index_instrument_7_740197687__K5_K22_1_2_3_4_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_23_24_25_26_27_28_29_30_31_32_33_] ON [dbo].[instrument]
DROP INDEX [_dta_index_instrument_7_740197687__K1_K27_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_28_29_30_31_32_33_] ON [dbo].[instrument]
ALTER TABLE [dbo].[instrument] DROP CONSTRAINT [FKf3lrd9wv4ceb93a309g0epdi8]
DROP STATISTICS [dbo].[instrument].[_dta_stat_740197687_1_37_27_19_23_26_28]
DROP STATISTICS [dbo].[instrument].[_dta_stat_740197687_37_27_19_23_26_28]
DROP STATISTICS [dbo].[instrument].[_dta_stat_740197687_26_1_37_27_19]
ALTER TABLE instrument DROP COLUMN wiid

GO

CREATE TABLE dbo.instrumentworkinstruction (workInstructionId int not null, plantId int not null);

ALTER TABLE dbo.instrumentworkinstruction 
       ADD CONSTRAINT FK_instrumentworkinstruction_plant 
       FOREIGN KEY (plantId) 
       REFERENCES dbo.instrument;

ALTER TABLE dbo.instrumentworkinstruction 
       ADD CONSTRAINT FK_instrumentworkinstruction_workinstruction 
       FOREIGN KEY (workInstructionId) 
       REFERENCES dbo.workinstruction;

ALTER TABLE spain.dbo.calibration 
       ADD workInstructionId int;

ALTER TABLE dbo.calibration 
       ADD CONSTRAINT FK_calibration_workinstruction 
       FOREIGN KEY (workInstructionId) 
       REFERENCES dbo.workinstruction;

DROP INDEX [IDX_calibration_wiversionid] ON [dbo].[calibration]
DROP INDEX [_dta_index_calibration_7_1653580929__K16_K17_K8_1_2_3_4_5_6_7_9_10_11_12_13_14_15_18] ON [dbo].[calibration]
DROP STATISTICS [dbo].[calibration].[_dta_stat_1653580929_11_13_18_17_15_14_10_16_12]
DROP STATISTICS [dbo].[calibration].[_dta_stat_1653580929_11_13_18_17_15_14_16_12]
DROP STATISTICS [dbo].[calibration].[_dta_stat_1653580929_1_11_13_18_17_15_14_10_16_12]
DROP STATISTICS [dbo].[calibration].[_dta_stat_1653580929_1_11_13_18_17_15_14_16_12]
ALTER TABLE [dbo].[calibration] DROP CONSTRAINT [FKm4l9ln8uvidh5cakd5vmbujk8]
ALTER TABLE calibration DROP COLUMN wiversionid

ALTER TABLE spain.dbo.workinstruction 
       ADD calibrationprocess int

ALTER TABLE dbo.workinstruction 
       ADD CONSTRAINT FK_workinstruction_calibrationprocess 
       FOREIGN KEY (calibrationprocess) 
       REFERENCES dbo.calibrationprocess;

DROP TRIGGER [dbo].[log_update_calibration]
DROP TRIGGER [dbo].[log_delete_calibration]

GO

INSERT INTO dbo.calibrationprocess(actionafterissue, applicationcomponent, description, fileextension, issueimmediately, process, quicksign, signingsupported, signonissue, toollink, uploadafterissue, uploadbeforeissue)
VALUES (0, 0, 'KaliPro is used for calibration', '.pdf', 0, 'KaliPro', 0, 0, 0, 'https://localhost:58159/service/start?tool=kalipro&calibrationId={0}', 0, 1)

GO

INSERT INTO dbversion(version) VALUES(391);

COMMIT TRAN