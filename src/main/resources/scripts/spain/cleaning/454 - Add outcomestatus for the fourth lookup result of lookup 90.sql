USE [spain];


BEGIN TRAN

	--create outcome status that connects to status '89-Awaiting TP Work'
	INSERT INTO dbo.outcomestatus (defaultoutcome,statusid) VALUES (0,89);
	
	--get the id of the newly created os
	declare @lastOsId int;
	select @lastOsId = max(os.id) from dbo.outcomestatus os;
	
	--create the fourth lookupresult
	INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (90,@lastOsId,4);

	INSERT INTO dbversion (version) VALUES (454);

COMMIT TRAN