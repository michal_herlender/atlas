USE [atlas]
GO

BEGIN TRAN

-- Sets the "groupwide" parameter
UPDATE [dbo].[systemdefault]
   SET [groupwide] = 1
 WHERE defaultid = 56;
GO

-- Initializes the start and end for some recently created system defaults
UPDATE [dbo].[systemdefault]
   SET [start] = 0
 WHERE start is null;
GO

UPDATE [dbo].[systemdefault]
   SET [endd] = 1
 WHERE endd is null;
GO
--

ALTER TABLE [dbo].[systemdefault] ALTER COLUMN [groupwide] bit not null;
ALTER TABLE [dbo].[systemdefault] ALTER COLUMN [start] int not null;
ALTER TABLE [dbo].[systemdefault] ALTER COLUMN [endd] int not null;

INSERT INTO [dbo].[dbversion](version) VALUES(617);

COMMIT TRAN