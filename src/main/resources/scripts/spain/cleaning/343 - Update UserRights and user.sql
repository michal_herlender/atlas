USE [spain]
Begin Tran

/*
Create a flag for Web User 
*/
alter table dbo.users 
	add webuser tinyint NULL
GO

UPDATE dbo.users SET webuser=0
FROM dbo.users
GO

UPDATE dbo.users SET webuser=1 
FROM dbo.users
INNER JOIN dbo.contact on dbo.users.personid = dbo.contact.personid
INNER JOIN dbo.subdiv on dbo.contact.subdivid = subdiv.subdivid
INNER JOIN dbo.company on dbo.subdiv.coid = dbo.company.coid
INNER JOIN dbo.corole on dbo.company.corole = dbo.corole.coroleid
where dbo.corole.corole!='Business'
GO

ALTER TABLE dbo.users ALTER COLUMN webuser tinyint NOT NULL
GO

/*
Add and Update Permission 
*/

insert into permission(groupId, permission)
values 
(1, 'CREATE_EMAIL'),
(3, 'ADD_EDIT_DEPARTMENT'),
(3, 'EDIT_CATEGORY'),
(3, 'UNLOCK_CREDIT_NOTE'),
(3, 'ALLOCATE_ITEM_TO_SUBDIVISION'),
(3, 'EDIT_VERIFICATION_STATUT'),
(2, 'EDIT_BUSINESS_INSTRUMENT'),
(2, 'CREATE_CERT'),
(3, 'ADMIN_CERT_MANAGEMENT'),
(3, 'PASSWORD_EDIT'),
(3, 'USER_RIGHT_MANAGEMENT'),
(3, 'REALLOCATE_MANAGEMENT'),
(3, 'ADMIN_EDIT_USER_PREFERENCE'),
(3, 'ADMIN_JI_CERTIFICATE'),
(2, 'MANAGE_JI_CERTIFICATE'),
(3, 'ADMIN_EDIT_PROCEDURE'),
(2, 'PROCEDURE_TRAINING_CERTIFICATION'),
(3, 'MANAGE_USER_COMPANY'),
(3, 'AUTO_CWMS'),
(3, 'ADMIN_EDIT_CONTACT_PROCEDURE_ACCREDITATION'),
(3, 'EDIT_SUBDIV_TURNAROUND'),
(2, 'OVERRIDE_JI_ACTIVITY'),
(2, 'VIEW_PERSON');

/* Already add on spain and some db so check if already add if not add it */
BEGIN
		IF NOT EXISTS ( SELECT * FROM permission 
						   WHERE groupId = 1
						   AND permission = 'EDIT_JOB_NO_LINK')
   BEGIN
		insert into permission(groupId, permission)
		values (1, 'EDIT_JOB_NO_LINK')
   END
END

/* Group change 1 to 3 */
update dbo.permission set groupId=3
where groupId=1 AND permission='UNLOCK_INVOICE';

update dbo.permission set groupId=3
where groupId=1 AND permission='ADD_PERSON';

/* Departement to Department */
update dbo.permission set permission='VIEW_CAPABILITIES_BY_DEPARTMENT'
where permission='VIEW_CAPABILITIES_BY_DEPARTEMENT';

update dbo.permission set permission='ALLOCATE_ITEM_TO_DEPARTMENT'
where permission='ALLOCATE_ITEM_TO_ENGINEER'

INSERT INTO dbversion(version) VALUES(343);

COMMIT TRAN