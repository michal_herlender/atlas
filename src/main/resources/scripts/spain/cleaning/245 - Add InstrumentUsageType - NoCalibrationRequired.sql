USE [spain]
GO

BEGIN TRAN

SET IDENTITY_INSERT [instrumentusagetype] ON

INSERT INTO [dbo].[instrumentusagetype]
           ([typeid]
		   ,[defaulttype]
           ,[fulldescription]
           ,[name]
           ,[recall])
     VALUES
           (23
		   ,''
           ,''
           ,'No Calibration Required'
           ,0)
GO

SET IDENTITY_INSERT [instrumentusagetype] OFF

INSERT INTO [dbo].[instrumentusagetypenametranslation]
           ([typeid]
           ,[locale]
           ,[translation])
     VALUES
           (23
           ,'en_GB'
           ,'No Calibration Required'),
           (23
           ,'fr_FR'
           ,'Aucune vérification requise'),
           (23
           ,'es_ES'
           ,'No se requiere calibración')
GO

insert into dbversion(version) values(245);

COMMIT TRAN