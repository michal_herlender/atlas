USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.jobitemhistory ADD fieldname VARCHAR(32);

	INSERT INTO dbversion(version) VALUES(500);

COMMIT TRAN