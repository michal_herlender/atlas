USE [spain]

BEGIN TRAN

    ALTER TABLE dbo.asnitem ADD caltypeid int
    go
    ALTER TABLE dbo.asn ADD createdon datetime2
    go
	ALTER TABLE dbo.asn ADD createdby int
	go
	
	ALTER TABLE dbo.asn ADD CONSTRAINT FK_asn_createdby 
		FOREIGN KEY (createdby) REFERENCES dbo.contact(personid) 
	go
	ALTER TABLE dbo.prebookingjobdetails DROP CONSTRAINT FK_prebookingjobdetails_createdby
	go
	ALTER TABLE dbo.prebookingjobdetails DROP COLUMN createdby
	go

	INSERT INTO dbversion(version) VALUES(282);

COMMIT TRAN