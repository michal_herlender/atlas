USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.jobexpenseitem ADD stateid INT;

GO

ALTER TABLE dbo.jobexpenseitem 
    ADD CONSTRAINT FK_jobexpenseitem_state
    FOREIGN KEY (stateid)
    REFERENCES dbo.itemstate(stateid);

SET IDENTITY_INSERT itemstate ON

INSERT INTO itemstate(stateid, active, description, retired, type) VALUES
(5001, 1, 'Created', 0, 'expenseitemstate'),
(5002, 0, 'Finished', 0, 'expenseitemstate')

SET IDENTITY_INSERT itemstate OFF

UPDATE jobexpenseitem SET stateid = 5001 WHERE invoiceable = 0

UPDATE jobexpenseitem SET stateid = 5002 WHERE invoiceable = 1

GO

CREATE TABLE dbo.jobexpenseitempo (
	id INT IDENTITY NOT NULL,
	lastModified DATETIME2,
	lastModifiedBy INT,
	expenseitemid INT,
	bpoid INT,
	poid INT,
	PRIMARY KEY (id)
);

ALTER TABLE dbo.jobexpenseitempo ADD CONSTRAINT UK_jobexpenseitempo_expenseitemid_bpoid_poid UNIQUE (expenseitemid, bpoid, poid)

GO

sp_rename 'jobitempo.jobitempoid', 'id'

GO

ALTER TABLE jobexpenseitem ADD quantity INT
ALTER TABLE jobexpenseitem ADD clientref VARCHAR(30)

GO

create table dbo.nominalcodeapplication (
	id int identity not null,
	costtypeid int,
	domainid int,
	familyid int,
	nominalcodeid int not null,
	subfamilyid int,
	primary key (id)
);

alter table dbo.nominalcodeapplication
	add constraint FK_nominalcodeapplication_domain
	foreign key (domainid)
	references dbo.instmodeldomain;

alter table dbo.nominalcodeapplication
	add constraint FK_nominalcodeapplication_family
	foreign key (familyid)
	references dbo.instmodelfamily;

alter table dbo.nominalcodeapplication
	add constraint FK_nominalcodeapplication_nominalcode
	foreign key (nominalcodeid)
	references dbo.nominalcode;

alter table dbo.nominalcodeapplication
	add constraint FK_nominalcodeapplication_subfamily
	foreign key (subfamilyid)
	references dbo.description;

alter table dbo.nominalcodeapplication
	add constraint UK_nominalcodeapplication
	unique (domainid, familyid, subfamilyid)

GO

declare @ncS001 int, @ncS003 int, @ncS004 int, @ncS006 int, @ncS007 int, @ncS008 int, @ncS009 int;
declare @adm1 int, @adm2 int;
declare @travel int, @asset int, @engineering int, @it int, @procurement int, @repair int, @additional int,
	@logistic int, @support int, @process int;

select @ncS001 = nc.id from nominalcode nc where nc.code = 'S001'
select @ncS003 = nc.id from nominalcode nc where nc.code = 'S003'
select @ncS004 = nc.id from nominalcode nc where nc.code = 'S004'
select @ncS006 = nc.id from nominalcode nc where nc.code = 'S006'
select @ncS007 = nc.id from nominalcode nc where nc.code = 'S007'
select @ncS008 = nc.id from nominalcode nc where nc.code = 'S008'
select @ncS009 = nc.id from nominalcode nc where nc.code = 'S009'
select @adm1 = sf.descriptionid from description sf where sf.description like 'ADM - Contribution%'
select @adm2 = sf.descriptionid from description sf where sf.description like 'ADM - Return without%'
select @travel = f.familyid from instmodelfamily f where f.name like 'Travel%'
select @asset = f.familyid from instmodelfamily f where f.name like 'Asset%'
select @engineering = f.familyid from instmodelfamily f where f.name like 'Engineering%'
select @it = f.familyid from instmodelfamily f where f.name like 'IT%'
select @procurement = f.familyid from instmodelfamily f where f.name like 'Procurement%'
select @repair = f.familyid from instmodelfamily f where f.name like 'Repair%'
select @additional = f.familyid from instmodelfamily f where f.name like 'Additional%'
select @logistic = f.familyid from instmodelfamily f where f.name like 'Logistic%'
select @support = f.familyid from instmodelfamily f where f.name like 'Support%'
insert into dbo.nominalcodeapplication(costtypeid, domainid, familyid, subfamilyid, nominalcodeid) values
	(0, null, null, @adm1, @ncS004),
	(0, null, null, @adm2, @ncS001),
	(0, null, @travel, null, @ncS004),
	(0, null, @asset, null, @ncS004),
	(0, null, @engineering, null, @ncS004),
	(0, null, @it, null, @ncS007),
	(0, null, @procurement, null, @ncS009),
	(0, null, @repair, null, @ncS003),
	(0, null, @additional, null, @ncS004),
	(0, null, @logistic, null, @ncS006),
	(0, null, @support, null, @ncS008),
	(0, null, @process, null, @ncS008)

GO

INSERT INTO dbversion(version) VALUES(703);

COMMIT TRAN