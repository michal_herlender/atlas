-- TO BE EXECUTED AFTER REPAIR WORKFLOW CHANGES (perform upgrade)

USE spain;

BEGIN TRAN

	DECLARE @a1 as int;
	DECLARE @s1 as int;	
	DECLARE @h1 as int;	
	DECLARE @a2 as int;
	DECLARE @s2 as int;	
	DECLARE @h2 as int;	

	--hook validate-rir-by-technician
	select @a1 = iss.stateid
	from itemstate iss
	where iss.description = 'Repair Inspection Report Validated by Technician';
	select @s1 = iss.stateid
	from itemstate iss
	where iss.description = 'Awaiting Repair Inspection Report Validation by Technician';
	
	INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,0,1,1,'validate-rir-by-technician',@a1);

	select @h1=h.id
	from dbo.hook h
	where h.name = 'validate-rir-by-technician';

	INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@a1,@h1,@s1);


	-- hook validate-rir-by-csr
	select @a2 = iss.stateid
	from itemstate iss
	where iss.description = 'Repair Inspection Report Validated by CSR';
	select @s2 = iss.stateid
	from itemstate iss
	where iss.description = 'Awaiting Repair Inspection Report Validation by CSR';
	
	INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,0,1,1,'validate-rir-by-csr',@a2);

	select @h2=h.id
	from dbo.hook h
	where h.name = 'validate-rir-by-csr';

	INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@a2,@h2,@s2);
	
	-- rename fields
	EXEC sp_RENAME 'dbo.repairinspectionreport.validatedon' , 'validatedbytechnicianon', 'COLUMN';

	EXEC sp_RENAME 'dbo.repairinspectionreport.validatedby' , 'technician', 'COLUMN';

	EXEC sp_RENAME 'dbo.repairinspectionreport.validated' , 'validatedbytechnician', 'COLUMN';

	--add fields for csr validation
	ALTER TABLE dbo.repairinspectionreport ADD validatedbycsr bit DEFAULT 0 NULL;
	ALTER TABLE dbo.repairinspectionreport ADD validatedbycsron datetime2 NULL;
	ALTER TABLE dbo.repairinspectionreport ADD csr int NULL;
	ALTER TABLE dbo.repairinspectionreport 
		ADD CONSTRAINT repairinspectionreport_contact_csr_FK FOREIGN KEY (csr) REFERENCES dbo.contact(personid);

	INSERT INTO dbversion (version) VALUES (443);

COMMIT TRAN