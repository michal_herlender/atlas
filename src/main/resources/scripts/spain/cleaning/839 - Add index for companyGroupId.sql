-- 2021-09-16 GB change from original script : Removed OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF as option
-- (not supported in SQL Server 2017 / 14.0 that we use in most test / production environments)


BEGIN TRAN
GO

CREATE NONCLUSTERED INDEX [IDX_company_companygroupid] ON [dbo].[company]
(
	[companygroupid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT INTO dbversion VALUES (839)

COMMIT TRAN