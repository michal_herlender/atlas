-- Script 172 - Fixes images table to autoincrement - 2017-09-07
-- Authors - Yassine Boutahar, Egbert Fohry, Galen Beck
-- Run before startup, should update ~61169 rows (from 2017-09-07 backup)
-- Note, SQL Server management studio may show compile warnings on references not created yet, 
-- but script should run OK. (Takes ~30 seconds to run on dev machine.)

USE [spain]
GO

BEGIN TRAN

-- Renames the OLD images table to imagesBU (and a conflicting foreign key name)

EXEC sp_rename 'dbo.images', 'imagesBU';

EXEC sp_rename 'FKhg63kxlxnnffcxjssvvlcmkxq', 'FK_imagesBU_personid'

-- Temporarily remove constraint on tagged image

ALTER TABLE [dbo].[taggedimage] DROP CONSTRAINT [FKA0B3B6F66A919BA]
GO

-- Creates a NEW images table with identity / autoincrement on PK (taken from schema create utility)

    create table dbo.images (
        type varchar(31) not null,
        id int identity not null,
        lastModified datetime,
        addedon datetime2 not null,
        description varchar(200),
        fileName varchar(255),
        rfilepath varchar(300),
        lastModifiedBy int,
        personid int,
        jobitemid int,
        modelid int,
        primary key (id)
    );

    alter table dbo.images 
        add constraint FKhg63kxlxnnffcxjssvvlcmkxq 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.images 
        add constraint FKblgy0o995ih803okk0456632s 
        foreign key (personid) 
        references dbo.contact;

    alter table dbo.images 
        add constraint FKg8na4qw0c3nq0fs4mo0j4g0cb 
        foreign key (jobitemid) 
        references dbo.jobitem;

    alter table dbo.images 
        add constraint FKk46113ygaw57fpfwr2pkstjsy 
        foreign key (modelid) 
        references dbo.instmodel;

-- Original script below (changed idOLD to id for imagesBU)

SET IDENTITY_INSERT [dbo].[images] on

INSERT INTO [dbo].[images]
           ([type]
		   ,[id]
			  ,[addedon]
			  ,[description]
			  ,[rfilepath]
			  ,[personid]
			  ,[jobitemid]
			  ,[modelid]
			  ,[fileName]
			  ,[lastModified]
			  ,[lastModifiedBy])
(SELECT [type]
			  ,[id]
			  ,[addedon]
			  ,[description]
			  ,[rfilepath]
			  ,[personid]
			  ,[jobitemid]
			  ,[modelid]
			  ,[fileName]
			  ,[lastModified]
			  ,[lastModifiedBy]
		  FROM [dbo].[imagesBU])
GO

SET IDENTITY_INSERT [dbo].[images] off

-- Re-add constraint on taggedimage to correct table (keeping the same Hibernate 4 based name for now)

ALTER TABLE [dbo].[taggedimage]  WITH CHECK ADD  CONSTRAINT [FKA0B3B6F66A919BA] FOREIGN KEY([imageid])
REFERENCES [dbo].[images] ([id])
GO

ALTER TABLE [dbo].[taggedimage] CHECK CONSTRAINT [FKA0B3B6F66A919BA]
GO

-- After verifying images transferred and all OK, manually delete the imagesBU table
-- (A simple DROP TABLE [dbo].[imagesBU] will do it

COMMIT TRAN