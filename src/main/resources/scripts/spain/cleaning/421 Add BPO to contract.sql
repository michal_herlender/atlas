USE [spain]
BEGIN TRAN

  alter table spain.dbo.contract
    add bpoid int;


  alter table dbo.contract
    add constraint FK4he4l5fd9okthg6g8bhyoefxn
      foreign key (bpoid)
        references dbo.bpo;


  INSERT INTO dbversion(version) VALUES (421)

  COMMIT TRAN


