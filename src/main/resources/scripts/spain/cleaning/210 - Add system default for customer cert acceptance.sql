BEGIN TRAN

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
  (22
    , 'boolean'
    , 'Defines if customer has certificate acceptance functionality on portal'
    , 'Customer Certificate Acceptance'
    , 'false', 0, 0, 9, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (22, 'en_GB', 'Defines if customer has certificate acceptance functionality on portal'),
  (22, 'en_ES', 'Define si el cliente tiene funcionalidad de aceptación de certificado en el portal'),
  (22, 'fr_FR', 'Définit si le client dispose d''une fonctionnalité d''acceptation de certificat sur le portail');

-- Enables system default to be configurable at only company level

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0, 22);
GO

-- Enables system defaults (new and old) for specific company roles - Client and Business Company

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (1, 22),
  (5, 22);

-- Add translations

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (22, 'en_GB', 'Customer Certificate Acceptance'),
  (22, 'es_ES', 'Aceptación del certificado del cliente'),
  (22, 'fr_FR', 'Acceptation du certificat client');
GO

INSERT INTO dbversion (version) VALUES (210);

-- Change to COMMIT when successfully tested

COMMIT TRAN