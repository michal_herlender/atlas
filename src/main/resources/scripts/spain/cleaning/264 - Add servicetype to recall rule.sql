use spain;

BEGIN TRAN

alter table dbo.recallrule
        add servicetypeid int;

 alter table dbo.recallrule
        add constraint FK2pisx90yubi3r7txnsl3bq0hh
        foreign key (servicetypeid)
        references dbo.servicetype;

insert into dbversion(version) values(264);

COMMIT TRAN
