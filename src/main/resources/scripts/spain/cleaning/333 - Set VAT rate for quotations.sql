USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[quotation]
	SET vatrate = 
	   (SELECT rate from vatrate 
		LEFT JOIN contact AS quotecontact on quotecontact.personid = quotation.personid
		LEFT JOIN companysettingsforallocatedcompany ON companysettingsforallocatedcompany.vatrate = vatrate.vatcode
		LEFT JOIN subdiv ON subdiv.subdivid = quotecontact.subdivid
		WHERE companysettingsforallocatedcompany.companyid = subdiv.coid
		AND companysettingsforallocatedcompany.orgid = quotation.orgid)
	WHERE vatrate is NULL;

GO

-- Two quotations without company business settings, we can assume 21% Spain (TIC-BIL quotations)
UPDATE [dbo].[quotation] SET vatrate = 21.00 where id in (8607, 8708);

UPDATE [dbo].[quotation] SET vatvalue = vatrate * totalcost / 100 WHERE vatrate is not null;

GO

UPDATE [dbo].[quotation] SET finalcost = vatvalue + totalcost WHERE vatrate is not null;

GO

INSERT INTO dbversion (version) VALUES (333);

COMMIT TRAN


