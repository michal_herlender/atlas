BEGIN TRAN

ALTER TABLE capability_filter  drop column requirementType;
ALTER TABLE [dbo].[capability_filter] DROP CONSTRAINT [FK_accreditedlab];
ALTER TABLE [dbo].[capability_filter] DROP CONSTRAINT [FK_defaultcalibrationprocess];
ALTER TABLE capability_filter  drop column accreditedlabid;
ALTER TABLE capability_filter  drop column calprocesid;
exec sp_rename 'capability_filter.comments', 'internalComments';
exec sp_rename 'capability_filter.description', 'externalComments';


INSERT INTO dbversion(version) VALUES(851);

COMMIT TRAN
