-- Little update to Spanish long name translations (trazable -> no acreditada)
-- 2018-10-02 - Galen Beck

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada con verificación CAR en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada in situ'
 WHERE locale = 'es_ES' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada con verificación CAR in situ'
 WHERE locale = 'es_ES' and servicetypeid = 18
GO

INSERT INTO dbversion(version) VALUES(310);

COMMIT TRAN