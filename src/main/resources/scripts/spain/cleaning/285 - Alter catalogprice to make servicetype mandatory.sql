-- Script 285 - Makes servicetype not nullable, and updates missing values
-- From now on, caltypeid / calibration type is not used by the ERP in the catalogprice entries - just servicetype.
-- There was a mix of 3 cases in production data - records with caltype and service type, records with caltype, records with servicetype!
-- So, we can just use servicetype from now on, all features have been updated accordingly.
-- At a future point, we could delete the caltypeid completely...

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[catalogprice]
   SET [servicetype] = (SELECT servicetypeid from calibrationtype where calibrationtype.caltypeid = catalogprice.caltypeid)
 WHERE servicetype is NULL;
GO

ALTER TABLE [dbo].[catalogprice] ALTER COLUMN servicetype int NOT NULL;
GO

INSERT INTO dbversion(version) VALUES(285);

COMMIT TRAN