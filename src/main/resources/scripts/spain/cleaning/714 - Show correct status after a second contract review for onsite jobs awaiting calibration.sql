USE [atlas]

BEGIN TRAN

	declare @hk int, @st1 int, @act int;

	select @hk = hk.id
		from hook hk where hk.name = 'perform-contract-review';
	
	select @st1 = iss.stateid
		from itemstate iss where iss.description like 'Awaiting onsite calibration';

	select @act = iss.stateid
		from itemstate iss where iss.description like 'Onsite contract review';

	INSERT INTO hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
		VALUES (1,1,@act,2,@st1);

	
	INSERT INTO dbversion VALUES (714)

COMMIT TRAN