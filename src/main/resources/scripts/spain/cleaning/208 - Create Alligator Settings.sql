-- Script 208 - inserts tables with localized settings for Alligator
-- Web service then retrieves these settings so we can have one global build for Alligator

USE [spain]

BEGIN TRAN

    create table dbo.alligatorsettings (
        id int identity not null,
        description varchar(100) not null,
        excellocaltempfolder varchar(255) not null,
        excelrelativetempfolder bit not null,
        excelsavetempcopy bit not null,
        globaldefault bit not null,
        uploadenabled bit not null,
        uploadmoveuponupload bit not null,
        uploadrawdatafolder varchar(255) not null,
        uploadtempdatafolder varchar(255) not null,
        uploadusewindowscredentials bit not null,
        uploadwindowsdomain varchar(255) not null,
        uploadwindowspassword varchar(255) not null,
        uploadwindowsusername varchar(255) not null,
        primary key (id)
    );

    create table dbo.alligatorsettingssubdiv (
        id int identity not null,
        subdivid int not null,
        templateid int not null,
        primary key (id)
    );

	GO

	DELETE FROM alligatorsettings;
	DELETE FROM alligatorsettingssubdiv;

	SET IDENTITY_INSERT [alligatorsettings] ON

	INSERT INTO dbo.alligatorsettings (
		id,
		description,
		excellocaltempfolder,
		excelrelativetempfolder,
		excelsavetempcopy,
		globaldefault,
		uploadenabled,
		uploadmoveuponupload,
		uploadrawdatafolder,
		uploadtempdatafolder,
		uploadusewindowscredentials,
		uploadwindowsdomain,
		uploadwindowspassword,
		uploadwindowsusername)
	VALUES (
		1,
		'Global Default',
		'C:\GroupERP\Certificates',
		1,
		1,
		1,
		0,
		1,
		'C:\GroupERP\RawDataFolder',
		'C:\GroupERP\TempDataFolder',
		0,
		'',
		'',
		''),
		(
		2,
		'ES-TEM-MAD/ZAZ',
		'C:\GroupERP\Certificates',
		1,
		1,
		0,
		1,
		1,
		'C:\GroupERP\RawDataFolder',
		'C:\GroupERP\TempDataFolder',
		1,
		'VHufQ+u5i9jc8LZGJtdtpuz2P0gSGuLXxO1Q87dUQac=',
		'BZN7ToPnQLbr/W8HD/YCA/TSYNtLwGEC',
		'LaxtETvj52h+LVI5qiMrRs8ztI9IF/Qu'),
		(
		3,
		'ES-TIC-BIO/VIT',
		'C:\GroupERP\Certificates',
		1,
		1,
		0,
		1,
		1,
		'\\TB-DC1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates',
		'\\TB-DC1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates_primarios',
		1,
		'VHufQ+u5i9jc8LZGJtdtpuz2P0gSGuLXxO1Q87dUQac=',
		'BZN7ToPnQLbr/W8HD/YCA/TSYNtLwGEC',
		'LaxtETvj52h+LVI5qiMrRs8ztI9IF/Qu')

	SET IDENTITY_INSERT [alligatorsettings] OFF

	GO

-- Subdivs: TEM-MAD, TEM-ZAZ - 6645, 6644 uses settings id 2
-- Subdivs: TIC-BIL, TIC-VIT - 6642, 6643 uses settings id 3
-- Subdiv : Netherlands - Zoetermeer - Gerard - 6923 uses settings ID 2 (Madrid - development)

	SET IDENTITY_INSERT [alligatorsettingssubdiv] ON

    INSERT INTO [alligatorsettingssubdiv] 
	(id, subdivid, templateid) VALUES
	(1, 6645, 2),
	(2, 6644, 2),
	(3, 6643, 3),
	(4, 6642, 3),
	(5, 6923, 2);

	SET IDENTITY_INSERT [alligatorsettingssubdiv] OFF

	GO

	INSERT INTO dbversion (version) VALUES (208);

	GO

COMMIT TRAN