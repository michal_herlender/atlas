
BEGIN TRAN;

-- Create service type with standard colors
insert into dbo.servicetype ([displaycolour],[displaycolourfasttrack],[longname],[shortname],[canbeperformedbyclient]) 
VALUES ('#FFFFFF','#FEE0B4','Calibration Managed By Client','CLIENT',1);



-- Create cal type for new service type with values from in house standard service type
insert into calibrationtype
			(accreditationspecific,
			active,
			certnoresetyearly,
			firstpagepapertype,
			labeltemplatepath,
			orderby,
			pdfcontinuationwatermark,
			pdfheaderwatermark,
			recalldateonlabel,
			restpagepapertype,
			servicetypeid,
			recallRequirementType)
select accreditationspecific,
	   1,
	   certnoresetyearly,
	   firstpagepapertype,
	   labeltemplatepath,
	   (select servicetypeid from servicetype s where s.shortname = 'CLIENT'),
	   pdfcontinuationwatermark,
	   pdfheaderwatermark,
	   recalldateonlabel,
	   restpagepapertype,
	   (select servicetypeid from servicetype s where s.shortname = 'CLIENT') as 'servicetypeid',
	   recallRequirementType
from calibrationtype
inner join servicetype on servicetype.servicetypeid = calibrationtype.servicetypeid
where servicetype.shortname = 'ST-IH';

-- Insert default quotation calibration conditions for new cal type and every business company
INSERT INTO [dbo].[defaultquotationcalibrationcondition]
(conditiontext, lastModified, caltypeid, orgid)
SELECT '',
'2016-01-01',
(select calibrationtype.caltypeid 
	FROM servicetype 
	INNER JOIN  calibrationtype
	ON servicetype.servicetypeid = calibrationtype.servicetypeid
	WHERE servicetype.shortname = 'CLIENT'), 
company.coid
FROM company
WHERE company.corole = 5;


-- Insert links to jobtypes
INSERT INTO jobtypecaltype (defaultvalue, jobtypeid, caltypeid)
  SELECT 0,4,caltypeid
  FROM calibrationtype
    INNER JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'CLIENT';


-- Insert accreditation levels for new cal type
INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'APPROVE', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'CLIENT';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'REMOVED', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'CLIENT';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'SUPERVISED', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'CLIENT';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'PERFORM', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'CLIENT';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'SIGN', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'CLIENT';



--Add translations
INSERT INTO servicetypelongnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.longname from servicetype
WHERE servicetype.shortname = 'CLIENT';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Calibración gestionada por el cliente'
from servicetype
where servicetype.shortname = 'CLIENT';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'fr_FR', 'Vérification gérée par le client'
from servicetype
where servicetype.shortname = 'CLIENT';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.shortname from servicetype
WHERE servicetype.shortname = 'CLIENT';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'es_ES','CLIENT'  from servicetype
WHERE servicetype.shortname = 'CLIENT';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'fr_FR','CLIENT' from servicetype
WHERE servicetype.shortname = 'CLIENT';

UPDATE servicetype SET servicetype.canbeperformedbyclient = 0 WHERE servicetype.longname LIKE 'External%';

COMMIT TRAN;





