-- In Jobitem dashboard move status 'awaiting cleaning' from Laboratory tab to Goods In/Out tab under other (ticket #1259)

USE [spain]
GO

BEGIN TRAN

	UPDATE dbo.stategrouplink
	SET groupid=35
	WHERE id = (
		select stg.id
		from dbo.stategrouplink stg
		join dbo.itemstate st on st.stateid = stg.stateid
		where st.description like 'Awaiting cleaning'
		and stg.type like 'CURRENT_SUBDIV'
	)
	
	go


	INSERT INTO dbversion(version) VALUES(329);

COMMIT TRAN
