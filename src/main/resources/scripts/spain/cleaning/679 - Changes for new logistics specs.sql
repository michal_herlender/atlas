USE [atlas]
GO

BEGIN TRAN
	
	declare @sdId int;
	
	INSERT INTO systemdefault (defaultdatatype,defaultdescription,defaultname,defaultvalue,endd,[start],groupid,groupwide)
		VALUES ('boolean','Defines whether to add a step for approving the creation of the delivery note','Approve delivery note creation by CSR','false',0,0,1,0);
	select @sdId = max(sd.defaultid)  from systemdefault sd;
	
	-- name translations	
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@sdId,'de_DE','Genehmigen Sie die Erstellung von Lieferscheinen durch CSR');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@sdId,'en_GB','Approve delivery note creation by CSR');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@sdId,'es_ES','Aprobar la creaci�n de albar�n por CSR');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@sdId,'fr_FR','Approuver la cr�ation du bon de livraison par le CRC');
		
	-- description translation
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@sdId,'de_DE','Definiert, ob ein Schritt zum Genehmigen der Erstellung des Lieferscheins hinzugef�gt werden soll');
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@sdId,'en_GB','Defines whether to add a step for approving the creation of the delivery note');
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@sdId,'es_ES','Define si agregar un paso para aprobar la creaci�n del albar�n');
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@sdId,'fr_FR','D�finit s''il faut ajouter une �tape d''approbation pour la cr�ation du bon de livraison');
		
	-- role
	INSERT INTO systemdefaultcorole (coroleid,defaultid)
		VALUES (5,@sdId);
		
	-- scope
	INSERT INTO systemdefaultscope ([scope],defaultid)
		VALUES (0,@sdId);
	INSERT INTO systemdefaultscope ([scope],defaultid)
		VALUES (2,@sdId);
		
		
		go
---- workflow
	DECLARE @newStatus int,
			@newActivity int,
			@awaitingDNtoClient int;
			
	select @awaitingDNtoClient = stateid from itemstate iss
		where iss.description like 'Awaiting delivery note to client';		

			
	INSERT INTO itemstate ([type],active,description,retired)
		VALUES ('workstatus',1,'Awaiting approval for delivery note creation',0);
	select @newStatus = max(stateid) from itemstate;	
	
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newStatus,'de_DE','Warten auf die Genehmigung zur Erstellung des Lieferscheins');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newStatus,'en_GB','Awaiting approval for delivery note creation');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newStatus,'es_ES','Esperando aprobaci�n para la creaci�n del albar�n de entrega');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newStatus,'fr_FR','En attente d''approbation pour la cr�ation du bon de livraison');

		
		
	INSERT INTO itemstate ([type],active,description,retired)
		VALUES ('workactivity',1,'Delivery note approved',0);
	select @newActivity = max(stateid) from itemstate;	
		
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newActivity,'de_DE','Lieferschein genehmigt');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newActivity,'en_GB','Delivery note approved');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newActivity,'es_ES','Albar�n de entrega aprobado');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newActivity,'fr_FR','Bon de livraison approuv�');
		
	-- next activity
	INSERT INTO nextactivity (manualentryallowed,activityid,statusid)
		VALUES (1,@newActivity,@newStatus);
	
	-- link
	INSERT INTO outcomestatus (defaultoutcome,activityid,statusid)
		VALUES (0,@newActivity,@awaitingDNtoClient);
		
	DECLARE @lu int,
			@os0 int,
			@os1 int;	
	-- lookup
	INSERT INTO lookupinstance (lookup,lookupfunction)
		VALUES ('Lookup_DNCreationApprovalRequired',61);
	select @lu = max(id) from lookupinstance;
		
	-- os
	INSERT INTO outcomestatus (defaultoutcome,lookupid,statusid)
		VALUES (0,@lu,@newStatus);
	select @os0 = max(id) from outcomestatus;
		
	INSERT INTO atlas.dbo.outcomestatus (defaultoutcome,lookupid,statusid)
		VALUES (0,@lu,@awaitingDNtoClient);
	select @os1 = max(id) from outcomestatus;
	
	-- lr
	INSERT INTO lookupresult (lookupid,outcomestatusid,resultmessage)
		VALUES (@lu,@os0,0);
	INSERT INTO lookupresult (lookupid,outcomestatusid,resultmessage)
		VALUES (@lu,@os1,1);
	
	-- link top of subworkflow
	DECLARE @luCompanyOnStop int,
			@luCompanyOnStopNoOs int;
	
	select @luCompanyOnStop = lu.id from lookupinstance lu
		where lu.lookup like 'Lookup_CompanyIsOnStop (inhouse return to client from this subdivision)';
	
	select @luCompanyOnStopNoOs = lr.outcomestatusid from lookupresult lr
		where lr.lookupid = @luCompanyOnStop and resultmessage = 1;
		
	UPDATE outcomestatus
		SET statusid=NULL,lookupid=@lu
		WHERE id=@luCompanyOnStopNoOs;
				
	UPDATE itemstate
		SET lookupid=@lu
		where description like 'Payment received';
		
	UPDATE itemstate
		SET lookupid=@lu
		where description like 'Company trusted';
		
	-----	
	go
		
	---- jobitem return to address
	ALTER TABLE jobitem ADD returnToAddress int NULL;
	GO
	ALTER TABLE jobitem ADD CONSTRAINT FK_address_addrid FOREIGN KEY (addrid) REFERENCES address(addrid);
	GO
	
		
	
	
	INSERT INTO dbversion(version) VALUES(679);

COMMIT TRAN

		