
USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.systemdefault ALTER COLUMN defaultvalue varchar(100) NOT NULL;
UPDATE dbo.systemdefault SET defaultvalue = '[MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY]' WHERE defaultname = 'Working days';

UPDATE dbo.systemdefaultapplication SET defaultvalue = '[MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY]' WHERE defaultvalue = 'MONDAY_TO_FRIDAY';
UPDATE dbo.systemdefaultapplication SET defaultvalue = '[MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY]' WHERE defaultvalue = 'MONDAY_TO_SATURDAY';

INSERT INTO dbversion(version) VALUES(685);

COMMIT TRAN



