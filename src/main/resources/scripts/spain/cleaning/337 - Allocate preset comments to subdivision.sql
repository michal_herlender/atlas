USE spain

BEGIN TRAN

ALTER TABLE presetcomment
DROP CONSTRAINT FKce580c1t0iksvudjcp0uat9sp

GO

UPDATE presetcomment SET orgid = 6642 /* Bilbao */
WHERE orgid = 6578 /* TIC */

UPDATE presetcomment SET orgid = 6645 /* Madrid */
WHERE orgid = 6579 /* TEM */

UPDATE presetcomment SET orgid = 6907 /* Toledo */
WHERE orgid = 6670 /* MCI */

UPDATE presetcomment SET orgid = 6951 /* Casablanca */
WHERE orgid = 6685 /* Morocco */

UPDATE presetcomment SET orgid = 6961 /* Metz */
WHERE orgid = 6690 /* Trescal SA */

GO

ALTER TABLE presetcomment
ADD CONSTRAINT FK42k4f3ni5ed54u4u61yprimax
FOREIGN KEY (orgid)
REFERENCES subdiv (subdivid)

INSERT INTO dbversion(version) VALUES(337);

COMMIT TRAN