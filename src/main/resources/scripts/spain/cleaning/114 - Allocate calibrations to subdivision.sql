USE spain

BEGIN TRAN

UPDATE calibration SET lastModified = starttime, orgid = subdivid
FROM calibration
LEFT JOIN address ON address.addrid = calibration.caladdressid

COMMIT TRAN