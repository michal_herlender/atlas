USE [atlas]

BEGIN TRAN

DECLARE @id int;

select @id = statusid
from basestatus WHERE name='Canceled' and type = 'certificate';

UPDATE dbo.basestatus
	SET name='Deleted'
	WHERE statusid = @id;

UPDATE dbo.basestatusnametranslation
	SET [translation]='Deleted'
	WHERE locale='en_GB' AND statusid = @id;
UPDATE dbo.basestatusnametranslation
	SET [translation]='Eliminado'
	WHERE locale='es_ES' AND statusid = @id;
UPDATE dbo.basestatusnametranslation
	SET [translation]='Supprim�'
	WHERE locale='fr_FR' AND statusid = @id;
UPDATE dbo.basestatusnametranslation
	SET [translation]='Gel�scht'
	WHERE locale='de_DE' AND statusid = @id;


	INSERT INTO dbversion(version) VALUES (637)

COMMIT TRAN