
USE [atlas]

BEGIN TRAN    
	
DECLARE @activity_4101 INT;

SELECT @activity_4101 = stateid FROM dbo.itemstate WHERE description = 'Failure report sent to client';

UPDATE dbo.itemstate SET description = 'Failure Report available' WHERE stateid = @activity_4101;

UPDATE dbo.itemstatetranslation SET translation = 'Failure Report available' WHERE stateid = @activity_4101 AND locale = 'en_GB';
UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie disponible' WHERE stateid = @activity_4101 AND locale = 'fr_FR';
UPDATE dbo.itemstatetranslation SET translation = 'Informe de falla disponible' WHERE stateid = @activity_4101 AND locale = 'es_ES';
UPDATE dbo.itemstatetranslation SET translation = 'Störungsmeldung vorhanden' WHERE stateid = @activity_4101 AND locale = 'de_DE';

GO

INSERT INTO dbversion(version) VALUES(538);

COMMIT TRAN