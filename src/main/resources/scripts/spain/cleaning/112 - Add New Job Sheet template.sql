-- Added comment including old job sheet VM in case we want to switch back and forth
-- (We might split the two files into separate template definitions)

USE [spain]

BEGIN TRAN

UPDATE dbo.template
SET templatepath = '/birt/docs/jobsheet.rptdesign'
--SET templatepath = '/docs/job sheet.vm'
WHERE name = 'Job Sheet'

COMMIT TRAN