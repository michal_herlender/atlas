-- Updates a few stategrouplinks involving transit statuses to ALLOCATED_SUBDIV (was CURRENT_SUBDIV)
-- so that they show up correctly when no address is set
-- Running on Spain-test and Spain, will update France-test on next deploy.
-- Galen Beck - 2017-05-05

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[stategrouplink]
   SET [type] = 'ALLOCATED_SUBDIV'
 WHERE [groupid] = 35 AND [stateid] in (2, 6, 48, 172, 193, 4070);
GO

COMMIT TRAN