USE [spain]

BEGIN TRAN

IF NOT EXISTS (SELECT [vatcode] FROM [dbo].[vatrate] WHERE [type] = 0 AND [countryid] = 100)
    INSERT INTO [dbo].[vatrate] ([vatcode], [description], [rate], [type], [countryid]) VALUES ('A', 'Luxembourg', 17, 0, 100);
IF NOT EXISTS (SELECT [vatcode] FROM [dbo].[vatrate] WHERE [type] = 1 AND [countryid] = 61 AND vatcode='B')
    INSERT INTO [dbo].[vatrate] ([vatcode], [description], [rate], [type], [countryid]) VALUES ('B', 'Overseas/DOM-TOM', 8.5, 1, 61);
IF NOT EXISTS (SELECT [vatcode] FROM [dbo].[vatrate] WHERE [type] = 1 AND [countryid] = 61 AND vatcode='C')
    INSERT INTO [dbo].[vatrate] ([vatcode], [description], [rate], [type], [countryid]) VALUES ('C', 'Exempt/Exon�r�', 0, 1, 61);

INSERT INTO dbversion(version) VALUES(305);

COMMIT TRAN