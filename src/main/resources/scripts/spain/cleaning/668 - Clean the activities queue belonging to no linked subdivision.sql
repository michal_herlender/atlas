use [atlas]
go

BEGIN TRAN

	DELETE FROM
	dbo.advesojobItemactivityqueue WHERE id IN (SELECT DISTINCT q.id FROM 
	dbo.advesojobItemactivityqueue q
	JOIN dbo.jobitemaction jia ON q.jobitemactionid = jia.id
	JOIN dbo.jobitem ji ON ji.jobitemid = jia.jobitemid
	JOIN dbo.job j ON j.jobid = ji.jobid
	JOIN dbo.contact cn ON cn.personid = j.personid
	JOIN dbo.subdiv s ON cn.subdivid = s.subdivid
	JOIN dbo.company c ON s.coid = c.coid
	JOIN dbo.subdiv ss ON j.orgid = ss.subdivid
	JOIN dbo.company cc ON ss.coid = cc.coid
	JOIN dbo.systemdefaultcorole sdc ON c.corole = sdc.coroleid
	LEFT JOIN dbo.systemdefaultapplication sda ON sda.orgid = ss.coid  
	AND sda.defaultid = 47 AND sda.subdivid = s.subdivid
	WHERE sda.id IS NULL OR sda.defaultvalue = 'false');
 
	INSERT INTO dbversion(version) VALUES(668);

COMMIT TRAN

