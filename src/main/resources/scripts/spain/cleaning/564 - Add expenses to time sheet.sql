USE [atlas];

GO

BEGIN TRAN

ALTER TABLE dbo.timesheetentry ADD expensevalue NUMERIC(19,2);

ALTER TABLE dbo.timesheetentry ADD expensecurrencyid INT;

ALTER TABLE dbo.timesheetentry
ADD CONSTRAINT FK_timesheetentry_expensecurrency
FOREIGN KEY (expensecurrencyid)
REFERENCES dbo.supportedcurrency;

INSERT INTO dbversion(version) VALUES(564);

COMMIT TRAN