-- Adds translations (if not already present) for short name translations of service types
-- Author Galen Beck 2016-10-10 (Updated 2016-10-12 to do new updates on Spain and Spain-Test)
-- Note COMMIT at end, change to ROLLBACK TRAN to test

USE [spain]
GO

BEGIN TRAN

-- Declare service type IDs, these can be different between the test and production databases

DECLARE @AccreditedInhouse INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'AC-IH');
DECLARE @StandardInhouse INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'ST-IH');
DECLARE @Other INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'OTHER');
DECLARE @AccreditedOnsite INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'AC-OS');
DECLARE @StandardOnsite INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'ST-OS');
DECLARE @Repair INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'REP');
DECLARE @AccreditedExternal INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'AC-EX');
DECLARE @StandardExternal INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'STD-EX');

DECLARE @AccreditedInhouseMV INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'AC-IH-MV');
DECLARE @StandardInhouseMV INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'ST-IH-MV');
DECLARE @AccreditedOnsiteMV INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'AC-OS-MV');
DECLARE @StandardOnsiteMV INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'ST-OS-MV');
DECLARE @AccreditedExternalMV INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'AC-EX-MV');
DECLARE @StandardExternalMV INT = (SELECT [servicetypeid] FROM [servicetype] WHERE [shortname] = 'ST-EX-MV');

-- French short translations were missing for original service types 1 to 8

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @AccreditedInhouse)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedInhouse,'fr_FR','VAC-EL')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'VAC-EL' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @AccreditedInhouse;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @StandardInhouse)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardInhouse,'fr_FR','VST-EL')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'VST-EL' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @StandardInhouse;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @Other)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@Other,'fr_FR','AUTRE')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'AUTRE' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @Other;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @AccreditedOnsite)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedOnsite,'fr_FR','VAC-SI')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'VAC-SI' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @AccreditedOnsite;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @StandardOnsite)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardOnsite,'fr_FR','VST-SI')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'VST-SI' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @StandardOnsite;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @Repair)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@Repair,'fr_FR','REP-EL')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'REP-EL' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @Repair;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @AccreditedExternal)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedExternal,'fr_FR','VAC-EX')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'VAC-EX' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @AccreditedExternal;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @StandardExternal)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardExternal,'fr_FR','VST-EX')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'VST-EX' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @StandardExternal;

-- All short translations were missing for six new service types (15 to 20 on spain production, 9 to 14 on spain-test)

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'en_GB' AND [servicetypeid] = @AccreditedInhouseMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedInhouseMV,'en_GB','AC-IH-MV')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'AC-IH-MV' WHERE 
	[locale] = 'en_GB' AND [servicetypeid] = @AccreditedInhouseMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'es_ES' AND [servicetypeid] = @AccreditedInhouseMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedInhouseMV,'es_ES','AC-EL-SJ')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'AC-EL-SJ' WHERE 
	[locale] = 'es_ES' AND [servicetypeid] = @AccreditedInhouseMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @AccreditedInhouseMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedInhouseMV,'fr_FR','EAC-EL')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'EAC-EL' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @AccreditedInhouseMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'en_GB' AND [servicetypeid] = @StandardInhouseMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardInhouseMV,'en_GB','ST-IH-MV')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'ST-IH-MV' WHERE 
	[locale] = 'en_GB' AND [servicetypeid] = @StandardInhouseMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'es_ES' AND [servicetypeid] = @StandardInhouseMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardInhouseMV,'es_ES','ST-EL-SJ')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'ST-EL-SJ' WHERE 
	[locale] = 'es_ES' AND [servicetypeid] = @StandardInhouseMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @StandardInhouseMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardInhouseMV,'fr_FR','EST-EL')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'EST-EL' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @StandardInhouseMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'en_GB' AND [servicetypeid] = @AccreditedOnsiteMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedOnsiteMV,'en_GB','AC-OS-MV')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'AC-OS-MV' WHERE 
	[locale] = 'en_GB' AND [servicetypeid] = @AccreditedOnsiteMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'es_ES' AND [servicetypeid] = @AccreditedOnsiteMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedOnsiteMV,'es_ES','AC-IS-SJ')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'AC-IS-SJ' WHERE 
	[locale] = 'es_ES' AND [servicetypeid] = @AccreditedOnsiteMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @AccreditedOnsiteMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedOnsiteMV,'fr_FR','EAC-SS')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'EAC-SI' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @AccreditedOnsiteMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'en_GB' AND [servicetypeid] = @StandardOnsiteMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardOnsiteMV,'en_GB','ST-OS-MV')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'ST-OS-MV' WHERE 
	[locale] = 'en_GB' AND [servicetypeid] = @StandardOnsiteMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'es_ES' AND [servicetypeid] = @StandardOnsiteMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardOnsiteMV,'es_ES','ST-IS-SJ')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'ST-IS-SJ' WHERE 
	[locale] = 'es_ES' AND [servicetypeid] = @StandardOnsiteMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @StandardOnsiteMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardOnsiteMV,'fr_FR','EST-SS')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'EST-SI' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @StandardOnsiteMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'en_GB' AND [servicetypeid] = @AccreditedExternalMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedExternalMV,'en_GB','AC-EX-MV')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'AC-EX-MV' WHERE 
	[locale] = 'en_GB' AND [servicetypeid] = @AccreditedExternalMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'es_ES' AND [servicetypeid] = @AccreditedExternalMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedExternalMV,'es_ES','AC-EX-SJ')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'AC-EX-SJ' WHERE 
	[locale] = 'es_ES' AND [servicetypeid] = @AccreditedExternalMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @AccreditedExternalMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@AccreditedExternalMV,'fr_FR','EAC-EX')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'EAC-EX' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @AccreditedExternalMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'en_GB' AND [servicetypeid] = @StandardExternalMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardExternalMV,'en_GB','ST-EX-MV')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'ST-EX-MV' WHERE 
	[locale] = 'en_GB' AND [servicetypeid] = @StandardExternalMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'es_ES' AND [servicetypeid] = @StandardExternalMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardExternalMV,'es_ES','ST-EX-SJ')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'ST-EX-SJ' WHERE 
	[locale] = 'es_ES' AND [servicetypeid] = @StandardExternalMV;

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypeshortnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = @StandardExternalMV)
	INSERT INTO [servicetypeshortnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (@StandardExternalMV,'fr_FR','EST-EX')
ELSE
    UPDATE [servicetypeshortnametranslation] SET [translation] = 'EST-EX' WHERE 
	[locale] = 'fr_FR' AND [servicetypeid] = @StandardExternalMV;

GO

COMMIT TRAN