-- Script written by Egbert to add a new lookup of "internal delivery requirements" 
-- after the activity indicating "Client approval not required"
-- Run on production and test servers 2017-05-10

BEGIN TRAN

DECLARE @Action_ClientApprovalNotRequired int, @Lookup_AfterClientApprNotRequ int, @Lookup_PreDeliveryRequirements3 int, @Outcome int, @Outcome_AwaitingReturnDelivery int

SET @Action_ClientApprovalNotRequired = (SELECT stateid FROM itemstate WHERE description = 'Client approval not required')
SET @Outcome_AwaitingReturnDelivery = (SELECT outcomestatus.id FROM outcomestatus LEFT JOIN itemstate ON itemstate.stateid = outcomestatus.statusid WHERE description = 'Awaiting internal return delivery note')
SET @Lookup_AfterClientApprNotRequ = (SELECT lookupid FROM itemstate WHERE stateid = @Action_ClientApprovalNotRequired)

INSERT INTO lookupinstance(lookup, lookupfunction) VALUES
('Lookup_PreDeliveryRequirements3', 39)

SET @Lookup_PreDeliveryRequirements3 = SCOPE_IDENTITY()

UPDATE itemstate SET lookupid = @Lookup_PreDeliveryRequirements3 WHERE stateid = @Action_ClientApprovalNotRequired

INSERT INTO outcomestatus(activityid, defaultoutcome, lookupid, statusid) VALUES
(NULL, 1, @Lookup_AfterClientApprNotRequ, NULL)

SET @Outcome = SCOPE_IDENTITY()

INSERT INTO lookupresult(lookupid, resultmessage, outcomestatusid) VALUES
(@Lookup_PreDeliveryRequirements3, 0, @Outcome),
(@Lookup_PreDeliveryRequirements3, 1, @Outcome_AwaitingReturnDelivery),
(@Lookup_PreDeliveryRequirements3, 2, @Outcome_AwaitingReturnDelivery)

COMMIT TRAN