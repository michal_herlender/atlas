-- Fixes garbled translations where accented characters were not inserted correctly
-- Uses translations confirmed by Claus Hakenesch (Germany CFO) 2019-11-11 as original file 2019-03-15 was missing some translations

USE [atlas]
GO

BEGIN TRAN

UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Grundstücke und Gebäude' WHERE locale='de_DE' and id = 1;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Betriebs- und Geschäftsausstattung' WHERE locale='de_DE' and id = 2;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Sonstige Software' WHERE locale='de_DE' and id = 3;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'ERP-Software' WHERE locale='de_DE' and id = 4;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - BESCHLEUNIGUNGSMESSER' WHERE locale='de_DE' and id = 5;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - AKUSTISCH' WHERE locale='de_DE' and id = 6;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - CHEMIE - UMWELT' WHERE locale='de_DE' and id = 7;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - KLIMATISCH' WHERE locale='de_DE' and id = 8;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - GASSTROM' WHERE locale='de_DE' and id = 9;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - WASSERSTROM' WHERE locale='de_DE' and id = 10;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - DIMENSIONAL' WHERE locale='de_DE' and id = 11;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - SONSTIGE' WHERE locale='de_DE' and id = 12;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - ELEKTRISCH' WHERE locale='de_DE' and id = 13;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - KRAFT - DREHMOMENT' WHERE locale='de_DE' and id = 14;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - INFORMATIK' WHERE locale='de_DE' and id = 15;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - MASSE - GEWICHT' WHERE locale='de_DE' and id = 16;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - DOMÄNÜBERGREIFEND' WHERE locale='de_DE' and id = 17;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - DRUCK' WHERE locale='de_DE' and id = 18;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - RADIOMETRY - PHOTOMETRY' WHERE locale='de_DE' and id = 19;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - IONISIERENDE STRAHLUNG' WHERE locale='de_DE' and id = 20;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - TEMPERATUR - HYGROMETRIE' WHERE locale='de_DE' and id = 21;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Technische Anlagen und Maschinen - VOLUMEN' WHERE locale='de_DE' and id = 22;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'IT Ausstattung' WHERE locale='de_DE' and id = 23;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Logistikausstattung' WHERE locale='de_DE' and id = 24;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Fuhrpark' WHERE locale='de_DE' and id = 25;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Wasserversorgung' WHERE locale='de_DE' and id = 26;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Stromversorgung' WHERE locale='de_DE' and id = 27;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Gasversorgung' WHERE locale='de_DE' and id = 28;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Software und IT-Verbrauchsmaterial' WHERE locale='de_DE' and id = 30;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Büromaterial' WHERE locale='de_DE' and id = 31;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Verbrauchsmaterial - Kopierer' WHERE locale='de_DE' and id = 32;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Sicherheit- und Überwachungsdienste' WHERE locale='de_DE' and id = 34;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Objektmieten' WHERE locale='de_DE' and id = 37;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Mieten - Mitarbeiterunterkünfte' WHERE locale='de_DE' and id = 38;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'KfZ-Leasing (langfristig)' WHERE locale='de_DE' and id = 39;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Mieten - Gebäude' WHERE locale='de_DE' and id = 45;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Wartung- und Instandhaltung Gebäude' WHERE locale='de_DE' and id = 46;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Wartung- und Instandhaltung Fuhrpark' WHERE locale='de_DE' and id = 47;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Wartung- und Instandhaltung Büro- und Geschäftausstattung' WHERE locale='de_DE' and id = 48;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'allgemeine Dokumentation' WHERE locale='de_DE' and id = 52;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Anwaltskosten' WHERE locale='de_DE' and id = 56;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Wirtschaftsprüfürkosten' WHERE locale='de_DE' and id = 57;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Anmelde- und Zulassungsgebühren ' WHERE locale='de_DE' and id = 58;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Sonstige gebühren' WHERE locale='de_DE' and id = 59;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Werbung und PR-Kosten' WHERE locale='de_DE' and id = 60;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Zuwendungen' WHERE locale='de_DE' and id = 62;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Kraftstoffe' WHERE locale='de_DE' and id = 65;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Taxi, Parken, Mautgebühren' WHERE locale='de_DE' and id = 66;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Hotel- und Bewirtungskosten' WHERE locale='de_DE' and id = 68;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umzugskosten Mitarbeiter' WHERE locale='de_DE' and id = 70;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Telefon' WHERE locale='de_DE' and id = 72;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Mobilfunk' WHERE locale='de_DE' and id = 73;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'IT Network / Data' WHERE locale='de_DE' and id = 74;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Kosten des Geldverkehrs' WHERE locale='de_DE' and id = 75;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Personalbeschaffung' WHERE locale='de_DE' and id = 77;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Management Fees' WHERE locale='de_DE' and id = 78;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Weiterbelastung Waren - Beschaffung NA' WHERE locale='de_DE' and id = 79;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Ersatzteile für Bestellung NA' WHERE locale='de_DE' and id = 82;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'externe Lohnarbeit NA' WHERE locale='de_DE' and id = 85;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'externe Lohnarbeit EU' WHERE locale='de_DE' and id = 86;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'externe Lohnarbeit EX' WHERE locale='de_DE' and id = 87;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'interne Lohnarbeit NA' WHERE locale='de_DE' and id = 88;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Verpackungsmaterial' WHERE locale='de_DE' and id = 91;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Equipment rentals ' WHERE locale='de_DE' and id = 95;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Nebenkosten - Stromversorgung' WHERE locale='de_DE' and id = 98;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Ersatzteile EU' WHERE locale='de_DE' and id = 101;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Verbrauchsmaterial NA' WHERE locale='de_DE' and id = 102;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Arbeitskleidung' WHERE locale='de_DE' and id = 108;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Miete - Archivierungsstätte' WHERE locale='de_DE' and id = 112;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Miete - Geschäftsgebäude' WHERE locale='de_DE' and id = 113;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Miete/Leasing KFZ (langfristig, > 6 Mo)' WHERE locale='de_DE' and id = 116;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Mieten - Büroausstattung' WHERE locale='de_DE' and id = 118;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Meiten - Kopierer' WHERE locale='de_DE' and id = 119;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Instandhaltung Gebäude' WHERE locale='de_DE' and id = 121;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Refürenzstandards Kalibrierung - Wiederkehrend - NA' WHERE locale='de_DE' and id = 122;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Refürenzstandards Reparturkosten - NA' WHERE locale='de_DE' and id = 123;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Refürenzstandards Kalibrierung - Wiederkehrend - EU' WHERE locale='de_DE' and id = 124;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Instandhaltung KFZ' WHERE locale='de_DE' and id = 126;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Leiharbeiter IDOE' WHERE locale='de_DE' and id = 136;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Pürfungsgebühren - Cofrac/DAKKS' WHERE locale='de_DE' and id = 138;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Fracht und Transportkosten Einkauf' WHERE locale='de_DE' and id = 139;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Fracht und Transportkosten Einkauf' WHERE locale='de_DE' and id = 140;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Tankkosten' WHERE locale='de_DE' and id = 148;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Taxi, Parkgebühren, Strafzettel' WHERE locale='de_DE' and id = 149;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Hotel- und Bewirtungskosten' WHERE locale='de_DE' and id = 151;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umzugskosten - Mitarbeiter' WHERE locale='de_DE' and id = 153;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze Kalibrierung' WHERE locale='de_DE' and id = 159;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Wertberichtigung Umsätze' WHERE locale='de_DE' and id = 160;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze Reparaturen' WHERE locale='de_DE' and id = 161;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Sonstige Umsätze' WHERE locale='de_DE' and id = 162;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze Ersatzteile' WHERE locale='de_DE' and id = 163;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze Fracht und Transport' WHERE locale='de_DE' and id = 164;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze IT-Dienstleistungen' WHERE locale='de_DE' and id = 165;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze Service Leistungen z.B. management fees' WHERE locale='de_DE' and id = 166;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze aus Geräteverkäufen' WHERE locale='de_DE' and id = 167;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze Vermietungen' WHERE locale='de_DE' and id = 168;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Umsätze Fremdvergabe' WHERE locale='de_DE' and id = 169;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Kalibrierung' WHERE locale='de_DE' and id = 170;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Justage' WHERE locale='de_DE' and id = 171;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Reparatur' WHERE locale='de_DE' and id = 172;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus sonstiges' WHERE locale='de_DE' and id = 173;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Ersatzteile' WHERE locale='de_DE' and id = 174;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Transport' WHERE locale='de_DE' and id = 175;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus IT' WHERE locale='de_DE' and id = 176;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Schulung / Audit / Fuhrpark' WHERE locale='de_DE' and id = 177;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Wiederverkauf' WHERE locale='de_DE' and id = 178;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Mieten' WHERE locale='de_DE' and id = 179;
UPDATE [dbo].[nominalcodetitletranslation] SET [translation] = 'Vorkasse - Umsätze aus Auslagerung' WHERE locale='de_DE' and id = 180;

GO

-- Some payment mode translations appear to be loaded with an incorrectly capitalized locale; this fixes them.

UPDATE [dbo].[paymentmodetranslation]
   SET [locale] = 'de_DE'
 WHERE [locale] = 'DE_de'
GO

INSERT INTO dbversion(version) VALUES(545);

COMMIT TRAN