-- Updates incorrect "inactive" zero dollar calibration costs on job costings
-- which were not appearing for invoicing

-- before running for production, we can change to ROLLBACK and 
-- verify updated count = number of records reported in query/export script U035
USE [atlas]

BEGIN TRAN

UPDATE [dbo].[costs_calibration]
	SET active = 1
	FROM jobcostingitem jci 
  INNER JOIN costs_calibration costs_cal ON jci.calcost_id = costs_cal.costid
  INNER JOIN costs_repair costs_rep ON jci.repaircost_id = costs_rep.costid
  INNER JOIN costs_purchase costs_pur ON jci.purchasecost_id = costs_pur.costid
  INNER JOIN costs_adjustment costs_adj ON jci.adjustmentcost_id = costs_adj.costid
  WHERE jci.finalcost = 0
  AND costs_cal.active = 0
  AND costs_rep.active = 0
  AND costs_pur.active = 0
  AND costs_adj.active = 0
  AND costs_cal.finalcost = 0

INSERT INTO dbversion(version) VALUES(821);

COMMIT TRAN