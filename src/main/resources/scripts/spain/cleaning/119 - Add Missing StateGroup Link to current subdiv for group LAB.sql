BEGIN TRAN

ALTER TABLE stategrouplink DROP CONSTRAINT UQ__stategro__02A2645F4FDCC2C0, UK_pvryc52owotucf6jvsrx1tk38

INSERT INTO stategrouplink(groupid, stateid, type)
SELECT 36, sgl.stateid, 'CURRENT_SUBDIV'
FROM stategrouplink AS sgl
WHERE sgl.groupid = 36

ROLLBACK TRAN