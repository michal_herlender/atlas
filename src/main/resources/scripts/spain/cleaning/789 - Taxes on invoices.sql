use atlas

begin tran

create table dbo.invoicetax (
	id int identity not null,
	amount numeric(10,2) not null,
	description varchar(30),
	rate numeric(10,2),
	tax numeric(10,2) not null,
	invoiceid int not null,
	vatrateid varchar(1),
	primary key (id)
);

go

alter table dbo.invoicetax add constraint FK_invoicetax_invoice 
foreign key (invoiceid) references dbo.invoice;

alter table dbo.invoicetax add constraint FK_invoicetax_vatrate 
foreign key (vatrateid) references dbo.vatrate;

alter table dbo.invoiceitem 
add taxable tinyint not null default 1, taxamount numeric(10,2);

alter table dbo.jobexpenseitempo add constraint FK_jobexpenseitempo_lastmodifiedby 
foreign key (lastModifiedBy) references dbo.contact;

alter table dbo.jobexpenseitempo add constraint FK_jobexpenseitempo_bpo 
foreign key (bpoid) references dbo.bpo;

alter table dbo.jobexpenseitempo add constraint FK_jobexpenseitempo_expenseitem 
foreign key (expenseitemid) references dbo.jobexpenseitem;

alter table dbo.jobexpenseitempo add constraint FK_jobexpenseitempo_po 
foreign key (poid) references dbo.po;

alter table invoice
add constraint DF_invoice_carriage default 0.00 for carriage

alter table creditnote
add constraint DF_creditnote_carriage default 0.00 for carriage

alter table jobcosting
add constraint DF_jobcosting_carriage default 0.00 for carriage

alter table quotation
add constraint DF_quotation_carriage default 0.00 for carriage

alter table tpquotation
add constraint DF_tpquotation_carriage default 0.00 for carriage

alter table hire
add constraint DF_hire_carriage default 0.00 for carriage

alter table purchaseorder
add constraint DF_purchaseorder_carriage default 0.00 for carriage

insert into dbversion(version) values(789);

commit tran