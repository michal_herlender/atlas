USE [atlas]

BEGIN TRAN

delete from permission where permission = 'FINANCIAL_POSITION_CURRENT';
delete from permission where permission = 'SALES_BY_COMPANIES';
delete from permission where permission = 'SALES_TRANSACTIONS';
delete from permission where permission = 'SALES_FIGURES_MONTHLY';
GO

INSERT INTO dbversion(version) VALUES(763);

COMMIT TRAN
