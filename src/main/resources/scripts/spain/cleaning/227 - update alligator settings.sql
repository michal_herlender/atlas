/*
 * Script 227 - Changes design of alligator settings to allow production/test settings
 * and to configure settings by contact
 * GB 2018-02-13
 */

USE [spain]

BEGIN TRAN
	
	DROP TABLE [dbo].[alligatorsettingssubdiv]
	GO

	DROP TABLE [dbo].[alligatorsettings]
	GO	

    create table dbo.alligatorsettings (
        id int identity not null,
        description varchar(100) not null,
        excellocaltempfolder varchar(255) not null,
        excelrelativetempfolder bit not null,
        excelsavetempcopy bit not null,
        globaldefault bit not null,
        uploadenabled bit not null,
        uploadmoveuponupload bit not null,
        uploadrawdatafolder varchar(255) not null,
        uploadtempdatafolder varchar(255) not null,
        uploadusewindowscredentials bit not null,
        uploadwindowsdomain varchar(255) not null,
        uploadwindowspassword varchar(255) not null,
        uploadwindowsusername varchar(255) not null,
        primary key (id)
    );

    create table dbo.alligatorsettingscontact (
        id int identity not null,
        productionsettingsid int not null,
        testsettingsid int not null,
        personid int not null,
        primary key (id)
    );

    create table dbo.alligatorsettingssubdiv (
        id int identity not null,
        productionsettingsid int not null,
        testsettingsid int not null,
        subdivid int not null,
        primary key (id)
    );

    alter table dbo.alligatorsettingscontact 
        add constraint UKfrdi5m2bv0fevjgv0hh2wm7f1 unique (personid);

    alter table dbo.alligatorsettingssubdiv 
        add constraint UKjti1vdeag98hhwv3935mi1w8j unique (subdivid);

    alter table dbo.alligatorsettingscontact 
        add constraint FKbqqo4ofktfbqgccgqam1heifi 
        foreign key (productionsettingsid) 
        references dbo.alligatorsettings;

    alter table dbo.alligatorsettingscontact 
        add constraint FKjmwrso3q2opu8k92xynpr4ssw 
        foreign key (testsettingsid) 
        references dbo.alligatorsettings;

    alter table dbo.alligatorsettingscontact 
        add constraint FKgod2b5kdm76bl7qvgtiufee2x 
        foreign key (personid) 
        references dbo.contact;

    alter table dbo.alligatorsettingssubdiv 
        add constraint FKpgj4vlvv0glpvcu831hc2yc14 
        foreign key (productionsettingsid) 
        references dbo.alligatorsettings;

    alter table dbo.alligatorsettingssubdiv 
        add constraint FKddgsm7ie1ngvitlybds8edrid 
        foreign key (testsettingsid) 
        references dbo.alligatorsettings;

    alter table dbo.alligatorsettingssubdiv 
        add constraint FKt309d8m0tnk0a8d581knx7hp9 
        foreign key (subdivid) 
        references dbo.subdiv;

	SET IDENTITY_INSERT [alligatorsettings] ON

	INSERT INTO dbo.alligatorsettings (
		id,
		description,
		excellocaltempfolder,
		excelrelativetempfolder,
		excelsavetempcopy,
		globaldefault,
		uploadenabled,
		uploadmoveuponupload,
		uploadrawdatafolder,
		uploadtempdatafolder,
		uploadusewindowscredentials,
		uploadwindowsdomain,
		uploadwindowspassword,
		uploadwindowsusername)
	VALUES (
		1,
		'Global Default',
		'C:\GroupERP\Certificates',
		0,
		0,
		1,
		0,
		0,
		'C:\GroupERP\RawDataFolder',
		'C:\GroupERP\TempDataFolder',
		0,
		'',
		'',
		''),
		(
		2,
		'ES-TEM-MAD Upload Users',
		'C:\GroupERP\Certificates',
		0,
		0,
		0,
		1,
		0,
		'C:\GroupERP\RawDataFolder',
		'C:\GroupERP\TempDataFolder',
		0,
		'',
		'',
		''),
		(
		3,
		'ES-TIC-BIO/VIT Production',
		'C:\GroupERP\Certificates',
		1,
		1,
		0,
		1,
		1,
		'\\TB-DC1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates',
		'\\TB-DC1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates_primarios',
		1,
		'VHufQ+u5i9jc8LZGJtdtpuz2P0gSGuLXxO1Q87dUQac=',
		'BZN7ToPnQLbr/W8HD/YCA/TSYNtLwGEC',
		'LaxtETvj52h+LVI5qiMrRs8ztI9IF/Qu'),
		(
		4,
		'ES-TIC-BIO/VIT Test',
		'C:\GroupERP\Certificates',
		1,
		0,
		0,
		1,
		0,
		'\\TB-DC1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates',
		'\\TB-DC1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates_primarios',
		1,
		'VHufQ+u5i9jc8LZGJtdtpuz2P0gSGuLXxO1Q87dUQac=',
		'BZN7ToPnQLbr/W8HD/YCA/TSYNtLwGEC',
		'LaxtETvj52h+LVI5qiMrRs8ztI9IF/Qu');

	SET IDENTITY_INSERT [alligatorsettings] OFF

-- Contact: TEM-MAD- personid 17290 (Miguel Angel) uses settings id 2 (Madrid - upload)
-- Contact : Netherlands - Zoetermeer - Gerard - 17287 uses settings ID 2 (Madrid - upload)
-- Other Madrid users : Marta Maquedano De Frutos, Iv�n S�nchez Mart�nez, Raul Arias Pascual, Miguel Remartinez

-- Subdivs: TIC-BIL, TIC-VIT - 6642, 6643 uses settings id 3 for production, settings id 4 for test

	SET IDENTITY_INSERT [alligatorsettingssubdiv] ON

    INSERT INTO [alligatorsettingssubdiv] 
	(id, subdivid, productionsettingsid, testsettingsid) VALUES
	(1, 6643, 3, 4),
	(2, 6642, 3, 4);

	SET IDENTITY_INSERT [alligatorsettingssubdiv] OFF

	SET IDENTITY_INSERT [alligatorsettingscontact] ON

    INSERT INTO [alligatorsettingscontact] 
	(id, personid, productionsettingsid, testsettingsid) VALUES
	(1, 17290, 2, 2),
	(2, 17287, 2, 2),
	(3, 17545, 2, 2),
	(4, 17550, 2, 2),
	(5, 17371, 2, 2),
	(6, 17365, 2, 2);

	SET IDENTITY_INSERT [alligatorsettingscontact] OFF

	GO


INSERT INTO dbversion VALUES (227);

COMMIT TRAN