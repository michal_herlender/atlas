USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE invoiceitem ADD shiptoaddressid INT, FOREIGN KEY(shiptoaddressid) REFERENCES address(addrid);
	ALTER TABLE creditnoteitem ADD shiptoaddressid INT, FOREIGN KEY(shiptoaddressid) REFERENCES address(addrid);
    
	GO

	-- 1. Set ship-to address by latest client delivery address for invoice items linked to a job item
	UPDATE invoiceitem SET shiptoaddressid = d.addressid
		FROM invoiceitem ii
		INNER JOIN deliveryitem di ON ii.jobitemid = di.jobitemid
			AND di.deliveryitemid = (
				SELECT MAX(jdi.deliveryitemid)
				FROM deliveryitem jdi
				WHERE jdi.deliveryitemtype = 'job'
					AND jdi.deliveryid IN (
						SELECT cd.deliveryid
						FROM delivery cd
						WHERE cd.deliverytype = 'client'
					)
					AND jdi.jobitemid = ii.jobitemid
			)
		LEFT JOIN delivery d ON di.deliveryid = d.deliveryid

	-- 2. Set ship-to address by job address for invoice items linked to a job item, but without client delivery
	UPDATE invoiceitem set shiptoaddressid = j.returnto
		FROM invoiceitem ii
		INNER JOIN jobitem ji ON ii.jobitemid = ji.jobitemid
		INNER JOIN job j ON ji.jobid = j.jobid
		WHERE ii.shiptoaddressid IS NULL
	
	-- 3. Set ship-to address by job address for invoice items linked to a job service
	UPDATE invoiceitem set shiptoaddressid = j.returnto
		FROM invoiceitem ii
		INNER JOIN jobexpenseitem jei ON ii.expenseitem = jei.id
		INNER JOIN job j ON jei.job = j.jobid

	-- 4. Set shipt-to address by invoice address, if there is no job item or job service linked
	UPDATE invoiceitem set shiptoaddressid = i.addressid
		FROM invoiceitem ii
		INNER JOIN invoice i ON ii.invoiceid = i.id
		WHERE ii.shiptoaddressid IS NULL

	GO

	ALTER TABLE invoiceitem ALTER COLUMN shiptoaddressid INT NOT NULL

	INSERT INTO dbversion(version) VALUES(817);

COMMIT TRAN