USE [atlas]

BEGIN TRAN

    DECLARE @engineer_inspection_activity INT,
        @engineer_inspection_status INT,
		@new_hook INT;

	SELECT @engineer_inspection_activity = stateid FROM dbo.itemstate WHERE [description] = 'Unit inspected by engineer (post third party work)';
	SELECT @engineer_inspection_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting inspection by engineer (post third party work)';

	INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
		VALUES (1,0,1,1,'auto-engineer-inspection',@engineer_inspection_activity);

	SELECT @new_hook=id FROM dbo.hook WHERE name = 'auto-engineer-inspection';

	INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@engineer_inspection_activity,@new_hook,@engineer_inspection_status);

	INSERT INTO dbversion(version) VALUES(663);

COMMIT TRAN