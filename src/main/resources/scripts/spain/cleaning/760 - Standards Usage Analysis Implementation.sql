USE [atlas]
GO

BEGIN TRAN
	
	-- Create the table "reversetraceabilitysettings"
	CREATE TABLE dbo.reversetraceabilitysettings (
		id INT IDENTITY NOT NULL PRIMARY KEY,
		reversetraceability BIT DEFAULT 0 NOT NULL,
		subdivid INT NOT NULL,
		startdate DATE NOT NULL
	);
	
	-- Create the table "standardsusageanalysis"
	CREATE TABLE dbo.standardsusageanalysis (
		id INT IDENTITY NOT NULL PRIMARY KEY,
		startdate DATE,
		finishdate DATE NOT NULL,
		instid INT NOT NULL,
		statusid INT NOT NULL,
		CONSTRAINT K_standardsusageanalysis_instrument FOREIGN KEY (instid) REFERENCES dbo.instrument(plantid)
	);
	
	-- Add permission for Standards Usage Analysis Search
	DECLARE @groupIDUnreleaedFeatures INT;
	SELECT @groupIDUnreleaedFeatures = [id] FROM [dbo].[permissiongroup] WHERE name = 'GROUP_UNRELEASED_FEATURES';
	INSERT INTO dbo.permission(groupId, permission)
	VALUES (@groupIDUnreleaedFeatures ,'STANDARDS_USAGE_ANALYSIS_SEARCH');
	
	-- Add system component 
	INSERT INTO dbo.systemcomponent (appendtoparentpath, component, componentname, foldersdropboundaries, foldersperitem,folderssplit, 
	folderssplitrange, foldersyear, numberedsubfolders, rootdir, usingparentsettings, parentid, emailtemplates)
	VALUES ('Standards Usage Analysis', 29, 'Standards Usage Analysis', 0, 0, 0, 0, 0, 0, 'Standards Usage Analysis', 1, null, 0);

	INSERT INTO dbversion(version) VALUES(760);

	GO

COMMIT TRAN