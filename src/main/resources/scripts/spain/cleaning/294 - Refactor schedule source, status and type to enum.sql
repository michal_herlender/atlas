USE [spain];

BEGIN TRAN

-- Note, the following constraints / tables don't seem to exist on spain production
-- Commenting out so script would run 2018-10-09 GB
--ALTER TABLE schedule DROP CONSTRAINT FKD66692978F2734CB;
--ALTER TABLE schedule DROP CONSTRAINT FKD666929775D07A02;
--ALTER TABLE schedule DROP CONSTRAINT FKD666929775A934A;
--ALTER TABLE repeatschedule DROP CONSTRAINT FK5FA4F53275A934A;
--DROP TABLE schedulesource;
--DROP TABLE schedulestatus;
--DROP TABLE scheduletype;

UPDATE schedule SET schedulesourceid = schedulesourceid - 1
UPDATE schedule SET schedulestatusid = schedulestatusid - 1
UPDATE schedule SET scheduletypeid = scheduletypeid - 1

INSERT INTO dbversion(version) values(294);

COMMIT TRAN