USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.validatedfreerepaircomponent ADD lastModifiedBy int NOT NULL;
ALTER TABLE dbo.validatedfreerepaircomponent ADD lastModified datetime2 NOT NULL;
ALTER TABLE dbo.validatedfreerepaircomponent ADD CONSTRAINT validatedfreerepaircomponent_contact_FK 
FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid);

ALTER TABLE dbo.validatedfreerepairoperation ADD lastModifiedBy int NOT NULL;
ALTER TABLE dbo.validatedfreerepairoperation ADD lastModified datetime2 NOT NULL;
ALTER TABLE dbo.validatedfreerepairoperation ADD CONSTRAINT validatedfreerepairoperation_contact_FK
FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid);

EXEC sp_RENAME 'dbo.repairinspectionreport.manufacturerwaranty' , 'manufacturerwarranty', 'COLUMN';

INSERT INTO dbversion (version) VALUES (434);

COMMIT TRAN