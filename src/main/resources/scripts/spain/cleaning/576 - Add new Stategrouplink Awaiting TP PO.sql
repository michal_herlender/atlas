USE [atlas]
GO

BEGIN TRAN

DECLARE @activity INT;

SELECT @activity = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting PO for third party work';

INSERT INTO atlas.dbo.stategrouplink
(groupid, stateid, [type])
VALUES(63, @activity, 'ALLOCATED_SUBDIV');

INSERT INTO dbversion(version) VALUES (576)

COMMIT TRAN