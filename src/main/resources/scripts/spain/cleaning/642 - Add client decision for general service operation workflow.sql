USE [atlas]

BEGIN TRAN

DECLARE @gso_doc_status INT,
			@gso_client_decision_workstatus INT,
			@gso_client_decision_workactivity INT,
			@new_lookup INT,
			@os1 INT,
			@lookup_gso_result_after_doc INT,
			@os2 INT,
			@lookup_gso_has_doc INT,
			@os_yes_gso_has_doc INT,
			@lastInsertedAO_accepted INT,
			@lastInsertedAO_rejected INT;

	SELECT @gso_doc_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting document upload for general operation';
	INSERT INTO dbo.stategrouplink(groupid,stateid,type) VALUES(68,@gso_doc_status,'ALLOCATED_SUBDIV');
	
	--create new table for general service operation document link with jobitem
	CREATE TABLE dbo.gsodocumentlink (
    id INT NOT NULL IDENTITY(1,1),
    gsodocumentid INT,
    jobitemid INT,
	lastModifiedBy INT,
    lastModified DATETIME2,
    PRIMARY KEY (id),
	CONSTRAINT gsodocuemntlink_gsodocumentid_gsodocument_FK FOREIGN KEY (gsodocumentid) REFERENCES dbo.gsodocument(id),
	CONSTRAINT gsodocuemntlink_jobitemid_jobitem_FK FOREIGN KEY (jobitemid) REFERENCES dbo.jobitem(jobitemid),
	CONSTRAINT gsodocuemntlink_lastModifiedBy_contact_FK FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid),
	);

	--update the new table with data from other general service operation tables
	INSERT INTO dbo.gsodocumentlink(gsodocumentid,jobitemid,lastModifiedBy,lastModified)
	SELECT gd.id,gl.jobitemid,gd.lastModifiedBy,gd.lastModified FROM dbo.generalserviceoperation gso join
	dbo.gsodocument gd on gso.id = gd.gsoid join
	dbo.gsolink gl on gso.id = gl.gsoid join
	dbo.actionoutcome ao on gl.actionoutcomeid = ao.id
	WHERE gl.actionoutcomeid IS NOT NULL AND ao.positiveoutcome = 1

	--add new status awaiting client decision for general service operation
	INSERT INTO dbo.itemstate([type],[description],active,retired,lookupid,actionoutcometype)
	VALUES ('workstatus','Awaiting client decision for general service operation',1,0,NULL,NULL)
	SET @gso_client_decision_workstatus = IDENT_CURRENT('itemstate');
	INSERT INTO atlas.dbo.itemstatetranslation(stateid, locale, [translation])
	VALUES(@gso_client_decision_workstatus, 'de_DE', 'Warten auf Kundenentscheidung für den allgemeinen Servicebetrieb'),
		(@gso_client_decision_workstatus, 'en_GB', 'Awaiting client decision for general service operation'),
		(@gso_client_decision_workstatus, 'es_ES', 'En espera de la decisión del cliente para la operación de servicios generales'),
		(@gso_client_decision_workstatus, 'fr_FR', 'En attente de la décision du client pour l''exploitation des services généraux');
	
	INSERT INTO dbo.itemstate([type],[description],active,retired,lookupid,actionoutcometype)
	VALUES ('workactivity','Client decision received for general service operation',1,0,NULL,'CLIENT_DECISION_GENERAL_SERVICE_OPERATION')
	SET @gso_client_decision_workactivity = IDENT_CURRENT('itemstate');
	INSERT INTO atlas.dbo.itemstatetranslation(stateid, locale, [translation])
	VALUES(@gso_client_decision_workactivity, 'de_DE', 'Kundenentscheidung für den allgemeinen Servicebetrieb erhalten'),
		(@gso_client_decision_workactivity, 'en_GB', 'Client decision received for general service operation'),
		(@gso_client_decision_workactivity, 'es_ES', 'Decisión del cliente recibida para la operación de servicio general'),
		(@gso_client_decision_workactivity, 'fr_FR', 'Décision du client reçue pour l''exploitation des services généraux');
	
	-- add nextactivity
	INSERT INTO dbo.nextactivity(manualentryallowed,statusid,activityid)
	VALUES (1,@gso_client_decision_workstatus,@gso_client_decision_workactivity);

	--add action outcomes
	INSERT INTO dbo.actionoutcome(defaultoutcome, description, positiveoutcome, activityid, active, [type], value)
	VALUES(1, 'General service operation - accepted', 1, @gso_client_decision_workactivity, 1, 'client_decision_gso', 'ACCEPTED');
    SET @lastInsertedAO_accepted = IDENT_CURRENT('actionoutcome');
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) 
	VALUES (@lastInsertedAO_accepted, 'en_GB', 'General service operation - accepted'),
	   (@lastInsertedAO_accepted, 'fr_FR', 'Fonctionnement des services généraux - accepté'),
	   (@lastInsertedAO_accepted, 'es_ES', 'Operación de servicio general - aceptado'),
	   (@lastInsertedAO_accepted, 'de_DE', 'Allgemeiner Servicebetrieb - akzeptiert');

	INSERT INTO dbo.actionoutcome(defaultoutcome, description, positiveoutcome, activityid, active, [type], value)
	VALUES(0, 'General service operation - rejected', 0, @gso_client_decision_workactivity, 1, 'client_decision_gso', 'REJECTED');
    SET @lastInsertedAO_rejected = IDENT_CURRENT('actionoutcome');
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) 
	VALUES (@lastInsertedAO_rejected, 'en_GB', 'General service operation - rejected'),
	   (@lastInsertedAO_rejected, 'fr_FR', 'Fonctionnement des services généraux - rejeté'),
	   (@lastInsertedAO_rejected, 'es_ES', 'Operación de servicios generales - rechazada'),
	   (@lastInsertedAO_rejected, 'de_DE', 'Allgemeiner Servicebetrieb - abgelehnt');

	INSERT INTO dbo.lookupinstance(lookup,lookupfunction)
	VALUES ('Lookup_ClientDecisionRequiredForGeneralServiceOperation',60);
	SELECT @new_lookup = MAX(id) FROM dbo.lookupinstance;

	-- add outcomestatues
	INSERT INTO dbo.outcomestatus (defaultoutcome,statusid) VALUES (1,@gso_client_decision_workstatus);
	select @os1 = max(id) from dbo.outcomestatus;
	SELECT @lookup_gso_result_after_doc = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_GeneralServiceOperationResult (after document upload)';
	INSERT INTO dbo.outcomestatus (defaultoutcome,lookupid)	VALUES (0,@lookup_gso_result_after_doc);
	select @os2 = max(id) from dbo.outcomestatus;
	
	-- add lookupresult
	INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@new_lookup,@os1,0);
	INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@new_lookup,@os2,1);
	
	--update lookup for gso client decision activity
	UPDATE dbo.itemstate SET lookupid = @lookup_gso_result_after_doc FROM dbo.itemstate WHERE stateid = @gso_client_decision_workactivity;
	
	SELECT @lookup_gso_has_doc = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_HasDocumentForMostRecentGeneralOperation';
	SELECT @os_yes_gso_has_doc = outcomestatusid FROM dbo.lookupresult WHERE lookupid = @lookup_gso_has_doc AND resultmessage = 0;
	UPDATE dbo.outcomestatus SET lookupid = @new_lookup WHERE id = @os_yes_gso_has_doc;
	UPDATE dbo.itemstate SET lookupid = @new_lookup WHERE [description] IN ('Document uploaded for general operation','Document not required for general operation');
	
	ALTER TABLE dbo.generalserviceoperation
	ALTER COLUMN startedon DATETIME2;
	ALTER TABLE dbo.generalserviceoperation
	ALTER COLUMN completedon DATETIME2;
	ALTER TABLE dbo.gsodocument
	ALTER COLUMN documentdate DATETIME2;
	ALTER TABLE dbo.generalserviceoperation
	ADD clientdecisionneeded BIT;
	
	INSERT INTO dbversion(version) VALUES (642)

COMMIT TRAN