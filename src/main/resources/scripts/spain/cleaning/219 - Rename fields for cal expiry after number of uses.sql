BEGIN TRAN;

USE spain;
GO
EXEC sp_rename 'instrument.calsintervalon', 'calexpiresafternumberofuses', 'COLUMN';
GO
EXEC sp_rename 'instrument.calssincelastcal', 'usessincelastcalibration', 'COLUMN';
GO
EXEC sp_rename 'instrument.calsinterval', 'permitednumberofuses', 'COLUMN';
GO

INSERT INTO dbversion VALUES (219);

COMMIT TRAN;