-- Author Tony Provost 2016-06-06
--
-- Fix bug #428: When importing employee Contact data into the ERP database, please ensure that the userinstructiontype table gets populated as well. 
--

BEGIN TRAN

USE [spain-reference];
GO

-- Insert record in table dbo.userpreferences
INSERT INTO dbo.userpreferences (jikeynavigation, mouseactive, mouseoverdelay, navbarposition, scheme, includeselfinreplies, contactid)
	SELECT 1, 1, 0, 0, 'Default', 0, personid
	FROM dbo.contact
		INNER JOIN dbo.subdiv ON subdiv.subdivid = contact.subdivid
		INNER JOIN dbo.company ON company.coid = subdiv.coid AND company.corole = 5
	WHERE contact.firstname != 'CWMS Auto User' 
		AND NOT EXISTS (
			SELECT contactid
			FROM dbo.userpreferences
			WHERE userpreferences.contactid = contact.personid)

-- Insert record in tabel dbo.userinstrcutiontype
INSERT INTO dbo.userinstructiontype (instructiontypeid, userprefid)
	SELECT instructiontype.instructiontypeid, userpreferences.id
	FROM dbo.instructiontype, dbo.contact
		INNER JOIN dbo.subdiv ON subdiv.subdivid = contact.subdivid
		INNER JOIN dbo.company ON company.coid = subdiv.coid AND company.corole = 5
		INNER JOIN dbo.userpreferences ON userpreferences.contactid = contact.personid
	WHERE contact.firstname != 'CWMS Auto User' 
		AND NOT EXISTS ( 
			SELECT *
			FROM dbo.userinstructiontype tmp
				INNER JOIN dbo.userpreferences tmp2 ON tmp2.id = tmp.userprefid 
			WHERE tmp.instructiontypeid = instructiontype.instructiontypeid
				AND tmp2.contactid = contact.personid 
			)
	ORDER BY contact.personid, instructiontype.instructiontypeid

ROLLBACK TRAN
