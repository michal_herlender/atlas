USE [spain];

BEGIN TRAN

ALTER TABLE dbo.contractreview ADD demandset INT;

--DROP TABLE 'servicetype_contract'
DROP TABLE [dbo].[servicetype_contract]

CREATE TABLE dbo.contractservicetypesettings (
       id INT IDENTITY NOT NULL,
        turnaround INT,
        contractid INT NOT NULL,
        servicetypeid INT NOT NULL,
        PRIMARY KEY (id)
    );

ALTER TABLE dbo.contractservicetypesettings
	ADD CONSTRAINT FK_contractservicetypesettings_contract
	FOREIGN KEY (contractid)
	REFERENCES dbo.contract;

ALTER TABLE dbo.contractservicetypesettings
	ADD CONSTRAINT FK_contractservicetypesettings_servicetype
	FOREIGN KEY (servicetypeid)
	REFERENCES dbo.servicetype;

ALTER TABLE dbo.contractservicetypesettings
	ADD CONSTRAINT UK_contractservicetypesettings_contract_servicetype UNIQUE (contractid, servicetypeid);
      
GO

INSERT INTO dbversion (version) VALUES (437);

COMMIT TRAN