USE [atlas]

BEGIN TRAN


	IF EXISTS(select sd.defaultid
		from systemdefault sd
		where defaultname = 'Approve delivery note creation by CSR')
	BEGIN

		declare @sdId int;
		
		-- get the id
		select @sdId = sd.defaultid
			from systemdefault sd
			where defaultname = 'Approve delivery note creation by CSR';

		---- delete systemdefault
		delete from systemdefaultdescriptiontranslation where defaultid = @sdId;

		delete from systemdefaultnametranslation where defaultid = @sdId;

		delete from systemdefaultcorole where defaultid = @sdId;

		delete from systemdefaultscope where defaultid = @sdId;

		delete from systemdefault where defaultid = @sdId;

		---- re-insert
		SET IDENTITY_INSERT [systemdefault] ON;
		INSERT INTO systemdefault (defaultid,defaultdatatype,defaultdescription,defaultname,defaultvalue,endd,[start],groupid,groupwide)
		VALUES (63, 'boolean','Defines whether to add a step for approving the creation of the delivery note','Approve delivery note creation by CSR','false',0,0,1,0);
		SET IDENTITY_INSERT [systemdefault] OFF;

		select @sdId = max(sd.defaultid)  from systemdefault sd;
	
		-- name translations	
		INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
			VALUES (@sdId,'de_DE','Genehmigen Sie die Erstellung von Lieferscheinen durch CSR');
		INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
			VALUES (@sdId,'en_GB','Approve delivery note creation by CSR');
		INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
			VALUES (@sdId,'es_ES','Aprobar la creación de albarán por CSR');
		INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
			VALUES (@sdId,'fr_FR','Approuver la création du bon de livraison par le CRC');
		
		-- description translation
		INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
			VALUES (@sdId,'de_DE','Definiert, ob ein Schritt zum Genehmigen der Erstellung des Lieferscheins hinzugefügt werden soll');
		INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
			VALUES (@sdId,'en_GB','Defines whether to add a step for approving the creation of the delivery note');
		INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
			VALUES (@sdId,'es_ES','Define si agregar un paso para aprobar la creación del albarán');
		INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
			VALUES (@sdId,'fr_FR','Définit s''il faut ajouter une étape d''approbation pour la création du bon de livraison');
		
		-- role
		INSERT INTO systemdefaultcorole (coroleid,defaultid)
			VALUES (5,@sdId);
		
		-- scope
		INSERT INTO systemdefaultscope ([scope],defaultid)
			VALUES (0,@sdId);
		INSERT INTO systemdefaultscope ([scope],defaultid)
			VALUES (2,@sdId);
				
	END
	

INSERT INTO dbversion(version) VALUES(688);

COMMIT TRAN