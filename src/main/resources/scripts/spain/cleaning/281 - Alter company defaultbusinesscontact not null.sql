-- Script 281
-- Makes default business contact not null, after altering some of the Trescal companies that have null default contacts
-- The Trescal companies with null contacts are initialized to their own default contact for their default subdivision.

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[company]
   SET [defaultbusinesscontact] = (SELECT subdiv.defpersonid FROM [dbo].subdiv WHERE subdiv.subdivid = company.defsubdivid)
 WHERE defaultbusinesscontact is null and corole = 5;
GO

ALTER TABLE [dbo].[company] ALTER COLUMN [defaultbusinesscontact] int not null; 

COMMIT TRAN