USE [atlas]
GO

BEGIN TRAN

DROP STATISTICS jobitemaction._dta_stat_2128726636_18_27_26_19_14_17_13_22_24_16_15_23_25;

ALTER TABLE jobitemaction DROP CONSTRAINT FKqbsg0nkq6la0nol3947rbt098;

ALTER TABLE jobitemaction DROP CONSTRAINT FKro2wre44gnfjbain6et2q0xqc;

DROP INDEX _dta_index_jobitemaction_7_2128726636__K17_K2_K16_K18_K19_1_3_4_5_6_7_8_9_10_11_12_13_14_15_20_21_22_23_24_25_26_27 on jobitemaction;
drop index IDX_jobitemaction_singletransitid on jobitemaction ;
DROP INDEX IDX_jobitemaction_businesstransitid on jobitemaction;
alter table jobitemaction drop column singletransitid;
alter table jobitemaction drop column businesstransitid;
drop table businesstransit;
drop table singletransit;

insert into dbversion(version) values(773);

 COMMIT TRAN
