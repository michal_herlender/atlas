USE spain;
GO

BEGIN TRAN

DELETE basestatusnametranslation
FROM basestatusnametranslation
LEFT JOIN basestatus ON basestatusnametranslation.statusid = basestatus.statusid
WHERE basestatus.type = 'instrument'

DELETE basestatusdescriptiontranslation
FROM basestatusdescriptiontranslation
LEFT JOIN basestatus ON basestatusdescriptiontranslation.statusid = basestatus.statusid
WHERE basestatus.type = 'instrument'

ALTER TABLE instrument DROP CONSTRAINT FK532D63E7B45F2ADB
ALTER TABLE instrumenthistory DROP CONSTRAINT FK37FC9ECDFB6D347B
ALTER TABLE instrumenthistory DROP CONSTRAINT FK37FC9ECD3D3FC202

UPDATE instrument SET statusid = statusid - 20 WHERE statusid >= 20;
UPDATE instrumenthistory SET newstatusid = newstatusid - 20 WHERE newstatusid >= 20;
UPDATE instrumenthistory SET oldstatusid = oldstatusid - 20 WHERE oldstatusid >= 20;

DELETE FROM basestatus WHERE type = 'instrument';

COMMIT TRAN