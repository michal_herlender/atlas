-- Strangely, there was no state group link for the "called off item" state and it was causing items to be callable off twice.
-- This inserts the state group link, and also inactivates any active call off items where the job items are 

USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (47,1020,null)
GO

INSERT INTO dbversion(version) VALUES(461)
GO

COMMIT TRAN