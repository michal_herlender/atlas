-- Script 869
-- Configure additional permission group and role for pricing management
-- At the start this would include the "assign sales category for instrument model" role
-- Based on feedback from Didier about DEV-2930

BEGIN TRAN

-- Remove existing permission which was added to instrument model management

DELETE FROM [dbo].[permission] WHERE permission = 'INSTRUMENT_MODEL_SALES_CATEGORY';

INSERT INTO [dbo].[perrole]
           ([name])
     VALUES
           ('ROLE_PRICING_MANAGEMENT')

INSERT INTO [dbo].[permissiongroup]
           ([name])
     VALUES
           ('GROUP_PRICING_MANAGEMENT')
GO

DECLARE @role_id_pricing int;
DECLARE @role_id_admin int;
DECLARE @group_id_pricing int;

SELECT @role_id_pricing = id from perrole WHERE perrole.name = 'ROLE_PRICING_MANAGEMENT'
SELECT @role_id_admin = id from perrole WHERE perrole.name = 'ROLE_ADMINISTRATOR'
SELECT @group_id_pricing = id from permissiongroup WHERE permissiongroup.name = 'GROUP_PRICING_MANAGEMENT'

INSERT INTO [dbo].[permission]
           ([groupId]
           ,[permission])
     VALUES
           (@group_id_pricing,'INSTRUMENT_MODEL_SALES_CATEGORY');

INSERT INTO [dbo].[rolegroup]
           ([roleId]
           ,[groupId])
     VALUES
           (@role_id_pricing
           ,@group_id_pricing)

-- Insert the GROUP_PRICING_MANAGEMENT into the ROLE_ADMINISTRATOR role
-- This makes the feature available to administrators for testing

INSERT INTO [dbo].[rolegroup]
           ([roleId]
           ,[groupId])
     VALUES
           (@role_id_admin
           ,@group_id_pricing)

INSERT INTO dbversion(version) VALUES(869);

COMMIT TRAN