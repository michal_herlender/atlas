USE spain

BEGIN TRAN

INSERT INTO lookupinstance (lookup, lookupfunction) VALUES ('Lookup_PreDeliveryRequirements2', 39)

DECLARE @Lookup int, @State_DN int, @State_DP int, @State_IT int, @Outcome_DN int, @Outcome_DP int, @Outcome_IT int,
@Activity_DN int, @Activity_DP int, @Activity_RC int

SELECT @Lookup = (SELECT id FROM lookupinstance WHERE lookup='Lookup_PreDeliveryRequirements2')

INSERT INTO itemstate (type, active, description, retired, lookupid) VALUES
('workstatus', 1, 'Awaiting internal return delivery note', 0, NULL),
('workstatus', 1, 'Awaiting despatch to returning business subdivision', 0, NULL),
('workstatus', 1, 'In transit to returning business subdivision', 0, NULL),
('workactivity', 1, 'Added to internal return delivery note', 0, NULL),
('workactivity', 1, 'Despatched to returning business subdivision', 0, NULL),
('workactivity', 1, 'Received at returning business subdivision', 0, @Lookup)

INSERT INTO itemstatetranslation (locale, stateid, translation)
SELECT 'en_GB', itemstate.stateid, description
FROM itemstate
LEFT JOIN itemstatetranslation ON itemstate.stateid = itemstatetranslation.stateid
WHERE translation IS NULL

SELECT @State_DN = (SELECT stateid FROM itemstate WHERE description='Awaiting internal return delivery note')
SELECT @State_DP = (SELECT stateid FROM itemstate WHERE description='Awaiting despatch to returning business subdivision')
SELECT @State_IT = (SELECT stateid FROM itemstate WHERE description='In transit to returning business subdivision')
SELECT @Activity_DN = (SELECT stateid FROM itemstate WHERE description='Added to internal return delivery note')
SELECT @Activity_DP = (SELECT stateid FROM itemstate WHERE description='Despatched to returning business subdivision')
SELECT @Activity_RC = (SELECT stateid FROM itemstate WHERE description='Received at returning business subdivision')

INSERT INTO outcomestatus (defaultoutcome, lookupid, statusid) VALUES
(0, @Lookup, NULL),
(0, NULL, @State_DN),
(0, NULL, @State_DP),
(0, NULL, @State_IT);

SELECT @Outcome_DN = (
	SELECT outcomestatus.id
	FROM outcomestatus
	WHERE statusid = @State_DN AND activityid IS NULL)
SELECT @Outcome_DP = (
	SELECT outcomestatus.id
	FROM outcomestatus
	WHERE statusid = @State_DP AND activityid IS NULL)
SELECT @Outcome_IT = (
	SELECT outcomestatus.id
	FROM outcomestatus
	WHERE statusid = @State_IT AND activityid IS NULL)

UPDATE lookupresult SET outcomestatusid = outcomestatus.id
FROM outcomestatus
WHERE lookupresult.lookupid = 67 AND lookupresult.resultmessage = 1 AND outcomestatus.lookupid = @Lookup

INSERT INTO lookupresult (lookupid, outcomestatusid, resultmessage)
SELECT @Lookup, 200, 0

INSERT INTO lookupresult (lookupid, outcomestatusid, resultmessage)
VALUES (@Lookup, @Outcome_DN, 1), (@Lookup, @Outcome_DN, 2)

INSERT INTO hookactivity (beginactivity, completeactivity, activityid, hookid, stateid)
SELECT 1, 1, activity.stateid, 10, state.stateid
FROM itemstate AS activity, itemstate AS state
WHERE activity.stateid = @Activity_DN AND state.stateid = @State_DN

INSERT INTO hookactivity (beginactivity, completeactivity, activityid, hookid, stateid)
SELECT 1, 1, activity.stateid, 11, state.stateid
FROM itemstate AS activity, itemstate AS state
WHERE activity.stateid = @Activity_DP AND state.stateid = @State_DP

INSERT INTO hookactivity (beginactivity, completeactivity, activityid, hookid, stateid)
SELECT 1, 1, activity.stateid, 12, state.stateid
FROM itemstate AS activity, itemstate AS state
WHERE activity.stateid = @Activity_RC AND state.stateid = @State_IT

INSERT INTO outcomestatus (defaultoutcome, activityid, statusid, lookupid) VALUES
(1, @Activity_DN, @State_DP, NULL),
(1, @Activity_DP, @State_IT, NULL),
(1, @Activity_RC, NULL, @Lookup)

INSERT INTO stategrouplink (stateid, groupid) VALUES
(@State_DN, 43),
(@State_DP, 4),
(@State_IT, 5),
(@State_DP, 11),
(@State_DN, 33),
(@State_DP, 34),
(@State_IT, 35)

COMMIT TRAN