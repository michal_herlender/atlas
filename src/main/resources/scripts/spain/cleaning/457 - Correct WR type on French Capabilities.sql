USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[procs]
   SET [reqtype] = 'REPAIR'
 WHERE procs.reference like '%REP%';
GO

INSERT INTO dbversion(version) VALUES(457)
GO

COMMIT TRAN