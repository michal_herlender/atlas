

USE [spain]

BEGIN TRAN

update dbo.permission set permission='ASSET_ADD_EDIT' where permission='ASSET_ADD_EDDIT'

update dbo.permission set permission='COMPANY_FLEXIBLE_FIELD_ADD_EDIT' where permission='COMPANY_FLEXIBLE_FIELD_ADD_EDDIT'

update dbo.permission set permission='ACCREDITATION_CONTACT_PROCEDURE_EDIT_ADMIN' where permission='CONTACT_PROCEDURE_ACCREDITATION_EDIT_ADMIN'

delete from dbo.permission where permission='COMPANY_RECALL_EDIT'
	
INSERT INTO dbversion(version) VALUES(384);

COMMIT TRAN