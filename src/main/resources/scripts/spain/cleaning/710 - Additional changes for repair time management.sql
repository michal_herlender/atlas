USE [atlas]

BEGIN TRAN

    DECLARE @repairtime_progactivity INT;

	SELECT @repairtime_progactivity = stateid FROM dbo.itemstate WHERE [description] = 'Production repair time recorded';
	
	INSERT INTO dbo.stategrouplink(groupid,stateid,[type])
	VALUES (75,@repairtime_progactivity,'CURRENT_SUBDIV');
	
	EXEC sp_rename 'dbo.repairinspectionreport.validatedbytechnician', 'completedbytechnician', 'COLUMN';
	GO
	EXEC sp_rename 'repairinspectionreport.validatedbytechnicianon', 'completedbytechnicianon', 'COLUMN';
	GO

	INSERT INTO dbversion VALUES (710)

COMMIT TRAN