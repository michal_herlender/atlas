USE [atlas];

GO

BEGIN TRAN

DECLARE @lookup_predeliveryrequirements INT,
        @activity_noactionrequired INT,
		@new_hook INT,
		@status_awaiting_cal INT;

SELECT @lookup_predeliveryrequirements = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_PreDeliveryRequirements (after lab calibration - after costing check)';

SELECT @activity_noactionrequired = stateid FROM dbo.itemstate WHERE [description] = 'No action required';

SELECT @status_awaiting_cal = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting calibration';

-- connect activity "No action required" to lookup "Lookup_PreDeliveryRequirements (after lab calibration - after costing check)"
UPDATE dbo.itemstate SET lookupid = @lookup_predeliveryrequirements WHERE stateid = @activity_noactionrequired;

-- disable manual entry to the activity "No action required"
UPDATE dbo.nextactivity SET manualentryallowed = 0 WHERE activityid = @activity_noactionrequired;

-- create "no-action-required-for-calibration" hook
INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
    VALUES (1,0,1,1,'no-action-required-for-calibration',@activity_noactionrequired);
SELECT @new_hook = MAX(id) FROM dbo.hook;

-- add hook activity
INSERT INTO dbo.hookactivity
(beginactivity, completeactivity, activityid, hookid, stateid)
VALUES(1, 1, @activity_noactionrequired, @new_hook, @status_awaiting_cal);

INSERT INTO dbversion(version) VALUES(565);

COMMIT TRAN