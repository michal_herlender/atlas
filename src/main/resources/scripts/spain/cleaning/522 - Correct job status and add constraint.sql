-- Corrects data from previous repair bug (DEV-1351) that would cause repair jobs to be lost from job searches and the dashboard.
-- 64 jobs were already updated on production today, this just fixes the same issue locally and on any test servers.
-- Added a JPA constraint on the field, but not a NOT NULL on column, as there indexes/stats that may or may not be relevant...

USE [atlas]
GO

BEGIN TRAN

UPDATE [dbo].[job]
 SET [statusid] = 1
 WHERE statusid is NULL
GO

INSERT INTO dbversion(version) VALUES(522);
GO

COMMIT TRAN