BEGIN TRAN;

-- Create service type with accredited colors
insert into dbo.servicetype ([displaycolour],[displaycolourfasttrack],[longname],[shortname]) 
VALUES ('#FFFFCC','#FFB9B9','Inter-Company Accredited Calibration','AC-IC');



-- Create cal type for new service type with values from in house accredited service type
insert into calibrationtype
			(accreditationspecific,
			active,
			certnoresetyearly,
			firstpagepapertype,
			labeltemplatepath,
			orderby,
			pdfcontinuationwatermark,
			pdfheaderwatermark,
			recalldateonlabel,
			restpagepapertype,
			servicetypeid)
select accreditationspecific,
	   1,
	   certnoresetyearly,
	   firstpagepapertype,
	   labeltemplatepath,
	   (select servicetypeid from servicetype s where s.shortname = 'AC-IC'),
	   pdfcontinuationwatermark,
	   pdfheaderwatermark,
	   recalldateonlabel,
	   restpagepapertype,
	   (select servicetypeid from servicetype s where s.shortname = 'AC-IC') as 'servicetypeid'
from calibrationtype
inner join servicetype on servicetype.servicetypeid = calibrationtype.servicetypeid
where servicetype.shortname = 'AC-IH';

-- Insert default quotation calibration conditions for new cal type and every business company
INSERT INTO [dbo].[defaultquotationcalibrationcondition]
(conditiontext, lastModified, caltypeid, orgid)
SELECT '',
'2016-01-01',
(select calibrationtype.caltypeid 
	FROM servicetype 
	INNER JOIN  calibrationtype
	ON servicetype.servicetypeid = calibrationtype.servicetypeid
	WHERE servicetype.shortname = 'AC-IC'), 
company.coid
FROM company
WHERE company.corole = 5;

-- Insert links to jobtypes
INSERT INTO jobtypecaltype (defaultvalue, jobtypeid, caltypeid)
	SELECT 0,1,caltypeid
	FROM calibrationtype
		INNER JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
	WHERE servicetype.shortname = 'AC-IC';

INSERT INTO jobtypecaltype (defaultvalue, jobtypeid, caltypeid)
  SELECT 0,2,caltypeid
  FROM calibrationtype
    INNER JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'AC-IC';


-- Insert accreditation levels for new cal type
INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'APPROVE', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'AC-IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'REMOVED', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'AC-IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'SUPERVISED', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'AC-IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'PERFORM', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'AC-IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'SIGN', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'AC-IC';



--Add translations
INSERT INTO servicetypelongnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.longname from servicetype
WHERE servicetype.shortname = 'AC-IC';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Calibración acreditada Inter-Company'
from servicetype
where servicetype.shortname = 'AC-IC';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'fr_FR', 'Vérification Accréditée Inter-sociétés'
from servicetype
where servicetype.shortname = 'AC-IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.shortname from servicetype
WHERE servicetype.shortname = 'AC-IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'es_ES',servicetype.shortname from servicetype
WHERE servicetype.shortname = 'AC-IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'fr_FR','AC-IS' from servicetype
WHERE servicetype.shortname = 'AC-IC';

COMMIT TRAN;