-- Creates SQL server function to get the delivery date of a given jobitem from the latest assiciated delivery note
-- created this function since the erp in a certain scenario can allow multiple client delivery notes for the same jobitem
-- and could not use 'limit' clause or 'top' in the subquery since they're not supported in jpa criteria api to narrow the result
-- Ahmed Yassine Boutahar (27/12/2017)

USE [spain]
GO

BEGIN TRAN 

	create function dbo.getDeliveryDateFromLastCreatedClientDelivery (@jobitemID int) returns Date
	as
	begin 
		declare @deliveryDate date
		
		select @deliveryDate = CAST(deliveryDate as date)
		from (
			select top 1 d.deliverydate as deliveryDate
			from dbo.delivery d
			join dbo.deliveryitem di on di.deliveryid=d.deliveryid
			join dbo.jobitem ji on ji.jobitemid = di.jobitemid
			where ji.jobitemid=@jobitemID
			order by d.creationdate desc , d.deliveryid desc
		) as deliveryDate
		
		return @deliveryDate
	
	END
	
	GO
	
	INSERT INTO dbversion (version) VALUES (209);

COMMIT TRAN