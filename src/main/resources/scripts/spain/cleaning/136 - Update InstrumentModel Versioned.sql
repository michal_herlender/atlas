-- Script to set lastModified on instrumentmodel and component
-- InstrumentModel is now versioned for Plantillas updates
-- Also adds indexes on lastModified (improves query performance)
-- Author Galen Beck - 2017-04-19

USE [spain]
GO

BEGIN TRAN;

UPDATE [dbo].[component] SET [lastModified] = '2017-04-19' WHERE [lastModified] IS NULL;
UPDATE [dbo].[instmodel] SET [lastModified] = '2017-04-19' WHERE [lastModified] IS NULL;
UPDATE [dbo].[salescategory] SET [lastModified] = '2017-04-19' WHERE [lastModified] IS NULL;
UPDATE [dbo].[description] SET [lastModified] = '2017-04-19' WHERE [lastModified] IS NULL;
UPDATE [dbo].[instmodelfamily] SET [lastModified] = '2017-04-19' WHERE [lastModified] IS NULL;
UPDATE [dbo].[instmodeldomain] SET [lastModified] = '2017-04-19' WHERE [lastModified] IS NULL;

ALTER table dbo.component ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.instmodel ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.salescategory ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.description ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.instmodelfamily ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.instmodeldomain ALTER COLUMN lastModified datetime NOT NULL;

CREATE INDEX IDX_instrument_lastModified ON instrument (lastModified);
CREATE INDEX IDX_instrumentmodel_lastModified ON instmodel (lastModified);

COMMIT TRAN;

