-- Script to make some minor alterations to new oauth2clientdetails table
-- rename some descriptions
-- and also add two client details and associated roles to US Calypso (cal tool) and US TAM (portal)
-- Galen Beck (2020-05-21)

USE [atlas]
GO

BEGIN TRAN

-- Section 1 - little updates to structure of client details

-- "clientid' is the real PK, removing [id] column as simplification
ALTER TABLE [oauth2clientdetails] DROP COLUMN [id];
-- Making remaining columns not nullable (precaution for future configurations)
ALTER TABLE [oauth2clientdetails] ALTER COLUMN [resourceIds] varchar(256) not null;
ALTER TABLE [oauth2clientdetails] ALTER COLUMN [scope] varchar(256) not null;
ALTER TABLE [oauth2clientdetails] ALTER COLUMN [accesstokenvalidity] int not null;
ALTER TABLE [oauth2clientdetails] ALTER COLUMN [refreshtokenvalidity] int not null;
-- Change description from 2000 to 100 characters (should be more than enough)
ALTER TABLE [oauth2clientdetails] ALTER COLUMN [description] nvarchar(100) not null;

GO


-- Section 2 - Little renaming of new roles to reflect the configuration of the external systems which use them
-- and invoice / credit note PDF permission to be more generalized
-- also renamed WEB permission to WEB_ACCESS (used by NEW_USER_RIGHTS granting internal users access to portal)

UPDATE [dbo].[perrole] SET [name] = 'ROLE_WEB_SERVICES_SIDETRACK' WHERE name = 'ROLE_WEB_SERVICES_INVOICE_CREDIT_NOTE'
UPDATE [dbo].[perrole] SET [name] = 'ROLE_WEB_SERVICES_MOBILEAPP_CLIENT' WHERE name = 'ROLE_WEB_SERVICES_PORTAL'
UPDATE [dbo].[perrole] SET [name] = 'ROLE_WEB_SERVICES_MOBILEAPP_LOGISTICS' WHERE name = 'ROLE_WEB_SERVICES_PREBOOKING'
UPDATE [dbo].[permission] SET [permission] = 'WEB_SERVICE_DOWNLOAD_INVOICE_OR_CREDIT_NOTE_PDF' WHERE [permission] = 'WEB_SERVICE_SIDETRADE_UPLOAD_INVOICE_OR_CREDIT_NOTE';
UPDATE [dbo].[permission] SET [permission] = 'WEB_SERVICE_MOBILE_CLIENT_DATA' WHERE [permission] = 'WEB_SERVICE_PORTAL';
UPDATE [dbo].[permission] SET [permission] = 'WEB_ACCESS' WHERE [permission] = 'WEB';



-- Section 3 - Add additional top level roles for US portal, US cal tool, and a temporary one to support the old "mobileapp" credentials

INSERT INTO [dbo].[perrole] ([name]) VALUES ('ROLE_WEB_SERVICES_US_PORTAL');
INSERT INTO [dbo].[perrole] ([name]) VALUES ('ROLE_WEB_SERVICES_US_CALIBRATION');
INSERT INTO [dbo].[perrole] ([name]) VALUES ('ROLE_WEB_SERVICES_MOBILEAPP_TEMPORARY');


-- Section 4 - Add and configure a new permission group for general data synchronization (to use by mutliple US systems, to start)

INSERT INTO [dbo].[permissiongroup] ([name]) VALUES ('GROUP_WEB_SERVICES_GENERAL_SYNC');

GO

-- Add GROUP_WEB_SERVICES_CORE into the roles used for web services

declare @roleSidetrack int, @roleMobileAppClient int, @roleMobileAppLogistics int;
declare @roleUSPortal int, @roleUSCalibration int, @roleMobileAppTemporary int;
declare @groupWebServiceCore int, @groupWebServiceGeneralSync int;
declare @groupWebServicePortal int, @groupWebServicePrebooking int;
declare @groupNewUserRights int;

-- @perRoleSidetrack int, @perRoleMobileAppClient int, @perRoleMobileAppLogistics int, @perRoleUSPortal
select @roleSidetrack =           [id] FROM [dbo].[perrole] where [name] = 'ROLE_WEB_SERVICES_SIDETRACK';
select @roleMobileAppClient =     [id] FROM [dbo].[perrole] where [name] = 'ROLE_WEB_SERVICES_MOBILEAPP_CLIENT';
select @roleMobileAppLogistics =  [id] FROM [dbo].[perrole] where [name] = 'ROLE_WEB_SERVICES_MOBILEAPP_LOGISTICS';
select @roleUSPortal =            [id] FROM [dbo].[perrole] where [name] = 'ROLE_WEB_SERVICES_US_PORTAL';
select @roleUSCalibration =       [id] FROM [dbo].[perrole] where [name] = 'ROLE_WEB_SERVICES_US_CALIBRATION';
select @roleMobileAppTemporary =    [id] FROM [dbo].[perrole] where [name] = 'ROLE_WEB_SERVICES_MOBILEAPP_TEMPORARY';

select @groupWebServiceCore =        [id] FROM [dbo].[permissiongroup] where [name] = 'GROUP_WEB_SERVICES_CORE';
select @groupWebServicePortal =      [id] FROM [dbo].[permissiongroup] where [name] = 'GROUP_WEB_SERVICES_PORTAL';
select @groupWebServicePrebooking =  [id] FROM [dbo].[permissiongroup] where [name] = 'GROUP_WEB_SERVICES_PREBOOKING';
select @groupWebServiceGeneralSync = [id] FROM [dbo].[permissiongroup] where [name] = 'GROUP_WEB_SERVICES_GENERAL_SYNC';
select @groupNewUserRights =         [id] FROM [dbo].[permissiongroup] where [name] = 'GROUP_NEW_USER_RIGHTS';

-- Add necessary groups into the temporary role for the mobile portal

INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleMobileAppTemporary, @groupWebServiceCore);
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleMobileAppTemporary, @groupWebServicePortal);
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleMobileAppTemporary, @groupWebServicePrebooking);

-- Add the "core" web service group into all the roles

INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleSidetrack, @groupWebServiceCore);
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleMobileAppClient, @groupWebServiceCore);
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleMobileAppLogistics, @groupWebServiceCore);
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleUSPortal, @groupWebServiceCore);
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleUSCalibration, @groupWebServiceCore);

-- Add the group for general data synchronization into the roles configured for the two US applications

INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleUSPortal, @groupWebServiceGeneralSync);
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleUSCalibration, @groupWebServiceGeneralSync);

-- Add the new permission for general synchronization into the group for general data synchronization

INSERT INTO [dbo].[permission] ([groupId],[permission]) VALUES (@groupWebServiceGeneralSync , 'WEB_SERVICE_GENERAL_DATA_SYNC')

-- Add the mobile web services permissions into 'GROUP_NEW_USER_RIGHTS' so all employees could use the logistics and client mobile apps

INSERT INTO [dbo].[permission] ([groupId],[permission]) VALUES (@groupNewUserRights, 'WEB_SERVICE_PING');
INSERT INTO [dbo].[permission] ([groupId],[permission]) VALUES (@groupNewUserRights, 'WEB_SERVICE_GET_CURRENT_USER');
INSERT INTO [dbo].[permission] ([groupId],[permission]) VALUES (@groupNewUserRights, 'WEB_SERVICE_MOBILE_CLIENT_DATA');
INSERT INTO [dbo].[permission] ([groupId],[permission]) VALUES (@groupNewUserRights, 'WEB_SERVICE_LOGISTICS_ADD_PREBOOKING');

-- Section 2 - add additional client details (two for US), one temporarily for reverse compatibility until mobile apps fully updated

INSERT INTO [dbo].[oauth2clientdetails]
           ([clientid]
           ,[clientSecret]
           ,[role]
           ,[resourceIds]
           ,[scope]
           ,[accesstokenvalidity]
           ,[refreshtokenvalidity]
           ,[description])
     VALUES
           ('mobileapp'
           ,'secret'
           ,@roleMobileAppTemporary
           ,'resource-server-rest-api'
           ,'all'
           ,36000
           ,36000
           ,'Temporary client details reflecting legacy values until client and logistics mobile apps updated'),
           ('us_calypso'
           ,'skd035;d03l'
           ,@roleUSCalibration
           ,'resource-server-rest-api'
           ,'all'
           ,36000
           ,36000
           ,'US Calypso Calibration Tool'),
           ('us_tam'
           ,'096-sf0jdg9'
           ,@roleUSPortal
           ,'resource-server-rest-api'
           ,'all'
           ,36000
           ,36000
           ,'US TAM Customer Portal');

GO

INSERT INTO dbversion(version) VALUES (633);

GO

COMMIT TRAN
