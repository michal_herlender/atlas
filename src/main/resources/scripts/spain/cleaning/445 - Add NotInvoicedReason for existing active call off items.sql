-- This script inserts a "not invoiced reason" for existing call off items that are missing not invoiced data
-- Galen 2019-05-24
USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[jobitemnotinvoiced]
           ([comments]
           ,[date]
           ,[reason]
           ,[contactid]
           ,[jobitemid]
           ,[periodicinvoiceid])
     SELECT
           'Upgrade - called off items are now automatically not invoiced'
           ,'2019-05-24'
           ,8
           ,17280
           ,[calloffitem].[offitem]
           ,null
	 FROM [dbo].[calloffitem] 
	   LEFT JOIN jobitem ON offitem = jobitem.jobitemid
	   LEFT JOIN jobitemnotinvoiced as alreadynotinvoiced on alreadynotinvoiced.jobitemid = jobitem.jobitemid
	 WHERE calloffitem.active = 1 and alreadynotinvoiced.jobitemid is null;

	INSERT INTO dbversion (version) VALUES (445);

GO

COMMIT TRAN
