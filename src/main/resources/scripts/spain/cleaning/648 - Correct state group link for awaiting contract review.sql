USE [atlas]
GO

BEGIN TRAN

	declare @Id int;
	
	select @id = iss.stateid
		from itemstate iss where iss.description =  'Awaiting Contract Review' and iss.retired = 0;
	
	INSERT INTO stategrouplink (groupid,stateid,[type])
		VALUES (64,@Id,'ALLOCATED_SUBDIV');

	INSERT INTO dbversion(version) VALUES (648);

COMMIT TRAN