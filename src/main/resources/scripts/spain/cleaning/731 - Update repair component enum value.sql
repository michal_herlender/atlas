-- It seems in script 706 we changed BOOKED to IN_STOCK but we also need the change below
-- ~150+ examples in production data already as PREBOOKED

-- Also change two service type definitions / translations after yesterday

USE [atlas]
GO

BEGIN TRAN

UPDATE dbo.freerepaircomponent SET [status] = 'TO_ORDER' WHERE [status] = 'PREBOOKED';
GO

DECLARE @trade_servicetype int;

SELECT @trade_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'EXCH';

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Trade-In'
 WHERE [servicetypeid] = @trade_servicetype AND [locale] in ('fr_FR', 'en_GB')

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'TRADE'
 WHERE [servicetypeid] = @trade_servicetype AND [locale] in ('fr_FR', 'en_GB')

 UPDATE [dbo].[servicetype]
   SET [shortname] = 'TRADE', [longname] = 'Trade-In'
 WHERE [servicetypeid] = @trade_servicetype

INSERT INTO dbversion(version) VALUES(731);

COMMIT TRAN