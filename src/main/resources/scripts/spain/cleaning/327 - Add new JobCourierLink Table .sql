-- Script 327 - Add new JobCourierLink table for storing Job-Courier data

USE [spain]

BEGIN TRAN

CREATE TABLE dbo.jobcourierlink(
		id int NOT NULL IDENTITY(1,1),
		courierid int Not NULL,
		jobid int NOT NULL,
		receiptdate datetime2,
		pickupdate datetime2,
		trackingnumber varchar(255),
		numberofpackages int,
		packagetype varchar(255),
		storagearea varchar(255),
		lastModified datetime2,
		lastModifiedBy int
	);

    alter table dbo.jobcourierlink
       add constraint PK_jobcourierlink PRIMARY KEY(id);

	alter table dbo.jobcourierlink
       add constraint FK_jobcourierlink_courierid_courier FOREIGN KEY(courierid) REFERENCES dbo.courier(courierid);

	alter table dbo.jobcourierlink
       add constraint FK_jobcourierlink_jobid_job FOREIGN KEY(jobid) REFERENCES dbo.job(jobid);

	alter table dbo.jobcourierlink
       add constraint FK_jobcourierlink_lastModifiedBy_contact FOREIGN KEY(lastModifiedBy) REFERENCES dbo.contact(personid);

INSERT INTO dbversion(version) VALUES(327);

COMMIT TRAN