-- Fixes reversed Spanish / French model translations
-- Galen Beck - 2018-03-20

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'TRANSPORT DES INSTRUMENTS  STANDARD'
 WHERE [modelid] = 41713 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'TRANSPORTE DE INSTRUMENTOS  EST�NDAR'
 WHERE [modelid] = 41713 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'TRANSPORT DES INSTRUMENTS  EXPRESS'
 WHERE [modelid] = 41714 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'TRANSPORTE DE INSTRUMENTOS  EXPRESO'
 WHERE [modelid] = 41714 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'FORMATION / COACHING CLIENTS'
 WHERE [modelid] = 41715 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'FORMACION CLIENTES'
 WHERE [modelid] = 41715 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'LICENSES LOGICIEL'
 WHERE [modelid] = 41716 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'LICENCIAS SOFTWARE'
 WHERE [modelid] = 41716 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'CONSEIL ET EXPERTISE'
 WHERE [modelid] = 41717 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'CONSULTORIA Y EXPERIENCIA'
 WHERE [modelid] = 41717 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'GESTION DE PARC'
 WHERE [modelid] = 41718 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'GESTION DE PARQUE'
 WHERE [modelid] = 41718 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'AUTRES SERVICES'
 WHERE [modelid] = 41719 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'OTROS SERVICIOS'
 WHERE [modelid] = 41719 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'FRAIS DE D�PLACEMENT   UN JOUR DE VOYAGE'
 WHERE [modelid] = 41720 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'GASTOS DE DESPLAZAMIENTO  UN D�A DE VIAJE'
 WHERE [modelid] = 41720 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'FRAIS DE D�PLACEMENT   JOURS CONS�CUTIFS DE VOYAGE'
 WHERE [modelid] = 41721 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'GASTOS DE DESPLAZAMIENTO  D�AS CONSECUTIVOS DE VIAJE'
 WHERE [modelid] = 41721 AND [locale] = 'es_ES';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'PR�PARATION DES INTERVENTIONS'
 WHERE [modelid] = 41722 AND [locale] = 'fr_FR';

UPDATE [dbo].[instmodeltranslation]
   SET [translation] = 'PREPARACION DE INTERVENCIONES'
 WHERE [modelid] = 41722 AND [locale] = 'es_ES';

GO
	
INSERT INTO dbversion (version) VALUES (238);

COMMIT TRAN