update dbo.calibrationtype
set orderby = 1
FROM dbo.calibrationtype
inner join dbo.servicetype on dbo.servicetype.servicetypeid = dbo.calibrationtype.servicetypeid
where dbo.servicetype.longname = 'In-House Accredited Calibration';

update dbo.calibrationtype
set orderby = 2
FROM dbo.calibrationtype
inner join dbo.servicetype on dbo.servicetype.servicetypeid = dbo.calibrationtype.servicetypeid
where dbo.servicetype.longname = 'In-House Standard Calibration';

update dbo.calibrationtype
set orderby = 3
FROM dbo.calibrationtype
inner join dbo.servicetype on dbo.servicetype.servicetypeid = dbo.calibrationtype.servicetypeid
where dbo.servicetype.longname = 'On-Site Accredited Calibration';

update dbo.calibrationtype
set orderby = 4
FROM dbo.calibrationtype
inner join dbo.servicetype on dbo.servicetype.servicetypeid = dbo.calibrationtype.servicetypeid
where dbo.servicetype.longname = 'On-Site Standard Calibration';

update dbo.calibrationtype
set orderby = 5
FROM dbo.calibrationtype
inner join dbo.servicetype on dbo.servicetype.servicetypeid = dbo.calibrationtype.servicetypeid
where dbo.servicetype.longname = 'External Accredited Calibration';

update dbo.calibrationtype
set orderby = 6
FROM dbo.calibrationtype
inner join dbo.servicetype on dbo.servicetype.servicetypeid = dbo.calibrationtype.servicetypeid
where dbo.servicetype.longname = 'External Standard Calibration';

update dbo.calibrationtype
set orderby = 7
FROM dbo.calibrationtype
inner join dbo.servicetype on dbo.servicetype.servicetypeid = dbo.calibrationtype.servicetypeid
where dbo.servicetype.longname = 'Other';
		
		