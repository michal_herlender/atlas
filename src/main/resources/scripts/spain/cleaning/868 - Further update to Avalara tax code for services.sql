BEGIN TRAN

UPDATE [dbo].[nominalcode]
   SET [avalarataxcode] = 'SR060200'
 WHERE [code] in ('S001', 'S002', 'S003', 'S004', 'S011', 'S901', 'S902', 'S903', 'S904', 'S911');
GO

INSERT INTO dbversion(version) VALUES(868);

COMMIT TRAN