-- Author Sothy Loeung
-- 2016-09-15

IF NOT EXISTS (SELECT 1 FROM [systemdefault] WHERE [defaultid] = 41)
BEGIN
	SET IDENTITY_INSERT [systemdefault] ON 
	INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 41, 'boolean', 'Defines if the default invoice type to print is detailed', 'Detailed invoice as default', 'true', 0, 0, 2, 0
	WHERE NOT EXISTS (SELECT * FROM [systemdefault] WHERE [defaultid] = 41)
	SET IDENTITY_INSERT [systemdefault] OFF;	
	
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (41,'en_GB','Detailed invoice as default')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (41,'fr_FR','Facture d�taill�e par d�faut')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (41,'es_ES','Factura detallada como predeterminada')
	
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (41,'en_GB','Detailed invoice type')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (41,'fr_FR','Facture d�taill�e par d�faut')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (41,'es_ES','Factura detallada como predeterminada')
	
	INSERT INTO systemdefaultscope (defaultid,scope)
	VALUES (41,0)
	
	INSERT INTO systemdefaultcorole (defaultid,coroleid)
	VALUES (41,5)
END
ELSE
BEGIN
	UPDATE [systemdefault]
	SET [defaultdatatype] = 'boolean',
	[defaultdescription] = 'Defines if the default invoice type to print is detailed',
	[defaultname] = 'Detailed invoice as default',
	[defaultvalue] = 'true',
	[endd] = 0,
	[start] = 0,
	[groupid] = 2,
	[groupwide] = 0
	WHERE defaultid = 41
END

IF NOT EXISTS (SELECT 1 FROM [systemdefault] WHERE [defaultid] = 42)
BEGIN
	SET IDENTITY_INSERT [systemdefault] ON 
	INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 42, 'boolean', 'Defines the default setting for printing discount fields', 'Print discount', 'true', 0, 0, 2, 0
	WHERE NOT EXISTS (SELECT * FROM [systemdefault] WHERE [defaultid] = 42)
	SET IDENTITY_INSERT [systemdefault] OFF;	
	
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (42,'en_GB','Print discount')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (42,'fr_FR','Imprimer la r�duction')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (42,'es_ES','Imprimir descuento')
	
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (42,'en_GB','Print discount')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (42,'fr_FR','Imprimer la r�duction')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (42,'es_ES','Imprimir descuento')
	
	INSERT INTO systemdefaultscope (defaultid,scope)
	VALUES (42,0)
	
	INSERT INTO systemdefaultcorole (defaultid,coroleid)
	VALUES (42,5)
END
ELSE
BEGIN
	UPDATE [systemdefault]
	SET [defaultdatatype] = 'boolean',
	[defaultdescription] = 'Defines the default setting for printing discount fields',
	[defaultname] = 'Print discount',
	[defaultvalue] = 'true',
	[endd] = 0,
	[start] = 0,
	[groupid] = 2,
	[groupwide] = 0
	WHERE defaultid = 42
END

IF NOT EXISTS (SELECT 1 FROM [systemdefault] WHERE [defaultid] = 43)
BEGIN
	SET IDENTITY_INSERT [systemdefault] ON 
	INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 43, 'boolean', 'Defines the default setting for printing unitary price fields', 'Print unitary price', 'true', 0, 0, 2, 0
	WHERE NOT EXISTS (SELECT * FROM [systemdefault] WHERE [defaultid] = 43)
	SET IDENTITY_INSERT [systemdefault] OFF;	
	
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (43,'en_GB','Print unitary price')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (43,'fr_FR','Imprimer le prix unitaire')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (43,'es_ES','Imprimir el precio unitario')

	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (43,'en_GB','Print unitary price')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (43,'fr_FR','Imprimer le prix unitaire')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (43,'es_ES','Imprimir el precio unitario')

	INSERT INTO systemdefaultscope (defaultid,scope)
	VALUES (43,0)

	INSERT INTO systemdefaultcorole (defaultid,coroleid)
	VALUES (43,5)
END
ELSE
BEGIN
	UPDATE [systemdefault]
	SET [defaultdatatype] = 'boolean',
	[defaultdescription] = 'Defines the default setting for printing unitary price fields',
	[defaultname] = 'Print unitary price',
	[defaultvalue] = 'true',
	[endd] = 0,
	[start] = 0,
	[groupid] = 2,
	[groupwide] = 0
	WHERE defaultid = 43
END

IF NOT EXISTS (SELECT 1 FROM [systemdefault] WHERE [defaultid] = 44)
BEGIN
	SET IDENTITY_INSERT [systemdefault] ON 
	INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 44, 'boolean', 'Defines the default setting for printing quantity fields', 'Print quantity', 'true', 0, 0, 2, 0
	WHERE NOT EXISTS (SELECT * FROM [systemdefault] WHERE [defaultid] = 44)
	SET IDENTITY_INSERT [systemdefault] OFF;	
	
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (44,'en_GB','Print quantity')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (44,'fr_FR','Imprimer la quantit�')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (44,'es_ES','Imprimir cantidad')

	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (44,'en_GB','Print quantity')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (44,'fr_FR','Imprimer la quantit�')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (44,'es_ES','Imprimir cantidad')

	INSERT INTO systemdefaultscope (defaultid,scope)
	VALUES (44,0)

	INSERT INTO systemdefaultcorole (defaultid,coroleid)
	VALUES (44,5)
END
ELSE
BEGIN
	UPDATE [systemdefault]
	SET [defaultdatatype] = 'boolean',
	[defaultdescription] = 'Defines the default setting for printing quantity fields',
	[defaultname] = 'Print quantity',
	[defaultvalue] = 'true',
	[endd] = 0,
	[start] = 0,
	[groupid] = 2,
	[groupwide] = 0
	WHERE defaultid = 44
END