-- Adds default text for quotation document in database (by language currently)

USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[quotationdocdefaulttext]
GO

INSERT INTO [dbo].[quotationdocdefaulttext]
           ([lastModified]
           ,[locale]
           ,[lastModifiedBy]
           ,[subject]
           ,[body]
		   )
     VALUES
           ('2018-12-06'
           ,'en_GB'
           ,17280
           ,'RE: Quotation Request'
           ,'In response to your request, we have pleasure in submitting our calibration quotation.

The prices quoted are subject to Trescal terms and conditions as per the attached summary.

The turnaround time is counted from the reception of your instruments and your order.

If we detect a hidden failure, an additional offer will be sent to you.

Return transport is provided at the expense and risk of the customer.

You may like to visit our website at www.trescal.com to see the additional services we provide to assist customers in the management of the calibration of their test equipment.

We trust the costs provided meet with your approval, but should you require further information please do not hesitate to contact the undersigned.

Kind regards,'
		   ),

           ('2018-12-06'
           ,'fr_FR'
           ,17280
           ,'Objet: Demande d''offre'
           ,'Pour faire suite � votre demande, nous avons le plaisir de vous communiquer notre offre.

Les prix indiqu�s dans les offres sont soumis � nos conditions g�n�rales de vente (voir derni�re page).

Les d�lais s''entendent � compter de la r�ception de vos instruments et de votre commande.

Si nous d�tectons une panne cach�e, une offre compl�mentaire vous sera adress�e.

Vous pouvez visiter notre site web www.trescal.com pour connaitre les services compl�mentaires que nous proposons.

En attente de votre approbation, nous restons � votre disposition pour tout renseignement.

Cordialement,'
		   ),

           ('2018-12-06'
           ,'de_DE'
           ,17280
           ,'RE: Kalibrier-Dienstleistungen'
           ,'Als Antwort auf Ihre Anfrage �bermitteln wir Ihnen gerne unser Angebot.

Die angegebenen Preise unterliegen den Gesch�ftsbedingungen von Trescal gem�� der beigef�gten Zusammenfassung.

Die Bearbeitungszeit wird ab dem Eingang Ihrer Bestellung gez�hlt.

Wenn wir einen versteckten Fehler entdecken, wird ein zus�tzliches Angebot an Sie gesendet.

Der R�cktransport erfolgt auf Kosten und Risiko des Kunden.

Gerne k�nnen Sie unsere Webseite www.trescal.com besuchen, um die zus�tzlichen Dienstleistungen zu sehen, die wir als Unterst�tzung f�r unsere Kunden im Kalibrier-Management der Ger�te anbieten, wie Kalibrier-Terminplanung, Ger�te-Verwaltung, Zertifikate-Download und andere Funktionen, die Ihnen bei der Verwaltung Ihrer Pr�f- und Messmittel helfen.

Wir hoffen, dass die angegebenen Kosten auf Ihre Zustimmung treffen werden. Sollten Sie jedoch weitere Informationen ben�tigen, dann z�gern Sie bitte nicht, mit dem Unterzeichner in Kontakt zu treten.

Mit freundlichen Gr��en,'
		   ),

           ('2018-12-06'
           ,'es_ES'
           ,17280
           ,'RE: Solicitud de oferta'
           ,'Tenemos el placer de enviarle nuestra oferta por los servicios requeridos.

Los precios ofertados estan sometidos a las condiciones generales adjuntas.

Los plazos se cuentan desde la recepci�n de su pedido.

Si detectamos un error oculto, se le enviar� una oferta adicional.

El transporte de regreso se realiza a expensas y riesgo del cliente.

Visite nuestra web a www.trescal.com para ver los servicios adicionales que tenemos para atender a nuestros clientes en la gesti�n de la calibraci�n de sus equipos, descarga de certificados y otras aplicaciones.

Confiamos que esta oferta sea de su agrado y si necesita ampliar la informaci�n, por favor no dude en ponerse en contacto con nosotros.

Atentamente,'
		   )

GO

INSERT INTO dbversion(version) VALUES(363);

COMMIT TRAN