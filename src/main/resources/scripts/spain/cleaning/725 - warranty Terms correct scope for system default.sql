USE [atlas]
GO

BEGIN TRAN

/* */
delete from systemdefaultscope where defaultid=64
delete from systemdefaultscope where defaultid=65

/* */
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (0, 64);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (2, 64);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (0, 65);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (2, 65);

INSERT INTO dbversion(version) VALUES(725);

COMMIT TRAN