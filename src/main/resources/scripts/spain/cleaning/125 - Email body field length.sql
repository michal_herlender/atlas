-- Removed restriction on email size as messages over 3000 characters were resulting in errors/not being saved
-- Galen Beck 2017-03-17

USE [spain]
GO

BEGIN TRAN

ALTER TABLE [email] ALTER COLUMN [body] VARCHAR(MAX);

COMMIT TRAN;