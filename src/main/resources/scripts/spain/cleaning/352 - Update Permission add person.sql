USE [spain]

Begin Tran

delete from dbo.permission where permission='ADD_PERSON';

insert into dbo.permission values (1,'ADD_PERSON');

INSERT INTO dbversion(version) VALUES(352);

COMMIT TRAN