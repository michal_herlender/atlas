
USE [spain]

BEGIN TRAN

UPDATE dbo.invoicestatusdescriptiontranslation 

SET [translation]='Job Items en attente de facturation interne'

WHERE statusid=7 AND locale='fr_FR';


INSERT INTO dbversion(version) VALUES(396);

COMMIT TRAN