USE [atlas]
GO

BEGIN TRAN

DECLARE @permissionGroupId INT;

SELECT @permissionGroupId=pg.id FROM permissiongroup pg where pg.name like 'GROUP_NEW_USER_RIGHTS';
INSERT INTO permission (groupId,permission) values(@permissionGroupId,'JOB_ITEM_NEW_GROUP_ADD');

INSERT INTO dbversion(version) VALUES (588);
COMMIT TRAN