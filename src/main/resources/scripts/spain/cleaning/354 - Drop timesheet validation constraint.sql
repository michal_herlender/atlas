use spain;

begin tran

alter table timesheetvalidation drop constraint UK_timesheetvalidation_employee_week_year_status;

insert into dbversion(version) values(354);

commit tran