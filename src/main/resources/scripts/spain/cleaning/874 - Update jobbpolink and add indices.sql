/*
  Upgrade script to solve issue with null "createdon" date in jobbpolink on old links (DEV-3021) and 
  to add indices to help with invoice preview performance (DEV-3020)

*/

BEGIN TRAN

UPDATE [dbo].[jobbpolink]
   SET [createdon] = '2021-01-13'
 WHERE [createdon] is null;
GO

-- Indices identified as improving performance

CREATE NONCLUSTERED INDEX [IDX_quotationitem_calcost_id]
ON [dbo].[quotationitem] ([calcost_id])

CREATE NONCLUSTERED INDEX [IDX_jobcostinglinkedcalibrationcost_quotecalcostid]
ON [dbo].[jobcostinglinkedcalibrationcost] ([quotecalcostid])

CREATE NONCLUSTERED INDEX [IDX_instrument_contract_instrumentid]
ON [dbo].[instrument_contract] ([instrumentid])

CREATE NONCLUSTERED INDEX [IDX_jobcostingitem_costingid]
ON [dbo].[jobcostingitem] ([costingid])

insert into dbversion(version) values(874);

COMMIT TRAN