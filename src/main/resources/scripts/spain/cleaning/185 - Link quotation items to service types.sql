use spain
go

begin tran

alter table dbo.quotationitem
	add servicetypeid int;
	
alter table dbo.quotationitem
	add constraint FK_quotationitem_servicetype
	foreign key (servicetypeid)
	references dbo.servicetype;

go

update quotationitem set servicetypeid = calibrationtype.servicetypeid
from quotationitem
left join calibrationtype on quotationitem.caltype_caltypeid = calibrationtype.caltypeid

commit tran