-- Updates PermissionGroup entity to add a new name

USE [spain]

BEGIN TRAN

    alter table dbo.permissiongroup 
        add name varchar(255) not null;

    alter table dbo.permissiongroup 
        add constraint UK_8w1ijc9ryyrx80310b2t3vl3u unique (name);

INSERT INTO dbversion(version) VALUES(280);

COMMIT TRAN