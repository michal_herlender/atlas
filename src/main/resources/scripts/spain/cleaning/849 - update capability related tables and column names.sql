BEGIN TRAN

exec sp_rename 'capability_servicetype', 'capability_filter';
exec sp_rename 'procs', 'capability';
exec sp_rename 'capability_filter.procedureid' , 'capabilityId';
exec sp_rename 'categorisedprocedure', 'categorised_capability';
exec sp_rename 'categorised_capability.procid', 'capabilityId';
exec sp_rename 'proceduretrainingrecord', 'capability_trainingrecord';
exec sp_rename 'capability_trainingrecord.procid', 'capabilityId';
exec sp_rename 'procedureaccreditation', 'capability_accreditation';
exec sp_rename 'capability_accreditation.procid', 'capabilityId';


exec sp_rename 'instrument.procid', 'capabilityId';
exec sp_rename 'servicecapability.procedureid', 'capabilityId';
exec sp_rename 'calibration.procedureid', 'capabilityId';
exec sp_rename 'workrequirement.procedureid', 'capabilityId';
exec sp_rename 'generalserviceoperation.procedureid', 'capabilityId';
exec sp_rename 'jobitem.procid', 'capabilityId';
exec sp_rename 'defaultstandard.procid', 'capabilityId';






INSERT INTO dbversion(version) VALUES(849);

COMMIT TRAN
