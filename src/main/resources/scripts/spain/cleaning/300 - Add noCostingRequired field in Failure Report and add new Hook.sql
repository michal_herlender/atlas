USE [spain]
GO

BEGIN TRAN

	ALTER TABLE dbo.faultreport ADD nocostingrequired bit
	go
	ALTER TABLE dbo.faultreport ADD outcomepreselected bit 
	go
	ALTER TABLE dbo.faultreport ADD sendtoclient bit
	go
		
	INSERT INTO dbversion(version) VALUES(300);
	
COMMIT TRAN