USE [atlas]
GO

BEGIN TRAN

DECLARE @countryId int;

select @countryId = country.countryid from country where countrycode = 'DE';

INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid],[lastModified],[lastModifiedBy])
     VALUES
           ('�','2nd Half 2020',16.00,1,@countryId,'2020-06-25',31485)

INSERT INTO dbversion(version) VALUES(654);

GO

COMMIT TRAN