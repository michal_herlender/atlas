-- Various updates:
-- Add Taiwan to country definition table
-- Change CALIGAS client company to a business company
-- Delete user accounts for CALIGAS from contacts (they should be recreated as business user accounts) 

USE [spain]

BEGIN TRAN

SET IDENTITY_INSERT [country] ON

INSERT INTO [dbo].[country]
           ([countryid]
		   ,[country]
           ,[countrycode]
           ,[memberOfUE]
           ,[addressprintformat])
     VALUES
           (194
		   ,'Taiwan'
           ,'TW'
           ,0
           ,'TOWN_COMMA_REGION_POSTCODE')
GO

SET IDENTITY_INSERT [country] OFF

UPDATE company set corole = 5 WHERE coid = 8251

DELETE FROM users WHERE username in 
	(SELECT username from users 
	LEFT JOIN contact ON users.personid = contact.personid 
	LEFT JOIN subdiv ON contact.subdivid = subdiv.subdivid
	WHERE subdiv.coid = 8251)
	
INSERT INTO dbversion(version) VALUES(383);

COMMIT TRAN