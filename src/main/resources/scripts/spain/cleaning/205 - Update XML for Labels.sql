-- Updated label design and some settings for Alligator printing

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[labelprinter]
	SET darknesslevel = 15,
		language = 'ZPL',
		resolution = 300,
		media = 1
	WHERE description <> 'TEC_ZGZ_23';

UPDATE [dbo].[labelprinter]
	SET darknesslevel = 30,
		language = 'TEC',
		resolution = 300,
		media = 2
	WHERE description = 'TEC_ZGZ_23';


DELETE FROM [dbo].[alligatorlabeltemplaterule]
DELETE FROM [dbo].[alligatorlabeltemplate]

DECLARE @XmlBodyENAC NVARCHAR(MAX)
DECLARE @XmlBodyENACSmall NVARCHAR(MAX)
DECLARE @XmlBodyStandard NVARCHAR(MAX)
DECLARE @XmlBodyStandardSmall NVARCHAR(MAX)

Set @XmlBodyENAC = '<template>
	<parameters>
		<parameter name="field1" value="" />
		<parameter name="field2" value="" />
		<parameter name="field3" value="" />
		<parameter name="field4" value="" />
		<parameter name="field5" value="" />
		<parameter name="field6" value="" />
		<parameter name="value1" value="" />
		<parameter name="value2" value="" />
		<parameter name="value3" value="" />
		<parameter name="value4" value="" />
		<parameter name="value5" value="" />
		<parameter name="value6" value="" />
		<parameter name="barcode" value="" />
		<parameter name="accred" value="" />
		<parameter name="footer" value=""/>
	</parameters>
	<name>ES_Cal_ENAC</name>
	<height>217</height>
	<width>435</width>
	<string ypos="44" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field1}" />
	<string ypos="66" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field2}" />
	<string ypos="88" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field3}" />
	<string ypos="110" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field4}" />
	<string ypos="132" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field5}" />
	<string ypos="154" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field6}" />
	<string ypos="44" xpos="125" height="20" width="200" rotationAngle="0" font="Verdana" size="17.00" text="${value1}" />
	<string ypos="66" xpos="125" height="20" width="200" rotationAngle="0" font="Verdana" size="17.00" text="${value2}" />
	<string ypos="88" xpos="125" height="20" width="300" rotationAngle="0" font="Verdana" size="17.00" text="${value3}" />
	<string ypos="110" xpos="125" height="20" width="300" rotationAngle="0" font="Verdana" size="17.00" text="${value4}" />
	<string ypos="132" xpos="125" height="20" width="300" rotationAngle="0" font="Verdana" size="17.00" text="${value5}" />
	<string ypos="154" xpos="125" height="20" width="300" rotationAngle="0" font="Verdana" size="17.00" text="${value6}" />
	<string ypos="176" xpos="300" height="20" width="95" rotationAngle="0" font="Verdana" size="17.00" text="${footer}" />
	<string ypos="77" xpos="300" height="20" width="145" rotationAngle="0" font="Verdana" size="14.00" text="${accred}" />
	<barcode ypos="176" xpos="5" height="24" width="250" rotationAngle="0" value="${barcode}" type="128" />
	<image ypos="5" xpos="300" height="72" width="112" rotationAngle="0" name="ENAC_logo_bw_es" />
	<image ypos="5" xpos="5" height="35" width="125" rotationAngle="0" name="trescal_black_no_curve" />
</template>'

Set @XmlBodyENACSmall = '<template>
	<parameters>
		<parameter name="field1" value="" />
		<parameter name="field2" value="" />
		<parameter name="field3" value="" />
		<parameter name="field4" value="" />
		<parameter name="field5" value="" />
		<parameter name="field6" value="" />
		<parameter name="value1" value="" />
		<parameter name="value2" value="" />
		<parameter name="value3" value="" />
		<parameter name="value4" value="" />
		<parameter name="value5" value="" />
		<parameter name="value6" value="" />
		<parameter name="barcode" value="" />
		<parameter name="accred" value="" />
		<parameter name="footer" value=""/>
	</parameters>
	<name>ES_Cal_ENAC_Small</name>
	<height>217</height>
	<width>435</width>
	<string ypos="150" xpos="30" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field1}" />
	<string ypos="150" xpos="42" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field2}" />
	<string ypos="150" xpos="54" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field3}" />
	<string ypos="150" xpos="66" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field4}" />
	<string ypos="150" xpos="78" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field5}" />
	<string ypos="150" xpos="90" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field6}" />
	<string ypos="75" xpos="30" height="12" width="70" rotationAngle="270" font="Verdana" size="12.00" text="${value1}" />
	<string ypos="75" xpos="42" height="12" width="70" rotationAngle="270" font="Verdana" size="12.00" text="${value2}" />
	<string ypos="5" xpos="54" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value3}" />
	<string ypos="5" xpos="66" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value4}" />
	<string ypos="5" xpos="78" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value5}" />
	<string ypos="5" xpos="90" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value6}" />
	<string ypos="5" xpos="42" height="12" width="95" rotationAngle="270" font="Verdana" size="12.00" text="${accred}" />
	<barcode ypos="5" xpos="105" height="14" width="195" rotationAngle="270" value="${barcode}" type="128" />
	<image ypos="5" xpos="2" height="40" width="62" rotationAngle="270" name="ENAC_logo_bw_es" />
	<image ypos="115" xpos="2" height="25" width="90" rotationAngle="270" name="trescal_black_no_curve" />
</template>'

Set @XmlBodyStandard = '<template>
	<parameters>
		<parameter name="field1" value="" />
		<parameter name="field2" value="" />
		<parameter name="field3" value="" />
		<parameter name="field4" value="" />
		<parameter name="field5" value="" />
		<parameter name="field6" value="" />
		<parameter name="value1" value="" />
		<parameter name="value2" value="" />
		<parameter name="value3" value="" />
		<parameter name="value4" value="" />
		<parameter name="value5" value="" />
		<parameter name="value6" value="" />
		<parameter name="barcode" value="" />
		<parameter name="footer" value=""/>
	</parameters>
	<name>Cal_Standard</name>
	<height>217</height>
	<width>435</width>
	<string ypos="44" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field1}" />
	<string ypos="66" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field2}" />
	<string ypos="88" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field3}" />
	<string ypos="110" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field4}" />
	<string ypos="132" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field5}" />
	<string ypos="154" xpos="5" height="20" width="106" rotationAngle="0" font="Verdana" size="17.00" text="${field6}" />
	<string ypos="44" xpos="125" height="20" width="200" rotationAngle="0" font="Verdana" size="17.00" text="${value1}" />
	<string ypos="66" xpos="125" height="20" width="200" rotationAngle="0" font="Verdana" size="17.00" text="${value2}" />
	<string ypos="88" xpos="125" height="20" width="300" rotationAngle="0" font="Verdana" size="17.00" text="${value3}" />
	<string ypos="110" xpos="125" height="20" width="300" rotationAngle="0" font="Verdana" size="17.00" text="${value4}" />
	<string ypos="132" xpos="125" height="20" width="300" rotationAngle="0" font="Verdana" size="17.00" text="${value5}" />
	<string ypos="154" xpos="125" height="20" width="300" rotationAngle="0" font="Verdana" size="17.00" text="${value6}" />
	<string ypos="176" xpos="300" height="20" width="95" rotationAngle="0" font="Verdana" size="17.00" text="${footer}" />
	<barcode ypos="176" xpos="5" height="24" width="250" rotationAngle="0" value="${barcode}" type="128" />
	<image ypos="5" xpos="5" height="35" width="125" rotationAngle="0" name="trescal_black_no_curve" />
</template>'

Set @XmlBodyStandardSmall = '<template>
	<parameters>
		<parameter name="field1" value="" />
		<parameter name="field2" value="" />
		<parameter name="field3" value="" />
		<parameter name="field4" value="" />
		<parameter name="field5" value="" />
		<parameter name="field6" value="" />
		<parameter name="value1" value="" />
		<parameter name="value2" value="" />
		<parameter name="value3" value="" />
		<parameter name="value4" value="" />
		<parameter name="value5" value="" />
		<parameter name="value6" value="" />
		<parameter name="barcode" value="" />
		<parameter name="accred" value="" />
		<parameter name="footer" value=""/>
	</parameters>
	<name>Cal_Standard_Small</name>
	<height>217</height>
	<width>435</width>
	<string ypos="150" xpos="30" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field1}" />
	<string ypos="150" xpos="42" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field2}" />
	<string ypos="150" xpos="54" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field3}" />
	<string ypos="150" xpos="66" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field4}" />
	<string ypos="150" xpos="78" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field5}" />
	<string ypos="150" xpos="90" height="12" width="60" rotationAngle="270" font="Verdana" size="12.00" text="${field6}" />
	<string ypos="5" xpos="30" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value1}" />
	<string ypos="5" xpos="42" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value2}" />
	<string ypos="5" xpos="54" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value3}" />
	<string ypos="5" xpos="66" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value4}" />
	<string ypos="5" xpos="78" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value5}" />
	<string ypos="5" xpos="90" height="12" width="140" rotationAngle="270" font="Verdana" size="12.00" text="${value6}" />
	<barcode ypos="5" xpos="105" height="14" width="195" rotationAngle="270" value="${barcode}" type="128" />
	<image ypos="115" xpos="2" height="25" width="90" rotationAngle="270" name="trescal_black_no_curve" />
</template>'

SET IDENTITY_INSERT [alligatorlabeltemplate] ON

INSERT INTO [dbo].[alligatorlabeltemplate]
           ([id],[description],[templatexml],[templatetype])
     VALUES
           (1,'ES_Cal_ENAC',@XmlBodyENAC,'ACCREDITED'),
           (2,'ES_Cal_ENAC_Small',@XmlBodyENACSmall,'ACCREDITED_SMALL'),
           (3,'Cal_Standard',@XmlBodyStandard,'STANDARD'),
           (4,'Cal_Standard_Small',@XmlBodyStandardSmall,'STANDARD_SMALL')

SET IDENTITY_INSERT [alligatorlabeltemplate] OFF

-- 6579, 6578 TEM and TIC
-- 6670 MCI
-- 6685 Trescal Maroc (no Accreditation)

INSERT INTO [dbo].[alligatorlabeltemplaterule]
           ([coid],[templateid])
     VALUES
           (6579, 1),
           (6579, 2),
           (6579, 3),
           (6579, 4),
           (6578, 1),
           (6578, 2),
           (6578, 3),
           (6578, 4),
           (6670, 1),
           (6670, 2),
           (6670, 3),
           (6670, 4),
           (6685, 3),
           (6685, 4);

GO

INSERT INTO dbversion (version) VALUES (205);

COMMIT TRAN

