-- Creates SQL server function to find n th previous cal date for and instrument
-- If the second parameter is 1 then the last cal date is returned
-- If the second parameter is 2 then the penultimate cal date is returned etc.. etc..
-- Scott Chamberlain 2017-11-08

USE [spain]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetNthCalDate](@PlantId int, @N int)
  RETURNS date
AS
  BEGIN
    DECLARE @caldate date

    SELECT @caldate = CAST(caldate AS date)
    FROM
      (SELECT instcerts.caldate as caldate
       FROM instcertlink
         LEFT JOIN certificate AS instcerts ON instcerts.certid = instcertlink.certid

       WHERE instcertlink.plantid = @PlantId

       UNION

       SELECT jobcerts.caldate as caldate
       FROM jobitem
         LEFT JOIN certlink ON certlink.jobitemid = jobitem.jobitemid
         LEFT JOIN certificate AS jobcerts ON jobcerts.certid = certlink.certid
       WHERE jobitem.plantid = @PlantId) AS allcaldates

    ORDER BY caldate DESC OFFSET (@N - 1) ROWS FETCH NEXT (1) ROWS ONLY

    RETURN @caldate

  END