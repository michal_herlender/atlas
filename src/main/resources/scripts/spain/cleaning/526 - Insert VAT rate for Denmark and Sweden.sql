-- Inserts two VAT rates for use in Denmark / Sweden by business companies
-- (mainly for demo/tryout purposes at first on test servers)

USE [atlas]
GO

BEGIN TRAN

DECLARE @countryIdDK int;

select @countryIdDK = country.countryid
from country where countrycode = 'DK'

DECLARE @countryIdSE int;

select @countryIdSE = country.countryid
from country where countrycode = 'SE'

INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid],[lastModified])
     VALUES
           ('E','Denmark',25.00,0,@countryIdDK,'2019-10-31'),
           ('F','Sweden',25.00,0,@countryIdSE,'2019-10-31')
GO

-- Also insert system default config for invoice printing options, for client companies (previously configured just for business companies !?!)

INSERT INTO [dbo].[systemdefaultcorole]
           ([coroleid]
           ,[defaultid])
     VALUES
           (1,41), (1,42), (1,43), (1,44); 
GO

INSERT INTO dbversion(version) VALUES(526);

GO

COMMIT TRAN