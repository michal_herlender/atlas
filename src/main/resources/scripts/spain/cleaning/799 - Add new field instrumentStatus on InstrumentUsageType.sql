Use atlas
Go

BEGIN TRAN

ALTER TABLE dbo.instrumentusagetype ADD instrumentstatus INT;
Go

UPDATE dbo.instrumentusagetype SET instrumentstatus = 0 WHERE [name] IN (
'Keep in calibration',
'Available',
'Not Shipped',
'Use With Limitations',
'In Maintenance',
'In Repair',
'Assign To Superior Group');

UPDATE dbo.instrumentusagetype SET instrumentstatus = 1 WHERE [name] IN (
'Out Of Service');

UPDATE dbo.instrumentusagetype SET instrumentstatus = 2 WHERE [name] IN (
'Use only as indicator',
'Calibrate Before Use',
'Lost',
'Inactive',
'Replacement',
'No Calibration Required');

INSERT INTO dbversion VALUES (799);

COMMIT TRAN