
USE [atlas]

BEGIN TRAN    
	
-- Change old failure report outcomes to new ones
--- case 1 : ERP_CALIBRATION -> 
---    INTERNAL_CALIBRATION_WITH_JUDGMENT -OR- INTERNAL_CALIBRATION_WITHOUT_JUDGMENT, based on the recommendation
UPDATE dbo.faultreport
	SET finaloutcome= CASE when 'CALIBRATION_WITH_JUDGMENT' in (select rec.recommendation from faultreportrecommendation rec where rec.faultreportid = faultrepid)
			then 'INTERNAL_CALIBRATION_WITH_JUDGMENT'
			else 'INTERNAL_CALIBRATION_WITHOUT_JUDGMENT'
		end
	WHERE finaloutcome = 'ERP_CALIBRATION'


--- case 2 : ERP_ADJUSTMENT -> 
---    INTERNAL_CALIBRATION_WITH_JUDGMENT -OR- INTERNAL_CALIBRATION_WITHOUT_JUDGMENT, based on the recommendation
UPDATE dbo.faultreport
	SET finaloutcome= CASE
		when 'CALIBRATION_WITH_JUDGMENT' in (select rec.recommendation from faultreportrecommendation rec where rec.faultreportid = faultrepid)
			then 'INTERNAL_CALIBRATION_WITH_JUDGMENT'
		when 'CALIBRATION_WITHOUT_JUDGMENT' in (select rec.recommendation from faultreportrecommendation rec where rec.faultreportid = faultrepid)
			then 'INTERNAL_CALIBRATION_WITHOUT_JUDGMENT'
		else
			finaloutcome
	end
	WHERE finaloutcome = 'ERP_ADJUSTMENT'
--- left cases that doesn't have values 'CALIBRATION_WITH_JUDGMENT' or 'CALIBRATION_WITHOUT_JUDGMENT' in recommendations,
--- => base on default caltype of jobiem
UPDATE dbo.faultreport
	SET finaloutcome= CASE
		when '%-SOC-%' like (select st.shortname from jobitem ji join calibrationtype ct on ji.caltypeid = ct.caltypeid
							join servicetype st on st.servicetypeid = ct.servicetypeid
							where ji.jobitemid = jobitemid)
			then 'INTERNAL_CALIBRATION_WITH_JUDGMENT'
		when '%-MVO-%' like (select st.shortname from jobitem ji join calibrationtype ct on ji.caltypeid = ct.caltypeid
						join servicetype st on st.servicetypeid = ct.servicetypeid
						where ji.jobitemid = jobitemid)
			then 'INTERNAL_CALIBRATION_WITHOUT_JUDGMENT'
		else
			finaloutcome
	end
	WHERE finaloutcome = 'ERP_ADJUSTMENT'


--- case 3 : ERP_REPAIR -> INTERNAL_REPAIR -OR- THIRD_PARTY_REPAIR based on 'operationbytrescal' field
UPDATE dbo.faultreport
	SET finaloutcome = case when operationbytrescal = 1 then 'INTERNAL_REPAIR' 
				else 'THIRD_PARTY_REPAIR' 
			end	
	where finaloutcome = 'ERP_REPAIR'
	

--- case 4 : THIRD_PARTY -> THIRD_PARTY_CALIBRATION_WITH_JUDGMENT, THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT, THIRD_PARTY_REPAIR
---- THIRD_PARTY_REPAIR
UPDATE dbo.faultreport
	SET finaloutcome = case when (select distinct tpr.repair from tprequirement tpr where tpr.jobitemid = jobitemid and tpr.repair=1) is not null
			then 'THIRD_PARTY_REPAIR' 
		else finaloutcome
	end	
	where finaloutcome = 'THIRD_PARTY'
---- THIRD_PARTY_CALIBRATION_WITH_JUDGMENT, THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT,
UPDATE dbo.faultreport
	SET finaloutcome= CASE
		when  (select distinct tpr.calibration from tprequirement tpr where tpr.jobitemid = jobitemid and tpr.calibration=1) is not null
		       and '%-SOC-%' like (select st.shortname from jobitem ji join calibrationtype ct on ji.caltypeid = ct.caltypeid
							join servicetype st on st.servicetypeid = ct.servicetypeid
							where ji.jobitemid = jobitemid)
			then 'THIRD_PARTY_CALIBRATION_WITH_JUDGMENT'
		when  (select distinct tpr.calibration from tprequirement tpr where tpr.jobitemid = jobitemid and tpr.calibration=1) is not null
		       and '%-MVO-%' like (select st.shortname from jobitem ji join calibrationtype ct on ji.caltypeid = ct.caltypeid
							join servicetype st on st.servicetypeid = ct.servicetypeid
							where ji.jobitemid = jobitemid)
			then 'THIRD_PARTY_CALIBRATION_WITH_JUDGMENT'
		else
			finaloutcome
	end
	WHERE finaloutcome = 'THIRD_PARTY'
	
--------------------------------------

UPDATE dbo.actionoutcome
SET activityid=NULL
WHERE type='FAULT_REPORT'

 
UPDATE dbo.itemstate
SET actionoutcometype=NULL
WHERE actionoutcometype='FAULT_REPORT'


INSERT INTO dbversion(version) VALUES(539);

COMMIT TRAN