USE [atlas]
GO

BEGIN TRAN
	--add customer service department type for General service operation
	DECLARE @gso_status INT,
			@gso_activity INT,
			@gso_onhold INT,
			@gso_doc_status INT,
			@gso_client_decision_status INT,
			@vbf_servicetype INT,
			@qual_servicetype INT,
			@new_caltype INT,
			@new_gso_hook INT,
			@gso_doc_activity INT,
			@upload_gso_doc_hook INT;

	SELECT @gso_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting general service operation';
	SELECT @gso_onhold = stateid FROM dbo.itemstate WHERE [description] = 'General service operation On-Hold';
	SELECT @gso_doc_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting document upload for general operation';
	SELECT @gso_client_decision_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting client decision for general service operation';
	INSERT INTO dbo.stategrouplink(groupid,stateid,type) VALUES(69,@gso_status,'ALLOCATED_SUBDIV');
	INSERT INTO dbo.stategrouplink(groupid,stateid,type) VALUES(69,@gso_onhold,'ALLOCATED_SUBDIV');
	INSERT INTO dbo.stategrouplink(groupid,stateid,type) VALUES(69,@gso_doc_status,'ALLOCATED_SUBDIV');
	INSERT INTO dbo.stategrouplink(groupid,stateid,type) VALUES(31,@gso_client_decision_status,'ALLOCATED_SUBDIV');

	INSERT INTO dbo.workassignment(depttypeid,stategroupid)
	VALUES (1,69);

	-- add vbf and qualification cal type
	SELECT @vbf_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'VCO';
	SELECT @qual_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'QUAL';
	
	INSERT INTO dbo.calibrationtype
	(accreditationspecific, active, certnoresetyearly, certprefix, firstpagepapertype, labeltemplatepath, nextcertno, orderby, pdfcontinuationwatermark, pdfheaderwatermark, recalldateonlabel, restpagepapertype, log_lastmodified, lastModified, log_userid, servicetypeid, recallRequirementType, calibrationWithJudgement)
	VALUES(0, 1, 1, NULL, 'A4_PLAIN', '/labels/birt/birt_label_standard.rptdesign', NULL, 42, NULL, NULL, 1, 'A4_PLAIN', NULL, NULL, NULL, @vbf_servicetype, 'FULL_CALIBRATION', 0);

	SELECT @new_caltype = MAX(caltypeid) FROM dbo.calibrationtype;
	INSERT INTO dbo.jobtypecaltype(caltypeid,jobtypeid,defaultvalue)
	VALUES (@new_caltype,1,1)

	INSERT INTO dbo.calibrationtype
	(accreditationspecific, active, certnoresetyearly, certprefix, firstpagepapertype, labeltemplatepath, nextcertno, orderby, pdfcontinuationwatermark, pdfheaderwatermark, recalldateonlabel, restpagepapertype, log_lastmodified, lastModified, log_userid, servicetypeid, recallRequirementType, calibrationWithJudgement)
	VALUES(0, 1, 1, NULL, 'A4_PLAIN', '/labels/birt/birt_label_standard.rptdesign', NULL, 43, NULL, NULL, 1, 'A4_PLAIN', NULL, NULL, NULL, @qual_servicetype, 'FULL_CALIBRATION', 0);
	SELECT @new_caltype = MAX(caltypeid) FROM dbo.calibrationtype;
	INSERT INTO dbo.jobtypecaltype(caltypeid,jobtypeid,defaultvalue)
	VALUES (@new_caltype,1,1)

	--update start gso hook activityid 
	--add new hookactivity for gso on-hold to gso activity
	SELECT @gso_activity = stateid FROM dbo.itemstate WHERE [description] = 'General service operation';
	SELECT @new_gso_hook = id FROM dbo.hook WHERE [name] = 'start-general-service-operation';
	UPDATE dbo.hook SET activityid = @gso_activity WHERE id = @new_gso_hook;
	DELETE FROM dbo.hookactivity WHERE hookid = @new_gso_hook;
	INSERT INTO dbo.hookactivity(beginactivity,completeactivity,hookid,activityid,stateid)
	VALUES (1,0,@new_gso_hook,@gso_activity,@gso_status),
		   (1,0,@new_gso_hook,@gso_activity,@gso_onhold);
	
	--add new hookactivity for upload gso document
	SELECT @gso_doc_activity = stateid FROM dbo.itemstate WHERE [description] = 'Document uploaded for general operation';
	SELECT @upload_gso_doc_hook = id FROM dbo.hook WHERE [name] = 'upload-general-service-operation-document';
	INSERT INTO dbo.hookactivity(beginactivity,completeactivity,hookid,activityid,stateid)
	VALUES (1,1,@upload_gso_doc_hook,@gso_doc_activity,@gso_doc_status);

	--delete unused hook with related hookactivity
	DECLARE @old_start_gso_hook INT,
			@old_complete_gso_hook INT,
			@old_upload_gso_doc_hook INT;
	SELECT @old_start_gso_hook = id FROM dbo.hook WHERE [name] = 'start-general-operation';
	SELECT @old_complete_gso_hook = id FROM dbo.hook WHERE [name] = 'complete-general-operation';
	SELECT @old_upload_gso_doc_hook = id FROM dbo.hook WHERE [name] = 'upload-general-document';
	DELETE FROM dbo.hookactivity WHERE hookid IN (@old_start_gso_hook,@old_complete_gso_hook,@old_upload_gso_doc_hook);
	DELETE FROM dbo.hook WHERE id IN (@old_start_gso_hook,@old_complete_gso_hook,@old_upload_gso_doc_hook);

	INSERT INTO dbversion(version) VALUES (643)

COMMIT TRAN