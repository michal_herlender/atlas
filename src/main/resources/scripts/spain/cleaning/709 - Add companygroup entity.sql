-- Creates new entity for company group
-- Note, the existing groupname in Company is preserved for reference / display / migration

USE [atlas]

BEGIN TRAN

    alter table dbo.company 
       add companygroupid int;

    create table dbo.companygroup (
       id int identity not null,
        lastModified datetime2 not null,
        active bit not null,
        groupname varchar(75) not null,
        lastModifiedBy int,
        primary key (id)
    );

	INSERT INTO dbversion VALUES (709)

COMMIT TRAN