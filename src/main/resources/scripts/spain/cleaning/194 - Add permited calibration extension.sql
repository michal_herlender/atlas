BEGIN TRAN;

USE spain;

create table dbo.permitedcalibrationextension (
        extensionid int identity not null,
        timeunit varchar(255),
        type varchar(255) not null,
        value int not null,
        plantid int not null,
        primary key (extensionid)
    );

INSERT INTO dbversion (version) VALUES (194);

COMMIT TRAN;