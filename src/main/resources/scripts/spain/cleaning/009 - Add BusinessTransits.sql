BEGIN TRAN

INSERT INTO [Spain].[dbo].[businesstransit]
           ([other]
           ,[dayofweek]
           ,[fromsubdivid]
           ,[transportmethodid]
           ,[tosubdivid])
     VALUES
           (null,2,6642,1,6643),
           (null,3,6642,1,6643),
           (null,4,6642,1,6643),
           (null,5,6642,1,6643),
           (null,6,6642,1,6643),
           (null,2,6643,1,6642),
           (null,3,6643,1,6642),
           (null,4,6643,1,6642),
           (null,5,6643,1,6642),
           (null,6,6643,1,6642),
           (null,2,6645,1,6644),
           (null,3,6645,1,6644),
           (null,4,6645,1,6644),
           (null,5,6645,1,6644),
           (null,6,6645,1,6644),
           (null,2,6644,1,6645),
           (null,3,6644,1,6645),
           (null,4,6644,1,6645),
           (null,5,6644,1,6645),
           (null,6,6644,1,6645)
GO

ROLLBACK TRAN