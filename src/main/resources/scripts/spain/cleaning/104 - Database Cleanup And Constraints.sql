-- 2016-12-05 Galen Beck - Adds constraints to tables for orgid and lastModified
-- For all tables where entities extend Versioned and Allocated

USE [spain]
GO

BEGIN TRAN

-- Entities mapped to the following tables extend Versioned, so need a lastModified constraint

ALTER table dbo.accessory ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.accreditationbody ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.accreditationbodyresource ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.address ALTER COLUMN lastModified datetime NOT NULL;

ALTER table dbo.bankaccount ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.businesscompanysettings ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.businesssubdivisionsettings ALTER COLUMN lastModified datetime NOT NULL;

-- On spain-production, migrated calreqs from Morocco had no lastModified.
UPDATE [dbo].[calreq] SET [lastModified] = '2016-12-06' WHERE lastModified is NULL;
GO
ALTER table dbo.calreq ALTER COLUMN lastModified datetime NOT NULL;

ALTER table dbo.company ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.contact ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.customquotationcalibrationcondition ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.baseinstruction ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.instrument ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.jobinstructionlink ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.subdiv ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.users ALTER COLUMN lastModified datetime NOT NULL;

-- Entities mapped to the following tables extend Allocated, so need both lastModified and orgid constraints

ALTER table dbo.bpo ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.bpo ALTER COLUMN orgid int NOT NULL;

-- Fixed bug in InvoicePOServiceImpl.ajaxAddInvoicePO() where orgid not set
UPDATE dbo.invoicepo
   SET dbo.invoicepo.orgid = (SELECT dbo.invoice.orgid FROM invoice where invoice.id = dbo.invoicepo.invid)
 WHERE dbo.invoicepo.orgid is null
GO
ALTER table dbo.invoicepo ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.invoicepo ALTER COLUMN orgid int NOT NULL;

-- Fixed bugs where orgid not set
-- in POServiceImpl:addPOsToJob, insertPOWithDetails 
-- in JobItemPOServiceImpl:insertNewPOAndJIPO
-- in JobItemServiceImpl.createJobItems
UPDATE po
   SET po.orgid = (SELECT subdiv.coid FROM subdiv LEFT JOIN job on subdiv.subdivid = job.orgid WHERE job.jobid = po.jobid)
 WHERE po.orgid is null
GO
ALTER table dbo.po ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.po ALTER COLUMN orgid int NOT NULL;

-- Original Antech data (2 rows) didn't have lastModified set
UPDATE [dbo].[accreditedlab] SET [lastModified] = '2016-12-04' WHERE [lastModified] IS NULL;
GO
ALTER table dbo.accreditedlab ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.accreditedlab ALTER COLUMN orgid int NOT NULL;

ALTER TABLE [dbo].[addresssettingsforallocatedcompany] DROP CONSTRAINT [UK_8gwe7r2icthr4gea7s8ubij6j]
ALTER table dbo.addresssettingsforallocatedcompany ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.addresssettingsforallocatedcompany ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[addresssettingsforallocatedcompany] ADD CONSTRAINT [UK_8gwe7r2icthr4gea7s8ubij6j] UNIQUE NONCLUSTERED 
(
	[orgid] ASC,
	[addressid] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

ALTER TABLE [dbo].[addresssettingsforallocatedsubdiv] DROP CONSTRAINT [UK_5bisbsnj4oykfkwd8078b6ohj]
ALTER table dbo.addresssettingsforallocatedsubdiv ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.addresssettingsforallocatedsubdiv ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[addresssettingsforallocatedsubdiv] ADD  CONSTRAINT [UK_5bisbsnj4oykfkwd8078b6ohj] UNIQUE NONCLUSTERED 
(
	[orgid] ASC,
	[addressid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

ALTER TABLE [dbo].[businessemails] DROP CONSTRAINT [UK_9ixgs744xwp7ywefb5ltvjmyg]
ALTER table dbo.businessemails ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.businessemails ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[businessemails] ADD  CONSTRAINT [UK_9ixgs744xwp7ywefb5ltvjmyg] UNIQUE NONCLUSTERED 
(
	[orgid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

ALTER table dbo.catalogprice ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.catalogprice ALTER COLUMN orgid int NOT NULL;

ALTER TABLE [dbo].[companyaccreditation] DROP CONSTRAINT [UK_inb5f5vw4qduv8n9t77snkeii]
ALTER table dbo.companyaccreditation ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.companyaccreditation ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[companyaccreditation] ADD  CONSTRAINT [UK_inb5f5vw4qduv8n9t77snkeii] UNIQUE NONCLUSTERED 
(
	[coid] ASC,
	[supplierid] ASC,
	[orgid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

-- 3 rows from July didn't have orgid set, presuming bug already fixed
UPDATE [dbo].[companyinstructionlink] SET [orgid] = 6579 WHERE [orgid] IS NULL;
GO
ALTER table dbo.companyinstructionlink ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.companyinstructionlink ALTER COLUMN orgid int NOT NULL;

ALTER TABLE [dbo].[companysettingsforallocatedcompany] DROP CONSTRAINT [UK_jeg07yvjbo8o26dy46h5vxwmx]
ALTER table dbo.companysettingsforallocatedcompany ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.companysettingsforallocatedcompany ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[companysettingsforallocatedcompany] ADD CONSTRAINT [UK_jeg07yvjbo8o26dy46h5vxwmx] UNIQUE NONCLUSTERED 
(
	[orgid] ASC,
	[companyid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

ALTER table dbo.contactinstructionlink ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.contactinstructionlink ALTER COLUMN orgid int NOT NULL;

-- null orgid values have been corrected by script 103, must be run before running this script
ALTER table dbo.defaultquotationcalibrationcondition ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.defaultquotationcalibrationcondition ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.defaultnote ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.defaultnote ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.emailtemplate ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.emailtemplate ALTER COLUMN orgid int NOT NULL;

-- GenericPricingEntity classes

ALTER table dbo.creditnote ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.creditnote ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.hire ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.hire ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.invoice ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.invoice ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.jobcosting ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.jobcosting ALTER COLUMN orgid int NOT NULL;

-- On test database, purchaseorders created from tpquotations where orgid was null!
-- Likely cascading from tpquoterequest issue
UPDATE dbo.purchaseorder
	SET dbo.purchaseorder.orgid = 
		(SELECT dbo.subdiv.coid FROM dbo.subdiv 
		 LEFT JOIN dbo.contact ON dbo.subdiv.subdivid = dbo.contact.subdivid 
		 WHERE dbo.contact.personid = dbo.purchaseorder.buspersonid)
	WHERE dbo.purchaseorder.orgid IS NULL;
GO
ALTER table dbo.purchaseorder ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.purchaseorder ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.quotation ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.quotation ALTER COLUMN orgid int NOT NULL;

-- On test database, 4 quotations from November appear to have null orgid; this will fix.
-- All 3 constructors for tpquotation seem to have org set OK, appears no bug currently
-- Likely cascaded from tpquoterequest not setting orgid
-- TIC 6578, TEM 6579
UPDATE [dbo].[tpquotation] SET [orgid] = 6578 WHERE [qno] like 'ESTIC%' AND [orgid] IS NULL;
GO
UPDATE [dbo].[tpquotation] SET [orgid] = 6579 WHERE [qno] like 'ESTEM%' AND [orgid] IS NULL;
GO
ALTER table dbo.tpquotation ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.tpquotation ALTER COLUMN orgid int NOT NULL;

-- Fixed bugs where orgid not set
-- in TPQuoteRequestFormController:onSubmit
-- and TPQuoteRequestFormController:createAjaxTPQuotationRequest
-- All were null, it seems.
UPDATE [dbo].[tpquoterequest] SET [orgid] = 6578 WHERE [requestno] like 'ESTIC%' AND [orgid] IS NULL;
GO
UPDATE [dbo].[tpquoterequest] SET [orgid] = 6579 WHERE [requestno] like 'ESTEM%' AND [orgid] IS NULL;
GO
ALTER table dbo.tpquoterequest ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.tpquoterequest ALTER COLUMN orgid int NOT NULL;

ALTER TABLE [dbo].[hiremodel] DROP CONSTRAINT [UK_57cojc8xqq9l5goo0rwm14d29]
ALTER table dbo.hiremodel ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.hiremodel ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[hiremodel] ADD  CONSTRAINT [UK_57cojc8xqq9l5goo0rwm14d29] UNIQUE NONCLUSTERED 
(
	[modelid] ASC,
	[orgid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

ALTER table dbo.job ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.job ALTER COLUMN orgid int NOT NULL;

ALTER TABLE [dbo].[numberformat] DROP CONSTRAINT [UK_5n5i3ys5gxqib8ccgb8019x43]
ALTER table dbo.numberformat ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.numberformat ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[numberformat] ADD  CONSTRAINT [UK_5n5i3ys5gxqib8ccgb8019x43] UNIQUE NONCLUSTERED 
(
	[orgid] ASC,
	[numerationType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

-- On test database, a number of blank numeration entries created for PO with no number
-- They can all be deleted.  Check production to see if numbers need reassignment?
DELETE FROM [dbo].[numeration]
      WHERE [orgid] IS null AND [count] = 1 AND numerationType = 'PURCHASE_ORDER'
GO
ALTER table dbo.numeration ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.numeration ALTER COLUMN orgid int NOT NULL;

-- Appears 4 job comments created by Adrian on test database had no orgid set, deleting
DELETE FROM [dbo].[presetcomment] WHERE [orgid] IS NULL AND [setbyid] = 5064 AND type= 'JOB';
GO
ALTER table dbo.presetcomment ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.presetcomment ALTER COLUMN orgid int NOT NULL;

UPDATE [dbo].[procs] SET [lastModified] = '2016-12-04' WHERE [lastModified] IS null;
GO
ALTER table dbo.procs ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.procs ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.quickpurchaseorder ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.quickpurchaseorder ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.customquotationgeneralcondition ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.customquotationgeneralcondition ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.defaultquotationgeneralcondition ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.defaultquotationgeneralcondition ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.quotationrequest ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.quotationrequest ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.scheduledquotationrequest ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.scheduledquotationrequest ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.servicecapability ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.servicecapability ALTER COLUMN orgid int NOT NULL;

-- Appears there is 1 row from bug on June 29 with orgid null, fixing
UPDATE [dbo].[subdivinstructionlink] SET [orgid] = 6579 WHERE [orgid] IS NULL AND id=765;
GO
ALTER table dbo.subdivinstructionlink ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.subdivinstructionlink ALTER COLUMN orgid int NOT NULL;

ALTER table dbo.supplierinvoice ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.supplierinvoice ALTER COLUMN orgid int NOT NULL;

-- 4 system defaults all for Madrid customers not set
UPDATE [dbo].[systemdefaultapplication] SET [orgid] = 6579 WHERE [orgid] is NULL;
GO

-- Existing constraint just exists for Spain-test
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME='UK_ju8nl8nfqp1yo2njus7wjcx9p')
BEGIN
	ALTER TABLE [dbo].[systemdefaultapplication] DROP CONSTRAINT [UK_ju8nl8nfqp1yo2njus7wjcx9p];
END
ALTER table dbo.systemdefaultapplication ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.systemdefaultapplication ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[systemdefaultapplication] ADD  CONSTRAINT [UK_ju8nl8nfqp1yo2njus7wjcx9p] UNIQUE NONCLUSTERED 
(
	[personid] ASC,
	[subdivid] ASC,
	[coid] ASC,
	[defaultid] ASC,
	[orgid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

ALTER table dbo.upcomingwork ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.upcomingwork ALTER COLUMN orgid int NOT NULL;

ALTER TABLE [dbo].[user_role] DROP CONSTRAINT [UK_brbeyx506w3la5nk6f1eqa470]
ALTER table dbo.user_role ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.user_role ALTER COLUMN orgid int NOT NULL;
ALTER TABLE [dbo].[user_role] ADD  CONSTRAINT [UK_brbeyx506w3la5nk6f1eqa470] UNIQUE NONCLUSTERED 
(
	[rolename] ASC,
	[username] ASC,
	[orgid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

ALTER table dbo.workinstruction ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.workinstruction ALTER COLUMN orgid int NOT NULL;

COMMIT TRAN