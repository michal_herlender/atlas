USE [atlas]
GO

BEGIN TRAN

INSERT INTO dbo.permission(groupId, permission)
VALUES (1 ,'JI_GENERAL_SERVICE_OPERATION'),
	   (1 ,'JI_REPAIR');

INSERT INTO dbversion(version) VALUES(720);

GO

COMMIT TRAN