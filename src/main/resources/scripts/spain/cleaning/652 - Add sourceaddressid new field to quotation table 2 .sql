USE [atlas]
GO

BEGIN TRAN

		-- Remove the sourceaddressid column created during the execution of the script 650
		ALTER TABLE dbo.quotation
		DROP COLUMN IF EXISTS sourceaddressid;
		GO
		
		ALTER TABLE dbo.quotation ADD sourceaddressid INT;
		GO

		UPDATE dbo.quotation 
		SET sourceaddressid = (SELECT addrid FROM dbo.address address
		INNER JOIN dbo.contact con on address.addrid = con.defaddress
		INNER JOIN dbo.subdiv subdiv on con.subdivid = subdiv.subdivid 
		INNER JOIN dbo.company comp on subdiv.coid = comp.coid 
		WHERE quotation.sourcedby = con.personid AND quotation.orgid = comp.coid)
		WHERE sourceaddressid IS NULL 
		GO

		UPDATE dbo.quotation 
		SET sourceaddressid = (SELECT addrid FROM dbo.address address
		INNER JOIN dbo.subdiv subdiv on address.addrid = subdiv.defaddrid
		INNER JOIN dbo.company comp on subdiv.subdivid = comp.defsubdivid 
		WHERE quotation.orgid = comp.coid)
		WHERE sourceaddressid IS NULL
		GO


		ALTER TABLE dbo.quotation ALTER COLUMN sourceaddressid INT NOT NULL;
		GO

		INSERT INTO dbversion(version) VALUES(652);

COMMIT TRAN