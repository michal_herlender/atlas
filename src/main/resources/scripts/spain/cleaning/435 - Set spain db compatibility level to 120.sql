-- Changes the database compatibility level to 120 (SQL Server 2014) per Philipper Redor's suggestion
-- Previously 100 (Matching original SQL Server 2008 in Zaragoza)
-- See https://docs.microsoft.com/en-us/sql/database-engine/breaking-changes-to-database-engine-features-in-sql-server-2016?view=sql-server-2014 for list of changes
-- Galen Beck - 2019-05-03

ALTER DATABASE [spain]  
SET COMPATIBILITY_LEVEL = 120;  
GO  

USE [spain]

INSERT INTO [dbversion] (version) VALUES (435);

GO