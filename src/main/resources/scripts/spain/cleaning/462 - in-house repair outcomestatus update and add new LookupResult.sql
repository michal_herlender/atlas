USE [spain];

BEGIN TRAN

	DECLARE @aw_rir int;
	DECLARE @oc int;
	
	SELECT @aw_rir = stateid FROM dbo.itemstate WHERE [description] = 'Item requires repair – awaiting costing to client';
	
	INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,lookupid,statusid)
	VALUES (0,NULL,NULL,@aw_rir);
	
	SELECT TOP 1 @oc = id FROM dbo.outcomestatus WHERE statusid = @aw_rir ORDER BY id DESC;
	
	INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (105,@oc,2);
	
	UPDATE dbo.outcomestatus SET statusid = NULL, lookupid = 90 WHERE id IN (27,29,30,32,308,309);

	INSERT INTO dbversion (version) VALUES (462);

COMMIT TRAN