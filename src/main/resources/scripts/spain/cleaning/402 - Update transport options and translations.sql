USE [spain]
GO

BEGIN TRAN

ALTER TABLE [dbo].[transportmethod] DROP CONSTRAINT [FK113054A202933D5]
GO

ALTER TABLE [dbo].[transportmethod] DROP COLUMN log_lastmodified;
ALTER TABLE [dbo].[transportmethod] DROP COLUMN log_userid;

GO

SET IDENTITY_INSERT [dbo].[transportmethod] ON
GO

INSERT INTO [dbo].[transportmethod]
           ([id]
		   ,[method]
           ,[optionperday]
           ,[optionpersubdiv]
           ,[lastModified])
     VALUES
           (7
		   ,'Other'
           ,0
           ,0
           ,'2019-02-14'),
		   (8
		   ,'Trescal Shuttle'
           ,0
           ,0
           ,'2019-02-14')
GO

SET IDENTITY_INSERT [dbo].[transportmethod] OFF
GO

UPDATE [dbo].[transportmethod] 
 SET [method] = 'Trescal Shuttle'
 WHERE id='1'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Cami�n de Trescal'
 WHERE id='1' and locale='ES_es'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Trescal Shuttle'
 WHERE id='1' and locale='en_GB'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Navette Trescal'
 WHERE id='1' and locale='fr_FR'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Client'
 WHERE id='2' and locale='fr_FR'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Transporteur client'
 WHERE id='3' and locale='fr_FR'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Sans'
 WHERE id='4' and locale='fr_FR'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Transporteur urgent'
 WHERE id='5' and locale='fr_FR'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Transporteur standard'
 WHERE id='6' and locale='fr_FR'
GO

INSERT INTO [dbo].[transportmethodtranslation]
           ([id],[locale],[translation])
     VALUES
           (7,'en_GB','Trescal Shuttle'),
           (7,'fr_FR','Navette Trescal'),
           (7,'de_DE','Shuttle'),
           (7,'es_ES','Cami�n de Trescal'),
           (8,'en_GB','Other'),
           (8,'fr_FR','Transport sp�cifique'),
           (8,'de_DE','Andere'),
           (8,'es_ES','Otros')
GO

INSERT INTO dbversion(version) VALUES(402);
GO

COMMIT TRAN