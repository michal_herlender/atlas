-- Update templateNames in ImportQuotationItemsUsingInstrumentOrInstrumentModel exchange format.
USE [atlas]
GO

BEGIN TRAN

DECLARE @lastexchangeFormatid INT;

SET @lastexchangeFormatid = (SELECT id FROM dbo.exchangeformat WHERE name LIKE 'ImportQuotationItemsUsingInstrumentOrInstrumentModel');

UPDATE dbo.exchangeformatfieldnamedetails 
SET templateName = 'Public note' 
WHERE exchangeFormatid = @lastexchangeFormatid AND fieldName LIKE 'PUBLIC_NOTE';

UPDATE dbo.exchangeformatfieldnamedetails 
SET templateName = 'Private note' 
WHERE exchangeFormatid = @lastexchangeFormatid AND fieldName LIKE 'PRIVATE_NOTE';

INSERT INTO dbversion(version) VALUES(670);

COMMIT TRAN
