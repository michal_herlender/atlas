BEGIN TRAN

USE [spain];
GO

-- Initialize parameters
DECLARE @Date DATETIME = GETDATE()
DECLARE @UserID INT 

-- Gets the CWMS Auto User
SELECT @UserID = personid FROM dbo.contact WITH (NOLOCK) WHERE firstname = 'CWMS Auto User'

DELETE FROM dbo.defaultquotationgeneralcondition
WHERE orgid IN (
	SELECT dbo.company.coid FROM dbo.company WHERE companycode IN ('TEM','TIC')
)

SET IDENTITY_INSERT dbo.defaultquotationgeneralcondition ON

IF EXISTS (SELECT 1 FROM dbo.defaultquotationgeneralcondition)
BEGIN
	INSERT INTO dbo.defaultquotationgeneralcondition(defgenconid,conditiontext,lastModified,log_userid,orgid)
	VALUES ((SELECT MAX(defgenconid) + 1 FROM dbo.defaultquotationgeneralcondition WITH (NOLOCK)),'',@Date,@UserID,(SELECT dbo.company.coid FROM dbo.company WHERE companycode = 'TEM'))
END
ELSE
BEGIN
	INSERT INTO dbo.defaultquotationgeneralcondition(defgenconid,conditiontext,lastModified,log_userid,orgid)
	VALUES (1,'<a href="http://www.itm.es/Docs/010401.pdf">http://www.itm.es/Docs/010401.pdf</a>',@Date,@UserID,(SELECT dbo.company.coid FROM dbo.company WHERE companycode = 'TEM'))
END

INSERT INTO dbo.defaultquotationgeneralcondition(defgenconid,conditiontext,lastModified,log_userid,orgid)
VALUES ((SELECT MAX(defgenconid) + 1 FROM dbo.defaultquotationgeneralcondition WITH (NOLOCK)),'<a href="http://www.itm.es/Docs/010401.pdf">http://www.itm.es/Docs/010401.pdf</a>',@Date,@UserID,(SELECT dbo.company.coid FROM dbo.company WHERE companycode = 'TIC'))

SET IDENTITY_INSERT dbo.defaultquotationgeneralcondition OFF

ROLLBACK TRAN