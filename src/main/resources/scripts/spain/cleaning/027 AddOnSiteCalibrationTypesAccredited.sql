insert into calibrationtype 
			(accreditationspecific,
			active,
			certnoresetyearly,
			firstpagepapertype,
			labeltemplatepath,
			orderby,
			pdfcontinuationwatermark,
			pdfheaderwatermark,
			recalldateonlabel,
			restpagepapertype,
			servicetypeid,
			orgid)

select accreditationspecific,
	   1,
	   certnoresetyearly,
	   firstpagepapertype,
	   labeltemplatepath,
	   orderby + 10,
	   pdfcontinuationwatermark,
	   pdfheaderwatermark,
	   recalldateonlabel,
	   restpagepapertype,
	   (select servicetypeid from servicetype s where s.longname = 'On-Site Accredited Calibration') as 'servicetypeid',
	   orgid
	    

from calibrationtype 
left join servicetype on servicetype.servicetypeid = calibrationtype.servicetypeid
where servicetype.longname = 'In-House Accredited Calibration';