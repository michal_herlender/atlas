Use atlas
Go

BEGIN TRAN

	DECLARE @item_desp_tp_activity INT,
		@tp_requirement_lookup INT,
		@destination_receipt_tp_lookup INT,
		@no_return_outcome INT,
		@new_tp_receipt_activity INT,
		@new_tp_receipt_status INT,
		@new_tp_receipt_outcome INT;

	SELECT @item_desp_tp_activity = stateid FROM dbo.itemstate WHERE [description] = 'Item despatched to third party';
	SELECT @tp_requirement_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_TPRequirements (third party after item despatched)';
	-- update itemstate.lookupid
	UPDATE dbo.itemstate SET lookupid = @tp_requirement_lookup WHERE stateid = @item_desp_tp_activity;
	
	-- update the NO_RETURN outcomestatus
	SELECT @destination_receipt_tp_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_ConfirmationDestinationReceiptRequired (after despatching to thirdparty)';
	SELECT @no_return_outcome = outcomestatusid FROM dbo.lookupresult WHERE resultmessage = 7 and lookupid = @tp_requirement_lookup;
	UPDATE dbo.outcomestatus SET lookupid = @destination_receipt_tp_lookup, statusid = NULL WHERE id = @no_return_outcome;
	
	-- add new status 
	INSERT INTO dbo.itemstate([type],retired,active,[description],lookupid,actionoutcometype)
	VALUES ('workstatus',0,1,'Awaiting confirmation of third party destination receipt; no return expected',NULL,NULL);
	SELECT @new_tp_receipt_status = MAX(stateid) FROM dbo.itemstate;
	INSERT INTO dbo.itemstatetranslation(stateid,locale,translation)
	VALUES (@new_tp_receipt_status,'en_GB','Awaiting confirmation of third party destination receipt; no return expected'),
			(@new_tp_receipt_status,'fr_FR','En attente de confirmation de la r�ception de destination par un tiers; aucun retour attendu'),
			(@new_tp_receipt_status,'es_ES','En espera de la confirmaci�n del recibo de destino de un tercero; no se espera retorno'),
			(@new_tp_receipt_status,'de_DE','Warten auf Best�tigung des Empfangs des Ziels eines Dritten; Keine R�ckkehr erwartet');
	
	-- add new_tp_receipt_status to the "AWAITING_CLIENT_RECEIPT_CONFIRMATION" group
	INSERT INTO dbo.stategrouplink(groupid,stateid,[type])
	VALUES (76, @new_tp_receipt_status, 'ALLOCATED_SUBDIV');
	
	-- add new outcomestatus for the new status
	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
	VALUES (0,NULL,NULL,@new_tp_receipt_status);
	SELECT @new_tp_receipt_outcome = MAX(id) FROM dbo.outcomestatus;
	-- link the lookupresult to new outcomestatus
	DECLARE @client_receipt_status INT,
			@client_receipt_yes_outcome INT,
			@client_receipt_no_outcome INT,
			@receipt_hook INT;
	SELECT @client_receipt_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting confirmation of destination receipt';
	SELECT @client_receipt_yes_outcome = id FROM dbo.outcomestatus WHERE activityid = @item_desp_tp_activity AND statusid = @client_receipt_status;
	UPDATE dbo.lookupresult SET outcomestatusid = @new_tp_receipt_outcome WHERE outcomestatusid = @client_receipt_yes_outcome;
	DELETE FROM dbo.outcomestatus WHERE id = @client_receipt_yes_outcome;
	
	-- add new activity
	INSERT INTO dbo.itemstate([type],retired,active,[description],lookupid,actionoutcometype)
	VALUES ('workactivity',0,1,'Third party destination receipt confirmed; no return expected',NULL,NULL);
	SELECT @new_tp_receipt_activity = MAX(stateid) FROM dbo.itemstate;
	INSERT INTO dbo.itemstatetranslation(stateid,locale,translation)
	VALUES (@new_tp_receipt_activity,'en_GB','Third party destination receipt confirmed; no return expected'),
			(@new_tp_receipt_activity,'fr_FR','R�ception de destination du tiers confirm�e; aucun retour attendu'),
			(@new_tp_receipt_activity,'es_ES','Recibo de destino de terceros confirmado; no se espera retorno'),
			(@new_tp_receipt_activity,'de_DE','Empfangsbest�tigung eines Drittanbieters best�tigt; Keine R�ckkehr erwartet');
	
	-- add new nextactivity (new status -> new activity)
	INSERT INTO dbo.nextactivity(manualentryallowed,statusid,activityid)
	VALUES (0,@new_tp_receipt_status,@new_tp_receipt_activity)
	
	-- add new hookactivity (new status -> new activity)
	SELECT @receipt_hook = id FROM dbo.hook WHERE [name] = 'destination-receipt-confirmed';
	INSERT INTO dbo.hookactivity(hookid,beginactivity,completeactivity,stateid,activityid)
	VALUES (@receipt_hook,1,1,@new_tp_receipt_status,@new_tp_receipt_activity);
	
	DECLARE @delivery_type_lookup INT,
			@new_after_tp_receipt_outcome INT,
			@tp_service_completed_status INT,
			@new_client_receipt_no_outcome INT;
	SELECT @delivery_type_lookup = lookupid FROM dbo.itemstate WHERE [description] = 'Destination receipt confirmed';
	UPDATE dbo.itemstate SET lookupid = NULL WHERE lookupid = @delivery_type_lookup;
	DELETE FROM dbo.lookupresult WHERE lookupid = @delivery_type_lookup;
	DELETE FROM dbo.lookupinstance WHERE id = @delivery_type_lookup;
	SELECT @tp_service_completed_status = stateid FROM dbo.itemstate WHERE [description] = 'Job item transferred to third party, service completed';
	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
	VALUES (0,@new_tp_receipt_activity,NULL,@tp_service_completed_status);
	
	SELECT @client_receipt_no_outcome = id FROM dbo.outcomestatus WHERE activityid = @item_desp_tp_activity AND lookupid = @tp_requirement_lookup;
	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
	VALUES (0,NULL,NULL,@tp_service_completed_status);
	SELECT @new_client_receipt_no_outcome = MAX(id) FROM dbo.outcomestatus;
	UPDATE dbo.lookupresult SET outcomestatusid = @new_client_receipt_no_outcome WHERE outcomestatusid = @client_receipt_no_outcome;
	DELETE FROM dbo.outcomestatus WHERE id = @client_receipt_yes_outcome;
	
	--Update the activity, status
	UPDATE dbo.itemstate SET [description] = 'Awaiting confirmation of client receipt' WHERE stateid = @client_receipt_status;
	UPDATE dbo.itemstatetranslation SET translation = 'Awaiting confirmation of client receipt' WHERE stateid = @client_receipt_status AND locale = 'en_GB';
	UPDATE dbo.itemstatetranslation SET translation = 'En attente de confirmation de la r�ception du client' WHERE stateid = @client_receipt_status AND locale = 'fr_FR';
	UPDATE dbo.itemstatetranslation SET translation = 'Esperando la confirmaci�n del recibo del cliente' WHERE stateid = @client_receipt_status AND locale = 'es_ES';
	UPDATE dbo.itemstatetranslation SET translation = 'Warten auf Best�tigung des Kundenempfangs' WHERE stateid = @client_receipt_status AND locale = 'de_DE';
	
	DECLARE @client_receipt_activity INT;
	SELECT @client_receipt_activity = stateid FROM dbo.itemstate WHERE [description] = 'Destination receipt confirmed';
	UPDATE dbo.itemstate SET [description] = 'Client receipt confirmed' WHERE stateid = @client_receipt_activity;
	UPDATE dbo.itemstatetranslation SET translation = 'Client receipt confirmed' WHERE stateid = @client_receipt_activity AND locale = 'en_GB';
	UPDATE dbo.itemstatetranslation SET translation = 'Re�u client confirm�' WHERE stateid = @client_receipt_activity AND locale = 'fr_FR';
	UPDATE dbo.itemstatetranslation SET translation = 'Recibo del cliente confirmado' WHERE stateid = @client_receipt_activity AND locale = 'es_ES';
	UPDATE dbo.itemstatetranslation SET translation = 'Kundenbeleg best�tigt' WHERE stateid = @client_receipt_activity AND locale = 'de_DE';
	
	--add non existing NextActivity for client receipt
	INSERT INTO dbo.nextactivity(manualentryallowed,statusid,activityid)
	VALUES (0,@client_receipt_status,@client_receipt_activity);

	INSERT INTO dbversion VALUES (802);

COMMIT TRAN