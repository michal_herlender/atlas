-- Create a Global Exchange format usable to import quotation items with the type QUOTATION_ITEMS_IMPORTATION
USE [atlas]
GO

BEGIN TRAN

DECLARE @lastexchangeFormatid int

INSERT INTO dbo.exchangeformat(name, description, exchangeFormatLevel, dateFormat, lastModified, lastModifiedBy, eftype)
VALUES ('ImportQuotationItems' ,'Exchange format usable to import quotation items','GLOBAL','DD_MM_YYYY', '2019-12-03 18:00:00', 17280, 'QUOTATION_ITEMS_IMPORTATION' );
SELECT @lastexchangeFormatid = MAX(id) FROM dbo.exchangeformat

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES  (1, 0, 'plantId', 'ID_TRESCAL', @lastexchangeFormatid),
		(2, 1, 'plantNo', 'PLANT_NO', @lastexchangeFormatid),
		(3, 1, 'serviceType', 'EXPECTED_SERVICE', @lastexchangeFormatid),
		(4, 1, 'catalogPrice', 'CATALOG_PRICE', @lastexchangeFormatid),
		(5, 1, 'discountRate', 'DISCOUNT_RATE', @lastexchangeFormatid),
		(6, 1, 'finalPrice', 'FINAL_PRICE', @lastexchangeFormatid),
		(7, 0, 'publicNote', 'PUBLIC_NOTE', @lastexchangeFormatid),
		(8, 0, 'privatenote', 'PRIVATE_NOTE', @lastexchangeFormatid);


INSERT INTO dbversion(version) VALUES(540);

COMMIT TRAN

