BEGIN TRAN;

USE spain;

ALTER TABLE dbo.certificate
  ADD certclass VARCHAR(255);

CREATE TABLE dbversion (id INTEGER NOT NULL IDENTITY PRIMARY KEY , version INTEGER);

INSERT INTO dbversion (version) VALUES (192);

COMMIT TRAN;