USE [Atlas]
GO

BEGIN TRAN

SET IDENTITY_INSERT [instrumentusagetype] ON

INSERT INTO [dbo].[instrumentusagetype]
           ([typeid]
		   ,[defaulttype]
           ,[fulldescription]
           ,[name]
           ,[recall]
	   , [instrumentstatus])
     VALUES
           (24
		,''
           ,''
           ,'Calibrated and not used'
           ,0
	   , 0)
GO

SET IDENTITY_INSERT [instrumentusagetype] OFF

INSERT INTO [dbo].[instrumentusagetypenametranslation]
           ([typeid]
           ,[locale]
           ,[translation])
     VALUES
           (24
           ,'en_GB'
           ,'Calibrated and not used'),
           (24
           ,'fr_FR'
           ,'V�rifi� et non utilis�'),
           (24
           ,'es_ES'
           ,'Calibrado y no utilizado'),
           (24
           ,'en_US'
           ,'Calibrated and not used'),
           (24
           ,'pt_PT'
           ,'Calibrado e n�o utilizado')
GO

/* ??*/
insert into dbversion(version) values(873);

COMMIT TRAN