
USE [atlas];

GO

BEGIN TRAN

ALTER TABLE dbo.company ALTER COLUMN documentlanguage VARCHAR(255) NULL;

INSERT INTO dbversion(version) VALUES(582);

COMMIT TRAN