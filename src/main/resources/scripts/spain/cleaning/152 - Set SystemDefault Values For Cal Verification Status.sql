-- Author Galen Beck 2017-05-29
-- Inserts SystemDefault values for cal verification status for selected customers

USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[systemdefaultapplication]
           ([defaultvalue]
           ,[coid]
           ,[defaultid]
           ,[lastModified]
           ,[orgid])
     VALUES
			('true',12662,46,'2017-05-29',6578),
			('true',9448,46,'2017-05-29',6578),
			('true',9661,46,'2017-05-29',6578),
			('true',9624,46,'2017-05-29',6578),
			('true',11499,46,'2017-05-29',6578),
			('true',9646,46,'2017-05-29',6578),
			('true',9616,46,'2017-05-29',6578),
			('true',8209,46,'2017-05-29',6578),
			('true',9648,46,'2017-05-29',6578),
			('true',8914,46,'2017-05-29',6578),
			('true',9759,46,'2017-05-29',6578),
			('true',10221,46,'2017-05-29',6578),
			('true',9628,46,'2017-05-29',6578),
			('true',9617,46,'2017-05-29',6578),
			('true',8334,46,'2017-05-29',6578),
			('true',8949,46,'2017-05-29',6578),
			('true',8683,46,'2017-05-29',6578),
			('true',9192,46,'2017-05-29',6578);
GO

INSERT INTO [dbo].[systemdefaultapplication]
           ([defaultvalue]
           ,[coid]
           ,[defaultid]
           ,[lastModified]
           ,[orgid])
     VALUES
			('true',9670,46,'2017-05-29',6579),
			('true',9674,46,'2017-05-29',6579),
			('true',10188,46,'2017-05-29',6579),
			('true',9491,46,'2017-05-29',6579),
			('true',9485,46,'2017-05-29',6579);
GO

COMMIT TRAN