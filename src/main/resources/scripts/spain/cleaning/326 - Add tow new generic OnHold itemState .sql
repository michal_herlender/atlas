-- Script 326 - add two new On-Hold ItemStatus

USE [spain]

BEGIN TRAN

INSERT INTO dbo.itemstate ([type],active,description,retired)
VALUES ('holdstatus',1,'On-Hold awaiting in logistic for technical issue',0);

DECLARE @lastItemStateId int;
SELECT @lastItemStateId = MAX(stateid) from dbo.itemstate;

INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
VALUES (@lastItemStateId,'en_GB','On-Hold awaiting in logistic for technical issue');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
VALUES (@lastItemStateId,'de_DE','On-Hold wartet in der Logistik auf technisches Problem');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
VALUES (@lastItemStateId,'es_ES','On-Hold esperando en logística por problema técnico');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
VALUES (@lastItemStateId,'fr_FR','En-attente en attente de logistique pour problème technique');

INSERT INTO dbo.itemstate ([type],active,description,retired)
VALUES ('holdstatus',1,'On-Hold awaiting in logistic for administrative issue',0);

SELECT @lastItemStateId = MAX(stateid) from dbo.itemstate;

INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
VALUES (@lastItemStateId,'en_GB','On-Hold awaiting in logistic for administrative issue');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
VALUES (@lastItemStateId,'de_DE','On-Hold wartet in der Logistik auf administrative Probleme');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
VALUES (@lastItemStateId,'es_ES','On-Hold awaiting in logistic for administrative issue');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
VALUES (@lastItemStateId,'fr_FR','En-attente dans la logistique pour la question administrative');

INSERT INTO dbversion(version) VALUES(326);

COMMIT TRAN