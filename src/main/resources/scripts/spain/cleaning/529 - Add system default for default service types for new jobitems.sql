USE [atlas]
GO

BEGIN TRAN

DECLARE @groupid int;
DECLARE @defaultid1 int;
DECLARE @defaultid2 int;

-- add a new group for the new system default
INSERT INTO dbo.systemdefaultgroup (description,dname)
	VALUES ('Default service type of new job items','Business - Default service type');
select @groupid = max(g.id) from dbo.systemdefaultgroup g
	

--translation for system default description
INSERT INTO dbo.systemdefaultgroupdescriptiontranslation (id, locale,[translation])
	VALUES (@groupid, 'en_GB','Default service type for new job items');
INSERT INTO dbo.systemdefaultgroupdescriptiontranslation (id, locale,[translation])
	VALUES (@groupid, 'fr_FR','Service par d�faut pour les nouveaux Jobitems');
INSERT INTO dbo.systemdefaultgroupdescriptiontranslation (id, locale,[translation])
	VALUES (@groupid, 'es_ES','Tipo de servicio predeterminado para nuevos elementos de trabajo');
	
--translation for system default name
INSERT INTO dbo.systemdefaultgroupnametranslation (id, locale,[translation])
	VALUES (@groupid, 'en_GB','Business - Default service type');
INSERT INTO dbo.systemdefaultgroupnametranslation (id, locale,[translation])
	VALUES (@groupid, 'fr_FR','Entreprise - Type de service par d�faut');
INSERT INTO dbo.systemdefaultgroupnametranslation (id, locale,[translation])
	VALUES (@groupid, 'es_ES','Empresa - tipo de servicio predeterminado');

-- add system default for default service types for standard jobs
INSERT INTO dbo.systemdefault (defaultdatatype,defaultdescription,defaultname,defaultvalue,groupid,groupwide)
	VALUES ('other','Defines the default service type of new job items for standard jobs','Default service type for new standard job items','TC-SOC-IL',@groupid,1);
select @defaultid1 = max(sd.defaultid) from dbo.systemdefault sd

-- add system default for default service types for onsite jobs
INSERT INTO dbo.systemdefault (defaultdatatype,defaultdescription,defaultname,defaultvalue,groupid,groupwide)
	VALUES ('other','Defines the default service type of new job items for onsite jobs','Default service type for new onsite job items','AC-MVO-OS',@groupid,1);
select @defaultid2 = max(sd.defaultid) from dbo.systemdefault sd


--name translation
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'en_GB','Default service type of new job items for standard jobs');
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'fr_FR','Type de service par d�faut des nouveaux Jobitems pour les Jobs standard');
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'es_ES','Tipo de servicio predeterminado de nuevos trabajos para trabajos est�ndar');

INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'en_GB','Default service type of new job items for onsite jobs');
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'fr_FR','Service par d�faut des nouveaux Jobitems pour les Jobs sur site');
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'en_GB','Tipo de servicio predeterminado de nuevos elementos de trabajo para trabajos en el sitio');

-- description translation
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'en_GB','Defines the default service type of new job items for standard jobs');
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'fr_FR','D�finit le type de service par d�faut des nouveaux Jobitems pour les Jobs standard.');
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'es_ES','Define el tipo de servicio predeterminado de nuevos elementos de trabajo para trabajos est�ndar');

INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'en_GB','Defines the default service type of new job items for onsite jobs');
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'fr_FR','D�finit le type de service par d�faut des nouveaux Jobitems pour les Jobs sur site');
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'es_ES','Define el tipo de servicio predeterminado de nuevos elementos de trabajo para trabajos en el sitio');

--scopes
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (0, @defaultid1);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (2, @defaultid1);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (0, @defaultid2);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (2, @defaultid2);

--company roles
INSERT INTO dbo.systemdefaultcorole (coroleid,defaultid)
	VALUES (5, @defaultid1);
INSERT INTO dbo.systemdefaultcorole (coroleid,defaultid)
	VALUES (5, @defaultid2);

INSERT INTO dbversion(version) VALUES(529);

COMMIT TRAN
