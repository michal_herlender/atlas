-- Consolidates email templates for France
-- (a) Deletes all email templates for Trescal SAS (France) except for Vendome
-- (b) Make the Vendome email templates company-wide

USE [atlas]
GO

BEGIN TRAN

DELETE FROM [dbo].[emailtemplate]
      WHERE orgid = 6690 and subdivid <> 6976
GO

UPDATE [dbo].[emailtemplate]
   SET [subdivid] = null
 WHERE orgid = 6690 and subdivid = 6976
GO

INSERT INTO dbversion(version) VALUES(557);

COMMIT TRAN