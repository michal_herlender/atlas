USE [spain]

BEGIN TRAN

	ALTER TABLE dbo.exchangeformat ADD eftype varchar(100) NOT NULL default('PREBOOKING')
	go
	
	INSERT INTO dbversion(version) VALUES(385);

COMMIT TRAN