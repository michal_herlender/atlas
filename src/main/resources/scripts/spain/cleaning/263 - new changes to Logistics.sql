USE [spain]
GO

BEGIN TRAN

	ALTER TABLE dbo.exchangeformat ADD linestoskip int DEFAULT 0
	go
	
	ALTER TABLE dbo.exchangeformat ADD sheetname varchar(100) 
	go

	ALTER TABLE dbo.asn ADD jobid int
	go
	ALTER TABLE dbo.asn ADD CONSTRAINT FK_asn_jobid FOREIGN KEY (jobid) REFERENCES dbo.job(jobid)
	go


COMMIT TRAN