use [atlas]

begin tran

-- Moved CDC script to utility script U029 as it is now maintained there
-- We don't need to run CDC on most test and development machines (and it is not possible on some of them)
-- so all changes to CDC functiona;ity should be put into U029 which can be rereun as necessary
-- If this script has already been run, it's OK too.

insert into dbversion(version) values(794);

commit tran