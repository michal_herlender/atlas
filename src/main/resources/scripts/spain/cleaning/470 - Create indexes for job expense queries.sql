-- Creates statistics to speed up invoicing expense query
-- As recommended by database engine tuning advisor

USE [spain]
GO

BEGIN TRAN

CREATE NONCLUSTERED INDEX [_dta_index_invoiceitem_8_1700201107__K24] ON [dbo].[invoiceitem]
(
	[expenseitem] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_job_8_464720708__K27_K1_K14] ON [dbo].[job]
(
	[orgid] ASC,
	[jobid] ASC,
	[personid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_464720708_14_27] ON [dbo].[job]([personid], [orgid])
go

SET ANSI_PADDING ON

go

CREATE NONCLUSTERED INDEX [_dta_index_company_8_1682105033__K1_5_19] ON [dbo].[company]
(
	[coid] ASC
)
INCLUDE ( 	[coname],
	[corole]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_subdiv_8_1419868125__K10_K1] ON [dbo].[subdiv]
(
	[coid] ASC,
	[subdivid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE NONCLUSTERED INDEX [_dta_index_jobexpenseitem_8_1117647410__K11_K8_K1] ON [dbo].[jobexpenseitem]
(
	[invoiceable] ASC,
	[job] ASC,
	[id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
go

CREATE STATISTICS [_dta_stat_1117647410_8_11] ON [dbo].[jobexpenseitem]([job], [invoiceable])
go

CREATE STATISTICS [_dta_stat_1117647410_8_1] ON [dbo].[jobexpenseitem]([job], [id])
go

CREATE STATISTICS [_dta_stat_1117647410_11_1_8] ON [dbo].[jobexpenseitem]([invoiceable], [id], [job])
go

INSERT INTO dbversion(version) VALUES(470);

COMMIT TRAN