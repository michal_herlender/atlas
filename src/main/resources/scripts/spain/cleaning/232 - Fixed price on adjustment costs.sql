USE spain;

BEGIN TRAN

	ALTER TABLE dbo.costs_adjustment ADD fixedPrice NUMERIC(19,2);
	
	GO
	
	UPDATE dbo.costs_adjustment SET fixedPrice = 0;
	
	INSERT INTO dbversion VALUES (232);

COMMIT TRAN