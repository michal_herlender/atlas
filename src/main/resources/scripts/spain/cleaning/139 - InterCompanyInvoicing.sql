IF NOT EXISTS (SELECT 1 FROM invoicetype WHERE name = 'Internal')
	INSERT INTO invoicetype(addtoaccount, deftosinglecost, includeonjobsheet, name)
	VALUES (0, 0, 0, 'Internal')

IF NOT EXISTS (SELECT 1 FROM itemstate WHERE description = 'Internal invoice item created')
	INSERT INTO itemstate (active, description, retired, type)
	VALUES (1, 'Internal invoice item created', 0, 'workactivity')

DECLARE @activityId int;

SET @activityId = (SELECT stateid FROM itemstate WHERE description = 'Internal invoice item created')

IF NOT EXISTS (SELECT 1 FROM itemstatetranslation WHERE stateid = @activityId)
	INSERT INTO itemstatetranslation (locale, stateid, translation)
	VALUES ('en_GB', @activityId, 'Internal invoice item created')

IF NOT EXISTS (SELECT 1 FROM hook WHERE name = 'create-internal-invoice-item')
	INSERT INTO hook(active, activityid, alwayspossible, beginactivity, completeactivity, name)
	VALUES (1, @activityId, 0, 1, 1, 'create-internal-invoice-item')
ELSE
	UPDATE hook SET activityid = @activityId
	WHERE name = 'create-internal-invoice-item'

INSERT INTO invoicestatus(approved, defaultstatus, description, issued, name, nextid, oninsert, onissue, requiresattention, potype)
VALUES (0, 0, 'Awaiting completion of internal invoice', 0, 'Internal invoice', 4, 0, 0, 1, 'STATUS')