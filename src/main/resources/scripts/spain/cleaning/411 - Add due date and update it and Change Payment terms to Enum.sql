-- 2019-03-12 GB -- Renumbered this script from 409 to 411, as we have already run the old 409 (payment terms - different entity!) 
-- on production and test, before it was deleted in the merge that added this script.
USE [spain]
GO

BEGIN TRAN
	
	-- Adding Duedate and paymentterms 
	ALTER TABLE spain.dbo.invoice ADD duedate datetime
	ALTER TABLE spain.dbo.invoice ADD paymentterms VARCHAR (10)
	ALTER TABLE spain.dbo.companysettingsforallocatedcompany ADD paymentterms VARCHAR (10)
	ALTER TABLE spain.dbo.supplierinvoice ADD paymentterms VARCHAR (10)
	ALTER TABLE spain.dbo.contract ADD paymentterms VARCHAR (10)
    go
    
	-- Updating paymentterm enum in invoice Table
	DECLARE @id int
	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30')
	update spain.dbo.invoice set paymentterms='P30' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM')
	update spain.dbo.invoice set paymentterms='P30EOM' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='EOM30')
	update spain.dbo.invoice set paymentterms='PEOM30' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM10')
	update spain.dbo.invoice set paymentterms='P30EOM10' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='45')
	update spain.dbo.invoice set paymentterms='P45' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='45EOM')
	update spain.dbo.invoice set paymentterms='P45EOM' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='EOM45')
	update spain.dbo.invoice set paymentterms='PEOM45' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='60')
	update spain.dbo.invoice set paymentterms='P60' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='85')
	update spain.dbo.invoice set paymentterms='P85' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='90')
	update spain.dbo.invoice set paymentterms='P90' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='120')
	update spain.dbo.invoice set paymentterms='P120' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='150')
	update spain.dbo.invoice set paymentterms='P150' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='180')
	update spain.dbo.invoice set paymentterms='P180' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='0')
	update spain.dbo.invoice set paymentterms='P0' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='NM25')
	update spain.dbo.invoice set paymentterms='PNM25' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM15')
	update spain.dbo.invoice set paymentterms='P30EOM15' where paymenttermsid=@id

	-- updating paymentterm enum in companysettingsforallocatedcompany

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P30' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P30EOM' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='EOM30')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='PEOM30' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM10')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P30EOM10' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='45')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P45' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='45EOM')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P45EOM' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='EOM45')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='PEOM45' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='60')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P60' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='85')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P85' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='90')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P90' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='120')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P120' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='150')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P150' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='180')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P180' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='0')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P0' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='NM25')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='PNM25' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM15')
	update spain.dbo.companysettingsforallocatedcompany set paymentterms='P30EOM15' where paymenttermsid=@id

	-- updating paymentterm enum in supplierinvoice
	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30')
	update spain.dbo.supplierinvoice set paymentterms='P30' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM')
	update spain.dbo.supplierinvoice set paymentterms='P30EOM' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='EOM30')
	update spain.dbo.supplierinvoice set paymentterms='PEOM30' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM10')
	update spain.dbo.supplierinvoice set paymentterms='P30EOM10' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='45')
	update spain.dbo.supplierinvoice set paymentterms='P45' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='45EOM')
	update spain.dbo.supplierinvoice set paymentterms='P45EOM' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='EOM45')
	update spain.dbo.supplierinvoice set paymentterms='PEOM45' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='60')
	update spain.dbo.supplierinvoice set paymentterms='P60' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='85')
	update spain.dbo.supplierinvoice set paymentterms='P85' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='90')
	update spain.dbo.supplierinvoice set paymentterms='P90' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='120')
	update spain.dbo.supplierinvoice set paymentterms='P120' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='150')
	update spain.dbo.supplierinvoice set paymentterms='P150' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='180')
	update spain.dbo.supplierinvoice set paymentterms='P180' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='0')
	update spain.dbo.supplierinvoice set paymentterms='P0' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='NM25')
	update spain.dbo.supplierinvoice set paymentterms='PNM25' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM15')
	update spain.dbo.supplierinvoice set paymentterms='P30EOM15' where paymenttermsid=@id


	-- updating paymentterm in contract
	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30')
	update spain.dbo.contract set paymentterms='P30' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM')
	update spain.dbo.contract set paymentterms='P30EOM' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='EOM30')
	update spain.dbo.contract set paymentterms='PEOM30' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM10')
	update spain.dbo.contract set paymentterms='P30EOM10' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='45')
	update spain.dbo.contract set paymentterms='P45' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='45EOM')
	update spain.dbo.contract set paymentterms='P45EOM' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='EOM45')
	update spain.dbo.contract set paymentterms='PEOM45' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='60')
	update spain.dbo.contract set paymentterms='P60' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='85')
	update spain.dbo.contract set paymentterms='P85' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='90')
	update spain.dbo.contract set paymentterms='P90' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='120')
	update spain.dbo.contract set paymentterms='P120' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='150')
	update spain.dbo.contract set paymentterms='P150' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='180')
	update spain.dbo.contract set paymentterms='P180' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='0')
	update spain.dbo.contract set paymentterms='P0' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='NM25')
	update spain.dbo.contract set paymentterms='PNM25' where paymenttermsid=@id

	set @id=(select DISTINCT  pt.id from paymentterms pt, invoice i where pt.id=i.paymenttermsid and pt.code='30EOM15')
	update spain.dbo.contract set paymentterms='P30EOM15' where paymenttermsid=@id
	
	
	--update due date in invoice table
	DECLARE @issuedate datetime
	DECLARE @duedate datetime
	DECLARE @getissuedate CURSOR
	SET @getissuedate = CURSOR FOR SELECT issuedate FROM invoice 
 
	OPEN @getissuedate
	FETCH NEXT
	FROM @getissuedate INTO @issuedate
	WHILE @@FETCH_STATUS = 0
	BEGIN
		update invoice set duedate = @issuedate where issuedate=@issuedate and paymentterms='P0' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(day,30,@issuedate)) where issuedate=@issuedate and paymentterms='P30' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(day,60,@issuedate)) where issuedate=@issuedate and paymentterms='P60' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(day,85,@issuedate)) where issuedate=@issuedate and paymentterms='P85' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(day,90,@issuedate)) where issuedate=@issuedate and paymentterms='P90' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(day,120,@issuedate)) where issuedate=@issuedate and paymentterms='P120' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(day,150,@issuedate)) where issuedate=@issuedate and paymentterms='P150' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(day,190,@issuedate)) where issuedate=@issuedate and paymentterms='P190' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(month,2,@issuedate)) where issuedate=@issuedate and paymentterms='P45' and issuedate is not null
		FETCH NEXT FROM @getissuedate INTO @issuedate
	END

	CLOSE @getissuedate
	DEALLOCATE @getissuedate

	DECLARE @issuedate2 datetime
	DECLARE @duedate2 datetime
	DECLARE @getissuedate2 CURSOR

	SET @getissuedate2 = CURSOR FOR SELECT issuedate FROM invoice 
 
	OPEN @getissuedate2
	FETCH NEXT
	FROM @getissuedate2 INTO @issuedate2
	WHILE @@FETCH_STATUS = 0
	BEGIN
		update invoice set duedate = EOMONTH((SELECT DATEADD(month,1,@issuedate2))) where issuedate=@issuedate2 and paymentterms='P30EOM' and issuedate is not null
		update invoice set duedate = (SELECT DATEADD(day,30,EOMONTH(@issuedate2))) where issuedate=@issuedate2 and paymentterms='PEOM30' and issuedate is not null
		DECLARE @Var datetime = (select DATEADD(MONTH,2,(@issuedate2)))
		update invoice set duedate = datefromparts(year(@Var), month(@Var), 10) where issuedate=@issuedate2 and paymentterms='P30EOM10' and issuedate is not null
		DECLARE @Var1 datetime = (select DATEADD(MONTH,2,(@issuedate2)))
		update invoice set duedate = datefromparts(year(@Var1), month(@Var1), 15) where issuedate=@issuedate2 and paymentterms='P30EOM15' and issuedate is not null
		DECLARE @Var3 datetime = (select DATEADD(MONTH,1,(@issuedate2)))
		update invoice set duedate = datefromparts(year(@Var3), month(@Var3),25) where issuedate=@issuedate2 and paymentterms='PNM25' and issuedate is not null
		update invoice set duedate = EOMONTH((select DATEADD(DD,45,@issuedate2)))  where issuedate=@issuedate2 and paymentterms='P45EOM' and issuedate is not null
		update invoice set duedate = (select DATEADD(day,45,EOMONTH(@issuedate2)))  where issuedate=@issuedate2 and paymentterms='PEOM45' and issuedate is not null
		FETCH NEXT FROM @getissuedate2 INTO @issuedate2
	END

	CLOSE @getissuedate2
	DEALLOCATE @getissuedate2

	INSERT INTO dbversion(version) VALUES(411);
	GO

COMMIT TRAN

