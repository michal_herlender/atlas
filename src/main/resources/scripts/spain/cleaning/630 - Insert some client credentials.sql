-- Insert some client credentials 

USE [atlas]
GO

BEGIN TRAN

	DECLARE @roleid_ws_prebooking int
	DECLARE @roleid_ws_portal int
	DECLARE @roleid_ws_invoice int

	set @roleid_ws_prebooking = (select id from dbo.perrole where name like 'ROLE_WEB_SERVICES_PREBOOKING')
	set @roleid_ws_portal = (select id from dbo.perrole where name like 'ROLE_WEB_SERVICES_PORTAL')
	set @roleid_ws_invoice = (select id from dbo.perrole where name like 'ROLE_WEB_SERVICES_INVOICE_CREDIT_NOTE')

	insert into dbo.oauth2clientdetails(id, clientid, clientSecret, role, resourceIds, scope, accesstokenvalidity, refreshtokenvalidity, description)
	values(1, 'mobileapp_logistics', 'sj938fj39f', @roleid_ws_prebooking,'resource-server-rest-api', 'all', 36000, 36000, 'Mobile Logistics Application'),
	  (2, 'mobileapp_portal', '20304f0202!', @roleid_ws_portal ,'resource-server-rest-api', 'all', 36000, 36000, 'Client Mobile Application'),
	  (3, 'sidetrade_client', '3n9%28394&', @roleid_ws_invoice,'resource-server-rest-api', 'all', 36000, 36000, 'Accounts Payable Sidetrack Integration');


	INSERT INTO dbversion(version) VALUES (630)

COMMIT TRAN