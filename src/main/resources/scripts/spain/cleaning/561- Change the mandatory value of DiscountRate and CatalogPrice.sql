-- Change the mandatory value of DiscountRate and CatalogPrice in the ImportQuotationItems 
-- exchange format (Global)

USE [atlas]
GO

BEGIN TRAN

DECLARE @id int

SET @id=(SELECT id FROM dbo.exchangeformat WHERE name like 'ImportQuotationItems' AND exchangeFormatLevel LIKE 'GLOBAL');
UPDATE dbo.exchangeformatfieldnamedetails SET mandatory=0 WHERE exchangeFormatid=@id AND fieldName LIKE 'CATALOG_PRICE'
UPDATE dbo.exchangeformatfieldnamedetails SET mandatory=0 WHERE exchangeFormatid=@id AND fieldName LIKE 'DISCOUNT_RATE'

INSERT INTO dbversion(version) VALUES(561);

COMMIT TRAN
