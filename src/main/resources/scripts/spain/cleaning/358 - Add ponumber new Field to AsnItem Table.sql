-- Add ponumber new field to asnitem table.

USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.asnitem ADD ponumber varchar(100);

INSERT INTO dbversion(version) VALUES(358);

COMMIT TRAN