USE [spain];

insert into dbo.servicetype ([displaycolour],[displaycolourfasttrack],[longname],[shortname]) VALUES ('#FFFFCC','#FFB9B9','External Accredited Calibration','AC-EX');

insert into calibrationtype 
			(accreditationspecific,
			active,
			certnoresetyearly,
			firstpagepapertype,
			labeltemplatepath,
			orderby,
			pdfcontinuationwatermark,
			pdfheaderwatermark,
			recalldateonlabel,
			restpagepapertype,
			servicetypeid,
			orgid)
select accreditationspecific,
	   1,
	   certnoresetyearly,
	   firstpagepapertype,
	   labeltemplatepath,
	   orderby + 10,
	   pdfcontinuationwatermark,
	   pdfheaderwatermark,
	   recalldateonlabel,
	   restpagepapertype,
	   (select servicetypeid from servicetype s where s.longname = 'External Accredited Calibration') as 'servicetypeid',
	   orgid
from calibrationtype 
left join servicetype on servicetype.servicetypeid = calibrationtype.servicetypeid
where servicetype.longname = 'In-House Accredited Calibration';


INSERT INTO [dbo].[defaultquotationcalibrationcondition] 
(conditiontext, islatest, lastModified, caltypeid) 
SELECT '',1,'2016-01-01',caltypeid 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname = 'External Accredited Calibration';


INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'APPROVE', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname = 'External Accredited Calibration'; 

INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'REMOVED', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname = 'External Accredited Calibration';

INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'SUPERVISED', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname = 'External Accredited Calibration';

INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'PERFORM', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname = 'External Accredited Calibration';

INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'SIGN', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname = 'External Accredited Calibration';

INSERT INTO servicetypelongnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.longname from servicetype
WHERE servicetype.longname = 'External Accredited Calibration';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Calibración acreditada Externo' 
from servicetype
where servicetype.longname = 'External Accredited Calibration';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.shortname from servicetype
WHERE servicetype.longname = 'External Accredited Calibration';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_ES','AC-EX' from servicetype
WHERE servicetype.longname = 'External Accredited Calibration';


