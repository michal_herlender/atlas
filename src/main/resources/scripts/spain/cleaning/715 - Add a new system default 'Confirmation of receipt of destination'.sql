
USE [atlas]
GO

BEGIN TRAN

	DECLARE @groupId INT;
	DECLARE @defaultIdDestinationReceiptDate INT = 66;

	SET IDENTITY_INSERT [systemdefault] ON; 

	SELECT @groupId = id from systemdefaultgroup where dname = 'Client - Delivery Note';
	INSERT INTO dbo.systemdefault(defaultid, defaultdatatype, defaultdescription, defaultname, defaultvalue, endd, [start], groupid, groupwide)
		VALUES(@defaultIdDestinationReceiptDate, 'boolean', 'Sets whether to capture the destination reception date',  'Confirmation of receipt of destination', 'false', 0, 0, @groupId, 1);
	
	SET IDENTITY_INSERT [systemdefault] OFF; 

	-- name translations	
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@defaultIdDestinationReceiptDate,'de_DE','Bestätigung des Eingangs des Bestimmungsortes');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@defaultIdDestinationReceiptDate,'en_GB','Confirmation of receipt of destination');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@defaultIdDestinationReceiptDate,'es_ES','Confirmación de recepción de destino');
	INSERT INTO systemdefaultnametranslation (defaultid,locale,[translation])
		VALUES (@defaultIdDestinationReceiptDate,'fr_FR','Confirmation de réception de la destination');

	-- description translation
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@defaultIdDestinationReceiptDate,'de_DE','Legt fest, ob das Empfangsdatum des Ziels erfasst werden soll');
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@defaultIdDestinationReceiptDate,'en_GB','Sets whether to capture the destination reception date');
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@defaultIdDestinationReceiptDate,'es_ES','Establece si se captura la fecha de recepción de destino');
	INSERT INTO systemdefaultdescriptiontranslation (defaultid,locale,[translation])
		VALUES (@defaultIdDestinationReceiptDate,'fr_FR','Définit s''il faut capturer la date de réception de destination');

	-- Enable setting parameters for both client and business companies
	INSERT INTO systemdefaultcorole (coroleid, defaultid)
		VALUES	(1, @defaultIdDestinationReceiptDate),
				(5, @defaultIdDestinationReceiptDate);

	-- Enable setting parameters at the company and subdiv levels
	INSERT INTO systemdefaultscope ([scope] ,defaultid)
		VALUES (0, @defaultIdDestinationReceiptDate),
			   (2, @defaultIdDestinationReceiptDate);

	GO

	INSERT INTO dbversion(version) VALUES(715);

COMMIT TRAN