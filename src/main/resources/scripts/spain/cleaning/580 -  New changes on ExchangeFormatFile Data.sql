-- GWB 2020-02-23 : Changed script as previous version wouldn't run on spain-test and france-test
-- due to "orphaned" files in the files database PrebookingFileData / ExchangeFormatFileData
-- which would result in primary key constraints, as the "orphaned" files would not be changed, by
-- the previous script.
-- This version deletes the orphaned files, which have no associated asn / prebookingfile data and are otherwise unusable.

USE [atlas];

GO

BEGIN TRAN

CREATE TABLE dbo.exchangeformatfile (
		id INT IDENTITY not null,
        lastModified DATETIME2,
        addedon DATETIME2,
        [fileName] VARCHAR(255),
        lastModifiedBy INT,
        personid INT,
		[type] VARCHAR(31),
        PRIMARY KEY (id)
    );

ALTER TABLE dbo.asn ADD exchangeformatfileid INT;
ALTER TABLE dbo.asn ADD CONSTRAINT FK_ASN_EXCHANGEFORMATFILE FOREIGN KEY (exchangeformatfileid)
REFERENCES dbo.exchangeformatfile(id);

GO

--(1) first we need to fill the new exchangeformatfile table with data from 'asn' and 'exchangeformatfiledata' tables
INSERT INTO dbo.exchangeformatfile(lastModified, addedon, [fileName], lastModifiedBy, personid,
 [type])
SELECT asn.lastModified, asn.createdon, efd.[fileName], asn.lastModifiedBy, asn.createdby,
'prebooking'
FROM    [atlasfiles].dbo.ExchangeFormatFileData efd
        INNER JOIN dbo.asn asn 
		ON efd.entityId = asn.id
		LEFT JOIN dbo.exchangeformatfile ef
            ON ef.id = asn.exchangeformatfileid;

--(2) update the exchangeformatfileid field value in asn table with correspending value from (1)
UPDATE  asn
SET    exchangeformatfileid = ef.id
FROM    [atlasfiles].dbo.ExchangeFormatFileData efd
		INNER JOIN dbo.asn asn
			ON efd.entityId = asn.id
        INNER JOIN dbo.exchangeformatfile ef
            ON asn.createdby = ef.personid AND asn.createdon = ef.addedon;

-- There appear to be orphaned files (on Spain-test and France-test) that have no corresponding asn
-- These can be removed

DELETE FROM [atlasfiles].dbo.ExchangeFormatFileData WHERE entityId NOT IN (SELECT id FROM [dbo].[asn]);
GO

--(3) get pk constraint name from atlasfiles.dbo.ExchangeFormtFileData (if needed - change below)
--USE [atlasfiles]
--GO
DECLARE @pk_constraint_name NVARCHAR(256);
SELECT 
  @pk_constraint_name = CONSTRAINT_NAME
FROM [atlasfiles].INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE TABLE_NAME = 'ExchangeFormatFileData';
PRINT @pk_constraint_name

--(4) delete primary key constraint for ExchangeFormatFileData to enable update of primary key
EXECUTE ('ALTER TABLE [atlasfiles].dbo.ExchangeFormatFileData DROP CONSTRAINT '+@pk_constraint_name);
GO

--(6) update entityId field with corresponding value from exchnageformatfile table (via asn)
UPDATE  efd
SET    entityId = ef.id
FROM    [atlasfiles].dbo.ExchangeFormatFileData efd
		INNER JOIN dbo.asn asn
			ON efd.entityId = asn.id
        INNER JOIN dbo.exchangeformatfile ef
            ON asn.exchangeformatfileid = ef.id;

--(7) add primary key constraint back in
ALTER TABLE [atlasfiles].dbo.ExchangeFormatFileData ADD CONSTRAINT ExchangeFormatFileData_PK PRIMARY KEY (entityId);
GO

-- Keep the filename for now so that the script results could be checked/verified manually
--ALTER TABLE atlasfiles.dbo.ExchangeFormatFileData DROP COLUMN [fileName];

INSERT INTO dbversion(version) VALUES(580);

COMMIT TRAN