USE [atlas]
GO

BEGIN TRAN

	-- set ship-to address to invoice address for all existing credit note items
	-- follows previous logic on accounting interface
	UPDATE creditnoteitem SET shiptoaddressid = i.addressid
		FROM creditnoteitem cni
		INNER JOIN creditnote cn ON cni.creditnoteid = cn.id
		INNER JOIN invoice i on cn.invid = i.id

	GO

	ALTER TABLE creditnoteitem ALTER COLUMN shiptoaddressid INT NOT NULL

	INSERT INTO dbversion(version) VALUES(818);

COMMIT TRAN