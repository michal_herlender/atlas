-- Create oauth2clientdetails table to stores the Client Credentials
USE [atlas]
GO

BEGIN TRAN

       create table dbo.oauth2clientdetails (
        id int not null,
		clientid varchar(256) not null,
		clientSecret varchar(256) not null,
		role int not null,
		resourceIds varchar(256),
		scope varchar(256),
		accesstokenvalidity integer,
		refreshtokenvalidity integer,
		description varchar(2000),
        primary key (clientid),
		foreign key (role) references dbo.perrole(id)
    );

INSERT INTO dbversion(version) VALUES (628)

COMMIT TRAN