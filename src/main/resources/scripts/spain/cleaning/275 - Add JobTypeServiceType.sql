-- Creates and configures table JobTypeServiceType for configuring available serice types by job type
-- Adds order column to service type to control ordering

USE [spain]

BEGIN TRAN

    create table dbo.jobtypeservicetype (
        id int identity not null,
        defaultvalue bit default 0 not null,
        jobtypeid int not null,
        servicetypeid int,
        primary key (id)
    );

    alter table dbo.servicetype 
        add orderby int;

	GO

	UPDATE dbo.servicetype SET orderby = 1 WHERE shortname = 'AC-IH';
	UPDATE dbo.servicetype SET orderby = 2 WHERE shortname = 'ST-IH';
	UPDATE dbo.servicetype SET orderby = 3 WHERE shortname = 'AC-OS';
	UPDATE dbo.servicetype SET orderby = 4 WHERE shortname = 'ST-OS';
	UPDATE dbo.servicetype SET orderby = 5 WHERE shortname = 'REP';
	UPDATE dbo.servicetype SET orderby = 6 WHERE shortname = 'AC-EX';
	UPDATE dbo.servicetype SET orderby = 7 WHERE shortname = 'STD-EX';
	UPDATE dbo.servicetype SET orderby = 8 WHERE shortname = 'OTHER';
	UPDATE dbo.servicetype SET orderby = 9 WHERE shortname = 'AC-IH-MV';
	UPDATE dbo.servicetype SET orderby = 10 WHERE shortname = 'ST-IH-MV';
	UPDATE dbo.servicetype SET orderby = 11 WHERE shortname = 'AC-OS-MV';
	UPDATE dbo.servicetype SET orderby = 12 WHERE shortname = 'ST-OS-MV';
	UPDATE dbo.servicetype SET orderby = 20 WHERE shortname = 'M';
	UPDATE dbo.servicetype SET orderby = 21 WHERE shortname = 'IC';
	UPDATE dbo.servicetype SET orderby = 30 WHERE shortname = 'CLIENT';
	UPDATE dbo.servicetype SET orderby = 31 WHERE shortname = 'CLIENT-EX';
	UPDATE dbo.servicetype SET orderby = 40 WHERE shortname = 'SERVICE';
-- The following still exist, but are going to be deleted soon...
	UPDATE dbo.servicetype SET orderby = 101 WHERE shortname = 'AC-EX-MV';
	UPDATE dbo.servicetype SET orderby = 102 WHERE shortname = 'ST-EX-MV';
	UPDATE dbo.servicetype SET orderby = 103 WHERE shortname = 'ST-IC';
	UPDATE dbo.servicetype SET orderby = 104 WHERE shortname = 'AC-IC';
	UPDATE dbo.servicetype SET orderby = 105 WHERE shortname = 'AC-IC-MV';
	UPDATE dbo.servicetype SET orderby = 106 WHERE shortname = 'ST-IC-MV';
	UPDATE dbo.servicetype SET orderby = 107 WHERE shortname = 'AC-OS-IC';
	UPDATE dbo.servicetype SET orderby = 108 WHERE shortname = 'ST-OS-IC';

	GO

--    alter table dbo.jobtypeservicetype 
--        drop constraint UKfk28yjxyow7dgp6oltj6f2f4u;

    alter table dbo.jobtypeservicetype 
        add constraint UKfk28yjxyow7dgp6oltj6f2f4u unique (servicetypeid, jobtypeid);

--    alter table dbo.servicetype 
--        drop constraint UK_kmula4dhv996h8926billn6g5;

    alter table dbo.servicetype 
        alter column orderby int not null;

    alter table dbo.servicetype 
        add constraint UK_kmula4dhv996h8926billn6g5 unique (orderby);

    alter table dbo.jobtypeservicetype 
        add constraint FK_jobtype_servicetype 
        foreign key (servicetypeid) 
        references dbo.servicetype;

-- Job Types : 1 - standard, 2 - single price, 4 - site
INSERT INTO [dbo].[jobtypeservicetype]
           ([defaultvalue],[jobtypeid],[servicetypeid])
     VALUES
           (1,1,(SELECT servicetypeid from servicetype where shortname='AC-IH')),
           (1,2,(SELECT servicetypeid from servicetype where shortname='AC-IH')),
           (1,4,(SELECT servicetypeid from servicetype where shortname='AC-OS'));
GO

INSERT INTO [dbo].[jobtypeservicetype]
           ([defaultvalue],[jobtypeid],[servicetypeid])
           SELECT 0,1,servicetypeid from servicetype where shortname in ('ST-IH', 'OTHER', 'AC-EX', 'STD-EX', 'AC-IH-MV', 'ST-IH-MV');
GO

INSERT INTO [dbo].[jobtypeservicetype]
           ([defaultvalue],[jobtypeid],[servicetypeid])
           SELECT 0,2,servicetypeid from servicetype where shortname in ('ST-IH', 'OTHER', 'AC-EX', 'STD-EX', 'AC-IH-MV', 'ST-IH-MV');
GO

INSERT INTO [dbo].[jobtypeservicetype]
           ([defaultvalue],[jobtypeid],[servicetypeid])
           SELECT 0,4,servicetypeid from servicetype where shortname in ('ST-OS', 'AC-OS-MV', 'ST-OS-MV');
GO

INSERT INTO dbversion(version) VALUES(275);

COMMIT TRAN