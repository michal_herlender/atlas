BEGIN TRAN

USE [spain]
GO

UPDATE [dbo].[actionoutcome]
   SET [description] = 'Item requires work at another subdivision of our business company, no PO required'
 WHERE id = 88;

UPDATE [dbo].[actionoutcometranslation]
   SET [translation] = 'Item requires work at another subdivision of our business company, no PO required'
 WHERE id = 88 and locale = 'en_GB';

UPDATE [dbo].[actionoutcometranslation]
   SET [translation] = 'El artículo requiere trabajo en otra de nuestras subdivisiónes, no hacer orden de compra'
 WHERE id = 88 and locale = 'es_ES';

-- A couple areas noted where better to have bsuiness company

UPDATE [dbo].[company] SET [currencyid] = 3 WHERE coid = 6578;
UPDATE [dbo].[company] SET [currencyid] = 3 WHERE coid = 6579;

-- Insert new lookupresult for return from TP -> Job Costing

INSERT INTO [dbo].[lookupresult]
           ([result],[activityid],[lookupid],[outcomestatusid],[resultmessage])
     VALUES
           ('g',100,46,127,6)

GO

ROLLBACK TRAN