use spain;

begin transaction;

update dbo.permission set permission='QUOTATION_LOW_LIMIT' where permission='LOW_QUOTATION_LIMIT';
update dbo.permission set permission='QUOTATION_MEDIUM_LIMIT' where permission='MEDIUM_QUOTATION_LIMIT';
update dbo.permission set permission='QUOTATION_HIGH_LIMIT' where permission='HIGH_QUOTATION_LIMIT';
update dbo.permission set permission='QUOTATION_LIMITLESS_LIMIT' where permission='LIMITLESS_QUOTATION_LIMIT';

update dbo.permissionLimit set permissionName='QUOTATION_LOW_LIMIT' where permissionName='LOW_QUOTATION_LIMIT';
update dbo.permissionLimit set permissionName='QUOTATION_MEDIUM_LIMIT' where permissionName='MEDIUM_QUOTATION_LIMIT';
update dbo.permissionLimit set permissionName='QUOTATION_HIGH_LIMIT' where permissionName='HIGH_QUOTATION_LIMIT';


INSERT INTO dbversion(version) VALUES(376);

commit transaction;