USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[labelprinter]
           ([description]
           ,[path]
           ,[addrid]
           ,[locid]
           ,[darknesslevel]
           ,[ipv4]
           ,[language]
           ,[resolution]
           ,[media])
     VALUES
           ('GX430T_US_SCL_TEST'
           ,''
           ,12210
           ,null
           ,15
           ,'192.168.0.100'
           ,'ZPL'
           ,300
           ,1);

GO

INSERT INTO dbversion (version) VALUES (206);

GO

COMMIT TRAN