-- DEV-2161 - fix inconsistent cal type configurations
USE [atlas]

BEGIN TRAN

DECLARE @pmCalTypeId int;
DECLARE @repCalTypeId int;
DECLARE @otherCalTypeId int;
DECLARE @clientCalTypeId int;
DECLARE @clientEXCalTypeId int;

DECLARE @pmServiceTypeId int;
DECLARE @repServiceTypeId int;
DECLARE @otherServiceTypeId int;
DECLARE @clientServiceTypeId int;
DECLARE @clientEXServiceTypeId int;
DECLARE @icilServiceTypeId int;

SELECT @pmServiceTypeId = servicetypeid FROM dbo.servicetype WHERE shortname = 'PM';
SELECT @repServiceTypeId = servicetypeid FROM dbo.servicetype WHERE shortname = 'REP';
SELECT @otherServiceTypeId = servicetypeid FROM dbo.servicetype WHERE shortname = 'OTHER';
SELECT @clientServiceTypeId = servicetypeid FROM dbo.servicetype WHERE shortname = 'CLIENT';
SELECT @clientEXServiceTypeId = servicetypeid FROM dbo.servicetype WHERE shortname = 'CLIENT-EX';

SELECT @icilServiceTypeId = servicetypeid FROM dbo.servicetype WHERE shortname = 'IC-IL';

SELECT @pmCalTypeId = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @pmServiceTypeId;
SELECT @repCalTypeId = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @repServiceTypeId;
SELECT @otherCalTypeId = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @otherServiceTypeId;
SELECT @clientCalTypeId = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @clientServiceTypeId;
SELECT @clientEXCalTypeId = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @clientEXServiceTypeId;


INSERT INTO [dbo].[jobtypecaltype]
           ([defaultvalue]
           ,[jobtypeid]
           ,[caltypeid])
     VALUES
           (0,@repCalTypeId,2)

DELETE FROM jobtypecaltype where jobtypeid = 4 and caltypeid = @otherCalTypeId;
DELETE FROM jobtypecaltype where jobtypeid = 4 and caltypeid = @clientCalTypeId;
DELETE FROM jobtypecaltype where jobtypeid = 4 and caltypeid = @clientEXCalTypeId;

INSERT INTO [dbo].[jobtypeservicetype]
           ([defaultvalue]
           ,[jobtypeid]
           ,[servicetypeid])
     VALUES
           (0,1,@pmServiceTypeId),
		   (0,2,@pmServiceTypeId),
		   (0,2,@icilServiceTypeId)

INSERT INTO [dbo].[calibrationaccreditationlevel]
           ([accredlevel]
           ,[caltypeid])
     VALUES
           ('APPROVE',@pmCalTypeId),
		   ('PERFORM',@pmCalTypeId),
		   ('REMOVED',@pmCalTypeId),
		   ('SUPERVISED',@pmCalTypeId)


INSERT INTO dbversion(version) VALUES(749);

COMMIT TRAN