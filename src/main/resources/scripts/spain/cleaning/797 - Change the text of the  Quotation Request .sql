Use atlas
Go

BEGIN TRAN

UPDATE quotationdocdefaulttext
SET

body = 'In response to your request, we have pleasure in submitting our calibration quotation.
The prices quoted are subject to Trescal terms and conditions as per the attached summary.
The turnaround time is counted from the reception of your instruments and your order.
If we detect a hidden failure, an additional offer will be sent to you.
Return transport is provided at the expense and risk of the customer.
In the absence of particular requirement from you, the declaration of conformity is pronounced without taking into account uncertainties of measure.
You may like to visit our website at www.trescal.com to see the additional services we provide to assist customers in the management of the calibration of their test equipment.
We trust the costs provided meet with your approval, but should you require further information please do not hesitate to contact the undersigned.
Kind regards,'

WHERE locale= 'en_GB'

UPDATE quotationdocdefaulttext
SET

body = 'Pour faire suite � votre demande, nous avons le plaisir de vous communiquer notre offre.
Les prix indiqu�s dans les offres sont soumis � nos conditions g�n�rales de vente (voir derni�re page).
Les d�lais s entendent � compter de la r�ception de vos instruments et de votre commande.
Si nous d�tectons une panne cach�e, une offre compl�mentaire vous sera adress�e.
Dans le cadre de nos prestations, et en l�absence d�exigence particuli�re de votre part, la d�claration de conformit� est prononc�e sans tenir compte des incertitudes de mesure
Vous pouvez visiter notre site web www.trescal.com pour connaitre les services compl�mentaires que nous proposons.
En attente de votre approbation, nous restons � votre disposition pour tout renseignement.
Cordialement,'

WHERE locale= 'fr_FR'


UPDATE quotationdocdefaulttext
SET

body = 'Tenemos el placer de enviarle nuestra oferta por los servicios requeridos.
Los precios ofertados estan sometidos a las condiciones generales adjuntas.
Los plazos se cuentan desde la recepci�n de su pedido.
Si detectamos un error oculto, se le enviar� una oferta adicional.
El transporte de regreso se realiza a expensas y riesgo del cliente.
A falta de alg�n requisito particular por su parte, se emite la declaraci�n de conformidad sin tener en cuenta las incertidumbres de medici�n.
Visite nuestra web a www.trescal.com para ver los servicios adicionales que tenemos para atender a nuestros clientes en la gesti�n de la calibraci�n de sus equipos, descarga de certificados y otras aplicaciones.
Confiamos que esta oferta sea de su agrado y si necesita ampliar la informaci�n, por favor no dude en ponerse en contacto con nosotros.
Atentamente,'

WHERE locale= 'es_ES'


UPDATE quotationdocdefaulttext
SET

body = 'wir bedanken uns f�r Ihre Anfrage und unterbreiten Ihnen nachfolgend unser Angebot.
Das Angebot wurde auf der Basis der uns vorliegenden Informationen erstellt. 
Eventuell auftretende Abweichungen preislicher Art k�nnen die Folge von fehlenden bzw. nicht exakten Angaben zum Kalibriergegenstand sein, Angebot freibleibend.
In Ermangelung einer besonderen Anforderung von Ihnen wird die Konformit�tserkl�rung ohne Ber�cksichtigung von Ma�unsicherheiten ausgesprochen
Unsere weiteren Dienstleistungen finden Sie auf unserer Webseite www.trescal.de oder rufen Sie uns an.
Mit freundlichen Gr��en'

WHERE locale= 'de_DE'




INSERT INTO dbversion VALUES (797)

COMMIT TRAN