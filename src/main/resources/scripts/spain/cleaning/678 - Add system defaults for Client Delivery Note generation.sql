-- Sets up 4 new system defaults into a new group for specifying defaults for client delivery list generation
-- Galen Beck - 2020-08-27

USE [atlas]
GO

BEGIN TRAN

declare @groupId int;
declare @defaultIdDateReceived int = 59;
declare @defaultIdPurchaseOrder int = 60;
declare @defaultIdCertificates int = 61;
declare @defaultIdTotalPrice int = 62;

INSERT INTO [dbo].[systemdefaultgroup]
           ([description]
           ,[dname])
     VALUES
           ('Defines parameters for generation of client delivery notes'
           ,'Client - Delivery Note')

SELECT @groupId = id from systemdefaultgroup where dname = 'Client - Delivery Note';

INSERT INTO [dbo].[systemdefaultgroupdescriptiontranslation]
           ([id]
           ,[locale]
           ,[translation])
     VALUES
           (@groupId, 'en_GB', 'Defines parameters for generation of client delivery notes'),
           (@groupId, 'fr_FR', 'D�finit les param�tres de g�n�ration des bons de livraison client'),
           (@groupId, 'es_ES', 'Define par�metros para la generaci�n de albaranes de clientes'),
           (@groupId, 'de_DE', 'Definiert Parameter f�r die Generierung von Kundenlieferscheinen')

INSERT INTO [dbo].[systemdefaultgroupnametranslation]
           ([id]
           ,[locale]
           ,[translation])
     VALUES
           (@groupId, 'en_GB', 'Client - Delivery Note'),
           (@groupId, 'fr_FR', 'Client - Bons de livraison'),
           (@groupId, 'es_ES', 'Cliente - Notas de entrega'),
           (@groupId, 'de_DE', 'Client - Lieferscheine')

SET IDENTITY_INSERT [systemdefault] ON; 

INSERT INTO dbo.systemdefault(defaultid, defaultdatatype, defaultdescription, defaultname, defaultvalue, endd, [start], groupid, groupwide)
VALUES(@defaultIdCertificates, 'boolean', 'Print certificate information on the delivery note',  'Print certificate information', 'true', 0, 0, @groupid, 1),
(@defaultIdDateReceived, 'boolean', 'Print date received on the delivery note', 'Print date received', 'true', 0, 0, @groupid, 1),
(@defaultIdPurchaseOrder, 'boolean', 'Print purchase order on the delivery note', 'Print purchase order', 'true', 0, 0, @groupid, 1),
(@defaultIdTotalPrice, 'boolean', 'Print total price on the delivery note', 'Print total price', 'false', 0, 0, @groupid, 1);

SET IDENTITY_INSERT [systemdefault] OFF; 


INSERT INTO dbo.systemdefaultnametranslation
(defaultid, locale, [translation])
VALUES(@defaultIdCertificates, 'en_GB', 'Print certificate information'),
      (@defaultIdCertificates, 'fr_FR', 'Imprimer les informations du certificat'),      
      (@defaultIdCertificates, 'de_DE', 'Zertifikatinformationen drucken'),
      (@defaultIdCertificates, 'es_ES', 'Imprimir informaci�n del certificado');

INSERT INTO dbo.systemdefaultdescriptiontranslation
(defaultid, locale, [translation])
VALUES(@defaultIdCertificates, 'en_GB', 'Print certificate information on the delivery note'),
      (@defaultIdCertificates, 'fr_FR', 'Imprimer les informations du certificat sur le bon de livraison'),
      (@defaultIdCertificates, 'de_DE', 'Drucken Sie die Zertifikatinformationen auf dem Lieferschein aus'),
      (@defaultIdCertificates, 'es_ES', 'Imprima la informaci�n del certificado en el albar�n de entrega');

INSERT INTO dbo.systemdefaultnametranslation
(defaultid, locale, [translation])
VALUES(@defaultIdDateReceived, 'en_GB', 'Print date received'),
      (@defaultIdDateReceived, 'fr_FR', 'Imprimer la date de r�ception'),
      (@defaultIdDateReceived, 'de_DE', 'Drucken Sie das Empfangsdatum aus'),
      (@defaultIdDateReceived, 'es_ES', 'Imprime la fecha de recepci�n');

INSERT INTO dbo.systemdefaultdescriptiontranslation
(defaultid, locale, [translation])
VALUES(@defaultIdDateReceived, 'en_GB', 'Print date received on the delivery note'),
      (@defaultIdDateReceived, 'fr_FR', 'Imprimer la date de r�ception sur le bon de livraison'),
      (@defaultIdDateReceived, 'de_DE', 'Druckdatum auf dem Lieferschein erhalten'),
      (@defaultIdDateReceived, 'es_ES', 'Imprimir fecha de recepci�n en el albar�n de entrega');

INSERT INTO dbo.systemdefaultnametranslation
(defaultid, locale, [translation])
VALUES(@defaultIdPurchaseOrder, 'en_GB', 'Print purchase order'),
      (@defaultIdPurchaseOrder, 'fr_FR', 'Imprimer le bon de commande'),
      (@defaultIdPurchaseOrder, 'de_DE', 'Bestellung drucken'),
      (@defaultIdPurchaseOrder, 'es_ES', 'Imprimir orden de compra');

INSERT INTO dbo.systemdefaultdescriptiontranslation
(defaultid, locale, [translation])
VALUES(@defaultIdPurchaseOrder, 'en_GB', 'Print purchase order on the delivery note'),
      (@defaultIdPurchaseOrder, 'fr_FR', 'Imprimer le bon de commande sur le bon de livraison'),
      (@defaultIdPurchaseOrder, 'de_DE', 'Bestellung auf dem Lieferschein ausdrucken'),
      (@defaultIdPurchaseOrder, 'es_ES', 'Imprimir orden de compra en el albar�n de entrega');

INSERT INTO dbo.systemdefaultnametranslation
(defaultid, locale, [translation])
VALUES(@defaultIdTotalPrice, 'en_GB', 'Print total price'),
      (@defaultIdTotalPrice, 'fr_FR', 'Imprimer le prix total'),
      (@defaultIdTotalPrice, 'de_DE', 'Gesamtpreis drucken'),
      (@defaultIdTotalPrice, 'es_ES', 'Imprimir precio total');

INSERT INTO dbo.systemdefaultdescriptiontranslation
(defaultid, locale, [translation])
VALUES(@defaultIdTotalPrice, 'en_GB', 'Print total price on the delivery note'),
      (@defaultIdTotalPrice, 'fr_FR', 'Imprimer le prix total sur le bon de livraison'),
      (@defaultIdTotalPrice, 'de_DE', 'Drucken Sie den Gesamtpreis auf den Lieferschein'),
      (@defaultIdTotalPrice, 'es_ES', 'Imprime el precio total en el albar�n de entrega');


-- Enable setting parameters for both client and business companies
INSERT INTO systemdefaultcorole (coroleid,defaultid)
	VALUES (1,@defaultIdDateReceived),
	(1,@defaultIdPurchaseOrder),
	(1,@defaultIdCertificates),
	(1,@defaultIdTotalPrice),
	(5,@defaultIdDateReceived),
	(5,@defaultIdPurchaseOrder),
	(5,@defaultIdCertificates),
	(5,@defaultIdTotalPrice);
		
-- Enable setting parameters at the company level only (subdiv / contact presumed unnecessary)
INSERT INTO systemdefaultscope ([scope],defaultid)
	VALUES (0,@defaultIdDateReceived),
	(0,@defaultIdPurchaseOrder),
	(0,@defaultIdCertificates),
	(0,@defaultIdTotalPrice);

INSERT INTO dbversion(version) VALUES(678);

COMMIT TRAN



