-- Based on the sheet "ERP Services FR 2018 05 15 - Update 24 09 2018.xlsx" provided by Didier Terrien
-- Because of the use of service types like ST-IH/AC-IH for mainly "measured values only" services in Spain,
-- we are reversing the service type definitions as part of this rename.
-- For example, the former AC-IH (servicetypeid 1) now becomes AC-MVO-IL,
-- and AC-IH-MV (servicetypeid 15) now becomes AC-SOC-IL,
-- Note, this script can/should be run again regardless of the original version (306) being run last Friday,
-- as a new file was provided with updated definitions.
-- Author Galen Beck - 2018-09-28 to 2018-10-01

USE [spain]
GO

BEGIN TRAN

-- Former ST-IH-MV
UPDATE [dbo].[servicetype] SET [longname] = 'Traceable calibration without statement of compliance in laboratory', [shortname] = 'TC-MVO-IL'
	WHERE [servicetypeid] = 2;
UPDATE [dbo].[servicetype] SET [description] = 'Formerly�ST-IH'
	WHERE [servicetypeid] = 2;
-- Former ST-IH
UPDATE [dbo].[servicetype] SET [longname] = 'Traceable calibration with statement of compliance in laboratory', [shortname] = 'TC-SOC-IL' 
	WHERE [servicetypeid] = 16;
UPDATE [dbo].[servicetype] SET [description] = 'Formerly�ST-IH-MV'
	WHERE [servicetypeid] = 16;
-- Former AC-IH-MV
UPDATE [dbo].[servicetype] SET [longname] = 'Accredited calibration without statement of compliance in laboratory', [shortname] = 'AC-MVO-IL' 
	WHERE [servicetypeid] = 1;
UPDATE [dbo].[servicetype] SET [description] = 'Formerly AC-IH'
	WHERE [servicetypeid] = 1;
-- Former AC-IH
UPDATE [dbo].[servicetype] SET [longname] = 'Accredited calibration with statement of compliance in laboratory', [shortname] = 'AC-SOC-IL' 
	WHERE [servicetypeid] = 15;
UPDATE [dbo].[servicetype] SET [description] = 'Formerly�AC-IH-MV'
	WHERE [servicetypeid] = 15;
-- Former ST-OS-MV
UPDATE [dbo].[servicetype] SET [longname] = 'Traceable calibration without statement of compliance on site', [shortname] = 'TC-MVO-OS' 
	WHERE [servicetypeid] = 5;
UPDATE [dbo].[servicetype] SET [description] = 'Formerly ST-OS'
	WHERE [servicetypeid] = 5;
-- Former ST-OS
UPDATE [dbo].[servicetype] SET [longname] = 'Traceable calibration with statement of compliance on site', [shortname] = 'TC-SOC-OS' 
	WHERE [servicetypeid] = 18;
UPDATE [dbo].[servicetype] SET [description] = 'Formerly ST-OS-MV'
	WHERE [servicetypeid] = 18;
-- Former AC-OS-MV
UPDATE [dbo].[servicetype] SET [longname] = 'Accredited calibration without statement of compliance on site', [shortname] = 'AC-MVO-OS' 
	WHERE [servicetypeid] = 4;
UPDATE [dbo].[servicetype] SET [description] = 'Formerly AC-OS'
	WHERE [servicetypeid] = 4;
-- Former AC-OS
UPDATE [dbo].[servicetype] SET [longname] = 'Accredited calibration with statement of compliance on site', [shortname] = 'AC-SOC-OS' 
	WHERE [servicetypeid] = 17;
UPDATE [dbo].[servicetype] SET [description] = 'Formerly AC-OS-MV'
	WHERE [servicetypeid] = 17;
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'TC-MVO-IL'
 WHERE locale = 'en_GB' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'TC-SOC-IL'
 WHERE locale = 'en_GB' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-MVO-IL'
 WHERE locale = 'en_GB' and servicetypeid = 1
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-SOC-IL'
 WHERE locale = 'en_GB' and servicetypeid = 15
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'TC-MVO-OS'
 WHERE locale = 'en_GB' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'TC-SOC-OS'
 WHERE locale = 'en_GB' and servicetypeid = 18
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-MVO-OS'
 WHERE locale = 'en_GB' and servicetypeid = 4
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-SOC-OS'
 WHERE locale = 'en_GB' and servicetypeid = 17
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'WE-OK-LA'
 WHERE locale = 'de_DE' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'WE-MK-LA'
 WHERE locale = 'de_DE' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AK-OK-LA'
 WHERE locale = 'de_DE' and servicetypeid = 1
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AK-MK-LA'
 WHERE locale = 'de_DE' and servicetypeid = 15
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'WE-OK-VO'
 WHERE locale = 'de_DE' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'WE-MK-VO'
 WHERE locale = 'de_DE' and servicetypeid = 18
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AK-OK-VO'
 WHERE locale = 'de_DE' and servicetypeid = 4
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AK-MK-VO'
 WHERE locale = 'de_DE' and servicetypeid = 17
GO


UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-EL'
 WHERE locale = 'es_ES' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-CAR-EL'
 WHERE locale = 'es_ES' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-EL'
 WHERE locale = 'es_ES' and servicetypeid = 1
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-CAR-EL'
 WHERE locale = 'es_ES' and servicetypeid = 15
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-IS'
 WHERE locale = 'es_ES' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-CAR-IS'
 WHERE locale = 'es_ES' and servicetypeid = 18
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-IS'
 WHERE locale = 'es_ES' and servicetypeid = 4
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-CAR-IS'
 WHERE locale = 'es_ES' and servicetypeid = 17
GO


UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ET-TR-LA'
 WHERE locale = 'fr_FR' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VE-TR-LA'
 WHERE locale = 'fr_FR' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ET-AC-LA'
 WHERE locale = 'fr_FR' and servicetypeid = 1
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VE-AC-LA'
 WHERE locale = 'fr_FR' and servicetypeid = 15
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ET-TR-SI'
 WHERE locale = 'fr_FR' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VE-TR-SI'
 WHERE locale = 'fr_FR' and servicetypeid = 18
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ET-AC-SI'
 WHERE locale = 'fr_FR' and servicetypeid = 4
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VE-AC-SI'
 WHERE locale = 'fr_FR' and servicetypeid = 17
GO


UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Traceable calibration without statement of compliance in laboratory'
 WHERE locale = 'en_GB' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Traceable calibration with statement of compliance in laboratory'
 WHERE locale = 'en_GB' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Accredited calibration without statement of compliance in laboratory'
 WHERE locale = 'en_GB' and servicetypeid = 1
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Accredited calibration with statement of compliance in laboratory'
 WHERE locale = 'en_GB' and servicetypeid = 15
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Traceable calibration without statement of compliance on site'
 WHERE locale = 'en_GB' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Traceable calibration with statement of compliance on site'
 WHERE locale = 'en_GB' and servicetypeid = 18
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Accredited calibration without statement of compliance on site'
 WHERE locale = 'en_GB' and servicetypeid = 4
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Accredited calibration with statement of compliance on site'
 WHERE locale = 'en_GB' and servicetypeid = 17
GO


UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Werkskalibrierung ohne Konfirmit�tsausage im Labor'
 WHERE locale = 'de_DE' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Werkskalibrierung mit Konfirmit�tsausage im Labor'
 WHERE locale = 'de_DE' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Akkreditierte Kalibrierung ohne Konfirmit�tsausage im Labor'
 WHERE locale = 'de_DE' and servicetypeid = 1
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Akkreditierte Kalibrierung mit Konfirmit�tsausage im Labor'
 WHERE locale = 'de_DE' and servicetypeid = 15
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Werkskalibrierung ohne Konfirmit�tsausage Vor-Ort'
 WHERE locale = 'de_DE' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Werkskalibrierung mit Konfirmit�tsausage Vor-Ort'
 WHERE locale = 'de_DE' and servicetypeid = 18
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Akkreditierte Kalibrierung ohne Konfirmit�tsausage Vor-Ort'
 WHERE locale = 'de_DE' and servicetypeid = 4
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Akkreditierte Kalibrierung mit Konfirmit�tsausage Vor-Ort'
 WHERE locale = 'de_DE' and servicetypeid = 17
GO


UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n trazable en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n trazable con verificaci�n CAR en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n acreditada en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = 1
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n acreditada con verificaci�n CAR en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = 15
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n trazable in situ'
 WHERE locale = 'es_ES' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n trazable con verificaci�n CAR in situ'
 WHERE locale = 'es_ES' and servicetypeid = 18
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n acreditada in situ'
 WHERE locale = 'es_ES' and servicetypeid = 4
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n acreditada con verificaci�n CAR in situ'
 WHERE locale = 'es_ES' and servicetypeid = 17
GO


UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Etalonnage tra�able en laboratoire'
 WHERE locale = 'fr_FR' and servicetypeid = 2
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification tra�able en laboratoire'
 WHERE locale = 'fr_FR' and servicetypeid = 16
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Etalonnage accr�dit� en laboratoire'
 WHERE locale = 'fr_FR' and servicetypeid = 1
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification accr�dit�e en laboratoire'
 WHERE locale = 'fr_FR' and servicetypeid = 15
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Etalonnage tra�able sur site'
 WHERE locale = 'fr_FR' and servicetypeid = 5
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification tra�able sur site'
 WHERE locale = 'fr_FR' and servicetypeid = 18
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Etalonnage accr�dit� sur site'
 WHERE locale = 'fr_FR' and servicetypeid = 4
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification accr�dit�e sur site'
 WHERE locale = 'fr_FR' and servicetypeid = 17
GO

INSERT INTO dbversion(version) VALUES(308);

COMMIT TRAN