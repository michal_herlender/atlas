-- Script to convert JobType to Enum (jobtype table no longer needed)
-- Author Galen Beck 2016-10-31

USE [spain]
GO

BEGIN TRAN

ALTER TABLE [dbo].[job] DROP CONSTRAINT [FK19BBDD3880D97];
GO

ALTER TABLE [dbo].[userpreferences] DROP CONSTRAINT [FK8551364D82A32794];
GO

DROP TABLE [dbo].[jobtype]
GO

COMMIT TRAN