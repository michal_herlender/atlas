-- Script to insert the same VAT rates that are on production
-- (recently added) to the test and development systems 
-- On production, we won't need to run this script as data already there.

USE [atlas]
GO

BEGIN TRAN

DECLARE @countryIdMY int;
DECLARE @countryIdSG int;
DECLARE @countryIdAT int;
DECLARE @countryIdBE int;
DECLARE @countryIdBR int;

DECLARE @countryIdCA int;
DECLARE @countryIdCZ int;
DECLARE @countryIdIT int;
DECLARE @countryIdMT int;
DECLARE @countryIdNL int;

DECLARE @countryIdRO int;
DECLARE @countryIdCH int;

select @countryIdMY = country.countryid from country where countrycode = 'MY';
select @countryIdSG = country.countryid from country where countrycode = 'SG';
select @countryIdAT = country.countryid from country where countrycode = 'AT';
select @countryIdBE = country.countryid from country where countrycode = 'BE';
select @countryIdBR = country.countryid from country where countrycode = 'BR';

select @countryIdCA = country.countryid from country where countrycode = 'CA';
select @countryIdCZ = country.countryid from country where countrycode = 'CZ';
select @countryIdIT = country.countryid from country where countrycode = 'IT';
select @countryIdMT = country.countryid from country where countrycode = 'MT';
select @countryIdNL = country.countryid from country where countrycode = 'NL';

select @countryIdRO = country.countryid from country where countrycode = 'RO';
select @countryIdCH = country.countryid from country where countrycode = 'CH';

INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid],[lastModified],[lastModifiedBy])
     VALUES
           ('H','Malaysia',6.00,0,@countryIdMY,'2019-02-01',17280),
           ('I','Singapore',7.00,0,@countryIdSG,'2019-02-01',17280),
           ('J','Austria',20.00,0,@countryIdAT,'2019-02-01',17280),
           ('K','Belgium',21.00,0,@countryIdBE,'2019-02-01',17280),
           ('L','Brazil',25.00,0,@countryIdBR,'2019-02-01',17280),
		   
           ('M','Canada - No Specific VAT Rate',0.00,0,@countryIdCA,'2019-02-01',17280),
           ('O','Czech Republic',21.00,0,@countryIdCZ,'2019-02-01',17280),
           ('P','Italy',22.00,0,@countryIdIT,'2019-02-01',17280),
           ('Q','Malta',18.00,0,@countryIdMT,'2019-02-01',17280),
           ('R','Netherlands',21.00,0,@countryIdNL,'2019-02-01',17280),

INSERT INTO dbversion(version) VALUES(571);

GO

COMMIT TRAN