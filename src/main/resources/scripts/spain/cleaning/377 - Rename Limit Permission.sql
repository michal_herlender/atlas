use spain;

begin transaction;

/* QUOTATION CUSTOMER */
update dbo.permission set permission='QUOTATION_CUSTOMER_LOW_LIMIT' where permission='QUOTATION_LOW_LIMIT';
update dbo.permission set permission='QUOTATION_CUSTOMER_MEDIUM_LIMIT' where permission='QUOTATION_MEDIUM_LIMIT';
update dbo.permission set permission='QUOTATION_CUSTOMER_HIGH_LIMIT' where permission='QUOTATION_HIGH_LIMIT';
update dbo.permission set permission='QUOTATION_CUSTOMER_LIMITLESS_LIMIT' where permission='QUOTATION_LIMITLESS_LIMIT';

update dbo.permissionLimit set permissionName='QUOTATION_CUSTOMER_LOW_LIMIT' where permissionName='QUOTATION_LOW_LIMIT';
update dbo.permissionLimit set permissionName='QUOTATION_CUSTOMER_MEDIUM_LIMIT' where permissionName='QUOTATION_MEDIUM_LIMIT';
update dbo.permissionLimit set permissionName='QUOTATION_CUSTOMER_HIGH_LIMIT' where permissionName='QUOTATION_HIGH_LIMIT';

/* PURCHASE ORDER OPEX */
update dbo.permission set permission='PURCHASE_ORDER_OPEX_LOW_LIMIT' where permission='OPEX_LOW_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_OPEX_MEDIUM_LIMIT' where permission='OPEX_MEDIUM_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_OPEX_HIGH_LIMIT' where permission='OPEX_HIGH_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_OPEX_LIMITLESS' where permission='OPEX_PURCHASE_LIMITLESS';

update dbo.permissionLimit set permissionName='PURCHASE_ORDER_OPEX_LOW_LIMIT' where permissionName='OPEX_LOW_PURCHASE_LIMIT';
update dbo.permissionLimit set permissionName='PURCHASE_ORDER_OPEX_MEDIUM_LIMIT' where permissionName='OPEX_MEDIUM_PURCHASE_LIMIT';
update dbo.permissionLimit set permissionName='PURCHASE_ORDER_OPEX_HIGH_LIMIT' where permissionName='OPEX_HIGH_PURCHASE_LIMIT';

/* PURCHASE ORDER GENEX */
update dbo.permission set permission='PURCHASE_ORDER_GENEX_LOW_LIMIT' where permission='GENEX_LOW_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_GENEX_MEDIUM_LIMIT' where permission='GENEX_MEDIUM_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_GENEX_HIGH_LIMIT' where permission='GENEX_HIGH_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_GENEX_LIMITLESS' where permission='GENEX_PURCHASE_LIMITLESS';

update dbo.permissionLimit set permissionName='PURCHASE_ORDER_GENEX_LOW_LIMIT' where permissionName='GENEX_LOW_PURCHASE_LIMIT';
update dbo.permissionLimit set permissionName='PURCHASE_ORDER_GENEX_MEDIUM_LIMIT' where permissionName='GENEX_MEDIUM_PURCHASE_LIMIT';
update dbo.permissionLimit set permissionName='PURCHASE_ORDER_GENEX_HIGH_LIMIT' where permissionName='GENEX_HIGH_PURCHASE_LIMIT';

/* PURCHASE ORDER CAPEX */
update dbo.permission set permission='PURCHASE_ORDER_CAPEX_LOW_LIMIT' where permission='CAPEX_LOW_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_CAPEX_MEDIUM_LIMIT' where permission='CAPEX_MEDIUM_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_CAPEX_HIGH_LIMIT' where permission='CAPEX_HIGH_PURCHASE_LIMIT';
update dbo.permission set permission='PURCHASE_ORDER_CAPEX_LIMITLESS' where permission='CAPEX_PURCHASE_LIMITLESS';

update dbo.permissionLimit set permissionName='PURCHASE_ORDER_CAPEX_LOW_LIMIT' where permissionName='CAPEX_LOW_PURCHASE_LIMIT';
update dbo.permissionLimit set permissionName='PURCHASE_ORDER_CAPEX_MEDIUM_LIMIT' where permissionName='CAPEX_MEDIUM_PURCHASE_LIMIT';
update dbo.permissionLimit set permissionName='PURCHASE_ORDER_CAPEX_HIGH_LIMIT' where permissionName='CAPEX_HIGH_PURCHASE_LIMIT';

INSERT INTO dbversion(version) VALUES(377);

commit transaction;