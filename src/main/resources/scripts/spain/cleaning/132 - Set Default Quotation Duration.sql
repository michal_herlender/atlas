-- Sets default quote duration to 120 for all business companies except TIC (Bilbao coid 6578) which is set to 365
-- Updates duration of ESTIC quotations to 365 and updates expiry date for issued quotations to 365 days from issued date

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[businesscompanysettings]
   SET [defaultquotationduration] = 120
 WHERE [companyid] <> 6578;

GO

UPDATE [dbo].[businesscompanysettings]
   SET [defaultquotationduration] = 365
 WHERE [companyid] = 6578;

GO

UPDATE [dbo].quotation
   SET [duration] = 365
 WHERE [orgid] = 6578;

GO

UPDATE [dbo].quotation
   SET [expirydate] = DATEADD("dd", 365, [issuedate])
 WHERE [orgid] = 6578 and [issued] = 1;

GO

COMMIT TRAN


