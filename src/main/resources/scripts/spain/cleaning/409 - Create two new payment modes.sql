USE [spain]
GO

BEGIN TRAN

SET IDENTITY_INSERT [paymentmode] ON;
GO

INSERT INTO [dbo].[paymentmode]
           ([id],[code],[description])
     VALUES
           (10,'DD','Direct Debit'),
           (11,'WTF','Foreign Wire Transfer');
GO

SET IDENTITY_INSERT [paymentmode] OFF;
GO

INSERT INTO [dbo].[paymentmodetranslation]
           ([paymentmodeid]
           ,[locale]
           ,[translation])
     VALUES
           (10,'de_DE','Lastschrift'),
           (10,'en_GB','Direct Debit'),
           (10,'es_ES','D�bito directo'),
           (10,'fr_FR','Pr�l�vement'),
           (11,'de_DE','Auslands�berweisung'),
           (11,'en_GB','Foreign Wire Transfer'),
           (11,'es_ES','Transferencia bancaria internacional'),
           (11,'fr_FR','Virement �tranger');
GO

INSERT INTO dbversion(version) VALUES(409)
GO

COMMIT TRAN