-- Author: Tony Provost - 2016-11-22
-- Updates french translations for transportmethod

BEGIN TRAN 

USE [spain]
GO

UPDATE [dbo].[transportmethodtranslation] SET [translation] = 'Transporteur client' WHERE [id] = 3 AND [locale] = 'fr_FR'
UPDATE [dbo].[transportmethodtranslation] SET [translation] = 'Transporteur - Jour suivant' WHERE [id] = 5 AND [locale] = 'fr_FR'
UPDATE [dbo].[transportmethodtranslation] SET [translation] = 'Transporteur - Spécifique' WHERE [id] = 6 AND [locale] = 'fr_FR'

GO

COMMIT TRAN