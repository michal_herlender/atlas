BEGIN TRAN

CREATE NONCLUSTERED INDEX IDX_quotationitem_quoteid ON dbo.quotationitem
( quoteid ASC )

CREATE NONCLUSTERED INDEX IDX_quotationitem_headingid ON dbo.quotationitem
( heading_headingid ASC )

CREATE NONCLUSTERED INDEX IDX_quotationitem_plantid ON dbo.quotationitem
( plantid ASC )
INCLUDE
( servicetypeid )

CREATE NONCLUSTERED INDEX IDX_quotationitem_modelid ON dbo.quotationitem
( modelid ASC )
INCLUDE
( servicetypeid )

CREATE NONCLUSTERED INDEX IDX_quotationitem_servicetypeid ON dbo.quotationitem
( servicetypeid ASC )
INCLUDE
( plantid, modelid )

INSERT INTO dbversion VALUES (862)

COMMIT TRAN