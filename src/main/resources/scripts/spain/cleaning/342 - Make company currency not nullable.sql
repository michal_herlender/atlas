USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[company]
   SET [currencyid] = 3
 WHERE currencyid is null;
GO

/****** Object:  Index [IDX_company_currencyid]    Script Date: 10/23/2018 8:19:58 PM ******/
DROP INDEX [IDX_company_currencyid] ON [dbo].[company]
GO

ALTER TABLE [company] ALTER COLUMN currencyid int not null;

/****** Object:  Index [IDX_company_currencyid]    Script Date: 10/23/2018 8:19:58 PM ******/
CREATE NONCLUSTERED INDEX [IDX_company_currencyid] ON [dbo].[company]
(
	[currencyid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT INTO dbversion(version) VALUES(342);

COMMIT TRAN


