BEGIN TRAN

UPDATE dbo.template
SET templatepath = '/birt/docs/invoice_proforma.rptdesign'
WHERE name = 'Invoice Pro-Forma HTML'

ROLLBACK TRAN