-- Removes unused / now obsolete type column from model range, since inheritrance model has changed
-- This fixes a bug in the TML sync with creating new models

USE [atlas]

BEGIN TRAN

ALTER TABLE [modelrange] DROP column [type];

insert into dbversion(version) values(795);
GO

COMMIT TRAN