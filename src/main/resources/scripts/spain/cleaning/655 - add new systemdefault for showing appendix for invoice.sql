GO

BEGIN TRAN

DECLARE @groupid INT;
SELECT @groupid = [id] FROM [dbo].[systemdefaultgroup] WHERE [dname] = 'Client - Invoicing';


DECLARE @new_sd INT;
SELECT @new_sd = 57;

SET IDENTITY_INSERT [systemdefault] ON; 

INSERT INTO dbo.systemdefault(defaultid, defaultdatatype, defaultdescription, defaultname, defaultvalue, endd, [start], groupid, groupwide)
VALUES(@new_sd, 'boolean', 'Defines an option to print invoice with or without details', 
'Print appendix', 'true', 0, 0, @groupid, 0);

SET IDENTITY_INSERT [systemdefault] OFF; 

INSERT INTO dbo.systemdefaultnametranslation
(defaultid, locale, [translation])
VALUES(@new_sd, 'fr_FR', 'Imprimer l''annexe'),
      (@new_sd, 'en_GB', 'Print appendix'),
      (@new_sd, 'es_ES', 'Imprimir ap�ndice'),
      (@new_sd, 'de_DE', 'Anhang drucken');

INSERT INTO dbo.systemdefaultdescriptiontranslation
(defaultid, locale, [translation])
VALUES(@new_sd, 'fr_FR', 'D�finit une option pour imprimer la facture avec ou sans d�tails'),
      (@new_sd, 'en_GB', 'Defines an option to print invoice with or without details'),
      (@new_sd, 'es_ES', 'Define una opci�n para imprimir la factura con o sin detalles.'),
      (@new_sd, 'de_DE', 'Definiert eine Option zum Drucken einer Rechnung mit oder ohne Details');

INSERT INTO dbo.systemdefaultcorole(coroleid,defaultid)
VALUES(1,@new_sd);

INSERT INTO dbo.systemdefaultscope(scope, defaultid)
VALUES(0,@new_sd);

INSERT INTO dbversion(version) VALUES(655);

GO

COMMIT TRAN