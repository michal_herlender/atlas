BEGIN TRAN

UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_CAPEX_LIMIT_1' WHERE permissionName = 'PURCHASE_ORDER_CAPEX_LOW_LIMIT'
UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_CAPEX_LIMIT_2' WHERE permissionName = 'PURCHASE_ORDER_CAPEX_MEDIUM_LIMIT'
UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_CAPEX_LIMIT_3' WHERE permissionName = 'PURCHASE_ORDER_CAPEX_HIGH_LIMIT'

UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_GENEX_LIMIT_1' WHERE permissionName = 'PURCHASE_ORDER_GENEX_LOW_LIMIT'
UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_GENEX_LIMIT_2' WHERE permissionName = 'PURCHASE_ORDER_GENEX_MEDIUM_LIMIT'
UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_GENEX_LIMIT_3' WHERE permissionName = 'PURCHASE_ORDER_GENEX_HIGH_LIMIT'

UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_OPEX_LIMIT_1' WHERE permissionName = 'PURCHASE_ORDER_OPEX_LOW_LIMIT'
UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_OPEX_LIMIT_2' WHERE permissionName = 'PURCHASE_ORDER_OPEX_MEDIUM_LIMIT'
UPDATE permissionlimit SET permissionName = 'PURCHASE_ORDER_OPEX_LIMIT_3' WHERE permissionName = 'PURCHASE_ORDER_OPEX_HIGH_LIMIT'

UPDATE permissionlimit SET typePermission = 'PURCHASE_CAPEX' WHERE typePermission = 'PurchaseCapex'
UPDATE permissionlimit SET typePermission = 'PURCHASE_GENEX' WHERE typePermission = 'PurchaseGenex'
UPDATE permissionlimit SET typePermission = 'PURCHASE_OPEX' WHERE typePermission = 'PurchaseOpex'
UPDATE permissionlimit SET typePermission = 'QUOTATION' WHERE typePermission = 'Quotation'

SET IDENTITY_INSERT permissionlimit ON;

INSERT INTO permissionlimit(id, permissionName, typePermission, levelPermission) VALUES
(13, 'PURCHASE_ORDER_OPEX_LIMIT_4', 'PURCHASE_OPEX', 4),
(14, 'PURCHASE_ORDER_OPEX_LIMIT_5', 'PURCHASE_OPEX', 5),
(15, 'PURCHASE_ORDER_OPEX_LIMIT_6', 'PURCHASE_OPEX', 6),
(16, 'PURCHASE_ORDER_OPEX_LIMIT_7', 'PURCHASE_OPEX', 7),
(17, 'PURCHASE_ORDER_GENEX_LIMIT_4', 'PURCHASE_GENEX', 4),
(18, 'PURCHASE_ORDER_GENEX_LIMIT_5', 'PURCHASE_GENEX', 5),
(19, 'PURCHASE_ORDER_GENEX_LIMIT_6', 'PURCHASE_GENEX', 6),
(20, 'PURCHASE_ORDER_GENEX_LIMIT_7', 'PURCHASE_GENEX', 7),
(21, 'PURCHASE_ORDER_CAPEX_LIMIT_4', 'PURCHASE_CAPEX', 4),
(22, 'PURCHASE_ORDER_CAPEX_LIMIT_5', 'PURCHASE_CAPEX', 5),
(23, 'PURCHASE_ORDER_CAPEX_LIMIT_6', 'PURCHASE_CAPEX', 6),
(24, 'PURCHASE_ORDER_CAPEX_LIMIT_7', 'PURCHASE_CAPEX', 7)

SET IDENTITY_INSERT permissionlimit OFF;

UPDATE permission SET permission = 'PURCHASE_ORDER_CAPEX_LIMIT_1' WHERE permission = 'PURCHASE_ORDER_CAPEX_LOW_LIMIT'
UPDATE permission SET permission = 'PURCHASE_ORDER_CAPEX_LIMIT_2' WHERE permission = 'PURCHASE_ORDER_CAPEX_MEDIUM_LIMIT'
UPDATE permission SET permission = 'PURCHASE_ORDER_CAPEX_LIMIT_3' WHERE permission = 'PURCHASE_ORDER_CAPEX_HIGH_LIMIT'

UPDATE permission SET permission = 'PURCHASE_ORDER_GENEX_LIMIT_1' WHERE permission = 'PURCHASE_ORDER_GENEX_LOW_LIMIT'
UPDATE permission SET permission = 'PURCHASE_ORDER_GENEX_LIMIT_2' WHERE permission = 'PURCHASE_ORDER_GENEX_MEDIUM_LIMIT'
UPDATE permission SET permission = 'PURCHASE_ORDER_GENEX_LIMIT_3' WHERE permission = 'PURCHASE_ORDER_GENEX_HIGH_LIMIT'

UPDATE permission SET permission = 'PURCHASE_ORDER_OPEX_LIMIT_1' WHERE permission = 'PURCHASE_ORDER_OPEX_LOW_LIMIT'
UPDATE permission SET permission = 'PURCHASE_ORDER_OPEX_LIMIT_2' WHERE permission = 'PURCHASE_ORDER_OPEX_MEDIUM_LIMIT'
UPDATE permission SET permission = 'PURCHASE_ORDER_OPEX_LIMIT_3' WHERE permission = 'PURCHASE_ORDER_OPEX_HIGH_LIMIT'

DECLARE @limitId INT = 13;
DECLARE @autoUserId INT;

SELECT @autoUserId = personid FROM contact WHERE contact.firstname = 'ERP Auto User'

WHILE @limitId <= 24
BEGIN
	INSERT INTO limitCompany(maximum, orgid, permLimitId, lastModified, lastModifiedBy)
	(
		SELECT 0, coid, @limitId, '2022-01-12', @autoUserId
		FROM company
		WHERE corole = 5
		GROUP BY coid
	)
	SET @limitId = @limitId + 1
END

INSERT INTO dbversion(version) VALUES(857);

COMMIT TRAN