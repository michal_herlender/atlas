--a) there are 0 job items on the job
--b) there are 1 or more job services/expenses on the job
--c) such job services/expenses are all on invoices which have been issued

begin tran
declare @result int;
DECLARE @counter int;
set @counter=1

declare D_CURSOR CURSOR
FOR
SELECT job.jobid /*job.jobno,, COUNT(jobitem.jobitemid) jobitemcount, COUNT(jobexpenseitem.id) jobservicecount, invoiceitem.id*/
FROM job
LEFT JOIN jobitem ON job.jobid = jobitem.jobid
LEFT JOIN jobexpenseitem ON jobexpenseitem.job = job.jobid
LEFT JOIN invoiceitem ON jobexpenseitem.id = invoiceitem.expenseitem
WHERE job.statusid = 1
AND (
SELECT COUNT(*)
FROM jobitem
WHERE jobitem.jobid = job.jobid ) = 0
AND
(
SELECT COUNT(*)
FROM jobexpenseitem jei
WHERE jobexpenseitem.job = job.jobid ) > 0
AND
jobexpenseitem.id in
(
SELECT jobexpenseitem.id
FROM jobexpenseitem
inner JOIN invoiceitem ON jobexpenseitem.id = invoiceitem.expenseitem
inner JOIN invoice ON invoice.id = invoiceitem.invoiceid
WHERE invoice.issued=1 AND invoice.typeid IN (1,3,4,6) 
)
GROUP BY job.jobno,job.jobid, invoiceitem.id

OPEN D_CURSOR

FETCH D_CURSOR
INTO @result

While @@FETCH_STATUS = 0
BEGIN
set @counter = @counter + 1 

PRINT 'JOBID: ' + CONVERT(VARCHAR,@RESULT)
PRINT 'COUNT: ' + convert(VARCHAR,@counter)

UPDATE job SET job.statusid = 3 where job.jobid in (@result)

FETCH D_CURSOR
	INTO @result
END

CLOSE D_CURSOR
DEALLOCATE D_CURSOR

insert into dbversion values (837);

commit tran

