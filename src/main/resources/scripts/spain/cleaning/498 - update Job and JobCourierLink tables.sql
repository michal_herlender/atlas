USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.job ADD [storagearea] [varchar](255) NULL;
	ALTER TABLE dbo.job ADD [numberofpackages] [int] NULL;
	ALTER TABLE dbo.job ADD [packagetype] [varchar](255) NULL;
	ALTER TABLE dbo.[jobcourierlink] DROP COLUMN storagearea;
	ALTER TABLE dbo.[jobcourierlink] DROP COLUMN numberofpackages;
	ALTER TABLE dbo.[jobcourierlink] DROP COLUMN packagetype;

	INSERT INTO dbversion(version) VALUES(498);

COMMIT TRAN