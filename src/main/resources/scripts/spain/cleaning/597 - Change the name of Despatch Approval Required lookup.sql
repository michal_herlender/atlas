--	Change the name of lookup 59 'Lookup_DespatchApprovalRequired (inhouse return to client)'
USE [atlas]
GO

BEGIN TRAN

UPDATE dbo.lookupinstance 
SET lookup = 'Lookup_JobItemAtInstrumentAddress'
WHERE lookup = 'Lookup_DespatchApprovalRequired (inhouse return to client)'

INSERT INTO dbversion(version) VALUES (597);
COMMIT TRAN