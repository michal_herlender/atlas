USE [atlas]
GO

BEGIN TRAN

DECLARE @groupEngineerManagement int;

SELECT @groupEngineerManagement = [id] FROM [dbo].[permissiongroup] WHERE name = 'GROUP_ENGINEER_MANAGEMENT';

INSERT INTO [dbo].[permission]
           ([groupId]
           ,[permission])
     VALUES
           (@groupEngineerManagement,'JI_CERTIFICATE_SUPPLEMENTARY');

INSERT INTO dbversion(version) VALUES(735);

COMMIT TRAN


