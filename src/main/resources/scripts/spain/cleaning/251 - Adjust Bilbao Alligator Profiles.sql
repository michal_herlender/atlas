-- Creates 1 new production and 1 new test profile for Bilbao
-- Corrects 1 existing profile
-- Note, this depends on script 247/248 having been run, if you are running it locally
-- GB 2018-05-09 - as requested by Obed

USE [spain]
GO

BEGIN TRAN

-- 1.  BF update

UPDATE [dbo].[alligatorsettings]
   SET [uploadrawdatafolder] = '\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates_primarios',
       [uploadtempdatafolder] = '\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates'
 WHERE [description] = 'ES-TIC-BIL BF Production';

UPDATE [dbo].[alligatorsettings]
   SET [uploadrawdatafolder] = '\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates_primarios',
       [uploadtempdatafolder] = '\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\BAJA FRECUENCIA\TRABAJOS\CALIBRACION ELECTRICA\PLANTILLAS\Certificates'
 WHERE [description] = 'ES-TIC-BIL BF Test';

-- 2.  New Dimensional path

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL DIM Production'
           ,'\\tb-dc1\Datos\estructura\DST\metrologia\Dimensional\Trabajos\GESTION ARETXABALETA\PLANTILLAS INFORMES\DIMENSIONAL MODERNO\PARA ERP\certificates_primarios'
           ,'\\tb-dc1\Datos\estructura\DST\metrologia\Dimensional\Trabajos\GESTION ARETXABALETA\PLANTILLAS INFORMES\DIMENSIONAL MODERNO\PARA ERP\certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Production');
GO

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL DIM Test'
           ,'\\tb-dc1\Datos\estructura\DST\metrologia\Dimensional\Trabajos\GESTION ARETXABALETA\PLANTILLAS INFORMES\DIMENSIONAL MODERNO\PARA ERP\certificates_primarios'
           ,'\\tb-dc1\Datos\estructura\DST\metrologia\Dimensional\Trabajos\GESTION ARETXABALETA\PLANTILLAS INFORMES\DIMENSIONAL MODERNO\PARA ERP\certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Test');
GO

INSERT INTO dbversion (version) VALUES (251);

COMMIT TRAN