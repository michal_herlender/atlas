-- Removes the "prototype" change records created last year; 
-- with the new CDC (change data capture) implementation, there is no need for them, and they are deleted now from the codebase
-- Galen Beck 2021-03-15

USE [atlas]
GO

BEGIN TRAN

DROP TABLE [dbo].[change_instmodel]
GO

DROP TABLE [dbo].[change_mfr]
GO

DROP TABLE [dbo].[change_subfamily]
GO

INSERT INTO dbo.scheduledtask (active, classname, description, lastrunsuccess, methodname, quartzjobname, taskname)
VALUES 
		(0
		,'org.trescal.cwms.core.cdc.schedule.CdcSubscriberQueueScheduledJob'
        ,'Processes ChangeQueue records and creates SubscriberQueue records to be consumed by multiple external systems based on their subscriptions'
        ,0
		,'processChangeQueue'
        ,'jobDetail_CdcSubscriberQueueScheduledJob'
        ,'CDC SubscriberQueue Loading');

GO


INSERT INTO dbversion(version) VALUES(786);

COMMIT TRAN