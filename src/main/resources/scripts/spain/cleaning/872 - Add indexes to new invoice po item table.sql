BEGIN TRAN

CREATE NONCLUSTERED INDEX IDX_invoicepoitem_invoiceitemid ON dbo.[invoice_po_item]
( invoiceitemid ASC )

CREATE NONCLUSTERED INDEX IDX_invoicepoitem_invpoid ON dbo.[invoice_po_item]
( invpoid ASC )

INSERT INTO dbversion VALUES (872)

COMMIT TRAN