USE [atlas]
GO

BEGIN TRAN

DELETE FROM itemstatetranslation WHERE stateid BETWEEN 4120 AND 4159 AND locale = 'de_DE'

INSERT INTO itemstatetranslation(stateid, locale, translation) VALUES
(4120,'de_DE','Wartet auf Fertigstellung der zusätzlichen Anforderungen'),
(4121,'de_DE','Zusätzliche Anforderungen aufgezeichnet'),
(4122,'de_DE','Serviceleistung auf neuem Auftrag fortgeführt, wartet auf endgültige Preiskalkulation für ursprüngliche Auftrag-Position'),
(4123,'de_DE','Position erfordert Reparatur - wartet auf Preiskalkulation an Kunde'),
(4124,'de_DE','Reparatur-Preiskalkulation an Kunde ausgehändigt'),
(4125,'de_DE','Reparatur-Preiskalkulation  ausgehändigt - wartet auf Kundenentscheid wegen Reparatur'),
(4126,'de_DE','Kundenentscheid wegen Reparatur erhalten'),
(4127,'de_DE','Keine Preiskalkulation für Reparatur erforderlich'),
(4128,'de_DE','Wartet auf Überprüfung des Reparaturinspektion-Berichts durch Techniker'),
(4129,'de_DE','Reparaturinspektion-Bericht durch Techniker überprüft'),
(4130,'de_DE','Wartet auf Überprüfung des Reparaturinspektion-Berichts durch Kundenbetreuer'),
(4131,'de_DE','Reparaturinspektion-Bericht durch Kundenbetreuer überprüft'),
(4132,'de_DE','Reparaturinspektion-Bericht verschoben'),
(4133,'de_DE','Wartet auf Überprüfung Reparatur-Abschlussbericht'),
(4134,'de_DE','Reparatur-Abschlussbericht überprüft'),
(4135,'de_DE','Wartet auf Aktualisierung Reparatur-Abschlussbericht'),
(4136,'de_DE','Reparatur-Abschlussbericht aktualisiert'),
(4137,'de_DE','Wartet auf Reparatur-Abschlussbericht (nach Fremd-Bearbeitung)'),
(4138,'de_DE','Reparatur-Abschlussbericht  (nach Fremd-Bearbeitung) aktualisiert'),
(4139,'de_DE','Keine Bestellung für Intercompany-Bearbeitung erforderlich'),
(4141,'de_DE','Serviceleistung auf neuer Auftrag-Position fortgeführt, wartet auf Freigabe der Preiskalkulation für ursprüngliche Position'),
(4142,'de_DE','Freigabe-Preiskalkulation für ursprüngliche Auftrag-Position an Kunde ausgehändigt'),
(4143,'de_DE','Auftrag-Preiskalkulation für ursprüngliche Auftrag-Position ausgegeben'),
(4144,'de_DE','Serviceleistung auf neuer Auftrag-Position fortgeführt, ursprüngliche Auftrag-Position abgeschlossen'),
(4145,'de_DE','Wartet auf Ausführung Allgemeine Serviceleistung'),
(4146,'de_DE','Ausführung Allgemeine Serviceleistung'),
(4147,'de_DE','Wartet auf Hochladen eines Allgemeine-Serviceleistung-Dokuments'),
(4148,'de_DE','Dokument für Allgemeine Serviceleistung hochgeladen'),
(4149,'de_DE','Dokument für Allgemeine Serviceleistung nicht erforderlich'),
(4150,'de_DE','Wartet auf Versand Informations-Fehlerbericht'),
(4151,'de_DE','Versand Informations-Fehlerbericht nicht erforderlich'),
(4152,'de_DE','Informations-Fehlerbericht an Kunde versandt'),
(4153,'de_DE','Informations-Fehlerbericht an externes System verfügbar'),
(4154,'de_DE','Position aus internem Lieferschein gelöscht'),
(4155,'de_DE','Position aus internem Rücklieferschein gelöscht'),
(4156,'de_DE','Position aus Lieferschein gelöscht'),
(4157,'de_DE','Position aus Lieferschein an Fremddienstleister gelöscht'),
(4158,'de_DE','Position aus Lieferschein an Fremddienstleister nach Bestellung gelöscht'),
(4159,'de_DE','Vor-Ort - Fehlerbericht verfügbar')

INSERT INTO dbversion VALUES (596)

COMMIT TRAN