-- For ticket #1264

USE [spain]
GO

BEGIN TRAN

	-- 'Client - Certificate' Group => id 9 
	SET IDENTITY_INSERT [systemdefault] ON
	INSERT INTO spain.dbo.systemdefault (defaultid,defaultdatatype,defaultdescription,defaultname,defaultvalue,endd,[start],groupid,groupwide)
	VALUES (51,'boolean','Allocate certificate numbers for each work requirement in advance of calibrations (onsite jobs)','Reserve certificate (onsite jobs)','true',0,0,9,1);
	SET IDENTITY_INSERT [systemdefault] OFF
	GO
	
	-- 5 = business role
	INSERT INTO spain.dbo.systemdefaultcorole (coroleid,defaultid)
	VALUES (5,51);

	--subdiv scope
	INSERT INTO spain.dbo.systemdefaultscope ([scope],defaultid)
	VALUES (2,51);
	--company scope
	INSERT INTO spain.dbo.systemdefaultscope ([scope],defaultid)
	VALUES (0,51);

	INSERT INTO spain.dbo.systemdefaultnametranslation (defaultid,locale,[translation])
	VALUES (51,'en_GB','Reserve Certificate');
	INSERT INTO spain.dbo.systemdefaultnametranslation (defaultid,locale,[translation])
	VALUES (51,'es_ES','Reserve un Certificado');
	INSERT INTO spain.dbo.systemdefaultnametranslation (defaultid,locale,[translation])
	VALUES (51,'fr_FR','Réserver un certificat');
	--go
	
	INSERT INTO spain.dbo.systemdefaultdescriptiontranslation (defaultid,locale,[translation])
	VALUES (51,'en_GB','Allocate certificate numbers for each work requirement in advance of calibration (onsite jobs)') 
	INSERT INTO spain.dbo.systemdefaultdescriptiontranslation (defaultid,locale,[translation])
	VALUES (51,'es_ES','Asignar un número de certificado para cada requisito de trabajo antes de la calibración (trabajos en el sitio)') 
	INSERT INTO spain.dbo.systemdefaultdescriptiontranslation (defaultid,locale,[translation])
	VALUES (51,'fr_FR','Attribuer un numéro de certificat pour chaque exigence de travail avant l''étalonnage (jobs sur site)');
	--go

	--get 'TRESCAl SA' id
	DECLARE @trescalSaId int
	select @trescalSaId = c.coid
	from dbo.company c
	where c.coname = 'TRESCAL SA'
	--print @trescalSaId

	--get 'A+ METROLOGIE' id
	DECLARE @aPlusId int
	select @aPlusId = c.coid
	from dbo.company c
	where c.coname like 'A+ METROLOGIE'
	--print @aPlusId

	-- set 'reserver certificate' to no/false for Trescal SA and A+
	INSERT INTO dbo.systemdefaultapplication (defaultvalue,coid,defaultid,orgid,lastModified)
	VALUES ('false',@trescalSaId,51,@trescalSaId,GETDATE()) 

	if @aPlusId IS NOT NULL begin
		INSERT INTO dbo.systemdefaultapplication (defaultvalue,coid,defaultid,orgid,lastModified)
		VALUES ('false',@aPlusId,51,@aPlusId,GETDATE()) 
	end else begin
		print 'a+ metreologie not found'
	end
	
	INSERT INTO dbversion (version) VALUES (332);

COMMIT TRAN