/*
Missing Index Details from SQLQuery198.sql - WINDOWS-TLM0MO4\DEVELOPER.spain (WINDOWS-TLM0MO4\galen (162))
The Query Processor estimates that implementing the following index could improve the query cost by 45.5683%.
*/

USE [spain]
GO

BEGIN TRAN

CREATE NONCLUSTERED INDEX [IDX_jobitem_duedate_addrid_jobid_stateid]
ON [dbo].[jobitem] ([duedate])
INCLUDE ([addrid],[jobid],[stateid])
GO

INSERT INTO dbversion(version) VALUES(398);

COMMIT TRAN