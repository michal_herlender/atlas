-- Author Galen Beck - 2016-11-07
-- Inserts translations for action outcomes and item states
-- Based on similar action oucome values
-- Minor corrections made 2016-11-09 (to rerun) as action outcome translations for id 97 and 98 were reversed

USE [spain]
GO

BEGIN TRAN

-- Step 1 - Spanish Action Outcomes

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 94 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (94, 'es_ES', 'Esperando el pedido del cliente')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Esperando el pedido del cliente' WHERE id = 94 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 95 AND locale = 'es_ES')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (95, 'es_ES', 'Esperando la respuesta del cliente')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Esperando la respuesta del cliente' WHERE id = 95 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 96 AND locale = 'es_ES')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (96, 'es_ES', 'Ninguna acci�n requerida')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Ninguna acci�n requerida' WHERE id = 96 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 97 AND locale = 'es_ES')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (97, 'es_ES', 'Esperando env�a oferta al cliente')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Esperando env�a oferta al cliente' WHERE id = 97 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 98 AND locale = 'es_ES')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (98, 'es_ES', 'Requiere calibraci�n in situ')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Requiere calibraci�n in situ' WHERE id = 98 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 99 AND locale = 'es_ES')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (99, 'es_ES', 'Calibrado con �xito')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrado con �xito' WHERE id = 99 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 100 AND locale = 'es_ES')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (100, 'es_ES', 'Unidad B.E.R.')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Unidad B.E.R.' WHERE id = 100 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 101 AND locale = 'es_ES')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (101, 'es_ES', 'Calibraci�n puesta en espera')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibraci�n puesta en espera' WHERE id = 101 AND locale = 'es_ES';

-- Step 2 - French Action Outcomes

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 94 AND locale = 'fr_FR')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (94, 'fr_FR', 'En attente du bon de commande du client')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'En attente du bon de commande du client' WHERE id = 94 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 95 AND locale = 'fr_FR')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (95, 'fr_FR', 'En attente du retour client')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'En attente du retour client' WHERE id = 95 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 96 AND locale = 'fr_FR')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (96, 'fr_FR', 'Pas d''action n�cessaire')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Pas d''action n�cessaire' WHERE id = 96 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 97 AND locale = 'fr_FR')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (97, 'fr_FR', 'En attente de devis client')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'En attente de devis client' WHERE id = 97 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 98 AND locale = 'fr_FR')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (98, 'fr_FR', 'N�cessite un v�rification sur site')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'N�cessite un v�rification sur site' WHERE id = 98 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 99 AND locale = 'fr_FR')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (99, 'fr_FR', 'Calibr� avec succ�s')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibr� avec succ�s' WHERE id = 99 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 100 AND locale = 'fr_FR')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (100, 'fr_FR', 'instrument �conomiquement non r�parable')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'instrument �conomiquement non r�parable' WHERE id = 100 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 101 AND locale = 'fr_FR')
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (101, 'fr_FR', 'Calibration mise en attente')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibration mise en attente' WHERE id = 101 AND locale = 'fr_FR';

-- Step 3 - Spanish Item States

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4047 AND locale = 'es_ES') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4047, 'es_ES', 'Esperando la calibraci�n in situ')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Esperando la calibraci�n in situ' WHERE stateid = 4047 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4048 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4048, 'es_ES', 'Pre-calibraci�n in situ')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Pre-calibraci�n in situ' WHERE stateid = 4048 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4049 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4049, 'es_ES', 'Trabajo in situ se ha realizado - esperando el coste del trabajo')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Servicio in situ se ha realizado - esperando el coste del trabajo' WHERE stateid = 4049 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4050 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4050, 'es_ES', 'Coste del trabajo in situ enviado al cliente')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Coste del trabajo in situ enviado al cliente' WHERE stateid = 4050 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4051 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4051, 'es_ES', 'Trabajo in situ terminado')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Trabajo in situ terminado' WHERE stateid = 4051 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4054 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4054, 'es_ES', 'Esperando revisi�n del contrato in situ')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Esperando revisi�n del contrato in situ' WHERE stateid = 4054 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4055 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4055, 'es_ES', 'Revisi�n del contrato in situ')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revisi�n del contrato in situ' WHERE stateid = 4055 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4056 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4056, 'es_ES', 'Revisi�n del contrato in situ - esperando orden de compra del cliente')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revisi�n del contrato in situ - esperando orden de compra del cliente' WHERE stateid = 4056 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4057 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4057, 'es_ES', 'Revisi�n de contrato in situ - esperando la respuesta del cliente')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revisi�n de contrato in situ - esperando la respuesta del cliente' WHERE stateid = 4057 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4058 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4058, 'es_ES', 'No se realiz� ning�n trabajo in situ')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'No se realiz� ning�n trabajo in situ' WHERE stateid = 4058 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4059 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4059, 'es_ES', 'Revisi�n de contrato - esperando env�a oferta al cliente')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revisi�n de contrato - esperando env�a oferta al cliente' WHERE stateid = 4059 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4060 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4060, 'es_ES', 'Calibraci�n in situ espera')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Calibraci�n in situ espera' WHERE stateid = 4060 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4061 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4061, 'es_ES', 'Calibrado in situ, esperando el certificado')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Calibrado in situ, esperando el certificado' WHERE stateid = 4061 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4062 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4062, 'es_ES', 'Certificado emitido por calibraci�n in situ')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Certificado emitido por calibraci�n in situ' WHERE stateid = 4062 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4063 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4063, 'es_ES', 'Calibrado in situ. Certificado en espera de la firma de aprobaci�n')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Calibrado in situ. Certificado en espera de la firma de aprobaci�n' WHERE stateid = 4063 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4064 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4064, 'es_ES', 'Certificado firmado por calibraci�n in situ')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Certificado firmado por calibraci�n in situ' WHERE stateid = 4064 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4065 AND locale = 'es_ES')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4065, 'es_ES', 'T�rmino del certificado in situ a seguir')
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'T�rmino del certificado in situ a seguir' WHERE stateid = 4065 AND locale = 'es_ES';

--19   4047 = 'Awaiting onsite calibration', 
--26   4048 = 'Pre-onsite calibration',
--33   4049 = 'Recorded onsite service - awaiting job costing',
--34   4050 = 'Onsite job costing issued to client',
--54   4051 = 'Completed onsite service',

--7    4054 = 'Awaiting onsite contract review'
--8    4055 = 'Onsite contract review'
--9    4056 = 'Onsite contract review - awaiting client purchase order'
--10   4057 = 'Onsite contract review - awaiting client feedback'
--54?  4058 = 'No onsite service performed'
--4052 4059 = 'Onsite contract review - awaiting quotation' (to client?)
--176  4060 = 'Onsite calibration on hold'
--27   4061 = 'Onsite calibration - awaiting certificate'
--30   4062 = 'Onsite calibration - certificate issued'
--31   4063 = 'Onsite calibration - awaiting certificate signing'
--32   4064 = 'Onsite calibration - certificate signed'
--199  4065 = 'Onsite calibration - certificate completion to follow'

-- Step 4 - French Item States

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4052 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4052, 'fr_FR', 'Revue de contrat - en attente devis au client')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revue de contrat - en attente devis au client' WHERE stateid = 4052 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4053 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4053, 'fr_FR', 'En attente - en attente bon de commande')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente - en attente bon de commande' WHERE stateid = 4053 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4047 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4047, 'fr_FR', 'En attente de v�rification sur site')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de v�rification sur site' WHERE stateid = 4047 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4048 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4048, 'fr_FR', 'Pr� v�rification sur site')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Pr� v�rification sur site' WHERE stateid = 4048 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4049 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4049, 'fr_FR', 'Instrument �talonn� sur site - en attente de chiffrage')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument �talonn� sur site - en attente de chiffrage' WHERE stateid = 4049 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4050 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4050, 'fr_FR', 'Chiffrage sur site envoy� au client')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Chiffrage sur site envoy� au client' WHERE stateid = 4050 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4051 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4051, 'fr_FR', 'Travaux termin�s sur site')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Travaux termin�s sur site' WHERE stateid = 4051 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4054 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4054, 'fr_FR', 'En attente de revue de contrat sur site')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de revue de contrat sur site' WHERE stateid = 4054 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4055 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4055, 'fr_FR', 'Revue de contrat sur site')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revue de contrat sur site' WHERE stateid = 4055 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4056 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4056, 'fr_FR', 'Revue de contrat sur site - en attente bon de commande client')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revue de contrat sur site - en attente bon de commande client' WHERE stateid = 4056 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4057 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4057, 'fr_FR', 'Revue de contrat sur site - en attente retour client')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revue de contrat sur site - en attente retour client' WHERE stateid = 4057 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4058 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4058, 'fr_FR', 'Aucun travail sur site effectu�')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Aucun travail sur site effectu�' WHERE stateid = 4058 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4059 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4059, 'fr_FR', 'Revue de contrat - en attente devis au client')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revue de contrat sur site - en attente devis au client' WHERE stateid = 4059 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4060 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4060, 'fr_FR', 'V�rification sur site en attente')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'V�rification sur site en attente' WHERE stateid = 4060 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4061 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4061, 'fr_FR', 'V�rifi� sur site, en attente de certificat')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'V�rifi� sur site, en attente de certificat' WHERE stateid = 4061 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4062 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4062, 'fr_FR', 'Num�ro de certificat �mis pour v�rification sur site')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Num�ro de certificat �mis pour v�rification sur site' WHERE stateid = 4062 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4063 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4063, 'fr_FR', '�talonn� sur site, certificat en attente de validation')
ELSE UPDATE dbo.itemstatetranslation SET translation = '�talonn� sur site, certificat en attente de validation' WHERE stateid = 4063 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4064 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4064, 'fr_FR', 'Certificat sign� pour v�rification sur site')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Certificat sign� pour v�rification sur site' WHERE stateid = 4064 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4065 AND locale = 'fr_FR')
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4065, 'fr_FR', 'Finalisation du certificat sur site � suivre')
ELSE UPDATE dbo.itemstatetranslation SET translation = 'Finalisation du certificat sur site � suivre' WHERE stateid = 4065 AND locale = 'fr_FR';

COMMIT TRAN