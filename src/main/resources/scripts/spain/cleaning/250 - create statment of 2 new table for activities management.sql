USE [spain]
GO
	
	BEGIN TRAN
	 
	    CREATE TABLE spain.dbo.advesotransferableactivity (
		id int NOT NULL IDENTITY(1,1),
		istransferredtoadveso bit DEFAULT 0 NOT NULL,
		isdisplayedonjoborderdetails bit DEFAULT 0 NOT NULL,
		itemactivityid int,
		lastModified datetime2,
		lastModifiedBy int,
		CONSTRAINT advesotransferableactivity_PK PRIMARY KEY (id)
	)
	GO
	
	CREATE TABLE spain.dbo.advesooverriddenactivitysetting (
		id int NOT NULL IDENTITY(1,1),
		istransferredtoadveso bit DEFAULT 0 NOT NULL,
		isdisplayedonjoborderdetails bit DEFAULT 0 NOT NULL,
		advesotransferableactivityid int NOT NULL,
		businesssubdivisionsettingsid int NOT NULL,
		lastModified datetime2,
		lastModifiedBy int,
		CONSTRAINT advesooverriddenactivitysetting_PK PRIMARY KEY (id),
		CONSTRAINT advesooverriddenactivitysetting_businesssubdivisionsettings_FK FOREIGN KEY (businesssubdivisionsettingsid) REFERENCES spain.dbo.businesssubdivisionsettings(id),
		CONSTRAINT advesooverriddenactivitysetting_advesotransferableactivity_FK FOREIGN KEY (advesotransferableactivityid) REFERENCES spain.dbo.advesotransferableactivity(id)
	)
	GO
	
	insert into dbversion(version) values(250);

COMMIT TRAN