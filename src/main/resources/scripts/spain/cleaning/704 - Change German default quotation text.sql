USE [atlas]
GO

BEGIN TRAN

UPDATE quotationdocdefaulttext
SET
subject = 'Angebot zu Ihrer Anfrage',
body = 'wir bedanken uns für Ihre Anfrage und unterbreiten Ihnen nachfolgend unser Angebot.

Das Angebot wurde auf der Basis der uns vorliegenden Informationen erstellt. 
Eventuell auftretende Abweichungen preislicher Art können die Folge von fehlenden bzw. nicht exakten Angaben zum Kalibriergegenstand sein, Angebot freibleibend.

Unsere weiteren Dienstleistungen finden Sie auf unserer Webseite www.trescal.de oder rufen Sie uns an.

Mit freundlichen Grüßen'

WHERE locale= 'de_DE'

INSERT INTO dbversion VALUES (704)

COMMIT TRAN