USE [atlas]

BEGIN TRAN

ALTER TABLE dbo.certificate ADD certstatus varchar(30) NULL;
go

UPDATE certificate SET certificate.certstatus = (
select REPLACE(REPLACE(UPPER(bs.name),' - ','_'),' ','_')
from certificate c 
join basestatus bs on c.status = bs.statusid
where c.certid = certificate.certid
);

-- delete column
DROP INDEX IDX_certificate_status ON dbo.certificate;
go
ALTER TABLE dbo.certificate DROP CONSTRAINT FKbf7upif9a1t17ojbc3ngeub3e;
GO
ALTER TABLE dbo.certificate DROP COLUMN status; 
GO

-- delete cert statuses from basestatus

DELETE FROM dbo.basestatusdescriptiontranslation 
WHERE statusid in (SELECT DISTINCT statusid FROM dbo.basestatus WHERE type = 'certificate');

DELETE FROM dbo.basestatusnametranslation 
WHERE statusid in (SELECT DISTINCT statusid FROM dbo.basestatus WHERE type = 'certificate');

DELETE FROM dbo.basestatus WHERE type = 'certificate';
	
	
INSERT INTO dbversion(version) VALUES (640)

COMMIT TRAN