-- Ready to run contract permissions on production upgrade 2019-06-19, as contract features are integrated into application, e.g. contract review, etc...

use [spain];

begin transaction;

insert into permission(groupId, permission)
values
(1, 'CONTRACT_SEARCH'),
(1, 'CONTRACT_ADD');

INSERT INTO dbversion(version) VALUES(375);

commit transaction;