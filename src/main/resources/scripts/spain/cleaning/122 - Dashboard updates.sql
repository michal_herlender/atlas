ALTER TABLE department DROP CONSTRAINT FK328E435229C2ACE5
ALTER TABLE departmenttypetranslation DROP CONSTRAINT FK_9ooepkt96me52qurj3ga9iumv
ALTER TABLE workassignment DROP CONSTRAINT FK4EC66C9E8049236A

DELETE departmenttype;

INSERT INTO [spain].[dbo].[workassignment] (depttypeid, stategroupid) VALUES (6, 45)

UPDATE stategrouplink SET groupid = 45, type = 'ALLOCATED_NOTCURRENT_SUBDIV'
WHERE groupid = 36 and type = 'ALLOCATED_SUBDIV'