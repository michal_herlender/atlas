USE [atlas]
GO

BEGIN TRAN

	DECLARE @last_os_id int,
	        @last_ao_id int;

	UPDATE dbo.actionoutcome SET value = 'APPROVED' WHERE value IN ('COSTS_APPROVED','REPLACEMENT_QUOTE_ACCEPTED','INSPECTION_COSTS_APPROVED','TP_COSTS_APPROVED');
	UPDATE dbo.actionoutcome SET value = 'REJECTED' WHERE value IN ('COSTS_REJECTED','REPLACEMENT_QUOTE_REJECTED','INSPECTION_COSTS_REJECTED','TP_COSTS_REJECTED');
	UPDATE dbo.actionoutcome SET value = 'REJECTED_REDO' WHERE value IN ('COSTS_REJECTED_REDO_COSTING','REPLACEMENT_QUOTE_REJECTED_REDO_COSTING','INSPECTION_COSTS_APPROVED_REDO_COSTING','TP_COSTS_APPROVED_REDO_COSTING');

	UPDATE dbo.outcomestatus SET activityid = 82 WHERE activityid = 81 AND statusid = 79;
	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,	'Costing rejected - redo costing',	0,	82,	1,	'client_decision_costing',	'REJECTED_REDO');
	SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@last_ao_id, 'de_DE',	'Kosten abgelehnt - Kosten wiederholen'),
	(@last_ao_id,	'en_GB',	'Costing rejected - redo Costing'),
	(@last_ao_id,	'es_ES',	'Costo rechazado - rehacer Costo'),
	(@last_ao_id,	'fr_FR',	'Coût rejeté - refaire le Coût');

	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,	'Costing rejected - redo costing',	0,	43,	1,	'client_decision_costing',	'REJECTED_REDO');
	SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@last_ao_id,	'de_DE',	'Kosten abgelehnt - Kosten wiederholen'),
	(@last_ao_id,	'en_GB',	'Costing rejected - redo Costing'),
	(@last_ao_id,	'es_ES',	'Costo rechazado - rehacer Costo'),
	(@last_ao_id,	'fr_FR',	'Coût rejeté - refaire le Coût');

	UPDATE dbo.outcomestatus SET activityid = 94 WHERE activityid = 93 AND statusid = 91;
	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,	'Costing rejected - redo costing',	0,	94,	1,	'client_decision_tp_insp_costs',	'REJECTED_REDO');
	SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@last_ao_id,	'de_DE',	'Inspektions-Kosten abgelehnt - Kosten wiederholen'),
	(@last_ao_id,	'en_GB',	'Inspection costs rejected - redo costing'),
	(@last_ao_id,	'es_ES',	'Costos de inspección rechazados - rehacer Costo'),
	(@last_ao_id,	'fr_FR',	'Frais d''expertise refusés - refaire le Coût');
	
	UPDATE dbo.outcomestatus SET activityid = 107 WHERE activityid = 106 AND statusid = 104;
	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,	'Costing rejected - redo costing',	0,	107,	1,	'client_decision_tp_job_costing',	'REJECTED_REDO');
	SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@last_ao_id,	'de_DE',	'Kunde lehnt Angebot des Fremddienstleiters ab - Kosten wiederholen'),
	(@last_ao_id,	'en_GB',	'Client rejects third party costing - redo costing'),
	(@last_ao_id,	'es_ES',	'El cliente rechaza el costo de terceros - rehacer Costo'),
	(@last_ao_id,	'fr_FR',	'Le client refuse le devis externe - refaire le Coût');

	UPDATE dbo.outcomestatus SET activityid = 148 WHERE activityid = 147 AND statusid = 145;
	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,	'Costing rejected - redo costing',	0,	148,	1,	'client_decision_exchange',	'REJECTED_REDO');
	SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@last_ao_id,	'de_DE',	'Kunde hat Angebot für Ersatzbeschaffung abgelehnt - Kosten wiederholen'),
	(@last_ao_id,	'en_GB',	'Client rejected quote for replacement - redo costing'),
	(@last_ao_id,	'es_ES',	'Cliente rechaza prespuesto para remplazo - rehacer Costo'),
	(@last_ao_id,	'fr_FR',	'Offre de remplacement refusé par le client - refaire le Coût');


	UPDATE dbo.outcomestatus SET activityid = 4083 WHERE activityid = 4084 AND statusid = 4087;
	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,	'Costing rejected - redo costing',	0,	4083,	1,	'client_decision_costing',	'REJECTED_REDO');
	SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@last_ao_id,	'de_DE',	'Kosten abgelehnt - Kosten wiederholen'),
	(@last_ao_id,	'en_GB',	'Costing rejected - redo Costing'),
	(@last_ao_id,	'es_ES',	'Costo rechazado - rehacer Costo'),
	(@last_ao_id,	'fr_FR',	'Coût rejeté - refaire le Coût');

	INSERT INTO dbo.outcomestatus(defaultoutcome, activityid, lookupid, statusid) VALUES 
	(0, 181, NULL, 33);
	SELECT @last_os_id = MAX(id) FROM dbo.outcomestatus;
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage) VALUES(181, 66, @last_os_id, 'c', 2);
	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,	'Costing rejected - redo costing',	0,	181,	1,	'client_decision_costing',	'REJECTED_REDO');
	SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@last_ao_id,	'de_DE',	'Kosten abgelehnt - Kosten wiederholen'),
	(@last_ao_id,	'en_GB',	'Costing rejected - redo Costing'),
	(@last_ao_id,	'es_ES',	'Costo rechazado - rehacer Costo'),
	(@last_ao_id,	'fr_FR',	'Coût rejeté - refaire le Coût');

	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,	'Costing rejected - redo costing',	0,	4126,	1,	'client_decision_costing',	'REJECTED_REDO');
	SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@last_ao_id,	'de_DE',	'Kosten abgelehnt - Kosten wiederholen'),
	(@last_ao_id,	'en_GB',	'Costing rejected - redo Costing'),
	(@last_ao_id,	'es_ES',	'Costo rechazado - rehacer Costo'),
	(@last_ao_id,	'fr_FR',	'Coût rejeté - refaire le Coût');
	
	INSERT INTO dbversion(version) VALUES(507);

COMMIT TRAN