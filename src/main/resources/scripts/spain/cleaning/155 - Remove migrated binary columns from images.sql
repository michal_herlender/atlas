-- Removes now unused filedata, thumbdata columns from primary database
-- This data has ben migrated to secondary database e.g. spainfiles
-- Galen Beck - 2017-06-05

USE [spain]
GO

BEGIN TRAN

ALTER TABLE [dbo].[images] DROP COLUMN [filedata]
GO

ALTER TABLE [dbo].[images] DROP COLUMN [thumbdata]
GO

COMMIT TRAN

