USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.repairinspectionreport ADD jobitemid int;
ALTER TABLE dbo.repairinspectionreport ADD CONSTRAINT repairinspectionreport_jobitem_FK FOREIGN KEY (jobitemid) REFERENCES dbo.jobitem(jobitemid);

INSERT INTO dbversion (version) VALUES (433);

COMMIT TRAN