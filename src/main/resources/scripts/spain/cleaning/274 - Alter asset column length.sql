-- Expands asset number format
-- Assets being used for JSP migration... currently used at Antech.

USE [spain]

BEGIN TRAN

ALTER TABLE [dbo].asset ALTER COLUMN assetno varchar(30) not null;

INSERT INTO dbversion(version) VALUES(274);

COMMIT TRAN
