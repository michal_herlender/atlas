USE [spain]
GO

BEGIN TRAN

--add finaloutcome field : field choosed by csr
ALTER TABLE dbo.faultreport ADD finaloutcome varchar(100) NULL;

-- add a new group link to detect if we are at state 'Awaiting selection of Fault Report outcome'
INSERT INTO spain.dbo.stategrouplink (groupid,stateid,[type]) VALUES (54,4099,'ALLOCATED_SUBDIV');

--add hook for selecting the final outcome
INSERT INTO spain.dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,1,1,1,'select-fr-final-outcome',4105);
	
--disable selecting manually the failure report outcome
UPDATE spain.dbo.nextactivity SET manualentryallowed=0 WHERE statusid = 4099;

INSERT INTO dbversion(version) VALUES(469);

COMMIT TRAN