USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.company ALTER COLUMN companycode VARCHAR(6);
	INSERT INTO dbversion(version) VALUES(501);

COMMIT TRAN