USE [atlas]
GO

BEGIN TRAN

CREATE NONCLUSTERED INDEX [_dta_index_address_12_517576882__K1_18] ON [dbo].[address]
(
	[addrid] ASC
)
INCLUDE ( 	[subdivid]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_address_12_517576882__K18_K1] ON [dbo].[address]
(
	[subdivid] ASC,
	[addrid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE STATISTICS [_dta_stat_517576882_1_18] ON [dbo].[address]([addrid], [subdivid])

CREATE NONCLUSTERED INDEX [_dta_index_certificate_12_946102411__K1_K4] ON [dbo].[certificate]
(
	[certid] ASC,
	[certdate] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_certificatevalidation_12_2073264499__K7_K1] ON [dbo].[certificatevalidation]
(
	[certid] ASC,
	[id] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_certlink_12_1122103038__K5_K4] ON [dbo].[certlink]
(
	[jobitemid] ASC,
	[certid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_company_12_1682105033__K1] ON [dbo].[company]
(
	[coid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_instcertlink_12_404196490__K4_K5] ON [dbo].[instcertlink]
(
	[certid] ASC,
	[plantid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_instrument_12_740197687__K22_K1_K19] ON [dbo].[instrument]
(
	[coid] ASC,
	[plantid] ASC,
	[addressid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE STATISTICS [_dta_stat_740197687_19_22] ON [dbo].[instrument]([addressid], [coid])

CREATE NONCLUSTERED INDEX [_dta_index_onbehalfitem_12_1273771595__K6_K7] ON [dbo].[onbehalfitem]
(
	[coid] ASC,
	[jobitemid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_subdiv_12_1419868125__K1] ON [dbo].[subdiv]
(
	[subdivid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

INSERT INTO dbversion(version) VALUES(489);

COMMIT TRAN