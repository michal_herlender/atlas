-- Makes various address fields not nullable, sets default empty values
USE [spain]

BEGIN TRAN

UPDATE [address] SET addr1 = '' WHERE addr1 is null;
GO
UPDATE [address] SET addr2 = '' WHERE addr2 is null;
GO
UPDATE [address] SET addr3 = '' WHERE addr3 is null;
GO
UPDATE [address] SET county = '' WHERE county is null;
GO
UPDATE [address] SET postcode = '' WHERE postcode is null;
GO
UPDATE [address] SET town = '' WHERE town is null;
GO

-- For addr1 and others
DROP INDEX [_dta_index_address_7_517576882__K1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20] ON [dbo].[address]
DROP INDEX [_dta_index_address_7_517576882__K1_K18_K4_K5_K6_K13_K9_K12_K8] ON [dbo].[address]
DROP INDEX [_dta_index_address_7_517576882__K3_K18_K1_K17_K4_2_5_6_7_8_9_10_11_12_13_14_15_16_19_20] ON [dbo].[address]

DROP STATISTICS [dbo].[address].[_dta_stat_517576882_4_3];
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_4_18_3];
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_1_17_4];
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_1_17_3_4];
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_17_1_18_4];
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_1_18_3_17_4];
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_4_5_6_13_9_12_8_1];

-- For active and others
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_3_18]
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_3_1]
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_17_3]
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_17_18_3]

-- For county and others
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_9_13_17]
DROP STATISTICS [dbo].[address].[_dta_stat_517576882_1_17_9_13]
GO

ALTER TABLE [address] ALTER COLUMN [addr1] varchar(100) NOT NULL;
ALTER TABLE [address] ALTER COLUMN [addr2] varchar(100) NOT NULL;
ALTER TABLE [address] ALTER COLUMN [addr3] varchar(100) NOT NULL;
ALTER TABLE [address] ALTER COLUMN [active] tinyint NOT NULL;
ALTER TABLE [address] ALTER COLUMN [county] varchar(30) NOT NULL;
ALTER TABLE [address] ALTER COLUMN [postcode] varchar(10) NOT NULL;
ALTER TABLE [address] ALTER COLUMN [town] varchar(50) NOT NULL;
ALTER TABLE [address] ALTER COLUMN [countryid] int NOT NULL;

GO

INSERT INTO dbversion(version) VALUES(338);

COMMIT TRAN