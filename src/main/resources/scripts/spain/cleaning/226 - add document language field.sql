USE [spain]
BEGIN TRAN

alter table dbo.company
  add documentlanguage varchar(255);

INSERT INTO dbversion VALUES (226);

COMMIT TRAN

