USE [atlas]

BEGIN TRAN

DECLARE @lookup_67 INT,
		@lookup_79 INT,
		@lookup_os_1 INT,
		@new_lookup INT,
		@new_status INT,
		@new_activity_1 INT,
		@new_activity_2 INT,
		@new_activity_3 INT,
		@lookup_79_os_1 INT,
		@new_lookup_os_1 INT;

SELECT @lookup_67 = id FROM lookupinstance WHERE lookup = 'Lookup_AdditionalDemandsCompleted';
SELECT @lookup_79 = id FROM lookupinstance WHERE lookup = 'Lookup_PreDeliveryRequirements (after lab calibration - before costing check)';

INSERT INTO dbo.lookupinstance(lookup, lookupfunction) VALUES('Lookup_FailureReportsRequireDelivery', 55);
SELECT @new_lookup = MAX(id) FROM lookupinstance;

INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Awaiting informational failure report to be sent',1,0,'workstatus',NULL,NULL);
SELECT @new_status = MAX(stateid) FROM itemstate;

INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Informational failure report does not need to be sent',1,0,'workactivity',@new_lookup,NULL);
SELECT @new_activity_1 = MAX(stateid) FROM itemstate;

INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Informational failure report sent to client',1,0,'workactivity',@new_lookup,NULL);
SELECT @new_activity_2 = MAX(stateid) FROM itemstate;

INSERT INTO dbo.itemstate(description,active,retired,type,lookupid,actionoutcometype) 
VALUES('Informational failure report available to external system',1,0,'workactivity',@new_lookup,NULL);
SELECT @new_activity_3 = MAX(stateid) FROM itemstate;

-- update all links
INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,statusid,lookupid) 
VALUES(0,NULL,NULL,@new_lookup);
SELECT @lookup_79_os_1 = MAX(id) FROM dbo.outcomestatus;
SELECT @lookup_os_1 = id FROM dbo.outcomestatus WHERE lookupid = @lookup_79 AND activityid IS NULL;
UPDATE dbo.lookupresult SET outcomestatusid = @lookup_79_os_1 WHERE outcomestatusid = @lookup_os_1;

INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,statusid,lookupid) 
VALUES(0,NULL,@new_status,NULL);
SELECT @new_lookup_os_1 = MAX(id) FROM dbo.outcomestatus;
INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage) 
VALUES(NULL,@new_lookup,@new_lookup_os_1,NULL,0),
	  (NULL,@new_lookup,@lookup_os_1,NULL,1);

INSERT INTO dbo.nextactivity(manualentryallowed,statusid,activityid)
VALUES (1,@new_status,@new_activity_1),
	   (1,@new_status,@new_activity_2),
	   (1,@new_status,@new_activity_3);

-- add stategrouplinks
INSERT INTO dbo.stategrouplink(groupid,stateid,type)
VALUES (59,@new_activity_1,'ALLOCATED_SUBDIV'),
	   (59,@new_activity_2,'ALLOCATED_SUBDIV'),
	   (59,@new_activity_3,'ALLOCATED_SUBDIV'),
	   (57,@new_activity_3,'ALLOCATED_SUBDIV');

-- add translations
INSERT INTO dbo.itemstatetranslation(stateid,locale,translation)
VALUES	(@new_status, 'fr_FR', 'En attente d''envoi du constat d''anomalie informationnel'),
		(@new_status, 'en_GB', 'Awaiting informational failure report to be sent'),
		(@new_status, 'es_ES', 'Esperando para enviar el informe de fallo informativa'),
		(@new_status, 'de_DE', 'Warten auf das Senden des informativen Fehlerberichts'),
		(@new_activity_1, 'fr_FR', 'Le constat d''anomalie informationnel n''a pas besoin d''être envoyé'),
		(@new_activity_1, 'en_GB', 'Informational failure report does not need to be sent'),
		(@new_activity_1, 'es_ES', 'Informe de fallo informativa no necesita ser enviado'),
		(@new_activity_1, 'de_DE', 'Informativer Fehlerbericht muss nicht gesendet werden'),
		(@new_activity_2, 'fr_FR', 'Constat d''anomalie informationnel envoyé au client'),
		(@new_activity_2, 'en_GB', 'Informational failure report sent to client'),
		(@new_activity_2, 'es_ES', 'informe de fallo informativa enviada al cliente'),
		(@new_activity_2, 'de_DE', 'Informativer Fehlerbericht an Client gesendet'),
		(@new_activity_3, 'fr_FR', 'Constat d''anomalie informationnel disponible pour le système externe'),
		(@new_activity_3, 'en_GB', 'Informational failure report available to external system'),
		(@new_activity_3, 'es_ES', 'Informe de fallo informativa disponible para el sistema externo'),
		(@new_activity_3, 'de_DE', 'Der informative Fehlerbericht steht dem externen System zur Verfügung');

-- add new field on faultreport
ALTER TABLE dbo.faultreport ADD requiredeliverytoclient bit DEFAULT 0;

INSERT INTO dbversion(version) VALUES(527);


COMMIT TRAN