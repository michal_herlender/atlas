USE [atlas]
GO

BEGIN TRAN

PRINT 'Inserting into calibrationrange from modelrange'

INSERT INTO calibrationrange
           ([lastModified]
           ,[endd]
           ,[maxIsInfinite]
           ,[maxvalue]
           ,[minvalue]
           ,[start]
           ,[relatedpoint]
           ,[lastModifiedBy]
           ,[maxuomid]
           ,[uomid]
           ,[relateduomid]
		   ,[old_modelrangeid])
     SELECT
           [lastModified]
           ,[endd]
           ,[maxIsInfinite]
           ,[maxvalue]
           ,[minvalue]
           ,[start]
           ,[relatedpoint]
           ,[lastModifiedBy]
           ,[maxuomid]
           ,[uomid]
           ,[relateduomid]
		   ,[id] FROM modelrange WHERE modelrange.type='calibration'
GO

PRINT 'Updating calreq to repoint to calibrationrange from modelrange'

-- Update existing calreq which pointed to modelrange with the new calibrationrange data
-- This should update about many values as we've inserted (~1220) - possibly, there are some orphaned ranges
UPDATE calreq SET calibrationrangeid = 
	(SELECT [id] FROM calibrationrange WHERE calreq.rangeid = calibrationrange.old_modelrangeid) 
	WHERE calreq.rangeid IS NOT NULL;

GO

-- Migrate existing instrument ranges; expect ~20478 rows

PRINT 'Inserting into instrumentrange from modelrange'

INSERT INTO [dbo].[instrumentrange]
           ([lastModified]
           ,[endd]
           ,[maxIsInfinite]
           ,[maxvalue]
           ,[minvalue]
           ,[start]
           ,[lastModifiedBy]
           ,[maxuomid]
           ,[uomid]
           ,[plantid])
     SELECT
           [lastModified]
           ,[endd]
           ,[maxIsInfinite]
           ,[maxvalue]
           ,[minvalue]
           ,[start]
           ,[lastModifiedBy]
           ,[maxuomid]
           ,[uomid]
		   ,[plantid] FROM modelrange WHERE modelrange.type='instrument'

GO

PRINT 'Removing rangeid from calreq'

-- We no longer need the rangeid field in calreq (now we use calibrationrangeid)
ALTER TABLE dbo.calreq DROP CONSTRAINT IF EXISTS FK96y9mdpr985l0uvyqsnx9mx5y
DROP STATISTICS dbo.calreq._dta_stat_1654453118_10_4_1_14
DROP STATISTICS dbo.calreq._dta_stat_1654453118_7_9_10_12_4
DROP STATISTICS dbo.calreq._dta_stat_1654453118_7_9_10_13_14_4
DROP STATISTICS dbo.calreq._dta_stat_1654453118_4_1_14_13_10_7
DROP STATISTICS dbo.calreq._dta_stat_1654453118_12_4_1_7_9_10
DROP STATISTICS dbo.calreq._dta_stat_1654453118_4_13_14_1_7_9_10

DROP INDEX _dta_index_calreq_7_1654453118__K14_K13_K4_K1_K7_K9_K10_2_3_5_6_8 on calreq
DROP INDEX _dta_index_calreq_7_1654453118__K13_K4_K1_K14_2_3_5_6_7_8_9_10 on calreq
DROP INDEX _dta_index_calreq_7_1654453118__K4_K1_K12_2_3_5_6_7_8_9_10 on calreq
DROP INDEX _dta_index_calreq_7_1654453118__K4_K1_K12_K10_K7_K9_2_3_5_6_8 on calreq
DROP INDEX _dta_index_calreq_7_1654453118__K4_K1_K14_2_3_5_6_7_8_9_10 on calreq
DROP INDEX _dta_index_calreq_7_1654453118__K4_K14_K1_2_3_5_6_7_8_9_10 on calreq
DROP INDEX _dta_index_calreq_7_1654453118__K4_K11_K1_2_3_5_6_7_8_9_10 on calreq

DROP STATISTICS dbo.calreq._dta_stat_1654453118_10_2_7
DROP STATISTICS dbo.calreq._dta_stat_1654453118_7_2_9_10
DROP STATISTICS dbo.calreq._dta_stat_1654453118_10_4_1_12_7

DROP INDEX IDX_calreq_rangeid on calreq
GO
ALTER TABLE calreq DROP COLUMN rangeid;
GO

PRINT 'Deleting OLD modelrange data moved to calibrationrange'

-- Delete the OLD modelrange data which have been moved to calibrationrange (~1220 rows)
-- Must not retain, as it's not a model range...

DELETE FROM modelrange WHERE type='calibration'
GO

-- Delete the OLD modelrange data which have been moved to instrumentrange (~20478 rows)
-- Must not retain, as it's not a model range...

PRINT 'Deleting OLD modelrange data moved to instrumentrange'

DELETE FROM modelrange WHERE type='instrument'
GO

INSERT INTO dbversion(version) VALUES(772);

COMMIT TRAN