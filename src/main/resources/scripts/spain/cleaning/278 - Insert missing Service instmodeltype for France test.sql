-- Script to insert missing model type on france-test
-- Likely just needs to be run on France test DB, as this seems to have been done manually on Spain.
-- Galen Beck 2018-06-28

USE [spain]

BEGIN TRAN

PRINT 'Insert model type if needed'

SET IDENTITY_INSERT [modeltype] ON
GO

IF NOT EXISTS (SELECT 1 FROM dbo.modeltype where instmodeltypeid=9) INSERT INTO [dbo].[modeltype]
           (instmodeltypeid
		   ,[active]
           ,[baseunits]
           ,[defaulttype]
           ,[modeltypedesc]
           ,[modeltypename]
           ,[modules]
           ,[canselect]
           ,[capability]
           ,[salecategory]
           ,[undefined])
     VALUES
           (9, 1, 0, 0, 'A model that defines a service', 'Service', 0, 0, 0, 0, 0)
GO

SET IDENTITY_INSERT [modeltype] OFF
GO

PRINT 'Insert/Update model type names if needed'

IF NOT EXISTS (SELECT 1 FROM dbo.modeltypenametranslation WHERE instmodeltypeid = 9 AND locale = 'en_GB') INSERT INTO dbo.modeltypenametranslation (instmodeltypeid, locale, translation) VALUES (9, 'en_GB', 'Service') ELSE UPDATE dbo.modeltypenametranslation SET translation = 'Service' WHERE instmodeltypeid = 9 AND locale = 'en_GB';
IF NOT EXISTS (SELECT 1 FROM dbo.modeltypenametranslation WHERE instmodeltypeid = 9 AND locale = 'es_ES') INSERT INTO dbo.modeltypenametranslation (instmodeltypeid, locale, translation) VALUES (9, 'es_ES', 'Servicio') ELSE UPDATE dbo.modeltypenametranslation SET translation = 'Servicio' WHERE instmodeltypeid = 9 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.modeltypenametranslation WHERE instmodeltypeid = 9 AND locale = 'fr_FR') INSERT INTO dbo.modeltypenametranslation (instmodeltypeid, locale, translation) VALUES (9, 'fr_FR', 'Service') ELSE UPDATE dbo.modeltypenametranslation SET translation = 'Service' WHERE instmodeltypeid = 9 AND locale = 'fr_FR';

PRINT 'Insert/Update model type descs if needed'

IF NOT EXISTS (SELECT 1 FROM dbo.modeltypedesctranslation WHERE instmodeltypeid = 9 AND locale = 'en_GB') INSERT INTO dbo.modeltypedesctranslation (instmodeltypeid, locale, translation) VALUES (9, 'en_GB', 'A model that defines a service') ELSE UPDATE dbo.modeltypedesctranslation SET translation = 'A model that defines a service' WHERE instmodeltypeid = 9 AND locale = 'en_GB';
IF NOT EXISTS (SELECT 1 FROM dbo.modeltypedesctranslation WHERE instmodeltypeid = 9 AND locale = 'es_ES') INSERT INTO dbo.modeltypedesctranslation (instmodeltypeid, locale, translation) VALUES (9, 'es_ES', 'Un modelo que define un servicio') ELSE UPDATE dbo.modeltypedesctranslation SET translation = 'Un modelo que define un servicio' WHERE instmodeltypeid = 9 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.modeltypedesctranslation WHERE instmodeltypeid = 9 AND locale = 'fr_FR') INSERT INTO dbo.modeltypedesctranslation (instmodeltypeid, locale, translation) VALUES (9, 'fr_FR', 'Un mod�le qui d�finit un service') ELSE UPDATE dbo.modeltypedesctranslation SET translation = 'Un mod�le qui d�finit un service' WHERE instmodeltypeid = 9 AND locale = 'fr_FR';

GO

PRINT 'Insert version'

INSERT INTO dbversion(version) VALUES(278);

COMMIT TRAN
