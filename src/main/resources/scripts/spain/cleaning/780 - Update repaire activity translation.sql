

USE [atlas]
GO

BEGIN TRAN

declare @id int;

Select id from actionoutcome ao where ao.description= 'Unit not repaired - further calibration necessary'

select @id =id from actionoutcome ao where ao.description= 'Unit not repaired - further calibration necessary';

update actionoutcometranslation set translation='instrument non réparé - vérification supplémentaire nécessaire' where locale='fr_FR' and id=@id

insert into dbversion(version) values(780);

COMMIT TRAN