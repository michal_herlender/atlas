USE [atlas]

BEGIN TRAN

DECLARE @statid int;

select @statid = ist.stateid
from itemstate ist
where ist.description = 'Awaiting Repair Completion Report Validation';


INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (58,@statid,'ALLOCATED_SUBDIV')

INSERT INTO dbversion(version) VALUES(523);


COMMIT TRAN