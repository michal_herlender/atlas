-- Author Galen Beck 2016-12-12
-- Makes call off item multi-subdiv aware
-- Must be run after startup (startup creates new columns)

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[calloffitem]
   SET [orgid] = 
	(SELECT job.orgid FROM job 
	 LEFT JOIN jobitem on job.jobid = jobitem.jobid WHERE jobitem.jobitemid = calloffitem.offitem)
   WHERE [orgid] IS NULL

UPDATE [dbo].[calloffitem]
   SET [lastModified] = '2016-12-11'
   WHERE [lastModified] IS NULL

ALTER table dbo.calloffitem ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.calloffitem ALTER COLUMN orgid int NOT NULL;

ROLLBACK TRAN