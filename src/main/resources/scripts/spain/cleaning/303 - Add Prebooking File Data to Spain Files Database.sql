-- Script to create new table 'PrebookingFileData' in spainfiles database 

USE [spainfiles]
GO

BEGIN TRAN

CREATE TABLE dbo.PrebookingFileData (
	prebookingId int NOT NULL,
	filedata varbinary(max),
	CONSTRAINT PrebookingFileData_PK PRIMARY KEY (prebookingId)
)

GO

COMMIT TRAN