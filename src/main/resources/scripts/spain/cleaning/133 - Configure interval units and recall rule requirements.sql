
BEGIN TRAN;

UPDATE recallrule SET intervalunit = 'MONTH' WHERE intervalunit is NULL;

UPDATE recallrule SET recallrequirementtype = 'FULL_CALIBRATION' WHERE recallrequirementtype is NULL;

UPDATE instrument SET calintervalunit = 'MONTH' WHERE calintervalunit is NULL;

UPDATE calibrationtype SET recallRequirementType = 'FULL_CALIBRATION' WHERE recallRequirementtype is NULL;

UPDATE certificate SET intervalunit = 'MONTH' WHERE intervalunit is NULL;


ROLLBACK TRAN;

