USE [atlas]
GO

BEGIN TRAN

		ALTER TABLE dbo.notificationsystemqueue ADD lastModifiedBy INT;
		ALTER TABLE dbo.notificationsystemqueue ADD lastModified DATETIME2;

		INSERT INTO dbversion(version) VALUES(653);

COMMIT TRAN