-- Author Galen Beck - 2014-03-17
-- Updates onsite workflow to correct incorrect logic in testing whether certificate issued after onsite cal
-- OutcomeStatus 237 (cert was issued) was connected to ItemStatus 4061 (calibrated, awaiting certifiate)
-- OutcomeStatus 238 (cert not issued) was connected to lookup 77 (item requires further calibration)
-- Now will be:
-- OutcomeStatus 237 (cert was issued) connected to lookup 76 (certificate signed?)
-- OutcomeStatus 238 (cert not issued) connected to ItemStatus 4061 (calibrated, awaiting certifiate)

USE [spain]
GO

UPDATE [dbo].[outcomestatus]
   SET [lookupid] = 76
      ,[statusid] = null
 WHERE [id] = 237
GO

UPDATE [dbo].[outcomestatus]
   SET [lookupid] = null
      ,[statusid] = 4061
 WHERE [id] = 238
GO
