-- Author Tony Provost - 2016-11-08
-- Change french translations of the first 204 item states

USE [spain]
GO

BEGIN TRAN

ALTER table dbo.itemstatetranslation ALTER COLUMN translation varchar(200) NOT NULL;

IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 1 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (1, 'fr_FR', 'Instrument collect� chez le client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument collect� chez le client' WHERE stateid = 1 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 2 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (2, 'fr_FR', 'Instrument en transit depuis le client vers la subdivision de service') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en transit depuis le client vers la subdivision de service' WHERE stateid = 2 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 3 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (3, 'fr_FR', 'Instrument re�u au service logistique') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument re�u au service logistique' WHERE stateid = 3 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 4 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (4, 'fr_FR', 'Instrument en attente d''exp�dition vers la subdivision de service') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente d''exp�dition vers la subdivision de service' WHERE stateid = 4 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 5 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (5, 'fr_FR', 'Instrument exp�di� vers la subdivision de service') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument exp�di� vers la subdivision de service' WHERE stateid = 5 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 6 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (6, 'fr_FR', 'Instrument en transfert de la subdivision de r�ception vers la subdivision de service') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en transfert de la subdivision de r�ception vers la subdivision de service' WHERE stateid = 6 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 7 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (7, 'fr_FR', 'En attente de revue de job item') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de revue de job item' WHERE stateid = 7 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 8 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (8, 'fr_FR', 'Revue de job item en cours') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revue de job item en cours' WHERE stateid = 8 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 9 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (9, 'fr_FR', 'Revue de job item en cours - en attente du bon de commande client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revue de job item en cours - en attente du bon de commande client' WHERE stateid = 9 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 10 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (10, 'fr_FR', 'Revue de job item en cours - en attente d''un "feed back" client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Revue de job item en cours - en attente d''un "feed back" client' WHERE stateid = 10 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 11 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (11, 'fr_FR', 'En attente du formulaire CSDS du client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du formulaire CSDS du client' WHERE stateid = 11 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 12 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (12, 'fr_FR', 'Formulaire CSDS re�u du client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Formulaire CSDS re�u du client' WHERE stateid = 12 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 13 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (13, 'fr_FR', 'Instrument en attente de nettoyage') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente de nettoyage' WHERE stateid = 13 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 14 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (14, 'fr_FR', 'Instrument nettoy�') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument nettoy�' WHERE stateid = 14 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 15 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (15, 'fr_FR', 'En attente d''ach�vement du constat d''anomalie') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''ach�vement du constat d''anomalie' WHERE stateid = 15 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 16 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (16, 'fr_FR', 'Constat d''anomalie effectu�') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie effectu�' WHERE stateid = 16 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 17 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (17, 'fr_FR', 'En attente d''inspection par un technicien') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''inspection par un technicien' WHERE stateid = 17 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 18 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (18, 'fr_FR', 'Instrument inspect� par un technicien') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument inspect� par un technicien' WHERE stateid = 18 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 19 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (19, 'fr_FR', 'En attente de v�rification') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de v�rification' WHERE stateid = 19 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 20 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (20, 'fr_FR', 'En attente de r�paration en interne') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de r�paration en interne' WHERE stateid = 20 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 21 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (21, 'fr_FR', 'En attente de r�paration en sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de r�paration en sous-traitance' WHERE stateid = 21 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 22 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (22, 'fr_FR', 'En attente de v�rification en sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de v�rification en sous-traitance' WHERE stateid = 22 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 23 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (23, 'fr_FR', 'En attente d''ajustage') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''ajustage' WHERE stateid = 23 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 24 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (24, 'fr_FR', 'R�parations mineures effectu�es') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'R�parations mineures effectu�es' WHERE stateid = 24 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 25 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (25, 'fr_FR', 'Instrument inspect� par un technicien avant v�rification') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument inspect� par un technicien avant v�rification' WHERE stateid = 25 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 26 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (26, 'fr_FR', 'Pr� v�rification') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Pr� v�rification' WHERE stateid = 26 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 27 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (27, 'fr_FR', 'V�rifi�, en attente de certificat') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'V�rifi�, en attente de certificat' WHERE stateid = 27 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 28 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (28, 'fr_FR', 'Instrument �conomiquement non r�parable - valorisation ("job pricing") � effectuer pour validation client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument �conomiquement non r�parable - valorisation ("job pricing") � effectuer pour validation client' WHERE stateid = 28 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 29 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (29, 'fr_FR', 'Valorisation ("job pricing") suite � inspection envoy� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation ("job pricing") suite � inspection envoy� au client' WHERE stateid = 29 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 30 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (30, 'fr_FR', 'Num�ro de certificat �mis') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Num�ro de certificat �mis' WHERE stateid = 30 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 31 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (31, 'fr_FR', 'Certificat en attente de validation') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Certificat en attente de validation' WHERE stateid = 31 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 32 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (32, 'fr_FR', 'Certificat sign�') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Certificat sign�' WHERE stateid = 32 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 33 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (33, 'fr_FR', 'Instrument �talonn� - en attente de valorisation ("job pricing") pour le client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument �talonn� - en attente de valorisation ("job pricing") pour le client' WHERE stateid = 33 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 34 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (34, 'fr_FR', 'Valorisation ("job pricing") envoy�e au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation ("job pricing") envoy�e au client' WHERE stateid = 34 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 35 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (35, 'fr_FR', 'Instrument v�rifi� - en attente de la valorisation ("job pricing") de confirmation du prix') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument v�rifi� - en attente de la valorisation ("job pricing") de confirmation du prix' WHERE stateid = 35 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 36 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (36, 'fr_FR', 'Valorisation de confirmation des prix ("job pricing") envoy� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation de confirmation des prix ("job pricing") envoy� au client' WHERE stateid = 36 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 37 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (37, 'fr_FR', 'Instrument n�cessitant une r�paration / un ajustage - constat d''anomalie � produire') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument n�cessitant une r�paration / un ajustage - constat d''anomalie � produire' WHERE stateid = 37 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 38 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (38, 'fr_FR', 'Constat d''anomalie R�paration / ajustage termin� ') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie R�paration / ajustage termin� ' WHERE stateid = 38 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 39 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (39, 'fr_FR', 'Instrument d�fectueux - en attente de valorisation ("job pricing") des travaux pour le client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument d�fectueux - en attente de valorisation ("job pricing") des travaux pour le client' WHERE stateid = 39 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 40 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (40, 'fr_FR', 'Instrument n�cessitant un ajustage - en attente de valorisation ("job pricing") des travaux pour le client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument n�cessitant un ajustage - en attente de valorisation ("job pricing") des travaux pour le client' WHERE stateid = 40 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 41 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (41, 'fr_FR', 'Valorisation des travaux de r�paration / ajustage ("job pricing") envoy� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation des travaux de r�paration / ajustage ("job pricing") envoy� au client' WHERE stateid = 41 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 42 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (42, 'fr_FR', 'En attente de la r�ponse client suite � envoi de la valorisation ("job pricing") des travaux pour r�paration / ajustage') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de la r�ponse client suite � envoi de la valorisation ("job pricing") des travaux pour r�paration / ajustage' WHERE stateid = 42 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 43 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (43, 'fr_FR', 'R�ponse client re�ue') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'R�ponse client re�ue' WHERE stateid = 43 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 44 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (44, 'fr_FR', 'En attente de l''�tiquette de "retour" sans travaux') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de l''�tiquette de "retour" sans travaux' WHERE stateid = 44 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 45 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (45, 'fr_FR', '�tiquette de "retour sans travaux" jointe � l''instrument') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = '�tiquette de "retour sans travaux" jointe � l''instrument' WHERE stateid = 45 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 46 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (46, 'fr_FR', 'En attente d''exp�dition vers un sous-traitance Trescal') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''exp�dition vers un sous-traitance Trescal' WHERE stateid = 46 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 47 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (47, 'fr_FR', 'Instrument exp�di� vers une sous-traitance Trescal') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument exp�di� vers une sous-traitance Trescal' WHERE stateid = 47 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 48 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (48, 'fr_FR', 'Instrument en transit vers une sous-traitance Trescal') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en transit vers une sous-traitance Trescal' WHERE stateid = 48 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 49 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (49, 'fr_FR', 'Instrument re�u par la soci�t� Trescal en charge de la sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument re�u par la soci�t� Trescal en charge de la sous-traitance' WHERE stateid = 49 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 50 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (50, 'fr_FR', 'En attente du bon de livraison du client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du bon de livraison du client' WHERE stateid = 50 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 51 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (51, 'fr_FR', 'Instrument ajout� au bon de livraison') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument ajout� au bon de livraison' WHERE stateid = 51 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 52 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (52, 'fr_FR', 'Instrument en attente d''exp�dition retour vers client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente d''exp�dition retour vers client' WHERE stateid = 52 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 53 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (53, 'fr_FR', 'Instrument retourn� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument retourn� au client' WHERE stateid = 53 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 54 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (54, 'fr_FR', 'Travaux termin�s, Instrument retourn� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Travaux termin�s, Instrument retourn� au client' WHERE stateid = 54 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 55 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (55, 'fr_FR', 'Date de livraison demand�e au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Date de livraison demand�e au client' WHERE stateid = 55 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 56 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (56, 'fr_FR', 'Date de livraison convenue avec le client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Date de livraison convenue avec le client' WHERE stateid = 56 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 57 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (57, 'fr_FR', 'R�paration en cours') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'R�paration en cours' WHERE stateid = 57 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 58 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (58, 'fr_FR', 'Instrument r�par� - en attente de valorisarion des travaux ("job pricing")') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument r�par� - en attente de valorisarion des travaux ("job pricing")' WHERE stateid = 58 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 59 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (59, 'fr_FR', 'Valorisation des travaux de r�paration ("job pricing") envoy� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation des travaux de r�paration ("job pricing") envoy� au client' WHERE stateid = 59 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 60 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (60, 'fr_FR', 'En attente d''une post-v�rification') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''une post-v�rification' WHERE stateid = 60 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 61 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (61, 'fr_FR', 'Post-v�rification en cours') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Post-v�rification en cours' WHERE stateid = 61 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 62 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (62, 'fr_FR', 'En attente de pi�ces de rechange') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de pi�ces de rechange' WHERE stateid = 62 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 63 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (63, 'fr_FR', 'Pi�ces de rechange re�ues') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Pi�ces de rechange re�ues' WHERE stateid = 63 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 64 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (64, 'fr_FR', 'En attente de composants � remplacer') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de composants � remplacer' WHERE stateid = 64 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 65 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (65, 'fr_FR', 'Composants de remplacement re�us') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Composants de remplacement re�us' WHERE stateid = 65 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 66 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (66, 'fr_FR', 'En attente de r�paration') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de r�paration' WHERE stateid = 66 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 67 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (67, 'fr_FR', 'Composants de remplacement install�s') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Composants de remplacement install�s' WHERE stateid = 67 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 68 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (68, 'fr_FR', 'R�glage en cours') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'R�glage en cours' WHERE stateid = 68 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 69 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (69, 'fr_FR', 'En attente d''approbation pour le certificat "en l''�tat"') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''approbation pour le certificat "en l''�tat"' WHERE stateid = 69 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 70 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (70, 'fr_FR', 'D�cision client re�ue pour le certificat "en l''�tat"') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'D�cision client re�ue pour le certificat "en l''�tat"' WHERE stateid = 70 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 71 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (71, 'fr_FR', 'En attente de certificat "en l''�tat"') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de certificat "en l''�tat"' WHERE stateid = 71 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 72 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (72, 'fr_FR', 'Num�ro de certificat "en l''�tat" �mis') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Num�ro de certificat "en l''�tat" �mis' WHERE stateid = 72 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 73 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (73, 'fr_FR', 'Instrument �conomiquement non r�parable - en attente de valorisation des travaux de v�rification pour le client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument �conomiquement non r�parable - en attente de valorisation des travaux de v�rification pour le client' WHERE stateid = 73 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 74 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (74, 'fr_FR', 'Valorisation des travaux de v�rification ("job pricing") envoy�e au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation des travaux de v�rification ("job pricing") envoy�e au client' WHERE stateid = 74 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 75 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (75, 'fr_FR', 'Instrument d�fectueux, en attente de d�cision client pour suite � donner') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument d�fectueux, en attente de d�cision client pour suite � donner' WHERE stateid = 75 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 76 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (76, 'fr_FR', 'D�cision client re�ue pour suite � donner') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'D�cision client re�ue pour suite � donner' WHERE stateid = 76 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 77 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (77, 'fr_FR', 'Ajustage en �chec - en attente de r�alisation du constat d''anomalie') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Ajustage en �chec - en attente de r�alisation du constat d''anomalie' WHERE stateid = 77 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 78 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (78, 'fr_FR', 'Constat d''anomalie effectu� suite � �chec d''ajustage') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie effectu� suite � �chec d''ajustage' WHERE stateid = 78 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 79 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (79, 'fr_FR', 'En attente de valorisation des travaux pour le client suite � �chec d''ajustage') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de valorisation des travaux pour le client suite � �chec d''ajustage' WHERE stateid = 79 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 80 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (80, 'fr_FR', 'Valorisation des travaux ("job pricing") envoy�e au client apr�s �chec de l''ajustage') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation des travaux ("job pricing") envoy�e au client apr�s �chec de l''ajustage' WHERE stateid = 80 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 81 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (81, 'fr_FR', 'En attente de d�cision client suite � valorisation des travaux envoy�e apr�s �chec de l''ajustage') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de d�cision client suite � valorisation des travaux envoy�e apr�s �chec de l''ajustage' WHERE stateid = 81 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 82 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (82, 'fr_FR', 'D�cision client re�ue concernant la valorisation des travaux apr�s �chec de l''ajustage') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'D�cision client re�ue concernant la valorisation des travaux apr�s �chec de l''ajustage' WHERE stateid = 82 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 83 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (83, 'fr_FR', 'Statut forc� par l''utilisateur') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Statut forc� par l''utilisateur' WHERE stateid = 83 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 84 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (84, 'fr_FR', 'Client param�tr� "bloqu�"') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Client param�tr� "bloqu�"' WHERE stateid = 84 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 85 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (85, 'fr_FR', 'En attente de paiement (apr�s "d�blocage" du statut du client)') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de paiement (apr�s "d�blocage" du statut du client)' WHERE stateid = 85 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 86 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (86, 'fr_FR', 'Paiement re�u (apr�s "d�blocage" du statut du client)') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Paiement re�u (apr�s "d�blocage" du statut du client)' WHERE stateid = 86 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 87 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (87, 'fr_FR', 'Client param�tr� "fiable"') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Client param�tr� "fiable"' WHERE stateid = 87 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 88 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (88, 'fr_FR', 'Instrument nettoy�') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument nettoy�' WHERE stateid = 88 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 89 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (89, 'fr_FR', 'En attente de sp�cification des travaux de sous-traitance attendus') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de sp�cification des travaux de sous-traitance attendus' WHERE stateid = 89 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 90 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (90, 'fr_FR', 'Travaux du sous-traitance enregistr�s') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Travaux du sous-traitance enregistr�s' WHERE stateid = 90 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 91 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (91, 'fr_FR', 'Instrument � envoyer au sous-traitant pour devis - en attente de valorisation du prix du devis') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument � envoyer au sous-traitant pour devis - en attente de valorisation du prix du devis' WHERE stateid = 91 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 92 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (92, 'fr_FR', 'Valorisation du prix du devis de sous-traitance envoy� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation du prix du devis de sous-traitance envoy� au client' WHERE stateid = 92 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 93 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (93, 'fr_FR', 'En attente d''approbation par le client du prix du devis de sous-traitance ') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''approbation par le client du prix du devis de sous-traitance ' WHERE stateid = 93 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 94 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (94, 'fr_FR', 'D�cision client re�ue concernant le prix du devis de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'D�cision client re�ue concernant le prix du devis de sous-traitance' WHERE stateid = 94 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 95 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (95, 'fr_FR', 'Instrument � retourner au client, en attente de la valorisation finale des travaux ("job pricing")') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument � retourner au client, en attente de la valorisation finale des travaux ("job pricing")' WHERE stateid = 95 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 96 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (96, 'fr_FR', 'Valorisation�finale des travaux ("job pricing")�envoy�e au�client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation�finale des travaux ("job pricing")�envoy�e au�client' WHERE stateid = 96 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 97 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (97, 'fr_FR', 'En attente du bon de livraison vers le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du bon de livraison vers le sous-traitant' WHERE stateid = 97 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 98 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (98, 'fr_FR', 'Ajout� au bon de livraison du sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Ajout� au bon de livraison du sous-traitant' WHERE stateid = 98 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 99 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (99, 'fr_FR', 'En attente d''exp�dition vers le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''exp�dition vers le sous-traitant' WHERE stateid = 99 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 100 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (100, 'fr_FR', 'Instrument exp�di� au sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument exp�di� au sous-traitant' WHERE stateid = 100 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 101 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (101, 'fr_FR', 'En attente d''un devis du sous-traitant pour les travaux � r�aliser') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''un devis du sous-traitant pour les travaux � r�aliser' WHERE stateid = 101 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 102 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (102, 'fr_FR', 'Devis re�u du sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Devis re�u du sous-traitant' WHERE stateid = 102 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 103 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (103, 'fr_FR', 'Instrument envoy� au sous-traitant, en attente du devis de travaux') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument envoy� au sous-traitant, en attente du devis de travaux' WHERE stateid = 103 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 104 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (104, 'fr_FR', 'En attente du calcul des co�ts de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du calcul des co�ts de sous-traitance' WHERE stateid = 104 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 105 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (105, 'fr_FR', 'Valorisation du prix des travaux de sous-traitance ("job pricing") envoy� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation du prix des travaux de sous-traitance ("job pricing") envoy� au client' WHERE stateid = 105 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 106 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (106, 'fr_FR', 'En attente de l''approbation du client suite � envoi de la valorisation ("job pricing") des travaux de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de l''approbation du client suite � envoi de la valorisation ("job pricing") des travaux de sous-traitance' WHERE stateid = 106 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 107 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (107, 'fr_FR', 'D�cision client re�ue concernant la valorisation des travaux de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'D�cision client re�ue concernant la valorisation des travaux de sous-traitance' WHERE stateid = 107 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 108 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (108, 'fr_FR', 'Pas de valorisation requise pour engager les travaux de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Pas de valorisation requise pour engager les travaux de sous-traitance' WHERE stateid = 108 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 109 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (109, 'fr_FR', 'En attente de la cr�ation de la commande d''achat au sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de la cr�ation de la commande d''achat au sous-traitant' WHERE stateid = 109 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 110 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (110, 'fr_FR', 'En attente de la commande d''achat pour la prestation d''inspection r�alis�e par le sous-traitant suite au refus par le client des travaux propos�s par le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de la commande d''achat pour la prestation d''inspection r�alis�e par le sous-traitant suite au refus par le client des travaux propos�s par le sous-traitant' WHERE stateid = 110 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 111 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (111, 'fr_FR', 'Commande d''achat pass�e au sous-traitant pour ses prestations d''inspection') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Commande d''achat pass�e au sous-traitant pour ses prestations d''inspection' WHERE stateid = 111 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 112 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (112, 'fr_FR', 'En attente du retour de l''instrument de chez le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du retour de l''instrument de chez le sous-traitant' WHERE stateid = 112 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 113 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (113, 'fr_FR', 'Instrument revenu du sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument revenu du sous-traitant' WHERE stateid = 113 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 114 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (114, 'fr_FR', 'En attente de la valorisation ("job pricing") finale pour le client, suite � son refus des travaux de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de la valorisation ("job pricing") finale pour le client, suite � son refus des travaux de sous-traitance' WHERE stateid = 114 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 115 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (115, 'fr_FR', 'Commande d''achat cr��e pour les travaux de sous traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Commande d''achat cr��e pour les travaux de sous traitance' WHERE stateid = 115 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 116 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (116, 'fr_FR', 'En attente du bon de livraison vers le sous-traitant apr�s envoi de la commande d''achat ') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du bon de livraison vers le sous-traitant apr�s envoi de la commande d''achat ' WHERE stateid = 116 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 117 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (117, 'fr_FR', 'Instrument ajout� au bon de livraison vers le sous-traitant apr�s envoi de la commande d''achat') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument ajout� au bon de livraison vers le sous-traitant apr�s envoi de la commande d''achat' WHERE stateid = 117 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 118 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (118, 'fr_FR', 'En attente d''exp�dition au sous-traitant apr�s envoi de la commande d''achat') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''exp�dition au sous-traitant apr�s envoi de la commande d''achat' WHERE stateid = 118 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 119 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (119, 'fr_FR', 'Instrument exp�di� au sous-traitant apr�s envoi de la commande d''achat') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument exp�di� au sous-traitant apr�s envoi de la commande d''achat' WHERE stateid = 119 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 120 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (120, 'fr_FR', 'Instrument en sous-traitance pour r�alisation des travaux') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en sous-traitance pour r�alisation des travaux' WHERE stateid = 120 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 121 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (121, 'fr_FR', 'Instrument demandant un engagement de frais suppl�mentaires par le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument demandant un engagement de frais suppl�mentaires par le sous-traitant' WHERE stateid = 121 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 122 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (122, 'fr_FR', 'En attente de valorisation ("job pricing") des frais suppl�mentaires annonc�s par le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de valorisation ("job pricing") des frais suppl�mentaires annonc�s par le sous-traitant' WHERE stateid = 122 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 123 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (123, 'fr_FR', 'Valorisation ("job pricing") des frais suppl�mentaires annonc�s par le sous-traitant envoy�e au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation ("job pricing") des frais suppl�mentaires annonc�s par le sous-traitant envoy�e au client' WHERE stateid = 123 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 124 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (124, 'fr_FR', 'En attente de la confirmation par le client de la valorisation des travaux suppl�mentaires de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de la confirmation par le client de la valorisation des travaux suppl�mentaires de sous-traitance' WHERE stateid = 124 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 125 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (125, 'fr_FR', 'D�cision du client re�ue concernant la valorisation des travaux suppl�mentaires de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'D�cision du client re�ue concernant la valorisation des travaux suppl�mentaires de sous-traitance' WHERE stateid = 125 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 126 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (126, 'fr_FR', 'En attente de creation de la commande d''achat au sous-traitant pour les travaux suppl�mentaires annonc�s') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de creation de la commande d''achat au sous-traitant pour les travaux suppl�mentaires annonc�s' WHERE stateid = 126 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 127 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (127, 'fr_FR', 'Commande d''achat envoy�e au sous-traitant pour les travaux suppl�mentaires annonc�s') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Commande d''achat envoy�e au sous-traitant pour les travaux suppl�mentaires annonc�s' WHERE stateid = 127 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 128 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (128, 'fr_FR', 'En attente du retour de l''instrument de chez le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du retour de l''instrument de chez le sous-traitant' WHERE stateid = 128 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 129 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (129, 'fr_FR', 'Instrument re�u en retour de sous-traitance au service logistique') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument re�u en retour de sous-traitance au service logistique' WHERE stateid = 129 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 130 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (130, 'fr_FR', 'En attente du certificat du sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du certificat du sous-traitant' WHERE stateid = 130 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 131 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (131, 'fr_FR', 'Certificat du sous-traitant ajout�') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Certificat du sous-traitant ajout�' WHERE stateid = 131 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 132 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (132, 'fr_FR', 'En attente de contr�le qualit� "post sous-traitance" par un technicien Trescal') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de contr�le qualit� "post sous-traitance" par un technicien Trescal' WHERE stateid = 132 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 133 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (133, 'fr_FR', 'Contr�le "post sous-traitance" effectu� par un technicien Trescal') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Contr�le "post sous-traitance" effectu� par un technicien Trescal' WHERE stateid = 133 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 134 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (134, 'fr_FR', 'Instrument en retour de sous-traitance non r�par�, en attente de mise � jour du constat d''anomalie') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en retour de sous-traitance non r�par�, en attente de mise � jour du constat d''anomalie' WHERE stateid = 134 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 135 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (135, 'fr_FR', 'Constat d''anomalie mis � jour') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie mis � jour' WHERE stateid = 135 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 136 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (136, 'fr_FR', 'Constat d''anomalie mis � jour, en attente de la valorisation des travaux ("job pricing")') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie mis � jour, en attente de la valorisation des travaux ("job pricing")' WHERE stateid = 136 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 137 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (137, 'fr_FR', 'Valorisation ("job pricing") des travaux envoy�e au client apr�s retour de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation ("job pricing") des travaux envoy�e au client apr�s retour de sous-traitance' WHERE stateid = 137 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 138 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (138, 'fr_FR', 'En attente de d�cision client concernant la valorisartion des travaux apr�s retour de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de d�cision client concernant la valorisartion des travaux apr�s retour de sous-traitance' WHERE stateid = 138 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 139 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (139, 'fr_FR', 'D�cision client re�ue � propos de la valorisation des travaux apr�s retour de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'D�cision client re�ue � propos de la valorisation des travaux apr�s retour de sous-traitance' WHERE stateid = 139 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 140 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (140, 'fr_FR', 'Instrument � remplacer par Trescal, en attente de valorisation finale') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument � remplacer par Trescal, en attente de valorisation finale' WHERE stateid = 140 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 141 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (141, 'fr_FR', 'Instrument �conomiquement non r�parable - en attente de la valorisation final des travaux') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument �conomiquement non r�parable - en attente de la valorisation final des travaux' WHERE stateid = 141 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 142 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (142, 'fr_FR', 'Valorisation finale des travaux envoy�e au client pour instrument "�conomiquement non r�parable"') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation finale des travaux envoy�e au client pour instrument "�conomiquement non r�parable"' WHERE stateid = 142 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 143 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (143, 'fr_FR', 'Instrument mis au rebut chez le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument mis au rebut chez le sous-traitant' WHERE stateid = 143 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 144 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (144, 'fr_FR', 'Instrument "�conomiquement non r�parable" - Devis sous-traitant re�u pour remplacement') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument "�conomiquement non r�parable" - Devis sous-traitant re�u pour remplacement' WHERE stateid = 144 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 145 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (145, 'fr_FR', 'En attente de la valorisation ("job pricing") au client pour �change/remplacement d''instrument') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de la valorisation ("job pricing") au client pour �change/remplacement d''instrument' WHERE stateid = 145 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 146 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (146, 'fr_FR', 'Valorisation pour �change/remplacement de l''instrument envoy� au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation pour �change/remplacement de l''instrument envoy� au client' WHERE stateid = 146 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 147 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (147, 'fr_FR', 'En attente de confirmation client suite � envoi de la valorisation pour �change/remplacement') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de confirmation client suite � envoi de la valorisation pour �change/remplacement' WHERE stateid = 147 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 148 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (148, 'fr_FR', 'D�cision client re�ue concernant la valorisation de l''�change/remplacement') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'D�cision client re�ue concernant la valorisation de l''�change/remplacement' WHERE stateid = 148 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 149 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (149, 'fr_FR', 'En attentedu retour d''instrument "cass�" de chez le sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attentedu retour d''instrument "cass�" de chez le sous-traitant' WHERE stateid = 149 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 150 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (150, 'fr_FR', 'Instrument "cass�" r�ceptionn�e en logistique en retour de sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument "cass�" r�ceptionn�e en logistique en retour de sous-traitance' WHERE stateid = 150 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 151 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (151, 'fr_FR', 'Instrument � remplacer - en attente de la modification de la commande d''achat au sous-traitant') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument � remplacer - en attente de la modification de la commande d''achat au sous-traitant' WHERE stateid = 151 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 152 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (152, 'fr_FR', 'Commande d''achat au sous-traitant modifi�e') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Commande d''achat au sous-traitant modifi�e' WHERE stateid = 152 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 153 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (153, 'fr_FR', 'En attente d''ajout au job de l''instrument remplac�') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''ajout au job de l''instrument remplac�' WHERE stateid = 153 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 154 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (154, 'fr_FR', 'instrument de remplacement ajout� au job') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'instrument de remplacement ajout� au job' WHERE stateid = 154 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 155 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (155, 'fr_FR', 'Instrument remplac� - en attente de la valorisation ("job pricing") finale des travaux') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument remplac� - en attente de la valorisation ("job pricing") finale des travaux' WHERE stateid = 155 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 156 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (156, 'fr_FR', 'Impossible de d�terminer le statut correct') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Impossible de d�terminer le statut correct' WHERE stateid = 156 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 157 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (157, 'fr_FR', 'Instrument mis en statut d''attente') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument mis en statut d''attente' WHERE stateid = 157 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 158 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (158, 'fr_FR', 'Instrument en attente - en attente de la r�paration d''un autre instrument') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente - en attente de la r�paration d''un autre instrument' WHERE stateid = 158 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 159 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (159, 'fr_FR', 'Instrument en attente - en attente de pi�ces d�tach�es du fournisseur') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente - en attente de pi�ces d�tach�es du fournisseur' WHERE stateid = 159 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 160 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (160, 'fr_FR', 'Instrument en attente - en attente de la r�paration par un autre service') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente - en attente de la r�paration par un autre service' WHERE stateid = 160 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 161 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (161, 'fr_FR', 'Instrument en attente - en attente d''une expertise') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente - en attente d''une expertise' WHERE stateid = 161 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 162 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (162, 'fr_FR', 'Instrument en attente - batteries en charge') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente - batteries en charge' WHERE stateid = 162 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 163 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (163, 'fr_FR', 'Instrument sorti du statut d''attente') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument sorti du statut d''attente' WHERE stateid = 163 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 164 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (164, 'fr_FR', 'Pas de valorisation des travaux ("job pricing") n�cessaire') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Pas de valorisation des travaux ("job pricing") n�cessaire' WHERE stateid = 164 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 165 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (165, 'fr_FR', 'Date de livraison requise') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Date de livraison requise' WHERE stateid = 165 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 166 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (166, 'fr_FR', 'Date de livraison valid�e') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Date de livraison valid�e' WHERE stateid = 166 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 167 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (167, 'fr_FR', 'En attente du paiement du client pour d�bloquer l''exp�dition') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente du paiement du client pour d�bloquer l''exp�dition' WHERE stateid = 167 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 168 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (168, 'fr_FR', 'Paiement re�u') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Paiement re�u' WHERE stateid = 168 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 169 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (169, 'fr_FR', 'Client "fiable"') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Client "fiable"' WHERE stateid = 169 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 170 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (170, 'fr_FR', 'En attente d''exp�dition vers une soci�t� Trescal pour sous-traitance de v�rification') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''exp�dition vers une soci�t� Trescal pour sous-traitance de v�rification' WHERE stateid = 170 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 171 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (171, 'fr_FR', 'Exp�di� vers une soci�t� Trescal pour sous-traitance de v�rification') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Exp�di� vers une soci�t� Trescal pour sous-traitance de v�rification' WHERE stateid = 171 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 172 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (172, 'fr_FR', 'En transit vers une soci�t� Trescal pour sous-traitance de v�rification') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En transit vers une soci�t� Trescal pour sous-traitance de v�rification' WHERE stateid = 172 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 173 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (173, 'fr_FR', 'Re�u par la soci�t� Trescal de sous-traitance pour v�rification') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Re�u par la soci�t� Trescal de sous-traitance pour v�rification' WHERE stateid = 173 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 174 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (174, 'fr_FR', 'Aucune action n�cessaire') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Aucune action n�cessaire' WHERE stateid = 174 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 175 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (175, 'fr_FR', 'Aucun nettoyage n�cessaire') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Aucun nettoyage n�cessaire' WHERE stateid = 175 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 176 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (176, 'fr_FR', 'Pr�-v�rification en attente') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Pr�-v�rification en attente' WHERE stateid = 176 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 177 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (177, 'fr_FR', 'Post-v�rification en attente') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Post-v�rification en attente' WHERE stateid = 177 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 178 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (178, 'fr_FR', 'Contact client �tabli pour d�cision relative au certificat') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Contact client �tabli pour d�cision relative au certificat' WHERE stateid = 178 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 179 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (179, 'fr_FR', 'Instrument en attente - autre raison') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente - autre raison' WHERE stateid = 179 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 180 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (180, 'fr_FR', 'Valorisation des travaux envoy�e - en attente d''approbation client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation des travaux envoy�e - en attente d''approbation client' WHERE stateid = 180 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 181 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (181, 'fr_FR', 'R�ponse client re�ue') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'R�ponse client re�ue' WHERE stateid = 181 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 182 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (182, 'fr_FR', 'Valorisation des travaux refus�e par le client- n�cessite une valorisation des frais d''inspection') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation des travaux refus�e par le client- n�cessite une valorisation des frais d''inspection' WHERE stateid = 182 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 183 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (183, 'fr_FR', 'Valorisation des frais d''inspection envoy�e au client') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation des frais d''inspection envoy�e au client' WHERE stateid = 183 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 184 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (184, 'fr_FR', 'En attente d''approbation par le client de la valorisation des frais d''inspection envoy�e') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente d''approbation par le client de la valorisation des frais d''inspection envoy�e' WHERE stateid = 184 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 185 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (185, 'fr_FR', 'Approbation client non n�cessaire') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Approbation client non n�cessaire' WHERE stateid = 185 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 186 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (186, 'fr_FR', 'Approbation re�ue') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Approbation re�ue' WHERE stateid = 186 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 187 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (187, 'fr_FR', 'Approbation non n�cessaire') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Approbation non n�cessaire' WHERE stateid = 187 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 188 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (188, 'fr_FR', 'En attente de valorisation ("job pricing") apr�s travaux sous-traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de valorisation ("job pricing") apr�s travaux sous-traitance' WHERE stateid = 188 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 189 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (189, 'fr_FR', 'Valorisation des travaux ("Job pricing") envoy�e au client apr�s r�alisation des travaux sous-trait�s') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Valorisation des travaux ("Job pricing") envoy�e au client apr�s r�alisation des travaux sous-trait�s' WHERE stateid = 189 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 190 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (190, 'fr_FR', 'Client exige une mise � jour ???') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Client exige une mise � jour ???' WHERE stateid = 190 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 191 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (191, 'fr_FR', 'En attente de retour vers la subdivision de r�f�rence pour inspection') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente de retour vers la subdivision de r�f�rence pour inspection' WHERE stateid = 191 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 192 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (192, 'fr_FR', 'Renvoy� � la subdivision de r�f�rence pour inspection ') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Renvoy� � la subdivision de r�f�rence pour inspection ' WHERE stateid = 192 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 193 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (193, 'fr_FR', 'En transit pour retour vers la subdivision de r�f�rence pour inspection') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En transit pour retour vers la subdivision de r�f�rence pour inspection' WHERE stateid = 193 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 194 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (194, 'fr_FR', 'R�ceptionn� � la subdivision de r�f�rence pour inspection') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'R�ceptionn� � la subdivision de r�f�rence pour inspection' WHERE stateid = 194 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 195 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (195, 'fr_FR', 'En attente�de l''inspection�finale') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'En attente�de l''inspection�finale' WHERE stateid = 195 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 196 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (196, 'fr_FR', 'Inspection finale effectu�e') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Inspection finale effectu�e' WHERE stateid = 196 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 197 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (197, 'fr_FR', 'Instrument �conomiquement non r�parable - en attente de r�alisation du constat d''anomalie') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument �conomiquement non r�parable - en attente de r�alisation du constat d''anomalie' WHERE stateid = 197 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 198 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (198, 'fr_FR', 'Constat d''anomalie pour instrument �conomiquement non r�parable termin�') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie pour instrument �conomiquement non r�parable termin�' WHERE stateid = 198 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 199 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (199, 'fr_FR', 'Certificat demeur� "ouvert" pour enregistrer d''autres v�rifications � venir') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Certificat demeur� "ouvert" pour enregistrer d''autres v�rifications � venir' WHERE stateid = 199 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 200 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (200, 'fr_FR', 'Instrument n�cessitant une r�paration / un ajustage - en attente du constat d''anomalie') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument n�cessitant une r�paration / un ajustage - en attente du constat d''anomalie' WHERE stateid = 200 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 201 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (201, 'fr_FR', 'Constat d''anomalie pour r�paration / ajustage termin�') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie pour r�paration / ajustage termin�' WHERE stateid = 201 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 202 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (202, 'fr_FR', '�chec de la r�paration en sous-traitance, en attente de la mise � jour du constat d''anomalie') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = '�chec de la r�paration en sous-traitance, en attente de la mise � jour du constat d''anomalie' WHERE stateid = 202 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 203 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (203, 'fr_FR', 'Constat d''anomalie mis � jour') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Constat d''anomalie mis � jour' WHERE stateid = 203 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 204 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (204, 'fr_FR', 'Instrument en attente - en attente d''approbation d''un autre instrument') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Instrument en attente - en attente d''approbation d''un autre instrument' WHERE stateid = 204 AND locale = 'fr_FR';

COMMIT TRAN