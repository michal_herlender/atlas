USE [spain]
BEGIN TRAN

create table dbo.subdiv_contract (
                                   contractid int not null,
                                   subdivid int not null
);

alter table dbo.subdiv_contract
  add constraint FK_subdivcontractlinktable_subdiv
    foreign key (subdivid)
      references dbo.subdiv;

alter table dbo.subdiv_contract
  add constraint FK_subdivcontractlinktable_contract
    foreign key (contractid)
      references dbo.contract;

INSERT INTO dbversion(version) VALUES(373);

commit transaction