USE [atlas]

BEGIN TRAN


    create table dbo.change_instmodel (
       changeid int not null,
        changetype int not null,
        descriptionid int,
        fullmodelname varchar(1000),
        model varchar(100),
        modelmfrtype varchar(255) not null,
        modelid int,
        modeltypeid int,
        quarantined bit not null,
        tmlid int,
        primary key (changeid)
    );

    create table dbo.change_mfr (
       changeid int not null,
        changetype int not null,
        active tinyint,
        genericmfr tinyint,
        mfrid int,
        name nvarchar(100),
        tmlid int,
        primary key (changeid)
    );

    create table dbo.change_queue (
       id int identity not null,
        entitytype int not null,
        processed bit not null,
        primary key (id)
    );

    create table dbo.change_subfamily (
       changeid int not null,
        changetype int not null,
        active bit,
        description nvarchar(100),
        tmlid int,
        primary key (changeid)
    );

    create table dbo.event_subscriber (
       id int identity not null,
        model varchar(100) not null,
        primary key (id)
    );

    create table dbo.event_subscription (
       id int identity not null,
        entitytype int not null,
        subscriberid int not null,
        primary key (id)
    );

    create table dbo.subscriber_queue (
       id int identity not null,
        processed bit not null,
        changeid int not null,
        subscriberid int not null,
        primary key (id)
    );

	create index IDX_change_queue_processed on dbo.change_queue (processed);

	create index IDX_subscriber_queue_processed_subscriberid on dbo.subscriber_queue (processed, subscriberid);

    alter table dbo.change_instmodel 
       add constraint FK_change_instmodel_changeid 
       foreign key (changeid) 
       references dbo.change_queue;

    alter table dbo.change_mfr 
       add constraint FK_change_mfr_changeid 
       foreign key (changeid) 
       references dbo.change_queue;

    alter table dbo.change_subfamily 
       add constraint FK_change_subfamily_changeid 
       foreign key (changeid) 
       references dbo.change_queue;

    alter table dbo.event_subscription 
       add constraint FK_event_subscription_subscriberid 
       foreign key (subscriberid) 
       references dbo.event_subscriber;

    alter table dbo.subscriber_queue 
       add constraint FK_subscriber_queue_changeid 
       foreign key (changeid) 
       references dbo.change_queue;

    alter table dbo.subscriber_queue 
       add constraint FK_subscriber_queue_subscriberid 
       foreign key (subscriberid) 
       references dbo.event_subscriber;

	INSERT INTO dbversion(version) VALUES (634)

-- Note - the following commands (in order) would remove all tables (in case we change/remove the design)
	
--drop table dbo.subscriber_queue;
--drop table dbo.event_subscription;
--drop table dbo.event_subscriber;
--drop table dbo.change_instmodel;
--drop table dbo.change_mfr;
--drop table dbo.change_subfamily;
--drop table dbo.change_queue;

COMMIT TRAN