-- Creates new table quotationdocdefaulttext to store default text for quotation
--  (currently global by language)
-- Removes and replaces the table quotationdoccustomtext to store custom text for
--  a specific quotation
-- Galen Beck 2018-12-06

USE [spain]

BEGIN TRAN

if exists (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'[dbo].[quotationdoccustomtext]') )
DROP TABLE [dbo].[quotationdoccustomtext]

if exists (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'[dbo].[quotationdocdefaulttext]') )
DROP TABLE [dbo].[quotationdocdefaulttext]

GO

create table dbo.quotationdoccustomtext (
    lastModified datetime2,
    body nvarchar(2000) not null,
    subject nvarchar(100) not null,
    quotation int not null,
    lastModifiedBy int,
    primary key (quotation)
);

create table dbo.quotationdocdefaulttext (
    id int identity not null,
    lastModified datetime2,
    body nvarchar(2000) not null,
    locale varchar(255) not null,
    subject nvarchar(100) not null,
    lastModifiedBy int,
    primary key (id)
);

alter table dbo.quotationdoccustomtext 
    add constraint FKc0wq2c6mk30ytnhhnsqgo6uch 
    foreign key (lastModifiedBy) 
    references dbo.contact;

alter table dbo.quotationdoccustomtext 
    add constraint FKt4comkso756erg5i8rtdsnd2l 
    foreign key (quotation) 
    references dbo.quotation;

alter table dbo.quotationdocdefaulttext 
    add constraint FKdli94a7yyi6o244q9xlm2xj38 
    foreign key (lastModifiedBy) 
    references dbo.contact;

INSERT INTO dbversion(version) VALUES(362);

COMMIT TRAN