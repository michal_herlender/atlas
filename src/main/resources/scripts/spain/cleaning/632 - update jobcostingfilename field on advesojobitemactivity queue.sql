USE [atlas]
GO

BEGIN TRAN

	UPDATE dbo.advesojobItemactivityqueue 
	SET jobcostingfilename = SUBSTRING(jobcostingfilename,0,CHARINDEX('&nbsp;', jobcostingfilename))
	WHERE jobcostingfilename IS NOT NULL AND CHARINDEX('&nbsp;', jobcostingfilename) > 0;

	INSERT INTO dbversion(version) VALUES (632);

COMMIT TRAN