USE [atlas]
GO

BEGIN TRAN

	INSERT INTO dbo.permission(groupId, permission)
	VALUES (3 ,'ADMIN_EDIT_TRANSPORT_OPTION');

	INSERT INTO dbversion(version) VALUES(746);

	GO

COMMIT TRAN