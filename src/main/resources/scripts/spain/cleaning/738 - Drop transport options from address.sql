use atlas

go

begin tran

declare @fk_transportin sysname, @fk_transportout sysname

select @fk_transportin = co.name from sys.foreign_key_columns fk
left join sys.objects as co on fk.constraint_object_id = co.object_id
left join sys.objects as pt on fk.parent_object_id = pt.object_id
left join sys.columns as pc on fk.parent_column_id = pc.column_id
where pt.name = 'address' and pc.name = 'transportinid'

select @fk_transportout = co.name from sys.foreign_key_columns fk
left join sys.objects as co on fk.constraint_object_id = co.object_id
left join sys.objects as pt on fk.parent_object_id = pt.object_id
left join sys.columns as pc on fk.parent_column_id = pc.column_id
where pt.name = 'address' and pc.name = 'transportoutid'

if @fk_transportin is not null exec('alter table address drop constraint ' + @fk_transportin)
if @fk_transportout is not null exec('alter table address drop constraint ' + @fk_transportout)

drop index IDX_address_transportinid on address
drop index IDX_address_transportoutid on address

go

alter table address drop column transportinid
alter table address drop column transportoutid

go

insert into permission(groupId, permission) values (1, 'ADDRESS_SETTINGS_DELETE')

insert into dbversion values(738)

commit tran