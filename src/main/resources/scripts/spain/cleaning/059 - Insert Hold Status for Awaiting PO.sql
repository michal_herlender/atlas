-- Author Galen Beck - 2016-09-07
-- Adds new hold status of "On hold - awaiting purchase order" for Assembla ticket #813

USE [spain]
BEGIN TRAN

INSERT INTO [spain].[dbo].[itemstate] ([type],[active],[description],[retired],[lookupid])
VALUES('holdstatus',1,'On hold - awaiting purchase order',0,NULL);

INSERT INTO [spain].[dbo].[itemstatetranslation] (stateid,locale,translation)
SELECT stateid,'en_GB','On hold - awaiting purchase order' FROM [spain].[dbo].[itemstate] WHERE [description] = 'On hold - awaiting purchase order'

INSERT INTO [spain].[dbo].[itemstatetranslation] (stateid,locale,translation)
SELECT stateid,'es_ES','En espera - esperando orden de compra' FROM [spain].[dbo].[itemstate] WHERE [description] = 'On hold - awaiting purchase order'

-- State Group 32 - Department - CS Other - 4 - Other (not adding to Laboratory 36)

INSERT INTO [spain].[dbo].[stategrouplink](groupid,stateid,type)
SELECT 32,stateid,NULL FROM [spain].[dbo].[itemstate] WHERE [description] = 'On hold - awaiting purchase order';

COMMIT TRAN