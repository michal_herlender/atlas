-- It seems that in script 604, there is a "clientapprovalon" field added, 
-- but in the Quotation entity it's called "clientacceptedon", causing a mismatch.
-- renaming field here (consistent with other "accepted" type fields) so that Quotations could work again.

-- Galen Beck - 2030-03-31

USE [atlas]
GO

BEGIN TRAN

EXEC sp_rename 'quotation.clientapprovalon' , 'clientacceptedon', 'COLUMN';

INSERT INTO dbversion(version) VALUES (605);

COMMIT TRAN