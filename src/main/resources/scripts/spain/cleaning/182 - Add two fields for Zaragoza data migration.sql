-- Script 182 to add two new fields for Zaragoza migration:

-- addresssettings.contract
-- instrument.defaultservicetype

-- Running on production and test - Galen Beck - 2017-11-06

USE [spain]
BEGIN TRAN

    alter table dbo.addressplantillassettings 
        add contract bit default 0;
	GO

	UPDATE [dbo].[addressplantillassettings]
	   SET contract = 0;
	GO

    alter table dbo.addressplantillassettings 
        alter column contract bit not null;

    alter table dbo.instrument 
        add defaultservicetype int;

    alter table dbo.instrument 
        add constraint FKqkmans58f126lwhwr4n0e8bg0 
        foreign key (defaultservicetype) 
        references dbo.servicetype;

COMMIT TRAN;