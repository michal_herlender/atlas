USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE repaircompletionreport ADD repaircompletiondate datetime2(7) NULL;
	
	GO

	INSERT INTO dbversion(version) VALUES(658);

COMMIT TRAN