-- Removes calibration types that are not used for internal use, from 
-- the procedure accreditation and calibrationaccreditationlevel tables

-- We are just left with 9 types for use:
-- Accredited/Standard + In Lab/Onsite + with CAR/without CAR
-- and OTHER since it's used by Madrid for functional checks

-- Removing : CLIENT, CLIENT-EX, IC, M, STD-EX, AC-EX

USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[procedureaccreditation]
      WHERE caltypeid in (33, 61, 62, 60, 59, 37);
GO

DELETE FROM [dbo].[calibrationaccreditationlevel]
      WHERE caltypeid in (33, 61, 62, 60, 59, 37);
GO

-- Set default cal type on job as mandatory field (1 record null!)

UPDATE [dbo].job SET defaultcaltypeid = 1 WHERE defaultcaltypeid is NULL;
GO

DROP STATISTICS [dbo].[job].[_dta_stat_464720708_1_20_11_15_10_12_16_23_18_14_13_24_21_19_17_22]
GO
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_1_20_11_15_12_16_23_18_14_13_24_21_19_17]
GO
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_20_11_15_10_12_16_23_18_14_13_24_21_19_17_22]
GO
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_20_11_15_12_16_23_18_14_13_24_21_19_17_22_1]
GO
DROP INDEX [IDX_job_defaultcaltypeid] ON [dbo].[job]
GO

ALTER TABLE [dbo].job ALTER COLUMN defaultcaltypeid int NOT NULL;

INSERT INTO dbversion(version) VALUES(311);

COMMIT TRAN