-- Author Galen Beck - 2016-08-2016
-- Inserts business subdivision settings for 4 subdivisions into system

USE [spain]
GO

-- MAD 6645, ZAZ 6644, BIO 6642, VIT 6643

INSERT INTO [dbo].[businesssubdivisionsettings]
		([lastModified],[ebitdahourlycostrate],[subdivid])
     VALUES
		('2016-08-19','59.40',6642),
		('2016-08-19','59.40',6643),
		('2016-08-19','70.40',6644),
		('2016-08-19','48.90',6645);
GO

