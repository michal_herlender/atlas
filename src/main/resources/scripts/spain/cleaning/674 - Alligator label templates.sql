-- Expands definitions for alligator label templates to accommodate Germany labels

USE [atlas]

BEGIN TRAN

    create table dbo.alligatorlabelparameter (
       id int identity not null,
        format varchar(30),
        name varchar(30) not null,
        text varchar(30),
        value varchar(255) not null,
        templateid int not null,
        primary key (id)
    );

    alter table dbo.alligatorlabeltemplate 
       add parameterstyle varchar(255);

    alter table dbo.alligatorlabelparameter 
       add constraint FK_alligatorlabelparameter_template 
       foreign key (templateid) 
       references dbo.alligatorlabeltemplate;

    GO

	update dbo.alligatorlabeltemplate 
	   set parameterstyle = 'AUTOMATIC';

	GO

    alter table dbo.alligatorlabeltemplate 
       alter column parameterstyle varchar(255) not null;

INSERT INTO dbversion(version) VALUES(674);

COMMIT TRAN