-- Script to delete "backup" job item images table - now that migrated table working well
-- Also fixes issue with image delete having constraint violation.
-- GB 2017-09-18

USE [spain]
GO

BEGIN TRAN

DROP TABLE [dbo].[imagesBU]
GO

COMMIT TRAN