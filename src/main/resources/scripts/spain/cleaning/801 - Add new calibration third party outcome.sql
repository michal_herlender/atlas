-- Add new TP Calibration Outcoume : FAILURE_REPORT
USE [atlas]

BEGIN TRAN

    DECLARE
		@new_outcomestatus INT,
		@activity_tpcert INT,
		@lookup_hasfailurereport INT,
		@lastInsertedAO_failure_report INT,
		@lookup_tpcaloutcome INT;

	SELECT @lookup_hasfailurereport = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_ItemHasFaultReport (inhouse after calibration failure)';
	SELECT @activity_tpcert = stateid FROM dbo.itemstate WHERE [description] = 'Third party certificate added';
	SELECT @lookup_tpcaloutcome = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_TPCalibrationOutcome (inhouse after third party work)';
	-- create new outcomestatus
	INSERT INTO dbo.outcomestatus(defaultoutcome, activityid, statusid, lookupid)
	VALUES (0, @activity_tpcert, null, @lookup_hasfailurereport);
	SELECT @new_outcomestatus = MAX(id) FROM dbo.outcomestatus;
	-- insert a new tp calibration outcome
	INSERT INTO dbo.lookupresult(resultmessage, result, outcomestatusid, lookupid, activityid)
	VALUES (6, 'g', @new_outcomestatus, @lookup_tpcaloutcome, @activity_tpcert);
	-- add a new action outcome
	INSERT INTO dbo.actionoutcome(defaultoutcome, description, positiveoutcome, activityid, active, [type], value)
	VALUES(0, 'Unit requires failure report', 0, @activity_tpcert, 1, 'post_tp_further_work', 'REQUIRES_FAILURE_REPORT');
	SET @lastInsertedAO_failure_report = IDENT_CURRENT('actionoutcome');
	-- add a new action outcome translation
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) 
	VALUES (@lastInsertedAO_failure_report, 'en_GB', 'Unit requires failure report'),
	       (@lastInsertedAO_failure_report, 'fr_FR', 'L''instrument nécessite un constat d''anomalie'),
	       (@lastInsertedAO_failure_report, 'es_ES', 'La unidad requiere un informe de fallas'),
	       (@lastInsertedAO_failure_report, 'de_DE', 'Gerät benötigt Fehlerbericht');

	INSERT INTO dbversion(version) VALUES(801);

COMMIT TRAN