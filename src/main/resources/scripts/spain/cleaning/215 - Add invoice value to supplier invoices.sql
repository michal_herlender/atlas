-- Author Paul Weston - 15/01/2018
-- Adding net and gross invoice values and currency to the supplier invoice (Ticket #1207)
-- Removing fields added in previous update of sql script 212

USE [spain]
GO

BEGIN TRAN

alter table dbo.purchaseorderitemprogressaction
  DROP COLUMN supplierinvoicegrossvalue;

alter table dbo.purchaseorderitemprogressaction
  DROP COLUMN supplierinvoicenetvalue;

alter table dbo.supplierinvoice
  add totalgross numeric(10,2);

alter table dbo.supplierinvoice
  add totalnet numeric(10,2);

alter table dbo.supplierinvoice
  add currencyid int;

alter table dbo.supplierinvoice add constraint supplierinvoicecurrency
foreign key (currencyid)
references dbo.supportedcurrency;

INSERT INTO dbversion (version) VALUES (215);

COMMIT TRAN