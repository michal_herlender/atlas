/* Change user companies - Tony Provost 2016-03-09 */

BEGIN TRAN 

USE [SPAIN];
GO

UPDATE dbo.contact 
SET lastModified = GETDATE()
	,email = 'christophe.dupriez@trescal.com'
	,telephone = '+33 (0)2 54 73 35 35'
	,position = 'Chief Information Officer'
	,subdivid = (SELECT subdivid FROM dbo.subdiv WHERE lower(subname) = lower('Vend�me') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
	,defaddress = (SELECT defaddrid FROM dbo.subdiv WHERE lower(subname) = lower('Vend�me') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
WHERE lower(lastName) = lower('Dupriez')

INSERT INTO user_role (rolename, username, lastModified, orgid)
SELECT 'ROLE_ADMINISTRATOR', dbo.users.username, GETDATE(), dbo.contact.subdivid
FROM dbo.contact, dbo.users
WHERE dbo.contact.personid = dbo.users.personid
	AND dbo.contact.lastName = 'Dupriez'
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.user_role
		WHERE dbo.user_role.username = dbo.users.username
			AND dbo.user_role.orgid = dbo.contact.subdivid
		)

UPDATE dbo.contact 
SET lastModified = GETDATE()
	,email = 'fran.manas@itm.es'
	,telephone = '+34667178855'
	,position = 'Manager'
	,subdivid = (SELECT subdivid FROM dbo.subdiv WHERE lower(subname) = lower('Bilbao') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
	,defaddress = (SELECT defaddrid FROM dbo.subdiv WHERE lower(subname) = lower('Bilbao') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
WHERE lower(lastName) = lower('Manas')

INSERT INTO user_role (rolename, username, lastModified, orgid)
SELECT 'ROLE_ADMINISTRATOR', dbo.users.username, GETDATE(), dbo.contact.subdivid
FROM dbo.contact, dbo.users
WHERE dbo.contact.personid = dbo.users.personid
	AND dbo.contact.lastName = 'Manas'
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.user_role
		WHERE dbo.user_role.username = dbo.users.username
			AND dbo.user_role.orgid = dbo.contact.subdivid
		)

UPDATE dbo.contact 
SET lastModified = GETDATE()
	,email = 'adrian.stevens@trescal.com'
	,telephone = '+44 (0) 1252 533 300'
	,position = 'IT Support'
	,subdivid = (SELECT subdivid FROM dbo.subdiv WHERE lower(subname) = lower('Great Yarmouth') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
	,defaddress = (SELECT defaddrid FROM dbo.subdiv WHERE lower(subname) = lower('Great Yarmouth') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
WHERE lower(lastName) = lower('Stevens')

INSERT INTO user_role (rolename, username, lastModified, orgid)
SELECT 'ROLE_ADMINISTRATOR', dbo.users.username, GETDATE(), dbo.contact.subdivid
FROM dbo.contact, dbo.users
WHERE dbo.contact.personid = dbo.users.personid
	AND dbo.contact.lastName = 'Stevens'
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.user_role
		WHERE dbo.user_role.username = dbo.users.username
			AND dbo.user_role.orgid = dbo.contact.subdivid
		)

UPDATE dbo.contact 
SET lastModified = GETDATE()
	,email = 'gerard.manintveld@trescal.com'
	,telephone = '+31 79 343 00 52'
	,subdivid = (SELECT subdivid FROM dbo.subdiv WHERE lower(subname) = lower('Zoetermeer') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
	,defaddress = (SELECT defaddrid FROM dbo.subdiv WHERE lower(subname) = lower('Zoetermeer') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
WHERE lower(lastName) = lower('Manintveld')

INSERT INTO user_role (rolename, username, lastModified, orgid)
SELECT 'ROLE_ADMINISTRATOR', dbo.users.username, GETDATE(), dbo.contact.subdivid
FROM dbo.contact, dbo.users
WHERE dbo.contact.personid = dbo.users.personid
	AND dbo.contact.lastName = 'Manintveld'
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.user_role
		WHERE dbo.user_role.username = dbo.users.username
			AND dbo.user_role.orgid = dbo.contact.subdivid
		)

UPDATE dbo.contact 
SET lastModified = GETDATE()
	,email = 'dominique.bernard@trescal.com'
	,telephone = '+33 (0)6 89 95 80 07'
	,subdivid = (SELECT subdivid FROM dbo.subdiv WHERE lower(subname) = lower('Rungis') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5 AND coname like '%Trescal%'))
	,defaddress = (SELECT defaddrid FROM dbo.subdiv WHERE lower(subname) = lower('Rungis') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5 AND coname like '%Trescal%'))
WHERE lower(lastName) = lower('Bernard')

INSERT INTO user_role (rolename, username, lastModified, orgid)
SELECT 'ROLE_ADMINISTRATOR', dbo.users.username, GETDATE(), dbo.contact.subdivid
FROM dbo.contact, dbo.users
WHERE dbo.contact.personid = dbo.users.personid
	AND dbo.contact.lastName = 'Bernard'
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.user_role
		WHERE dbo.user_role.username = dbo.users.username
			AND dbo.user_role.orgid = dbo.contact.subdivid
		)

UPDATE dbo.contact 
SET lastModified = GETDATE()
	,email = 'galen.beck@trescal.us'
	,subdivid = (SELECT subdivid FROM dbo.subdiv WHERE lower(subname) = lower('San Francisco') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
	,defaddress = (SELECT defaddrid FROM dbo.subdiv WHERE lower(subname) = lower('San Francisco') AND coid IN (SELECT coid FROM dbo.company WHERE corole = 5))
WHERE lower(lastName) = lower('Beck')

INSERT INTO user_role (rolename, username, lastModified, orgid)
SELECT 'ROLE_ADMINISTRATOR', dbo.users.username, GETDATE(), dbo.contact.subdivid
FROM dbo.contact, dbo.users
WHERE dbo.contact.personid = dbo.users.personid
	AND dbo.contact.lastName = 'Beck'
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.user_role
		WHERE dbo.user_role.username = dbo.users.username
			AND dbo.user_role.orgid = dbo.contact.subdivid
		)

ROLLBACK TRAN
