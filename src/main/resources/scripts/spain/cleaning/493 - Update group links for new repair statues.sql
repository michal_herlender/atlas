USE [atlas]
GO

BEGIN TRAN

declare @stateid int;

-- link 'Awaiting repair completion report update' to CS Other group

select @stateid = stateid from itemstate where description = 'Awaiting repair completion report update';

INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (32,@stateid,'ALLOCATED_SUBDIV');
	
-- link 'Awaiting repair completion report update (post third party work)' to CS Other group

select @stateid = stateid from itemstate where description = 'Awaiting repair completion report update (post third party work)';

INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (32,@stateid,'ALLOCATED_SUBDIV');
	
-- link 'Service continued on new job item, awaiting confirmation costing on old job item' to CS job costing group

select @stateid = stateid from itemstate where description = 'Service continued on new job item, awaiting confirmation costing on old job item';

INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (30,@stateid,'ALLOCATED_SUBDIV');
	
-- set 'Service continued on new job item, old job item completed' as inactive

select @stateid = stateid from itemstate where description = 'Service continued on new job item, old job item completed';

UPDATE dbo.itemstate SET active=0
	WHERE stateid=@stateid;

INSERT INTO dbversion(version) VALUES(493);

COMMIT TRAN