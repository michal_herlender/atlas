BEGIN TRAN

	INSERT INTO accessorytranslation(accessoryid, locale, translation)
	VALUES
	(1, 'de_DE', 'Zubehör'),
	(2, 'de_DE', 'Adapter'),
	(3, 'de_DE', 'Batterie'),
	(4, 'de_DE', 'Kiste'),
	(5, 'de_DE', 'Kabelrolle'),
	(6, 'de_DE', 'Kalibrierschein'),
	(7, 'de_DE', 'Tasche'),
	(8, 'de_DE', 'CD'),
	(9, 'de_DE', 'Ladegerät'),
	(10, 'de_DE', 'Klemme'),
	(11, 'de_DE', 'Kommunikationskabel'),
	(12, 'de_DE', 'Kundenverpackung'),
	(13, 'de_DE', 'Dokumentation'),
	(14, 'de_DE', 'Externer Sensor/Fühler'),
	(15, 'de_DE', 'Anschlussteil'),
	(16, 'de_DE', 'Halfter'),
	(17, 'de_DE', 'Anschluss'),
	(18, 'de_DE', 'Netzkabel'),
	(19, 'de_DE', 'Handbuch'),
	(20, 'de_DE', 'Öl'),
	(21, 'de_DE', 'Außenverpackung'),
	(22, 'de_DE', 'Palette'),
	(23, 'de_DE', 'Druckanschlussleitung'),
	(24, 'de_DE', 'Drucker'),
	(25, 'de_DE', 'Akku'),
	(26, 'de_DE', 'spezielles Kabel'),
	(27, 'de_DE', 'Riemen'),
	(28, 'de_DE', 'Testkabel'),
	(29, 'de_DE', 'Gewicht'),
	(31, 'de_DE', 'Verpackung'),
	(32, 'de_DE', 'Karte'),
	(33, 'de_DE', 'Netzgerät'),
	(34, 'de_DE', 'USB Speicherstick'),
	(35, 'de_DE', 'Modul'),
	(36, 'de_DE', 'Krokodilklemme'),
	(37, 'de_DE', 'Originalverpackung'),
	(38, 'de_DE', 'Rucksack')

	GO

	INSERT INTO dbversion (version) VALUES (242)

COMMIT TRAN