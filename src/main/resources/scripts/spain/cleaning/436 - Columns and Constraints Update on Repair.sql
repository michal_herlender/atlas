USE [spain]
GO

BEGIN TRAN

-- If script 434 has successfully, run, this isn't needed
-- EXEC sp_RENAME 'repairinspectionreport.manufacturerwaranty' , 'manufacturerwarranty', 'COLUMN';

ALTER TABLE dbo.freerepaircomponent DROP CONSTRAINT freerepaircomponent_freerepairoperation_FK;
ALTER TABLE dbo.freerepaircomponent ADD CONSTRAINT freerepaircomponent_freerepairoperation_FK 
FOREIGN KEY (freerepairoperationid) REFERENCES dbo.freerepairoperation(id); 

GO

INSERT INTO dbversion (version) VALUES (436);
GO

COMMIT TRAN