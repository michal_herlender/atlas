-- Add permission to import quotation items

USE [atlas]

BEGIN TRAN

	Insert into dbo.permission(groupId,permission) values 
	(1, 'IMPORT_QUOTATION_ITEMS')
	
	 INSERT INTO dbversion(version) VALUES(562);

COMMIT TRAN 