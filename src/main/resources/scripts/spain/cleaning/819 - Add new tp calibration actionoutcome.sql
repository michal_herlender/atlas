USE [atlas]
GO

BEGIN TRAN

	DECLARE @work_completed_status INT,
		@tp_cert_added_activity INT,
		@new_outcomestatus INT,
		@lookup_tpcaloutcome INT,
		@last_ao_id INT;

	SELECT @work_completed_status = stateid FROM dbo.itemstate WHERE [description] = 'Work completed, despatched to client';
	SELECT @tp_cert_added_activity = stateid FROM dbo.itemstate WHERE [description] = 'Third party certificate added';
	SELECT @lookup_tpcaloutcome = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_TPCalibrationOutcome (inhouse after third party work)';
	
	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
		VALUES (0,@tp_cert_added_activity,NULL,@work_completed_status);
	
	SELECT @new_outcomestatus = MAX(id) FROM dbo.outcomestatus;
	-- insert a new tp calibration outcome
	INSERT INTO dbo.lookupresult(resultmessage, result, outcomestatusid, lookupid, activityid)
		VALUES (7, 'h', @new_outcomestatus, @lookup_tpcaloutcome, @tp_cert_added_activity);
	
	INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
		VALUES (0,	'No further work necessary, no delivery required',	0,	@tp_cert_added_activity,	1,	'post_tp_further_work',	'NO_FURTHER_WORK_NECESSARY_NO_DELIVERY_REQUIRED');
		SELECT @last_ao_id = MAX(id) FROM dbo.actionoutcome;
		INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
		VALUES (@last_ao_id,	'de_DE',	'Keine weiteren Arbeiten notwendig, keine Lieferung erforderlich'),
		(@last_ao_id,	'en_GB',	'No further work necessary, no delivery required'),
		(@last_ao_id,	'es_ES',	'No es necesario ning�n trabajo adicional, no se requiere entrega'),
		(@last_ao_id,	'fr_FR',	'Aucun autre travail n�cessaire, aucune livraison requise');

	INSERT INTO dbversion(version) VALUES(819);

COMMIT TRAN