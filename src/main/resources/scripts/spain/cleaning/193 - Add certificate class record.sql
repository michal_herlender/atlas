BEGIN TRAN

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
  (32
    , 'boolean'
    , 'Defines if certificates for this client can have a class'
    , 'Certificate Class'
    , 'false', 0, 0, 9, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (32, 'en_GB', 'Defines if certificates for this client can have a certificate class'),
  (32, 'en_ES', 'Define si los certificados para este cliente pueden tener una clase de certificado'),
  (32, 'fr_FR', 'Définit si les certificats pour ce client peuvent avoir un classe de certificat');

-- Enables system default to be configurable at only company level

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0, 32);
GO

-- Enables system defaults (new and old) for specific company roles - Client and Business Company

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (1, 32),
  (5, 32);

-- Add translations

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (31, 'en_GB', 'Certificate Class'),
  (31, 'es_ES', 'Clase de certificado'),
  (31, 'fr_FR', 'Classe de certificat');
GO


INSERT INTO dbversion (version) VALUES (193);

-- Change to COMMIT when successfully tested

COMMIT TRAN