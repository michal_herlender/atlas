USE spain;

BEGIN TRAN;

alter table dbo.certificatevalidation
  add validationstatus varchar(255);

GO

UPDATE dbo.certificatevalidation SET dbo.certificatevalidation.validationstatus = 'ACCEPTED';

GO

INSERT INTO dbversion (version) VALUES (213);

COMMIT TRAN;