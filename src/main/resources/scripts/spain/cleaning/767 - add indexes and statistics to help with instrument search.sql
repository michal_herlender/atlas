USE [atlas]

BEGIN TRAN

CREATE STATISTICS [_dta_stat_1653580929_5_1] ON [dbo].[calibration]([caldate], [id])

CREATE NONCLUSTERED INDEX [_dta_index_description_9_199671759__K1_K8_10] ON [dbo].[description]
(
	[descriptionid] ASC,
	[familyid] ASC
)
INCLUDE ( 	[typology]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE STATISTICS [_dta_stat_199671759_8_1] ON [dbo].[description]([familyid], [descriptionid])

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_instrument_9_740197687__K17_1_3_10_11_15_22_24_27_39_40_44_50] ON [dbo].[instrument]
(
	[serialno] ASC
)
INCLUDE ( 	[plantid],
	[addedon],
	[recalldate],
	[plantno],
	[scrapped],
	[coid],
	[lastcalid],
	[modelid],
	[customerdescription],
	[formerbarcode],
	[modelname],
	[defaultservicetype]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE STATISTICS [_dta_stat_740197687_1_50_27_22] ON [dbo].[instrument]([plantid], [defaultservicetype], [modelid], [coid])

CREATE STATISTICS [_dta_stat_740197687_22_50] ON [dbo].[instrument]([coid], [defaultservicetype])

CREATE STATISTICS [_dta_stat_740197687_24_50_27] ON [dbo].[instrument]([lastcalid], [defaultservicetype], [modelid])

CREATE STATISTICS [_dta_stat_740197687_27_50_22_24_1_17] ON [dbo].[instrument]([modelid], [defaultservicetype], [coid], [lastcalid], [plantid], [serialno])

CREATE NONCLUSTERED INDEX [_dta_index_jobitem_9_2016726237__K30_K1_K38_K31] ON [dbo].[jobitem]
(
	[plantid] ASC,
	[jobitemid] ASC,
	[stateid] ASC,
	[jobid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_jobitem_9_2016726237__K31_K30_K1_K38] ON [dbo].[jobitem]
(
	[jobid] ASC,
	[plantid] ASC,
	[jobitemid] ASC,
	[stateid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE STATISTICS [_dta_stat_2109250569_1_4] ON [dbo].[mfr]([mfrid], [genericmfr])

INSERT INTO dbversion(version) VALUES(767);

COMMIT TRAN