-- Update IP addresses for label printers

USE [spain]
GO

UPDATE [dbo].[labelprinter]
   SET [ipv4] = '172.28.1.26' WHERE [description] ='GX430t_MAD_26'; 

UPDATE [dbo].[labelprinter]
   SET [ipv4] = '172.28.1.27' WHERE [description] ='GX430t_MAD_27'; 

UPDATE [dbo].[labelprinter]
   SET [ipv4] = '172.28.1.28' WHERE [description] ='GX430t_MAD_28'; 

UPDATE [dbo].[labelprinter]
   SET [ipv4] = '172.22.1.23' WHERE [description] ='TEC_ZGZ_23'; 

UPDATE [dbo].[labelprinter]
   SET [ipv4] = '172.28.1.30' WHERE [description] ='GX430t_MAD_30'; 

UPDATE [dbo].[labelprinter]
   SET path = '';

GO


