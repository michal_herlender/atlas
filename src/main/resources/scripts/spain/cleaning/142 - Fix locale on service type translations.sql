-- Corrects 6 short name translations for inter-company which were set to en_ES in database
-- Galen Beck - 2017-05-05, am running on production & test immediately

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [locale] = 'es_ES'
 WHERE [locale] = 'en_ES'
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-IC-SJ'
 WHERE [translation] = 'AC-IC-MV' AND [locale] = 'es_ES'
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-IC-SJ'
 WHERE [translation] = 'ST-IC-MV' AND [locale] = 'es_ES'
GO

COMMIT TRAN