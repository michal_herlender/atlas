-- Updates users table to set webaware=1 for everyone which is now used to enable user account

USE [spain]
GO

UPDATE [dbo].[users]
   SET [webaware] = 1
GO

