USE [spain]
GO

BEGIN TRAN

	INSERT INTO dbo.systemcomponent (appendtoparentpath,component,componentname,foldersdropboundaries,foldersperitem,folderssplit,folderssplitrange,foldersyear,numberedsubfolders,rootdir,usingparentsettings,parentid,emailtemplates)
	VALUES ('Repair Reports',26,'Repair Inspection Report',0,0,0,0,0,0,'Repair Inspection Report',1,1,0);
	
	INSERT INTO dbo.systemcomponent (appendtoparentpath,component,componentname,foldersdropboundaries,foldersperitem,folderssplit,folderssplitrange,foldersyear,numberedsubfolders,rootdir,usingparentsettings,parentid,emailtemplates)
	VALUES ('Repair Reports',27,'Repair Completion Report',0,0,0,0,0,0,'Repair Completion Report',1,1,0);
	
	
	INSERT INTO dbversion (version) VALUES (450);

COMMIT TRAN