/*
 * As discussed in Vendome meeting : 
 * Delete the timeIncrement table, and make a link between Calibration and JobItemActivity (JobItemAction)
 * with a new table CalibrationActivityLink
*/
USE [spain]
GO

BEGIN TRAN

	--delete the table
	DROP TABLE dbo.timeincrement 
	go

	CREATE TABLE dbo.calibrationactivitylink (
		id int not null IDENTITY(1,1),
		calid int,
		jobitemactivityid int,
		CONSTRAINT calibrationactivitylink_PK PRIMARY KEY (id),
		CONSTRAINT calibration_id_FK FOREIGN KEY (calid) REFERENCES dbo.calibration(id),
		CONSTRAINT jobitemactivityid_id_FK FOREIGN KEY (jobitemactivityid) REFERENCES dbo.jobitemaction(id)
	) 
	go
	
	insert into dbversion(version) values(254)
	go
	
COMMIT TRAN