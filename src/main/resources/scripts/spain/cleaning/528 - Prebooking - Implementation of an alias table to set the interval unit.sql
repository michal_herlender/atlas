USE [atlas]
GO

BEGIN TRAN

create table dbo.aliasgroup (
       id int identity not null,
        lastModified datetime2 not null,
        createdOn datetime2,
        description varchar(255),
        name varchar(255),
        lastModifiedBy int,
        createdby int,
        primary key (id)
    );

create table dbo.aliasintervalunit (
       id int identity not null,
        alias varchar(255),
        value varchar(255),
        aliasgroupid int,
        primary key (id)
    );

create table dbo.aliasservicetype (
       id int identity not null,
        alias varchar(255),
        aliasgroupid int,
        servicetypeid int,
        primary key (id)
    );

create table dbo.aliasboolean (
       id int identity not null,
        alias varchar(255),
        value bit,
        aliasgroupid int,
        primary key (id)
    );

alter table atlas.dbo.exchangeformat add aliasgroupid int;

alter table dbo.aliasboolean  add constraint FK2airq052hx04m4lt981tq6goq foreign key (aliasgroupid) references dbo.aliasgroup;

alter table dbo.aliasgroup add constraint FKfyifk5231lligwh65684ggc4c foreign key (lastModifiedBy) references dbo.contact;

alter table dbo.aliasgroup add constraint FKm257ko0oy6n8bnunq0huvvyaq foreign key (createdby) references dbo.contact;

alter table dbo.aliasintervalunit add constraint FKiktvhigv9hn22mbf85klfl827 foreign key (aliasgroupid) references dbo.aliasgroup;

alter table dbo.aliasservicetype add constraint FKd49cvgwp0c0yp0ns86ptso1dh foreign key (aliasgroupid) references dbo.aliasgroup;

alter table dbo.aliasservicetype add constraint FKbcfc1rwg3m0etuld5sofymt9u foreign key (servicetypeid) references dbo.servicetype;

alter table dbo.exchangeformat add constraint FKs9r09wu4qnecl9jottyf9l68h foreign key (aliasgroupid) references dbo.aliasgroup;

INSERT INTO dbversion(version) VALUES(528);

COMMIT TRAN
