-- Updates class name of renamed scheduled task for Adveso services
-- It seems 473 didn't have the current class name (also fixed spelling of Synchronizer)

USE [spain]
GO

BEGIN TRAN

DECLARE @csrpart_status INT,
		@last_outcomestatus INT,
		@lookup_107 INT,
		@lookup_90 INT,
		@outcome_to_107 INT,
		@lookup_54 INT,
		@oc_to_delete_1 INT,
		@oc_to_delete_2 INT,
		@oc_to_delete_3 INT,
		@oc_to_delete_4 INT,
		@oc_to_update_1 INT,
		@oc_to_update_2 INT,
		@oc_to_update_3 INT,
		@oc_to_update_4 INT,
		@status_awaitingwrselection INT,
		@status_unsuccessfulawafr INT,
		@activity_133 INT,
		@new_ao1 INT,
		@new_ao2 INT,
		@new_ao3 INT,
		@new_ao4 INT;

SELECT @csrpart_status = stateid
FROM itemstate 
WHERE description = 'Awaiting Repair Inspection Report Validation by CSR';

SELECT @lookup_107 = id
FROM lookupinstance 
WHERE [lookup] = 'Lookup_NeedRepairInspectionReport';

SELECT @lookup_90 = id
FROM lookupinstance 
WHERE [lookup] = 'Lookup_NextWorkRequirementByJobCompany (before lab calibration)';

SELECT @outcome_to_107 = outcomestatusid
FROM lookupresult
WHERE lookupid = @lookup_90 and resultmessage = 4

UPDATE outcomestatus SET statusid = NULL, lookupid = @lookup_107 WHERE id = @outcome_to_107;

INSERT INTO outcomestatus(activityid,defaultoutcome,lookupid,statusid)
VALUES(NULL, 0, NULL, @csrpart_status);

SELECT TOP(1) @last_outcomestatus = id FROM outcomestatus ORDER BY id DESC;

INSERT INTO lookupresult(lookupid, outcomestatusid, resultmessage)
VALUES(@lookup_107,@last_outcomestatus,2)

SELECT @activity_133 = stateid FROM itemstate WHERE [description] = 'Unit inspected by engineer (post third party work)';

UPDATE actionoutcome SET active = 0, defaultoutcome = 0, positiveoutcome = 0 WHERE activityid = @activity_133;

INSERT INTO actionoutcome(defaultoutcome,active,positiveoutcome,activityid,[description],[type],[value]) VALUES
(1, 1, 1, @activity_133, 'No further work necessary for current work requirement', 'post_tp_further_work', 'NO_FURTHER_WORK_FOR_CURRENT_WR'),
(0, 1, 0, @activity_133, 'Further work necessary by selection of next work requirement', 'post_tp_further_work', 'FURTHER_WORK_NECESSARY_BY_SELECTION_OF_NEXT_WR'),
(0, 1, 0, @activity_133, 'Work has not completed return to supplier', 'post_tp_further_work', 'NOT_COMPLETED_RETURN_TO_SUPPLIER'),
(0, 1, 0, @activity_133, 'Repair was not successful', 'post_tp_further_work', 'UNSUCCESSFUL');

SELECT @new_ao1 = id FROM actionoutcome WHERE activityid = @activity_133 AND [value] = 'NO_FURTHER_WORK_FOR_CURRENT_WR';
SELECT @new_ao2 = id FROM actionoutcome WHERE activityid = @activity_133 AND [value] = 'FURTHER_WORK_NECESSARY_BY_SELECTION_OF_NEXT_WR';
SELECT @new_ao3 = id FROM actionoutcome WHERE activityid = @activity_133 AND [value] = 'NOT_COMPLETED_RETURN_TO_SUPPLIER';
SELECT @new_ao4 = id FROM actionoutcome WHERE activityid = @activity_133 AND [value] = 'UNSUCCESSFUL';

INSERT INTO actionoutcometranslation(id, locale, translation) VALUES
(@new_ao1,'fr_FR','Aucun travail supplémentaire nécessaire pour les besoins actuels'),
(@new_ao1,'en_GB','No further work necessary for current work requirement'),
(@new_ao1,'es_ES','No se necesitan más trabajos para el requisito de trabajo actual.'),
(@new_ao1,'de_DE','Für den aktuellen Arbeitsbedarf sind keine weiteren Arbeiten erforderlich'),
(@new_ao2,'fr_FR','Travaux supplémentaires nécessaires en sélectionnant la prochaine tâche requise'),
(@new_ao2,'en_GB','Further work necessary by selection of next work requirement'),
(@new_ao2,'es_ES','Trabajo adicional necesario por la selección del siguiente requisito de trabajo'),
(@new_ao2,'de_DE','Weitere Arbeiten durch Auswahl des nächsten Arbeitsbedarfs erforderlich'),
(@new_ao3,'fr_FR','Le travail n''est pas terminé Retour au fournisseur'),
(@new_ao3,'en_GB','Work has not completed return to supplier'),
(@new_ao3,'es_ES','El trabajo no se ha completado volver al proveedor'),
(@new_ao3,'de_DE','Die Rücksendung an den Lieferanten ist noch nicht abgeschlossen'),
(@new_ao4,'fr_FR','La réparation n''a pas réussi'),
(@new_ao4,'en_GB','Repair was not successful'),
(@new_ao4,'es_ES','Reparación no tuvo éxito'),
(@new_ao4,'de_DE','Reparatur nicht erfolgreich');

SELECT @lookup_54 = id FROM lookupinstance WHERE [lookup] = 'Lookup_PostTPInspectionOutcome (inhouse after third party work)';
SELECT @oc_to_delete_1 = outcomestatusid FROM lookupresult WHERE lookupid = @lookup_54 and resultmessage = 4;
SELECT @oc_to_delete_2 = outcomestatusid FROM lookupresult WHERE lookupid = @lookup_54 and resultmessage = 5;
SELECT @oc_to_delete_3 = outcomestatusid FROM lookupresult WHERE lookupid = @lookup_54 and resultmessage = 6;
SELECT @oc_to_delete_4 = outcomestatusid FROM lookupresult WHERE lookupid = @lookup_54 and resultmessage = 7;

DELETE FROM lookupresult WHERE lookupid = @lookup_54 AND resultmessage > 3;
DELETE FROM outcomestatus WHERE id IN (@oc_to_delete_1, @oc_to_delete_2, @oc_to_delete_3, @oc_to_delete_4);

SELECT @oc_to_update_1 = outcomestatusid FROM lookupresult WHERE lookupid = @lookup_54 AND resultmessage = 0;
SELECT @oc_to_update_2 = outcomestatusid FROM lookupresult WHERE lookupid = @lookup_54 AND resultmessage = 1;
SELECT @oc_to_update_3 = outcomestatusid FROM lookupresult WHERE lookupid = @lookup_54 AND resultmessage = 2;
SELECT @oc_to_update_4 = outcomestatusid FROM lookupresult WHERE lookupid = @lookup_54 AND resultmessage = 3;

SELECT @status_awaitingwrselection = stateid FROM itemstate WHERE [description] = 'Awaiting selection of next work requirement';
SELECT @status_unsuccessfulawafr = stateid FROM itemstate WHERE [description] = 'Third party repair un-successful, awaiting fault report update';

UPDATE outcomestatus SET defaultoutcome = 1,lookupid = @lookup_90, statusid = NULL WHERE id = @oc_to_update_1;
UPDATE outcomestatus SET defaultoutcome = 0,lookupid = NULL, statusid = @status_awaitingwrselection WHERE id = @oc_to_update_2;
UPDATE outcomestatus SET defaultoutcome = 0,lookupid = @lookup_90, statusid = NULL WHERE id = @oc_to_update_3;
UPDATE outcomestatus SET defaultoutcome = 0,lookupid = NULL, statusid = @status_unsuccessfulawafr WHERE id = @oc_to_update_4;

INSERT INTO dbversion(version) VALUES(478);

COMMIT TRAN

