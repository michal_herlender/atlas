-- Hides the invoice amount text for Germany (special configuration for Morocco only)

USE [atlas]
GO

BEGIN TRAN

UPDATE [dbo].[businessdocumentsettings]
   SET [invoiceAmountText] = 0
 WHERE [businesscompanyid] = 6681
GO

INSERT INTO dbversion(version) VALUES(595);

COMMIT TRAN