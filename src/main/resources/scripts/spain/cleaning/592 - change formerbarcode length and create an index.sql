-- Script to insert the same VAT rates that are on production
-- (recently added) to the test and development systems 
-- On production, we won't need to run this script as data already there.

USE [atlas]
GO

BEGIN TRAN

alter table instrument alter column formerbarcode nvarchar(50)
CREATE NONCLUSTERED INDEX [IDX_instrument_formerbarcode] ON [dbo].[instrument]
(
	[formerbarcode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT INTO dbversion(version) VALUES(592);

GO

COMMIT TRAN
