USE [atlas]
GO

BEGIN TRAN 
   
-- delete columns from exchange format 'FR_FINAl_OUTCOME' that is also removed from enum
DELETE FROM dbo.exchangeformatfieldnamedetails
WHERE fieldName='FR_FINAL_OUTCOME';


--new hookactivity for onsite for hook make-fr-available for adveso

declare @hookid int;
select @hookid = id
from dbo.hook
where name = 'make-fr-available-to-external-managment-system'

declare @stateid int;
select @stateid = stateid
from dbo.itemstate
where description= 'Onsite - awaiting Fault Report to be sent';

declare @lookupid int;
select @lookupid = id
from dbo.lookupinstance
where lookup like 'Lookup_ClientResponseRequiredOnFaultReport (onsite fault report)'


-- create new activity
declare @newactivity int;
INSERT INTO dbo.itemstate ([type],active,description,retired,lookupid)
	VALUES ('workactivity',1,'Onsite - Failure Report available',0,@lookupid);
select @newactivity = max(stateid)
from dbo.itemstate;
-- translations
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@newactivity,'de_DE','Vor Ort - Störungsmeldung vorhanden');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@newactivity,'en_GB','Onsite - Failure Report available');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@newactivity,'es_ES','En el sitio - Informe de falla disponible');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@newactivity,'fr_FR','Sur site - Constat d''anomalie disponible');


INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@newactivity,@hookid,@stateid);
	
	
-- change outcome status 
declare @os1 int;
select @os1 = os.id
from dbo.outcomestatus os 
join dbo.lookupresult lr on lr.outcomestatusid = os.id
where lr.lookupid = 98 and lr.resultmessage = 1;

declare @lookupid2 int;
select @lookupid2 = id
from dbo.lookupinstance
where lookup like 'Lookup_FailureReportOutcomePreselected (onsite)'

UPDATE dbo.outcomestatus
	SET statusid=NULL,lookupid=@lookupid2
	WHERE id=@os1;



INSERT INTO dbversion(version) VALUES(549);

COMMIT TRAN
  