USE [atlas]
GO

BEGIN TRAN

DECLARE @new_lookup INT,
		@jobcosting_status INT,
		@noaction_activity INT,
		@lookup_80 INT,
		@yes_os INT,
		@no_os INT;

SELECT @jobcosting_status = stateid FROM dbo.itemstate WHERE [description] = 'Service completed, awaiting job costing';
SELECT @noaction_activity = stateid FROM dbo.itemstate WHERE [description] = 'No action required';
SELECT @lookup_80 = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_PreDeliveryRequirements (after lab calibration - after costing check)';


-- create lookup 'Lookup_ItemToDeliverNeedJobCosting'
INSERT INTO dbo.lookupinstance (lookup,lookupfunction) VALUES ('Lookup_ItemToDeliverNeedJobCosting',57);
SELECT @new_lookup = max(id) from dbo.lookupinstance;

--add outcomestatus
INSERT INTO dbo.outcomestatus (activityid, lookupid, statusid, defaultoutcome) 
VALUES (@noaction_activity, null, @jobcosting_status, 0);
select @yes_os = max(id) from dbo.outcomestatus;
INSERT INTO dbo.outcomestatus (activityid, lookupid, statusid, defaultoutcome) 
VALUES (@noaction_activity, @lookup_80, null, 1);
select @no_os = max(id) from dbo.outcomestatus;

-- create lookup result
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@new_lookup,@yes_os,0);
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@new_lookup,@no_os,1);

UPDATE dbo.itemstate SET lookupid = @new_lookup WHERE stateid = @noaction_activity;

INSERT INTO dbversion(version) VALUES (573)

COMMIT TRAN