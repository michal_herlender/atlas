-- Add edit role permission and remove old userright deletions

USE [spain]


BEGIN TRAN

delete from dbo.permission where permission='USERRIGHT_DELETE'

insert into dbo.permission values (3,'EDIT_ROLE')

INSERT INTO dbversion(version) VALUES(390);


COMMIT TRAN