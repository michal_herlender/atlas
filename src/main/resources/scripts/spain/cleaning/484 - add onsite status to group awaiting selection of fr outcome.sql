USE [spain]
GO

BEGIN TRAN


-- ass status 'Onsite - awaiting selection of failure report outcome' to group 54-AWAITING_SELECTION_OF_FR_OUTCOME
INSERT INTO spain.dbo.stategrouplink (groupid,stateid,[type])
	VALUES (54,4108,'ALLOCATED_SUBDIV') 
	
GO

INSERT INTO dbversion(version) VALUES(484);

COMMIT TRAN