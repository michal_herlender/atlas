use atlas;

begin tran

alter table invoicetax alter column rate numeric(8,6);

alter table creditnotetax alter column rate numeric(8,6);

insert into dbversion(version) values(796);

commit tran