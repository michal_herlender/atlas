-- Suggestions from SQL profiler - 209-07-24 in testing France invoicing company wide

USE [atlas]

BEGIN TRAN

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [_dta_index_company_12_1682105033__K1_K5_19] ON [dbo].[company]
(
	[coid] ASC,
	[coname] ASC
)
INCLUDE ( 	[corole]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_invoiceitem_12_1700201107__K16_17] ON [dbo].[invoiceitem]
(
	[invoiceid] ASC
)
INCLUDE ( 	[jobitemid]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_invoiceitem_12_1700201107__K24_K16] ON [dbo].[invoiceitem]
(
	[expenseitem] ASC,
	[invoiceid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE STATISTICS [_dta_stat_1700201107_16_24_17] ON [dbo].[invoiceitem]([invoiceid], [expenseitem], [jobitemid])

CREATE STATISTICS [_dta_stat_1700201107_17_24] ON [dbo].[invoiceitem]([jobitemid], [expenseitem])

CREATE NONCLUSTERED INDEX [_dta_index_itemstate_12_352720309__K3_2] ON [dbo].[itemstate]
(
	[active] ASC
)
INCLUDE ( 	[stateid]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE STATISTICS [_dta_stat_352720309_3_2] ON [dbo].[itemstate]([active], [stateid])

CREATE NONCLUSTERED INDEX [_dta_index_job_12_464720708__K1_14_27] ON [dbo].[job]
(
	[jobid] ASC
)
INCLUDE ( 	[personid],
	[orgid]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_jobitem_12_2016726237__K38_K31_K1] ON [dbo].[jobitem]
(
	[stateid] ASC,
	[jobid] ASC,
	[jobitemid] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE STATISTICS [_dta_stat_1200317494_1_6] ON [dbo].[jobitemnotinvoiced]([id], [jobitemid])

INSERT INTO dbversion(version) VALUES(480);

COMMIT TRAN