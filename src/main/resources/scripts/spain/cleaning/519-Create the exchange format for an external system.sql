--Create the exchange format for an external system (Adveso or Mobile App)
USE [atlas]
GO
BEGIN TRAN

declare @lastexchangeFormatid int

INSERT INTO dbo.exchangeformat (name, description, exchangeFormatLevel, dateFormat, lastModified, lastModifiedBy, eftype)
VALUES ('ExchangeFormatExternalSystem', 'Exchange format usable for External System (mobile app)', 'GLOBAL', 'DD_MM_YYYY', '2019-10-17 12:09:20', 17280, 'PREBOOKING_FROM_MOBILEAPP')

SELECT @lastexchangeFormatid = MAX(id) FROM dbo.exchangeformat

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (1, 1, 'idTrescal', 'ID_TRESCAL', @lastexchangeFormatid)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (2, 0, 'domain', 'UNDEFINED', @lastexchangeFormatid)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (3, 0, 'customerDescription', 'CUSTOMER_DESCRIPTION', @lastexchangeFormatid)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (4, 0, 'plantNo', 'PLANT_NO', @lastexchangeFormatid)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (5, 0, 'serialNo', 'SERIAL_NUMBER', @lastexchangeFormatid)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (6, 0, 'clientref', 'CLIENT_REF', @lastexchangeFormatid)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (7, 0, 'accessories', 'UNDEFINED', @lastexchangeFormatid)

INSERT INTO dbversion(version) VALUES(519);

COMMIT TRAN