BEGIN TRAN

create table dbo.invoice_po_item
(
    id            int not null,
    invoiceitemid int,
    invpoid       int,
    primary key (id)
);

create sequence dbo.invoice_po_item_id_sequence start with 1 increment by 1;

alter table dbo.invoice_po_item
    add constraint FK_invoicepoitem_invoiceitem
        foreign key (invoiceitemid)
            references dbo.invoiceitem;

alter table dbo.invoice_po_item
    add constraint FK_invoicepoitem_invoicepo
        foreign key (invpoid)
            references dbo.invoicepo;

INSERT INTO invoice_po_item
(  id
, [invoiceitemid]
, [invpoid])
select (next value for invoice_po_item_id_sequence), invoiceitemid, invpoid
from invoiceitempo
where invpoid is not null

INSERT INTO dbversion VALUES (867)

COMMIT TRAN
