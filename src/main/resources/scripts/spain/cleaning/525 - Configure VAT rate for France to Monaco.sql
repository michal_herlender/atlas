-- Inserts a new VAT rate D for Eurozone (inter-country) use for sales from France to Monaco
-- Special case where France VAT rate of 20% is applied rather than normal inter-country rate of 0%
-- Updates two existing companies to have correct rate
-- Also fixes address format for Monaco to be same as France.

USE [atlas]
GO

BEGIN TRAN

UPDATE [dbo].[country]
   SET [addressprintformat] = 'POSTCODE_TOWN', 
       [memberOfUE] = 1
 WHERE countrycode = 'MC'
GO

INSERT INTO [dbo].[vatrate]
           ([vatcode]
           ,[description]
           ,[rate]
           ,[type]
           ,[countryid]
           ,[lastModified])
     VALUES
           ('D','France to Monaco',20.00,2,61,'2019-10-30')
GO

UPDATE [dbo].[companysettingsforallocatedcompany]
   SET [vatrate] = 'D'
 FROM [dbo].[companysettingsforallocatedcompany] settings
  LEFT JOIN company on company.coid = settings.companyid
  LEFT JOIN country on company.countryid = country.countryid
  WHERE countrycode = 'MC' and orgid = 6690
GO

SELECT settings.[id]
      ,settings.[active]
      ,settings.[onstop]
      ,settings.[orgid]
      ,settings.[companyid]
      ,settings.[vatrate]
      ,settings.[taxable]
	  ,company.coname
  FROM [dbo].[companysettingsforallocatedcompany] settings
  LEFT JOIN company on company.coid = settings.companyid
  LEFT JOIN country on company.countryid = country.countryid
  WHERE countrycode = 'MC' and orgid = 6690
GO

INSERT INTO dbversion(version) VALUES(525);

GO

COMMIT TRAN