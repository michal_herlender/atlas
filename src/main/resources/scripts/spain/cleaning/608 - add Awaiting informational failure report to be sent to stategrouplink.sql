USE atlas
Go

BEGIN TRAN

DECLARE @itemState INT;

select @itemState=its.stateid from itemstate its where its.description like 'Awaiting informational failure report to be sent';

insert into stategrouplink (groupid,stateid,type) values (32,@itemState,'ALLOCATED_SUBDIV');

INSERT INTO dbversion(version) VALUES (608);

COMMIT TRAN



