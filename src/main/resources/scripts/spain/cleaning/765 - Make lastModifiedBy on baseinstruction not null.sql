-- Fixes issue observed on production where some very old instructions
-- didn't have a lastModifiedBy set; there would be a silent failure and 500 on load balancer
-- set this to the auto user which is 8740 on all systems
-- such instructions will now load OK

USE [atlas]

BEGIN TRAN

UPDATE baseinstruction set lastModifiedBy = 8740 where lastModifiedBy IS null

GO

ALTER TABLE baseinstruction ALTER COLUMN lastModifiedBy int NOT NULL

INSERT INTO dbversion(version) VALUES(765);

COMMIT TRAN
