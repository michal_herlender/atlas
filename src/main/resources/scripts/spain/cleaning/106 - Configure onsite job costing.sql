-- StateGroupLink 30 is Customer Service - Job Costing

USE [spain]
GO

BEGIN TRAN

DECLARE @State_JC int;

SELECT @State_JC = (SELECT stateid FROM itemstate WHERE description='Recorded onsite service - awaiting job costing');

IF NOT EXISTS (SELECT 1 FROM stategrouplink WHERE stateid = @State_JC AND groupid = 30)

INSERT INTO [dbo].[stategrouplink]
           ([groupid],[stateid],[type])
     VALUES
           (30,@State_JC,'ALLOCATED_SUBDIV')
GO

COMMIT TRAN