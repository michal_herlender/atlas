USE [atlas]
GO
BEGIN TRAN

-- 2016-10-03 - TP : Change lenght of column for translation
ALTER table dbo.actionoutcometranslation ALTER COLUMN translation varchar(200) NOT NULL;
IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 53 AND locale = 'fr_FR') INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (53, 'fr_FR', 'L''instrument n''est pas cot�,  le sous-traitant peut coter sans voir l''instrument') ELSE UPDATE dbo.actionoutcometranslation SET translation = 'L''instrument n''est pas cot�,  le sous-traitant peut coter sans voir l''instrument' WHERE id = 53 AND locale = 'fr_FR';

COMMIT TRAN

USE [atlas]
GO

BEGIN TRAN

-- 2016-10-03 - TP : Change lenght of column for translation
ALTER table dbo.itemstatetranslation ALTER COLUMN translation varchar(200) NOT NULL;
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 115 AND locale = 'fr_FR') INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (115, 'fr_FR', 'Commande d''achat cr��e pour les travaux de sous traitance') ELSE UPDATE dbo.itemstatetranslation SET translation = 'Commande d''achat cr��e pour les travaux de sosu-traitance' WHERE stateid = 115 AND locale = 'fr_FR';

COMMIT TRAN


-- Author Tony Provost - 2016-11-08
-- Change french translations of the first 204 item states

BEGIN TRAN

ALTER table dbo.itemstatetranslation ALTER COLUMN translation varchar(200) NOT NULL;
IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = 115 AND locale = 'fr_FR') 
	INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (115, 'fr_FR', 'Commande d''achat cr��e pour les travaux de sous traitance') 
	ELSE UPDATE dbo.itemstatetranslation SET translation = 'Commande d''achat cr��e pour les travaux de sous traitance' WHERE stateid = 115 AND locale = 'fr_FR';

INSERT INTO dbversion(version) VALUES(809);

COMMIT TRAN


