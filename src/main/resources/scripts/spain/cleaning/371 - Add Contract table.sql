USE [spain]
BEGIN TRAN

create table dbo.contract (
                            id int identity not null,
                            lastModified datetime2 not null,
                            contractno varchar(255) not null,
                            contracttype varchar(255),
                            dbmanagement bit,
                            duration int not null,
                            durationunit varchar(255) not null,
                            enddate date not null,
                            financialrule varchar(255),
                            hourlyrate numeric(19,2),
                            invoicefrequency int,
                            legalContractNo varchar(255),
                            outstandingpremium numeric(19,2),
                            planificationrule varchar(255),
                            premium numeric(19,2),
                            software varchar(255),
                            startdate date not null,
                            transfertype varchar(255),
                            lastModifiedBy int,
                            orgid int not null,
                            clientcompanyid int not null,
                            clientcontactid int not null,
                            invoicingmanagerid int,
                            paymentmodeid int,
                            paymenttermsid int,
                            quotationId int,
                            trescalmanagerid int not null,
                            description varchar(255),
                            currencyid int,
                            primary key (id)
);



alter table dbo.contract
  add constraint FK241kkufum0uw419svqivacm9f
    foreign key (lastModifiedBy)
      references dbo.contact;

alter table dbo.contract
  add constraint FKfrgnb3hm8r0n94kxrkupnxo83
    foreign key (orgid)
      references dbo.company;

alter table dbo.contract
  add constraint FK2wytbaysryqs8br8blw0a5dc1
    foreign key (clientcompanyid)
      references dbo.company;

alter table dbo.contract
  add constraint FKbviysissgra2s0thohee36h4m
    foreign key (clientcontactid)
      references dbo.contact;

alter table dbo.contract
  add constraint FKitrvbiqs9wc5m922wksun4d8s
    foreign key (invoicingmanagerid)
      references dbo.contact;

alter table dbo.contract
  add constraint FKki5ruy80dugy2nfmgobgb7tcp
    foreign key (paymentmodeid)
      references dbo.paymentmode;

alter table dbo.contract
  add constraint FKta8kbw4p1rxrn44iicsu5c78k
    foreign key (paymenttermsid)
      references dbo.paymentterms;

alter table dbo.contract
  add constraint FKl6v8yu2gia0067try49j492w3
    foreign key (quotationId)
      references dbo.quotation;

alter table dbo.contract
  add constraint FKkv0anfx47oqyavu6uh7ok93yb
    foreign key (trescalmanagerid)
      references dbo.contact;

alter table dbo.contract
  add constraint FKo0m49jgnfhq6ug27ifvpcou73
    foreign key (currencyid)
      references dbo.supportedcurrency;

INSERT INTO dbversion(version) VALUES(371);

COMMIT TRAN

