USE [spain]
BEGIN TRAN

    alter table dbo.instrumentcomplementaryfield 
        add nominalValue numeric(19,2);

INSERT INTO dbversion VALUES (228);

COMMIT tran
