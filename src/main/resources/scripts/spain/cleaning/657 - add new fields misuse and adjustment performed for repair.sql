USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.repairinspectionreport ADD misuse BIT;
	ALTER TABLE dbo.repairinspectionreport ADD misusecomment NVARCHAR(2000);
	ALTER TABLE dbo.repaircompletionreport ADD adjustmentperformed BIT;

	GO

	-- update misuse NULL value with false
	UPDATE dbo.repairinspectionreport SET misuse = 0 WHERE misuse IS NULL;
	ALTER TABLE dbo.repairinspectionreport ALTER COLUMN misuse BIT NOT NULL;
	-- update adjustmentperformed NULL value with false
	UPDATE dbo.repaircompletionreport SET adjustmentperformed = 0 WHERE adjustmentperformed IS NULL;
	ALTER TABLE dbo.repaircompletionreport ALTER COLUMN adjustmentperformed BIT NOT NULL;

	INSERT INTO dbversion(version) VALUES(657);

COMMIT TRAN