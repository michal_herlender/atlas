-- Author Galen Beck
-- Inserts job type / cal type links
-- Needs to be run after successful app startup to create jobtypecaltype table

USE [spain]
GO

BEGIN TRAN

-- Declare calibration type IDs, these can be different between the test and production databases

DECLARE @AccreditedInhouse INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'AC-IH');
DECLARE @StandardInhouse INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'ST-IH');
DECLARE @Other INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'OTHER');
DECLARE @AccreditedOnsite INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'AC-OS');
DECLARE @StandardOnsite INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'ST-OS');
DECLARE @AccreditedExternal INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'AC-EX');
DECLARE @StandardExternal INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'STD-EX');

DECLARE @AccreditedInhouseMV INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'AC-IH-MV');
DECLARE @StandardInhouseMV INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'ST-IH-MV');
DECLARE @AccreditedOnsiteMV INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'AC-OS-MV');
DECLARE @StandardOnsiteMV INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'ST-OS-MV');
DECLARE @AccreditedExternalMV INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'AC-EX-MV');
DECLARE @StandardExternalMV INT = (SELECT [caltypeid] FROM [calibrationtype] LEFT JOIN [servicetype] ON [calibrationtype].[servicetypeid]=[servicetype].[servicetypeid] WHERE [servicetype].[shortname] = 'ST-EX-MV');

-- Note Job Type IDs are 1 - Standard, 2 - Single Price, 4 - Onsite Job

INSERT INTO [dbo].[jobtypecaltype]
           ([defaultvalue],[jobtypeid],[caltypeid])
     VALUES
           (1,1,@AccreditedInhouse),
           (0,1,@StandardInhouse),
           (0,1,@Other),
           (0,1,@AccreditedExternal),
           (0,1,@StandardExternal),
           (0,1,@AccreditedInhouseMV),
           (0,1,@StandardInhouseMV),
           (0,1,@AccreditedExternalMV),
           (0,1,@StandardExternalMV),

           (1,2,@AccreditedInhouse),
           (0,2,@StandardInhouse),
           (0,2,@Other),
           (0,2,@AccreditedExternal),
           (0,2,@StandardExternal),
           (0,2,@AccreditedInhouseMV),
           (0,2,@StandardInhouseMV),
           (0,2,@AccreditedExternalMV),
           (0,2,@StandardExternalMV),

           (1,4,@AccreditedOnsite),
           (0,4,@StandardOnsite),
           (0,4,@Other),
           (0,4,@AccreditedOnsiteMV),
           (0,4,@StandardOnsiteMV)

GO

COMMIT TRAN