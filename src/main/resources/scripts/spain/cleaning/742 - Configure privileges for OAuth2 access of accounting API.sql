-- Create new permission group for access to /api/accounting web service via OAuth2
-- Used for accounting data synchronization - update to Atlas hence the "sync" naming

USE [atlas]
GO

BEGIN TRAN

-- Step 1 - add new group for accounting sync features

INSERT INTO [dbo].[permissiongroup] ([name]) VALUES ('GROUP_WEB_SERVICE_ACCOUNTING_SYNC')
GO

-- Step 2 - Add new role for accounting sync (will contain core group + this new one)

INSERT INTO [dbo].[perrole] ([name]) VALUES ('ROLE_WEB_SERVICES_ACCOUNTING_SYNC');
GO

DECLARE @groupAccounting int;
SELECT @groupAccounting = [id] FROM [dbo].[permissiongroup] WHERE name = 'GROUP_WEB_SERVICE_ACCOUNTING_SYNC';
PRINT @groupAccounting;

DECLARE @groupWebServiceCore int;
SELECT @groupWebServiceCore = [id] FROM [dbo].[permissiongroup] where [name] = 'GROUP_WEB_SERVICES_CORE';
PRINT @groupWebServiceCore;

DECLARE @roleWebServicesAccounting int;
select @roleWebServicesAccounting =       [id] FROM [dbo].[perrole] where [name] = 'ROLE_WEB_SERVICES_ACCOUNTING_SYNC';
PRINT @roleWebServicesAccounting;

-- Step 3 - Add permission for accounting sync to new group

INSERT INTO [dbo].[permission] ([groupId],[permission]) VALUES (@groupAccounting , 'WEB_SERVICE_ACCOUNTING_SYNC')

-- Step 4 - Add groups into new role

-- Add the "core" web service group into the new role
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleWebServicesAccounting, @groupWebServiceCore);
-- Add the "accounting sync" web service group into the new role
INSERT INTO [dbo].[rolegroup]  ([roleId] ,[groupId]) VALUES (@roleWebServicesAccounting, @groupAccounting);

-- Step 5 - Create new client details for authenticating with the new role

INSERT INTO [dbo].[oauth2clientdetails]
           ([clientid]
           ,[clientSecret]
           ,[role]
           ,[resourceIds]
           ,[scope]
           ,[accesstokenvalidity]
           ,[refreshtokenvalidity]
           ,[description])
     VALUES
           ('accounting_sync'
           ,'k50@060f7H'
           ,@roleWebServicesAccounting
           ,'resource-server-rest-api'
           ,'all'
           ,36000
           ,36000
           ,'Accounting Synchronization')


INSERT INTO dbversion(version) VALUES (742);

GO

COMMIT TRAN