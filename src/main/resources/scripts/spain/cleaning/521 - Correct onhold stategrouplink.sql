-- Removes activity "Item taken off hold" from state group link 19 - "on hold".
-- This was introduced in script 464, but causes a bug DEV-1102 where :
-- the use removes an item from hold, deletes the removal activty, and then tries again to remove from hold.
-- The system would incorrectly treat the "off hold" activity as an "on hold" activity resulting in an incorrect status
-- The job item would then be stuck with no recourse

USE [atlas]
GO

BEGIN TRAN

DELETE FROM [dbo].[stategrouplink]
      WHERE [stategrouplink].groupid = 19 and [stategrouplink].stateid = 163
GO

INSERT INTO dbversion(version) VALUES(521);
GO

COMMIT TRAN