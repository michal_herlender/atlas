-- Updates german transport method translations (inserting if missing)

USE [spain]
GO

BEGIN TRAN

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=1 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Trescal-Shuttle' WHERE id=1 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (1,'de_DE','Trescal-Shuttle');

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=2 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Kunde' WHERE id=2 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (2,'de_DE','Kunde')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=3 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Kurierdienst Kunde' WHERE id=3 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (3,'de_DE','Kurierdienst Kunde')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=4 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Keine' WHERE id=4 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (4,'de_DE','Keine')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=5 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Express-Versand' WHERE id=5 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (5,'de_DE','Express-Versand')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=6 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Standard-Versand' WHERE id=6 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (6,'de_DE','Standard-Versand')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=7 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Spezifischer-Versand' WHERE id=7 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (7,'de_DE','Spezifischer-Versand')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=8 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Trescal-Shuttle 1' WHERE id=8 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (8,'de_DE','Trescal-Shuttle 1')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=9 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Trescal-Shuttle 2' WHERE id=9 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (9,'de_DE','Trescal-Shuttle 2')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=10 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Trescal-Shuttle 3' WHERE id=10 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (10,'de_DE','Trescal-Shuttle 3')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=11 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Trescal-Shuttle 4' WHERE id=11 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (11,'de_DE','Trescal-Shuttle 4')

IF EXISTS (SELECT 1 FROM transportmethodtranslation WHERE id=12 and locale='de_DE') 
	UPDATE transportmethodtranslation SET translation = 'Trescal-Shuttle 5' WHERE id=12 and locale='de_DE'
ELSE 
	INSERT INTO transportmethodtranslation VALUES (12,'de_DE','Trescal-Shuttle 5')

INSERT INTO dbversion(version) VALUES(406);
GO

COMMIT TRAN