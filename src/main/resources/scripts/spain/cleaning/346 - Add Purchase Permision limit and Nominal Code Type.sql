USE [spain]
Begin Tran

/* Create table who store manageable limit of purchasse permission*/
Create Table [spain].[dbo].purchasepermissionbycompany(
id int identity not null,
permissionName varchar(255) not null,
maximum numeric(10,2) not null,
orgid int not null,
lastModified datetime2(7) not null,
lastModifiedBy int null,
primary key (id)
);

ALTER TABLE spain.dbo.purchasepermissionbycompany
ADD CONSTRAINT FK_purchasePermission_company 
FOREIGN KEY(orgid) REFERENCES spain.dbo.company(coid);

ALTER TABLE spain.dbo.purchasepermissionbycompany
ADD CONSTRAINT FK_purchasePermission_contact
FOREIGN KEY(lastModifiedBy) REFERENCES spain.dbo.contact(personid);

/* Insert new permission on Purchase Limit in  each company where corole = 5 'Bussiness' */

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'OPEX_LOW_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'OPEX_MEDUIM_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'OPEX_HIGH_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'GENEX_LOW_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'GENEX_MEDUIM_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'GENEX_HIGH_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'CAPEX_LOW_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'CAPEX_MEDUIM_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.purchasepermissionbycompany(permissionName,maximum,orgid,lastModified)
  SELECT 'CAPEX_HIGH_PURCHASE_LIMIT', 10000, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

/* Add column Type to be able to diferentiate CAPEX OPEX GENEX */
alter table dbo.nominalcode add typenominalcode varchar(255) null
Go

UPDATE dbo.nominalcode SET typenominalcode='CAPEX'
FROM dbo.nominalcode 
where code like 'PC%'
GO

UPDATE dbo.nominalcode SET typenominalcode='OPEX'
FROM dbo.nominalcode 
where code like 'PO%'
GO

UPDATE dbo.nominalcode SET typenominalcode='GENEX'
FROM dbo.nominalcode 
where code like 'PG%'
GO

/* ADD mangagemnt to admin userright and add limitless to every user*/
insert into permission(groupId, permission)
values 
(3, 'MANAGE_PURCHASE_LIMIT'),
(1, 'OPEX_PURCHASE_LIMITLESS'),
(1, 'GENEX_PURCHASE_LIMITLESS'),
(1, 'CAPEX_PURCHASE_LIMITLESS')

INSERT INTO dbversion(version) VALUES(346);

COMMIT TRAN