-- Fixes / adjusts some system default settings which were available for "client" but not for "business"

use [atlas];
go

begin tran

-- Insert new systemdefaultcorole entries for some system defaults currently only available at "client"
-- which also make sense to have at "business" company level

/*
	11 : Client - Certificate : Electronic Certs Only
	25 : Client - Certificate : Obsolete Certificate Functionality
	15 : Client - Costings : Approved Limit
	14 : Client - Costings : Confirmation Costing
	3 : Client - Invoicing : Monthly Invoice
	4 : Client - Invoicing : PO Invoice
	57 : Client - Invoicing : Print appendix
	16 : Client - Misc : Delivery Approval 
	56 : Client - Misc : Disallow same plant number
	5 : Client - Misc : Part-Delivery
	50 : Fault report settings : Send Fault report to client
*/

INSERT INTO [dbo].[systemdefaultcorole]
           ([coroleid]
           ,[defaultid])
     VALUES
           (5, 11)
           ,(5, 25)
           ,(5, 15)
           ,(5, 14)
           ,(5, 3)
           ,(5, 4)
           ,(5, 57)
           ,(5, 16)
           ,(5, 56)
           ,(5, 5)
           ,(5, 50)
GO

UPDATE [dbo].[systemdefault]
   SET [defaultdescription] = 'Whether an additonal quality control step is used in the calibration tool'
 WHERE [defaultid] = 68
GO

UPDATE [dbo].[systemdefault]
   SET [defaultdescription] = 'Whether an adjustment is performed when calculating the recall date for an instrument'
 WHERE [defaultid] = 69
GO

/*
	Commented out, will run on US servers only.
	
	36 : Client - Misc : Print Subdivision Name on Address : False -> True
	4 : Client - Misc : PO Invoice : False -> True
	14 : Client - Costings : Confirmation Costing : True -> False
*/

/*
UPDATE [dbo].[systemdefault] SET [defaultvalue] = 'TRUE' WHERE defaultid = 36
UPDATE [dbo].[systemdefault] SET [defaultvalue] = 'TRUE' WHERE defaultid = 4
UPDATE [dbo].[systemdefault] SET [defaultvalue] = 'FALSE' WHERE defaultid = 14
*/

insert into dbversion values (829)

commit tran