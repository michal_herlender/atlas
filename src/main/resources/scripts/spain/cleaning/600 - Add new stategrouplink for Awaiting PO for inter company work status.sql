
USE [atlas]
GO
BEGIN TRAN

DECLARE @awaitinginternalpo INT;

SELECT @awaitinginternalpo = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting PO for inter company work';

INSERT INTO dbo.stategrouplink(groupid,stateid,type) VALUES (44,@awaitinginternalpo,'ALLOCATED_SUBDIV');

INSERT INTO dbversion(version) VALUES (600);

COMMIT TRAN