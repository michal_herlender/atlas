-- Removes unused logging columns from several tables.
-- Assumption made that any affected indices containing log_lastmodified / log_userid are unused so they are removed as well.

USE [atlas]

BEGIN TRAN

ALTER TABLE [dbo].[mfr] DROP CONSTRAINT [FK1A5F9202933D5]
ALTER TABLE [mfr] DROP COLUMN log_lastmodified;
ALTER TABLE [mfr] DROP COLUMN log_userid;

ALTER TABLE [dbo].[instmodel] DROP CONSTRAINT [FK26590A3202933D5]
DROP INDEX [_dta_index_instmodel_7_516196889__K1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25] ON [dbo].[instmodel]
DROP INDEX [_dta_index_instmodel_7_516196889__K21_K22_K15_1_2_3_4_5_6_7_8_9_10_11_12_13_14_16_17_18_19_20_23_24_25] ON [dbo].[instmodel]
ALTER TABLE [instmodel] DROP COLUMN log_lastmodified;
ALTER TABLE [instmodel] DROP COLUMN log_userid;

ALTER TABLE [dbo].[description] DROP CONSTRAINT [FK993583FC202933D5]
ALTER TABLE [description] DROP COLUMN log_lastmodified;
ALTER TABLE [description] DROP COLUMN log_userid;

INSERT INTO dbversion(version) VALUES(682);

COMMIT TRAN