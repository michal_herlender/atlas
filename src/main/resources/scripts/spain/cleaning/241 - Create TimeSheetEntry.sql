USE [spain]
GO

BEGIN TRAN
 
    create table dbo.timesheetentry (
        id int identity not null,
        comment varchar(255),
        duration bigint not null,
        start datetime2 not null,
        employeeid int not null,
        jobid int,
        servicetypeid int not null,
        subdivid int,
        primary key (id)
    );

    alter table dbo.timesheetentry 
        add constraint FK_timesheetentry_employee 
        foreign key (employeeid) 
        references dbo.contact;

    alter table dbo.timesheetentry 
        add constraint FK_timesheetentry_job 
        foreign key (jobid) 
        references dbo.job;

    alter table dbo.timesheetentry 
        add constraint FK_timesheetentry_servicetype 
        foreign key (servicetypeid) 
        references dbo.servicetype;

    alter table dbo.timesheetentry 
        add constraint FK_timesheetentry_subdiv 
        foreign key (subdivid) 
        references dbo.subdiv;
	
	insert into dbversion(version) values(241);

COMMIT TRAN