/* Delete Invoice Type "Valves" (requirement of D.Bernard) - T.Provost 2016-04-04 */

BEGIN TRAN 

USE [SPAIN];
GO

BEGIN TRAN

DELETE FROM [dbo].[invoicetypetranslation] WHERE [id] IN (SELECT [id] FROM [dbo].[invoicetype] WHERE [name] = 'Valves')
GO

DELETE FROM [dbo].[invoicedoctypetemplate] WHERE [typeid] IN (SELECT [id] FROM [dbo].[invoicetype] WHERE [name] = 'Valves')
GO

DELETE FROM [dbo].[invoicetype] WHERE [name] = 'Valves'
GO

ROLLBACK TRAN