-- This script fixes null cal addresses on inter-subdiv or inter-company job items 
-- Galen Beck - 2017-04-26
-- First part of script identifies job items with issue, uncomment remainder once verified

USE [spain]
GO

SELECT [jobitemid]
      ,[itemno]
      ,[caladdrid]
      ,[addrid]
      ,[plantid]
      ,jobitem.[jobid]
      ,[nextworkreqid]
      ,jobitem.[stateid]
	  ,job.jobno
  FROM jobitem
  LEFT JOIN job on jobitem.jobid = job.jobid
  WHERE stateid = 19 AND [caladdrid] is null;

GO

/*

BEGIN TRAN

UPDATE [dbo].[jobitem]
   SET [caladdrid] = [addrid]
 WHERE stateid = 19 AND [caladdrid] is null;

GO

COMMIT TRAN */

