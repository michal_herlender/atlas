USE [atlas]
GO

BEGIN TRAN

INSERT INTO dbo.calibrationprocess (actionafterissue,applicationcomponent,description,fileextension,issueimmediately,process,quicksign,signonissue,signingsupported,uploadafterissue,uploadbeforeissue)
	VALUES (0,0,'TLM generates the Certificate document that will be imported afterwards.','.pdf',0,'TLM IMPORTED',0,0,0,0,1);
	
INSERT INTO dbversion(version) VALUES(544);
	


COMMIT TRAN