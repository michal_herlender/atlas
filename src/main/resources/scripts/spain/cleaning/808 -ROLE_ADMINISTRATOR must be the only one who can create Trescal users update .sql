USE [atlas]
Go

BEGIN TRAN


	DELETE  from permission where permission='ADD_CONTACT_BUSINESS'

	DECLARE @PermissionGroupId INT;
	SELECT @PermissionGroupId= id FROM dbo.permissiongroup WHERE name='GROUP_USER_RIGHT_MANAGEMENT ';

	INSERT INTO atlas.dbo.permission (groupId, permission) VALUES(@PermissionGroupId, 'ADD_CONTACT_BUSINESS');

INSERT INTO dbversion(version) VALUES(808);

COMMIT TRAN
