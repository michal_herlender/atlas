-- Create new table 'jobitemhistory'

USE [spain]
GO

BEGIN TRAN

CREATE TABLE dbo.jobitemhistory (
	id int NOT NULL IDENTITY(1,1),
	jobitemid int NOT NULL,
	changeby varchar(100),
	changedate datetime,
	comments text,
	duedateupdated tinyint,
	oldduedate datetime,
	newduedate datetime,
	CONSTRAINT jobitemhistory_PK PRIMARY KEY (id),
	CONSTRAINT jobitemhistory_jobitem_FK FOREIGN KEY (jobitemid) REFERENCES dbo.jobitem(jobitemid)
)
GO

ALTER TABLE dbo.notificationsystemfieldchange ADD changedate datetime
GO

INSERT INTO dbversion(version) VALUES(288);

COMMIT TRAN