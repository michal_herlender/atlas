use spain;

begin tran

if not exists (select * from stategrouplink where groupid = 49 and stateid = 172)
begin
	insert into stategrouplink(groupid, stateid, type) values (49, 172, 'ALLOCATED_SUBDIV')
end

insert into dbversion values (378)

commit tran