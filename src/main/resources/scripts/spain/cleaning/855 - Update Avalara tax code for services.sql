BEGIN TRAN

UPDATE [dbo].[nominalcode]
   SET [avalarataxcode] = 'S0000001'
 WHERE [avalarataxcode] = 'S0000000'
GO

INSERT INTO dbversion(version) VALUES(855);

COMMIT TRAN