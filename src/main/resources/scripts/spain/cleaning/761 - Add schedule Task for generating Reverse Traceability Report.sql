USE [atlas]
GO

BEGIN TRAN

	INSERT INTO dbo.scheduledtask (active, classname, description, lastrunsuccess, methodname, quartzjobname, taskname)
	VALUES 
		  (0
		   ,'org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.ReverseTraceabilityMailer'
           ,'Search for failures for standards belonging to any business subdivision, generate reverse traceability report and send an email to subscribers'
           ,0
		   ,'findfailures'
           ,'jobDetail_ReverseTraceabilityMailerTarget'
           ,'Reverse traceability');

	GO

	INSERT INTO dbversion(version) VALUES(761);

COMMIT TRAN
