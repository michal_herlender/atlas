Use atlas
Go

BEGIN TRAN

	-- ThirdParty Workflow
	DECLARE @awaiting_rcr_update_tp_status INT,
		@awaiting_ie_tp_status INT,
		@awaiting_selection_next_wr_tp_status INT,
		@awaiting_po_intercomp_tp_status INT,
		@awaiting_rcr_validation_tp_status INT,
		@ji_completed_sameaddress_tp_status INT,
		@awaiting_client_payment_tp_status INT,
		@awaiting_approval_dn_tp_status INT,
		@awaiting_dn_client_tp_status INT,
		@awaiting_despatch_bc_tp_status INT,
		@delivery_date_required_tp_status INT,
		@awaiting_despatch_client_tp_status INT,
		@tp_repair_unsuccessful_tp_status INT,
		@awaiting_dn_tp_status INT,
		@awaiting_despatch_tp_status INT,
		@awaiting_confirm_destination_tp_status INT,
		@awaiting_inhouse_repair_tp_status INT,
		@awaiting_adjust_tp_status INT,
		@awaiting_costing_tp_status INT,
		@awaiting_work_completed_tp_status INT,
		@unit_transit_bc_tp_status INT,
		@awaiting_completion_additional_tp_status INT,
		@awaiting_informational_fr_tp_status INT;

	SELECT @awaiting_rcr_update_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting repair completion report update (post third party work)';
	SELECT @awaiting_ie_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting inspection by engineer (post third party work)';
	SELECT @awaiting_selection_next_wr_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting selection of next work requirement';
	SELECT @awaiting_po_intercomp_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting PO for inter company work';
	SELECT @awaiting_rcr_validation_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting Repair Completion Report Validation';
	SELECT @ji_completed_sameaddress_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Job item completed at same address as instrument address';
	SELECT @awaiting_client_payment_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting client payment prior to despatch';
	SELECT @awaiting_approval_dn_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting approval for delivery note creation';
	SELECT @awaiting_dn_client_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting delivery note to client';
	SELECT @awaiting_despatch_bc_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting despatch to business company';
	SELECT @delivery_date_required_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Delivery date required from client';
	SELECT @awaiting_despatch_client_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting despatch to client';
	SELECT @tp_repair_unsuccessful_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Third party repair un-successful, awaiting fault report update';
	SELECT @awaiting_dn_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting delivery note to third party';
	SELECT @awaiting_despatch_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting despatch to third party';
	SELECT @awaiting_confirm_destination_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting confirmation of destination receipt';
	SELECT @awaiting_inhouse_repair_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting in-house repair';
	SELECT @awaiting_adjust_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting adjustment';
	SELECT @awaiting_costing_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting costing after third party work';
	SELECT @awaiting_work_completed_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Work completed, despatched to client';
	SELECT @unit_transit_bc_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Unit in transit to business company';
	SELECT @awaiting_completion_additional_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting completion of additional demands';
	SELECT @awaiting_informational_fr_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting informational failure report to be sent';


	INSERT INTO dbo.stategrouplink(groupid, stateid, [type])
	VALUES (78, @awaiting_rcr_update_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_ie_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_selection_next_wr_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_po_intercomp_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_rcr_validation_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @ji_completed_sameaddress_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_client_payment_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_approval_dn_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_dn_client_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_despatch_bc_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @delivery_date_required_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_despatch_client_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @tp_repair_unsuccessful_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_dn_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_despatch_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_confirm_destination_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_inhouse_repair_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_adjust_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_costing_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_work_completed_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @unit_transit_bc_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_completion_additional_tp_status, 'ALLOCATED_SUBDIV'),
			(78, @awaiting_informational_fr_tp_status, 'ALLOCATED_SUBDIV');

	-- Add new activity 'Third party certificate cancel and replace'
	INSERT INTO dbo.itemstate([type],active,retired,[description],lookupid,actionoutcometype)
	VALUES ('workactivity', 1, 0, 'Third party certificate cancel and replace', NULL, NULL);
	
	DECLARE @new_activity INT;
	SELECT @new_activity = MAX(stateid) FROM dbo.itemstate;
	
	INSERT INTO dbo.itemstatetranslation(stateid,locale,translation)
	VALUES (@new_activity, 'en_GB', 'Third party certificate cancel and replace'),
			(@new_activity, 'fr_FR', 'Annulation et remplacement du certificat tiers'),
			(@new_activity, 'es_ES', 'Cancelar y reemplazar certificado de terceros'),
			(@new_activity, 'de_DE', 'Zertifikat eines Drittanbieters stornieren und ersetzen');
	
	-- Add new hook for the activity 'Third party certificate cancel and replace'
	INSERT INTO dbo.hook(active,activityid,alwayspossible,beginactivity,completeactivity,[name])
	VALUES (1, NULL, 0, 1, 1, 'start-thirdparty-certificate-replacement');
	
	DECLARE @new_hook INT;
	SELECT @new_hook = MAX(id) FROM dbo.hook;

	INSERT INTO dbo.hookactivity(beginactivity,completeactivity,hookid,activityid,stateid)
	VALUES (1, 1, @new_hook, @new_activity, @awaiting_rcr_update_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_ie_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_selection_next_wr_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_po_intercomp_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_rcr_validation_tp_status),
			(1, 1, @new_hook, @new_activity, @ji_completed_sameaddress_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_client_payment_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_approval_dn_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_dn_client_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_despatch_bc_tp_status),
			(1, 1, @new_hook, @new_activity, @delivery_date_required_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_despatch_client_tp_status),
			(1, 1, @new_hook, @new_activity, @tp_repair_unsuccessful_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_dn_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_despatch_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_confirm_destination_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_inhouse_repair_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_adjust_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_costing_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_work_completed_tp_status),
			(1, 1, @new_hook, @new_activity, @unit_transit_bc_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_completion_additional_tp_status),
			(1, 1, @new_hook, @new_activity, @awaiting_informational_fr_tp_status);
	
	-- Add new outcomestatus for the new activity 'Third party certificate cancel and replace'
	DECLARE @awaiting_tp_work INT;
	SELECT @awaiting_tp_work = stateid FROM dbo.itemstate WHERE [description] = 'Unit at third party for work';
	INSERT INTO dbo.outcomestatus(defaultoutcome,lookupid,activityid,statusid)
	VALUES (1, NULL, @new_activity, @awaiting_tp_work);

	-- Onsite workflow
	DECLARE @onsite_awaiting_jobcosting INT,
			@onsite_awaiting_informational_fr INT,
			@onsite_complete_service INT;
	
	SELECT @onsite_awaiting_jobcosting = stateid FROM dbo.itemstate WHERE [description] = 'Onsite - calibration performed, awaiting job costing';
	SELECT @onsite_awaiting_informational_fr = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting informational failure report to be sent';
	SELECT @onsite_complete_service = stateid FROM dbo.itemstate WHERE [description] = 'Completed onsite service';

	INSERT INTO dbo.stategrouplink(groupid, stateid, [type])
	VALUES (79, @onsite_awaiting_jobcosting, 'ALLOCATED_SUBDIV'),
			(79, @onsite_awaiting_informational_fr, 'ALLOCATED_SUBDIV'),
			(79, @onsite_complete_service, 'ALLOCATED_SUBDIV');

	-- Add new activity 'Onsite certificate cancel and replace'
	INSERT INTO dbo.itemstate([type],active,retired,[description],lookupid,actionoutcometype)
	VALUES ('workactivity', 1, 0, 'Onsite certificate cancel and replace', NULL, NULL);
	
	SELECT @new_activity = MAX(stateid) FROM dbo.itemstate;
	
	INSERT INTO dbo.itemstatetranslation(stateid,locale,translation)
	VALUES (@new_activity, 'en_GB', 'Onsite certificate cancel and replace'),
			(@new_activity, 'fr_FR', 'Annulation et remplacement du certificat sur site'),
			(@new_activity, 'es_ES', 'Cancelar y reemplazar certificado in situ'),
			(@new_activity, 'de_DE', 'Onsite certificaat annuleren en vervangen');
	
	-- Add new hook for the activity 'Onsite certificate cancel and replace'
	INSERT INTO dbo.hook(active,activityid,alwayspossible,beginactivity,completeactivity,[name])
	VALUES (1, NULL, 0, 1, 1, 'start-onsite-certificate-replacement');
	
	SELECT @new_hook = MAX(id) FROM dbo.hook;

	INSERT INTO dbo.hookactivity(beginactivity,completeactivity,hookid,activityid,stateid)
	VALUES (1, 1, @new_hook, @new_activity, @onsite_awaiting_jobcosting),
			(1, 1, @new_hook, @new_activity, @onsite_awaiting_informational_fr),
			(1, 1, @new_hook, @new_activity, @onsite_complete_service);
	
	-- Add new outcomestatus for the new activity 'Onsite certificate cancel and replace'
	DECLARE @awaiting_onsite_cal INT;
	SELECT @awaiting_onsite_cal = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting onsite calibration';
	INSERT INTO dbo.outcomestatus(defaultoutcome,lookupid,activityid,statusid)
	VALUES (1, NULL, @new_activity, @awaiting_onsite_cal);

	INSERT INTO dbversion VALUES (800);

COMMIT TRAN