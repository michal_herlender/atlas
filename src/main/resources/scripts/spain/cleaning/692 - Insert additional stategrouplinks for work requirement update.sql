-- Inserts a state group link for five additional logistics-out status values so that if we add a new work requirement, 
-- the work requirement hook 'select-next-work-requirement' would be enabled to fire.
-- Relates to ticket DEV-1831

USE [atlas]
GO

BEGIN TRAN

declare @stateId_1 int;
declare @stateId_2 int;
declare @stateId_3 int;
declare @stateId_4 int;
declare @stateId_5 int;

select @stateId_1 = stateid from dbo.itemstate where itemstate.description = 'Service completed, awaiting confirmation costing';
select @stateId_2 = stateid from dbo.itemstate where itemstate.description = 'Awaiting delivery note to client';
select @stateId_3 = stateid from dbo.itemstate where itemstate.description = 'Awaiting client payment prior to despatch';
select @stateId_4 = stateid from dbo.itemstate where itemstate.description = 'Awaiting internal return delivery note';
select @stateId_5 = stateid from dbo.itemstate where itemstate.description = 'Awaiting approval for delivery note creation';

PRINT @stateId_1;
PRINT @stateId_2;
PRINT @stateId_3;
PRINT @stateId_4;
PRINT @stateId_5;

-- Group ID is defined in enum as 52 - SELECT_NEXT_WORK_REQUIREMENT
-- type is not relevant but must be set

INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (52, @stateId_1, 'ALLOCATED_SUBDIV'),
		   (52, @stateId_2, 'ALLOCATED_SUBDIV'),
		   (52, @stateId_3, 'ALLOCATED_SUBDIV'),
		   (52, @stateId_4, 'ALLOCATED_SUBDIV'),
		   (52, @stateId_5, 'ALLOCATED_SUBDIV')
GO

INSERT INTO dbversion(version) VALUES(692);

COMMIT TRAN