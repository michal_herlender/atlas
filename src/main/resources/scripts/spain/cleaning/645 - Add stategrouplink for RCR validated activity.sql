USE [atlas]
GO

BEGIN TRAN
	
	DECLARE @rcr_validation_activity INT;

	SELECT @rcr_validation_activity = stateid FROM dbo.itemstate WHERE [description] = 'Repair Completion Report Validated';
	
	INSERT INTO dbo.stategrouplink(groupid,stateid,type)
	VALUES (70, @rcr_validation_activity, 'CURRENT_SUBDIV');

	INSERT INTO dbversion(version) VALUES (645);

COMMIT TRAN