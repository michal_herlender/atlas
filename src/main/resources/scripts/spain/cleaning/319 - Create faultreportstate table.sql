-- Add table for storing failure report state
-- (we now support multiple states for one failure report)

USE [spain]
GO

BEGIN TRAN

    create table dbo.faultreportstate (
       faultreportid int not null,
        state varchar(255)
    );

    alter table dbo.faultreportstate 
       add constraint FK_faultreportstate_faultreport
       foreign key (faultreportid) 
       references dbo.faultreport;

	INSERT INTO dbversion(version) VALUES(319);

COMMIT TRAN