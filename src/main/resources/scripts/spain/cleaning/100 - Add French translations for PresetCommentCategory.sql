-- Author Tony Provost 2016-11-22
-- Add frenchs tranlsations for default preset comment category

BEGIN TRAN 

USE [spain];
GO

DECLARE @pccLocale VARCHAR(10) = 'fr_FR'
DECLARE @pccTranslation VARCHAR(1000)
DECLARE @pccId INT

SET @pccId = 1
SET @pccTranslation = 'V�rif/Etal'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 2
SET @pccTranslation = 'G�n�ral'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 3
SET @pccTranslation = 'R�paration'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 4
SET @pccTranslation = 'Livraison'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 5
SET @pccTranslation = 'Transport'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 6
SET @pccTranslation = 'Urgence'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 7
SET @pccTranslation = 'D�lai'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 8
SET @pccTranslation = 'V�rif/Etal �lectrique'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 9
SET @pccTranslation = 'Rappel V�rif/Etal'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 10
SET @pccTranslation = 'Devis'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 11
SET @pccTranslation = 'Commande d''achat'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

SET @pccId = 12
SET @pccTranslation = 'Facturation'
IF NOT EXISTS (SELECT 1 FROM [dbo].[presetcommentcategorytranslation] WHERE [id] = @pccId AND [locale] = @pccLocale)
	INSERT INTO [dbo].[presetcommentcategorytranslation] ([id], [locale], [translation]) VALUES (@pccId, @pccLocale, @pccTranslation)
ELSE
	UPDATE [dbo].[presetcommentcategorytranslation] SET [translation] = @pccTranslation WHERE [id] = @pccId AND [locale] = @pccLocale

COMMIT TRAN