-- Script to configure 'Not Accredited' procedure types for active Morocco, Spain, and France locations
-- Also adds France lab accreditations for known/active France locations
-- Renames Great Yarmouth accreditations properly
-- Author : Galen Beck - 2017-01-16

USE [spain]
GO

BEGIN TRAN

-- Note, using ENAC logo for France (as demo) until proper COFRAC logo is obtained
-- Once obtained, this record should be updated to point to the new logo

INSERT INTO [dbo].[accreditationbodyresource]
           ([lastModified],[resourcePath],[type],[accreditationbodyid])
     VALUES
           ('2017-01-19','labels/birt/callogo_ENAC.png',1,2);
GO

SET IDENTITY_INSERT [accreditedlab] ON;

INSERT INTO [dbo].[accreditedlab]
           ([id],[lastModified],[labno],[description],[orgid],[accreditationbodyid])
     VALUES
			(9,'2017-01-19','','Bilbao - No acreditado',6642,null),
			(10,'2017-01-19','','Vitoria - No acreditado',6643,null),
			(11,'2017-01-19','','Zaragoza - No acreditado',6644,null),
			(12,'2017-01-19','','Madrid - No acreditado',6645,null),
			(13,'2017-01-19','','Casablanca - Non accr�dit�',6951,null),
			(14,'2017-01-19','','Le Havre - Non accr�dit�',6950,null),
			(15,'2017-01-19','','Lyon - Non accr�dit�',6975,null),
			(16,'2017-01-19','2-1317','Metz - Etalonnage - Electricit�-Magn�tisme',6961,2),
			(17,'2017-01-19','2-1419','Metz - Etalonnage - Pression',6961,2),
			(18,'2017-01-19','','Metz - Non accr�dit�',6961,null),
			(19,'2017-01-19','2-1823','Rungis - Etalonnage - Electricit�-Magn�tisme',6969,2),
			(20,'2017-01-19','2-1824','Rungis - Etalonnage - Temps-Fr�quence',6969,2),
			(21,'2017-01-19','2-5993','Rungis - Etalonnage - Temp�rature',6969,2),
			(22,'2017-01-19','','Rungis - Non accr�dit�',6969,null),
			(23,'2017-01-19','2-1576','Toulouse - Etalonnage - Electricit�-Magn�tisme',6965,2),
			(24,'2017-01-19','2-1577','Toulouse - Etalonnage - Temps-Fr�quence',6965,2),
			(25,'2017-01-19','2-1668','Toulouse - Etalonnage - Temp�rature',6965,2),
			(26,'2017-01-19','2-1699','Toulouse - Etalonnage - M�trologie Dimensionnelle',6965,2),
			(27,'2017-01-19','2-2061','Toulouse - Etalonnage - Hygrom�trie',6965,2),
			(28,'2017-01-19','','Toulouse - Non accr�dit�',6965,null),
			(29,'2017-01-19','2-1209','Vend�me - Etalonnage - M�trologie Dimensionnelle',6976,2),
			(30,'2017-01-19','2-1631','Vend�me - Etalonnage - Force',6976,2),
			(31,'2017-01-19','2-1953','Vend�me - Etalonnage - Masse',6976,2),
			(32,'2017-01-19','','Vend�me - Non accr�dit�',6976,null)
GO

SET IDENTITY_INSERT [accreditedlab] OFF;

UPDATE [dbo].[accreditedlab] SET [description] = 'Great Yarmouth - Calibration' WHERE [id] = 1;
UPDATE [dbo].[accreditedlab] SET [description] = 'Great Yarmouth - Calibration' WHERE [id] = 2;
UPDATE [dbo].[accreditedlab] SET [description] = 'Great Yarmouth - Not Accredited' WHERE [id] = 8;

GO

-- Update non-accredited procedures to point to correct places rather than the old Antech one
-- Spain
UPDATE [dbo].[procs] SET [accreditedlabid] = 9 WHERE [accreditedlabid] = 8 AND [orgid] = 6642;
GO
UPDATE [dbo].[procs] SET [accreditedlabid] = 10 WHERE [accreditedlabid] = 8 AND [orgid] = 6643;
GO
UPDATE [dbo].[procs] SET [accreditedlabid] = 11 WHERE [accreditedlabid] = 8 AND [orgid] = 6644;
GO
UPDATE [dbo].[procs] SET [accreditedlabid] = 12 WHERE [accreditedlabid] = 8 AND [orgid] = 6645;
GO
-- Morocco
UPDATE [dbo].[procs] SET [accreditedlabid] = 13 WHERE [accreditedlabid] = 8 AND [orgid] = 6951;
GO
-- France
UPDATE [dbo].[procs] SET [accreditedlabid] = 14 WHERE [accreditedlabid] = 8 AND [orgid] = 6950;
GO
UPDATE [dbo].[procs] SET [accreditedlabid] = 15 WHERE [accreditedlabid] = 8 AND [orgid] = 6975;
GO
UPDATE [dbo].[procs] SET [accreditedlabid] = 18 WHERE [accreditedlabid] = 8 AND [orgid] = 6961;
GO
UPDATE [dbo].[procs] SET [accreditedlabid] = 22 WHERE [accreditedlabid] = 8 AND [orgid] = 6969;
GO
UPDATE [dbo].[procs] SET [accreditedlabid] = 28 WHERE [accreditedlabid] = 8 AND [orgid] = 6965;
GO
UPDATE [dbo].[procs] SET [accreditedlabid] = 32 WHERE [accreditedlabid] = 8 AND [orgid] = 9976;
GO

COMMIT TRAN