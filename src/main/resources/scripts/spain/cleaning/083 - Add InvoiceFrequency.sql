-- Author Tony Provost 2016-11-02
-- Add new record in table invoicefrequency : EQ - End of quarter based on recapitulative statement for the quarter completed

BEGIN TRAN 

USE [spain];
GO

SET IDENTITY_INSERT [invoicefrequency] ON
GO
 
IF NOT EXISTS (SELECT 1 FROM [invoicefrequency] WHERE [id] = 8)
	INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (8, 'EQ', 'End of quarter based on recapitulative statement for the quarter completed')
GO

-- English Translation
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 8 AND locale = 'en_GB') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (8, 'en_GB', 'End of quarter based on recapitulative statement for the quarter completed')											
GO
-- Spanish Translation
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 8 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (8, 'es_ES', 'A final del trimestre se factura todo lo realizado durante ese trimestre')											
GO

-- French Translation
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 8 AND locale = 'fr_FR') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (8, 'fr_FR', 'Facturation récapitulative fin de trimestre')											
GO

SET IDENTITY_INSERT [invoicefrequency] OFF
GO

COMMIT TRAN


