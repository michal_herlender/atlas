-- Script 266 - Adds actionoutcometype to itemstate table (optional field, used by itemactivity only)
-- Initializes existing activities that have actionoutcomes
-- Used by workflow editor to provide available action outcomes when used

USE [spain]

BEGIN TRAN

alter table dbo.itemstate 
      add actionoutcometype varchar(255);

GO

-- Intialized based on analysis from Excel file

UPDATE [dbo].[itemstate] SET actionoutcometype='CONTRACT_REVIEW' WHERE stateid=8;
UPDATE [dbo].[itemstate] SET actionoutcometype='FAULT_REPORT' WHERE stateid=16;
UPDATE [dbo].[itemstate] SET actionoutcometype='ENGINEER_INSPECTION' WHERE stateid=18;
UPDATE [dbo].[itemstate] SET actionoutcometype='ENGINEER_INSPECTION' WHERE stateid=25;
UPDATE [dbo].[itemstate] SET actionoutcometype='CALIBRATION' WHERE stateid=26;
UPDATE [dbo].[itemstate] SET actionoutcometype='FAULT_REPORT' WHERE stateid=38;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_COSTING' WHERE stateid=43;
UPDATE [dbo].[itemstate] SET actionoutcometype='REPAIR' WHERE stateid=57;
UPDATE [dbo].[itemstate] SET actionoutcometype='CALIBRATION' WHERE stateid=61;
UPDATE [dbo].[itemstate] SET actionoutcometype='COMPONENTS_FITTED' WHERE stateid=67;
UPDATE [dbo].[itemstate] SET actionoutcometype='ADJUSTMENT' WHERE stateid=68;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_AS_FOUND' WHERE stateid=70;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_FURTHER_WORK' WHERE stateid=76;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_COSTING' WHERE stateid=82;
UPDATE [dbo].[itemstate] SET actionoutcometype='TP_REQUIREMENTS' WHERE stateid=90;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_TP_INSPECTION_COSTING' WHERE stateid=94;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_TP_JOB_COSTING' WHERE stateid=107;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_TP_FURTHER_WORK' WHERE stateid=125;
UPDATE [dbo].[itemstate] SET actionoutcometype='POST_TP_FURTHER_WORK' WHERE stateid=131;
UPDATE [dbo].[itemstate] SET actionoutcometype='POST_TP_FURTHER_WORK' WHERE stateid=133;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_EXCHANGE' WHERE stateid=148;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_COSTING' WHERE stateid=181;
UPDATE [dbo].[itemstate] SET actionoutcometype='CALIBRATION' WHERE stateid=4048;
UPDATE [dbo].[itemstate] SET actionoutcometype='CONTRACT_REVIEW' WHERE stateid=4055;
UPDATE [dbo].[itemstate] SET actionoutcometype='CALIBRATION' WHERE stateid=4079;
UPDATE [dbo].[itemstate] SET actionoutcometype='ADJUSTMENT' WHERE stateid=4081;
UPDATE [dbo].[itemstate] SET actionoutcometype='CLIENT_DECISION_COSTING' WHERE stateid=4083;

INSERT INTO dbversion VALUES (266)

COMMIT TRAN