USE [atlas]
GO

BEGIN TRAN

	DECLARE @last_os_id int;
 
	-- for lookup : 41 - Lookup_ClientDecision
	INSERT INTO dbo.outcomestatus(defaultoutcome, activityid, lookupid, statusid) VALUES 
	(0, 81, NULL, 79);
	SELECT @last_os_id = MAX(id) FROM dbo.outcomestatus;
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage) VALUES(82, 41, @last_os_id, 'c', 2);

	-- for lookup : 45 - Lookup_ClientDecision
	INSERT INTO dbo.outcomestatus(defaultoutcome, activityid, lookupid, statusid) VALUES 
	(0, 93, NULL, 91);
	SELECT @last_os_id = MAX(id) FROM dbo.outcomestatus;
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage) VALUES(94, 45, @last_os_id, 'c', 2);

	-- for lookup : 48 - Lookup_ClientDecision
	INSERT INTO dbo.outcomestatus(defaultoutcome, activityid, lookupid, statusid) VALUES 
	(0, 106, NULL, 104);
	SELECT @last_os_id = MAX(id) FROM dbo.outcomestatus;
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage) VALUES(107, 48, @last_os_id, 'c', 2);

	-- for lookup : 56 - Lookup_ClientDecision
	INSERT INTO dbo.outcomestatus(defaultoutcome, activityid, lookupid, statusid) VALUES 
	(0, 147, NULL, 145);
	SELECT @last_os_id = MAX(id) FROM dbo.outcomestatus;
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage) VALUES(148, 56, @last_os_id, 'c', 2);

	-- for lookup : 83 - Lookup_ClientDecision
	INSERT INTO dbo.outcomestatus(defaultoutcome, activityid, lookupid, statusid) VALUES 
	(0, 4084, NULL, 4087);
	SELECT @last_os_id = MAX(id) FROM dbo.outcomestatus;
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage) VALUES(4083, 83, @last_os_id, 'c', 2);

	INSERT INTO dbversion(version) VALUES(503);

COMMIT TRAN