USE [spain]
GO

BEGIN TRAN

	ALTER TABLE dbo.faultreport ADD [source] varchar(100) NULL
 	GO

	UPDATE dbo.itemstatetranslation
	SET [translation]='En attente de l''envoi au client du constat d''anomalie'
	WHERE [translation]='En attente l''envoie du constat d''anomalie' 
	
	INSERT INTO dbversion(version) VALUES (423);

COMMIT TRAN