-- Author Galen Beck - 2016-10-12
-- Inserts optional free zone Vat Rate for Morocco
-- Note VatRateType type 1 - Country Special (0 is Country Default)
USE [spain]
GO

BEGIN TRAN;

IF NOT EXISTS (SELECT [vatcode] FROM [dbo].[vatrate] WHERE [type] = 1 AND [countryid] = 117)
    INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid])
    VALUES
           ('8','Morocco Free Zone',0.00,1,117);
GO

COMMIT TRAN;