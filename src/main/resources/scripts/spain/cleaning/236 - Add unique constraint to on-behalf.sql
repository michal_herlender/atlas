USE [spain]
BEGIN TRAN

    alter table dbo.onbehalfitem 
        add constraint UK80d7ha27vueaicjmfv3eh6lwo unique (jobitemid);

INSERT INTO dbversion (version) VALUES (236);

COMMIT TRAN