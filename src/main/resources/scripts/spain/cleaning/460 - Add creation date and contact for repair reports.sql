USE [spain];


BEGIN TRAN

	ALTER TABLE dbo.repairinspectionreport ADD creationdate datetime2 NULL;
	ALTER TABLE dbo.repairinspectionreport ADD createdby int NULL;
	ALTER TABLE dbo.repairinspectionreport 
		ADD CONSTRAINT repairinspectionreport_contact_createdby_FK FOREIGN KEY (createdby) REFERENCES dbo.contact(personid);

	INSERT INTO dbversion (version) VALUES (460);

COMMIT TRAN