INSERT INTO instrumentusagetype (name, fulldescription, recall,defaulttype)
VALUES ('Replacement','',0,0);

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Replacement'),
        'en_GB',
        'Replacement');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Replacement'),
        'es_ES',
        'Repuesto');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Replacement'),
        'fr_FR',
        'Rechange');