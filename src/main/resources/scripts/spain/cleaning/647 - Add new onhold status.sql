USE [atlas]
GO

BEGIN TRAN

	declare @maxId int;
	
	INSERT INTO itemstate ([type],active,description,retired)
		VALUES ('holdstatus',1,'On hold - Site job, Repair or Third party issue',0);
		
	select @maxId = max(stateid) from itemstate;
	
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@maxId,'de_DE','In der Warteschleife - Site-Job, Reparatur oder Problem mit Drittanbietern');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@maxId,'en_GB','On hold - Site job, Repair or Third party issue');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@maxId,'es_ES','En espera: trabajo en el sitio, reparaci�n o problema de terceros');
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@maxId,'fr_FR','En attente - Travail sur site, probl�me de r�paration ou sous traitance');
	

	INSERT INTO dbversion(version) VALUES (647);

COMMIT TRAN