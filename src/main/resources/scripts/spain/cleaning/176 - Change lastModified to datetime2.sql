-- Script 176 - Helps resolve optimistic locking issues
-- Galen Beck 2017-10-10
-- Changes all Versioned entities to datetime2 not null
-- Changes all Auditable entities to datetime2 (null allowed) - included for consistency's sake
-- May take 2+ minutes to run on local development machines

USE [spain]

BEGIN TRAN

ALTER TABLE dbo.accessory ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.accreditationbody ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.accreditationbodyresource ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.accreditedlab ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.additionalcostcontact ALTER COLUMN lastModified datetime2;

print('address')

ALTER TABLE dbo.address ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.addressplantillassettings ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.addresssettingsforallocatedcompany ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.addresssettingsforallocatedsubdiv ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.asset ALTER COLUMN lastModified datetime2;

print('assetnote')

ALTER TABLE dbo.assetnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.bankaccount ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.bankdetail ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.baseinstruction ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.batchcalibration ALTER COLUMN lastModified datetime2;

print('bpo')

ALTER TABLE dbo.bpo ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.businessarea ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.businesscompanysettings ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.businessemails ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.businesssubdivisionsettings ALTER COLUMN lastModified datetime2 not null;

print('calibration')

ALTER TABLE dbo.calibration ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.calibrationpoint ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.calibrationpointset ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.callink ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.calloffitem ALTER COLUMN lastModified datetime2 not null;

print('calreq')

ALTER TABLE dbo.calreq ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.catalogprice ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.certificate ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.certificatevalidation ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.certlink ALTER COLUMN lastModified datetime2;

print('certnote')

ALTER TABLE dbo.certnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.characteristicdescription ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.collectedinstrument ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.company ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.companyaccount ALTER COLUMN lastModified datetime2;

print('companyaccreditation')

ALTER TABLE dbo.companyaccreditation ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.companyinstructionlink ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.companysettingsforallocatedcompany ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.component ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.contact ALTER COLUMN lastModified datetime2 not null;

print('contactinstructionlink')

ALTER TABLE dbo.contactinstructionlink ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.contractreview ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.contractreviewitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.costs_adjustment ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.costs_calibration ALTER COLUMN lastModified datetime2;

print('costs_inspection')

ALTER TABLE dbo.costs_inspection ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.costs_purchase ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.costs_repair ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.courier ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.courierdespatch ALTER COLUMN lastModified datetime2;

print('courierdespatchtype')

ALTER TABLE dbo.courierdespatchtype ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.creditcard ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.creditnote ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.creditnoteitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.creditnotenote ALTER COLUMN lastModified datetime2;

print('customquotationcalibrationcondition')

ALTER TABLE dbo.customquotationcalibrationcondition ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.customquotationgeneralcondition ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.defaultnote ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.defaultquotationcalibrationcondition ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.defaultquotationgeneralcondition ALTER COLUMN lastModified datetime2 not null;

print('defaultstandard')

ALTER TABLE dbo.defaultstandard ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.defect ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.delivery ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.deliveryitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.deliveryitemnote ALTER COLUMN lastModified datetime2;

print('deliverynote')

ALTER TABLE dbo.deliverynote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.departmentmember ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.description ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.dimensional_thread ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.email ALTER COLUMN lastModified datetime2;

print('emailrecipient')

ALTER TABLE dbo.emailrecipient ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.emailtemplate ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.faultreport ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.faultreportaction ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.faultreportdescription ALTER COLUMN lastModified datetime2;

print('faultreportinstruction')

ALTER TABLE dbo.faultreportinstruction ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.freehandcontact ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.hire ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.hireaccessory ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.hirecrossitem ALTER COLUMN lastModified datetime2;

print('hireinstrument')

ALTER TABLE dbo.hireinstrument ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.hireitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.hireitemaccessory ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.hiremodel ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.hirenote ALTER COLUMN lastModified datetime2;

print('hirereplacedinstrument')

ALTER TABLE dbo.hirereplacedinstrument ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.hiresuspenditem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.images ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.imagetag ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.instcertlink ALTER COLUMN lastModified datetime2;

print('instmodel')

DROP INDEX [IDX_instrumentmodel_lastModified] ON [dbo].[instmodel]
GO

ALTER TABLE dbo.instmodel ALTER COLUMN lastModified datetime2 not null;

CREATE INDEX IDX_instrumentmodel_lastModified ON instmodel (lastModified);
GO

ALTER TABLE dbo.instmodeldomain ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.instmodelfamily ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.instmodelsubfamily ALTER COLUMN lastModified datetime2;

DROP INDEX [IDX_instrument_lastModified] ON [dbo].[instrument]
GO

ALTER TABLE dbo.instrument ALTER COLUMN lastModified datetime2 not null;

CREATE INDEX IDX_instrument_lastModified ON instrument (lastModified);
GO

print('instrumentnote')

ALTER TABLE dbo.instrumentnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.instrumentvalidationaction ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.instrumentvalidationissue ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.invoice ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.invoiceitem ALTER COLUMN lastModified datetime2;

print('invoiceitempo')

ALTER TABLE dbo.invoiceitempo ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.invoicejoblink ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.invoicenote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.invoicepo ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.job ALTER COLUMN lastModified datetime2 not null;

print('jobcosting')

ALTER TABLE dbo.jobcosting ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.jobcostingitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.jobcostingitemnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.jobcostingnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.jobinstructionlink ALTER COLUMN lastModified datetime2 not null;

print('jobitem')

ALTER TABLE dbo.jobitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.jobitemaction ALTER COLUMN lastModified datetime2;

 ALTER TABLE dbo.jobitemgroup ALTER COLUMN lastModified datetime2;

 ALTER TABLE dbo.jobitemnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.jobitempo ALTER COLUMN lastModified datetime2;

print('jobitemworkrequirement')

ALTER TABLE dbo.jobitemworkrequirement ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.jobnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.jobquotelink ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.location ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.mailgroupmember ALTER COLUMN lastModified datetime2;

print('mfr')

ALTER TABLE dbo.mfr ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.modelnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.modeloption ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.modelpartof ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.modelrange ALTER COLUMN lastModified datetime2;

print('nominalcode')

ALTER TABLE dbo.nominalcode ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.numberformat ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.numeration ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.onbehalfitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.pat ALTER COLUMN lastModified datetime2;

print('po')

ALTER TABLE dbo.po ALTER COLUMN lastModified datetime2 not null;

 ALTER TABLE dbo.pointsettemplate ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.presetcomment ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.procedureaccreditation ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.procedureheading ALTER COLUMN lastModified datetime2;

print('procs')

ALTER TABLE dbo.procs ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.purchaseorder ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.purchaseorderitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.purchaseorderitemnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.purchaseorderjobitem ALTER COLUMN lastModified datetime2;

print('purchaseordernote')

ALTER TABLE dbo.purchaseordernote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.quickpurchaseorder ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.quotation ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.quotationitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.quotationrequest ALTER COLUMN lastModified datetime2 not null;

print('quoteheading')

ALTER TABLE dbo.quoteheading ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.quoteitemnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.quotenote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.recall ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.recalldetail ALTER COLUMN lastModified datetime2;

print('recallitem')

ALTER TABLE dbo.recallitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.recallnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.recallresponsenote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.recallrule ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.repeatschedule ALTER COLUMN lastModified datetime2;

print('requirement')

ALTER TABLE dbo.requirement ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.role ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.salescategory ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.schedule ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.scheduledquotationrequest ALTER COLUMN lastModified datetime2 not null;

print('scheduleequipment')

ALTER TABLE dbo.scheduleequipment ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.schedulenote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.servicecapability ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.standardused ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.subdiv ALTER COLUMN lastModified datetime2 not null;

print('subdivinstructionlink')

ALTER TABLE dbo.subdivinstructionlink ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.supplierinvoice ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.systemdefaultapplication ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.taggedimage ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.templatepoint ALTER COLUMN lastModified datetime2;

print('tpinstruction')

ALTER TABLE dbo.tpinstruction ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.tpquotation ALTER COLUMN lastModified datetime2 not null;

 ALTER TABLE dbo.tpquotationitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.tpquoteitemnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.tpquotelink ALTER COLUMN lastModified datetime2;

print('tpquotenote')

ALTER TABLE dbo.tpquotenote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.tpquoterequest ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.tpquoterequestitem ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.tpquoterequestitemnote ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.tpquoterequestnote ALTER COLUMN lastModified datetime2;

print('tprequirement')

ALTER TABLE dbo.tprequirement ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.transportmethod ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.transportoption ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.uom ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.upcomingwork ALTER COLUMN lastModified datetime2 not null;

print('user_role')

ALTER TABLE dbo.user_role ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.userpreferences ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.users ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.vatrate ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.webresource ALTER COLUMN lastModified datetime2;

print('workinstruction')

ALTER TABLE dbo.workinstruction ALTER COLUMN lastModified datetime2 not null;

ALTER TABLE dbo.workinstructionversion ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.workrequirement ALTER COLUMN lastModified datetime2;

ALTER TABLE dbo.workrequirementprocedure ALTER COLUMN lastModified datetime2;

print('... DONE ...')

COMMIT TRAN