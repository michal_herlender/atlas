USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[jobtypeservicetype]
           ([defaultvalue]
           ,[jobtypeid]
           ,[servicetypeid])
     VALUES
           (0, 1, 6),
           (0, 2, 6)
GO

ALTER TABLE servicetype ALTER COLUMN displaycolour varchar(10) NOT NULL
ALTER TABLE servicetype ALTER COLUMN displaycolourfasttrack varchar(10) NOT NULL
ALTER TABLE servicetype ALTER COLUMN canbeperformedbyclient bit NOT NULL
ALTER TABLE servicetype ALTER COLUMN domaintype int NOT NULL
ALTER TABLE servicetype ADD repair bit 
GO

UPDATE servicetype SET repair = 0 WHERE servicetype.shortname <> 'REP';
UPDATE servicetype SET repair = 1 WHERE servicetype.shortname = 'REP';

ALTER TABLE servicetype ALTER COLUMN repair bit NOT NULL
GO

INSERT INTO dbversion (version) values (334);

GO

COMMIT TRAN