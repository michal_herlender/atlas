/*
	Initialization of the table dbo.presetcommentcategory
	1°) Define the list of preset comment category (cf.point 124 of file Spain Localization Project) (list defined by AS)
	2°) Initialize only spanish data (cf.point 98 of file Spain Localization Project)
	
	Modification date : 2016-05-10 : Loeung Sothy
*/
BEGIN TRAN

USE [spain];
GO

-- Initialize parameters
DECLARE @Date DATETIME = GETDATE()
DECLARE @UserID INT 

-- Gets the CWMS Auto User
SELECT @UserID = personid FROM dbo.contact WITH (NOLOCK) WHERE firstname = 'CWMS Auto User'

-- Empty the table dbo.presetcomment
DELETE FROM dbo.presetcommentcategory

-- Insert spanish preset
SET IDENTITY_INSERT dbo.presetcommentcategory ON

IF EXISTS (SELECT 1 FROM dbo.presetcommentcategory)
BEGIN
	INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
	VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Calibración',@Date,@UserID)
END
ELSE
BEGIN
	INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
	VALUES (1,'Calibración',@Date,@UserID)
END

INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'General',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Reparación',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Entrega',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Portes',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'S Urgente',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Cambio',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Solo Cal Elec',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Aviso',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Presupuestos',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Órdenes de compra',@Date,@UserID)
INSERT INTO dbo.presetcommentcategory (id,category,seton,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcommentcategory WITH (NOLOCK)),'Facturación',@Date,@UserID)

SET IDENTITY_INSERT dbo.presetcommentcategory OFF

ROLLBACK TRAN