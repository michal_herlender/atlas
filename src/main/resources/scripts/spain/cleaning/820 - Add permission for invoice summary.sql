USE [atlas]

BEGIN TRAN


	DECLARE @PermissionGroupId INT;
	SELECT @PermissionGroupId= id FROM dbo.permissiongroup WHERE name='GROUP_NEW_USER_RIGHTS';

	INSERT INTO permission (groupId, permission) VALUES(@PermissionGroupId, 'INVOICE_AWAITING_INVOICE_SUMMARY');

INSERT INTO dbversion(version) VALUES(820);

COMMIT TRAN