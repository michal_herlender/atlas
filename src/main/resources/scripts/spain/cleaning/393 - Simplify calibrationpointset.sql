-- Removing unused fields from calibrationpointset

USE [spain]

BEGIN TRAN

/****** Object:  Index [IDX_calibrationpointset_plantid]    Script Date: 2/4/2019 3:36:05 PM ******/
DROP INDEX [IDX_calibrationpointset_plantid] ON [dbo].[calibrationpointset]
GO

/****** Object:  Index [IDX_calibrationpointset_jobitemid]    Script Date: 2/4/2019 3:36:43 PM ******/
DROP INDEX [IDX_calibrationpointset_jobitemid] ON [dbo].[calibrationpointset]
GO

/****** Object:  Index [IDX_calibrationpointset_reqid]    Script Date: 2/4/2019 3:36:57 PM ******/
DROP INDEX [IDX_calibrationpointset_reqid] ON [dbo].[calibrationpointset]
GO

ALTER TABLE [dbo].[calibrationpointset] DROP CONSTRAINT [FKcqnymood3uypfxjoxlaepcsaj]
GO

ALTER TABLE [dbo].[calibrationpointset] DROP CONSTRAINT [FKm4p7lbnqvsisaqboalc5k092q]
GO

ALTER TABLE [dbo].[calibrationpointset] DROP CONSTRAINT [FK3664CCBC202933D5]
GO

ALTER TABLE [dbo].[calibrationpointset] DROP CONSTRAINT [FK3664CCBCFBBE68AC]
GO

/****** Object:  Statistic [_dta_stat_2037582297_5_4_3]    Script Date: 2/4/2019 3:35:07 PM ******/
DROP STATISTICS [dbo].[calibrationpointset].[_dta_stat_2037582297_5_4_3]
GO

/****** Object:  Index [_dta_index_calibrationpointset_7_2037582297__K1_2_3_4_5]    Script Date: 2/4/2019 3:34:30 PM ******/
DROP INDEX [_dta_index_calibrationpointset_7_2037582297__K1_2_3_4_5] ON [dbo].[calibrationpointset]
GO

ALTER TABLE calibrationpointset DROP COLUMN log_lastmodified
ALTER TABLE calibrationpointset DROP COLUMN log_userid
ALTER TABLE calibrationpointset DROP COLUMN plantid
ALTER TABLE calibrationpointset DROP COLUMN jobitemid
ALTER TABLE calibrationpointset DROP COLUMN reqid

GO

INSERT INTO dbversion(version) VALUES(393);

COMMIT TRAN