USE [spain]

Begin Tran

--ALTER 'courier' TABLE TO ADD NEW 'orgid' FIELD AND THE NEW FK CONSTRAINT ON THAT FIELD
ALTER TABLE dbo.courier ADD orgid int;
ALTER TABLE dbo.courier ADD CONSTRAINT courier_company_FK FOREIGN KEY (orgid) REFERENCES dbo.company(coid);
GO

--DUPLICAT QUERY FOR COURIER: this script allow to duplicat courier data for all business company 
--                            1) we extract all business company ID from Delivery, Hire and JobCourierLink
--							  2) we try to do scalar product of UNION Result and all courier items
INSERT INTO courier(defaultcourier,lastModified,lastModifiedBy,log_lastmodified,log_userid,[name],webtrackurl,orgid)
SELECT c.defaultcourier,c.lastModified,c.lastModifiedBy,c.log_lastmodified,c.log_userid,c.[name],c.webtrackurl,our.coid
FROM courier c
join
(SELECT distinct co.coid
FROM dbo.hire h 
join dbo.subdiv s on h.orgid = s.subdivid
join dbo.company co on co.coid = s.coid
union
SELECT distinct co.coid
FROM dbo.delivery d 
join dbo.subdiv s on d.orgid = s.subdivid
join dbo.company co on co.coid = s.coid
UNION
SELECT distinct co.coid
FROM dbo.jobcourierlink j 
join dbo.job jb on j.jobid = jb.jobid
join dbo.contact con on jb.personid = con.personid
join dbo.subdiv s on con.subdivid = s.subdivid 
join dbo.company co on co.coid = s.coid
) our on c.orgid IS NULL
ORDER BY c.courierid;

--DUPLICAT QUERY FOR COURIERDESPATCHTYPE
INSERT INTO courierdespatchtype([description],courierid,defaultforcourier)
SELECT cdt.[description],c2.courierid,cdt.defaultforcourier
FROM dbo.courierdespatchtype cdt 
join dbo.courier c1 on c1.courierid = cdt.courierid
join dbo.courier c2 on c1.[name] = c2.[name] and c2.orgid is not null;

--CURSOR UPDATE FOR COURIERDESPATCH using DELIVERY association
DECLARE @cdid_d INT
DECLARE @new_value_d INT
DECLARE @getid_d CURSOR

SET @getid_d = CURSOR FOR
SELECT distinct cd.courierdespatchid,cdt.cdtid
FROM dbo.courierdespatch cd
join dbo.delivery d on d.courierdespatchid = cd.courierdespatchid
join dbo.subdiv s on s.subdivid = d.orgid
join dbo.company c on c.coid = s.coid
join dbo.courierdespatchtype cdt2 on cdt2.cdtid = cd.cdtypeid
join dbo.courier cc2 on cc2.courierid = cdt2.courierid
join dbo.courier cc on  cc.name = cc2.name and cc.orgid = c.coid
join dbo.courierdespatchtype cdt on cc.courierid = cdt.courierid
OPEN @getid_d
FETCH NEXT
FROM @getid_d INTO @cdid_d, @new_value_d
WHILE @@FETCH_STATUS = 0
BEGIN
    update dbo.courierdespatch set cdtypeid=@new_value_d where courierdespatchid=@cdid_d;
	FETCH NEXT
    FROM @getid_d INTO @cdid_d, @new_value_d
END

CLOSE @getid_d
DEALLOCATE @getid_d;

--CURSOR UPDATE FOR COURIERDESPATCH using HIRE association
DECLARE @cdid_h INT
DECLARE @new_value_h INT
DECLARE @getid_h CURSOR

SET @getid_h = CURSOR FOR
SELECT distinct cd.courierdespatchid,cdt.cdtid
FROM dbo.courierdespatch cd
join dbo.hire h on h.courierdespatchid = cd.courierdespatchid
join dbo.subdiv s on s.subdivid = h.orgid
join dbo.company c on c.coid = s.coid
join dbo.courierdespatchtype cdt2 on cdt2.cdtid = cd.cdtypeid
join dbo.courier cc2 on cc2.courierid = cdt2.courierid
join dbo.courier cc on  cc.name = cc2.name and cc.orgid = c.coid
join dbo.courierdespatchtype cdt on cc.courierid = cdt.courierid
OPEN @getid_h
FETCH NEXT
FROM @getid_h INTO @cdid_h, @new_value_h
WHILE @@FETCH_STATUS = 0
BEGIN
    update dbo.courierdespatch set cdtypeid=@new_value_h where courierdespatchid=@cdid_h;
	FETCH NEXT
    FROM @getid_h INTO @cdid_h, @new_value_h
END

CLOSE @getid_h
DEALLOCATE @getid_h;

INSERT INTO dbversion(version) VALUES(348);

COMMIT TRAN