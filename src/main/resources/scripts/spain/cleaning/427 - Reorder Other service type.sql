USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[calibrationtype]
   SET [orderby] = 30
 WHERE servicetypeid = 6;
GO

UPDATE [dbo].[calibrationtype]
   SET [orderby] = 31
 WHERE servicetypeid = 3;
GO

UPDATE [dbo].[servicetype]
   SET [orderby] = 31
 WHERE servicetypeid = 3;
GO

INSERT INTO dbversion (version) VALUES (427);
GO

COMMIT TRAN
