USE [spain]

BEGIN TRAN

ALTER TABLE dbo.instrument
 ALTER COLUMN customerdescription nvarchar(max) COLLATE Latin1_General_CI_AI NULL;

GO

INSERT INTO dbversion(version) VALUES(307);

COMMIT TRAN
