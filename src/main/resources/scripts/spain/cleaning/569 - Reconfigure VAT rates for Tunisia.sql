-- Adds a "Special Zone" VAT rate for use in Tunisia by local business company
-- Also updates settings for any existing company relationship with local business company, to use this rate
-- According to Lucie / local country manager, we are exempt from charging VAT in Tunisia
-- This also sets the existing country default VAT rate to 19.00 %
-- (which seems to have been done on production without any documentation or ticket, causing some urgency)
-- I'm running this script on production today once tested on test, as they need it for Tunisia soon.
-- Galen Beck - 2020-01-31

USE [atlas]
GO

BEGIN TRAN

DECLARE @countryIdTN int;

select @countryIdTN = country.countryid
from country where countrycode = 'TN'

UPDATE [dbo].[vatrate]
	SET [rate] = 19.00,
	[lastModified] = '2019-01-31'
	WHERE vatcode = 'G';

INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid],[lastModified])
     VALUES
           ('U','Tunisia Free Zone',0.00,1,@countryIdTN,'2019-01-31');

DECLARE @companyIdBusinessTunisia int;

select @companyIdBusinessTunisia = company.coid
from company where countryid = @countryIdTN and corole = 5;

UPDATE [dbo].[companysettingsforallocatedcompany]
   SET [vatrate] = 'U'
 WHERE orgid = @companyIdBusinessTunisia

INSERT INTO dbversion(version) VALUES(569);

GO

COMMIT TRAN