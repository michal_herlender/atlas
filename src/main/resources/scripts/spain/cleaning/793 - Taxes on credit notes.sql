use atlas

begin tran

create table dbo.creditnotetax (
	id int identity not null,
	amount numeric(10,2) not null,
	description varchar(30),
	rate numeric(10,2),
	tax numeric(10,2) not null,
	creditnoteid int not null,
	vatrateid varchar(1),
	primary key (id)
);

go

alter table dbo.creditnotetax add constraint FK_creditnotetax_invoice 
foreign key (creditnoteid) references dbo.creditnote;

alter table dbo.creditnotetax add constraint FK_creditnotetax_vatrate 
foreign key (vatrateid) references dbo.vatrate;

alter table dbo.creditnoteitem 
add taxable tinyint not null default 1, taxamount numeric(10,2);

insert into dbversion(version) values(793);

commit tran