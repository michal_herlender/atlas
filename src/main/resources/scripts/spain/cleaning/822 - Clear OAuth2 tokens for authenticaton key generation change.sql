-- Script 822 : The authentication key generator has changed (see DEV-2665) so existing authentication ids can't be reused
-- Delete existing tokens and start fresh

USE [atlas]

BEGIN TRAN

DELETE FROM [oauth2refreshtoken]

DELETE FROM [oauth2accesstoken]

INSERT INTO dbversion(version) VALUES(822);

COMMIT TRAN