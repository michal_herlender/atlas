-- Adds and configures two new service types (DEV-2748) for US platform only
-- These are used for calibrations "without data".

USE [atlas]
GO

BEGIN TRAN

DECLARE @region varchar(10);
SELECT @region = region FROM dbconfig;
PRINT @region;

-- Renumber PM / IC-IL / IC-OS on all platforms to make additional space
UPDATE servicetype SET orderby = 15 WHERE shortname = 'PM';
UPDATE servicetype SET orderby = 16 WHERE shortname = 'IC-IL';
UPDATE servicetype SET orderby = 17 WHERE shortname = 'IC-OS';

-- Remove unused / removed fields from calibrationtype
ALTER TABLE [dbo].[calibrationtype] DROP CONSTRAINT [FK_nwgp45rpxr8b64fycdtgwmxf2];

ALTER TABLE calibrationtype DROP COLUMN certnoresetyearly;
ALTER TABLE calibrationtype DROP COLUMN certprefix;
ALTER TABLE calibrationtype DROP COLUMN nextcertno;
ALTER TABLE calibrationtype DROP COLUMN log_lastmodified;
ALTER TABLE calibrationtype DROP COLUMN log_userid;
ALTER TABLE calibrationtype DROP COLUMN firstpagepapertype;
ALTER TABLE calibrationtype DROP COLUMN labeltemplatepath;
ALTER TABLE calibrationtype DROP COLUMN pdfcontinuationwatermark;
ALTER TABLE calibrationtype DROP COLUMN pdfheaderwatermark;
ALTER TABLE calibrationtype DROP COLUMN restpagepapertype;
ALTER TABLE calibrationtype DROP COLUMN recalldateonlabel;

if (@region = 'US')
BEGIN

	DECLARE @sn_TC_C_IL varchar(10) = 'TC-C-IL';
	DECLARE @sn_TC_C_OS varchar(10) = 'TC-C-OS'; 

	DECLARE @ln_TC_C_IL varchar(100) = 'Traceable calibration with statement of conformity in laboratory';
	DECLARE @ln_TC_C_OS varchar(100) = 'Traceable calibration with statement of conformity on site'; 

	-- Changed to have defined service type ID to match TST2 (easier between test/production systems for data migration)

	SET IDENTITY_INSERT [dbo].[servicetype] ON;

	PRINT 'Inserting 2 new service types:';
	INSERT INTO [dbo].[servicetype]
           ([servicetypeid]
		   ,[displaycolour]
           ,[displaycolourfasttrack]
           ,[longname]
           ,[shortname]
           ,[canbeperformedbyclient]
           ,[domaintype]
           ,[orderby]
           ,[repair])
     VALUES
           (42
		   ,'#FFFFFF'
           ,'#FEE0B4'
           ,@ln_TC_C_IL
           ,@sn_TC_C_IL
           ,0
           ,0
           ,9
           ,0),
           (43
		   ,'#FFFFCC'
           ,'#FFB9B9'
           ,@ln_TC_C_OS
           ,@sn_TC_C_OS
           ,0
           ,0
           ,10
           ,0);

	SET IDENTITY_INSERT [dbo].[servicetype] OFF;
	
	DECLARE @st_TC_C_IL int;
	DECLARE @st_TC_C_OS int; 

	SELECT @st_TC_C_IL = servicetypeid FROM servicetype WHERE shortname = @sn_TC_C_IL;
	SELECT @st_TC_C_OS = servicetypeid FROM servicetype WHERE shortname = @sn_TC_C_OS;

	PRINT 'New service type IDs: (2 expected)';
	PRINT @st_TC_C_IL;
	PRINT @st_TC_C_OS;

	-- Changed to have defined service type ID to match TST2 (easier between test/production systems for data migration)

	SET IDENTITY_INSERT [dbo].[calibrationtype] ON;

	PRINT 'Inserting 2 new cal types:';
	INSERT INTO [dbo].[calibrationtype]
			   ([caltypeid]
			   ,[accreditationspecific]
			   ,[active]
			   ,[orderby]
			   ,[lastModified]
			   ,[servicetypeid]
			   ,[recallRequirementType]
			   ,[calibrationWithJudgement])
     VALUES
           (1076,0,1,9,SYSDATETIME(),@st_TC_C_IL,'FULL_CALIBRATION',1),
		   (1077,0,1,10,SYSDATETIME(),@st_TC_C_OS,'FULL_CALIBRATION',1);

	SET IDENTITY_INSERT [dbo].[calibrationtype] OFF;

	DECLARE @ct_TC_C_IL int;
	DECLARE @ct_TC_C_OS int; 

	SELECT @ct_TC_C_IL = caltypeid FROM calibrationtype WHERE servicetypeid = @st_TC_C_IL;
	SELECT @ct_TC_C_OS = caltypeid FROM calibrationtype WHERE servicetypeid = @st_TC_C_OS;

	PRINT 'New cal type IDs: (2 expected)';
	PRINT @ct_TC_C_IL;
	PRINT @ct_TC_C_OS;

	PRINT 'Inserting calibration accreditation levels';
	INSERT INTO [dbo].[calibrationaccreditationlevel]
			   ([accredlevel],[caltypeid])
		 VALUES
			   ('SUPERVISED',@ct_TC_C_IL),
			   ('PERFORM',@ct_TC_C_IL),
			   ('SIGN',@ct_TC_C_IL),
			   ('APPROVE',@ct_TC_C_IL),
			   ('REMOVED',@ct_TC_C_IL),
			   ('SUPERVISED',@ct_TC_C_OS),
			   ('PERFORM',@ct_TC_C_OS),
			   ('SIGN',@ct_TC_C_OS),
			   ('APPROVE',@ct_TC_C_OS),
			   ('REMOVED',@ct_TC_C_OS);

DECLARE @st_AC_EX int;
DECLARE @st_STD_EX int; 

SELECT @st_AC_EX = servicetypeid FROM servicetype WHERE shortname = 'AC-EX';
SELECT @st_STD_EX = servicetypeid FROM servicetype WHERE shortname = 'STD-EX';

PRINT 'AC-EX / STD-EX service type IDs: (2 expected)';
PRINT @st_AC_EX;
PRINT @st_STD_EX;

DECLARE @ct_AC_EX int;
DECLARE @ct_STD_EX int; 

SELECT @ct_AC_EX = caltypeid FROM calibrationtype WHERE servicetypeid = @st_AC_EX;
SELECT @ct_STD_EX = caltypeid FROM calibrationtype WHERE servicetypeid = @st_STD_EX;

PRINT 'AC-EX / STD-EX cal type IDs: (2 expected)';
PRINT @ct_AC_EX;
PRINT @ct_STD_EX;

PRINT 'Deleting AC-EX and STD-EX from job type cal type';
DELETE FROM [dbo].[jobtypecaltype] WHERE [caltypeid] in (@ct_AC_EX, @ct_STD_EX);

PRINT 'Deleting AC-EX and TC-EX from job type service type';
DELETE FROM [dbo].[jobtypeservicetype] WHERE [servicetypeid] in (@st_AC_EX, @st_STD_EX);

-- 1=Standard, 2=Single Price, 4=Site
PRINT 'Inserting job type cal type';
INSERT INTO [dbo].[jobtypecaltype]
           ([defaultvalue]
           ,[jobtypeid]
           ,[caltypeid])
     VALUES
           (0,1,@ct_TC_C_IL),
           (0,2,@ct_TC_C_IL),
           (0,4,@ct_TC_C_OS);

PRINT 'Inserting job type service type';
INSERT INTO [dbo].[jobtypeservicetype]
           ([defaultvalue]
           ,[jobtypeid]
           ,[servicetypeid])
     VALUES
           (0,1,@st_TC_C_IL),
           (0,2,@st_TC_C_IL),
           (0,4,@st_TC_C_OS);


PRINT 'Inserting short translations';

INSERT INTO [dbo].[servicetypeshortnametranslation]
           ([servicetypeid]
           ,[locale]
           ,[translation])
     VALUES
           (@st_TC_C_IL,'en_GB',@sn_TC_C_IL),
		   (@st_TC_C_IL,'en_US',@sn_TC_C_IL),
		   (@st_TC_C_IL,'es_ES','TC-SM-EL'),
		   (@st_TC_C_IL,'de_DE','WE-OM-LA'),
		   (@st_TC_C_IL,'fr_FR','VE-SM-LA'),
           (@st_TC_C_OS,'en_GB',@sn_TC_C_OS),
		   (@st_TC_C_OS,'en_US',@sn_TC_C_OS),
		   (@st_TC_C_OS,'es_ES','TC-SM-IS'),
		   (@st_TC_C_OS,'de_DE','WE-OM-VO'),
		   (@st_TC_C_OS,'fr_FR','VE-SM-SI');

PRINT 'Inserting long translations';

INSERT INTO [dbo].[servicetypelongnametranslation]
           ([servicetypeid]
           ,[locale]
           ,[translation])
     VALUES
           (@st_TC_C_IL,'en_GB',@ln_TC_C_IL),
		   (@st_TC_C_IL,'en_US',@ln_TC_C_IL),
		   (@st_TC_C_IL,'es_ES','Calibración no acreditada sin resultados de medición en laboratorio'),
		   (@st_TC_C_IL,'de_DE','Werkskalibrierung ohne Messergebnisse im Labor'),
		   (@st_TC_C_IL,'fr_FR','Vérification traçable sans résultats de mesure en laboratoire'),
           (@st_TC_C_OS,'en_GB',@ln_TC_C_OS),
		   (@st_TC_C_OS,'en_US',@ln_TC_C_OS),
		   (@st_TC_C_OS,'es_ES','Calibración no acreditada sin resultados de medición in situ'),
		   (@st_TC_C_OS,'de_DE','Werkskalibrierung ohne Messergebnisse Vor-Ort'),
		   (@st_TC_C_OS,'fr_FR','Vérification traçable sans résultats de mesure sur site');

END

insert into dbversion values (833);

COMMIT TRAN