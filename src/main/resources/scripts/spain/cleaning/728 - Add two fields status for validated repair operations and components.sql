USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.validatedfreerepairoperation ADD validatedstatus VARCHAR(100) NULL;

ALTER TABLE dbo.validatedfreerepaircomponent ADD validatedstatus VARCHAR(100) NULL;

INSERT INTO dbversion VALUES (728)

COMMIT TRAN
