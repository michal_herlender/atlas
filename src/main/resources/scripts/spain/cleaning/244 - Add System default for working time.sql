BEGIN TRAN

USE [spain];
GO
	drop table dbo.timesheetentry;
GO
    create table dbo.timesheetentry (
        id int identity not null,
        comment varchar(255),
        duration bigint not null,
        start datetime2 not null,
        timeactivity varchar(255) not null,
        employeeid int not null,
        jobid int,
        subdivid int,
        primary key (id)
    );

	create index IDX_timesheetentry_timeactivity on dbo.timesheetentry (timeactivity);

    alter table dbo.timesheetentry 
        add constraint FK_timesheetentry_employee 
        foreign key (employeeid) 
        references dbo.contact;

    alter table dbo.timesheetentry 
        add constraint FK_timesheetentry_job 
        foreign key (jobid) 
        references dbo.job;

    alter table dbo.timesheetentry 
        add constraint FK_timesheetentry_subdiv 
        foreign key (subdivid) 
        references dbo.subdiv;
GO
	insert into dbversion(version) values(244);

COMMIT TRAN