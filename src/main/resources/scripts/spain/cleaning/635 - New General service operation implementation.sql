USE [atlas]
GO

BEGIN TRAN

--tables creation for General service operation
	CREATE TABLE dbo.generalserviceoperation (
    id INT NOT NULL IDENTITY(1,1),
    startedby INT,
    startedon DATETIME,
	completedby INT,
    completedon DATETIME,
    [status] INT,
	servicetypeid INT,
	addressid INT,
	gsonumber VARCHAR(30),
    orgid INT,
	lastModifiedBy INT,
    lastModified DATETIME2,
    PRIMARY KEY (id),
	CONSTRAINT gso_startedby_contact_FK FOREIGN KEY (startedby) REFERENCES dbo.contact(personid),
	CONSTRAINT gso_completedby_contact_FK FOREIGN KEY (completedby) REFERENCES dbo.contact(personid),
	CONSTRAINT gso_status_basestatus_FK FOREIGN KEY ([status]) REFERENCES dbo.basestatus(statusid),
	CONSTRAINT gso_servicetypeid_servicetype_FK FOREIGN KEY (servicetypeid) REFERENCES dbo.servicetype(servicetypeid),
	CONSTRAINT gso_addressid_address_FK FOREIGN KEY (addressid) REFERENCES dbo.[address](addrid),
	CONSTRAINT gso_orgid_subdiv_FK FOREIGN KEY (orgid) REFERENCES dbo.subdiv(subdivid),
	CONSTRAINT gso_lastModifiedBy_contact_FK FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid),
	);

	CREATE TABLE dbo.gsodocument (
    id INT NOT NULL IDENTITY(1,1),
    documentnumber VARCHAR(30) NOT NULL,
	documentdate DATETIME NOT NULL,
	gsoid INT,
    lastModifiedBy INT,
    lastModified DATETIME2,
    PRIMARY KEY (id),
	CONSTRAINT gsodocument_gsoid_gso_FK FOREIGN KEY (gsoid) REFERENCES dbo.generalserviceoperation(id),
	CONSTRAINT gsodocument_lastModifiedBy_contact_FK FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid),
	);

	CREATE TABLE dbo.gsolink (
    id INT NOT NULL IDENTITY(1,1),
    timespent INT,
	gsoid INT,
    jobitemid INT,
	actionoutcomeid INT,
    lastModifiedBy INT,
    lastModified DATETIME2,
    PRIMARY KEY (id),
	CONSTRAINT gsolink_gsoid_gso_FK FOREIGN KEY (gsoid) REFERENCES dbo.generalserviceoperation(id),
	CONSTRAINT gsolink_jobitemid_jobitem_FK FOREIGN KEY (jobitemid) REFERENCES dbo.jobitem(jobitemid),
	CONSTRAINT gsolink_actionoutcomeid_actionoutcome_FK FOREIGN KEY (actionoutcomeid) REFERENCES dbo.actionoutcome(id),
	CONSTRAINT gsolink_lastModifiedBy_contact_FK FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid),
	);

	-- add general service operation status with translations
	INSERT INTO dbo.basestatus([type], defaultstatus, description, name, accepted, followingissue, followingreceipt, issued, requiringattention, rejected)
	VALUES('generalserviceoperation', 1, '', 'On-going', 0, 0, 0, 0, 0, 0);
 	DECLARE @gso_ongoing_status INT = IDENT_CURRENT('basestatus');
	INSERT INTO dbo.basestatusnametranslation(statusid, locale, [translation])
	VALUES(@gso_ongoing_status, 'de_DE', 'L?uft'),
	  (@gso_ongoing_status, 'en_GB', 'On-going'),
	  (@gso_ongoing_status, 'es_ES', 'En curso'),
	  (@gso_ongoing_status, 'fr_FR', 'En cours');

	INSERT INTO dbo.basestatus([type], defaultstatus, description, name, accepted, followingissue, followingreceipt, issued, requiringattention, rejected)
	VALUES('generalserviceoperation', 0, '', 'Cancelled', 0, 0, 0, 0, 0, 0);
	DECLARE @gso_cancelled_status INT = IDENT_CURRENT('basestatus');
	INSERT INTO dbo.basestatusnametranslation(statusid, locale, [translation])
	VALUES(@gso_cancelled_status, 'de_DE', 'Abgebrochen'),
	  (@gso_cancelled_status, 'en_GB', 'Cancelled'),
	  (@gso_cancelled_status, 'es_ES', 'Cancelado'),
	  (@gso_cancelled_status, 'fr_FR', 'Annulé');

	INSERT INTO dbo.basestatus([type], defaultstatus, description, name, accepted, followingissue, followingreceipt, issued, requiringattention, rejected)
	VALUES('generalserviceoperation', 0, '', 'Complete', 0, 0, 0, 0, 0, 0);
	DECLARE @gso_complete_status INT = IDENT_CURRENT('basestatus');
	INSERT INTO dbo.basestatusnametranslation(statusid, locale, [translation])
	VALUES(@gso_complete_status, 'de_DE', 'Fertiggestellt'),
	  (@gso_complete_status, 'en_GB', 'Complete'),
	  (@gso_complete_status, 'es_ES', 'Completo'),
	  (@gso_complete_status, 'fr_FR', 'Terminé');

	INSERT INTO dbo.basestatus([type], defaultstatus, description, name, accepted, followingissue, followingreceipt, issued, requiringattention, rejected)
	VALUES('generalserviceoperation', 0, '', 'On hold', 0, 0, 0, 0, 0, 0);
	DECLARE @gso_onhold_status INT = IDENT_CURRENT('basestatus');
	INSERT INTO dbo.basestatusnametranslation(statusid, locale, [translation])
	VALUES(@gso_onhold_status, 'de_DE', '"auf Halt"'),
	  (@gso_onhold_status, 'en_GB', 'On hold'),
	  (@gso_onhold_status, 'es_ES', 'En espera'),
	  (@gso_onhold_status, 'fr_FR', 'En attente');

	-- Add permission for new general service operation
	INSERT INTO dbo.permission (groupId,permission)
	VALUES (1,'GENERALSERVICEOPERATION_NEW');

	-- Add new State Group Link for 'Awaiting general service operation' status
	DECLARE @awaiting_gso_status INT;
	SELECT @awaiting_gso_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting general service operation';
	INSERT INTO dbo.stategrouplink(groupid,stateid,type)
	VALUES (64, @awaiting_gso_status, 'ALLOCATED_SUBDIV');

	-- Add new State Group Link for 'General service operation' activity
	DECLARE @performedactivity int
	SELECT @performedactivity = stateid FROM dbo.itemstate WHERE description = 'General service operation';
	 INSERT INTO dbo.stategrouplink(groupid,stateid,type)
	VALUES (65, @performedactivity, 'ALLOCATED_SUBDIV');

	-- Update the action outcome type for the activity 'General service operation'
	UPDATE dbo.itemstate SET actionoutcometype = 'GENERAL_SERVICE_OPERATION' WHERE stateid = @performedactivity;

	-- Add new hook 'start-general-service-operation' with hook activity
	INSERT INTO dbo.hook(active, alwayspossible, beginactivity, completeactivity, name, activityid)
	VALUES(1, 0, 1, 0, 'start-general-service-operation', NULL);
	DECLARE @hook_start_gso int = IDENT_CURRENT('hook');	 
	INSERT INTO dbo.hookactivity
	(beginactivity, completeactivity, activityid, hookid, stateid)
	VALUES(1, 0, @performedactivity, @hook_start_gso, @awaiting_gso_status);

	-- Add new hook 'complete-general-service-operation' with hook activity
	INSERT INTO dbo.hook(active, alwayspossible, beginactivity, completeactivity, name, activityid)
	VALUES(1, 0, 0, 1, 'complete-general-service-operation', @performedactivity);
	DECLARE @hook_complete_gso int = IDENT_CURRENT('hook');
	INSERT INTO dbo.hookactivity(beginactivity, completeactivity, activityid, hookid, stateid)
	VALUES(0, 1, @performedactivity, @hook_complete_gso, @performedactivity);

	-- Add new hook 'upload-general-service-operation-document' with hook activity
	DECLARE @upload_gso_document_activity INT;
	SELECT @upload_gso_document_activity = stateid FROM dbo.itemstate WHERE description = 'Document uploaded for general operation';
	INSERT INTO dbo.hook(active, alwayspossible, beginactivity, completeactivity, name, activityid)
	VALUES(1, 1, 1, 1, 'upload-general-service-operation-document', @upload_gso_document_activity);
	INSERT INTO dbo.hookactivity(beginactivity, completeactivity, activityid, hookid, stateid)
	VALUES(0, 1, @performedactivity, @hook_complete_gso, @performedactivity);

	-- Add new action outcomes for 'General service operation' activity
	DECLARE @lastInsertedAO_completed_succ INT,
			@lastInsertedAO_completed_unsu INT,
			@lastInsertedAO_failedredo INT,
			@lastInsertedAO_onhold INT;

	INSERT INTO dbo.actionoutcome(defaultoutcome, description, positiveoutcome, activityid, active, [type], value)
	VALUES(1, 'Completed - Successfully', 1, @performedactivity, 1, 'general_service_operation', 'COMPLETED_SUCCESSFULLY');
    SET @lastInsertedAO_completed_succ = IDENT_CURRENT('actionoutcome');
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) 
	VALUES (@lastInsertedAO_completed_succ, 'en_GB', 'Completed - Successfully'),
	   (@lastInsertedAO_completed_succ, 'fr_FR', 'Complété avec succès'),
	   (@lastInsertedAO_completed_succ, 'es_ES', 'Completado satisfactoriamente'),
	   (@lastInsertedAO_completed_succ, 'de_DE', 'Erfolgreich beendet');
      
	INSERT INTO dbo.actionoutcome(defaultoutcome, description, positiveoutcome, activityid, active, [type], value)
	VALUES (0, 'Completed - Unsuccessfully', 1, @performedactivity, 1, 'general_service_operation', 'COMPLETED_UNSUCCESSFULLY');
	SET @lastInsertedAO_completed_unsu = IDENT_CURRENT('actionoutcome');
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) 
	VALUES (@lastInsertedAO_completed_unsu, 'en_GB', 'Completed - Unsuccessfully'),
	   (@lastInsertedAO_completed_unsu, 'fr_FR', 'Terminé - sans succès'),
	   (@lastInsertedAO_completed_unsu, 'es_ES', 'Completado - Sin éxito'),
	   (@lastInsertedAO_completed_unsu, 'de_DE', 'Abgeschlossen - Erfolglos');

	INSERT INTO dbo.actionoutcome(defaultoutcome, description, positiveoutcome, activityid, active, [type], value)
	VALUES(0, 'Failed - Redo', 0, @performedactivity, 1, 'general_service_operation', 'FAILED_REDO');
   	SET @lastInsertedAO_failedredo= IDENT_CURRENT('actionoutcome');
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) 
	VALUES (@lastInsertedAO_failedredo, 'en_GB', 'Failed - Redo'),
	   (@lastInsertedAO_failedredo, 'fr_FR', 'Échec - Refaire'),
	   (@lastInsertedAO_failedredo, 'es_ES', 'Fallido - Rehacer'),
	   (@lastInsertedAO_failedredo, 'de_DE', 'Fehlgeschlagen - Wiederherstellen');
      
	INSERT INTO dbo.actionoutcome(defaultoutcome, description, positiveoutcome, activityid, active, [type], value)
	VALUES (0, 'On-Hold', 0, @performedactivity, 1, 'general_service_operation', 'ON_HOLD');
	SET @lastInsertedAO_onhold = IDENT_CURRENT('actionoutcome');
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) 
	VALUES (@lastInsertedAO_onhold, 'en_GB', 'On-Hold'),
	   (@lastInsertedAO_onhold, 'fr_FR', 'En attente'),
	   (@lastInsertedAO_onhold, 'es_ES', 'En espera'),
	   (@lastInsertedAO_onhold, 'de_DE', 'In Wartestellung');

	-- Add system component 
	INSERT INTO dbo.systemcomponent (appendtoparentpath,component,componentname,foldersdropboundaries,foldersperitem,folderssplit,folderssplitrange,foldersyear,numberedsubfolders,rootdir,usingparentsettings,parentid,emailtemplates)
	VALUES ('General Service Operations',28,'General Service Operation',0,0,0,0,0,0,'General Service Operation',1,1,0);
	
	-- Add new status on hold
	DECLARE @gso_onhold_workstatus INT,
	        @onhold_oc INT,
			@failed_oc INT,
			@lookup_gso_result INT,
			@old_outcomestatus_cancelled INT;

	INSERT INTO dbo.itemstate(active,description,lookupid,retired,type)
	VALUES (1,'General service operation On-Hold',NULL,0,'workstatus');
	SET @gso_onhold_workstatus = IDENT_CURRENT('itemstate');
	INSERT INTO atlas.dbo.itemstatetranslation(stateid, locale, [translation])
	VALUES(@gso_onhold_workstatus, 'de_DE', 'Allgemeiner Servicebetrieb In der Warteschleife'),
		(@gso_onhold_workstatus, 'en_GB', 'General service operation On-Hold'),
		(@gso_onhold_workstatus, 'es_ES', 'Operación de servicio general en espera'),
		(@gso_onhold_workstatus, 'fr_FR', 'Fonctionnement des services généraux En attente');
		
	-- Link new status to group resume general service operation
	INSERT INTO dbo.stategrouplink(groupid,stateid,type)
	VALUES (66, @gso_onhold_workstatus, 'ALLOCATED_SUBDIV');

	-- Add new outcome status	
	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
	VALUES (0,@performedactivity, NULL, @gso_onhold_workstatus);
	SET @onhold_oc = IDENT_CURRENT('outcomestatus');
	
	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
	VALUES (0,@performedactivity, NULL, @awaiting_gso_status);
	SET @failed_oc = IDENT_CURRENT('outcomestatus');
	
	SELECT @lookup_gso_result = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_GeneralServiceOperationResult';
	SELECT @old_outcomestatus_cancelled = outcomestatusid FROM dbo.lookupresult WHERE lookupid = @lookup_gso_result AND resultmessage = 1;
	UPDATE dbo.lookupresult SET outcomestatusid = @failed_oc WHERE outcomestatusid = @old_outcomestatus_cancelled;
	DELETE FROM dbo.outcomestatus WHERE id = @old_outcomestatus_cancelled;
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage)
	VALUES (@performedactivity, @lookup_gso_result, @onhold_oc, NULL, 2);

	INSERT INTO dbversion VALUES (635)

COMMIT TRAN