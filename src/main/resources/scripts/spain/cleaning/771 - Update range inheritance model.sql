USE [atlas]

BEGIN TRAN

    create table dbo.calibrationrange (
       id int identity not null,
        lastModified datetime2,
        endd double precision,
        maxIsInfinite bit default 0 not null,
        maxvalue nvarchar(2000),
        minvalue nvarchar(2000),
        start double precision,
        relatedpoint numeric(19,2),
        lastModifiedBy int,
        maxuomid int,
        uomid int not null,
        relateduomid int,
        primary key (id)
    );

    alter table dbo.calreq 
       add calibrationrangeid int;

    create table dbo.instrumentrange (
       id int identity not null,
        lastModified datetime2,
        endd double precision,
        maxIsInfinite bit default 0 not null,
        maxvalue nvarchar(2000),
        minvalue nvarchar(2000),
        start double precision,
        lastModifiedBy int,
        maxuomid int,
        uomid int not null,
        plantid int not null,
        primary key (id)
    );

    alter table dbo.calibrationrange 
       add constraint FKplndlqh7qom2mtoj7c85rvs83 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.calibrationrange 
       add constraint FKsd1rbdwepime3bdu30glvvmno 
       foreign key (maxuomid) 
       references dbo.uom;

    alter table dbo.calibrationrange 
       add constraint FKnhslwyrh0pw2dodaxkh111vle 
       foreign key (uomid) 
       references dbo.uom;

    alter table dbo.calibrationrange 
       add constraint FKdbh1tdnc88strd03lw0g0mfjv 
       foreign key (relateduomid) 
       references dbo.uom;

    alter table dbo.calreq 
       add constraint FK_calreq_calibrationrange 
       foreign key (calibrationrangeid) 
       references dbo.calibrationrange;

    alter table dbo.instrumentrange 
       add constraint FK5b85cdxbm18i3wtmpgk0fw24a 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.instrumentrange 
       add constraint FKi8ma8q37e9lcqvmhi5aqpw0ho 
       foreign key (maxuomid) 
       references dbo.uom;

    alter table dbo.instrumentrange 
       add constraint FKcjl3xr5ike4o9h5a1f2ud02qr 
       foreign key (uomid) 
       references dbo.uom;

    alter table dbo.instrumentrange 
       add constraint FK_instrumentrange_instrument 
       foreign key (plantid) 
       references dbo.instrument;

-- Add a temporary column (no foreign key) to store the original model range id

alter table dbo.calibrationrange 
 add old_modelrangeid int;

	INSERT INTO dbversion(version) VALUES(771);

COMMIT TRAN