-- Little change to the size of the capability_servicetype comments table, to 2000 characters.
-- Moved the insert / migration function from 700 to 701 after this column size change.
-- If 700 has already been run in its previous version (which could show errors), it's OK - 701 will delete the entries and re-insert.


USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.capability_servicetype ALTER COLUMN comments VARCHAR(2000) NOT NULL;

-- Delete any previously migrated data (if older version of 700 had been run)

DELETE FROM dbo.capability_servicetype;

GO

	DECLARE @TC_SOC_IL_Id INT;
	DECLARE @AC_SOC_IL_Id INT;
	DECLARE @TC_SOC_OS_Id INT;
	DECLARE @AC_SOC_OS_Id INT;
	DECLARE @TC_MVO_IL_Id INT;
	DECLARE @AC_MVO_IL_Id INT;
	DECLARE @TC_MVO_OS_Id INT;
	DECLARE @AC_MVO_OS_Id INT;
	DECLARE @REP_Id INT;

	SELECT @TC_SOC_IL_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'TC-SOC-IL';
	SELECT @AC_SOC_IL_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'AC-SOC-IL';
	SELECT @TC_SOC_OS_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'TC-SOC-OS';
	SELECT @AC_SOC_OS_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'AC-SOC-OS';
	SELECT @TC_MVO_IL_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'TC-MVO-IL';
	SELECT @AC_MVO_IL_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'AC-MVO-IL';
	SELECT @TC_MVO_OS_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'TC-MVO-OS';
	SELECT @AC_MVO_OS_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'AC-MVO-OS';
	SELECT @REP_Id = servicetypeid FROM dbo.servicetype WHERE shortname LIKE 'REP';

    -- migrate the data from the "capabilityfilter" table to the new table "capability_servicetype"

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @REP_Id FROM dbo.capabilityfilter 
	WHERE servicetyperepair = 1

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @TC_SOC_IL_Id FROM dbo.capabilityfilter 
	WHERE locationlaboratory = 1 AND supportnonaccredited = 1 AND servicetypeverification = 1

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @AC_SOC_IL_Id FROM dbo.capabilityfilter 
	WHERE locationlaboratory = 1 AND supportaccredited = 1 AND servicetypeverification = 1

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @TC_SOC_OS_Id FROM dbo.capabilityfilter 
	WHERE locationonsite = 1 AND supportnonaccredited = 1 AND servicetypeverification = 1

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @AC_SOC_OS_Id FROM dbo.capabilityfilter 
	WHERE locationonsite = 1 AND supportaccredited = 1 AND servicetypeverification = 1

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @TC_MVO_IL_Id FROM dbo.capabilityfilter 
	WHERE locationlaboratory = 1 AND servicetypecalibration = 1 AND supportnonaccredited = 1 AND servicetyperepair = 0

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @AC_MVO_IL_Id FROM dbo.capabilityfilter 
	WHERE locationlaboratory = 1 AND servicetypecalibration = 1  AND supportaccredited = 1 AND servicetyperepair = 0

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @TC_MVO_OS_Id FROM dbo.capabilityfilter 
	WHERE locationonsite = 1 AND servicetypecalibration = 1 AND supportnonaccredited = 1 AND servicetyperepair = 0

	INSERT INTO dbo.capability_servicetype (lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, servicetypeid) 
	SELECT lastModified, bestlab, comments, filtertype, procedureid, lastModifiedBy, @AC_MVO_OS_Id FROM dbo.capabilityfilter 
	WHERE locationonsite = 1 AND servicetypecalibration = 1 AND supportaccredited = 1 AND servicetyperepair = 0

	INSERT INTO dbversion(version) VALUES(701);

COMMIT TRAN
