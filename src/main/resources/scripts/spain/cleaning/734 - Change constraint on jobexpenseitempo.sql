BEGIN TRAN

ALTER TABLE jobexpenseitempo
DROP CONSTRAINT IF EXISTS UK_jobexpenseitempo_expenseitemid_bpoid_poid

ALTER TABLE jobexpenseitempo
ADD CONSTRAINT UK_jobexpenseitempo_expenseitemid UNIQUE (expenseitemid)

GO

INSERT INTO dbversion VALUES (734)

COMMIT TRAN