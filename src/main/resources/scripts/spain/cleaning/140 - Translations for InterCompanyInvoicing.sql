-- ES/FR (and default EN) translations for internal invoicing
-- Galen Beck 2017-04-25

USE [spain]

BEGIN TRAN

DECLARE @activityId int;

SET @activityId = (SELECT stateid FROM itemstate WHERE description = 'Internal invoice item created')

IF NOT EXISTS (SELECT 1 FROM itemstatetranslation WHERE stateid = @activityId AND locale = 'es_ES')
	INSERT INTO itemstatetranslation (locale, stateid, translation)
	VALUES ('es_ES', @activityId, 'Elemento de factura interna creado')

IF NOT EXISTS (SELECT 1 FROM itemstatetranslation WHERE stateid = @activityId AND locale = 'fr_FR')
	INSERT INTO itemstatetranslation (locale, stateid, translation)
	VALUES ('fr_FR', @activityId, 'El�ment de facture interne cr��')
	
GO

DECLARE @statusId int;

SET @statusId = (SELECT statusid FROM invoicestatus WHERE description = 'Awaiting completion of internal invoice')

IF NOT EXISTS (SELECT 1 FROM invoicestatusnametranslation WHERE statusid = @statusId AND locale = 'en_GB')
	INSERT INTO invoicestatusnametranslation (locale, statusid, translation)
	VALUES ('en_GB', @statusId, 'Internal invoice')

IF NOT EXISTS (SELECT 1 FROM invoicestatusnametranslation WHERE statusid = @statusId AND locale = 'es_ES')
	INSERT INTO invoicestatusnametranslation (locale, statusid, translation)
	VALUES ('es_ES', @statusId, 'Factura interna')

IF NOT EXISTS (SELECT 1 FROM invoicestatusnametranslation WHERE statusid = @statusId AND locale = 'fr_FR')
	INSERT INTO invoicestatusnametranslation (locale, statusid, translation)
	VALUES ('fr_FR', @statusId, 'Facture interne')

IF NOT EXISTS (SELECT 1 FROM invoicestatusdescriptiontranslation WHERE statusid = @statusId AND locale = 'en_GB')
	INSERT INTO invoicestatusdescriptiontranslation (locale, statusid, translation)
	VALUES ('en_GB', @statusId, 'Awaiting completion of internal invoice')

IF NOT EXISTS (SELECT 1 FROM invoicestatusdescriptiontranslation WHERE statusid = @statusId AND locale = 'es_ES')
	INSERT INTO invoicestatusdescriptiontranslation (locale, statusid, translation)
	VALUES ('es_ES', @statusId, 'Esperar la finalizaci�n de la factura interna')

IF NOT EXISTS (SELECT 1 FROM invoicestatusdescriptiontranslation WHERE statusid = @statusId AND locale = 'fr_FR')
	INSERT INTO invoicestatusdescriptiontranslation (locale, statusid, translation)
	VALUES ('fr_FR', @statusId, 'En attente de la facture interne')
	
GO

COMMIT TRAN