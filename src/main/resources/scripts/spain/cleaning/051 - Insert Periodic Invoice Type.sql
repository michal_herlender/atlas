-- Author Galen Beck 2016-07-26
-- Creates new InvoiceType and associated entities for "Periodic" invoice

BEGIN TRAN

USE [spain]
GO

SET IDENTITY_INSERT [invoicetype] ON
GO

INSERT INTO [dbo].[invoicetype]
           ([id],
		   [addtoaccount]
           ,[deftosinglecost]
           ,[includeonjobsheet]
           ,[name])
     VALUES
           (6, 1, 1, 1, 'Periodic');
GO

SET IDENTITY_INSERT [invoicetype] OFF
GO

IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 6 AND locale = 'en_GB') 
	INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (6, 'en_GB', 'Periodic')
ELSE 
	UPDATE dbo.invoicetypetranslation SET translation = 'Periodic' WHERE id = 5 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 6 AND locale = 'es_ES') 
	INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (6, 'es_ES', 'Periódico')
ELSE 
	UPDATE dbo.invoicetypetranslation SET translation = 'Periódico' WHERE id = 5 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicedoctypetemplate WHERE compdoctypeid = 20 AND typeid = 6)
	INSERT INTO [dbo].[invoicedoctypetemplate]
           ([id]
		   ,[defaulttemplate]
           ,[compdoctypeid]
           ,[templateId]
           ,[description]
           ,[coid]
           ,[suptemplateid]
           ,[typeid])
     VALUES
           (30,0,20,16,'Invoice Periodic HTML (Using Sales)',null,null,6);

IF NOT EXISTS (SELECT 1 FROM dbo.invoicedoctypetemplate WHERE compdoctypeid = 64 AND typeid = 6)
	INSERT INTO [dbo].[invoicedoctypetemplate]
           ([id]
		   ,[defaulttemplate]
           ,[compdoctypeid]
           ,[templateId]
           ,[description]
           ,[coid]
           ,[suptemplateid]
           ,[typeid])
     VALUES
           (31,0,64,21,'Invoice Periodic RTF (Using Sales)',null,null,6);

GO

ROLLBACK TRAN