USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.users ADD passwordInvalidated tinyint not null
	CONSTRAINT d_users_invalidatedPassword DEFAULT (0);
	ALTER TABLE dbo.users ADD passwordInvalidatedAt datetime;
	INSERT INTO dbversion(version) VALUES(504);

COMMIT TRAN