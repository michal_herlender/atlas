-- Updates stategrouplinktype enum for newly created stategrouplink from blank string value
-- This stategrouplinktype is really just used for "dashboard" stategrouplinks
-- but is needed for the StateGroupLink to load without an exception

USE [atlas]
GO

BEGIN TRAN

UPDATE [dbo].[stategrouplink]
   SET [type] = 'CURRENT_SUBDIV'
 WHERE type = ''
GO
	
INSERT INTO dbversion(version) VALUES(672);

COMMIT TRAN