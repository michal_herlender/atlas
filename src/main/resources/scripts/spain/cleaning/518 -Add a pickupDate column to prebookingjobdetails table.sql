--Add a pickupDate column to prebookingjobdetails table
USE [atlas]
GO
BEGIN TRAN

	ALTER TABLE dbo.prebookingjobdetails ADD pickupDate DATETIME2;

	INSERT INTO dbversion(version) VALUES(518);

COMMIT TRAN