USE [atlas]

BEGIN TRAN

update certificate set calibrationverificationstatus = 'ACCEPTABLE', restriction = 1 where calibrationverificationstatus = 'APPLICABLE'

	INSERT INTO dbversion VALUES (712)

COMMIT TRAN