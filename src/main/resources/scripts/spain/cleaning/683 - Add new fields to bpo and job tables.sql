
USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.bpo ADD jobid INT NULL;
ALTER TABLE dbo.bpo ADD CONSTRAINT FK_bpo_job FOREIGN KEY (jobid) REFERENCES dbo.job(jobid);

ALTER TABLE dbo.job ADD defaultbpoid INT NULL;
ALTER TABLE dbo.job ADD CONSTRAINT FK_job_default_bpo FOREIGN KEY (defaultbpoid) REFERENCES dbo.bpo(poid);

INSERT INTO dbversion(version) VALUES(683);

COMMIT TRAN



