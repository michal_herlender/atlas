USE [atlas]
Go


BEGIN TRAN

    create table dbo.checkout (
       id int identity not null,
        checkInDate datetime2,
        checkOutComplet bit,
        checkOutDate datetime2,
        plantid int not null,
        jobid int ,
        techid int not null,
        primary key (id)
    );


 alter table dbo.checkout add constraint FK8vkhfabqcrm03b3g402vwd3of foreign key (plantid) references dbo.instrument;
 alter table dbo.checkout add constraint FKmhseygnx5aoot1wip11qiaxq3 foreign key (jobid) references dbo.job;
 alter table dbo.checkout add constraint FKqdg2ogkpcxc6acyfmvth47nxq foreign key (techid) references dbo.contact;

 create table dbo.confidencecheck (
       id int identity not null,
        description varchar(255),
        performedOn datetime2,
        result varchar(10),
        techid int not null,
        primary key (id)
    );

alter table dbo.confidencecheck  add constraint FKtm464c8liospc1dh4hey5tluo foreign key (techid) references dbo.contact;

create table dbo.checkout_confidencecheck (
       confidencecheckid int not null,
        checkoutid int not null
    );

alter table dbo.checkout_confidencecheck add constraint FK_checkout_confidencecheck__checkout foreign key (checkoutid) references dbo.checkout;
alter table dbo.checkout_confidencecheck  add constraint FK_checkout_confidencecheck_linktable_confidence foreign key (confidencecheckid) references dbo.confidencecheck;


INSERT INTO dbversion(version) VALUES(755);

COMMIT TRAN

