USE [spain]

BEGIN TRAN

delete from spain.dbo.permission where permission='EDIT_CONTACT_PROCEDURE_ACCREDITATION'
delete from spain.dbo.permission where permission='SEARCH_PROCEDURE'
delete from spain.dbo.permission where permission='EDIT_PROCEDURE'
delete from spain.dbo.permission where permission='EDIT_WORK_INSTRUCTION'
delete from spain.dbo.permission where permission='EDIT_WORK_INSTRUCTION_VERSION'
delete from spain.dbo.permission where permission='CYLINDRICAL_STANDARD'
delete from spain.dbo.permission where permission='WIRE'
delete from spain.dbo.permission where permission='CURRENT_FINANCIAL_POSITION'
delete from spain.dbo.permission where permission='SALES_TRANSACTIONS'
delete from spain.dbo.permission where permission='SALES_BY_COMPANIES'
delete from spain.dbo.permission where permission='MONTHLY_SALES_FIGURES'
delete from spain.dbo.permission where permission='WEB'
delete from spain.dbo.permission where permission='EDIT_INSTRUCTION'
delete from spain.dbo.permission where permission='ADD_EDIT_FLEXIBLE_FIELD_COMPANY'
delete from spain.dbo.permission where permission='DELETE_BANK_ACCOUNT'
delete from spain.dbo.permission where permission='CREATE_BPO_FOR_COMPANY'
delete from spain.dbo.permission where permission='COMPANY_ON_STOP_TRUSTED'
delete from spain.dbo.permission where permission='COMPANY_ACTIVATE_DEACTIVATE'
delete from spain.dbo.permission where permission='EDIT_COMPANY_BPO'
delete from spain.dbo.permission where permission='EDIT_COMPANY_SETTINGS '
delete from spain.dbo.permission where permission='EDIT_CAPABILITY_FILTER'
delete from spain.dbo.permission where permission='EDIT_INSTRUMENT_COMPL_FIELD'
delete from spain.dbo.permission where permission='EDIT_MEASURE_POSSIBILITY_INSTRUMENT'
delete from spain.dbo.permission where permission='CREATE_PO_JOB'
delete from spain.dbo.permission where permission='CLIENT_PO_ADD_RMV_JOB_ITEM'
delete from spain.dbo.permission where permission='EDIT_CLIENT_PO'
delete from spain.dbo.permission where permission='DE_ACTIVATE_CLIENT_PO'
delete from spain.dbo.permission where permission='REMOVE_BPO'
delete from spain.dbo.permission where permission='SELECT_CONTACT_JOB'
delete from spain.dbo.permission where permission='EDIT_JOB_INVOICING'
delete from spain.dbo.permission where permission='EDIT_CALIBRATION_ACCREDITATION'
delete from spain.dbo.permission where permission='ADD_STANDARD_PROCEDURE'
delete from spain.dbo.permission where permission='ADD_CAL_REQ_INSTRUMENT '
delete from spain.dbo.permission where permission='EDIT_CAL_REQ_INSTRUMENT '
delete from spain.dbo.permission where permission='DELETE_CAL_REQ_INSTRUMENT '
delete from spain.dbo.permission where permission='SHOW_HISTORY_CAL_REQ_INSTRUMENT '
delete from spain.dbo.permission where permission='ALLOCATE_ITEM_TO_ENGINEER'
delete from spain.dbo.permission where permission='ISSUE_PURCHASSE_ORDER'
delete from spain.dbo.permission where permission='ADD_INVOICE_PURCHASSE_ORDER'
delete from spain.dbo.permission where permission='DELETE_INSTRUMENT_IN_JOB'
delete from spain.dbo.permission where permission='LINK_QUOTATION'
delete from spain.dbo.permission where permission='REMOVE_JOB_QUOTE_LINK'
delete from spain.dbo.permission where permission='UNLOCK_INVOICE'
delete from spain.dbo.permission where permission='DELETE_ITEM_INVOICE'
delete from spain.dbo.permission where permission='ADD_INVOICE_DELIVERY'
delete from spain.dbo.permission where permission='REMOVE_INVOICE_DELIVERY'
delete from spain.dbo.permission where permission='INVOICE_DOCUMENT'
delete from spain.dbo.permission where permission='ADD_EDDIT_USER_GROUP'
delete from spain.dbo.permission where permission='EDIT_LOCATION'

	
insert into dbo.permission(groupId, permission)
values (2, 'EDIT_CONTACT_PROCEDURE_ACCREDITATION'),
(3, 'SEARCH_PROCEDURE'),
(2, 'EDIT_PROCEDURE'),
(2, 'EDIT_WORK_INSTRUCTION'),
(2, 'EDIT_WORK_INSTRUCTION_VERSION'),
(2, 'CYLINDRICAL_STANDARD'),
(2, 'WIRE'),
(2, 'CURRENT_FINANCIAL_POSITION'),
(2, 'SALES_TRANSACTIONS'),
(2, 'SALES_BY_COMPANIES'),
(2, 'MONTHLY_SALES_FIGURES'),
(1, 'WEB'),
(1, 'EDIT_INSTRUCTION'),
(1, 'ADD_EDIT_FLEXIBLE_FIELD_COMPANY'),
(1, 'DELETE_BANK_ACCOUNT'),
(1, 'CREATE_BPO_FOR_COMPANY'),
(1, 'COMPANY_ON_STOP_TRUSTED'),
(1, 'COMPANY_ACTIVATE_DEACTIVATE'),
(1, 'EDIT_COMPANY_BPO'),
(1, 'EDIT_COMPANY_SETTINGS '),
(1, 'EDIT_CAPABILITY_FILTER'),
(1, 'EDIT_INSTRUMENT_COMPL_FIELD'),
(1, 'EDIT_MEASURE_POSSIBILITY_INSTRUMENT'),
(1, 'CREATE_PO_JOB'),
(1, 'CLIENT_PO_ADD_RMV_JOB_ITEM'),
(1, 'EDIT_CLIENT_PO'),
(1, 'DE_ACTIVATE_CLIENT_PO'),
(1, 'REMOVE_BPO'),
(1, 'SELECT_CONTACT_JOB'),
(1, 'EDIT_JOB_INVOICING'),
(1, 'EDIT_CALIBRATION_ACCREDITATION'),
(1, 'ADD_STANDARD_PROCEDURE'),
(1, 'ADD_CAL_REQ_INSTRUMENT '),
(1, 'EDIT_CAL_REQ_INSTRUMENT '),
(1, 'DELETE_CAL_REQ_INSTRUMENT '),
(1, 'SHOW_HISTORY_CAL_REQ_INSTRUMENT '),
(1, 'ALLOCATE_ITEM_TO_ENGINEER'),
(1, 'ISSUE_PURCHASE_ORDER'),
(1, 'ADD_INVOICE_PURCHASE_ORDER'),
(1, 'DELETE_INSTRUMENT_IN_JOB'),
(1, 'LINK_QUOTATION'),
(1, 'REMOVE_JOB_QUOTE_LINK'),
(1, 'UNLOCK_INVOICE'),
(1, 'DELETE_ITEM_INVOICE'),
(1, 'ADD_INVOICE_DELIVERY'),
(1, 'REMOVE_INVOICE_DELIVERY'),
(1, 'INVOICE_DOCUMENT'),
(1, 'ADD_EDIT_USER_GROUP'),
(1, 'EDIT_LOCATION');

update dbo.permission 
set permission='ADD_EDIT_ASSET'
where permission='ADD_EDDIT_ASSET';

update dbo.permission 
set permission='DISPLAY_LINK_PURCHASE_ORDER'
where permission='DISPLAY_LINK_PURCHASSE_ORDER';


update dbo.permission 
set permission='DISPLAY_LINK_PURCHASE_ORDER_ITEM'
where permission='DISPLAY_LINK_PURCHASSE_ORDER_ITEM';


INSERT INTO dbversion(version) VALUES(335);

COMMIT TRAN