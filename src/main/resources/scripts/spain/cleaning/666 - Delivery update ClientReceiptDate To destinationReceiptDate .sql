use [atlas]
go

BEGIN TRAN
	
	EXEC sp_rename 'delivery.clientreceiptdate', 'destinationReceiptDate', 'COLUMN';
	GO

INSERT INTO dbversion(version) VALUES(666);

COMMIT TRAN