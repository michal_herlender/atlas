-- Script to delete one duplicate user (in production) and add uniqueness constraint for person id in user table

BEGIN TRAN

USE [spain]
GO

-- To find duplicates (i.e. 2 users with smae personid)
--SELECT personid, count(personid)
--  FROM [dbo].[users] GROUP BY personid HAVING (count(personid) > 1)

-- To find user record details about any selected duplicates
-- SELECT [username],[log_userid],[personid],[lastModified]
--  FROM [spain].[dbo].[users] where personid = 24141;

-- To find person record details about any selected duplicates / creatore
-- SELECT [personid],[email],[firstname],[lastname],[log_userid],[creditCard_ccid],[subdivid],[lastModified],[locale]
--  FROM [dbo].[contact] where personid in (17368, 24141);

-- Delete duplicate user (manual decision about which to delete needed)
DELETE FROM [dbo].[user_role]
      WHERE username = 'mcampos';
GO

DELETE FROM [dbo].[users]
      WHERE username = 'mcampos';
GO

-- Add constraint
ALTER TABLE [dbo].[users]
ADD CONSTRAINT uc_personid UNIQUE(personid);
GO

ROLLBACK TRAN