-- Author Galen Beck - 2016-09-22
-- Inserts default Vat Rates for Morocco, France, and Germany
USE [spain]
GO

BEGIN TRAN;

IF NOT EXISTS (SELECT [vatcode] FROM [dbo].[vatrate] WHERE [type] = 0 AND [countryid] = 117)
    INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid])
    VALUES
           ('5','Morocco',20.00,0,117);
GO

IF NOT EXISTS (SELECT [vatcode] FROM [dbo].[vatrate] WHERE [type] = 0 AND [countryid] = 61)
    INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid])
    VALUES
           ('6','France',20.00,0,61);
GO

IF NOT EXISTS (SELECT [vatcode] FROM [dbo].[vatrate] WHERE [type] = 0 AND [countryid] = 65)
    INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid])
    VALUES
           ('7','Germany',19.00,0,65);
GO

COMMIT TRAN;


