update dbo.servicetype set longname='In-House Accredited Calibration' where longname='Accredited Calibration';
update dbo.servicetype set longname='In-House Standard Calibration' where longname='Standard Calibration';
insert into dbo.servicetype ([displaycolour],[displaycolourfasttrack],[longname],[shortname]) VALUES ('#FFFFCC','#FFB9B9','On-Site Accredited Calibration','ACC');
insert into dbo.servicetype ([displaycolour],[displaycolourfasttrack],[longname],[shortname]) VALUES ('#FFFFFF','#FEE0B4','On-Site Standard Calibration','STD');
insert into dbo.servicetype ([displaycolour],[displaycolourfasttrack],[longname],[shortname]) VALUES ('#CCFFFF','#10E010','Repair','REP');