USE [atlas]

BEGIN TRAN
	DECLARE @gso_activity INT,
			@gso_status INT,
			@gso_doc_activity INT,
			@gso_doc_notrequired_activity INT,
			@complete_beforedoc_oc INT,
			@complete_afterdoc_oc INT,
			@needs_fr_afterdoc_oc INT,
			@lookup_gso_result INT,
			@lookup_90 INT,
			@new_lookup INT,
			@lookup_has_doc INT,
			@os_yes_has_doc INT,
			@lookup_has_fr INT,
			@onhold_status INT;

	--fix stategrouplink issue
	SELECT @gso_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting general service operation';
	SELECT @gso_activity = stateid FROM dbo.itemstate WHERE [description] = 'General service operation';
	SELECT @onhold_status = stateid FROM dbo.itemstate WHERE [description] = 'General service operation On-Hold';
	UPDATE dbo.stategrouplink SET groupid = 65 WHERE stateid = @gso_status AND groupid = 64;
	UPDATE dbo.stategrouplink SET groupid = 66 WHERE stateid = @gso_activity AND groupid = 65;
	UPDATE dbo.stategrouplink SET groupid = 67 WHERE stateid = @onhold_status AND groupid = 66;

	--add needs_failure_report lookup result
	SELECT @lookup_gso_result = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_GeneralServiceOperationResult';
	SELECT @complete_beforedoc_oc = outcomestatusid FROM dbo.lookupresult WHERE lookupid = @lookup_gso_result AND resultmessage = 0;
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage)
	VALUES (@gso_activity, @lookup_gso_result, @complete_beforedoc_oc, NULL, 3);

	
	SELECT @lookup_90 = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_NextWorkRequirementByJobCompany (before lab calibration)';
	UPDATE dbo.outcomestatus SET statusid = NULL, lookupid = @lookup_90 WHERE activityid = @gso_activity AND statusid = @gso_status;

	INSERT INTO dbo.lookupinstance([lookup], lookupfunction) VALUES('Lookup_GeneralServiceOperationResult (after document upload)', 54);
	SELECT @new_lookup = MAX(id) FROM dbo.lookupinstance;

	SELECT @lookup_has_doc = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_HasDocumentForMostRecentGeneralOperation';
	UPDATE dbo.lookupinstance SET lookupfunction = 59 WHERE id = @lookup_has_doc;
	SELECT @os_yes_has_doc = outcomestatusid FROM dbo.lookupresult WHERE lookupid = @lookup_has_doc AND resultmessage = 0;
	UPDATE dbo.outcomestatus SET lookupid = @new_lookup WHERE id = @os_yes_has_doc; 

	SELECT @gso_doc_notrequired_activity = stateid FROM dbo.itemstate WHERE [description] = 'Document not required for general operation';
	SELECT @gso_doc_activity = stateid FROM dbo.itemstate WHERE [description] = 'Document uploaded for general operation';
	UPDATE dbo.itemstate SET lookupid = @new_lookup WHERE stateid IN (@gso_doc_notrequired_activity, @gso_doc_activity);

	SELECT @lookup_has_fr = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_ItemHasFaultReport (inhouse after calibration failure)';
	-- Add new outcome status	
	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
	VALUES (1,NULL, @lookup_90, NULL);
	SET @complete_afterdoc_oc = IDENT_CURRENT('outcomestatus');
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage)
	VALUES (NULL, @new_lookup, @complete_afterdoc_oc, NULL, 0);

	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
	VALUES (0,NULL, @lookup_has_fr, NULL);
	SET @needs_fr_afterdoc_oc = IDENT_CURRENT('outcomestatus');
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage)
	VALUES (NULL, @new_lookup, @needs_fr_afterdoc_oc, NULL, 3);

	-- add two new service type
	-- for VBF and QUALIFICATION
	DECLARE @vbf_servicetype INT,
			@qual_servicetype INT;

	INSERT INTO dbo.servicetype(shortname, longname, displaycolour, displaycolourfasttrack, domaintype, repair, canbeperformedbyclient, orderby)
	VALUES ('VCO', 'Verification of correct operation', '#FFFFFF', '#FFFFFF', 0, 0, 0, 42)
	SET @vbf_servicetype = IDENT_CURRENT('servicetype');
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@vbf_servicetype, 'en_GB', 'VPO')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@vbf_servicetype, 'en_GB', 'Verification of proper operation')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@vbf_servicetype, 'es_ES', 'VCF')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@vbf_servicetype, 'es_ES', 'Verificación del correcto funcionamiento')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@vbf_servicetype, 'fr_FR', 'VBF')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@vbf_servicetype, 'fr_FR', 'Vérification du bon fonctionnement')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@vbf_servicetype, 'de_DE', 'UOB')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@vbf_servicetype, 'de_DE', 'Überprüfung des ordnungsgemäßen Betriebs')
	INSERT INTO dbo.jobtypeservicetype(defaultvalue,jobtypeid,servicetypeid)
	VALUES (1, 1, @vbf_servicetype);

	INSERT INTO dbo.servicetype(shortname, longname, displaycolour, displaycolourfasttrack, domaintype, repair, canbeperformedbyclient, orderby)
	VALUES ('QUAL', 'Qualification', '#FFFFFF', '#FFFFFF', 0, 0, 0, 43)
	SET @qual_servicetype = IDENT_CURRENT('servicetype');
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@qual_servicetype, 'en_GB', 'QUAL')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@qual_servicetype, 'en_GB', 'Qualification')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@qual_servicetype, 'es_ES', 'CALI')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@qual_servicetype, 'es_ES', 'Calificación')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@qual_servicetype, 'fr_FR', 'QUAL')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@qual_servicetype, 'fr_FR', 'Qualification')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@qual_servicetype, 'de_DE', 'QUAL')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@qual_servicetype, 'de_DE', 'Qualifikation')
	INSERT INTO dbo.jobtypeservicetype(defaultvalue,jobtypeid,servicetypeid)
	VALUES (1, 1, @qual_servicetype);

	INSERT INTO dbversion(version) VALUES (639)

COMMIT TRAN