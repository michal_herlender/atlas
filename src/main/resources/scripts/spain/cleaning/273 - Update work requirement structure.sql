-- Alters structure of workrequirement, no longer need workrequirementprocedure

USE [spain]

BEGIN TRAN

    alter table dbo.workrequirement 
        add addressid int;

    alter table dbo.workrequirement 
        add procedureid int;

    alter table dbo.workrequirement 
        add servicetypeid int;

    alter table dbo.workrequirement 
        add constraint FK_workrequirement_address 
        foreign key (addressid) 
        references dbo.address;

    alter table dbo.workrequirement 
        add constraint FK_workrequirement_procedure 
        foreign key (procedureid) 
        references dbo.procs;

    alter table dbo.workrequirement 
        add constraint FK_workrequirement_servicetype 
        foreign key (servicetypeid) 
        references dbo.servicetype;

GO

PRINT 'Updating work requirement - procedure id'

UPDATE [dbo].[workrequirement]
   SET procedureid = (SELECT procid FROM workrequirementprocedure WHERE workrequirementprocedure.reqid = workrequirement.id);

GO

PRINT 'Updating work requirement - address id'

UPDATE [dbo].[workrequirement]
   SET addressid = (SELECT addrid FROM department WHERE department.deptid = workrequirement.deptid)
   WHERE workrequirement.deptid is not null;

GO

PRINT 'Updating work requirement - service type id'

UPDATE [dbo].[workrequirement]
   SET workrequirement.servicetypeid = 
		(SELECT calibrationtype.servicetypeid FROM calibrationtype
		 INNER JOIN jobitem ON jobitem.caltypeid = calibrationtype.caltypeid
		 INNER JOIN jobitemworkrequirement ON jobitemworkrequirement.jobitemid = jobitem.jobitemid
		 WHERE jobitemworkrequirement.reqid = workrequirement.id);

GO

-- can't do yet as we seem to have orphaned workrequirements... potentially remove them?
--    alter table dbo.workrequirement 
--        alter column servicetypeid int not null;
	
INSERT INTO dbversion(version) VALUES(273);


COMMIT TRAN