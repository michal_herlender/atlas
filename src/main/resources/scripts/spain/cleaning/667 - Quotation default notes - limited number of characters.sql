use [atlas]
go

BEGIN TRAN

	
alter table defaultnote 
	alter column note varchar(5000) 
	go

ALTER TABLE defaultnotenotetranslation
		DROP CONSTRAINT PK__defaultn__640E83D93184B279;
go

alter table defaultnotenotetranslation 
    alter column translation varchar(5000) 

ALTER TABLE defaultnotenotetranslation
ADD CONSTRAINT PK__defaultn__640E83D93184B279 PRIMARY KEY (defaultnoteid,locale);

go

INSERT INTO dbversion(version) VALUES(667);

COMMIT TRAN

