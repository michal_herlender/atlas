USE [atlas]
GO

BEGIN TRAN
	
	DECLARE @gso_status INT,
			@gso_activity INT,
			@gso_onhold INT,
			@gso_client_decision_workactivity INT,
			@lastInsertedAO_rejected_redo INT;

	SELECT @gso_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting general service operation';
	SELECT @gso_activity = stateid FROM dbo.itemstate WHERE [description] = 'General service operation';
	SELECT @gso_onhold = stateid FROM dbo.itemstate WHERE [description] = 'General service operation On-Hold';
	
	INSERT INTO dbo.stategrouplink(groupid,stateid,type)
	VALUES (36, @gso_status, 'CURRENT_SUBDIV'),
		   (36, @gso_activity, 'CURRENT_SUBDIV'),
		   (36, @gso_onhold, 'CURRENT_SUBDIV');

    SELECT @gso_client_decision_workactivity = stateid FROM dbo.itemstate WHERE [description] = 'Client decision received for general service operation';
	INSERT INTO dbo.actionoutcome(defaultoutcome, description, positiveoutcome, activityid, active, [type], value)
	VALUES(0, 'General service operation - rejected redo', 0, @gso_client_decision_workactivity, 1, 'client_decision_gso', 'REJECTED_REDO');
    SET @lastInsertedAO_rejected_redo = IDENT_CURRENT('actionoutcome');
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) 
	VALUES (@lastInsertedAO_rejected_redo, 'en_GB', 'General service operation - rejected redo'),
	   (@lastInsertedAO_rejected_redo, 'fr_FR', 'Opération des services généraux - rejeté refaire'),
	   (@lastInsertedAO_rejected_redo, 'es_ES', 'Operación de servicio general - rehacer rechazado'),
	   (@lastInsertedAO_rejected_redo, 'de_DE', 'Allgemeiner Servicebetrieb - abgelehntes Wiederherstellen');

	INSERT INTO dbversion(version) VALUES (644);

COMMIT TRAN