USE [atlas]

BEGIN TRAN

	UPDATE exchangeformatfile SET [type]='PrebookingFile';

	ALTER TABLE asn DROP COLUMN filename;
	
	INSERT INTO dbversion VALUES (713)

COMMIT TRAN