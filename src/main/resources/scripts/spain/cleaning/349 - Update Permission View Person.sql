USE [spain]

Begin Tran

delete from dbo.permission where permission='VIEW_PERSON';

insert into dbo.permission values (1,'VIEW_PERSON');

INSERT INTO dbversion(version) VALUES(349);

COMMIT TRAN