USE spain;

BEGIN TRAN;

INSERT INTO basestatus (type,defaultstatus,name, accepted, followingissue, followingreceipt, issued, requiringattention, rejected) 
VALUES ('certificate',0,'Obsolete',0,0,0,1,0,0);

GO

INSERT INTO basestatusnametranslation (statusid,locale,translation) 
VALUES ((SELECT basestatus.statusid FROM basestatus WHERE basestatus.name = 'Obsolete'), 'en_GB', 'Obsolete'),
((SELECT basestatus.statusid FROM basestatus WHERE basestatus.name = 'Obsolete'), 'fr_FR', 'Obsolète'),
((SELECT basestatus.statusid FROM basestatus WHERE basestatus.name = 'Obsolete'), 'es_ES', 'Obsoleto')

INSERT INTO dbversion VALUES (218);

COMMIT TRAN;