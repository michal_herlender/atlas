BEGIN TRAN

ALTER TABLE dbo.certificatevalidation ALTER COLUMN datevalidated DATE

INSERT INTO dbversion VALUES (866)

COMMIT TRAN