USE [atlas]
GO

BEGIN TRAN

DECLARE @lookup INT,
			@OS_id INT,
			@itemState INT;

select @lookup=li.id from lookupinstance li where li.lookup ='Lookup_ItemQuotedBypassCosting (OnSite)';

select @OS_id=lr.outcomestatusid from lookupresult lr where lr.lookupid=@lookup and lr.resultmessage=1;

select @itemState=ist.stateid from itemstate ist where  ist.description like 'Recorded onsite service - awaiting job costing';

update outcomestatus set lookupid = null , statusid= @itemState where id=@OS_id;

INSERT INTO dbversion(version) VALUES (587);

COMMIT TRAN