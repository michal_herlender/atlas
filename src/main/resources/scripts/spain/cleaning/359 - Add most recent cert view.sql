--Creates view containing some information from the last certificate for each instrment.
--This is used by the Excel instrument download for some customers to overcome limitations
--of JPA criteria queries.

USE [spain]
GO

BEGIN TRAN

if exists (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[mostrecentcertinfo]') )
DROP VIEW [dbo].[mostrecentcertinfo]

GO

CREATE VIEW mostrecentcertinfo AS

SELECT instrument.plantid, certificate.deviation, certificate.calibrationverificationstatus
FROM instrument
       left join certificate ON Certificate.certid = (
    SELECT MAX(allcerts.certid)
    FROM
      (SELECT c1.certid, c1.calibrationverificationstatus, c1.deviation
       FROM jobitem
              LEFT JOIN certlink cl ON cl.jobitemid = jobitem.jobitemid
              LEFT JOIN certificate c1 ON c1.certid = cl.certid
       WHERE jobitem.plantid = instrument.plantid
       UNION
       SELECT c2.certid, c2.calibrationverificationstatus, c2.deviation
       FROM instcertlink icl
              LEFT JOIN certificate c2 ON c2.certid = icl.certid
       WHERE icl.plantid = instrument.plantid) AS allcerts
  )

GO

INSERT INTO dbversion (version)
VALUES (359);

COMMIT TRAN