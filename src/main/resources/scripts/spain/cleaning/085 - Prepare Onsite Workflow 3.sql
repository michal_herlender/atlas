-- Author: Galen Beck 2016-11-04
-- Creates new on-site workflow
-- Note, IDs inserted as they are referenced in system admin workflow documentation 

USE [spain]
GO

BEGIN TRAN

-- Step 1 - Create New Lookup Instances

SET IDENTITY_INSERT [lookupinstance] ON;

INSERT INTO [dbo].[lookupinstance]
            ([id],[lookup],[lookupfunction])
     VALUES (73,'Lookup_ContractReviewOutcome',10),
			(74,'Lookup_PostCalibration',30),
			(75,'Lookup_CertIssuedFromCal',4),
			(76,'Lookup_CertificateIsSigned',3),
			(77,'Lookup_ItemRequiresFurtherCalibration',28);
GO

SET IDENTITY_INSERT [lookupinstance] OFF;

-- Step 2 - Create new Item State (Status and Activity) Definitions
-- Note - TODO translations for Item States in separate script to allow easier updates

SET IDENTITY_INSERT [itemstate] ON;

INSERT INTO [dbo].[itemstate]
           ([stateid],[type],[active],[description],[retired],[lookupid])
     VALUES
           (4054,'workstatus',1,'Awaiting onsite contract review',0,null),
           (4055,'workactivity',1,'Onsite contract review',0,73),
           (4056,'workstatus',1,'Onsite contract review - awaiting client purchase order',0,null),
           (4057,'workstatus',1,'Onsite contract review - awaiting client feedback',0,null),
           (4058,'workstatus',0,'No onsite service performed',0,null),
           (4059,'workstatus',1,'Onsite contract review - awaiting quotation to client',0,null),
           (4060,'workstatus',1,'Onsite calibration on hold',0,null),
           (4061,'workstatus',1,'Onsite calibration - awaiting certificate',0,null),
           (4062,'workactivity',1,'Onsite calibration - certificate issued',0,76),
           (4063,'workstatus',1,'Onsite calibration - awaiting certificate signing',0,null),
           (4064,'workactivity',1,'Onsite calibration - certificate signed',0,77),
           (4065,'workactivity',1,'Onsite calibration - certificate completion to follow',0,77);

GO

SET IDENTITY_INSERT [itemstate] OFF;

-- Step 3 - Update names / lookups for the existing onsite service definitions

UPDATE [dbo].[itemstate] SET [description] = 'Awaiting onsite calibration', 
							 [lookupid] = null
						 WHERE [stateid] = 4047;
UPDATE [dbo].[itemstate] SET [description] = 'Pre-onsite calibration',
							 [lookupid] = 74
						 WHERE [stateid] = 4048;
UPDATE [dbo].[itemstate] SET [description] = 'Recorded onsite service - awaiting job costing',
							 [lookupid] =  null
						 WHERE [stateid] = 4049;
UPDATE [dbo].[itemstate] SET [description] = 'Onsite job costing issued to client',
							 [lookupid] = null
						 WHERE [stateid] = 4050;
UPDATE [dbo].[itemstate] SET [description] = 'Completed onsite service',
							 [lookupid] = null
						 WHERE [stateid] = 4051;

GO

-- Delete existing translations for now (en_GB will get re-inserted at startup)

DELETE FROM [dbo].[itemstatetranslation]
      WHERE stateid in (4047, 4048, 4049, 4050, 4051);

-- Step 4 - Insert Outcome Status values to be wired into all new lookups created
-- Note, defaultoutcome appears to be irrelevant

-- First, remove original connection created as part of proof of concept - not needed

DELETE FROM [dbo].[outcomestatus]
      WHERE [activityid] = 4048 AND [statusid] = 4049;
GO

SET IDENTITY_INSERT [outcomestatus] ON;

INSERT INTO [dbo].[outcomestatus]
           ([id],[defaultoutcome],[activityid],[lookupid],[statusid])
     VALUES
-- For 73 Lookup_ContractReviewOutcome (Contract Review Activity 4055)
           (229,0,4055,null,4056),
           (230,0,4055,null,4057),
           (231,0,4055,null,4058),
           (232,0,4055,null,4059),
           (233,0,4055,null,4047),
-- For 74 Lookup_PostCalibration
           (234,0,4048,75,null),
           (235,0,4048,null,4049),
           (236,0,4048,null,4060),
-- For 75 Lookup_CertIssuedFromCal
           (237,0,null,null,4061),
           (238,0,null,77,null),
-- For 76 Lookup_CertificateIsSigned
           (239,0,null,77,null),
           (240,0,null,null,4063),
-- For 77 Lookup_ItemRequiresFurtherCalibration
           (241,0,null,null,4047),
           (242,0,null,null,4049);
GO

SET IDENTITY_INSERT [outcomestatus] OFF;

-- Update Outcome status 2 (initial start) to connect to 4054 - contract review

UPDATE [dbo].[outcomestatus]
   SET [statusid] = 4054
 WHERE [id] = 2

GO

-- Step 5 - Insert Lookup Results for all new lookups created
-- TODO - determine if activityid needs to be present?  Seems superfluous and irrelevant.

INSERT INTO [dbo].[lookupresult]
           ([result],[activityid],[lookupid],[outcomestatusid],[resultmessage])
     VALUES
-- For 73 Lookup_ContractReviewOutcome
           ('',4055,73,229,6),
           ('',4055,73,230,7),
           ('',4055,73,231,8),
           ('',4055,73,232,9),
           ('',4055,73,233,10),
-- For 74 Lookup_PostCalibration
           ('',4048,74,234,0),
           ('',4048,74,235,8),
           ('',4048,74,236,9),
-- For 75 Lookup_CertIssuedFromCal
           ('',null,75,237,0),
           ('',null,75,238,1),
-- For 76 Lookup_CertificateIsSigned
           ('',4050,76,239,0),
           ('',4050,76,240,1),
-- For 77 Lookup_ItemRequiresFurtherCalibration
           ('',null,77,241,0),
           ('',null,77,242,1);

GO

-- Step 6 - Create action outcomes for contract review and calibration completion
-- Defaultoutcome flag determines default selected option
-- NOTE similar (but different) translations both in database and message files!

SET IDENTITY_INSERT [actionoutcome] ON;

INSERT INTO [dbo].[actionoutcome]
           ([id],[defaultoutcome],[description],[positiveoutcome],[activityid],[active])
     VALUES
           (94,0,'Awaiting client purchase order',1,4055,1),
           (95,0,'Awaiting client feedback',0,4055,1),
           (96,0,'No action required',0,4055,1),
           (97,0,'Awaiting quotation to client',0,4055,1),
           (98,1,'Requires on-site calibration',0,4055,1),
           (99,1,'Calibrated successfully',1,4048,1),
           (100,0,'Unit B.E.R.',0,4048,1),
           (101,0,'Calibration placed on hold',0,4048,1);
GO

SET IDENTITY_INSERT [actionoutcome] OFF;

-- Step 7 - Insert NextActivity values to identify what activity is possible in each status

SET IDENTITY_INSERT [nextactivity] ON;

INSERT INTO [dbo].[nextactivity]
           ([id],[manualentryallowed],[activityid],[statusid])
     VALUES
           (106,0,4055,4054),
		   (107,1,4055,4056),
		   (108,1,4055,4057),
		   (109,1,4055,4059),
		   (110,0,4062,4061),
		   (111,0,4064,4063),
		   (112,0,4065,4063);
GO

SET IDENTITY_INSERT [nextactivity] OFF;

-- Remove manual entry allowed for transitions present from proof of concept

UPDATE [dbo].[nextactivity] SET [manualentryallowed] = 0 WHERE [id] = 104;
UPDATE [dbo].[nextactivity] SET [manualentryallowed] = 0 WHERE [id] = 105;

-- Step 8 - Insert and update state group membership
-- Start with removing existing membership (from proof of concept)

DELETE FROM [dbo].[stategrouplink]
      WHERE [stateid] in (4047, 4049)
GO

-- Group 6 - calibration
-- Group 18 - awaiting calibration
-- Group 27 - resume calibration
-- Group 40 - before onsite
-- Group 41 - during onsite
-- Group 42 - after onsite

-- 4054 'Awaiting onsite contract review'
-- 4056 'Onsite contract review - awaiting client purchase order'
-- 4057 'Onsite contract review - awaiting client feedback'
-- 4059 'Onsite contract review - awaiting quotation'

-- 4047 'Awaiting onsite calibration'
-- 4048 'Pre-onsite calibration'
-- 4060 'Onsite calibration on hold'

-- 4061 'Onsite calibration - awaiting certificate'
-- 4063 'Onsite calibration - awaiting certificate signing'
-- 4049 'Recorded onsite service - awaiting job costing'

INSERT INTO [dbo].[stategrouplink]
           ([groupid],[stateid])
     VALUES
           (40,4054),
           (40,4056),
           (40,4057),
           (40,4059),
           (41,4047),
           (41,4048),
           (41,4060),
           (42,4061),
           (42,4063),
           (42,4049),
           (18,4047),
		   (6,4048),
           (27,4060);
GO

-- Step 9 - Create Hook Activities for various hooks

-- 2 - perform-contract-review

INSERT INTO [dbo].[hookactivity]
           ([beginactivity],[completeactivity],[activityid],[hookid],[stateid])
     VALUES
           (1,1,4055,2,4054),
           (1,1,4055,2,4056),
           (1,1,4055,2,4057),
           (1,1,4055,2,4059);
GO

-- 4 - issue-job-costing

INSERT INTO [dbo].[hookactivity]
           ([beginactivity],[completeactivity],[activityid],[hookid],[stateid])
     VALUES
           (1,1,4050,4,4049);
GO

-- 5 - complete-calibration
-- Note, this one is a little strange as state id is an activity, not a state (workflow editor should change?)
-- Am matching how complete-calibration is wired for existing activities
-- Appears to be specified this way, because it is a complete-activity only.

INSERT INTO [dbo].[hookactivity]
           ([beginactivity],[completeactivity],[activityid],[hookid],[stateid])
     VALUES
           (0,1,4048,5,4048);
GO

-- 8 - begin-calibration

INSERT INTO [dbo].[hookactivity]
           ([beginactivity],[completeactivity],[activityid],[hookid],[stateid])
     VALUES
           (1,0,4048,8,4047),
           (1,0,4048,8,4060);
GO

-- 9 - create-certificate

INSERT INTO [dbo].[hookactivity]
           ([beginactivity],[completeactivity],[activityid],[hookid],[stateid])
     VALUES
           (1,1,4062,9,4061);
GO

-- 18 - sign-certificate

INSERT INTO [dbo].[hookactivity]
           ([beginactivity],[completeactivity],[activityid],[hookid],[stateid])
     VALUES
           (1,1,4064,18,4063);
GO

-- 25 - delay-certificate

INSERT INTO [dbo].[hookactivity]
           ([beginactivity],[completeactivity],[activityid],[hookid],[stateid])
     VALUES
           (1,1,4065,25,4063);
GO

COMMIT TRAN