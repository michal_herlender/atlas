USE [atlas]
GO

BEGIN TRAN

UPDATE itemstatetranslation SET translation = 'Client contacté pour décision relative au certificat' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Client contact progressed for certificate decision')

UPDATE itemstatetranslation SET translation = 'Mise à jour demandée par le client' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Client requested update')

UPDATE itemstatetranslation SET translation = 'Client contacté pour décision relative au chiffrage' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Client contact progressed for costing decision')

UPDATE itemstatetranslation SET translation = 'Client contacté pour décision relative au constat d’anomalie' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Client contact progressed for decision on faulty unit')

UPDATE itemstatetranslation SET translation = 'Sous-traitant contacté pour la mise à jour du rapport' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Third party progressed for update report')

UPDATE itemstatetranslation SET translation = 'Réponse à fournir suite à la demande reçue du sous-traitant' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Response to progress request received from third party')

UPDATE itemstatetranslation SET translation = 'Réponse à fournir suite à la demande reçue du Client' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Response to progress request received from client')

UPDATE itemstatetranslation SET translation = 'Client contacté pour la livraison programmée' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Client progressed for delivery scheduling')

UPDATE itemstatetranslation SET translation = 'Réponse reçue pour demande de date de livraison' where locale='fr_FR' and 
stateid=(select stateid from itemstate where description='Response received for delivery date request')

INSERT INTO dbversion(version) VALUES(516);

COMMIT TRAN 

