BEGIN TRAN;
USE spain;

DELETE cd FROM quotecaldefaults cd 
INNER JOIN defaultquotationcalibrationcondition cc ON cd.conditionid = cc.defcalconid
INNER JOIN calibrationtype ct ON cc.caltypeid = ct.caltypeid 
WHERE ct.orgid <> 6645;

DELETE cc FROM defaultquotationcalibrationcondition cc
INNER JOIN calibrationtype ct ON cc.caltypeid = ct.caltypeid  
WHERE ct.orgid <> 6645;

DELETE ca FROM calibrationaccreditationlevel ca
INNER JOIN calibrationtype ct ON ct.caltypeid = ca.caltypeid
WHERE ct.orgid <> 6645;

DELETE FROM [spain].[dbo].[calibrationtype] where orgid <> 6645;


ROLLBACK TRAN;