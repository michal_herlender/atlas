USE [spain]
GO

BEGIN TRAN

SET IDENTITY_INSERT [dbo].[transportmethod] ON
GO

INSERT INTO [dbo].[transportmethod]
           ([id]
		   ,[method]
           ,[optionperday]
           ,[optionpersubdiv]
           ,[lastModified])
     VALUES
           (9,'Trescal Shuttle - 2',0,0,'2019-02-14'),
           (10,'Trescal Shuttle - 3',0,0,'2019-02-14'),
           (11,'Trescal Shuttle - 4',0,0,'2019-02-14'),
           (12,'Trescal Shuttle - 5',0,0,'2019-02-14')
GO

SET IDENTITY_INSERT [dbo].[transportmethod] OFF
GO

-- Update to new Spanish

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Veh�culo de Trescal'
 WHERE id=1 and locale='ES_es'
GO

-- Fix 7 and 8 (translations were reversed)

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Other'
 WHERE id=7 and locale='en_GB'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Transport sp�cifique'
 WHERE id=7 and locale='fr_FR'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Andere'
 WHERE id=7 and locale='DE_de'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Otros'
 WHERE id=7 and locale='ES_es'
GO

-- Fix 8 and append 1 e.g. Shuttle number 1

UPDATE [dbo].[transportmethod] 
 SET [method] = 'Trescal Shuttle - 1'
 WHERE id=8
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Trescal Shuttle - 1'
 WHERE id=8 and locale='en_GB'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Navette Trescal - 1'
 WHERE id=8 and locale='fr_FR'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Shuttle - 1'
 WHERE id=8 and locale='de_DE'
GO

UPDATE [dbo].[transportmethodtranslation] 
 SET [translation] = 'Veh�culo de Trescal - 1'
 WHERE id=8 and locale='ES_es'
GO

INSERT INTO [dbo].[transportmethodtranslation]
           ([id],[locale],[translation])
     VALUES
           (9,'en_GB','Trescal Shuttle - 2'),
           (9,'fr_FR','Navette Trescal - 2'),
           (9,'de_DE','Shuttle - 2'),
           (9,'es_ES','Veh�culo de Trescal - 2'),

           (10,'en_GB','Trescal Shuttle - 3'),
           (10,'fr_FR','Navette Trescal - 3'),
           (10,'de_DE','Shuttle - 3'),
           (10,'es_ES','Veh�culo de Trescal - 3'),

           (11,'en_GB','Trescal Shuttle - 4'),
           (11,'fr_FR','Navette Trescal - 4'),
           (11,'de_DE','Shuttle - 4'),
           (11,'es_ES','Veh�culo de Trescal - 4'),

           (12,'en_GB','Trescal Shuttle - 5'),
           (12,'fr_FR','Navette Trescal - 5'),
           (12,'de_DE','Shuttle - 5'),
           (12,'es_ES','Veh�culo de Trescal - 5')
GO

-- Update existing "day of week" configured transport options (Mon-Fri) to point to the new shuttle options (1-5)

UPDATE dbo.transportoption 
SET transportmethodid = 8, dayofweek = null
WHERE dbo.transportoption.transportmethodid = 1 and dbo.transportoption.dayofweek = 2
GO

UPDATE dbo.transportoption 
SET transportmethodid = 9, dayofweek = null
WHERE dbo.transportoption.transportmethodid = 1 and dbo.transportoption.dayofweek = 3
GO

UPDATE dbo.transportoption 
SET transportmethodid = 10, dayofweek = null
WHERE dbo.transportoption.transportmethodid = 1 and dbo.transportoption.dayofweek = 4
GO

UPDATE dbo.transportoption 
SET transportmethodid = 11, dayofweek = null
WHERE dbo.transportoption.transportmethodid = 1 and dbo.transportoption.dayofweek = 5
GO

UPDATE dbo.transportoption 
SET transportmethodid = 12, dayofweek = null
WHERE dbo.transportoption.transportmethodid = 1 and dbo.transportoption.dayofweek = 6
GO

INSERT INTO dbversion(version) VALUES(403);
GO

COMMIT TRAN