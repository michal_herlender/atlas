USE spain;

BEGIN TRAN;

ALTER TABLE dbo.certificate
  ADD deviation NUMERIC(15, 5);

COMMIT TRAN;