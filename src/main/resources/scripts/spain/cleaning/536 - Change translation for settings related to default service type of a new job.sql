USE [atlas]

BEGIN TRAN

UPDATE dbo.systemdefaultnametranslation
	SET [translation]='Default service type of new onsite jobs'
	WHERE defaultid=54 AND locale='en_GB' AND [translation]='Default service type of new job items for onsite jobs' ;
UPDATE dbo.systemdefaultnametranslation
	SET [translation]='Tipo de servicio predeterminado de nuevos trabajos en el sitio',locale='es_ES'
	WHERE defaultid=54 AND locale='en_GB' AND [translation]='Tipo de servicio predeterminado de nuevos elementos de trabajo para trabajos en el sitio' ;
UPDATE dbo.systemdefaultnametranslation
	SET [translation]='Type de service par d�faut des nouveaux Job sur site'
	WHERE defaultid=54 AND locale='fr_FR' AND [translation]='Type de service par d�faut des nouveaux Jobitems pour les travaux sur site' ;
UPDATE dbo.systemdefaultnametranslation
	SET [translation]='Default service type of new standard jobs'
	WHERE defaultid=53 AND locale='en_GB' AND [translation]='Default service type of new job items for standard jobs' ;
UPDATE dbo.systemdefaultnametranslation
	SET [translation]='Tipo de servicio predeterminado de nuevos trabajos est�ndar'
	WHERE defaultid=53 AND locale='es_ES' AND [translation]='Tipo de servicio predeterminado de nuevos trabajos para trabajos est�ndar' ;
UPDATE dbo.systemdefaultnametranslation
	SET [translation]='Type de service par d�faut des nouveaux Jobs standard'
	WHERE defaultid=53 AND locale='fr_FR' AND [translation]='Type de service par d�faut des nouveaux Jobitems pour les Jobs standard' ;

UPDATE dbo.systemdefaultdescriptiontranslation
	SET [translation]='Defines the default service type of new onsite jobs'
	WHERE defaultid=54 AND locale='en_GB' AND [translation]='Defines the default service type of new job items for onsite jobs' ;
UPDATE dbo.systemdefaultdescriptiontranslation
	SET [translation]='Define el tipo de servicio predeterminado de nuevos trabajos en el sitio'
	WHERE defaultid=54 AND locale='es_ES' AND [translation]='Define el tipo de servicio predeterminado de nuevos elementos de trabajo para trabajos en el sitio' ;
UPDATE dbo.systemdefaultdescriptiontranslation
	SET [translation]='D�finit le type de service par d�faut des nouveaux Jobs sur site'
	WHERE defaultid=54 AND locale='fr_FR' AND [translation]='D�finit le type de service par d�faut des Jobitems de travail pour les Jobs sur site' ;
UPDATE dbo.systemdefaultdescriptiontranslation
	SET [translation]='Defines the default service type of new standard jobs'
	WHERE defaultid=53 AND locale='en_GB' AND [translation]='Defines the default service type of new job items for standard jobs' ;
UPDATE dbo.systemdefaultdescriptiontranslation
	SET [translation]='Define el tipo de servicio predeterminado de nuevos trabajos est�ndar'
	WHERE defaultid=53 AND locale='es_ES' AND [translation]='Define el tipo de servicio predeterminado de nuevos elementos de trabajo para trabajos est�ndar' ;
UPDATE dbo.systemdefaultdescriptiontranslation
	SET [translation]='D�finit le type de service par d�faut des nouveaux Jobs standard.'
	WHERE defaultid=53 AND locale='fr_FR' AND [translation]='D�finit le type de service par d�faut des Jobitems de travail pour les Jobs standard.' ;


INSERT INTO dbversion(version) VALUES(536);

COMMIT TRAN

