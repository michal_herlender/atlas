-- This script creates the fields needed for lastModifiedBy and lastModified before running 169.
-- Created from SchemaUpdateTool and stripped down to just lastModifiedBy and lastModified fields for now
-- as there are some other naming issues around foreign key constraints.
-- May take up to 1 minute to run.
-- Galen Beck - 2017-08-27

USE [spain]
BEGIN TRAN

    alter table dbo.accessory 
        add lastModifiedBy int;

    alter table dbo.accreditationbody 
        add lastModifiedBy int;

    alter table dbo.accreditationbodyresource 
        add lastModifiedBy int;

    alter table dbo.accreditedlab 
        add lastModifiedBy int;

    alter table dbo.additionalcostcontact 
        add lastModified datetime;

    alter table dbo.additionalcostcontact 
        add lastModifiedBy int;

    alter table dbo.address 
        add lastModifiedBy int;

    alter table dbo.addressplantillassettings 
        add lastModifiedBy int;

    alter table dbo.addresssettingsforallocatedcompany 
        add lastModifiedBy int;

    alter table dbo.addresssettingsforallocatedsubdiv 
        add lastModifiedBy int;

    alter table dbo.asset 
        add lastModified datetime;

    alter table dbo.asset 
        add lastModifiedBy int;

    alter table dbo.assetnote 
        add lastModified datetime;

    alter table dbo.assetnote 
        add lastModifiedBy int;

    alter table dbo.bankaccount 
        add lastModifiedBy int;

    alter table dbo.bankdetail 
        add lastModified datetime;

    alter table dbo.bankdetail 
        add lastModifiedBy int;

    alter table dbo.baseinstruction 
        add lastModifiedBy int;

    alter table dbo.batchcalibration 
        add lastModified datetime;

    alter table dbo.batchcalibration 
        add lastModifiedBy int;

    alter table dbo.bpo 
        add lastModifiedBy int;

    alter table dbo.businessarea 
        add lastModified datetime;

    alter table dbo.businessarea 
        add lastModifiedBy int;

    alter table dbo.businesscompanysettings 
        add lastModifiedBy int;

    alter table dbo.businessemails 
        add lastModifiedBy int;

    alter table dbo.businesssubdivisionsettings 
        add lastModifiedBy int;

    alter table dbo.calibration 
        add lastModifiedBy int;

    alter table dbo.calibrationpoint 
        add lastModified datetime;

    alter table dbo.calibrationpoint 
        add lastModifiedBy int;

    alter table dbo.calibrationpointset 
        add lastModified datetime;

    alter table dbo.calibrationpointset 
        add lastModifiedBy int;

    alter table dbo.callink 
        add lastModified datetime;

    alter table dbo.callink 
        add lastModifiedBy int;

    alter table dbo.calloffitem 
        add lastModifiedBy int;

    alter table dbo.calreq 
        add lastModifiedBy int;

    alter table dbo.catalogprice 
        add lastModifiedBy int;

    alter table dbo.certificate 
        add lastModified datetime;

    alter table dbo.certificate 
        add lastModifiedBy int;

    alter table dbo.certificatevalidation 
        add lastModified datetime;

    alter table dbo.certificatevalidation 
        add lastModifiedBy int;

    alter table dbo.certlink 
        add lastModified datetime;

    alter table dbo.certlink 
        add lastModifiedBy int;

    alter table dbo.certnote 
        add lastModified datetime;

    alter table dbo.certnote 
        add lastModifiedBy int;

    alter table dbo.characteristicdescription 
        add lastModified datetime;

    alter table dbo.characteristicdescription 
        add lastModifiedBy int;

    alter table dbo.collectedinstrument 
        add lastModified datetime;

    alter table dbo.collectedinstrument 
        add lastModifiedBy int;

    alter table dbo.company 
        add lastModifiedBy int;

    alter table dbo.companyaccount 
        add lastModified datetime;

    alter table dbo.companyaccount 
        add lastModifiedBy int;

    alter table dbo.companyaccreditation 
        add lastModifiedBy int;

    alter table dbo.companyinstructionlink 
        add lastModifiedBy int;

    alter table dbo.companysettingsforallocatedcompany 
        add lastModifiedBy int;

    alter table dbo.component 
        add lastModifiedBy int;

    alter table dbo.contact 
        add lastModifiedBy int;

    alter table dbo.contactinstructionlink 
        add lastModifiedBy int;

    alter table dbo.contractreview 
        add lastModified datetime;

    alter table dbo.contractreview 
        add lastModifiedBy int;

    alter table dbo.contractreviewitem 
        add lastModified datetime;

    alter table dbo.contractreviewitem 
        add lastModifiedBy int;

    alter table dbo.costs_adjustment 
        add lastModified datetime;

    alter table dbo.costs_adjustment 
        add lastModifiedBy int;

    alter table dbo.costs_calibration 
        add lastModified datetime;

    alter table dbo.costs_calibration 
        add lastModifiedBy int;

    alter table dbo.costs_inspection 
        add lastModified datetime;

    alter table dbo.costs_inspection 
        add lastModifiedBy int;

    alter table dbo.costs_purchase 
        add lastModified datetime;

    alter table dbo.costs_purchase 
        add lastModifiedBy int;

    alter table dbo.costs_repair 
        add lastModified datetime;

    alter table dbo.costs_repair 
        add lastModifiedBy int;

    alter table dbo.courier 
        add lastModified datetime;

    alter table dbo.courier 
        add lastModifiedBy int;

    alter table dbo.courierdespatch 
        add lastModified datetime;

    alter table dbo.courierdespatch 
        add lastModifiedBy int;

    alter table dbo.courierdespatchtype 
        add lastModified datetime;

    alter table dbo.courierdespatchtype 
        add lastModifiedBy int;

    alter table dbo.creditcard 
        add lastModified datetime;

    alter table dbo.creditcard 
        add lastModifiedBy int;

    alter table dbo.creditnote 
        add lastModifiedBy int;

    alter table dbo.creditnoteitem 
        add lastModified datetime;

    alter table dbo.creditnoteitem 
        add lastModifiedBy int;

    alter table dbo.creditnotenote 
        add lastModified datetime;

    alter table dbo.creditnotenote 
        add lastModifiedBy int;

    alter table dbo.customquotationcalibrationcondition 
        add lastModifiedBy int;

    alter table dbo.customquotationgeneralcondition 
        add lastModifiedBy int;

    alter table dbo.defaultnote 
        add lastModifiedBy int;

    alter table dbo.defaultquotationcalibrationcondition 
        add lastModifiedBy int;

    alter table dbo.defaultquotationgeneralcondition 
        add lastModifiedBy int;

    alter table dbo.defaultstandard 
        add lastModified datetime;

    alter table dbo.defaultstandard 
        add lastModifiedBy int;

    alter table dbo.defect 
        add lastModified datetime;

    alter table dbo.defect 
        add lastModifiedBy int;

    alter table dbo.delivery 
        add lastModifiedBy int;

    alter table dbo.deliveryitem 
        add lastModified datetime;

    alter table dbo.deliveryitem 
        add lastModifiedBy int;

    alter table dbo.deliveryitemnote 
        add lastModified datetime;

    alter table dbo.deliveryitemnote 
        add lastModifiedBy int;

    alter table dbo.deliverynote 
        add lastModified datetime;

    alter table dbo.deliverynote 
        add lastModifiedBy int;

    alter table dbo.departmentmember 
        add lastModified datetime;

    alter table dbo.departmentmember 
        add lastModifiedBy int;

    alter table dbo.description 
        add lastModifiedBy int;

    alter table dbo.dimensional_thread 
        add lastModified datetime;

    alter table dbo.dimensional_thread 
        add lastModifiedBy int;

    alter table dbo.email 
        add lastModified datetime;

    alter table dbo.email 
        add lastModifiedBy int;

    alter table dbo.emailrecipient 
        add lastModified datetime;

    alter table dbo.emailrecipient 
        add lastModifiedBy int;

    alter table dbo.emailtemplate 
        add lastModifiedBy int;

    alter table dbo.faultreport 
        add lastModified datetime;

    alter table dbo.faultreport 
        add lastModifiedBy int;

    alter table dbo.faultreportaction 
        add lastModifiedBy int;

    alter table dbo.faultreportdescription 
        add lastModifiedBy int;

    alter table dbo.faultreportinstruction 
        add lastModifiedBy int;

    alter table dbo.freehandcontact 
        add lastModified datetime;

    alter table dbo.freehandcontact 
        add lastModifiedBy int;

    alter table dbo.hire 
        add lastModifiedBy int;

    alter table dbo.hireaccessory 
        add lastModified datetime;

    alter table dbo.hireaccessory 
        add lastModifiedBy int;

    alter table dbo.hirecrossitem 
        add lastModified datetime;

    alter table dbo.hirecrossitem 
        add lastModifiedBy int;

    alter table dbo.hireinstrument 
        add lastModified datetime;

    alter table dbo.hireinstrument 
        add lastModifiedBy int;

    alter table dbo.hireitem 
        add lastModified datetime;

    alter table dbo.hireitem 
        add lastModifiedBy int;

    alter table dbo.hireitemaccessory 
        add lastModified datetime;

    alter table dbo.hireitemaccessory 
        add lastModifiedBy int;

    alter table dbo.hiremodel 
        add lastModifiedBy int;

    alter table dbo.hirenote 
        add lastModified datetime;

    alter table dbo.hirenote 
        add lastModifiedBy int;

    alter table dbo.hirereplacedinstrument 
        add lastModified datetime;

    alter table dbo.hirereplacedinstrument 
        add lastModifiedBy int;

    alter table dbo.hiresuspenditem 
        add lastModified datetime;

    alter table dbo.hiresuspenditem 
        add lastModifiedBy int;

    alter table dbo.images 
        add lastModified datetime;

    alter table dbo.images 
        add lastModifiedBy int;

    alter table dbo.imagetag 
        add lastModified datetime;

    alter table dbo.imagetag 
        add lastModifiedBy int;

    alter table dbo.instcertlink 
        add lastModified datetime;

    alter table dbo.instcertlink 
        add lastModifiedBy int;

    alter table dbo.instmodel 
        add lastModifiedBy int;

    alter table dbo.instmodeldomain 
        add lastModifiedBy int;

    alter table dbo.instmodelfamily 
        add lastModifiedBy int;

    alter table dbo.instmodelsubfamily 
        add lastModified datetime;

    alter table dbo.instmodelsubfamily 
        add lastModifiedBy int;

    alter table dbo.instrument 
        add lastModifiedBy int;

    alter table dbo.instrumentnote 
        add lastModified datetime;

    alter table dbo.instrumentnote 
        add lastModifiedBy int;

    alter table dbo.instrumentvalidationaction 
        add lastModified datetime;

    alter table dbo.instrumentvalidationaction 
        add lastModifiedBy int;

    alter table dbo.instrumentvalidationissue 
        add lastModified datetime;

    alter table dbo.instrumentvalidationissue 
        add lastModifiedBy int;

    alter table dbo.invoice 
        add lastModifiedBy int;

    alter table dbo.invoiceitem 
        add lastModified datetime;

    alter table dbo.invoiceitem 
        add lastModifiedBy int;

    alter table dbo.invoiceitempo 
        add lastModified datetime;

    alter table dbo.invoiceitempo 
        add lastModifiedBy int;

    alter table dbo.invoicejoblink 
        add lastModified datetime;

    alter table dbo.invoicejoblink 
        add lastModifiedBy int;

    alter table dbo.invoicenote 
        add lastModified datetime;

    alter table dbo.invoicenote 
        add lastModifiedBy int;

    alter table dbo.invoicepo 
        add lastModifiedBy int;

    alter table dbo.job 
        add lastModifiedBy int;

    alter table dbo.jobcosting 
        add lastModifiedBy int;

    alter table dbo.jobcostingitem 
        add lastModified datetime;

    alter table dbo.jobcostingitem 
        add lastModifiedBy int;

    alter table dbo.jobcostingitemnote 
        add lastModified datetime;

    alter table dbo.jobcostingitemnote 
        add lastModifiedBy int;

    alter table dbo.jobcostingnote 
        add lastModified datetime;

    alter table dbo.jobcostingnote 
        add lastModifiedBy int;

    alter table dbo.jobinstructionlink 
        add lastModifiedBy int;

    alter table dbo.jobitem 
        add lastModified datetime;

    alter table dbo.jobitem 
        add lastModifiedBy int;

    alter table dbo.jobitemaction 
        add lastModified datetime;

    alter table dbo.jobitemaction 
        add lastModifiedBy int;

    alter table dbo.jobitemgroup 
        add lastModified datetime;

    alter table dbo.jobitemgroup 
        add lastModifiedBy int;

    alter table dbo.jobitemnote 
        add lastModified datetime;

    alter table dbo.jobitemnote 
        add lastModifiedBy int;

    alter table dbo.jobitempo 
        add lastModified datetime;

    alter table dbo.jobitempo 
        add lastModifiedBy int;

    alter table dbo.jobitemworkrequirement 
        add lastModified datetime;

    alter table dbo.jobitemworkrequirement 
        add lastModifiedBy int;

    alter table dbo.jobnote 
        add lastModified datetime;

    alter table dbo.jobnote 
        add lastModifiedBy int;

    alter table dbo.jobquotelink 
        add lastModified datetime;

    alter table dbo.jobquotelink 
        add lastModifiedBy int;

    alter table dbo.location 
        add lastModified datetime;

    alter table dbo.location 
        add lastModifiedBy int;

    alter table dbo.mailgroupmember 
        add lastModified datetime;

    alter table dbo.mailgroupmember 
        add lastModifiedBy int;

    alter table dbo.mfr 
        add lastModified datetime;

    alter table dbo.mfr 
        add lastModifiedBy int;

    alter table dbo.modelnote 
        add lastModified datetime;

    alter table dbo.modelnote 
        add lastModifiedBy int;

    alter table dbo.modeloption 
        add lastModified datetime;

    alter table dbo.modeloption 
        add lastModifiedBy int;

    alter table dbo.modelpartof 
        add lastModified datetime;

    alter table dbo.modelpartof 
        add lastModifiedBy int;

    alter table dbo.modelrange 
        add lastModified datetime;

    alter table dbo.modelrange 
        add lastModifiedBy int;

    alter table dbo.nominalcode 
        add lastModified datetime;

    alter table dbo.nominalcode 
        add lastModifiedBy int;

    alter table dbo.numberformat 
        add lastModifiedBy int;

    alter table dbo.numeration 
        add lastModifiedBy int;

    alter table dbo.onbehalfitem 
        add lastModified datetime;

    alter table dbo.onbehalfitem 
        add lastModifiedBy int;

    alter table dbo.pat 
        add lastModified datetime;

    alter table dbo.pat 
        add lastModifiedBy int;

    alter table dbo.po 
        add lastModifiedBy int;

    alter table dbo.pointsettemplate 
        add lastModified datetime;

    alter table dbo.pointsettemplate 
        add lastModifiedBy int;

    alter table dbo.presetcomment 
        add lastModifiedBy int;

    alter table dbo.procedureaccreditation 
        add lastModified datetime;

    alter table dbo.procedureaccreditation 
        add lastModifiedBy int;

    alter table dbo.procedureheading 
        add lastModified datetime;

    alter table dbo.procedureheading 
        add lastModifiedBy int;

    alter table dbo.procs 
        add lastModifiedBy int;

    alter table dbo.purchaseorder 
        add lastModifiedBy int;

    alter table dbo.purchaseorderitem 
        add lastModified datetime;

    alter table dbo.purchaseorderitem 
        add lastModifiedBy int;

    alter table dbo.purchaseorderitemnote 
        add lastModified datetime;

    alter table dbo.purchaseorderitemnote 
        add lastModifiedBy int;

    alter table dbo.purchaseorderjobitem 
        add lastModified datetime;

    alter table dbo.purchaseorderjobitem 
        add lastModifiedBy int;

    alter table dbo.purchaseordernote 
        add lastModified datetime;

    alter table dbo.purchaseordernote 
        add lastModifiedBy int;

    alter table dbo.quickpurchaseorder 
        add lastModifiedBy int;

    alter table dbo.quotation 
        add lastModifiedBy int;

    alter table dbo.quotationitem 
        add lastModified datetime;

    alter table dbo.quotationitem 
        add lastModifiedBy int;

    alter table dbo.quotationrequest 
        add lastModifiedBy int;

    alter table dbo.quoteheading 
        add lastModified datetime;

    alter table dbo.quoteheading 
        add lastModifiedBy int;

    alter table dbo.quoteitemnote 
        add lastModified datetime;

    alter table dbo.quoteitemnote 
        add lastModifiedBy int;

    alter table dbo.quotenote 
        add lastModified datetime;

    alter table dbo.quotenote 
        add lastModifiedBy int;

    alter table dbo.recall 
        add lastModifiedBy int;

    alter table dbo.recalldetail 
        add lastModified datetime;

    alter table dbo.recalldetail 
        add lastModifiedBy int;

    alter table dbo.recallitem 
        add lastModified datetime;

    alter table dbo.recallitem 
        add lastModifiedBy int;

    alter table dbo.recallnote 
        add lastModified datetime;

    alter table dbo.recallnote 
        add lastModifiedBy int;

    alter table dbo.recallresponsenote 
        add lastModified datetime;

    alter table dbo.recallresponsenote 
        add lastModifiedBy int;

    alter table dbo.recallrule 
        add lastModified datetime;

    alter table dbo.recallrule 
        add lastModifiedBy int;

    alter table dbo.repeatschedule 
        add lastModified datetime;

    alter table dbo.repeatschedule 
        add lastModifiedBy int;

    alter table dbo.requirement 
        add lastModified datetime;

    alter table dbo.requirement 
        add lastModifiedBy int;

    alter table dbo.salescategory 
        add lastModifiedBy int;

    alter table dbo.schedule 
        add lastModified datetime;

    alter table dbo.schedule 
        add lastModifiedBy int;

    alter table dbo.scheduledquotationrequest 
        add lastModifiedBy int;

    alter table dbo.scheduleequipment 
        add lastModified datetime;

    alter table dbo.scheduleequipment 
        add lastModifiedBy int;

    alter table dbo.schedulenote 
        add lastModified datetime;

    alter table dbo.schedulenote 
        add lastModifiedBy int;

    alter table dbo.servicecapability 
        add lastModifiedBy int;

    alter table dbo.standardused 
        add lastModified datetime;

    alter table dbo.standardused 
        add lastModifiedBy int;

    alter table dbo.subdiv 
        add lastModifiedBy int;

    alter table dbo.subdivinstructionlink 
        add lastModifiedBy int;

    alter table dbo.supplierinvoice 
        add lastModifiedBy int;

    alter table dbo.systemdefaultapplication 
        add lastModifiedBy int;

    alter table dbo.taggedimage 
        add lastModified datetime;

    alter table dbo.taggedimage 
        add lastModifiedBy int;

    alter table dbo.templatepoint 
        add lastModified datetime;

    alter table dbo.templatepoint 
        add lastModifiedBy int;

    alter table dbo.tpinstruction 
        add lastModifiedBy int;

    alter table dbo.tpquotation 
        add lastModifiedBy int;

    alter table dbo.tpquotationitem 
        add lastModified datetime;

    alter table dbo.tpquotationitem 
        add lastModifiedBy int;

    alter table dbo.tpquoteitemnote 
        add lastModified datetime;

    alter table dbo.tpquoteitemnote 
        add lastModifiedBy int;

    alter table dbo.tpquotelink 
        add lastModified datetime;

    alter table dbo.tpquotelink 
        add lastModifiedBy int;

    alter table dbo.tpquotenote 
        add lastModified datetime;

    alter table dbo.tpquotenote 
        add lastModifiedBy int;

    alter table dbo.tpquoterequest 
        add lastModifiedBy int;

    alter table dbo.tpquoterequestitem 
        add lastModified datetime;

    alter table dbo.tpquoterequestitem 
        add lastModifiedBy int;

    alter table dbo.tpquoterequestitemnote 
        add lastModified datetime;

    alter table dbo.tpquoterequestitemnote 
        add lastModifiedBy int;

    alter table dbo.tpquoterequestnote 
        add lastModified datetime;

    alter table dbo.tpquoterequestnote 
        add lastModifiedBy int;

    alter table dbo.tprequirement 
        add lastModified datetime;

    alter table dbo.tprequirement 
        add lastModifiedBy int;

    alter table dbo.transportmethod 
        add lastModified datetime;

    alter table dbo.transportmethod 
        add lastModifiedBy int;

    alter table dbo.transportoption 
        add lastModified datetime;

    alter table dbo.transportoption 
        add lastModifiedBy int;

    alter table dbo.uom 
        add lastModified datetime;

    alter table dbo.uom 
        add lastModifiedBy int;

    alter table dbo.upcomingwork 
        add lastModifiedBy int;

    alter table dbo.user_role 
        add lastModifiedBy int;

    alter table dbo.userpreferences 
        add lastModified datetime;

    alter table dbo.userpreferences 
        add lastModifiedBy int;

    alter table dbo.users 
        add lastModifiedBy int;

    alter table dbo.vatrate 
        add lastModified datetime;

    alter table dbo.vatrate 
        add lastModifiedBy int;

    alter table dbo.webresource 
        add lastModified datetime;

    alter table dbo.webresource 
        add lastModifiedBy int;

    alter table dbo.workinstruction 
        add lastModifiedBy int;

    alter table dbo.workinstructionversion 
        add lastModified datetime;

    alter table dbo.workinstructionversion 
        add lastModifiedBy int;

    alter table dbo.workrequirement 
        add lastModified datetime;

    alter table dbo.workrequirement 
        add lastModifiedBy int;

    alter table dbo.workrequirementprocedure 
        add lastModified datetime;

    alter table dbo.workrequirementprocedure 
        add lastModifiedBy int;

    alter table dbo.accessory 
        add constraint FK8sr1u7c8leek8enhkctd4672p 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.accreditationbody 
        add constraint FKki0xgfsk9701uaf9spj8whr64 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.accreditationbodyresource 
        add constraint FKsw1c31rgy8bytgho5n3ygmr7k 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.accreditedlab 
        add constraint FKgej658pb49n2rajx5wc3eo2wy 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.additionalcostcontact 
        add constraint FK3nrefxc876avd637242ee1gxh 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.address 
        add constraint FK8iv95flu18sgqqw5v8d8c55kf 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.addressplantillassettings 
        add constraint FKq9e8gafjw6cracvngwbldyfxl 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.addresssettingsforallocatedcompany 
        add constraint FK3uu92vail6fnm33xsseh1fca7 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.addresssettingsforallocatedsubdiv 
        add constraint FK38bspic8niroracrkolk57bc 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.asset 
        add constraint FK44jnocx0i698v28pphddbd6ef 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.assetnote 
        add constraint FKnmdayov00usr2kmfio9ura87s 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.bankaccount 
        add constraint FKmifiyst1ub3gss1q3ji8mm67b 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.bankdetail 
        add constraint FK7mv8b99pl8edx1kn0jmn3d6wp 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.baseinstruction 
        add constraint FKpcu2n5oehsxutwywpclmajp0w 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.batchcalibration 
        add constraint FKomiu1uqmoel83hwso64qcrox6 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.bpo 
        add constraint FKd0cjn0ym5myj1wpbxrfy6v15y 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.businessarea 
        add constraint FKr16slxusxjcbwd05js0k3qe54 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.businesscompanysettings 
        add constraint FK6b8i550gtav56gi5h7mwm068w 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.businessemails 
        add constraint FKpcwo30jd4akm12w5vsrwq9epr 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.businesssubdivisionsettings 
        add constraint FKtbcjwcl1gc8k3n4qiskvuayn5 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.calibration 
        add constraint FKlopdpiv8lvposb9ymlri9k03u 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.calibrationpoint 
        add constraint FKbrub39phpixuwea7qybvi84fx 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.calibrationpointset 
        add constraint FKkvjg33tvan8vtgxc6jv0rinws 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.callink 
        add constraint FKmk3wvfghgcaysee0dq3nbxvbg 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.calloffitem 
        add constraint FKl022gqhc572n8cjv2syw4xum0 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.calreq 
        add constraint FKo67u03mb3tgfmx92sdvi6k3t7 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.catalogprice 
        add constraint FK4itvwwth1l0hfv001uvfg5x3 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.certificate 
        add constraint FK8byxtlj2pv9vo0spf59jawe23 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.certificatevalidation 
        add constraint FKm4fo0h8y7lwgs5fo3114rssqs 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.certlink 
        add constraint FKevx2587ih2k7it76svs34rcj 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.certnote 
        add constraint FK3a5loi7wouu98tfv678oscd6b 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.characteristicdescription 
        add constraint FKn2jt4duciqfxj6k12y9lbl4ch 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.collectedinstrument 
        add constraint FKaber48pfr10m63mbxfcy4enkm 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.company 
        add constraint FKedcg2mnmj4nyme2dm0g67i23s 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.companyaccount 
        add constraint FKr5cor3klgxq88hxeqirvhrp3 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.companyaccreditation 
        add constraint FKwo87e0os2q3ev76q4g4094 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.companyinstructionlink 
        add constraint FKe1o5u7jc7na9d23e87vhhmac3 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.companysettingsforallocatedcompany 
        add constraint FK17t7leqq00ws8mv98t4qfy9ei 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.component 
        add constraint FKh5sf3skl3co8i6sajaybxgel5 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.contact 
        add constraint FK3kad6d3ed0lnq6phox0xim0s5 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.contactinstructionlink 
        add constraint FKqgkadp0im9a2lh986m375iixj 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.contractreview 
        add constraint FKgohq8xlpjjnkwdhmkskfr006o 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.contractreviewitem 
        add constraint FKdtcq80hmayndxy9bhpfa1pw98 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.costs_adjustment 
        add constraint FK746he5wxjc3irwbqykiidfkvm 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.costs_calibration 
        add constraint FKi9qfyx10ehdfqdgj2qwndp4di 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.costs_inspection 
        add constraint FKnlsq5btdytidg1yusgge8avs2 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.costs_purchase 
        add constraint FKnnw8ks3p4cqjmxsx0sxahujtv 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.costs_repair 
        add constraint FK8hp17pbtj9rl96brv1fax1a2w 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.courier 
        add constraint FKe7xp84h4j30avs51kynl825w4 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.courierdespatch 
        add constraint FKjyq2kn37bafk1e67rfuy033x6 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.courierdespatchtype 
        add constraint FKnlu0l2mdrwq5bi2fjdkfulmki 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.creditcard 
        add constraint FKj15q0rvfhay406nfumgv8hw3h 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.creditnote 
        add constraint FKo4hxcq78jftxyg3x7914r57nh 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.creditnoteitem 
        add constraint FKs94m9kdnqskxhw60w8y2k60dt 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.creditnotenote 
        add constraint FKgxwyoq45qgq4kfhljow564fkh 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.customquotationcalibrationcondition 
        add constraint FK7cxtao1mua2a9vga79i55am9p 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.customquotationgeneralcondition 
        add constraint FK92hiauapsdm6aiw4sbi0u6yko 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.defaultnote 
        add constraint FKsh51pd80r8x50wd9jmtiplgkg 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.defaultquotationcalibrationcondition 
        add constraint FKqth7pbjqdpey7a1fkvv388w14 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.defaultquotationgeneralcondition 
        add constraint FKf69pylf3cpg7mikrehxxmnv9r 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.defaultstandard 
        add constraint FK4w4wpfxh3p04csg4a9mu6uucf 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.defect 
        add constraint FK7118uuq1wncbt0sv8cqjwjut3 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.delivery 
        add constraint FK8xmds9dmg2a8asgqakv2nt7bx 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.deliveryitem 
        add constraint FKr0ve9gbindogi83ffvmuinlj4 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.deliveryitemnote 
        add constraint FK8keumasj381fhbs6atld0p0t0 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.deliverynote 
        add constraint FK20s12mcfuj3cxo400lnuh9yw 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.departmentmember 
        add constraint FK4ix5q4pmmmjvk6cge15s90v5o 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.description 
        add constraint FK966k1ttukmbafkcovakfp0dlx 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.dimensional_thread 
        add constraint FKq7pormd34tbyq7rx4n67w769n 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.email 
        add constraint FKry3b6m1ckmr2oopaylvnggrw5 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.emailrecipient 
        add constraint FK3g4y7kokc8iw5igjlcfjanpiq 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.emailtemplate 
        add constraint FKpn3jdsl7dnixdsh96y2rlg1x4 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.faultreport 
        add constraint FK2o3cnk8o691cfybieeva63np2 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.faultreportaction 
        add constraint FKf3idqsmdcp63y2v1yi1h9n4ys 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.faultreportdescription 
        add constraint FKkrt8l5batm35dsrl2hwdylv5h 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.faultreportinstruction 
        add constraint FKhb40k1nd29osttsk5iso1djtn 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.freehandcontact 
        add constraint FK7utp6dbx7wkx2tb4kw5nrpyha 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hire 
        add constraint FKoasavt346amg8men3gnirnwsg 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hireaccessory 
        add constraint FKlgrogcufopdgil6fe41jl8y2t 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hirecrossitem 
        add constraint FK2ctc8dppahmttftt4tj9sn7gn 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hireinstrument 
        add constraint FK90sn4kb9mpax8tp511vflncp0 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hireitem 
        add constraint FK2ll8o9vd8hv413ep9o4ttmcvi 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hireitemaccessory 
        add constraint FK5kcrumviamut9mg6tsj5o4aj4 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hiremodel 
        add constraint FKt1r7ko6d1y1t59nmb52ql6gde 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hirenote 
        add constraint FKpyi3oh7w6o4erpctulq63ir0l 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hirereplacedinstrument 
        add constraint FKha3y14i5leui8wwq2qhb8elg8 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.hiresuspenditem 
        add constraint FKawiqfk915wdaj122idfcvkhgm 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.images 
        add constraint FKhg63kxlxnnffcxjssvvlcmkxq 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.imagetag 
        add constraint FKntc2ot8ec3157l0hhogk9cobw 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instcertlink 
        add constraint FK2eol97qjqdlui2tss360nyo02 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instmodel 
        add constraint FKr510p9frto3astvp50l62id9v 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instmodeldomain 
        add constraint FKo8vg79nxus6ubu0vl42xfmx2u 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instmodelfamily 
        add constraint FKp0buy4s5cefdtpcnaurxvpdsn 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instmodelsubfamily 
        add constraint FK1d6fml6rhx97gx7gq6bp5468e 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instrument 
        add constraint FK66670ov2xlp4wdsqfcpfhb5yl 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instrumentnote 
        add constraint FK1b90ji8axy3l61bhyaorkxf9h 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instrumentvalidationaction 
        add constraint FK6331pd6mucgr242j4nnf0q4jn 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.instrumentvalidationissue 
        add constraint FK24hjvupb8u3kmyf4vysbn6x1g 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.invoice 
        add constraint FKsf0jibgboq58yvwnrcrx0pgfy 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.invoiceitem 
        add constraint FKgvxwn30m4wlxtro5f8d16v6vu 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.invoiceitempo 
        add constraint FKhvupeyua129cyqdwduhbq33md 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.invoicejoblink 
        add constraint FKrxr3fv06pufn6ef50bjwjt2lm 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.invoicenote 
        add constraint FK2o9bgl7cpxg22bxkbm5ky8iic 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.invoicepo 
        add constraint FKod329lud26jscvil9okaqeoel 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.job 
        add constraint FKbksy7pakg8sk0xm16bhnyg77n 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobcosting 
        add constraint FKfg76i1yv6qr33fas2tdo1mddt 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobcostingitem 
        add constraint FKkjixn0vd90w5xvvlsg54j7dfq 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobcostingnote 
        add constraint FK1q7wtmj9pcmf9l4pqpnt8pdre 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobinstructionlink 
        add constraint FKm11tvyno3n8yyhgp32s0fhwtp 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobitem 
        add constraint FKt3nas6q5jn1u7u81q7fhd1boj 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobitemaction 
        add constraint FKbqv8i3w98j6rgpw3th2x6dh3g 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobitemgroup 
        add constraint FK6uwqcvicua9c5cto2q46lyyyp 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobitemnote 
        add constraint FK42x27ytm86wd8dl687s6eds1l 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobitempo 
        add constraint FKk5susg22yrkhkwu9bue54eaf7 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobitemworkrequirement 
        add constraint FKsglm1lqmbwtsehkt1l8gbgjuc 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobnote 
        add constraint FKow2w7yeiqycxyq4whsdscqmg0 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobquotelink 
        add constraint FKqm5m0xcrdxk97i4ads0lgagon 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.location 
        add constraint FK9tcnu4lcng9djp8v6p8si5vu9 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.mailgroupmember 
        add constraint FK27wolg4oy31al6hikdeul0kjj 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.mfr 
        add constraint FKlk0jslabjmloh34jlj1d32j90 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.modelnote 
        add constraint FK1wlojo6qq1wibw963ysjo2ex4 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.modeloption 
        add constraint FK6qshr893kf13ova1u7oku15of 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.modelpartof 
        add constraint FKa7r6cbjcx9na1ek2nn5tir47w 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.modelrange 
        add constraint FK4jx5vpkd6vdc2kp42l8n3nk64 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.nominalcode 
        add constraint FKbdl23gr791koa478ntqc3pfxt 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.numberformat 
        add constraint FKo7kiiuipg9jfdd6s3jl6vh0e4 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.numeration 
        add constraint FKeyi8ep8hkjc0iru21c4qwxqmm 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.onbehalfitem 
        add constraint FK3kdre72is3nayhkxqe0h0s8xf 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.pat 
        add constraint FK8tfwr81tb00huxobo41l7jxl4 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.po 
        add constraint FK2bupe3en8oyja62lwncdmoxw4 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.pointsettemplate 
        add constraint FKsd0snewdf4c72gwmy5jlpwmlf 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.presetcomment 
        add constraint FKi2tlp76j2v3lkxfm7uy243ar0 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.procedureaccreditation 
        add constraint FK6sn8f767qpc6dr8onm5arsc9v 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.procedureheading 
        add constraint FKon0deqosx2otj9bcjjqhnqcv1 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.procs 
        add constraint FK2aefsw296k09p5ep5mnhb17c 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.purchaseorder 
        add constraint FKagccm16f47m62h1de9jhsm09h 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.purchaseorderitem 
        add constraint FKqtqagywdx2vg2ea1r0rqc02v1 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.purchaseorderitemnote 
        add constraint FK55o9am8fi17iq2eb7abajxru 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.purchaseorderjobitem 
        add constraint FK4fyj80n41cnrddeu85mu2go8d 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.purchaseordernote 
        add constraint FKhdv1dl35gx6awx37wfr8c9cvp 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.quickpurchaseorder 
        add constraint FK8hp3bjp585t124l2qsuqgx2o9 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.quotation 
        add constraint FK5ve3t15aiivpyoiyb7986hpmx 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.quotationitem 
        add constraint FK87urmwwbmx7g5ed07ygdgbi00 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.quotationrequest 
        add constraint FKanajuui2pwvn150v2dtumop4u 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.quoteheading 
        add constraint FKqqtpeh6mjhm1x3yrcnxcg8gg2 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.quoteitemnote 
        add constraint FK8c8f8mdvafl78mn7aaiwe62na 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.quotenote 
        add constraint FKmewysd3b9wmlu8t8ywdatpt4x 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.recall 
        add constraint FK6swpwhcynyjw8erk4pv5vrg8j 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.recalldetail 
        add constraint FK8otq8fawjx2fsrjtm2hjdg5pg 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.recallitem 
        add constraint FKgaola9icnhmrw7r1rfx5h0ld9 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.recallnote 
        add constraint FKddbxv9v6b04ax9o9cck8baskb 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.recallresponsenote 
        add constraint FKr2o1xk9m0sah4phda1gpjpryg 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.recallrule 
        add constraint FK4v5oeumh0v31rkv1w20hk0i0a 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.repeatschedule 
        add constraint FKdvk2m7y1iakje97adg8776rtp 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.requirement 
        add constraint FKlilb12q4sv3aark3wn1uqe5w5 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.salescategory 
        add constraint FKl5ceqj6smfg2acfigd5fr582n 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.schedule 
        add constraint FKpj51sae8xl41c9tqpb54hg14c 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.scheduledquotationrequest 
        add constraint FKmrlua642kxsk9v632wsou3156 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.scheduleequipment 
        add constraint FKsvlnllajr0sdaime9kujj0pjd 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.schedulenote 
        add constraint FK5at06qo567yi6hg5hpyw7enjo 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.servicecapability 
        add constraint FK58le9aby6hc7l0ql9hg59fssj 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.standardused 
        add constraint FK2x9s1pmj23dgqmypbdy35t8ns 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.subdiv 
        add constraint FKehg3bsf3gtun1nwbngcr42lce 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.subdivinstructionlink 
        add constraint FK4fd3ygdhaxuai3a38j72xobvk 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.supplierinvoice 
        add constraint FKsapl8amu4gmfwaukhaa2t2iho 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.systemdefaultapplication 
        add constraint FKryica0n029gxehfaxfshtv75s 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.taggedimage 
        add constraint FKeffgpc3y0tmvuepkrlca9e2tm 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.templatepoint 
        add constraint FK9spkful054cgtwn51gtphr9dh 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpinstruction 
        add constraint FK9nvhohvyhm0v5eftnqvya8h9y 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquotation 
        add constraint FKt88pakcx745hnx4xbu04k9wha 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquotationitem 
        add constraint FKebvfpxofgnh9rlwq92efmqw1v 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquoteitemnote 
        add constraint FKo6lbfd5vmp1tyaaxq1677g0t7 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquotelink 
        add constraint FK7c4vtu4tk5gtsvw1bai9r0091 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquotenote 
        add constraint FK1ulppxm54drvw5xtfr9p0on39 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquoterequest 
        add constraint FKtqmrhckhkg3eensyi9oi4lrg8 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquoterequestitem 
        add constraint FKb35p3tut675slmmr5ugb99cfj 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquoterequestitemnote 
        add constraint FKc3astawd76fkej9lbugoic462 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tpquoterequestnote 
        add constraint FK9d4i9se4cgaqc7e54k13anng2 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.tprequirement 
        add constraint FKcskty3ew9esvh2fe7vyd5y70e 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.transportmethod 
        add constraint FKacovs9g3qtqg2smbf25cmexc2 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.transportoption 
        add constraint FKsg1hyancv8fesyyam42wrtjp 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.uom 
        add constraint FKlpuxdse6f57xrdxos7b19p2g3 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.upcomingwork 
        add constraint FKm8voa1jm4p8fp7y2gok4k661o 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.user_role 
        add constraint FK1ry4vlhxmsioql9j37rbm9wrw 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.userpreferences 
        add constraint FKr13r7pbqhakkan7iplkgrxuu9 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.users 
        add constraint FK741ovu7dub0o3fot3py3i3a8h 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.vatrate 
        add constraint FKrti9lpmdowuccqsh2o9mo2j4v 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.webresource 
        add constraint FKqmqbch1h9yfeuji027qovm0e5 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.workinstruction 
        add constraint FKwgt2taoy68x9ikvaj683t699 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.workinstructionversion 
        add constraint FKtofp8r7bs5baba8cdf4i2k1lf 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.workrequirement 
        add constraint FK4tmomif9xjhuspmhcgb41syvl 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.workrequirementprocedure 
        add constraint FKjhwmvokfys1axpmythlbxytal 
        foreign key (lastModifiedBy) 
        references dbo.contact;

COMMIT TRAN