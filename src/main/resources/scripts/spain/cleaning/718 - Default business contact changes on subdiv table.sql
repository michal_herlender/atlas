USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.businesssubdivisionsettings ADD defaultbusinesscontact INT;

GO

ALTER TABLE dbo.businesssubdivisionsettings ADD CONSTRAINT 
FK_businesssubdivsettings_contact FOREIGN KEY(defaultbusinesscontact) 
REFERENCES dbo.contact(personid);

UPDATE dbo.businesssubdivisionsettings 
SET defaultbusinesscontact = CASE WHEN s.defpersonid IS NOT NULL 
THEN s.defpersonid ELSE c.defaultbusinesscontact END 
FROM dbo.businesssubdivisionsettings bss JOIN dbo.subdiv s 
ON bss.subdivid = s.subdivid JOIN company c ON s.coid = c.coid
WHERE bss.id = id;

ALTER TABLE dbo.businesssubdivisionsettings 
ALTER COLUMN defaultbusinesscontact INT NOT NULL;

DECLARE @constraintName VARCHAR(50);
DECLARE @runString VARCHAR(2000);

SELECT @constraintName = CONSTRAINT_NAME
FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE
WHERE TABLE_NAME = 'subdiv' AND COLUMN_NAME = 'defaultbusinesscontact';

EXECUTE ('ALTER TABLE [dbo].[subdiv] DROP CONSTRAINT ' + @constraintName);

ALTER TABLE dbo.subdiv DROP COLUMN defaultbusinesscontact;

INSERT INTO dbversion VALUES (718)

COMMIT TRAN