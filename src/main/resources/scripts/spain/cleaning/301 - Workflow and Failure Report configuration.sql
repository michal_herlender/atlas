-- Script 298 - Configuration
-- Should be successfully run BEFORE running the updateWorkflow command in the Admin menu
-- August 2018 - Ahmed Yassine Boutahar, Galen Beck (lookup renaming only)

USE [spain]

BEGIN TRAN
	-- Renames the Lookup instances to have unique (and more identifiable) names
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemAtFirstLocation (1 inactive)' WHERE id = 1;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasBeenContractReviewed (inhouse goods in)' WHERE id = 2;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_COSHHFormRequired (inhouse bypass contract review)' WHERE id = 3;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ContractReviewOutcome (inhouse after contract review)' WHERE id = 4;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_COSHHFormRequired (inhouse after contract review)' WHERE id = 5;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CleaningRequired (inhouse after contract review or bypass)' WHERE id = 6;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CleaningRequired (inhouse after coshh form)' WHERE id = 7;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ContractReviewOutcome (inhouse bypass contract review)' WHERE id = 8;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ContractReviewOutcome (inhouse bypass contract review after cleaning)' WHERE id = 9;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_InitialWorkRequired (inhouse after contract review or bypass)' WHERE id = 10;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_InitialWorkRequired (inhouse after initial fault report)' WHERE id = 11;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_InitialWorkRequired (inhouse after initial engineer inspection)' WHERE id = 12;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CalibrationInspectionOutcome (inhouse after engineer inspection)' WHERE id = 13;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasFaultReport (inhouse after engineer inspection)' WHERE id = 14;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_PostCalibration (inhouse after lab calibration)' WHERE id = 15;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasFaultReport (inhouse after calibration failure)' WHERE id = 16;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CertificateIsSigned (after lab calibration)' WHERE id = 17;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting (orifice plate)' WHERE id = 18;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ConfirmationCostRequired (orifice plate)' WHERE id = 20;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CostsWithinApprovedLimit (21 inactive)' WHERE id = 21;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_LatestCalibrationOrInspectionOutcome (23 inactive)' WHERE id = 23;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_LatestCalibrationOrInspectionOutcome (24 inactive)' WHERE id = 24;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_LatestCalibrationOrInspectionOutcome (25 inactive)' WHERE id = 25;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (inhouse after repair/adjustment costing)' WHERE id = 26;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemAtDespatchLocation (inhouse after delivery note creation)' WHERE id = 27;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemAtDespatchLocation (inhouse after despatch to business subdivision)' WHERE id = 28;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_DespatchApprovalRequired (inhouse after delivery note creation)' WHERE id = 29;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_RepairOutcome (inhouse after repair)' WHERE id = 30;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ConfirmationCostRequired (inhouse after successful repair)' WHERE id = 31;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ComponentsFittedOutcome (inhouse after repair components fitted)' WHERE id = 32;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_AdjustmentOutcome (inhouse after adjustment)' WHERE id = 33;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasBeenCalibrated (inhouse after adjustment)' WHERE id = 34;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasFaultReport (inhouse after adjustment)' WHERE id = 35;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_AsFoundCertificates (inhouse after adjustment)' WHERE id = 36;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (inhouse after unable to adjust, onto third party)' WHERE id = 37;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (inhouse after adjustment, decision on as found certificate)' WHERE id = 38;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CostsWithinApprovedLimit (inhouse after unable to adjust, onto inhouse repair 39)' WHERE id = 39;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CostsWithinApprovedLimit (duplicate of 39)' WHERE id = 40;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (inhouse after unable to adjust, decision on costing)' WHERE id = 41;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_AdjustmentOutcome (inhouse after unable to adjust, after decision on costing)' WHERE id = 42;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_AdjustmentOutcome (duplicate of 42)' WHERE id = 43;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_TPRequirements (third party after requirements recorded)' WHERE id = 44;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (third party after inspection costing to client)' WHERE id = 45;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_TPRequirements (third party return for repair)' WHERE id = 46;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (third party after job costing to client)' WHERE id = 48;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemAtTP (third party after client decision)' WHERE id = 49;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemAtTP (third party after PO creation)' WHERE id = 50;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_FurtherTPWorkClientResponse (third party after client decision for further work)' WHERE id = 51;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemCalibratedAtTP (inhouse after third party work)' WHERE id = 52;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_TPCalibrationOutcome (inhouse after third party work)' WHERE id = 53;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_PostTPInspectionOutcome (inhouse after third party work)' WHERE id = 54;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_UnitHasBeenExchanged (inhouse after third party work, broken unit)' WHERE id = 55;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (third party after client decision for exchange)' WHERE id = 56;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_PostCalibration (58 inactive)' WHERE id = 58;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_DespatchApprovalRequired (inhouse return to client)' WHERE id = 59;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemAtDespatchLocation (inhouse despatch approval)' WHERE id = 60;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CompanyIsOnStop (inhouse return to client from this subdivision)' WHERE id = 61;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemAtCalibrationLocation (before lab calibration)' WHERE id = 62;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CertIssuedFromCal (after lab calibration)' WHERE id = 63;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_TypeOfLatestCalibration (after lab calibration on hold)' WHERE id = 64;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemRequiresFurtherCalibration (after lab calibration)' WHERE id = 65;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (inhouse orifice plate)' WHERE id = 66;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemIsOrificePlate (after lab calibration)' WHERE id = 67;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemAtInspectionLocation (inhouse orifice plate)' WHERE id = 68;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasFaultReport (inhouse BER after engineer inspection)' WHERE id = 69;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_FaultReportAwaitingUpdate (inhouse after engineer inspection)' WHERE id = 70;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_TPItemReturned (third party return for repair)' WHERE id = 71;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_JobIsOnsite (goods in)' WHERE id = 72;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ContractReviewOutcome (onsite)' WHERE id = 73;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_PostCalibration (onsite after calibration)' WHERE id = 74;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CertIssuedFromCal (onsite after calibration)' WHERE id = 75;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CertificateIsSigned (onsite after calibration)' WHERE id = 76;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemRequiresFurtherCalibration (onsite after calibration)' WHERE id = 77;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_PreDeliveryRequirements (before lab calibration)' WHERE id = 78;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_PreDeliveryRequirements (after lab calibration - before costing check)' WHERE id = 79;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_PreDeliveryRequirements (after lab calibration - after costing check)' WHERE id = 80;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasBeenCalibrated (onsite)' WHERE id = 81;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_AdjustmentOutcome (onsite)' WHERE id = 82;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientDecision (onsite)' WHERE id = 83;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_LatestCalibrationOrInspectionOutcome (onsite - approved for adjustment)' WHERE id = 84;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_LatestCalibrationOrInspectionOutcome (onsite - needs approval for adjustment)' WHERE id = 85;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CostsWithinApprovedLimit (onsite after fault report)' WHERE id = 86;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_PostCalibration (onsite after fault report)' WHERE id = 87;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_FaultReportAwaitingUpdate (onsite)' WHERE id = 88;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ItemHasFaultReport (onsite)' WHERE id = 89;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_NextWorkRequirementByJobCompany (before lab calibration)' WHERE id = 90;
	-- These last 5 IDs will not normally exist yet, including for development / documentation purposes
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_FaultReportIsTobeSent (inhouse fault report)' WHERE id = 91;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_ClientResponseRequiredOnFaultReport (inhouse fault report)' WHERE id = 92;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_LatestFaultReportOutcome (inhouse fault report)' WHERE id = 93;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_CostsWithinApprovedLimit (inhouse fault report)' WHERE id = 94;
	UPDATE [dbo].[lookupinstance] SET [lookup] = 'Lookup_LatestFaultReportOutcome (inhouse repair or adjustment)' WHERE id = 95;

	GO

	-- // For Activity : 26 - Pre-Calibration
	--create actionoutcome
	SET IDENTITY_INSERT dbo.actionoutcome ON
	go
	-- // For Activity : 26 - Pre-Calibration
	--create actionoutcome
	INSERT INTO dbo.actionoutcome(id,defaultoutcome,description, positiveoutcome,activityid,active,type,value)
		VALUES(135, 0, 'Not Successful', 0, 26, 1, 'calibration', 'NOT_SUCCESSFUL');
	-- translation
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
		VALUES (135,'en_GB','Not Successful') 
	go
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
		VALUES (135,'es_ES','No exitoso') 
	go
	INSERT INTO dbo.actionoutcometranslation (id,locale,[translation])
		VALUES (135,'fr_FR','Pas de succès') 
	go
	SET IDENTITY_INSERT dbo.actionoutcome OFF
	go

	--deactivate other actionouctomes (KEEP JUST: SUCCESSFUL, ONHOLD, NOT SUCCESSFUL)
	UPDATE dbo.actionoutcome SET active=0 WHERE id=21 ;
	UPDATE dbo.actionoutcome SET active=0 WHERE id=22 ;
	UPDATE dbo.actionoutcome SET active=0 WHERE id=23 ;
	UPDATE dbo.actionoutcome SET active=0 WHERE id=24 ;
	UPDATE dbo.actionoutcome SET active=0 WHERE id=79 ;
	UPDATE dbo.actionoutcome SET active=0 WHERE id=80 ;
	UPDATE dbo.actionoutcome SET active=0 WHERE id=81 ;
	UPDATE dbo.actionoutcome SET active=0 WHERE id=82 ;

	--create outcomestatus for not successful : link to lookup : Lookup_FaultReportAwaitingUpdate
	-- Note - no need to create, existing outcome status 44 does what we need (connects to lookup 16)

	-- create lookupresult for Lookup 15 : Lookup_PostCalibration, link to existing outcome status 44 (connects to lookup 16)
	-- Commented out id, used select due to key constraint issue (not same everywhere)
	INSERT INTO dbo.lookupresult ([result],activityid,lookupid,outcomestatusid,resultmessage) VALUES ('k',26,15,44,10);
	GO

	-- new failure report fields
	ALTER TABLE dbo.faultreport ADD estdeliverytime int
	ALTER TABLE dbo.faultreport ADD approvaltype varchar(100) 
	ALTER TABLE dbo.faultreport ADD clientapproval bit
	ALTER TABLE dbo.faultreport ADD clientapprovalbyid int
	ALTER TABLE dbo.faultreport ADD clientapprovalon datetime
	ALTER TABLE dbo.faultreport ADD clientcomments varchar(200)
	ALTER TABLE dbo.faultreport ADD operationbytrescal bit
	ALTER TABLE dbo.faultreport ADD operationinlaboratory bit
	ALTER TABLE dbo.faultreport ADD state varchar(100)
	ALTER TABLE dbo.faultreport ADD recommendation varchar(100)
	ALTER TABLE dbo.faultreport ADD version varchar(100) 
	ALTER TABLE dbo.faultreport ADD techniciancomments varchar(200) 
	ALTER TABLE dbo.faultreport ADD issuedate datetime
	ALTER TABLE dbo.faultreport ADD technicianid int
	ALTER TABLE dbo.faultreport ADD managervalidation bit
	ALTER TABLE dbo.faultreport ADD clientresponseneeded bit
	ALTER TABLE dbo.faultreport ADD managervalidationbyid int
	ALTER TABLE dbo.faultreport ADD managervalidationon datetime
	ALTER TABLE dbo.faultreport ADD managercomments varchar(200)
	ALTER TABLE dbo.faultreport ADD otherstate varchar(100)
	ALTER TABLE dbo.faultreport ADD trescalref varchar(100)
	--add field to record the client response if it different from the fr recommmendation
	ALTER TABLE dbo.faultreport ADD clientoutcome varchar(100) 
	go
	
	-- FK
	ALTER TABLE dbo.faultreport ADD CONSTRAINT FK_faultreport_clientapprovalbyid FOREIGN KEY (clientapprovalbyid) REFERENCES dbo.contact(personid) 
	ALTER TABLE dbo.faultreport ADD CONSTRAINT FK_faultreport_technicianid FOREIGN KEY (technicianid) REFERENCES dbo.contact(personid) 
	ALTER TABLE dbo.faultreport ADD CONSTRAINT FK_faultreport_managervalidationbyid FOREIGN KEY (managervalidationbyid) REFERENCES dbo.contact(personid) 
	go
	
	--failure report - itemaccessory link
	ALTER TABLE dbo.itemaccessory ADD faultreportid int
	go
	
	-- FK
	ALTER TABLE dbo.itemaccessory ADD CONSTRAINT FK_itemaccessory_faultreportid FOREIGN KEY (faultreportid) REFERENCES dbo.faultreport(faultrepid) 
	go

		--//** setting to whether not to sent the fault report or let the CSR decide
	SET IDENTITY_INSERT [systemdefaultgroup] ON
	GO

	INSERT INTO [systemdefaultgroup]
	(id, description, dname)
	VALUES (13, 'Fault report settings', 'Fault report settings')

	INSERT INTO [systemdefaultgroupdescriptiontranslation]
	(id, locale, translation)
	VALUES
		(13, 'en_GB', 'Fault report settings'),
		(13, 'fr_FR', 'Paramètres du constat d''anomalie');

	INSERT INTO [systemdefaultgroupnametranslation]
	(id, locale, translation)
	VALUES
		(13, 'en_GB', 'Fault report settings'),
		(13, 'de_DE', 'Paramètres du constat d''anomalie');
	GO

	SET IDENTITY_INSERT [systemdefaultgroup] OFF
	GO

	SET IDENTITY_INSERT [systemdefault] ON
	GO

	INSERT INTO [systemdefault]
	([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
	VALUES
		(50, 'other', 'Defines whether to let the CSR decides to send the fault report or not to send it at all', 'Send Fault report to client', 'CSR_DECISION', 0, 0, 13, 1);
	GO

	SET IDENTITY_INSERT [systemdefault] OFF
	GO

	INSERT INTO [systemdefaultdescriptiontranslation]
	([defaultid], [locale], [translation])
	VALUES
	  (50, 'en_GB', 'Defines whether to let the CSR decides to send the fault report or not to send it at all'),
	  (50, 'es_ES', 'Define si dejar que el CSR decida enviar el informe de fallas o no enviarlo en absoluto'),
	  (50, 'fr_FR', 'Définit si le CRC décide d''envoyer le constat d''anomalie ou de ne pas l''envoyer du tout'),
	  (50, 'de_DE', 'Definiert, ob der CSR entscheiden soll, den Fehlerbericht zu senden oder überhaupt nicht zu senden');

	INSERT INTO [dbo].[systemdefaultscope]
	([scope], [defaultid])
	VALUES
	  (0,50);
	GO

	INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
	  (1, 50);

	INSERT INTO [dbo].[systemdefaultnametranslation]
	([defaultid], [locale], [translation])
	VALUES
	  (35, 'en_GB', 'Send Fault report to client'),
	  (35, 'es_ES', 'Enviar informe de fallas al cliente'),
	  (35, 'fr_FR', 'Envoyer le ocnstat d''anomalie au client'),
	  (35, 'de_DE', 'Senden Sie den Fehlerbericht an den Client');
	GO

	INSERT INTO dbversion(version) VALUES(301);

COMMIT TRAN