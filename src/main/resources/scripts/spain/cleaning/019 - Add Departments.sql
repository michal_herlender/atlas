/* Add Departments for Zaragoza - Tony Provost 2016-03-07 */

BEGIN TRAN 

USE [SPAIN];
GO

IF NOT EXISTS (SELECT * FROM [dbo].[department] WHERE [subdivid] = 6644 AND lower([name]) = lower('Calidad'))
	INSERT INTO [dbo].[department] ([name], [addrid], [subdivid], [typeid], [shortname])
	VALUES ('Calidad', 11758, 6644, 1, NULL)
IF NOT EXISTS (SELECT * FROM [dbo].[department] WHERE [subdivid] = 6644 AND lower([name]) = lower('Técnico'))
	INSERT INTO [dbo].[department] ([name], [addrid], [subdivid], [typeid], [shortname])
	VALUES ('Técnico', 11758, 6644, 3, NULL)
IF NOT EXISTS (SELECT * FROM [dbo].[department] WHERE [subdivid] = 6644 AND lower([name]) = lower('Laboratorio'))	
	INSERT INTO [dbo].[department] ([name], [addrid], [subdivid], [typeid], [shortname])
	VALUES ('Laboratorio', 11758, 6644, 3, NULL)
IF NOT EXISTS (SELECT * FROM [dbo].[department] WHERE [subdivid] = 6644 AND lower([name]) = lower('Comercial'))
	INSERT INTO [dbo].[department] ([name], [addrid], [subdivid], [typeid], [shortname])
	VALUES ('Comercial', 11758, 6644, 1, NULL)
IF NOT EXISTS (SELECT * FROM [dbo].[department] WHERE [subdivid] = 6644 AND lower([name]) = lower('Compras'))	
	INSERT INTO [dbo].[department] ([name], [addrid], [subdivid], [typeid], [shortname])
	VALUES ('Compras', 11758, 6644, 4, NULL)
IF NOT EXISTS (SELECT * FROM [dbo].[department] WHERE [subdivid] = 6644 AND lower([name]) = lower('Finanzas/Administración'))
	INSERT INTO [dbo].[department] ([name], [addrid], [subdivid], [typeid], [shortname])
	VALUES ('Finanzas/Administración', 11758, 6644, 2, NULL)
IF NOT EXISTS (SELECT * FROM [dbo].[department] WHERE [subdivid] = 6644 AND lower([name]) = lower('Informática'))	
	INSERT INTO [dbo].[department] ([name], [addrid], [subdivid], [typeid], [shortname])
	VALUES ('Informática', 11758, 6644, 5, NULL)
IF NOT EXISTS (SELECT * FROM [dbo].[department] WHERE [subdivid] = 6644 AND lower([name]) = lower('Almacén'))
	INSERT INTO [dbo].[department] ([name], [addrid], [subdivid], [typeid], [shortname])
	VALUES ('Almacén', 11758, 6644, 4, NULL)
GO

ROLLBACK TRAN