USE spain;

BEGIN TRAN

ALTER TABLE dbo.faultreport ALTER COLUMN techniciancomments VARCHAR (2000);

INSERT INTO dbversion (version) VALUES (439);

COMMIT TRAN