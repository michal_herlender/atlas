USE [atlas]
GO

BEGIN TRAN

DECLARE @statusid int;
SELECT @statusid = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting repair completion report update (post third party work)';
PRINT '@statusid = '
PRINT @statusid

-- add stategroup 'Awaiting RCR update' to status 'Awaiting repair completion report update (post third party work)'
INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (56,@statusid,'ALLOCATED_SUBDIV');

INSERT INTO dbversion(version) VALUES(511);

COMMIT TRAN