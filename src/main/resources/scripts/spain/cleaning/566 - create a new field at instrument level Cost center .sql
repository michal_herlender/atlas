USE [atlas];

GO

BEGIN TRAN

/* adding Cost center to instrument Table */
ALTER TABLE instrument ADD defaultClientRef VARCHAR (30);

INSERT INTO dbversion(version) VALUES(566);

COMMIT TRAN