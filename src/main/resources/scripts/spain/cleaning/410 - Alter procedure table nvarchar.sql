-- Changing description of capability / procedure to accommodate special characters
-- Adds text field to recall company configuration with custom text

-- Galen Beck - 2019-03-11

USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.procs ALTER COLUMN description nvarchar(2000);
GO

alter table dbo.recallcompanyconfiguration 
    add customtext nvarchar(2000);
GO

UPDATE dbo.recallcompanyconfiguration SET customtext = ''
	WHERE customtext is null;
GO

alter table dbo.recallcompanyconfiguration 
    alter column customtext nvarchar(2000) not null;
GO

-- Add missing constraint

alter table dbo.businessdocumentsettings 
   add constraint FK_businessdocsettings_businesscompany 
   foreign key (businesscompanyid) 
   references dbo.company;
GO

INSERT INTO dbversion(version) VALUES(410)
GO

COMMIT TRAN