BEGIN TRAN


create table dbo.po_origin
(
    type           varchar(31) not null,
    id             bigint identity not null,
    lastModified   datetime2,
    lastModifiedBy int,
    jobid          int,
    poId           int,
    primary key (id)
);
alter table dbo.po_origin
    add quoteId int;

alter table dbo.po_origin
    add constraint FKdrycse79rqrdnodrgsthovi2e
        foreign key (quoteId)
            references dbo.tpquotation;
alter table dbo.purchaseorder
    add poOriginId bigint;
alter table dbo.po_origin
    add constraint FK19t0ktfcpm33c030svo2bs2wn
        foreign key (jobid)
            references dbo.job;

alter table dbo.po_origin
    add constraint FKtqtmo4h7ohc1safnegilgf6kd
        foreign key (poId)
            references dbo.purchaseorder;

alter table dbo.purchaseorder
    add constraint FK6obuimlh9bo57kb1gaybx90jd
        foreign key (poOriginId)
            references dbo.po_origin;

alter table dbo.po_origin
    add constraint FK7d59vbjaki4ypqj7reopaarhb
        foreign key (lastModifiedBy)
            references dbo.contact;

INSERT INTO dbversion(version)
VALUES (854);

COMMIT TRAN
