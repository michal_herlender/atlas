USE [atlas]

BEGIN TRAN

ALTER TABLE dbo.instrument
 ALTER COLUMN customerdescription nvarchar(max) COLLATE Latin1_General_CI_AS NULL;

GO

INSERT INTO dbversion(version) VALUES(762);

COMMIT TRAN
