-- Add new Field 'bpoid' for associating the 'PrebookingJobDetails' to 'Bpo' (ticket #1250)

USE [spain]
GO

BEGIN TRAN

	ALTER TABLE dbo.prebookingjobdetails ADD bpoid int;

	ALTER TABLE dbo.prebookingjobdetails ADD CONSTRAINT FK_prebookingjobdetails_poid_bpo FOREIGN KEY(bpoid) REFERENCES dbo.bpo(poid);

	INSERT INTO dbversion(version) VALUES(336);

COMMIT TRAN
