USE [atlas]
GO

BEGIN TRAN

	INSERT INTO dbo.stategrouplink(groupid,stateid,[type]) VALUES(55, 4126, 'ALLOCATED_SUBDIV');
	
	DELETE FROM dbo.advesooverriddenactivitysetting WHERE advesotransferableactivityid IN 
	(SELECT id FROM dbo.advesotransferableactivity WHERE itemactivityid = 4142);
	
	DELETE FROM dbo.advesotransferableactivity WHERE itemactivityid = 4142;
	
	ALTER TABLE dbo.advesotransferableactivity ADD CONSTRAINT advesotransferableactivity_itemstate_fk 
	FOREIGN KEY(itemactivityid) REFERENCES itemstate(stateid);

	INSERT INTO dbversion(version) VALUES(502);

COMMIT TRAN