-- Puts this recently created status into StateGroup 42 (3 - After Onsite) dashboard tab

USE [atlas]

BEGIN TRAN

    DECLARE @stateid_sitejob_issue INT;

	SELECT @stateid_sitejob_issue = stateid FROM dbo.itemstate WHERE [description] = 'On hold - Site job, Repair or Third party issue';

	INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (42
           ,@stateid_sitejob_issue
           ,'ALLOCATED_SUBDIV');

	INSERT INTO dbversion(version) VALUES(664);

COMMIT TRAN