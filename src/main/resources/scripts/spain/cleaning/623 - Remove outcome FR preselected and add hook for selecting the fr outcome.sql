USE [atlas]
GO

BEGIN TRAN

	
---- for in house
DECLARE @luFrOutcomePreSelectedId int;
select @luFrOutcomePreSelectedId = id from lookupinstance loi where loi.lookup = 'Lookup_FailureReportOutcomePreselected (inhouse)';

-- delete outcome statuses
--delete from outcomestatus where id in (select lur.outcomestatusid from lookupresult lur where lur.lookupid = @luFrOutcomePreSelectedId);

-- delete lookupresults
delete from lookupresult where lookupid = @luFrOutcomePreSelectedId;

-- remove lookup from activities
UPDATE itemstate set lookupid=NULL WHERE lookupid = @luFrOutcomePreSelectedId;

-- create outcome statuses
DECLARE @status int, @act1 int, @act2 int;
select @status = ist.stateid from itemstate ist where ist.description = 'Awaiting selection of Fault Report outcome';
select @act1 = ist.stateid from itemstate ist where ist.description = 'Fault Report does not need to be sent';
select @act2 = ist.stateid from itemstate ist where ist.description = 'Client response received from failure report';

INSERT INTO outcomestatus (defaultoutcome,activityid,statusid) VALUES (1,@act1,@status);
INSERT INTO outcomestatus (defaultoutcome,activityid,statusid) VALUES (1,@act2,@status);

-- modify outcomes of lookups that use to go the to be deleted lookup
UPDATE outcomestatus SET lookupid=NULL, statusid=@status WHERE lookupid = @luFrOutcomePreSelectedId;

-- delete lookup results and lookup
delete from lookupresult where lookupid=@luFrOutcomePreSelectedId;

delete from lookupinstance where id =@luFrOutcomePreSelectedId;

---- for onsite
DECLARE @luOnSiteFrOutcomePreSelectedId int;
select @luOnSiteFrOutcomePreSelectedId = id from lookupinstance loi where loi.lookup = 'Lookup_FailureReportOutcomePreselected (onsite)';

-- delete outcome statuses
--delete from outcomestatus where id in (select lur.outcomestatusid from lookupresult lur where lur.lookupid = @luOnSiteFrOutcomePreSelectedId and lur);

-- delete lookupresults
delete from lookupresult where lookupid = @luOnSiteFrOutcomePreSelectedId;

-- remove lookup from activities
UPDATE itemstate set lookupid=NULL WHERE lookupid = @luOnSiteFrOutcomePreSelectedId;

-- create outcome statuses
DECLARE @statusOnsite int, @actOnsite int;
select @statusOnsite = ist.stateid from itemstate ist where ist.description = 'Onsite - awaiting selection of failure report outcome';
select @actOnsite = ist.stateid from itemstate ist where ist.description = 'Onsite - client response received from failure report';

INSERT INTO outcomestatus (defaultoutcome,activityid,statusid) VALUES (1,@actOnsite,@statusOnsite);

-- modify outcomes of lookups that use to go the to be deleted lookup
UPDATE outcomestatus SET lookupid=NULL, statusid=@statusOnsite WHERE lookupid = @luOnSiteFrOutcomePreSelectedId;

-- delete lookup results and lookup
delete from lookupresult where lookupid=@luOnSiteFrOutcomePreSelectedId;

delete from lookupinstance where id =@luOnSiteFrOutcomePreSelectedId;

---- delete deuplicated hooks
delete from hook where name = 'select-fr-final-outcome' or name = 'select-fr-final-outcome-onsite';
	
		
INSERT INTO dbversion(version) VALUES(623);

COMMIT TRAN