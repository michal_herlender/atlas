
USE [atlas];

GO

BEGIN TRAN

DECLARE @repairclientreponse_activity INT;

SELECT @repairclientreponse_activity = stateid FROM dbo.itemstate WHERE [description] = 'Client decision received for repair';
DELETE FROM dbo.stategrouplink WHERE groupid = 55 AND stateid = @repairclientreponse_activity;

INSERT INTO dbversion(version) VALUES(586);

COMMIT TRAN