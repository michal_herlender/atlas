USE [spain]
GO

BEGIN TRAN

ALTER TABLE notificationsystemqueue ALTER COLUMN lasterrormessage varchar(1000);
ALTER TABLE advesojobItemactivityqueue ALTER COLUMN lasterrormessage varchar(1000);

INSERT INTO dbversion(version) VALUES(476);

COMMIT TRAN

