begin TRAN

    alter table dbo.capability_servicetype
       add active bit not null default 1;

    alter table dbo.capability_servicetype
       add description nvarchar(2000);

    alter table dbo.capability_servicetype
       add requirementType varchar(255);

    alter table dbo.capability_servicetype
       add standardTime int;

    alter table dbo.capability_servicetype
       add accreditedlabid int;

    alter table dbo.capability_servicetype
       add calprocesid int;

    alter table dbo.capability_servicetype
       add defworkinstructionid int;

    alter table dbo.capability_servicetype
       add constraint FK_accreditedlab
       foreign key (accreditedlabid)
       references dbo.accreditedlab;

    alter table dbo.capability_servicetype
       add constraint FK_defaultcalibrationprocess
       foreign key (calprocesid)
       references dbo.calibrationprocess;

    alter table dbo.capability_servicetype
       add constraint FK_defaultworkinstruction
       foreign key (defworkinstructionid)
       references dbo.workinstruction;

    create table dbo.categorised_capability_filter (
       id int identity not null,
        category_id int,
        filter_id int,
        primary key (id)
    );

    alter table dbo.categorised_capability_filter
       add constraint FKlvd9pm01d31ilmuspkpwb3v87
       foreign key (category_id)
       references dbo.procedurecategory;

    alter table dbo.categorised_capability_filter
       add constraint FK6pi9fl25owqtg1uor3wuvhvbm
       foreign key (filter_id)
       references dbo.capability_servicetype;

INSERT INTO dbversion(version) VALUES(848);

COMMIT TRAN
