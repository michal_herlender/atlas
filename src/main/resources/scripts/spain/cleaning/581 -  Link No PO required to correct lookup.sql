
USE [atlas];

GO

BEGIN TRAN

DECLARE @lookup_itematcalloc INT,
		@activity_noporequired INT;

SELECT @lookup_itematcalloc = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_ItemAtCalibrationLocation (before lab calibration)';
SELECT @activity_noporequired = stateid FROM dbo.itemstate WHERE [description] = 'No PO required for intercompany work';

UPDATE dbo.itemstate SET lookupid = @lookup_itematcalloc WHERE stateid = @activity_noporequired;

INSERT INTO dbversion(version) VALUES(581);

COMMIT TRAN