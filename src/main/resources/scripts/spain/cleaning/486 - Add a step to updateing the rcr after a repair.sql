-- add a step after finishing repair to update the rcr : adding an activity a status and a hook
USE [atlas]
GO

BEGIN TRAN

declare @stateid int;
declare @activityid int;

-- add status
INSERT INTO dbo.itemstate ([type],active,description,retired) VALUES ('workstatus',1,'Awaiting repair completion report update',0);
select @stateid = max(stateid) from dbo.itemstate;

-- add activity and link it to lookup 30 : repair outcome
INSERT INTO dbo.itemstate ([type],active,description,retired,lookupid) VALUES ('workactivity',1,'Repair completion report updated',0,30);

select @activityid = max(stateid) from dbo.itemstate;


-- next acivity (manual)
INSERT INTO dbo.nextactivity (manualentryallowed,activityid,statusid) VALUES (1,@activityid,@stateid);

-- translations
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@stateid,'de_DE','Warten auf Aktualisierung des Reparaturabschlussberichts')
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@stateid,'en_GB','Awaiting repair completion report update') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@stateid,'es_ES','En espera de actualización de informe de finalización de reparación') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@stateid,'fr_FR','En attente de mise à jour du rapport d''achèvement de la réparation') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid,'de_DE','Reparaturabschlussbericht aktualisiert')
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid,'en_GB','Repair completion report updated') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid,'es_ES','Informe de finalización de reparación actualizado') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid,'fr_FR','Rapport d''achèvement de la réparation mis à jour') 

--add group 'Awaiting RCR update' to the status
INSERT INTO dbo.stategrouplink (groupid,stateid,[type]) VALUES (56,@stateid,'ALLOCATED_SUBDIV');

-- add hook
INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name) VALUES (1,0,1,1,'update-rcr');

declare @hookid int;

select @hookid = max(id) from dbo.hook;

-- add hookactivity
INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)	VALUES (1,1,@activityid,@hookid,@stateid);

declare @lo int;
-- add lookup 'spare parts'
INSERT INTO dbo.lookupinstance (lookup,lookupfunction) VALUES ('Lookup_NeedSpareParts',52);
select @lo = max(id) from dbo.lookupinstance;


declare @os1 int;
declare @os2 int;
declare @os3 int;

-- add outcomestatues
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)	VALUES (1,@stateid);
select @os1 = max(id) from dbo.outcomestatus;
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)	VALUES (0,64);
select @os2 = max(id) from dbo.outcomestatus;
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)	VALUES (0,62);
select @os3 = max(id) from dbo.outcomestatus;

-- add lookupresult
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@lo,@os1,0);
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@lo,@os2,1);
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@lo,@os3,2);

-- link 'repairing' activity to lookup 'needrepair'
UPDATE dbo.itemstate SET lookupid=@lo WHERE stateid=57;

-- link activity 'replacement component received' to status '20 awaiting repair'
UPDATE dbo.outcomestatus SET statusid=20 WHERE id=87;

-- retire unused status and activity
UPDATE dbo.itemstate SET retired=1 WHERE stateid=66;
UPDATE dbo.itemstate SET retired=1 WHERE stateid=67;

-------------------------

-- create the same status and acivity for post tp work
declare @stateid2 int;
declare @activityid2 int;

-- add status
INSERT INTO dbo.itemstate ([type],active,description,retired) VALUES ('workstatus',1,'Awaiting repair completion report update (post third party work)',0);
select @stateid2 = max(stateid) from dbo.itemstate;

-- add same activity but for post tp work
INSERT INTO dbo.itemstate ([type],active,description,retired) VALUES ('workactivity',1,'Repair completion report updated (post third party work)',0);
select @activityid2 = max(stateid) from dbo.itemstate;

-- link the two
INSERT INTO dbo.nextactivity (manualentryallowed,activityid,statusid) VALUES (0,@activityid2,@stateid2);

-- link the activity to status 'awaiting inspection by engineer (post third party work)'
INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,statusid) VALUES (0,@activityid2,132);

-- translations
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@stateid2,'de_DE','Warten auf Aktualisierung des Reparaturabschlussberichts (nach Fremddienstleistung)')
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@stateid2,'en_GB','Awaiting repair completion report update (post third party work)') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@stateid2,'es_ES','En espera de actualización de informe de finalización de reparación (posterior al trabajo)') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@stateid2,'fr_FR','En attente de mise à jour du rapport d''achèvement de la réparation (post sous-traitance)') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid2,'de_DE','Reparaturabschlussbericht aktualisiert (nach Fremddienstleistung)')
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid2,'en_GB','Repair completion report updated (post third party work)') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid2,'es_ES','Informe de finalización de reparación actualizado (posterior al trabajo)') 
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid2,'fr_FR','Rapport d''achèvement de la réparation mis à jour (post sous-traitance)') 

declare @lo2 int;
-- create lookup 'item was on tp for repairs'
INSERT INTO dbo.lookupinstance (lookup,lookupfunction) VALUES ('Lookup_ItemWasAtTPForRepairs',53);
select @lo2 = max(id) from dbo.lookupinstance;

declare @os4 int;
declare @os5 int;

-- create os : one goes to 'awaiting inspection by engineer (post third party work)' the other one to awaiting rcr update
INSERT INTO dbo.outcomestatus (statusid, defaultoutcome) VALUES (132, 0);
select @os4 = max(id) from dbo.outcomestatus;
INSERT INTO dbo.outcomestatus (statusid, defaultoutcome) VALUES (@stateid2, 1);
select @os5 = max(id) from dbo.outcomestatus;

-- create lookup result
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@lo2,@os5,0);
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@lo2,@os4,1);

--after unit received back, if it's not a calibration go to new lookup
UPDATE dbo.outcomestatus SET lookupid=@lo2,statusid=NULL WHERE id=150;

-- with the hook, link the @stateid2 to @activityid2
INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)	VALUES (1,1,@activityid2,@hookid,@stateid2);

-- renaming
UPDATE dbo.lookupinstance SET lookup='Lookup_NeedRepairCompletionReportValidation' WHERE id=109; 

INSERT INTO dbversion(version) VALUES(486);

COMMIT TRAN