USE [atlas]
GO

BEGIN TRAN

alter table dbo.businesscompanysettings add warrantyCalibration int;
alter table dbo.businesscompanysettings add warrantyRepaire int;

GO

update dbo.businesscompanysettings set warrantyCalibration=30;
update dbo.businesscompanysettings set warrantyRepaire=90;

alter table dbo.businesscompanysettings alter column warrantyCalibration int not null;
alter table dbo.businesscompanysettings alter column warrantyRepaire int not null;


alter table dbo.contract add warrantyCalibration int;
alter table dbo.contract add warrantyRepaire int;



DECLARE @groupid int;
DECLARE @defaultid1 int = 64;
DECLARE @defaultid2 int = 65;
-- add a new group for the new system default
INSERT INTO dbo.systemdefaultgroup (description,dname)
	VALUES ('Default Warranty','Business - Default Warranty');
select @groupid = max(g.id) from dbo.systemdefaultgroup g


--translation for system default description
INSERT INTO dbo.systemdefaultgroupdescriptiontranslation (id, locale,[translation])
	VALUES (@groupid, 'en_GB','Default Warranty');
INSERT INTO dbo.systemdefaultgroupdescriptiontranslation (id, locale,[translation])
	VALUES (@groupid, 'fr_FR','Garantie par défaut');
INSERT INTO dbo.systemdefaultgroupdescriptiontranslation (id, locale,[translation])
	VALUES (@groupid, 'es_ES','Garantía predeterminada');


--translation for system default name
INSERT INTO dbo.systemdefaultgroupnametranslation (id, locale,[translation])
	VALUES (@groupid, 'en_GB','Business - Default Warranty');
INSERT INTO dbo.systemdefaultgroupnametranslation (id, locale,[translation])
	VALUES (@groupid, 'fr_FR','Entreprise - Garantie par défaut');
INSERT INTO dbo.systemdefaultgroupnametranslation (id, locale,[translation])
	VALUES (@groupid, 'es_ES','Empresa - Garantía predeterminada');


SET IDENTITY_INSERT [systemdefault] ON;

-- add system default for default service types for standard jobs
INSERT INTO dbo.systemdefault (defaultid,defaultdatatype,endd,start,defaultdescription,defaultname,defaultvalue,groupid,groupwide)
	VALUES (@defaultid1, 'integer',0,0,'Calibration Warranty in Days','Calibration Warranty','-1',@groupid,1);


INSERT INTO dbo.systemdefault (defaultid, defaultdatatype,endd,start,defaultdescription,defaultname,defaultvalue,groupid,groupwide)
	VALUES (@defaultid2, 'integer',0,0,'Repair Warranty In Days','Repair Warranty','-1',@groupid,1);

SET IDENTITY_INSERT [systemdefault] OFF; 

--name translation
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'en_GB','Calibration Warranty');
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'fr_FR','Garantie d étalonnage');
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'es_ES','Garantía de calibración');

INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'en_GB','Repair Warranty');
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'fr_FR','Garantie de réparation');
INSERT INTO dbo.systemdefaultnametranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'es_ES','Garantía de reparación');


-- description translation
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'en_GB','Calibration Warranty in Days');
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'fr_FR','Garantie d étalonnage en jours');
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid1, 'es_ES','Garantía de calibración en días');

INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'en_GB','Repair Warranty in Days');
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'fr_FR','Garantie de réparation en jours');
INSERT INTO dbo.systemdefaultdescriptiontranslation (defaultid, locale,[translation])
	VALUES (@defaultid2, 'es_ES','Garantía de reparación en días');


--scopes
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (1, @defaultid1);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (2, @defaultid1);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (1, @defaultid2);
INSERT INTO dbo.systemdefaultscope ([scope],defaultid)
	VALUES (2, @defaultid2);

--company roles
INSERT INTO dbo.systemdefaultcorole (coroleid,defaultid)
	VALUES (5, @defaultid1),(1, @defaultid1);
INSERT INTO dbo.systemdefaultcorole (coroleid,defaultid)
	VALUES (5, @defaultid2),(1, @defaultid2);


INSERT INTO dbversion VALUES (717)

COMMIT TRAN