USE [atlas]
GO

BEGIN TRAN
	
	DECLARE @OsFrFinalOutcome INT,
			@RepairTpOS INT,
			@statusId INT;

	select @OsFrFinalOutcome = li.id from lookupinstance li 
		where li.lookup = 'Lookup_LatestFailureReportOutcome (for onsite)'; 
	
	select @RepairTpOS = lr.outcomestatusid from lookupresult lr
		where lr.resultmessage = 1 and lr.lookupid = @OsFrFinalOutcome;
		
	select @statusId = iss.stateid from itemstate iss
		where iss.description = 'Recorded onsite service - awaiting job costing'
		
	UPDATE outcomestatus SET statusid=@statusId
		WHERE id=@RepairTpOS;


	INSERT INTO dbversion(version) VALUES (646);

COMMIT TRAN