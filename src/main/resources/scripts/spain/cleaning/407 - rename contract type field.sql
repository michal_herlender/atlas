USE spain
GO

BEGIN TRAN

ALTER TABLE contract ADD invoicingmethod varchar(255)
GO

UPDATE contract SET invoicingmethod = 'PERIODIC' WHERE contracttype = 'PREMIUM'
GO

UPDATE contract SET invoicingmethod = 'INDIVIDUAL' WHERE contracttype = 'PAY_AS_YOU_GO'
GO

UPDATE contract SET software = 'ADVESO' WHERE software IS NOT NULL
GO

UPDATE contract SET transfertype = 'REST' WHERE transfertype IS NOT NULL
GO

ALTER TABLE contract DROP COLUMN contracttype
GO

ALTER TABLE contract DROP COLUMN planificationrule
GO

ALTER TABLE contract DROP COLUMN financialrule
GO

ALTER TABLE contract DROP COLUMN hourlyrate
GO

INSERT INTO dbversion(version) VALUES(407)
GO

COMMIT TRAN