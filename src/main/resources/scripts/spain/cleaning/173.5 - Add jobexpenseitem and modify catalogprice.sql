-- Script 173.5 to run before startup, and run before 174
-- Creates / alters tables for jobexpenseitem and moving servicetype to catalogprice
-- 2017-09-25
-- Author Galen Beck (extracted from SchemaUpdateTool)

USE [spain]

BEGIN TRAN

    alter table dbo.catalogprice 
        add servicetype int;

    alter table dbo.catalogprice 
        add constraint FKlm7582eg9bcxumtt27b14hlle 
        foreign key (servicetype) 
        references dbo.servicetype;

    alter table dbo.instmodeldomain 
        add domaintype int;

    create table dbo.jobexpenseitem (
        id int identity not null,
        lastModified datetime not null,
        comment varchar(255),
        date date,
        itemNo int,
        cost numeric(10,2) not null,
        lastModifiedBy int,
        job int not null,
        serviceType int not null,
        primary key (id)
    );

    alter table dbo.jobexpenseitem 
        add constraint UQ_jobexpenseitem unique (job, itemNo);

    alter table dbo.jobexpenseitem 
        add constraint FKm9xk66ymaht485038vcfmt9q9 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobexpenseitem 
        add constraint FK_jobexpenseitem_job 
        foreign key (job) 
        references dbo.job;

    alter table dbo.jobexpenseitem 
        add constraint FK_jobexpenseitem_serviceType 
        foreign key (serviceType) 
        references dbo.servicetype;

    alter table dbo.servicetype 
        add domaintype int;

COMMIT TRAN