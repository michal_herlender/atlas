-- Scipt 265 - 2018-06-07 GB
-- Initializes support for action outcome enums

USE [spain]

BEGIN TRAN

    alter table dbo.actionoutcome 
        add type varchar(31);

    alter table dbo.actionoutcome 
        add value varchar(255);

	GO

	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'REQUIRES_INHOUSE_CALIBRATION' WHERE activityid = 8 and description = 'Requires in-house calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'REQUIRES_THIRD_PARTY_CALIBRATION' WHERE activityid = 8 and description = 'Requires third party calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 8 and description = 'Requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 8 and description = 'Requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'REQUIRES_ADJUSTMENT' WHERE activityid = 8 and description = 'Requires adjustment';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'REQUIRES_INSPECTION_TO_DETERMINE_WORK' WHERE activityid = 8 and description = 'Requires inspection to determine work';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'AWAITING_CLIENT_PURCHASE_ORDER' WHERE activityid = 8 and description = 'Awaiting client purchase order';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'AWAITING_CLIENT_FEEDBACK' WHERE activityid = 8 and description = 'Awaiting client feedback';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'NO_ACTION_REQUIRED' WHERE activityid = 8 and description = 'No action required';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'AWAITING_QUOTATION_TO_CLIENT' WHERE activityid = 8 and description = 'Awaiting quotation to client';
	UPDATE [dbo].[actionoutcome] SET type = 'fault_report', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 16 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'fault_report', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 16 and description = 'Unit requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'fault_report', value = 'REQUIRES_ADJUSTMENT' WHERE activityid = 16 and description = 'Unit requires adjustment';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_IN_HOUSE_CALIBRATION' WHERE activityid = 18 and description = 'Unit requires in-house calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_THIRD_PARTY_CALIBRATION' WHERE activityid = 18 and description = 'Unit requires third party calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 18 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 18 and description = 'Unit requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_ADJUSTMENT' WHERE activityid = 18 and description = 'Unit requires adjustment';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'READY_FOR_CALIBRATION' WHERE activityid = 25 and description = 'Inspected with no problems, ready for calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_THIRD_PARTY_CALIBRATION' WHERE activityid = 25 and description = 'Unit requires third party calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 25 and description = 'Unit requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 25 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'REQUIRES_ADJUSTMENT' WHERE activityid = 25 and description = 'Unit requires adjustment';
	UPDATE [dbo].[actionoutcome] SET type = 'engineer_inspection', value = 'IS_BER' WHERE activityid = 25 and description = 'Unit is B.E.R.';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'SUCCESSFUL' WHERE activityid = 26 and description = 'Calibrated successfully';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'IS_BER' WHERE activityid = 26 and description = 'Unit B.E.R.';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_THIRD_PARTY_CALIBRATION' WHERE activityid = 26 and description = 'Unit requires third party calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 26 and description = 'Unit requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 26 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_ADJUSTMENT_PRE_APPROVED' WHERE activityid = 26 and description = 'Calibrated - requires adjustment (pre-approved)';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_ADJUSTMENT_NOT_YET_APPROVED' WHERE activityid = 26 and description = 'Calibrated - requires adjustment (not yet approved)';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 26 and description = 'Calibrated - requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_INHOUSE_REPAIR' WHERE activityid = 26 and description = 'Calibrated - requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'ON_HOLD' WHERE activityid = 26 and description = 'Calibration placed on hold';
	
	UPDATE [dbo].[actionoutcome] SET type = 'fault_report', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 38 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'fault_report', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 38 and description = 'Unit requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'fault_report', value = 'REQUIRES_ADJUSTMENT' WHERE activityid = 38 and description = 'Unit requires adjustment';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_costing', value = 'COSTS_APPROVED' WHERE activityid = 43 and description = 'Costing accepted';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_costing', value = 'COSTS_REJECTED' WHERE activityid = 43 and description = 'Costing rejected';
	UPDATE [dbo].[actionoutcome] SET type = 'repair', value = 'REPAIRED_NO_FURTHER_ACTION_REQUIRED' WHERE activityid = 57 and description = 'Unit repaired - no further action required';
	UPDATE [dbo].[actionoutcome] SET type = 'repair', value = 'REPAIRED_FURTHER_CALIBRATION_NECESSARY' WHERE activityid = 57 and description = 'Unit repaired - further calibration necessary';
	UPDATE [dbo].[actionoutcome] SET type = 'repair', value = 'UNABLE_TO_REPAIR_REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 57 and description = 'Unable to repair - requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'repair', value = 'UNABLE_TO_REPAIR_BER' WHERE activityid = 57 and description = 'Unable to repair - Unit B.E.R.';
	UPDATE [dbo].[actionoutcome] SET type = 'repair', value = 'SPARES_REQUIRED' WHERE activityid = 57 and description = 'Spares required';
	UPDATE [dbo].[actionoutcome] SET type = 'repair', value = 'REPLACEMENT_COMPONENTS_REQUIRED' WHERE activityid = 57 and description = 'Replacement components required';
	UPDATE [dbo].[actionoutcome] SET type = 'repair', value = 'NOT_REPAIRED_NO_FURTHER_ACTION_REQUIRED' WHERE activityid = 57 and description = 'Unit not repaired - no further action required';
	UPDATE [dbo].[actionoutcome] SET type = 'repair', value = 'NOT_REPAIRED_FURTHER_CALIBRATION_NECESSARY' WHERE activityid = 57 and description = 'Unit not repaired - further calibration necessary';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'SUCCESSFUL' WHERE activityid = 61 and description = 'Calibrated successfully';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'IS_BER' WHERE activityid = 61 and description = 'Unit B.E.R.';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_THIRD_PARTY_CALIBRATION' WHERE activityid = 61 and description = 'Unit requires third party calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 61 and description = 'Unit requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 61 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_ADJUSTMENT_PRE_APPROVED' WHERE activityid = 61 and description = 'Calibrated - requires adjustment (pre-approved)';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_ADJUSTMENT_NOT_YET_APPROVED' WHERE activityid = 61 and description = 'Calibrated - requires adjustment (not yet approved)';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 61 and description = 'Calibrated - requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_INHOUSE_REPAIR' WHERE activityid = 61 and description = 'Calibrated - requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'ON_HOLD' WHERE activityid = 61 and description = 'Calibration placed on hold';
	
	UPDATE [dbo].[actionoutcome] SET type = 'components_fitted', value = 'REQUIRES_FURTHER_REPAIR' WHERE activityid = 67 and description = 'Requires further repair';
	UPDATE [dbo].[actionoutcome] SET type = 'components_fitted', value = 'REPAIRED_REQUIRES_FURTHER_CALIBRATION' WHERE activityid = 67 and description = 'Repaired, requires further calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'components_fitted', value = 'REPAIRED_REQUIRES_NO_FURTHER_CALIBRATION' WHERE activityid = 67 and description = 'Repaired, requires no further calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'adjustment', value = 'UNIT_ADJUSTED' WHERE activityid = 68 and description = 'Unit adjusted';
	UPDATE [dbo].[actionoutcome] SET type = 'adjustment', value = 'UNABLE_TO_ADJUST_BER' WHERE activityid = 68 and description = 'Unable to adjust - Unit B.E.R.';
	UPDATE [dbo].[actionoutcome] SET type = 'adjustment', value = 'UNABLE_TO_ADJUST_REQUIRES_INHOUSE_REPAIR' WHERE activityid = 68 and description = 'Unable to adjust - requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'adjustment', value = 'UNABLE_TO_ADJUST_REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 68 and description = 'Unable to adjust - requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_as_found', value = 'APPROVED_AS_FOUND_CERTIFICATE' WHERE activityid = 70 and description = 'Client approved ''as found'' certificate';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_as_found', value = 'REFUSED_AS_FOUND_CERTIFICATE' WHERE activityid = 70 and description = 'Client refused ''as found'' certificate';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_further_work', value = 'TP_REPAIR_APPROVED' WHERE activityid = 76 and description = 'Client approves third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_further_work', value = 'FURTHER_WORK_REJECTED' WHERE activityid = 76 and description = 'Client rejects further work';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_costing', value = 'COSTS_APPROVED' WHERE activityid = 82 and description = 'Costing approved';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_costing', value = 'COSTS_REJECTED' WHERE activityid = 82 and description = 'Costing rejected';
	UPDATE [dbo].[actionoutcome] SET type = 'tp_requirements', value = 'NOT_QUOTED_TP_QUOTES_WITHOUT_ITEM' WHERE activityid = 90 and description = 'Item is not quoted, third party can quote without seeing item';
	UPDATE [dbo].[actionoutcome] SET type = 'tp_requirements', value = 'NOT_QUOTED_SEND_TO_TP_FOR_QUOTE' WHERE activityid = 90 and description = 'Item is not quoted, send to third party for quote';
	UPDATE [dbo].[actionoutcome] SET type = 'tp_requirements', value = 'NOT_QUOTED_SEND_TO_TP_FOR_QUOTE_INSPECTION_COSTS' WHERE activityid = 90 and description = 'Item is not quoted, send to third party for quote (with inspection costs to client)';
	UPDATE [dbo].[actionoutcome] SET type = 'tp_requirements', value = 'PRE_QUOTED_SEND_CLIENT_COSTS' WHERE activityid = 90 and description = 'Item is pre-quoted, send client job costing';
	UPDATE [dbo].[actionoutcome] SET type = 'tp_requirements', value = 'PRE_QUOTED_NOT_COSTED_SEND_TO_TP_FOR_WORK' WHERE activityid = 90 and description = 'Item is pre-quoted but not costed, send item to third party for work';
	UPDATE [dbo].[actionoutcome] SET type = 'tp_requirements', value = 'PRE_QUOTED_COSTED_SEND_TO_TP_FOR_WORK' WHERE activityid = 90 and description = 'Item is pre-quoted and costed, send item to third party for work';
	UPDATE [dbo].[actionoutcome] SET type = 'tp_requirements', value = 'ANOTHER_BUSINESS_SUBDIVISION_NO_PO_REQUIRED' WHERE activityid = 90 and description = 'Item requires work at another subdivision of our business company, no PO required';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_tp_insp_costs', value = 'INSPECTION_COSTS_APPROVED' WHERE activityid = 94 and description = 'Inspection costs approved';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_tp_insp_costs', value = 'INSPECTION_COSTS_REJECTED' WHERE activityid = 94 and description = 'Inspection costs rejected';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_tp_job_costing', value = 'TP_COSTS_APPROVED' WHERE activityid = 107 and description = 'Client approves third party costing';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_tp_job_costing', value = 'TP_COSTS_REJECTED' WHERE activityid = 107 and description = 'Client rejects third party costing';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_tp_further_work', value = 'APPROVED_FURTHER_WORK' WHERE activityid = 125 and description = 'Client has approved further work';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_tp_further_work', value = 'INSPECTION_FEES_ONLY' WHERE activityid = 125 and description = 'Client requested no further action, inspection fees only';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_tp_further_work', value = 'AS_FOUND_RESULTS_ONLY' WHERE activityid = 125 and description = 'Client has approved as-found results only';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'NO_FURTHER_WORK_NECESSARY' WHERE activityid = 131 and description = 'No further work necessary';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_INHOUSE_CALIBRATION' WHERE activityid = 131 and description = 'Unit requires in-house calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 131 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_ADJUSTMENT' WHERE activityid = 131 and description = 'Unit requires adjustment';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_FURTHER_TP_WORK' WHERE activityid = 131 and description = 'Unit requires further third party work';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_COSTING_BEFORE_CLIENT_DESPATCH' WHERE activityid = 131 and description = 'Unit requires costing before despatch to client';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'NO_FURTHER_WORK_NECESSARY' WHERE activityid = 133 and description = 'No further work necessary';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_INHOUSE_CALIBRATION' WHERE activityid = 133 and description = 'Unit requires in-house calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 133 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_ADJUSTMENT' WHERE activityid = 133 and description = 'Unit requires adjustment';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_FURTHER_TP_WORK' WHERE activityid = 133 and description = 'Unit requires further third party work';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'WORK_NOT_COMPLETED_AT_TP' WHERE activityid = 133 and description = 'Work has not been completed at third party';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'REQUIRES_COSTING_BEFORE_CLIENT_DESPATCH' WHERE activityid = 133 and description = 'Unit requires costing before despatch to client';
	UPDATE [dbo].[actionoutcome] SET type = 'post_tp_further_work', value = 'TP_REPAIR_UNSUCCESSFUL_RETURN_TO_TP' WHERE activityid = 133 and description = 'Third party repair un-successful - return to third party';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_exchange', value = 'REPLACEMENT_QUOTE_ACCEPTED' WHERE activityid = 148 and description = 'Client accepted quote for replacement';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_exchange', value = 'REPLACEMENT_QUOTE_ACCEPTED' WHERE activityid = 148 and description = 'Client rejected quote for replacement';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_costing', value = 'COSTS_APPROVED' WHERE activityid = 181 and description = 'Costing approved';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_costing', value = 'COSTS_REJECTED' WHERE activityid = 181 and description = 'Costing rejected';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'SUCCESSFUL' WHERE activityid = 4048 and description = 'Calibrated successfully';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'IS_BER' WHERE activityid = 4048 and description = 'Unit B.E.R.';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'ON_HOLD' WHERE activityid = 4048 and description = 'Calibration placed on hold';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_THIRD_PARTY_CALIBRATION' WHERE activityid = 4048 and description = 'Unit requires third party calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 4048 and description = 'Unit requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 4048 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_ADJUSTMENT_PRE_APPROVED' WHERE activityid = 4048 and description = 'Calibrated - requires adjustment (pre-approved)';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_ADJUSTMENT_NOT_YET_APPROVED' WHERE activityid = 4048 and description = 'Calibrated - requires adjustment (not yet approved)';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 4048 and description = 'Calibrated - requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_INHOUSE_REPAIR' WHERE activityid = 4048 and description = 'Calibrated - requires in-house repair';
	
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'AWAITING_CLIENT_PURCHASE_ORDER' WHERE activityid = 4055 and description = 'Awaiting client purchase order';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'AWAITING_CLIENT_FEEDBACK' WHERE activityid = 4055 and description = 'Awaiting client feedback';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'NO_ACTION_REQUIRED' WHERE activityid = 4055 and description = 'No action required';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'AWAITING_QUOTATION_TO_CLIENT' WHERE activityid = 4055 and description = 'Awaiting quotation to client';
	UPDATE [dbo].[actionoutcome] SET type = 'contract_review', value = 'REQUIRES_ONSITE_CALIBRATION' WHERE activityid = 4055 and description = 'Requires on-site calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'SUCCESSFUL' WHERE activityid = 4079 and description = 'Calibrated successfully';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'IS_BER' WHERE activityid = 4079 and description = 'Unit B.E.R.';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_THIRD_PARTY_CALIBRATION' WHERE activityid = 4079 and description = 'Unit requires third party calibration';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 4079 and description = 'Unit requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'REQUIRES_INHOUSE_REPAIR' WHERE activityid = 4079 and description = 'Unit requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_ADJUSTMENT_PRE_APPROVED' WHERE activityid = 4079 and description = 'Calibrated - requires adjustment (pre-approved)';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_ADJUSTMENT_NOT_YET_APPROVED' WHERE activityid = 4079 and description = 'Calibrated - requires adjustment (not yet approved)';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 4079 and description = 'Calibrated - requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'CALIBRATED_REQUIRES_INHOUSE_REPAIR' WHERE activityid = 4079 and description = 'Calibrated - requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'calibration', value = 'ON_HOLD' WHERE activityid = 4079 and description = 'Calibration placed on hold';
	
	UPDATE [dbo].[actionoutcome] SET type = 'adjustment', value = 'UNIT_ADJUSTED' WHERE activityid = 4081 and description = 'Unit adjusted';
	UPDATE [dbo].[actionoutcome] SET type = 'adjustment', value = 'UNABLE_TO_ADJUST_BER' WHERE activityid = 4081 and description = 'Unable to adjust - Unit B.E.R.';
	UPDATE [dbo].[actionoutcome] SET type = 'adjustment', value = 'UNABLE_TO_ADJUST_REQUIRES_INHOUSE_REPAIR' WHERE activityid = 4081 and description = 'Unable to adjust - requires in-house repair';
	UPDATE [dbo].[actionoutcome] SET type = 'adjustment', value = 'UNABLE_TO_ADJUST_REQUIRES_THIRD_PARTY_REPAIR' WHERE activityid = 4081 and description = 'Unable to adjust - requires third party repair';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_costing', value = 'COSTS_APPROVED' WHERE activityid = 4083 and description = 'Costing accepted';
	UPDATE [dbo].[actionoutcome] SET type = 'client_decision_costing', value = 'COSTS_APPROVED' WHERE activityid = 4083 and description = 'Costing rejected';

	alter table dbo.actionoutcome alter column type varchar(31) not null;

	alter table dbo.actionoutcome alter column value varchar(255) not null;

insert into dbversion(version) values(265);

COMMIT TRAN;