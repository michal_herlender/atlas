
USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.gsodocument ADD supplementaryforid INT NULL;
	
	ALTER TABLE dbo.gsodocument ADD CONSTRAINT FK_supplementaryforid_gsodocument 
	FOREIGN KEY (supplementaryforid)  REFERENCES dbo.gsodocument(id);
	
	DECLARE @require_client_decision_lookup INT,
			@new_activity INT,
			@new_hook INT;
	
	SELECT @require_client_decision_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_ClientDecisionRequiredForGeneralServiceOperation';
	
	INSERT INTO dbo.itemstate
	([type], active, description, retired, lookupid, actionoutcometype)
	VALUES('workactivity', 1, 'Document uploaded for general operation (Cancel and Replace)', 0, @require_client_decision_lookup, NULL);
	
	SELECT @new_activity = MAX(stateid) FROM dbo.itemstate;
	
	INSERT INTO atlas.dbo.itemstatetranslation
	(stateid, locale, [translation])
	VALUES(@new_activity, 'fr_FR', 'Document téléchargé pour opération générale (Annuler et Remplacer)'),
		  (@new_activity, 'es_ES', 'Documento cargado para operación general (Cancelar y reemplazar)'),
		  (@new_activity, 'en_GB', 'Document uploaded for general operation (Cancel and Replace)'),
		  (@new_activity, 'de_DE', 'Dokument für den allgemeinen Betrieb hochgeladen (Abbrechen und Ersetzen)');
	
	INSERT INTO dbo.hook
	(active, alwayspossible, beginactivity, completeactivity, name, activityid)
	VALUES(1, 1, 1, 1, 'upload-gso-document-cancel-replace', @new_activity);
	
	DECLARE @awaiting_gso_doc int;
	SELECT @new_hook = MAX(id) FROM dbo.hook;
	SELECT @awaiting_gso_doc = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting document upload for general operation';
	
	INSERT INTO dbo.hookactivity
	(beginactivity, completeactivity, activityid, hookid, stateid)
	VALUES(0, 1, @new_activity, @new_hook, @awaiting_gso_doc);
		
		 
	DECLARE @constraint_name VARCHAR(100);
	SELECT @constraint_name = CONSTRAINT_NAME FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE 
	WHERE TABLE_NAME = 'generalserviceoperation' and COLUMN_NAME = 'servicetypeid';
	EXEC ('ALTER TABLE dbo.generalserviceoperation DROP CONSTRAINT ' + @constraint_name);
	ALTER TABLE dbo.generalserviceoperation DROP COLUMN servicetypeid;
	
	ALTER TABLE dbo.generalserviceoperation ADD caltypeid INT;
	ALTER TABLE dbo.generalserviceoperation ADD CONSTRAINT FK_calibrationtype_generalserviceoperation 
	FOREIGN KEY (caltypeid) REFERENCES dbo.calibrationtype(caltypeid);
	
	ALTER TABLE dbo.generalserviceoperation ADD procedureid INT;
	ALTER TABLE dbo.generalserviceoperation ADD CONSTRAINT FK_procs_generalserviceoperation 
	FOREIGN KEY (procedureid) REFERENCES dbo.procs(id);
	
	DECLARE @vbf_caltype INT;
	SELECT @vbf_caltype = ct.caltypeid FROM dbo.calibrationtype ct JOIN dbo.servicetype st 
	ON ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'VCO';
	
	INSERT INTO dbo.calibrationaccreditationlevel
	(accredlevel, caltypeid)
	VALUES('APPROVE', @vbf_caltype),
		  ('PERFORM', @vbf_caltype),
		  ('REMOVED', @vbf_caltype),
	      ('SUPERVISED', @vbf_caltype);
	
	INSERT INTO dbversion(version) VALUES(676);

COMMIT TRAN

