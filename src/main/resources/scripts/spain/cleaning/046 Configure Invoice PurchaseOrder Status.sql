BEGIN TRAN

-- Author Galen Beck - 2016-06-24
-- Performs minor updates to configure new PO status and invoice fields

USE [spain]
GO

UPDATE [dbo].[invoicestatus]
   SET approved = 1
 WHERE statusid >= 5;

UPDATE [dbo].[invoicestatus]
   SET issued = 1
 WHERE statusid = 6;

UPDATE [dbo].[purchaseorderstatus]
   SET approved = 1
 WHERE statusid >= 5;

-- Initialize the business subdivision of all purchase order items to Madrid

UPDATE [dbo].[purchaseorderitem]
   SET orgid = 6645
 WHERE orgid is NULL;

GO

ROLLBACK TRAN