-- This script removes a large number of unnecessary, obsolete fields from instmodel
-- which are now also removed from the domain definitions and legacy code (if still present)
-- Galen Beck 2020-12-04

USE [atlas]

BEGIN TRAN

--ALTER TABLE [dbo].[instmodel] DROP COLUMN [];

ALTER TABLE [dbo].[instmodel] DROP CONSTRAINT [CK__instmodel__insto__22951AFD]
GO

ALTER TABLE [dbo].[instmodel] DROP CONSTRAINT [CK__instmodel__caldu__23893F36]
GO

DROP INDEX [IDX_instmodel_stocklastcheckedby] ON [dbo].[instmodel]
GO

-- for stocklastcheckedby
ALTER TABLE [dbo].[instmodel] DROP CONSTRAINT [FKq9e3wup3mta9r12d7f8k86u3c]
GO

-- for alwaysbasefirst
DROP STATISTICS [dbo].[instmodel].[_dta_stat_516196889_8_1]
GO

-- for procid
DROP INDEX [IDX_instmodel_procid] ON [dbo].[instmodel]
GO
DROP STATISTICS [dbo].[instmodel].[_dta_stat_516196889_23_1_21_22_24]
GO
ALTER TABLE [dbo].[instmodel] DROP CONSTRAINT [FKdnbucaovdlvdcde32qmfbfrjb]
GO

-- for wiid
DROP INDEX [IDX_instmodel_wiid] ON [dbo].[instmodel]
GO
ALTER TABLE [dbo].[instmodel] DROP CONSTRAINT [FK74g2bhwyqxxvg6awuw5ngilbi]
GO


ALTER TABLE [dbo].[instmodel] DROP COLUMN [instock];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [minstocklevel];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [salescost];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [stocklastchecked];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [calduration];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [labeltype];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [stocklastcheckedby];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [TEMP_ID];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [IsDetailedSaleCategory];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [techSpec];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [adjsupport];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [alwaysbasefirst];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [cleaned];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [defaultchecksheet];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [donotrecall];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [coshhrequired];

ALTER TABLE [dbo].[instmodel] DROP COLUMN [procid];
ALTER TABLE [dbo].[instmodel] DROP COLUMN [wiid];

insert into dbversion values(741);

COMMIT TRAN