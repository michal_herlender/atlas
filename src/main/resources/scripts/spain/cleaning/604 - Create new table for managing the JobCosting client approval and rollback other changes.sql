
USE [atlas]
GO


BEGIN TRAN

-- add the statusid to jobcosting
ALTER TABLE dbo.jobcosting ADD statusid INT;
-- add new field clientapprovalon to the quotation 
ALTER TABLE dbo.quotation ADD clientapprovalon DATE;

GO

-- update statusid with last statusid from jobcostingstatushistory
UPDATE
    jc
SET
    jc.statusid = jcsh.statusid
FROM
    dbo.jobcosting AS jc
    INNER JOIN dbo.jobcostingstatushistory AS jcsh
        ON jc.id = jcsh.jobcostingid
AND jcsh.statusupdatedon = (SELECT MAX(statusupdatedon) FROM dbo.jobcostingstatushistory 
WHERE jobcostingid = jc.id);

-- remove all fk constraint on table jobcostingstatushistory
DECLARE @constraint_name VARCHAR(100);
SELECT @constraint_name = CONSTRAINT_NAME FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE 
WHERE TABLE_NAME = 'jobcostingstatushistory' and COLUMN_NAME = 'statusid';
EXEC ('ALTER TABLE dbo.jobcostingstatushistory DROP CONSTRAINT ' + @constraint_name);

SELECT @constraint_name = CONSTRAINT_NAME FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE 
WHERE TABLE_NAME = 'jobcostingstatushistory' and COLUMN_NAME = 'jobcostingid';
EXEC ('ALTER TABLE dbo.jobcostingstatushistory DROP CONSTRAINT ' + @constraint_name);

-- drop the jobcostingstatushistory table
DROP TABLE dbo.jobcostingstatushistory;

-- add new table jobcostingclientapproval
CREATE TABLE dbo.jobcostingclientapproval (
    id INT IDENTITY(1,1) NOT NULL,
	jobcostingid INT NOT NULL,
	clientapproval BIT,
    clientapprovalon DATE,
    clientapprovalcomment nvarchar(1000),
    PRIMARY KEY (id)
);

ALTER TABLE dbo.jobcostingclientapproval 
    ADD CONSTRAINT FK_jcclientapproval_jobcosting
    FOREIGN KEY (jobcostingid) 
    REFERENCES dbo.jobcosting(id);

-- delete 'Validated','Cancelled','On-Hold' jobcosting status
DELETE FROM dbo.basestatusnametranslation WHERE statusid IN 
(SELECT statusid FROM dbo.basestatus WHERE [type] = 'jobcosting' 
AND [name] IN ('Validated','Cancelled','On-Hold'));
DELETE FROM dbo.basestatus WHERE [type] = 'jobcosting' AND [name] IN 
('Validated','Cancelled','On-Hold');

-- rename 'Discarded' jobcosting status to 'Cancelled'
DECLARE @jc_deprecatedstatus INT;
SELECT @jc_deprecatedstatus = statusid FROM dbo.basestatus 
WHERE [type] = 'jobcosting' AND [name] = 'Discarded';
UPDATE dbo.basestatus SET [name] = 'Cancelled' WHERE statusid = @jc_deprecatedstatus;
UPDATE dbo.basestatusnametranslation SET translation = 'Cancelled' 
WHERE locale = 'en_GB' AND statusid = @jc_deprecatedstatus;
UPDATE dbo.basestatusnametranslation SET translation = 'Annulé' 
WHERE locale = 'fr_FR' AND statusid = @jc_deprecatedstatus;
UPDATE dbo.basestatusnametranslation SET translation = 'Cancelado' 
WHERE locale = 'es_ES' AND statusid = @jc_deprecatedstatus;
UPDATE dbo.basestatusnametranslation SET translation = 'Abgesagt' 
WHERE locale = 'de_DE' AND statusid = @jc_deprecatedstatus;

INSERT INTO dbversion(version) VALUES (604);

COMMIT TRAN