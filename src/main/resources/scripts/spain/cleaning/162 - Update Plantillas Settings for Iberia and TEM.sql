-- Script 162, run AFTER startup
-- Also sets 'usesPlantillas" to true for TEM business company
-- Sets 'syncToPlantillas' to true for TEM and Iberia

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[businesscompanysettings]
   SET [usesPlantillas] = 1
 WHERE [companyid] = 6579;
GO

UPDATE [dbo].[companysettingsforallocatedcompany]
   SET [syncToPlantillas] = 1
 WHERE [orgid] = 6579
   AND [companyid] IN (6579, 10153)
GO

-- Initialize Iberia address plantillas settings to former cutsomer ID 11048 per Lazaro

UPDATE [dbo].[addressplantillassettings]
   SET [formerCustomerId] = 11048
   WHERE addressid IN (
       SELECT addressid FROM addressplantillassettings 
	   INNER JOIN address on address.addrid = addressplantillassettings.addressId
	   INNER JOIN subdiv on address.subdivid = subdiv.subdivid
	   WHERE subdiv.coid = 10153)

INSERT INTO [dbo].[addressplantillassettings]
           ([lastModified]
           ,[ftpPassword]
           ,[ftpServer]
           ,[ftpUser]
           ,[metraClient]
           ,[nextDateOnCertificate]
           ,[sendByFtp]
           ,[textToleranceFailure]
           ,[textUncertaintyFailure]
           ,[orgid]
           ,[addressId]
           ,[certificateAddressId]
           ,[managedByAddressId]
           ,[formerCustomerId])
     SELECT CURRENT_TIMESTAMP,'','','',0,0,0,'','',6579,[address].addrid,null,null,11048
	 FROM address
	 INNER JOIN subdiv on address.subdivid = subdiv.subdivid
	 LEFT JOIN addressplantillassettings ON addressplantillassettings.addressId = address.addrid
	 WHERE subdiv.coid = 10153 AND addressplantillassettings.addressId IS NULL

GO

COMMIT TRAN
