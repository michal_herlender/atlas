USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[scheduledtask]
      WHERE classname = 'org.trescal.cwms.core.tools.FileCheckerTool'
GO

DELETE FROM [dbo].[scheduledtask]
      WHERE classname = 'org.trescal.cwms.core.misc.entity.ids.db.IdsServiceImpl'
GO

INSERT INTO dbversion(version) VALUES(466);

COMMIT TRAN