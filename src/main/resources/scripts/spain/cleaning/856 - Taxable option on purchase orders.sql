BEGIN TRAN

ALTER TABLE dbo.businesscompanysettings ADD "useTaxableOption" BIT NOT NULL DEFAULT 0

ALTER TABLE dbo.purchaseorder ADD "taxable" INT NOT NULL DEFAULT 0

INSERT INTO dbversion(version) VALUES(856);

COMMIT TRAN