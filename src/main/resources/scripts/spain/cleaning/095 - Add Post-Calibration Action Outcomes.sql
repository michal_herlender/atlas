USE [spain]

BEGIN TRAN;

SET IDENTITY_INSERT [actionoutcome] ON;

INSERT INTO [dbo].[actionoutcome]
           ([id],[defaultoutcome],[description],[positiveoutcome],[activityid],[active])
     VALUES
           (102,1,'Calibrated successfully',1,61,1),
           (103,0,'Unit B.E.R.',0,61,1),
           (104,0,'Unit requires third party calibration',0,61,1),
           (105,0,'Unit requires third party repair',0,61,1),
           (106,0,'Unit requires in-house repair',0,61,1),
           (107,0,'Calibrated - requires adjustment (pre-approved)',1,61,1),
           (108,0,'Calibrated - requires adjustment (not yet approved)',1,61,1),
           (109,0,'Calibrated - requires third party repair',1,61,1),
           (110,0,'Calibrated - requires in-house repair',1,61,1),
           (111,0,'Calibration placed on hold',0,61,1)
GO

SET IDENTITY_INSERT [actionoutcome] OFF;

COMMIT TRAN;