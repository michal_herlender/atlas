-- Makes company document language not nullable
-- See comments in DEV-1005 (essential to be not nullable, for various reasons)

USE [atlas];

GO

BEGIN TRAN

-- Any nullable values are expected only on test and local servers (remains not null on production)

UPDATE dbo.company SET documentlanguage = 'en_GB' WHERE documentlanguage IS NULL;
GO

ALTER TABLE dbo.company ALTER COLUMN documentlanguage VARCHAR(255) NOT NULL;

INSERT INTO dbversion(version) VALUES(594);

COMMIT TRAN