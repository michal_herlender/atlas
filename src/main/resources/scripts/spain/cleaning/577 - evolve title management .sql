USE [atlas]
Go

BEGIN TRAN

alter table dbo.title add countryid int;

GO

alter table dbo.title  add constraint FK_title_country foreign key (countryid) references dbo.country;

alter table dbo.title add id int IDENTITY(1,1);

alter table dbo.contact drop FKqkkedqojbgf1wwujg89a8wao3;
alter table dbo.title DROP PK__title__E52A1BB2546C6DB3;

ALTER TABLE dbo.title ADD CONSTRAINT PK_title_id PRIMARY KEY (id);

DECLARE @morocco INT,@france INT,@germany INT,@Spain INT,@USA INT, @tunisia INT;

select @morocco=c.countryid from dbo.country c where c.countrycode like 'MA';
select @france= c.countryid from dbo.country c where c.countrycode like 'fr';
select @Spain= c.countryid from dbo.country c where c.countrycode like 'es';
select @USA= c.countryid from dbo.country c where c.countrycode like 'us';
select @germany= c.countryid from dbo.country c where c.countrycode like 'de';
select @tunisia=c.countryid from dbo.country c where c.countrycode like 'TN'

update dbo.title set countryid=@USA

Insert into dbo.title (title,countryid) 
values ('M.',@morocco),('Mme',@morocco),('Mlle',@morocco),('Dr',@morocco);

Insert into dbo.title (title,countryid) 
values ('M.',@france),('Mme',@france),('Mlle',@france),('Dr',@france);

Insert into dbo.title (title,countryid) 
values ('Herr',@germany),('Frau',@germany);

Insert into dbo.title (title,countryid) 
values ('Sr.',@Spain),('Sra.',@Spain);

Insert into dbo.title (title,countryid) 
values ('M.',@tunisia),('Mme',@tunisia),('Mlle',@tunisia),('Dr',@tunisia);

INSERT INTO dbversion(version) VALUES (577);

COMMIT TRAN
