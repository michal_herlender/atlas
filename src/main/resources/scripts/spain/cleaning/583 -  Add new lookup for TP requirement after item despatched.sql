
USE [atlas];

GO

BEGIN TRAN

DECLARE @new_lookup INT,
		@tpreq_lookup INT,
		@new_lookup_os1 INT,
		@new_lookup_os2 INT,
		@itemdesptotp_activity INT;

INSERT INTO dbo.lookupinstance([lookup], lookupfunction) VALUES('Lookup_TPRequirements (third party after item despatched)', 35);
SELECT @new_lookup = MAX(id) FROM dbo.lookupinstance;

SELECT @tpreq_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_TPRequirements (third party return for repair)';
SELECT @itemdesptotp_activity = stateid FROM dbo.itemstate WHERE [description] = 'Item despatched to third party';

SELECT @new_lookup_os1 = outcomestatusid FROM dbo.lookupresult WHERE lookupid = @tpreq_lookup AND 
resultmessage = 0;
SELECT @new_lookup_os2 = outcomestatusid FROM dbo.lookupresult WHERE lookupid = @tpreq_lookup AND 
resultmessage = 3;

INSERT INTO atlas.dbo.lookupresult
([result], activityid, lookupid, outcomestatusid, resultmessage)
VALUES('a', @itemdesptotp_activity, @new_lookup, @new_lookup_os1, 0),
		('b', @itemdesptotp_activity, @new_lookup, @new_lookup_os1, 1),
		('c', @itemdesptotp_activity, @new_lookup, @new_lookup_os1, 2),
		('d', @itemdesptotp_activity, @new_lookup, @new_lookup_os2, 3),
		('e', @itemdesptotp_activity, @new_lookup, @new_lookup_os2, 4),
		('f', @itemdesptotp_activity, @new_lookup, @new_lookup_os2, 5),
		('g', @itemdesptotp_activity, @new_lookup, @new_lookup_os2, 6);

UPDATE dbo.outcomestatus SET lookupid = NULL WHERE id IN (@new_lookup_os1,@new_lookup_os2);
UPDATE dbo.itemstate SET lookupid = @new_lookup WHERE stateid = @itemdesptotp_activity;

INSERT INTO dbversion(version) VALUES(583);

COMMIT TRAN