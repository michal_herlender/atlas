USE spain
GO

BEGIN tran

UPDATE quotationitem SET servicetypeid = calibrationtype.servicetypeid
FROM quotationitem
LEFT JOIN calibrationtype ON quotationitem.caltype_caltypeid = calibrationtype.caltypeid
WHERE quotationitem.servicetypeid IS NULL;

INSERT INTO dbversion VALUES (225);

COMMIT tran