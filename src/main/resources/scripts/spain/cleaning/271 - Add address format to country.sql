-- Script 270 - Adds and intialized
-- Lets us have variations in printed address format by country
-- Most formats determined from:
-- http://www.columbia.edu/~fdc/postal/
-- https://www.parcelforce.com/help-and-advice/sending/address-formats-world


BEGIN TRAN

alter table dbo.country 
    add addressprintformat varchar(255);
GO

PRINT 'POSTCODE_TOWN for Morocco and Tunisia'
UPDATE [dbo].[country]
   SET addressprintformat = 'POSTCODE_TOWN'
 WHERE countrycode IN ('MA', 'TN')
GO

PRINT 'POSTCODE_TOWN_COMMA_REGION for China and Mexico'
UPDATE [dbo].[country]
   SET addressprintformat = 'POSTCODE_TOWN_COMMA_REGION'
 WHERE countrycode IN ('CN', 'MX')
GO

PRINT 'POSTCODE_TOWN_REGIONCODE for Italy'
UPDATE [dbo].[country]
   SET addressprintformat = 'POSTCODE_TOWN_REGIONCODE'
 WHERE countrycode IN ('IT')
GO

PRINT 'POSTCODE_TOWN_NEWLINE_REGION for Malaysia'
UPDATE [dbo].[country]
   SET addressprintformat = 'POSTCODE_TOWN_NEWLINE_REGION'
 WHERE countrycode IN ('MY')
GO

PRINT 'SPECIAL_SPAIN'
UPDATE [dbo].[country]
   SET addressprintformat = 'SPECIAL_SPAIN'
 WHERE countrycode IN ('ES')
GO

PRINT 'SPECIAL_BRASIL'
UPDATE [dbo].[country]
   SET addressprintformat = 'SPECIAL_BRASIL'
 WHERE countrycode IN ('BR')
GO

PRINT 'SPECIAL_HUNGARY'
UPDATE [dbo].[country]
   SET addressprintformat = 'SPECIAL_HUNGARY'
 WHERE countrycode IN ('HU')
GO

PRINT 'SPECIAL_JAPAN'
UPDATE [dbo].[country]
   SET addressprintformat = 'SPECIAL_JAPAN'
 WHERE countrycode IN ('JP')
GO

PRINT 'TOWN_NEWLINE_POSTCODE for UK, Russia, Ukraine, Kazakh'
UPDATE [dbo].[country]
   SET addressprintformat = 'TOWN_NEWLINE_POSTCODE'
 WHERE countrycode IN ('GB', 'RU', 'UA', 'KZ')
GO

PRINT 'TOWN_POSTCODE for New Zealand, Thailand, Singapore'
UPDATE [dbo].[country]
   SET addressprintformat = 'TOWN_POSTCODE'
 WHERE countrycode IN ('NZ', 'TH', 'SG')
GO

PRINT 'TOWN_REGIONCODE_POSTCODE for Australia, USA, Canada'
UPDATE [dbo].[country]
   SET addressprintformat = 'TOWN_REGIONCODE_POSTCODE'
 WHERE countrycode IN ('US', 'CA', 'AU')
GO

PRINT 'POSTCODE_TOWN for remainder EU as default'
UPDATE [dbo].[country]
   SET addressprintformat = 'POSTCODE_TOWN'
 WHERE memberOfUE = 1 and addressprintformat is null
GO

PRINT 'DEFAULT for remainder of countries'
UPDATE [dbo].[country]
   SET addressprintformat = 'DEFAULT'
 WHERE addressprintformat is null
GO

alter table dbo.country 
    alter column addressprintformat varchar(255) not null;
GO

INSERT INTO dbversion(version) VALUES(271);

COMMIT TRAN