-- Creates tables for Alligator Label Printing configuration
-- Author Galen Beck - 2017-12-14

USE [spain]
BEGIN TRAN

    create table dbo.alligatorlabelmedia (
        id int identity not null,
        gap numeric(15,5) not null,
        height numeric(15,5) not null,
        name varchar(50) not null,
        paddingbottom numeric(15,5) not null,
        paddingleft numeric(15,5) not null,
        paddingright numeric(15,5) not null,
        paddingtop numeric(15,5) not null,
        rotationangle numeric(15,5) not null,
        width numeric(15,5) not null,
        primary key (id)
    );

	SET IDENTITY_INSERT [alligatorlabelmedia] ON

	INSERT INTO [dbo].[alligatorlabelmedia]
	(id, gap, height, name, paddingbottom, paddingleft, paddingright, paddingtop, rotationangle, width)
	VALUES
	(1, 2.5, 25, 'Label 50 x 25 mm', 0, 0, 0, 0, 0, 50),
	(2, 2.5, 25, 'Label 50 x 25 mm ES-TEM-ZAZ', 0, 20, 0, 0, 0, 50)

	SET IDENTITY_INSERT [alligatorlabelmedia] OFF

    create table dbo.alligatorlabeltemplate (
        id int identity not null,
        description varchar(100) not null,
        templatexml varchar(max) not null,
        templatetype varchar(255) not null,
        primary key (id)
    );

    create table dbo.alligatorlabeltemplaterule (
        id int identity not null,
        coid int,
        templateid int,
        primary key (id)
    );

    alter table dbo.labelprinter 
        add darknesslevel int;

    alter table dbo.labelprinter 
        add ipv4 varchar(15);

    alter table dbo.labelprinter 
        add language varchar(255);

    alter table dbo.labelprinter 
        add resolution int;

    alter table dbo.labelprinter 
        add media int;

	GO

	UPDATE [dbo].[labelprinter]
	SET darknesslevel = 15,
		ipv4 = '',
		language = 'ZPL',
		resolution = 300,
		media = 1
	WHERE description <> 'TEC_ZGZ_23';

	UPDATE [dbo].[labelprinter]
	SET darknesslevel = 30,
		ipv4 = '',
		language = 'TEC',
		resolution = 300,
		media = 2
	WHERE description = 'TEC_ZGZ_23';

	GO

    alter table dbo.labelprinter 
        alter column darknesslevel int not null;

    alter table dbo.labelprinter 
        alter column ipv4 varchar(15) not null;

    alter table dbo.labelprinter 
        alter column language varchar(255) not null;

    alter table dbo.labelprinter 
        alter column resolution int not null;

    alter table dbo.labelprinter 
        alter column media int not null;

    alter table dbo.alligatorlabeltemplaterule 
        add constraint FKkhw8eq17ovx4jg2jwugjd62j3 
        foreign key (coid) 
        references dbo.company;

    alter table dbo.alligatorlabeltemplaterule 
        add constraint FKffuy4ta748sir0kte6lapf3b0 
        foreign key (templateid) 
        references dbo.alligatorlabeltemplate;

    alter table dbo.labelprinter 
        add constraint FKj71s4nh3uovqiwesuon8bmkee 
        foreign key (media) 
        references dbo.alligatorlabelmedia;

GO

DECLARE @XmlBody NVARCHAR(MAX)

Set @XmlBody = '<template>
	<parameters>
		<parameter name="calDate" value="" />
		<parameter name="dueDate" value="" />
		<parameter name="identification" value="" />
		<parameter name="barcode" value="" />
		<parameter name="documentName" value="" />
		<parameter name="serial" value="" />
		<parameter name="accred" value="" />
	</parameters>
	<name>ES_Cal_ENAC</name>
	<height>217</height>
	<width>435</width>
	<string ypos="44" xpos="15" height="134" width="103" rotationAngle="0" font="Verdana" size="18.00" text="Fecha cal&#xA;Sig cal&#xA;ID No&#xA;Equip No&#xA;Serie No&#xA;Cert" />
	<string ypos="44" xpos="125" height="134" width="273" rotationAngle="0" font="Verdana" size="18.00" text="${calDate}&#xA;${dueDate}&#xA;${identification}&#xA;${barcode}&#xA;${documentName}&#xA;${serial}" />
	<string ypos="184" xpos="302" height="25" width="112" rotationAngle="0" font="Verdana" size="18.00" text="${barcode}" />
	<string ypos="59" xpos="295" height="19" width="112" rotationAngle="0" font="Verdana" size="14.00" text="${accred}" />
	<barcode ypos="178" xpos="37" height="41" width="265" rotationAngle="0" value="${barcode}" type="128" />
	<image ypos="4" xpos="295" height="59" width="147" rotationAngle="0" name="ENAC_logo_bw_es" />
	<image ypos="4" xpos="11" height="37" width="147" rotationAngle="0" name="trescal_black_no_curve" />
</template>'

SET IDENTITY_INSERT [alligatorlabeltemplate] ON

INSERT INTO [dbo].[alligatorlabeltemplate]
           ([id],[description],[templatexml],[templatetype])
     VALUES
           (1,'ES_Cal_ENAC',@XmlBody,'ACCREDITED'),
           (2,'ES_Cal_ENAC',@XmlBody,'ACCREDITED_SMALL'),
           (3,'ES_Cal_ENAC',@XmlBody,'STANDARD'),
           (4,'ES_Cal_ENAC',@XmlBody,'STANDARD_SMALL')

SET IDENTITY_INSERT [alligatorlabeltemplate] OFF

INSERT INTO [dbo].[alligatorlabeltemplaterule]
           ([coid],[templateid])
     VALUES
           (6579, 1),
           (6579, 2),
           (6579, 3),
           (6579, 4),
           (6578, 1),
           (6578, 2),
           (6578, 3),
           (6578, 4),
           (6670, 1),
           (6670, 2),
           (6670, 3),
           (6670, 4);

GO

INSERT INTO dbversion (version) VALUES (203);

COMMIT TRAN;
