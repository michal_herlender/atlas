USE [atlas]
Go

BEGIN TRAN


	DECLARE @Permissionrole INT;
	SELECT @Permissionrole= id FROM dbo.perrole where name='ROLE_ADMINISTRATOR';
	DECLARE @PermissionRoleID INT
	DECLARE @GetPermissionRoleID CURSOR
	SET @GetPermissionRoleID = CURSOR FOR SELECT rg.groupId FROM rolegroup rg WHERE rg.roleId=@Permissionrole  
 
	OPEN @GetPermissionRoleID
	FETCH NEXT
	FROM @GetPermissionRoleID INTO @PermissionRoleID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO atlas.dbo.permission (groupId, permission) VALUES(@PermissionRoleID, 'ADD_CONTACT_BUSINESS');
		print(@PermissionRoleID)

		FETCH NEXT FROM @GetPermissionRoleID INTO @PermissionRoleID
	END

	CLOSE @GetPermissionRoleID
	DEALLOCATE @GetPermissionRoleID


INSERT INTO dbversion(version) VALUES(807);

COMMIT TRAN
