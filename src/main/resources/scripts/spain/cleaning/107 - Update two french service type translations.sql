-- Script to update two service type short name descriptions for France

USE [spain]
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-SI'
 WHERE [servicetypeid] = 4 AND [locale] = 'fr_FR';

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-SI'
 WHERE [servicetypeid] = 5 AND [locale] = 'fr_FR';

GO