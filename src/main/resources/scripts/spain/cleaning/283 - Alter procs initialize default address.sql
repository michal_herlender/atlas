-- Script 283
-- Initializes default address column for procedure (capability) and makes it a mandatory field
-- This is used in new workflow / work requirement changes

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[procs]
   SET [defaddressid] = 
	(SELECT TOP 1 department.addrid FROM department
		INNER JOIN procedurecategory on department.deptid = procedurecategory.departmentid
		INNER JOIN categorisedprocedure on procedurecategory.id = categorisedprocedure.categoryid
		WHERE categorisedprocedure.procid = procs.id)
 WHERE defaddressid is null
GO

/****** Index does not appear to be necessary ******/
DROP INDEX [IDX_procs_defaddressid] ON [dbo].[procs]
GO

ALTER TABLE [procs] ALTER COLUMN defaddressid int not null;

INSERT INTO dbversion(version) VALUES(283);

COMMIT TRAN