-- Remove and delete permissions for deleted Velocity document generation controllers
-- Galen Beck 2018-12-05

USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[permission] WHERE permission = 'QUOTE_DOCS';
DELETE FROM [dbo].[permission] WHERE permission = 'TP_QUOTE_DOCS';
DELETE FROM [dbo].[permission] WHERE permission = 'TP_QR_DOCS';
DELETE FROM [dbo].[permission] WHERE permission = 'PRINT_DEL_NOTE';

DELETE FROM [dbo].[permission] WHERE permission = 'COST_CENTRE_DOC';
DELETE FROM [dbo].[permission] WHERE permission = 'PURCHASE_ORDER_DOC';


INSERT INTO dbversion(version) VALUES(361);

COMMIT TRAN