-- Inserts 8 missing French service type long descriptions into system
-- Author Galen Beck - 2016-10-10
-- Note COMMIT at end, change to ROLLBACK TRAN to test

USE [spain]
GO

BEGIN TRAN

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypelongnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = 1)
	INSERT INTO [servicetypelongnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (1,'fr_FR','Vérification accrédité en laboratoire');
GO

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypelongnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = 2)
	INSERT INTO [servicetypelongnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (2,'fr_FR','Vérification standard en laboratoire');
GO

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypelongnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = 3)
	INSERT INTO [servicetypelongnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (3,'fr_FR','Autre');
GO

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypelongnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = 4)
	INSERT INTO [servicetypelongnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (4,'fr_FR','Vérification accrédité sur site');
GO

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypelongnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = 5)
	INSERT INTO [servicetypelongnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (5,'fr_FR','Vérification standard sur site');
GO

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypelongnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = 6)
	INSERT INTO [servicetypelongnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (6,'fr_FR','Réparation');
GO

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypelongnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = 7)
	INSERT INTO [servicetypelongnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (7,'fr_FR','Vérification accrédité externe');
GO

IF NOT EXISTS 
	(SELECT 1 FROM [servicetypelongnametranslation] WHERE 
	 [locale] = 'fr_FR' AND [servicetypeid] = 8)
	INSERT INTO [servicetypelongnametranslation] ([servicetypeid],[locale],[translation])
     VALUES (8,'fr_FR','Vérification standard externe');

GO

COMMIT TRAN;