-- Script 199 - Prevents sourcedby from being null (breaks quotation home if so)

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[quotation]
   SET [sourcedby] = [createdby] WHERE sourcedby is null;

GO

DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_1_29_36_31_37]

DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_29_36_31_37]

DROP INDEX [IDX_quotation_sourcedby] ON [dbo].[quotation]
GO

ALTER TABLE [dbo].[quotation] ALTER COLUMN [sourcedby] int not null;
GO

/****** Object:  Index [IDX_quotation_sourcedby]    Script Date: 11/12/2017 15:06:27 ******/
CREATE NONCLUSTERED INDEX [IDX_quotation_sourcedby] ON [dbo].[quotation]
(
	[sourcedby] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

INSERT INTO dbversion (version) VALUES (199);

COMMIT TRAN