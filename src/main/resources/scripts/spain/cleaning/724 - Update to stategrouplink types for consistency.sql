-- Script to correct two state group links
-- Galen Beck 2020-11-09

-- For the dashboard related stategroup links, the "type" of the stategroup links needs to be consistent with the type definition of the stategroup.
-- For the stategroup 35 (Logistics : 3 - Other) the link type is CURRENT_SUBDIV (based on the current address of the job)
-- For the stategroup 49 (Logistics : 4 - In Transit) the link type is ALLOCATED_SUBDIV (based on the owning subdivision of the job)
-- If these are inconsistent, clicking through to the "header" of a stategroup will give results which are inconsistent with 
--   clicking through to the "state" and the dashboard results may be incorrect.

-- (a) Existing issue noted where stategrouplink type for state "Awaiting completion of additional demands" was ALLOCATED_SUBDIV (update)
--   - we change the link type to CURRENT_SUBDIV
-- (b) New issue from script 722 where state "Awaiting confirmation of client receipt" was CURRENT_SUBDIV and in group 35
--   - we change the link type to ALLOCATED_SUBDIV and move to group 49 (Logistics : 4 - In Transit) because the job item will have no current address

USE [atlas]
GO

BEGIN TRAN

DECLARE @state_id_additional_demands INT,
        @state_id_client_receipt INT

SELECT @state_id_client_receipt = stateid FROM dbo.itemstate WHERE description = 'Awaiting confirmation of client receipt';

SELECT @state_id_additional_demands = stateid FROM dbo.itemstate WHERE description = 'Awaiting completion of additional demands';

-- (a) Fix existing issue with additional demands

UPDATE [dbo].[stategrouplink]
   SET [type] = 'CURRENT_SUBDIV'
   WHERE [groupid] = 35 AND [stateid] = @state_id_additional_demands;

-- (b) Move and fix link type for client receipt

UPDATE [dbo].[stategrouplink]
   SET [type] = 'ALLOCATED_SUBDIV', [groupid] = 49
   WHERE [groupid] = 35 AND [stateid] = @state_id_client_receipt;

INSERT INTO dbversion(version) VALUES(724);

COMMIT TRAN