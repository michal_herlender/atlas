/*
Missing Index Details from SQLQuery33.sql - BELO-PC\SQLEXPRESS2014.spain (BeLo-PC\Galen (80))
The Query Processor estimates that implementing the following index could improve the query cost by 98.9861%.
*/


USE [spain]
GO
CREATE NONCLUSTERED INDEX [IDX_instrumentcomplementaryfield_plantid]
ON [dbo].[instrumentcomplementaryfield] ([plantid])

GO

