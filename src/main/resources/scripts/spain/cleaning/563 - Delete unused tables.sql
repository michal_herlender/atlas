USE [atlas]

BEGIN TRAN

DROP TABLE companyinstructionlinkallocated;

DROP TABLE subdivinstructionlinkallocated;

DROP TABLE contactinstructionlinkallocated;

INSERT INTO dbversion(version) VALUES(563);

COMMIT TRAN