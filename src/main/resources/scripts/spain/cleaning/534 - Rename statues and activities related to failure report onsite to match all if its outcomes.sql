USE [atlas]


BEGIN TRAN

UPDATE dbo.itemstate
	SET description='Onsite - Failure report completed'
	WHERE stateid=4090 
UPDATE dbo.itemstate
	SET description='Onsite - Awaiting completion of failure report'
	WHERE stateid=4091 

UPDATE dbo.itemstatetranslation
	SET [translation]='Vor-Ort - Fehlerbericht validiert'
	WHERE stateid=4088 AND locale='de_DE' AND [translation]='Vor-Ort - warten auf Validierung des Fehlerberichts' 
UPDATE dbo.itemstatetranslation
	SET [translation]='Onsite - Failure report validated'
	WHERE stateid=4088 AND locale='en_GB' AND [translation]='Onsite - awaiting failure report to be validated' 
UPDATE dbo.itemstatetranslation
	SET [translation]='En el sitio: informe de falla validado'
	WHERE stateid=4088 AND locale='es_ES' AND [translation]='En el sitio: se espera la validaci�n del informe de fallas' 
UPDATE dbo.itemstatetranslation
	SET [translation]='Sur site - Constat d''anomalie valid�'
	WHERE stateid=4088 AND locale='fr_FR' AND [translation]='Sur site - En attente de validation du constat d''anomalie' 
UPDATE dbo.itemstatetranslation
	SET [translation]='Vor-Ort - Fehlerbericht fertiggestellt'
	WHERE stateid=4090 AND locale='de_DE' AND [translation]='Vor-Ort - Fehlerbericht f�r Reparatur/Justage fertiggestellt' 
UPDATE dbo.itemstatetranslation
	SET [translation]='Onsite - Failure report completed'
	WHERE stateid=4090 AND locale='en_GB' AND [translation]='Onsite - repair / adjustment fault report completed' 
UPDATE dbo.itemstatetranslation
	SET [translation]='In situ - informe de la incidencias terminado'
	WHERE stateid=4090 AND locale='es_ES' AND [translation]='In situ - informe de la incidencias de reparaci�n/ajuste terminado' 
UPDATE dbo.itemstatetranslation
	SET [translation]='Sur site - constat d''anomalie termin�'
	WHERE stateid=4090 AND locale='fr_FR' AND [translation]='Sur site - constat d''anomalie r�paration / ajustage termin�' 
UPDATE dbo.itemstatetranslation
	SET [translation]='Vor-Ort - Warten auf Fertigstellung des Fehlerberichts'
	WHERE stateid=4091 AND locale='de_DE' AND [translation]='Vor-Ort - PM muss repariert/justiert werden - warten auf Fertigstellung des Fehlerberichts' 
UPDATE dbo.itemstatetranslation
	SET [translation]='Onsite - Awaiting completion of failure report'
	WHERE stateid=4091 AND locale='en_GB' AND [translation]='Onsite - instrument requires repair / adjustment - awaiting completion of fault report' 
UPDATE dbo.itemstatetranslation
	SET [translation]='In situ - Esperando la realizaci�n del informe de incidencias'
	WHERE stateid=4091 AND locale='es_ES' AND [translation]='In situ - instrumento requiere reparaci�n/ajuste - esperando la realizaci�n del informe de incidencias' 
UPDATE dbo.itemstatetranslation
	SET [translation]='Sur site - Constat d''anomalie � produire'
	WHERE stateid=4091 AND locale='fr_FR' AND [translation]='Sur site - instrument n�cessitant une r�paration / un ajustage - constat d''anomalie � produire' 


INSERT INTO dbversion(version) VALUES(534);

COMMIT TRAN  