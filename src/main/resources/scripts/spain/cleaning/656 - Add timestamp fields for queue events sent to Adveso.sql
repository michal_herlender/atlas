use atlas
GO

BEGIN TRAN

ALTER TABLE dbo.notificationsystemqueue ADD createdon datetime2(7) NULL;

ALTER TABLE dbo.advesojobItemactivityqueue ADD createdon datetime2(7) NULL;

ALTER TABLE dbo.advesojobItemactivityqueue ADD lastModifiedBy INT;
ALTER TABLE dbo.advesojobItemactivityqueue ADD lastModified DATETIME2;


INSERT INTO dbversion(version) VALUES(656);

GO

COMMIT TRAN