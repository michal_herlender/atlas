-- Script to add servicetypeid as a field in parallel with caltype on certificate
-- These can be maintained in parallel until all caltype references can be removed from certificate

USE [atlas]
GO

BEGIN TRAN

alter table dbo.certificate 
    add servicetypeid int;

alter table dbo.certificate 
    add constraint FK_certificate_servicetype 
    foreign key (servicetypeid) 
    references dbo.servicetype;

GO

update certs
    set certs.servicetypeid = ct.servicetypeid
	from
	dbo.certificate certs
	join
	calibrationtype ct on certs.caltype = ct.caltypeid
GO

insert into dbversion(version) values(792);

COMMIT TRAN
