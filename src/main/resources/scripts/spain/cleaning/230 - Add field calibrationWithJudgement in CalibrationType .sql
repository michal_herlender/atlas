USE spain;

BEGIN TRAN;

	-- if field 'calibrationWithJudgement' exists
	IF EXISTS(SELECT 1 FROM sys.columns 
	          WHERE Name = N'calibrationWithJudgement'
	          AND Object_ID = Object_ID(N'dbo.calibrationtype'))
	BEGIN
		-- remove 'not null' constraint, set by previous script
	   ALTER TABLE dbo.calibrationtype ALTER COLUMN calibrationWithJudgement bit NULL
	   
	   	UPDATE dbo.calibrationtype
		set calibrationWithJudgement = null
	END
	ELSE
	BEGIN
		ALTER TABLE dbo.calibrationtype ADD calibrationWithJudgement bit
	END
	
	
	
	--set value of field calibrationWithJudgement
	UPDATE dbo.calibrationtype
	set calibrationWithJudgement = (
		select case when (st.shortname like '%-MV') then 0 else 1 end as calibrationWithJudgement
		from dbo.servicetype st
		where calibrationtype.servicetypeid = st.servicetypeid and st.shortname not in ('OTHER','SERVICE','M')
	)
	
	--INSERT INTO dbversion VALUES (230);

COMMIT TRAN;

