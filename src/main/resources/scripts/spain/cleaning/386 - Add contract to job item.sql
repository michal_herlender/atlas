use spain;

begin transaction;

alter table spain.dbo.jobitem
       add contractid int;


alter table dbo.jobitem
       add constraint FKrpll1q40ubdb6kpde427vnj2b
              foreign key (contractid)
                     references dbo.contract;

INSERT INTO dbversion(version) VALUES(386);

commit transaction;