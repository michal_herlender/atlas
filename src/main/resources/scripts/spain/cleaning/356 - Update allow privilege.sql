USE [spain]

Begin Tran

delete from dbo.permission where permission='MANAGE_USER_COMPANY';

insert into dbo.permission values (2,'MANAGE_USER_COMPANY');

insert into dbo.permission values (3,'ADMIN_MANAGE_USER_COMPANY');

INSERT INTO dbversion(version) VALUES(356);

COMMIT TRAN