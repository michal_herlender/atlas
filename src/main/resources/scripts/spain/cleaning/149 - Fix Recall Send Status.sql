-- Script to fix effects of bug where recall send status was not sent
-- Will need to run after running a recall before sending a subsequent recall
-- GB 2017-05-25

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[recalldetail]
   SET [recallsendstatus] = 'SENT'
 WHERE [error] = 0 AND pathtofile <> '' AND [recallsendstatus] = 'TO_SEND'

UPDATE [dbo].[recalldetail]
   SET [recallsendstatus] = 'ERROR'
 WHERE [error] = 1 AND pathtofile <> '' AND [recallsendstatus] = 'TO_SEND'

COMMIT TRAN

GO
