USE [atlas]

BEGIN TRAN

declare @ao1 int; 
declare @os1 int; 

-- add action outcome : 'No action required' for activity '18-Unit inspected by engineer'
INSERT INTO dbo.actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
	VALUES (0,'No action required',0,18,1,'engineer_inspection','NO_ACTION_REQUIRED');
select @ao1 = max(id) from dbo.actionoutcome;
	
-- translations
INSERT INTO atlas.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@ao1,'en_GB','No action required');
INSERT INTO atlas.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@ao1,'es_ES','Ninguna acci�n requerida');
INSERT INTO atlas.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@ao1,'fr_FR','Pas d''action n�cessaire');
INSERT INTO atlas.dbo.actionoutcometranslation (id,locale,[translation])
	VALUES (@ao1,'de_DE','Keine Aktion erforderlich');
	
-- action outcomestatus going to lookup '59-Despatch Approval required?'
INSERT INTO dbo.outcomestatus (defaultoutcome,lookupid)	VALUES (0,59);
select @os1 = max(id) from dbo.outcomestatus;

-- lookup result for lookup '12 - Lookup_InitialWorkRequired (inhouse after initial engineer inspection)'
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (12,@os1,7);
	
-- lookup result for lookup '11 - Lookup_InitialWorkRequired (inhouse after initial fault report)'
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (11,@os1,7);
	
-- lookup result for lookup '10 - Lookup_InitialWorkRequired (inhouse after contract review or bypass)'
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (10,@os1,7);


INSERT INTO dbversion(version) VALUES(537);

COMMIT TRAN

