use atlas;
go

begin tran

ALTER TABLE dbo.jobitem ALTER COLUMN accessoryFreeText varchar(500) ;

insert into dbversion values (831)

commit tran