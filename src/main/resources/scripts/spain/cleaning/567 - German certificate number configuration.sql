USE [atlas];

BEGIN TRAN

ALTER TABLE [dbo].[numberformat] DROP CONSTRAINT [UK_q1y6p9butly0fm1e8bup8fdva]
GO

INSERT INTO [dbo].[numberformat] (format, global, universal, lastModified, lastModifiedBy, numerationType, orgid)
VALUES ('s-yy000000', 0, 0, '2020-01-29', 31485, 'CERTIFICATE', 6681)

INSERT INTO dbversion (version) VALUES (567)

COMMIT TRAN