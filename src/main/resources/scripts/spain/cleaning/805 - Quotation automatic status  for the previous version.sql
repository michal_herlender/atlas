USE [atlas]
Go


BEGIN TRAN

INSERT INTO dbo.basestatus
([type], defaultstatus, name, accepted, followingissue, followingreceipt, issued, requiringattention, rejected)
VALUES('quotation', 0, 'Replaced with new version', 0, 0, 0, 0, 0, 0);

DECLARE @statusId INT;
SELECT @statusId=MAX(statusid) FROM basestatus

INSERT INTO atlas.dbo.basestatusnametranslation
(statusid, locale, [translation])
VALUES(@statusId, 'en_GB', 'Replaced with new version');

INSERT INTO atlas.dbo.basestatusnametranslation
(statusid, locale, [translation])
VALUES(@statusId, 'fr_FR', 'Remplac� par une nouvelle version');


INSERT INTO atlas.dbo.basestatusnametranslation
(statusid, locale, [translation])
VALUES(@statusId, 'es_ES', 'Reemplazado con nueva versi�n');


INSERT INTO atlas.dbo.basestatusnametranslation
(statusid, locale, [translation])
VALUES(@statusId, 'de_DE', 'Ersetzt durch neue Version');


INSERT INTO dbversion(version) VALUES(805);

COMMIT TRAN


