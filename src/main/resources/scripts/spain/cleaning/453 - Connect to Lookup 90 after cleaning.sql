USE [spain];


BEGIN TRAN

	-- route activity '14-Unit cleaned' to lookup 90-Lookup_NextWorkRequirementByJobCompany
	UPDATE dbo.itemstate SET lookupid=90 WHERE stateid=14;
	
	-- route activity '175-Cleaning not required' to lookup 90-Lookup_NextWorkRequirementByJobCompany
	UPDATE dbo.itemstate SET lookupid=90 WHERE stateid=175;
	
	INSERT INTO dbversion (version) VALUES (453);

COMMIT TRAN