-- Script 171 - when running standalone schema update tool with Hibernate 5.1:
-- Corrects many mismatches detected where Hibernate wants to drop and create tables, mainly where
-- we have @Column(nullable=false) definitions but the fields were never made null in the database.
--
-- Note that some are more complex at this time (indexes exist so fields can't be modified, must drop/recreate index and stats)
-- In this case, I removed the nullable=false from the entity mapping and added a TODO to fix
-- Author Galen Beck - 2017-08-25
-- Tested / run mainly on the 2017-05025 database backup (takes ~20 sec to run)

USE [spain]
GO

BEGIN TRAN

ALTER TABLE [dbo].[actionoutcome] ALTER COLUMN [active] bit NOT NULL;
ALTER TABLE [dbo].[actionoutcome] ALTER COLUMN [defaultoutcome] bit NOT NULL;
ALTER TABLE [dbo].[actionoutcome] ALTER COLUMN [positiveoutcome] bit NOT NULL;
ALTER TABLE [dbo].[assetstatus] ALTER COLUMN [active] bit NOT NULL;

ALTER TABLE [dbo].[assettype] ALTER COLUMN [insttype] bit NOT NULL;

--Remove old constraint on audittriggerswitch (not currently used...)
ALTER TABLE [dbo].[audittriggerswitch] DROP CONSTRAINT [UQ__audittri__CC3728AE3E52440B];
ALTER TABLE [dbo].[audittriggerswitch] ALTER COLUMN [enabled] bit not null;

ALTER TABLE [dbo].[basestatus] ALTER COLUMN [defaultstatus] bit not null;
ALTER TABLE [dbo].[basestatus] ALTER COLUMN [accepted] bit not null;
ALTER TABLE [dbo].[basestatus] ALTER COLUMN [followingissue] bit not null;
ALTER TABLE [dbo].[basestatus] ALTER COLUMN [followingreceipt] bit not null;
ALTER TABLE [dbo].[basestatus] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[basestatus] ALTER COLUMN [requiringattention] bit not null;
ALTER TABLE [dbo].[basestatus] ALTER COLUMN [rejected] bit not null;

ALTER TABLE [dbo].[batchcalibration] ALTER COLUMN [choosenext] bit;
ALTER TABLE [dbo].[batchcalibration] ALTER COLUMN [passwordaccepted] bit;

ALTER TABLE [dbo].[bpo] ALTER COLUMN [active] bit;
ALTER TABLE [dbo].[bpo] ALTER COLUMN [expirwarningsent] bit;

ALTER TABLE [dbo].[calibrationprocess] ALTER COLUMN [actionafterissue] bit;
ALTER TABLE [dbo].[calibrationprocess] ALTER COLUMN [applicationcomponent] bit;
ALTER TABLE [dbo].[calibrationprocess] ALTER COLUMN [issueimmediately] bit;
ALTER TABLE [dbo].[calibrationprocess] ALTER COLUMN [quicksign] bit;
ALTER TABLE [dbo].[calibrationprocess] ALTER COLUMN [signingsupported] bit;
ALTER TABLE [dbo].[calibrationprocess] ALTER COLUMN [signonissue] bit;
ALTER TABLE [dbo].[calibrationprocess] ALTER COLUMN [uploadafterissue] bit;
ALTER TABLE [dbo].[calibrationprocess] ALTER COLUMN [uploadbeforeissue] bit;

ALTER TABLE [dbo].[calibrationtype] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[calibrationtype] ALTER COLUMN [accreditationspecific] bit not null;
ALTER TABLE [dbo].[calibrationtype] ALTER COLUMN [recalldateonlabel] bit;
-- callink.certissued participates in index; dropping for now.
DROP STATISTICS [dbo].[callink].[_dta_stat_322100188_3_8];
DROP INDEX [_dta_index_callink_7_322100188__K8_K1_K7_2_3_4_5_6_9] ON [dbo].[callink];
ALTER TABLE [dbo].[callink] ALTER COLUMN [certissued] bit not null;
ALTER TABLE [dbo].[callink] ALTER COLUMN [mainitem] bit;

ALTER TABLE [dbo].[calloffitem] ALTER COLUMN [active] bit not null;

ALTER TABLE [dbo].[calreq] ALTER COLUMN [publish] bit not null;
-- Lots of indexes on active, but not needed to get update to pass?

-- It seems a previous hibernate version added constraint >0; removing for now
--ALTER TABLE [dbo].[certificate] DROP CONSTRAINT [DF_certificate_intervalunit];
ALTER TABLE [dbo].[certificate] ALTER COLUMN [certpreviewed] bit;

-- It seems these 5 indexes use both fields, but they're looking obsolete.
DROP INDEX [_dta_index_company_7_1682105033__K17_K21_K24_K1_K25_K23_K20_K19_K18_K16_K15_2_3_4_5_6_7_8_9_10_11_12_13_14_22] ON [dbo].[company];
DROP INDEX [_dta_index_company_7_1682105033__K19_K1_K25_K20_K18_K15_K16_K17_K21_K23_K24_2_3_4_5_6_7_8_9_10_11_12_13_14_22] ON [dbo].[company];
DROP INDEX [_dta_index_company_7_1682105033__K16_K1_K25_K20_K19_K18_K15_K17_K21_K23_K24_K5_2_3_4_6_7_8_9_10_11_12_13_14_22] ON [dbo].[company];
DROP INDEX [_dta_index_company_7_1682105033__K1_K20_K25_K19_K18_K15_K16_K17_K21_K23_K24_2_3_4_5_6_7_8_9_10_11_12_13_14_22] ON [dbo].[company];
DROP INDEX [_dta_index_company_7_1682105033__K15_K1_K17_K21_K24_K16_K18_K19_K20_K23_K25_2_3_4_5_6_7_8_9_10_11_12_13_14_22] ON [dbo].[company];

ALTER TABLE [dbo].[company] ALTER COLUMN [onstop] bit;
ALTER TABLE [dbo].[company] ALTER COLUMN [partnercomp] bit;

ALTER TABLE [dbo].[companyaccreditation] ALTER COLUMN [emailalert] bit;

ALTER TABLE [dbo].[componentdoctypetemplate] ALTER COLUMN [defaulttemplate] bit;
ALTER TABLE [dbo].[connecteddevice] ALTER COLUMN [online] bit;
ALTER TABLE [dbo].[connecteddevice] ALTER COLUMN [readbytes] bit;
--Not done - contact has 3 indexes related to active - but is OK!
-- TODO - costs_adjustment +_ other costs have index with active and autoset!!!

ALTER TABLE [dbo].[courier] ALTER COLUMN [defaultcourier] bit not null;
ALTER TABLE [dbo].[courierdespatchtype] ALTER COLUMN [defaultforcourier] bit not null;
ALTER TABLE [dbo].[creditnote] ALTER COLUMN [creditnoteno] varchar(30) not null;
ALTER TABLE [dbo].[creditnote] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[creditnoteitem] ALTER COLUMN [partofbaseunit] bit not null;

ALTER TABLE [dbo].[creditnotestatus] ALTER COLUMN [defaultstatus] bit not null;
ALTER TABLE [dbo].[creditnotestatus] ALTER COLUMN [oninsert] bit not null;
ALTER TABLE [dbo].[creditnotestatus] ALTER COLUMN [onissue] bit not null;
ALTER TABLE [dbo].[creditnotestatus] ALTER COLUMN [requiresattention] bit not null;
ALTER TABLE [dbo].[defaultnote] ALTER COLUMN [publish] bit not null;
ALTER TABLE [dbo].[defaultstandard] ALTER COLUMN [enforceuse] bit not null;
ALTER TABLE [dbo].[defect] ALTER COLUMN [calsealbroken] bit;
ALTER TABLE [dbo].[delivery] ALTER COLUMN [sigcaptured] bit;
ALTER TABLE [dbo].[deliveryitem] ALTER COLUMN [despatched] bit not null;
ALTER TABLE [dbo].[departmentrole] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[departmentrole] ALTER COLUMN [manage] bit not null;
ALTER TABLE [dbo].[description] ALTER COLUMN [alwaysbasefirst] bit not null;
-- Dropping index on entityid and component for now
DROP INDEX [_dta_index_email_7_887674210__K5_K4_1_2_3_6_7_8_9_10_11] ON [dbo].[email]
ALTER TABLE [dbo].[email] ALTER COLUMN [publish] bit not null;
ALTER TABLE [dbo].[engineerallocation] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[exception] ALTER COLUMN [reviewed] bit not null;
ALTER TABLE [dbo].[faultreport] ALTER COLUMN [precert] bit not null;
ALTER TABLE [dbo].[faultreport] ALTER COLUMN [prerecord] bit not null;
ALTER TABLE [dbo].[faultreport] ALTER COLUMN [awaitingfinalisation] bit not null;
ALTER TABLE [dbo].[faultreport] ALTER COLUMN [ber] bit not null;

EXEC sp_rename 'dbo.faultreportaction.lastmodified', 'lastModified', 'COLUMN';
EXEC sp_rename 'dbo.faultreportdescription.lastmodified', 'lastModified', 'COLUMN';
EXEC sp_rename 'dbo.faultreportinstruction.lastmodified', 'lastModified', 'COLUMN';

ALTER TABLE [dbo].[faultreportaction] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[faultreportdescription] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[faultreportinstruction] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[faultreportinstruction] ALTER COLUMN [showontpdelnote] bit not null;

DROP STATISTICS [dbo].[hire].[_dta_stat_1000507489_16_31_33];
ALTER TABLE [dbo].[hire] ALTER COLUMN [accountsvarified] bit not null;
ALTER TABLE [dbo].[hire] ALTER COLUMN [enquiry] bit not null;
ALTER TABLE [dbo].[hire] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[hireaccessory] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[hireitem] ALTER COLUMN [offhire] bit not null;
ALTER TABLE [dbo].[hireitem] ALTER COLUMN [partofbaseunit] bit not null;
ALTER TABLE [dbo].[hireitem] ALTER COLUMN [suspended] bit not null;
ALTER TABLE [dbo].[hireitemaccessory] ALTER COLUMN [returned] bit not null;
-- May need to recreate constraint
--ALTER TABLE [dbo].[hook] DROP CONSTRAINT [UQ__hook__72E12F1B6C390A4C]
ALTER TABLE [dbo].[hook] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[hook] ALTER COLUMN [alwayspossible] bit not null;
ALTER TABLE [dbo].[hook] ALTER COLUMN [beginactivity] bit not null;
ALTER TABLE [dbo].[hook] ALTER COLUMN [completeactivity] bit not null;
ALTER TABLE [dbo].[hookactivity] ALTER COLUMN [beginactivity] bit not null;
ALTER TABLE [dbo].[hookactivity] ALTER COLUMN [completeactivity] bit not null;
-- instmodel and instrumenthistory have multiple indexes - not updated at this time
ALTER TABLE [dbo].[instrumentstoragetype] ALTER COLUMN [recall] bit not null;
ALTER TABLE [dbo].[instrumentstoragetype] ALTER COLUMN [defaulttype] bit not null;
ALTER TABLE [dbo].[instrumentusagetype] ALTER COLUMN [recall] bit not null;
ALTER TABLE [dbo].[instrumentusagetype] ALTER COLUMN [defaulttype] bit not null;
ALTER TABLE [dbo].[invoice] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[invoicedoctypetemplate] ALTER COLUMN [defaulttemplate] bit;
ALTER TABLE [dbo].[invoiceitem] ALTER COLUMN [partofbaseunit] bit not null;
ALTER TABLE [dbo].[invoicestatus] ALTER COLUMN [defaultstatus] bit not null;
ALTER TABLE [dbo].[invoicestatus] ALTER COLUMN [oninsert] bit not null;
ALTER TABLE [dbo].[invoicestatus] ALTER COLUMN [onissue] bit not null;
ALTER TABLE [dbo].[invoicestatus] ALTER COLUMN [requiresattention] bit not null;
ALTER TABLE [dbo].[invoicestatus] ALTER COLUMN [approved] bit not null;
ALTER TABLE [dbo].[invoicestatus] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[invoicetype] ALTER COLUMN [addtoaccount] bit not null;
ALTER TABLE [dbo].[invoicetype] ALTER COLUMN [deftosinglecost] bit not null;
ALTER TABLE [dbo].[invoicetype] ALTER COLUMN [includeonjobsheet] bit not null;
ALTER TABLE [dbo].[itemstate] ALTER COLUMN [active] bit not null;
ALTER TABLE [dbo].[itemstate] ALTER COLUMN [retired] bit not null;

ALTER TABLE [dbo].[jobcosting] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[jobcostingitem] ALTER COLUMN [partofbaseunit] bit not null;
ALTER TABLE [dbo].[jobstatus] ALTER COLUMN [complete] bit not null;
ALTER TABLE [dbo].[jobstatus] ALTER COLUMN [defaultstatus] bit not null;
ALTER TABLE [dbo].[jobstatus] ALTER COLUMN [readytoinvoice] bit not null;
-- Index on id and regdate - recreate if needed after lastModified addition?
DROP INDEX [_dta_index_purchaseorder_7_1750297295__K9D_K1D_2_3_4_5_6_7_8_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27] ON [dbo].[purchaseorder]
ALTER TABLE [dbo].[purchaseorder] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[procedurecategory] ALTER COLUMN [departmentdefault] bit not null;
ALTER TABLE [dbo].[purchaseorderitem] ALTER COLUMN [partofbaseunit] bit not null;
ALTER TABLE [dbo].[purchaseorderstatus] ALTER COLUMN [approved] bit not null;
ALTER TABLE [dbo].[purchaseorderstatus] ALTER COLUMN [oncomplete] bit not null;
ALTER TABLE [dbo].[purchaseorderstatus] ALTER COLUMN [oncancel] bit not null;
ALTER TABLE [dbo].[purchaseorderstatus] ALTER COLUMN [defaultstatus] bit not null;
ALTER TABLE [dbo].[purchaseorderstatus] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[purchaseorderstatus] ALTER COLUMN [oninsert] bit not null;
ALTER TABLE [dbo].[purchaseorderstatus] ALTER COLUMN [onissue] bit not null;
ALTER TABLE [dbo].[purchaseorderstatus] ALTER COLUMN [requiresattention] bit not null;

-- Has two indices on [regdate],[currencyid],[id],[createdby],personid], recreate later if needed?
DROP INDEX [_dta_index_quotation_7_1010818663__K9_K29_K1_K28_K31_2_3_4_5_6_7_8_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_30_32_] ON [dbo].[quotation]
DROP INDEX [_dta_index_quotation_7_1010818663__K9_K28_K31_K29_K1_2_3_4_5_6_7_8_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_30_32_] ON [dbo].[quotation]
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_1_22_26_6_16_9_5];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_1_5_9_22_26_6];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_1_6_16];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_22_31_6_16_10];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_22_6_16_10];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_31_1_5_9_22_26_6_16];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_31_1_6_16_10_22];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_31_6_16_10];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_6_16_10_1_22];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_9_1_22_26_6];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_9_31_1_22_26_6_16];
ALTER TABLE [dbo].[quotation] ALTER COLUMN [issued] bit not null;

-- Has multiple indexes on quotationitem, recreate later if needed?
DROP INDEX [_dta_index_quotationitem_7_1186819290__K22_K20_K1_K24_K26_K21_K27_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_23_25] ON [dbo].[quotationitem]
DROP INDEX [_dta_index_quotationitem_7_1186819290__K27_K1_K20_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_21_22_23_24_25_26] ON [dbo].[quotationitem]
DROP INDEX [_dta_index_quotationitem_7_1186819290__K21_K20_K1_K24_K22_K26_K27_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_23_25] ON [dbo].[quotationitem]
DROP INDEX [_dta_index_quotationitem_7_1186819290__K1_K24_K22_K26_K21_K20_K27_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_23_25] ON [dbo].[quotationitem]
DROP INDEX [_dta_index_quotationitem_7_1186819290__K22_K26_K25_K1_K20_K27_K24_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_21_23] ON [dbo].[quotationitem]
DROP INDEX [_dta_index_quotationitem_7_1186819290__K21_K20_K1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_22_23_24_25_26_27] ON [dbo].[quotationitem]
DROP STATISTICS [dbo].[quotationitem].[_dta_stat_1186819290_7_26]
ALTER TABLE [dbo].[quotationitem] ALTER COLUMN [partofbaseunit] bit not null;

ALTER TABLE [dbo].[quoteheading] ALTER COLUMN [headingno] int not null;
ALTER TABLE [dbo].[tpquotation] ALTER COLUMN [issued] bit not null;
ALTER TABLE [dbo].[tpquotationitem] ALTER COLUMN [partofbaseunit] bit not null;
ALTER TABLE [dbo].[tpquoterequest] ALTER COLUMN [issued] bit not null;

COMMIT TRAN