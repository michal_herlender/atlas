USE [atlas]
GO

BEGIN TRAN

--set stategrouplink for activity '4101 - Failure report is available to External Management System'
INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(57, 4101, 'ALLOCATED_SUBDIV');
GO

INSERT INTO dbversion(version) VALUES(497);

COMMIT TRAN