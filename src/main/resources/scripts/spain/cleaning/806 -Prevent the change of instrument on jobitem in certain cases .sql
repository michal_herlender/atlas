USE [atlas]
Go

BEGIN TRAN

DECLARE @PermissionGroupId INT;
SELECT @PermissionGroupId= id FROM dbo.permissiongroup WHERE name='GROUP_ENGINEER_MANAGEMENT';

INSERT INTO atlas.dbo.permission
(groupId, permission)
VALUES(@PermissionGroupId, 'JI_CHANGE_INSTRUMENT');

INSERT INTO dbversion(version) VALUES(806);

COMMIT TRAN
