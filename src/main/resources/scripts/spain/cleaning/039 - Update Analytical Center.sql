BEGIN TRAN

USE [spain];
GO

DECLARE @TEMID INT,@TICID INT

SELECT @TEMID = dbo.company.coid
FROM dbo.company
WHERE companycode = 'TEM'

SELECT @TICID = dbo.company.coid
FROM dbo.company
WHERE companycode = 'TIC'

UPDATE dbo.subdiv
SET analyticalCenter = '101'
WHERE subname = 'Madrid'
AND coid = @TEMID

UPDATE dbo.subdiv
SET analyticalCenter = '102'
WHERE subname = 'Zaragoza'
AND coid = @TEMID

UPDATE dbo.subdiv
SET analyticalCenter = '103'
WHERE subname = 'Bilbao'
AND coid = @TICID

UPDATE dbo.subdiv
SET analyticalCenter = '104'
WHERE subname = 'Vitoria'
AND coid = @TICID

ROLLBACK TRAN