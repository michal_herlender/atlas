/* UUpdated Analytical Center of Spain Business Companies - Author Tony Provost 2016-04-08 */

BEGIN TRAN 

USE [spain];
GO

DECLARE @CompanyID INT

SET @CompanyID = 6579
UPDATE dbo.subdiv 
SET analyticalcenter = 1
	,lastModified = GETDATE()
WHERE coid = @CompanyID
	AND lower(subname) = ('zaragoza')
UPDATE dbo.subdiv 
SET analyticalcenter = 2
	,lastModified = GETDATE()
WHERE coid = @CompanyID
	AND lower(subname) = ('madrid')	
	
SET @CompanyID = 6578
UPDATE dbo.subdiv 
SET analyticalcenter = 3
	,lastModified = GETDATE()
WHERE coid = @CompanyID
	AND lower(subname) = ('bilbao')

ROLLBACK TRAN