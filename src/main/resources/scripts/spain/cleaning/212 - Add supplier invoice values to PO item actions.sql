USE [spain]
GO

BEGIN TRAN

  alter table dbo.purchaseorderitemprogressaction
  add supplierinvoicegrossvalue numeric(10,2);

  alter table dbo.purchaseorderitemprogressaction
  add supplierinvoicenetvalue numeric(10,2);

  INSERT INTO dbversion (version) VALUES (212);

COMMIT TRAN