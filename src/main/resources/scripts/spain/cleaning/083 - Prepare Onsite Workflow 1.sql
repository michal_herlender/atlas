-- Prepares existing workflow statuses for a separate tab for onsite service
-- Author: Galen Beck - 2016-11-1

USE [spain]
GO

BEGIN TRAN

-- StateGroup table is no longer used - has been converted to Enum

ALTER TABLE [dbo].[workassignment] DROP CONSTRAINT [FK4EC66C9EDF0CD8FF];
ALTER TABLE [dbo].[stategrouplink] DROP CONSTRAINT [FKF4911D8837F1B2B0];

DROP TABLE [dbo].[stategroup]
GO

-- Repurpose unused IT tab for Onsite Service

UPDATE [dbo].[departmenttypetranslation]
   SET [translation] = 'On-site'
 WHERE [id] = 5 AND [locale] = 'en_GB';

UPDATE [dbo].[departmenttypetranslation]
   SET [translation] = 'Sur site'
 WHERE [id] = 5 AND [locale] = 'fr_FR';

UPDATE [dbo].[departmenttypetranslation]
   SET [translation] = 'In situ'
 WHERE [id] = 5 AND [locale] = 'es_ES';

-- Note: New stategroup enum values are as follows (now link to dept 5 - On-site)
-- Before - 40, During - 41, After - 42

IF NOT EXISTS (SELECT 1 FROM [dbo].[workassignment] WHERE [stategroupid] = 40 AND [depttypeid] = 5)
	INSERT INTO [dbo].[workassignment] ([depttypeid],[stategroupid])
     VALUES (5,40);

IF NOT EXISTS (SELECT 1 FROM [dbo].[workassignment] WHERE [stategroupid] = 41 AND [depttypeid] = 5)
	INSERT INTO [dbo].[workassignment] ([depttypeid],[stategroupid])
     VALUES (5,41);

IF NOT EXISTS (SELECT 1 FROM [dbo].[workassignment] WHERE [stategroupid] = 42 AND [depttypeid] = 5)
	INSERT INTO [dbo].[workassignment] ([depttypeid],[stategroupid])
     VALUES (5,42);

GO

-- 4047 (Awaiting Onsite Service) to Group 1, 4049 (Recorded Onsite Service - Awaiting Job Costing) to Group 3

IF NOT EXISTS (SELECT 1 FROM [dbo].[stategrouplink] WHERE [groupid] = 40 AND [stateid] = 4047)
	INSERT INTO [dbo].[stategrouplink] ([groupid],[stateid],[type])
     VALUES (40,4047,NULL);

IF NOT EXISTS (SELECT 1 FROM [dbo].[stategrouplink] WHERE [groupid] = 42 AND [stateid] = 4049)
	INSERT INTO [dbo].[stategrouplink] ([groupid],[stateid],[type])
     VALUES (42,4049,NULL);

GO

COMMIT TRAN

GO


