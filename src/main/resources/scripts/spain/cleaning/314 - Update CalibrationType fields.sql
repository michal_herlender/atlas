-- Corrects value of calibrationWithJudgement field after recent cal type / service type renaming

USE [spain]

BEGIN TRAN

-- Set the TC-SOC-IL, AC-SOC-IL, TC-SOC-OS, AC-SOC-OS to 1
-- FR : (VE-TR-LA, VE-AC-LA, VE-TR-SI, VE-AC-SI) to 1 (Verification in FR)
UPDATE calibrationtype set calibrationWithJudgement=1 WHERE servicetypeid=16
UPDATE calibrationtype set calibrationWithJudgement=1 WHERE servicetypeid=15
UPDATE calibrationtype set calibrationWithJudgement=1 WHERE servicetypeid=18
UPDATE calibrationtype set calibrationWithJudgement=1 WHERE servicetypeid=17
-- Set most others to 0
UPDATE calibrationtype set calibrationWithJudgement=0 WHERE servicetypeid=2
UPDATE calibrationtype set calibrationWithJudgement=0 WHERE servicetypeid=1
UPDATE calibrationtype set calibrationWithJudgement=0 WHERE servicetypeid=5
UPDATE calibrationtype set calibrationWithJudgement=0 WHERE servicetypeid=4
UPDATE calibrationtype set calibrationWithJudgement=0 WHERE servicetypeid=27
UPDATE calibrationtype set calibrationWithJudgement=0 WHERE servicetypeid=28
UPDATE calibrationtype set calibrationWithJudgement=0 WHERE servicetypeid=3
-- Set AC-EX, STD-EX, CLIENT, CLIENT-EX to 1 (as called Verification in FR)
UPDATE calibrationtype set calibrationWithJudgement=1 WHERE servicetypeid=7
UPDATE calibrationtype set calibrationWithJudgement=1 WHERE servicetypeid=8
UPDATE calibrationtype set calibrationWithJudgement=1 WHERE servicetypeid=29
UPDATE calibrationtype set calibrationWithJudgement=1 WHERE servicetypeid=30
UPDATE calibrationtype set calibrationWithJudgement=0 WHERE servicetypeid=31

GO

ALTER TABLE dbo.calibrationtype ALTER COLUMN calibrationWithJudgement bit not null;
go
ALTER TABLE dbo.calibrationtype ALTER COLUMN servicetypeid int not null;
go
ALTER TABLE dbo.calibrationtype ALTER COLUMN recallRequirementType varchar(20) not null;
go

INSERT INTO dbversion(version) VALUES(314);

COMMIT TRAN