-- Fixes workflow configuration issues from rename of INHOUSE_REPAIR to ERP_REPAIR and INHOUSE_ADJUSTMENT to ERP_ADJUSTMENT
-- Also updates old activity outcomes from (now inactive) activities 16 and 38
-- (Maybe, these activities shouldn't have fault report outcomes any longer)

USE [spain]
GO

BEGIN TRAN;

UPDATE [dbo].[actionoutcome]
   SET [value] = 'ERP_REPAIR'
 WHERE type = 'fault_report' and value = 'INHOUSE_REPAIR'
GO

UPDATE [dbo].[actionoutcome]
SET [value] = 'ERP_ADJUSTMENT'
 WHERE type = 'fault_report' and value = 'INHOUSE_ADJUSTMENT'
GO

UPDATE [dbo].[actionoutcome]
   SET [value] = 'ERP_REPAIR'
 WHERE type = 'fault_report' and value = 'REQUIRES_INHOUSE_REPAIR'
GO

UPDATE [dbo].[actionoutcome]
   SET [value] = 'THIRD_PARTY'
 WHERE type = 'fault_report' and value = 'REQUIRES_THIRD_PARTY_REPAIR'
GO

UPDATE [dbo].[actionoutcome]
SET [value] = 'ERP_ADJUSTMENT'
 WHERE type = 'fault_report' and value = 'REQUIRES_ADJUSTMENT'
GO

INSERT INTO dbversion(version) VALUES(317);

COMMIT TRAN;