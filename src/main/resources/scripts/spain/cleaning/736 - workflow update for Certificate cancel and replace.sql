USE [atlas]
GO

BEGIN TRAN

DECLARE @awaiting_po_status INT,
		@awaiting_internal_del_status INT,
		@awaiting_despatch_bus_status INT,
		@awaiting_selection_next_wr_status INT,
		@awaiting_completion_demands_status INT,
		@awaiting_informational_fr_status INT,
		@service_completed_status INT,
		@costing_issued_status INT,
		@costing_rejected_status INT,
		@inspection_costing_issued_status INT,
		@completed_at_same_address_status INT,
		@awaiting_approval_del_status INT,
		@awaiting_client_payment_status INT,
		@awaiting_despatch_to_client_status INT,
		@work_completed_status INT,
		@awaiting_client_receipt_confirmation_status INT;

	SELECT @awaiting_po_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting PO for inter company work';
	SELECT @awaiting_internal_del_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting internal delivery note';
	SELECT @awaiting_despatch_bus_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting despatch to business company for work';
	SELECT @awaiting_selection_next_wr_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting selection of next work requirement';
	SELECT @awaiting_completion_demands_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting completion of additional demands';
	SELECT @awaiting_informational_fr_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting informational failure report to be sent';
	SELECT @service_completed_status = stateid FROM dbo.itemstate WHERE [description] = 'Service completed, awaiting job costing';
	SELECT @costing_issued_status = stateid FROM dbo.itemstate WHERE [description] = 'Costing issued - awaiting client approval';
	SELECT @costing_rejected_status = stateid FROM dbo.itemstate WHERE [description] = 'Client rejected initial costing - requires inspection costing';
	SELECT @inspection_costing_issued_status = stateid FROM dbo.itemstate WHERE [description] = 'Inspection costing issued - awaiting approval';
	SELECT @completed_at_same_address_status = stateid FROM dbo.itemstate WHERE [description] = 'Job item completed at same address as instrument address';
	SELECT @awaiting_approval_del_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting approval for delivery note creation';
	SELECT @awaiting_client_payment_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting client payment prior to despatch';
	SELECT @awaiting_despatch_to_client_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting despatch to client';
	SELECT @work_completed_status = stateid FROM dbo.itemstate WHERE [description] = 'Work completed, despatched to client';
	SELECT @awaiting_client_receipt_confirmation_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting confirmation of client receipt';
	
	INSERT INTO dbo.stategrouplink(groupid, stateid, [type])
	VALUES (77, @awaiting_po_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_internal_del_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_despatch_bus_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_selection_next_wr_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_completion_demands_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_informational_fr_status, 'ALLOCATED_SUBDIV'),
			(77, @service_completed_status, 'ALLOCATED_SUBDIV'),
			(77, @costing_issued_status, 'ALLOCATED_SUBDIV'),
			(77, @costing_rejected_status, 'ALLOCATED_SUBDIV'),
			(77, @inspection_costing_issued_status, 'ALLOCATED_SUBDIV'),
			(77, @completed_at_same_address_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_approval_del_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_client_payment_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_despatch_to_client_status, 'ALLOCATED_SUBDIV'),
			(77, @work_completed_status, 'ALLOCATED_SUBDIV'),
			(77, @awaiting_client_receipt_confirmation_status, 'ALLOCATED_SUBDIV');
	
	-- Add new activity 'Certificate cancel and replace'
	INSERT INTO dbo.itemstate([type],active,retired,[description],lookupid,actionoutcometype)
	VALUES ('workactivity', 1, 0, 'Certificate cancel and replace', NULL, NULL);
	
	DECLARE @new_activity INT;
	SELECT @new_activity = MAX(stateid) FROM dbo.itemstate;
	
	INSERT INTO dbo.itemstatetranslation(stateid,locale,translation)
	VALUES (@new_activity, 'en_GB', 'Certificate cancel and replace'),
			(@new_activity, 'fr_FR', 'Annulation et remplacement du certificat'),
			(@new_activity, 'es_ES', 'Cancelar y reemplazar certificado'),
			(@new_activity, 'de_DE', 'Zertifikat stornieren und ersetzen');
	
	-- Add new hook for the activity 'Certificate cancel and replace'
	INSERT INTO dbo.hook(active,activityid,alwayspossible,beginactivity,completeactivity,[name])
	VALUES (1, NULL, 0, 1, 1, 'start-certificate-replacement');
	
	DECLARE @new_hook INT;
	SELECT @new_hook = MAX(id) FROM dbo.hook;
	
	INSERT INTO dbo.hookactivity(beginactivity,completeactivity,hookid,activityid,stateid)
	VALUES (1, 1, @new_hook, @new_activity, @awaiting_po_status),
			(1, 1, @new_hook, @new_activity, @awaiting_internal_del_status),
			(1, 1, @new_hook, @new_activity, @awaiting_despatch_bus_status),
			(1, 1, @new_hook, @new_activity, @awaiting_selection_next_wr_status),
			(1, 1, @new_hook, @new_activity, @awaiting_completion_demands_status),
			(1, 1, @new_hook, @new_activity, @awaiting_informational_fr_status),
			(1, 1, @new_hook, @new_activity, @service_completed_status),
			(1, 1, @new_hook, @new_activity, @costing_issued_status),
			(1, 1, @new_hook, @new_activity, @costing_rejected_status),
			(1, 1, @new_hook, @new_activity, @inspection_costing_issued_status),
			(1, 1, @new_hook, @new_activity, @completed_at_same_address_status),
			(1, 1, @new_hook, @new_activity, @awaiting_approval_del_status),
			(1, 1, @new_hook, @new_activity, @awaiting_client_payment_status),
			(1, 1, @new_hook, @new_activity, @awaiting_despatch_to_client_status),
			(1, 1, @new_hook, @new_activity, @work_completed_status),
			(1, 1, @new_hook, @new_activity, @awaiting_client_receipt_confirmation_status);
	
	-- Add new outcomestatus for the new activity 'Certificate cancel and replace'
	DECLARE @awaiting_cal_status INT;
	SELECT @awaiting_cal_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting calibration';
	INSERT INTO dbo.outcomestatus(defaultoutcome,lookupid,activityid,statusid)
	VALUES (1, NULL, @new_activity, @awaiting_cal_status);
	
	-- Add new lookup result for 'Lookup_PreDeliveryRequirements' to deal with cancel and replace case
	-- the case when we already despatch the item to the client 
	-- GB 2020-11-23: Changed script below to remove duplicate variable declaration, as it would fail with the following error:
	-- Msg 134, Level 15, State 1, Line 106
	-- The variable name '@service_completed_status' has already been declared. Variable names must be unique within a query batch or stored procedure
	DECLARE -- @service_completed_status INT,
			@pre_delivery_lookup INT,
			@new_os INT;
	SELECT @service_completed_status = stateid FROM dbo.itemstate WHERE [description] = 'Work completed, despatched to client';
	
	INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,lookupid,statusid)
	VALUES (0, NULL, NULL, @service_completed_status);
	SELECT @new_os = MAX(id) FROM dbo.outcomestatus;
	SELECT @pre_delivery_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_PreDeliveryRequirements (after lab calibration - before costing check)';
	INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage)
	VALUES (NULL, @pre_delivery_lookup, @new_os, NULL, 3);

	INSERT INTO dbversion(version) VALUES(736);

COMMIT TRAN


