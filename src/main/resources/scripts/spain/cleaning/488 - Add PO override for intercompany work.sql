-- Script 488 - allows a manual override from state 4074 when no PO required (e.g. multiple work requirements)
-- Galen Beck - 2019-08-10
USE [atlas]
GO

BEGIN TRAN

-- add activity and link it to state 4074 - Awaiting PO for intercompany work
INSERT INTO dbo.itemstate ([type],active,description,retired,lookupid) VALUES ('workactivity',1,'No PO required for intercompany work',0,null);
GO

declare @activityid int;
declare @stateid int;

select @stateid = stateid from itemstate where description = 'Awaiting PO for inter company work';
select @activityid = max(stateid) from dbo.itemstate;

-- next activity (manual)
INSERT INTO dbo.nextactivity (manualentryallowed,activityid,statusid) VALUES (1,@activityid,@stateid);

-- translations
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid,'de_DE','Keine Bestellung f�r Inter-Company Auftrag ben�tigt');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid,'en_GB','No PO required for intercompany work') ;
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid,'es_ES','Ning�n orden de compra requerido para el trabajo inter empresa');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])	VALUES (@activityid,'fr_FR','Pas de bon de commande requise pour le travail inter-soci�t�');
GO

INSERT INTO dbversion(version) VALUES(488);

COMMIT TRAN