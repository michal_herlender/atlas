USE [spain];
GO

BEGIN TRAN

SET IDENTITY_INSERT [systemdefaultgroup] ON
GO

INSERT INTO [systemdefaultgroup]
(id, description, dname)
VALUES (11, 'Working time settings', 'Business - Working Time')

INSERT INTO [systemdefaultgroupdescriptiontranslation]
(id, locale, translation)
VALUES
	(11, 'en_GB', 'Working time settings'),
	(11, 'de_DE', 'Einstellungen zur Arbeitszeit');

INSERT INTO [systemdefaultgroupnametranslation]
(id, locale, translation)
VALUES
	(11, 'en_GB', 'Business - Working Time'),
	(11, 'de_DE', 'Intern - Arbeitszeit');
GO

SET IDENTITY_INSERT [systemdefaultgroup] OFF
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
	(34, 'double', 'Defines working hours per day for an employee', 'Working hours per day', '8', 24, 0, 11, 1),
	(35, 'other', 'Defines working days for an employee', 'Working days', 'MONDAY_TO_FRIDAY', 0, 0, 11, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (34, 'en_GB', 'Defines working hours per day for an employee'),
  (34, 'es_ES', 'Define hora de trabajo al día'),
  (34, 'fr_FR', 'Définit heure de travail par jour'),
  (34, 'de_DE', 'Bestimmt die Arbeitsstunden pro Tag für einen Angestellten'),
  (35, 'en_GB', 'Defines working days for an employee'),
  (35, 'es_ES', 'Define días de trabajo'),
  (35, 'fr_FR', 'Définit jours de travail'),
  (35, 'de_DE', 'Bestimmt die Arbeitstage für einen Angestellten');

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0,34), (1, 34), (2, 34), (0,35), (1, 35), (2, 35);
GO

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (5, 34), (5, 35);

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (34, 'en_GB', 'Working hours per day'),
  (34, 'es_ES', 'Hora de trabajo al día'),
  (34, 'fr_FR', 'Heure de travail par jour'),
  (34, 'de_DE', 'Arbeitsstunden pro Tag'),
  (35, 'en_GB', 'Working days'),
  (35, 'es_ES', 'Días de trabajo'),
  (35, 'fr_FR', 'Jours de travail'),
  (35, 'de_DE', 'Arbeitstage');
GO

create table dbo.permission (
    groupId int not null,
    permission varchar(255)
);

create table dbo.permissiongroup (
    id int identity not null,
    primary key (id)
);

create table dbo.perrole (
    id int identity not null,
    primary key (id)
);

create table dbo.rolegroup (
    roleId int not null,
    groupId int not null
);

alter table dbo.timesheetentry 
    add fulltime bit not null;

create table dbo.timesheetvalidation (
    id int identity not null,
    lastModified datetime2 not null,
    carryover bigint not null,
    registeredtime bigint,
    rejectionreason varchar(255),
    status int not null,
    validatedon date,
    week int not null,
    workingtime bigint,
    year int,
    lastModifiedBy int,
    employeeid int not null,
    validatedby int,
    primary key (id)
);

alter table dbo.timesheetvalidation 
    add constraint UK_timesheetvalidation_employee_week_year unique (employeeid, week, year);

alter table dbo.permission 
    add constraint FK_permission_permissiongroup 
    foreign key (groupId) 
    references dbo.permissiongroup;

alter table dbo.rolegroup 
    add constraint FK_rolegroup_group 
    foreign key (groupId) 
    references dbo.permissiongroup;

alter table dbo.rolegroup 
    add constraint FK_rolegroup_role 
    foreign key (roleId) 
    references dbo.perrole;

alter table dbo.timesheetvalidation 
    add constraint FKsgp3wushxdwrp1nh4hr6662pk 
    foreign key (lastModifiedBy) 
    references dbo.contact;

alter table dbo.timesheetvalidation 
    add constraint FK_timesheetvalidation_employee 
    foreign key (employeeid) 
    references dbo.contact;

alter table dbo.timesheetvalidation 
    add constraint FK_timesheetvalidation_validatedby 
    foreign key (validatedby) 
    references dbo.contact;

INSERT INTO stategrouplink(groupid, stateid, type)
VALUES
	(48, 26, 'ALLOCATED_SUBDIV'),
	(48, 57, 'ALLOCATED_SUBDIV'),
	(48, 61, 'ALLOCATED_SUBDIV'),
	(48, 68, 'ALLOCATED_SUBDIV');

INSERT INTO dbversion(version) VALUES(261);

COMMIT TRAN