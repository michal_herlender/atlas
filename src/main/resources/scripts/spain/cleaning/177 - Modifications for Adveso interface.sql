use spain;

BEGIN TRAN

-- [add queue table] ----------------------
CREATE TABLE advesojobItemactivityqueue (
	id int NOT NULL IDENTITY(1,1),
	issent bit DEFAULT ((0)),
	sentOn datetime,
	isreceived bit,
	receivedOn datetime,
	istreated bit,
	treatedOn datetime,
	lasterrormessage varchar(200),
	lasterrormessageOn datetime,
	modifiedOn datetime,
	jobitemactionid int NOT NULL FOREIGN KEY REFERENCES dbo.jobitemaction(id)
) 

-- [add schedule job] ----------------------
INSERT INTO [dbo].[scheduledtask]
           ([active]
		   ,[lastrunsuccess]
           ,[classname]
           ,[description]
           ,[methodname]
           ,[quartzjobname]
           ,[taskname])
     VALUES
           (1
		   ,0
           ,'org.trescal.cwms.rest.adveso.schedule.AdvesoSyncActivitiesScheduledJob'
           ,'sync activities to adveso'
           ,'syncActivities'
           ,'jobDetail_SynchronizeActivitiesToAdveso'
           ,'Synchronize JobItem Activities To Adveso')


COMMIT TRAN