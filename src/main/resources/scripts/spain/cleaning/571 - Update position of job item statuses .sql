USE [atlas]
Go

BEGIN TRAN

DECLARE @itemState1 INT,
		@itemState2 INT;

SELECT @itemState1=its.stateid from itemstate its where its.description like 'Service continued on new job item, awaiting final costing on old job item'
SELECT @itemState2=its.stateid from itemstate its where its.description like 'Costing issued for repair - awaiting client decision for repair'

update stategrouplink set groupid=30 where stateid=@itemState1 and groupid=32

update stategrouplink set groupid=31 where stateid=@itemState2 and groupid=32


INSERT INTO dbversion(version) VALUES (571);

COMMIT TRAN