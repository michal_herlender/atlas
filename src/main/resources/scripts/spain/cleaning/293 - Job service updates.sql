use [spain];

begin tran

alter table dbo.jobexpenseitem
	add invoiceable bit
	
create table dbo.permissiongrouptranslation (
	permissiongroup int not null,
	locale varchar(255) not null,
	translation nvarchar(1000) not null
);

create table dbo.userrole (
	id int identity not null,
	role int not null,
	subdiv int not null,
	userName varchar(20) not null,
	primary key (id)
);

alter table dbo.permissiongrouptranslation
	add constraint FK_permissiongrouptranslation_permissiongroup
	foreign key (permissiongroup)
	references dbo.permissiongroup;

alter table dbo.userrole
	add constraint FK_userrole_role
	foreign key (role)
	references dbo.perrole;

alter table dbo.userrole
	add constraint FK_userrole_subdiv
	foreign key (subdiv)
	references dbo.subdiv;

alter table dbo.userrole
	add constraint FK_userrole_userName
	foreign key (userName)
	references dbo.users;

insert into dbversion(version) values(293);

commit tran