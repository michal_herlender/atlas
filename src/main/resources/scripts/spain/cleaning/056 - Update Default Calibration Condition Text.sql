-- Author: Galen Beck -- 2016-08-19
-- Updates default text used for quotation disclaimer by calibration type for Madrid

USE [spain]
GO

-- cal type 1 - AC-EL
-- cal type 2 - ST-EL
-- cal type 25 - AC-IS
-- cal type 29 - ST-IS

UPDATE [dbo].[defaultquotationcalibrationcondition]
   SET [conditiontext] = '</br></br><p><strong>*ST-EL= Calibración no acreditada en laboratorio no cubierta por la acreditación ENAC ni por los acuerdos internacionales</strong></p>'
 WHERE [caltypeid] = 1;

UPDATE [dbo].[defaultquotationcalibrationcondition]
   SET [conditiontext] = '</br></br><p><strong>*AC-EL= Calibración acreditada en laboratorio bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC)</strong></p>'
 WHERE [caltypeid] = 2;

UPDATE [dbo].[defaultquotationcalibrationcondition]
   SET [conditiontext] = '</br></br><p><strong>*ST-IS= Calibración no acreditada in situ no cubierta por la acreditación ENAC ni por los acuerdos internacionales</strong></p>'
 WHERE [caltypeid] = 25;

UPDATE [dbo].[defaultquotationcalibrationcondition]
   SET [conditiontext] = '</br></br><p><strong>*AC-IS= Calibración acreditada in situ bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC)</strong></p>'
 WHERE [caltypeid] = 29;

GO