BEGIN TRAN

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
  (25
    , 'boolean'
    , 'Defines if customer is able to mark certificates as obsolete via the portal'
    , 'Obsolete Certificate Functionality'
    , 'false', 0, 0, 9, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (25, 'en_GB', 'Defines if customer is able to mark certificates as obsolete via the portal'),
  (25, 'en_ES', 'Define si el cliente puede marcar los certificados como obsoletos a través del portal'),
  (25, 'fr_FR', 'Définit si le client peut marquer les certificats comme obsolètes via le portail');

-- Enables system default to be configurable at only company level

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0, 25);
GO

-- Enables for clients only

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (1, 25);

-- Add translations

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (25, 'en_GB', 'Obsolete Certificate Functionality'),
  (25, 'es_ES', 'Funcionalidad de certificado obsoleto'),
  (25, 'fr_FR', 'Fonctionnalité de certificat obsolète');
GO

INSERT INTO dbversion (version) VALUES (233);

-- Change to COMMIT when successfully tested

COMMIT TRAN