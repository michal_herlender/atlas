-- DEV-2673
-- 
-- Removes entry from script 824 (state was assignied to INSPECTIONTRANSIT state group 39)
-- Assigns state for two missing states (IDs below are just EMEA prod and may differ)
-- 
-- 5005 - On hold – repair awaiting for supplier price quotation
-- (adding to dashboard group : CSR - 4 - Other)

-- 5012 - Awaiting confirmation of third party destination receipt; no return expected
-- (adding to dashboard group : Logistics - 4 - Other)
-- (adding to dashboard group : CSR - 4 - Other)


BEGIN TRAN

DECLARE @stateid_SPQ int;
DECLARE @stateid_TBDR int;

DECLARE @stategroup_INSPECTIONTRANSIT int = 39;
DECLARE @stategroup_CSOTHER int = 32;
DECLARE @stategroup_IN_TRANSIT int = 49;
DECLARE @stategroup_TPWORK int = 37;

SELECT @stateid_SPQ = it.stateid FROM itemstate it 
	WHERE it.description = 'On hold – repair awaiting for supplier price quotation';
SELECT @stateid_TBDR = it.stateid FROM itemstate it
	WHERE it.description = 'Awaiting confirmation of third party destination receipt; no return expected';

PRINT '@stateid_SPQ = '+ CAST(@stateid_SPQ as Varchar(4));
PRINT '@stateid_TBDR = '+ CAST(@stateid_TBDR as Varchar(4));

DELETE FROM stategrouplink where groupid = 39 and stateid = @stateid_SPQ;

INSERT INTO stategrouplink (groupid, stateid, [type]) 
	VALUES
		(@stategroup_CSOTHER, @stateid_SPQ, 'ALLOCATED_SUBDIV'),
		(@stategroup_IN_TRANSIT, @stateid_TBDR, 'ALLOCATED_SUBDIV'),
		(@stategroup_TPWORK, @stateid_TBDR, 'ALLOCATED_SUBDIV');

INSERT INTO dbversion VALUES (838)

COMMIT TRAN