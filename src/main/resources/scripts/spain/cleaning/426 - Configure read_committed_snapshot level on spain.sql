-- Galen Beck - 2019-04-04
-- Turns on the read_committed_snapshot isolation level (was read_committed) on just spain
-- Refer to https://docs.microsoft.com/en-us/sql/t-sql/statements/set-transaction-isolation-level-transact-sql?view=sql-server-2017

alter database spain set read_committed_snapshot on with rollback immediate;

USE [spain]

INSERT INTO dbversion(version) VALUES (426);

-- Note the following can be used to check this option:
--  SELECT [name],[database_id],[snapshot_isolation_state],[snapshot_isolation_state_desc],[is_read_committed_snapshot_on]
--  FROM [master].[sys].[databases] where name='spain'