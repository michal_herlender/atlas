-- Author Galen Beck 2015-02-04
-- Add AddressType for Legal Registration
-- Future database will remove addresstype table (now an Enum) but this is the simplest fix for now.

USE [spain];

-- 2016-02-01 - GB: Add new addresstype to prevent foreign key constraint issue, until it is removed in a future DB.
IF NOT EXISTS (SELECT * FROM [addresstype] WHERE [id] = 5)
	SET IDENTITY_INSERT [addresstype] ON
	INSERT INTO [addresstype] ([id], [businesscomponly], [type]) 
	VALUES (5, 0, 'Legal Registration')
	SET IDENTITY_INSERT [addresstype] OFF;