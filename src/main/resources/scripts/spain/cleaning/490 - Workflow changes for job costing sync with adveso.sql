USE [atlas]
GO

BEGIN TRAN

DECLARE @jc_adveso_new_activity int,
		@jc_repair_status int,
		@jc_onsite_adjust_status int,
		@jc_inspection_status int,
		@jc_exchange_status int,
		@jc_faultreport_status int,
		@jc_tp_further_work_status int,
		@jc_tp_work_status int,
		@jc_adjust_status int,
		@jc_status int,
        @new_hook int,
        @new_lookup int,
        @new_os_1 int,
        @new_os_2 int,
        @new_os_3 int,
        @new_os_4 int,
        @new_os_5 int,
        @new_os_6 int,
        @new_os_7 int,
        @new_os_8 int,
        @new_os_9 int,
		@jc_status_after_repair_status int,
		@jc_status_after_onsite_adjust_status int,
		@jc_status_after_inspection_status int,
		@jc_status_after_exchange_status int,
		@jc_status_after_faultreport_status int,
		@jc_status_after_tp_further_work_status int,
		@jc_status_after_tp_work_status int,
		@jc_status_after_adjust_status int,
		@jc_status_after_status int;

INSERT INTO dbo.itemstate
([type], active, [description], retired, lookupid, actionoutcometype)
VALUES('workactivity', 1, 'Job Costing is available to External Management System', 0, NULL, NULL);

SELECT @jc_adveso_new_activity = stateid FROM dbo.itemstate WHERE [description] = 'Job Costing is available to External Management System';

INSERT INTO dbo.itemstatetranslation
(stateid, locale, [translation])
VALUES(@jc_adveso_new_activity, 'de_DE', 'Auftragskalkulation für extermes Managementsystem verfügbar');
INSERT INTO dbo.itemstatetranslation
(stateid, locale, [translation])
VALUES(@jc_adveso_new_activity, 'en_GB', 'Job Costing is available to External Management System');
INSERT INTO dbo.itemstatetranslation
(stateid, locale, [translation])
VALUES(@jc_adveso_new_activity, 'es_ES', 'El Coste del trabajo está disponible para el sistema de gestión externo');
INSERT INTO dbo.itemstatetranslation
(stateid, locale, [translation])
VALUES(@jc_adveso_new_activity, 'fr_FR', 'Le Coût de travail est disponible pour le système de gestion externe');

INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(57, @jc_adveso_new_activity, 'ALLOCATED_SUBDIV');

INSERT INTO dbo.hook
(active, alwayspossible, beginactivity, completeactivity, [name], activityid)
VALUES(1, 0, 1, 1, 'issue-job-costing-to-adveso', @jc_adveso_new_activity);

SELECT @new_hook = id FROM dbo.hook WHERE [name] = 'issue-job-costing-to-adveso';

SELECT @jc_repair_status = stateid FROM itemstate WHERE [description] = 'Item requires repair – awaiting costing to client';
SELECT @jc_onsite_adjust_status = stateid FROM itemstate WHERE [description] = 'Onsite - unit requires adjustment - awaiting costing to client';
SELECT @jc_inspection_status = stateid FROM itemstate WHERE [description] = 'Client rejected initial costing - requires inspection costing';
SELECT @jc_exchange_status = stateid FROM itemstate WHERE [description] = 'Awaiting costing to client for exchange';
SELECT @jc_faultreport_status = stateid FROM itemstate WHERE [description] = 'Fault report updated, awaiting job costing';
SELECT @jc_tp_further_work_status = stateid FROM itemstate WHERE [description] = 'Awaiting costing for further work at third party';
SELECT @jc_tp_work_status = stateid FROM itemstate WHERE [description] = 'Awaiting job costing for third party work';
SELECT @jc_adjust_status = stateid FROM itemstate WHERE [description] = 'Item requires adjustment – awaiting costing to client';
SELECT @jc_status = stateid FROM itemstate WHERE [description] = 'Service completed, awaiting job costing';

INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
VALUES (1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_repair_status),
	(1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_onsite_adjust_status),
	(1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_inspection_status),
	(1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_exchange_status),
	(1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_faultreport_status),
	(1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_tp_further_work_status),
	(1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_tp_work_status),
	(1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_adjust_status),
	(1,	1,	@jc_adveso_new_activity,	@new_hook,	@jc_status);

INSERT INTO dbo.nextactivity
(manualentryallowed, activityid, statusid)
VALUES(0, @jc_adveso_new_activity, @jc_repair_status),
(0, @jc_adveso_new_activity, @jc_onsite_adjust_status),
(0, @jc_adveso_new_activity, @jc_inspection_status),
(0, @jc_adveso_new_activity, @jc_exchange_status),
(0, @jc_adveso_new_activity, @jc_faultreport_status),
(0, @jc_adveso_new_activity, @jc_tp_further_work_status),
(0, @jc_adveso_new_activity, @jc_tp_work_status),
(0, @jc_adveso_new_activity, @jc_adjust_status),
(0, @jc_adveso_new_activity, @jc_status);

INSERT INTO dbo.lookupinstance
([lookup], lookupfunction)
VALUES('Lookup_JobCostingClientDecision', 54);

SELECT @new_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_JobCostingClientDecision';

UPDATE dbo.itemstate SET lookupid = @new_lookup WHERE stateid = @jc_adveso_new_activity;

SELECT @jc_status_after_repair_status = stateid FROM itemstate WHERE [description] = 'Costing issued for repair - awaiting client decision for repair';
SELECT @jc_status_after_onsite_adjust_status = stateid FROM itemstate WHERE [description] = 'Onsite - costing issued for repair/adjustment - awaiting client decision';
SELECT @jc_status_after_inspection_status = stateid FROM itemstate WHERE [description] = 'Inspection costing issued - awaiting approval';
SELECT @jc_status_after_exchange_status = stateid FROM itemstate WHERE [description] = 'Exchange costing issued, awaiting client decision';
SELECT @jc_status_after_faultreport_status = stateid FROM itemstate WHERE [description] = 'Awaiting client decision after third party return';
SELECT @jc_status_after_tp_further_work_status = stateid FROM itemstate WHERE [description] = 'Awaiting client response for further work at third party';
SELECT @jc_status_after_tp_work_status = stateid FROM itemstate WHERE [description] = 'Awaiting client approval for third party work after job costing';
SELECT @jc_status_after_adjust_status = stateid FROM itemstate WHERE [description] = 'Costing issued for repair/adjustment - awaiting client decision';
SELECT @jc_status_after_status = stateid FROM itemstate WHERE [description] = 'Costing issued - awaiting client approval';

INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,statusid)
VALUES (0,	@jc_adveso_new_activity,	@jc_status_after_repair_status),
(0,	@jc_adveso_new_activity,	@jc_status_after_onsite_adjust_status),
(0,	@jc_adveso_new_activity,	@jc_status_after_inspection_status),
(0,	@jc_adveso_new_activity,	@jc_status_after_exchange_status),
(0,	@jc_adveso_new_activity,	@jc_status_after_faultreport_status),
(0,	@jc_adveso_new_activity,	@jc_status_after_tp_further_work_status),
(0,	@jc_adveso_new_activity,	@jc_status_after_tp_work_status),
(0,	@jc_adveso_new_activity,	@jc_status_after_adjust_status),
(0,	@jc_adveso_new_activity,	@jc_status_after_status);

SELECT @new_os_1 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_repair_status;
SELECT @new_os_2 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_onsite_adjust_status;
SELECT @new_os_3 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_inspection_status;
SELECT @new_os_4 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_exchange_status;
SELECT @new_os_5 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_faultreport_status;
SELECT @new_os_6 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_tp_further_work_status;
SELECT @new_os_7 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_tp_work_status;
SELECT @new_os_8 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_adjust_status;
SELECT @new_os_9 = id FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity AND statusid = @jc_status_after_status;

INSERT INTO dbo.lookupresult ([result],activityid,lookupid,outcomestatusid,resultmessage)
VALUES ('a',	@jc_adveso_new_activity,	@new_lookup,	@new_os_1,	0),
	('b',	@jc_adveso_new_activity,	@new_lookup,	@new_os_2,	1),
	('c',	@jc_adveso_new_activity,	@new_lookup,	@new_os_3,	2),
	('d',	@jc_adveso_new_activity,	@new_lookup,	@new_os_4,	3),
	('e',	@jc_adveso_new_activity,	@new_lookup,	@new_os_5,	4),
	('f',	@jc_adveso_new_activity,	@new_lookup,	@new_os_6,	5),
	('g',	@jc_adveso_new_activity,	@new_lookup,	@new_os_7,	6),
	('h',	@jc_adveso_new_activity,	@new_lookup,	@new_os_8,	7),
	('i',	@jc_adveso_new_activity,	@new_lookup,	@new_os_9,	8);

ALTER TABLE dbo.advesojobItemactivityqueue ADD jobcostingfilename VARCHAR(200);

INSERT INTO dbversion(version) VALUES(490);

COMMIT TRAN