-- Galen Beck 2016-09-21
-- Script to register hookactivity of "raise-purchase-order" (hook 21) for 
-- state id of 126 - "Awaiting PO to cover further third party work"
-- to be associated with activity id 127 - "PO raised to cover further third party work"

USE [spain]
GO

IF NOT EXISTS (SELECT [stateid], [hookid] FROM [dbo].[hookactivity] where [stateid] = 126 AND [hookid] = 21)

INSERT INTO [dbo].[hookactivity]
           ([beginactivity]
           ,[completeactivity]
           ,[activityid]
           ,[hookid]
           ,[stateid])
     VALUES
           (1 
           ,1
           ,127
           ,21
           ,126);

GO


