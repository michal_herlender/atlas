
-- Actual parameter values may differ, what you see is a default string representation of values
USE [spain]
GO

BEGIN TRAN

UPDATE dbo.invoicestatusdescriptiontranslation
SET [translation]='Invoices awaiting approval and issue'
WHERE statusid=4 AND locale='en_GB' AND [translation]='Awaiting approval and issue';
 
UPDATE dbo.invoicestatusdescriptiontranslation
SET [translation]='Facturas esperar aprobaci�n y emisi�n'
WHERE statusid=4 AND locale='es_ES' AND [translation]='Esperar aprobaci�n y emisi�n';

UPDATE dbo.invoicestatusdescriptiontranslation
SET [translation]='Factures en attente d''approbation et d''�dition'
WHERE statusid=4 AND locale='fr_FR' AND [translation]='En attente d''approbation et d''�dition';



	INSERT INTO dbversion(version) VALUES(413);
	GO

COMMIT TRAN

