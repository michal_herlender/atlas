USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.repairinspectionreport ADD clientdescription nvarchar(2000);
ALTER TABLE dbo.repairinspectionreport ADD clientinstructions nvarchar(2000);
ALTER TABLE dbo.repairinspectionreport ADD internaldescription nvarchar(2000);
ALTER TABLE dbo.repairinspectionreport ADD internalinspection nvarchar(2000);
ALTER TABLE dbo.repairinspectionreport ADD internaloperations nvarchar(2000);
ALTER TABLE dbo.repairinspectionreport ADD manufacturerwaranty bit;
ALTER TABLE dbo.repairinspectionreport ADD trescalwarranty bit;
ALTER TABLE dbo.repairinspectionreport ADD internalinstructions nvarchar(2000);
ALTER TABLE dbo.repairinspectionreport ADD externalinstructions nvarchar(2000);
ALTER TABLE dbo.repairinspectionreport ADD leadtime int;
ALTER TABLE dbo.repairinspectionreport ADD completiondate datetime2;

ALTER TABLE dbo.freerepairoperation ADD [position] int;
ALTER TABLE dbo.freerepairoperation DROP COLUMN [order];
ALTER TABLE dbo.freerepairoperation ADD tpcoid int;
ALTER TABLE dbo.freerepairoperation ADD CONSTRAINT freerepairoperation_company_FK FOREIGN KEY (tpcoid) REFERENCES dbo.company(coid);

INSERT INTO dbversion (version) VALUES (430);

COMMIT TRAN