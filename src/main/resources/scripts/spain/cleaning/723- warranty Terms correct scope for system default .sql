USE [atlas]
GO

BEGIN TRAN

update systemdefaultscope set scope=0 where defaultid=64 and scope=2
update systemdefaultscope set scope=0 where defaultid=65 and scope=2

INSERT INTO dbversion(version) VALUES(723);

COMMIT TRAN