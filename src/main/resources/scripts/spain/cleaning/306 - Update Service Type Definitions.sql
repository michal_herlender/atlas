-- Based on the sheet "ERP Services FR 2018 05 15 -Update 05 09 2018.xlsx" provided by Didier Terrien
-- English / base short names are all OK

USE [spain]
GO

BEGIN TRAN


UPDATE [dbo].[servicetype] SET [longname] = 'Calibration traceable without global statement of compliance in laboratory' 
	WHERE [shortname] = 'ST-IH-MV';
UPDATE [dbo].[servicetype] SET [longname] = 'Calibration traceable with global statement of compliance in laboratory' 
	WHERE [shortname] = 'ST-IH';
UPDATE [dbo].[servicetype] SET [longname] = 'Calibration accredited without global statement of compliance in laboratory' 
	WHERE [shortname] = 'AC-IH-MV';
UPDATE [dbo].[servicetype] SET [longname] = 'Calibration accredited with global statement of compliance in laboratory' 
	WHERE [shortname] = 'AC-IH';
UPDATE [dbo].[servicetype] SET [longname] = 'Calibration traceable without global statement of compliance on site' 
	WHERE [shortname] = 'ST-OS-MV';
UPDATE [dbo].[servicetype] SET [longname] = 'Calibration traceable with global statement of compliance on site' 
	WHERE [shortname] = 'ST-OS';
UPDATE [dbo].[servicetype] SET [longname] = 'Calibration accredited without global statement of compliance on site' 
	WHERE [shortname] = 'AC-OS-MV';
UPDATE [dbo].[servicetype] SET [longname] = 'Calibration accredited with global statement of compliance on site' 
	WHERE [shortname] = 'AC-OS';
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'WE-OK-LA'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'WE-MK-LA'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AK-OK-LA'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AK-MK-LA'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'WE-OK-VO'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'WE-MK-VO'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AK-OK-VO'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AK-MK-VO'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS')
GO


UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-EL'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-CAR-EL'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-EL'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-CAR-EL'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-IS'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-CAR-IS'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-IS'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'AC-CAR-IS'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS')
GO


UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ET-TR-LA'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VE-TR-LA'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ET-AC-LA'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VE-AC-LA'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ET-TR-SI'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VE-TR-SI'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ET-AC-SI'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS-MV')
GO
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VE-AC-SI'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS')
GO


UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibration traceable without global statement of compliance in laboratory'
 WHERE locale = 'en_GB' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibration traceable with global statement of compliance in laboratory'
 WHERE locale = 'en_GB' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibration accredited without global statement of compliance in laboratory'
 WHERE locale = 'en_GB' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibration accredited with global statement of compliance in laboratory'
 WHERE locale = 'en_GB' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibration traceable without global statement of compliance on site'
 WHERE locale = 'en_GB' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibration traceable with global statement of compliance on site'
 WHERE locale = 'en_GB' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibration accredited without global statement of compliance on site'
 WHERE locale = 'en_GB' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibration accredited with global statement of compliance on site'
 WHERE locale = 'en_GB' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS')
GO


UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Werkskalibrierung ohne Konfirmit�tsausage im Labor'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Werkskalibrierung mit Konfirmit�tsausage im Labor'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Akkreditierte Kalibrierung ohne Konfirmit�tsausage im Labor'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Akkreditierte Kalibrierung mit Konfirmit�tsausage im Labor'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Werkskalibrierung ohne Konfirmit�tsausage Vor-Ort'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Werkskalibrierung mit Konfirmit�tsausage Vor-Ort'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Akkreditierte Kalibrierung ohne Konfirmit�tsausage Vor-Ort'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Akkreditierte Kalibrierung mit Konfirmit�tsausage Vor-Ort'
 WHERE locale = 'de_DE' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS')
GO


UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n trazable en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n trazable con verificaci�n CAR en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n acreditada en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n acreditada con verificaci�n CAR en laboratorio'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n trazable in situ'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n trazable con verificaci�n CAR in situ'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n acreditada in situ'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibraci�n acreditada con verificaci�n CAR in situ'
 WHERE locale = 'es_ES' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS')
GO


UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Etalonnage tra�able en laboratoire'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification tra�able en laboratoire'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-IH')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Etalonnage accr�dit� en laboratoire'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification accr�dit�e en laboratoire'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-IH')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Etalonnage tra�able sur site'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification tra�able sur site'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'ST-OS')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Etalonnage accr�dit� sur site'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS-MV')
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification accr�dit�e sur site'
 WHERE locale = 'fr_FR' and servicetypeid = (select servicetypeid from servicetype where shortname = 'AC-OS')
GO

INSERT INTO dbversion(version) VALUES(306);

COMMIT TRAN