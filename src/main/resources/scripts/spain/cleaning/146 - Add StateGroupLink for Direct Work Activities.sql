-- Adds state group links for "Direct Work Activities" as identified by Adrian
-- ("Direct Work Activities.xls" sent by email April 5, 2017)
-- Author Galen Beck - 2017-05-10

USE [spain]
GO

INSERT INTO [dbo].[stategrouplink]
           ([groupid],[stateid],[type])
     VALUES
	(46,16,'CURRENT_SUBDIV'),
	(46,18,'CURRENT_SUBDIV'),
	(46,24,'CURRENT_SUBDIV'),
	(46,25,'CURRENT_SUBDIV'),
	(46,26,'CURRENT_SUBDIV'),
	(46,30,'CURRENT_SUBDIV'),
	(46,32,'CURRENT_SUBDIV'),
	(46,38,'CURRENT_SUBDIV'),
	(46,45,'CURRENT_SUBDIV'),
	(46,57,'CURRENT_SUBDIV'),
	(46,61,'CURRENT_SUBDIV'),
	(46,67,'CURRENT_SUBDIV'),
	(46,68,'CURRENT_SUBDIV'),
	(46,72,'CURRENT_SUBDIV'),
	(46,78,'CURRENT_SUBDIV'),
	(46,133,'CURRENT_SUBDIV'),
	(46,135,'CURRENT_SUBDIV'),
	(46,196,'CURRENT_SUBDIV'),
	(46,198,'CURRENT_SUBDIV'),
	(46,199,'CURRENT_SUBDIV'),
	(46,201,'CURRENT_SUBDIV'),
	(46,203,'CURRENT_SUBDIV'),
	(46,4048,'CURRENT_SUBDIV'),
	(46,4062,'CURRENT_SUBDIV'),
	(46,4064,'CURRENT_SUBDIV'),
	(46,4065,'CURRENT_SUBDIV')
GO