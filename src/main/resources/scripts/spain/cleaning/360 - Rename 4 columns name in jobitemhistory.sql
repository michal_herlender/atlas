-- Rename 4 columns in jobitemhistory table.

USE [spain]
GO

BEGIN TRAN

EXEC sp_RENAME 'jobitemhistory.comments' , 'comment', 'COLUMN';

EXEC sp_RENAME 'jobitemhistory.duedateupdated' , 'valueupdated', 'COLUMN';

EXEC sp_RENAME 'jobitemhistory.newduedate' , 'newvalue', 'COLUMN';

EXEC sp_RENAME 'jobitemhistory.oldduedate' , 'oldvalue', 'COLUMN';

INSERT INTO dbversion(version) VALUES(360);

COMMIT TRAN