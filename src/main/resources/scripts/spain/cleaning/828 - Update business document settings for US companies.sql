-- Updates "businessdocumentsettings" for US to put address on left side

use [atlas];
go

begin tran

-- Insert new settings for Trescal Inc and ISS business companies only
DECLARE @coid_TUS int;
DECLARE @coid_ISS int;

SELECT @coid_TUS = coid from company where coname='TRESCAL INC' and corole=5;
SELECT @coid_ISS = coid from company where coname='INTEGRATED SERVICE SOLUTIONS INC' and corole=5;

PRINT @coid_TUS;
PRINT @coid_ISS;

INSERT INTO [dbo].[businessdocumentsettings]
           ([recipientAddressOnLeft]
           ,[globalDefault]
           ,[invoiceAmountText]
           ,[businesscompanyid]
           ,[deliveryNoteSignedCertificates])
     VALUES
           (1, 0, 0, @coid_TUS, 0),
		   (1, 0, 0, @coid_ISS, 0);
GO

insert into dbversion values (828)

commit tran