BEGIN TRAN

UPDATE quotationdocdefaulttext
SET

body = 'Tenemos el placer de enviarle nuestra oferta por los servicios requeridos.

Los precios ofertados estan sometidos a las condiciones generales adjuntas.

Los plazos se cuentan desde la recepción de su pedido.

Si detectamos un error oculto, se le enviará una oferta adicional.

El transporte de regreso se realiza a expensas y riesgo del cliente.

Visite nuestra web a www.trescal.com para ver los servicios adicionales que tenemos para atender a nuestros clientes en la gestión de la calibración de sus equipos, descarga de certificados y otras aplicaciones.

Confiamos que esta oferta sea de su agrado y si necesita ampliar la información, por favor no dude en ponerse en contacto con nosotros.

Atentamente,'

WHERE locale= 'es_ES'




INSERT INTO dbversion VALUES (845)

COMMIT TRAN