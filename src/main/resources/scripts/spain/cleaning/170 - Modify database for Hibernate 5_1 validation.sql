-- Script 170 - Modify database for Hibernate 5 schema validation
-- Galen Beck 2017-08-21
-- Modifies some database columns to match Hibernate 5.1 DDL validation expectations
-- Run before startup if using hibernate.hbm2ddl.auto=validate otherwise fatal exceptions will prevent startup.

USE [spain]

BEGIN TRAN

-- Convert accreditedlab to BIT type
ALTER TABLE [dbo].[accreditedlab] DROP COLUMN [watermark]
ALTER TABLE [dbo].[accreditedlab] ADD [watermark] BIT DEFAULT 0;
UPDATE [dbo].[accreditedlab] SET [watermark] = 1 where id = 1;
UPDATE [dbo].[accreditedlab] SET [watermark] = 0 where watermark IS NULL;
ALTER TABLE [dbo].[accreditedlab] ALTER COLUMN [watermark] BIT NOT NULL;

-- Column was "text" type which has mapping issues and is an obsolete SQL server type - change to varchar(max)
ALTER TABLE [dbo].[exception] DROP COLUMN [stacktrace]
ALTER TABLE [dbo].[exception] ADD [stacktrace] varchar(max);

-- Removed SQL for jobcosting.issued and jobcostingitem.partofbaseunit 2017-08-27 - now bit in 171

--For 'jobcostingitemnote' and 'jobcostingnote' publish and active were both bit - all other in class are tinyint
-- Migrate publish and active data from bit to tinyint fields for consistency with other entities
PRINT 'jobcostingitemnote';
ALTER TABLE [dbo].[jobcostingitemnote] ALTER COLUMN [active] tinyint NOT NULL;
ALTER TABLE [dbo].[jobcostingitemnote] ALTER COLUMN [publish] tinyint NOT NULL;

PRINT 'jobcostingnote';
ALTER TABLE [dbo].[jobcostingnote] ALTER COLUMN [active] tinyint NOT NULL;
ALTER TABLE [dbo].[jobcostingnote] ALTER COLUMN [publish] tinyint NOT NULL;

-- Noticed that calibration (Allocated entity) did not have not-null constraint on lastModified
ALTER TABLE [dbo].[calibration] ALTER COLUMN [lastModified] datetime NOT NULL;

COMMIT TRAN


