use spain

begin tran

alter table dbo.catalogprice
add standardTime bigint;

insert into dbversion(version) values (299)

commit tran