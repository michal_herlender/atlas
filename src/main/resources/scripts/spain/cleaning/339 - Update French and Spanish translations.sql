USE [spain]

BEGIN TRAN

	--replace '' (two quote caracters) by '  
	update dbo.itemstatetranslation 
	set translation = REPLACE(translation,'''''','''')
	where [translation] like '%''''%'
	
	
	UPDATE dbo.itemstatetranslation
	SET [translation]='En attente l''envoie du constat d''anomalie'
	WHERE locale='fr_FR' AND [translation]='An attente d''envoyer le constat d''anomalie' 
	go
	

	UPDATE dbo.actionoutcometranslation
	SET [translation]='Ajustage ERP'
	WHERE locale='fr_FR' AND [translation]='Ajustement ERP' 
	go
	
	UPDATE dbo.itemstatetranslation
	SET [translation]='En espera de la finalización del informe de fallo'
	WHERE locale='es_ES' AND [translation]='Awaiting completion of failure report' 
	go
	
	
	UPDATE dbo.itemstatetranslation
	SET [translation]='En espera de validación del informe de fallos'
	WHERE locale='es_ES' AND [translation]='Awaiting validation of failure report' 
	go

	INSERT INTO dbversion(version) VALUES(339);

COMMIT TRAN