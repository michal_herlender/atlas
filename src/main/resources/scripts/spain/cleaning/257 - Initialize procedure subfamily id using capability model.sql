-- Script 257 (run after 256 to create subfamilyid column)
-- Author Galen Beck - 2018-05-28

USE [spain]
GO

BEGIN TRAN

-- Updates ~1157 rows for Madrid procedures using Capability models
-- TBD how to initialize for other subdivisions

UPDATE [dbo].[procs]
   SET [procs].[subfamilyid] = 
		(SELECT instmodel.descriptionid 
			FROM [dbo].[servicecapability]
			LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
			WHERE instmodel.mfrid = 1 AND servicecapability.procedureid = procs.id AND
			instmodel.model = '/' AND instmodel.modeltypeid = 7)   
 WHERE procs.id in (SELECT 
      [procedureid] 
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  WHERE instmodel.mfrid = 1 AND
  instmodel.model = '/' AND instmodel.modeltypeid = 7);
GO

-- Template
--INSERT INTO [dbo].[capabilityfilter]
--           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
--           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
--           ,[lastModifiedBy],[procedureid])
--     VALUES
--           ('2018-05-28',0,'',<filtertype>,<locationlaboratory>,<locationonsite>
--           ,1,0,<supportaccredited>,<supportnonaccredited>
--           ,17280,<procedureid>)

-- reqtype=THIRDPARTY, shortname in ('AC-IH','ST-IH') indicates no support inhouse
-- (286 rows affected)

INSERT INTO [dbo].[capabilityfilter]
           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
           ,[lastModifiedBy],[procedureid])
     SELECT
           '2018-05-28',0,'','NO',1,0
           ,1,0,calibrationtype.accreditationspecific,(~calibrationtype.accreditationspecific)
           ,17280,[servicecapability].[procedureid]
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  LEFT JOIN procs on [procedureid] = [procs].id 
  LEFT JOIN calibrationtype on [calibrationtypeid] = calibrationtype.caltypeid
  LEFT JOIN servicetype on calibrationtype.[servicetypeid] = servicetype.servicetypeid
  WHERE instmodel.mfrid = 1 AND instmodel.model = '/' AND instmodel.modeltypeid = 7
  AND servicetype.shortname IN ('AC-IH','ST-IH')
  AND reqtype = 'THIRD_PARTY'

GO

-- reqtype=THIRDPARTY, shortname in ('AC-OS','ST-OS') indicates no support onsite
-- (240 rows affected)

INSERT INTO [dbo].[capabilityfilter]
           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
           ,[lastModifiedBy],[procedureid])
     SELECT
           '2018-05-28',0,'','NO',0,1
           ,1,0,calibrationtype.accreditationspecific,(~calibrationtype.accreditationspecific)
           ,17280,[servicecapability].[procedureid]
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  LEFT JOIN procs on [procedureid] = [procs].id 
  LEFT JOIN calibrationtype on [calibrationtypeid] = calibrationtype.caltypeid
  LEFT JOIN servicetype on calibrationtype.[servicetypeid] = servicetype.servicetypeid
  WHERE instmodel.mfrid = 1 AND instmodel.model = '/' AND instmodel.modeltypeid = 7
  AND servicetype.shortname IN ('AC-OS','ST-OS')
  AND reqtype = 'THIRD_PARTY'

GO

-- reqtype=ONSITE indicates support onsite
-- (227 rows affected)

INSERT INTO [dbo].[capabilityfilter]
           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
           ,[lastModifiedBy],[procedureid])
     SELECT
           '2018-05-28',0,'','MAYBE',0,1
           ,1,0,calibrationtype.accreditationspecific,(~calibrationtype.accreditationspecific)
           ,17280,[servicecapability].[procedureid]
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  LEFT JOIN procs on [procedureid] = [procs].id 
  LEFT JOIN calibrationtype on [calibrationtypeid] = calibrationtype.caltypeid
  LEFT JOIN servicetype on calibrationtype.[servicetypeid] = servicetype.servicetypeid
  WHERE instmodel.mfrid = 1 AND instmodel.model = '/' AND instmodel.modeltypeid = 7
  AND reqtype = 'ONSITE'


-- reqtype=ONSITE indicates support in laboratory
-- (393 rows affected)

INSERT INTO [dbo].[capabilityfilter]
           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
           ,[lastModifiedBy],[procedureid])
     SELECT
           '2018-05-28',0,'','MAYBE',1,0
           ,1,0,calibrationtype.accreditationspecific,(~calibrationtype.accreditationspecific)
           ,17280,[servicecapability].[procedureid]
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  LEFT JOIN procs on [procedureid] = [procs].id 
  LEFT JOIN calibrationtype on [calibrationtypeid] = calibrationtype.caltypeid
  LEFT JOIN servicetype on calibrationtype.[servicetypeid] = servicetype.servicetypeid
  WHERE instmodel.mfrid = 1 AND instmodel.model = '/' AND instmodel.modeltypeid = 7
  AND reqtype = 'CALIBRATION'
GO

insert into dbversion(version) values(257)
go

COMMIT TRAN
