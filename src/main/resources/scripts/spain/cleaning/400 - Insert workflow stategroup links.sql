USE [spain]
GO

BEGIN TRAN

-- Add state grouop links for active new status values:
-- 4117 - Unit scrapped � awaiting confirmation costing
-- 3018 - Item to be scrapped

INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (30,4117,'ALLOCATED_SUBDIV'),
           (30,3018,'ALLOCATED_SUBDIV')

INSERT INTO dbversion(version) VALUES(400);
GO

-- Retire old disconnected status
-- 1907 - Client requested return of unit, awaiting delivery note

UPDATE [dbo].[itemstate] SET retired = 1 WHERE stateid = 1907;
GO

-- Set as 'inactive' (work completed)
-- 4118 - Work completed, unit scrapped without return to client

UPDATE [dbo].[itemstate] SET active = 0 WHERE stateid = 4118;
GO

INSERT INTO dbversion(version) VALUES(400);
GO

COMMIT TRAN