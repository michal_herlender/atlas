-- Script 277 - Update service types
-- Author Galen Beck - 2018-06-02
-- Script to replace unwanted service types with their replacements
-- should work on different databases (e.g. France-test) where ids may not be the same

-- Note, ROLLBACK TRAN is at end, change to COMMIT once run successfully

USE [spain]
GO

BEGIN TRAN

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewServiceTypeId]')	AND type IN (N'FN'))
	DROP FUNCTION [dbo].[NewServiceTypeId]
GO

-- Function to map old service types to new
CREATE FUNCTION dbo.[NewServiceTypeId](@oldST_id int)
  RETURNS int
AS
  BEGIN
	IF (@oldST_id = NULL)
		RETURN CAST ('NewServiceTypeId : oldST_id was null' as int)
	DECLARE @oldST_name as varchar(10);
	DECLARE @newST_id as int;
	DECLARE @newST_name as varchar(10);
	SELECT @oldST_name = (SELECT shortname from dbo.[servicetype] WHERE servicetypeid = @oldST_id);
	SET @newST_name = CASE 
		WHEN @oldST_name = 'AC-EX-MV' THEN 'AC-EX'
		WHEN @oldST_name = 'ST-EX-MV' THEN 'STD-EX'	-- Note slight name difference!
		WHEN @oldST_name = 'ST-IC' THEN 'ST-IH'
		WHEN @oldST_name = 'AC-IC' THEN 'AC-IH'
		WHEN @oldST_name = 'AC-IC-MV' THEN 'ST-IH'
		WHEN @oldST_name = 'ST-IC-MV' THEN 'ST-IH'
		WHEN @oldST_name = 'AC-OS-IC' THEN 'AC-OS'
		WHEN @oldST_name = 'ST-OS-IC' THEN 'ST-OS'
		ELSE
			'OTHER'
		END
		-- Force error in user defined function via cast to int
		IF @newST_name = 'OTHER'
			RETURN CAST ('NewServiceTypeId : newST_name was OTHER for oldST_id '+@oldST_id as INT);
	
	SELECT @newST_id = (SELECT servicetypeid FROM servicetype WHERE servicetype.shortname = @newST_name)
	-- Force error in user defined function via cast to int
	IF @newST_id = NULL
		RETURN CAST ('NewServiceTypeId : newST_id was null for oldST_id '+@oldST_id as INT);
  RETURN @newST_id
END

GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewCalTypeId]')	AND type IN (N'FN'))
	DROP FUNCTION [dbo].[NewCalTypeId]
GO

-- Gets new cal type for an old cal type
-- Does a SELECT for each replacement, could optimize later if needed
CREATE FUNCTION dbo.[NewCalTypeId](@oldCT_id int)
  RETURNS int
AS
  BEGIN
	IF (@oldCT_id = NULL)
		RETURN CAST ('NewCalTypeId : oldCT_id was null' as int)
	DECLARE @oldST_id as int
	DECLARE @newST_id as int
	DECLARE @newCT_id as int
	SELECT @oldST_id = (SELECT servicetypeid FROM calibrationtype WHERE calibrationtype.caltypeid = @oldCT_id);
	SET @newST_id = dbo.[NewServiceTypeId](@oldST_id);
	SET @newCT_id = (SELECT caltypeid FROM calibrationtype WHERE calibrationtype.servicetypeid = @newST_id);
	-- Force error in user defined function via cast to varchar
	IF @newCT_id = NULL
		RETURN CAST ('NewCalTypeId : newCT_id was null for oldCT_id '+@oldCT_id as INT);

  RETURN @newCT_id
END

GO

DECLARE @oldST_ids TABLE(id int);

INSERT INTO @oldST_ids (id) 
	SELECT servicetypeid FROM servicetype WHERE servicetype.shortname IN(
		'AC-EX-MV', 'ST-EX-MV', 'ST-IC', 'AC-IC', 'AC-IC-MV', 'ST-IC-MV', 'AC-OS-IC', 'ST-OS-IC');

DECLARE @oldCT_ids TABLE(id int);

INSERT INTO @oldCT_ids (id)
	SELECT caltypeid FROM calibrationtype WHERE calibrationtype.servicetypeid in (SELECT id from @oldST_ids);

PRINT 'DELETE FROM [servicetypelongnametranslation]...'

DELETE FROM [dbo].[servicetypelongnametranslation]
      WHERE servicetypeid IN (SELECT id from @oldST_ids);

PRINT 'DELETE FROM [servicetypeshortnametranslation]...'

DELETE FROM [dbo].[servicetypeshortnametranslation]
      WHERE servicetypeid IN (SELECT id from @oldST_ids);

PRINT 'DELETE FROM [calibrationaccreditationlevel]...'

DELETE FROM [dbo].[calibrationaccreditationlevel]
      WHERE caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'DELETE FROM [quotecaltypedefault]...'

DELETE FROM [dbo].[quotecaltypedefault]
      WHERE caltype_caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'DELETE FROM [quotecaldefaults]...'

DELETE FROM [dbo].[quotecaldefaults]
	WHERE conditionid IN (
		SELECT defcalconid FROM [dbo].[defaultquotationcalibrationcondition]
			WHERE caltypeid IN (SELECT id FROM @oldCT_ids));

DROP TRIGGER [dbo].[log_delete_defaultquotationcalibrationcondition]

PRINT 'DELETE FROM [defaultquotationcalibrationcondition]...'

DELETE FROM [dbo].[defaultquotationcalibrationcondition]
      WHERE caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'DELETE FROM [jobtypecaltype]...'

DELETE FROM [dbo].[jobtypecaltype]
      WHERE caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'DELETE FROM [procedureaccreditation]...'

DELETE FROM [dbo].[procedureaccreditation]
      WHERE caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'UPDATE [quotationitem]... cal type'

UPDATE [dbo].[quotationitem]
   SET [caltype_caltypeid] = dbo.[NewCalTypeId]([caltype_caltypeid])
 WHERE [caltype_caltypeid] IN (SELECT id FROM @oldCT_ids);

PRINT 'UPDATE [quotationitem]... service type'

UPDATE [dbo].[quotationitem]
   SET [servicetypeid] = dbo.[NewServiceTypeId]([servicetypeid])
 WHERE servicetypeid IN (SELECT id from @oldST_ids);

PRINT 'UPDATE [calibration]... cal type'

UPDATE [dbo].[calibration]
   SET [caltypeid] = dbo.[NewCalTypeId]([caltypeid])
 WHERE [caltypeid] IN (SELECT id FROM @oldCT_ids);

PRINT 'UPDATE [certificate]... cal type'

UPDATE [dbo].[certificate]
   SET [caltype] = dbo.[NewCalTypeId]([caltype])
 WHERE [caltype] IN (SELECT id FROM @oldCT_ids);

PRINT 'UPDATE [job]... cal type'

UPDATE [dbo].[job]
   SET defaultcaltypeid = dbo.[NewCalTypeId]([defaultcaltypeid])
 WHERE [defaultcaltypeid] IN (SELECT id FROM @oldCT_ids);

PRINT 'UPDATE [jobitem]... cal type'

UPDATE [dbo].[jobitem]
   SET caltypeid = dbo.[NewCalTypeId]([caltypeid])
 WHERE [caltypeid] IN (SELECT id FROM @oldCT_ids);

-- If running this in order (after scripts 273, 274, 275, 276, etc...) the following is needed

PRINT 'UPDATE [workrequirement]... cal type'

UPDATE [dbo].[workrequirement]
   SET servicetypeid = dbo.[NewServiceTypeId]([servicetypeid])
 WHERE [servicetypeid] IN (SELECT id FROM @oldST_ids);

-- End of addition post 273

PRINT 'UPDATE [instrument]... service type'

UPDATE [dbo].[instrument]
   SET defaultservicetype = dbo.[NewServiceTypeId]([defaultservicetype])
 WHERE [defaultservicetype] IN (SELECT id FROM @oldST_ids);

PRINT 'UPDATE [tpquotationcaltypedefault]... cal type'

UPDATE [dbo].[tpquotationcaltypedefault]
   SET caltypeid = dbo.[NewCalTypeId]([caltypeid])
 WHERE caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'UPDATE [tpquotationitem]... cal type'

UPDATE [dbo].[tpquotationitem]
   SET caltype_caltypeid = dbo.[NewCalTypeId](caltype_caltypeid)
 WHERE caltype_caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'UPDATE [tpquoterequestitem]... cal type'

UPDATE [dbo].[tpquoterequestitem]
   SET caltypeid = dbo.[NewCalTypeId](caltypeid)
 WHERE caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'ALTER contractreviewlinkedcalibrationcost'

DROP INDEX IDX_contractreviewlinkedcalibrationcost_pricelistitemid ON [dbo].contractreviewlinkedcalibrationcost;
DROP STATISTICS [dbo].contractreviewlinkedcalibrationcost._dta_stat_910626287_1_3_4_5;
DROP STATISTICS [dbo].contractreviewlinkedcalibrationcost._dta_stat_910626287_3_5_4_2;
DROP STATISTICS [dbo].contractreviewlinkedcalibrationcost._dta_stat_910626287_2_1_3_4_5;
DROP INDEX _dta_index_contractreviewlinkedcalibrationc_7_910626287__K2_K1_K3_K4_K5 ON [dbo].contractreviewlinkedcalibrationcost;
DROP STATISTICS [dbo].contractreviewlinkedcalibrationcost._dta_stat_910626287_4_1_2;
ALTER TABLE [dbo].contractreviewlinkedcalibrationcost DROP CONSTRAINT FK76BEECD452FE7755;
ALTER TABLE [dbo].contractreviewlinkedcalibrationcost DROP COLUMN pricelistitemid;

PRINT 'ALTER jobcostinglinkedcalibrationcost'

ALTER TABLE [dbo].jobcostinglinkedcalibrationcost DROP CONSTRAINT FK_mne37g8bsangghde855p5qls8;
ALTER TABLE [dbo].jobcostinglinkedcalibrationcost DROP COLUMN pricelistitemid;

PRINT 'DROP unused old tables'

DROP TABLE LOG_pricelistitemnote
DROP TABLE LOG_pricelistitemcost
DROP TABLE LOG_pricelistitem
DROP TABLE LOG_pricelistnote
DROP TABLE LOG_pricelist

DROP TABLE pricelistcaltypedefault
DROP TABLE pricelistitemnote
DROP TABLE pricelistitemcost
DROP TABLE pricelistitem
DROP TABLE pricelistnote
DROP TABLE pricelist
DROP TABLE reusablecertno

PRINT 'DELETE old cal types'

DELETE FROM [dbo].calibrationtype WHERE caltypeid IN (SELECT id FROM @oldCT_ids);

PRINT 'DELETE old service types'

DELETE FROM [dbo].servicetype WHERE servicetypeid IN (SELECT id FROM @oldST_ids);

-- Clean up of functions

DROP FUNCTION [dbo].[NewCalTypeId]
DROP FUNCTION [dbo].[NewServiceTypeId]

INSERT INTO dbversion(version) VALUES(277);

COMMIT TRAN