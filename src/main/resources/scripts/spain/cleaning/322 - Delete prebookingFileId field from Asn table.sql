-- Delete prebookingfileid from Asn Table

USE [spain]
GO

BEGIN TRAN

    ALTER TABLE dbo.asn DROP CONSTRAINT asn_prebookingfile_FK;
	
	ALTER TABLE dbo.asn DROP COLUMN prebookingfileid;

	INSERT INTO dbversion(version) VALUES(322);

COMMIT TRAN