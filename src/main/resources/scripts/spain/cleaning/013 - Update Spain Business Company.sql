/* Update Spain Business Companies - Author Tony Provost 2016-01-21 */

BEGIN TRAN 

USE [spain];
GO

DECLARE @BusinessCompanyID INT

-- Updated General information (name, legal identifier, fiscal identifier, defsubdiv in table dbo.company)
SET @BusinessCompanyID = 6578
UPDATE dbo.company 
SET coname = 'TRESCAL IB�RICA DE CALIBRACI�N SL'
	,legalidentifier = 'B95769923'
	,companycode = 'TIC'
	,fiscalidentifier = 'ESB95769923'
	,defsubdivid = (SELECT subdivid FROM dbo.subdiv WHERE coid = @BusinessCompanyID and lower(subname) = ('bilbao'))
	,lastModified = GETDATE()
WHERE coid = @BusinessCompanyID;

SET @BusinessCompanyID = 6579
UPDATE dbo.company 
SET coname = 'TRESCAL ESPA�A DE METROLOGIA SL'
	,legalidentifier = 'B95025714'
	,companycode = 'TEM'
	,fiscalidentifier = 'ESB95025714' 
	,defsubdivid = (SELECT subdivid FROM dbo.subdiv WHERE coid = @BusinessCompanyID and lower(subname) = ('zaragoza'))
	,lastModified = GETDATE()
WHERE coid = @BusinessCompanyID;

-- Updated Legal information (table dbo.businesscompanysetttings)
SET @BusinessCompanyID = 6578
IF EXISTS(SELECT * FROM dbo.businesscompanysettings WHERE companyid = @BusinessCompanyID)
	BEGIN	
		UPDATE dbo.businesscompanysettings
			SET capital = '479 000,00 �'
				,legalregistration1 = 'Registro Mercantil de Bizkaia Tomo5465, Folio 75, Hoja BI-64.537 CIF. B95025714'
				,legalregistration2 = ''
				,logo = 'TRESCAL'
				,interbranchsalesmode = 'SALES_CATALOG'
				,interbranchdiscountrate = 20
			WHERE companyid = @BusinessCompanyID
	END
ELSE
	BEGIN	
		INSERT INTO dbo.businesscompanysettings (capital, legalregistration1, legalregistration2, logo, interbranchsalesmode, interbranchdiscountrate, companyid)
		VALUES ('479 000,00 �', 'Registro Mercantil de Bizkaia Tomo5465, Folio 75, Hoja BI-64.537 CIF. B95025714', '', 'TRESCAL', 'SALES_CATALOG', 20, @BusinessCompanyID)
		UPDATE dbo.company SET businesssettingsid = @@IDENTITY, lastModified = GETDATE() WHERE coid = @BusinessCompanyID
	END

SET @BusinessCompanyID = 6579
IF EXISTS(SELECT * FROM dbo.businesscompanysettings WHERE companyid = @BusinessCompanyID)
	BEGIN	
		UPDATE dbo.businesscompanysettings
			SET capital = '901 264,92 �'
				,legalregistration1 = 'Registro Mercantil de Zaragoza Tomo 4132, Folio 37, Hoja Z-58973 CIF. B95025714'
				,legalregistration2 = ''
				,logo = 'TRESCAL'
				,interbranchsalesmode = 'SALES_CATALOG'
				,interbranchdiscountrate = 20
			WHERE companyid = @BusinessCompanyID
	END
ELSE
	BEGIN	
		INSERT INTO dbo.businesscompanysettings (capital, legalregistration1, legalregistration2, logo, interbranchsalesmode, interbranchdiscountrate, companyid)
		VALUES ('901 264,92 �', 'Registro Mercantil de Zaragoza Tomo 4132, Folio 37, Hoja Z-58973 CIF. B95025714', '', 'TRESCAL', 'SALES_CATALOG', 20, @BusinessCompanyID)
		UPDATE dbo.company SET businesssettingsid = @@IDENTITY, lastModified = GETDATE() WHERE coid = @BusinessCompanyID
	END	

-- Update Finance Information
SET @BusinessCompanyID = 6578
UPDATE dbo.companysettingsforallocatedcompany
SET taxable = 1
	,porequired = 1
	,invoicefrequencyid = (SELECT id FROM dbo.invoicefrequency WHERE code = 'OG')
	,paymentmodeid = (SELECT id FROM dbo.paymentmode WHERE code = 'WT') 
	,paymenttermsid = (SELECT id FROM dbo.paymentterms WHERE code = 'NM25')
	,lastModified = GETDATE()
WHERE companyid = @BusinessCompanyID 	
	AND orgid = @BusinessCompanyID

SET @BusinessCompanyID = 6579
UPDATE dbo.companysettingsforallocatedcompany
SET taxable = 1
	,porequired = 1
	,invoicefrequencyid = (SELECT id FROM dbo.invoicefrequency WHERE code = 'OG')
	,paymentmodeid = (SELECT id FROM dbo.paymentmode WHERE code = 'WT') 
	,paymenttermsid = (SELECT id FROM dbo.paymentterms WHERE code = 'NM25')
	,lastModified = GETDATE()
WHERE companyid = @BusinessCompanyID
	AND orgid = @BusinessCompanyID

-- Updated BankAccount information
SET @BusinessCompanyID = 6578
IF NOT EXISTS (SELECT * FROM dbo.bankaccount WHERE companyid = @BusinessCompanyID AND iban = 'ES6600491800182210587002' AND swiftbic = 'BSCHESMM')
	BEGIN
		INSERT INTO dbo.bankaccount (bankname, iban, swiftbic, accountno, currencyid, companyid)
		VALUES ('Banco Santander', 'ES6600491800182210587002', 'BSCHESMM', '00491800182210587002', 3, @BusinessCompanyID)
		IF (@@ROWCOUNT != 0)
			BEGIN
				UPDATE dbo.companysettingsforallocatedcompany 
				SET bankaccountid = @@IDENTITY
					,lastModified = GETDATE() 
				WHERE companyid = @BusinessCompanyID 
					AND orgid = @BusinessCompanyID
			END
	END 
ELSE
	BEGIN
		UPDATE dbo.companysettingsforallocatedcompany 
		SET bankaccountid = (SELECT id FROM dbo.bankaccount WHERE companyid = @BusinessCompanyID AND iban = 'ES6600491800182210587002' AND swiftbic = 'BSCHESMM')
			,lastModified = GETDATE() 
		WHERE companyid = @BusinessCompanyID 
			AND orgid = @BusinessCompanyID
	END 
	
	
SET @BusinessCompanyID = 6579
IF NOT EXISTS (SELECT * FROM dbo.bankaccount WHERE companyid = @BusinessCompanyID AND iban = 'ES6300495483692716364144' AND swiftbic = 'BSCHESMM')
	BEGIN
		INSERT INTO dbo.bankaccount (bankname, iban, swiftbic, accountno, currencyid, companyid)
		VALUES ('Banco Santander', 'ES6300495483692716364144', 'BSCHESMM', '00495483692716364144', 3, @BusinessCompanyID)
		IF (@@ROWCOUNT != 0)
			BEGIN
				UPDATE dbo.companysettingsforallocatedcompany 
				SET bankaccountid = @@IDENTITY
					,lastModified = GETDATE() 
				WHERE companyid = @BusinessCompanyID 
					AND orgid = @BusinessCompanyID
			END
	END 
ELSE
	BEGIN
		UPDATE dbo.companysettingsforallocatedcompany 
		SET bankaccountid = (SELECT id FROM dbo.bankaccount WHERE companyid = @BusinessCompanyID AND iban = 'ES6300495483692716364144' AND swiftbic = 'BSCHESMM')
			,lastModified = GETDATE() 
		WHERE companyid = @BusinessCompanyID 
			AND orgid = @BusinessCompanyID
	END 
	
ROLLBACK TRAN
--COMMIT TRAN
GO