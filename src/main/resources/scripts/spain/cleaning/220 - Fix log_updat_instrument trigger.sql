USE [spain]
GO
/****** Object:  Trigger [dbo].[log_update_instrument]    Script Date: 02/02/2018 10:07:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[log_update_instrument]
ON [dbo].[instrument]
AFTER UPDATE AS BEGIN  IF EXISTS
(SELECT * FROM audittriggerswitch WHERE enabled = 1)
  BEGIN  insert into LOG_instrument
  (plantid, addedon, calfrequency, calibrationStandard,
   calsinterval, calsintervalon, calssincelastcal,
   firmwareaccesscode, recalldate, plantno, recallednotreceived,
   replacedon, requirement, scrapped, scrappedon, serialno, addressid,
   addedby, baseunitid, coid, personid, lastcalid, locationid, mfrid,
   modelid, procid, replacedby, replacementid, scrappedby, scrappedonji,
   statusid, storageid, threadid, usageid, wiid, assetid, log_validfrom,
   log_validto, log_userid, log_actiontype)
    SELECT plantid, addedon, calfrequency, calibrationStandard,
      permitednumberofuses, calexpiresafternumberofuses, usessincelastcalibration,
      firmwareaccesscode, recalldate, plantno, recallednotreceived, replacedon,
      requirement, scrapped, scrappedon, serialno, addressid, addedby, baseunitid,
      coid, personid, lastcalid, locationid, mfrid, modelid, procid, replacedby,
      replacementid, scrappedby, scrappedonji, statusid, storageid, threadid, usageid,
      wiid, assetid,
      case when log_lastmodified is null then (dbo.unix_timestamp(GETUTCDATE())*1000)
      else log_lastmodified END, case when (dbo.unix_timestamp(GETUTCDATE())*1000) <= log_lastmodified
      then (log_lastmodified + 1) else (dbo.unix_timestamp(GETUTCDATE())*1000)
                                 END, case when log_userid is null then 1 else log_userid END, 'UPDATE' FROM deleted END END;



INSERT INTO dbversion VALUES (220);