USE spain;

BEGIN TRAN

ALTER TABLE lookupresult ALTER COLUMN result VARCHAR(1) NULL

GO

INSERT INTO lookupinstance (lookup, lookupfunction) VALUES ('Lookup_PreDeliveryRequirements', 39)
INSERT INTO itemstate (type, active, description, retired)
VALUES
('workstatus', 1, 'Awaiting internal delivery note', 0)

GO

INSERT INTO itemstatetranslation (stateid, locale, translation)
SELECT itemstate.stateid, 'en_GB', 'Awaiting internal delivery note'
FROM itemstate
WHERE description = 'Awaiting internal delivery note'

INSERT INTO itemstatetranslation (stateid, locale, translation)
SELECT itemstate.stateid, 'fr_FR', 'En attente de bon de livraison interne'
FROM itemstate
WHERE description = 'Awaiting internal delivery note'

INSERT INTO itemstatetranslation (stateid, locale, translation)
SELECT itemstate.stateid, 'es_ES', 'Esperando la nota de entrega interna'
FROM itemstate
WHERE description = 'Awaiting internal delivery note'

INSERT INTO stategrouplink (stateid, groupid)
SELECT stateid, 43
FROM itemstate
WHERE description='Awaiting internal delivery note'

GO

INSERT INTO outcomestatus (defaultoutcome, statusid) VALUES (0, 170)
INSERT INTO outcomestatus (defaultoutcome, statusid)
SELECT 0, itemstate.stateid
FROM itemstate
WHERE description='Awaiting internal delivery note'

UPDATE outcomestatus SET lookupid = lookupinstance.id, statusid = NULL
FROM lookupinstance
WHERE outcomestatus.id = 192 AND lookupinstance.lookupfunction = 39

INSERT INTO lookupresult (lookupid, outcomestatusid, resultmessage)
SELECT lookupinstance.id, outcomestatus.id, 1
FROM lookupinstance, outcomestatus
LEFT JOIN itemstate ON outcomestatus.statusid = itemstate.stateid
WHERE lookupfunction = 39 AND itemstate.description='Awaiting internal delivery note'

INSERT INTO lookupresult (lookupid, outcomestatusid, resultmessage)
SELECT lookupinstance.id, outcomestatus.id, 0
FROM lookupinstance, outcomestatus
WHERE lookupfunction = 39 AND outcomestatus.statusid = 170

INSERT INTO itemstate (active, description, type, retired)
VALUES (1, 'Added to internal delivery note', 'workactivity', 0)

INSERT INTO itemstatetranslation (locale, stateid, translation)
SELECT 'en_GB', stateid, 'Added to internal delivery note'
FROM itemstate
WHERE description = 'Added to internal delivery note'

INSERT INTO itemstatetranslation (locale, stateid, translation)
SELECT 'fr_FR', stateid, 'Ajout� au bon de livraison interne'
FROM itemstate
WHERE description = 'Added to internal delivery note'

INSERT INTO itemstatetranslation (locale, stateid, translation)
SELECT 'es_ES', stateid, 'A�adido a la nota de entrega interna'
FROM itemstate
WHERE description = 'Added to internal delivery note'


INSERT INTO hookactivity (beginactivity, completeactivity, activityid, hookid, stateid)
SELECT 1, 1, activity.stateid, 10, state.stateid
FROM itemstate AS activity, itemstate AS state
WHERE activity.description = 'Added to internal delivery note' AND state.description = 'Awaiting internal delivery note'

INSERT INTO outcomestatus (defaultoutcome, activityid, statusid)
SELECT 1, stateid, 170
FROM itemstate
WHERE description = 'Added to internal delivery note'

COMMIT TRAN