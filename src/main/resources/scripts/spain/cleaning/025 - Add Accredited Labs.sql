-- Author Galen Beck 2016-04-29
-- Populates accreditationbody, Updates/Populates accreditedcallab

BEGIN TRAN 

USE [spain];
GO

SET IDENTITY_INSERT [accreditationbody] ON
GO

INSERT INTO [accreditationbody] ([id], [lastModified] ,[shortName] ,[countryid]) 
       VALUES (1, '2016-04-29', 'A2LA' , 185);
INSERT INTO [accreditationbody] ([id], [lastModified] ,[shortName] ,[countryid]) 
       VALUES (2, '2016-04-29', 'Cofrac' , 162);
INSERT INTO [accreditationbody] ([id], [lastModified] ,[shortName] ,[countryid]) 
       VALUES (3, '2016-04-29', 'ENAC' , 61);
INSERT INTO [accreditationbody] ([id], [lastModified] ,[shortName] ,[countryid]) 
       VALUES (4, '2016-04-29', 'DAkkS' , 65);
INSERT INTO [accreditationbody] ([id], [lastModified] ,[shortName] ,[countryid]) 
       VALUES (5, '2016-04-29', 'SAC-SINGLAS' , 1);
INSERT INTO [accreditationbody] ([id], [lastModified] ,[shortName] ,[countryid]) 
       VALUES (6, '2016-04-29', 'UKAS' , 184);

GO

SET IDENTITY_INSERT [accreditationbody] OFF
GO

UPDATE [accreditedlab] SET [accreditationbodyid] = 6 WHERE [accreditationbodyid] = NULL;
GO

INSERT INTO [accreditationbodyresource] ([lastModified],[resourcePath],[type],[accreditationbodyid])
     VALUES ('2016-04-29','labels/birt/callogo_ENAC.png',1,3)
GO

-- AccreditedLab no longer should have uniqueness constraint on lab number
ALTER TABLE [dbo].[accreditedlab] DROP CONSTRAINT [UQ__accredit__48239416060DEAE8]
GO

SET IDENTITY_INSERT [accreditedlab] ON
GO

INSERT INTO [accreditedlab] ([id],[description],[labno],[watermark],[lastModified],[orgid],[accreditationbodyid])
     VALUES (3,'Bilbao - Calibration','33-LC10-182 ed 3',0,'2016-04-29',6642,3);
INSERT INTO [accreditedlab] ([id],[description],[labno],[watermark],[lastModified],[orgid],[accreditationbodyid])
     VALUES (4,'Vitoria - Calibration','33-LC10-182 ed 3',0,'2016-04-29',6643,3);
INSERT INTO [accreditedlab] ([id],[description],[labno],[watermark],[lastModified],[orgid],[accreditationbodyid])
     VALUES (5,'Zaragoza - Calibration','111-LC10-078 ed 8',0,'2016-04-29',6644,3);
INSERT INTO [accreditedlab] ([id],[description],[labno],[watermark],[lastModified],[orgid],[accreditationbodyid])
     VALUES (6,'Madrid - Calibration','46-LC10-185 ed 7',0,'2016-04-29',6645,3);

GO

SET IDENTITY_INSERT [accreditedlab] OFF
GO

UPDATE [procs] SET [accreditedlabid] = 3 WHERE [orgid] = 6642;
UPDATE [procs] SET [accreditedlabid] = 4 WHERE [orgid] = 6643;
UPDATE [procs] SET [accreditedlabid] = 5 WHERE [orgid] = 6644;
UPDATE [procs] SET [accreditedlabid] = 6 WHERE [orgid] = 6645;

ROLLBACK TRAN
GO