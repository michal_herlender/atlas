BEGIN TRAN;

-- Create service type with standard colors
insert into dbo.servicetype ([displaycolour],[displaycolourfasttrack],[longname],[shortname]) 
VALUES ('#FFFFFF','#FEE0B4','Inter-Company Standard  Calibration','ST-IC');


-- Create cal type for new service type with values from in house standard service type
insert into calibrationtype
			(accreditationspecific,
			active,
			certnoresetyearly,
			firstpagepapertype,
			labeltemplatepath,
			orderby,
			pdfcontinuationwatermark,
			pdfheaderwatermark,
			recalldateonlabel,
			restpagepapertype,
			servicetypeid)
select accreditationspecific,
	   1,
	   certnoresetyearly,
	   firstpagepapertype,
	   labeltemplatepath,
	   (select servicetypeid from servicetype s where s.shortname = 'ST-IC'),
	   pdfcontinuationwatermark,
	   pdfheaderwatermark,
	   recalldateonlabel,
	   restpagepapertype,
	   (select servicetypeid from servicetype s where s.shortname = 'ST-IC') as 'servicetypeid'
from calibrationtype
inner join servicetype on servicetype.servicetypeid = calibrationtype.servicetypeid
where servicetype.shortname = 'ST-IH';

-- Insert default quotation calibration conditions for new cal type and every business company
INSERT INTO [dbo].[defaultquotationcalibrationcondition]
(conditiontext, lastModified, caltypeid, orgid)
SELECT '',
'2016-01-01',
(select calibrationtype.caltypeid 
	FROM servicetype 
	INNER JOIN  calibrationtype
	ON servicetype.servicetypeid = calibrationtype.servicetypeid
	WHERE servicetype.shortname = 'ST-IC'), 
company.coid
FROM company
WHERE company.corole = 5;

-- Insert links to jobtypes
INSERT INTO jobtypecaltype (defaultvalue, jobtypeid, caltypeid)
    SELECT 0,1,caltypeid
    FROM calibrationtype
    INNER JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
    WHERE servicetype.shortname = 'ST-IC';

INSERT INTO jobtypecaltype (defaultvalue, jobtypeid, caltypeid)
  SELECT 0,2,caltypeid
  FROM calibrationtype
    INNER JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'ST-IC';


-- Insert accreditation levels for new cal type
INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'APPROVE', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'ST-IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'REMOVED', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'ST-IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'SUPERVISED', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'ST-IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'PERFORM', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'ST-IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
SELECT 'SIGN', [caltypeid]
FROM [dbo].[calibrationtype]
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.shortname = 'ST-IC';



--Add translations
INSERT INTO servicetypelongnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.longname from servicetype
WHERE servicetype.shortname = 'ST-IC';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Calibración no acreditada Inter-Company'
from servicetype
where servicetype.shortname = 'ST-IC';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'fr_FR', 'Vérification Standard Inter-sociétés'
from servicetype
where servicetype.shortname = 'ST-IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.shortname from servicetype
WHERE servicetype.shortname = 'ST-IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'es_ES',servicetype.shortname from servicetype
WHERE servicetype.shortname = 'ST-IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'fr_FR','ST-IS' from servicetype
WHERE servicetype.shortname = 'ST-IC';

COMMIT TRAN;
