-- Updates the order in calibrationtype, servicetype of the SERVICE record to be at the very last
-- Needed for quotation rendering and selection purposes.

USE [spain]

BEGIN TRAN

UPDATE [dbo].[calibrationtype] SET orderby = 40 WHERE servicetypeid = (SELECT servicetypeid FROM servicetype WHERE shortname='SERVICE');

GO

UPDATE [dbo].[servicetype] SET orderby = 40 WHERE shortname='SERVICE';

GO

insert into dbversion(version) values(355);

COMMIT TRAN