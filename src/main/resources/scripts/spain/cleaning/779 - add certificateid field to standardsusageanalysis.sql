USE [atlas]
GO

BEGIN TRAN

	-- Delete all rows in standardsusageanalysis table
	DELETE FROM dbo.standardsusageanalysis;

	-- Add new filed 'certificateid'
	ALTER TABLE dbo.standardsusageanalysis ADD certificateid INT NOT NULL;
	ALTER TABLE dbo.standardsusageanalysis ADD CONSTRAINT FK_standardsusageanalysis_certificate FOREIGN KEY (certificateid) REFERENCES dbo.certificate(certid);


	INSERT INTO dbversion(version) values(779);

COMMIT TRAN