USE [spain]
BEGIN TRAN

ALTER TABLE instrumentcomplementaryfield
       ADD calibrationstatusoverride varchar(255);

GO

INSERT INTO dbversion(version) VALUES(291);

COMMIT TRAN