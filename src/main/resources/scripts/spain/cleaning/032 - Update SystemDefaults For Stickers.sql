-- Author Galen Beck 2016-04-29
-- Updates existing SystemDefault related entities for Stickers

BEGIN TRAN 

USE [spain];
GO

-- Create new Group for client stickers

SET IDENTITY_INSERT [systemdefaultgroup] ON
GO

INSERT INTO [systemdefaultgroup] ([id],[description],[dname])
     VALUES (10, 'Client sticker attributes','Client - Stickers');
GO

SET IDENTITY_INSERT [systemdefaultgroup] OFF
GO

INSERT INTO [systemdefaultgroupdescriptiontranslation] ([id],[locale],[translation])
     VALUES (10,'en_GB','Client sticker attributes'),
            (10,'es_ES','Las atributos de la etiqueta del cliente');
     
INSERT INTO [systemdefaultgroupnametranslation] ([id],[locale],[translation])
     VALUES (10,'en_GB','Client - Stickers'),
            (10,'es_ES','Cliente - Etiquetas');

-- Update existing sticker attributes (System Defaults 6, 19, and 26) to use new Group and default values             
            
UPDATE [dbo].[systemdefault]
   SET [defaultdatatype] = 'string'
      ,[defaultdescription] = 'Defines if this company receives a recall date on standard calibration labels'
      ,[defaultname] = 'Standard Label Recall Date'
      ,[defaultvalue] = 'Yes Without Date'
      ,[groupid] = 10
      ,[groupwide] = 1
 WHERE defaultid = 19
GO

UPDATE [dbo].[systemdefaultdescriptiontranslation]
   SET [translation] = 'Defines if this company receives a recall date on standard calibration labels'
 WHERE defaultid = 19 AND locale = 'en_GB';
GO

UPDATE [dbo].[systemdefaultdescriptiontranslation]
   SET [translation] = 'Define si una compañía quiere fecha de próxima calibración en las etiquetas de calibración trazable'
 WHERE defaultid = 19 AND locale = 'es_ES';

UPDATE [dbo].[systemdefault]
   SET [defaultdatatype] = 'string'
      ,[defaultdescription] = 'Defines if this company receives a recall date on accredited calibration labels'
      ,[defaultname] = 'Accredited Label Recall Date'
      ,[defaultvalue] = 'Yes Without Date'
      ,[groupid] = 10
      ,[groupwide] = 1
 WHERE defaultid = 6
GO

UPDATE [dbo].[systemdefaultdescriptiontranslation]
   SET [translation] = 'Define si una compañía quiere fecha de próxima calibración en las etiquetas de calibración acreditadas'
 WHERE defaultid = 6 AND locale = 'es_ES';
GO

UPDATE [dbo].[systemdefaultdescriptiontranslation]
   SET [translation] = 'Defines if this company receives a recall date on accredited calibration labels'
 WHERE defaultid = 6 AND locale = 'en_GB';

UPDATE [dbo].[systemdefault]
   SET [groupid] = 10
      ,[defaultvalue] = 'true'
      ,[groupwide] = 1
 WHERE defaultid = 29
GO
  
 -- Enables existing system defaults to be configurable at any scope of company, subdiv, contact
 
 INSERT INTO [dbo].[systemdefaultscope]
           ([scope],[defaultid])
     VALUES
           (1,6),
           (2,6),
           (1,19),
           (2,19),
           (0,29),
           (1,29),
           (2,29);
 
GO

-- Change to COMMIT when successfully tested

ROLLBACK TRAN