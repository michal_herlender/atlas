USE [spain];


BEGIN TRAN

	-- wire the lookup result 6-NOT_REPAIRED_NO_FURTHER_ACTION of the 30-Lookup_RepairOutcome
	-- to 67-Lookup_AdditionalDemandsCompleted instead of 44-Awaiting rejection label
	UPDATE dbo.outcomestatus SET statusid=NULL,lookupid=67 WHERE id=228;
	
	INSERT INTO dbversion (version) VALUES (451);

COMMIT TRAN