
USE [atlas]

BEGIN TRAN
	
	--------------------------------------------------------------------------------------------------------------------
	------------------------------------ update schedule table ---------------------------------------------------------

	-- delete statistics from schedule table if exist
	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_13_7' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_13_7;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_7_12' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_7_12;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_5_7' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_5_7;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_5_13_7' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_5_13_7;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_12_13_7_5' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_12_13_7_5;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_8_14' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_8_14;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_5_14_8' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_5_14_8;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_1_8_14_5' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_1_8_14_5;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2079346472_1_14_5' 
		AND object_ID = OBJECT_ID ('dbo.schedule'))
	DROP STATISTICS dbo.schedule._dta_stat_2079346472_1_14_5;  
	GO

	-- delete/re-create indexes from schedule table and make schedule fields not nullable
	DROP INDEX IDX_schedule_addressid ON dbo.schedule;
	UPDATE dbo.schedule SET addressid = 12210  WHERE addressid IS NULL;
	ALTER TABLE dbo.schedule ALTER COLUMN addressid INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_schedule_addressid] ON [dbo].[schedule](addressid ASC) 
		WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];

	
	DROP INDEX IDX_schedule_contactid ON dbo.schedule;
	UPDATE dbo.schedule SET contactid = 17280  WHERE contactid IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN contactid INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_schedule_contactid] ON [dbo].[schedule](contactid ASC) 
		WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];

	DROP INDEX IDX_schedule_createdby ON dbo.schedule;
	UPDATE dbo.schedule SET createdby = 17280  WHERE createdby IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN createdby INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_schedule_createdby] ON [dbo].[schedule](createdby ASC) 
		WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];

	UPDATE dbo.schedule SET createdon = SYSDATETIME() WHERE createdon IS NULL;
	ALTER TABLE dbo.schedule ALTER COLUMN createdon DATETIME2 NOT NULL;

	UPDATE dbo.schedule SET repeatschedule = 0 WHERE repeatschedule IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN repeatschedule BIT NOT NULL;

	UPDATE dbo.schedule SET scheduledate = SYSDATETIME() WHERE scheduledate IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN scheduledate DATETIME2 NOT NULL;

	DROP INDEX IDX_schedule_scheduledfromsubdivid ON dbo.schedule;
	UPDATE dbo.schedule SET scheduledfromsubdivid = 6940  WHERE scheduledfromsubdivid IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN scheduledfromsubdivid INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_schedule_scheduledfromsubdivid] ON [dbo].[schedule](scheduledfromsubdivid ASC) 
		WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];

	DROP INDEX IDX_schedule_schedulesourceid ON dbo.schedule;
	UPDATE dbo.schedule SET schedulesourceid = 0 WHERE schedulesourceid IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN schedulesourceid INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_schedule_schedulesourceid] ON [dbo].[schedule](schedulesourceid ASC) 
		WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];

	DROP INDEX IDX_schedule_schedulestatusid ON dbo.schedule;
	UPDATE dbo.schedule SET schedulestatusid = 0 WHERE schedulestatusid IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN schedulestatusid INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_schedule_schedulestatusid] ON [dbo].[schedule](schedulestatusid ASC) 
		WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];

	DROP INDEX IDX_schedule_scheduletypeid ON dbo.schedule; 
	UPDATE dbo.schedule SET scheduletypeid = 0 WHERE scheduletypeid IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN scheduletypeid INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_schedule_scheduletypeid] ON [dbo].[schedule](scheduletypeid ASC) 
		WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];

	-- rename the scheduledfromsubdivid column
	EXEC sp_rename 'dbo.schedule.scheduledfromsubdivid', 'orgid', 'COLUMN';
	GO
	
	-- delete unused columns
	ALTER TABLE dbo.schedule DROP COLUMN source;
	ALTER TABLE dbo.schedule DROP COLUMN status;
	ALTER TABLE dbo.schedule DROP COLUMN type;

	-- add new column transportoptionid 
	ALTER TABLE dbo.schedule ADD transportoptionid INT NULL 
		CONSTRAINT FK_scheduled_transportoption FOREIGN KEY (transportoptionid) REFERENCES dbo.transportoption(id);
	GO
	DECLARE @id INT;
	SELECT TOP 1 @id = id FROM dbo.transportoption;
	UPDATE dbo.schedule SET transportoptionid = @id WHERE transportoptionid IS NULL; 
	ALTER TABLE dbo.schedule ALTER COLUMN transportoptionid INT NOT NULL;

	-- add FK constraints
	ALTER TABLE dbo.schedule DROP CONSTRAINT IF EXISTS FKlgiy7ff3ho7wg7k3dfwiffdml
	ALTER TABLE dbo.schedule ADD CONSTRAINT FK_schedule_subdiv FOREIGN KEY (orgid) REFERENCES dbo.subdiv(subdivid);

	-- delete unused fileds
	ALTER TABLE dbo.schedule DROP CONSTRAINT IF EXISTS FKD6669297202933D5;
	ALTER TABLE dbo.schedule DROP CONSTRAINT IF EXISTS FKrp6nxbq03jnwdv0dpbr2y9e7t;
	DROP  INDEX IDX_schedule_freehandid ON dbo.schedule
	ALTER TABLE dbo.schedule DROP COLUMN log_userid;
	ALTER TABLE dbo.schedule DROP COLUMN log_lastmodified;
	ALTER TABLE dbo.schedule DROP COLUMN freehandid;

	
	--------------------------------------------------------------------------------------------------------------------
	------------------------------------ update repeatschedule table ---------------------------------------------------

	-- delete data in repeatschedule table if exist (in my database locally this table is empty)
	DELETE FROM dbo.repeatschedule;

	-- rename the scheduledfromsubdivid column
	EXEC sp_rename 'dbo.repeatschedule.scheduledfromsubdivid', 'orgid', 'COLUMN';
	GO
	
	-- drop indexes 
	DROP INDEX IDX_repeatschedule_createdbyid ON dbo.repeatschedule
	DROP INDEX IDX_repeatschedule_scheduledfromsubdivid ON dbo.repeatschedule
	GO
	
	-- add "not null" constraints
	ALTER TABLE dbo.repeatschedule ALTER COLUMN active BIT NOT NULL;
	ALTER TABLE dbo.repeatschedule ALTER COLUMN createdon DATETIME NOT NULL;

	ALTER TABLE dbo.repeatschedule ALTER COLUMN createdbyid INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_repeatschedule_createdbyid] ON [dbo].[repeatschedule](createdbyid ASC) 
	WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];
	
	ALTER TABLE dbo.repeatschedule ALTER COLUMN orgid INT NOT NULL;
	CREATE NONCLUSTERED INDEX [IDX_repeatschedule_orgid] ON [dbo].[repeatschedule](orgid ASC) 
	WITH (PAD_INDEX = OFF, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, SORT_IN_TEMPDB = OFF, FILLFACTOR =100) ON [PRIMARY];

	-- delete unused fileds
	ALTER TABLE dbo.repeatschedule DROP CONSTRAINT IF EXISTS FK5FA4F532202933D5;
	ALTER TABLE dbo.repeatschedule DROP CONSTRAINT IF EXISTS FKimj7p6scu41rx75ylgo5bguyo;
	ALTER TABLE dbo.repeatschedule DROP CONSTRAINT IF EXISTS FKdvk2m7y1iakje97adg8776rtp;
	ALTER TABLE dbo.repeatschedule DROP COLUMN type;
	ALTER TABLE dbo.repeatschedule DROP COLUMN log_userid;
	ALTER TABLE dbo.repeatschedule DROP COLUMN log_lastmodified;

	-- add FK constraints
	ALTER TABLE dbo.repeatschedule 
		ADD CONSTRAINT FK_repeatschedule_subdiv FOREIGN KEY (orgid) REFERENCES dbo.subdiv(subdivid);
	ALTER TABLE dbo.repeatschedule 
		ADD CONSTRAINT FK_repeatschedule_contact FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid);

		
	--------------------------------------------------------------------------------------------------------------------
	------------------------------------ create scheduleddelivery table ------------------------------------------------

	CREATE TABLE dbo.scheduleddelivery (
		id INT IDENTITY NOT NULL PRIMARY KEY,
		deliveryid INT NOT NULL,
		scheduleid INT NOT NULL,
		scheduleddeliverystatusid INT NOT NULL,
		CONSTRAINT FK_scheduleddelivery_delivery FOREIGN KEY (deliveryid) REFERENCES dbo.delivery(deliveryid),
		CONSTRAINT FK_scheduleddelivery_schedule FOREIGN KEY (scheduleid) REFERENCES dbo.Schedule(scheduleId)
	);

	-- delete idexes from jobitem table
	DROP INDEX IDX_jobitem_scheduleid ON dbo.jobitem
	DROP INDEX _dta_index_jobitem_7_2016726237__K30_K38_K21_K31_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_22_23_24_25_26_27_28_29_32_ ON dbo.jobitem;
	DROP INDEX _dta_index_jobitem_7_2016726237__K38_K21_K33_K1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_22_23_24_25_26_27_28_29_30_31_ ON dbo.jobitem;
	DROP INDEX _dta_index_jobitem_7_2016726237__K33_K38_K21_K6_K31_K7_1_2_3_4_5_8_9_10_11_12_13_14_15_16_17_18_19_20_22_23_24_25_26_27_28_29_ ON dbo.jobitem;

	-- delete statistics from jobitem table
	IF EXISTS (SELECT name FROM sys.stats  
			WHERE name = '_dta_stat_2016726237_31_21_37' 
			AND object_ID = OBJECT_ID ('dbo.jobitem'))
		DROP STATISTICS dbo.jobitem._dta_stat_2016726237_31_21_37;  
		GO

	IF EXISTS (SELECT name FROM sys.stats  
			WHERE name = '_dta_stat_2016726237_1_21_37_26' 
			AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_1_21_37_26;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_1_38_21_37_26_31' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_1_38_21_37_26_31;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18_30' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18_30;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_33_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_33_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18_13' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18_13;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_33_1_21_37_26_31_17_38_32_36_29_16_39_25_19_35' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_33_1_21_37_26_31_17_38_32_36_29_16_39_25_19_35;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_31_1_21_37_26_17_38_32_36_29_16_39_25_19_35_18' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_31_1_21_37_26_17_38_32_36_29_16_39_25_19_35_18;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_38_21_37_26_31' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_38_21_37_26_31;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_35_21_37_26_31_17_38_32_36_29_16_39_25' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_35_21_37_26_31_17_38_32_36_29_16_39_25;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_22_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_22_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_30_1_21_37_26_31_17_38_32_36_29_16_39_25_19_35' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_30_1_21_37_26_31_17_38_32_36_29_16_39_25_19_35;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_14_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_14_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_1_22_21_37_26_31_17_38_32_36_29_16_39_25_19_35' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_1_22_21_37_26_31_17_38_32_36_29_16_39_25_19_35;  
	GO

	IF EXISTS (SELECT name FROM sys.stats  
		WHERE name = '_dta_stat_2016726237_27_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18' 
		AND object_ID = OBJECT_ID ('dbo.jobitem'))
	DROP STATISTICS dbo.jobitem._dta_stat_2016726237_27_21_37_26_31_17_38_32_36_29_16_39_25_19_35_18;  
	GO

	-- delete scheduleid column from jobitem table
	ALTER TABLE dbo.jobitem DROP CONSTRAINT IF EXISTS FK7adtmv3rm3h8lvn4y9ucoc2y4;
	GO

	ALTER TABLE dbo.jobitem DROP COLUMN scheduleid;

	-- delete scheduleid column from hire table
	ALTER TABLE dbo.hire DROP CONSTRAINT IF EXISTS FKc12ha37w5qxq5xyeynt5by33k;
	DROP INDEX IDX_hire_scheduleid ON dbo.hire;
	ALTER TABLE dbo.hire DROP COLUMN scheduleid;


	INSERT INTO dbversion VALUES (751)

COMMIT TRAN