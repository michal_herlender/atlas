-- Creates VAT codes:
-- 'W' for Korea, 'X' for New Zealand, 'Y' for Portugal
-- Adds currency for New Zealand and Korea
-- Note, the "proper" won symbol needs to be in unicode hence the conversion of a couple columns to nvarchar.

USE [atlas]
GO

BEGIN TRAN

ALTER TABLE [supportedcurrency] ALTER COLUMN [currencyersymbol] nvarchar(10);
ALTER TABLE [supportedcurrency] ALTER COLUMN [currencysymbol] nvarchar(3);

GO

INSERT INTO [dbo].[supportedcurrency]
           ([accountscode]
           ,[currencycode]
           ,[currencyersymbol]
           ,[currencyname]
           ,[currencysymbol]
           ,[exchangerate]
           ,[orderby]
           ,[ratelastset]
           ,[rateset]
           ,[ratelastsetby]
           ,[majorpluralname]
           ,[majorsingularname]
           ,[minorexponent]
           ,[minorpluralname]
           ,[minorsingularname]
           ,[minorunit]
           ,[symbolposition])
     VALUES
           ('NZD'
           ,'NZD'
           ,'NZ$'
           ,'New Zealand dollar'
           ,'NZ$'
           ,1.71
           ,16
           ,'2020-12-02'
           ,1
           ,17280
           ,'dollars'
           ,'dollar'
           ,2
           ,'cents'
           ,'cent'
           ,1
           ,'PREFIX_NO_SPACE'),
		   ('KRW'
           ,'KRW'
           ,'&#65510;'
           ,'South Korean won'
           ,N'￦'
           ,133505.57
           ,17
           ,'2020-12-02'
           ,1
           ,17280
           ,'won'
           ,'won'
           ,2
           ,'jeon'
           ,'jeon'
           ,1
           ,'PREFIX_NO_SPACE')
GO

-- Note, the "proper" won symbol needs to be in unicode hence the conversion of a couple columns to nvarchar.

DECLARE @countryIdNZ int;
select @countryIdNZ = country.countryid from country where countrycode = 'NZ';

PRINT @countryIdNZ;

DECLARE @countryIdKR int;
select @countryIdKR = country.countryid from country where countrycode = 'KR';

PRINT @countryIdKR;

DECLARE @countryIdPT int;
select @countryIdPT = country.countryid from country where countrycode = 'PT';

PRINT @countryIdPT;

-- 'W' for Korea, 'X' for New Zealand, 'Y' for Portugal

INSERT INTO [dbo].[vatrate]
           ([vatcode],[description],[rate],[type],[countryid],[lastModified],[lastModifiedBy])
     VALUES
           ('W','South Korea',10.00,0,@countryIdKR,'2020-12-02',17280),
           ('X','New Zealand',15.00,0,@countryIdNZ,'2020-12-02',17280),
           ('Y','Portugal',23.00,0,@countryIdPT,'2020-12-02',17280);

INSERT INTO dbversion(version) VALUES(739);

GO

COMMIT TRAN