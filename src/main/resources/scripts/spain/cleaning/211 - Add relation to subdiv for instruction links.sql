-- Evolution of “client instructions” management (ticket #1210) :
-- Possibility to manage instruction on a business subdiv level as well as on the company level
-- Ahmed Yassine Boutahar (04/01/2018)

USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.companyinstructionlink ADD issubdivinstruction bit default 0;
go

ALTER TABLE dbo.companyinstructionlink ADD businesssubdivid int;
go

ALTER table dbo.companyinstructionlink 
        add constraint FK_businesssubdivid_subdivid 
        foreign key (businesssubdivid) 
        references dbo.subdiv;

ALTER TABLE dbo.subdivinstructionlink ADD issubdivinstruction bit default 0;
go

ALTER TABLE dbo.subdivinstructionlink ADD businesssubdivid int;
go

ALTER table dbo.subdivinstructionlink 
        add constraint FK_subdivlink_businesssubdivid_subdivid 
        foreign key (businesssubdivid) 
        references dbo.subdiv;
        
INSERT INTO dbversion (version) VALUES (211);

COMMIT TRAN