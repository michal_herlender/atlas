USE [spain]

BEGIN TRAN

	ALTER TABLE dbo.faultreport ALTER COLUMN precert bit NULL
	ALTER TABLE dbo.faultreport ALTER COLUMN prerecord bit NULL

	INSERT INTO dbversion(version) VALUES(315);

COMMIT TRAN