-- Script 259
-- Initializes subfamilies on procedures for ZGZ Zaragoza data subdiv 6644
-- Author Galen Beck - 2018-05-31

USE [spain]
GO

BEGIN TRAN

-- Updates ~110 procedures to have subfamily (currently using using Sales Category models!!!)
UPDATE [dbo].[procs]
   SET [procs].[subfamilyid] = 
		(SELECT instmodel.descriptionid 
			FROM [dbo].[servicecapability]
			LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
			WHERE instmodel.mfrid = 1 AND servicecapability.procedureid = procs.id AND
			instmodel.model = '/' AND instmodel.modeltypeid = 6 AND 
			servicecapability.orgid = 6644)   
 WHERE procs.id in (SELECT 
      [procedureid] 
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  WHERE instmodel.mfrid = 1 AND
  instmodel.model = '/' AND instmodel.modeltypeid = 6 AND 
  servicecapability.orgid = 6644);
GO

-- Clear out any existing capability filters from previous runs of script
DELETE FROM [dbo].[capabilityfilter]
      WHERE capabilityfilter.id IN 
	  (SELECT capabilityfilter.id FROM [capabilityfilter] 
	  LEFT JOIN procs on procs.id = capabilityfilter.procedureid
	  WHERE orgid = 6644)
GO

-- service types are properly set for ZGZ service capabilities - onsite
-- (43 rows affected)

INSERT INTO [dbo].[capabilityfilter]
           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
           ,[lastModifiedBy],[procedureid])
     SELECT
           '2018-05-31',0,'','MAYBE',0,1
           ,1,0,calibrationtype.accreditationspecific,(~calibrationtype.accreditationspecific)
           ,17280,[servicecapability].[procedureid]
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  LEFT JOIN procs on [procedureid] = [procs].id 
  LEFT JOIN calibrationtype on [calibrationtypeid] = calibrationtype.caltypeid
  LEFT JOIN servicetype on calibrationtype.[servicetypeid] = servicetype.servicetypeid
  WHERE instmodel.mfrid = 1 AND instmodel.model = '/' AND instmodel.modeltypeid = 6 AND 
  servicecapability.orgid = 6644 
  AND servicetype.shortname IN ('AC-OS','ST-OS')

-- service types are properly set for ZGZ service capabilities - laboratory
-- (67 rows affected)

INSERT INTO [dbo].[capabilityfilter]
           ([lastModified],[bestlab],[comments],[filtertype],[locationlaboratory],[locationonsite]
           ,[servicetypecalibration],[servicetyperepair],[supportaccredited],[supportnonaccredited]
           ,[lastModifiedBy],[procedureid])
     SELECT
           '2018-05-31',0,'','MAYBE',1,0
           ,1,0,calibrationtype.accreditationspecific,(~calibrationtype.accreditationspecific)
           ,17280,[servicecapability].[procedureid]
  FROM [dbo].[servicecapability]
  LEFT JOIN instmodel on [servicecapability].[modelid] = [instmodel].modelid
  LEFT JOIN procs on [procedureid] = [procs].id 
  LEFT JOIN calibrationtype on [calibrationtypeid] = calibrationtype.caltypeid
  LEFT JOIN servicetype on calibrationtype.[servicetypeid] = servicetype.servicetypeid
  WHERE instmodel.mfrid = 1 AND instmodel.model = '/' AND instmodel.modeltypeid = 6 AND 
  servicecapability.orgid = 6644 
  AND servicetype.shortname IN ('AC-IH','ST-IH')
GO

insert into dbversion(version) values(259)
go

COMMIT TRAN