-- TO BE EXECUTED AFTER REPAIR WORKFLOW CHANGES (perform upgrade)

USE spain;

BEGIN TRAN

	DECLARE @a1 as int;
	DECLARE @s1 as int;	
	DECLARE @h1 as int;	

	--hook validate-rir-by-technician
	select @a1 = iss.stateid
	from itemstate iss
	where iss.description = 'Repair Completion Report Validated';
	select @s1 = iss.stateid
	from itemstate iss
	where iss.description = 'Awaiting Repair Completion Report Validation';
	
	INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,0,1,1,'validate-rcr',@a1);

	select @h1=h.id
	from dbo.hook h
	where h.name = 'validate-rcr';

	INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@a1,@h1,@s1);
	
	--add fields for csr validation
	ALTER TABLE dbo.repaircompletionreport ADD validated bit DEFAULT 0 NULL;
	ALTER TABLE dbo.repaircompletionreport ADD validatedon datetime2 NULL;
	ALTER TABLE dbo.repaircompletionreport ADD validatedby int NULL;
	ALTER TABLE dbo.repaircompletionreport 
		ADD CONSTRAINT repaircompletionreport_validatedby_FK FOREIGN KEY (validatedby) REFERENCES dbo.contact(personid);

	INSERT INTO dbversion (version) VALUES (447);

COMMIT TRAN