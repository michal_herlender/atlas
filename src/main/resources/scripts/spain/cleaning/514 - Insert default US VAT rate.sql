-- Inserts a "default" US sales tax rate - 2019-10-01 - Galen Beck
-- Previously on test only, adding and running on production and test now, so that it works automatically when refreshing production -> test.
-- At a future date, when US tax is more thoroughly specified/implemented, we would likely update this record.

USE [atlas]
GO

BEGIN TRAN

INSERT INTO [dbo].[vatrate]
           ([vatcode]
           ,[description]
           ,[rate]
           ,[type]
           ,[countryid]
           ,[lastModified]
           ,[lastModifiedBy])
     VALUES
           ('N','US - No Specific VAT Rate',0.000,0,185,'2019-07-24',17280);
GO

insert into dbversion(version) values (514)

COMMIT TRAN