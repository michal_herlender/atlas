USE [atlas]
GO

BEGIN TRAN

ALTER TABLE [oauth2accesstoken] ALTER COLUMN serializedauthentication varchar(max);
ALTER TABLE [oauth2refreshtoken] ALTER COLUMN serializedauthentication varchar(max);

INSERT INTO dbversion(version) VALUES(510);

COMMIT TRAN