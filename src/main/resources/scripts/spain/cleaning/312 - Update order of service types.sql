-- Script 311
-- Updates the order of service types to match recent discussion
-- 2018-10-03 Galen Beck

USE [spain]

BEGIN TRAN

UPDATE calibrationtype set orderby=1 WHERE servicetypeid=16
UPDATE calibrationtype set orderby=2 WHERE servicetypeid=15
UPDATE calibrationtype set orderby=3 WHERE servicetypeid=18
UPDATE calibrationtype set orderby=4 WHERE servicetypeid=17
UPDATE calibrationtype set orderby=5 WHERE servicetypeid=2
UPDATE calibrationtype set orderby=6 WHERE servicetypeid=1
UPDATE calibrationtype set orderby=7 WHERE servicetypeid=5
UPDATE calibrationtype set orderby=8 WHERE servicetypeid=4
UPDATE calibrationtype set orderby=10 WHERE servicetypeid=27
UPDATE calibrationtype set orderby=11 WHERE servicetypeid=28
UPDATE calibrationtype set orderby=20 WHERE servicetypeid=3
UPDATE calibrationtype set orderby=21 WHERE servicetypeid=7
UPDATE calibrationtype set orderby=22 WHERE servicetypeid=8
UPDATE calibrationtype set orderby=23 WHERE servicetypeid=29
UPDATE calibrationtype set orderby=24 WHERE servicetypeid=30
UPDATE calibrationtype set orderby=25 WHERE servicetypeid=31

-- Service types have a unique constraint on order by, so we first reset the orderby to high unique numbers

UPDATE servicetype set orderby=(servicetypeid+100)
GO

UPDATE servicetype set orderby=1 WHERE servicetypeid=16
UPDATE servicetype set orderby=2 WHERE servicetypeid=15
UPDATE servicetype set orderby=3 WHERE servicetypeid=18
UPDATE servicetype set orderby=4 WHERE servicetypeid=17
UPDATE servicetype set orderby=5 WHERE servicetypeid=2
UPDATE servicetype set orderby=6 WHERE servicetypeid=1
UPDATE servicetype set orderby=7 WHERE servicetypeid=5
UPDATE servicetype set orderby=8 WHERE servicetypeid=4
UPDATE servicetype set orderby=10 WHERE servicetypeid=27
UPDATE servicetype set orderby=11 WHERE servicetypeid=28
UPDATE servicetype set orderby=20 WHERE servicetypeid=3
UPDATE servicetype set orderby=21 WHERE servicetypeid=7
UPDATE servicetype set orderby=22 WHERE servicetypeid=8
UPDATE servicetype set orderby=23 WHERE servicetypeid=29
UPDATE servicetype set orderby=24 WHERE servicetypeid=30
UPDATE servicetype set orderby=25 WHERE servicetypeid=31
UPDATE servicetype set orderby=30 WHERE servicetypeid=6

INSERT INTO dbversion(version) VALUES(312);

COMMIT TRAN