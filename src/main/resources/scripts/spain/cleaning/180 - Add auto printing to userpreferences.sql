-- Script 180 - Add auto printing setting to user preferences
-- AYB 2017-11-01

USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.userpreferences ADD autoprintlabel tinyint DEFAULT 0;

GO

UPDATE [dbo].[userpreferences]
   SET autoprintlabel = 0;

GO

COMMIT TRAN



