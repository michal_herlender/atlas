-- Fixes enum definitions for the single schedule in the system, 
-- the enum values were -1 in production so schedule wouldn't load for testing.

BEGIN TRAN

USE [spain]
GO

UPDATE [dbo].[schedule]
   SET [schedulesourceid] = 0
      ,[schedulestatusid] = 0
      ,[scheduletypeid] = 0
GO

insert into dbversion(version) values (351);

COMMIT TRAN