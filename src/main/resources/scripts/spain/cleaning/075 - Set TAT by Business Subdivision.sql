-- Author Galen Beck 2016-10-18
-- Sets up default TAT by subdivision (to run after application startup)
USE [spain]
GO

BEGIN TRAN

-- MAD 6645, ZAZ 6644, BIO 6642, VIT 6643
-- BIO and VIT
UPDATE [dbo].[businesssubdivisionsettings] SET [defaultturnaround] = 7 WHERE [subdivid] = 6642;
UPDATE [dbo].[businesssubdivisionsettings] SET [defaultturnaround] = 7 WHERE [subdivid] = 6643;
-- MAD and ZAZ
UPDATE [dbo].[businesssubdivisionsettings] SET [defaultturnaround] = 14 WHERE [subdivid] = 6644;
UPDATE [dbo].[businesssubdivisionsettings] SET [defaultturnaround] = 14 WHERE [subdivid] = 6645;
-- Morocco 6951
UPDATE [dbo].[businesssubdivisionsettings] SET [defaultturnaround] = 5 WHERE [subdivid] = 6951;
-- Antech 6900 GY and 6901 Aberdeen

IF NOT EXISTS (SELECT 1 FROM [businesssubdivisionsettings] WHERE [subdivid] = 6951)
	INSERT INTO [businesssubdivisionsettings] 
	 ([lastModified],[ebitdahourlycostrate],[subdivid],[defaultturnaround])
	 VALUES ('2016-10-18','345.00',6951,5)
ELSE
	UPDATE [businesssubdivisionsettings] 
	SET [ebitdahourlycostrate] = '345.00',
	[defaultturnaround] = 5
	WHERE [subdivid] = 6951

IF NOT EXISTS (SELECT 1 FROM [businesssubdivisionsettings] WHERE [subdivid] = 6900)
	INSERT INTO [businesssubdivisionsettings]
	  ([lastModified],[ebitdahourlycostrate],[subdivid],[defaultturnaround])
	  VALUES ('2016-10-18','100.00',6900,7);

IF NOT EXISTS (SELECT 1 FROM [businesssubdivisionsettings] WHERE [subdivid] = 6901)
	INSERT INTO [businesssubdivisionsettings]
	  ([lastModified],[ebitdahourlycostrate],[subdivid],[defaultturnaround])
	  VALUES ('2016-10-18','100.00',6901,7);

UPDATE [dbo].[systemdefault]
   SET [defaultdatatype] = 'integer'
      ,[defaultdescription] = 'Sets job turnaround default, 0 = use business subdivision default'
      ,[defaultname] = 'Turnaround'
      ,[defaultvalue] = '0'
      ,[endd] = 28
      ,[start] = 0
 WHERE [defaultid] = 2;

UPDATE [dbo].[systemdefaultdescriptiontranslation]
   SET [translation] = 'Sets job turnaround default, 0 = use business subdivision default'
 WHERE [defaultid] = 2 and [locale] = 'en_GB';

UPDATE [dbo].[systemdefaultdescriptiontranslation]
   SET [translation] = 'Especifica el plazo de devoluci�n por trabajos, 0 = utilizar el valor predeterminado de la subdivisi�n de negocios'
 WHERE [defaultid] = 2 and [locale] = 'es_ES';

UPDATE [dbo].[systemdefaultdescriptiontranslation]
   SET [translation] = 'D�finit le d�lai de traitement des jobs, 0 = utiliser la valeur par d�faut de la subdivision d''affaires'
 WHERE [defaultid] = 2 and [locale] = 'fr_FR';

GO

COMMIT TRAN