use [atlas]
go

begin tran

--- Create sequence ---
declare @maxId int;
select @maxId = max(id) + 1 from dbo.invoiceitem;
print @maxId;

declare @sqlCommand varchar(100);
SET @sqlCommand = 'create sequence dbo.invoiceitem_id_sequence start with ' + CAST(@maxId as varchar(10)) + ' increment by 1;'
print @sqlCommand;
exec(@sqlCommand)

go

--- Remove foreign keys from old table ---
--- Naming of invoice item ship-to constraint seems to vary per system, add as needed ---
-- Note, DROP IF EXISTS supported in SQL Server 2016+, we should have 2017(+) on all platforms

-- From Egbert's local copy
alter table invoiceitem drop constraint if exists FK__invoiceit__shipt__641938A9;
-- From Atlas US database
ALTER TABLE [dbo].[invoiceitem] DROP CONSTRAINT if exists [FK__invoiceit__shipt__4B829509];

alter table invoiceitem drop constraint
	FK_invoiceitem_expenseitem,
	FK_invoiceitem_servicecost,
	FK2aiowux9lmitlk3e3a3b4t27b,
	FKCCA85FC0202933D5,
	FKgvxwn30m4wlxtro5f8d16v6vu,
	FKj7gwf0p4gkxtlj0u5sh052o3q,
	FKjfs87b1rupv2w4wnrchli0fgc,
	FKjg5gu8glorfs6sfvr4rrr3r1u,
	FKn2phq8ftouwoe0e59fkfb0pf0,
	FKo4v9r65dl9f41rphm6b6xtfda,
	FKp2ytoi2ynhm0ajb0h5uudyemw,
	FKsbjvdl3gr20we3isp5w1rhmjg,
	UQ__invoicei__3213E83E6A1BB7B0

alter table invoiceitempo drop constraint FKqw4r05tmfpq7h3iyx5rhadek4

alter table invoiceitem drop column log_lastmodified, log_userid

--- Create new table without identity ---
create table dbo.new_invoiceitem (
	id int not null,
	finalcost numeric(10,2) not null,
	discountrate numeric(5,2) not null,
	discountvalue numeric(10,2) not null,
	itemno int not null,
	partofbaseunit bit not null,
	quantity int not null,
	totalcost numeric(10,2) not null,
	breakupcosts tinyint not null,
	description varchar(500),
	serialno varchar(30),
	adjustmentcost_id int,
	calcost_id int,
	invoiceid int not null,
	jobitemid int,
	nominalid int,
	purchasecost_id int,
	repaircost_id int,
	orgid int,
	lastModified datetime2,
	lastModifiedBy int,
	expenseitem int,
	servicecost int,
	taxable tinyint not null,
	taxamount numeric(10,2),
	shiptoaddressid int not null,
	primary key (id)
);

go

--- Shift data ---
alter table dbo.invoiceitem switch to dbo.new_invoiceitem

go

--- Delete old table ---
drop table dbo.invoiceitem

--- Rename new table ---
exec sys.sp_rename 'dbo.new_invoiceitem', 'invoiceitem'

--- Create foreign keys for new table ---
alter table dbo.invoiceitem add constraint FK_invoiceitem_lastmodifiedby
	foreign key (lastModifiedBy) references dbo.contact;

alter table dbo.invoiceitem add constraint FK_invoiceitem_shiptoaddress 
	foreign key (shiptoaddressid) references dbo.address;

alter table dbo.invoiceitem add constraint FK_invoiceitem_invoice
	foreign key (invoiceid) references dbo.invoice;

alter table dbo.invoiceitem add constraint FK_invoiceitem_expenseitem
	foreign key (expenseitem) references dbo.jobexpenseitem;

alter table dbo.invoiceitem add constraint FK_invoiceitem_jobitem 
	foreign key (jobitemid) references dbo.jobitem;

alter table dbo.invoiceitem add constraint FK_invoiceitem_nominalcode
	foreign key (nominalid) references dbo.nominalcode;

alter table dbo.invoiceitem add constraint FK_invoiceitem_organisation
	foreign key (orgid) references dbo.subdiv;

alter table dbo.invoiceitempo add constraint FK_invoiceitempo_invoiceitem 
	foreign key (invoiceitemid) references dbo.invoiceitem;

insert into dbversion values (825)

commit tran