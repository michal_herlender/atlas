USE spain;

BEGIN TRAN

EXEC sp_rename 'permissionLimit', 'permissionlimit';

GO

create table dbo.contractdemand (
	id int identity not null,
	demandset int,
	contractid int,
	domainid int,
	subfamilyid int,
    primary key (id)
);

create table dbo.demandmodel (
	id int identity not null,
    demand int,
    model int,
    primary key (id)
);
    
create table dbo.jobitemdemand (
	id int identity not null,
    additionalDemand varchar(255) not null,
    demandStatus varchar(255) not null,
    comment varchar(255),
    jobItemId int not null,
    primary key (id)
);

alter table dbo.jobcourierlink
   add constraint UKo659u3y6vtcaruj5frr6hdac2 unique (jobid, courierid);

alter table dbo.jobitemdemand
   add constraint UK_jobitemdemand_jobitem_additionaldemand unique (jobItemId, additionalDemand);
       
create index IDX_quotationitem_plantid on dbo.quotationitem (plantid);

alter table dbo.contractdemand 
   add constraint FK_contractdemand_contract 
   foreign key (contractid) 
   references dbo.contract;

alter table dbo.contractdemand 
   add constraint FK_contractdemand_domain 
   foreign key (domainid) 
   references dbo.instmodeldomain;

alter table dbo.contractdemand 
   add constraint FK_contractdemand_subfamily 
   foreign key (subfamilyid) 
   references dbo.description;

alter table dbo.demandmodel 
   add constraint FKqkaxw1cwk3nm4xnq31kjk4pfq 
   foreign key (model) 
   references dbo.instmodel;
   
alter table dbo.jobitemdemand 
   add constraint FK_jobitemdemand_jobitem 
   foreign key (jobItemId) 
   references dbo.jobitem;

INSERT INTO dbversion(version) VALUES (419)

COMMIT TRAN