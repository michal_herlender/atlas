
USE [atlas]

BEGIN TRAN

DECLARE @activity_tpcert INT,
		@activity_onsitecert INT,
		@status_aw_rcr INT,
		 @new_lookup INT,
		 @os_1 INT,
		 @os_2 INT,
		 @lookup_tpcaloutcome INT;

SELECT @activity_tpcert = stateid, @lookup_tpcaloutcome = lookupid FROM dbo.itemstate WHERE [description] = 'Third party certificate added';
SELECT @activity_onsitecert = stateid, @lookup_tpcaloutcome = lookupid FROM dbo.itemstate WHERE [description] = 'Onsite calibration - certificate signed';
SELECT @status_aw_rcr = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting repair completion report update (post third party work)';

INSERT INTO dbo.stategrouplink
(groupid, stateid, [type])
VALUES(53, @activity_tpcert, 'CURRENT_SUBDIV'),
      (53, @activity_onsitecert, 'CURRENT_SUBDIV');

-- create lookup 'ItemRepairedAtTP'
INSERT INTO dbo.lookupinstance (lookup,lookupfunction) VALUES ('Lookup_ItemRepairedAtTP',56);
SELECT @new_lookup = max(id) from dbo.lookupinstance;

--add outcomestatus
INSERT INTO dbo.outcomestatus (activityid, lookupid, statusid, defaultoutcome) 
VALUES (@activity_tpcert, null, @status_aw_rcr, 0);
select @os_1 = max(id) from dbo.outcomestatus;
INSERT INTO dbo.outcomestatus (activityid, lookupid, statusid, defaultoutcome) 
VALUES (@activity_tpcert, @lookup_tpcaloutcome, null, 1);
select @os_2 = max(id) from dbo.outcomestatus;

-- create lookup result
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@new_lookup,@os_1,0);
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage) VALUES (@new_lookup,@os_2,1);

UPDATE dbo.itemstate SET lookupid = @new_lookup WHERE stateid = @activity_tpcert;

INSERT INTO dbversion(version) VALUES(550);

COMMIT TRAN