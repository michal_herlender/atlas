use [atlas];

begin tran

create table dbo.avalaraconfig (
	coid int not null,
	accountid int,
	avalaracode varchar(255),
	avalaraid int,
	environment varchar(255),
	licensekey varchar(255),
	primary key (coid)
);

go

alter table dbo.avalaraconfig
	add constraint FK_avalaraconfig_company
	foreign key (coid)
	references dbo.company;

alter table dbo.avalaraconfig
	add constraint UK_avalaraconfig_accountid unique (accountid);

insert into dbo.avalaraconfig(coid, accountid, licensekey, avalaracode, avalaraid, environment)
values (6682, 2001481432, 'D880FF78A489EFE6', 'DEFAULT', 2707451, 'Sandbox');

alter table dbo.address
	add validatedon datetime2;

alter table dbo.address
add validator int;

alter table dbo.address
add validatedby int;

alter table dbo.address
	add constraint FK_address_validatedby
	foreign key (validatedby)
	references dbo.contact;

alter table nominalcode
add avalarataxcode varchar(8);

go

update nominalcode set avalarataxcode = 'S0000000' where id = 159
update nominalcode set avalarataxcode = 'S0000000' where id = 160
update nominalcode set avalarataxcode = 'S0000000' where id = 161
update nominalcode set avalarataxcode = 'S0000000' where id = 162
update nominalcode set avalarataxcode = 'P0000000' where id = 163
update nominalcode set avalarataxcode = 'FR000000' where id = 164
update nominalcode set avalarataxcode = 'SW054000' where id = 165
update nominalcode set avalarataxcode = 'SP140000' where id = 166
update nominalcode set avalarataxcode = 'P0000000' where id = 167
update nominalcode set avalarataxcode = 'PR091000' where id = 168
update nominalcode set avalarataxcode = 'S0000000' where id = 169

go

insert into dbversion(version) values(768);

commit tran