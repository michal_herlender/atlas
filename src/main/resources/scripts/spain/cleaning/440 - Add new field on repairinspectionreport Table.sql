USE spain;

BEGIN TRAN

ALTER TABLE dbo.repairinspectionreport ADD validated bit DEFAULT 0 NOT NULL;

INSERT INTO dbversion (version) VALUES (440);

COMMIT TRAN