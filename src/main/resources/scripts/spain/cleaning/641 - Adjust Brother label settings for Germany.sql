-- Changes default media for Brother printer to 12 mm tape, and adjusts offset for 12 mm tape
-- Galen Beck - 2020-06-09

USE [atlas]
GO

BEGIN TRAN

-- Script 609 did identity insert, so it's guaranteed that the 12 mm tape has ID 3

UPDATE [dbo].[labelprinter]
   SET [media] = 3
 WHERE description = 'Brother PT-P750W'
GO

UPDATE [dbo].[alligatorlabelmedia]
   SET [paddingleft] = 3
 WHERE [name] = 'Brother P-touch 12 mm tape'
GO

INSERT INTO dbversion(version) VALUES (641)

COMMIT TRAN
