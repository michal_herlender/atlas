USE [atlas]
GO

BEGIN TRAN

	DECLARE @repair_calibrationtype INT,
			@repair_servicetype INT;
	
	SELECT @repair_servicetype = servicetypeid FROM dbo.servicetype
		WHERE  shortname LIKE 'REP';
		
	SELECT @repair_calibrationtype = caltypeid FROM dbo.calibrationtype
		WHERE  servicetypeid = @repair_servicetype;

	INSERT INTO atlas.dbo.calibrationaccreditationlevel (accredlevel,caltypeid)
	VALUES ('APPROVE', @repair_calibrationtype),
			('PERFORM', @repair_calibrationtype),
			('REMOVED', @repair_calibrationtype),
			('SIGN', @repair_calibrationtype),
			('SUPERVISED', @repair_calibrationtype);
		
	INSERT INTO dbversion(version) VALUES(622);

COMMIT TRAN