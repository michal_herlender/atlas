-- Author Galen Beck - 2016-11-30
-- Adds state group links for four active states that had no stategrouplink

-- 156 - Unable to determine correct status
-- 202 - Third party repair un-successful, awaiting fault report update
-- 204 - On hold - Awaiting approval of another item
-- 1020 - Called Off
-- 4066 - Awaiting internal delivery note

USE [spain]
GO

INSERT INTO [dbo].[stategrouplink]
           ([groupid],[stateid],[type])
     VALUES
           (32,156,null),
		   (36,202,null),
		   (32,204,null),
		   (32,1020,null),
		   (33,4066,null);
GO

