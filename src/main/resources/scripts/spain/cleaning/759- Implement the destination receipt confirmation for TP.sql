
USE [atlas]

BEGIN TRAN

DECLARE @destination_receipt_date_status INT,
		@destination_receipt_date_activity INT,
		@destination_receipt_date_lookup INT,
		@item_despatched_tp_activity INT,
		@item_despatched_tp_status INT,
		@new_outcomestatus INT;

-- update the receipt status and activity description
SELECT @destination_receipt_date_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting confirmation of client receipt';
SELECT @destination_receipt_date_activity = stateid FROM dbo.itemstate WHERE [description] = 'Client receipt confirmed';

UPDATE dbo.itemstate SET [description] = 'Awaiting confirmation of destination receipt' WHERE stateid = @destination_receipt_date_status;
UPDATE dbo.itemstatetranslation SET translation = 'Awaiting confirmation of destination receipt' WHERE stateid = @destination_receipt_date_status AND locale = 'en_GB';
UPDATE dbo.itemstatetranslation SET translation = 'En attente de confirmation de r�ception de destination' WHERE stateid = @destination_receipt_date_status AND locale = 'fr_FR';
UPDATE dbo.itemstatetranslation SET translation = 'Esperando confirmaci�n del recibo de destino' WHERE stateid = @destination_receipt_date_status AND locale = 'es_ES';
UPDATE dbo.itemstatetranslation SET translation = 'Warten auf Best�tigung des Zielempfangs' WHERE stateid = @destination_receipt_date_status AND locale = 'de_DE';

UPDATE dbo.itemstate SET [description] = 'Destination receipt confirmed' WHERE stateid = @destination_receipt_date_activity;
UPDATE dbo.itemstatetranslation SET translation = 'Destination receipt confirmed' WHERE stateid = @destination_receipt_date_activity AND locale = 'en_GB';
UPDATE dbo.itemstatetranslation SET translation = 'Re�u de destination confirm�' WHERE stateid = @destination_receipt_date_activity AND locale = 'fr_FR';
UPDATE dbo.itemstatetranslation SET translation = 'Recibo de destino confirmado' WHERE stateid = @destination_receipt_date_activity AND locale = 'es_ES';
UPDATE dbo.itemstatetranslation SET translation = 'Bestimmungsortbeleg best�tigt' WHERE stateid = @destination_receipt_date_activity AND locale = 'de_DE';

-- update the Lookup_ConfirmationClientReceiptRequired name
SELECT @destination_receipt_date_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_ConfirmationClientReceiptRequired';
UPDATE dbo.lookupinstance SET [lookup] = 'Lookup_ConfirmationDestinationReceiptRequired' WHERE id = @destination_receipt_date_lookup;

SELECT @item_despatched_tp_activity = stateid FROM dbo.itemstate WHERE [description] = 'Item despatched to third party';
SELECT @item_despatched_tp_status = stateid FROM dbo.itemstate WHERE [description] = 'Job item transferred to third party, service completed';

-- update the activityid for 2 existing lookupresults
DECLARE @item_despatch_client_activity INT;
SELECT @item_despatch_client_activity = stateid FROM dbo.itemstate WHERE [description] = 'Unit despatched to client';
UPDATE dbo.lookupresult SET activityid = @item_despatch_client_activity WHERE lookupid = @destination_receipt_date_lookup;

-- add new outcomestatus (1) 'Item despatched to third party' -> 'Awaiting confirmation of destination receipt'
INSERT INTO dbo.outcomestatus(defaultoutcome,activityid,statusid,lookupid)
VALUES (0,@item_despatched_tp_activity,@destination_receipt_date_status,null);

SELECT @new_outcomestatus = MAX(id) FROM dbo.outcomestatus;

-- add new lookupresult YES : os(1)
INSERT INTO dbo.lookupresult(resultmessage,result,outcomestatusid,lookupid,activityid)
VALUES (0, null, @new_outcomestatus, @destination_receipt_date_lookup, @item_despatched_tp_activity);

-- add new lookupresult NO : lookup_tprequirements
DECLARE @tp_requirements_lookup INT;
SELECT @tp_requirements_lookup = lookupid FROM dbo.itemstate WHERE stateid = @item_despatched_tp_activity;
INSERT INTO dbo.lookupresult(resultmessage,result,outcomestatusid,lookupid,activityid)
VALUES (1, @tp_requirements_lookup, null, @destination_receipt_date_lookup, @item_despatched_tp_activity);

-- add new Lookup_LastDeliveryType
INSERT INTO lookupinstance([lookup], lookupfunction) VALUES('Lookup_LastDeliveryType', 63);
DECLARE @lastdeliverytype_lookup INT;
SELECT @lastdeliverytype_lookup = MAX(id) FROM dbo.lookupinstance;

-- get outcomestatus (2) for 'Unit despatched to client' -> 'Work completed, despatched to client'
DECLARE @client_work_completed_status INT;
SELECT @client_work_completed_status = stateid FROM dbo.itemstate WHERE [description] = 'Work completed, despatched to client';
DECLARE @client_work_completed_os INT;
SELECT @client_work_completed_os = id FROM dbo.outcomestatus WHERE statusid = @client_work_completed_status AND activityid = @item_despatch_client_activity;

-- add new outcomestatus (3) for 'Item despatched to third party' -> 'Job item transferred to third party, service completed'
INSERT INTO dbo.outcomestatus(defaultoutcome,lookupid,activityid,statusid)
VALUES (0,null,@item_despatched_tp_activity,@item_despatched_tp_status);
DECLARE @tp_work_completed_os INT;
SELECT @tp_work_completed_os = max(id) FROM dbo.outcomestatus;

-- add two new lookupresults for cases : os(2) and os(3)
INSERT INTO dbo.lookupresult(resultmessage,result,outcomestatusid,lookupid,activityid)
VALUES (0, null, @client_work_completed_os, @lastdeliverytype_lookup, null),
	   (1, null, @tp_work_completed_os, @lastdeliverytype_lookup, null);

-- update the activities lookup 
UPDATE dbo.itemstate SET lookupid = @destination_receipt_date_lookup WHERE stateid = @item_despatched_tp_activity;
UPDATE dbo.itemstate SET lookupid = @lastdeliverytype_lookup WHERE stateid = @destination_receipt_date_activity;

INSERT INTO dbversion(version) VALUES(759);

COMMIT TRAN