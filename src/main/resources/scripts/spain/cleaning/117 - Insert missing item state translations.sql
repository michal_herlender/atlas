-- Author Galen Beck - 2017-01-20
-- Insert/Update french and spainish translations for some missing item state translations

USE [spain]
GO

CREATE PROCEDURE SetItemStateTranslation (
	@ItemStateId INT,
	@Translation VARCHAR(200),
	@Locale VARCHAR(255)
)
AS
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.itemstatetranslation WHERE stateid = @ItemStateId AND locale = @Locale) 
		INSERT INTO dbo.itemstatetranslation (stateid, locale, translation) VALUES (@ItemStateId, @Locale, @Translation) 
		ELSE UPDATE dbo.itemstatetranslation SET translation = @Translation WHERE stateid = @ItemStateId AND locale = @Locale;
END
GO

-- 4068 - Awaiting internal return delivery note
EXECUTE SetItemStateTranslation 4068, 'En attente du bordereau de livraison interne du retour', 'fr_FR';
EXECUTE SetItemStateTranslation 4068, 'Esperando la albar�n interna de retorno', 'es_ES';

-- 4069 - Awaiting despatch to returning business subdivision
EXECUTE SetItemStateTranslation 4069, 'En attente d''exp�dition au la division de retour', 'fr_FR';
EXECUTE SetItemStateTranslation 4069, 'Esperando env�o a la subdivision de negocios de retorno', 'es_ES';

-- 4070 - In transit to returning business subdivision
EXECUTE SetItemStateTranslation 4070, 'En transit vers la division de retour', 'fr_FR';
EXECUTE SetItemStateTranslation 4070, 'En tr�nsito a la subdivision de negocios de retorno', 'es_ES';

-- 4071 - Added to internal return delivery note
EXECUTE SetItemStateTranslation 4071, 'Ajout� au bordereau de livraison interne du retour', 'fr_FR';
EXECUTE SetItemStateTranslation 4071, 'A�adido a la albar�n interna de retorno', 'es_ES';

-- 4072 - Despatched to returning business subdivision
EXECUTE SetItemStateTranslation 4072, 'Exp�di� � la division de retour', 'fr_FR';
EXECUTE SetItemStateTranslation 4072, 'Enviado a la subdivision de negocios de retorno', 'es_ES';

-- 4073 - Received at returning business subdivision
EXECUTE SetItemStateTranslation 4073, 'Re�u � la division de retour', 'fr_FR';
EXECUTE SetItemStateTranslation 4073, 'Recibido en la divisi�n de retorno', 'es_ES';

-- 4074 - Awaiting PO for inter company work
EXECUTE SetItemStateTranslation 4074, 'En attente de bon de commande pour le travail inter-soci�t�', 'fr_FR';

-- 4075 - PO created for inter company work
EXECUTE SetItemStateTranslation 4075, 'Bon de commande cre� pour travail inter-soci�t�', 'fr_FR';

DROP PROCEDURE SetItemStateTranslation;