-- Script to insert imagefile data values to spainfiles database from spain
-- EF / GB 2017-05-23
-- Must be run AFTER startup so that the ImageFileData table is created in the spainfiles database
-- Adjust as needed for test deployment e.g. spain-test, spainfiles-test
-- Note, this will take up to ten minutes to run, lots of data!

USE [spainfiles]
GO

INSERT INTO [dbo].[ImageFileData]
           ([imageId]
           ,[filedata])
	SELECT id,
		filedata
	FROM [spain].[dbo].images
GO

INSERT INTO [dbo].[ImageThumbData]
           ([imageId]
           ,[thumbdata])
	SELECT id,
		thumbdata
	FROM [spain].[dbo].images
GO
