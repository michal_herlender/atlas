BEGIN TRAN

alter table deliveryitem alter column despatchedon datetime2;
INSERT INTO dbversion(version) VALUES(844);

COMMIT TRAN
