USE [spain]
BEGIN TRAN

    create table dbo.servicetype_contract (
       contractid int not null,
        servicetypeid int not null,
        primary key (contractid, servicetypeid)
    );

    alter table dbo.servicetype_contract 
       add constraint FKl31er5rxjr6dht7uhlbfmugq4 
       foreign key (servicetypeid) 
       references dbo.servicetype;

    alter table dbo.servicetype_contract 
       add constraint FKhkisdljwjm4jb1nb8ybxktg2i 
       foreign key (contractid) 
       references dbo.contract;
       
    INSERT INTO dbversion(version) VALUES (425);

COMMIT TRAN