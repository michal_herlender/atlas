-- Updates null values in [scheduledtask] [lastrunsuccess] field to false, and adds constraint for future insertions.
-- (schedule task page won't work with newly inserted data, due to nulls in primitive boolean field)

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[scheduledtask]
   SET [lastrunsuccess] = 0
 WHERE [lastrunsuccess] is null;
GO

ALTER TABLE [dbo].[scheduledtask] ALTER COLUMN [lastrunsuccess] tinyint NOT NULL;
GO

INSERT INTO dbversion(version) VALUES(475);
GO

COMMIT TRAN