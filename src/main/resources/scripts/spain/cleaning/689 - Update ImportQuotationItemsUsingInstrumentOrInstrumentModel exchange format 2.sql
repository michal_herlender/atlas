
-- Update ImportQuotationItemsUsingInstrumentOrInstrumentModel exchange format.
-- The final price and the service type are no longer mandatory fields.
USE [atlas]
GO

BEGIN TRAN

DECLARE @lastexchangeFormatid INT;

SET @lastexchangeFormatid = (SELECT id FROM dbo.exchangeformat WHERE name LIKE 'ImportQuotationItemsUsingInstrumentOrInstrumentModel');

UPDATE dbo.exchangeformatfieldnamedetails 
SET mandatory = 0 
WHERE exchangeFormatid = @lastexchangeFormatid AND fieldName LIKE 'EXPECTED_SERVICE';

UPDATE dbo.exchangeformatfieldnamedetails 
SET mandatory = 0 
WHERE exchangeFormatid = @lastexchangeFormatid AND fieldName LIKE 'FINAL_PRICE';

INSERT INTO dbversion(version) VALUES(689);

COMMIT TRAN