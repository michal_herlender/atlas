USE [atlasfiles]
GO

BEGIN TRAN

ALTER TABLE dbo.PrebookingFileData ADD [fileName] VARCHAR(255) NULL;

COMMIT TRAN

USE [atlas]
GO

BEGIN TRAN

INSERT INTO dbversion(version) VALUES (574)

COMMIT TRAN