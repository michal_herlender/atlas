-- Note, this script takes a while to run (3 min) - requires some patience!
-- 2021-04-07 GB : Little updates to be compatible with SQL Server 2017

Use [atlas]
Go

BEGIN TRAN

--ASSET
ALTER TABLE [asset]  ALTER COLUMN editableaddedon date;
ALTER TABLE [asset]  ALTER COLUMN purchasedon date;
ALTER TABLE [asset]  ALTER COLUMN sysaddedon date;

USE [atlas]
GO

--COMPANY

ALTER TABLE [company]  ALTER COLUMN createdate date;

ALTER TABLE [companyaccreditation]  ALTER COLUMN expirydate date;
ALTER TABLE [companyaccreditation]  ALTER COLUMN lastemailalert date;


ALTER TABLE [companysettingsforallocatedcompany]  ALTER COLUMN onstopsince date;


DROP INDEX [_dta_index_contact_7_414624520__K1_K27_K29_K28_K11_K13_2_3_4_5_6_7_8_9_10_12_14_15_16_17_18_19_20_21_22_23_24_25_26] ON [dbo].[contact];
DROP INDEX [_dta_index_contact_7_414624520__K27_K1_K29_K28_K11_K13_2_3_4_5_6_7_8_9_10_12_14_15_16_17_18_19_20_21_22_23_24_25_26] ON [dbo].[contact];
DROP INDEX [_dta_index_contact_7_414624520__K3_K27_K1_K29_K28_K11_K13_2_4_5_6_7_8_9_10_12_14_15_16_17_18_19_20_21_22_23_24_25_26] ON [dbo].[contact];
ALTER TABLE [contact]  ALTER COLUMN createdate date;
CREATE NONCLUSTERED INDEX [_dta_index_contact_7_414624520__K27_K1_K29_K28_K11_K13_2_3_4_5_6_7_8_9_10_12_14_15_16_17_18_19_20_21_22_23_24_25_26] ON [dbo].[contact]
(
	[subdivid] ASC,
	[personid] ASC,
	[groupid] ASC,
	[title] ASC,
	[firstname] ASC,
	[lastname] ASC
)
INCLUDE([log_lastmodified],[active],[createdate],[creditrating],[deactreason],[deacttime],[email],[encryptedpassword],[fax],[lastactivity],[mobile],[position],[preference],[salescomm],[statusmessage],[telephone],[telephoneext],[log_userid],[creditCard_ccid],[defaddress],[deflocation],[defprinterid],[discount]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
CREATE NONCLUSTERED INDEX [_dta_index_contact_7_414624520__K3_K27_K1_K29_K28_K11_K13_2_4_5_6_7_8_9_10_12_14_15_16_17_18_19_20_21_22_23_24_25_26] ON [dbo].[contact]
(
	[active] ASC,
	[subdivid] ASC,
	[personid] ASC,
	[groupid] ASC,
	[title] ASC,
	[firstname] ASC,
	[lastname] ASC
)
INCLUDE([log_lastmodified],[createdate],[creditrating],[deactreason],[deacttime],[email],[encryptedpassword],[fax],[lastactivity],[mobile],[position],[preference],[salescomm],[statusmessage],[telephone],[telephoneext],[log_userid],[creditCard_ccid],[defaddress],[deflocation],[defprinterid],[discount]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [_dta_index_contact_7_414624520__K1_K27_K29_K28_K11_K13_2_3_4_5_6_7_8_9_10_12_14_15_16_17_18_19_20_21_22_23_24_25_26] ON [dbo].[contact]
(
	[personid] ASC,
	[subdivid] ASC,
	[groupid] ASC,
	[title] ASC,
	[firstname] ASC,
	[lastname] ASC
)
INCLUDE([log_lastmodified],[active],[createdate],[creditrating],[deactreason],[deacttime],[email],[encryptedpassword],[fax],[lastactivity],[mobile],[position],[preference],[salescomm],[statusmessage],[telephone],[telephoneext],[log_userid],[creditCard_ccid],[defaddress],[deflocation],[defprinterid],[discount]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

DROP INDEX [_dta_index_subdiv_7_1419868125__K1_K12_K10_K11_2_3_4_5_6_7_8_9] ON [dbo].[subdiv];
DROP INDEX [_dta_index_subdiv_7_1419868125__K10_K1_K11_K12_2_3_4_5_6_7_8_9] ON [dbo].[subdiv];
DROP INDEX [_dta_index_subdiv_7_1419868125__K10_K1_K11_K12_K8_2_3_4_5_6_7_9] ON [dbo].[subdiv];
DROP INDEX [_dta_index_subdiv_7_1419868125__K4_K10_K1_K11_K12_K8_2_3_5_6_7_9] ON [dbo].[subdiv];

ALTER TABLE [subdiv] ALTER COLUMN createdate date;

CREATE NONCLUSTERED INDEX [_dta_index_subdiv_7_1419868125__K4_K10_K1_K11_K12_K8_2_3_5_6_7_9] ON [dbo].[subdiv]
(
	[active] ASC,
	[coid] ASC,
	[subdivid] ASC,
	[defaddrid] ASC,
	[defpersonid] ASC,
	[subname] ASC
)
INCLUDE([log_lastmodified],[acccontact],[createdate],[deactreason],[deacttime],[log_userid]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [_dta_index_subdiv_7_1419868125__K10_K1_K11_K12_K8_2_3_4_5_6_7_9] ON [dbo].[subdiv]
(
	[coid] ASC,
	[subdivid] ASC,
	[defaddrid] ASC,
	[defpersonid] ASC,
	[subname] ASC
)
INCLUDE([log_lastmodified],[acccontact],[active],[createdate],[deactreason],[deacttime],[log_userid]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [_dta_index_subdiv_7_1419868125__K10_K1_K11_K12_2_3_4_5_6_7_8_9] ON [dbo].[subdiv]
(
	[coid] ASC,
	[subdivid] ASC,
	[defaddrid] ASC,
	[defpersonid] ASC
)
INCLUDE([log_lastmodified],[acccontact],[active],[createdate],[deactreason],[deacttime],[subname],[log_userid]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE NONCLUSTERED INDEX [_dta_index_subdiv_7_1419868125__K1_K12_K10_K11_2_3_4_5_6_7_8_9] ON [dbo].[subdiv]
(
	[subdivid] ASC,
	[defpersonid] ASC,
	[coid] ASC,
	[defaddrid] ASC
)
INCLUDE([log_lastmodified],[acccontact],[active],[createdate],[deactreason],[deacttime],[subname],[log_userid]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

--Delivery Note
DROP STATISTICS [dbo].[courierdespatch].[_dta_stat_251147940_5_4];
DROP STATISTICS [dbo].[courierdespatch].[_dta_stat_251147940_7_4_5];
ALTER TABLE [courierdespatch] ALTER COLUMN creationdate date;
ALTER TABLE [courierdespatch] ALTER COLUMN despatchdate date;
CREATE STATISTICS [_dta_stat_251147940_7_4_5] ON [dbo].[courierdespatch]([cdtypeid], [creationdate], [despatchdate]);
CREATE STATISTICS [_dta_stat_251147940_5_4] ON [dbo].[courierdespatch]([despatchdate], [creationdate]);
GO

ALTER TABLE [delivery] ALTER COLUMN deliverydate date;
ALTER TABLE [delivery] ALTER COLUMN creationdate date;

ALTER TABLE [deliveryitem] ALTER COLUMN despatchedon date;


--Hire

ALTER TABLE [hire] ALTER COLUMN enquirydate date;
ALTER TABLE [hire] ALTER COLUMN hiredate date;

ALTER TABLE [hireitem] ALTER COLUMN offhiredate date;
ALTER TABLE [hirereplacedinstrument] ALTER COLUMN replacedon date;

ALTER TABLE [hiresuspenditem] ALTER COLUMN enddate date;
ALTER TABLE [hiresuspenditem] ALTER COLUMN startdate date;

--Instrument

DROP INDEX [_dta_index_instrument_9_740197687__K17_1_3_10_11_15_22_24_27_39_40_44_50] ON [dbo].[instrument];

ALTER TABLE [instrument] ALTER COLUMN addedon date;
ALTER TABLE [instrument] ALTER COLUMN recalldate date;
ALTER TABLE [instrument] ALTER COLUMN replacedon date;
ALTER TABLE [instrument] ALTER COLUMN scrappedon date;

CREATE NONCLUSTERED INDEX [_dta_index_instrument_9_740197687__K17_1_3_10_11_15_22_24_27_39_40_44_50] ON [dbo].[instrument]
(
	[serialno] ASC
)
INCLUDE([plantid],[addedon],[recalldate],[plantno],[scrapped],[coid],[lastcalid],[modelid],[customerdescription],[formerbarcode],[modelname],[defaultservicetype]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];


ALTER TABLE [instrumentvalidationissue] ALTER COLUMN resolvedon date;
ALTER TABLE [instrumentvalidationissue] ALTER COLUMN raisedon date;

ALTER TABLE [permitedcalibrationextension] ALTER COLUMN extensionEndDate date;

ALTER TABLE [instmodel] ALTER COLUMN addedon date;

ALTER TABLE [component] ALTER COLUMN stocklastchecked date;

--Calibration & JOBS
DROP STATISTICS [dbo].[calibration].[_dta_stat_1653580929_5_1];
ALTER TABLE [calibration] ALTER COLUMN caldate date;
CREATE STATISTICS [_dta_stat_1653580929_5_1] ON [dbo].[calibration]([caldate], [id]);
GO


ALTER TABLE [standardused] ALTER COLUMN recalldate date;


DROP STATISTICS [dbo].[job].[_dta_stat_464720708_1_9];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_1_14_9_7];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_1_14_20_22_9_7];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_1_14_22_9_7];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_1_20_22_9_7];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_1_22_9_7];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_9_7_1];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_9_7_14];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_9_7_24];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_22_9_7];
DROP STATISTICS [dbo].[job].[_dta_stat_464720708_22_24_9_7];

DROP INDEX [_dta_index_job_7_464720708__K9D_K7D_1_22] ON [dbo].[job]


ALTER TABLE [job] ALTER COLUMN agreeddeldate date;
ALTER TABLE [job] ALTER COLUMN datecomplete date;
ALTER TABLE [job] ALTER COLUMN regdate date;

CREATE STATISTICS [_dta_stat_464720708_1_9] ON [dbo].[job]([jobid], [regdate]);
CREATE STATISTICS [_dta_stat_464720708_1_14_9_7] ON [dbo].[job]([jobid], [personid], [regdate], [jobno]);
CREATE STATISTICS [_dta_stat_464720708_1_14_20_22_9_7] ON [dbo].[job]([jobid], [personid], [statusid], [returnto], [regdate], [jobno]);
CREATE STATISTICS [_dta_stat_464720708_1_14_22_9_7] ON [dbo].[job]([jobid], [personid], [returnto], [regdate], [jobno]);
CREATE STATISTICS [_dta_stat_464720708_1_20_22_9_7] ON [dbo].[job]([jobid], [statusid], [returnto], [regdate], [jobno]);
CREATE STATISTICS [_dta_stat_464720708_9_7_1] ON [dbo].[job]([regdate], [jobno], [jobid]);
CREATE STATISTICS [_dta_stat_464720708_9_7_14] ON [dbo].[job]([regdate], [jobno], [personid]);
CREATE STATISTICS [_dta_stat_464720708_9_7_24] ON [dbo].[job]([regdate], [jobno], [typeid]);
CREATE STATISTICS [_dta_stat_464720708_22_9_7] ON [dbo].[job]([returnto], [regdate], [jobno]);
CREATE STATISTICS [_dta_stat_464720708_22_24_9_7] ON [dbo].[job]([returnto], [typeid], [regdate], [jobno]);

CREATE NONCLUSTERED INDEX [_dta_index_job_7_464720708__K9D_K7D_1_22] ON [dbo].[job]
(
	[regdate] DESC,
	[jobno] DESC
)
INCLUDE([jobid],[returnto]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
GO



DROP INDEX [_dta_index_jobitem_7_2016726237__K38_K33_K31_K6] ON [dbo].[jobitem];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_1_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_38_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_6_38_1];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_31_6_33];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_6_31_7];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_38_31_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_31_21_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_6_21_1_31];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_6_33_38_31];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_38_1_31_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_1_33_21_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_6_21_33_31];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_1_33_38_6_31];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_21_1_31_38_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_1_38_21_33_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_1_21_33_31_6];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_33_38_21_6_31_7];
DROP STATISTICS [dbo].[jobitem].[_dta_stat_2016726237_21_33_31_38_6_1];
DROP INDEX [IDX_jobitem_duedate_addrid_jobid_stateid] ON [dbo].[jobitem];

ALTER TABLE [jobitem] ALTER COLUMN agreeddeldate date;
ALTER TABLE [jobitem] ALTER COLUMN duedate date;
ALTER TABLE [jobitem] ALTER COLUMN transitdate date;
CREATE NONCLUSTERED INDEX [_dta_index_jobitem_7_2016726237__K38_K33_K31_K6] ON [dbo].[jobitem]
(
	[stateid] ASC,
	[procid] ASC,
	[jobid] ASC,
	[duedate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
CREATE NONCLUSTERED INDEX [IDX_jobitem_duedate_addrid_jobid_stateid] ON [dbo].[jobitem]
(
	[duedate] ASC
)
INCLUDE([addrid],[jobid],[stateid]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
CREATE STATISTICS [_dta_stat_2016726237_1_6] ON [dbo].[jobitem]([jobitemid], [duedate]);
CREATE STATISTICS [_dta_stat_2016726237_38_6] ON [dbo].[jobitem]([stateid], [duedate]);
CREATE STATISTICS [_dta_stat_2016726237_6_38_1] ON [dbo].[jobitem]([duedate], [stateid], [jobitemid]);
CREATE STATISTICS [_dta_stat_2016726237_31_6_33] ON [dbo].[jobitem]([jobid], [duedate], [procid]);
CREATE STATISTICS [_dta_stat_2016726237_6_31_7] ON [dbo].[jobitem]([duedate], [jobid], [itemno]);
CREATE STATISTICS [_dta_stat_2016726237_38_31_6] ON [dbo].[jobitem]([stateid], [jobid], [duedate]);
CREATE STATISTICS [_dta_stat_2016726237_31_21_6] ON [dbo].[jobitem]([jobid], [caltypeid], [duedate]);
CREATE STATISTICS [_dta_stat_2016726237_6_21_1_31] ON [dbo].[jobitem]([duedate], [caltypeid], [jobitemid], [jobid]);
CREATE STATISTICS [_dta_stat_2016726237_6_33_38_31] ON [dbo].[jobitem]([duedate], [procid], [stateid], [jobid]);
CREATE STATISTICS [_dta_stat_2016726237_1_33_21_6] ON [dbo].[jobitem]([jobitemid], [procid], [caltypeid], [duedate]);
CREATE STATISTICS [_dta_stat_2016726237_6_21_33_31] ON [dbo].[jobitem]([duedate], [caltypeid], [procid], [jobid]);
CREATE STATISTICS [_dta_stat_2016726237_1_33_38_6_31] ON [dbo].[jobitem]([jobitemid], [procid], [stateid], [duedate], [jobid]);
CREATE STATISTICS [_dta_stat_2016726237_21_1_31_38_6] ON [dbo].[jobitem]([caltypeid], [jobitemid], [jobid], [stateid], [duedate]);
CREATE STATISTICS [_dta_stat_2016726237_1_38_21_33_6] ON [dbo].[jobitem]([jobitemid], [stateid], [caltypeid], [procid], [duedate]);
CREATE STATISTICS [_dta_stat_2016726237_1_21_33_31_6] ON [dbo].[jobitem]([jobitemid], [caltypeid], [procid], [jobid], [duedate]);
CREATE STATISTICS [_dta_stat_2016726237_33_38_21_6_31_7] ON [dbo].[jobitem]([procid], [stateid], [caltypeid], [duedate], [jobid], [itemno]);
CREATE STATISTICS [_dta_stat_2016726237_21_33_31_38_6_1] ON [dbo].[jobitem]([caltypeid], [procid], [jobid], [stateid], [duedate], [jobitemid]);

ALTER TABLE [jobexpenseitemnotinvoiced] ALTER COLUMN [date] date;
ALTER TABLE [jobitemnotinvoiced] ALTER COLUMN [date] date;

ALTER TABLE [pat] ALTER COLUMN testdate date;

GO

-- User & Pricing

DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_5_9_31]
DROP INDEX [_dta_index_invoice_7_1236199454__K16_K1_K24_K30_K20_K11_K27_K13_8_18_19] ON [dbo].[invoice];
DROP INDEX [_dta_index_invoice_7_1236199454__K20_K1_K16_K24_K30_K11_K27_K13_8_18_19] ON [dbo].[invoice];

ALTER TABLE [users] ALTER COLUMN last_login date;
ALTER TABLE [users] ALTER COLUMN registered_date date;

ALTER TABLE [creditnote] ALTER COLUMN reqdate date;
ALTER TABLE [creditnote] ALTER COLUMN expirydate date;
ALTER TABLE [hire] ALTER COLUMN reqdate date;
ALTER TABLE [hire] ALTER COLUMN expirydate date;
ALTER TABLE [invoice] ALTER COLUMN reqdate date;
ALTER TABLE [invoice] ALTER COLUMN expirydate date;
ALTER TABLE [jobcosting] ALTER COLUMN reqdate date;
ALTER TABLE [jobcosting] ALTER COLUMN expirydate date;
ALTER TABLE [purchaseorder] ALTER COLUMN reqdate date;
ALTER TABLE [purchaseorder] ALTER COLUMN expirydate date;
ALTER TABLE [quotation] ALTER COLUMN reqdate date;
ALTER TABLE [quotation] ALTER COLUMN expirydate date;
ALTER TABLE [tpquotation] ALTER COLUMN reqdate date;
ALTER TABLE [tpquotation] ALTER COLUMN expirydate date;
ALTER TABLE [tpquoterequest] ALTER COLUMN reqdate date;
ALTER TABLE [tpquoterequest] ALTER COLUMN expirydate date;


ALTER TABLE [invoice] ALTER COLUMN invoicedate date;

ALTER TABLE [purchaseorderitem] ALTER COLUMN deliverydate date;

ALTER TABLE [supplierinvoice] ALTER COLUMN invoicedate date;
ALTER TABLE [supplierinvoice] ALTER COLUMN paymentdate date;

CREATE STATISTICS [_dta_stat_1010818663_5_9_31] ON [dbo].[quotation]([expirydate], [regdate], [personid]);
CREATE NONCLUSTERED INDEX [_dta_index_invoice_7_1236199454__K16_K1_K24_K30_K20_K11_K27_K13_8_18_19] ON [dbo].[invoice]
(
	[accountstatus] ASC,
	[id] ASC,
	[currencyid] ASC,
	[typeid] ASC,
	[pricingtype] ASC,
	[carriage] ASC,
	[addressid] ASC,
	[totalcost] ASC
)
INCLUDE([rate],[invno],[invoicedate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
CREATE NONCLUSTERED INDEX [_dta_index_invoice_7_1236199454__K20_K1_K16_K24_K30_K11_K27_K13_8_18_19] ON [dbo].[invoice]
(
	[pricingtype] ASC,
	[id] ASC,
	[accountstatus] ASC,
	[currencyid] ASC,
	[typeid] ASC,
	[carriage] ASC,
	[addressid] ASC,
	[totalcost] ASC
)
INCLUDE([rate],[invno],[invoicedate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
GO

--Procedures & Quotations

DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_21_1];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_1_36_21];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_1_21_9];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_21_9];
DROP STATISTICS [dbo].[quotation].[_dta_stat_1010818663_1_9_36_21];
DROP STATISTICS [dbo].[quotationrequest].[_dta_stat_1474820316_9_15_18];
DROP STATISTICS [dbo].[quotationrequest].[_dta_stat_1474820316_9_18_22_20_21];
DROP STATISTICS [dbo].[quotationrequest].[_dta_stat_1474820316_22_20_21_23_9_18_15];
DROP STATISTICS [dbo].[quotationrequest].[_dta_stat_1474820316_15_18];

ALTER TABLE [defaultstandard] ALTER COLUMN seton date;

ALTER TABLE [procs] ALTER COLUMN deactivatedon date;

ALTER TABLE [procedureaccreditation] ALTER COLUMN awarded date;

ALTER TABLE [quotation] ALTER COLUMN acceptedon date;
ALTER TABLE [quotation] ALTER COLUMN duedate date;
ALTER TABLE [quotation] ALTER COLUMN contractstartdate date;

ALTER TABLE [quotationrequest] ALTER COLUMN duedate date;
ALTER TABLE [quotationrequest] ALTER COLUMN reqdate date;

ALTER TABLE [scheduledquotationrequestdaterange] ALTER COLUMN startdate date;
ALTER TABLE [scheduledquotationrequestdaterange] ALTER COLUMN finishdate date;

CREATE STATISTICS [_dta_stat_1010818663_21_1] ON [dbo].[quotation]([duedate], [id]);
CREATE STATISTICS [_dta_stat_1010818663_21_9] ON [dbo].[quotation]([duedate], [regdate]);
CREATE STATISTICS [_dta_stat_1010818663_1_21_9] ON [dbo].[quotation]([id], [duedate], [regdate]);
CREATE STATISTICS [_dta_stat_1010818663_1_36_21] ON [dbo].[quotation]([id], [statusid], [duedate]);
CREATE STATISTICS [_dta_stat_1010818663_1_9_36_21] ON [dbo].[quotation]([id], [regdate], [statusid], [duedate]);
CREATE STATISTICS [_dta_stat_1474820316_9_15_18] ON [dbo].[quotationrequest]([duedate], [reqdate], [status]);
CREATE STATISTICS [_dta_stat_1474820316_9_18_22_20_21] ON [dbo].[quotationrequest]([duedate], [status], [loggedby], [coid], [personid]);
CREATE STATISTICS [_dta_stat_1474820316_22_20_21_23_9_18_15] ON [dbo].[quotationrequest]([loggedby], [coid], [personid], [quoteid], [duedate], [status], [reqdate]);
CREATE STATISTICS [_dta_stat_1474820316_15_18] ON [dbo].[quotationrequest]([reqdate], [status]);
GO

--Recall & Schedule & System

ALTER TABLE [recall] ALTER COLUMN [date] date;
ALTER TABLE [recall] ALTER COLUMN datefrom date;
ALTER TABLE [recall] ALTER COLUMN dateto date;

ALTER TABLE [recallrule] ALTER COLUMN deactivatedon date;
ALTER TABLE [recallrule] ALTER COLUMN seton date;
ALTER TABLE [recallrule] ALTER COLUMN calibrateOn date;

ALTER TABLE [schedule] ALTER COLUMN createdon date;
ALTER TABLE [schedule] ALTER COLUMN scheduledate date;

ALTER TABLE [closeddate] ALTER COLUMN closeddate date;
ALTER TABLE [upcomingwork] ALTER COLUMN duedate date;
ALTER TABLE [upcomingwork] ALTER COLUMN startdate date;

GO

--Tools & TpCost & Workflow
ALTER TABLE [tpquotelink] ALTER COLUMN [date] date;
ALTER TABLE [tpquoterequest] ALTER COLUMN receiptdate date;
ALTER TABLE [tpquoterequest] ALTER COLUMN duedate date;

ALTER TABLE [engineerallocation] ALTER COLUMN allocatedfor date;
ALTER TABLE [engineerallocation] ALTER COLUMN allocatedon date;
ALTER TABLE [engineerallocation] ALTER COLUMN allocateduntil date;
GO

INSERT INTO dbversion VALUES (798);

COMMIT TRAN