delete from servicetypelongnametranslation;

insert into servicetypelongnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.longname from servicetype;

insert into servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Calibración acreditada en laboratorio' 
from servicetype
where servicetype.longname = 'In-House Accredited Calibration';

insert into servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Calibración trazable en laboratorio' 
from servicetype
where servicetype.longname = 'In-House Standard Calibration';

insert into servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Calibración acreditada in situ' 
from servicetype
where servicetype.longname = 'On-Site Accredited Calibration';

insert into servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Calibración trazable in situ' 
from servicetype
where servicetype.longname = 'On-Site Standard Calibration';

insert into servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Otro' 
from servicetype
where servicetype.longname = 'Other';

insert into servicetypelongnametranslation (servicetypeid, locale, translation)
select servicetype.servicetypeid, 'es_ES', 'Reparar' 
from servicetype
where servicetype.longname = 'Repair';

delete from serviceypeshortnametranslation;

insert into serviceypeshortnametranslation (servicetypeid,locale,translation)
select servicetype.servicetypeid,'en_GB',servicetype.shortname from servicetype;