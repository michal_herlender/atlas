-- Note : this script became utility U021 (with evolutions) to recalculate quotation costs
-- It's moved to the utility area in case we'd like to run it again
-- Removed the main part of script part to be faster for deployment/upgrades, but just preserving the sequence number for consistency
-- If it's been run already locally there's no need to rerun it.  GB 2020-07-24

USE [atlas]
GO

BEGIN TRAN




INSERT INTO dbversion(version) VALUES(673);

COMMIT TRAN

