USE [atlas]
GO

BEGIN TRAN

DECLARE @jc_adveso_new_activity int,
        @new_hook int,
        @new_lookup int;
      
SELECT @jc_adveso_new_activity = stateid FROM dbo.itemstate WHERE [description] = 'Job Costing is available to External Management System';

SELECT @new_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_JobCostingClientDecision';
	
DELETE FROM dbo.itemstatetranslation WHERE stateid = @jc_adveso_new_activity;

DELETE FROM dbo.stategrouplink WHERE groupid = 57 AND stateid = @jc_adveso_new_activity;

DELETE FROM dbo.hookactivity WHERE activityid = @jc_adveso_new_activity;

DELETE FROM dbo.nextactivity WHERE activityid = @jc_adveso_new_activity;

UPDATE dbo.itemstate SET lookupid = NULL WHERE stateid = @jc_adveso_new_activity;

DELETE FROM dbo.lookupresult WHERE activityid = @jc_adveso_new_activity AND lookupid = @new_lookup;

DELETE FROM dbo.outcomestatus WHERE activityid = @jc_adveso_new_activity;
 
DELETE FROM dbo.hook WHERE [name] = 'issue-job-costing-to-adveso';

DELETE FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_JobCostingClientDecision';

DELETE FROM dbo.jobitemaction WHERE activityid = @jc_adveso_new_activity;

DELETE FROM dbo.itemstate WHERE [description] = 'Job Costing is available to External Management System';

--delete all stategrouplink linked with group 57 - UPLOAD_COSTING_TO_ADVESO
DELETE FROM dbo.stategrouplink WHERE groupid = 57;

--set stategrouplink for activity '4124 - Repair costing issued to client'
INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(55, 4124, 'ALLOCATED_SUBDIV');
--set stategrouplink for activity '4086 - Onsite - repair/adjustment costing issued to client'
INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(55, 4086, 'ALLOCATED_SUBDIV');
--set stategrouplink for activity '183 - Inspection costing issued'
INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(55, 183, 'ALLOCATED_SUBDIV');
--set stategrouplink for activity '146 - Exchange costing issued to client'
INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(55, 146, 'ALLOCATED_SUBDIV');
--set stategrouplink for activity '123 - Costing issued for further work at third party'
INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(55, 123, 'ALLOCATED_SUBDIV');
--set stategrouplink for activity '105 - Job costing issued to client for third party work'
INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(55, 105, 'ALLOCATED_SUBDIV');
--set stategrouplink for activity '137 - Job costing issued to client after third party return'
INSERT INTO dbo.stategrouplink(groupid, stateid, [type]) VALUES(55, 137, 'ALLOCATED_SUBDIV');

ALTER TABLE dbo.jobcosting ADD issuemethod VARCHAR(20);
ALTER TABLE dbo.advesojobItemactivityqueue ADD linkedentitytype VARCHAR(40);
ALTER TABLE dbo.advesojobItemactivityqueue ADD linkedentityid int;
GO

UPDATE dbo.advesojobItemactivityqueue
SET
    linkedentitytype = CASE WHEN certificateid IS NOT NULL THEN 'Certificate' ELSE CASE WHEN jobcostingfilename IS NOT NULL THEN 'JobCosting' ELSE NULL END END,
    linkedentityid = CASE WHEN certificateid IS NOT NULL THEN certificateid ELSE NULL END;

ALTER TABLE dbo.advesojobItemactivityqueue DROP advesojobItemactivityqueue_certificate_FK
ALTER TABLE dbo.advesojobItemactivityqueue DROP COLUMN certificateid;

GO
INSERT INTO dbversion(version) VALUES(495);

COMMIT TRAN