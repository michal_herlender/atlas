-- Updates some of the Alligator profile names for Bilbao (run after script 247)
-- Galen Beck - 2018-04-20

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL AF-ACEL Production'
 WHERE [description] = 'ES-TIC-BIL HF-ACEL Production';

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL AF-ACEL Test'
 WHERE [description] = 'ES-TIC-BIL HF-ACEL Test';

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL AF-SONO Production'
 WHERE [description] = 'ES-TIC-BIL HF-SONO Production';

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL AF-SONO Test'
 WHERE [description] = 'ES-TIC-BIL HF-SONO Test';

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL MEC Production'
 WHERE [description] = 'ES-TIC-BIL HF-MEC Production';

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL MEC Test'
 WHERE [description] = 'ES-TIC-BIL HF-MEC Test';

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL TEMP Production'
 WHERE [description] = 'ES-TIC-BIL HF-TEMP Production';

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL TEMP Test'
 WHERE [description] = 'ES-TIC-BIL HF-TEMP Test';

INSERT INTO dbversion (version) VALUES (248);

COMMIT TRAN