-- Updates recall configuration to use new classes that run individual smaller transactions

USE [spain]
GO

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.core.recall.generator.RecallGeneratorTaskImpl'
 WHERE [classname] = 'org.trescal.cwms.core.recall.generator.RecallGeneratorServiceImpl';

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.core.recall.generator.RecallSenderTaskImpl'
 WHERE [classname] = 'org.trescal.cwms.core.recall.generator.RecallSenderServiceImpl';

GO

INSERT INTO dbversion (version) VALUES (207);

GO