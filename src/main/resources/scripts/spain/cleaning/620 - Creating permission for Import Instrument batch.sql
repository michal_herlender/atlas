use atlas
go

BEGIN TRAN

	Insert into dbo.permission(groupId,permission) values 
	(1, 'IMPORT_INSTRUMENTS_BATCH')
		
	INSERT INTO dbversion(version) VALUES(620);

COMMIT TRAN