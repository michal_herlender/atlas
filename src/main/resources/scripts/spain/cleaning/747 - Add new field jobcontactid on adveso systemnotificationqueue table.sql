USE [atlas]
GO

BEGIN TRAN

	-- this field will be filled to refer the job contact in case of 
	-- instrument and job belong to two different subdivisions
	ALTER TABLE dbo.notificationsystemqueue ADD jobcontactid INT NULL;

	ALTER TABLE dbo.notificationsystemqueue ADD CONSTRAINT FK_notificationqueue_contact 
	FOREIGN KEY (jobcontactid) REFERENCES dbo.contact(personid);

	INSERT INTO dbversion(version) VALUES(747);

COMMIT TRAN