-- Creates a default alias group for Intermes
-- Note, if any exchange formats for Intermes have already been created (doesn't appear to be the case anywhere), they should be manually updated

USE [atlas];

BEGIN TRAN

-- get a contact
declare @contactid int, @business_coid int, @ag1 int, @ag1default bit

select @contactid = c.personid from dbo.contact c INNER JOIN dbo.users u ON u.personid = c.personid WHERE u.username = 'GalenB';

select @business_coid = c.coid
from company c where c.coname like 'Intermes'

-- check if the business company has already a default alias group
if (select max(cast(ag.defaultBusinessCompany as INT)) from aliasgroup ag where ag.orgid = @business_coid) = 1
	 set @ag1default = 0
else 
	set @ag1default = 1

-- create default alias group for 'Intermes'
INSERT INTO dbo.aliasgroup (lastModified,createdOn,description,name,lastModifiedBy,createdby,defaultBusinessCompany,orgid)
	VALUES ('2020-01-15 12:00:00.000','2020-01-15 12:00:00.000','Default alias group for business company','Default alias group (Intermes)',@contactid,@contactid,@ag1default,@business_coid);
select @ag1 = max(ag.id) from aliasgroup ag;

-- aliases for interval unit
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('an','YEAR',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('ann�es','YEAR',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('A','YEAR',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('mois','MONTH',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('M','MONTH',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('semaine','WEEK',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('semaines','WEEK',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('S','WEEK',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('jour','DAY',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('jours','DAY',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('J','DAY',@ag1);

INSERT INTO dbversion(version) VALUES(599);

COMMIT TRAN


