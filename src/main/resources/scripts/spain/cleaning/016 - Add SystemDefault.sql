/* Add Spain Business Companies - Author Tony Provost 2016-002-04 */

BEGIN TRAN 

USE [SPAIN];
GO

-- 2016-02-04 - TP: Add system default for Spain Data Migration
-- Add new Groups
SET IDENTITY_INSERT [systemdefaultgroup] ON
GO

INSERT INTO [systemdefaultgroup] ([id], [description], [dname])
VALUES 
	(8, 'Client FTP attributes', 'Client - FTP')
	,(9, 'Client Certificate attributes', 'Client - Certificate' )
GO

SET IDENTITY_INSERT [systemdefaultgroup] OFF
GO

-- Define New Parameters for Group "Client FTP attributes"
SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 30, 'string', 'Defines the FTP Server of the company', 'FTP Server', '', 0, 0, 8, 1 
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefault] WHERE [defaultid] = 30)
INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 31, 'string', 'Defines the FTP User of the company', 'FTP User', '', 0, 0, 8, 1 
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefault] WHERE [defaultid] = 31)
INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 32, 'string', 'Defines the FTP Password of the company', 'FTP Pwd', '', 0, 0, 8, 1
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefault] WHERE [defaultid] = 32)
GO
	
SET IDENTITY_INSERT [systemdefault] OFF
GO

-- 0=COMPANY / 2=SUBDIV
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 0, 30
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=0 AND [defaultid]=30)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 2, 30
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=2 AND [defaultid]=30)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 0, 31
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=0 AND [defaultid]=31)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 2, 31
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=2 AND [defaultid]=31)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 0, 32
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=0 AND [defaultid]=32)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 2, 32
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=2 AND [defaultid]=32)

-- 1=Customer
INSERT INTO [systemdefaultcorole] ([coroleid], [defaultid])
	SELECT 1, 30
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultcorole] WHERE [coroleid]=1 AND [defaultid]=30)
INSERT INTO [systemdefaultcorole] ([coroleid], [defaultid])
	SELECT 1, 31
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultcorole] WHERE [coroleid]=1 AND [defaultid]=31)
INSERT INTO [systemdefaultcorole] ([coroleid], [defaultid])
	SELECT 1, 32
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultcorole] WHERE [coroleid]=1 AND [defaultid]=32)	
	
-- Define New Parameters for Group "Client Certificate attributes"
SET IDENTITY_INSERT [systemdefault] ON 
GO

INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 33, 'boolean', 'Defines if the company want to receive certificates by FTP', 'Send By FTP', 'false', 0, 0, 9, 1 
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefault] WHERE [defaultid] = 33)
INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 34, 'boolean', 'Defines if the client want to have next date in certificates', 'Put Next Date', 'false', 0, 0, 9, 1 
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefault] WHERE [defaultid] = 34)
INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 35, 'string', 'Defines the text for uncertainty failure', 'Incertainty Failure', '', 0, 0, 9, 1 
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefault] WHERE [defaultid] = 35)
INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 36, 'string', 'Defines the text for tolerance failure', 'Tolerance Failure', '', 0, 0, 9, 1 
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefault] WHERE [defaultid] = 36)
GO

-- 0=COMPANY / 2=SUBDIV
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 0, 33
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=0 AND [defaultid]=33)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 2, 33
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=2 AND [defaultid]=33)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 0, 34
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=0 AND [defaultid]=34)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 2, 34
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=2 AND [defaultid]=34)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 0, 35
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=0 AND [defaultid]=35)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 2, 35
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=2 AND [defaultid]=35)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 0, 36
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=0 AND [defaultid]=36)
INSERT INTO [systemdefaultscope] ([scope], [defaultid])
	SELECT 2, 36
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultscope] WHERE [scope]=2 AND [defaultid]=36)

-- 1=Customer
INSERT INTO [systemdefaultcorole] ([coroleid], [defaultid])
	SELECT 1, 33
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultcorole] WHERE [coroleid]=1 AND [defaultid]=33)
INSERT INTO [systemdefaultcorole] ([coroleid], [defaultid])
	SELECT 1, 34
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultcorole] WHERE [coroleid]=1 AND [defaultid]=34)
INSERT INTO [systemdefaultcorole] ([coroleid], [defaultid])
	SELECT 1, 35
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultcorole] WHERE [coroleid]=1 AND [defaultid]=35)	
INSERT INTO [systemdefaultcorole] ([coroleid], [defaultid])
	SELECT 1, 36
	WHERE NOT EXISTS (SELECT [defaultid] FROM [systemdefaultcorole] WHERE [coroleid]=1 AND [defaultid]=36)	
	
SET IDENTITY_INSERT [systemdefault] OFF
GO

-- Change Group of system default "Electronic Certs Only"
UPDATE [systemdefault] SET [groupid] = 9 WHERE [defaultID] = 11
GO
	
ROLLBACK TRAN
--COMMIT TRAN
GO