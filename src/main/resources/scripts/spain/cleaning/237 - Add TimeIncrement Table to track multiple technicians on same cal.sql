USE spain

BEGIN TRAN

	CREATE TABLE dbo.timeincrement (
		id int NOT NULL IDENTITY(1,1),
		startstamp datetime,
		timeSpent int,
		callinkid int NOT NULL,
		technicianid int NOT NULL,
		primary key (id),
		CONSTRAINT timeincrement_callink_FK FOREIGN KEY (callinkid) REFERENCES dbo.callink(id),
		CONSTRAINT timeincrement_contact_FK FOREIGN KEY (technicianid) REFERENCES dbo.contact(personid)
	)
	
	GO
	
	INSERT INTO dbversion (version) VALUES (237);
	
COMMIT TRAN