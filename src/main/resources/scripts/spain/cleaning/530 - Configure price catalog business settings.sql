-- Adds new column to businesscompanysettings to control use of price catalog at business level
-- By default on for everyone except Trescal SAS (France)

USE [atlas]

BEGIN TRAN    
	
alter table dbo.businesscompanysettings 
    add usePriceCatalog bit default 1;

GO

DECLARE @coidTrescalSAS INT;

SELECT @coidTrescalSAS = coid FROM company WHERE coname = 'Trescal SAS' and corole = 5;

UPDATE [dbo].[businesscompanysettings]
   SET usePriceCatalog = 1

UPDATE [dbo].[businesscompanysettings]
   SET usePriceCatalog = 0
 WHERE companyid = @coidTrescalSAS

alter table dbo.businesscompanysettings 
    alter column usePriceCatalog bit not null;

GO

INSERT INTO dbversion(version) VALUES(530);

COMMIT TRAN