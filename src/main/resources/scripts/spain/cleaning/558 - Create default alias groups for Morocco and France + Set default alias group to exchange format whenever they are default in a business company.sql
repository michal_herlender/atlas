use atlas;

begin tran

-- get a contact
declare @contactid int, @trescalsasid int, @trescalmarocid int, @ag1 int, @ag2 int, @ag1default bit, @ag2default bit;

select @contactid = c.personid
from contact c where c.lastname like 'Boutahar'

select @trescalsasid = c.coid
from company c where c.coname like 'TRESCAL SAS'

select @trescalmarocid = c.coid
from company c where c.coname like 'TRESCAL MAROC SARL'

-- check if 'Trescal SAS' has already a default alias group
if (select max(cast(ag.defaultBusinessCompany as INT)) from aliasgroup ag where ag.orgid = @trescalsasid) = 1
	 set @ag1default = 0
else 
	set @ag1default = 1

-- create default alias group for 'TRESCAL SAS' & 'TRESCAL MAROC'
INSERT INTO dbo.aliasgroup (lastModified,createdOn,description,name,lastModifiedBy,createdby,defaultBusinessCompany,orgid)
	VALUES ('2020-01-15 12:00:00.000','2020-01-15 12:00:00.000','Default alias group for business company','Default alias group (Trescal SAS)',@contactid,@contactid,@ag1default,@trescalsasid);
select @ag1 = max(ag.id) from aliasgroup ag;

-- aliases for interval unit
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('an','YEAR',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('ann�es','YEAR',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('A','YEAR',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('mois','MONTH',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('M','MONTH',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('semaine','WEEK',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('semaines','WEEK',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('S','WEEK',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('jour','DAY',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('jours','DAY',@ag1);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('J','DAY',@ag1);

-- check if 'Trescal Maroc' has already a default alias group
if (select max(cast(ag.defaultBusinessCompany as INT)) from aliasgroup ag where ag.orgid = @trescalmarocid) = 1
	 set @ag2default = 0
else 
	set @ag2default = 1

INSERT INTO dbo.aliasgroup (lastModified,createdOn,description,name,lastModifiedBy,createdby,defaultBusinessCompany,orgid)
	VALUES ('2020-01-15 12:00:00.000','2020-01-15 12:00:00.000','Default alias group for business company','Default alias group (Trescal Maroc)',@contactid,@contactid,@ag2default,@trescalmarocid);
select @ag2 = max(ag.id) from aliasgroup ag;

INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('an','YEAR',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('ann�es','YEAR',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('A','YEAR',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('mois','MONTH',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('M','MONTH',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('semaine','WEEK',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('semaines','WEEK',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('S','WEEK',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('jour','DAY',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('jours','DAY',@ag2);
INSERT INTO dbo.aliasintervalunit (alias,value,aliasgroupid)
	VALUES ('J','DAY',@ag2);

-- set default alias group on exchange format from the default one set for the related business company
UPDATE dbo.exchangeformat
	SET aliasgroupid=(select ag.id 
						from aliasgroup ag 
						where ag.defaultBusinessCompany=1 and 
						ag.orgid = (select c.coid from dbo.exchangeformat ef 
									left join subdiv s on s.subdivid = ef.businessSubdivid
									join company c on c.coid = ef.businessCompanyid or c.coid = s.coid
									where ef.id = dbo.exchangeformat.id
									) 
					)
INSERT INTO dbversion(version) VALUES(558);

commit tran


