use [spain];

begin tran

alter table dbo.calibrationprocess add toollink varchar(255);

go

insert into dbo.calibrationprocess(actionafterissue, applicationcomponent, description, fileextension, issueimmediately, process, quicksign, signingsupported, signonissue, toollink, uploadafterissue, uploadbeforeissue)
values (0, 0, 'PROKAL is used for calibration', '.pdf', 0, 'PROKAL', 0, 0, 0, 'https://localhost:58159/service/start?tool=prokal&calibrationId={0}', 0, 1)

go

insert into dbversion(version) values(292);

commit tran