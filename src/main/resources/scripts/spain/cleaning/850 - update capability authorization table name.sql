BEGIN TRAN

exec sp_rename 'capability_accreditation', 'capability_authorization';
exec sp_rename 'capability_authorization.accreditationfor', 'authorizationFor';

INSERT INTO dbversion(version) VALUES(850);

COMMIT TRAN
