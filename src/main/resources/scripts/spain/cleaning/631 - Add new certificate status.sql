USE [atlas]
GO

BEGIN TRAN

INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
	VALUES ('certificate',0,'Canceled',0,0,0,0,0,0);

DECLARE @statusid int;

select @statusid = max(statusid) from basestatus;
	

INSERT INTO dbo.basestatusnametranslation (statusid, locale,[translation])
	VALUES (@statusid,'en_GB','Canceled');
INSERT INTO dbo.basestatusnametranslation (statusid, locale,[translation])
	VALUES (@statusid,'fr_FR','Annul�');
INSERT INTO dbo.basestatusnametranslation (statusid, locale,[translation])
	VALUES (@statusid,'es_ES','Cancelado');
INSERT INTO dbo.basestatusnametranslation (statusid, locale,[translation])
	VALUES (@statusid,'de_DE','Abgebrochen');

	

	INSERT INTO dbversion(version) VALUES (631);

COMMIT TRAN