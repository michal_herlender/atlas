-- Created a scheduledtask for the CDC processing job; only needs to be active on US servers or for development (for now)
-- so is created as inactive

USE [atlas]
GO

BEGIN TRAN

	INSERT INTO dbo.scheduledtask (active, classname, description, lastrunsuccess, methodname, quartzjobname, taskname)
	VALUES 
		  (0
		   ,'org.trescal.cwms.core.cdc.schedule.CdcDataTransformationScheduledJob'
           ,'Reads and transforms SQL Server Change Data Capture (CDC) events to be consumed by multiple external systems'
           ,0
		   ,'sync'
           ,'jobDetail_CdcDataTransformationScheduledJob'
           ,'CDC Data Transformation');

	GO

	INSERT INTO dbversion(version) VALUES(783);

COMMIT TRAN