-- Script 268 - Alter procedures to be able to have null accredited lab
-- Remove "fake" Not-Accredited AccreditedLab entries
-- Require accreditation body to be set on accreditedlab entry

USE [spain]

BEGIN TRAN

ALTER TABLE [dbo].[procs]
	ALTER COLUMN [accreditedlabid] [int] NULL

GO

PRINT 'Updating procedures without accreditation body'

UPDATE [dbo].[procs]
   SET [accreditedlabid] = NULL
 WHERE procs.id in 
		(SELECT procs.id FROM procs 
		 LEFT JOIN accreditedlab on procs.accreditedlabid = accreditedlab.id 
		 WHERE accreditedlab.accreditationbodyid is NULL)
GO

PRINT 'Deleting accredited labs that are not accredited'

DELETE FROM [dbo].[accreditedlab]
      WHERE accreditationbodyid is NULL;

GO

ALTER TABLE [dbo].[accreditedlab]
	ALTER COLUMN [accreditationbodyid] [int] NOT NULL

GO

INSERT INTO dbversion VALUES (268)

GO

COMMIT TRAN