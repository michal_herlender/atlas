USE [atlas]
GO

BEGIN TRAN

	DECLARE @gso_doc_upload_activity INT,
		    @gso_doc_not_required INT;

	SELECT @gso_doc_upload_activity = stateid FROM dbo.itemstate WHERE [description] = 'Document uploaded for general operation';
	SELECT @gso_doc_not_required = stateid FROM dbo.itemstate WHERE [description] = 'Document not required for general operation';
	
	INSERT INTO dbo.stategrouplink(groupid,stateid,type)
	VALUES (71, @gso_doc_upload_activity, 'ALLOCATED_SUBDIV'),
		   (71, @gso_doc_not_required, 'ALLOCATED_SUBDIV');

	INSERT INTO dbversion(version) VALUES (649);

COMMIT TRAN