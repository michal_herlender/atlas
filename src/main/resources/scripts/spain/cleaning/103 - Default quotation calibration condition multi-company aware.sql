USE spain

ALTER TABLE defaultquotationcalibrationcondition DROP COLUMN islatest

ALTER TABLE defaultquotationcalibrationcondition DROP CONSTRAINT UQ__defaultq__658BEDC23F9B6DFF

ALTER TABLE defaultquotationcalibrationcondition DROP CONSTRAINT UQ__defaultq__21EB944C4277DAAA

DROP TRIGGER log_update_defaultquotationcalibrationcondition

UPDATE defaultquotationcalibrationcondition SET orgid = 6579 WHERE orgid IS NULL

INSERT INTO defaultquotationcalibrationcondition (conditiontext, caltypeid, lastModified, orgid)
SELECT '', caltypeid, '2016-12-01', coid
FROM calibrationtype, company
WHERE company.coid != 6579 AND company.corole = 5