USE [spain]
GO
--Add lines that have 19 as value in the groupid attribute, for each item that contains in its description 'onHold'
--158: On hold - awaiting repair of another item
--159: On hold - awaiting parts from supplier
--160: On hold - awaiting repair in another department
--161: On hold - awaiting expert recommendation
--162: On hold - awaiting repair in another department
--179: On hold - other
--204: On hold - Awaiting approval of another item
--4046: On hold - awaiting identification of instrument
--4053: On hold - awaiting purchase order

BEGIN TRAN

INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (19,158,null),
           (19,159,null),
		   (19,160,null),
           (19,161,null),
		   (19,162,null),
           (19,163,null),
		   (19,179,null),
           (19,204,null),
		   (19,4046,null),
		   (19,4053,null)

INSERT INTO dbversion(version) VALUES(464);
GO

COMMIT TRAN