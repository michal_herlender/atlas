-- ids used are verified against the prod db
USE [atlas]
GO

BEGIN TRAN

-- connect activity 'Failure report does not need to be sent' to lookup 'Failure Report Outcome preselected?'
DELETE FROM dbo.outcomestatus WHERE id=295;

UPDATE atlas.dbo.itemstate SET lookupid=93 WHERE stateid=4102;


-- connect 'NO' LR of the lookup 'Client Response required on Failure Report?' to lookup 'Failure Report Outcome preselected?'
UPDATE dbo.outcomestatus SET statusid=NULL,lookupid=93 WHERE id=294;

	
-- delete duplicated OS for this hook
DELETE FROM dbo.lookupresult WHERE id=348;
DELETE FROM dbo.lookupresult WHERE id=349;


-- create hook 'make-available-to-external-managment-system'
INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,0,1,1,'make-fr-available-to-external-managment-system',4101);
	
DECLARE @hk int;

select @hk = max(id) from dbo.hook;
	
INSERT INTO atlas.dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,4101,@hk,4098);


INSERT INTO dbversion(version) VALUES(494);

COMMIT TRAN