-- Inserting German translations for job status, since there were none, and it causes some query problems to have without.
-- Translations based on phrases in use in Atlas to be hopefully consistent; they can easily be modified later with an update.

USE [atlas]
GO

BEGIN TRAN

INSERT INTO [dbo].[jobstatusnametranslation]
           ([statusid]
           ,[locale]
           ,[translation])
     VALUES
           (1,'de_DE','Laufend'),
           (2,'de_DE','Wartet auf Rechnung'),
           (3,'de_DE','Fertiggestellte')
GO

INSERT INTO dbversion(version) VALUES(551);

COMMIT TRAN