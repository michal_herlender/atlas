-- Removing unused fields from hireinstrument
-- In the future (US development) we plan to expand it a bit (allocated company, active / inactive , etc...)

USE [spain]

BEGIN TRAN

ALTER TABLE [dbo].[hireinstrument] DROP CONSTRAINT [FK145B21DB202933D5]
GO

ALTER TABLE hireinstrument DROP column ponumber
ALTER TABLE hireinstrument DROP column purchasedate
ALTER TABLE hireinstrument DROP column purchaseno
ALTER TABLE hireinstrument DROP column supplier
ALTER TABLE hireinstrument DROP column log_lastmodified
ALTER TABLE hireinstrument DROP column log_userid

GO

INSERT INTO dbversion(version) VALUES(394);

COMMIT TRAN