BEGIN TRAN

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid],[defaultdatatype],[defaultdescription],[defaultname],[defaultvalue],[endd],[start],[groupid],[groupwide])
VALUES
  (20
    ,'boolean'
    ,'Defines if this client can create certificate validation report documents on the portal'
    ,'Certificate Validation Report'
    ,'false',0,0,9,1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (20, 'en_GB', 'Defines if this client can create certificate validation report documents on the portal'),
  (20, 'fr_FR', 'Définit si ce client peut créer des documents de rapport validation de certificat sur le portail'),
  (20, 'en_ES', 'Define si este cliente puede crear validación de certificados el documentos de informe en el portal');

-- Enables system default to be configurable at only company level

INSERT INTO [dbo].[systemdefaultscope]
([scope],[defaultid])
VALUES
  (0,20);
GO

-- Enables system defaults (new and old) for specific company roles - Client and Business Company

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid],[defaultid]) VALUES
  (1,20),
  (5,20);

-- Add translations

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid],[locale],[translation])
VALUES
  (20,'en_GB','Certificate Validation Report'),
  (20,'es_ES','Informe de validación de certificado'),
  (20,'fr_FR','Rapport de Validation de certificat');
GO

-- Change to COMMIT when successfully tested

COMMIT TRAN