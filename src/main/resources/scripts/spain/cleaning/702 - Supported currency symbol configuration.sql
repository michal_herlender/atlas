-- Updates some of the Atlas currency symbols to correct symbol display values
-- (not ISO currency codes)
-- Adds a new symbolposition enum which denotes the placement of the currency symbol relative to the value

USE [atlas];

BEGIN TRAN;

    alter table dbo.supportedcurrency 
       add symbolposition varchar(255);

	GO

	-- Potential values are PREFIX_NO_SPACE, PREFIX_SPACE, POSTFIX_SPACE

	UPDATE dbo.supportedcurrency SET symbolposition = 'PREFIX_NO_SPACE'
	WHERE currencycode in ('GBP', 'USD', 'SGD', 'CAD')

	UPDATE dbo.supportedcurrency SET symbolposition = 'PREFIX_SPACE'
	WHERE currencycode in ('DKK', 'CHF', 'BRL')

	UPDATE dbo.supportedcurrency SET symbolposition = 'POSTFIX_SPACE'
	WHERE currencycode in ('EUR', 'SEK', 'TND', 'MAD', 'RON', 'CZK')

	GO

	-- Fix up some currency symbols where ISO currecncy codes were incorrectly used

	UPDATE dbo.supportedcurrency SET currencysymbol = 'kr.' WHERE currencycode = 'DKK';
	UPDATE dbo.supportedcurrency SET currencysymbol = 'kr' WHERE currencycode = 'SEK';
	-- To confirm local preference - dh, dh., dhs or Dhs (note, we use dt for TND)
	UPDATE dbo.supportedcurrency SET currencysymbol = 'dh' WHERE currencycode = 'MAD';

	-- For clarify / differentiation from USD $ we preface others e.g. C$, S$ for Singapore, Canada
	-- per acceptable local conventions

	UPDATE dbo.supportedcurrency SET currencysymbol = 'S$' WHERE currencycode = 'SGD';

    alter table dbo.supportedcurrency 
       alter column symbolposition varchar(255) not null;

	INSERT INTO dbversion(version) VALUES(702);

COMMIT TRAN;