-- Adds constraints to fields and cleans up unused fields
-- Initializes existing boolean values if null, as these were causing errors on some profiles (e.g. rapid add item)
-- Note, locale is now in contact (as of long ago)
-- Galen Beck - 2018-09-06

BEGIN TRAN

USE [spain]

UPDATE [userpreferences] SET includeselfinreplies = 0 where includeselfinreplies is NULL;
GO

UPDATE [userpreferences] SET autoprintlabel = 0 where autoprintlabel is NULL;
GO

ALTER TABLE [userpreferences] DROP COLUMN [locale]
ALTER TABLE [userpreferences] ALTER COLUMN [includeselfinreplies] tinyint not null;
ALTER TABLE [userpreferences] ALTER COLUMN [autoprintlabel] tinyint not null;

GO

INSERT INTO dbversion(version) VALUES(290);

COMMIT TRAN