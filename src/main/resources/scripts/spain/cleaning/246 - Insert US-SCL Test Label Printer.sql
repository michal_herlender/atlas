USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[labelprinter]
           ([description]
           ,[path]
           ,[addrid]
           ,[locid]
           ,[darknesslevel]
           ,[ipv4]
           ,[language]
           ,[resolution]
           ,[media])
     VALUES
           ('ZDesigner GX420t'
           ,''
           ,12210
           ,null
           ,15
           ,''
           ,'ZPL'
           ,203
           ,1);
GO

UPDATE [dbo].[subdiv]
   SET [subdivcode] = 'SCL', [subname] = 'Santa Clara'
 WHERE [subdivid] = 6940;
GO

INSERT INTO dbversion (version) VALUES (246);

GO

COMMIT TRAN