USE [spain]
GO

BEGIN TRAN

	-- delete prebookingjobdetailsid constraint from contractreview table if exists
	IF (OBJECT_ID('dbo.FK_prebookingjobdetails_prebookingjobdetailsid', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE dbo.contractreview DROP CONSTRAINT FK_prebookingjobdetails_prebookingjobdetailsid
	END

	IF (OBJECT_ID('dbo.FK_prebookingjobdetails_prebookingjobdetailsid_contractreview', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE dbo.contractreview DROP CONSTRAINT FK_prebookingjobdetails_prebookingjobdetailsid_contractreview
	END

	

	-- delete prebookingdetailsid constraint from po table if exists
	IF (OBJECT_ID('dbo.FK_prebookingjobdetails_id', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE dbo.po DROP CONSTRAINT FK_prebookingjobdetails_id
	END

	--** delete table prebookingjobdetails if exits
	IF EXISTS (SELECT * 
	    FROM INFORMATION_SCHEMA.TABLES   
	    WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'prebookingjobdetails')
	BEGIN
	
		--** drop all constraints 
		DECLARE @sql3 NVARCHAR(MAX) = N'';
		SELECT @sql3 += N'
		ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
		+ '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) +
		' DROP CONSTRAINT ' + QUOTENAME(name) + ';'
		FROM sys.objects
		WHERE type_desc LIKE '%CONSTRAINT'
		AND OBJECT_NAME(PARENT_OBJECT_ID) LIKE 'prebookingjobdetails';
		--print @sql3;
		EXEC sp_executesql @sql3;
		  
		--** drop table
		DROP TABLE [dbo].[prebookingjobdetails];
	
	END

	--** delete table asnitem if exits
	IF EXISTS (SELECT * 
	    FROM INFORMATION_SCHEMA.TABLES   
	    WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'asnitem')
	BEGIN
	
		--** drop all constraints 
		DECLARE @sql NVARCHAR(MAX) = N'';
		SELECT @sql += N'
		ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
		+ '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) +
		' DROP CONSTRAINT ' + QUOTENAME(name) + ';'
		FROM sys.objects
		WHERE type_desc LIKE '%CONSTRAINT'
		AND OBJECT_NAME(PARENT_OBJECT_ID) LIKE 'asnitem';
		--print @sql;
		EXEC sp_executesql @sql;
		  
		--** drop table
		DROP TABLE [dbo].[asnitem];
	
	END

	--** delete table asn if exits
	IF EXISTS (SELECT * 
	    FROM INFORMATION_SCHEMA.TABLES   
	    WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'asn')
	BEGIN
	
		--** drop all constraints 
		DECLARE @sql1 NVARCHAR(MAX) = N'';
		SELECT @sql1 += N'
		ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
		+ '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) +
		' DROP CONSTRAINT ' + QUOTENAME(name) + ';'
		FROM sys.objects
		WHERE type_desc LIKE '%CONSTRAINT'
		AND OBJECT_NAME(PARENT_OBJECT_ID) LIKE 'asn';
		--print @sql1;
		EXEC sp_executesql @sql1;
		  
		--** drop table
		DROP TABLE [dbo].[asn];
	
	END

	--** delete table asnitemanalysisresult if exits
	IF EXISTS (SELECT * 
	    FROM INFORMATION_SCHEMA.TABLES   
	    WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'asnitemanalysisresult')
	BEGIN
	
		--** drop all constraints 
		DECLARE @sql2 NVARCHAR(MAX) = N'';
		SELECT @sql2 += N'
		ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
		+ '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) +
		' DROP CONSTRAINT ' + QUOTENAME(name) + ';'
		FROM sys.objects
		WHERE type_desc LIKE '%CONSTRAINT'
		AND OBJECT_NAME(PARENT_OBJECT_ID) LIKE 'asnitemanalysisresult';
		--print @sql2;
		EXEC sp_executesql @sql2;
		  
		--** drop table
		DROP TABLE [dbo].[asnitemanalysisresult];
	
	END

	

	CREATE TABLE dbo.asn (
		id int NOT NULL IDENTITY(1,1),
		jobtype varchar(50),
		sourceapi varchar(50),
		filename varchar(100),
		exchangeformatid int,
		lastModified datetime2,
		lastModifiedBy int,
		CONSTRAINT PK_asn_id PRIMARY KEY (id),
		CONSTRAINT FK_asn_exchangeformatid FOREIGN KEY (exchangeformatid) REFERENCES dbo.exchangeformat(id),
		CONSTRAINT FK_asn_lastModifiedBy FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid)
	) 
	go

	
	
	CREATE TABLE dbo.prebookingjobdetails (
		id int NOT NULL IDENTITY(1,1),
		asnid int,
		bookedinaddrid int,
		bookedinlocid int,
		clientref varchar(30),
		personid int,
		createdby int,
		currencyid int,
		defaultpoid int,
		estcarout numeric,
		inoptionid int,
		returnoptionid int,
		returnto int,
		returntoloc int,
		CONSTRAINT PK_prebookingjobdetails_id PRIMARY KEY (id),
		CONSTRAINT FK_prebookingjobdetails_asnid FOREIGN KEY (asnid) REFERENCES dbo.asn(id),
		CONSTRAINT FK_prebookingjobdetails_bookedinaddrid FOREIGN KEY (bookedinaddrid) REFERENCES dbo.address(addrid),
		CONSTRAINT FK_prebookingjobdetails_bookedinlocid FOREIGN KEY (bookedinlocid) REFERENCES dbo.location(locationid),
		CONSTRAINT FK_prebookingjobdetails_personid FOREIGN KEY (personid) REFERENCES dbo.contact(personid),
		CONSTRAINT FK_prebookingjobdetails_createdby FOREIGN KEY (createdby) REFERENCES dbo.contact(personid),
		CONSTRAINT FK_prebookingjobdetails_currencyid FOREIGN KEY (currencyid) REFERENCES dbo.supportedcurrency(currencyid),
		CONSTRAINT FK_prebookingjobdetails_defaultpoid FOREIGN KEY (defaultpoid) REFERENCES dbo.po(poid),
		CONSTRAINT FK_prebookingjobdetails_inoptionid FOREIGN KEY (inoptionid) REFERENCES dbo.transportoption(id),
		CONSTRAINT FK_prebookingjobdetails_returnoptionid FOREIGN KEY (returnoptionid) REFERENCES dbo.transportoption(id),
		CONSTRAINT FK_prebookingjobdetails_returnto FOREIGN KEY (returnto) REFERENCES dbo.address(addrid),
		CONSTRAINT FK_prebookingjobdetails_returntoloc FOREIGN KEY (returntoloc) REFERENCES dbo.location(locationid)
	) 
	go
	
	
	
	
	CREATE TABLE dbo.asnitemanalysisresult (
		id int NOT NULL IDENTITY(1,1),
		nextaction varchar(50),
		comment varchar(100),
		jobitemid int,
		lastModified datetime2,
		lastModifiedBy int,
		CONSTRAINT PK_asnitemanalysisresult_id PRIMARY KEY (id),
		CONSTRAINT FK_asnitemanalysisresult_jobitemid FOREIGN KEY (jobitemid) REFERENCES dbo.jobitem(jobitemid),
		CONSTRAINT FK_asnitemanalysisresult_lastModifiedBy FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid)
	) 
	go
	
	CREATE TABLE dbo.asnitem (
		id int NOT NULL IDENTITY(1,1),
		itemindex int,
		comment varchar(100),
		status varchar(100),
		asnid int,
		instrumentid int,
		analysisresultid int,
		lastModified datetime2,
		lastModifiedBy int,
		CONSTRAINT PK_asnitem_id PRIMARY KEY (id),
		CONSTRAINT FK_asnitem_asnid FOREIGN KEY (asnid) REFERENCES dbo.asn(id),
		CONSTRAINT FK_asnitem_analysisresultid FOREIGN KEY (analysisresultid) REFERENCES dbo.instrument(plantid),
		CONSTRAINT FK_asnitem_instrumentid FOREIGN KEY (instrumentid) REFERENCES dbo.instrument(plantid),
		CONSTRAINT FK_asnitem_lastModifiedBy FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid)
	) 
	go
	
	
	-- add prebookingdetailsid to po table if it doesn't exist
	IF COL_LENGTH('dbo.po','prebookingdetailsid') IS NULL
	BEGIN
		ALTER TABLE dbo.po ADD prebookingdetailsid int 
	END
	go
	-- add prebookingdetailsid constraint to po table
	ALTER TABLE dbo.po ADD CONSTRAINT FK_prebookingjobdetails_id FOREIGN KEY (prebookingdetailsid) REFERENCES dbo.prebookingjobdetails(id) 
	go
	
	
	-- add prebookingjobdetailsid to contractreview table if it doesn't exist
	IF COL_LENGTH('dbo.contractreview','prebookingjobdetailsid') IS NULL
	BEGIN
		ALTER TABLE dbo.contractreview ADD prebookingjobdetailsid int
	END
	
	go
	-- add prebookingjobdetailsid constraint to contractreview table
    ALTER TABLE dbo.contractreview ADD CONSTRAINT FK_prebookingjobdetails_prebookingjobdetailsid
   		FOREIGN KEY (prebookingjobdetailsid) REFERENCES dbo.prebookingjobdetails(id)
   	go

	INSERT INTO dbversion VALUES (262)

COMMIT TRAN