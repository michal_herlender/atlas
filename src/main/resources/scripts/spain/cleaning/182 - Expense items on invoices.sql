use spain
go

begin tran

alter table dbo.costs_service 
        add nominal int;

alter table dbo.costs_service 
    add constraint FK_costs_service_nominalcode 
    foreign key (nominal) 
    references dbo.nominalcode;
    
commit tran