-- Minor cleanup of some company-related fields which should have constraints
-- as part of US API creation
-- Some fields commented out at the end as a TODO to update in phase 2 - (too many indices / stats for now)

USE [atlas]
GO

BEGIN TRAN

ALTER TABLE [country] ALTER COLUMN country varchar(30) not null;
ALTER TABLE [country] ALTER COLUMN countrycode varchar(6) not null;

ALTER TABLE [description] ALTER COLUMN active bit not null;

ALTER TABLE [companysettingsforallocatedcompany] ALTER COLUMN active bit NOT NULL;
ALTER TABLE [companysettingsforallocatedcompany] ALTER COLUMN onstop bit NOT NULL;
ALTER TABLE [companysettingsforallocatedcompany] ALTER COLUMN status varchar(255) NOT NULL;

ALTER TABLE [address] ALTER COLUMN town varchar(50) NOT NULL;

INSERT INTO dbversion(version) VALUES(748);

COMMIT TRAN

-- Changes to do in a future phase later are below

--ALTER TABLE [instmodel] ALTER COLUMN model varchar(100) not null;
--ALTER TABLE [description] ALTER COLUMN family int not null;

--ALTER TABLE [company] ALTER COLUMN corole int NOT NULL;
--ALTER TABLE [company] ALTER COLUMN countryid int NOT NULL;

--ALTER TABLE [subdiv] ALTER COLUMN coid int NOT NULL;
--ALTER TABLE [subdiv] ALTER COLUMN subname varchar(75) not null;
--ALTER TABLE [subdiv] ALTER COLUMN active tinyint not null;

--ALTER TABLE [address] ALTER COLUMN subdiv int NOT NULL;

--ALTER TABLE [contact] ALTER COLUMN active tinyint not null;
--ALTER TABLE [contact] ALTER COLUMN email varchar(200) not null;
--ALTER TABLE [contact] ALTER COLUMN firstname varchar(30) not null;
--ALTER TABLE [contact] ALTER COLUMN lastname varchar(30) not null;
--ALTER TABLE [contact] ALTER COLUMN subdiv int not null;
