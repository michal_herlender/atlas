-- Author Galen Beck 2016-07-25
-- Simple script to update job status from 'Awaiting Invoice' (2) to 'Complete' (3) for all existing jobs
-- 'Awaiting Invoice' status is no longer used as we do job item based invoicing now

USE [spain]
GO

BEGIN TRAN;

UPDATE [dbo].[job]
   SET [statusid] = 3
 WHERE [statusid] = 2;
GO

COMMIT TRAN;