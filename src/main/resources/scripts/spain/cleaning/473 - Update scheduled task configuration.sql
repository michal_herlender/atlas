-- Updates class name of renamed scheduled task for Adveso services
-- Deletes ClearTempDirectoryTool (deleted from codebase, static temp dirs no longer used)

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.external.adveso.schedule.AdvesoSyncActivitiesScheduledJob'
 WHERE [classname] = 'org.trescal.cwms.rest.adveso.schedule.AdvesoSyncActivitiesScheduledJob'
GO

DELETE FROM [dbo].[scheduledtask]
      WHERE [classname] = 'org.trescal.cwms.web.utilities.ClearTempDirectoryTool'
GO

INSERT INTO dbversion(version) VALUES(473);

COMMIT TRAN

