USE [spain]
GO

BEGIN TRAN

		--Adjustment----------
	DECLARE @adjId int
	DECLARE @adjCursor cursor

	--get list of ids 
	set @adjCursor = cursor for
	select ao.id
	from dbo.actionoutcome ao
	WHERE ( [value] like 'INHOUSE_ADJUSTMENT' or [value] like 'ERP_ADJUSTMENT' )
	and [type] like 'fault_report'

	OPEN @adjCursor
	FETCH NEXT
	FROM @adjCursor INTO @adjId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		UPDATE dbo.actionoutcome SET [value]='ERP_ADJUSTMENT',[description]='ERP Adjustment'
		WHERE id = @adjId;
		UPDATE dbo.actionoutcometranslation
		SET [translation]='ERP Adjustment'
		WHERE id= @adjId AND locale='en_GB';
		UPDATE dbo.actionoutcometranslation
		SET [translation]='Ajuste ERP'
		WHERE id= @adjId AND locale='es_ES';
		UPDATE dbo.actionoutcometranslation
		SET [translation]='Ajustement ERP'
		WHERE id= @adjId AND locale='fr_FR';

		FETCH NEXT
		FROM @adjCursor INTO @adjId
	END

	CLOSE @adjCursor
	DEALLOCATE @adjCursor

	--Repair---------
	DECLARE @RepairId int
	DECLARE @RepairCursor cursor

	--get list of ids 
	set @RepairCursor = cursor for
	select ao.id
	from dbo.actionoutcome ao
	WHERE ( [value] like 'INHOUSE_REPAIR' or [value] like 'ERP_REPAIR' )
	and [type] like 'fault_report'

	OPEN @RepairCursor
	FETCH NEXT
	FROM @RepairCursor INTO @RepairId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		UPDATE dbo.actionoutcome SET [value]='ERP_REPAIR',[description]='ERP Repair'
		WHERE id = @RepairId;
		UPDATE dbo.actionoutcometranslation
		SET [translation]='ERP Repair'
		WHERE id= @RepairId AND locale='en_GB';
		UPDATE dbo.actionoutcometranslation
		SET [translation]='Réparation ERP'
		WHERE id= @RepairId AND locale='es_ES';
		UPDATE dbo.actionoutcometranslation
		SET [translation]='Reparación ERP'
		WHERE id= @RepairId AND locale='fr_FR';

		FETCH NEXT
		FROM @RepairCursor INTO @RepairId
	END

	CLOSE @RepairCursor
	DEALLOCATE @RepairCursor

	--Disable actionoutcomes linked to retired itemActivities-----
	UPDATE ao SET active='0'
	FROM dbo.actionoutcome ao
	JOIN dbo.itemstate its on its.stateid = ao.activityid
	WHERE its.retired = '1'

	INSERT INTO dbversion(version) VALUES(321);

COMMIT TRAN