-- Create a Global Exchange format usable to import quotation items 
-- using instrument and  instrument model 24/04/2020
USE [atlas]
GO

BEGIN TRAN

DECLARE @lastexchangeFormatid int

INSERT INTO dbo.exchangeformat(name, description, exchangeFormatLevel, dateFormat, lastModified, lastModifiedBy, eftype)
VALUES ('ImportQuotationItemsUsingInstrumentOrInstrumentModel' ,'Exchange format usable to import quotation items using both instrument / instrument model','GLOBAL','DD_MM_YYYY', '2020-04-22 18:00:00', 17280, 'QUOTATION_ITEMS_IMPORTATION' );
SELECT @lastexchangeFormatid = MAX(id) FROM dbo.exchangeformat

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES  (1, 0, 'ID Trescal', 'ID_TRESCAL', @lastexchangeFormatid),
		(2, 0, 'Plant No', 'PLANT_NO', @lastexchangeFormatid),
		(3, 0, 'Instrument Model ID', 'INSTRUMENT_MODEL_ID', @lastexchangeFormatid),
		(4, 0, 'TML Instrument Model ID', 'TML_INSTRUMENT_MODEL_ID', @lastexchangeFormatid),
		(5, 1, 'Expected service', 'EXPECTED_SERVICE', @lastexchangeFormatid),
		(6, 0, 'Catalog price', 'CATALOG_PRICE', @lastexchangeFormatid),
		(7, 0, 'Discount Rate', 'DISCOUNT_RATE', @lastexchangeFormatid),
		(8, 1, 'Final price', 'FINAL_PRICE', @lastexchangeFormatid),
		(9, 0, 'Private note', 'PUBLIC_NOTE', @lastexchangeFormatid),
		(10, 0, 'Public note', 'PRIVATE_NOTE', @lastexchangeFormatid);

INSERT INTO dbversion(version) VALUES(618);

COMMIT TRAN