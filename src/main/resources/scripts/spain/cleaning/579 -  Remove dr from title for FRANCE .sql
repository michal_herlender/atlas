USE [atlas];
GO

BEGIN TRAN

DECLARE @france INT;

SELECT @france= c.countryid FROM dbo.country c WHERE c.countrycode like 'fr';

DELETE FROM dbo.title WHERE countryid=@france and title='Dr';

INSERT INTO dbversion(version) VALUES(579);

COMMIT TRAN