USE [atlas]
GO

BEGIN TRAN

UPDATE dbo.itemstate SET active = 0  WHERE [description] = 'Called Off';

INSERT INTO dbversion(version) VALUES (613);

COMMIT TRAN