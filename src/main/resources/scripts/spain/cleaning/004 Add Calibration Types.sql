-- Author Galen Beck 2015-01-15
-- Adds Calibration Types for Madrid, Bilbao, and Vitoria subdivs into system
-- Presumes that Cal Types for Zaragoza (6644) are already present
-- NOTE: If an error regarding unique key constraint is observed try deleting that key 
-- There seem to be duplicate unique keys on the ID in some copies of the database
-- For me the bad constraint was UQ__calibrat__08096C360E6E26BF
-- Note - there was an error in accreditationspecific, updated 2016-05-19 GB (not that we expect to run this file again) and fixed via upgrades.sql

BEGIN TRAN

USE [spain];
GO

INSERT INTO [dbo].[calibrationtype]
           ([accreditationspecific]
           ,[active]
           ,[certnoresetyearly]
           ,[certprefix]
           ,[firstpagepapertype]
           ,[labeltemplatepath]
           ,[nextcertno]
           ,[orderby]
           ,[pdfcontinuationwatermark]
           ,[pdfheaderwatermark]
           ,[recalldateonlabel]
           ,[restpagepapertype]
           ,[lastModified]
           ,[servicetypeid]
           ,[orgid])
     VALUES
         (1,1,0,'S','CERT_HEADED','/labels/zebra/zebra_label_saccal.vm',10000,1,'SAC_continuation.tif','SACV_header.tif',0,'CERT_CONTINUATION','2016-01-13',1,6645),
         (0,1,1,'','A4_PLAIN','/labels/zebra/zebra_label_stdcal.vm',1,2,NULL,NULL,0,'A4_PLAIN','2016-01-13',2,6645),
         (0,1,0,'C','A4_PLAIN','/labels/zebra/zebra_label_cofccal.vm',1,3,NULL,NULL,0,'A4_PLAIN','2016-01-13',3,6645),
         (1,1,0,'S','CERT_HEADED','/labels/zebra/zebra_label_saccal.vm',10000,1,'SAC_continuation.tif','SACV_header.tif',0,'CERT_CONTINUATION','2016-01-13',1,6643),
         (0,1,1,'','A4_PLAIN','/labels/zebra/zebra_label_stdcal.vm',1,2,NULL,NULL,0,'A4_PLAIN','2016-01-13',2,6643),
         (0,1,0,'C','A4_PLAIN','/labels/zebra/zebra_label_cofccal.vm',1,3,NULL,NULL,0,'A4_PLAIN','2016-01-13',3,6643),
         (1,1,0,'S','CERT_HEADED','/labels/zebra/zebra_label_saccal.vm',10000,1,'SAC_continuation.tif','SACV_header.tif',0,'CERT_CONTINUATION','2016-01-13',1,6642),
         (0,1,1,'','A4_PLAIN','/labels/zebra/zebra_label_stdcal.vm',1,2,NULL,NULL,0,'A4_PLAIN','2016-01-13',2,6642),
         (0,1,0,'C','A4_PLAIN','/labels/zebra/zebra_label_cofccal.vm',1,3,NULL,NULL,0,'A4_PLAIN','2016-01-13',3,6642);

GO

-- Extra part to Add empty quote condition text for every calibration type added by Scott Chamberlain 2016-02-08

DELETE FROM [dbo].[quotecaldefaults];
GO

DELETE FROM [dbo].[defaultquotationcalibrationcondition];
GO

INSERT INTO [dbo].[defaultquotationcalibrationcondition] (conditiontext, islatest, lastModified, caltypeid) SELECT '',1,'2016-01-01',caltypeid FROM [dbo].[calibrationtype];
GO

ROLLBACK TRAN