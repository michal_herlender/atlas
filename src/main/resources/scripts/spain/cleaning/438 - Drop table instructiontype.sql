USE [spain];

BEGIN TRAN

ALTER TABLE dbo.userinstructiontype DROP CONSTRAINT FKEE0B299DA2BF3EFD
ALTER TABLE dbo.instruction DROP CONSTRAINT FK11F8EC8EA2BF3EFD
DROP TABLE dbo.instructiontype

INSERT INTO dbversion (version) VALUES (438);

COMMIT TRAN