-- Fixes some translations for short & long name translations of service types
-- Author Galen Beck 2016-10-10

USE [spain]
GO

-- Had been AC-SS
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-SS'
 WHERE [servicetypeid] = 5 AND [locale] = 'fr_FR';

-- Service types 7 and 8 had bad locales for Spanish
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [locale] = 'es_ES'
 WHERE [locale] = 'en_ES';

-- Was STD-EX but should be ST-EX for consistency
UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ST-EX'
 WHERE [servicetypeid] = 8 AND [locale] = 'en_GB';

-- Was Reparer (should be Réparation)
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Réparation'
 WHERE [servicetypeid] = 6 AND [locale] = 'fr_FR';

GO