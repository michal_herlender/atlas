BEGIN TRAN

--- Create sequence ---

DECLARE @maxId INT;
SELECT @maxId = max(id) + 1 FROM dbo.quotationitem;

DECLARE @sqlCommand VARCHAR(100);
SET @sqlCommand = 'CREATE SEQUENCE dbo.quotation_item_id_sequence START WITH ' + CAST(@maxId AS VARCHAR(10)) + ' INCREMENT BY 1'
EXEC(@sqlCommand)

GO

DECLARE @tableId INT
DECLARE @dropForeignKeys VARCHAR(MAX)
DECLARE @dropConstraints VARCHAR(MAX)

SELECT @tableId = object_id
FROM sys.tables
WHERE name = 'quotationitem'

SET @dropForeignKeys = ''
SET @dropConstraints = ''

--- Drop all foreign keys refering to quotationitem ---

SELECT @dropForeignKeys = @dropForeignKeys + 'ALTER TABLE ' + QUOTENAME(t.name) + ' DROP CONSTRAINT ' + QUOTENAME(k.name) + CHAR(13) + CHAR(10)
FROM sys.foreign_keys k
JOIN sys.objects t ON k.parent_object_id = t.object_id
WHERE referenced_object_id = @tableId

EXEC(@dropForeignKeys)

--- Drop all constraints on quotationitem, but not the primary key ---

SELECT @dropConstraints = @dropConstraints + 'ALTER TABLE quotationitem DROP CONSTRAINT ' + QUOTENAME(name) + CHAR(13) + CHAR(10)
FROM sys.objects
WHERE parent_object_id = @tableId AND type <> 'PK'

EXEC(@dropConstraints)

--- Drop obsolete columns on quotationitem ---

ALTER TABLE quotationitem DROP COLUMN log_lastmodified, log_userid

--- Create new table and copy all items ---

CREATE TABLE dbo.new_quotationitem (
	id INT NOT NULL,
	finalcost NUMERIC(10,2) NOT NULL,
	discountrate NUMERIC(5,2) NOT NULL,
	discountvalue NUMERIC(10,2) NOT NULL,
	itemno INT NOT NULL,
	partofbaseunit BIT NOT NULL,
	quantity INT NOT NULL,
	totalcost NUMERIC(10,2) NOT NULL,
	tpcarin NUMERIC(10,2) NOT NULL,
	tpcarinmarkupvalue NUMERIC(10,2) NOT NULL,
	tpcarmarkuprate NUMERIC(10,2) NOT NULL,
	tpcarmarkupsrc VARCHAR(255) NOT NULL,
	tpcarout NUMERIC(10,2) NOT NULL,
	tpcaroutmarkupvalue NUMERIC(10,2) NOT NULL,
	tpcartotal NUMERIC(10,2) NOT NULL,
	inspection NUMERIC(10,2) NOT NULL,
	plantno VARCHAR(50),
	baseid INT,
	calcost_id INT,
	caltype_caltypeid INT,
	heading_headingid INT,
	modelid INT,
	purchasecost_id INT,
	quoteid INT NOT NULL,
	plantid INT,
	lastModified DATETIME2(7),
	lastModifiedBy INT,
	servicetypeid INT,
	CONSTRAINT PK_quotationitem PRIMARY KEY CLUSTERED (id)
)

ALTER TABLE dbo.quotationitem SWITCH TO dbo.new_quotationitem

--- Drop old table and rename new table ---

DROP TABLE dbo.quotationitem

EXEC sp_rename 'dbo.new_quotationitem', 'quotationitem'

GO

--- Create foreign keys for new table ---

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_lastmodifiedby
	FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact;

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_baseid
	FOREIGN KEY (baseid) REFERENCES dbo.quotationitem

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_calcost
	FOREIGN KEY (calcost_id) REFERENCES dbo.costs_calibration
	
ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_caltype
	FOREIGN KEY (caltype_caltypeid) REFERENCES dbo.calibrationtype

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_heading
	FOREIGN KEY (heading_headingid) REFERENCES dbo.quoteheading

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_plantid
	FOREIGN KEY (plantid) REFERENCES dbo.instrument

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_modelid
	FOREIGN KEY (modelid) REFERENCES dbo.instmodel

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_quoteid
	FOREIGN KEY (quoteid) REFERENCES dbo.quotation

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_servicetype
	FOREIGN KEY (servicetypeid) REFERENCES dbo.servicetype

ALTER TABLE dbo.quotationitem ADD CONSTRAINT FK_quotationitem_purchasecost
	FOREIGN KEY (purchasecost_id) REFERENCES dbo.costs_purchase

ALTER TABLE dbo.quoteitemnote ADD CONSTRAINT FK_quoteitemnote_quotationitem
	FOREIGN KEY (id) REFERENCES dbo.quotationitem

ALTER TABLE dbo.tpquoterequestitem ADD CONSTRAINT FK_tpquoterequestitem_quoteitemid
	FOREIGN KEY (quoteitemid) REFERENCES dbo.quotationitem

--- Drop obsolete table ---

DROP TABLE dbo.quotationitempartof

--- Update Scheduled Task ---

UPDATE scheduledtask SET classname = 'org.trescal.cwms.core.quotation.service.ScheduledQuotationGenerator'
WHERE classname = 'org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db.ScheduledQuotationRequestServiceImpl'

INSERT INTO dbversion VALUES (859)

COMMIT TRAN