-- Author Tony Provost 2016-01-21
-- Adds CalibrationProcess Metcal

BEGIN TRAN

USE [spain];
GO

SET IDENTITY_INSERT [calibrationprocess] ON
GO

INSERT INTO [calibrationprocess] (
	[id]
	,[actionafterissue]
	,[applicationcomponent]
	,[description]
	,[fileextension]
	,[issueimmediately]
	,[process]
	,[quicksign]
	,[signonissue]
	,[signingsupported]
	,[uploadafterissue]
	,[uploadbeforeissue]
	)
VALUES (3, 1, 0, 'Metcal templates are used to generate the Certificate document.', NULL, 1, 'Metcal', 0, 0, 1, 0, 0)
GO

SET IDENTITY_INSERT [calibrationprocess] OFF
GO

COMMIT TRAN