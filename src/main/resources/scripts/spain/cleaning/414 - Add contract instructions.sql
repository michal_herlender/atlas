USE [spain]
BEGIN TRAN

  create table dbo.contract_instructions (
                                           Contract_id int not null,
                                           instructions int not null,
                                           primary key (Contract_id, instructions)
  );

  alter table dbo.contract_instructions
    add constraint UK_bg272ldlwfolptj5yd85qunbu unique (instructions);


  alter table dbo.contract_instructions
    add constraint FKpnhbndxyvy4syq01ehfa5o0f
      foreign key (instructions)
        references dbo.baseinstruction;

  alter table dbo.contract_instructions
    add constraint FKjs3uojddtsnqmaxm3kqyqc789
      foreign key (Contract_id)
        references dbo.contract;

  INSERT INTO dbversion(version) VALUES(414);

COMMIT TRAN
