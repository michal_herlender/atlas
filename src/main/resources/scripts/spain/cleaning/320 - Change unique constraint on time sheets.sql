use spain

begin tran

    alter table dbo.timesheetvalidation 
       drop constraint UK_timesheetvalidation_employee_week_year;

    alter table dbo.timesheetvalidation 
       add constraint UK_timesheetvalidation_employee_week_year_status unique (employeeid, week, year, status);

insert into dbversion(version) values(320);

commit tran