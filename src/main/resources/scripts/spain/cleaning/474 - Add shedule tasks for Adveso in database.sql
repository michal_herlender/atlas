USE [spain]
GO

BEGIN TRAN

INSERT INTO dbo.scheduledtask (active,classname,description,methodname,quartzjobname,taskname)
VALUES (0,'org.trescal.cwms.core.external.adveso.schedule.AdvesoJobItemActivityDeletionScheduledJob','Deletes old Adveso Job item activities (for now configured for one year)','deleteAlreadySentAdvesoJobItemActivities','advesoJobItemActivityDeletion','Adveso JobItem Activity Deletion Scheduled Job')
GO

INSERT INTO dbo.scheduledtask (active,classname,description,methodname,quartzjobname,taskname)
VALUES (0,'org.trescal.cwms.core.external.adveso.schedule.AdvesoNotificationSystemDeletionScheduledJob','Delete old Adveso Notifications (currently configured for one year)','deleteAlreadySentAdvesoNotificationsSystem','advesoNotificationSystemDeletion','Adveso Notification System Deletion') 
GO

UPDATE dbo.scheduledtask SET methodname='sync' WHERE id=17 
GO

INSERT INTO dbversion(version) VALUES(474);

COMMIT TRAN

