-- Renames existing service types (DEV-2748)
-- Adds a "dbconfig" table (similar to dbversion, but with one row).
-- This is needed so that we could differentiate between US and EMEA databases when running configuration scripts
-- The default here is EMEA; verify value after running on US databases as 'US' to confirm initialization
-- (value is used in next script)
-- We rename the English service types everywhere but in the next script 2 new service types are added for the US platform only (for now)

USE [atlas]

BEGIN TRAN

    create table dbo.dbconfig (
       id int identity not null,
        region varchar(10) not null,
        duplicate bit not null default 0,
        primary key (id)
    );

	GO

	alter table dbconfig add constraint chk_dbconfig_duplicate check (duplicate = 0);
	alter table dbconfig add constraint uc_dbconfig_duplicate unique (duplicate);
	GO

-- Duplicate column enforces there to be just one row in the table (where duplicate=0)
-- Detect US database by presence of US date format in global system default for calibration dates
	
DECLARE @isPlatformUS int;
SELECT @isPlatformUS = COUNT(1) FROM systemdefault WHERE [defaultid] = 48 and [defaultvalue] = 'MM/dd/yyyy';

IF (@isPlatformUS = 0)
BEGIN	
	insert into dbconfig (region) values ('EMEA');
	print 'EMEA database configured';
END
ELSE 
BEGIN	
	insert into dbconfig (region) values ('US');
	print 'US database configured';
END

-- Remove unused table

DROP TABLE servicetypedescriptiontranslation;

DECLARE @st_TC_SOC_IL int;
DECLARE @st_AC_SOC_IL int; 
DECLARE @st_TC_SOC_OS int;
DECLARE @st_AC_SOC_OS int;
DECLARE @st_TC_MVO_IL int;
DECLARE @st_AC_MVO_IL int;
DECLARE @st_TC_MVO_OS int;
DECLARE @st_AC_MVO_OS int;

SELECT @st_TC_SOC_IL = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'TC-SOC-IL';
SELECT @st_AC_SOC_IL = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'AC-SOC-IL';
SELECT @st_TC_SOC_OS = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'TC-SOC-OS';
SELECT @st_AC_SOC_OS = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'AC-SOC-OS';
SELECT @st_TC_MVO_IL = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'TC-MVO-IL';
SELECT @st_AC_MVO_IL = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'AC-MVO-IL';
SELECT @st_TC_MVO_OS = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'TC-MVO-OS';
SELECT @st_AC_MVO_OS = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'AC-MVO-OS';

PRINT 'Renaming 8 service type for IDs: (8 IDs should appear below)'
PRINT @st_TC_SOC_IL;
PRINT @st_AC_SOC_IL;
PRINT @st_TC_SOC_OS;
PRINT @st_AC_SOC_OS;
PRINT @st_TC_MVO_IL;
PRINT @st_AC_MVO_IL;
PRINT @st_TC_MVO_OS;
PRINT @st_AC_MVO_OS;

DECLARE @sn_TC_MC_IL varchar(10) = 'TC-MC-IL';
DECLARE @sn_AC_MC_IL varchar(10) = 'AC-MC-IL'; 
DECLARE @sn_TC_MC_OS varchar(10) = 'TC-MC-OS';
DECLARE @sn_AC_MC_OS varchar(10) = 'AC-MC-OS'; 
DECLARE @sn_TC_M_IL varchar(10) = 'TC-M-IL';
DECLARE @sn_AC_M_IL varchar(10) = 'AC-M-IL'; 
DECLARE @sn_TC_M_OS varchar(10) = 'TC-M-OS';
DECLARE @sn_AC_M_OS varchar(10) = 'AC-M-OS'; 

DECLARE @ln_TC_MC_IL varchar(100) = 'Traceable calibration with measurement results and statement of conformity in laboratory';
DECLARE @ln_AC_MC_IL varchar(100) = 'Accredited calibration with measurement results and statement of conformity in laboratory'; 
DECLARE @ln_TC_MC_OS varchar(100) = 'Traceable calibration with measurement results and statement of conformity on site';
DECLARE @ln_AC_MC_OS varchar(100) = 'Accredited calibration with measurement results and statement of conformity on site'; 
DECLARE @ln_TC_M_IL varchar(100) = 'Traceable calibration with measurement results in laboratory';
DECLARE @ln_AC_M_IL varchar(100) = 'Accredited calibration with measurement results in laboratory'; 
DECLARE @ln_TC_M_OS varchar(100) = 'Traceable calibration with measurement results on site';
DECLARE @ln_AC_M_OS varchar(100) = 'Accredited calibration with measurement results on site'; 

UPDATE [servicetype] SET shortname=@sn_TC_MC_IL, longname=@ln_TC_MC_IL WHERE servicetypeid = @st_TC_SOC_IL;
UPDATE [servicetype] SET shortname=@sn_AC_MC_IL, longname=@ln_AC_MC_IL WHERE servicetypeid = @st_AC_SOC_IL;
UPDATE [servicetype] SET shortname=@sn_TC_MC_OS, longname=@ln_TC_MC_OS WHERE servicetypeid = @st_TC_SOC_OS;
UPDATE [servicetype] SET shortname=@sn_AC_MC_OS, longname=@ln_AC_MC_OS WHERE servicetypeid = @st_AC_SOC_OS;
UPDATE [servicetype] SET shortname=@sn_TC_M_IL, longname=@ln_TC_M_IL WHERE servicetypeid = @st_TC_MVO_IL;
UPDATE [servicetype] SET shortname=@sn_AC_M_IL, longname=@ln_AC_M_IL WHERE servicetypeid = @st_AC_MVO_IL;
UPDATE [servicetype] SET shortname=@sn_TC_M_OS, longname=@ln_TC_M_OS WHERE servicetypeid = @st_TC_MVO_OS;
UPDATE [servicetype] SET shortname=@sn_AC_M_OS, longname=@ln_AC_M_OS WHERE servicetypeid = @st_AC_MVO_OS;

UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = @sn_TC_MC_IL
	WHERE [servicetypeid] = @st_TC_SOC_IL and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = @sn_AC_MC_IL
	WHERE [servicetypeid] = @st_AC_SOC_IL and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = @sn_TC_MC_OS
	WHERE [servicetypeid] = @st_TC_SOC_OS and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = @sn_AC_MC_OS
	WHERE [servicetypeid] = @st_AC_SOC_OS and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = @sn_TC_M_IL
	WHERE [servicetypeid] = @st_TC_MVO_IL and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = @sn_AC_M_IL
	WHERE [servicetypeid] = @st_AC_MVO_IL and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = @sn_TC_M_OS
	WHERE [servicetypeid] = @st_TC_MVO_OS and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = @sn_AC_M_OS
	WHERE [servicetypeid] = @st_AC_MVO_OS and [locale] in ('en_GB', 'en_US')

UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = @ln_TC_MC_IL
	WHERE [servicetypeid] = @st_TC_SOC_IL and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = @ln_AC_MC_IL
	WHERE [servicetypeid] = @st_AC_SOC_IL and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = @ln_TC_MC_OS
	WHERE [servicetypeid] = @st_TC_SOC_OS and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = @ln_AC_MC_OS
	WHERE [servicetypeid] = @st_AC_SOC_OS and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = @ln_TC_M_IL
	WHERE [servicetypeid] = @st_TC_MVO_IL and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = @ln_AC_M_IL
	WHERE [servicetypeid] = @st_AC_MVO_IL and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = @ln_TC_M_OS
	WHERE [servicetypeid] = @st_TC_MVO_OS and [locale] in ('en_GB', 'en_US')
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = @ln_AC_M_OS
	WHERE [servicetypeid] = @st_AC_MVO_OS and [locale] in ('en_GB', 'en_US')

-- Exising global values in both US and EMEA systems:
-- Regular (53) :       TC-SOC-IL -> TC-MC-IL
-- Onsite (54) :        AC-MVO-OS -> AC-M-OS
-- Single Price (55) :  AC-MVO-IL -> AC-M-IL

PRINT 'Update system defaults'

UPDATE [dbo].[systemdefault] SET [defaultvalue] = 'TC-MC-IL' WHERE defaultid = 53;
UPDATE [dbo].[systemdefault] SET [defaultvalue] = 'AC-M-OS' WHERE defaultid = 54;
UPDATE [dbo].[systemdefault] SET [defaultvalue] = 'AC-M-IL' WHERE defaultid = 55;

PRINT 'Update system default application'

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'TC-MC-IL'
 WHERE [defaultid] in (53,54,55) and defaultvalue = 'TC-SOC-IL';

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'AC-MC-IL'
 WHERE [defaultid] in (53,54,55) and defaultvalue = 'AC-SOC-IL';

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'TC-MC-OS'
 WHERE [defaultid] in (53,54,55) and defaultvalue = 'TC-SOC-OS';

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'AC-MC-OS'
 WHERE [defaultid] in (53,54,55) and defaultvalue = 'AC-SOC-OS';

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'TC-M-IL'
 WHERE [defaultid] in (53,54,55) and defaultvalue = 'TC-MVO-IL';

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'AC-M-IL'
 WHERE [defaultid] in (53,54,55) and defaultvalue = 'AC-MVO-IL';

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'TC-M-OS'
 WHERE [defaultid] in (53,54,55) and defaultvalue = 'TC-MVO-OS';

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'AC-M-OS'
 WHERE [defaultid] in (53,54,55) and defaultvalue = 'AC-MVO-OS';

insert into dbversion values (832);

COMMIT TRAN