USE [spain];

DECLARE
@alterStatement NVARCHAR(200),
@foreignKey NVARCHAR(100),
@parentTable NVARCHAR(100);

WHILE (SELECT COUNT(*) FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID('ids')) > 0
BEGIN
	SELECT @foreignKey = foreignkey.name, @parentTable = parenttable.name FROM sys.foreign_keys 
	INNER JOIN sys.objects AS foreignkey ON sys.foreign_keys.object_id = foreignkey.object_id
	INNER JOIN sys.objects AS parenttable ON sys.foreign_keys.parent_object_id = parenttable.object_id
	WHERE referenced_object_id = OBJECT_ID('ids');

	SET @alterStatement = 'ALTER TABLE ' + @parentTable + ' DROP CONSTRAINT ' + @foreignKey;
	EXECUTE sp_executesql @alterStatement; 
END;

IF OBJECT_ID('ids') IS NOT NULL DROP TABLE [dbo].[ids];

DELETE FROM [dbo].[numberformat];