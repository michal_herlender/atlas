USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.repaircompletionreport ADD rcrnumber varchar(30);

INSERT INTO dbversion(version) VALUES(483);

COMMIT TRAN