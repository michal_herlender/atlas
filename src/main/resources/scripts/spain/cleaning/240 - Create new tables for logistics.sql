USE [spain]
GO

BEGIN TRAN

	--** delete table exchangeformatfieldnamedetails2 (created for testing) if exits
	IF EXISTS (SELECT * 
	    FROM INFORMATION_SCHEMA.TABLES   
	    WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'exchangeformatfieldnamedetails2')
	BEGIN
	
		--** drop all constraints 
		DECLARE @sql NVARCHAR(MAX) = N'';
		SELECT @sql += N'
		ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
		+ '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) +
		' DROP CONSTRAINT ' + QUOTENAME(name) + ';'
		FROM sys.objects
		WHERE type_desc LIKE '%CONSTRAINT'
		AND OBJECT_NAME(PARENT_OBJECT_ID) LIKE 'exchangeformatfieldnamedetails2';
		--print @sql;
		EXEC sp_executesql @sql;
		  
		--** drop table
		DROP TABLE [dbo].[exchangeformatfieldnamedetails2];
	
	END

	--** delete table exchangeformatfieldnamedetails if exits
	IF EXISTS (SELECT * 
	    FROM INFORMATION_SCHEMA.TABLES   
	    WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'exchangeformatfieldnamedetails')
	BEGIN
	
		--** drop all constraints 
		DECLARE @sql2 NVARCHAR(MAX) = N'';
		SELECT @sql2 += N'
		ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
		+ '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) +
		' DROP CONSTRAINT ' + QUOTENAME(name) + ';'
		FROM sys.objects
		WHERE type_desc LIKE '%CONSTRAINT'
		AND OBJECT_NAME(PARENT_OBJECT_ID) LIKE 'exchangeformatfieldnamedetails';
		--print @sql2;
		EXEC sp_executesql @sql2;
		  
		--** drop table
		DROP TABLE [dbo].[exchangeformatfieldnamedetails];
	
	END

	--** delete table exchange format if exits
	IF EXISTS (SELECT * 
	    FROM INFORMATION_SCHEMA.TABLES   
	    WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'exchangeformat')
	BEGIN
	  PRINT 'Table Exists';
	
		--ALTER TABLE [dbo].[exchangeformat] DROP CONSTRAINT [FK_company_coid];
		--** drop all constraints 
		DECLARE @sql3 NVARCHAR(MAX) = N'';
		SELECT @sql3 += N'
		ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
		+ '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) +
		' DROP CONSTRAINT ' + QUOTENAME(name) + ';'
		FROM sys.objects
		WHERE type_desc LIKE '%CONSTRAINT'
		AND OBJECT_NAME(PARENT_OBJECT_ID) LIKE 'exchangeformat';
		--print @sql3;
		EXEC sp_executesql @sql3;
	  	  
	  DROP TABLE [dbo].[exchangeformat];
	
	END
	
	go

	--create the table
	CREATE TABLE [dbo].[exchangeformat](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[name] [varchar](100) NOT NULL,
		[description] [varchar](200) NULL,
		[clientCompanyid] [int] NULL,
		[businessCompanyid] [int] NULL,
		[businessSubdivid] [int] NULL,
		[exchangeFormatLevel][varchar](100) NOT NULL,
		[dateFormat][varchar](100) NOT NULL,
		[lastModified] [datetime2](7) NOT NULL,
		[lastModifiedBy] [int] NOT NULL,
		CONSTRAINT PK_exchangeformat_id PRIMARY KEY (id),
		CONSTRAINT FK_ClientCompany_coid FOREIGN KEY ([clientCompanyid]) REFERENCES dbo.company(coid),
		CONSTRAINT FK_BusinessCompany_coid FOREIGN KEY ([businessCompanyid]) REFERENCES dbo.company(coid),
		CONSTRAINT FK_BusinessSubdiv_subdivid FOREIGN KEY ([businessSubdivid]) REFERENCES dbo.subdiv(subdivid)
	)
	
	go

	CREATE TABLE dbo.exchangeformatfieldnamedetails (
		id int NOT NULL IDENTITY(1,1),
		[position] int NOT NULL,
		mandatory bit NOT NULL,
		templateName varchar(50) NOT NULL,
		fieldName varchar(50) NOT NULL,
		exchangeFormatid int NOT NULL,
		CONSTRAINT PK_exchangeformatfieldnamedetails_id PRIMARY KEY (id),
		CONSTRAINT FK_exchangeformat_id FOREIGN KEY (exchangeFormatid) REFERENCES dbo.exchangeformat(id)
	) 
	
	go

	INSERT INTO dbversion (version) VALUES (240);

	go

COMMIT TRAN