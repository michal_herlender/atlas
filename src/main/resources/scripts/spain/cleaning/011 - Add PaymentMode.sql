-- Author Tony Provost 2016-01-28
-- Populate table paymentmode with default records (table for finance)

BEGIN TRAN 

USE [spain];
GO

SET IDENTITY_INSERT [paymentmode] ON
GO
  
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (1, 'CK', 'Check')											
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (2, 'WT', 'Wire Transfer')
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (3, 'CC', 'Credit card')
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (4, 'BE', 'Bill of exchange')
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (5, 'DC', 'Documentary credit')
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (6, 'PN', 'Promissory note')
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (7, 'CO', 'Confirming')
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (8, 'BD', 'Bank draft')
INSERT INTO [paymentmode] ([id], [code], [description]) VALUES (9, 'CP', 'Cash payment')
GO

-- English Translation
INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) SELECT [id], 'en_GB', [description] FROM [paymentmode] WHERE [id] NOT IN (SELECT [paymentmodeid] FROM [paymentmodetranslation] WHERE [locale] = 'en_GB') AND [description] <> ''
GO

-- Spanish Translation
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 1 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (1, 'es_ES', 'Cheque')											
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 2 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (2, 'es_ES', 'Transferencia')
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 3 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (3, 'es_ES', 'Tarjeta')
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 4 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (4, 'es_ES', 'Letra de cambio')
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 5 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (5, 'es_ES', 'Document de cr�dito')
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 6 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (6, 'es_ES', 'Pagar�')
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 7 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (7, 'es_ES', 'Confirming')
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 8 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (8, 'es_ES', 'Giro')
IF NOT EXISTS (SELECT 1 FROM [paymentmodetranslation] WHERE [paymentmodeid] = 9 AND locale = 'es_ES') 
	INSERT INTO [paymentmodetranslation] ([paymentmodeid], [locale], [translation]) VALUES (9, 'es_ES', 'Contado')
GO

SET IDENTITY_INSERT [paymentmode] OFF
GO

COMMIT TRAN
GO