USE [atlas]
GO

BEGIN TRAN

DECLARE @rir_init_status INT,
		@rir_init_activity INT,
		@need_rir_lookup INT,
		@rir_val_status INT,
		@rir_val_activity INT,
		@rir_init_hook INT,
		@rir_csr_rev_status INT,
		@rir_csr_rev_activity INT ,
		@rir_manager_val_status INT,
		@rir_manager_val_activity INT,
		@rir_val_manager_hook INT,
		@rir_onhold_status INT,
		@repair_time_activity INT;

-- Update existing status and activity  'rir validation by technician' with translations
SELECT @rir_val_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting Repair Inspection Report Validation by Technician';
SELECT @rir_val_activity = stateid FROM dbo.itemstate WHERE [description] = 'Repair Inspection Report Validated by Technician';

UPDATE dbo.itemstate SET [description] = 'Awaiting Repair Inspection Report Completion by Technician' WHERE stateid = @rir_val_status;
UPDATE dbo.itemstatetranslation SET translation = 'Awaiting Repair Inspection Report Completion by Technician' WHERE stateid = @rir_val_status AND locale = 'en_GB';
UPDATE dbo.itemstatetranslation SET translation = 'En attente de l''ach�vement du rapport d''inspection de r�paration par le technicien' WHERE stateid = @rir_val_status AND locale = 'fr_FR';
UPDATE dbo.itemstatetranslation SET translation = 'En espera de que el t�cnico complete el informe de inspecci�n de reparaci�n' WHERE stateid = @rir_val_status AND locale = 'es_ES';
UPDATE dbo.itemstatetranslation SET translation = 'Warten auf den Abschluss des Reparaturinspektionsberichts durch den Techniker' WHERE stateid = @rir_val_status AND locale = 'de_DE';

UPDATE dbo.itemstate SET [description] = 'Repair Inspection Report Completed by Technician' WHERE stateid = @rir_val_activity;
UPDATE dbo.itemstatetranslation SET translation = 'Repair Inspection Report Completed by Technician' WHERE stateid = @rir_val_activity AND locale = 'en_GB';
UPDATE dbo.itemstatetranslation SET translation = 'Rapport d''inspection de r�paration rempli par le technicien' WHERE stateid = @rir_val_activity AND locale = 'fr_FR';
UPDATE dbo.itemstatetranslation SET translation = 'Informe de inspecci�n de reparaci�n completado por un t�cnico' WHERE stateid = @rir_val_activity AND locale = 'es_ES';
UPDATE dbo.itemstatetranslation SET translation = 'Reparaturinspektionsbericht vom Techniker ausgef�llt' WHERE stateid = @rir_val_activity AND locale = 'de_DE';

-- add new status 'awaiting rir validation by manager' with translations
INSERT INTO dbo.itemstate
	([type], active, description, retired, lookupid, actionoutcometype)
	VALUES('workstatus', 1, 'Awaiting Repair Inspection Report Validation by Manager', 0, NULL, NULL);

SELECT @rir_manager_val_status = MAX(stateid) FROM dbo.itemstate;
	
INSERT INTO dbo.itemstatetranslation
(stateid, locale, [translation])
VALUES(@rir_manager_val_status, 'fr_FR', 'En attente de validation du rapport d''inspection de r�paration par le Responsable'),
	  (@rir_manager_val_status, 'es_ES', 'A la espera de la validaci�n del informe de inspecci�n de reparaci�n por el gerente'),
	  (@rir_manager_val_status, 'en_GB', 'Awaiting Repair Inspection Report Validation by Manager'),
	  (@rir_manager_val_status, 'de_DE', 'Warten auf die �berpr�fung des Reparaturinspektionsberichts durch den Manager');

-- add new activity 'rir validation by manager' with translations
INSERT INTO dbo.itemstate
	([type], active, description, retired, lookupid, actionoutcometype)
	VALUES('workactivity', 1, 'Repair Inspection Report validated by Manager', 0, NULL, NULL);

SELECT @rir_manager_val_activity = MAX(stateid) FROM dbo.itemstate;
	
INSERT INTO dbo.itemstatetranslation
(stateid, locale, [translation])
VALUES(@rir_manager_val_activity, 'fr_FR', 'Rapport d''inspection de r�paration valid� par le responsable'),
	  (@rir_manager_val_activity, 'es_ES', 'Informe de Inspecci�n de Reparaci�n validado por el Gerente'),
	  (@rir_manager_val_activity, 'en_GB', 'Repair Inspection Report validated by Manager'),
	  (@rir_manager_val_activity, 'de_DE', 'Vom Manager validierter Reparaturpr�fbericht');

-- add nextactivity for the new activities rir initialization and rir manager validation
INSERT INTO dbo.nextactivity(manualentryallowed,statusid,activityid)
VALUES (0,@rir_manager_val_status,@rir_manager_val_activity);

-- update the outcomestatus of Lookup_NeedRepairInspectionReport
-- to route to the new status awaiting rir initialization
SELECT @need_rir_lookup = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_NeedRepairInspectionReport';

UPDATE dbo.outcomestatus SET statusid = @rir_manager_val_status 
WHERE id = (SELECT outcomestatusid FROM dbo.lookupresult WHERE lookupid = @need_rir_lookup AND resultmessage = 2);

UPDATE dbo.outcomestatus SET statusid = @rir_manager_val_status WHERE activityid = @rir_val_activity;

-- add new hook initiate-rir-by-technician with hookactivity
UPDATE dbo.hook SET [name] = 'rir-review-by-csr' WHERE [name] = 'validate-rir-by-csr';

INSERT INTO dbo.hook
(active, alwayspossible, beginactivity, completeactivity, name, activityid)
VALUES(1, 1, 1, 1, 'validate-rir-by-manager', @rir_manager_val_activity);
SELECT @rir_val_manager_hook = MAX(id) FROM dbo.hook;	
INSERT INTO dbo.hookactivity(beginactivity, completeactivity, activityid, hookid, stateid)
VALUES(0, 1, @rir_manager_val_activity, @rir_val_manager_hook, @rir_manager_val_status);

-- update rir csr validation to rir manager validation
SELECT @rir_csr_rev_status = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting Repair Inspection Report Validation by CSR';
SELECT @rir_csr_rev_activity = stateid FROM dbo.itemstate WHERE [description] = 'Repair Inspection Report Validated by CSR';

UPDATE dbo.itemstate SET [description] = 'Awaiting Repair Inspection Report Review by CSR' WHERE stateid = @rir_csr_rev_status;
UPDATE dbo.itemstatetranslation SET translation = 'Awaiting Repair Inspection Report Review by CSR' WHERE stateid = @rir_csr_rev_status AND locale = 'en_GB';
UPDATE dbo.itemstatetranslation SET translation = 'En attente d''examen du rapport d''inspection de r�paration par le CRC' WHERE stateid = @rir_csr_rev_status AND locale = 'fr_FR';
UPDATE dbo.itemstatetranslation SET translation = 'Pendiente de revisi�n del informe de inspecci�n de reparaci�n por CSR' WHERE stateid = @rir_csr_rev_status AND locale = 'es_ES';
UPDATE dbo.itemstatetranslation SET translation = 'Warten auf �berpr�fung des Reparaturinspektionsberichts durch CSR' WHERE stateid = @rir_csr_rev_status AND locale = 'de_DE';

UPDATE dbo.itemstate SET [description] = 'Repair Inspection Report Reviewed by CSR' WHERE stateid = @rir_csr_rev_activity;
UPDATE dbo.itemstatetranslation SET translation = 'Repair Inspection Report Reviewed by CSR' WHERE stateid = @rir_csr_rev_activity AND locale = 'en_GB';
UPDATE dbo.itemstatetranslation SET translation = 'Rapport d''inspection de r�paration r�vis� par le CRC' WHERE stateid = @rir_csr_rev_activity AND locale = 'fr_FR';
UPDATE dbo.itemstatetranslation SET translation = 'Informe de inspecci�n de reparaci�n revisado por CSR' WHERE stateid = @rir_csr_rev_activity AND locale = 'es_ES';
UPDATE dbo.itemstatetranslation SET translation = 'Reparaturpr�fbericht von CSR gepr�ft' WHERE stateid = @rir_csr_rev_activity AND locale = 'de_DE';

-- Update the repair time activity name
SELECT @repair_time_activity = stateid FROM dbo.itemstate WHERE [description] = 'Production repair time recording';

UPDATE dbo.itemstate SET [description] = 'Production repair time recorded' WHERE stateid = @repair_time_activity;
UPDATE dbo.itemstatetranslation SET translation = 'Production repair time recorded' WHERE stateid = @repair_time_activity AND locale = 'en_GB';
UPDATE dbo.itemstatetranslation SET translation = 'Temps de r�paration de production enregistr�' WHERE stateid = @repair_time_activity AND locale = 'fr_FR';
UPDATE dbo.itemstatetranslation SET translation = 'Tiempo de reparaci�n de producci�n registrado' WHERE stateid = @repair_time_activity AND locale = 'es_ES';
UPDATE dbo.itemstatetranslation SET translation = 'Produktionsreparaturzeit aufgezeichnet' WHERE stateid = @repair_time_activity AND locale = 'de_DE';

-- update two outcomestatus 
INSERT INTO dbo.outcomestatus(defaultoutcome,lookupid,activityid,statusid)
VALUES (1,NULL,@rir_manager_val_activity,@rir_csr_rev_status);

-- add new manager validation fields to rir table
ALTER TABLE dbo.repairinspectionreport ADD manager INT NULL;
ALTER TABLE dbo.repairinspectionreport ADD CONSTRAINT repairinspectionreport_contact_manager_FK 
FOREIGN KEY(manager) REFERENCES dbo.contact(personid);
ALTER TABLE dbo.repairinspectionreport ADD validatedbymanager BIT NULL;
ALTER TABLE dbo.repairinspectionreport ADD validatedbymanageron DATETIME2 NULL;

-- add rir manager validation to laboratory dashboard
INSERT INTO dbo.stategrouplink(groupid,stateid,[type])
VALUES (36,@rir_manager_val_status,'CURRENT_SUBDIV');

INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
VALUES (74,@rir_manager_val_activity,'ALLOCATED_SUBDIV');

-- Add new status : On hold � repair awaiting for supplier price quotation
INSERT INTO dbo.itemstate
	([type], active, [description], retired, lookupid, actionoutcometype)
	VALUES('holdstatus', 1, 'On hold � repair awaiting for supplier price quotation', 0, NULL, NULL);

SELECT @rir_onhold_status = MAX(stateid) FROM dbo.itemstate;
	
INSERT INTO dbo.itemstatetranslation
(stateid, locale, [translation])
VALUES(@rir_onhold_status, 'fr_FR', 'En attente - r�paration en attente d''un devis fournisseur'),
	  (@rir_onhold_status, 'es_ES', 'En espera - reparaci�n en espera de cotizaci�n de precio del proveedor'),
	  (@rir_onhold_status, 'en_GB', 'On hold � repair awaiting for supplier price quotation'),
	  (@rir_onhold_status, 'de_DE', 'Warteschleife - Reparatur wartet auf Lieferantenpreisangebot');

INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
VALUES (19,@rir_onhold_status,'ALLOCATED_SUBDIV');

-- update repair inspection/completion report
GO
EXEC sp_rename 'dbo.repairinspectionreport.validatedbycsr', 'reviewedbycsr', 'COLUMN';
GO
EXEC sp_rename 'repairinspectionreport.validatedbycsron', 'reviewedbycsron', 'COLUMN';
GO
ALTER TABLE dbo.repaircompletionreport
ALTER COLUMN adjustmentperformed VARCHAR(16) NULL;
GO
UPDATE dbo.repaircompletionreport SET adjustmentperformed = 'YES' WHERE adjustmentperformed = '1';
UPDATE dbo.repaircompletionreport SET adjustmentperformed = 'NO' WHERE adjustmentperformed = '0';
UPDATE dbo.freerepaircomponent SET [status] = 'IN_STOCK' WHERE [status] = 'BOOKED';
GO
ALTER TABLE dbo.repairinspectionreport ALTER COLUMN misuse BIT NULL;

INSERT INTO dbversion(version) VALUES(706);

COMMIT TRAN