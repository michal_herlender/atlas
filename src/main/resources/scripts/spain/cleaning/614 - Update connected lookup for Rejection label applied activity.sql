USE [atlas]
GO

BEGIN TRAN

DECLARE @lookup_79 INT;

SELECT @lookup_79 = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_PreDeliveryRequirements (after lab calibration - before costing check)';

UPDATE dbo.itemstate SET lookupid = @lookup_79 WHERE [description] = 'Rejection label applied';

INSERT INTO dbversion(version) VALUES (614);

COMMIT TRAN