-- Author: Galen Beck -- 2016-11-11
-- Updates long name translations; we need to use 'no acreditada' instead of 'standard' or 'trazable'

USE [spain]
GO

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada en laboratorio'
 WHERE [locale] = 'es_ES' AND [servicetypeid] = 2;

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada in situ'
 WHERE [locale] = 'es_ES' AND [servicetypeid] = 5;

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada externo'
 WHERE [locale] = 'es_ES' AND [servicetypeid] = 8;

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada en laboratorio sin juicio'
 WHERE [locale] = 'es_ES' AND [servicetypeid] = 16;

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada in situ sin juicio'
 WHERE [locale] = 'es_ES' AND [servicetypeid] = 18;

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Calibración no acreditada externo sin juicio'
 WHERE [locale] = 'es_ES' AND [servicetypeid] = 20;

GO