USE spain;

BEGIN TRAN

ALTER TABLE spain.dbo.job
	ADD copyreceiptdatetoitems bit NOT NULL DEFAULT 0;

INSERT INTO dbversion(version) VALUES(471);

COMMIT TRAN