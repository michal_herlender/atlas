USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[description]
   SET [typology] = 0
 WHERE tmlid is NULL
GO

ALTER TABLE [description] ALTER COLUMN typology int not null;

INSERT INTO dbversion(version) VALUES(341);

COMMIT TRAN


