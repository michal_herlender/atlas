USE [atlas]
GO

BEGIN TRAN

alter table atlas.dbo.aliasgroup  add defaultBuisnessCompanyid int;
alter table dbo.aliasgroup add constraint FK_aliasgroup_defaultBuisnessCompany foreign key (defaultBuisnessCompanyid) references dbo.company;


/*renaming old CONSTRAINTS*/

alter table atlas.dbo.aliasgroup DROP CONSTRAINT FKfyifk5231lligwh65684ggc4c; 
alter table dbo.aliasgroup add constraint FK_aliasgroup_contact foreign key (lastModifiedBy) references dbo.contact;

alter table atlas.dbo.aliasgroup DROP CONSTRAINT FKm257ko0oy6n8bnunq0huvvyaq; 
alter table dbo.aliasgroup add constraint FK_aliasgroup_contact_createdby  foreign key (createdby) references dbo.contact;

alter table atlas.dbo.aliasservicetype DROP CONSTRAINT FKd49cvgwp0c0yp0ns86ptso1dh; 
alter table dbo.aliasservicetype add constraint FK_aliasservicetype_aliasgroup_id foreign key (aliasgroupid) references dbo.aliasgroup;

alter table atlas.dbo.aliasservicetype DROP CONSTRAINT FKbcfc1rwg3m0etuld5sofymt9u; 
alter table dbo.aliasservicetype add constraint FK_aliasservicetype_aliasgroup_servicetypeid foreign key (servicetypeid) references dbo.servicetype;

alter table atlas.dbo.exchangeformat DROP CONSTRAINT FKs9r09wu4qnecl9jottyf9l68h; 
alter table dbo.exchangeformat add constraint FK_exchangeformat_aliasgroup_id foreign key (aliasgroupid) references dbo.aliasgroup;

alter table atlas.dbo.aliasboolean DROP CONSTRAINT FK2airq052hx04m4lt981tq6goq; 
alter table dbo.aliasboolean  add constraint FK_aliasboolean_aliasgroup_id foreign key (aliasgroupid) references dbo.aliasgroup;

alter table atlas.dbo.aliasintervalunit DROP CONSTRAINT FKiktvhigv9hn22mbf85klfl827; 
alter table dbo.aliasintervalunit add constraint FK_aliasintervalunit_aliasgroup_id foreign key (aliasgroupid) references dbo.aliasgroup;

INSERT INTO dbversion(version) VALUES(532);

COMMIT TRAN  