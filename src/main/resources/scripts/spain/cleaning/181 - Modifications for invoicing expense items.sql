-- Script 181 - Adds fields for job expense changes, should fix various exceptions observed on test system
-- Galen Beck - 6 Nov 2017

USE [spain]
BEGIN TRAN

    alter table dbo.costs_service 
        add costtype varchar(31);

	GO

	UPDATE [dbo].[costs_service] SET [costtype] = 'costs_service' WHERE costtype is NULL;

	alter table dbo.costs_service 
        alter column costtype varchar(31) not null;

    alter table dbo.invoiceitem 
        add expenseitem int;

    alter table dbo.invoiceitem 
        add servicecost int;

    alter table dbo.invoiceitem 
        add constraint FK_invoiceitem_expenseitem 
        foreign key (expenseitem) 
        references dbo.jobexpenseitem;

    alter table dbo.invoiceitem 
        add constraint FK_invoiceitem_servicecost 
        foreign key (servicecost) 
        references dbo.costs_service;

COMMIT TRAN