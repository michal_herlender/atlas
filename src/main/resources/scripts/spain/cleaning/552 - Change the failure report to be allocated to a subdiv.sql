USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.faultreport ADD orgid int NULL;
ALTER TABLE dbo.faultreport ADD CONSTRAINT faultreport_orgid_subdiv_subdivid_FK 
	FOREIGN KEY (orgid) REFERENCES atlas.dbo.subdiv(subdivid);


INSERT INTO dbversion(version) VALUES(552);

COMMIT TRAN