
USE spain;

BEGIN TRAN;

ALTER TABLE dbo.certificatevalidation
  ADD personid int;

ALTER TABLE dbo.certificatevalidation
  ADD CONSTRAINT FK_CERTVALIDATION_TO_CONTACT
FOREIGN KEY (personid)
REFERENCES dbo.contact;


INSERT INTO dbversion (version) VALUES (202);

COMMIT TRAN;