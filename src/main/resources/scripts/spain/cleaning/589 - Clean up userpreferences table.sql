-- This script does two things:
-- 1) Cleans up the userpreferences table and makes contact optional
-- 2) Creates a new userpreferencesdefault table to be used for global/company/etc defaults

USE [atlas]
GO

BEGIN TRAN

-- Step 1 - Clean up userpreferences

-- Unused column since ~2015
ALTER TABLE dbo.userpreferences
  DROP COLUMN log_lastmodified;

-- Unused
DROP INDEX [IDX_userpreferences_jobtypeid] ON [dbo].[userpreferences]
GO

-- Never used, actually we use defaultJobType (Hibernate mapping corrected)
ALTER TABLE dbo.userpreferences
  DROP COLUMN jobtypeid;

-- Constraint on log_userid
ALTER TABLE [dbo].[userpreferences] DROP CONSTRAINT [FK8551364D202933D5]
GO

  -- Unused column since ~2015
ALTER TABLE dbo.userpreferences
  DROP COLUMN log_userid;

  -- Make defaultJobType not nullable
UPDATE dbo.userpreferences SET defaultJobType = 1 WHERE defaultJobType IS NULL;
GO

ALTER TABLE dbo.userpreferences 
ALTER COLUMN defaultJobType int not null;

-- Repurposed column (previously did nothing) to be nullable, now use to describe default UserPrefrences
ALTER TABLE dbo.userpreferences
  ALTER COLUMN scheme varchar(40);

-- Make contactid nullable (allows userpreferences to be added with default settings for new contacts)
ALTER TABLE dbo.userpreferences 
ALTER COLUMN contactid int null;

-- Existng scheme values irrelevant ("default" is meaningless and unused)
UPDATE dbo.userpreferences SET scheme = null;

-- Step 2 - Create new userpreferencesdefault table

    create table dbo.userpreferencesdefault (
       id int identity not null,
        globaldefault tinyint not null,
        addressid int,
        coid int,
        subdivid int,
        userpreferencesid int not null,
        primary key (id)
    );

    alter table dbo.userpreferencesdefault 
       add constraint UK_userpreferencesdefault_definition unique (globaldefault, coid, subdivid, addressid);

    alter table dbo.userpreferencesdefault 
       add constraint FK_userpreferencesdefault_addressid 
       foreign key (addressid) 
       references dbo.address;

    alter table dbo.userpreferencesdefault 
       add constraint FK_userpreferencesdefault_coid 
       foreign key (coid) 
       references dbo.company;

    alter table dbo.userpreferencesdefault 
       add constraint FK_userpreferencesdefault_subdivid 
       foreign key (subdivid) 
       references dbo.subdiv;

    alter table dbo.userpreferencesdefault 
       add constraint FK_userpreferencesdefault_userpreferencesid 
       foreign key (userpreferencesid) 
       references dbo.userpreferences;

INSERT INTO dbversion(version) VALUES(589);

COMMIT TRAN
