use [atlas]

begin tran

insert into stategrouplink(groupid, stateid, type)
values
(48, 38, 'CURRENT_SUBDIV'),
(48, 201, 'CURRENT_SUBDIV'),
(48, 4048, 'CURRENT_SUBDIV'),
(48, 4079, 'CURRENT_SUBDIV'),
(48, 4081, 'CURRENT_SUBDIV'),
(48, 4088, 'CURRENT_SUBDIV'),
(48, 4090, 'CURRENT_SUBDIV')

insert into dbversion(version) values (506)

commit tran