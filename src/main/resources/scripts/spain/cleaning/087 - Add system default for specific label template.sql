IF NOT EXISTS (SELECT 1 FROM [systemdefault] WHERE [defaultid] = 45)
BEGIN
	SET IDENTITY_INSERT [systemdefault] ON 
	INSERT INTO [systemdefault] ([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide]) 
	SELECT 45, 'string', 'Defines the label template to use', 'Label template', 'STANDARD', 0, 0, 10, 0
	WHERE NOT EXISTS (SELECT * FROM [systemdefault] WHERE [defaultid] = 45)
	SET IDENTITY_INSERT [systemdefault] OFF;	
	
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (45,'en_GB','Label template')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (45,'fr_FR','Mod�le d''�tiquette')
	INSERT INTO [systemdefaultnametranslation] (defaultid,locale,translation)
	VALUES (45,'es_ES','Modelo de etiqueta')
	
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (45,'en_GB','Label template')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (45,'fr_FR','Mod�le d''�tiquette')
	INSERT INTO [systemdefaultdescriptiontranslation] (defaultid,locale,translation)
	VALUES (45,'es_ES','Modelo de etiqueta')
	
	INSERT INTO systemdefaultscope (defaultid,scope)
	VALUES (45,0)
	
	INSERT INTO systemdefaultcorole (defaultid,coroleid)
	VALUES (45,1)	
	INSERT INTO systemdefaultcorole (defaultid,coroleid)
	VALUES (45,5)
END
ELSE
BEGIN
	UPDATE [systemdefault]
	SET [defaultdatatype] = 'string',
	[defaultdescription] = 'Defines the label template to use',
	[defaultname] = 'Label template',
	[defaultvalue] = 'STANDARD',
	[endd] = 0,
	[start] = 0,
	[groupid] = 10,
	[groupwide] = 0
	WHERE defaultid = 45
END
