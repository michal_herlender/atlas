USE [atlas]
GO

BEGIN TRAN

DECLARE @itemstate1 int = (select it.stateid from itemstate it where it.description 
								like '%Recorded onsite service - awaiting job costing%');

DECLARE @itemstate2 int = (select it.stateid from itemstate it where it.description 
								like '%Onsite - calibration failed, awaiting inspection costing to client%');


Update itemstate set description='Onsite - calibration performed, awaiting job costing' where stateid=@itemstate1

Update itemstate set description='Onsite - calibration performed, awaiting inspection costing' where stateid=@itemstate2


update itemstatetranslation set translation='Sur site - Prestation réalisée, en attente de chiffrage' where stateid=@itemstate1 and locale='fr_FR'
update itemstatetranslation set translation='Onsite - calibration performed, awaiting job costing' where stateid=@itemstate1 and locale='en_GB'
update itemstatetranslation set translation='In situ - Trabajo se ha realizado, esperando coste del trabajo' where stateid=@itemstate1 and locale='es_ES'
update itemstatetranslation set translation='Vor-Ort - Aufgezeichneter Auftrag - wartet auf Auftrag-Preiskalkulation' where stateid=@itemstate1 and locale='de_DE'


update itemstatetranslation set translation='Sur site - Prestation réalisée, en attente de chiffrage d inspection' where stateid=@itemstate2 and locale='fr_FR'
update itemstatetranslation set translation='Onsite - calibration performed, awaiting inspection costing' where stateid=@itemstate2 and locale='en_GB'
update itemstatetranslation set translation='In situ - Trabajo se ha realizado, esperando coste de inspección' where stateid=@itemstate2 and locale='es_ES'
update itemstatetranslation set translation='Vor-Ort  wartet auf Inspektions-Preiskalkulation' where stateid=@itemstate2 and locale='de_DE'

INSERT INTO dbversion VALUES (727)

COMMIT TRAN
