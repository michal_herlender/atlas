USE spain;
GO

IF NOT EXISTS (SELECT * FROM businessemails WHERE orgid = 6645)
INSERT INTO businessemails(lastModified, account, autoService, collections, goodsIn, quality, quotations, recall, sales, web, orgid)
VALUES (
	'2016-10-01',
	'finanzas.estem-mad@trescal.com',
	'autoservicio.estem-mad@trescal.com',
	'recogidas.estem-mad@trescal.com',
	'logistica.estem-mad@trescal.com',
	'calidad.estem-mad@trescal.com',
	'ofertas.estem-mad@trescal.com',
	'recalibraciones.estem-mad@trescal.com',
	'comercial.estem-mad@trescal.com',
	'web.estem-mad@trescal.com',
	6645);