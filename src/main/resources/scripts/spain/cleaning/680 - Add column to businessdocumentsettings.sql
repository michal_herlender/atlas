-- Adds an additional column to business document settings, 
-- to configure that only SIGNED certificates appear on delivery notes for companies using TLM processes
-- For the "regular" Atlas companies, SIGNING_NOT_SUPPORTED is normal to also use

USE [atlas]
GO

BEGIN TRAN

    alter table dbo.businessdocumentsettings 
       add deliveryNoteSignedCertificates bit;

	GO

	UPDATE businessdocumentsettings SET deliveryNoteSignedCertificates = 0;

	GO

	alter table dbo.businessdocumentsettings 
	   alter column deliveryNoteSignedCertificates bit not null;

	declare @coidTrescalSAS int;
	declare @coidTrescalSARL int;
	declare @coidTrescalMaroc int;
	declare @coidIntermes int;

	-- Company names may vary on older databases / on test.
	select @coidTrescalSAS = coid from company where coname IN ('TRESCAL SAS', 'TRESCAL SA') and corole = 5;
	select @coidTrescalSARL = coid from company where coname = 'TRESCAL SARL' and corole = 5;
	select @coidTrescalMaroc = coid from company where coname = 'TRESCAL MAROC SARL' and corole = 5;
	select @coidIntermes = coid from company where coname IN ('INTERMES', 'INTERMES SAS') and corole = 5;

	PRINT '4 coids should show - if not check company names'
	PRINT @coidTrescalSAS;
	PRINT @coidTrescalSARL;
	PRINT @coidTrescalMaroc;
	PRINT @coidIntermes;

-- Morocco should already have an entity to configure special invoice settings; update it...

UPDATE [dbo].[businessdocumentsettings]
   SET deliveryNoteSignedCertificates = 1
   WHERE businesscompanyid = @coidTrescalMaroc;

INSERT INTO [dbo].[businessdocumentsettings]
           ([recipientAddressOnLeft]
           ,[globalDefault]
           ,[invoiceAmountText]
		   ,[deliveryNoteSignedCertificates]
           ,[businesscompanyid])
     VALUES
	       (0,0,0,1,@coidTrescalSAS),
		   (0,0,0,1,@coidTrescalSARL),
		   (0,0,0,1,@coidIntermes);

INSERT INTO dbversion(version) VALUES(680);

COMMIT TRAN
