-- Fixes an issue where both the lookup id and status are set on two of the new outcome statuses from script 679.
-- This seems not to cause a bug in the workflow logic as statuses are checked first, but it looks in
-- the workflow editor that the lookup is wired back to itself.
-- Clearing the lookup id in both of these outcome statuses fixes the issue so it's clear that each are wired to statuses.
-- Galen Beck 2020-09-10

USE [atlas]
GO

BEGIN TRAN

DECLARE @lu int;
DECLARE @lr0 int;
DECLARE @lr1 int;
DECLARE @os0 int;
DECLARE @os1 int;

select @lu = [id] FROM [dbo].[lookupinstance] where lookup = 'Lookup_DNCreationApprovalRequired';

PRINT @lu;

select @lr0 = [id] FROM [dbo].[lookupresult] where lookupid = @lu and resultmessage = 0;
select @lr1 = [id] FROM [dbo].[lookupresult] where lookupid = @lu and resultmessage = 1;

PRINT @lr0;
PRINT @lr1;

select @os0 = [outcomestatusid] FROM [dbo].[lookupresult] where id = @lr0;
select @os1 = [outcomestatusid] FROM [dbo].[lookupresult] where id = @lr1;

PRINT @os0;
PRINT @os1;

update outcomestatus set lookupid = null where id in (@os0, @os1);

INSERT INTO dbversion(version) VALUES(691);

COMMIT TRAN

GO


