-- Changes the definitions of the two "small" Germany label templates to match general requirements for all labs

USE [atlas]
GO

BEGIN TRAN

DECLARE @smallGermanyAccreditedId INT;
DECLARE @smallGermanyTraceableId INT;

SET @smallGermanyAccreditedId = (SELECT [id] FROM [dbo].[alligatorlabeltemplate] WHERE [description] = 'DE-Mahlow Brother Accredited Small')
SET @smallGermanyTraceableId = (SELECT [id] FROM [dbo].[alligatorlabeltemplate] WHERE [description] = 'DE-Mahlow Brother Traceable Small')

UPDATE [dbo].[alligatorlabeltemplate]
   SET [description] = 'Germany Brother Accredited',
      [parameterstyle] = 'FIXED'
 WHERE [id] = @smallGermanyAccreditedId

 UPDATE [dbo].[alligatorlabeltemplate]
   SET [description] = 'Germany Brother Traceable',
      [parameterstyle] = 'FIXED'
 WHERE [id] = @smallGermanyTraceableId

DELETE FROM [dbo].[alligatorlabelparameter]
      WHERE [templateid] in (@smallGermanyAccreditedId, @smallGermanyTraceableId);

INSERT INTO [dbo].[alligatorlabelparameter]
           ([format],[name],[text],[value],[templateid])
     VALUES
           (null,'value1',null,'CERT_NUMBER_RIGHT_PART_TRIMMED',@smallGermanyAccreditedId),
           (null,'value2','D-K-','TEXT',@smallGermanyAccreditedId),
           (null,'value3',null,'CERT_ACCREDITATION_NUMBER',@smallGermanyAccreditedId),
           ('yyyy-MM','value4',null,'CERT_CAL_DATE',@smallGermanyAccreditedId),
           (null,'barcode',null,'INST_TRESCAL_ID',@smallGermanyAccreditedId);

INSERT INTO [dbo].[alligatorlabelparameter]
           ([format],[name],[text],[value],[templateid])
     VALUES
           (null,'barcode',null,'INST_TRESCAL_ID',@smallGermanyTraceableId),
           (null,'field1','N�chste Kal. :','TEXT',@smallGermanyTraceableId),
           ('dd-MM-yyyy','value1',null,'INST_DUE_DATE',@smallGermanyTraceableId);

UPDATE [dbo].[alligatorlabeltemplate]
   SET [templatexml] = '<template>
  <parameters>
    <parameter name="field1" value="Cert No" type="string" />
    <parameter name="value1" value="123456" type="string" />
    <parameter name="field2" value="Accred 1" type="string" />
    <parameter name="value2" value="D-K-" type="string" />
    <parameter name="field3" value="Accred 2" type="string" />
    <parameter name="value3" value="15015-01-06" type="string" />
    <parameter name="field4" value="Due Date" type="string" />
    <parameter name="value4" value="2021-03" type="string" />
  </parameters>
  <name>Germany Brother Accredited</name>
  <height>307</height>
  <width>217</width>
  <string ypos="4" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="${value1}" />
  <string ypos="56" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="${value2}" />
  <string ypos="104" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="${value3}" />
  <string ypos="156" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="${value4}" />
  <barcode ypos="204" xpos="56" height="105" width="105" rotationAngle="0" value="${barcode}" type="ECC200" />
  <rectangle ypos="0" xpos="4" height="200" width="209" rotationAngle="0" />
  <rectangle ypos="48" xpos="4" height="104" width="209" rotationAngle="0" />
  <defaultPrinter>Brother PT-P750W</defaultPrinter>
</template>'
 WHERE [id] = @smallGermanyAccreditedId

 UPDATE [dbo].[alligatorlabeltemplate]
   SET [templatexml] = '<template>
  <parameters>
    <parameter name="field1" value="N�chste Kal. :" type="string" />
    <parameter name="value1" value="27.03.2021" type="string" />
    <parameter name="barcode" value="1234567" type="string" />
  </parameters>
  <name>Germany Brother Traceable</name>
  <height>307</height>
  <width>217</width>
  <string ypos="158" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="${field1}" />
  <string ypos="202" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="${value1}" />
  <barcode ypos="49" xpos="56" height="105" width="105" rotationAngle="0" value="${barcode}" type="ECC200" />
  <rectangle ypos="0" xpos="4" height="240" width="209" rotationAngle="0" />
  <image ypos="10" xpos="46" height="35" width="125" rotationAngle="0" name="trescal_black_no_curve" />
  <defaultPrinter>Brother PT-P750W</defaultPrinter>
</template>'
 WHERE [id] = @smallGermanyTraceableId

UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-10' WHERE [description] = 'Braunschweig - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-01' WHERE [description] = 'Darmstadt - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-07' WHERE [description] = 'Donauw�rth - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-13' WHERE [description] = 'Egmating - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-03' WHERE [description] = 'Esslingen - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-08' WHERE [description] = 'Halver - Kalibrierung' and labno = 'D-K-15015-01-08'
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-02-00' WHERE [description] = 'Halver - Kalibrierung' and labno = 'D-K-15015-02-00';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-09' WHERE [description] = 'Hildesheim - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-14' WHERE [description] = 'Kiel - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-11' WHERE [description] = 'Leipzig - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-06' WHERE [description] = 'Mahlow - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-02' WHERE [description] = 'Neustadt - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-04' WHERE [description] = 'Parchim - Kalibrierung';
UPDATE [dbo].[accreditedlab] SET [labno] = '15015-01-12' WHERE [description] = 'Wetzlar - Kalibrierung';



INSERT INTO dbversion(version) VALUES(675);

COMMIT TRAN

