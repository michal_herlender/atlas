-- Adds an additional column "Fiscal ID of subdivision "to business document settings, 

USE [atlas]
GO

BEGIN TRAN

    alter table dbo.businessdocumentsettings 
       add subdivisionFiscalIdentifier bit;

	GO

	UPDATE businessdocumentsettings SET subdivisionFiscalIdentifier = 0;

	GO

INSERT INTO dbversion(version) VALUES(834);

COMMIT TRAN
