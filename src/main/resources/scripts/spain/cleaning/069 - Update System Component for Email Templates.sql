-- Simple script to update system component to be aware of which components can have email templates configured
-- Author: Galen Beck 2016-10-06

USE [spain]
GO

UPDATE [dbo].[systemcomponent]
   SET [emailtemplates] = 1
 WHERE [defaultemailsubject] IS NOT NULL;
GO

UPDATE [dbo].[systemcomponent]
   SET [emailtemplates] = 0
 WHERE [defaultemailsubject] IS NULL;
GO