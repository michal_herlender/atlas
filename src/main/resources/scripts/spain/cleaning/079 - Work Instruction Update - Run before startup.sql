-- Author Galen Beck 2016-10-21
-- Prepares WorkInstruction to be Subdivision specific rather than Company specific
-- We don't store any production data in these tables so the columns can be deleted / recreated

USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[workinstructionversion]
GO

DELETE FROM [dbo].[workinstruction]
GO

DROP INDEX IDX_workinstruction_processId ON [workinstruction];

-- Remove unique constraint on title - too rigid
ALTER TABLE [workinstruction] DROP CONSTRAINT UQ__workinst__E52A1BB32917FB5A

ALTER TABLE [workinstruction] DROP CONSTRAINT FK_gd2hkrtcre155kew1tu8v9u8b;
ALTER TABLE [workinstruction] DROP CONSTRAINT FK33BAC0DD13CE812E;
ALTER TABLE [workinstruction] DROP COLUMN orgid;
ALTER TABLE [workinstruction] DROP COLUMN processId;

COMMIT TRAN