USE [spain]
BEGIN TRAN

alter table dbo.calreq
  add descid int;

alter table dbo.calreq
  add constraint FK1r0mnibldw8u96jj6ea9punec
foreign key (descid)
references dbo.description;

INSERT INTO dbversion VALUES (229);

COMMIT TRAN