-- Creates 4 new production and 4 new test profiles for Bilbao
-- Used to save certs per user
-- Galen Beck - 2018-04-20 - uses settings from Obed

USE [spain]
GO

BEGIN TRAN

-- 3.     Low frequency (rename)

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL BF Production'
 WHERE [description] = 'ES-TIC-BIO/VIT Production';

UPDATE [dbo].[alligatorsettings]
   SET [description] = 'ES-TIC-BIL BF Test'
 WHERE [description] = 'ES-TIC-BIO/VIT Test';

-- 1.     High Frequency/Accelerometer

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL HF-ACEL Production'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\ALTA FRECUENCIA\TRABAJOS\ACELEROMETRIA\Plantillas\Certificates_primarios'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\ALTA FRECUENCIA\TRABAJOS\ACELEROMETRIA\Plantillas\Certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Production');
GO

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL HF-ACEL Test'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\ALTA FRECUENCIA\TRABAJOS\ACELEROMETRIA\Plantillas\Certificates_primarios'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\ALTA FRECUENCIA\TRABAJOS\ACELEROMETRIA\Plantillas\Certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Test');
GO

-- 2.     High frequency/Sonometer

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL HF-SONO Production'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\ALTA FRECUENCIA\TRABAJOS\SONOMETROS\Plantillas\Certificates_primarios'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\ALTA FRECUENCIA\TRABAJOS\SONOMETROS\Plantillas\Certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Production');
GO

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL HF-SONO Test'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\ALTA FRECUENCIA\TRABAJOS\SONOMETROS\Plantillas\Certificates_primarios'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\ALTA FRECUENCIA\TRABAJOS\SONOMETROS\Plantillas\Certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Test');
GO

-- 4.     Mechanic (Mecanica)

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL HF-MEC Production'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\MECANICA\TRABAJOS\PLANTILLAS\Certificates_primarios'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\MECANICA\TRABAJOS\PLANTILLAS\Certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Production');
GO

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL HF-MEC Test'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\MECANICA\TRABAJOS\PLANTILLAS\Certificates_primarios'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\MECANICA\TRABAJOS\PLANTILLAS\Certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Test');
GO

-- 5.     Temperature

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL HF-TEMP Production'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\TEMPERATURA\TRABAJOS\CALIBRACIÓN TEMPERATURA\PLANTILLAS\certificates_primarios'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\TEMPERATURA\TRABAJOS\CALIBRACIÓN TEMPERATURA\PLANTILLAS\certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Production');
GO

INSERT INTO [dbo].[alligatorsettings]
           ([description]
           ,[uploadrawdatafolder]
           ,[uploadtempdatafolder]
           ,[excellocaltempfolder]
           ,[excelrelativetempfolder]
           ,[excelsavetempcopy]
           ,[globaldefault]
           ,[uploadenabled]
           ,[uploadmoveuponupload]
           ,[uploadusewindowscredentials]
           ,[uploadwindowsdomain]
           ,[uploadwindowspassword]
           ,[uploadwindowsusername])
     (SELECT 'ES-TIC-BIL HF-TEMP Test'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\TEMPERATURA\TRABAJOS\CALIBRACIÓN TEMPERATURA\PLANTILLAS\certificates_primarios'
           ,'\\tb-dc1\Datos\Actividad_Laboratorios\Metrologia\TEMPERATURA\TRABAJOS\CALIBRACIÓN TEMPERATURA\PLANTILLAS\certificates'
           ,[excellocaltempfolder]
		  ,[excelrelativetempfolder]
		  ,[excelsavetempcopy]
		  ,[globaldefault]
		  ,[uploadenabled]
		  ,[uploadmoveuponupload]
		  ,[uploadusewindowscredentials]
		  ,[uploadwindowsdomain]
		  ,[uploadwindowspassword]
		  ,[uploadwindowsusername]
	  FROM [dbo].[alligatorsettings] WHERE [description] = 'ES-TIC-BIL BF Test');
GO

INSERT INTO dbversion (version) VALUES (247);

COMMIT TRAN