-- See script 784 for the script to create stored procedure; it seems that the entire script became a stored procedure
-- (including table modification / dbversion) as previously written
-- Updated this script to remove stored procedure creation so the other parts would run as intended.

USE [atlas]
GO

BEGIN TRAN

GO

-- Add new table processed_changeentity 
CREATE TABLE dbo.processed_changeentity(
changeentity INT PRIMARY KEY NOT NULL,
lastprocessedlsn BINARY(10) NULL);

INSERT INTO dbo.processed_changeentity(changeentity,lastprocessedlsn)
VALUES (0, NULL),
		(1, NULL),
		(2, NULL),
		(3, NULL),
		(4, NULL),
		(5, NULL),
		(6, NULL),
		(7, NULL),
		(8, NULL),
		(9, NULL),
		(10, NULL),
		(11, NULL),
		(12, NULL),
		(13, NULL),
		(14, NULL),
		(15, NULL),
		(16, NULL),
		(17, NULL),
		(18, NULL);

-- Add new field payload 
ALTER TABLE dbo.change_queue ADD payload VARCHAR(MAX) NOT NULL;
ALTER TABLE dbo.change_queue ADD changetype INT NULL;

insert into dbversion(version) values(782);

COMMIT TRAN