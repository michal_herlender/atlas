-- For DEV-2894
-- Bug: Job item of repair service with external subcontracting capability in status "Awaiting Repair Inspection Report Validation by Manager" is not in the dashboard.
-- Adds to Subcontracting / Third Party dashboard

BEGIN TRAN

DECLARE @stateid int;
DECLARE @groupid_TPWORK int = 37;

SELECT @stateid = [stateid]
  FROM [dbo].[itemstate] where description = 'Awaiting Repair Inspection Report Validation by Manager'

PRINT @stateid;

INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (@groupid_TPWORK
           ,@stateid
           ,'ALLOCATED_SUBDIV')
GO

INSERT INTO dbversion(version) VALUES(870);

COMMIT TRAN