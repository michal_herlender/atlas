-- Fixes two new French service type translations where I'd managed 
-- to write a mixture of French and Spanish in script 729

USE [atlas]
GO

BEGIN TRAN

DECLARE @vco_os_servicetype int;
DECLARE @ic_os_servicetype int;

SELECT @vco_os_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'VCO-OS';
SELECT @ic_os_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'IC-OS';

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Surveillance métrologique sur site'
 WHERE [servicetypeid] = @ic_os_servicetype AND [locale] = 'fr_FR'

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Vérification du bon fonctionnement sur site'
 WHERE [servicetypeid] = @vco_os_servicetype AND [locale] = 'fr_FR'

INSERT INTO dbversion(version) VALUES (730)	

COMMIT TRAN
