--  the script is to be executed only on test and dev env NOT for production
-- Creating Selenium user for for testing purposes only 
-- username = integration.selenium  Passsword = Trescal123*

use [spain]
GO

BEGIN TRAN

insert into contact (email,firstname,lastname,lastModified,subdivid,groupid,defaddress,active,hrid,preference,locale) 
values('seleniumintergration@gmail.com','intergration',' Selenium',(SELECT SYSDATETIME()),
(select subdivid from subdiv where subname='casablanca' and coid=(select coid from company where coname='TRESCAL MAROC SARL')),(select groupid from usergroup where name='Default Group'),
(select addrid from address where addr1='Zone Indusparc - Sidi Moumen' and subdivid=(select subdivid from subdiv where subname='casablanca' and coid=(select coid from company where coname='TRESCAL MAROC SARL'))),1,'MA-10','E','en_GB')

insert into users (username,password,personid,webaware,webuser,webtermsaccepted,lastModified,lastModifiedBy) 
values ('integration.selenium','$e0801$c79HhGbGohAEbYSUf51lfiTWktsiN4Lhp/efX1E5BmdTDZ8Lh/6XMtf/SLtiAVPoXgZvJLWGZ5uv5nZP6dILew==$yJHTiC7X6JORDAfsoKPrCh+6fMAv8JOoqn0gf3sTlcc=',
(select personid from contact where email='seleniumintergration@gmail.com')
,1,0,0,(SELECT SYSDATETIME()),(select personid from contact where email='soufiane.amankachi@trescal.com'))

DECLARE @count int
	DECLARE @id int
	DECLARE @getid  CURSOR
	SET @count = (select count(*) from subdiv s join company c on s.coid=c.coid
						where c.corole=(select coroleid from corole where description='Business Companies'))
	SET @getid = CURSOR FOR SELECT subdivid from subdiv s join company c on s.coid=c.coid
							where c.corole=(select coroleid from corole where description='Business Companies')

	OPEN @getid
	FETCH NEXT
	FROM @getid INTO @id
	WHILE @@FETCH_STATUS = 0
	BEGIN
		insert into userrole (role,userName,lastModified,lastModifiedBy,orgid) values(4,'integration.selenium','2019-03-18 10:17:53',
		(select personid from contact where email='soufiane.amankachi@trescal.com'),@id)
		FETCH NEXT FROM @getid INTO @id
	END

insert into userpreferences (jikeynavigation,mouseactive,mouseoverdelay,navbarposition,scheme,contactid,includeselfinreplies,autoprintlabel,lastModified,lastModifiedBy)
values(1,1,0,0,'Default',(select personid from contact where email='seleniumintergration@gmail.com'),0,0,
(SELECT SYSDATETIME()),(select personid from contact where email='soufiane.amankachi@trescal.com'))

--permission to start calibration 'MA-CAS-00055'
insert into procedureaccreditation (accredlevel,awarded,awardedby,accreditationfor,caltypeid,procid,lastModified,lastModifiedBy)
values('APPROVE',(SELECT SYSDATETIME()),
(select personid from contact where email='soufiane.amankachi@trescal.com'),
(select personid from contact where email='seleniumintergration@gmail.com'),
(select caltypeid from calibrationtype where caltypeid=1),
(select id from procs where reference='MA-CAS-00055'),
(SELECT SYSDATETIME()),
(select personid from contact where email='soufiane.amankachi@trescal.com'))

INSERT INTO dbversion(version) VALUES (431);
COMMIT TRAN