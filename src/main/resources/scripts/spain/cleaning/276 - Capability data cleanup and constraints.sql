-- Adds ServiceCapability constraints and fixes few rows of data
-- Removes the Capability and CapabilityTranslation tables (older implementation of Subfamily, we use Description instead)

USE [spain]

BEGIN TRAN

-- Remove capabilityid from instmodel (unused)
ALTER table instmodel DROP CONSTRAINT FK55l70ih0emcdojx4v4kyr8hy4;
GO
ALTER table instmodel DROP COLUMN capabilityid;
GO

-- Unused tables, removed along with their entities / service / dao from codebase.
DROP TABLE capabilitytranslation;
DROP TABLE capability;

-- There were 2 bad entries (no procedure) for Acoustic Calibrator and Defibrillator

DELETE FROM [dbo].[servicecapability]
      WHERE modelid in (34118, 39147) and orgid = 6645;

ALTER TABLE servicecapability ALTER column calibrationtypeid int not null;
ALTER TABLE servicecapability ALTER column procedureid int not null;

INSERT INTO dbversion(version) VALUES(276);

COMMIT TRAN