USE [atlas]
GO

BEGIN TRAN

declare @lookupid int;
declare @lr1 int;
declare @lr2 int;
declare @os1 int;
declare @os2 int;

INSERT INTO dbo.lookupinstance (lookup,lookupfunction)
	VALUES ('Lookup_LatestFailureReportOutcome (onsite repair or adjustment)',43) 
	
select @lookupid = max(li.id) from dbo.lookupinstance li

-- create outcome status
INSERT INTO dbo.outcomestatus (defaultoutcome,lookupid)
	VALUES (0,90)
select @os1 = max(os.id) from dbo.outcomestatus os
 
INSERT INTO atlas.dbo.outcomestatus (defaultoutcome,lookupid)
	VALUES (0,90)
select @os2 = max(os.id) from dbo.outcomestatus os

--create lookup results
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lookupid,@os1,1)

INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lookupid,@os2,2)


-- link the outcome for lookup results 'WITHIN_LIMIT' and 'NO_COSTING_REQUIRED' of lookup '100 - Lookup_CostsWithinApprovedLimit'
-- that goes currently to '4082 - Onsite - awaiting adjustment' to the lookup created above instead
UPDATE atlas.dbo.outcomestatus
	SET lookupid=@lookupid,statusid=NULL
	WHERE id=328

INSERT INTO dbversion(version) VALUES(533);

COMMIT TRAN  