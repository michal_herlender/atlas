-- Add missing cal authorization levels from DEV-2949

BEGIN TRAN

DECLARE @caltype_AC_EX INT ;
DECLARE @caltype_STD_EX INT ;
DECLARE @caltype_CLIENT INT ;
DECLARE @caltype_CLIENT_EX INT ;
DECLARE @caltype_SERVICE INT ;
DECLARE @caltype_ADMIN INT ;
DECLARE @caltype_TRADE INT ;
DECLARE @caltype_LT INT ;

DECLARE @lvl_REMOVED	varchar(255) = 'REMOVED';
DECLARE @lvl_SUPERVISED varchar(255) = 'SUPERVISED';
DECLARE @lvl_PERFORM varchar(255) = 'PERFORM';
DECLARE @lvl_APPROVE varchar(255) = 'APPROVE';

SELECT @caltype_AC_EX = ct.[caltypeid] FROM [dbo].[servicetype] st LEFT JOIN calibrationtype ct on ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'AC-EX';
SELECT @caltype_STD_EX = ct.[caltypeid] FROM [dbo].[servicetype] st LEFT JOIN calibrationtype ct on ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'STD-EX';
SELECT @caltype_CLIENT = ct.[caltypeid] FROM [dbo].[servicetype] st LEFT JOIN calibrationtype ct on ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'CLIENT';
SELECT @caltype_CLIENT_EX = ct.[caltypeid] FROM [dbo].[servicetype] st LEFT JOIN calibrationtype ct on ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'CLIENT-EX';
SELECT @caltype_SERVICE = ct.[caltypeid] FROM [dbo].[servicetype] st LEFT JOIN calibrationtype ct on ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'SERVICE';
SELECT @caltype_ADMIN = ct.[caltypeid] FROM [dbo].[servicetype] st LEFT JOIN calibrationtype ct on ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'ADMIN';
SELECT @caltype_TRADE = ct.[caltypeid] FROM [dbo].[servicetype] st LEFT JOIN calibrationtype ct on ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'TRADE';
SELECT @caltype_LT = ct.[caltypeid] FROM [dbo].[servicetype] st LEFT JOIN calibrationtype ct on ct.servicetypeid = st.servicetypeid WHERE st.shortname = 'LT';

PRINT @caltype_AC_EX ;
PRINT @caltype_STD_EX ;
PRINT @caltype_CLIENT ;
PRINT @caltype_CLIENT_EX ;
PRINT @caltype_SERVICE ;
PRINT @caltype_ADMIN ;
PRINT @caltype_TRADE ;
PRINT @caltype_LT ;

INSERT INTO [dbo].[calibrationaccreditationlevel]
           ([accredlevel]
           ,[caltypeid])
     VALUES
           (@lvl_REMOVED, @caltype_AC_EX),
           (@lvl_SUPERVISED, @caltype_AC_EX),
           (@lvl_PERFORM, @caltype_AC_EX),
           (@lvl_APPROVE, @caltype_AC_EX),

           (@lvl_REMOVED, @caltype_STD_EX),
           (@lvl_SUPERVISED, @caltype_STD_EX),
           (@lvl_PERFORM, @caltype_STD_EX),
           (@lvl_APPROVE, @caltype_STD_EX),

           (@lvl_REMOVED, @caltype_CLIENT),
           (@lvl_SUPERVISED, @caltype_CLIENT),
           (@lvl_PERFORM, @caltype_CLIENT),
           (@lvl_APPROVE, @caltype_CLIENT),

           (@lvl_REMOVED, @caltype_CLIENT_EX),
           (@lvl_SUPERVISED, @caltype_CLIENT_EX),
           (@lvl_PERFORM, @caltype_CLIENT_EX),
           (@lvl_APPROVE, @caltype_CLIENT_EX),

           (@lvl_REMOVED, @caltype_SERVICE),
           (@lvl_SUPERVISED, @caltype_SERVICE),
           (@lvl_PERFORM, @caltype_SERVICE),
           (@lvl_APPROVE, @caltype_SERVICE),

           (@lvl_REMOVED, @caltype_ADMIN),
           (@lvl_SUPERVISED, @caltype_ADMIN),
           (@lvl_PERFORM, @caltype_ADMIN),
           (@lvl_APPROVE, @caltype_ADMIN),

           (@lvl_REMOVED, @caltype_TRADE),
           (@lvl_SUPERVISED, @caltype_TRADE),
           (@lvl_PERFORM, @caltype_TRADE),
           (@lvl_APPROVE, @caltype_TRADE),

           (@lvl_REMOVED, @caltype_LT),
           (@lvl_SUPERVISED, @caltype_LT),
           (@lvl_PERFORM, @caltype_LT),
           (@lvl_APPROVE, @caltype_LT);

INSERT INTO dbversion(version) VALUES(871);

GO

COMMIT TRAN