BEGIN TRAN;

USE spain;
GO

EXEC sp_rename 'instrument.permitednumberofuses', 'permittednumberofuses', 'COLUMN';
GO

INSERT INTO dbversion VALUES (222);
GO

COMMIT TRAN;