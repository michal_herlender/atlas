-- Author Galen Beck - 2017-01-20
-- Adds state group links for one active state that had no stategrouplink

-- 4074 - Awaiting PO for inter company work - now in third party group - ALLOCATED_SUBDIV type

USE [spain]
GO

INSERT INTO [dbo].[stategrouplink]
           ([groupid],[stateid],[type])
     VALUES
		   (37,4074,'ALLOCATED_SUBDIV');
GO

