use atlas;
go

begin tran

create index IDX_invoiceitem_invoice on dbo.invoiceitem (invoiceid);
create index IDX_invoiceitem_jobitem on dbo.invoiceitem (jobitemid);
create index IDX_invoiceitem_expenseitem on dbo.invoiceitem (expenseitem);

insert into dbversion values (826)

commit tran