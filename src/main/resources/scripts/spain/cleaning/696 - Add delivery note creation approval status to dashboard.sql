USE [atlas]

BEGIN TRAN

	DECLARE @status int
	SELECT @status = stateid FROM dbo.itemstate WHERE description = 'Awaiting approval for delivery note creation';

	INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
		VALUES (32,@status,'ALLOCATED_SUBDIV');

	INSERT INTO dbversion(version) VALUES (696)

COMMIT TRAN