USE [atlas]
GO

BEGIN TRAN

alter table instrument add  [simplifiedSerialNo] [nvarchar](max);
alter table instrument add  [simplifiedPlantNo] [nvarchar](max);


CREATE NONCLUSTERED INDEX [simpleSerialNo_SimplePlantNo_index] ON [dbo].[instrument]
(
[plantid] ASC
)
INCLUDE ( 	[simplifiedSerialNo],
[simplifiedPlantNo]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

INSERT INTO dbversion(version) VALUES(615);

COMMIT TRAN
