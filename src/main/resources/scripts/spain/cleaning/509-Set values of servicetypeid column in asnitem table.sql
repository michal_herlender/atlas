--Set values of servicetypeid column in asnitem table
USE [atlas]
GO

BEGIN TRAN

 UPDATE 
 dbo.asnitem
 SET servicetypeid=(SELECT servicetypeid FROM dbo.calibrationtype 
 WHERE dbo.asnitem.caltypeid=dbo.calibrationtype.caltypeid)

INSERT INTO dbversion(version) VALUES(509);

COMMIT TRAN