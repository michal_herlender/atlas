USE [spain]

BEGIN TRAN

    create table dbo.jobcostingitemremark (
       id int identity not null,
        costType int,
        remark nvarchar(255) not null,
        remarktype int not null,
        pricingitemid int not null,
        primary key (id)
    );

    create table dbo.defaultpricingremark (
       id int identity not null,
        costType int,
        remark nvarchar(255) not null,
        remarktype int not null,
        locale varchar(255) not null,
        orgid int not null,
        primary key (id)
    );

    alter table dbo.defaultpricingremark 
       add constraint FK_defaultpricingremark_organisation 
       foreign key (orgid) 
       references dbo.company;

    alter table dbo.jobcostingitemremark 
       add constraint FKofgchg33ghmao1e517awgsisw 
       foreign key (pricingitemid) 
       references dbo.jobcostingitem;

	INSERT INTO dbversion(version) VALUES(415);

COMMIT TRAN