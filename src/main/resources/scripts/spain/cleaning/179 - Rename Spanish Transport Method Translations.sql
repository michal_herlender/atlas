-- Script 179 - Renames transport method translations per Zaragoza document
-- GB 2017-10-25

USE [spain]
GO

BEGIN TRAN

-- Client / was Cliente
UPDATE [dbo].[transportmethodtranslation] SET [translation] = 'Sus medios' WHERE locale = 'es_ES' and id = 2;

-- Client Courier / was Transporte del cliente
UPDATE [dbo].[transportmethodtranslation] SET [translation] = 'Portes Debidos' WHERE locale = 'es_ES' and id = 3;

-- Local / was Local
UPDATE [dbo].[transportmethodtranslation] SET [translation] = 'Nuestros Medios' WHERE locale = 'es_ES' and id = 4;

-- Courier Next Day / was Transporte - dia siguente
UPDATE [dbo].[transportmethodtranslation] SET [translation] = 'Portes Pagados � Urgente' WHERE locale = 'es_ES' and id = 5;

-- Courier Specific / was Transporte - especifico
UPDATE [dbo].[transportmethodtranslation] SET [translation] = 'Portes Pagados' WHERE locale = 'es_ES' and id = 6;

COMMIT TRAN

GO


