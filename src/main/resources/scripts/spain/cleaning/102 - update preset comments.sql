BEGIN TRAN

INSERT INTO presetcommentcategorytranslation(id, locale, translation) VALUES
(1, 'en_GB', 'Calibration'),
(2, 'en_GB', 'General'),
(3, 'en_GB', 'Repair'),
(4, 'en_GB', 'Delivery'),
(5, 'en_GB', 'Carriage'),
(6, 'en_GB', 'Fast Track'),
(7, 'en_GB', 'Turnaround'),
(8, 'en_GB', 'Elec Cal Only'),
(9, 'en_GB', 'Advise'),
(10, 'en_GB', 'Quotations'),
(11, 'en_GB', 'Purchase Order'),
(12, 'en_GB', 'Invoicing');

INSERT INTO presetcommentcategorytranslation(id, locale, translation)
SELECT id, 'es_ES', category
FROM presetcommentcategory
WHERE id <= 12;

UPDATE presetcomment SET categoryid = NULL WHERE categoryid > 12;

DELETE FROM presetcommentcategory WHERE id > 12;

COMMIT TRAN