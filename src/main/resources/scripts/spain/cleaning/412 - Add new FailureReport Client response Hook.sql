
USE [spain]
GO

BEGIN TRAN
	
	declare @startstate int
	SELECT @startstate = stateid FROM dbo.itemstate WHERE description = 'Awaiting client response on failure report';
	 
	declare @performedactivity int
	SELECT @performedactivity = stateid FROM dbo.itemstate WHERE description = 'Client response received from failure report';
	 
	INSERT INTO dbo.hook
	(active, alwayspossible, beginactivity, completeactivity, name, activityid)
	VALUES(1, 0, 1, 1, 'failurereport-clientresponse', NULL);
	 
	declare @lastInsertedHook int = IDENT_CURRENT('hook');
	 
	INSERT INTO spain.dbo.hookactivity
	(beginactivity, completeactivity, activityid, hookid, stateid)
	VALUES(1, 1, @performedactivity, @lastInsertedHook, @startstate);

	INSERT INTO dbversion(version) VALUES(412);
	GO

COMMIT TRAN

