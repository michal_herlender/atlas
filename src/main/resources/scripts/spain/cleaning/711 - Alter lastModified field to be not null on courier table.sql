USE [atlas]

BEGIN TRAN
 
	UPDATE dbo.courier SET lastModified = SYSDATETIME();
	
	ALTER TABLE dbo.courier
	ALTER COLUMN lastModified DATETIME2 NOT NULL;

	INSERT INTO dbversion VALUES (711)

COMMIT TRAN