USE [atlas]
GO

BEGIN TRAN

ALTER TABLE purchaseorderstatus ADD statusorder int;
GO

UPDATE purchaseorderstatus set statusorder=1 where name='created';
UPDATE purchaseorderstatus set statusorder=2 where name='awaiting construction';
UPDATE purchaseorderstatus set statusorder=3 where name='constructed';
UPDATE purchaseorderstatus set statusorder=4 where name='awaiting pre-approval';
UPDATE purchaseorderstatus set statusorder=5 where name='pre-approved';
UPDATE purchaseorderstatus set statusorder=6 where name='awaiting approval';
UPDATE purchaseorderstatus set statusorder=7 where name='approved';
UPDATE purchaseorderstatus set statusorder=8 where name='awaiting pre-issue';
UPDATE purchaseorderstatus set statusorder=9 where name='pre-issued';
UPDATE purchaseorderstatus set statusorder=10 where name='awaiting issue';																
UPDATE purchaseorderstatus set statusorder=11 where name='issued' and potype='ACTION';
UPDATE purchaseorderstatus set statusorder=12 where name='issued' and potype='STATUS';
UPDATE purchaseorderstatus set statusorder=13 where name='completed' and potype='ACTION';
UPDATE purchaseorderstatus set statusorder=14 where name='completed' and potype='STATUS';
UPDATE purchaseorderstatus set statusorder=15 where name='cancelled' and potype='ACTION';
UPDATE purchaseorderstatus set statusorder=16 where name='cancelled' and potype='STATUS';

ALTER TABLE purchaseorderstatus alter column statusorder int NOT NULL;


INSERT INTO purchaseorderstatusnametranslation (statusid, locale,translation)
	 VALUES ((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting pre-approval'), 'fr_FR','En attente de pré-validation'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'Pre-approved'), 'fr_FR','Pré-validée'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'Awaiting pre-issue'), 'fr_FR','En attente d’affectation achat'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'Pre-issued'), 'fr_FR','Affectation achat effectuée'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting pre-approval'), 'es_ES','Esperando pre-aprobación'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'Pre-approved'), 'es_ES','Pre-aprobada'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'Awaiting pre-issue'), 'es_ES','Esperando pre-emisión'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'Pre-issued'), 'es_ES','Pre-emitida'), 
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'created'), 'de_DE','Erstellt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting construction'), 'de_DE','Warten auf den Bau'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'constructed'), 'de_DE','Gebaut'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting approval'), 'de_DE','Warten auf Genehmigung'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'approved'), 'de_DE','Genehmigt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting issue'), 'de_DE','Warten auf Ausgabe'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='ACTION'), 'de_DE','Ausgegeben'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='STATUS'), 'de_DE','Ausgegeben'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='ACTION'), 'de_DE','Abgeschlossen'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='STATUS'), 'de_DE','Abgeschlossen'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='ACTION'), 'de_DE','Abgesagt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='STATUS'), 'de_DE','Abgesagt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting pre-approval'), 'de_DE','Warten auf Vorabgenehmigung'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'pre-approved'), 'de_DE','Vorgeprüft'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting pre-issue'), 'de_DE','Warten auf die Vorausgabe'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'pre-issued'), 'de_DE','Vorab ausgestellt');
			

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Created'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'created') AND locale='en_GB' AND [translation]='created';

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Creado'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'created') AND locale='es_ES' AND [translation]='creado';

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Créé'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'created') AND locale='fr_FR' AND [translation]='créé';

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Awaiting construction'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting construction') AND locale='en_GB' AND [translation]='awaiting construction';

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Esperar creación'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting construction') AND locale='es_ES' AND [translation]='esperar creación';

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='En attente de construction'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting construction') AND locale='fr_FR' AND [translation]='en attente de construction';

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Constructed'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'constructed') AND locale='en_GB' AND [translation]='constructed';
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Creada'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'constructed') AND locale='es_ES' AND [translation]='creada';
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Construit'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'constructed') AND locale='fr_FR' AND [translation]='construit';

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Awaiting approval'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting approval') AND locale='en_GB' AND [translation]='awaiting approval' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Esperar la aprobación'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting approval') AND locale='es_ES' AND [translation]='esperar la aprobación' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Attente d''approbation'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting approval') AND locale='fr_FR' AND [translation]='attente d''approbation' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Approved'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'approved') AND locale='en_GB' AND [translation]='approved' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Aprobado'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'approved') AND locale='es_ES' AND [translation]='aprobado' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Approuvé'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'approved') AND locale='fr_FR' AND [translation]='approuvé' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Awaiting issue'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting issue') AND locale='en_GB' AND [translation]='awaiting issue' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Esperando envío'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting issue') AND locale='es_ES' AND [translation]='esperando envío' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Attente d''édition'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting issue') AND locale='fr_FR' AND [translation]='attente d''édition' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Issued'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='ACTION') AND locale='en_GB' AND [translation]='issued' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Enviada'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='ACTION') AND locale='es_ES' AND [translation]='enviada' ;
	 
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Edité'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='ACTION') AND locale='fr_FR' AND [translation]='édité' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Issued'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='STATUS') AND locale='en_GB' AND [translation]='issued' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Enviada'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='STATUS') AND locale='es_ES' AND [translation]='enviada' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Edité'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='STATUS') AND locale='fr_FR' AND [translation]='édité' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Completed'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='ACTION') AND locale='en_GB' AND [translation]='completed' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Terminado'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='ACTION') AND locale='es_ES' AND [translation]='terminado' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Terminée'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='ACTION') AND locale='fr_FR' AND [translation]='terminée' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Completed'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='STATUS') AND locale='en_GB' AND [translation]='completed' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Terminado'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='STATUS') AND locale='es_ES' AND [translation]='terminado' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Terminée'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='STATUS') AND locale='fr_FR' AND [translation]='terminée' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Cancelled'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='ACTION') AND locale='en_GB' AND [translation]='cancelled' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Cancelado'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='ACTION') AND locale='es_ES' AND [translation]='cancelado' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Annulé'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='ACTION') AND locale='fr_FR' AND [translation]='annulé' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Cancelled'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='STATUS') AND locale='en_GB' AND [translation]='cancelled' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Cancelado'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='STATUS') AND locale='es_ES' AND [translation]='cancelado' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Annulé'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='STATUS') AND locale='fr_FR' AND [translation]='annulé' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Awaiting pre-approval'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting pre-approval') AND locale='en_GB' AND [translation]='awaiting pre-approval' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Pre-approved'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'pre-approved') AND locale='en_GB' AND [translation]='pre-approved' ;

UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Awaiting pre-issue'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting pre-issue') AND locale='en_GB' AND [translation]='awaiting pre-issue' ;
	
UPDATE atlas.dbo.purchaseorderstatusnametranslation SET [translation]='Pre-issued'
	WHERE statusid=(select pso.statusid from purchaseorderstatus pso where pso.name = 'pre-issued') AND locale='en_GB' AND [translation]='pre-issued' ;


INSERT INTO atlas.dbo.purchaseorderstatusdescriptiontranslation (statusid,locale,[translation])
	VALUES ((select pso.statusid from purchaseorderstatus pso where pso.name = 'created'),'de_DE','Bestellung erstellt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting construction'),'de_DE','Warten auf den Bau der Bestellung'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'constructed'),'de_DE','Bestellung erstellt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting approval'),'de_DE','Warten auf die Genehmigung des Kontos vor der Ausstellung'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'approved'),'de_DE','Bestellung von Konten genehmigt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting issue'),'de_DE','Warten auf Problem'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='ACTION'),'de_DE','Bestellung ausgestellt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'issued' and potype='STATUS'),'de_DE','Bestellung erteilt, bis Wareneingang & Service erhalten'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='ACTION'),'de_DE','Bestellung erteilt, bis Wareneingang & Service erhalten'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'completed' and potype='STATUS'),'de_DE','Bestellung abgeschlossen'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='ACTION'),'de_DE','Keine Waren & Dienstleistungen erhalten, Bestellung storniert'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'cancelled' and potype='STATUS'),'de_DE','Bestellung storniert'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting pre-approval'),'de_DE','Warten auf Vorabgenehmigung vor Genehmigung'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'pre-approved'),'de_DE','Bestellung vorab genehmigt'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'awaiting pre-issue'),'de_DE','Warten auf Vorausgabe vor Ausgabe'),
			((select pso.statusid from purchaseorderstatus pso where pso.name = 'pre-issued'),'de_DE','Bestellung vorab ausgestellt');

	
INSERT INTO dbversion(version) VALUES (607);

COMMIT TRAN