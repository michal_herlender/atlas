USE [Atlas]
GO

BEGIN TRAN

DROP INDEX [_dta_index_modelrange_7_457768688__K2_K7_K12_3_4_5_6_10_11] ON [dbo].[modelrange]
GO

ALTER TABLE [instrumentrange] ALTER COLUMN [start] float not null;
ALTER TABLE [calibrationrange] ALTER COLUMN [start] float not null;
ALTER TABLE [modelrange] ALTER COLUMN [start] float not null;

INSERT INTO dbversion(version) VALUES(815);

COMMIT TRAN