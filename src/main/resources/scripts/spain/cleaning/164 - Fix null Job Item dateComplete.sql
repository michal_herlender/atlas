-- Script 164 - updates job item completed date
-- There was a bug between approx 2017-06-01 and 2017-06-21 where 
-- the jobitem dateComplete, job dateComplete, job status were not updating via advice / interceptor
-- This just updates the jobitem completed date (job changes to come)

USE [spain]
GO

BEGIN TRAN

UPDATE t1
   SET t1.[datecomplete] = 
			(SELECT TOP 1 jobitemaction.endstamp 
				FROM jobitemaction 
				WHERE jobitemaction.jobitemid = t1.jobitemid 
				ORDER BY jobitemaction.endstamp DESC)
   FROM [jobitem] AS t1
   INNER JOIN itemstate AS t2 on t1.stateid = t2.stateid
 WHERE t1.datecomplete is null and t2.active = 0

GO

COMMIT TRAN


