-- Script 161, run BEFORE startup (otherwise context gets invalid dat)
-- Galen Beck 2017-06-26
-- Deletes unused system default definitions (previously reserved for Plantillas)
-- If the IDs are reused later (see SystemDefaultNames.UNUSED_xx) replacement definitions can be inserted.

USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[systemdefaultnametranslation]
      WHERE defaultid in (30, 31, 32, 33, 34, 35, 36);
GO

DELETE FROM [dbo].[systemdefaultdescriptiontranslation]
      WHERE defaultid in (30, 31, 32, 33, 34, 35, 36);
GO

-- On production these system defaults were completely unused (0 rows deleted)
DELETE FROM [dbo].[systemdefaultapplication]
      WHERE defaultid in (30, 31, 32, 33, 34, 35, 36);
GO

DELETE FROM [dbo].[systemdefaultcorole]
      WHERE defaultid in (30, 31, 32, 33, 34, 35, 36);
GO

DELETE FROM [dbo].[systemdefaultscope]
      WHERE defaultid in (30, 31, 32, 33, 34, 35, 36);
GO

DELETE FROM [dbo].[systemdefault]
      WHERE defaultid in (30, 31, 32, 33, 34, 35, 36);
GO

COMMIT TRAN
