-- Change definitions of M to PM
-- Galen Beck 2019-03-21 -- Didier's / Christophe's request for France

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[servicetype]
   SET [description] = 'Formerly M'
      ,[longname] = 'Preventive Maintenance'
      ,[shortname] = 'PM'
 WHERE servicetypeid = 27;
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'PM'
 WHERE [servicetypeid] = 27 and locale = 'en_GB';
GO

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Preventive maintenance'
 WHERE [servicetypeid] = 27 and locale = 'en_GB';
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'MP'
 WHERE [servicetypeid] = 27 and locale = 'fr_FR';
GO

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Maintenance préventive'
 WHERE [servicetypeid] = 27 and locale = 'fr_FR';
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VW'
 WHERE [servicetypeid] = 27 and locale = 'de_DE';
GO

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Reparatur & Wartung - Vorbeugende Wartung'
 WHERE [servicetypeid] = 27 and locale = 'de_DE';
GO

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'MP'
 WHERE [servicetypeid] = 27 and locale = 'es_ES';
GO

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Mantenimiento preventivo'
 WHERE [servicetypeid] = 27 and locale = 'es_ES';
GO

INSERT INTO dbversion(version) VALUES(418);

COMMIT TRAN