USE [atlas]
GO

BEGIN TRAN

declare @status int;

select @status = ist.stateid from itemstate ist where ist.description like 'Awaiting third party certificate';

INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (62,@status,'ALLOCATED_SUBDIV');

INSERT INTO dbversion(version) VALUES(560);

COMMIT TRAN