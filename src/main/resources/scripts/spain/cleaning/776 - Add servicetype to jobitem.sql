-- As part of the data export APIs, it's required to provide the service type for each job item
-- This prevents having to refer to / export the calibration types
-- The application has been updated to always set the service type, when the calibration type is set.
-- As a second phase we can remove the calibration type from job item once all "get" references are removed.


USE [atlas]
GO

BEGIN TRAN

-- There seem to be three orphaned job items with all null fields on production (old bugs?)
-- These can be removed...
DELETE FROM [dbo].[jobitem]
      WHERE jobid is null
GO

alter table dbo.jobitem 
    add servicetypeid int;

alter table dbo.jobitem 
    add constraint FK_jobitem_servicetypeid 
    foreign key (servicetypeid) 
    references dbo.servicetype;

GO

update ji
    set ji.servicetypeid = ct.servicetypeid
	from
	dbo.jobitem ji
	join
	calibrationtype ct on ji.caltypeid = ct.caltypeid
GO

alter table dbo.jobitem 
    alter column servicetypeid int not null;

insert into dbversion(version) values(776);

COMMIT TRAN