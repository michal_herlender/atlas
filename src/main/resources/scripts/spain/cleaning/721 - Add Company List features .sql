USE [atlas]

BEGIN TRAN

	-- Add new permissions for searching Company Lists, deleting a company list member
	-- and company list usage
	INSERT INTO dbo.permission (groupId, permission) VALUES 
	(1, 'COMPANY_LIST_SEARCH'),
	(1, 'COMPANY_LIST_MEMBER_DELETE'),
	(1, 'COMPANY_LIST_USAGE_DELETE')
	
	-- Create the new table companylist
	CREATE TABLE dbo.companylist (
        id INT IDENTITY NOT NULL PRIMARY KEY,
        name VARCHAR(50) NOT NULL,
		orgid INT NOT NULL,
		active BIT NOT NULL,
		lastModified DATETIME2 NOT NULL,
        lastModifiedBy INT NOT NULL,
		CONSTRAINT FK_companylist_company FOREIGN KEY (orgid) REFERENCES dbo.company(coid),
		CONSTRAINT FK_companylist_contact FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid)
    );

	-- Create the new table companylistmember
	CREATE TABLE dbo.companylistmember (
        id INT IDENTITY NOT NULL PRIMARY KEY,
		companylistid INT NOT NULL,
		active BIT NOT NULL,
		coid INT NOT NULL,
		lastModified DATETIME2 NOT NULL,
        lastModifiedBy INT NOT NULL,
		CONSTRAINT FK_companylistmember_contact FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid),
		CONSTRAINT FK_companylistmember_companylist FOREIGN KEY (companylistid) REFERENCES dbo.companylist(id),
		CONSTRAINT FK_companylistmember_company FOREIGN KEY (coid) REFERENCES dbo.company(coid)
    );

	-- Create the new table companylistusage
	CREATE TABLE dbo.companylistusage (
        id INT IDENTITY NOT NULL PRIMARY KEY,
		lastModified DATETIME2 NOT NULL,
        lastModifiedBy INT NOT NULL,
		companylistid INT NOT NULL,
		coid INT NOT NULL,
		CONSTRAINT FK_companylistusage_contact FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid),
		CONSTRAINT FK_companylistusage_companylist FOREIGN KEY (companylistid) REFERENCES dbo.companylist(id),
		CONSTRAINT FK_companylistusage_company FOREIGN KEY (coid) REFERENCES dbo.company(coid)
    );

    
	-- Add the filed requiresuppliercompanylist to companysettingsforallocatedcompany table
	ALTER TABLE dbo.companysettingsforallocatedcompany ADD requiresuppliercompanylist BIT DEFAULT 0 NOT NULL;
	GO
	
	INSERT INTO dbversion(version) VALUES(721);

COMMIT TRAN 