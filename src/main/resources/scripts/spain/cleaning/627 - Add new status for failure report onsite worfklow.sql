USE [atlas]
GO

BEGIN TRAN

DECLARE @newStatusId int, @lookupId int, @outcomeStatusId int;

INSERT INTO dbo.itemstate ([type],active,description,retired)
	VALUES ('workstatus',1,'Onsite � Awaiting CSR decision',0);
select @newStatusId = max(iss.stateid) from itemstate iss;

INSERT INTO dbo.itemstatetranslation (locale,[translation],stateid)
	VALUES ('en_GB','Onsite � Awaiting CSR decision',@newStatusId);
INSERT INTO dbo.itemstatetranslation (locale,[translation],stateid)
	VALUES ('fr_FR','Sur site - En attente d''une d�cision CRC',@newStatusId);
INSERT INTO dbo.itemstatetranslation (locale,[translation],stateid)
	VALUES ('es_ES','En el sitio: en espera de la decisi�n de CSR',@newStatusId);
INSERT INTO dbo.itemstatetranslation (locale,[translation],stateid)
	VALUES ('de_DE','Vor Ort - Warten auf CSR-Entscheidung',@newStatusId);
	
select @lookupId = id from lookupinstance li 
	where li.lookup = 'Lookup_LatestFailureReportOutcome (for onsite)';
	
INSERT INTO dbo.outcomestatus (defaultoutcome,statusid)
	VALUES (0, @newStatusId);
select @outcomeStatusId = max(id) from outcomestatus;

INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lookupId, @outcomeStatusId,1);
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@lookupId, @outcomeStatusId,3);

INSERT INTO dbo.stategrouplink (groupid,stateid,[type])
	VALUES (42, @newStatusId, 'ALLOCATED_SUBDIV');


INSERT INTO dbversion VALUES (627)

COMMIT TRAN