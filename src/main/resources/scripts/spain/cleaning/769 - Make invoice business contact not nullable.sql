-- A couple invoices (or less) on each system have no business contact set, updating and adding constraint
USE [atlas]
GO

BEGIN TRAN

UPDATE [dbo].[invoice]
   SET [businesscontactid] = [createdby]
 WHERE [businesscontactid] IS NULL;
GO

ALTER TABLE [dbo].[invoice] ALTER COLUMN [businesscontactid] int NOT NULL;
GO

insert into dbversion(version) values(769);

 COMMIT TRAN