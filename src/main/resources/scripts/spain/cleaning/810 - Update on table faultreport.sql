USE [atlas]
GO

BEGIN TRAN

alter table dbo.faultreport
    ALTER COLUMN clientcomments nvarchar(500);
    
    insert into dbversion(version) values(810);

COMMIT TRAN