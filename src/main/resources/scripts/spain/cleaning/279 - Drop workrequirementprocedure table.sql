-- Script 279 - Removed workrequirementprocedure table (no longer used)
-- Corrects issue with foreign key preventing work requirement deletion

USE [spain]
GO

BEGIN TRAN

/****** Object:  Table [dbo].[workrequirementprocedure]    Script Date: 7/9/2018 12:10:02 PM ******/
DROP TABLE [dbo].[workrequirementprocedure]
GO

INSERT INTO dbversion(version) VALUES(279);

COMMIT TRAN