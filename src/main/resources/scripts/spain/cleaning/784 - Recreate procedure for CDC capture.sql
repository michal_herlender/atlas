-- It seems in script 782 that the 'CREATE TABLE' got included in the stored procedure definition
-- (maybe multiple statements were assembled in the script 782, after testing individually?)
-- so here am dropping / recreating the stored procedure, where it can then be the only / final statement in the batch

USE [atlas]
GO

BEGIN TRAN

insert into dbversion(version) values(784);
GO

-- Insert for EngineerAllocation
INSERT INTO dbo.processed_changeentity(changeentity,lastprocessedlsn)
VALUES (19, NULL);

DROP PROCEDURE IF EXISTS [dbo].[getAllCdcChangeFromLastProcessedLsn]
GO

-- Stored Procedure getAllCdcChangeFromLastProcessedLsn
CREATE PROCEDURE getAllCdcChangeFromLastProcessedLsn
    @lastprocessedlsn BINARY(10),
    @table_name VARCHAR(50) 
AS   
BEGIN
	DECLARE @function_name VARCHAR(100), @sql_command VARCHAR(200), @op_type varchar(50), 
	@begin_lsn_str VARCHAR(100), @end_lsn_str VARCHAR(100), @from binary(10), @to binary(10), 
	@query_from nvarchar(max), @query_to nvarchar(max),@query_exist nvarchar(max),@exist BINARY(10)

	SELECT @function_name = 'cdc.fn_cdc_get_all_changes_'+@table_name
	SELECT @op_type = 'all'
	SELECT @query_from = 'SELECT @from = MIN(__$start_lsn) FROM  cdc.'+@table_name+'_CT'
	EXEC sp_executesql @query_from, N'@from binary(10) out', @from out
	SELECT @query_to = 'SELECT @to = MAX(__$start_lsn) FROM  cdc.'+@table_name+'_CT'
	EXEC sp_executesql @query_to, N'@to binary(10) out', @to out
	SELECT @query_exist = 'SELECT @exist = __$start_lsn FROM  cdc.'+@table_name+'_CT WHERE __$start_lsn = '+CONVERT(VARCHAR(100), @lastprocessedlsn, 1)
	EXEC sp_executesql @query_exist, N'@exist binary(10) out', @exist out
	SELECT @begin_lsn_str = CASE 
       WHEN @exist IS NULL THEN CONVERT(VARCHAR(100), @from, 1)
       ELSE CONVERT(VARCHAR(100), @lastprocessedlsn, 1)
	   END
	SELECT @end_lsn_str = CONVERT(VARCHAR(100), @to, 1)
	SELECT @sql_command = 'SELECT DISTINCT __$start_lsn FROM ' + @function_name+'('+@begin_lsn_str+','+@end_lsn_str+','''+@op_type+''')'
	EXEC (@sql_command) 
END
GO

COMMIT TRAN