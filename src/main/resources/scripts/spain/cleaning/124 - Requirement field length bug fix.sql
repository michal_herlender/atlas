-- Requirement now used for showing QuoteItemNote on job item, needs to be 2000 (to match note)
-- Big notes were failing at persist time, previously 500 characters
-- Added separate field for label on Requirement due to 2000 char limit for integration tests
-- Galen Beck - 2017-03-17

USE [spain]
GO

BEGIN TRAN

DROP INDEX _dta_index_requirement_7_1727345218__K10_K1_K8_2_3_4_5_6_7_9 ON [requirement]

ALTER TABLE [requirement] ALTER COLUMN [requirement] NVARCHAR(2000);

COMMIT TRAN;