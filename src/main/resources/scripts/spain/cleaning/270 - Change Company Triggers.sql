USE [spain]
GO
/****** Object:  Trigger [dbo].[log_update_company]    Script Date: 15.06.2018 14:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 ALTER TRIGGER [dbo].[log_update_company] ON [dbo].[company] AFTER UPDATE AS BEGIN  IF EXISTS (SELECT * FROM audittriggerswitch WHERE enabled = 1) BEGIN  insert into LOG_company (coid, active, companyref, coname, createdate, deactreason, deacttime, onstop, onstopsince, partnercomp, rate, accountid, accountaddressid, accountpersonid, businessareaid, corole, countryid, createdby, currencyid, defsubdivid, onstopby, vatcode, onstopreason, log_validfrom, log_validto, log_userid, log_actiontype) SELECT coid, active, companyref, coname, createdate, deactreason, deacttime, onstop, onstopsince, partnercomp, rate, accountid, accountaddressid, accountpersonid, businessareaid, corole, countryid, createdby, currencyid, defsubdivid, onstopby, vatcode, onstopreason, case when log_lastmodified is null then (dbo.unix_timestamp(GETUTCDATE())*1000) else log_lastmodified END, case when (dbo.unix_timestamp(GETUTCDATE())*1000) <= log_lastmodified then (log_lastmodified + 1) else (dbo.unix_timestamp(GETUTCDATE())*1000) END, case when log_userid is null then 1 else log_userid END, 'UPDATE' FROM deleted END END;

GO
/****** Object:  Trigger [dbo].[log_delete_company]    Script Date: 15.06.2018 14:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 ALTER TRIGGER [dbo].[log_delete_company] ON [dbo].[company] AFTER DELETE AS BEGIN  IF EXISTS (SELECT * FROM audittriggerswitch WHERE enabled = 1) BEGIN  insert into LOG_company (coid, active, companyref, coname, createdate, deactreason, deacttime, onstop, onstopsince, partnercomp, rate, accountid, accountaddressid, accountpersonid, businessareaid, corole, countryid, createdby, currencyid, defsubdivid, onstopby, vatcode, onstopreason, log_validfrom, log_validto, log_userid, log_actiontype) SELECT coid, active, companyref, coname, createdate, deactreason, deacttime, onstop, onstopsince, partnercomp, rate, accountid, accountaddressid, accountpersonid, businessareaid, corole, countryid, createdby, currencyid, defsubdivid, onstopby, vatcode, onstopreason, case when log_lastmodified is null then (dbo.unix_timestamp(GETUTCDATE())*1000) else log_lastmodified END, case when (dbo.unix_timestamp(GETUTCDATE())*1000) <= log_lastmodified then (log_lastmodified + 1) else (dbo.unix_timestamp(GETUTCDATE())*1000) END, case when log_userid is null then 1 else log_userid END, 'DELETE' FROM deleted END END;
 
INSERT INTO dbversion(version) VALUES(270);
