-- Galen Beck 2018-08-31

-- Script to manage fault report as a bona fide document (in the jobs hierarchy)
-- Note, after merge we could likely eliminate some deprecated fields (email related)

USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[systemcomponent]
           ([appendtoparentpath]
           ,[component]
           ,[componentname]
           ,[defaultemailbodyreference]
           ,[defaultemailsubject]
           ,[foldersdropboundaries]
           ,[foldersperitem]
           ,[folderssplit]
           ,[folderssplitrange]
           ,[foldersyear]
           ,[numberedsubfolderdivider]
           ,[numberedsubfolders]
           ,[prependtoid]
           ,[rootdir]
           ,[usingparentsettings]
           ,[parentid]
           ,[emailtemplates])
     VALUES
           ('Failure Reports'
           ,25
           ,'Failure Report'
           ,null
           ,null
           ,0
           ,0
           ,0
           ,0
           ,0
           ,null
           ,0
           ,null
           ,'Failure Reports'
           ,1
           ,1
           ,1);
GO

INSERT INTO [dbo].[systemsubdirectory]
           ([dirname]
           ,[componentid])
     VALUES
           ('Failure Reports'
           ,1)
GO

INSERT INTO dbversion(version) VALUES(302);

COMMIT TRAN
