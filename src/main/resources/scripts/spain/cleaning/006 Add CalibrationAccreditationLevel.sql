-- Author Galen Beck 2015-01-18
-- Adds CalibrationAccreditationLevel values for Madrid(6644), Bilbao(6643), and Vitoria(6642) subdivs into system
-- Presumes that Cal Types for Madrid (6645) are already present

BEGIN TRAN

USE [spain];
GO

INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'APPROVE', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 1 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'APPROVE', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 2 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'APPROVE', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 3 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'PERFORM', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 1 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'REMOVED', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 1 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'REMOVED', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 2 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'REMOVED', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 3 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SIGN', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 2 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SIGN', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 3 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SUPERVISED', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 1 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SUPERVISED', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 2 AND [orgid] in (6644, 6643, 6642);
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SUPERVISED', [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 3 AND [orgid] in (6644, 6643, 6642);

GO

COMMIT TRAN