-- Initializes boolean field for quotation discount display on documents

USE [spain]
GO

BEGIN TRAN

alter table dbo.quotation 
    add doc_discounts tinyint default 0;

GO

update dbo.quotation
    set doc_discounts = 0 where doc_discounts is null;

GO

alter table dbo.quotation 
    alter column doc_discounts tinyint not null;

INSERT INTO dbversion(version) VALUES(328);

COMMIT TRAN
