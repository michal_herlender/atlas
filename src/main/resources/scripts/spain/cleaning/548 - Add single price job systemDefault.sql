USE [atlas]

BEGIN TRAN

-- Group ID may vary between development installations
DECLARE @groupid INT;
SELECT @groupid = [id] FROM [dbo].[systemdefaultgroup] WHERE [dname] = 'Business - Default service type';

-- System Defaults have enum ordinal mapping, so new enum must always be ID 55
DECLARE @new_sd INT;
SELECT @new_sd = 55;

SET IDENTITY_INSERT [systemdefault] ON; 

INSERT INTO dbo.systemdefault(defaultid, defaultdatatype, defaultdescription, defaultname, defaultvalue, endd, [start], groupid, groupwide)
VALUES(@new_sd, 'other', 'Defines the default service type of new jobitems for single price jobs', 
'Default service type for new single price jobitems', 'AC-MVO-IL', NULL, NULL, @groupid, 1);

SET IDENTITY_INSERT [systemdefault] OFF; 

INSERT INTO dbo.systemdefaultnametranslation
(defaultid, locale, [translation])
VALUES(@new_sd, 'fr_FR', 'Type de service par défaut des nouveaux Jobs prix unique'),
      (@new_sd, 'en_GB', 'Default service type of new single price jobs'),
	  (@new_sd, 'es_ES', 'Tipo de servicio predeterminado de nuevos trabajos de precio único'),
	  (@new_sd, 'de_DE', 'Standardservicetyp für neue Einzelpreisjobs');

INSERT INTO dbversion(version) VALUES(548);

COMMIT TRAN