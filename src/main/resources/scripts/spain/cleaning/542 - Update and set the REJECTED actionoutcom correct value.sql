--Update and set the REJECTED actionoutcom correct value
--detected on french test server i don't think if it is the same case on other system

USE [atlas]
GO

BEGIN TRAN

DECLARE @actionoutcome_78 INT,
		@activity_148 INT;

SELECT @activity_148 = stateid FROM itemstate WHERE description = 'Client decision received on exchange';

SELECT @actionoutcome_78 = id FROM dbo.actionoutcome WHERE value = 'APPROVED' AND activityid = @activity_148 AND positiveoutcome = 0;
UPDATE dbo.actionoutcome SET value = 'REJECTED' WHERE id = @actionoutcome_78;

INSERT INTO dbversion(version) VALUES(542);

COMMIT TRAN