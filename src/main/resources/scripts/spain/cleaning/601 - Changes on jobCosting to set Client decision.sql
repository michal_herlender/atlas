
USE [atlas]
GO
BEGIN TRAN

DECLARE @activity_clientresponse INT,
		@status_clientresponse INT;

SELECT @activity_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Client response received';
SELECT @status_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Costing issued - awaiting client approval';

UPDATE dbo.nextactivity SET manualentryallowed = 0 WHERE statusid = @status_clientresponse AND activityid = @activity_clientresponse;
	 
INSERT INTO dbo.hook(active, alwayspossible, beginactivity, completeactivity, name, activityid)
VALUES(1, 0, 1, 1, 'job-costing-client-decision', @activity_clientresponse);
	 
DECLARE @lastInsertedHook INT = IDENT_CURRENT('hook');
	 
INSERT INTO dbo.hookactivity(beginactivity, completeactivity, activityid, hookid, stateid)
VALUES(1, 1, @activity_clientresponse, @lastInsertedHook, @status_clientresponse);

ALTER TABLE dbo.jobcosting ADD clientapprovalon DATETIME2 NULL;
ALTER TABLE dbo.jobcosting ADD clientapprovalcomment VARCHAR(2000) NULL;
ALTER TABLE dbo.jobcosting ADD clientapproval bit NULL;
ALTER TABLE dbo.jobcosting ADD statusupdatedon DATETIME2 NULL;

INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
VALUES ('jobcosting',0,'Accepted',1,0,0,0,0,0);
DECLARE @lastInsertedjcstatus_1 INT = IDENT_CURRENT('basestatus');
INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@lastInsertedjcstatus_1,'en_GB','Accepted'),
	   (@lastInsertedjcstatus_1,'fr_FR','Accepté'),
	   (@lastInsertedjcstatus_1,'es_ES','Aceptado'),
       (@lastInsertedjcstatus_1,'de_DE','Akzeptiert');

INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
VALUES ('jobcosting',0,'Rejected',0,0,0,0,0,1);
DECLARE @lastInsertedjcstatus_2 INT = IDENT_CURRENT('basestatus');
INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@lastInsertedjcstatus_2,'en_GB','Rejected'),
	   (@lastInsertedjcstatus_2,'fr_FR','Rejeté'),
	   (@lastInsertedjcstatus_2,'es_ES','Rechazado'),
       (@lastInsertedjcstatus_2,'de_DE','Abgelehnt');

INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
VALUES ('jobcosting',0,'Rejected - Redo',0,0,0,0,0,1);
DECLARE @lastInsertedjcstatus_3 INT = IDENT_CURRENT('basestatus');
INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@lastInsertedjcstatus_3,'en_GB','Rejected - Redo'),
	   (@lastInsertedjcstatus_3,'fr_FR','Rejeté - Rétablir'),
	   (@lastInsertedjcstatus_3,'es_ES','Rechazado - Rehacer'),
       (@lastInsertedjcstatus_3,'de_DE','Abgelehnt - Wiederherstellen');

INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
VALUES ('jobcosting',0,'Validated',0,0,0,0,1,0);
DECLARE @lastInsertedjcstatus_4 INT = IDENT_CURRENT('basestatus');
INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@lastInsertedjcstatus_4,'en_GB','Validated'),
	   (@lastInsertedjcstatus_4,'fr_FR','Validé'),
	   (@lastInsertedjcstatus_4,'es_ES','Validado'),
       (@lastInsertedjcstatus_4,'de_DE','Bestätigt');

INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
VALUES ('jobcosting',0,'Cancelled',0,0,0,0,1,0);
DECLARE @lastInsertedjcstatus_5 INT = IDENT_CURRENT('basestatus');
INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@lastInsertedjcstatus_5,'en_GB','Cancelled'),
	   (@lastInsertedjcstatus_5,'fr_FR','Annulé'),
	   (@lastInsertedjcstatus_5,'es_ES','Cancelado'),
       (@lastInsertedjcstatus_5,'de_DE','Abgesagt');

INSERT INTO dbo.basestatus ([type],defaultstatus,name,accepted,followingissue,followingreceipt,issued,requiringattention,rejected)
VALUES ('jobcosting',0,'On-Hold',0,0,0,0,1,0);
DECLARE @lastInsertedjcstatus_6 INT = IDENT_CURRENT('basestatus');
INSERT INTO dbo.basestatusnametranslation (statusid,locale,[translation])
VALUES (@lastInsertedjcstatus_6,'en_GB','On-Hold'),
	   (@lastInsertedjcstatus_6,'fr_FR','En attente'),
	   (@lastInsertedjcstatus_6,'es_ES','En espera'),
       (@lastInsertedjcstatus_6,'de_DE','In Wartestellung');

INSERT INTO dbversion(version) VALUES (601);

COMMIT TRAN