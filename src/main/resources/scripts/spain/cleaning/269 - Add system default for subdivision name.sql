-- Inserts system default 36 (reusing an old unused definition) for address formatting
-- Author Galen Beck

USE [spain];
GO

BEGIN TRAN

DELETE FROM [systemdefaultcorole] WHERE defaultid = 36;
GO

DELETE FROM [systemdefaultnametranslation] WHERE defaultid = 36;
GO

DELETE FROM [systemdefaultdescriptiontranslation] WHERE [defaultid] = 36;
GO

DELETE FROM [systemdefaultscope] WHERE defaultid = 36;
GO

DELETE FROM [systemdefault] WHERE [defaultid] = 36;
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
	(36, 'boolean', 'Prints the subdivision name on formatted addresses', 'Print Subdivision Name on Address', 'false', 0, 0, 1, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (36, 'en_GB', 'Prints the subdivision name on formatted addresses'),
  (36, 'es_ES', 'Imprime el nombre de la subdivisi�n en direcciones formateadas'),
  (36, 'fr_FR', 'Imprime le nom de la subdivision sur les adresses format�es'),
  (36, 'de_DE', 'Druckt den Unterteilungsnamen auf formatierten Adressen');

-- Applies to subdivisions (2) and companies (0)

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0, 36), (2, 36);
GO

-- Applies to all company roles

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (1, 36), (2, 36), (3, 36), (4, 36), (5, 36);

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (36, 'en_GB', 'Print Subdivision Name on Address'),
  (36, 'es_ES', 'Imprimir el nombre de la subdivisi�n en la direcci�n'),
  (36, 'fr_FR', 'Imprimer le nom de la subdivision sur l''adresse'),
  (36, 'de_DE', 'Unterteilungsname auf Adresse drucken');
GO

INSERT INTO dbversion(version) VALUES(269);

COMMIT TRAN