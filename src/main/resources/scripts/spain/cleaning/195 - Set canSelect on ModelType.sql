/*
  Script 195 - Sets canselect bit on modeltype (indicating that model type is used on instruments) and makes not nullable
  Should be run before application startup.  Previously some values were null.
*/

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[modeltype] SET [canselect] = 1;
UPDATE [dbo].[modeltype] SET [canselect] = 0 WHERE [modeltypename] in ('Sales category', 'Capability', 'Service');
ALTER TABLE [dbo].[modeltype] ALTER COLUMN [canselect] bit not null;

INSERT INTO dbversion (version) VALUES (195);

COMMIT TRAN

GO