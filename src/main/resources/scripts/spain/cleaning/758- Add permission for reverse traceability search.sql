USE [atlas]
GO

BEGIN TRAN

	DECLARE @groupIDUnreleaedFeatures INT;
	
	SELECT @groupIDUnreleaedFeatures = [id] FROM [dbo].[permissiongroup] WHERE name = 'GROUP_UNRELEASED_FEATURES';
	INSERT INTO dbo.permission(groupId, permission)
	VALUES (@groupIDUnreleaedFeatures ,'REVERCE_TRACEABILITY_SEARCH');

	INSERT INTO dbversion(version) VALUES(758);

	GO

COMMIT TRAN