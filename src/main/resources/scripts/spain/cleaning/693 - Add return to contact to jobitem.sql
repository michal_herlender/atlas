USE [atlas]
GO

BEGIN TRAN

ALTER TABLE jobitem ADD returnToContact int NULL;

GO

ALTER TABLE dbo.jobitem ADD CONSTRAINT FK_contact_personid FOREIGN KEY (returnToContact) REFERENCES atlas.dbo.contact(personid);


GO

INSERT INTO dbversion(version) VALUES(693);

COMMIT TRAN