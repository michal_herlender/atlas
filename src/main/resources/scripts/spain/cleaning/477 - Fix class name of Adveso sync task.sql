-- Updates class name of renamed scheduled task for Adveso services
-- It seems 473 didn't have the current class name (also fixed spelling of Synchronizer)

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.core.external.adveso.schedule.AdvesoSynchronizerScheduledJob'
 WHERE [taskname] = 'Synchronize JobItem Activities To Adveso'
GO

INSERT INTO dbversion(version) VALUES(477);

COMMIT TRAN

