BEGIN TRAN;

UPDATE instrumentusagetype SET name='Use only as indicator' WHERE name='Indication Only';

UPDATE instrumentusagetypenametranslation set translation = 'Use Only as Indicator' WHERE translation = 'Indication Only';

UPDATE instrumentusagetypenametranslation SET translation = 'Usar solo como indicador' WHERE translation = 'Indicación solamente';

UPDATE instrumentusagetypenametranslation SET translation = 'A n’utiliser qu’en tant qu’indicateur' WHERE translation ='Indication seulement';

UPDATE instrumentusagetypenametranslation SET translation = 'A vérifier avant utilisation' WHERE translation = 'Vérifier avant utilisation';

UPDATE instrumentusagetypenametranslation SET translation = 'A garder en vérification' WHERE translation = 'Gardez à l''étalonnage';

INSERT INTO instrumentusagetype (name, fulldescription, recall,defaulttype)
VALUES ('Available','',1,0),
  ('Not Shipped','',0,0),
  ('Out Of Service','',0,0),
  ('Lost','',0,0),
  ('Inactive','',0,0),
  ('Use With Limitations','',1,0),
  ('In Maintenance','',0,0),
  ('In Repair','',0,0),
  ('Assign To Superior Group','',0,0);

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
    VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Available'),
            'en_GB',
            'Available');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Available'),
        'es_ES',
        'Disponible');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Available'),
        'fr_FR',
        'Disponible');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Not Shipped'),
        'en_GB',
        'Not Shipped');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Not Shipped'),
        'es_ES',
        'No Enviado');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Not Shipped'),
        'fr_FR',
        'Pas livré');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Out Of Service'),
        'en_GB',
        'Out Of Service');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Out Of Service'),
        'es_ES',
        'Fuera de Servicio');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Out Of Service'),
        'fr_FR',
        'Hors service');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Lost'),
        'en_GB',
        'Lost');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Lost'),
        'es_ES',
        'Perdido');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Lost'),
        'fr_FR',
        'Perdu');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Inactive'),
        'en_GB',
        'Inactive');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Inactive'),
        'es_ES',
        'Inactivo');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Inactive'),
        'fr_FR',
        'Inactif');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Use With Limitations'),
        'en_GB',
        'Use With Limitations');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Use With Limitations'),
        'es_ES',
        'Usable con limitaciones');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Use With Limitations'),
        'fr_FR',
        'A utiliser avec restrictions');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'In Maintenance'),
        'en_GB',
        'In Maintenance');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'In Maintenance'),
        'es_ES',
        'En Mantenimiento');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'In Maintenance'),
        'fr_FR',
        'En maintenance');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'In Repair'),
        'en_GB',
        'In Repair');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'In Repair'),
        'es_ES',
        'En reparación');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'In Repair'),
        'fr_FR',
        'En réparation');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Assign To Superior Group'),
        'en_GB',
        'Assign To Superior Group');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Assign To Superior Group'),
        'es_ES',
        'Asignado a grupo superior');

INSERT INTO instrumentusagetypenametranslation (typeid,locale,translation)
VALUES ((SELECT instrumentusagetype.typeid FROM instrumentusagetype WHERE name = 'Assign To Superior Group'),
        'fr_FR',
        'A affecter à un groupe supérieur');

COMMIT TRAN;