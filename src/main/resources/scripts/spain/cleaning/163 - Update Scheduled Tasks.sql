-- Script 163 - Galen Beck - 2017-06-30
-- Run before startup to prevent old failed queued reports from being resent now that it works
-- Renaming of some scheduled task classes in database to correspond to application

USE [spain]
GO

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.core.recall.generator.RecallSenderServiceImpl'
 WHERE [classname] = 'org.trescal.cwms.core.recall.generator.RecallSenderImpl'
GO

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.core.tools.autoservice.AutoClientServiceImpl'
 WHERE [classname] = 'org.trescal.cwms.core.tools.autoservice.AutoServiceClientImpl'
GO

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.core.tools.autoservice.AutoSupplierServiceImpl'
 WHERE [classname] = 'org.trescal.cwms.core.tools.autoservice.AutoServiceSupplierImpl'
GO

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatSchedulerServiceImpl'
 WHERE [classname] = 'org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatScheduler'
GO

UPDATE [dbo].[scheduledtask]
   SET [classname] = 'org.trescal.cwms.core.recall.tool.GenerateRecallService'
 WHERE [classname] = 'org.trescal.cwms.core.recall.tool.GenerateRecall'
GO

-- Set old queued reports (previously, they've all failed) to inactve status
UPDATE [dbo].[queuedreport]
   SET [active] = 0
GO


