-- Author Tony Provost 2016-01-28
-- Populate table invoicefrequency with default records (table for finance)

BEGIN TRAN 

USE [spain];
GO

SET IDENTITY_INSERT [paymentterms] ON
GO
  
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (1, '30', '30 days net')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (2, '30EOM', '30 days end of month')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (3, 'EOM30', 'End of month 30 days')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (4, '30EOM10', '30 days end of month the 10th')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (5, '45', '45 days net')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (6, '45EOM', '45 days end of month')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (7, 'EOM45', 'End of months 45 days')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (8, '60', '60 days net')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (9, '85', '85 days net')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (10, '90', '90 days net')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (11, '120', '120 days net')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (12, '150', '150 days net')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (13, '180', '180 days net')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (14, '0', '0 day')
INSERT INTO [paymentterms] ([id], [code], [description]) VALUES (15, 'NM25', 'Next month the 25th')
GO

-- English Translation
INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) SELECT [id], 'en_GB', [description] FROM [paymentterms] WHERE [id] NOT IN (SELECT [paymenttermsid] FROM [paymenttermstranslation] WHERE [locale] = 'en_GB') AND [description] <> ''
GO

-- Spanish Translation
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 1 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (1, 'es_ES', '30 d�as netos')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 2 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (2, 'es_ES', '30 d�as al final de mes')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 3 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (3, 'es_ES', 'Fin de mes 30 d�as')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 4 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (4, 'es_ES', '30 d�as fin de mes pago el d�a 10')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 5 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (5, 'es_ES', '45 d�as netos')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 6 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (6, 'es_ES', '45 d�as fin de mes')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 7 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (7, 'es_ES', 'Fin de mes 45 d�as')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 8 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (8, 'es_ES', '60 d�as netos')		
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 9 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (9, 'es_ES', '85 d�as netos')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 10 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (10, 'es_ES', '90 d�as netos')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 11 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (11, 'es_ES', '120 d�as netos')
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 12 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (12, 'es_ES', '150 d�as netos')
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 13 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (13, 'es_ES', '180 d�as netos')
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 14 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (14, 'es_ES', '0 d�a')	
IF NOT EXISTS (SELECT 1 FROM [paymenttermstranslation] WHERE [paymenttermsid] = 15 AND locale = 'es_ES') 
	INSERT INTO [paymenttermstranslation] ([paymenttermsid], [locale], [translation]) VALUES (15, 'es_ES', 'Mes siguiente el di� 25')		
GO

SET IDENTITY_INSERT [paymentterms] OFF
GO

COMMIT TRAN
GO


