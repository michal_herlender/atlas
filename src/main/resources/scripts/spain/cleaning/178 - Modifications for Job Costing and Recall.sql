-- Database update script, extracted from script-update.sql for 
-- GB 2017-10-25

USE [spain];

BEGIN TRAN

    create table dbo.costs_service (
        costid int identity not null,
        lastModified datetime2,
        active tinyint not null,
        autoset tinyint not null,
        costtypeid int not null,
        customorderby int,
        discountrate numeric(5,2) not null,
        discountvalue numeric(10,2) not null,
        finalcost numeric(10,2) not null,
        hourlyrate numeric(10,2),
        labourcost numeric(10,2),
        labourtime int,
        totalcost numeric(10,2) not null,
        lastModifiedBy int,
        primary key (costid)
    );

    create table dbo.jobcostingexpenseitem (
        id int identity not null,
        lastModified datetime2,
        finalcost numeric(10,2) not null,
        discountrate numeric(5,2) not null,
        discountvalue numeric(10,2) not null,
        itemno int not null,
        partofbaseunit bit not null,
        quantity int not null,
        totalcost numeric(10,2) not null,
        lastModifiedBy int,
        jobCosting int not null,
        jobExpenseItem int not null,
        serviceCost int,
        primary key (id)
    );

    alter table dbo.jobexpenseitem 
        add model int;

    alter table dbo.recallrule 
        add calculateFromCert bit;

    alter table dbo.recallrule 
        add calibrateOn datetime2;

    alter table dbo.recallrule 
        add updateInstrument bit;

    alter table dbo.costs_service 
        add constraint FKcfu1k9yt1tsqkmx6mk1ho6mnt 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobcostingexpenseitem 
        add constraint FKasdmsqyrtjwrstx681ak9714n 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.jobcostingexpenseitem 
        add constraint FK_jobcostingexpenseitem_jobCosting 
        foreign key (jobCosting) 
        references dbo.jobcosting;

    alter table dbo.jobcostingexpenseitem 
        add constraint FK_jobcostingexpenseitem_jobExpenseItem 
        foreign key (jobExpenseItem) 
        references dbo.jobexpenseitem;

    alter table dbo.jobcostingexpenseitem 
        add constraint FK_jobcostingexpenseitem_serviceCost 
        foreign key (serviceCost) 
        references dbo.costs_service;

    alter table dbo.jobexpenseitem 
        add constraint FK_jobexpenseitem_model 
        foreign key (model) 
        references dbo.instmodel;

COMMIT TRAN