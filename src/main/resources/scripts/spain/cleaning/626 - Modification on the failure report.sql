USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.faultreport ADD dispensation bit NULL;
GO

UPDATE dbo.faultreport SET dispensation = 1
where faultrepid = (select faultreportid from faultreportrecommendation 
					where faultreportid = faultrepid and recommendation = 'DISPENSATION')

DELETE from faultreportrecommendation where recommendation = 'DISPENSATION';

-- remove unused field in exchange format
DELETE FROM dbo.exchangeformatfieldnamedetails
	WHERE fieldName = 'WORKREQUIRMENT_EQUIVALENT_ID'


INSERT INTO dbversion VALUES (626)

COMMIT TRAN