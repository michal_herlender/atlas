-- Galen Beck - 2018-09-06
-- Updated definition for table 'jobitemhistory' created in script 288  to fix a couple issues and be comsistent with the SchemaUpdateTool
-- Note, this should be run after 288 (just more straightforward this way for people who have already run it, since it's in the trunk.)

USE [spain];

BEGIN TRAN

drop table jobitemhistory;

go

create table dbo.jobitemhistory (
    id int identity not null,
    changedate datetime2 not null,
    comments nvarchar(1000),
    duedateupdated tinyint not null,
    newduedate date,
    oldduedate date,
    changeby int not null,
    jobitemid int not null,
    primary key (id)
);

alter table dbo.jobitemhistory 
    add constraint FK_jobitemhistory_changeby 
    foreign key (changeby) 
    references dbo.contact;

alter table dbo.jobitemhistory 
    add constraint FK_jobitemhistory_jobitemid 
    foreign key (jobitemid) 
    references dbo.jobitem;
	
ALTER TABLE dbo.notificationsystemfieldchange ALTER COLUMN changedate datetime2;
GO

INSERT INTO dbversion(version) VALUES(289);

COMMIT TRAN