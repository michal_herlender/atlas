USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[procs]
   SET [reqtype] = 'THIRD_PARTY'
 WHERE active = 1 and (reference like '%EXTERN%' or name like '%TParty%') and reqtype <> 'THIRD_PARTY'
GO

INSERT INTO dbversion(version) VALUES(459)
GO

COMMIT TRAN