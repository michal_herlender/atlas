USE [atlas]
GO

BEGIN TRAN

DECLARE @contractreview_activity INT,
		@lookup_59 INT,
		@lookup_79 INT;

SELECT @contractreview_activity = stateid FROM dbo.itemstate WHERE [description] = 'Contract review';
SELECT @lookup_59 = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_JobItemAtInstrumentAddress';
SELECT @lookup_79 = id FROM dbo.lookupinstance WHERE [lookup] = 'Lookup_PreDeliveryRequirements (after lab calibration - before costing check)';

UPDATE dbo.outcomestatus SET lookupid = @lookup_79 WHERE activityid = @contractreview_activity 
AND lookupid = @lookup_59;

INSERT INTO dbversion(version) VALUES (610)

COMMIT TRAN