update jobitem set lastContractReviewBy_personid =
	(select top 1 contractreview.personid
		from contractreviewitem
		left join contractreview on contractreviewitem.reviewid = contractreview.id
		where contractreviewitem.jobitemid = jobitem.jobitemid
		order by contractreview.log_lastmodified desc
	);
GO

drop function selectedjobitems;
drop function transportmethodname;
GO

create function transportmethodname(@methodid int, @locale varchar(255)) returns table
as return
select transportmethodtranslation.translation
from transportmethodtranslation
where id = @methodid and locale = @locale;
GO

create function selectedjobitems(@locale varchar(255)) returns table
as return
select jobitem.jobitemid,
	jobitem.itemno,
	(select count(*) from jobitem as jiSQ where jiSQ.jobid = job.jobid) as jobitemcount,
	jobitem.duedate,
	job.jobid,
	job.jobno,
	job.regdate,
	clientcontact.personid as clientcontactid,
	(clientcontact.firstname + ' ' + clientcontact.lastname) as clientcontactname,
	clientcompany.coid,
	clientcompany.coname,
	transportmethodname.translation as transportout,
	instrument.plantid,
	instrument.plantno,
	instrument.serialno,
	instrument.modelid,
	procs.reference,
	itemstate.stateid,
	(contractreviewby.firstname + ' ' + contractreviewby.lastname) as contractreviewer
from jobitem
left join job on jobitem.jobid = job.jobid
left join contact as clientcontact on job.personid = clientcontact.personid
left join subdiv as clientsubdiv on clientcontact.subdivid = clientsubdiv.subdivid
left join company as clientcompany on clientsubdiv.coid = clientcompany.coid
left join transportoption as transportoutopt on jobitem.returnoptionid = transportoutopt.id
left join instrument on jobitem.plantid = instrument.plantid
left join procs on jobitem.procid = procs.id
left join itemstate on jobitem.stateid = itemstate.stateid
left join contact as contractreviewby on jobitem.lastContractReviewBy_personid = contractreviewby.personid
cross apply transportmethodname(transportoutopt.transportmethodid,@locale)