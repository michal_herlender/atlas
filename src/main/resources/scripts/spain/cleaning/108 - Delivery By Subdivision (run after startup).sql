-- Upgrades delivery to multicompany - must be run after startup
-- Written to work on both test database (more subdivs) and on production
-- 6645 MAD, 6644 ZAZ, 6642 BIO, 6643 VIT, 6951 CAS
-- Authour Galen Beck - 2016-12-07

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[delivery] SET [orgid] = 6645 WHERE [deliveryno] like 'ESTEM-MAD%';
UPDATE [dbo].[delivery] SET [orgid] = 6644 WHERE [deliveryno] like 'ESTEM-ZAZ%';
UPDATE [dbo].[delivery] SET [orgid] = 6643 WHERE [deliveryno] like 'ESTIC-BIL%';
UPDATE [dbo].[delivery] SET [orgid] = 6643 WHERE [deliveryno] like 'ESTIC-BIO%';
UPDATE [dbo].[delivery] SET [orgid] = 6642 WHERE [deliveryno] like 'ESTIC-VIT%';
UPDATE [dbo].[delivery] SET [orgid] = 6951 WHERE [deliveryno] like 'MA030CAS%';

GO

UPDATE [dbo].[delivery] SET [lastModified] = '2016-12-06' WHERE [lastModified] IS NULL;

GO

ALTER table dbo.delivery ALTER COLUMN lastModified datetime NOT NULL;
ALTER table dbo.delivery ALTER COLUMN orgid int NOT NULL;

COMMIT TRAN