-- Add table for storing failure report recommendation
-- (we now support multiple recommendations for one failure report)

USE [spain]
GO

BEGIN TRAN

    create table dbo.faultreportrecommendation (
       faultreportid int not null,
        recommendation varchar(255)
    );

    alter table dbo.faultreportrecommendation 
       add constraint FKhlv75m4s1cymas9g4jks12h7e 
       foreign key (faultreportid) 
       references dbo.faultreport;

	INSERT INTO dbversion(version) VALUES(316);

COMMIT TRAN