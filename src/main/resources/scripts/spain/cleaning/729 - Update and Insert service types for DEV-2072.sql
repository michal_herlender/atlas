-- Updates to service types and creation of 5 new service types
-- For DEV-2072 ticket - Galen Beck - 2020-11-12

USE [atlas]

BEGIN TRAN

-- Updates to existing service types (mainly to separate in-laboratory from onsite, fix some fasttrack colors)

DECLARE @ic_il_servicetype int;
DECLARE @service_servicetype int;
DECLARE @vco_il_servicetype int;
DECLARE @qual_servicetype int;
DECLARE @val_servicetype int;

DECLARE @qual_caltype int;
DECLARE @val_caltype int;

SELECT @ic_il_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'IC';
SELECT @service_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'SERVICE';
SELECT @vco_il_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'VCO';
SELECT @qual_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'QUAL';
SELECT @val_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'VAL';

SELECT @qual_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @qual_servicetype;
SELECT @val_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @val_servicetype;

-- Update 5 exising service types (various fields)

UPDATE [dbo].[servicetype]
   SET [longname] = 'Intermediate control in laboratory' ,[shortname] = 'IC-IL'
 WHERE servicetypeid = @ic_il_servicetype

UPDATE [dbo].[servicetype]
   SET [displaycolourfasttrack] = '#FEE0B4' 
 WHERE servicetypeid = @service_servicetype

UPDATE [dbo].[servicetype]
   SET [displaycolourfasttrack] = '#FEE0B4' ,[longname] = 'Verification of correct operation in laboratory' ,[shortname] = 'VCO-IL'
 WHERE servicetypeid = @vco_il_servicetype 

UPDATE [dbo].[servicetype]
   SET [displaycolourfasttrack] = '#FEE0B4' ,[orderby] = 45
 WHERE servicetypeid = @val_servicetype 

UPDATE [dbo].[servicetype]
   SET [displaycolourfasttrack] = '#FEE0B4' ,[orderby] = 44
 WHERE servicetypeid = @qual_servicetype 

-- Update long names for 2 service types, 4 languages each

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Kalibrierung, Analyse und Test - Zwischenpr�fung im Labor'
 WHERE [servicetypeid] = @ic_il_servicetype AND [locale] = 'de_DE'

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Intermediate control in laboratory'
 WHERE [servicetypeid] = @ic_il_servicetype AND [locale] = 'en_GB'

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Control intermedio en laboratorio'
 WHERE [servicetypeid] = @ic_il_servicetype AND [locale] = 'es_ES'

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Surveillance m�trologique en laboratoire'
 WHERE [servicetypeid] = @ic_il_servicetype AND [locale] = 'fr_FR'

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = '�berpr�fung des ordnungsgem��en Betriebs im Labor'
 WHERE [servicetypeid] = @vco_il_servicetype AND [locale] = 'de_DE'

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Verification of proper operation in laboratory'
 WHERE [servicetypeid] = @vco_il_servicetype AND [locale] = 'en_GB'

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Verificaci�n del correcto funcionamiento en laboratorio'
 WHERE [servicetypeid] = @vco_il_servicetype AND [locale] = 'es_ES'

UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'V�rification du bon fonctionnement en laboratoire'
 WHERE [servicetypeid] = @vco_il_servicetype AND [locale] = 'fr_FR'
 
-- Update short names for 2 service types, 4 languages each

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ZP-LA'
 WHERE [servicetypeid] = @ic_il_servicetype AND [locale] = 'de_DE'

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'IC-IL'
 WHERE [servicetypeid] = @ic_il_servicetype AND [locale] = 'en_GB'

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'CI-EL'
 WHERE [servicetypeid] = @ic_il_servicetype AND [locale] = 'es_ES'

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'SM-LA'
 WHERE [servicetypeid] = @ic_il_servicetype AND [locale] = 'fr_FR'

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'UOB-LA'
 WHERE [servicetypeid] = @vco_il_servicetype AND [locale] = 'de_DE'

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VPO-IL'
 WHERE [servicetypeid] = @vco_il_servicetype AND [locale] = 'en_GB'

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VCF-EL'
 WHERE [servicetypeid] = @vco_il_servicetype AND [locale] = 'es_ES'

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'VBF-LA'
 WHERE [servicetypeid] = @vco_il_servicetype AND [locale] = 'fr_FR'

-- Update orderby for two cal type entries

UPDATE [dbo].[calibrationtype]
   SET [orderby] = 44
 WHERE caltypeid = @qual_caltype 

UPDATE [dbo].[calibrationtype]
   SET [orderby] = 45
 WHERE caltypeid = @val_caltype 

-- Insert five new service types

INSERT INTO [dbo].[servicetype]
           ([displaycolour] ,[displaycolourfasttrack] ,[longname] ,[shortname] ,[canbeperformedbyclient] ,[domaintype] ,[orderby] ,[repair])
     VALUES
           ('#FFFFFF' ,'#FEE0B4' ,'Intermediate control on site' ,'IC-OS' ,1 ,0 ,12 ,0),
		   ('#FFFFFF' ,'#FEE0B4' ,'Verification of correct operation on site' ,'VCO-OS' ,0 ,0 , 43, 0),
		   ('#FFFFFF' ,'#FEE0B4' ,'Administrative task' ,'ADMIN' ,0 ,0 , 46, 0),
		   ('#FFFFFF' ,'#FEE0B4' ,'Exchange' ,'EXCH' ,0 ,0 , 47, 0),
		   ('#FFFFFF' ,'#FEE0B4' ,'Logistics transfer' ,'LT' ,0 ,0 ,48 ,0)
GO

-- Insert five new calibration types for new service types

INSERT INTO [dbo].[calibrationtype]
           ([accreditationspecific]
           ,[active]
           ,[certnoresetyearly]
           ,[firstpagepapertype]
           ,[labeltemplatepath]
           ,[orderby]
           ,[recalldateonlabel]
           ,[restpagepapertype]
           ,[lastModified]
           ,[servicetypeid]
           ,[recallRequirementType]
           ,[calibrationWithJudgement])
     VALUES
           (0 ,1 ,0 ,'A4_PLAIN' ,'/labels/birt/birt_label_standard.rptdesign' ,12 ,1 ,'A4_PLAIN' ,'2020-11-12'
           ,(SELECT servicetypeid FROM servicetype WHERE shortname= 'IC-OS') ,'FULL_CALIBRATION' ,0),
           (0 ,1 ,0 ,'A4_PLAIN' ,'/labels/birt/birt_label_standard.rptdesign' ,43 ,1 ,'A4_PLAIN' ,'2020-11-12'
           ,(SELECT servicetypeid FROM servicetype WHERE shortname= 'VCO-OS') ,'FULL_CALIBRATION' ,0),
           (0 ,1 ,0 ,'A4_PLAIN' ,'/labels/birt/birt_label_standard.rptdesign' ,46 ,1 ,'A4_PLAIN' ,'2020-11-12'
           ,(SELECT servicetypeid FROM servicetype WHERE shortname= 'ADMIN') ,'FULL_CALIBRATION' ,0),
           (0 ,1 ,0 ,'A4_PLAIN' ,'/labels/birt/birt_label_standard.rptdesign' ,47 ,1 ,'A4_PLAIN' ,'2020-11-12'
           ,(SELECT servicetypeid FROM servicetype WHERE shortname= 'EXCH') ,'FULL_CALIBRATION' ,0),
           (0 ,1 ,0 ,'A4_PLAIN' ,'/labels/birt/birt_label_standard.rptdesign' ,48 ,1 ,'A4_PLAIN' ,'2020-11-12'
           ,(SELECT servicetypeid FROM servicetype WHERE shortname= 'LT') ,'FULL_CALIBRATION' ,0)
GO

-- Obtain new service type and calibration type ids

DECLARE @ic_os_servicetype int;
DECLARE @vco_os_servicetype int;
DECLARE @admin_servicetype int;
DECLARE @exch_servicetype int;
DECLARE @log_servicetype int;

DECLARE @ic_os_caltype int;
DECLARE @vco_os_caltype int;
DECLARE @admin_caltype int;
DECLARE @exch_caltype int;
DECLARE @log_caltype int;

SELECT @ic_os_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'IC-OS';
SELECT @vco_os_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'VCO-OS';
SELECT @admin_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'ADMIN';
SELECT @exch_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'EXCH';
SELECT @log_servicetype = servicetypeid FROM dbo.servicetype WHERE shortname = 'LT';

SELECT @ic_os_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @ic_os_servicetype;
SELECT @vco_os_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @vco_os_servicetype;
SELECT @admin_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @admin_servicetype;
SELECT @exch_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @exch_servicetype;
SELECT @log_caltype = caltypeid FROM dbo.calibrationtype WHERE servicetypeid = @log_servicetype;

-- Insert long translations for new service types

INSERT INTO [dbo].[servicetypelongnametranslation]
           ([servicetypeid]
           ,[locale]
           ,[translation])
     VALUES
           (@ic_os_servicetype,'de_DE','Kalibrierung, Analyse und Test - Zwischenpr�fung Vor-Ort'),
           (@ic_os_servicetype,'en_GB','Intermediate control on site'),
           (@ic_os_servicetype,'es_ES','Control intermedio in situ'),
           (@ic_os_servicetype,'fr_FR','Surveillance m�trologique sur situ'),

           (@vco_os_servicetype,'de_DE','�berpr�fung des ordnungsgem��en Betriebs Vor-Ort'),
           (@vco_os_servicetype,'en_GB','Verification of proper operation on site'),
           (@vco_os_servicetype,'es_ES','Verificaci�n del correcto funcionamiento in situ'),
           (@vco_os_servicetype,'fr_FR','V�rification du bon fonctionnement sur situ'),

           (@admin_servicetype,'de_DE','Verwaltungsaufgabe'),
           (@admin_servicetype,'en_GB','Administrative task'),
           (@admin_servicetype,'es_ES','Tarea administrativa'),
           (@admin_servicetype,'fr_FR','Recette administrative'),

           (@exch_servicetype,'de_DE','Austausch'),
           (@exch_servicetype,'en_GB','Exchange'),
           (@exch_servicetype,'es_ES','Intercambio'),
           (@exch_servicetype,'fr_FR','Echange'),

           (@log_servicetype,'de_DE','Logistiktransfer'),
           (@log_servicetype,'en_GB','Logistics transfer'),
           (@log_servicetype,'es_ES','Transferencia log�stica'),
           (@log_servicetype,'fr_FR','Transfert logistique')

-- Insert short translations for new service types

INSERT INTO [dbo].[servicetypeshortnametranslation]
           ([servicetypeid]
           ,[locale]
           ,[translation])
     VALUES
           (@ic_os_servicetype,'de_DE','ZP-VO'),
           (@ic_os_servicetype,'en_GB','IC-OS'),
           (@ic_os_servicetype,'es_ES','CI-IS'),
           (@ic_os_servicetype,'fr_FR','SM-SI'),

           (@vco_os_servicetype,'de_DE','UOB-VO'),
           (@vco_os_servicetype,'en_GB','VPO-OS'),
           (@vco_os_servicetype,'es_ES','VCF-IS'),
           (@vco_os_servicetype,'fr_FR','VBF-SI'),

           (@admin_servicetype,'de_DE','VA'),
           (@admin_servicetype,'en_GB','ADMIN'),
           (@admin_servicetype,'es_ES','TA'),
           (@admin_servicetype,'fr_FR','RA'),

           (@exch_servicetype,'de_DE','AUST'),
           (@exch_servicetype,'en_GB','EXCH'),
           (@exch_servicetype,'es_ES','ICB'),
           (@exch_servicetype,'fr_FR','ECH'),

           (@log_servicetype,'de_DE','LT'),
           (@log_servicetype,'en_GB','LT'),
           (@log_servicetype,'es_ES','TL'),
           (@log_servicetype,'fr_FR','TL')

-- Insert new job type / cal type - job type 1 = standard, job type 4 = onsite

INSERT INTO [dbo].[jobtypecaltype]
           ([defaultvalue]
           ,[jobtypeid]
           ,[caltypeid])
     VALUES
           (0 ,4 ,@ic_os_caltype),
           (0 ,4 ,@vco_os_caltype),
           (0 ,1 ,@admin_caltype),
           (0 ,1 ,@exch_caltype),
           (0 ,1 ,@log_caltype)

-- Insert new job type / service type - job type 1 = standard, job type 4 = onsite

INSERT INTO [dbo].[jobtypeservicetype]
           ([defaultvalue]
           ,[jobtypeid]
           ,[servicetypeid])
     VALUES
           (0 ,4 ,@ic_os_servicetype),
           (0 ,4 ,@vco_os_servicetype),
           (0 ,1 ,@admin_servicetype),
           (0 ,1 ,@exch_servicetype),
           (0 ,1 ,@log_servicetype)

-- Insert into calibrationacreditationlevel for authorization control of users by service type
-- Just IC-OS and VCO-OS are included
-- SIGN generally just used for "accredited" two step processes

INSERT INTO dbo.calibrationaccreditationlevel
	(accredlevel, caltypeid)
	VALUES('APPROVE', @ic_os_caltype),
		  ('PERFORM', @ic_os_caltype),
		  ('REMOVED', @ic_os_caltype),
	      ('SUPERVISED', @ic_os_caltype),
	      ('APPROVE', @vco_os_caltype),
		  ('PERFORM', @vco_os_caltype),
		  ('REMOVED', @vco_os_caltype),
	      ('SUPERVISED', @vco_os_caltype);

INSERT INTO dbversion(version) VALUES (729)	

COMMIT TRAN