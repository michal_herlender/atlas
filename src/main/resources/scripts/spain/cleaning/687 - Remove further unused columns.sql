-- Removes further unused columns from several tables.

USE [atlas]

BEGIN TRAN

-- Column is entirely null, unused in codebase
ALTER TABLE [country] DROP COLUMN [description];
-- Column contains partial information about a rename from ~2017, unused in codebase (and no entity definition)
ALTER TABLE [servicetype] DROP COLUMN [description];

-- Column is entirely unused
ALTER TABLE [description] DROP COLUMN [alwaysbasefirst];

-- Default procedure id on description is entirely null - unused since Antech
ALTER TABLE [dbo].[description] DROP CONSTRAINT [FKjar8hfapve7ueg3inb8efuixt]
DROP INDEX [IDX_description_defaultprocid] ON [dbo].[description]
ALTER TABLE [description] DROP COLUMN [defaultprocid];

INSERT INTO dbversion(version) VALUES(687);

COMMIT TRAN