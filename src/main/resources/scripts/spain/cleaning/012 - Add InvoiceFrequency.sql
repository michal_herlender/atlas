-- Author Tony Provost 2016-01-28
-- Populate table invoicefrequency with default records (table for finance)

BEGIN TRAN 

USE [spain];
GO

SET IDENTITY_INSERT [invoicefrequency] ON
GO
  
INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (1, 'OG', 'On going invoicing as work orders are completed')
INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (2, 'EM', 'End of month based on recapitulative statement for the month completed')
INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (3, 'BM', 'Beginning of the month for the coming month')
INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (4, 'BQ', 'Beginning of quarter for the coming quarter')
INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (5, 'FX', 'Fix annual invoiced by 1/12')
INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (6, 'PO', 'One invoice per purcharse order')
INSERT INTO [invoicefrequency] ([id], [code], [description]) VALUES (7, 'DN', 'One invoice per delivery note')
GO

-- English Translation
INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) SELECT [id], 'en_GB', [description] FROM [invoicefrequency] WHERE [id] NOT IN (SELECT [invoicefrequencyid] FROM [invoicefrequencytranslation] WHERE [locale] = 'en_GB') AND [description] <> ''
GO

-- Spanish Translation
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 1 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (1, 'es_ES', 'Facturación cada vez que se termine un trabajo')											
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 2 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (2, 'es_ES', 'A final de mes se factura todo lo realizado durante ese mes')
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 3 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (3, 'es_ES', 'Facturación a principio de mes adelantada')
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 4 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (4, 'es_ES', 'Facturación trimestral adelantada')
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 5 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (5, 'es_ES', 'Facturación fija bianual')
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 6 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (6, 'es_ES', 'Una factura por pedido')
IF NOT EXISTS (SELECT 1 FROM [invoicefrequencytranslation] WHERE [invoicefrequencyid] = 7 AND locale = 'es_ES') 
	INSERT INTO [invoicefrequencytranslation] ([invoicefrequencyid], [locale], [translation]) VALUES (7, 'es_ES', 'Una factura por albarán')
GO

SET IDENTITY_INSERT [invoicefrequency] OFF
GO

COMMIT TRAN
GO


