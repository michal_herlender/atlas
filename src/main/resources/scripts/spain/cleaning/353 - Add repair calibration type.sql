USE [spain];

BEGIN TRAN;

-- Create cal type for repair service type, if it doesn't exist already
-- (It appears to have been previously created without a script, in production but not everywhere)
IF NOT EXISTS
    (
    SELECT 1 FROM calibrationtype ct WHERE ct.servicetypeid =6
    )
BEGIN
  INSERT INTO calibrationtype
        (accreditationspecific,active,firstpagepapertype,labeltemplatepath,orderby,recalldateonlabel,
        restpagepapertype,servicetypeid,recallRequirementType,calibrationWithJudgement)
        VALUES(0,1,'A4_PLAIN','/labels/birt/birt_label_standard.rptdesign',26,0,'A4_PLAIN',6,'SERVICE_TYPE',0);
END

GO

-- Insert default quotation calibration conditions for new cal type and every business company
INSERT INTO [dbo].[defaultquotationcalibrationcondition]
(conditiontext, lastModified, caltypeid, orgid)
SELECT '',
'2016-01-01',
(SELECT calibrationtype.caltypeid
	FROM calibrationtype
	WHERE calibrationtype.servicetypeid = 6),
company.coid
FROM company
WHERE company.corole = 5;

GO

-- Insert links to jobtype, if it doesn't exist already (it seems to on production, but not on some test databases)
IF NOT EXISTS
    (
    SELECT 1 FROM jobtypecaltype JOIN calibrationtype ON calibrationtype.caltypeid = jobtypecaltype.caltypeid WHERE calibrationtype.servicetypeid = 6
    )
	BEGIN
		INSERT INTO jobtypecaltype (defaultvalue, jobtypeid, caltypeid)
		  SELECT 0,1,caltypeid
		  FROM calibrationtype
			WHERE calibrationtype.servicetypeid = 6;
	END

GO

INSERT INTO dbversion(version) VALUES (353);

COMMIT TRAN;