-- Updates definitions for 'Interim Calibration' to 'Intermediate Control' per Didier's request

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[servicetype]
   SET [longname] = 'Intermediate Control'
 WHERE [servicetypeid] = 28;
GO

-- English

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'IC'
 WHERE [servicetypeid] = 28 and [locale] = 'en_GB';
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Intermediate Control'
 WHERE [servicetypeid] = 28 and [locale] = 'en_GB';
GO

-- French

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'SM'
 WHERE [servicetypeid] = 28 and [locale] = 'fr_FR';
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Surveillance m�trologique'
 WHERE [servicetypeid] = 28 and [locale] = 'fr_FR';
GO

-- Spanish

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'CI'
 WHERE [servicetypeid] = 28 and [locale] = 'es_ES';
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Control intermedio'
 WHERE [servicetypeid] = 28 and [locale] = 'es_ES';
GO

-- German

UPDATE [dbo].[servicetypeshortnametranslation]
   SET [translation] = 'ZP'
 WHERE [servicetypeid] = 28 and [locale] = 'de_DE';
GO
UPDATE [dbo].[servicetypelongnametranslation]
   SET [translation] = 'Kalibrierung, Analyse und Test - Zwischenpr�fung'
 WHERE [servicetypeid] = 28 and [locale] = 'de_DE';
GO


INSERT INTO dbversion(version) VALUES(467);

COMMIT TRAN
