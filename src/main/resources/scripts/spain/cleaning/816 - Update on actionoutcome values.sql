USE [atlas]
GO

BEGIN TRAN


UPDATE dbo.actionoutcome
	SET value=N'REQUIRES_INHOUSE_REPAIR'
	WHERE id=9;
	
UPDATE dbo.actionoutcome
	SET value=N'REQUIRES_THIRD_PARTY_REPAIR'
	WHERE id=10;

    
    insert into dbversion(version) values(816);

COMMIT TRAN