/*
 * As discussed in Vendome meeting : 
 * Delete the timeIncrement table, and make a link between Calibration and JobItemActivity (JobItemAction)
 * with a new table CalibrationActivityLink
*/
USE [spain]
GO

BEGIN TRAN

	--update description value
	UPDATE spain.dbo.systemdefaultgroup
	SET description='Link to management park'
	WHERE id=12 
	go

	--delete foreign key constraint in column businesssubdivisionsettingsid
	ALTER TABLE spain.dbo.advesooverriddenactivitysetting DROP CONSTRAINT advesooverriddenactivitysetting_businesssubdivisionsettings_FK 
	go
	--delete column businesssubdivisionsettingsid
	ALTER TABLE spain.dbo.advesooverriddenactivitysetting DROP COLUMN businesssubdivisionsettingsid 
	go
	--add new column subdivid
	ALTER TABLE spain.dbo.advesooverriddenactivitysetting ADD subdivid int NOT NULL 
	go
	--add foreign key constraint in new column subdivid
	ALTER TABLE spain.dbo.advesooverriddenactivitysetting ADD CONSTRAINT advesooverriddenactivitysetting_subdiv_FK FOREIGN KEY (subdivid) REFERENCES spain.dbo.subdiv(subdivid) ON DELETE CASCADE ON UPDATE CASCADE 
	go
	
	insert into dbversion(version) values(255)
	go
	
COMMIT TRAN