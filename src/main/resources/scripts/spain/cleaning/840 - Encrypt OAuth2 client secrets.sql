-- Updates client secrets to be Scrypt encoded
-- See DEV-2777 (needed in Spring Security 5)
-- Compatible only with Spring Security 5 for OAuth2 (e.g. trunk)

BEGIN TRAN

UPDATE [dbo].[oauth2clientdetails]
   SET [clientSecret] = 
   '{scrypt}$e0801$5ymr8tNfb9oede8Y1FKfFrhd0P0siy4G/HHkz5ZUytSv3D5d12W4+V5LV/zKhsZ0lQ57zTUYyc2QBnZtSKZKew==$8Ba+05+vA+4/GyKl5TOa1BlDM8t6Aquf2UOrRfgvZU8='
 WHERE [clientid] = 'accounting_sync';
GO

UPDATE [dbo].[oauth2clientdetails]
   SET [clientSecret] = 
   '{scrypt}$e0801$22r7H1ntjjujq3Nx3keq+kTzIeY7j7r1x4i5fuJlQo0oSilzMhJPLTQAXgbYxToLn3spEXoQxdJ6ZHgyEzDlIQ==$EkcO9KxhME1C8LSHpDZpcED0MbMip7qKIUz1LtPw45I='
 WHERE [clientid] = 'mobileapp';
GO

UPDATE [dbo].[oauth2clientdetails]
   SET [clientSecret] = 
   '{scrypt}$e0801$HDoyST/jkNUaVmgYPcNbN4BIiSKaSjQkpW1bL7IUjMnOzC0JtwWHFCczx1yWF4fhjbEAPqEMouun/+bkwoNpyQ==$mBXGtHk2F4vAVu3jmE40hbymo2/E/JdEiIPCntRQUqc='
 WHERE [clientid] = 'mobileapp_logistics';
GO

UPDATE [dbo].[oauth2clientdetails]
   SET [clientSecret] = 
   '{scrypt}$e0801$fjpb6V7+YoaJwqzzA/6PvJJ3RstntZrNNTRfVAaKqp9F+cx0MTULXzdC5TwrpMVmbX0P+bxzu4yO8T4ErGo45w==$u3/ymNnTE/tkZTVqQ7/K1ziaxIB5O5xEqyp8OCBDvgU='
 WHERE [clientid] = 'mobileapp_portal';
GO

UPDATE [dbo].[oauth2clientdetails]
   SET [clientSecret] = 
   '{scrypt}$e0801$4VV8h7Jz6aKkaDmYXI8fIAh1sdirK3x6Us3Fkdb6hyzf0gNu2EzCzLzkxwgoXIRoyIriz8M34pinTeDWYe+VYA==$IgFxG38Ts+le0vUzdfbv8BgEINyat3NT78qlZPqwojA='
 WHERE [clientid] = 'sidetrade_client';
GO

UPDATE [dbo].[oauth2clientdetails]
   SET [clientSecret] = 
   '{scrypt}$e0801$ZT5MbQGiumIEi6AxV61s0G7mLvNeFRXr8B9Fkv+RvDxwk4jcIQ9Y8DIl2PVxZUqNhVi02OVydHkQUJF1uiXFgQ==$EbqrR2+SQMElQI+FHaHZh4LQz2jtxEmm+QT8rSIxPio='
 WHERE [clientid] = 'us_calypso';
GO

UPDATE [dbo].[oauth2clientdetails]
   SET [clientSecret] = 
   '{scrypt}$e0801$1wkUSAgCIoNPCpYbGNMjZivwcS86YOTZkeP/Z+JFCcbrE+ij/4GW9huIZG+Aid/cSrgI6xgW4ArRrSZFaZH9yA==$qnEeKWnXRF3ATk5wIqkm0xahRZPBq4gKvPHtDr3oMNs='
 WHERE [clientid] = 'us_tam';
GO

INSERT INTO dbversion VALUES (840);

COMMIT TRAN