USE atlas
Go

BEGIN TRAN


---- Modify existing exchange format
UPDATE dbo.exchangeformat
	SET eftype='OPERATIONS_IMPORTATION'
	WHERE eftype='CALIBRATIONS_IMPORTATION';

-- add column 'OPERATOIN TYPE' to already existing ef
declare  @efid int;
declare @maxposition int;

select top 1 @efid = ef.id
from exchangeformat ef where ef.eftype = 'OPERATIONS_IMPORTATION'

select @maxposition = max(d.position)
from exchangeformatfieldnamedetails d where d.exchangeFormatid = @efid

INSERT INTO dbo.exchangeformatfieldnamedetails ([position],mandatory,templateName,fieldName,exchangeFormatid)
	VALUES (@maxposition,1,'OPERATIONTYPE','OPERATION_TYPE',@efid);

	
UPDATE dbo.exchangeformatfieldnamedetails
	SET fieldName='OPERATION_DATE',templateName='INTERVENTION_DATE'
	WHERE fieldName='CALIBRATION_DATE';
	
UPDATE dbo.exchangeformatfieldnamedetails
	SET fieldName='OPERATION_START_DATE',templateName='INTERVENTION_START_ON'
	WHERE fieldName='CALIBRATION_START_DATE';

DELETE FROM dbo.exchangeformatfieldnamedetails
	WHERE fieldName = 'CALIBRATION_END_DATE';
	
DELETE FROM dbo.exchangeformatfieldnamedetails
	WHERE fieldName = 'FR_ISSUE_DATE';
	
DELETE FROM dbo.exchangeformatfieldnamedetails
	WHERE fieldName = 'FR_VALIDATED_ON';
	
DELETE FROM dbo.exchangeformatfieldnamedetails
	WHERE fieldName = 'FR_VALIDATED_BY';
	
UPDATE dbo.exchangeformatfieldnamedetails
	SET templateName='INTERVENTION_TIME',fieldName='OPERATION_DURATION'
	WHERE fieldName='CALIBRATION_TIME';
	
UPDATE dbo.exchangeformatfieldnamedetails
	SET templateName='DOCUMENT_NO',fieldName='DOCUMENT_NO'
	WHERE fieldName='CERTIFICATE_NO';
	
UPDATE dbo.exchangeformatfieldnamedetails
	SET templateName='INTERVENTION_VALIDATION_ON',fieldName='OPERATION_VALIDATED_ON'
	WHERE fieldName='CERTIFICATE_VALIDATED_ON';
	
UPDATE dbo.exchangeformatfieldnamedetails
	SET templateName='INTERVENTION_VALIDATION_BY',fieldName='OPERATION_VALIDATED_BY_HRID'
	WHERE fieldName='CERTIFICATE_VALIDATED_BY_HRID';
	
UPDATE dbo.exchangeformatfieldnamedetails
	SET fieldName='OPERATION_BY_HRID'
	WHERE fieldName='TECHNICIAN_HRID';
	
---- add failure report possibility after 'awaiting calibration' and also for onsite
-- rename activity and set outcome type
UPDATE dbo.itemstate
	SET actionoutcometype='NO_OPERATION_PERFORMED',description='No operation performed'
	WHERE description='No action required';
	
-- translations
UPDATE dbo.itemstatetranslation
	SET [translation]='Keine Operation ausgef�hrt '
	WHERE stateid=174 AND locale='de_DE' AND [translation]='Keine Operation  ausgef�hrt ';
UPDATE dbo.itemstatetranslation
	SET [translation]='No operation performed'
	WHERE stateid=174 AND locale='en_GB' AND [translation]='No operation  performed';
UPDATE dbo.itemstatetranslation
	SET [translation]='Ninguna operaci�n realizada'
	WHERE stateid=174 AND locale='es_ES' AND [translation]='Ninguna operaci�n  realizada';
UPDATE dbo.itemstatetranslation
	SET [translation]='Aucune op�ration effectu�e'
	WHERE stateid=174 AND locale='fr_FR' AND [translation]='Aucune op�ration  effectu�e';


-- add action outcomes
INSERT INTO dbo.actionoutcome (description, defaultoutcome,positiveoutcome,active,[type],value)
	VALUES ('Failure report not required',1,0,1,'no_operation_performed','FAILURE_REPORT_NOT_REQUIRED');
INSERT INTO dbo.actionoutcome (description, defaultoutcome,positiveoutcome,active,[type],value)
	VALUES ('Failure report required',0,0,1,'no_operation_performed','FAILURE_REPORT_REQUIRED');


-- rename hook
UPDATE dbo.hook
	SET name='no-operation-performed'
	WHERE name='no-action-required-for-calibration';
-- get the id
declare @hookid int;
select @hookid = h.id
from hook h
where h.name='no-operation-performed'

-- rename inhouse lookup
UPDATE dbo.lookupinstance
	SET lookup='Lookup_NoOperationPerformedOutcome (inhouse)'
	WHERE lookup='Lookup_ItemToDeliverNeedJobCosting'

-- get inhouse lookup
declare @inhouselu int;
select @inhouselu = li.id
from lookupinstance li
where li.lookup = 'Lookup_NoOperationPerformedOutcome (inhouse)'

-- modify hook outcomes
declare @yesos int;
declare @noos int;

--get yes = FAILURE_REPORT_NOT_REQUIRED os id
select @yesos = lr.outcomestatusid
from lookupresult lr
where lr.lookupid = @inhouselu and lr.resultmessage = 0;

-- get lookup 90
declare @lu90 int;
select @lu90 = lii.id
from lookupinstance lii
where lii.lookup = 'Lookup_NextWorkRequirementByJobCompany (before lab calibration)';

-- wire yes = FAILURE_REPORT_NOT_REQUIRED to lookup 90
UPDATE dbo.outcomestatus
	SET statusid=NULL,lookupid=@lu90
	WHERE id=@yesos;

--get no = FAILURE_REPORT_REQUIRED os id
select @noos = lr.outcomestatusid
from lookupresult lr
where lr.lookupid = @inhouselu and lr.resultmessage = 1;

-- get lookup 16 : has failure report
declare @lu16 int;
select @lu16 = lii.id
from lookupinstance lii
where lii.lookup = 'Lookup_ItemHasFaultReport (inhouse after calibration failure)';

-- wire no = FAILURE_REPORT_REQUIRED to lookup 
UPDATE dbo.outcomestatus
	SET statusid=NULL,lookupid=@lu16
	WHERE id=@noos;
	

-- onsite

-- create new lookup for onsite
INSERT INTO dbo.lookupinstance (lookup,lookupfunction)
	VALUES ('Lookup_NoOperationPerformedOutcome (onsite)',57);
declare @oslu int;
select @oslu = max(id)
from lookupinstance;

-- create new activity for onsite
INSERT INTO dbo.itemstate ([type],description,retired,actionoutcometype, lookupid, active)
	VALUES ('workactivity','No onsite operation performed',0,'NO_OPERATION_PERFORMED', @oslu, 1);
declare @calactid int;
select @calactid = max(iss.stateid)
from itemstate iss;

-- translations
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@calactid,'en_GB','No onsite operation performed');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@calactid,'fr_FR','Aucune op�ration sur site effectu�e');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@calactid,'es_ES','No se realiz� ninguna operaci�n en el sitio');
INSERT INTO dbo.itemstatetranslation (stateid,locale,[translation])
	VALUES (@calactid,'de_DE','Keine Vor-Ort-Operation durchgef�hrt');


-- modify the hook to support the onsite case
-- get 'awaiting onsite calibration' id
declare @calstatusid int;
select @calstatusid = iss.stateid
from itemstate iss
where iss.description = 'Awaiting onsite calibration';

INSERT INTO dbo.hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
	VALUES (1,1,@calactid,@hookid,@calstatusid);

-- create lookup results and outcome statues
-- get lookup Lookup_ItemQuotedBypassCosting (OnSite)
declare @notrequiredlookup int;
select @notrequiredlookup =  iss.id
from lookupinstance iss
where iss.lookup = 'Lookup_ItemQuotedBypassCosting (OnSite)';

-- FAILURE_REPORT_NOT_REQUIRED
INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,lookupid)
	VALUES (1,@calactid,@notrequiredlookup);

declare @notrequiredos int;
select @notrequiredos = max(id)
from outcomestatus;

-- get lookup has failure report for onsite
declare @requiredlookup int;
select @requiredlookup =  iss.id
from lookupinstance iss
where iss.lookup = 'Lookup_ItemHasFaultReport (onsite)';

-- FAILURE_REPORT_REQUIRED
INSERT INTO dbo.outcomestatus (defaultoutcome,activityid,lookupid)
	VALUES (0,@calactid,@requiredlookup);

declare @requiredos int;
select @requiredos = max(id)
from outcomestatus;

-- create lookup results
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@oslu,@notrequiredos,0);
INSERT INTO dbo.lookupresult (lookupid,outcomestatusid,resultmessage)
	VALUES (@oslu,@requiredos,1);

	


INSERT INTO dbversion(version) VALUES (612);

COMMIT TRAN



