-- Author Galen Beck - 2016-09-12
-- Adjusts new contract review translation slightly per Miguel's email to GB
-- Relies on script 058 having alredy been run to insert translation

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[actionoutcometranslation]
   SET [translation] = 'Esperando env�a oferta al cliente'
 WHERE [locale] = 'es_ES' AND [id] IN (
 SELECT [id] FROM [dbo].[actionoutcome] WHERE [description] = 'Awaiting quotation to client');

GO

UPDATE [dbo].[itemstatetranslation] 
   SET [translation] = 'Revisi�n de contrato - esperando env�a oferta al cliente'
 WHERE [locale] = 'es_ES' AND [stateid] IN (
SELECT [stateid] FROM [dbo].[itemstate] WHERE [description] = 'Contract review - awaiting quotation to client');

GO

COMMIT TRAN