-- Script 733

-- Configures permissions for batch import features to move out of "development flag" mode and to be ready for release
-- These will be part of ROLE_UNRELEASED_FEATURES which will need to be added to one's production account manually 
--   (if post-release verification on production with live data is desired)
-- On all other machines (dev/test) GROUP_UNRELEASED_FEATURES will be part of ROLE_ADMINISTRATOR
-- The new permissions IMPORT_INSTRUMENTS_BATCH and IMPORT_QUOTATION_ITEMS_BATCH are now part of GROUP_UNRELEASED_FEATURES
-- This GROUP_UNRELEASED_FEATURES permissions group can change over time; when we're ready to release features to production,
--   a new script should remove the permission and add it to the appropriate group

-- Galen Beck 2020-11-16

USE [atlas]

BEGIN TRAN

INSERT INTO [dbo].[perrole]
           ([name])
     VALUES
           ('ROLE_UNRELEASED_FEATURES')

INSERT INTO [dbo].[permissiongroup]
           ([name])
     VALUES
           ('GROUP_UNRELEASED_FEATURES')
GO

DECLARE @role_id_unreleased int;
DECLARE @role_id_admin int;
DECLARE @group_id_unreleased int;

SELECT @role_id_unreleased = id from perrole WHERE perrole.name = 'ROLE_UNRELEASED_FEATURES'
SELECT @role_id_admin = id from perrole WHERE perrole.name = 'ROLE_ADMINISTRATOR'
SELECT @group_id_unreleased = id from permissiongroup WHERE permissiongroup.name = 'GROUP_UNRELEASED_FEATURES'

INSERT INTO [dbo].[permission]
           ([groupId]
           ,[permission])
     VALUES
           (@group_id_unreleased,'IMPORT_OPERATIONS_BATCH'),
		   (@group_id_unreleased,'IMPORT_QUOTATION_ITEMS_BATCH')

INSERT INTO [dbo].[rolegroup]
           ([roleId]
           ,[groupId])
     VALUES
           (@role_id_unreleased
           ,@group_id_unreleased)

-- On development and test machines, insert the GROUP_UNRELEASED_FEATURES into the ROLE_ADMINISTRATOR role
-- This preserves the status quo for the development / test environments
-- This statement below will be commented out on production

INSERT INTO [dbo].[rolegroup]
           ([roleId]
           ,[groupId])
     VALUES
           (@role_id_admin
           ,@group_id_unreleased)

INSERT INTO dbversion(version) VALUES(733);

COMMIT TRAN