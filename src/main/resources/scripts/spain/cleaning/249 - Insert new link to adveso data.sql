-- Edited slightly to use identity insert (and order of operations), 
-- as not all databases seem to have same increment values - GB 2018-05-03
USE [spain]
GO

BEGIN TRAN

	SET IDENTITY_INSERT [systemdefaultgroup] ON
	
	INSERT INTO spain.dbo.systemdefaultgroup (id,description,dname)
	VALUES (12,'Link to Adveso','Link - Adveso');

	SET IDENTITY_INSERT [systemdefaultgroup] OFF

	SET IDENTITY_INSERT [systemdefault] ON

	INSERT INTO spain.dbo.systemdefault (defaultid,defaultdatatype,defaultdescription,defaultname,defaultvalue,endd,[start],groupid,groupwide)
	VALUES (47,'boolean','Link to Adveso ','Link to Adveso','false',0,0,12,1);

	SET IDENTITY_INSERT [systemdefault] OFF
	
	INSERT INTO spain.dbo.systemdefaultcorole (coroleid,defaultid)
	VALUES (1,47);
	
	INSERT INTO spain.dbo.systemdefaultdescriptiontranslation (defaultid,locale,[translation])
	VALUES (47,'en_GB','Defines whether the business subdivision is related to adveso') 
	--go
	INSERT INTO spain.dbo.systemdefaultdescriptiontranslation (defaultid,locale,[translation])
	VALUES (47,'es_ES','Define si la subdivisión comercial está relacionada con adveso') 
	--go
	INSERT INTO spain.dbo.systemdefaultdescriptiontranslation (defaultid,locale,[translation])
	VALUES (47,'fr_FR','Définit si la business subdivision est liée à adveso');
	
	INSERT INTO spain.dbo.systemdefaultgroupdescriptiontranslation (id,locale,[translation])
	VALUES (12,'en_GB','Defines whether the business subdivision is related to adveso') 
	--go
	INSERT INTO spain.dbo.systemdefaultgroupdescriptiontranslation (id,locale,[translation])
	VALUES (12,'es_ES','Define si la subdivisión comercial está relacionada con adveso') 
	--go
	INSERT INTO spain.dbo.systemdefaultgroupdescriptiontranslation (id,locale,[translation])
	VALUES (12,'fr_FR','Définit si la business subdivision est liée à adveso');
	
	INSERT INTO spain.dbo.systemdefaultgroupnametranslation (id,locale,[translation])
	VALUES (12,'en_GB','Link to adveso');
	INSERT INTO spain.dbo.systemdefaultgroupnametranslation (id,locale,[translation])
	VALUES (12,'es_ES','Enlace a adveso');
	INSERT INTO spain.dbo.systemdefaultgroupnametranslation (id,locale,[translation])
	VALUES (12,'fr_FR','Lier à adveso');
	
	INSERT INTO spain.dbo.systemdefaultnametranslation (defaultid,locale,[translation])
	VALUES (47,'en_GB','Link to Adveso');
	INSERT INTO spain.dbo.systemdefaultnametranslation (defaultid,locale,[translation])
	VALUES (47,'es_ES','Enlace a adveso');
	INSERT INTO spain.dbo.systemdefaultnametranslation (defaultid,locale,[translation])
	VALUES (47,'fr_FR','Lier à adveso');
	
	INSERT INTO spain.dbo.systemdefaultscope ([scope],defaultid)
	VALUES (2,47);
	
	INSERT INTO dbversion (version) VALUES (249);

COMMIT TRAN