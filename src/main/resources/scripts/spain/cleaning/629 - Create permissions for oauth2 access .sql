-- Create permissions related to OAuth2 and Mobile Web services access

USE [atlas]
GO

BEGIN TRAN

	DECLARE @groupid_ws_core int
	DECLARE @groupid_ws_prebooking int
	DECLARE @groupid_ws_portal int
	DECLARE @groupid_ws_invoice int
	DECLARE @roleid_ws_core int
	DECLARE @roleid_ws_prebooking int
	DECLARE @roleid_ws_portal int
	DECLARE @roleid_ws_invoice int

	-- add permission groups --
	insert into dbo.permissiongroup(name) 
	values
	('GROUP_WEB_SERVICES_CORE'),
	('GROUP_WEB_SERVICES_PREBOOKING'),
	('GROUP_WEB_SERVICES_PORTAL'),
	('GROUP_WEB_SERVICES_INVOICE_CREDIT_NOTE');

	set @groupid_ws_core = (select id from dbo.permissiongroup where name like 'GROUP_WEB_SERVICES_CORE')
	set @groupid_ws_prebooking = (select id from dbo.permissiongroup where name like 'GROUP_WEB_SERVICES_PREBOOKING')
	set @groupid_ws_portal = (select id from dbo.permissiongroup where name like 'GROUP_WEB_SERVICES_PORTAL')
	set @groupid_ws_invoice = (select id from dbo.permissiongroup where name like 'GROUP_WEB_SERVICES_INVOICE_CREDIT_NOTE')

	-- add permissions --
	insert into permission(groupId, permission)  
    values
	(@groupid_ws_core, 'WEB_SERVICE_PING'),
	(@groupid_ws_core, 'WEB_SERVICE_GET_CURRENT_USER'),
	(@groupid_ws_prebooking, 'WEB_SERVICE_LOGISTICS_ADD_PREBOOKING'),
	(@groupid_ws_portal, 'WEB_SERVICE_PORTAL'),
	(@groupid_ws_invoice, 'WEB_SERVICE_SIDETRADE_UPLOAD_INVOICE_OR_CREDIT_NOTE');


	-- add roles --
	insert into dbo.perrole(name)
	values
	('ROLE_WEB_SERVICES_CORE'),
	('ROLE_WEB_SERVICES_PREBOOKING'),
	('ROLE_WEB_SERVICES_PORTAL'),
	('ROLE_WEB_SERVICES_INVOICE_CREDIT_NOTE');

	set @roleid_ws_core = (select id from dbo.perrole where name like 'ROLE_WEB_SERVICES_CORE')
	set @roleid_ws_prebooking = (select id from dbo.perrole where name like 'ROLE_WEB_SERVICES_PREBOOKING')
	set @roleid_ws_portal = (select id from dbo.perrole where name like 'ROLE_WEB_SERVICES_PORTAL')
	set @roleid_ws_invoice = (select id from dbo.perrole where name like 'ROLE_WEB_SERVICES_INVOICE_CREDIT_NOTE')

	-- add role groups --
	insert into rolegroup(roleId, groupId)
	values
	(@roleid_ws_core, @groupid_ws_core),
	(@roleid_ws_prebooking, @groupid_ws_prebooking),
	(@roleid_ws_portal, @groupid_ws_portal),
	(@roleid_ws_invoice, @groupid_ws_invoice)


   INSERT INTO dbversion(version) VALUES (629)

COMMIT TRAN