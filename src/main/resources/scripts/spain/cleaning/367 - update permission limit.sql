USE [spain]

Begin Tran

use spain



Create Table [spain].[dbo].permissionLimit(
id int identity not null,
permissionName varchar(255) not null,
typePermission varchar(255) not null,
levelPermission int not null,
primary key (id)
);

Create Table [spain].[dbo].limitCompany(
id int identity not null,
permLimitId int not null,
maximum numeric(10,2) not null,
orgid int not null,
lastModified datetime2(7) not null,
lastModifiedBy int null,
primary key (id)
);

CREATE UNIQUE INDEX  I_permissionlimit  
   ON dbo.limitCompany(permLimitId,orgid);
   Go

ALTER TABLE spain.dbo.limitCompany
ADD CONSTRAINT FK_limitCompany_company 
FOREIGN KEY(orgid) REFERENCES spain.dbo.company(coid);
Go

ALTER TABLE spain.dbo.limitCompany
ADD CONSTRAINT FK_limitCompany_contact
FOREIGN KEY(lastModifiedBy) REFERENCES spain.dbo.contact(personid);
Go

ALTER TABLE spain.dbo.limitCompany
ADD CONSTRAINT FK_limitCompany_permLimit
FOREIGN KEY(permLimitId) REFERENCES spain.dbo.permissionLimit(id);
Go

SET IDENTITY_INSERT dbo.permissionLimit on
Insert into dbo.permissionLimit(id,permissionName,typePermission,levelPermission)
values 
(1,'LOW_QUOTATION_LIMIT','Quotation',1),
(2,'MEDIUM_QUOTATION_LIMIT','Quotation',2),
(3,'HIGH_QUOTATION_LIMIT','Quotation',3),
(4,'OPEX_LOW_PURCHASE_LIMIT','PurchaseOpex',1),
(5,'OPEX_MEDIUM_PURCHASE_LIMIT','PurchaseOpex',2),
(6,'OPEX_HIGH_PURCHASE_LIMIT','PurchaseOpex',3),
(7,'GENEX_LOW_PURCHASE_LIMIT','PurchaseGenex',1),
(8,'GENEX_MEDIUM_PURCHASE_LIMIT','PurchaseGenex',2),
(9,'GENEX_HIGH_PURCHASE_LIMIT','PurchaseGenex',3),
(10,'CAPEX_LOW_PURCHASE_LIMIT','PurchaseCapex',1),
(11,'CAPEX_MEDIUM_PURCHASE_LIMIT','PurchaseCapex',2),
(12,'CAPEX_HIGH_PURCHASE_LIMIT','PurchaseCapex',3)

SET IDENTITY_INSERT dbo.permissionLimit off

INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 4, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='OPEX_LOW_PURCHASE_LIMIT';
  
INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 5, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='OPEX_MEDIUM_PURCHASE_LIMIT';

INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 6, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='OPEX_HIGH_PURCHASE_LIMIT';
  
  INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 7, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='GENEX_LOW_PURCHASE_LIMIT';

  INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 8, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='GENEX_MEDIUM_PURCHASE_LIMIT';

  INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 9, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='GENEX_HIGH_PURCHASE_LIMIT';

  INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 10, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='CAPEX_LOW_PURCHASE_LIMIT';

  INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 11, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='CAPEX_MEDIUM_PURCHASE_LIMIT';

  INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 12, maximum, orgid ,'2010-01-01 10:00:00.3814412'
  FROM dbo.purchasepermissionbycompany
  where permissionName='CAPEX_HIGH_PURCHASE_LIMIT';

INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 1, 0, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 2, 0, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

INSERT INTO spain.dbo.limitCompany(permLimitId,maximum,orgid,lastModified)
  SELECT 3, 0, coid ,'2010-01-01 10:00:00.3814412'
  FROM spain.dbo.company where corole=5;

 /* PO Subdiv reference */
ALTER TABLE dbo.purchaseorder
ADD subdivRef INTEGER;
 
ALTER TABLE spain.dbo.purchaseorder
ADD CONSTRAINT FK_poSubdivRef
FOREIGN KEY(subdivRef) REFERENCES spain.dbo.subdiv(subdivid);
Go


/*Version Not null*/

/*
update spain.dbo.purchaseorder set subdivRef=contact.subdivid from spain.dbo.purchaseorder 
  inner join spain.dbo.contact on purchaseorder.buspersonid = contact.personid

ALTER TABLE spain.dbo.purchaseorder ALTER COLUMN subdivRef INTEGER NOT NULL
*/

/* Replace by a generic table */  
drop table dbo.purchasepermissionbycompany;

 
INSERT INTO dbversion(version) VALUES(367);

COMMIT TRAN