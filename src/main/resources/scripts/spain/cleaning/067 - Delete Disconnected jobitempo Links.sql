-- Script to clean out / delete disconnected jobitempo links (approx 43 rows) resulting from BPO bug
-- To report rows before deletion, run U002 Find Job Item... script in utilities directory
-- That script should be manually verified to have same row count before committing this update
-- Author: Galen Beck - 2016-09-27

USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[jobitempo]
      WHERE [jobitempo].jobitempoid IN (
			SELECT [jobitempo].jobitempoid FROM [jobitempo]
				LEFT JOIN [jobitem] ON [jobitempo].jobitemid = [jobitem].jobitemid
				LEFT JOIN [job] ON [jobitem].jobid = [job].jobid
				 WHERE ([job].bpoid <> [jobitempo].bpoid) OR 
						([job].bpoid IS NULL AND [jobitempo].bpoid IS NOT NULL));
GO

ROLLBACK TRAN


