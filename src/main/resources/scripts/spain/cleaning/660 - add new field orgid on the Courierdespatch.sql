USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.courierdespatch ADD orgid INT;
	GO 
	
	-- update orgid of existing items
	UPDATE dbo.courierdespatch SET orgid = (SELECT subdivid FROM dbo.contact WHERE personid = createdbyid)
	
	ALTER TABLE dbo.courierdespatch ALTER COLUMN orgid INT NOT NULL;
	
	ALTER TABLE dbo.courierdespatch ADD CONSTRAINT fk_courierdespatch_orgid_subdiv 
	FOREIGN KEY (orgid) REFERENCES dbo.subdiv(subdivid);

	INSERT INTO dbversion(version) VALUES(660);

COMMIT TRAN