USE spain;

BEGIN TRANSACTION;

ALTER TABLE spain.dbo.contract
       ADD instrumentlisttype varchar(255);

CREATE TABLE dbo.instrument_contract (
       contractid INT NOT NULL,
        instrumentid INT NOT NULL
    );

ALTER TABLE dbo.instrument_contract
       ADD CONSTRAINT FK278e7wc3r04e8n72k7t59o1ab
       FOREIGN KEY (instrumentid)
       REFERENCES dbo.instrument;

ALTER TABLE dbo.instrument_contract
       ADD CONSTRAINT FKtmtfkkwt9lkq5i4ja7av2jfbg
       FOREIGN KEY (contractid)
       REFERENCES dbo.contract;

INSERT INTO dbversion(version) VALUES(388);

COMMIT TRANSACTION
