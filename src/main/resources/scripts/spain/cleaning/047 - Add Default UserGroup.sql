BEGIN TRAN

-- Author Tony Provost - 2016-06-30
-- Add 4 default group (not link to a company) for customer portal 

USE [spain]
GO

DECLARE @groupName VARCHAR(50) 

DECLARE @TempGroup TABLE (
	groupName VARCHAR(50) NOT NULL
	,groupDesc VARCHAR(255) NOT NULL 
	)

DECLARE @TempGroupValues TABLE (
	groupName VARCHAR(50) NOT NULL
	,groupType VARCHAR(200) NOT NULL
	,groupTypeValue VARCHAR(8) NOT NULL)
	
-- Populate temporary table
SET @groupName = 'Basic Personal'
INSERT INTO @TempGroup (groupName, groupdesc) VALUES (@groupName, 'The ''Basic Personal'' group allows a read-only mode access on the perimeter ''personal'' of the contact. Not editable')
INSERT INTO @TempGroupValues (groupName, groupType, groupTypeValue) VALUES (@groupName, 'Add instrument', 'no')
	,(@groupName, 'Book Jobs', 'no')
	,(@groupName, 'Certificate validation', 'none')
	,(@groupName, 'Manage', 'no')
	,(@groupName, 'Update', 'none')
	,(@groupName, 'View','personal')

SET @groupName = 'Basic Address'
INSERT INTO @TempGroup (groupName, groupdesc) VALUES (@groupName, 'The ''Basic Address'' group allows a read-only mode access on the perimeter ''address'' of the contact. Not editable')
INSERT INTO @TempGroupValues (groupName, groupType, groupTypeValue) VALUES (@groupName, 'Add instrument', 'no')
	,(@groupName, 'Book Jobs', 'no')
	,(@groupName, 'Certificate validation', 'none')
	,(@groupName, 'Manage', 'no')
	,(@groupName, 'Update', 'none')
	,(@groupName, 'View','address')

SET @groupName = 'Basic Subdivision'
INSERT INTO @TempGroup (groupName, groupdesc) VALUES (@groupName, 'The ''Basic Subdivision'' group allows a read-only mode access on the perimeter ''subdivision'' of the contact. Not editable')
INSERT INTO @TempGroupValues (groupName, groupType, groupTypeValue)
VALUES (@groupName, 'Add instrument', 'no')
	,(@groupName, 'Book Jobs', 'no')
	,(@groupName, 'Certificate validation', 'none')
	,(@groupName, 'Manage', 'no')
	,(@groupName, 'Update', 'none')
	,(@groupName, 'View','subdiv')

SET @groupName = 'Basic Company'
INSERT INTO @TempGroup (groupName, groupdesc) VALUES (@groupName, 'The ''Basic Company'' group allows a read-only mode access on the perimeter ''company'' of the contact. Not editable')
INSERT INTO @TempGroupValues (groupName, groupType, groupTypeValue)
VALUES (@groupName, 'Add instrument', 'no')
	,(@groupName, 'Book Jobs', 'no')
	,(@groupName, 'Certificate validation', 'none')
	,(@groupName, 'Manage', 'no')
	,(@groupName, 'Update', 'none')
	,(@groupName, 'View','company')

-- Insert new usergroup with their properties
INSERT INTO dbo.usergroup (name, description, systemdefault, compdefault)
	SELECT groupname, groupdesc, 0, 0
	FROM @TempGroup AS tmp
	WHERE NOT EXISTS (SELECT * FROM dbo.usergroup WHERE name = tmp.groupname COLLATE French_CI_AI)

INSERT INTO dbo.usergroupvalues (groupid, valueid)
	SELECT usergroup.groupid, usergrouptypevalue.valueid
	FROM dbo.usergrouptypevalue, @TempGroupvalues AS tmp, dbo.usergroup 
	WHERE usergroup.name = tmp.groupName COLLATE French_CI_AI
		AND usergrouptypevalue.propertyid = (
			SELECT propertyid
			FROM dbo.usergrouptype
			WHERE name = tmp.groupType COLLATE French_CI_AI
			)
		AND usergrouptypevalue.value = tmp.groupTypeValue COLLATE French_CI_AI
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.usergroupvalues, dbo.usergroup, dbo.usergrouptypevalue, dbo.usergrouptype
			WHERE usergroupvalues.groupid = usergroup.groupid
				AND usergroup.name = tmp.groupName COLLATE French_CI_AI
				AND usergrouptypevalue.propertyid = (
					SELECT propertyid
					FROM dbo.usergrouptype
					WHERE name = tmp.groupType COLLATE French_CI_AI
					)
				AND usergrouptypevalue.valueid = usergroupvalues.valueid
				AND usergrouptypevalue.value = tmp.groupTypeValue COLLATE French_CI_AI
			)
	ORDER BY usergrouptypevalue.propertyid

COMMIT TRAN
