-- Author Galen Beck

USE [spain]

BEGIN TRAN;

-- Step 1 - English Action Outcomes

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 102 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (102, 'en_GB', 'Calibrated successfully')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrated successfully' WHERE id = 102 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 103 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (103, 'en_GB', 'Unit B.E.R.')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Unit B.E.R.' WHERE id = 103 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 104 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (104, 'en_GB', 'Unit requires third party calibration')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Unit requires third party calibration' WHERE id = 104 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 105 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (105, 'en_GB', 'Unit requires third party repair')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Unit requires third party repair' WHERE id = 105 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 106 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (106, 'en_GB', 'Unit requires in-house repair')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Unit requires in-house repair' WHERE id = 106 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 107 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (107, 'en_GB', 'Calibrated - requires adjustment (pre-approved)')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrated - requires adjustment (pre-approved)' WHERE id = 107 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 108 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (108, 'en_GB', 'Calibrated - requires adjustment (not yet approved)')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrated - requires adjustment (not yet approved)' WHERE id = 108 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 109 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (109, 'en_GB', 'Calibrated - requires third party repair')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrated - requires third party repair' WHERE id = 109 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 110 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (110, 'en_GB', 'Calibrated - requires in-house repair')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrated - requires in-house repair' WHERE id = 110 AND locale = 'en_GB';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 111 AND locale = 'en_GB') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (102, 'en_GB', 'Calibration placed on hold')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibration placed on hold' WHERE id = 102 AND locale = 'en_GB';

-- Step 2 - Spanish Action Outcomes

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 102 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (102, 'es_ES', 'Calibrado con �xito')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrado con �xito' WHERE id = 102 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 103 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (103, 'es_ES', 'Unidad B.E.R.')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Unidad B.E.R.' WHERE id = 103 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 104 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (104, 'es_ES', 'La unidad requiere calibraci�n de terceros')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'La unidad requiere calibraci�n de terceros' WHERE id = 104 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 105 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (105, 'es_ES', 'La unidad requiere reparaci�n de terceros')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'La unidad requiere reparaci�n de terceros' WHERE id = 105 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 106 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (106, 'es_ES', 'La unidad requiere reparaci�n interna')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'La unidad requiere reparaci�n interna' WHERE id = 106 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 107 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (107, 'es_ES', 'Calibrado - requiere ajuste (pre-aprobado)')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrado - requiere ajuste (pre-aprobado)' WHERE id = 107 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 108 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (108, 'es_ES', 'Calibrado - requiere ajuste (todav�a no aprobado)')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrado - requiere ajuste (todav�a no aprobado)' WHERE id = 108 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 109 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (109, 'es_ES', 'Calibrado - requiere reparaci�n de terceros')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrado - requiere reparaci�n de terceros' WHERE id = 109 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 110 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (110, 'es_ES', 'Calibrado - requiere reparaci�n interna')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibrado - requiere reparaci�n interna' WHERE id = 110 AND locale = 'es_ES';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 111 AND locale = 'es_ES') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (111, 'es_ES', 'Calibraci�n puesta en espera')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibraci�n puesta en espera' WHERE id = 111 AND locale = 'es_ES';

-- Step 3 - French Action Outcomes

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 102 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (102, 'fr_FR', 'Calibr� avec succ�s')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibr� avec succ�s' WHERE id = 102 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 103 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (103, 'fr_FR', 'instrument �conomiquement non r�parable')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'instrument �conomiquement non r�parable' WHERE id = 103 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 104 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (104, 'fr_FR', 'L''instrument n�cessite un v�rification en sous-traitance')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'L''instrument n�cessite un v�rification en sous-traitance' WHERE id = 104 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 105 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (105, 'fr_FR', 'L''instrument n�cessite une r�paration en sous-traitance')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'L''instrument n�cessite une r�paration en sous-traitance' WHERE id = 105 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 106 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (106, 'fr_FR', 'L''instrument n�cessite une r�paration en interne')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'L''instrument n�cessite une r�paration en interne' WHERE id = 106 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 107 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (107, 'fr_FR', 'Calibr� - n�cessite un ajustage (pr�-approuv�)')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibr� - n�cessite un ajustage (pr�-approuv�)' WHERE id = 107 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 108 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (108, 'fr_FR', 'Calibr� - n�cessite un ajustage (pas encore approuv�)')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibr� - n�cessite un ajustage (pas encore approuv�)' WHERE id = 108 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 109 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (109, 'fr_FR', 'Calibr� - n�cessite une r�paration en sous-traitance')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibr� - n�cessite une r�paration en sous-traitance' WHERE id = 109 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 110 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (110, 'fr_FR', 'Calibr� - n�cessite une r�paration en interne')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibr� - n�cessite une r�paration en interne' WHERE id = 110 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.actionoutcometranslation WHERE id = 111 AND locale = 'fr_FR') 
	INSERT INTO dbo.actionoutcometranslation (id, locale, translation) VALUES (111, 'fr_FR', 'Calibration mise en attente')
	ELSE UPDATE dbo.actionoutcometranslation SET translation = 'Calibration mise en attente' WHERE id = 111 AND locale = 'fr_FR';

COMMIT TRAN;