-- Updates recall settings in preparation for next Iberia Recall
-- Sets global system default for recall generation to false
-- Then, inserts contact level "system default application" value for all iberia default contacts to true
--       (except for the 12558 subdivid IBERIA LINEAS AEREAS ESPA�A S.A.U. (Madrid), no recall to be sent)
-- Also ensures that all contacts have mail group membership for recall
-- Galen Beck - 2017-06-19

USE [spain]
GO

BEGIN TRAN

-- 1 row expected
UPDATE [dbo].[systemdefault]
   SET [defaultvalue] = 'false'
 WHERE [defaultid] = 1;

GO

-- ~138 rows expected (1 for each subdiv)
INSERT INTO [dbo].[systemdefaultapplication]
           ([defaultvalue]
           ,[coid]
           ,[personid]
           ,[subdivid]
           ,[defaultid]
           ,[lastModified]
           ,[orgid])
    SELECT 'true',null,contact.personid,null,1,'2017-06-19',6579
	FROM dbo.contact
	LEFT JOIN dbo.subdiv ON contact.subdivid = subdiv.subdivid
	WHERE subdiv.coid = 10153 AND subdiv.defpersonid = contact.personid AND subdiv.subdivid <> 12558;
GO

-- ~31 expected (some new contacts without mailgroupmembership)
INSERT INTO [dbo].[mailgroupmember]
           ([personid]
           ,[mailgroupid])
	SELECT contact.personid, 4 
	FROM contact
	INNER JOIN dbo.subdiv ON subdiv.subdivid = contact.subdivid 
	LEFT JOIN dbo.mailgroupmember ON contact.personid = mailgroupmember.personid AND mailgroupid = 4
	WHERE subdiv.coid = 10153 and mailgroupmember.mailgroupid IS NULL
	ORDER BY personid;
GO

COMMIT TRAN;
