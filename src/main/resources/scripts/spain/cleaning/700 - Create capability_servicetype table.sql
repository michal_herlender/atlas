-- GB note 2020-09-25 : I moved the insertion / migration part to script 701 as it would fail for me with recent full data sets
-- Some capability filter entries seem to have over 255 character comments (old size was up to 2000), size is expacted in script 701
-- If this script 700 has already been run locally, no need to re-run it, we could just run 701.

USE [atlas]
GO

BEGIN TRAN

	-- create the table "capability_servicetype"
	CREATE TABLE dbo.capability_servicetype (
		id INT IDENTITY NOT NULL PRIMARY KEY,
		lastModified DATETIME2 NOT NULL,
		bestlab BIT DEFAULT 0 NOT NULL,
		comments VARCHAR(255) NOT NULL,
		filtertype VARCHAR(255) NOT NULL,
		procedureid INT NOT NULL,
		lastModifiedBy INT,
		servicetypeid INT NOT NULL,
		CONSTRAINT FK_capabilityservicetype_procs FOREIGN KEY (procedureid) REFERENCES dbo.procs(id),
		CONSTRAINT FK_capabilityservicetype_contact FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid),
		CONSTRAINT FK_capabilityservicetype_servicetype FOREIGN KEY (servicetypeid) REFERENCES dbo.servicetype(servicetypeid)
	);

	INSERT INTO dbversion(version) VALUES(700);

COMMIT TRAN
