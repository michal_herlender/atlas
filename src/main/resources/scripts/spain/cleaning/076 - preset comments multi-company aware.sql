USE spain;
GO

UPDATE presetcomment SET orgid = 6579, lastModified = seton

UPDATE presetcomment SET type='ACTIVITY' WHERE type='activity'
UPDATE presetcomment SET type='CALIBRATION_REQUIREMENT' WHERE type='calibrationrequirement'
UPDATE presetcomment SET type='COMPLETE_CALIBRATION' WHERE type='completecalibration'
UPDATE presetcomment SET type='CONTRACT_REVIEW' WHERE type='contractreview'
UPDATE presetcomment SET type='EMAIL' WHERE type='email'
UPDATE presetcomment SET type='FAULT_ACTION' WHERE type='faultaction'
UPDATE presetcomment SET type='FAULT_DESCRIPTION' WHERE type='faultdescription'
UPDATE presetcomment SET type='INVOICE' WHERE type='invoice'
UPDATE presetcomment SET type='JOB' WHERE type='job'
UPDATE presetcomment SET type='JOB_COSTING' WHERE type='jobcosting'
UPDATE presetcomment SET type='JOB_COSTING_ITEM' WHERE type='jobcostingitem'
UPDATE presetcomment SET type='JOB_ITEM' WHERE type='jobitem'
UPDATE presetcomment SET type='ON_HOLD' WHERE type='onhold'
UPDATE presetcomment SET type='QUOTATION' WHERE type='quotation'
UPDATE presetcomment SET type='QUOTATION_ITEM' WHERE type='quotationitem'