/* Adds Supported Currency used by Trescal Companies - Author Tony Provost 2016-01-21 (http://www.xe.com/symbols.php) */
-- 2016-02-03 Tony Provost: Add currency for  Czech Republic

BEGIN TRAN 

USE [spain];
GO

ALTER TABLE [supportedcurrency] ALTER COLUMN currencysymbol varchar(3)
GO

SET IDENTITY_INSERT [supportedcurrency] ON
GO
  
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (1, '£', 'GBP', '&pound;', 'Sterling', '£', 0.73, 5, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (2, 'USD','USD', '$', 'Dollar', '$', 1.13, 5, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (3, 'IE01', 'EUR', '&euro;', 'Euro', '€', 1.00, 5, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (4, 'SGD', 'SGD', 'S$', 'Singapore Dollar', 'S$', 1.58, 5, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (5, 'DKK', 'DKK', '&oslash;re', 'Danish Krone', 'øre', 7.46272, 5, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (6, 'SEK', 'SEK', '&ouml;re', 'Swedish Krona', 'öre', 9.32891, 6, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (7, 'TND', 'TND', 'dt', 'Tunisian Dinar', 'dt', 2.22094, 7, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (8, 'MAD', 'MAD', 'mad', 'Moroccan Dirham', 'mad', 10.7538, 8, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (9, 'CAD', 'CAD', 'C$', 'Canadian dollar', 'C$', 1.55873, 9, GETDATE(), 1, 5064)
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (10, 'CHF', 'CHF', 'CHF', 'Swiss Franc', 'CHF', 1.09, 10, GETDATE(), 1, 5064)		
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (11, 'RON', 'RON', 'lei', 'Romanian Leu', 'lei', 4.52, 11, GETDATE(), 1, 5064)	
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (12, 'BRL', 'BRL', 'R$', 'Brazilian Real', 'R$', 4.51271, 12, GETDATE(), 1, 5064)												
INSERT INTO [supportedcurrency] ([currencyid], [accountscode], [currencycode], [currencyersymbol], [currencyname], [currencysymbol], [exchangerate], [orderby], [ratelastset], [rateset], [ratelastsetby])
VALUES (13, 'CZK', 'CZK', 'K&#269;', 'Czech Koruna', 'Kč', 26.9978 , 13, GETDATE(), 1, 5064)		
GO

SET IDENTITY_INSERT [supportedcurrency] OFF
GO

COMMIT TRAN
GO


