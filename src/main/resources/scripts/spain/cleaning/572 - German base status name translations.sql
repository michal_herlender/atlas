USE [atlas]
GO

BEGIN TRAN

DELETE FROM basestatusnametranslation WHERE locale = 'de_DE'
GO

INSERT INTO basestatusnametranslation(statusid, locale, translation)
VALUES
(1,'de_DE','Angefragt'),
(2,'de_DE','Angenommen'),
(3,'de_DE','Wartet auf Info'),
(4,'de_DE','Verloren'),
(5,'de_DE','Wartet auf Info von Kunde'),
(6,'de_DE','Wartet auf Fremd-Angebot'),
(7,'de_DE','Angabot an Kunde ausgeh�ndigt'),
(8,'de_DE','Nicht ausgef�hrt'),
(9,'de_DE','M�ndliches Angebot'),
(10,'de_DE','Nur Preiskalkulation - Nicht an Kunde ausgeh�ndigt'),
(11,'de_DE','Erfordert Preisfreigabe'),
(12,'de_DE','Angebot auf Webseite angelegt, erfordert Freigabe'),
(13,'de_DE','Nicht ausgeh�ndigt'),
(14,'de_DE','Angefragt'),
(15,'de_DE','Eingegangen'),
(16,'de_DE','M�ndlich eingegangen'),
(17,'de_DE','Angebot eingegangen'),
(18,'de_DE','Fax eingegangen'),
(19,'de_DE','E-Mail eingegangen'),
(23,'de_DE','Neu'),
(24,'de_DE','Gel�st'),
(25,'de_DE','Aktion l�uft'),
(26,'de_DE','Erstellt'),
(27,'de_DE','Ausgeh�ndigt'),
(28,'de_DE','Verworfen'),
(29,'de_DE','Wartet auf Bearbeitung'),
(30,'de_DE','L�uft'),
(31,'de_DE','Abgebrochen'),
(32,'de_DE','Fertiggestellt'),
(33,'de_DE','Wir k�nnen nicht kalibrieren'),
(34,'de_DE','"auf Halt"'),
(35,'de_DE','Nicht unterschrieben - wartet auf Unterschrift'),
(36,'de_DE','Reserviert'),
(37,'de_DE','Unterschrieben - wartet auf Freigabe'),
(38,'de_DE','Unterschrieben'),
(39,'de_DE','Unterschreiben nicht unterst�tzt'),
(40,'de_DE','Im Aufbau'),
(41,'de_DE','Wartet auf Freigabe'),
(42,'de_DE','Ausgegeben'),
(43,'de_DE','Verworfen'),
(44,'de_DE','Geht auf Angebot zur�ck'),
(45,'de_DE','B.E.R. (Reparatur unwirtschaftlich)'),
(46,'de_DE','Besch�digt'),
(47,'de_DE','Verloren'),
(48,'de_DE','Wird gerade repariert'),
(49,'de_DE','Verf�gbar'),
(52,'de_DE','Anfrage'),
(53,'de_DE','Mietvertrag'),
(54,'de_DE','Abgelehnt'),
(55,'de_DE','Fertiggestellt'),
(56,'de_DE','Wartet auf Rechnung'),
(57,'de_DE','Stammt von Webseite'),
(58,'de_DE','Preise freigegeben, fertig zur Ausgabe'),
(59,'de_DE','Geht auf Angebot zur�ck'),
(60,'de_DE','Wir k�nnen nicht anbieten, au�erhalb Leistungsumfang'),
(61,'de_DE','Veraltet'),
(62,'de_DE','Muss ersetzt werden'),
(63,'de_DE','Ersetzt')

INSERT INTO dbversion(version) VALUES (572)

COMMIT TRAN