USE [atlas]
GO

BEGIN TRAN

	INSERT INTO dbo.permission(groupId, permission)
	VALUES (1 ,'GOOD_OUT_SCHEDULE_UPDATER');

	INSERT INTO dbversion(version) VALUES(752);

	GO

COMMIT TRAN