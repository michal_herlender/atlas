USE [spain]

Begin Tran

 /* Remove PO Subdiv reference */
ALTER TABLE dbo.purchaseorder
DROP  FK_poSubdivRef;

ALTER TABLE dbo.purchaseorder
DROP COLUMN subdivRef;
 
INSERT INTO dbversion(version) VALUES(369);

COMMIT TRAN