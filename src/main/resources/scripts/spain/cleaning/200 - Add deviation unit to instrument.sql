USE spain;

BEGIN TRAN;

ALTER TABLE dbo.instrumentcomplementaryfield
  ADD uomid INT;

ALTER TABLE dbo.instrumentcomplementaryfield
  ADD CONSTRAINT FK_compfields_to_uom
FOREIGN KEY (uomid)
REFERENCES dbo.uom;

INSERT INTO uomnametranslation (id,locale,translation) VALUES (61,'en_GB','');

INSERT INTO uomnametranslation (id,locale,translation) VALUES (61,'es_ES','');

INSERT INTO dbversion (version) VALUES (200);

COMMIT TRAN;