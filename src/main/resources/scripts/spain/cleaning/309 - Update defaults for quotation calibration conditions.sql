-- Updates default text for quotation renaming and inserts some additional values related to service type renaming
-- Should only be run once, otherwise duplicate values will be inserted (though OK for development)

USE [spain]
GO

BEGIN TRAN

UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*ET-AC-SI= Etalonnage accr�dit�e sur site (certification COFRAC-Essai n�1-6190)</strong></p>' WHERE caltypeid=25 AND orgid=6685
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*ET-TR-LA= Etalonnage en laboratoire non accr�dit� (raccordement aux �talons nationaux)</strong></p>' WHERE caltypeid=2 AND orgid=6685
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*VE-TR-LA= V�rification en laboratoire non accr�dit� (raccordement aux �talons nationaux)</strong></p>' WHERE caltypeid=48 AND orgid=6685
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*ET-TR-SI= Etalonnage sur site non accr�dit� (raccordement aux �talons nationaux)</strong></p>' WHERE caltypeid=29 AND orgid=6685
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*VE-TR-SI= V�rification sur site non accr�dit� (raccordement aux �talons nationaux)</strong></p>' WHERE caltypeid=50 AND orgid=6685
INSERT INTO [dbo].[defaultquotationcalibrationcondition] ([caltypeid],[lastModified],[orgid],[lastModifiedBy],[conditiontext]) VALUES
(49,'2018-09-29',6685,17280,'<br/><br/><p><strong>*VE-AC-SI= V�rification accr�dit�e sur site (certification COFRAC-Essai n�1-6190)</strong></p>'),
(48,'2018-09-29',6578,17280,'<br/><br/><p><strong>*ST-CAR-EL= Calibraci�n en laboratorio no acreditada con verificaci�n CAR (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>'),
(48,'2018-09-29',6579,17280,'<br/><br/><p><strong>*ST-CAR-EL= Calibraci�n en laboratorio no acreditada con verificaci�n CAR (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>'),
(48,'2018-09-29',6670,17280,'<br/><br/><p><strong>*ST-CAR-EL= Calibraci�n en laboratorio no acreditada con verificaci�n CAR (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>'),
(47,'2018-09-29',6578,17280,'<br/><br/><p><strong>*AC-CAR-EL= Calibraci�n en laboratorio acreditada con verificaci�n CAR bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>'),
(47,'2018-09-29',6579,17280,'<br/><br/><p><strong>*AC-CAR-EL= Calibraci�n en laboratorio acreditada con verificaci�n CAR bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>'),
(47,'2018-09-29',6670,17280,'<br/><br/><p><strong>*AC-CAR-EL= Calibraci�n en laboratorio acreditada con verificaci�n CAR bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>'),
(50,'2018-09-29',6578,17280,'<br/><br/><p><strong>*ST-CAR-IS= Calibraci�n in situ no acreditada con verificaci�n CAR (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>'),
(50,'2018-09-29',6579,17280,'<br/><br/><p><strong>*ST-CAR-IS= Calibraci�n in situ no acreditada con verificaci�n CAR (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>'),
(50,'2018-09-29',6670,17280,'<br/><br/><p><strong>*ST-CAR-IS= Calibraci�n in situ no acreditada con verificaci�n CAR (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>'),
(49,'2018-09-29',6578,17280,'<br/><br/><p><strong>*AC-CAR-IS= Calibraci�n in situ acreditada con verificaci�n CAR bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>'),
(49,'2018-09-29',6579,17280,'<br/><br/><p><strong>*AC-CAR-IS= Calibraci�n in situ acreditada con verificaci�n CAR bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>'),
(49,'2018-09-29',6670,17280,'<br/><br/><p><strong>*AC-CAR-IS= Calibraci�n in situ acreditada con verificaci�n CAR bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>');
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*AC-EX= Calibraci�n por laboratorio distinto a Trescal Espa�a acreditada bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>' WHERE caltypeid=33 AND orgid=6578
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*AC-EX= Calibraci�n por laboratorio distinto a Trescal Espa�a acreditada bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>' WHERE caltypeid=33 AND orgid=6579
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*AC-EX= Calibraci�n por laboratorio distinto a Trescal Espa�a acreditada bajo norma UNE EN ISO 17025 (certificado con marca ILAC-ENAC o equivalente)</strong></p>' WHERE caltypeid=33 AND orgid=6670
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*ST-EX= Calibraci�n por laboratorio distinto a Trescal Espa�a no acreditada (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>' WHERE caltypeid=37 AND orgid=6578
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*ST-EX= Calibraci�n por laboratorio distinto a Trescal Espa�a no acreditada (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>' WHERE caltypeid=37 AND orgid=6579
UPDATE [defaultquotationcalibrationcondition] SET [conditiontext]='<br/><br/><p><strong>*ST-EX= Calibraci�n por laboratorio distinto a Trescal Espa�a no acreditada (no cubierta por la acreditaci�n ENAC ni por acuerdos internacionales)</strong></p>' WHERE caltypeid=37 AND orgid=6670

INSERT INTO dbversion(version) VALUES(309);

COMMIT TRAN