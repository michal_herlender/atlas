-- Updates the new logistics hold states to be in a consistent state group, just to show for current subdiv.

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[stategrouplink]
   SET [type] = 'CURRENT_SUBDIV'
 WHERE stateid in (4096, 4097);

INSERT INTO dbversion(version) VALUES(357);

COMMIT TRAN