-- It seems there can be quite long descriptions in the US
-- So far "Central Ohio Transit Authority Tax" = 34 characters but sure there could be longer ones...
-- Expanding from 30 to 100 characters to resolve DEV-2697

use atlas;
go

begin tran

ALTER TABLE creditnotetax ALTER COLUMN description varchar(100);
ALTER TABLE invoicetax ALTER COLUMN description varchar(100);

insert into dbversion values (827)

commit tran