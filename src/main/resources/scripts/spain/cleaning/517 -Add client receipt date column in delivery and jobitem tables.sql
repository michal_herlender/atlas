--Add clientreceiptdate column in jobitem and delivery tables
USE [atlas]
GO

BEGIN TRAN

    ALTER TABLE dbo.jobitem ADD clientreceiptdate datetime2;
    ALTER TABLE dbo.delivery ADD clientreceiptdate datetime2;
    
	INSERT INTO dbversion(version) VALUES(517);

COMMIT TRAN