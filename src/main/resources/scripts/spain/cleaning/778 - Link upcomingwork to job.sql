-- Some configuration for US requirements
-- Add calibration process for Calypso, add sticker format in MM/dd/yyyy format
-- Also adds a new table linking upcomingwork to job, (upcomingworkjoblink)
-- will use in DEV-2306 / DEV-2307 during March, but first adding so that we could define data export format

USE [atlas]
GO

BEGIN TRAN

create table dbo.upcomingworkjoblink (
   id int identity not null,
	active bit not null,
	jobid int not null,
	upcomingworkid int not null,
	primary key (id)
);

alter table dbo.upcomingworkjoblink 
   add constraint FK_upcomingworkjoblink_jobid 
   foreign key (jobid) 
   references dbo.job;

alter table dbo.upcomingworkjoblink 
   add constraint FK_upcomingworkjoblink_upcomingworkid 
   foreign key (upcomingworkid) 
   references dbo.upcomingwork;	

INSERT INTO [dbo].[calibrationprocess]
           ([actionafterissue]
           ,[applicationcomponent]
           ,[description]
           ,[fileextension]
           ,[issueimmediately]
           ,[process]
           ,[quicksign]
           ,[signonissue]
           ,[signingsupported]
           ,[uploadafterissue]
           ,[uploadbeforeissue]
           ,[toollink]
           ,[toolcontinuelink])
     VALUES
           (0
           ,0
           ,'Calypso is used for calibration'
           ,'.pdf'
           ,0
           ,'Calypso'
           ,0
           ,0
           ,0
           ,0
           ,1
           ,null
           ,null)
GO

insert into dbversion(version) values(778);

COMMIT TRAN