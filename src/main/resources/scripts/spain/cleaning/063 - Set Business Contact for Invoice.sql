-- Author Galen Beck - 2016-09-19
-- Initializes businesscontactid field on invoice

USE [spain]
GO

UPDATE [dbo].[invoice]
   SET [businesscontactid] = [createdby]
 WHERE [businesscontactid] IS NULL;
GO

