BEGIN TRAN

UPDATE jobexpenseitem SET quantity = 1 WHERE quantity IS NULL

GO

ALTER TABLE jobexpenseitem ALTER COLUMN quantity INT NOT NULL

GO

INSERT INTO dbversion VALUES (708)

COMMIT TRAN