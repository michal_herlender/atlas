USE [spain]
GO

BEGIN TRAN

DELETE FROM dbo.itemstatetranslation
WHERE locale = 'de_DE' AND translation=''

INSERT INTO transportoption(active, transportmethodid, subdivid, lastModified, lastModifiedBy)
SELECT 1, transportmethod.id, subdiv.subdivid, '2018-08-27', 8740
FROM subdiv, transportmethod
WHERE subdiv.coid = 6681 AND transportmethod.id != 1

INSERT INTO dbversion(version) VALUES(286);

COMMIT TRAN