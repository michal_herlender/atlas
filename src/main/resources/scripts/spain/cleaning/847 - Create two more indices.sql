-- From query regression / resource analyzer on production 2021-10-13

BEGIN TRAN

CREATE NONCLUSTERED INDEX [IDX_calreq_type_active_compid_modelid]
ON [dbo].[calreq] ([type],[active],[compid],[modelid])
INCLUDE ([deactivatedon],[instructions],[deactivatedbyid],[pointsetid],[publicinstructions],[publish],[lastModified],[lastModifiedBy],[calibrationrangeid])

CREATE NONCLUSTERED INDEX [IDX_calreq_type_active_modelid]
ON [dbo].[calreq] ([type],[active],[modelid])
INCLUDE ([deactivatedon],[instructions],[deactivatedbyid],[pointsetid],[publicinstructions],[publish],[lastModified],[lastModifiedBy],[calibrationrangeid])

INSERT INTO dbversion values (847);

COMMIT TRAN