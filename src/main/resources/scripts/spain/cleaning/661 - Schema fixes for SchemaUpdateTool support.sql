-- Script 661 - Hibernate Schema vs SQL cleanup
-- Adds a number of missing constraints (and a couple fields) such that the Hibernate SchemaUpdateTool doesn't report any extra missing items.
-- Galen Beck - 2020-07-07

USE [atlas]

BEGIN TRAN

    alter table dbo.componentinventory 
       add lastModified datetime2;

    alter table dbo.componentinventory 
       add lastModifiedBy int;

    alter table dbo.advesojobItemactivityqueue 
       add constraint FKcj38ooqvfv234p2xa8vch3o8u 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.advesooverriddenactivitysetting 
       add constraint FK54ntg235fs4h6aimljuu8ms2r 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.advesotransferableactivity 
       add constraint FKhi1aw09c0777j636yhg629x89 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.asnitem 
       add constraint FK_asnitem_servicetypeid 
       foreign key (servicetypeid) 
       references dbo.servicetype;

    alter table dbo.componentinventory 
       add constraint FKifmo24cpbeivwpuc94c5ivej6 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.exchangeformat 
       add constraint FK7yux4wb4s28qivwj0hfwtmns4 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.exchangeformatfile 
       add constraint FKj323esy0yxquyt2kd28gi25s6 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.exchangeformatfile 
       add constraint FK_exchangeformatfile_personid 
       foreign key (personid) 
       references dbo.contact;

    alter table dbo.jobcosting 
       add constraint FK_jobcosting_statusid 
       foreign key (statusid) 
       references dbo.basestatus;

    alter table dbo.notificationsystemqueue 
       add constraint FKc1kagvpi4benpmp4hoobmexd4 
       foreign key (lastModifiedBy) 
       references dbo.contact;

    alter table dbo.quotation 
       add constraint FK_quotation_sourceaddressid 
       foreign key (sourceaddressid) 
       references dbo.address;

	INSERT INTO dbversion(version) VALUES(661);

COMMIT TRAN