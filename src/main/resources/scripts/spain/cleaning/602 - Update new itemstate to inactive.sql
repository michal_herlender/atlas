-- Fixes a little issue with the new 'Job item completed at same address as instrument address'
-- where the itemstate was 'active' from script 598.

USE [atlas]
GO

BEGIN TRAN

DECLARE @itemstatus int;
SELECT @itemstatus = stateid FROM dbo.itemstate WHERE description = 'Job item completed at same address as instrument address';
PRINT 'Updating state id:'
PRINT @itemstatus;

UPDATE dbo.itemstate SET active = 0 WHERE stateid = @itemstatus;
GO

-- Update job items to set date complete to 2020-03-20 (just today on production)

DECLARE @itemstatus int;
SELECT @itemstatus = stateid FROM dbo.itemstate WHERE description = 'Job item completed at same address as instrument address';
PRINT 'Updating state id:'
PRINT @itemstatus;

PRINT 'Setting datecomplete on job items'

UPDATE dbo.jobitem SET datecomplete = '2020-03-20' WHERE stateid = @itemstatus;

-- Update job status to 'complete' and completion date, for any job with job items in this state, where all job items are complete

DECLARE @potentialjobcount int;
SELECT @potentialjobcount = COUNT (DISTINCT jobid) FROM jobitem WHERE stateid = @itemstatus;

PRINT 'Potentially affected jobs'
PRINT @potentialjobcount;

DECLARE @jobstatus int;
SELECT @jobstatus = dbo.jobstatus.statusid FROM dbo.jobstatus WHERE name = 'Complete';

PRINT 'Completed jobstatus.statusid:'
PRINT @jobstatus;

PRINT 'Updating datecomplete, jobstatus on jobs'

UPDATE dbo.job SET datecomplete = '2020-03-20', statusid = @jobstatus 
WHERE (jobid in (SELECT DISTINCT jobid FROM jobitem WHERE stateid = @itemstatus))

-- Note, the above assumes that the jobs don't have job items in active statuses; display all job items below to check.

SELECT ji.jobid, ji.itemno, ji.stateid, ist.active 
	FROM jobitem ji 
	LEFT JOIN itemstate ist on ist.stateid = ji.stateid 
	WHERE ji.jobid IN 
		(SELECT DISTINCT jobid FROM jobitem WHERE stateid = @itemstatus);

INSERT INTO dbversion(version) VALUES (602);

COMMIT TRAN