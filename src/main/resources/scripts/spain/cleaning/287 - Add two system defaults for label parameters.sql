-- Inserts and configures system defaults 48 for cal label date format, and 49 for cal label technician name
-- Galen Beck - 2018-09-05

USE [spain]
GO

BEGIN TRAN

DELETE FROM [systemdefaultcorole] WHERE defaultid in (48, 49);
GO

DELETE FROM [systemdefaultnametranslation] WHERE defaultid in (48, 49);
GO

DELETE FROM [systemdefaultdescriptiontranslation] WHERE [defaultid] in (48, 49);
GO

DELETE FROM [systemdefaultscope] WHERE defaultid in (48, 49);
GO

DELETE FROM [systemdefault] WHERE [defaultid] in (48, 49);
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
	(48, 'string', 'Defines the date format for calibration labels', 'Date format for calibration labels', 'dd-MM-yyyy', 0, 0, 10, 1),
	(49, 'boolean', 'Prints the technician name on calibration labels', 'Technician name on calibration labels', 'false', 0, 0, 10, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (48, 'en_GB', 'Defines the date format for calibration labels'),
  (48, 'es_ES', 'Define el formato '),
  (48, 'fr_FR', 'D�finit le format de date pour �tiquettes de v�rification'),
  (48, 'de_DE', 'Definiert das Datumsformat f�r Kalibrier-Aufkleber');

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (49, 'en_GB', 'Prints the technician name on calibration labels'),
  (49, 'es_ES', 'Imprime el nombre del t�cnico en las etiquetas de calibraci�n'),
  (49, 'fr_FR', 'Imprime le nom du technicien sur les �tiquettes de v�rification'),
  (49, 'de_DE', 'Drucken Sie den Namen des Technikers auf Kalibrier-Aufkleber');

-- Applies to companies (0), contacts (1), and subdivisions (2)

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0, 48), (1, 48), (2, 48),
  (0, 49), (1, 49), (2, 49);
GO

-- Applies to client and business company roles only

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (1, 48), (5, 48),
  (1, 49), (5, 49);

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (48, 'en_GB', 'Date format for calibration labels'),
  (48, 'es_ES', 'Formato de fecha para etiquetas de calibraci�n'),
  (48, 'fr_FR', 'Format de date pour �tiquettes de v�rification'),
  (48, 'de_DE', 'Datumsformat f�r Kalibrier-Aufkleber');
GO

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (49, 'en_GB', 'Technician name on calibration labels'),
  (49, 'es_ES', 'El nombre del t�cnico en las etiquetas de calibraci�n'),
  (49, 'fr_FR', 'Nom du technicien sur les �tiquettes de v�rification'),
  (49, 'de_DE', 'Name des Technikers auf Kalibrier-Aufkleber');
GO

INSERT INTO dbversion(version) VALUES(287);

COMMIT TRAN