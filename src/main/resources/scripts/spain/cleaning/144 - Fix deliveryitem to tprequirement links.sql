-- This script corrects existing "bad data" where there are multiple delivery items for 
-- the same third party requirement; the delivery items are deleted or updated as appropriate.
-- Finally the existing index is dropped and a new unique index that tolerates nulls is created

-- Author Galen Beck - 2017-05-08

USE [spain]
GO

BEGIN TRAN

DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 236;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 236;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 236;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 235;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 235;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 235;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 3146;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 3146;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 3146;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 8794;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 8794;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 8794;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 7028;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 7028;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 7028;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 5625;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 5625;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 5625;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 12541;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 12541;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 12541;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 13002;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 13002;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 13002;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 12318;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 12318;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 12318;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 14570;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 14570;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 14570;
GO
UPDATE [dbo].[deliveryitem] SET [tprequirementid] = 1958 WHERE [deliveryitemid] = 20409;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 18405;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 18405;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 18405;
GO
UPDATE [dbo].[deliveryitem] SET [tprequirementid] = 2010 WHERE [deliveryitemid] = 20685;
GO
UPDATE [dbo].[deliveryitem] SET [tprequirementid] = 1960 WHERE [deliveryitemid] = 20410;
GO
UPDATE [dbo].[deliveryitem] SET [tprequirementid] = 1963 WHERE [deliveryitemid] = 20413;
GO
UPDATE [dbo].[deliveryitem] SET [tprequirementid] = 1929 WHERE [deliveryitemid] = 20585;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 18765;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 18765;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 18765;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 18766;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 18766;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 18766;
GO
DELETE FROM [dbo].[deliveryitemaccessory] WHERE [deliveryitemid] = 18767;
DELETE FROM [dbo].[deliveryitemnote] WHERE [deliveryitemid] = 18767;
DELETE FROM [dbo].[deliveryitem] WHERE [deliveryitemid] = 18767;
GO

-- No need for FK86C02847382C95FD and deliveryitemid column in tprequirement!
DROP INDEX IDX_tprequirement_deliveryitemid ON [tprequirement];
ALTER TABLE [tprequirement] DROP CONSTRAINT FK86C02847382C95FD;
ALTER TABLE [tprequirement] DROP COLUMN deliveryitemid;

-- If the above does not run for you locally, there may be additional duplicate delivery items to delete on your DB copy
-- The following query can detect them (run separately) and then resolve manually / add to personal script if needed
-- SELECT [jobitemid],[tprequirementid],COUNT(1) AS DI_Count
--  FROM [dbo].[deliveryitem] 
--  WHERE [tprequirementid] is not null
--  GROUP BY [jobitemid],[tprequirementid]
--  HAVING COUNT(1) > 1;

DROP INDEX IDX_deliveryitem_tprequirementid ON [dbo].[deliveryitem];

-- Note, this may indicate a syntax error but will run OK as the index is deleted first above...
CREATE UNIQUE INDEX IDX_deliveryitem_tprequirementid ON [dbo].[deliveryitem](tprequirementid) WHERE tprequirementid IS NOT NULL;

COMMIT TRAN

GO