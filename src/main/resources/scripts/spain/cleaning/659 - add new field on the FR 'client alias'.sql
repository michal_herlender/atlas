USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE faultreport ADD clientalias varchar(100) NULL;
	
	GO

	INSERT INTO dbversion(version) VALUES(659);

COMMIT TRAN