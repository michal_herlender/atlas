BEGIN TRAN;

UPDATE servicetype SET canbeperformedbyclient = 0;

UPDATE servicetype SET canbeperformedbyclient = 1
WHERE shortname LIKE '%EX%';

UPDATE servicetype SET canbeperformedbyclient = 1
WHERE shortname = 'M';

UPDATE servicetype SET canbeperformedbyclient = 1
WHERE shortname = 'IC';

COMMIT TRAN;