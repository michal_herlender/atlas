USE [spain]

BEGIN TRAN
	IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'businessdocumentsettings')
		DROP TABLE businessdocumentsettings
	GO

    create table dbo.businessdocumentsettings (
       id int identity not null,
        recipientAddressOnLeft bit not null,
        globalDefault bit not null,
        invoiceAmountText bit not null,
        businesscompanyid int,
        primary key (id)
    );
	GO

	SET IDENTITY_INSERT businessdocumentsettings ON
	GO

	-- Special business companies configured here
	-- 6681 = Trescal GmbH (Germany)
	-- 6685 = Trescal Maroc (Morocco)

	INSERT INTO dbo.businessdocumentsettings 
		(id,recipientAddressOnLeft,globalDefault,invoiceAmountText,businesscompanyid)
		VALUES
		(1,0,1,0,null),
		(2,1,0,1,6681),
		(3,0,0,1,6685)
	GO

	SET IDENTITY_INSERT businessdocumentsettings OFF
	GO

	INSERT INTO dbversion(version) VALUES(408)
	GO

COMMIT TRAN