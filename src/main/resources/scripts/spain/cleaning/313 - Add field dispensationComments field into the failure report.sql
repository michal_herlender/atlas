USE [spain]

BEGIN TRAN

	ALTER TABLE dbo.faultreport ADD dispensationcomments varchar(100) 
	go

	INSERT INTO dbversion(version) VALUES(313);

COMMIT TRAN