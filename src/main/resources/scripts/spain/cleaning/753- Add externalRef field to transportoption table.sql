USE [atlas]
GO

BEGIN TRAN

	ALTER TABLE dbo.transportoption ADD externalref VARCHAR(10) NULL;

	INSERT INTO dbversion(version) VALUES(753);

	GO

COMMIT TRAN