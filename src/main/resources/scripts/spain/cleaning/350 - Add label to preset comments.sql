use spain;

begin tran

alter table spain.dbo.presetcomment 
add label varchar(50);

insert into dbversion(version) values (350);

commit tran