USE [atlas]
GO

BEGIN TRAN

UPDATE company set documentlanguage ='en_GB' where countryid!=(select cou.countryid from country cou where cou.country='france') and 
countryid!=(select cou.countryid from country cou where cou.country='Morocco') and 
countryid!=(select cou.countryid from country cou where cou.country='spain') and 
countryid!=(select cou.countryid from country cou where cou.country='Germany') 

UPDATE company set documentlanguage ='fr_FR' where countryid=(select cou.countryid from country cou where cou.country='france') 
UPDATE company set documentlanguage ='fr_FR' where countryid=(select cou.countryid from country cou where cou.country='Morocco') 
UPDATE company set documentlanguage ='es_ES' where countryid=(select cou.countryid from country cou where cou.country='spain') 
UPDATE company set documentlanguage ='de_DE' where countryid=(select cou.countryid from country cou where cou.country='Germany')

ALTER TABLE company ALTER COLUMN documentlanguage varchar(255) not null;

INSERT INTO dbversion(version) VALUES(496);

COMMIT TRAN 