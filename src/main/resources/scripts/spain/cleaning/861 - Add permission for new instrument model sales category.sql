-- GB Note 2022-02-03 : I renumbered this script to 861 just as we also had a script 859 at approximately the same time...
-- and for now am using the permission group 'GROUP_CAPABILITY_MANAGEMENT' as most likely those that manage capabilities... 
-- would manage sales category assignment to instrument models.

BEGIN TRAN


	DECLARE @PermissionGroupId INT;
	SELECT @PermissionGroupId= id FROM dbo.permissiongroup WHERE name='GROUP_CAPABILITY_MANAGEMENT';

	INSERT INTO permission (groupId, permission) VALUES(@PermissionGroupId, 'INSTRUMENT_MODEL_SALES_CATEGORY');

INSERT INTO dbversion(version) VALUES(861);

COMMIT TRAN