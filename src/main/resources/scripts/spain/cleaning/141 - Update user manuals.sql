USE [spain]

BEGIN TRAN

INSERT INTO dbo.usermanual (filebinary, locale, filename)
VALUES ((SELECT * FROM OPENROWSET(BULK 'F:\MSSQL\manual_es.pdf', SINGLE_BLOB) rs),'es_ES','manual_es.pdf'),
	((SELECT * FROM OPENROWSET(BULK 'F:\MSSQL\manual_en.pdf', SINGLE_BLOB) rs),'en_GB','manual_en.pdf')

ROLBACK TRAN