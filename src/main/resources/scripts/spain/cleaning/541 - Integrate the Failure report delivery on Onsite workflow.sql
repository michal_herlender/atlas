
USE [atlas]
GO

BEGIN TRAN 

DECLARE @activity_4050 INT,
		@activity_4077 INT,
		@status_4051 INT,
		@lookup_1113 INT,
		@new_os INT;

SELECT @activity_4050 = stateid FROM dbo.itemstate WHERE description = 'Onsite job costing issued to client';
SELECT @activity_4077 = stateid FROM dbo.itemstate WHERE description = 'Onsite - inspection costing issued to client';
SELECT @status_4051 = stateid FROM dbo.itemstate WHERE description = 'Completed onsite service';
SELECT @lookup_1113 = id FROM dbo.lookupinstance WHERE lookup = 'Lookup_FailureReportsRequireDelivery';

--delete outcomestatus
DELETE FROM dbo.outcomestatus WHERE activityid = @activity_4050 and statusid = @status_4051;
DELETE FROM dbo.outcomestatus WHERE activityid = @activity_4077 and statusid = @status_4051;

--set lookup to activities
UPDATE dbo.itemstate SET lookupid = @lookup_1113 WHERE stateid IN (@activity_4050, @activity_4077);

--add new outcomestatus
INSERT INTO dbo.outcomestatus(activityid,defaultoutcome,lookupid,statusid)
VALUES (NULL,0,NULL,@status_4051);
SELECT @new_os = MAX(id) FROM dbo.outcomestatus;

--add new lookupresult
INSERT INTO dbo.lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage)
VALUES(NULL,@lookup_1113,@new_os,NULL,2);

INSERT INTO dbversion(version) VALUES(541);

COMMIT TRAN