-- Database update script to add column for fault report numbers
-- and number format for fault report numbers
-- SC 2017-11-06

USE [spain];

BEGIN TRAN

alter table dbo.faultreport
  add faultreportnumber varchar(30);


INSERT INTO numberformat (format, numerationType, lastModified, global, universal, orgid)
VALUES ('lcstyy000000','FAULT_REPORT',GETDATE(),0,0,6685);

COMMIT TRAN;