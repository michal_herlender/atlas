-- Today Gerard discovered that domain/username are switched for Bilbao
-- Galen Beck - 2018-03-20

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[alligatorsettings]
   SET [uploadwindowsdomain] = 'LaxtETvj52h+LVI5qiMrRs8ztI9IF/Qu',
      [uploadwindowsusername] = 'VHufQ+u5i9jc8LZGJtdtpuz2P0gSGuLXxO1Q87dUQac='
 WHERE description in ('ES-TIC-BIO/VIT Production','ES-TIC-BIO/VIT Test');
	
INSERT INTO dbversion (version) VALUES (239);

GO

COMMIT TRAN