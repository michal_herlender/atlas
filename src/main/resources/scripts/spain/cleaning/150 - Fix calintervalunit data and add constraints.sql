-- Fixes migrated data (and data created while intervalunit was not being set) and adds constraints
-- GB 2017-05-25
-- Takes 10-15 seconds to run

USE [spain];

BEGIN TRAN

UPDATE recallrule SET intervalunit = 'MONTH' WHERE intervalunit is NULL;
GO
ALTER table dbo.recallrule ALTER COLUMN intervalunit varchar(20) NOT NULL;

UPDATE recallrule SET recallrequirementtype = 'FULL_CALIBRATION' WHERE recallrequirementtype is NULL;
GO
ALTER table dbo.recallrule ALTER COLUMN recallrequirementtype varchar(20) NOT NULL;

-- Some migrated and previously created instruments had no calintervalunit
UPDATE instrument SET calintervalunit = 'MONTH' WHERE calintervalunit is NULL;
GO
ALTER table dbo.instrument ALTER COLUMN calintervalunit varchar(20) NOT NULL;

UPDATE calibrationtype SET recallRequirementType = 'FULL_CALIBRATION' WHERE recallRequirementtype is NULL;
GO
ALTER table dbo.calibrationtype ALTER COLUMN recallRequirementType varchar(20) NOT NULL;

-- Some certificates had no calintervalunit (causing exceptions upon edit)
UPDATE certificate SET intervalunit = 'MONTH' WHERE intervalunit is NULL;
GO
ALTER table dbo.certificate ALTER COLUMN intervalunit varchar(20) NOT NULL;

COMMIT TRAN