USE atlas
GO

BEGIN TRAN

	ALTER TABLE dbo.instrument DROP CONSTRAINT FK_instrument_last_gso;
	
	ALTER TABLE dbo.instrument DROP COLUMN lastgsoid;
	
	INSERT INTO dbversion VALUES(745)

COMMIT TRAN