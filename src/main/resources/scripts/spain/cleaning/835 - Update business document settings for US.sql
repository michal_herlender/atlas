-- Inserts a replacement businessdocumentsettings for US business company
-- It seems likely that on some older databases the US company was named 'TRESCAL INC.' rather than 'TRESCAL INC' so script 828 had some inconsistency. 
-- It seems safer / easier to just use the coid 6682 which is old enough to exist on all systems
-- ISS (33794) not in active use yet
-- This also makes the new field from script 834 not-nullable.

use [atlas];
go

begin tran

-- Remove (potentially) bad businessdocumentsettings from script 828

-- Trescal Inc and ISS US business company IDs (second many ot exist on older dev systems = OK)
DECLARE @coid_TUS int = 6682;
DECLARE @coid_ISS int = 33794;

DELETE FROM [dbo].[businessdocumentsettings] WHERE globalDefault= 0 and 
	([businesscompanyid] is null or [businesscompanyid] in (6682, 33794));

INSERT INTO [dbo].[businessdocumentsettings]
           ([recipientAddressOnLeft]
           ,[globalDefault]
           ,[invoiceAmountText]
           ,[businesscompanyid]
           ,[deliveryNoteSignedCertificates]
		   ,[subdivisionFiscalIdentifier])
     VALUES
           (1, 0, 0, @coid_TUS, 0, 1);
GO

alter table dbo.businessdocumentsettings 
    alter column subdivisionFiscalIdentifier bit not null;

INSERT INTO dbversion(version) VALUES(835);

COMMIT TRAN