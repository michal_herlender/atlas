USE [spain];


BEGIN TRAN

	ALTER TABLE dbo.advesojobItemactivityqueue ADD certificateid int;
	ALTER TABLE dbo.advesojobItemactivityqueue ADD CONSTRAINT advesojobItemactivityqueue_certificate_FK 
	FOREIGN KEY (certificateid) REFERENCES dbo.certificate(certid);
	
	--DECLARE @stateid int;

	--SELECT @stateid = stateid FROM dbo.itemstate WHERE description = 'Certificate signed';

	
	INSERT INTO [dbo].[stategrouplink]
           ([groupid]
           ,[stateid]
           ,[type])
     VALUES
           (53,32,'CURRENT_SUBDIV');

	INSERT INTO dbversion (version) VALUES (468);

COMMIT TRAN