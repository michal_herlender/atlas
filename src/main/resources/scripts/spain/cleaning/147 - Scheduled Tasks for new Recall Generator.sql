-- Galen Beck - 2017-05-12
-- Creates two inactive schedule tasks
-- We don't want to run yet on production so manual activation is necessary

USE [spain]
GO

INSERT INTO [dbo].[scheduledtask]
           ([active]
           ,[classname]
           ,[description]
           ,[lastrunmessage]
           ,[lastrunsuccess]
           ,[lastruntime]
           ,[methodname]
           ,[quartzjobname]
           ,[taskname])
     VALUES
           (0,'org.trescal.cwms.core.recall.generator.RecallGeneratorImpl'
           ,'Generates Recall, RecallDetail, RecallItem entities to be sent by RecallSender'
           ,''
           ,0
           ,null
           ,'executeTask'
           ,'jobDetail_RecallGenerator'
           ,'Recall Generator'),
		   (0,'org.trescal.cwms.core.recall.generator.RecallSenderImpl'
           ,'Sends emails to clients using RecallDetail previously created by RecallGenerator'
           ,''
           ,0
           ,null
           ,'executeTask'
           ,'jobDetail_RecallSender'
           ,'Recall Sender')
GO


