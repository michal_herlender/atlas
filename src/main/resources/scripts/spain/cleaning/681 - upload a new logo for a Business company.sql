
USE [atlas]
GO

BEGIN TRAN

	create table dbo.systemimage (
       id int identity not null,
        description varchar(255),
        filedata varbinary(max),
        fileName varchar(255),
        type varchar(255),
        coid int,
        primary key (id)
    );
	
	
alter table dbo.systemimage add constraint FK_systemimage_company foreign key (coid) references dbo.company;
       
       
	INSERT INTO dbversion(version) VALUES(681);

COMMIT TRAN

