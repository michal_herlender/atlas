USE [spain]
GO

BEGIN TRAN

CREATE TABLE dbo.freerepairoperation (
	id int NOT NULL IDENTITY(1,1),
	name varchar(100),
	labourtime int,
	cost numeric,
	[order] int,
	operationtype varchar(100) NOT NULL,
	operationstatus varchar(100),
	addedafterrirvalidation bit DEFAULT 0 NOT NULL,
	repairinspectionreportid int,
	CONSTRAINT freerepairoperation_PK PRIMARY KEY (id),
);

CREATE TABLE dbo.freerepaircomponent (
	id int NOT NULL IDENTITY(1,1),
	name varchar(100),
	quantity int DEFAULT 0 NOT NULL,
	cost numeric,
	status varchar(100),
	addedafterrirvalidation bit DEFAULT 0 NOT NULL,
	freerepairoperationid int,
	componentid int,
	CONSTRAINT freerepaircomponent_PK PRIMARY KEY (id),
	CONSTRAINT freerepaircomponent_component_FK FOREIGN KEY (componentid) REFERENCES dbo.component(id),
	CONSTRAINT freerepaircomponent_freerepairoperation_FK FOREIGN KEY (id) 
	REFERENCES dbo.freerepairoperation(id)
);

CREATE TABLE dbo.validatedfreerepairoperation (
	id int NOT NULL IDENTITY(1,1),
	labourtime int NOT NULL,
	cost numeric,
	freerepairoperationid int,
	repaircompletionreportid int,
	CONSTRAINT validatedfreerepairoperation_PK PRIMARY KEY (id),
	CONSTRAINT validatedfreerepairoperation_freerepairoperation_FK 
	FOREIGN KEY (freerepairoperationid) REFERENCES dbo.freerepairoperation(id)
);

CREATE TABLE dbo.validatedfreerepaircomponent (
	id int NOT NULL IDENTITY(1,1),
	quantity int DEFAULT 0 NOT NULL,
	cost numeric,
	freerepaircomponentid int,
	validatedfreerepairoperationid int,
	CONSTRAINT validatedfreerepaircomponent_PK PRIMARY KEY (id),
	CONSTRAINT validatedfreerepaircomponent_freerepaircomponent_FK 
	FOREIGN KEY (freerepaircomponentid) REFERENCES dbo.freerepaircomponent(id),
	CONSTRAINT validatedfreerepaircomponent_validatedfreerepairoperation_FK 
	FOREIGN KEY (validatedfreerepairoperationid) REFERENCES dbo.validatedfreerepairoperation(id)
);

CREATE TABLE dbo.repairinspectionreport (
	id int NOT NULL IDENTITY(1,1),
	validatedon datetime2,
	validatedby int,
	lastModified datetime2,
	lastModifiedBy int,
	CONSTRAINT repairinspectionreport_PK PRIMARY KEY (id),
	CONSTRAINT repairinspectionreport_contact_FK 
	FOREIGN KEY (validatedby) REFERENCES dbo.contact(personid),
	CONSTRAINT repairinspectionreport_contact_FK_1 
	FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid)
);

CREATE TABLE dbo.repaircompletionreport (
	id int NOT NULL IDENTITY(1,1),
	repairinspectionreportid int,
	lastModifiedBy int,
	lastModified datetime2,
	CONSTRAINT repaircompletionreport_PK PRIMARY KEY (id),
	CONSTRAINT repaircompletionreport_repairinspectionreport_FK 
	FOREIGN KEY (repairinspectionreportid) REFERENCES dbo.repairinspectionreport(id),
	CONSTRAINT repaircompletionreport_contact_FK 
	FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid)
);

ALTER TABLE dbo.freerepairoperation ADD CONSTRAINT freerepairoperation_repairinspectionreport_FK 
FOREIGN KEY (repairinspectionreportid) REFERENCES dbo.repairinspectionreport(id);

ALTER TABLE dbo.validatedfreerepairoperation ADD CONSTRAINT validatedfreerepairoperation_repaircompletionreport_FK 
FOREIGN KEY (repaircompletionreportid) REFERENCES dbo.repaircompletionreport(id);

CREATE TABLE dbo.componentinventory (
	id int NOT NULL IDENTITY(1,1),
	businesssubdivid int NOT NULL,
	suppliercompanyid int NOT NULL,
	componentid int NOT NULL,
	CONSTRAINT componentinventory_PK PRIMARY KEY (id),
	CONSTRAINT componentinventory_subdiv_FK FOREIGN KEY (businesssubdivid) REFERENCES dbo.subdiv(subdivid),
	CONSTRAINT componentinventory_company_FK FOREIGN KEY (suppliercompanyid) REFERENCES dbo.company(coid),
	CONSTRAINT componentinventory_component_FK FOREIGN KEY (componentid) REFERENCES dbo.component(id)
);

INSERT INTO dbversion (version) VALUES (429);
GO

COMMIT TRAN