-- 3 changes included in this script:

-- 1) Insert two new nominal codes - S012 and S912 for "Sales reimbursed travel expense"
-- 2) Configure the "invoice in advance" nominal codes to have Avatax codes
-- 3) Fix spelling from "sparts" to "parts" in existing codes


USE [atlas]
GO

BEGIN TRAN

DECLARE @UserId INT;
DECLARE @Now datetime2 = SYSDATETIME();
SELECT @UserId= personid FROM dbo.users WHERE username='GalenB';
PRINT @UserId;
-- Expected to be 17280

INSERT INTO [dbo].[nominalcode]
           ([code]
           ,[ledger]
           ,[title]
           ,[lastModified]
           ,[lastModifiedBy]
           ,[avalarataxcode])
     VALUES
           ('S012'
           ,'SALES_LEDGER'
           ,'Sales reimbursed travel expenses'
           ,@Now
           ,@UserId
		   ,'OT010400'),
           ('S912'
           ,'SALES_LEDGER'
           ,'Invoice in advance - Sales reimbursed travel expenses'
           ,@Now
           ,@UserId
           ,'OT010400')
GO

DECLARE @UserId INT;
DECLARE @Now datetime2 = SYSDATETIME();
SELECT @UserId= personid FROM dbo.users WHERE username='GalenB';
PRINT @UserId;
-- Expected to be 17280

DECLARE @S005Id INT;
DECLARE @S905Id INT;

SELECT @S005Id = id from dbo.nominalcode WHERE code='S005';
SELECT @S905Id = id from dbo.nominalcode WHERE code='S905';

PRINT @S005Id;
PRINT @S905Id;

-- Fix the "spare sparts" mistranslation

UPDATE [dbo].[nominalcode]
   SET [title] = 'Sales spare parts'
 WHERE [code] = 'S005'

 UPDATE [dbo].[nominalcodetitletranslation]
   SET [translation] = 'Sales spare parts'
 WHERE [id] = @S005Id and locale in ('en_GB', 'en_US')

UPDATE [dbo].[nominalcode]
   SET [title] = 'Invoice in advance - Sales spare parts'
 WHERE [code] = 'S905'

 UPDATE [dbo].[nominalcodetitletranslation]
   SET [translation] = 'Invoice in advance - Sales spare parts'
 WHERE [id] = @S905Id and locale in ('en_GB', 'en_US')

 -- Insert new translations for S012 and S912 
DECLARE @S012Id INT;
DECLARE @S912Id INT;
SELECT @S012Id = id from dbo.nominalcode WHERE code='S012';
SELECT @S912Id = id from dbo.nominalcode WHERE code='S912';

PRINT @S012Id;
PRINT @S912Id;

INSERT INTO [dbo].[nominalcodetitletranslation]
           ([id]
           ,[locale]
           ,[translation])
     VALUES
           (@S012Id, 'en_GB', 'Sales reimbursed travel expenses'),
           (@S012Id, 'en_US', 'Sales reimbursed travel expenses'),
           (@S012Id, 'es_ES', 'Refacturacion gastos de viaje'),
           (@S012Id, 'fr_FR', 'Chiffre d''affaires - Frais de d�placement'),
           (@S012Id, 'de_DE', 'Reisekosten'),
           (@S912Id, 'en_GB', 'Invoice in advance - Sales reimbursed travel expenses'),
           (@S912Id, 'en_US', 'Invoice in advance - Sales reimbursed travel expenses'),
           (@S912Id, 'es_ES', 'Factura por adelantado - Refacturacion gastos de viaje'),
           (@S912Id, 'fr_FR', 'Facture � l''avance - Chiffre d''affaires - Frais de d�placement'),
           (@S912Id, 'de_DE', 'Vorkasse - Reisekosten')
GO

-- Update S9** codes to have the same tax codes as corresponding S0** codes

UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'S0000000' WHERE [code] = 'S901'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'S0000000' WHERE [code] = 'S902'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'S0000000' WHERE [code] = 'S903'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'S0000000' WHERE [code] = 'S904'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'P0000000' WHERE [code] = 'S905'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'FR000000' WHERE [code] = 'S906'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'SW054000' WHERE [code] = 'S907'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'SP140000' WHERE [code] = 'S908'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'P0000000' WHERE [code] = 'S909'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'PR091000' WHERE [code] = 'S910'
UPDATE [dbo].[nominalcode] SET [avalarataxcode] = 'S0000000' WHERE [code] = 'S911'
GO


insert into dbversion(version) values(814);

COMMIT TRAN