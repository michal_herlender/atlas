-- Remove foreign key constraint on mailgroupid in mailgroupmember; remove mailgrouptype table
-- MailGroupType is now a proper enum, table no longer needed.
-- GB 2017-06-15

USE [spain]
GO

BEGIN TRAN;

ALTER TABLE [dbo].[mailgroupmember] DROP CONSTRAINT [FK42EF77C275F76F1B]
GO

DROP TABLE mailgrouptype;
GO

COMMIT TRAN;