-- Change the type of the cost column in tables freerepairoperation and freerepaircomponent
USE [spain]
GO

BEGIN TRAN

ALTER TABLE dbo.freerepairoperation
ALTER COLUMN cost numeric(10,2);
GO

ALTER TABLE dbo.freerepaircomponent
ALTER COLUMN cost numeric(10,2);
GO

INSERT INTO dbversion(version) VALUES(481);

COMMIT TRAN