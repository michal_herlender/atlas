-- Removing unused fields from hireinstrument
-- In the future (US development) we plan to expand it a bit (allocated company, active / inactive , etc...)

USE [spain]

BEGIN TRAN

insert into dbo.permission values (1,'ACCOUNTING_WEB_SERVICE')

INSERT INTO dbo.permission
SELECT id,'ACCOUNTING_WEB_SERVICE' FROM permissiongroup 
WHERE permissiongroup.name like '%CWMS%';

INSERT INTO dbversion(version) VALUES(395);

COMMIT TRAN