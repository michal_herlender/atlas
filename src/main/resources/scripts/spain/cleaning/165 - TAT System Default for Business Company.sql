-- Inserts config setting so that we can have a system default with 
-- the TAT (Turnaround Time) setting for business companies.
-- Sets global default value back to 0 (the system default value is actually not used, it comes from business subdiv)

USE [spain]
GO

INSERT INTO [dbo].[systemdefaultcorole]
           ([coroleid]
           ,[defaultid])
     VALUES
           (5,2)
GO

UPDATE [dbo].[systemdefault]
   SET [defaultvalue] = '0'
 WHERE defaultid = 2;



