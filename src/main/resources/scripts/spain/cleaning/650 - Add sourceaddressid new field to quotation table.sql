USE [atlas]
GO

BEGIN TRAN

		ALTER TABLE dbo.quotation ADD sourceaddressid INT;
		GO

		UPDATE dbo.quotation 
		SET sourceaddressid = (SELECT addrid FROM dbo.address address
		INNER JOIN dbo.contact con on con.defaddress = address.addrid
		WHERE quotation.sourcedby = con.personid)
		WHERE sourceaddressid IS NULL 
		GO

		ALTER TABLE dbo.quotation ALTER COLUMN sourceaddressid INT NOT NULL;
		GO

		INSERT INTO dbversion(version) VALUES(650);

COMMIT TRAN