--Add itemsFromJson column to asn table
USE [atlas]
GO
BEGIN TRAN
    ALTER TABLE dbo.asn ADD itemsFromJson VARCHAR(max);

	INSERT INTO dbversion(version) VALUES(520);

COMMIT TRAN