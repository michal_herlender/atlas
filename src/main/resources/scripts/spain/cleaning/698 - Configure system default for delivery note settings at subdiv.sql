-- Sets up 4 new system defaults into a new group for specifying defaults for client delivery list generation
-- Galen Beck - 2020-08-27

USE [atlas]
GO

BEGIN TRAN

declare @groupId int;
declare @defaultIdDateReceived int = 59;
declare @defaultIdPurchaseOrder int = 60;
declare @defaultIdCertificates int = 61;
declare @defaultIdTotalPrice int = 62;

-- Enable setting parameters at the subdivision level (enum ordinal position = 2 for Scope.SUBDIV)
INSERT INTO systemdefaultscope ([scope],defaultid)
	VALUES (2,@defaultIdDateReceived),
	(2,@defaultIdPurchaseOrder),
	(2,@defaultIdCertificates),
	(2,@defaultIdTotalPrice);

INSERT INTO dbversion(version) VALUES(698);

COMMIT TRAN
