-- Script 256 to update procedure (aka capability) for capability filter support
-- Galen Beck - 2018-05-25

-- 1) add optional (for now) link to description / subfamily
-- 2) make workrequirement type not nullable
-- 3) Create definition for capabilityfilter

USE [spain]

BEGIN TRAN

   alter table dbo.procs 
       add subfamilyid int;

   alter table dbo.procs 
       add constraint FK_procs_description 
       foreign key (subfamilyid) 
       references dbo.description;

	alter table dbo.procs
		alter column reqtype [varchar](255) NOT NULL

    create table dbo.capabilityfilter (
        id int identity not null,
        lastModified datetime2 not null,
        bestlab bit default 0 not null,
        comments varchar(255) not null,
        filtertype varchar(255) not null,
        locationlaboratory bit default 0 not null,
        locationonsite bit default 0 not null,
        servicetypecalibration bit default 0 not null,
        servicetyperepair bit default 0 not null,
        supportaccredited bit default 0 not null,
        supportnonaccredited bit default 0 not null,
        lastModifiedBy int,
        procedureid int not null,
        primary key (id)
    );

    alter table dbo.capabilityfilter 
        add constraint FKr2vipi1yix6f0p8q6s8kantk2 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.capabilityfilter 
        add constraint FK_capabilityfilter_procs 
        foreign key (procedureid) 
        references dbo.procs;

insert into dbversion(version) values(256)
go

COMMIT TRAN