USE [atlas]
GO

BEGIN TRAN

	DECLARE @awaiting_tp_work INT,
			@tp_cancel_and_replace INT,
			@awaiting_tp_cert INT;
		
	SELECT @awaiting_tp_work = stateid FROM dbo.itemstate WHERE [description] = 'Unit at third party for work';
	SELECT @tp_cancel_and_replace = stateid FROM dbo.itemstate WHERE [description] = 'Third party certificate cancel and replace';
	SELECT @awaiting_tp_cert = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting third party certificate';
	
	UPDATE dbo.outcomestatus SET statusid = @awaiting_tp_cert 
	WHERE activityid = @tp_cancel_and_replace AND statusid = @awaiting_tp_work;
    
    insert into dbversion(version) values(811);

COMMIT TRAN