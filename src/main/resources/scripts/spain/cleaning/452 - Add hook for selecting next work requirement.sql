USE [spain];


BEGIN TRAN

	-- add state 'awaiting selection of next work requirement' to stategroup 'select next workrequirement'
	-- Actual parameter values may differ, what you see is a default string representation of values
	INSERT INTO dbo.stategrouplink (groupid,stateid,[type]) VALUES (52,4092,'ALLOCATED_SUBDIV');
	
	--create hook
	INSERT INTO dbo.hook (active,alwayspossible,beginactivity,completeactivity,name,activityid)
	VALUES (1,1,1,1,'select-next-work-requirement',4093);

	
	INSERT INTO dbversion (version) VALUES (452);

COMMIT TRAN