
USE [atlas]
GO

BEGIN TRAN

	-- Drop optionpersubdiv column (unused column)
	ALTER TABLE dbo.transportmethod DROP COLUMN optionpersubdiv
	GO
	
	-- Add a new column "active" in transportmethod table
	ALTER TABLE dbo.transportmethod ADD active BIT DEFAULT 1 NOT NULL;
	GO
	
	-- Disable "Trescal Shuttle" transport method
	UPDATE dbo.transportmethod SET active = 0 WHERE method = 'Trescal Shuttle'

	-- Add new column localizedname in the transportoption table
	ALTER TABLE dbo.transportoption ADD localizedname VARCHAR(255) NULL;

	-- Create new table transportoptionschedulerule
	CREATE TABLE dbo.transportoptionschedulerule (
		id INT IDENTITY NOT NULL PRIMARY KEY,
		transportoptionid INT NOT NULL,
		cutofftime TIME NOT NULL,
		dayofweek VARCHAR(50) NOT NULL,
		CONSTRAINT FK_transportoptionschedulerule_transportoption FOREIGN KEY (transportoptionid) REFERENCES dbo.transportoption(id)
	);
		
	-- Update "Trescal Shuttle" transport method
	UPDATE dbo.transportmethod SET optionperday = 1 WHERE method = 'Trescal Shuttle - 1'
	UPDATE dbo.transportmethod SET optionperday = 1 WHERE method = 'Trescal Shuttle - 2'
	UPDATE dbo.transportmethod SET optionperday = 1 WHERE method = 'Trescal Shuttle - 3'
	UPDATE dbo.transportmethod SET optionperday = 1 WHERE method = 'Trescal Shuttle - 4'
	UPDATE dbo.transportmethod SET optionperday = 1 WHERE method = 'Trescal Shuttle - 5'

	INSERT INTO dbversion(version) VALUES(743);

COMMIT TRAN