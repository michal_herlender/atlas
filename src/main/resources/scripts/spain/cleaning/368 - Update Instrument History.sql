-- Adds columns to instrument history table to track instruments being moved between company
-- Galen Beck - 2018-12-14

USE [spain]
GO

BEGIN TRAN

alter table dbo.instrumenthistory 
    add companyupdated tinyint;

alter table dbo.instrumenthistory 
    add newcompanyid int;

alter table dbo.instrumenthistory 
    add oldcompanyid int;

GO

alter table dbo.instrumenthistory 
    add constraint FK_instrumenthistory_newCompany 
    foreign key (newcompanyid) 
    references dbo.company;

alter table dbo.instrumenthistory 
    add constraint FK_instrumenthistory_oldCompany 
    foreign key (oldcompanyid) 
    references dbo.company;

GO

update dbo.instrumenthistory set companyupdated = 0;

GO

alter table dbo.instrumenthistory 
    alter column companyupdated tinyint not null;

INSERT INTO dbversion(version) VALUES(368);

COMMIT TRAN


