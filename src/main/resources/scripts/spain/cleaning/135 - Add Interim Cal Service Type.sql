BEGIN TRAN;

-- Create service type with standard colors
insert into dbo.servicetype ([displaycolour],[displaycolourfasttrack],[longname],[shortname])
VALUES ('#FFFFFF','#FEE0B4','Interim Calibration','IC');



-- Create cal type for new service type with values from in house standard service type
insert into calibrationtype
(accreditationspecific,
 active,
 certnoresetyearly,
 firstpagepapertype,
 labeltemplatepath,
 orderby,
 pdfcontinuationwatermark,
 pdfheaderwatermark,
 recalldateonlabel,
 restpagepapertype,
 servicetypeid,
 recallRequirementType)
  select accreditationspecific,
    1,
    certnoresetyearly,
    firstpagepapertype,
    labeltemplatepath,
    (select servicetypeid from servicetype s where s.shortname = 'IC'),
    pdfcontinuationwatermark,
    pdfheaderwatermark,
    recalldateonlabel,
    restpagepapertype,
    (select servicetypeid from servicetype s where s.shortname = 'IC') as 'servicetypeid',
    'INTERIM_CALIBRATION'
  from calibrationtype
    inner join servicetype on servicetype.servicetypeid = calibrationtype.servicetypeid
  where servicetype.shortname = 'ST-IH';

-- Insert default quotation calibration conditions for new cal type and every business company
INSERT INTO [dbo].[defaultquotationcalibrationcondition]
(conditiontext, lastModified, caltypeid, orgid)
  SELECT '',
    '2016-01-01',
    (select calibrationtype.caltypeid
     FROM servicetype
       INNER JOIN  calibrationtype
         ON servicetype.servicetypeid = calibrationtype.servicetypeid
     WHERE servicetype.shortname = 'IC'),
    company.coid
  FROM company
  WHERE company.corole = 5;


-- Insert links to jobtypes
INSERT INTO jobtypecaltype (defaultvalue, jobtypeid, caltypeid)
  SELECT 0,1,caltypeid
  FROM calibrationtype
    INNER JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'IC';

INSERT INTO jobtypecaltype (defaultvalue, jobtypeid, caltypeid)
  SELECT 0,2,caltypeid
  FROM calibrationtype
    INNER JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'IC';




-- Insert accreditation levels for new cal type
INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
  SELECT 'APPROVE', [caltypeid]
  FROM [dbo].[calibrationtype]
    LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
  SELECT 'REMOVED', [caltypeid]
  FROM [dbo].[calibrationtype]
    LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
  SELECT 'SUPERVISED', [caltypeid]
  FROM [dbo].[calibrationtype]
    LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
  SELECT 'PERFORM', [caltypeid]
  FROM [dbo].[calibrationtype]
    LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'IC';

INSERT INTO [dbo].[calibrationaccreditationlevel]
([accredlevel] ,[caltypeid])
  SELECT 'SIGN', [caltypeid]
  FROM [dbo].[calibrationtype]
    LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
  WHERE servicetype.shortname = 'IC';



--Add translations
INSERT INTO servicetypelongnametranslation (servicetypeid,locale,translation)
  select servicetype.servicetypeid,'en_GB',servicetype.longname from servicetype
  WHERE servicetype.shortname = 'IC';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
  select servicetype.servicetypeid, 'es_ES', 'Calibración Intervenir'
  from servicetype
  where servicetype.shortname = 'IC';

INSERT INTO servicetypelongnametranslation (servicetypeid, locale, translation)
  select servicetype.servicetypeid, 'fr_FR', 'Étalonnage Intermédiaire'
  from servicetype
  where servicetype.shortname = 'IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
  select servicetype.servicetypeid,'en_GB',servicetype.shortname from servicetype
  WHERE servicetype.shortname = 'IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
  select servicetype.servicetypeid,'es_ES','CI'  from servicetype
  WHERE servicetype.shortname = 'IC';

INSERT INTO servicetypeshortnametranslation (servicetypeid,locale,translation)
  select servicetype.servicetypeid,'fr_FR','EI' from servicetype
  WHERE servicetype.shortname = 'IC';

COMMIT TRAN;

