BEGIN TRAN

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
([defaultid], [defaultdatatype], [defaultdescription], [defaultname], [defaultvalue], [endd], [start], [groupid], [groupwide])
VALUES
  (21
    , 'boolean'
    , 'Defines if instruments can have a calibration extension'
    , 'Calibration Extensions'
    , 'false', 0, 0, 9, 1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
([defaultid], [locale], [translation])
VALUES
  (21, 'en_GB', 'Defines if instruments can have a calibration extension'),
  (21, 'en_ES', 'Define si los instrumentos pueden tener una extensión de calibración'),
  (21, 'fr_FR', 'Définit si les instruments peuvent avoir une extension d''étalonnage');

-- Enables system default to be configurable at only company level

INSERT INTO [dbo].[systemdefaultscope]
([scope], [defaultid])
VALUES
  (0, 21);
GO

-- Enables system defaults (new and old) for specific company roles - Client and Business Company

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid], [defaultid]) VALUES
  (1, 21),
  (5, 21);

-- Add translations

INSERT INTO [dbo].[systemdefaultnametranslation]
([defaultid], [locale], [translation])
VALUES
  (21, 'en_GB', 'Calibration Extensions'),
  (21, 'es_ES', 'Extensiones de calibración'),
  (21, 'fr_FR', 'Extensions d''étalonnage');
GO

INSERT INTO dbversion (version) VALUES (197);

-- Change to COMMIT when successfully tested

COMMIT TRAN