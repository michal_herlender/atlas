USE [atlas]
GO

BEGIN TRAN


DECLARE @new_lookup INT,
		@state1 INT,@state2 INT,
		@yes_os INT,@no_os INT;

/*
	Creating lookupinstance
*/

insert into dbo.lookupinstance (lookup,lookupfunction) values ('Lookup_ItemQuotedBypassCosting (OnSite)',58);
SELECT @new_lookup = max(id) from dbo.lookupinstance;

update dbo.outcomestatus set lookupid=@new_lookup , statusid=null  where 
id in (select id from outcomestatus os where os.activityid is null and os.lookupid is null and 
os.statusid =(select ist.stateid from itemstate ist where ist.description like 'Recorded onsite service - awaiting job costing'))

/*
	creating OS and LR for the lookupinstance
*/
 
select @state1=its.stateid from itemstate its where its.description like 'Completed onsite service'
select @state2=its.stateid from itemstate its where its.description like 'Recorded onsite service - awaiting job costing'

insert into dbo.outcomestatus (defaultoutcome,statusid) values (0,@state1)
select @yes_os = max(id) from dbo.outcomestatus;
insert into dbo.outcomestatus (defaultoutcome,statusid) values (0,@state2)
select @no_os = max(id) from dbo.outcomestatus;

INSERT INTO dbo.lookupresult ([result],lookupid,outcomestatusid,resultmessage) 
VALUES  ('a',@new_lookup,@yes_os,0),
			('b',@new_lookup,@no_os,1);


/*
	Modify workflow to work with the new lookup
*/

update dbo.outcomestatus set lookupid=@new_lookup , statusid=null 
where id=(select os.id from outcomestatus os where 
os.activityid=(select ist.stateid from itemstate ist where ist.description='Onsite calibration - certificate completion to follow'))

update dbo.outcomestatus set lookupid=@new_lookup , statusid=null 
where id=(select os.id from outcomestatus os where 
os.activityid=(select ist.stateid from itemstate ist where ist.description='Onsite calibration - certificate signed'))

update dbo.itemstate set lookupid=@new_lookup where description like 'Onsite calibration - certificate completion to follow'
update dbo.itemstate set lookupid=@new_lookup where description like 'Onsite calibration - certificate signed'



INSERT INTO dbversion(version) VALUES (584)

COMMIT TRAN