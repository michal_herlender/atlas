use spain
go

begin tran

	create table dbo.subdivsettingsforallocatedsubdiv (
        id int identity not null,
        lastModified datetime2 not null,
        lastModifiedBy int,
        orgid int not null,
        businesscontactid int,
        subdivid int not null,
        primary key (id)
    );

	create index IDX_subdivsettingsforallocatedsubdiv_subdivid on dbo.subdivsettingsforallocatedsubdiv (subdivid);

    alter table dbo.subdivsettingsforallocatedsubdiv 
        add constraint UK_subdivsettingsforallocatedsubdiv_orgid_subdivid unique (orgid, subdivid);

    alter table dbo.subdivsettingsforallocatedsubdiv 
        add constraint FKkxejjdt7r621j4dv7q0pgfiwq 
        foreign key (lastModifiedBy) 
        references dbo.contact;

    alter table dbo.subdivsettingsforallocatedsubdiv 
        add constraint FKg0ul02mx9vfqb9a39ucq3sof9 
        foreign key (orgid) 
        references dbo.subdiv;

    alter table dbo.subdivsettingsforallocatedsubdiv 
        add constraint FK_subdivsettingsforallocatedsubdiv_businesscontactid 
        foreign key (businesscontactid) 
        references dbo.contact;

    alter table dbo.subdivsettingsforallocatedsubdiv 
        add constraint FK_subdivsettingsforallocatedsubdiv_subdivid 
        foreign key (subdivid) 
        references dbo.subdiv;
        
commit tran