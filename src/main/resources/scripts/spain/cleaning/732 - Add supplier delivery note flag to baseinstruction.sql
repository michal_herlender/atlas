-- Script 732 - creates a new optional field to track if an instruction 
-- (currently expected to be Transport aka "Carriage" instruction) should appear on a supplier delivery note
-- This can be set independently of the current includeondelnote which has been meant to be for "client" delivery notes

USE [atlas]

BEGIN TRAN

    alter table dbo.baseinstruction 
       add includeonsupplierdelnote bit;

INSERT INTO dbversion(version) VALUES(732);

COMMIT TRAN