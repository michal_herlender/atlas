-- DEV-

USE [atlas]

BEGIN TRAN

ALTER TABLE [capability_servicetype] ALTER COLUMN [comments] nvarchar(2000);

INSERT INTO dbversion(version) VALUES(750);

COMMIT TRAN