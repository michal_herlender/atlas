BEGIN TRAN
	
	UPDATE scheduledtask
	SET methodname = 'createQuotationsFromScheduledQuotationRequests'
	WHERE methodname = 'createQuotationsFromSceduledQuotationRequests';
	
	INSERT INTO dbversion (version) VALUES (214);
	
COMMIT TRAN