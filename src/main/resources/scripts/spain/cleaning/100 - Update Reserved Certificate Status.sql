-- Author Galen Beck 2016-12-01
-- Updates the unused "Unsigned - Old" certificate status to "Reserved"

USE [spain]
GO

UPDATE [dbo].[basestatus]
   SET [name] = 'Reserved'
 WHERE [statusid] = 36
GO

UPDATE [dbo].[basestatusnametranslation]
   SET [translation] = 'R�serv�'
 WHERE [statusid] = 36 and [locale] = 'fr_FR'

UPDATE [dbo].[basestatusnametranslation]
   SET [translation] = 'Reserved'
 WHERE [statusid] = 36 and [locale] = 'en_GB'

UPDATE [dbo].[basestatusnametranslation]
   SET [translation] = 'Reservado'
 WHERE [statusid] = 36 and [locale] = 'es_ES'

GO


