USE [atlas]

BEGIN TRAN

    DECLARE @val_servicetype INT;

	INSERT INTO dbo.servicetype(shortname, longname, displaycolour, displaycolourfasttrack, domaintype, repair, canbeperformedbyclient, orderby)
	VALUES ('VAL', 'Validation', '#FFFFFF', '#FFFFFF', 0, 0, 0, 44)
	SET @val_servicetype = IDENT_CURRENT('servicetype');
	
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@val_servicetype, 'en_GB', 'VAL')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@val_servicetype, 'en_GB', 'Validation')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@val_servicetype, 'es_ES', 'VAL')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@val_servicetype, 'es_ES', 'Validación')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@val_servicetype, 'fr_FR', 'VAL')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@val_servicetype, 'fr_FR', 'Validation')
	INSERT INTO dbo.servicetypeshortnametranslation(servicetypeid, locale, translation)
	VALUES (@val_servicetype, 'de_DE', 'VAL')
	INSERT INTO dbo.servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES (@val_servicetype, 'de_DE', 'Validierung')
	
	INSERT INTO dbo.jobtypeservicetype(defaultvalue,jobtypeid,servicetypeid)
	VALUES (1, 1, @val_servicetype);

	INSERT INTO dbversion(version) VALUES(662);

COMMIT TRAN