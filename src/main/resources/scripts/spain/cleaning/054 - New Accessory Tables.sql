/****** Script for SelectTopNRows command from SSMS  ******/
USE spain;
GO

/*** Run this first, then restart tomcat to recreate the tables ***/

/*
sp_rename accessory, accessory_old;
*/

/*** If there is something going wrong, you can delete the following tables and recreate the data ***/

/*
DELETE accessorytranslation;
DELETE itemaccessory;
DELETE groupaccessory;
DELETE accessory;
GO
*/

/*** After the restart of tomcat run this part to include the data ***/

/*
INSERT INTO accessory(description)
VALUES
	('accessory'),
	('adapter'),
	('battery'),
	('box'),
	('cable reel'),
	('calibration certificate'),
	('case'),
	('cd'),
	('charger'),
	('clamp'),
	('communication lead'),
	('customer package'),
	('documentation'),
	('external sensor/probe'),
	('fittings'),
	('holster'),
	('interface'),
	('mains lead'),
	('manual'),
	('oil'),
	('outer box/case'),
	('pallet'),
	('pressure connection lead'),
	('printer'),
	('rechargeable battery'),
	('special lead'),
	('straps'),
	('test lead'),
	('weights')
	
UPDATE accessory SET lastModified='01.07.2016'

INSERT INTO itemaccessory(quantity, accessory_id, jobitemid)
SELECT  accessory_old.qty, accessory.id, accessory_old.jobitemid
FROM accessory_old
LEFT JOIN accessory ON accessory_old.description = accessory.description
WHERE type = 'item' and accessory_old.freetexts = 0

UPDATE jobitem SET accessoryFreeText = accessory_old.description
FROM jobitem
LEFT JOIN accessory_old ON jobitem.jobitemid = accessory_old.jobitemid
WHERE accessory_old.freetexts = 1

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'en_GB', description
FROM accessory

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Accesorio'
FROM accessory
WHERE accessory.description = 'accessory'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Adaptador'
FROM accessory
WHERE accessory.description = 'adapter'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Pila'
FROM accessory
WHERE accessory.description = 'battery'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Caja de cart�n'
FROM accessory
WHERE accessory.description = 'box'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Rollo de cable'
FROM accessory
WHERE accessory.description = 'cable reel'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Certificado'
FROM accessory
WHERE accessory.description = 'calibration certificate'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Maleta'
FROM accessory
WHERE accessory.description = 'case'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'CD'
FROM accessory
WHERE accessory.description = 'cd'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Cargador'
FROM accessory
WHERE accessory.description = 'charger'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Pinza'
FROM accessory
WHERE accessory.description = 'clamp'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Cable de comunicaciones'
FROM accessory
WHERE accessory.description = 'communication lead'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Arc�n cliente'
FROM accessory
WHERE accessory.description = 'customer package'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Documentacion'
FROM accessory
WHERE accessory.description = 'documentation'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Sonda/sensor externo'
FROM accessory
WHERE accessory.description = 'external sensor/probe'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Racores'
FROM accessory
WHERE accessory.description = 'fittings'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Funda'
FROM accessory
WHERE accessory.description = 'holster'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Interface'
FROM accessory
WHERE accessory.description = 'interface'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Cable de alimentaci�n'
FROM accessory
WHERE accessory.description = 'mains lead'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Manual'
FROM accessory
WHERE accessory.description = 'manual'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Aceite'
FROM accessory
WHERE accessory.description = 'oil'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Estuche'
FROM accessory
WHERE accessory.description = 'outer box/case'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Palet'
FROM accessory
WHERE accessory.description = 'pallet'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Latiguillo de presi�n'
FROM accessory
WHERE accessory.description = 'pressure connection lead'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Impresora'
FROM accessory
WHERE accessory.description = 'printer'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Bater�a'
FROM accessory
WHERE accessory.description = 'rechargeable battery'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Cable especial'
FROM accessory
WHERE accessory.description = 'special lead'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Correas'
FROM accessory
WHERE accessory.description = 'straps'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Cable de medidas'
FROM accessory
WHERE accessory.description = 'test lead'

INSERT INTO accessorytranslation(accessoryid, locale, translation)
SELECT id, 'es_ES', 'Masas'
FROM accessory
WHERE accessory.description = 'weights'

UPDATE deliveryitemaccessory SET accessoryId = accessory.id
FROM deliveryitemaccessory
LEFT JOIN accessory ON accessory.description = deliveryitemaccessory.description

UPDATE deliveryitem SET accessoryFreeText = description
FROM deliveryitem
LEFT JOIN deliveryitemaccessory ON deliveryitem.deliveryitemid = deliveryitemaccessory.deliveryitemid
WHERE deliveryitemaccessory.freetexts = 1

DELETE FROM deliveryitemaccessory WHERE freetexts = 1

UPDATE jobitemgroup SET accessoryFreeText = accessory_old.description
FROM jobitemgroup
LEFT JOIN accessory_old ON jobitemgroup.id = accessory_old.groupid
WHERE accessory_old.freetexts = 1

INSERT INTO groupaccessory(quantity, accessory_id, groupid)
SELECT accessory_old.qty, accessory.id, accessory_old.groupid
FROM accessory_old
LEFT JOIN accessory ON accessory_old.description = accessory.description
WHERE type = 'group' and accessory_old.freetexts = 0

INSERT INTO groupaccessory(quantity, accessory_id, groupid)
SELECT 1, accessory.id, jobitemgroup.id
FROM accessory, jobitemgroup
WHERE accessory.description = 'case' AND jobitemgroup.withcase = 1

*/

/*** Finally, you can delete the old accessory table (but only, if you're sure, that all is going right!!! ***/

/*
DELETE accessory_old;
*/