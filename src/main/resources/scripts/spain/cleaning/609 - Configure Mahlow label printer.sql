-- The XML definitions are obtained from a file
-- "Brother Sticker Calculations"

USE [atlas]
GO

BEGIN TRAN

-- Cleans up potentially conflicting data from earlier manual tests of label printer

PRINT 'Cleaning up test data'

DECLARE @labelprinterid int;

SELECT @labelprinterid = id FROM labelprinter WHERE description = 'Brother PT-P750W';

UPDATE [userpreferences] SET labelprinterid = null WHERE labelprinterid = @labelprinterid;

DELETE FROM labelprinter WHERE id = @labelprinterid;

GO

DELETE FROM alligatorlabelmedia WHERE id in (3,4,5);

-- Start of insert routine

DECLARE @Xml_Traceable VARCHAR(max);
DECLARE @Xml_Accredited VARCHAR(max);

SET @Xml_Traceable = '<template>
  <parameters>
    <parameter name="value6" value="MAH-12345678" type="string" />
    <parameter name="barcode" value="1234567" type="string" />
    <parameter name="value3" value="12-345" type="string" />
    <parameter name="value2" value="12.25.2020" type="string" />
  </parameters>
  <name>DE-Mahlow Brother Traceable</name>
  <height>300</height>
  <width>217</width>
  <string ypos="4" xpos="8" height="30" width="201" rotationAngle="0" font="Verdana" size="20" text="${value6}" />
  <string ypos="151" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="ID: ${value3}" />
  <string ypos="199" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="N�chste Kal." />
  <string ypos="243" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="${value2}" />
  <barcode ypos="42" xpos="8" height="105" width="201" rotationAngle="0" value="${barcode}" type="ECC200" />
  <rectangle ypos="0" xpos="4" height="287" width="209" rotationAngle="0" />
  <rectangle ypos="38" xpos="4" height="157" width="209" rotationAngle="0" />
  <defaultPrinter>Brother PT-P750W</defaultPrinter>
</template>';

SET @Xml_Accredited = '<template>
  <parameters>
    <parameter name="value6" value="MAH-12345678" type="string" />
    <parameter name="barcode" value="1234567" type="string" />
    <parameter name="value1" value="2020-12" type="string" />
  </parameters>
  <name>DE-Mahlow Brother Accredited</name>
  <height>300</height>
  <width>217</width>
  <string ypos="4" xpos="8" height="30" width="201" rotationAngle="0" font="Verdana" size="20" text="${value6}" />
  <string ypos="151" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="D-K-" />
  <string ypos="195" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="15015-01-06" />
  <string ypos="243" xpos="8" height="40" width="201" rotationAngle="0" font="Verdana" size="27" text="${value1}" />
  <barcode ypos="42" xpos="8" height="105" width="201" rotationAngle="0" value="${barcode}" type="ECC200" />
  <rectangle ypos="0" xpos="4" height="291" width="209" rotationAngle="0" />
  <rectangle ypos="38" xpos="4" height="201" width="209" rotationAngle="0" />
  <defaultPrinter>Brother PT-P750W</defaultPrinter>
</template>';

PRINT 'Inserting alligatorlabeltemplate'

SET IDENTITY_INSERT [alligatorlabeltemplate] ON

INSERT INTO [dbo].[alligatorlabeltemplate]
           ([id],[description],[templatexml],[templatetype])
     VALUES
           (5,'DE-Mahlow Brother Accredited',@Xml_Accredited,'ACCREDITED'),
           (6,'DE-Mahlow Brother Accredited Small',@Xml_Accredited,'ACCREDITED_SMALL'),
           (7,'DE-Mahlow Brother Traceable',@Xml_Traceable,'STANDARD'),
           (8,'DE-Mahlow Brother Traceable Small',@Xml_Traceable,'STANDARD_SMALL')
GO

SET IDENTITY_INSERT [alligatorlabeltemplate] OFF

-- Trescal GmbH is 6681 (will exist in all databases...)

PRINT 'Inserting alligatorlabeltemplaterule'

INSERT INTO [dbo].[alligatorlabeltemplaterule]
           ([coid],[templateid])
     VALUES
           (6681, 5),
           (6681, 6),
           (6681, 7),
           (6681, 8)

-- Create 3 new media definitions for Brother media (note lengths are fixed - aspect ration ~ 1.38 per calcs/design)

SET IDENTITY_INSERT [alligatorlabelmedia] ON

PRINT 'Inserting alligatorlabelmedia'

INSERT INTO [dbo].[alligatorlabelmedia]
(id, gap, height, name, paddingbottom, paddingleft, paddingright, paddingtop, rotationangle, width)
VALUES
(3, 0, 17, 'Brother P-touch 12 mm tape', 0, 0, 0, 0, 0, 12),
(4, 0, 25, 'Brother P-touch 18 mm tape', 0, 0, 0, 0, 0, 18),
(5, 0, 34, 'Brother P-touch 24 mm tape', 0, 0, 0, 0, 0, 24)

SET IDENTITY_INSERT [alligatorlabelmedia] OFF

-- Create printer defintion for Mahlow (though anyone could use it if named the same locally)
-- Mahlow address is 12199 (will exist in all databases...)
-- Assume 18 mm media for now (ID 4)
-- Only 180 dpi resolution supported (360x180 doesn't work yet with Alligator)

PRINT 'Inserting labelprinter'

INSERT INTO [dbo].[labelprinter]
           ([description]
           ,[path]
           ,[addrid]
           ,[darknesslevel]
           ,[ipv4]
           ,[language]
           ,[resolution]
           ,[media])
     VALUES
           ('Brother PT-P750W'
           ,''
           ,12199
           ,30
           ,''
           ,'BROTHER'
           ,180
           ,4)
GO

-- Label printer ID will likely vary by system

DECLARE @labelprinterid int;

SELECT @labelprinterid = id FROM labelprinter WHERE description = 'Brother PT-P750W';

-- Mahlow subdiv is 6929 (will exist in all databases)
-- Update prference for the new label printer for all Mahlow contacts, as well as others who may want to use for testing
-- (Tina/Egbert/Michal/Galen)

PRINT 'Updating user preferences for additional users'

UPDATE [dbo].[userpreferences]
   SET [labelprinterid] = @labelprinterid
 WHERE contactid in (SELECT personid FROM contact WHERE subdivid = 6929);

PRINT 'Updating user preferences for additional users'

UPDATE [dbo].[userpreferences]
   SET [labelprinterid] = @labelprinterid
 WHERE contactid in (SELECT personid FROM users WHERE username in ('michal.herlender','tina.kuhbach','egbert.fohry','GalenB'));

INSERT INTO dbversion(version) VALUES (609);

COMMIT TRAN