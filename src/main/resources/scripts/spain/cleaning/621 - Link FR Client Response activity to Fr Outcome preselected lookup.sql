use atlas
go

BEGIN TRAN

	-- to be able to simulate the client respone, we link the activity 'Client respoonse received'
	-- to outcome 'FrOutcomePreselected' so we will not be blocked by the status 'awaiting fr outcome'
	declare @clientResponseReceived int;
	declare @frOutcomePreselected int;

	select @clientResponseReceived = iss.stateid from itemstate iss
		where iss.description like 'Client response received from failure report';

	select @frOutcomePreselected = li.id from lookupinstance li 
		where li.lookup = 'Lookup_FailureReportOutcomePreselected (inhouse)';

	delete from outcomestatus where activityid = @clientResponseReceived;

	update itemstate set lookupid = @frOutcomePreselected
		where stateid = @clientResponseReceived;


	-- for onsite
		declare @onsiteClientResponseReceived int;
	declare @onsiteFrOutcomePreselected int;

	select @onsiteClientResponseReceived = iss.stateid from itemstate iss
		where iss.description like 'Onsite - client response received from failure report';

	select @onsiteFrOutcomePreselected = li.id from lookupinstance li 
		where li.lookup = 'Lookup_FailureReportOutcomePreselected (onsite)';

	delete from outcomestatus where activityid = @onsiteClientResponseReceived;

	update itemstate set lookupid = @onsiteFrOutcomePreselected
		where stateid = @onsiteClientResponseReceived;
		
		-- add hook for sending the failure report
	declare @awaitingFrToBeSent int;
	declare @frSent int;

	select @awaitingFrToBeSent = iss.stateid from itemstate iss
		where iss.description like 'Awaiting Fault Report to be sent';

	select @frSent = iss.stateid from itemstate iss
		where iss.description like 'Fault Report sent to client';

	declare @hookid int;
	INSERT INTO hook (active,alwayspossible,beginactivity,completeactivity,name)
		VALUES (1,0,1,1,'send-failure-report-to-client');
	select @hookid = max(id) from hook;

	INSERT INTO hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
		VALUES (1,1,@frSent,@hookid,@awaitingFrToBeSent);


	declare @onsiteAwaitingFrToBeSent int;
	declare @onsiteFrSent int;

	select @onsiteAwaitingFrToBeSent = iss.stateid from itemstate iss
		where iss.description like 'Onsite - awaiting Fault Report to be sent';

	select @onsiteFrSent = iss.stateid from itemstate iss
		where iss.description like 'Onsite - failure report sent to client';

	INSERT INTO hookactivity (beginactivity,completeactivity,activityid,hookid,stateid)
		VALUES (1,1,@onsiteFrSent,@hookid,@onsiteAwaitingFrToBeSent);
		
	INSERT INTO dbversion(version) VALUES(621);

COMMIT TRAN