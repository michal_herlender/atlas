USE [spain]
GO

BEGIN TRAN

-- add a new group link 'costing issued' to state 'Job costing issued to client'
INSERT INTO spain.dbo.stategrouplink (groupid,stateid,[type]) VALUES (55,34,'ALLOCATED_SUBDIV');

-- add a new group link 'costing issued' to state 'Repair/adjustment costing issued to client'
INSERT INTO spain.dbo.stategrouplink (groupid,stateid,[type]) VALUES (55,41,'ALLOCATED_SUBDIV');

-- add a new group link 'costing issued' to state 'Repair costing issued to client'
INSERT INTO spain.dbo.stategrouplink (groupid,stateid,[type]) VALUES (55,59,'ALLOCATED_SUBDIV');


INSERT INTO dbversion(version) VALUES(472);

COMMIT TRAN