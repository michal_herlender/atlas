USE [spain]
Begin Tran

-- Update groupe id of new In Transit group
UPDATE dbo.stategrouplink SET groupid=49 WHERE id IN (85, 89, 115, 215, 250);

-- add new Workassignment to new In Transit group
INSERT INTO dbo.workassignment (depttypeid,stategroupid)
VALUES (4,49);

INSERT INTO dbversion(version) VALUES(345);

COMMIT TRAN