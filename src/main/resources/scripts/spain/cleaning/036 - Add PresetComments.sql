/*
	Initialization of the table dbo.presetcomment
	1�) Empty the table (cf.point 99 of file Spain Localization Project)
	2�) Initialize only spanish data (cf.point 100 of file Spain Localization Project)
	
	Modification date : 2016-05-10 : Loeung Sothy
*/
BEGIN TRAN

USE [spain];
GO

-- Initialize parameters
DECLARE @Date DATETIME = GETDATE()
DECLARE @UserID INT 

-- Gets the CWMS Auto User
SELECT @UserID = personid FROM dbo.contact WITH (NOLOCK) WHERE firstname = 'CWMS Auto User'

-- Empty the table dbo.presetcomment
DELETE FROM dbo.presetcomment

-- Insert spanish preset
SET IDENTITY_INSERT dbo.presetcomment ON

IF EXISTS (SELECT 1 FROM dbo.presetcomment)
BEGIN
	INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
	VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcomment WITH (NOLOCK)),'email','Estimado se�or(a), adjunto factura de nuestros servicios. Para cualquier aclaraci�n, por favor contacte con el departamento de contabilidad, tel�fono 916250900, mail a Estela.lorenzo@trescal.com',@Date,@UserID,@UserID)
END
ELSE
BEGIN
	INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
	VALUES (1,'email','Estimado se�or(a), adjunto factura de nuestros servicios. Para cualquier aclaraci�n, por favor contacte con el departamento de contabilidad, tel�fono 916250900, mail a Estela.lorenzo@trescal.com',@Date,@UserID,@UserID)
END

INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcomment WITH (NOLOCK)),'email','"Estimado se�or(a), adjunto factura. Para cualquier aclaraci�n, por favor contacte con nuestro departamento de ventas."',@Date,@UserID,@UserID)
INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcomment WITH (NOLOCK)),'email','Este mensaje es �nicamente informativo y no requiere respuesta. Adjunto confirmaci�n de los costes de calibraci�n y detalles de env�o para sus equipos de medida.',@Date,@UserID,@UserID)
INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcomment WITH (NOLOCK)),'email','Adjunto costes de calibraci�n/reparaci�n para sus equipos de medida a la espera de su aprobaci�n / orden de compras.',@Date,@UserID,@UserID)
INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcomment WITH (NOLOCK)),'email','Los detalles de la aver�a y sus costes se encuentran en la p�gina 2 del documento adjunto.',@Date,@UserID,@UserID)
INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcomment WITH (NOLOCK)),'email','Adjunto costes de calibraci�n y detalles de env�o para sus equipos de medida a la espera de su aprobaci�n / orden de compras.',@Date,@UserID,@UserID)
INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcomment WITH (NOLOCK)),'email','Adjunto confirmaci�n de costes de calibraci�n / reparaci�n para su equipo de medida a la espera de su aprobaci�n / orden de compras.',@Date,@UserID,@UserID)
INSERT INTO dbo.presetcomment (id,type,comment,seton,log_userid,setbyid)
VALUES ((SELECT MAX(id) + 1 FROM dbo.presetcomment WITH (NOLOCK)),'email','Hemos recibido su equipo para calibraci�n pero no podemos realizarla a falta de informaci�n adicional (por favor, vea documento adjunto)',@Date,@UserID,@UserID)

SET IDENTITY_INSERT dbo.presetcomment OFF

ROLLBACK TRAN