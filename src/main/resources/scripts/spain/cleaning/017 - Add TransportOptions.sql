/* Add Spain Transport Options for Zaragoza, Bilbao, Iberica - Author Galen Beck 2016-03-03 */

BEGIN TRAN 

USE [SPAIN];
GO

INSERT INTO [transportoption]
           ([active]
           ,[dayofweek]
           ,[transportmethodid]
           ,[subdivid])
     VALUES
           (1,2,1,6644),
           (1,3,1,6644),
           (1,4,1,6644),
           (1,5,1,6644),
           (1,6,1,6644),
           (1,null,2,6644),
           (1,null,3,6644),
           (1,null,4,6644),
           (1,null,5,6644),
           (1,null,6,6644),
           (1,2,1,6643),
           (1,3,1,6643),
           (1,4,1,6643),
           (1,5,1,6643),
           (1,6,1,6643),
           (1,null,2,6643),
           (1,null,3,6643),
           (1,null,4,6643),
           (1,null,5,6643),
           (1,null,6,6643),
           (1,2,1,6642),
           (1,3,1,6642),
           (1,4,1,6642),
           (1,5,1,6642),
           (1,6,1,6642),
           (1,null,2,6642),
           (1,null,3,6642),
           (1,null,4,6642),
           (1,null,5,6642),
           (1,null,6,6642);
GO

ROLLBACK TRAN
GO