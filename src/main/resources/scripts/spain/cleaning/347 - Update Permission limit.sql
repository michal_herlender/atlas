USE [spain]
Begin Tran

Update dbo.permission set permission='GENEX_MEDIUM_PURCHASE_LIMIT'
where permission='GENEX_MEDUIM_PURCHASE_LIMIT';
Update dbo.permission set permission='OPEX_MEDIUM_PURCHASE_LIMIT'
where permission='OPEX_MEDUIM_PURCHASE_LIMIT';
Update dbo.permission set permission='CAPEX_MEDIUM_PURCHASE_LIMIT'
where permission='CAPEX_MEDUIM_PURCHASE_LIMIT';


Update dbo.purchasepermissionbycompany set permissionName='GENEX_MEDIUM_PURCHASE_LIMIT'
where permissionName='GENEX_MEDUIM_PURCHASE_LIMIT';
Update dbo.purchasepermissionbycompany set permissionName='OPEX_MEDIUM_PURCHASE_LIMIT'
where permissionName='OPEX_MEDUIM_PURCHASE_LIMIT';
Update dbo.purchasepermissionbycompany set permissionName='CAPEX_MEDIUM_PURCHASE_LIMIT'
where permissionName='CAPEX_MEDUIM_PURCHASE_LIMIT';


INSERT INTO dbversion(version) VALUES(347);

COMMIT TRAN