INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'APPROVE', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname IN ('On-Site Accredited Calibration','On-Site Standard Calibration'); 

INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'REMOVED', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname IN ('On-Site Accredited Calibration','On-Site Standard Calibration')

INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'SUPERVISED', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname IN ('On-Site Accredited Calibration','On-Site Standard Calibration')

INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'PERFORM', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname IN ('On-Site Accredited Calibration')

INSERT INTO [dbo].[calibrationaccreditationlevel] 
([accredlevel] ,[caltypeid]) 
SELECT 'SIGN', [caltypeid] 
FROM [dbo].[calibrationtype] 
LEFT JOIN servicetype ON servicetype.servicetypeid = calibrationtype.servicetypeid
WHERE servicetype.longname IN ('On-Site Standard Calibration')
