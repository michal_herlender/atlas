-- DEV-2522 : JOB PRICING (after Quotation received from third party) : Client decision registered through the job pricing should update workflow status.

USE [atlas]
GO

BEGIN TRAN

DECLARE @activity_tp_clientresponse INT,
		@status_tp_clientresponse INT,
		@hook_jc_clientdecision INT;

SELECT @status_tp_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Awaiting client approval for third party work after job costing';
PRINT @status_tp_clientresponse;

SELECT @activity_tp_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Client decision received on third party work after job costing';
PRINT @activity_tp_clientresponse;

-- Eventually, we would also (possibly) want to disallow the manual entry - keeping for now as fallback
--UPDATE dbo.nextactivity SET manualentryallowed = 0 WHERE statusid = @status_tp_clientresponse AND activityid = @activity_tp_clientresponse;
	 
SELECT @hook_jc_clientdecision = id FROM dbo.hook WHERE [name] = 'job-costing-client-decision';
	 
INSERT INTO dbo.hookactivity(beginactivity, completeactivity, activityid, hookid, stateid)
VALUES
(1, 1, @activity_tp_clientresponse, @hook_jc_clientdecision, @status_tp_clientresponse);

INSERT INTO dbversion(version) VALUES (830);

COMMIT TRAN