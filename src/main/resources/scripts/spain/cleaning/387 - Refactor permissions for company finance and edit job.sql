-- Author Galen Beck - 2019-01-24
-- Creates new permission for editing job, and renames
-- Just two screens for editing finance settings now:
-- 1 - for business companies
-- 2 - for other companies (supplier, utility, prospect, client)

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[permission]
   SET [permission] = 'COMPANY_FINANCE_EDIT_BUSINESS'
 WHERE [permission] = 'BUSINESS_COMPANY_EDIT'
GO

UPDATE [dbo].[permission]
   SET [permission] = 'COMPANY_FINANCE_EDIT_NON_BUSINESS'
 WHERE [permission] = 'COMPANY_CLIENT_EDIT'
GO

DELETE FROM [dbo].[permission]
      WHERE [permission] = 'COMPANY_SUPPLIER_EDIT'
GO

INSERT INTO [dbo].[permission]
           ([groupId]
           ,[permission])
     VALUES
           (1,'JOB_EDIT')
GO

INSERT INTO dbversion(version) VALUES(387);

COMMIT TRAN 