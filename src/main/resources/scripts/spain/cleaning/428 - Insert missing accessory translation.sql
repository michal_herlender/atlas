USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[accessorytranslation]
           ([accessoryid]
           ,[locale]
           ,[translation])
     VALUES
           (37
           ,'fr_FR'
           ,'bo�te d''origine de l''�quipement')
GO

INSERT INTO dbversion (version) VALUES (428);
GO

COMMIT TRAN