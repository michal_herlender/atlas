USE [spain];


BEGIN TRAN

	ALTER TABLE dbo.repairinspectionreport ADD repaircompletionreportid int NULL;
	ALTER TABLE dbo.repairinspectionreport ADD CONSTRAINT fk_rir_rcr FOREIGN KEY (repaircompletionreportid) REFERENCES repaircompletionreport(id)

	GO

	UPDATE dbo.repairinspectionreport  SET repaircompletionreportid = rcr.id FROM dbo.repairinspectionreport AS rir JOIN dbo.repaircompletionreport AS rcr ON rir.id = rcr.repairinspectionreportid
	
	GO

	ALTER TABLE dbo.repaircompletionreport DROP CONSTRAINT repaircompletionreport_repairinspectionreport_FK;
	ALTER TABLE dbo.repaircompletionreport DROP COLUMN repairinspectionreportid;

	INSERT INTO dbversion (version) VALUES (458);

COMMIT TRAN