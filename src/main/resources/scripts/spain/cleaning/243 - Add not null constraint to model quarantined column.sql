-- Updates models (just 1 in production) where quarantined flag not set
-- Adds constraint to table
-- Galen Beck - 2018-04-03

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[instmodel]
   SET [quarantined] = 0
 WHERE quarantined is NULL;

GO

ALTER TABLE [dbo].[instmodel]
	ALTER COLUMN [quarantined] bit not null;

GO

insert into dbversion(version) values(243);

COMMIT TRAN