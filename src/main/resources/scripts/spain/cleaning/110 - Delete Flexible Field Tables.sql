-- Be careful running this!
-- Run it only once before starting version 2275 or above


BEGIN TRAN

DROP TABLE instrumentfielddefinitionycustomer
GO

DROP TABLE instrumentfielddefinitiontranslation
GO

DROP TABLE instrumentvaluestring
GO

DROP TABLE instrumentvalueselection
GO

DROP TABLE instrumentvaluenumeric
GO

DROP TABLE instrumentvaluedatetime
GO

DROP TABLE instrumentvalueboolean
GO

DROP TABLE instrumentvalue
GO

DROP TABLE instrumentfieldvalue
GO

DROP TABLE instrumentfieldlibrary
GO

DROP TABLE instrumentfielddefinition
GO

ROLLBACK TRAN