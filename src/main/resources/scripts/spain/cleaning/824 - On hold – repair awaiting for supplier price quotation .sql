USE [atlas]
GO

BEGIN TRAN

DECLARE @itemstateID int

SELECT @itemstateID = it.stateid FROM itemstate it WHERE it.description LIKE '%repair awaiting for supplier price quotation%';

INSERT INTO stategrouplink (groupid, stateid, [type]) VALUES(39, @itemstateID, 'ALLOCATED_SUBDIV');

INSERT INTO dbversion VALUES (824)

COMMIT TRAN