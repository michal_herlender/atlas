-- Script to alter SQL function in database
-- 
-- Initial script used a 'dbo' reference which was persisted as 'spain.dbo', 
-- so function would error out, when run on a database restored as 'spain-test' or other name.
-- This updates it to use a generic name.  Fixes invoice home error on Spain test server.

BEGIN TRAN

USE [spain]
GO
/****** Object:  UserDefinedFunction [dbo].[getDeliveryDateFromLastCreatedClientDelivery]    Script Date: 6/18/2018 9:48:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER function [dbo].[getDeliveryDateFromLastCreatedClientDelivery] (@jobitemID int) returns Date
as
begin 
	declare @deliveryDate date
		
	select @deliveryDate = CAST(deliveryDate as date)
	from (
		select top 1 d.deliverydate as deliveryDate
		from delivery d
		join deliveryitem di on di.deliveryid=d.deliveryid
		join jobitem ji on ji.jobitemid = di.jobitemid
		where ji.jobitemid=@jobitemID
		order by d.creationdate desc , d.deliveryid desc
	) as deliveryDate
		
	return @deliveryDate
	
end

GO
	
INSERT INTO dbversion(version) VALUES(272);

COMMIT TRAN