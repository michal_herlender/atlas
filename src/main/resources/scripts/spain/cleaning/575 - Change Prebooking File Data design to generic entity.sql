-- Note : There are three databases referenced below in USE and one in the first UPDATE, 
-- four manual changes needed, if running against databases different than [atlas] and [atlasfiles]

USE [atlas]
GO

BEGIN TRAN

UPDATE  pfd
SET     [fileName] = pf.[fileName], prebookingId = asn.id
FROM    atlasfiles.dbo.PrebookingFileData pfd
        INNER JOIN dbo.prebookingfile pf
            ON pfd.prebookingId = pf.id
		INNER JOIN atlas.dbo.asn asn
			ON asn.id = pf.prebookingid;

ALTER TABLE dbo.prebookingfile DROP CONSTRAINT prebookingfile_asn_FK;
ALTER TABLE dbo.prebookingfile DROP CONSTRAINT prebookingfile_contact_FK;
ALTER TABLE dbo.prebookingfile DROP CONSTRAINT prebookingfile_contact_FK_1;

DROP TABLE dbo.prebookingfile;

COMMIT TRAN

USE [atlasfiles]
GO

BEGIN TRAN

EXEC sp_rename 'PrebookingFileData', 'ExchangeFormatFileData';

EXEC sp_rename 'ExchangeFormatFileData.prebookingId' , 'entityId', 'COLUMN';

COMMIT TRAN

USE [atlas]
GO

BEGIN TRAN

INSERT INTO dbversion(version) VALUES (575)

COMMIT TRAN