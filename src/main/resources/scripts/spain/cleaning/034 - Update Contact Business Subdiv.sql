-- Author Tony Provost 2016-05-09
-- Update default contact on business subdiv: FirstName = 'Contact', LastName = Name of subdiv

BEGIN TRAN 

USE [spain];
GO

UPDATE dbo.contact
SET firstname = 'Contact'
	,lastName = (
		SELECT subname
		FROM dbo.subdiv
		WHERE subdiv.subdivid = contact.subdivid
		)
	,lastModified = GETDATE()
WHERE firstName = 'Standard'
	AND subdivid IN (
		SELECT dbo.subdiv.subdivid
		FROM dbo.subdiv
			,dbo.company
		WHERE company.coid = subdiv.coid
			AND company.corole = 5
		)

-- Change to COMMIT when successfully tested
ROLLBACK TRAN