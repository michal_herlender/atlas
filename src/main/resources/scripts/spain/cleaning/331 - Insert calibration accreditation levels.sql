-- Normalizes all cal types to have the same accreditation / authorization levels possible
-- Some were not configured before

USE [spain]
GO

BEGIN TRAN

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='SIGN' AND caltypeid='1') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('SIGN', 1);
	GO

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='PERFORM' AND caltypeid='2') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('PERFORM', 2);
	GO

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='PERFORM' AND caltypeid='3') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('PERFORM', 3);
	GO

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='SIGN' AND caltypeid='25') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('SIGN', 25);
	GO

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='PERFORM' AND caltypeid='29') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('PERFORM', 29);
	GO

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='PERFORM' AND caltypeid='48') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('PERFORM', 48);
	GO

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='SIGN' AND caltypeid='48') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('SIGN', 48);
	GO

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='PERFORM' AND caltypeid='50') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('PERFORM', 50);
	GO

	IF NOT EXISTS (SELECT 1 FROM calibrationaccreditationlevel WHERE accredlevel='SIGN' AND caltypeid='50') 
		INSERT INTO calibrationaccreditationlevel (accredlevel, caltypeid) VALUES ('SIGN', 50);
	GO

	INSERT INTO dbversion(version) VALUES(331);

COMMIT TRAN
