-- 68 = Quality Control Required (used by cal tool to enforce for certain customers/subdivs)
-- 69 = Recall Date Adjustment (also used by cal tool in certificate generation)
USE [atlas]
GO

BEGIN TRAN

declare @ID_group int;
select @ID_group = id from systemdefaultgroup where dname = 'Client - Certificate';

set identity_insert systemdefault ON;
INSERT  into systemdefault (defaultid,defaultdatatype,defaultdescription,defaultname,defaultvalue,groupid,groupwide,endd,start)
values(68,'boolean','Whether an additional quality control step is used in the calibration tool','Requires Quality Control','false',@ID_group,1,0,0),
      (69,'string','Whether an adjustment is performed when calculating the recall date for an instrument','Recall Date Adjustment','No',@ID_group,1,0,0);

set identity_insert systemdefault OFF;

insert into systemdefaultcorole (coroleid,defaultid)
  values (1,68),(5,68),(1,69),(5,69);

insert into systemdefaultscope (scope,defaultid)
  values (0,68),(2,68),(3,68),(0,69),(2,69),(3,69);
  
insert into systemdefaultnametranslation
  (defaultid, locale, translation)
  values 
  (68,'de_DE','Erfordert Qualitätskontrolle'),
  (68,'en_GB','Requires Quality Control'),
  (68,'es_ES','Requiere control de calidad'),
  (68,'fr_FR','Nécessite un contrôle de qualité'),
  (69,'de_DE','Anpassung des Rückrufdatums'),
  (69,'en_GB','Recall Date Adjustment'),
  (69,'es_ES','Ajuste de la fecha de próxima calibración'),
  (69,'fr_FR','Ajustement de la date de rappel');

insert into systemdefaultdescriptiontranslation
  (defaultid, locale, translation)
  values 
  (68,'de_DE',''),
  (68,'en_GB','Whether an additional quality control step is used in the calibration tool'),
  (68,'es_ES',''),
  (68,'fr_FR',''),
  (69,'de_DE',''),
  (69,'en_GB','Whether an adjustment is performed when calculating the recall date for an instrument'),
  (69,'es_ES',''),
  (69,'fr_FR','');

INSERT INTO dbversion(version) VALUES(787);

COMMIT TRAN
