-- Script to update the 'Book Jobs' user group value from 'no' to 'yes' for all generic (company-less) user groups
-- Necessary update as any contacts using new generic groups will not show up in Goods In as selectable contact
-- Author Galen Beck - 2016-07-18

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[usergroupvalues]
   SET [valueid] = 1
 WHERE [valueid] = 2 AND [groupid] IN
       (SELECT [groupid] FROM [spain].[dbo].[usergroup] WHERE coid is NULL);
GO

COMMIT TRAN