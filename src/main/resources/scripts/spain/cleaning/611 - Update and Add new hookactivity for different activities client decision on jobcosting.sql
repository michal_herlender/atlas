USE [atlas]
GO

BEGIN TRAN

DECLARE @activity_adj_clientresponse INT,
		@activity_exch_clientresponse INT,
		@activity_insp_clientresponse INT,
		@activity_onsite_clientresponse INT,
		@activity_rep_clientresponse INT,
		@status_adj_clientresponse INT,
		@status_exch_clientresponse INT,
		@status_insp_clientresponse INT,
		@status_onsite_clientresponse INT,
		@status_rep_clientresponse INT,
		@hook_jc_clientdecision INT;

SELECT @status_adj_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Costing issued for adjustment - awaiting client decision';
SELECT @status_exch_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Exchange costing issued, awaiting client decision';
SELECT @status_insp_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Inspection costing issued - awaiting approval';
SELECT @status_onsite_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Onsite - costing issued for repair/adjustment - awaiting client decision';
SELECT @status_rep_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Costing issued for repair - awaiting client decision for repair';

SELECT @activity_adj_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Client decision received for adjustment costing';
SELECT @activity_exch_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Client decision received on exchange';
SELECT @activity_insp_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Approval received';
SELECT @activity_onsite_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Onsite - client decision received';
SELECT @activity_rep_clientresponse = stateid FROM dbo.itemstate WHERE [description] = 'Client decision received for repair';

UPDATE dbo.nextactivity SET manualentryallowed = 0 WHERE statusid = @status_adj_clientresponse AND activityid = @activity_adj_clientresponse;
UPDATE dbo.nextactivity SET manualentryallowed = 0 WHERE statusid = @status_exch_clientresponse AND activityid = @activity_exch_clientresponse;
UPDATE dbo.nextactivity SET manualentryallowed = 0 WHERE statusid = @status_insp_clientresponse AND activityid = @activity_insp_clientresponse;
UPDATE dbo.nextactivity SET manualentryallowed = 0 WHERE statusid = @status_onsite_clientresponse AND activityid = @activity_onsite_clientresponse;
UPDATE dbo.nextactivity SET manualentryallowed = 0 WHERE statusid = @status_rep_clientresponse AND activityid = @activity_rep_clientresponse;
	 
SELECT @hook_jc_clientdecision = id FROM dbo.hook WHERE [name] = 'job-costing-client-decision';
	 
INSERT INTO dbo.hookactivity(beginactivity, completeactivity, activityid, hookid, stateid)
VALUES
(1, 1, @activity_adj_clientresponse, @hook_jc_clientdecision, @status_adj_clientresponse),
(1, 1, @activity_exch_clientresponse, @hook_jc_clientdecision, @status_exch_clientresponse),
(1, 1, @activity_insp_clientresponse, @hook_jc_clientdecision, @status_insp_clientresponse),
(1, 1, @activity_onsite_clientresponse, @hook_jc_clientdecision, @status_onsite_clientresponse),
(1, 1, @activity_rep_clientresponse, @hook_jc_clientdecision, @status_rep_clientresponse);

INSERT INTO dbversion(version) VALUES (611);

COMMIT TRAN