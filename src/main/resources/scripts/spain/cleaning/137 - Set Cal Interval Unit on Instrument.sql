-- Script to update any instruments created today where cal interval unit not set
-- Galen Beck 2016-04-21

USE [spain]
GO

BEGIN TRAN

UPDATE [dbo].[instrument]
   SET [calintervalunit] = 'MONTH'
 WHERE calintervalunit is NULL;

GO

COMMIT TRAN;