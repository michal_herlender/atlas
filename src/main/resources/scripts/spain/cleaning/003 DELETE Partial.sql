-- Author Galen Beck 2015-01-15
-- Step 2 of deletion - partial deletion of tables in order that should not violate referential constraints

BEGIN TRAN

USE [spain]

UPDATE [dbo].[contact] SET [groupid] = 1;
UPDATE [dbo].[contact] SET [subdivid] = 6645 WHERE [personid] = 8740;
UPDATE [dbo].[contact] SET [subdivid] = 6645 WHERE [personid] = 5064;
UPDATE [dbo].[contact] SET [defaddress] = 11757 WHERE [personid] = 8740;
UPDATE [dbo].[contact] SET [defaddress] = 11757 WHERE [personid] = 5064;
DELETE FROM [dbo].[addresssettingsforallocatedcompany] WHERE [addressid] NOT IN (11755, 11756, 11757, 11758) OR [orgid] NOT IN (6578, 6579);
DELETE FROM [dbo].[companysettingsforallocatedcompany] WHERE [orgid] NOT IN (6578, 6579) OR [companyid] NOT IN (6578, 6579);
DELETE FROM [dbo].[calibrationtype] WHERE [orgid] NOT IN (6642, 6643, 6644, 6645);
DELETE FROM [dbo].[user_role] WHERE [orgid] NOT IN (6578, 6579);
DELETE FROM [dbo].[usergroupvalues] WHERE [groupid] <> 1;
DELETE FROM [dbo].[usergroup] WHERE [systemdefault] = 0;
DELETE FROM [dbo].[userpreferences] WHERE [contactid] NOT IN (SELECT [personid] from [dbo].[contact] WHERE [subdivid] IN (6642, 6643, 6644, 6645));
DELETE FROM [dbo].[users] WHERE [personid] NOT IN (SELECT [personid] from [dbo].[contact] WHERE [subdivid] IN (6642, 6643, 6644, 6645));
UPDATE [subdiv] SET [log_userid] = NULL;
UPDATE [subdiv] SET [defpersonid] = NULL;
UPDATE [company] SET [createdby] = NULL;
UPDATE [company] SET [log_userid] = NULL;
UPDATE [address] SET [log_userid] = NULL;
UPDATE [addressfunction] SET [log_userid] = NULL;
UPDATE [users] SET [log_userid] = NULL;
UPDATE [contact] SET [log_userid] = NULL;
DELETE FROM [dbo].[contact] WHERE [subdivid] NOT IN (6642, 6643, 6644, 6645);
UPDATE [department] SET [subdivid] = 6645 WHERE [typeid] IN (1, 2, 5);
DELETE FROM [dbo].[department] WHERE [subdivid] NOT IN (6642, 6643, 6644, 6645);
DELETE FROM [dbo].[location] WHERE [addressid] NOT IN (11755, 11756, 11757, 11758);
DELETE FROM [dbo].[addressfunction] WHERE [addressid] NOT IN (11755, 11756, 11757, 11758);
UPDATE [subdiv] SET [defaddrid] = NULL;
UPDATE [department] SET [addrid] = 11757;
DELETE FROM [dbo].[address] WHERE [addrid] NOT IN (11755, 11756, 11757, 11758);
UPDATE [transportoption] set [subdivid] = 6645;
UPDATE [company] SET [defsubdivid] = NULL;
DELETE FROM [dbo].[subdiv] WHERE [subdivid] NOT IN (6642, 6643, 6644, 6645);
DELETE FROM [dbo].[company] WHERE [coid] NOT IN (6578, 6579);
INSERT INTO [dbo].[user_role] ([rolename], [username], [orgid]) SELECT 'ROLE_ADMINISTRATOR', [username], 6644 FROM [users];
INSERT INTO [dbo].[user_role] ([rolename], [username], [orgid]) SELECT 'ROLE_ADMINISTRATOR', [username], 6645 FROM [users];
INSERT INTO [dbo].[user_role] ([rolename], [username], [orgid]) SELECT 'ROLE_ADMINISTRATOR', [username], 6642 FROM [users];
INSERT INTO [dbo].[user_role] ([rolename], [username], [orgid]) SELECT 'ROLE_ADMINISTRATOR', [username], 6643 FROM [users];

ROLLBACK TRAN