USE [spain]

BEGIN TRAN

	UPDATE dbo.faultreport
	SET version='FAULT_REPORT'
	WHERE (select count(*) from dbo.faultreportstate where dbo.faultreportstate.faultreportid = dbo.faultreport.faultrepid) = 0 
	go

	UPDATE dbo.faultreport
	SET version='FAILURE_REPORT'
	WHERE (select count(*) from dbo.faultreportstate where dbo.faultreportstate.faultreportid = dbo.faultreport.faultrepid) > 0
	go

	INSERT INTO dbversion(version) VALUES(340);

COMMIT TRAN