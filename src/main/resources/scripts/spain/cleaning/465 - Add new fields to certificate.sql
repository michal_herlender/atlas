USE [spain]
GO

BEGIN TRAN

	ALTER TABLE dbo.certificate ADD optimization bit NULL;
	ALTER TABLE dbo.certificate ADD repair bit NULL;
	ALTER TABLE dbo.certificate ADD restriction bit NULL;
	ALTER TABLE dbo.certificate ADD adjustment bit NULL;

	INSERT INTO dbversion(version) VALUES(465);

COMMIT TRAN