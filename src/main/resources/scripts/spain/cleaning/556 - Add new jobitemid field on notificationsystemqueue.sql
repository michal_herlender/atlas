USE [atlas]
GO

BEGIN TRAN

ALTER TABLE dbo.notificationsystemqueue ADD jobitemid INT;

ALTER TABLE dbo.notificationsystemqueue ADD CONSTRAINT fk_notificationsystemqueue_jobitem 
FOREIGN KEY (jobitemid) REFERENCES jobitem(jobitemid);

ALTER TABLE dbo.notificationsystemqueue ALTER COLUMN operationtype VARCHAR(32);

INSERT INTO dbversion(version) VALUES(556);

COMMIT TRAN