-- remove the lookup from all workflow wirings
USE [atlas]
GO

BEGIN TRAN
--wire activity 'Certificate signed' to lookup 90
UPDATE dbo.itemstate SET lookupid=90 WHERE stateid=32
GO

--wire activity 'Certificate completion to follow' to lookup 90
UPDATE dbo.itemstate SET lookupid=90 WHERE stateid=199 
GO

UPDATE dbo.outcomestatus SET lookupid=90 WHERE id=46 
GO
UPDATE dbo.outcomestatus SET lookupid=90 WHERE id=195 
GO

-- wire activity 'Received at returning business subdivision' to status 90
UPDATE dbo.itemstate SET lookupid=90 WHERE stateid=4073
GO

-- add manual activity 'no PO required' after status 'awaiting po for intercompany work'
-- GB note : this is actually not implemented here, but in script 488 instead.
UPDATE dbo.itemstate SET lookupid=90 WHERE stateid=4073 
GO

INSERT INTO dbversion(version) VALUES(487);

COMMIT TRAN