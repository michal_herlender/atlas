USE [atlas]
;

BEGIN TRAN

	declare @lu1 int, @lu2 int, @statusid int, @os1 int, @act1 int, @ao1 int, @ao2 int, @newstatus int, @statusid2 int, @os2 int, @os3 int, @act2 int;

	select @lu1 = lui.id from lookupinstance lui where lui.lookup = 'Lookup_TPRequirements (third party after requirements recorded)';

	select @lu2 = lui.id from lookupinstance lui where lui.lookup = 'Lookup_TPRequirements (third party after item despatched)';

	select @statusid = ist.stateid from itemstate ist where ist.description = 'Awaiting delivery note to third party' and ist.retired = 0;

	INSERT INTO outcomestatus (defaultoutcome,statusid) VALUES (0,@statusid);

	select @os1 = max(id) from outcomestatus;

	select @act1 = ist.stateid from itemstate ist where ist.description = 'Third party requirements recorded'; 

	INSERT INTO lookupresult ([result],activityid,lookupid,outcomestatusid,resultmessage)
		VALUES ('h',@act1,@lu1,@os1,7);
	INSERT INTO lookupresult ([result],activityid,lookupid,outcomestatusid,resultmessage)
		VALUES ('i',@act1,@lu1,@os1,8);


	INSERT INTO actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
		VALUES (0,'Item is covered by a specific contract with the supplier, no supplier PO required, and no return back to Trescal',0,@act1,1,'tp_requirements','SUPPLIER_CONTRACT_NO_PO_REQUIRED_NO_RETURN');

	select @ao1 = max(ao.id) from actionoutcome ao;

	INSERT INTO actionoutcometranslation (id,locale,[translation])
		VALUES (@ao1,'de_DE','Der Artikel unterliegt einem bestimmten Vertrag mit dem Lieferanten, es ist keine Lieferantenbestellung erforderlich und keine R�cksendung an Trescal') ;
	INSERT INTO actionoutcometranslation (id,locale,[translation])
		VALUES (@ao1,'en_GB','Item is covered by a specific contract with the supplier, no supplier PO required, and no return back to Trescal') ;
	INSERT INTO actionoutcometranslation (id,locale,[translation])
		VALUES (@ao1,'es_ES','El art�culo est� cubierto por un contrato espec�fico con el proveedor, no se requiere una orden de compra del proveedor y no se devuelve a Trescal') ;
	INSERT INTO actionoutcometranslation (id,locale,[translation])
		VALUES (@ao1,'fr_FR','L''article est couvert par un contrat sp�cifique avec le fournisseur, aucun bon de commande fournisseur n''est requis et aucun retour � Trescal') ;
	
	INSERT INTO actionoutcome (defaultoutcome,description,positiveoutcome,activityid,active,[type],value)
		VALUES (0,'Item is covered by a specific contract with the supplier, no supplier PO required, but will return back to Trescal',0,@act1,1,'tp_requirements','SUPPLIER_CONTRACT_NO_PO_REQUIRED_WITH_RETURN');

	select @ao2 = max(ao.id) from actionoutcome ao;

	INSERT INTO actionoutcometranslation (id,locale,[translation])
		VALUES (@ao2,'de_DE','Der Artikel unterliegt einem bestimmten Vertrag mit dem Lieferanten, es ist keine Lieferantenbestellung erforderlich, er wird jedoch an Trescal zur�ckgesandt') ;
	INSERT INTO actionoutcometranslation (id,locale,[translation])
		VALUES (@ao2,'en_GB','Item is covered by a specific contract with the supplier, no supplier PO required, but will return back to Trescal') ;
	INSERT INTO actionoutcometranslation (id,locale,[translation])
		VALUES (@ao2,'es_ES','El art�culo est� cubierto por un contrato espec�fico con el proveedor, no se requiere una orden de compra del proveedor, pero regresar� a Trescal') ;
	INSERT INTO actionoutcometranslation (id,locale,[translation])
		VALUES (@ao2,'fr_FR','L''article est couvert par un contrat sp�cifique avec le fournisseur, aucun bon de commande fournisseur n''est requis, mais retournera � Trescal') ;

	INSERT INTO itemstate ([type],active,description,retired)
		VALUES ('workstatus',0,'Job item transferred to third party, service completed',0);

	select @newstatus = max(ist.stateid) from itemstate ist;

	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newstatus,'de_DE','Auftrag an Dritte �bertragen, Service abgeschlossen') ;
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newstatus,'en_GB','Job item transferred to third party, service completed') ;
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newstatus,'es_ES','Elemento de trabajo transferido a un tercero, servicio completado') ;
	INSERT INTO itemstatetranslation (stateid,locale,[translation])
		VALUES (@newstatus,'fr_FR','�l�ment de travail transf�r� � un tiers, service termin�') ;

	select @statusid2 = ist.stateid from itemstate ist where ist.description = 'Unit at third party for work'; 

	INSERT INTO outcomestatus (defaultoutcome,lookupid,statusid) VALUES (0,@lu2,@newstatus);

	select @os2 = max(id) from outcomestatus;

	INSERT INTO outcomestatus (defaultoutcome,lookupid,statusid) VALUES (0,@lu2,@statusid2);

	select @os3 = max(id) from outcomestatus;

	select @act2 = ist.stateid from itemstate ist where ist.description = 'Item despatched to third party'; 

	INSERT INTO lookupresult ([result],activityid,lookupid,outcomestatusid,resultmessage)
		VALUES ('h',@act2,@lu2,@os2,7);
	INSERT INTO lookupresult ([result],activityid,lookupid,outcomestatusid,resultmessage)
		VALUES ('i',@act2,@lu2,@os3,8);


INSERT INTO dbversion(version) VALUES(726);

COMMIT TRAN