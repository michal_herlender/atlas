-- Galen Beck 2018-08-31

-- Script to manage fault report as a bona fide document (in the jobs hierarchy)
-- Note, after merge we could likely eliminate some deprecated fields (email related)

USE [spain]
GO

BEGIN TRAN

CREATE TABLE dbo.prebookingfile (
	id int NOT NULL IDENTITY(1,1),
	lastModified datetime2(7),
	addedon datetime2(7),
	fileName varchar(255),
	lastModifiedBy int,
	personid int,
	prebookingid int,
	CONSTRAINT prebookingfile_PK PRIMARY KEY (id),
	CONSTRAINT prebookingfile_asn_FK FOREIGN KEY (prebookingid) REFERENCES dbo.asn(id),
	CONSTRAINT prebookingfile_contact_FK FOREIGN KEY (personid) REFERENCES dbo.contact(personid),
	CONSTRAINT prebookingfile_contact_FK_1 FOREIGN KEY (lastModifiedBy) REFERENCES dbo.contact(personid)
) 

GO

ALTER TABLE dbo.asn ADD prebookingfileid int

GO

ALTER TABLE dbo.asn ADD CONSTRAINT asn_prebookingfile_FK FOREIGN KEY (prebookingfileid) REFERENCES dbo.prebookingfile(id)

GO

INSERT INTO dbversion(version) VALUES(304);

COMMIT TRAN
