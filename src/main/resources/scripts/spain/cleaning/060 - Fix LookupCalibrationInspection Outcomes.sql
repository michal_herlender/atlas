-- Script to fix wiring issue with workflow in lookup calibration / inspection outcome
-- Repair and third party were reversed
-- Assembla ticket # 817
-- Author Galen Beck - 2016-09-07

BEGIN TRAN

USE [spain]
GO

UPDATE [dbo].[outcomestatus]
   SET [statusid] = 89 WHERE [id] = 64;
GO

UPDATE [dbo].[outcomestatus]
   SET [statusid] = 20 WHERE [id] = 66;
GO

COMMIT TRAN