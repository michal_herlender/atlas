USE spain;
GO

INSERT INTO actionoutcome(active,activityid,defaultoutcome,description,positiveoutcome)
VALUES
(1,57,0,'Unit not repaired - no further action required',0),
(1,57,0,'Unit not repaired - further calibration necessary',0)

INSERT INTO actionoutcometranslation(id,locale,translation)
SELECT id,'en_GB','Unit not repaired - no further action required'
FROM actionoutcome
WHERE description = 'Unit not repaired - no further action required'

INSERT INTO actionoutcometranslation(id,locale,translation)
SELECT id,'en_GB','Unit not repaired - further calibration necessary'
FROM actionoutcome
WHERE description = 'Unit not repaired - further calibration necessary'

INSERT INTO outcomestatus(activityid,defaultoutcome,lookupid,statusid)
SELECT 57,0,NULL,stateid
FROM itemstate
WHERE description = 'Awaiting rejection label'

INSERT INTO lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage)
SELECT 57,30,outcomestatus.id,'',6
FROM outcomestatus
LEFT JOIN itemstate ON outcomestatus.statusid = itemstate.stateid
WHERE description = 'Awaiting rejection label' AND activityid = 57

INSERT INTO lookupresult(activityid,lookupid,outcomestatusid,result,resultmessage)
SELECT 57,30,outcomestatus.id,'',7
FROM outcomestatus
LEFT JOIN itemstate ON outcomestatus.statusid = itemstate.stateid
WHERE description = 'Awaiting post-calibration' AND activityid = 57