BEGIN TRAN

ALTER TABLE engineerallocation ALTER COLUMN timeofdayuntil varchar(255) NULL;
ALTER TABLE engineerallocation ALTER COLUMN allocateduntil date NULL;
ALTER TABLE engineerallocation ALTER COLUMN timeofday varchar(255) NULL;
ALTER TABLE engineerallocation ADD estimatedallocationtime int;


INSERT INTO dbversion(version) VALUES(860);

COMMIT TRAN