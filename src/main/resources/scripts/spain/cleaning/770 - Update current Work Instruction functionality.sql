
USE [atlas];

BEGIN TRAN;

 	-- updating the current Bilbao data to be at the business company
 	ALTER TABLE dbo.workinstruction DROP CONSTRAINT IF EXISTS FKkjc0lohvlwwoc2tn1jo3iurgk;
 	GO
 	UPDATE dbo.workinstruction 
 	SET orgid = (SELECT subdiv.coid FROM dbo.subdiv subdiv
 	LEFT JOIN dbo.company comp ON comp.coid = subdiv.coid
 	WHERE orgid = subdiv.subdivid)

 	-- add FK constraint on orgid field
 	ALTER TABLE dbo.workinstruction ADD CONSTRAINT FK_workinstruction_company FOREIGN KEY (orgid) REFERENCES dbo.company(coid);

 	-- add "descid" field to workinstruction table
 	ALTER TABLE dbo.workinstruction ADD descid INT NULL;
 	ALTER TABLE dbo.workinstruction ADD CONSTRAINT FK_workinstruction_description FOREIGN KEY (descid) REFERENCES dbo.description(descriptionid);

 	-- remove "processId" filed from workinstruction table
 	ALTER TABLE dbo.workinstruction DROP CONSTRAINT IF EXISTS FKfy8traj4g7gra3jhebt32aatd;
 	ALTER TABLE dbo.workinstruction DROP COLUMN processId;

 	-- remove "defaultTemplate" field from workinstruction table
 	ALTER TABLE dbo.workinstruction DROP COLUMN defaultTemplate; 

 	-- remove "lab" field from workinstruction table
 	ALTER TABLE dbo.workinstruction DROP COLUMN lab; 
 	
 	-- remove "log_lastmodified" field from workinstruction table
  	ALTER TABLE dbo.workinstruction DROP COLUMN log_lastmodified; 

  	-- remove "ownerid" filed from workinstruction table
 	ALTER TABLE dbo.workinstruction DROP CONSTRAINT IF EXISTS FKa3jslpdouiul5rnaqlbosvpg8;
 	DROP INDEX IDX_workinstruction_ownerid ON dbo.workinstruction;
 	ALTER TABLE dbo.workinstruction DROP COLUMN ownerid;

   	-- remove "calibrationprocess" filed from workinstruction table
   	ALTER TABLE dbo.workinstruction DROP CONSTRAINT IF EXISTS FK_workinstruction_calibrationprocess;
 	ALTER TABLE dbo.workinstruction DROP COLUMN calibrationprocess;
 
 	INSERT INTO dbversion(version) VALUES(770);
 
 COMMIT TRAN;
