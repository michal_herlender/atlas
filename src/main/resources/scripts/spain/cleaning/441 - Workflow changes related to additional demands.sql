USE spain;

BEGIN TRAN

INSERT INTO dbo.itemstate (active, description, retired, type)
VALUES (1, 'Awaiting completion of additional demands', 0, 'workstatus')

DECLARE @lastItemStateId int;
SELECT @lastItemStateId = MAX(stateid) from dbo.itemstate;

INSERT INTO dbo.stategrouplink (groupid, stateid, type)
VALUES (35, @lastItemStateId, 'ALLOCATED_SUBDIV')

INSERT INTO dbo.itemstatetranslation (stateid, locale, translation)
VALUES
	(@lastItemStateId, 'en_GB', 'Awaiting completion of additional demands'),
	(@lastItemStateId, 'es_ES', 'Awaiting completion of additional demands'),
	(@lastItemStateId, 'fr_FR', 'Awaiting completion of additional demands'),
	(@lastItemStateId, 'de_DE', 'Awaiting completion of additional demands'),
	(@lastItemStateId, 'nl_NL', 'Awaiting completion of additional demands')

INSERT INTO dbo.lookupresult (lookupid, outcomestatusid, resultmessage)
VALUES (67, 211, 0)

INSERT INTO dbo.lookupresult (lookupid, outcomestatusid, resultmessage)
VALUES (67, 246, 1)

UPDATE dbo.outcomestatus
SET activityid = null, defaultoutcome = 0, lookupid = null, statusid = @lastItemStateId
WHERE id = 211

UPDATE dbo.lookupinstance
SET lookup = 'Lookup_Additional_demands_completed', lookupfunction = 46
WHERE id = 67

DELETE FROM dbo.lookupresult
WHERE id = 235

DELETE FROM dbo.lookupresult
WHERE id = 234

INSERT INTO dbo.itemstate (active, description, retired, actionoutcometype, lookupid, type)
VALUES (1, 'Additional demands recorded', 0, null, 67, 'workactivity')

DECLARE @lastActivityId int;
SELECT @lastActivityId = MAX(stateid) from dbo.itemstate;

INSERT INTO dbo.itemstatetranslation (stateid, locale, translation)
VALUES
	(@lastActivityId, 'en_GB', 'Additional demands recorded'),
	(@lastActivityId, 'es_ES', 'Additional demands recorded'),
	(@lastActivityId, 'fr_FR', 'Additional demands recorded'),
	(@lastActivityId, 'de_DE', 'Additional demands recorded'),
	(@lastActivityId, 'nl_NL', 'Additional demands recorded')

INSERT INTO dbo.hook (active, activityid, alwayspossible, beginactivity, completeactivity, name)
VALUES (1, @lastActivityId, 0, 1, 1, 'record-additional-demands')

DECLARE @lastHookId int;
SELECT @lastHookId = MAX(id) from dbo.hook;

INSERT INTO dbo.hookactivity (activityid, beginactivity, completeactivity, hookid, stateid)
VALUES (@lastActivityId, 1, 1, @lastHookId, @lastItemStateId)

INSERT INTO dbversion (version) VALUES (441);

COMMIT TRAN