USE [atlas]
GO

BEGIN TRAN

/*Inserting new group for new setting for creating instrument without Inst_Client_ID (plantno) */

Insert into dbo.systemdefaultgroup (description,dname) values('Instrument creation settings','Business - Instrument creation');

/*Inserting new group name translation done */

insert into dbo.systemdefaultgroupnametranslation (id,locale,translation) values 
((select id from systemdefaultgroup where dname='Business - Instrument creation') ,'en_GB','Business - Instrument creation');

insert into dbo.systemdefaultgroupnametranslation (id,locale,translation) values 
((select id from systemdefaultgroup where dname='Business - Instrument creation') ,'fr_FR','Business - Création d instruments');

insert into dbo.systemdefaultgroupnametranslation (id,locale,translation) values 
((select id from systemdefaultgroup where dname='Business - Instrument creation') ,'es_ES','Negocios - Creación de instrumentos');

insert into dbo.systemdefaultgroupnametranslation (id,locale,translation) values 
((select id from systemdefaultgroup where dname='Business - Instrument creation') ,'de_DE','Business - Instrumentenerstellung');

/*Inserting new group description translation */

insert into dbo.systemdefaultgroupdescriptiontranslation (id,locale,translation) values 
((select id from systemdefaultgroup where dname='Business - Instrument creation') ,'en_GB','Instrument creation settings');

insert into dbo.systemdefaultgroupdescriptiontranslation (id,locale,translation) values 
((select id from systemdefaultgroup where dname='Business - Instrument creation') ,'fr_FR','Paramètres de création d instrument');

insert into dbo.systemdefaultgroupdescriptiontranslation (id,locale,translation) values 
((select id from systemdefaultgroup where dname='Business - Instrument creation') ,'es_ES','Configuración de creación de instrumentos');

insert into dbo.systemdefaultgroupdescriptiontranslation (id,locale,translation) values 
((select id from systemdefaultgroup where dname='Business - Instrument creation') ,'de_DE','Einstellungen zur Instrumentenerstellung');

/* inserting the new system default setting - SystemDefaultNames is key - ordinal based - 52 by definition */

SET IDENTITY_INSERT [systemdefault] ON

Insert into dbo.systemdefault (defaultid,defaultdatatype,defaultdescription,defaultname,defaultvalue,endd,start,groupid,groupwide) 
values(52,'boolean','Defines if a business company can create an instrument without Inst_Client_ID (plantno)','Create Instrument without plant No',
'true',0,0,(select id from systemdefaultgroup where dname='Business - Instrument creation'),1);

SET IDENTITY_INSERT [systemdefault] OFF

/* inserting default description translation of the new system default settings */

insert into dbo.systemdefaultdescriptiontranslation (defaultid,locale,translation) values 
(52,'en_GB','Defines if a business company can create an instrument without Inst_Client_ID (plantno)');

insert into dbo.systemdefaultdescriptiontranslation (defaultid,locale,translation) values 
(52,'fr_FR','Définit si une entreprise peut créer un instrument sans Inst_Client_ID (plantno)');

insert into dbo.systemdefaultdescriptiontranslation (defaultid,locale,translation) values 
(52,'es_ES','Define si una empresa comercial puede crear un instrumento sin Inst_Client_ID (plantno)');

insert into dbo.systemdefaultdescriptiontranslation (defaultid,locale,translation) values 
(52,'de_DE','Legt fest, ob eine Firma ein Instrument ohne Inst_Client_ID (plantno) anlegen kann');

/* inserting default name translation  of the new system default settings */

insert into dbo.systemdefaultnametranslation (defaultid,locale,translation) values 
(52,'en_GB','Create Instrument without plant No');

insert into dbo.systemdefaultnametranslation (defaultid,locale,translation) values 
(52,'fr_FR','Créer un instrument sans plant No');

insert into dbo.systemdefaultnametranslation (defaultid,locale,translation) values 
(52,'es_ES','Crear instrumento sin plant No');

insert into dbo.systemdefaultnametranslation (defaultid,locale,translation) values 
(52,'de_DE','Instrument ohne Werksnummer anlegen');

/* inserting system default scope */

insert into dbo.systemdefaultscope (scope,defaultid) values(0,52);

/* inserting system default corole && Setting it to business companies only */ 

insert into dbo.systemdefaultcorole (coroleid,defaultid) values((select coroleid from corole where corole='Business'),52);

/* set the setting to create an instrument to false for Trescal SAS as mentioned in the ticket DEV-1032 */

insert into dbo.systemdefaultapplication (defaultvalue,coid,defaultid,lastModified,orgid,lastModifiedBy) 
	values('false',(select coid from company where coname='Trescal SAS'),
	52,
	(select GETDATE()),(select coid from company where coname='Trescal SAS'),(select personid from contact where email='soufiane.amankachi@trescal.com'))

insert into dbversion(version) values (512)

COMMIT TRAN