-- Author Galen Beck 2017-05-29
-- Creates new SystemDefault entity (46) for cal verification status and performs related configuration

BEGIN TRAN 

USE [spain];
GO

SET IDENTITY_INSERT [systemdefault] ON
GO

INSERT INTO [systemdefault]
           ([defaultid],[defaultdatatype],[defaultdescription],[defaultname],[defaultvalue],[endd],[start],[groupid],[groupwide])
     VALUES
           (46
		   ,'boolean'
           ,'Defines if certificates for this client require calibration verification'
           ,'Calibration Verification'
           ,'false',0,0,9,1);
GO

SET IDENTITY_INSERT [systemdefault] OFF
GO

INSERT INTO [systemdefaultdescriptiontranslation]
   ([defaultid], [locale], [translation])
   VALUES
   (46, 'en_GB', 'Defines if certificates for this client require calibration verification'),
   (46, 'en_ES', 'Define si los certificados para este cliente requieren verificación de calibración'),
   (46, 'fr_FR', 'Définit si les certificats pour ce client nécessitent une vérification d''étalonnage');

 -- Enables system default to be configurable at only company level
 
 INSERT INTO [dbo].[systemdefaultscope]
           ([scope],[defaultid])
     VALUES
           (0,46);
GO

-- Enables system defaults (new and old) for specific company roles - Client and Business Company

INSERT INTO [dbo].[systemdefaultcorole] ([coroleid],[defaultid]) VALUES
 (1,46),
 (5,46);
 
-- Add translations

INSERT INTO [dbo].[systemdefaultnametranslation]
           ([defaultid],[locale],[translation])
     VALUES
           (46,'en_GB','Calibration Verification'),
           (46,'es_ES','Verificación del CAR del cliente'),
           (46,'fr_FR','Vérification de l''étalonnage');
GO

-- Change to COMMIT when successfully tested

COMMIT TRAN