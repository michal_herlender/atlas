USE [spain];

DROP INDEX IDX_defaultquotationgeneralcondition_lastupdateby ON [defaultquotationgeneralcondition];
ALTER TABLE [defaultquotationgeneralcondition] DROP CONSTRAINT FK232BEB9E70BBEFCA;
ALTER TABLE [defaultquotationgeneralcondition] DROP COLUMN lastupdateon;
ALTER TABLE [defaultquotationgeneralcondition] DROP COLUMN lastupdateby;
UPDATE [defaultquotationgeneralcondition] SET [orgid] = 6579  WHERE [orgid] IS NULL;

DROP INDEX _dta_index_customquotationgeneralcondition_7_763149764__K5_K1_2_3_4 ON [customquotationgeneralcondition];
DROP INDEX IDX_customquotationgeneralcondition_lastupdateby ON [customquotationgeneralcondition];
ALTER TABLE [customquotationgeneralcondition] DROP CONSTRAINT FK666E6C6E70BBEFCA;
ALTER TABLE [customquotationgeneralcondition] DROP COLUMN lastupdateon;
ALTER TABLE [customquotationgeneralcondition] DROP COLUMN lastupdateby;