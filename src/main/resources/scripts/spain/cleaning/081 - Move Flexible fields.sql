IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME IN ('instrumentfieldvaluenumeric','instrumentfieldvalueselection','instrumentfieldvaluedatetime','instrumentfieldvaluestring','instrumentfieldvalueboolean'))
BEGIN
	DROP TABLE dbo.instrumentfieldvaluestring
	DROP TABLE dbo.instrumentfieldvalueboolean
	DROP TABLE dbo.instrumentfieldvaluedatetime
	DROP TABLE dbo.instrumentfieldvaluenumeric
	DROP TABLE dbo.instrumentfieldvalueselection
END

BEGIN TRAN

SET IDENTITY_INSERT dbo.instrumentvalue ON
		
INSERT INTO dbo.instrumentvalue (instrumentFieldValueid,plantid,instrumentFieldDefinitionid)
SELECT instrumentFieldValueid,plantid,dbo.instrumentfielddefinition.instrumentFieldDefinitionid
FROM dbo.instrumentfieldvalue
INNER JOIN dbo.instrumentfielddefinition
ON dbo.instrumentfielddefinition.instrumentFieldDefinitionid = dbo.instrumentfieldvalue.instrumentFieldDefinitionid
WHERE dbo.instrumentfielddefinition.fieldtype = 'BIT'
		
SET IDENTITY_INSERT dbo.instrumentvalue OFF
-- Changed valueBoolean to value on line 23 below - GB 2016-11-06		
INSERT INTO dbo.instrumentvalueboolean (instrumentFieldValueid,value)
SELECT dbo.instrumentfieldvalue.instrumentFieldValueid,CAST(dbo.instrumentfieldvalue.value AS BIT)
FROM dbo.instrumentfieldvalue
INNER JOIN dbo.instrumentfielddefinition
ON dbo.instrumentfielddefinition.instrumentFieldDefinitionid = dbo.instrumentfieldvalue.instrumentFieldDefinitionid
WHERE dbo.instrumentfielddefinition.fieldtype = 'BIT'

INSERT INTO dbo.instrumentcomplementaryfield (plantid,formerBarCode,formerFamily,formerCharacteristics,formerLocalDescription,formerManufacturer,formerModel,formerName)
SELECT plantid,formerBarCode,FormerFamily.Val,FormerCharacteristics.Val,formerLocalDescription,FormerManufacturer.Val,FormerModel.Val,formerName
FROM dbo.instrument
OUTER APPLY (
	SELECT IFV.value Val
	FROM dbo.instrumentfieldvalue IFV
	INNER JOIN dbo.instrumentfielddefinition IFD
	ON IFD.instrumentFieldDefinitionid = IFV.instrumentFieldDefinitionid
	WHERE IFD.name = 'Former Family'
	AND IFV.plantid = dbo.instrument.plantid
) FormerFamily
OUTER APPLY (
	SELECT IFV.value Val
	FROM dbo.instrumentfieldvalue IFV
	INNER JOIN dbo.instrumentfielddefinition IFD
	ON IFD.instrumentFieldDefinitionid = IFV.instrumentFieldDefinitionid
	WHERE IFD.name = 'Former Characteristics'
	AND IFV.plantid = dbo.instrument.plantid
) FormerCharacteristics
OUTER APPLY (
	SELECT IFV.value Val
	FROM dbo.instrumentfieldvalue IFV
	INNER JOIN dbo.instrumentfielddefinition IFD
	ON IFD.instrumentFieldDefinitionid = IFV.instrumentFieldDefinitionid
	WHERE IFD.name = 'Former Manufacturer'
	AND IFV.plantid = dbo.instrument.plantid
) FormerManufacturer
OUTER APPLY (
	SELECT IFV.value Val
	FROM dbo.instrumentfieldvalue IFV
	INNER JOIN dbo.instrumentfielddefinition IFD
	ON IFD.instrumentFieldDefinitionid = IFV.instrumentFieldDefinitionid
	WHERE IFD.name = 'Former Model'
	AND IFV.plantid = dbo.instrument.plantid
) FormerModel
WHERE dbo.instrument.plantid NOT IN (SELECT dbo.instrumentcomplementaryfield.plantid FROM dbo.instrumentcomplementaryfield)

COMMIT TRAN