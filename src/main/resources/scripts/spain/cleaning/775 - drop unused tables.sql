USE [atlas]

begin TRAN

drop table importmodelitem;
drop table importmodel;
GO

insert into dbversion(version) values(775);

commit TRAN
