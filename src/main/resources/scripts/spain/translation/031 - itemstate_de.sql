USE spain;
GO

BEGIN TRAN

	INSERT INTO itemstatetranslation(stateid, locale, translation)
	SELECT ist.stateid, 'de_DE', ist.translation
	FROM itemstatetranslation ist
	WHERE locale = 'en_GB'

COMMIT TRAN