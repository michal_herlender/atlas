USE spain;
GO

BEGIN TRAN

	INSERT INTO servicetypeshortnametranslation(servicetypeid, locale, translation)
	SELECT stsnt.servicetypeid, 'de_DE', stsnt.translation
	FROM servicetypeshortnametranslation AS stsnt
	WHERE locale='en_GB'

	INSERT INTO servicetypelongnametranslation(servicetypeid, locale, translation)
	VALUES
		(1, 'de_DE', 'Interne akkreditierte Kalibrierung'),
		(2, 'de_DE', 'Interne Werkskalibrierung'),
		(3, 'de_DE', 'Anderes'),
		(4, 'de_DE', 'Akkreditierte Vor-Ort-Kalibrierung'),
		(5, 'de_DE', 'Vor-Ort-Werkskalibrierung'),
		(6, 'de_DE', 'Reparatur'),
		(7, 'de_DE', 'Externe akkreditierte Kalibrierung'),
		(8, 'de_DE', 'Externe Werksalibrierung'),
		(15, 'de_DE', 'Interne akkreditierte Kalibrierung ohne Entscheid'),
		(16, 'de_DE', 'Interne Werkskalibrierung ohne Entscheid'),
		(17, 'de_DE', 'Akkreditierte Vor-Ort-Kalibrierung ohne Entscheid'),
		(18, 'de_DE', 'Vor-Ort-Werkskalibrierung ohne Entscheid'),
		(19, 'de_DE', 'Externe akkreditierte Kalibrierung ohne Entscheid'),
		(20, 'de_DE', 'Externe Werkskalibrierung ohne Entscheid'),
		(21, 'de_DE', 'Gruppeninterne Werkskalibrierung'),
		(22, 'de_DE', 'Gruppeninterne akkreditierte Kalibrierung'),
		(23, 'de_DE', 'Gruppeninterne akkreditierte Kalibrierung ohne Entscheid'),
		(24, 'de_DE', 'Gruppeninterne Werkskalibrierung ohne Entscheid'),
		(25, 'de_DE', 'Gruppeninterne akkreditierte Vor-Ort-Kalibrierung'),
		(26, 'de_DE', 'Gruppeninterne Vor-Ort-Werkskalibrierung'),
		(27, 'de_DE', 'Wartung'),
		(28, 'de_DE', 'Hauseigene Kalibrierung'),
		(29, 'de_DE', 'Kalibrierung durch Kunden'),
		(30, 'de_DE', 'Externe Kalibrierung durch Kunden'),
		(31, 'de_DE', 'Service')

COMMIT TRAN