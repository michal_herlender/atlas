USE [Spain];
GO

BEGIN TRAN

UPDATE [servicetype]
SET longname = 'In-House Accredited Calibration', shortname = 'AC-IH' WHERE servicetypeid = 1;
UPDATE [servicetype]
SET longname = 'In-House Standard Calibration', shortname = 'ST-IH' WHERE servicetypeid = 2;
UPDATE [servicetype]
SET longname = 'Other', shortname = 'OTHER' WHERE servicetypeid = 3;
UPDATE [servicetype]
SET longname = 'On-Site Accredited Calibration', shortname = 'AC-OS' WHERE servicetypeid = 4;
UPDATE [servicetype]
SET longname = 'On-Site Standard Calibration', shortname = 'ST-OS' WHERE servicetypeid = 5;
UPDATE [servicetype]
SET longname = 'Repair', shortname = 'REP' WHERE servicetypeid = 6;
GO

DELETE FROM [servicetypelongnametranslation];
GO

INSERT INTO [servicetypelongnametranslation] (servicetypeid, locale, translation)
VALUES
(1, 'en_GB', 'In-House Accredited Calibration'),
(1, 'es_ES', 'Calibración acreditada en laboratorio'),
(2, 'en_GB', 'In-House Standard Calibration'),
(2, 'es_ES', 'Calibración trazable en laboratorio'),
(3, 'en_GB', 'Other'),
(3, 'es_ES', 'Otros'),
(4, 'en_GB', 'On-Site Accredited Calibration'),
(4, 'es_ES', 'Calibración acreditada in situ'),
(5, 'en_GB', 'On-Site Standard Calibration'),
(5, 'es_ES', 'Calibración trazable in situ'),
(6, 'en_GB', 'Repair'),
(6, 'es_ES', 'Reparar');

IF OBJECT_ID('serviceypeshortnametranslation', 'U') IS NOT NULL
DROP TABLE [serviceypeshortnametranslation];
GO

DELETE FROM [servicetypeshortnametranslation];
GO

INSERT INTO [servicetypeshortnametranslation] (servicetypeid, locale, translation)
VALUES
(1, 'en_GB', 'AC-IH'),
(1, 'es_ES', 'AC-EL'),
(2, 'en_GB', 'ST-IH'),
(2, 'es_ES', 'ST-EL'),
(3, 'en_GB', 'OTHER'),
(3, 'es_ES', 'OTROS'),
(4, 'en_GB', 'AC-OS'),
(4, 'es_ES', 'AC-IS'),
(5, 'en_GB', 'ST-OS'),
(5, 'es_ES', 'ST-IS'),
(6, 'en_GB', 'REP'),
(6, 'es_ES', 'REP');

ROLLBACK TRAN