USE spain;
GO

BEGIN TRAN

	INSERT INTO departmenttypetranslation(id, locale, translation)
	VALUES
		(1, 'de_DE', 'Administration'),
		(2, 'de_DE', 'Rechnungswesen'),
		(3, 'de_DE', 'Labor'),
		(4, 'de_DE', 'Versand'),
		(5, 'de_DE', 'Vor-Ort'),
		(6, 'de_DE', 'Fremddienstleister')

COMMIT TRAN
