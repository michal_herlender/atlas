USE spain;
GO

BEGIN TRAN

	INSERT INTO transportmethodtranslation(id, locale, translation)
	VALUES
		(1, 'de_DE', 'Shuttle'),
		(2, 'de_DE', 'Kunde'),
		(3, 'de_DE', 'Courier'),
		(4, 'de_DE', 'Lokal'),
		(5, 'de_DE', 'Courier Express'),
		(6, 'de_DE', 'Courier - kundenspezifisch')

COMMIT TRAN