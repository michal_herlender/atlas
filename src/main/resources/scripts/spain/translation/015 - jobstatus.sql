BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.jobstatusnametranslation WHERE statusid = 1 AND locale = 'es_ES') INSERT INTO dbo.jobstatusnametranslation (statusid, locale, translation) VALUES (1, 'es_ES', 'En curso') ELSE UPDATE dbo.jobstatusnametranslation SET translation = 'En curso' WHERE statusid = 1 AND locale = 'dbo.jobstatusnametranslation';
IF NOT EXISTS (SELECT 1 FROM dbo.jobstatusnametranslation WHERE statusid = 2 AND locale = 'es_ES') INSERT INTO dbo.jobstatusnametranslation (statusid, locale, translation) VALUES (2, 'es_ES', 'Esperando factura') ELSE UPDATE dbo.jobstatusnametranslation SET translation = 'Esperando factura' WHERE statusid = 2 AND locale = 'dbo.jobstatusnametranslation';
IF NOT EXISTS (SELECT 1 FROM dbo.jobstatusnametranslation WHERE statusid = 3 AND locale = 'es_ES') INSERT INTO dbo.jobstatusnametranslation (statusid, locale, translation) VALUES (3, 'es_ES', 'Completo') ELSE UPDATE dbo.jobstatusnametranslation SET translation = 'Completo' WHERE statusid = 3 AND locale = 'dbo.jobstatusnametranslation';

COMMIT TRAN