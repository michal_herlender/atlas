@echo OFF

REM Define variables
set SQLFILE=DataTranslation-SPAIN.sql
set LOGFILE=DataTranslation-SPAIN.log
set SERVER=(local)\SQLEXPRESS
set DATABASE=spain
set TRUSTEDCNX=Y
set USER=sa
set PWD=Wobbegong13

echo.
echo 1. Set Variables
echo ************************
echo SQLFile: %SQLFILE%
echo LOGFile: %LOGFILE%
echo SQLServer: %SERVER%
echo Database: %DATABASE%
if %TRUSTEDCNX%==Y (
	echo Login: Use trusted connection
) else (
	echo Login: User=%USER% Pwd=%PWD%
)
PAUSE

echo.
echo 2. Generation Script SQL
echo ************************
echo In Progress...
if exist %SQLFile% del /F /Q %SQLFile%
echo PRINT "%DATE% %TIME% %SQLFile%" > %SQLFile%
echo. >> %SQLFile%
for /F "tokens=*" %%f in ('dir /b /on ^"0*.sql^"') do (
	echo PRINT "%%f" >> %SQLFile%
	echo :r ".\%%f" >> %SQLFile%
	echo GO >> %SQLFile%
	echo. >> %SQLFile%
	)
echo. >> %SQLFile%
echo PRINT "%DATE% %TIME% %SQLFile%" >> %SQLFile%
if exist %SQLFile% (
	echo Result: OK
) else (
	echo Result: Error - File doesn't exist!
	PAUSE
)

echo.
echo 3. Execution Script SQL
echo ***********************
echo In Progress...
if exist %LOGFile% del /F /Q %LOGFile%
if %TRUSTEDCNX%==Y sqlcmd -S %SERVER% -d %DATABASE% -E -i "%SQLFILE%" -o "%LOGFILE%"
if %TRUSTEDCNX%==N sqlcmd -S %SERVER% -d %DATABASE% -U %USER% -P %PWD% -i "%SQLFILE%" -o "%LOGFILE%"
FINDSTR /C:"Message" %LOGFILE%
if %errorlevel%==1 (
	echo Result: OK
) else (
	echo Result: Error - See above or file %LOGFILE%!
)
PAUSE