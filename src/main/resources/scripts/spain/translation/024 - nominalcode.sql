-- Last Modification: 
--		TProvost 2016-05-19 : Updated tranlsations after return of finance department

USE [Spain];
GO

BEGIN TRAN

DELETE FROM [nominalcodetitletranslation] WHERE locale IN ('es_ES', 'en_GB', 'fr_FR');
GO

-- Default title
UPDATE [dbo].[nominalcode] SET [title] = 'Buildings and fixtures' WHERE [code] = 'PC001'
UPDATE [dbo].[nominalcode] SET [title] = 'Furnitures' WHERE [code] = 'PC002'
UPDATE [dbo].[nominalcode] SET [title] = 'Software - non technical' WHERE [code] = 'PC003'
UPDATE [dbo].[nominalcode] SET [title] = 'Software - technical and production' WHERE [code] = 'PC004'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- ACCELEROMETER' WHERE [code] = 'PC005'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- ACOUSTIC' WHERE [code] = 'PC006'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment -�CHEMISTRY - ENVIRONMENT' WHERE [code] = 'PC007'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- CLIMATIC' WHERE [code] = 'PC008'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- GAS FLOW' WHERE [code] = 'PC009'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�-�LIQUID FLOW' WHERE [code] = 'PC010'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�-�DIMENSIONAL' WHERE [code] = 'PC011'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- MISC' WHERE [code] = 'PC012'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- ELECTRICAL' WHERE [code] = 'PC013'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�-�FORCE - TORQUE' WHERE [code] = 'PC014'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�-�INFORMATIC' WHERE [code] = 'PC015'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�-�MASS - WEIGHT' WHERE [code] = 'PC016'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- MULTIDOMAIN' WHERE [code] = 'PC017'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- PRESSURE' WHERE [code] = 'PC018'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�- RADIOMETRY - PHOTOMETRY' WHERE [code] = 'PC019'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�-�RADIATION IONISATION' WHERE [code] = 'PC020'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�-�TEMPERATURE - HYGROMETRY' WHERE [code] = 'PC021'
UPDATE [dbo].[nominalcode] SET [title] = 'Production equipment�-�VOLUME' WHERE [code] = 'PC022'
UPDATE [dbo].[nominalcode] SET [title] = 'IT equipment' WHERE [code] = 'PC023'
UPDATE [dbo].[nominalcode] SET [title] = 'Logistic equipment' WHERE [code] = 'PC024'
UPDATE [dbo].[nominalcode] SET [title] = 'Vehicle' WHERE [code] = 'PC025'
UPDATE [dbo].[nominalcode] SET [title] = 'Utlities - Water ' WHERE [code] = 'PG001'
UPDATE [dbo].[nominalcode] SET [title] = 'Utilities - Electricity ' WHERE [code] = 'PG002'
UPDATE [dbo].[nominalcode] SET [title] = 'Utilities - Gaz' WHERE [code] = 'PG003'
UPDATE [dbo].[nominalcode] SET [title] = 'Consumables - Genex' WHERE [code] = 'PG004'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchases Softwares and other IT items' WHERE [code] = 'PG005'
UPDATE [dbo].[nominalcode] SET [title] = 'Office supplies' WHERE [code] = 'PG006'
UPDATE [dbo].[nominalcode] SET [title] = 'Photocopier consumables' WHERE [code] = 'PG007'
UPDATE [dbo].[nominalcode] SET [title] = 'Payroll outsourcing' WHERE [code] = 'PG008'
UPDATE [dbo].[nominalcode] SET [title] = 'Security & surveillance services' WHERE [code] = 'PG009'
UPDATE [dbo].[nominalcode] SET [title] = 'Archival depositories rental' WHERE [code] = 'PG010'
UPDATE [dbo].[nominalcode] SET [title] = 'Finance lease payments' WHERE [code] = 'PG011'
UPDATE [dbo].[nominalcode] SET [title] = 'Premises renting costs' WHERE [code] = 'PG012'
UPDATE [dbo].[nominalcode] SET [title] = 'Renting costs - employees housing' WHERE [code] = 'PG013'
UPDATE [dbo].[nominalcode] SET [title] = 'Long-term car rentals' WHERE [code] = 'PG014'
UPDATE [dbo].[nominalcode] SET [title] = 'Short-term car rentals' WHERE [code] = 'PG015'
UPDATE [dbo].[nominalcode] SET [title] = 'Office equipment rentals ' WHERE [code] = 'PG016'
UPDATE [dbo].[nominalcode] SET [title] = 'IT equipment rentals' WHERE [code] = 'PG017'
UPDATE [dbo].[nominalcode] SET [title] = 'Postage equipment rentals' WHERE [code] = 'PG018'
UPDATE [dbo].[nominalcode] SET [title] = 'Photocopier rentals' WHERE [code] = 'PG019'
UPDATE [dbo].[nominalcode] SET [title] = 'Premises rates' WHERE [code] = 'PG020'
UPDATE [dbo].[nominalcode] SET [title] = 'Maintenance of premises' WHERE [code] = 'PG021'
UPDATE [dbo].[nominalcode] SET [title] = 'Vehicles maintenance' WHERE [code] = 'PG022'
UPDATE [dbo].[nominalcode] SET [title] = 'Office and IT equipments maintenance' WHERE [code] = 'PG023'
UPDATE [dbo].[nominalcode] SET [title] = 'IT maintenance (software and hardware)' WHERE [code] = 'PG024'
UPDATE [dbo].[nominalcode] SET [title] = 'Extrnal IT assistance - external consulting' WHERE [code] = 'PG025'
UPDATE [dbo].[nominalcode] SET [title] = 'Waste skips rental' WHERE [code] = 'PG026'
UPDATE [dbo].[nominalcode] SET [title] = 'General documentation ' WHERE [code] = 'PG027'
UPDATE [dbo].[nominalcode] SET [title] = 'Seminars, conferences ' WHERE [code] = 'PG028'
UPDATE [dbo].[nominalcode] SET [title] = 'Temporary Indirect Genex staff costs' WHERE [code] = 'PG029'
UPDATE [dbo].[nominalcode] SET [title] = 'Indirect Genex staff on loan' WHERE [code] = 'PG030'
UPDATE [dbo].[nominalcode] SET [title] = 'Lawyer fees' WHERE [code] = 'PG031'
UPDATE [dbo].[nominalcode] SET [title] = 'Audit fees' WHERE [code] = 'PG032'
UPDATE [dbo].[nominalcode] SET [title] = 'Registration fees' WHERE [code] = 'PG033'
UPDATE [dbo].[nominalcode] SET [title] = 'Other fees' WHERE [code] = 'PG034'
UPDATE [dbo].[nominalcode] SET [title] = 'Advertising expenses' WHERE [code] = 'PG035'
UPDATE [dbo].[nominalcode] SET [title] = 'Shows expenses' WHERE [code] = 'PG036'
UPDATE [dbo].[nominalcode] SET [title] = 'Goodies' WHERE [code] = 'PG037'
UPDATE [dbo].[nominalcode] SET [title] = 'Tips, gifts' WHERE [code] = 'PG038'
UPDATE [dbo].[nominalcode] SET [title] = 'Moving costs' WHERE [code] = 'PG039'
UPDATE [dbo].[nominalcode] SET [title] = 'Fuel costs' WHERE [code] = 'PG040'
UPDATE [dbo].[nominalcode] SET [title] = 'Taxi, parking, tolls' WHERE [code] = 'PG041'
UPDATE [dbo].[nominalcode] SET [title] = 'Ticketing services' WHERE [code] = 'PG042'
UPDATE [dbo].[nominalcode] SET [title] = 'Hotels and restaurants' WHERE [code] = 'PG043'
UPDATE [dbo].[nominalcode] SET [title] = 'Invitations' WHERE [code] = 'PG044'
UPDATE [dbo].[nominalcode] SET [title] = 'Moving costs - Staff' WHERE [code] = 'PG045'
UPDATE [dbo].[nominalcode] SET [title] = 'Postage costs ' WHERE [code] = 'PG046'
UPDATE [dbo].[nominalcode] SET [title] = 'Telecoms costs' WHERE [code] = 'PG047'
UPDATE [dbo].[nominalcode] SET [title] = 'Mobile phones' WHERE [code] = 'PG048'
UPDATE [dbo].[nominalcode] SET [title] = 'IT telecoms - Network / Data' WHERE [code] = 'PG049'
UPDATE [dbo].[nominalcode] SET [title] = 'Bank fees' WHERE [code] = 'PG050'
UPDATE [dbo].[nominalcode] SET [title] = 'Union dues and professional contributions' WHERE [code] = 'PG051'
UPDATE [dbo].[nominalcode] SET [title] = 'Recruitment costs' WHERE [code] = 'PG052'
UPDATE [dbo].[nominalcode] SET [title] = 'Management fees' WHERE [code] = 'PG053'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchase of goods for resale - Procurement NA' WHERE [code] = 'PO001'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchase of goods for resale - Procurement EU' WHERE [code] = 'PO002'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchase of goods for resale - Procurement EX' WHERE [code] = 'PO003'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchases spare parts for job order NA' WHERE [code] = 'PO004'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchases spare parts for job order  EU' WHERE [code] = 'PO005'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchases spare parts for job order  EX' WHERE [code] = 'PO006'
UPDATE [dbo].[nominalcode] SET [title] = 'External subcontracting costs NA' WHERE [code] = 'PO007'
UPDATE [dbo].[nominalcode] SET [title] = 'External subcontracting costs EU' WHERE [code] = 'PO008'
UPDATE [dbo].[nominalcode] SET [title] = 'External subcontracting costs EX' WHERE [code] = 'PO009'
UPDATE [dbo].[nominalcode] SET [title] = 'Internal subcontracting costs NA' WHERE [code] = 'PO010'
UPDATE [dbo].[nominalcode] SET [title] = 'Internal subcontracting costs EU' WHERE [code] = 'PO011'
UPDATE [dbo].[nominalcode] SET [title] = 'Internal subcontracting costs EX' WHERE [code] = 'PO012'
UPDATE [dbo].[nominalcode] SET [title] = 'Packaging costs' WHERE [code] = 'PO013'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchase of services for resale - Procurement NA' WHERE [code] = 'PO014'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchase of services for resale - Procurement EU' WHERE [code] = 'PO015'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchase of services for resale - Procurement EX' WHERE [code] = 'PO016'
UPDATE [dbo].[nominalcode] SET [title] = 'Equipment rentals ' WHERE [code] = 'PO017'
UPDATE [dbo].[nominalcode] SET [title] = 'External subcontracting costs - Passthrough contract' WHERE [code] = 'PO018'
UPDATE [dbo].[nominalcode] SET [title] = 'Utilities - Water ' WHERE [code] = 'PO019'
UPDATE [dbo].[nominalcode] SET [title] = 'Utilities - Electricity ' WHERE [code] = 'PO020'
UPDATE [dbo].[nominalcode] SET [title] = 'Utilities - Gaz' WHERE [code] = 'PO021'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchases spare parts NA' WHERE [code] = 'PO022'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchases spare parts EU' WHERE [code] = 'PO023'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchases consumables NA ' WHERE [code] = 'PO024'
UPDATE [dbo].[nominalcode] SET [title] = 'Purchases consumables EU' WHERE [code] = 'PO025'
UPDATE [dbo].[nominalcode] SET [title] = 'Small items of equipement NA' WHERE [code] = 'PO026'
UPDATE [dbo].[nominalcode] SET [title] = 'Small items of equipement EX' WHERE [code] = 'PO027'
UPDATE [dbo].[nominalcode] SET [title] = 'Small items of equipement EU' WHERE [code] = 'PO028'
UPDATE [dbo].[nominalcode] SET [title] = 'Consumables - Genex' WHERE [code] = 'PO029'
UPDATE [dbo].[nominalcode] SET [title] = 'Working cloth (lab coats, lab jackets, shoes, etc�) ' WHERE [code] = 'PO030'
UPDATE [dbo].[nominalcode] SET [title] = 'Office supplies' WHERE [code] = 'PO031'
UPDATE [dbo].[nominalcode] SET [title] = 'Photocopier consumables' WHERE [code] = 'PO032'
UPDATE [dbo].[nominalcode] SET [title] = 'Security & surveillance services' WHERE [code] = 'PO033'
UPDATE [dbo].[nominalcode] SET [title] = 'Archival depositories rental' WHERE [code] = 'PO034'
UPDATE [dbo].[nominalcode] SET [title] = 'Premises renting costs' WHERE [code] = 'PO035'
UPDATE [dbo].[nominalcode] SET [title] = 'Renting costs - employees housing' WHERE [code] = 'PO036'
UPDATE [dbo].[nominalcode] SET [title] = 'Internal Standards rentals ' WHERE [code] = 'PO037'
UPDATE [dbo].[nominalcode] SET [title] = 'Long-term car rentals' WHERE [code] = 'PO038'
UPDATE [dbo].[nominalcode] SET [title] = 'Short-term car rentals' WHERE [code] = 'PO039'
UPDATE [dbo].[nominalcode] SET [title] = 'Office equipment rentals ' WHERE [code] = 'PO040'
UPDATE [dbo].[nominalcode] SET [title] = 'Photocopier rentals' WHERE [code] = 'PO041'
UPDATE [dbo].[nominalcode] SET [title] = 'Premises rates' WHERE [code] = 'PO042'
UPDATE [dbo].[nominalcode] SET [title] = 'Maintenance of premises' WHERE [code] = 'PO043'
UPDATE [dbo].[nominalcode] SET [title] = 'Own standards periodic calibration costs - NA' WHERE [code] = 'PO044'
UPDATE [dbo].[nominalcode] SET [title] = 'Own standards repair costs - NA' WHERE [code] = 'PO045'
UPDATE [dbo].[nominalcode] SET [title] = 'Own standards periodic calibration costs - EU' WHERE [code] = 'PO046'
UPDATE [dbo].[nominalcode] SET [title] = 'Own standards repair costs - EU' WHERE [code] = 'PO047'
UPDATE [dbo].[nominalcode] SET [title] = 'Vehicles maintenance' WHERE [code] = 'PO048'
UPDATE [dbo].[nominalcode] SET [title] = 'Office and IT equipments maintenance' WHERE [code] = 'PO049'
UPDATE [dbo].[nominalcode] SET [title] = 'Laundry services' WHERE [code] = 'PO050'
UPDATE [dbo].[nominalcode] SET [title] = 'Waste skips rental' WHERE [code] = 'PO051'
UPDATE [dbo].[nominalcode] SET [title] = 'Technical documentation' WHERE [code] = 'PO052'
UPDATE [dbo].[nominalcode] SET [title] = 'General documentation ' WHERE [code] = 'PO053'
UPDATE [dbo].[nominalcode] SET [title] = 'Seminars, conferences ' WHERE [code] = 'PO054'
UPDATE [dbo].[nominalcode] SET [title] = 'Temporary Direct staff costs' WHERE [code] = 'PO055'
UPDATE [dbo].[nominalcode] SET [title] = 'Temporary Indirect Opex staff costs' WHERE [code] = 'PO056'
UPDATE [dbo].[nominalcode] SET [title] = 'Direct staff on loan' WHERE [code] = 'PO057'
UPDATE [dbo].[nominalcode] SET [title] = 'Indirect Opex staff on loan' WHERE [code] = 'PO058'
UPDATE [dbo].[nominalcode] SET [title] = 'Other fees' WHERE [code] = 'PO059'
UPDATE [dbo].[nominalcode] SET [title] = 'Cofrac Audit fees' WHERE [code] = 'PO060'
UPDATE [dbo].[nominalcode] SET [title] = 'Freight and carriage costs on purchase NA & EX' WHERE [code] = 'PO061'
UPDATE [dbo].[nominalcode] SET [title] = 'Freight and carriage costs on purchase  EU' WHERE [code] = 'PO062'
UPDATE [dbo].[nominalcode] SET [title] = 'Freight and carriage costs on sale NA' WHERE [code] = 'PO063'
UPDATE [dbo].[nominalcode] SET [title] = 'Freight and carriage costs interbranch NA' WHERE [code] = 'PO064'
UPDATE [dbo].[nominalcode] SET [title] = 'Freight and carriage costs on sale UE' WHERE [code] = 'PO065'
UPDATE [dbo].[nominalcode] SET [title] = 'Freight and carriage costs on sale inter company UE' WHERE [code] = 'PO066'
UPDATE [dbo].[nominalcode] SET [title] = 'Freight and carriage costs on sale intercompany EX' WHERE [code] = 'PO067'
UPDATE [dbo].[nominalcode] SET [title] = 'Freight and carriage costs on sale EX' WHERE [code] = 'PO068'
UPDATE [dbo].[nominalcode] SET [title] = 'Moving costs' WHERE [code] = 'PO069'
UPDATE [dbo].[nominalcode] SET [title] = 'Fuel costs' WHERE [code] = 'PO070'
UPDATE [dbo].[nominalcode] SET [title] = 'Taxi, parking, tolls' WHERE [code] = 'PO071'
UPDATE [dbo].[nominalcode] SET [title] = 'Ticketing services' WHERE [code] = 'PO072'
UPDATE [dbo].[nominalcode] SET [title] = 'Hotels and restaurants' WHERE [code] = 'PO073'
UPDATE [dbo].[nominalcode] SET [title] = 'Invitations' WHERE [code] = 'PO074'
UPDATE [dbo].[nominalcode] SET [title] = 'Moving costs - Staff' WHERE [code] = 'PO075'
UPDATE [dbo].[nominalcode] SET [title] = 'Postage costs ' WHERE [code] = 'PO076'
UPDATE [dbo].[nominalcode] SET [title] = 'Telecoms costs' WHERE [code] = 'PO077'
UPDATE [dbo].[nominalcode] SET [title] = 'Mobile phones' WHERE [code] = 'PO078'
UPDATE [dbo].[nominalcode] SET [title] = 'Union dues and professional contributions' WHERE [code] = 'PO079'
UPDATE [dbo].[nominalcode] SET [title] = 'Recruitment costs' WHERE [code] = 'PO080'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales calibration' WHERE [code] = 'S001'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales adjustment' WHERE [code] = 'S002'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales repair' WHERE [code] = 'S003'
UPDATE [dbo].[nominalcode] SET [title] = 'Other sales' WHERE [code] = 'S004'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales spare sparts' WHERE [code] = 'S005'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales carriage' WHERE [code] = 'S006'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales IT' WHERE [code] = 'S007'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales expertise/audit/training/park management' WHERE [code] = 'S008'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales reseller' WHERE [code] = 'S009'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales renting' WHERE [code] = 'S010'
UPDATE [dbo].[nominalcode] SET [title] = 'Sales outsource' WHERE [code] = 'S011'
GO

-- English translations
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Buildings and fixtures' FROM [dbo].[nominalcode] WHERE [code] = 'PC001'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Furnitures' FROM [dbo].[nominalcode] WHERE [code] = 'PC002'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Software - non technical' FROM [dbo].[nominalcode] WHERE [code] = 'PC003'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Software - technical and production' FROM [dbo].[nominalcode] WHERE [code] = 'PC004'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- ACCELEROMETER' FROM [dbo].[nominalcode] WHERE [code] = 'PC005'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- ACOUSTIC' FROM [dbo].[nominalcode] WHERE [code] = 'PC006'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment -�CHEMISTRY - ENVIRONMENT' FROM [dbo].[nominalcode] WHERE [code] = 'PC007'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- CLIMATIC' FROM [dbo].[nominalcode] WHERE [code] = 'PC008'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- GAS FLOW' FROM [dbo].[nominalcode] WHERE [code] = 'PC009'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�-�LIQUID FLOW' FROM [dbo].[nominalcode] WHERE [code] = 'PC010'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�-�DIMENSIONAL' FROM [dbo].[nominalcode] WHERE [code] = 'PC011'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- MISC' FROM [dbo].[nominalcode] WHERE [code] = 'PC012'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- ELECTRICAL' FROM [dbo].[nominalcode] WHERE [code] = 'PC013'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�-�FORCE - TORQUE' FROM [dbo].[nominalcode] WHERE [code] = 'PC014'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�-�INFORMATIC' FROM [dbo].[nominalcode] WHERE [code] = 'PC015'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�-�MASS - WEIGHT' FROM [dbo].[nominalcode] WHERE [code] = 'PC016'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- MULTIDOMAIN' FROM [dbo].[nominalcode] WHERE [code] = 'PC017'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- PRESSURE' FROM [dbo].[nominalcode] WHERE [code] = 'PC018'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�- RADIOMETRY - PHOTOMETRY' FROM [dbo].[nominalcode] WHERE [code] = 'PC019'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�-�RADIATION IONISATION' FROM [dbo].[nominalcode] WHERE [code] = 'PC020'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�-�TEMPERATURE - HYGROMETRY' FROM [dbo].[nominalcode] WHERE [code] = 'PC021'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Production equipment�-�VOLUME' FROM [dbo].[nominalcode] WHERE [code] = 'PC022'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'IT equipment' FROM [dbo].[nominalcode] WHERE [code] = 'PC023'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Logistic equipment' FROM [dbo].[nominalcode] WHERE [code] = 'PC024'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Vehicle' FROM [dbo].[nominalcode] WHERE [code] = 'PC025'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Utlities - Water ' FROM [dbo].[nominalcode] WHERE [code] = 'PG001'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Utilities - Electricity ' FROM [dbo].[nominalcode] WHERE [code] = 'PG002'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Utilities - Gaz' FROM [dbo].[nominalcode] WHERE [code] = 'PG003'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Consumables - Genex' FROM [dbo].[nominalcode] WHERE [code] = 'PG004'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchases Softwares and other IT items' FROM [dbo].[nominalcode] WHERE [code] = 'PG005'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Office supplies' FROM [dbo].[nominalcode] WHERE [code] = 'PG006'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Photocopier consumables' FROM [dbo].[nominalcode] WHERE [code] = 'PG007'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Payroll outsourcing' FROM [dbo].[nominalcode] WHERE [code] = 'PG008'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Security & surveillance services' FROM [dbo].[nominalcode] WHERE [code] = 'PG009'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Archival depositories rental' FROM [dbo].[nominalcode] WHERE [code] = 'PG010'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Finance lease payments' FROM [dbo].[nominalcode] WHERE [code] = 'PG011'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Premises renting costs' FROM [dbo].[nominalcode] WHERE [code] = 'PG012'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Renting costs - employees housing' FROM [dbo].[nominalcode] WHERE [code] = 'PG013'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Long-term car rentals' FROM [dbo].[nominalcode] WHERE [code] = 'PG014'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Short-term car rentals' FROM [dbo].[nominalcode] WHERE [code] = 'PG015'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Office equipment rentals ' FROM [dbo].[nominalcode] WHERE [code] = 'PG016'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'IT equipment rentals' FROM [dbo].[nominalcode] WHERE [code] = 'PG017'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Postage equipment rentals' FROM [dbo].[nominalcode] WHERE [code] = 'PG018'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Photocopier rentals' FROM [dbo].[nominalcode] WHERE [code] = 'PG019'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Premises rates' FROM [dbo].[nominalcode] WHERE [code] = 'PG020'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Maintenance of premises' FROM [dbo].[nominalcode] WHERE [code] = 'PG021'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Vehicles maintenance' FROM [dbo].[nominalcode] WHERE [code] = 'PG022'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Office and IT equipments maintenance' FROM [dbo].[nominalcode] WHERE [code] = 'PG023'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'IT maintenance (software and hardware)' FROM [dbo].[nominalcode] WHERE [code] = 'PG024'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Extrnal IT assistance - external consulting' FROM [dbo].[nominalcode] WHERE [code] = 'PG025'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Waste skips rental' FROM [dbo].[nominalcode] WHERE [code] = 'PG026'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'General documentation ' FROM [dbo].[nominalcode] WHERE [code] = 'PG027'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Seminars, conferences ' FROM [dbo].[nominalcode] WHERE [code] = 'PG028'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Temporary Indirect Genex staff costs' FROM [dbo].[nominalcode] WHERE [code] = 'PG029'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Indirect Genex staff on loan' FROM [dbo].[nominalcode] WHERE [code] = 'PG030'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Lawyer fees' FROM [dbo].[nominalcode] WHERE [code] = 'PG031'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Audit fees' FROM [dbo].[nominalcode] WHERE [code] = 'PG032'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Registration fees' FROM [dbo].[nominalcode] WHERE [code] = 'PG033'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Other fees' FROM [dbo].[nominalcode] WHERE [code] = 'PG034'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Advertising expenses' FROM [dbo].[nominalcode] WHERE [code] = 'PG035'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Shows expenses' FROM [dbo].[nominalcode] WHERE [code] = 'PG036'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Goodies' FROM [dbo].[nominalcode] WHERE [code] = 'PG037'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Tips, gifts' FROM [dbo].[nominalcode] WHERE [code] = 'PG038'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Moving costs' FROM [dbo].[nominalcode] WHERE [code] = 'PG039'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Fuel costs' FROM [dbo].[nominalcode] WHERE [code] = 'PG040'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Taxi, parking, tolls' FROM [dbo].[nominalcode] WHERE [code] = 'PG041'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Ticketing services' FROM [dbo].[nominalcode] WHERE [code] = 'PG042'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Hotels and restaurants' FROM [dbo].[nominalcode] WHERE [code] = 'PG043'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Invitations' FROM [dbo].[nominalcode] WHERE [code] = 'PG044'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Moving costs - Staff' FROM [dbo].[nominalcode] WHERE [code] = 'PG045'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Postage costs ' FROM [dbo].[nominalcode] WHERE [code] = 'PG046'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Telecoms costs' FROM [dbo].[nominalcode] WHERE [code] = 'PG047'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Mobile phones' FROM [dbo].[nominalcode] WHERE [code] = 'PG048'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'IT telecoms - Network / Data' FROM [dbo].[nominalcode] WHERE [code] = 'PG049'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Bank fees' FROM [dbo].[nominalcode] WHERE [code] = 'PG050'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Union dues and professional contributions' FROM [dbo].[nominalcode] WHERE [code] = 'PG051'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Recruitment costs' FROM [dbo].[nominalcode] WHERE [code] = 'PG052'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Management fees' FROM [dbo].[nominalcode] WHERE [code] = 'PG053'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchase of goods for resale - Procurement NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO001'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchase of goods for resale - Procurement EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO002'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchase of goods for resale - Procurement EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO003'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchases spare parts for job order NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO004'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchases spare parts for job order  EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO005'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchases spare parts for job order  EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO006'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'External subcontracting costs NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO007'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'External subcontracting costs EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO008'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'External subcontracting costs EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO009'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Internal subcontracting costs NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO010'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Internal subcontracting costs EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO011'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Internal subcontracting costs EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO012'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Packaging costs' FROM [dbo].[nominalcode] WHERE [code] = 'PO013'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchase of services for resale - Procurement NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO014'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchase of services for resale - Procurement EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO015'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchase of services for resale - Procurement EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO016'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Equipment rentals ' FROM [dbo].[nominalcode] WHERE [code] = 'PO017'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'External subcontracting costs - Passthrough contract' FROM [dbo].[nominalcode] WHERE [code] = 'PO018'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Utilities - Water ' FROM [dbo].[nominalcode] WHERE [code] = 'PO019'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Utilities - Electricity ' FROM [dbo].[nominalcode] WHERE [code] = 'PO020'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Utilities - Gaz' FROM [dbo].[nominalcode] WHERE [code] = 'PO021'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchases spare parts NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO022'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchases spare parts EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO023'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchases consumables NA ' FROM [dbo].[nominalcode] WHERE [code] = 'PO024'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Purchases consumables EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO025'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Small items of equipement NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO026'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Small items of equipement EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO027'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Small items of equipement EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO028'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Consumables - Genex' FROM [dbo].[nominalcode] WHERE [code] = 'PO029'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Working cloth (lab coats, lab jackets, shoes, etc�) ' FROM [dbo].[nominalcode] WHERE [code] = 'PO030'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Office supplies' FROM [dbo].[nominalcode] WHERE [code] = 'PO031'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Photocopier consumables' FROM [dbo].[nominalcode] WHERE [code] = 'PO032'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Security & surveillance services' FROM [dbo].[nominalcode] WHERE [code] = 'PO033'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Archival depositories rental' FROM [dbo].[nominalcode] WHERE [code] = 'PO034'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Premises renting costs' FROM [dbo].[nominalcode] WHERE [code] = 'PO035'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Renting costs - employees housing' FROM [dbo].[nominalcode] WHERE [code] = 'PO036'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Internal Standards rentals ' FROM [dbo].[nominalcode] WHERE [code] = 'PO037'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Long-term car rentals' FROM [dbo].[nominalcode] WHERE [code] = 'PO038'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Short-term car rentals' FROM [dbo].[nominalcode] WHERE [code] = 'PO039'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Office equipment rentals ' FROM [dbo].[nominalcode] WHERE [code] = 'PO040'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Photocopier rentals' FROM [dbo].[nominalcode] WHERE [code] = 'PO041'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Premises rates' FROM [dbo].[nominalcode] WHERE [code] = 'PO042'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Maintenance of premises' FROM [dbo].[nominalcode] WHERE [code] = 'PO043'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Own standards periodic calibration costs - NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO044'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Own standards repair costs - NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO045'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Own standards periodic calibration costs - EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO046'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Own standards repair costs - EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO047'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Vehicles maintenance' FROM [dbo].[nominalcode] WHERE [code] = 'PO048'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Office and IT equipments maintenance' FROM [dbo].[nominalcode] WHERE [code] = 'PO049'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Laundry services' FROM [dbo].[nominalcode] WHERE [code] = 'PO050'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Waste skips rental' FROM [dbo].[nominalcode] WHERE [code] = 'PO051'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Technical documentation' FROM [dbo].[nominalcode] WHERE [code] = 'PO052'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'General documentation ' FROM [dbo].[nominalcode] WHERE [code] = 'PO053'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Seminars, conferences ' FROM [dbo].[nominalcode] WHERE [code] = 'PO054'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Temporary Direct staff costs' FROM [dbo].[nominalcode] WHERE [code] = 'PO055'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Temporary Indirect Opex staff costs' FROM [dbo].[nominalcode] WHERE [code] = 'PO056'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Direct staff on loan' FROM [dbo].[nominalcode] WHERE [code] = 'PO057'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Indirect Opex staff on loan' FROM [dbo].[nominalcode] WHERE [code] = 'PO058'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Other fees' FROM [dbo].[nominalcode] WHERE [code] = 'PO059'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Cofrac Audit fees' FROM [dbo].[nominalcode] WHERE [code] = 'PO060'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Freight and carriage costs on purchase NA & EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO061'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Freight and carriage costs on purchase  EU' FROM [dbo].[nominalcode] WHERE [code] = 'PO062'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Freight and carriage costs on sale NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO063'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Freight and carriage costs interbranch NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO064'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Freight and carriage costs on sale UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO065'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Freight and carriage costs on sale inter company UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO066'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Freight and carriage costs on sale intercompany EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO067'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Freight and carriage costs on sale EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO068'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Moving costs' FROM [dbo].[nominalcode] WHERE [code] = 'PO069'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Fuel costs' FROM [dbo].[nominalcode] WHERE [code] = 'PO070'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Taxi, parking, tolls' FROM [dbo].[nominalcode] WHERE [code] = 'PO071'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Ticketing services' FROM [dbo].[nominalcode] WHERE [code] = 'PO072'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Hotels and restaurants' FROM [dbo].[nominalcode] WHERE [code] = 'PO073'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Invitations' FROM [dbo].[nominalcode] WHERE [code] = 'PO074'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Moving costs - Staff' FROM [dbo].[nominalcode] WHERE [code] = 'PO075'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Postage costs ' FROM [dbo].[nominalcode] WHERE [code] = 'PO076'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Telecoms costs' FROM [dbo].[nominalcode] WHERE [code] = 'PO077'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Mobile phones' FROM [dbo].[nominalcode] WHERE [code] = 'PO078'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Union dues and professional contributions' FROM [dbo].[nominalcode] WHERE [code] = 'PO079'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Recruitment costs' FROM [dbo].[nominalcode] WHERE [code] = 'PO080'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales calibration' FROM [dbo].[nominalcode] WHERE [code] = 'S001'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales adjustment' FROM [dbo].[nominalcode] WHERE [code] = 'S002'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales repair' FROM [dbo].[nominalcode] WHERE [code] = 'S003'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Other sales' FROM [dbo].[nominalcode] WHERE [code] = 'S004'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales spare sparts' FROM [dbo].[nominalcode] WHERE [code] = 'S005'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales carriage' FROM [dbo].[nominalcode] WHERE [code] = 'S006'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales IT' FROM [dbo].[nominalcode] WHERE [code] = 'S007'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales expertise/audit/training/park management' FROM [dbo].[nominalcode] WHERE [code] = 'S008'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales reseller' FROM [dbo].[nominalcode] WHERE [code] = 'S009'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales renting' FROM [dbo].[nominalcode] WHERE [code] = 'S010'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'en_GB', 'Sales outsource' FROM [dbo].[nominalcode] WHERE [code] = 'S011'
GO

-- Spanish translations
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Construcciones' FROM [dbo].[nominalcode] WHERE [code] = 'PC001';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mobiliario' FROM [dbo].[nominalcode] WHERE [code] = 'PC002';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Software - no t�cnico' FROM [dbo].[nominalcode] WHERE [code] = 'PC003';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Software - t�cnico y de producci�n' FROM [dbo].[nominalcode] WHERE [code] = 'PC004';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - ACELER�METRO' FROM [dbo].[nominalcode] WHERE [code] = 'PC005';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - AC�STICA' FROM [dbo].[nominalcode] WHERE [code] = 'PC006';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - QU�MICA - MEDIO AMBIENTE' FROM [dbo].[nominalcode] WHERE [code] = 'PC007';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - CLIM�TICO' FROM [dbo].[nominalcode] WHERE [code] = 'PC008';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - CAUDAL DE GAS' FROM [dbo].[nominalcode] WHERE [code] = 'PC009';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - CAUDAL DE L�QUIDOS' FROM [dbo].[nominalcode] WHERE [code] = 'PC010';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - DIMENSIONAL' FROM [dbo].[nominalcode] WHERE [code] = 'PC011';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - MISC' FROM [dbo].[nominalcode] WHERE [code] = 'PC012';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - EL�CTRICO' FROM [dbo].[nominalcode] WHERE [code] = 'PC013';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - FUERZA - PAR' FROM [dbo].[nominalcode] WHERE [code] = 'PC014';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - INFORM�TICA' FROM [dbo].[nominalcode] WHERE [code] = 'PC015';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - MASA - PESO' FROM [dbo].[nominalcode] WHERE [code] = 'PC016';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - MULTIDOMINIO' FROM [dbo].[nominalcode] WHERE [code] = 'PC017';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - PRESI�N' FROM [dbo].[nominalcode] WHERE [code] = 'PC018';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - RADIOMETR�A - FOTOMETR�A' FROM [dbo].[nominalcode] WHERE [code] = 'PC019';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - RADIACI�N IONIZACI�N' FROM [dbo].[nominalcode] WHERE [code] = 'PC020';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - TEMPERATURA - HIGROMETR�A' FROM [dbo].[nominalcode] WHERE [code] = 'PC021';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de producci�n - VOLUMEN' FROM [dbo].[nominalcode] WHERE [code] = 'PC022';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos inform�ticos' FROM [dbo].[nominalcode] WHERE [code] = 'PC023';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Equipos de log�stica' FROM [dbo].[nominalcode] WHERE [code] = 'PC024';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Elementos de transporte' FROM [dbo].[nominalcode] WHERE [code] = 'PC025';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Agua' FROM [dbo].[nominalcode] WHERE [code] = 'PG001';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Electricidad oficinas' FROM [dbo].[nominalcode] WHERE [code] = 'PG002';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gas' FROM [dbo].[nominalcode] WHERE [code] = 'PG003';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Suministro mantenimiento fuera de la actividad laboratorio' FROM [dbo].[nominalcode] WHERE [code] = 'PG004';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Consumibles inform�tica' FROM [dbo].[nominalcode] WHERE [code] = 'PG005';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Material de oficina' FROM [dbo].[nominalcode] WHERE [code] = 'PG006';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Consumibles para fotocopiadora' FROM [dbo].[nominalcode] WHERE [code] = 'PG007';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Subcontrataci�n sueldo' FROM [dbo].[nominalcode] WHERE [code] = 'PG008';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Control de presencias' FROM [dbo].[nominalcode] WHERE [code] = 'PG009';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Subcontrataci�n/alquiler espacio archivo' FROM [dbo].[nominalcode] WHERE [code] = 'PG010';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Arrendamiento financiero mobiliario' FROM [dbo].[nominalcode] WHERE [code] = 'PG011';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler oficinas ' FROM [dbo].[nominalcode] WHERE [code] = 'PG012';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Otros alquileres (Tecnallia�)' FROM [dbo].[nominalcode] WHERE [code] = 'PG013';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Renting veh�culos general' FROM [dbo].[nominalcode] WHERE [code] = 'PG014';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler veh�culos Corta duraci�n' FROM [dbo].[nominalcode] WHERE [code] = 'PG015';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler material Oficina' FROM [dbo].[nominalcode] WHERE [code] = 'PG016';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler material inform�tico' FROM [dbo].[nominalcode] WHERE [code] = 'PG017';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler material franqueo' FROM [dbo].[nominalcode] WHERE [code] = 'PG018';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler fotocopiadora' FROM [dbo].[nominalcode] WHERE [code] = 'PG019';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Comisi�n sobre ventas' FROM [dbo].[nominalcode] WHERE [code] = 'PG020';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mantenimiento locales' FROM [dbo].[nominalcode] WHERE [code] = 'PG021';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mantenimiento de veh�culos general' FROM [dbo].[nominalcode] WHERE [code] = 'PG022';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Limpieza oficinas' FROM [dbo].[nominalcode] WHERE [code] = 'PG023';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mantenimiento inform�tico recurrente' FROM [dbo].[nominalcode] WHERE [code] = 'PG024';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Asistencia inform�tica espec�fica' FROM [dbo].[nominalcode] WHERE [code] = 'PG025';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Dep�sitos de residuos' FROM [dbo].[nominalcode] WHERE [code] = 'PG026';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gastos sanitarios' FROM [dbo].[nominalcode] WHERE [code] = 'PG027';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Seminarios, coloquios, conferencias' FROM [dbo].[nominalcode] WHERE [code] = 'PG028';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Personal interino indirecto' FROM [dbo].[nominalcode] WHERE [code] = 'PG029';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Personal desplazado indirecto' FROM [dbo].[nominalcode] WHERE [code] = 'PG030';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Honorarios abogados' FROM [dbo].[nominalcode] WHERE [code] = 'PG031';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Servicios profesionales independientes' FROM [dbo].[nominalcode] WHERE [code] = 'PG032';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Otros servicios (correos�)' FROM [dbo].[nominalcode] WHERE [code] = 'PG033';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Asesor�as profesionales' FROM [dbo].[nominalcode] WHERE [code] = 'PG034';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Anuncios Inserciones Publicidad' FROM [dbo].[nominalcode] WHERE [code] = 'PG035';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Salones' FROM [dbo].[nominalcode] WHERE [code] = 'PG036';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Regalos clientes' FROM [dbo].[nominalcode] WHERE [code] = 'PG037';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Propinas, subvenciones' FROM [dbo].[nominalcode] WHERE [code] = 'PG038';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Traslado empresa' FROM [dbo].[nominalcode] WHERE [code] = 'PG039';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Combustibles' FROM [dbo].[nominalcode] WHERE [code] = 'PG040';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Taxi, Aparcamiento, Peaje' FROM [dbo].[nominalcode] WHERE [code] = 'PG041';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Cajero' FROM [dbo].[nominalcode] WHERE [code] = 'PG042';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Hoteles y restaurante' FROM [dbo].[nominalcode] WHERE [code] = 'PG043';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Invitaciones' FROM [dbo].[nominalcode] WHERE [code] = 'PG044';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gastos viaje' FROM [dbo].[nominalcode] WHERE [code] = 'PG045';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Liberaciones' FROM [dbo].[nominalcode] WHERE [code] = 'PG046';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Telefon�a fija, Fax' FROM [dbo].[nominalcode] WHERE [code] = 'PG047';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Telefon�a m�vil' FROM [dbo].[nominalcode] WHERE [code] = 'PG048';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Telecomunicaciones - Redes  de datos' FROM [dbo].[nominalcode] WHERE [code] = 'PG049';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gastos bancarios' FROM [dbo].[nominalcode] WHERE [code] = 'PG050';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Cotizaciones sindicales y profesionales' FROM [dbo].[nominalcode] WHERE [code] = 'PG051';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gastos contrataci�n de personal' FROM [dbo].[nominalcode] WHERE [code] = 'PG052';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Prestaciones Grupo' FROM [dbo].[nominalcode] WHERE [code] = 'PG053';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Compra equipo/mat para venta' FROM [dbo].[nominalcode] WHERE [code] = 'PO001';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Compra mercanc�a - suministro para venta UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO002';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Compra mercanc�a - suministro para venta EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO003';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Compra repuestos labo' FROM [dbo].[nominalcode] WHERE [code] = 'PO004';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Piezas de recambio clientes externos UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO005';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Piezas de recambio sobre clientes externos EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO006';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Reparaciones equipos clientes' FROM [dbo].[nominalcode] WHERE [code] = 'PO007';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Reparaciones grupo equipos clientes' FROM [dbo].[nominalcode] WHERE [code] = 'PO008';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Laboratorios externos' FROM [dbo].[nominalcode] WHERE [code] = 'PO009';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Laboratorios externos grupo' FROM [dbo].[nominalcode] WHERE [code] = 'PO010';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Subcontrataci�n externa Grupo para clientes externos UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO011';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Subcontrataci�n externa Grupo para clientes externos EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO012';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Embalajes' FROM [dbo].[nominalcode] WHERE [code] = 'PO013';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Compra Servicio - compra/venta NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO014';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Compra Servicio - comprar/venta UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO015';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Compra Servicio - comprar/venta EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO016';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler de equipos clientes externos' FROM [dbo].[nominalcode] WHERE [code] = 'PO017';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Subcontrataci�n Contrato Transporte' FROM [dbo].[nominalcode] WHERE [code] = 'PO018';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Agua' FROM [dbo].[nominalcode] WHERE [code] = 'PO019';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Electricidad laboratorio' FROM [dbo].[nominalcode] WHERE [code] = 'PO020';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gas' FROM [dbo].[nominalcode] WHERE [code] = 'PO021';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Piezas de recambio origen NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO022';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Compra material laboratorio' FROM [dbo].[nominalcode] WHERE [code] = 'PO023';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Consumibles laboratorios ' FROM [dbo].[nominalcode] WHERE [code] = 'PO024';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Consumibles laboratorios origen UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO025';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Peque�a herramienta origen NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO026';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Peque�a herramienta origen EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO027';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Peque�a Herramienta origen UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO028';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Suministro mantenimiento fuera de la actividad laboratorio' FROM [dbo].[nominalcode] WHERE [code] = 'PO029';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ropa de trabajo/material de seguridad' FROM [dbo].[nominalcode] WHERE [code] = 'PO030';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Suministros administrativos' FROM [dbo].[nominalcode] WHERE [code] = 'PO031';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Consumo suministro fotocopiadora' FROM [dbo].[nominalcode] WHERE [code] = 'PO032';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Vigilancia' FROM [dbo].[nominalcode] WHERE [code] = 'PO033';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler laboratorio' FROM [dbo].[nominalcode] WHERE [code] = 'PO034';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler oficinas "sat�lites" (Vigo, Sevilla�)' FROM [dbo].[nominalcode] WHERE [code] = 'PO035';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler inmobiliario para el personal' FROM [dbo].[nominalcode] WHERE [code] = 'PO036';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler material para parque interno' FROM [dbo].[nominalcode] WHERE [code] = 'PO037';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Renting veh�culos opex' FROM [dbo].[nominalcode] WHERE [code] = 'PO038';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler material Oficina' FROM [dbo].[nominalcode] WHERE [code] = 'PO039';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler fotocopiadora' FROM [dbo].[nominalcode] WHERE [code] = 'PO040';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Alquiler inform�tica laboratorio' FROM [dbo].[nominalcode] WHERE [code] = 'PO041';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Cargas de alquiler' FROM [dbo].[nominalcode] WHERE [code] = 'PO042';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mantenimiento locales' FROM [dbo].[nominalcode] WHERE [code] = 'PO043';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Reparaciones varias laboratorio' FROM [dbo].[nominalcode] WHERE [code] = 'PO044';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Limpieza laboratorio' FROM [dbo].[nominalcode] WHERE [code] = 'PO045';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Calibraci�n nuestros patrones' FROM [dbo].[nominalcode] WHERE [code] = 'PO046';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mantenimiento Parque Interno - Reparaci�n - UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO047';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mantenimiento de veh�culos opex' FROM [dbo].[nominalcode] WHERE [code] = 'PO048';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mantenimiento Material oficina y Material inform�tico' FROM [dbo].[nominalcode] WHERE [code] = 'PO049';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mantenimiento ropa de trabajo' FROM [dbo].[nominalcode] WHERE [code] = 'PO050';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Dep�sitos de residuos' FROM [dbo].[nominalcode] WHERE [code] = 'PO051';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Documentaci�n General y otras suscripciones' FROM [dbo].[nominalcode] WHERE [code] = 'PO052';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Seminarios, coloquios, conferencias' FROM [dbo].[nominalcode] WHERE [code] = 'PO053';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Personal interino directo' FROM [dbo].[nominalcode] WHERE [code] = 'PO054';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Personal interino indirecto' FROM [dbo].[nominalcode] WHERE [code] = 'PO055';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Personal desplazado directo' FROM [dbo].[nominalcode] WHERE [code] = 'PO056';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Personal desplazado indirecto' FROM [dbo].[nominalcode] WHERE [code] = 'PO057';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gastos formaci�n directos/indirectos' FROM [dbo].[nominalcode] WHERE [code] = 'PO058';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Honorarios certificaci�n y auditor�as Cofrac' FROM [dbo].[nominalcode] WHERE [code] = 'PO059';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gastos ENAC' FROM [dbo].[nominalcode] WHERE [code] = 'PO060';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Mensajeros' FROM [dbo].[nominalcode] WHERE [code] = 'PO061';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Portes' FROM [dbo].[nominalcode] WHERE [code] = 'PO062';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Transporte sobre ventas inter agencia NACIONAL' FROM [dbo].[nominalcode] WHERE [code] = 'PO063';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Transporte sobre ventas clientes UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO064';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Transporte sobre ventas clientes UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO065';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Transporte sobre ventas intra Grupo UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO066';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Transporte sobre ventas intra Grupo EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO067';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Transporte sobre ventas clientes EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO068';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Traslado empresa' FROM [dbo].[nominalcode] WHERE [code] = 'PO069';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Combustibles' FROM [dbo].[nominalcode] WHERE [code] = 'PO070';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Taxi, Aparcamiento, Peaje' FROM [dbo].[nominalcode] WHERE [code] = 'PO071';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Invitaciones' FROM [dbo].[nominalcode] WHERE [code] = 'PO072';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Hoteles y restaurante' FROM [dbo].[nominalcode] WHERE [code] = 'PO073';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Liberaciones' FROM [dbo].[nominalcode] WHERE [code] = 'PO074';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gastos viaje' FROM [dbo].[nominalcode] WHERE [code] = 'PO075';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Liberaciones' FROM [dbo].[nominalcode] WHERE [code] = 'PO076';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Telefon�a fija, Fax' FROM [dbo].[nominalcode] WHERE [code] = 'PO077';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Telefon�a m�vil' FROM [dbo].[nominalcode] WHERE [code] = 'PO078';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Cotizaciones sindicales y profesionales' FROM [dbo].[nominalcode] WHERE [code] = 'PO079';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Gastos contrataci�n de personal' FROM [dbo].[nominalcode] WHERE [code] = 'PO080';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas calibraci�n' FROM [dbo].[nominalcode] WHERE [code] = 'S001';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas ajuste' FROM [dbo].[nominalcode] WHERE [code] = 'S002';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas reparaci�n' FROM [dbo].[nominalcode] WHERE [code] = 'S003';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Otras ventas' FROM [dbo].[nominalcode] WHERE [code] = 'S004';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas piezas de repuesto' FROM [dbo].[nominalcode] WHERE [code] = 'S005';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas transporte' FROM [dbo].[nominalcode] WHERE [code] = 'S006';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas IT' FROM [dbo].[nominalcode] WHERE [code] = 'S007';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas consultor�a/auditor�a/formaci�n/gesti�n del parque' FROM [dbo].[nominalcode] WHERE [code] = 'S008';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas suministros' FROM [dbo].[nominalcode] WHERE [code] = 'S009';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas arrendamiento' FROM [dbo].[nominalcode] WHERE [code] = 'S010';
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'es_ES', 'Ventas subcontrataci�n' FROM [dbo].[nominalcode] WHERE [code] = 'S011';
GO

-- French translations
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Agencements et b�timents' FROM [dbo].[nominalcode] WHERE [code] = 'PC001'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Mobiliers' FROM [dbo].[nominalcode] WHERE [code] = 'PC002'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Logiciels - non techniques' FROM [dbo].[nominalcode] WHERE [code] = 'PC003'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Logiciels - technique et production' FROM [dbo].[nominalcode] WHERE [code] = 'PC004'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production ACCELEROMETRIE' FROM [dbo].[nominalcode] WHERE [code] = 'PC005'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production ACOUSTIQUE' FROM [dbo].[nominalcode] WHERE [code] = 'PC006'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production CHIMIE - ENVIRONNEMENT' FROM [dbo].[nominalcode] WHERE [code] = 'PC007'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production CLIMATIQUE' FROM [dbo].[nominalcode] WHERE [code] = 'PC008'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production DEBITMETRIE GAZ' FROM [dbo].[nominalcode] WHERE [code] = 'PC009'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production DEBITMETRIE LIQUIDE' FROM [dbo].[nominalcode] WHERE [code] = 'PC010'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production DIMENSIONNEL' FROM [dbo].[nominalcode] WHERE [code] = 'PC011'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production DIVERS' FROM [dbo].[nominalcode] WHERE [code] = 'PC012'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production ELECTRIQUE' FROM [dbo].[nominalcode] WHERE [code] = 'PC013'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production FORCE - COUPLE' FROM [dbo].[nominalcode] WHERE [code] = 'PC014'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production INFORMATIQUE' FROM [dbo].[nominalcode] WHERE [code] = 'PC015'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production MASSE - BALANCE' FROM [dbo].[nominalcode] WHERE [code] = 'PC016'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production MULTIDOMAINE' FROM [dbo].[nominalcode] WHERE [code] = 'PC017'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production PRESSION' FROM [dbo].[nominalcode] WHERE [code] = 'PC018'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production RADIOMETRIE - PHOTOMETRIE' FROM [dbo].[nominalcode] WHERE [code] = 'PC019'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production RAYONNEMENT IONISANT' FROM [dbo].[nominalcode] WHERE [code] = 'PC020'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production TEMPERATURE - HYGROMETRIE' FROM [dbo].[nominalcode] WHERE [code] = 'PC021'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements de production VOLUME' FROM [dbo].[nominalcode] WHERE [code] = 'PC022'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements informatique' FROM [dbo].[nominalcode] WHERE [code] = 'PC023'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Equipements logisitque' FROM [dbo].[nominalcode] WHERE [code] = 'PC024'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'V�hicules' FROM [dbo].[nominalcode] WHERE [code] = 'PC025'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Eau' FROM [dbo].[nominalcode] WHERE [code] = 'PG001'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'El�ctricit�' FROM [dbo].[nominalcode] WHERE [code] = 'PG002'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Gaz' FROM [dbo].[nominalcode] WHERE [code] = 'PG003'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Fourniture entretien hors activit� labo' FROM [dbo].[nominalcode] WHERE [code] = 'PG004'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Achats logiciels + autres petits �quipements' FROM [dbo].[nominalcode] WHERE [code] = 'PG005'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Fournitures administrative' FROM [dbo].[nominalcode] WHERE [code] = 'PG006'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Consommation fourniture photocopieur' FROM [dbo].[nominalcode] WHERE [code] = 'PG007'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous-traitance paie' FROM [dbo].[nominalcode] WHERE [code] = 'PG008'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Gardiennage' FROM [dbo].[nominalcode] WHERE [code] = 'PG009'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous-traitance / location espace archive' FROM [dbo].[nominalcode] WHERE [code] = 'PG010'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Cr�dit bail mobilier' FROM [dbo].[nominalcode] WHERE [code] = 'PG011'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location immobili�re' FROM [dbo].[nominalcode] WHERE [code] = 'PG012'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location immobili�re pour le personnel' FROM [dbo].[nominalcode] WHERE [code] = 'PG013'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location v�hicule Longue Dur�e' FROM [dbo].[nominalcode] WHERE [code] = 'PG014'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location v�hicule Courte Dur�e' FROM [dbo].[nominalcode] WHERE [code] = 'PG015'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location mat�riel Bureau' FROM [dbo].[nominalcode] WHERE [code] = 'PG016'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location mat�riel informatique' FROM [dbo].[nominalcode] WHERE [code] = 'PG017'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location mat�riel affranchissement' FROM [dbo].[nominalcode] WHERE [code] = 'PG018'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location photocopieur' FROM [dbo].[nominalcode] WHERE [code] = 'PG019'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Charges locatives' FROM [dbo].[nominalcode] WHERE [code] = 'PG020'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretiens locaux' FROM [dbo].[nominalcode] WHERE [code] = 'PG021'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretiens v�hicules' FROM [dbo].[nominalcode] WHERE [code] = 'PG022'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretien Mat�riel bureau et Mat�riel informatique' FROM [dbo].[nominalcode] WHERE [code] = 'PG023'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Maintenance informatique r�currente' FROM [dbo].[nominalcode] WHERE [code] = 'PG024'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Assistance informatique ponctuelle' FROM [dbo].[nominalcode] WHERE [code] = 'PG025'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Bennes d�chets' FROM [dbo].[nominalcode] WHERE [code] = 'PG026'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Documentation G�n�rale et autres abonnements' FROM [dbo].[nominalcode] WHERE [code] = 'PG027'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'S�minaire, colloques, conf�rences' FROM [dbo].[nominalcode] WHERE [code] = 'PG028'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Personnel int�rimaire indirect' FROM [dbo].[nominalcode] WHERE [code] = 'PG029'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Personnel d�tach� indirect' FROM [dbo].[nominalcode] WHERE [code] = 'PG030'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Honoraires avocats' FROM [dbo].[nominalcode] WHERE [code] = 'PG031'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Honotraires CAC et autres audits financiers' FROM [dbo].[nominalcode] WHERE [code] = 'PG032'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Frais d''actes' FROM [dbo].[nominalcode] WHERE [code] = 'PG033'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Autres honoraires' FROM [dbo].[nominalcode] WHERE [code] = 'PG034'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Annonces Insertions Publicit�' FROM [dbo].[nominalcode] WHERE [code] = 'PG035'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Salons' FROM [dbo].[nominalcode] WHERE [code] = 'PG036'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Cadeaux clients' FROM [dbo].[nominalcode] WHERE [code] = 'PG037'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Pourboires, dons' FROM [dbo].[nominalcode] WHERE [code] = 'PG038'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'D�m�nagement entreprise' FROM [dbo].[nominalcode] WHERE [code] = 'PG039'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Carburants' FROM [dbo].[nominalcode] WHERE [code] = 'PG040'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Taxi, Parking, P�age' FROM [dbo].[nominalcode] WHERE [code] = 'PG041'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Billetterie' FROM [dbo].[nominalcode] WHERE [code] = 'PG042'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'H�tels et restaurant' FROM [dbo].[nominalcode] WHERE [code] = 'PG043'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Invitations' FROM [dbo].[nominalcode] WHERE [code] = 'PG044'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Frais d�m�nagement Personnel' FROM [dbo].[nominalcode] WHERE [code] = 'PG045'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Affranchissements' FROM [dbo].[nominalcode] WHERE [code] = 'PG046'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'T�l�phonie fixe, Fax' FROM [dbo].[nominalcode] WHERE [code] = 'PG047'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'T�l�phonie mobile' FROM [dbo].[nominalcode] WHERE [code] = 'PG048'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Telecom - R�seau data' FROM [dbo].[nominalcode] WHERE [code] = 'PG049'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'NAais bancaires' FROM [dbo].[nominalcode] WHERE [code] = 'PG050'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Cotisation syndicales et professionnelles' FROM [dbo].[nominalcode] WHERE [code] = 'PG051'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Frais recrutement de personnel' FROM [dbo].[nominalcode] WHERE [code] = 'PG052'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Prestations Groupe' FROM [dbo].[nominalcode] WHERE [code] = 'PG053'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Achat marchandise - procurement produits n�goce NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO001'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Achat marchandise - procurement produits n�goce UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO002'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Achat marchandise - procurement produits n�goce EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO003'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Pi�ces d�tach�es sur affaires NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO004'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Pi�ces d�tach�es sur affaires UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO005'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Pi�ces d�tach�es sur affaires EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO006'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous traitance externe hors Groupe sur affaires NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO007'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous traitance externe hors Groupe sur affaires UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO008'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous traitance externe hors Groupe sur affaires EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO009'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous traitance externe  Groupe sur affaires NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO010'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous traitance externe  Groupe sur affaires UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO011'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous traitance externe  Groupe sur affaires EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO012'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Emballages' FROM [dbo].[nominalcode] WHERE [code] = 'PO013'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Achat Service - procurement - n�goce NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO014'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Achat Service - procurement - n�goce UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO015'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Achat Service - procurement - n�goce EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO016'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location �quipement sur affaire' FROM [dbo].[nominalcode] WHERE [code] = 'PO017'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous traitance  Contrat Portage' FROM [dbo].[nominalcode] WHERE [code] = 'PO018'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Eau' FROM [dbo].[nominalcode] WHERE [code] = 'PO019'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'El�ctricit�' FROM [dbo].[nominalcode] WHERE [code] = 'PO020'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Gaz' FROM [dbo].[nominalcode] WHERE [code] = 'PO021'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Pi�ces d�tach�es origine NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO022'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Pi�ces d�tach�es origine UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO023'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Consommables laboratoires origine NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO024'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Consommables laboratoires origine UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO025'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Petit outillage origine NA' FROM [dbo].[nominalcode] WHERE [code] = 'PO026'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Petit outillage origine EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO027'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Petit outillage origine UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO028'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Fourniture entretien hors activit� labo' FROM [dbo].[nominalcode] WHERE [code] = 'PO029'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'V�tement de travail / mat�riel de s�curit�' FROM [dbo].[nominalcode] WHERE [code] = 'PO030'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Fournitures administrative' FROM [dbo].[nominalcode] WHERE [code] = 'PO031'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Consommation fourniture photocopieur' FROM [dbo].[nominalcode] WHERE [code] = 'PO032'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Gardiennage' FROM [dbo].[nominalcode] WHERE [code] = 'PO033'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Sous-traitance / location espace archive' FROM [dbo].[nominalcode] WHERE [code] = 'PO034'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location immobili�re' FROM [dbo].[nominalcode] WHERE [code] = 'PO035'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location immobili�re pour le personnel' FROM [dbo].[nominalcode] WHERE [code] = 'PO036'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location mat�riel pour parc interne' FROM [dbo].[nominalcode] WHERE [code] = 'PO037'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location v�hicule Longue Dur�e' FROM [dbo].[nominalcode] WHERE [code] = 'PO038'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location v�hicule Courte Dur�e' FROM [dbo].[nominalcode] WHERE [code] = 'PO039'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location mat�riel Bureau' FROM [dbo].[nominalcode] WHERE [code] = 'PO040'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Location photocopieur' FROM [dbo].[nominalcode] WHERE [code] = 'PO041'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Charges locatives' FROM [dbo].[nominalcode] WHERE [code] = 'PO042'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretiens locaux' FROM [dbo].[nominalcode] WHERE [code] = 'PO043'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretien Parc Interne - V�rification p�riodique - NATIONAL' FROM [dbo].[nominalcode] WHERE [code] = 'PO044'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretien Parc Interne - R�paration - NATIONAL' FROM [dbo].[nominalcode] WHERE [code] = 'PO045'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretien Parc Interne - V�rification p�riodique - UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO046'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretien Parc Interne - R�paration - UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO047'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretiens v�hicules' FROM [dbo].[nominalcode] WHERE [code] = 'PO048'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretien Mat�riel bureau et Mat�riel informatique' FROM [dbo].[nominalcode] WHERE [code] = 'PO049'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Entretien v�tement travail' FROM [dbo].[nominalcode] WHERE [code] = 'PO050'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Bennes d�chets' FROM [dbo].[nominalcode] WHERE [code] = 'PO051'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Documentation technique' FROM [dbo].[nominalcode] WHERE [code] = 'PO052'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Documentation G�n�rale et autres abonnements' FROM [dbo].[nominalcode] WHERE [code] = 'PO053'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'S�minaire, colloques, conf�rences' FROM [dbo].[nominalcode] WHERE [code] = 'PO054'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Personnel int�rimaire direct' FROM [dbo].[nominalcode] WHERE [code] = 'PO055'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Personnel int�rimaire indirect' FROM [dbo].[nominalcode] WHERE [code] = 'PO056'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Personnel d�tach� direct' FROM [dbo].[nominalcode] WHERE [code] = 'PO057'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Personnel d�tach� indirect' FROM [dbo].[nominalcode] WHERE [code] = 'PO058'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Autres honoraires' FROM [dbo].[nominalcode] WHERE [code] = 'PO059'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Honoraires certification et audit Cofrac' FROM [dbo].[nominalcode] WHERE [code] = 'PO060'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Transport sur achat National et Export' FROM [dbo].[nominalcode] WHERE [code] = 'PO061'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Transport sur achat UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO062'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Transport sur ventes NATIONAL' FROM [dbo].[nominalcode] WHERE [code] = 'PO063'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Transport sur ventes inter agence NATIONAL' FROM [dbo].[nominalcode] WHERE [code] = 'PO064'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Transport sur ventes clients UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO065'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Transport sur ventes intra Groupe UE' FROM [dbo].[nominalcode] WHERE [code] = 'PO066'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Transport sur ventes intra Groupe EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO067'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Transport sur ventes clients EX' FROM [dbo].[nominalcode] WHERE [code] = 'PO068'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'D�m�nagement entreprise' FROM [dbo].[nominalcode] WHERE [code] = 'PO069'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Carburants' FROM [dbo].[nominalcode] WHERE [code] = 'PO070'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Taxi, Parking, P�age' FROM [dbo].[nominalcode] WHERE [code] = 'PO071'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Billetterie' FROM [dbo].[nominalcode] WHERE [code] = 'PO072'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'H�tels et restaurant' FROM [dbo].[nominalcode] WHERE [code] = 'PO073'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Invitations' FROM [dbo].[nominalcode] WHERE [code] = 'PO074'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Frais d�m�nagement Personnel' FROM [dbo].[nominalcode] WHERE [code] = 'PO075'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Affranchissements' FROM [dbo].[nominalcode] WHERE [code] = 'PO076'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'T�l�phonie fixe, Fax' FROM [dbo].[nominalcode] WHERE [code] = 'PO077'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'T�l�phonie mobile' FROM [dbo].[nominalcode] WHERE [code] = 'PO078'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Cotisation syndicales et professionnelles' FROM [dbo].[nominalcode] WHERE [code] = 'PO079'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Frais recrutement de personnel' FROM [dbo].[nominalcode] WHERE [code] = 'PO080'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Calibration' FROM [dbo].[nominalcode] WHERE [code] = 'S001'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Ajustage' FROM [dbo].[nominalcode] WHERE [code] = 'S002'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - R�paration' FROM [dbo].[nominalcode] WHERE [code] = 'S003'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Autres prestations' FROM [dbo].[nominalcode] WHERE [code] = 'S004'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Pi�ces d�tach�es' FROM [dbo].[nominalcode] WHERE [code] = 'S005'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Transport' FROM [dbo].[nominalcode] WHERE [code] = 'S006'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Informatique' FROM [dbo].[nominalcode] WHERE [code] = 'S007'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Prestations de conseil' FROM [dbo].[nominalcode] WHERE [code] = 'S008'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Procurement' FROM [dbo].[nominalcode] WHERE [code] = 'S009'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Location' FROM [dbo].[nominalcode] WHERE [code] = 'S010'
INSERT INTO [dbo].[nominalcodetitletranslation] ([id], [locale], [translation]) SELECT [id], 'fr_FR', 'Chiffre d''affaires - Sous traitance externe' FROM [dbo].[nominalcode] WHERE [code] = 'S011'
GO

ROLLBACK TRAN