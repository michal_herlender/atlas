USE spain;
GO

BEGIN TRAN

	INSERT INTO defaultnotelabeltranslation(defaultnoteid, locale, translation)
	VALUES
		(1, 'de_DE', 'Transport'),
		(2, 'de_DE', 'Durchlauf'),
		(3, 'de_DE', 'Schnellverfahren'),
		(4, 'de_DE', 'Erinnerungsfunktion'),
		(5, 'de_DE', 'Versandservice'),
		(6, 'de_DE', 'Angebotsbezug'),
		(10, 'de_DE', 'Batterie / Sicherung')

	INSERT INTO defaultnotenotetranslation(defaultnoteid, locale, translation)
	VALUES
		(1, 'de_DE', 'Kosten ausschließlich für Abholung und Lieferung'),
		(2, 'de_DE', 'Zehn (10) Arbeitstage ab Eingang des Prüfmittels'),
		(3, 'de_DE', '50% Aufpreis, Durchlauf in drei (3) Tagen ab Eingang des Prüfmittels zzgl. Lieferzeit'),
		(4, 'de_DE', 'Trescal bietet eine monatliche Erinnerungsfunktion an, welche über anstehende Kalibrierungen informiert.'),
		(5, 'de_DE', ''),
		(6, 'de_DE', '<p style="color:red;">Der Angebotsbezug unten auf dem Dokument muss auf allen Liefer- und Bestelldokumenten angegeben werden. Ein Fehlen dieser Angabe kann zu Änderungen von Service und Preisen fühen.</p>'),
		(10, 'de_DE', 'Bitte liefern Sie Ihre Prüfmittel mit Batterien und Sicherungen in perfektem Zustand. Der Kunde akzeptiert andererseits Kosten in Höhe bis zu 12€ für die Beschaffung von geeigneten Batterien und Sicherungen.')

COMMIT TRAN