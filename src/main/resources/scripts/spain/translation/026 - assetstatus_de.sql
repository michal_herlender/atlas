USE spain;
GO

BEGIN TRAN

	INSERT INTO assetstatustranslation(id, locale, translation)
	VALUES
		(1, 'de_DE', 'Aktiver Bestand'),
		(2, 'de_DE', 'Aussortierter, aber noch existierender Bestand'),
		(3, 'de_DE', 'Ausgemusterter Bestand'),
		(4, 'de_DE', 'Reservebestand')

COMMIT TRAN