BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 1 AND locale = 'es_ES') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (1, 'es_ES', '1') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'Calibraci�n' WHERE id = 1 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 2 AND locale = 'es_ES') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (2, 'es_ES', '2') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'V�lvulas' WHERE id = 2 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 3 AND locale = 'es_ES') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (3, 'es_ES', '3') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'Ventas' WHERE id = 3 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 4 AND locale = 'es_ES') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (4, 'es_ES', '4') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'Alquiler' WHERE id = 4 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 5 AND locale = 'es_ES') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (5, 'es_ES', '5') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'Pro forma' WHERE id = 5 AND locale = 'es_ES';

COMMIT TRAN