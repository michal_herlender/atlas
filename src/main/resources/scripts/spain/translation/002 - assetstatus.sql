BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.assetstatustranslation WHERE id = 1 AND locale = 'es_ES') INSERT INTO dbo.assetstatustranslation (id, locale, translation) VALUES (1, 'es_ES', 'Activo en uso') ELSE UPDATE dbo.assetstatustranslation SET translation = 'Activo en uso' WHERE id = 1 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.assetstatustranslation WHERE id = 2 AND locale = 'es_ES') INSERT INTO dbo.assetstatustranslation (id, locale, translation) VALUES (2, 'es_ES', 'Activo retirado (pero todav�a existe)') ELSE UPDATE dbo.assetstatustranslation SET translation = 'Activo retirado (pero todav�a existe)' WHERE id = 2 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.assetstatustranslation WHERE id = 3 AND locale = 'es_ES') INSERT INTO dbo.assetstatustranslation (id, locale, translation) VALUES (3, 'es_ES', 'Activo desechado') ELSE UPDATE dbo.assetstatustranslation SET translation = 'Activo desechado' WHERE id = 3 AND locale = 'es_ES';
IF NOT EXISTS (SELECT 1 FROM dbo.assetstatustranslation WHERE id = 4 AND locale = 'es_ES') INSERT INTO dbo.assetstatustranslation (id, locale, translation) VALUES (4, 'es_ES', 'Activo de reserva') ELSE UPDATE dbo.assetstatustranslation SET translation = 'Activo de reserva' WHERE id = 4 AND locale = 'es_ES';

COMMIT TRAN