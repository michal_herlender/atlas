BEGIN TRAN;

UPDATE contact 
	SET contact.groupid = (SELECT u1.groupid FROM usergroup u1 WHERE u1.name = 'Subdivision View')
	WHERE contact.groupid IN (SELECT u2.groupid FROM usergroup u2 WHERE u2.name IN ('GRUPO SUBDIVISON','Basic Subdiv','PER_SUBDIVISION'));

UPDATE contact 
	SET contact.groupid = (SELECT u1.groupid FROM usergroup u1 WHERE u1.name = 'Company View')
	WHERE contact.groupid IN (SELECT u2.groupid FROM usergroup u2 WHERE u2.name IN ('Nivel Compa�ia','COMPANY','Compa�ia','standar COMPA�IA','PER_COMPANY'));

ROLLBACK TRAN;