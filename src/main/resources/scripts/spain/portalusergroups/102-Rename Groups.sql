BEGIN TRAN;

UPDATE usergroup SET usergroup.name = 'Address View', usergroup.description = 'This group allows a user to view instruments belonging to their address' WHERE usergroup.name='Basic Address';
UPDATE usergroup SET usergroup.name = 'Subdivision View', usergroup.description = 'This group allows a user to view instruments belonging to their subdivision' WHERE usergroup.name='Basic Subdivision';
UPDATE usergroup SET usergroup.name = 'Company View', usergroup.description = 'This group allows a user to view instruments belonging to their company' WHERE usergroup.name='Basic Company';


ROLLBACK TRAN;