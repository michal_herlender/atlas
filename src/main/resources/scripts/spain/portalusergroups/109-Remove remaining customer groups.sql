BEGIN TRAN;

DELETE ugv FROM usergroupvalues ugv
WHERE ugv.groupid IN(
	SELECT  DISTINCT usergroup.groupid
    FROM usergroup
     WHERE coid >0);

DELETE ug FROM usergroup ug
WHERE ug.groupid IN(SELECT  DISTINCT usergroup.groupid 
FROM usergroup
  LEFT JOIN usergroupvalues ON usergroupvalues.groupid = usergroup.groupid
  WHERE usergroup.coid > 0 
  AND valueid IS NULL)

ROLLBACK TRAN;