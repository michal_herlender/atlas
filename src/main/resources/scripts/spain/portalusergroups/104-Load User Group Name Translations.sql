BEGIN TRAN;

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'en_GB' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM  usergroup 
                                                       WHERE usergroup.name = 'Default Group')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'en_GB', 
         'Default Group' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Default Group'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'fr_FR' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM usergroup 
                                                       WHERE usergroup.name = 'Default Group')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'fr_FR', 
         'Group par d�faut' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Default Group'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'es_ES' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM usergroup 
                                                       WHERE usergroup.name = 'Default Group')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'es_ES', 
         'Grupo por defecto' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Default Group'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'en_GB' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM usergroup 
                                                       WHERE usergroup.name = 'Address View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'en_GB', 
         'Address View' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Address View'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'fr_FR' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM usergroup 
                                                       WHERE usergroup.name = 'Address View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'fr_FR', 
         'Vue Adresse' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Address View'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'es_ES' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM   usergroup 
                                                       WHERE usergroup.name = 'Address View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'es_ES', 
         'Vista por direccion' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Address View'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'en_GB' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM usergroup 
                                                       WHERE usergroup.name = 'Subdivision View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'en_GB', 
         'Subdivision View' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Subdivision View'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'fr_FR' 
                      AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                              FROM usergroup 
                                                              WHERE usergroup.name = 'Subdivision View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'fr_FR', 
         'Vue Subdivision' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Subdivision View'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'es_ES' 
                      AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                              FROM usergroup 
                                                              WHERE usergroup.name = 'Subdivision View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'es_ES', 
         'Vista por subdivisi�n' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Subdivision View'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'en_GB' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM usergroup 
                                                       WHERE usergroup.name = 'Company View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'en_GB', 
         'Company View' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Company View'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'fr_FR' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM usergroup 
                                                       WHERE usergroup.name = 'Company View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'fr_FR', 
         'Vue Soci�t�' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Company View'; 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'es_ES' 
               AND usergroupnametranslation.groupid = (SELECT usergroup.groupid 
                                                       FROM usergroup 
                                                       WHERE usergroup.name = 'Company View')) 
  INSERT INTO usergroupnametranslation 
  SELECT usergroup.groupid, 
         'es_ES', 
         'Vista por empresa' 
  FROM   usergroup 
  WHERE  usergroup.name = 'Company View';  

  IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'en_GB' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Edit Instruments')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'en_GB', 
         'Edit Instruments' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Edit Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'fr_FR' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Edit Instruments')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'fr_FR', 
         'Modifier les instruments' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Edit Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'es_ES' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Edit Instruments')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'es_ES', 
         'Editar instrumentos' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Edit Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'en_GB' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Add Calibrations')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'en_GB', 
         'Add Calibrations' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Add Calibrations' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'fr_FR' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Add Calibrations')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'fr_FR', 
         'Ajouter des calibrations' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Add Calibrations' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'es_ES' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Add Calibrations')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'es_ES', 
         'A�adir calibraciones' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Add Calibrations' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'en_GB' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Validate Certificates' 
                                                             )) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'en_GB', 
         'Validate Certificates' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Validate Certificates' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'fr_FR' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Validate Certificates' 
                                                             )) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'fr_FR', 
         'Valider des certificats' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Validate Certificates' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'es_ES' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Validate Certificates' 
                                                             )) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'es_ES', 
         'Validar certificados' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Validate Certificates' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'en_GB' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Add Instruments')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'en_GB', 
         'Add Instruments' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Add Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'fr_FR' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Add Instruments')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'fr_FR', 
         'Ajouter des instruments' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Add Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupnametranslation 
               WHERE  usergroupnametranslation.locale = 'es_ES' 
                      AND usergroupnametranslation.groupid = (SELECT ug.groupid 
                                                              FROM 
                          usergroup ug 
                                                              WHERE 
                          ug.NAME = 'Add Instruments')) 
  INSERT INTO usergroupnametranslation 
  SELECT groupid, 
         'es_ES', 
         'A�adir instrumentos' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Add Instruments' 

  ROLLBACK TRAN;