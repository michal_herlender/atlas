BEGIN TRAN;

IF NOT EXISTS (SELECT 1 FROM usergroup WHERE usergroup.name = 'Edit Instruments')
INSERT INTO usergroup (compdefault, description, name, systemdefault) VALUES (0,'Default Edit Instrument Group','Edit Instruments',0);

IF NOT EXISTS (SELECT 1 FROM usergroup WHERE usergroup.name = 'Add Calibrations')
INSERT INTO usergroup (compdefault, description, name, systemdefault) VALUES (0,'Default Add Calibration Group','Add Calibrations',0);

IF NOT EXISTS (SELECT 1 FROM usergroup WHERE usergroup.name = 'Validate Certificates')
INSERT INTO usergroup (compdefault, description, name, systemdefault) VALUES (0,'Default Validate Certificate Group','Validate Certificates',0);

IF NOT EXISTS (SELECT 1 FROM usergroup WHERE usergroup.name = 'Add Instruments')
INSERT INTO usergroup (compdefault, description, name, systemdefault) VALUES (0,'Default Add Instrument Group','Add Instruments',0);

ROLLBACK TRAN;