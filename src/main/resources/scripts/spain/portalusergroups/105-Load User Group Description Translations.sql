BEGIN TRAN;

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'en_GB'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Default Group'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'en_GB',
			'This group allows a user to view instruments belonging to them'
		FROM usergroup
		WHERE usergroup.name = 'Default Group'; 

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'fr_FR'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Default Group'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'fr_FR',
			'Ce groupe permet � un utilisateur de voir les instruments lui appartenant'
		FROM usergroup
		WHERE usergroup.name = 'Default Group';

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'es_ES'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Default Group'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'es_ES',
			'Este grupo permite al usuario ver los instrumentos que le pertenecen'
		FROM usergroup
		WHERE usergroup.name = 'Default Group';



IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'en_GB'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Address View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'en_GB',
			'This group allows a user to view instruments belonging to their address'
		FROM usergroup
		WHERE usergroup.name = 'Address View';

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'fr_FR'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Address View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'fr_FR',
			'Ce groupe permet � un utilisateur de voir les instruments attach�s � son adresse'
		FROM usergroup
		WHERE usergroup.name = 'Address View';

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'es_ES'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Address View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'es_ES',
			'Este grupo permite al usuario ver los instrumentos que pertenecen a su direcci�n'
		FROM usergroup
		WHERE usergroup.name = 'Address View';



IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'en_GB'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Subdivision View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'en_GB',
			'This group allows a user to view instruments belonging to their subdivision'
		FROM usergroup
		WHERE usergroup.name = 'Subdivision View'; 

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'fr_FR'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Subdivision View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'fr_FR',
			'Ce groupe permet � un utilisateur de voir les instruments de sa subdivision'
		FROM usergroup
		WHERE usergroup.name = 'Subdivision View';

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'es_ES'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Subdivision View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'es_ES',
			'Este grupo permite al usuario ver los instrumentos que pertenecen a su subdivisi�n'
		FROM usergroup
		WHERE usergroup.name = 'Subdivision View';



IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'en_GB'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Company View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'en_GB',
			'This group allows a user to view instruments belonging to their company'
		FROM usergroup
		WHERE usergroup.name = 'Company View'; 

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'fr_FR'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Company View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'fr_FR',
			'Ce groupe permet � un utilisateur de voir les instruments de sa soci�t�'
		FROM usergroup
		WHERE usergroup.name = 'Company View';

IF NOT EXISTS (	SELECT 1 
		FROM usergroupdescriptiontranslation
		WHERE usergroupdescriptiontranslation.locale = 'es_ES'
		AND usergroupdescriptiontranslation.groupid = (	SELECT usergroup.groupid
								FROM usergroup
								WHERE usergroup.name = 'Company View'))
	INSERT INTO usergroupdescriptiontranslation	
		SELECT 	usergroup.groupid,
			'es_ES',
			'Este grupo permite al usuario ver los instrumentos que pertenecen a su empresa'
		FROM usergroup
		WHERE usergroup.name = 'Company View';

        IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'en_GB' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Edit Instruments')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'en_GB', 
  'This group allows a user to edit/view instruments belonging to their company' 
  FROM   usergroup 
  WHERE  usergroup.NAME = 'Edit Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'fr_FR' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Edit Instruments')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'fr_FR', 
'Ce groupe permet � un utilisateur de voir/modifier les instruments appartenant � sa soci�t�' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Edit Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'es_ES' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Edit Instruments')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'es_ES', 
'Este grupo permite al usuario editar/ver los instrumentos que pertenecen a su empresa' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Edit Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'en_GB' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Add Calibrations')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'en_GB', 
'This group allows a user to add/edit/view instruments belonging to their company plus add calibrations and validate certificates' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Add Calibrations' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'fr_FR' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Add Calibrations')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'fr_FR', 
'Ce groupe permet � un utilisateur d�ajouter/modifier/voir les instruments de sa soci�t�, mais aussi d�enregistrer des calibrations et d�en valider leur(s) certificat(s)' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Add Calibrations' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'es_ES' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Add Calibrations')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'es_ES', 
'Este grupo permite al usuario a�adir/editar/ver los instrumentos que pertenecen a su empresa, a�adir calibraciones y validar certificados' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Add Calibrations' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'en_GB' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                              ug.NAME = 'Validate Certificates')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'en_GB', 
'This group allows a user to view instruments belonging to their company and validate certificates' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Validate Certificates' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'fr_FR' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                              ug.NAME = 'Validate Certificates')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'fr_FR', 
'Ce groupe permet � un utilisateur de voir les instruments de sa soci�t� et d�en valider les certificats' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Validate Certificates' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'es_ES' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                              ug.NAME = 'Validate Certificates')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'es_ES', 
'Este grupo permite al usuario ver los instrumentos que pertenecen a su empresa y validar los certificados' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Validate Certificates' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'en_GB' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Add Instruments')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'en_GB', 
'This group allows a user to add/edit/view instruments belonging to their company' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Add Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'fr_FR' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Add Instruments')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'fr_FR', 
'Ce groupe permet � un utilisateur d�ajouter/modifier/voir les instruments de sa soci�t�' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Add Instruments' 

IF NOT EXISTS (SELECT 1 
               FROM   usergroupdescriptiontranslation 
               WHERE  usergroupdescriptiontranslation.locale = 'es_ES' 
                      AND usergroupdescriptiontranslation.groupid = 
                          (SELECT ug.groupid 
                           FROM 
                          usergroup ug 
                                                                     WHERE 
                          ug.NAME = 'Add Instruments')) 
  INSERT INTO usergroupdescriptiontranslation 
  SELECT groupid, 
         'es_ES', 
'Este grupo permite al usuario a�adir/editar/ver los instrumentos que pertenecen a su empresa' 
FROM   usergroup 
WHERE  usergroup.NAME = 'Add Instruments' 

ROLLBACK TRAN;