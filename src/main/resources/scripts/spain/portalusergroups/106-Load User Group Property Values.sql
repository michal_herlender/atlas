BEGIN TRAN

-- Give groups the correct rights

-- Default Group : VIEW_LEVEL = CONTACT, ADD_INSTRUMENT = NONE, UPDATE_INSTRUMENT = NONE, VALIDATE_CERTIFICATE = NONE, CREATE_CALIBRATION = NONE
DELETE ugpv FROM usergrouppropertyvalue ugpv INNER JOIN usergroup ON usergroup.groupid = ugpv.groupid  WHERE usergroup.name = 'Default Group'
INSERT INTO usergrouppropertyvalue (groupid,property,value)
VALUES  ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Default Group'),'VIEW_LEVEL','CONTACT'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Default Group'),'ADD_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Default Group'),'UPDATE_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Default Group'),'VALIDATE_CERTIFICATE','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Default Group'),'CREATE_CALIBRATION','NONE')

-- Address View : VIEW_LEVEL = ADDRESS, ADD_INSTRUMENT = NONE, UPDATE_INSTRUMENT = NONE, VALIDATE_CERTIFICATE = NONE, CREATE_CALIBRATION = NONE
DELETE ugpv FROM usergrouppropertyvalue ugpv INNER JOIN usergroup ON usergroup.groupid = ugpv.groupid  WHERE usergroup.name = 'Address View'
INSERT INTO usergrouppropertyvalue (groupid,property,value)
VALUES  ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Address View'),'VIEW_LEVEL','ADDRESS'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Address View'),'ADD_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Address View'),'UPDATE_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Address View'),'VALIDATE_CERTIFICATE','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Address View'),'CREATE_CALIBRATION','NONE')

-- Subdivision View : VIEW_LEVEL = SUBDIV, ADD_INSTRUMENT = NONE, UPDATE_INSTRUMENT = NONE, VALIDATE_CERTIFICATE = NONE, CREATE_CALIBRATION = NONE
DELETE ugpv FROM usergrouppropertyvalue ugpv INNER JOIN usergroup ON usergroup.groupid = ugpv.groupid  WHERE usergroup.name = 'Subdivision View'
INSERT INTO usergrouppropertyvalue (groupid,property,value)
VALUES  ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Subdivision View'),'VIEW_LEVEL','SUBDIV'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Subdivision View'),'ADD_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Subdivision View'),'UPDATE_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Subdivision View'),'VALIDATE_CERTIFICATE','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Subdivision View'),'CREATE_CALIBRATION','NONE')

-- Company View : VIEW_LEVEL = COMPANY, ADD_INSTRUMENT = NONE, UPDATE_INSTRUMENT = NONE, VALIDATE_CERTIFICATE = NONE, CREATE_CALIBRATION = NONE
DELETE ugpv FROM usergrouppropertyvalue ugpv INNER JOIN usergroup ON usergroup.groupid = ugpv.groupid  WHERE usergroup.name = 'Company View'
INSERT INTO usergrouppropertyvalue (groupid,property,value)
VALUES  ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Company View'),'VIEW_LEVEL','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Company View'),'ADD_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Company View'),'UPDATE_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Company View'),'VALIDATE_CERTIFICATE','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Company View'),'CREATE_CALIBRATION','NONE')

-- Edit Instruments : VIEW_LEVEL = COMPANY, ADD_INSTRUMENT = NONE, UPDATE_INSTRUMENT = COMPANY, VALIDATE_CERTIFICATE = NONE, CREATE_CALIBRATION = NONE
DELETE ugpv FROM usergrouppropertyvalue ugpv INNER JOIN usergroup ON usergroup.groupid = ugpv.groupid  WHERE usergroup.name = 'Edit Instruments'
INSERT INTO usergrouppropertyvalue (groupid,property,value)
VALUES  ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Edit Instruments'),'VIEW_LEVEL','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Edit Instruments'),'ADD_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Edit Instruments'),'UPDATE_INSTRUMENT','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Edit Instruments'),'VALIDATE_CERTIFICATE','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Edit Instruments'),'CREATE_CALIBRATION','NONE')

-- Add Calibrations : VIEW_LEVEL = COMPANY, ADD_INSTRUMENT = COMPANY, UPDATE_INSTRUMENT = COMPANY, VALIDATE_CERTIFICATE = COMPANY, CREATE_CALIBRATION = COMPANY
DELETE ugpv FROM usergrouppropertyvalue ugpv INNER JOIN usergroup ON usergroup.groupid = ugpv.groupid  WHERE usergroup.name = 'Add Calibrations'
INSERT INTO usergrouppropertyvalue (groupid,property,value)
VALUES  ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Calibrations'),'VIEW_LEVEL','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Calibrations'),'ADD_INSTRUMENT','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Calibrations'),'UPDATE_INSTRUMENT','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Calibrations'),'VALIDATE_CERTIFICATE','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Calibrations'),'CREATE_CALIBRATION','COMPANY')

-- Validate Certificates : VIEW_LEVEL = COMPANY, ADD_INSTRUMENT = NONE, UPDATE_INSTRUMENT = NONE, VALIDATE_CERTIFICATE = COMPANY, CREATE_CALIBRATION = NONE
DELETE ugpv FROM usergrouppropertyvalue ugpv INNER JOIN usergroup ON usergroup.groupid = ugpv.groupid  WHERE usergroup.name = 'Validate Certificates'
INSERT INTO usergrouppropertyvalue (groupid,property,value)
VALUES  ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Validate Certificates'),'VIEW_LEVEL','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Validate Certificates'),'ADD_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Validate Certificates'),'UPDATE_INSTRUMENT','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Validate Certificates'),'VALIDATE_CERTIFICATE','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Validate Certificates'),'CREATE_CALIBRATION','NONE')

-- Add Instruments : view = company, add inst = yes, update inst = yes, validate cert = company, create cal = no
-- Add Instruments : VIEW_LEVEL = COMPANY, ADD_INSTRUMENT = COMPANY, UPDATE_INSTRUMENT = COMPANY, VALIDATE_CERTIFICATE = NONE, CREATE_CALIBRATION = NONE
DELETE ugpv FROM usergrouppropertyvalue ugpv INNER JOIN usergroup ON usergroup.groupid = ugpv.groupid  WHERE usergroup.name = 'Add Instruments'
INSERT INTO usergrouppropertyvalue (groupid,property,value)
VALUES  ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Instruments'),'VIEW_LEVEL','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Instruments'),'ADD_INSTRUMENT','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Instruments'),'UPDATE_INSTRUMENT','COMPANY'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Instruments'),'VALIDATE_CERTIFICATE','NONE'),
        ((SELECT groupid FROM usergroup WHERE usergroup.name = 'Add Instruments'),'CREATE_CALIBRATION','NONE')

ROLLBACK TRAN
