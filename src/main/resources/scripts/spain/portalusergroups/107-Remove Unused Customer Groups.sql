BEGIN TRAN;

DELETE ugv FROM usergroupvalues ugv
WHERE ugv.groupid IN(
	SELECT  DISTINCT usergroup.groupid


  FROM usergroup
  INNER JOIN usergroupvalues ON usergroupvalues.groupid = usergroup.groupid
  INNER JOIN usergrouptypevalue ON usergrouptypevalue.valueid = usergroupvalues.valueid
  INNER JOIN usergrouptype ON usergrouptype.propertyid = usergrouptypevalue.propertyid
  LEFT JOIN contact ON contact.groupid = usergroup.groupid
  WHERE coid >0

  GROUP BY usergroup.groupid
  HAVING COUNT(contact.personid) = 0);



DELETE ug FROM usergroup ug
WHERE ug.groupid IN(SELECT  DISTINCT usergroup.groupid 
FROM usergroup
  LEFT JOIN usergroupvalues ON usergroupvalues.groupid = usergroup.groupid
  WHERE usergroup.coid > 0 
  AND valueid IS NULL)

ROLLBACK TRAN;