BEGIN TRAN;

UPDATE contact 
	SET contact.groupid = (SELECT u1.groupid FROM usergroup u1 WHERE u1.name = 'Default Group')
	WHERE contact.groupid = (SELECT u2.groupid FROM usergroup u2 WHERE u2.name = 'Basic Personal');

UPDATE usergroupvalues 
	SET usergroupvalues.groupid = (SELECT u1.groupid FROM usergroup u1 WHERE u1.name = 'Default Group')
	WHERE usergroupvalues.groupid = (SELECT u2.groupid FROM usergroup u2 WHERE u2.name = 'Basic Personal');

DELETE FROM usergroup WHERE usergroup.name = 'Basic Personal';

UPDATE usergroup SET usergroup.description = 'This group allows a user to view instruments belonging to them' 
WHERE usergroup.name = 'Default Group';

ROLLBACK TRAN;