-- Utility to fix recall details and contacts, for contacts that don't have any default address
-- Note ROLLBACK TRAN at end, test first, and adapt as needed for other recall dates if using in future
-- After running utility (as written) the intention is to then be able to successfully run recall, again.
-- Galen Beck 2018-08-01

USE [atlas]
GO

BEGIN TRAN

PRINT 'Cancelling unsent 2019-07-20 recalls'
GO

UPDATE [dbo].[recalldetail]
   SET [recallsendstatus] = 'CANCELLED'
 FROM recalldetail
 JOIN recall on recalldetail.recallid = recall.id
 WHERE [recallsendstatus] = 'TO_SEND' AND recall.recallno = '2019-07-20'
GO

PRINT 'Fixing default addresses on contacts using subdivs'
GO

UPDATE [dbo].contact 
   SET defaddress = (SELECT TOP 1 subdiv.defaddrid FROM subdiv where subdiv.subdivid = contact.subdivid)
 WHERE defaddress IS NULL
GO

PRINT 'Fixing addresses on recalldetail using fixed contacts'
GO

UPDATE [dbo].recalldetail 
   SET recalldetail.addrid = (SELECT contact.defaddress FROM contact where contact.personid = recalldetail.personid)
 WHERE recalldetail.addrid IS NULL
GO

PRINT 'Displaying remaining contacts with null default addresses'
GO

SELECT contact.[personid]
      ,contact.[active]
      ,contact.[createdate]
      ,contact.[email]
      ,contact.[firstname]
      ,contact.[lastname]
      ,contact.[defaddress]
      ,contact.[lastModified]
	  ,(SELECT COUNT(1) FROM address where address.subdivid = subdiv.subdivid) as addrcount
	  ,subdiv.defaddrid
	  ,subdiv.subname
	  ,subdiv.subdivid
	  ,subdiv.lastModified
	  ,company.coname
	  ,company.coid
  FROM [dbo].[contact] 
  LEFT JOIN subdiv on subdiv.subdivid = contact.subdivid
  LEFT JOIN company on subdiv.coid = company.coid
  WHERE defaddress IS NULL
GO

PRINT 'Displaying remaining recalldetails for 2018-08-01 with null addresses'
GO

SELECT [recalldetail].[id]
      ,[error]
      ,[ignoredallonjob]
      ,[ignoredonsetting]
      ,[numberofitems]
      ,[pathtofile]
      ,[recallsendtype]
      ,[recalltype]
      ,[addrid]
      ,recalldetail.[personid]
      ,[recallid]
      ,[recallerror]
      ,[recallsendstatus]
	  ,recall.recallno
	  ,contact.firstname
	  ,contact.lastname
	  ,subdiv.subdivid
	  ,subdiv.subname
	  ,company.coid
	  ,company.coname
  FROM [dbo].[recalldetail]
  LEFT JOIN recall on recalldetail.recallid = recall.id
  LEFT JOIN contact on contact.personid = recalldetail.personid
  LEFT JOIN subdiv on subdiv.subdivid = contact.subdivid
  LEFT JOIN company on company.coid = subdiv.coid
  WHERE recallsendstatus = 'TO_SEND' AND recalldetail.addrid is null AND recall.recallno = '2019-08-01';
GO

-- Can comment out above date, if needed

PRINT 'Cancelling unsent 2019-08-01 recalls which would still fail'
GO

UPDATE [dbo].[recalldetail]
   SET [recallsendstatus] = 'CANCELLED'
 FROM recalldetail
 JOIN recall on recalldetail.recallid = recall.id
 WHERE [recallsendstatus] = 'TO_SEND' AND recall.recallno = '2019-08-01' AND recalldetail.addrid IS NULL
GO

-- Can comment out above date, if needed

ROLLBACK TRAN