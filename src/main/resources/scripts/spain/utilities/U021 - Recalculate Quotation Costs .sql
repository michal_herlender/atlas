-- Utility script to recalculate the totalCost, vatValue, and finalCost on all quotations
-- based on the quotation items contained in the quotation; updates totalCost 
-- Developed by Soufiane Amankachi 2020-07-24
-- Note, this takes ~5 minutes to run as it checks all quotations

USE [atlas]
GO

BEGIN TRAN


Declare @QuotId int
Declare @TotalCost numeric (10,2)
Declare @VatValue numeric(10,2)
Declare @VatRate numeric(10,2)
Declare @QuoteItemCost numeric(10,2)

SELECT * INTO #Temp FROM quotation SET NOCOUNT ON

WHILE (SELECT Count(*) FROM #Temp ) > 0 
	
	BEGIN
		Select Top 1 @QuotId = Id From #Temp SET NOCOUNT ON
			SET @TotalCost = null SET NOCOUNT ON
			SET @TotalCost= (select totalcost from #Temp Where Id = @QuotId) SET NOCOUNT ON
			SET @VatRate = null SET NOCOUNT ON
			SET @VatRate= (select vatrate from #Temp Where Id = @QuotId) SET NOCOUNT ON
			SET @VatValue = null SET NOCOUNT ON
	
				SET @QuoteItemCost = (select sum(finalcost) from quotationitem qit where qit.quoteid=@QuotId) SET NOCOUNT ON
				SET @VatValue = ((@VatRate*@QuoteItemCost)/100)

			if(@TotalCost != @QuoteItemCost)
				 BEGIN	
						 print 'Quotation ID  '+ CAST(@QuotId AS VARCHAR)
						 print 'Before Calc  '+ CAST(@TotalCost AS VARCHAR)
						 print  'Total is '+ CAST(@QuoteItemCost AS VARCHAR)
						 print  'VatRate '+ CAST(@VatRate AS VARCHAR)
						 print  'VatValue '+ CAST(@VatValue AS VARCHAR)
						 print '-------------------------------------------'

						 UPDATE quotation SET [totalcost]=@QuoteItemCost ,[vatvalue] = @VatValue , [finalcost] = @QuoteItemCost+@VatValue where id=@QuotId

				 END
			SET @QuoteItemCost = null SET NOCOUNT ON
		Delete #Temp Where Id = @QuotId SET NOCOUNT ON

	END

Drop Table #Temp SET NOCOUNT ON

COMMIT TRAN

