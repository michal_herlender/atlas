-- Utility script U008

-- This script deletes duplicate certificatevalidation records for the same certificate ID
--  (retaining the newest record for each)
-- See script U007 for a way to identify the duplicates
-- Note ROLLBACK TRAN; verify the row count / records to be deleted with U007,
--  and then use this script with COMMIT TRAN

-- Galen Beck - 2019-05-14

USE [tempdb]
GO

-- Clean up temporary table, if it exists from a previous failure of this script
IF OBJECT_ID('#duplicate_certval') IS NOT NULL
	DROP TABLE #duplicate_certval
GO

USE [atlas]
GO

BEGIN TRAN

-- Create a temporary table summarizing count of validation records for each certid
SELECT certificate.[certid] as cert_id,
(SELECT COUNT(*) FROM certificatevalidation where certificatevalidation.certid = certificate.certid) as val_count,
(SELECT TOP 1 certificatevalidation.id FROM certificatevalidation where certificatevalidation.certid = certificate.certid ORDER BY certificatevalidation.id DESC) as max_certval_id
INTO #duplicate_certval
FROM [dbo].[certificate]
GO

-- Retain just the records that indicate duplicates (0, 1 = no duplicates)
DELETE FROM #duplicate_certval WHERE val_count < 2
GO


-- This performs the deletion (the last statement count is the number of deletions)
DELETE FROM [dbo].[certificatevalidation] 
WHERE certid IN 
	(SELECT cert_id FROM #duplicate_certval
		WHERE val_count > 1
	)
AND id NOT IN
	(SELECT max_certval_id FROM #duplicate_certval 
		WHERE cert_id = certid
	)
GO

ROLLBACK TRAN

-- Clear up [tempdb] in case connection left open in SSMS

USE [tempdb]
GO

IF OBJECT_ID('#duplicate_certval') IS NOT NULL
	DROP TABLE #duplicate_certval
	GO
GO