BEGIN TRAN

UPDATE contact
SET title = ''
WHERE title IS NULL

UPDATE contact
SET fax = ''
WHERE fax IS NULL

UPDATE contact
SET mobile = ''
WHERE mobile IS NULL

UPDATE contact
SET position = ''
WHERE position IS NULL

UPDATE contact
SET telephone = ''
WHERE telephone IS NULL

UPDATE contact
SET firstname = '/'
WHERE firstname = ''

COMMIT TRAN