-- Inserts current US sales tax rates
-- For test only, as prod setup will need different configuration / new entities

USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[vatrate]
([vatcode],[description],[rate],[type],[countryid],[lastModified],[lastModifiedBy])
VALUES
('O','MI - Hartland (Detroit)',6.000,1,185,'2019-07-05',17280),
('P','OH - Cleveland',8.000,1,185,'2019-07-05',17280),
('Q','IL - Arlington Heights (Chicago)',10.000,1,185,'2019-07-05',17280),
('R','TX - Stafford (Houston)',8.250,1,185,'2019-07-05',17280),
('S','TX - Irving (Dallas)',8.250,1,185,'2019-07-05',17280),
('T','AL - Mobile',10.000,1,185,'2019-07-05',17280),
('U','CA - Santa Clara',9.000,1,185,'2019-07-05',17280),
('V','GA - Alpharetta (Atlanta)',7.750,1,185,'2019-07-05',17280),
('W','NC - Cornelius (Charlotte)',7.250,1,185,'2019-07-05',17280),
('X','NJ - Boonton (Newark)',6.625,1,185,'2019-07-05',17280),
('Y','MD - Towson (Baltimore)',6.000,1,185,'2019-07-05',17280),
('Z','WI - Milwaukee',5.600,1,185,'2019-07-05',17280)

COMMIT TRAN