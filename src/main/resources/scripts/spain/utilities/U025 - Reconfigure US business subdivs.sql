-- Script to delete some incorrect US subdivs on US production server
-- From CD 2021-01-19
-- Delete subdivision Hartland, Philadelphia, Houston and Greer (we�re waiting an answer from Diane or Bill) in Trescal INC
-- (GB - Holding off on Greer for now)
-- Rename Subdivision Detroit in Hartland (I�ve an error message when I want do it)
-- Note intentional ROLLBACK at end, change to COMMIT when tested on each machine

USE [atlas]
GO

BEGIN TRAN

DECLARE @coid_TrescalUS INT;

DECLARE @subdiv_Detroit INT;
--DECLARE @subdiv_Greer INT;
DECLARE @subdiv_Hartland INT;
DECLARE @subdiv_Houston INT;
DECLARE @subdiv_Philadelphia INT;

SELECT @coid_TrescalUS = coid from company where coname = 'TRESCAL INC.' AND corole = 5;

SELECT @subdiv_Detroit = subdivid from subdiv WHERE coid=@coid_TrescalUS AND subname = 'Detroit';
--SELECT @subdiv_Greer = subdivid from subdiv WHERE coid=@coid_TrescalUS AND subname = 'Greer';
SELECT @subdiv_Hartland = subdivid from subdiv WHERE coid=@coid_TrescalUS AND subname = 'Hartland';
SELECT @subdiv_Houston = subdivid from subdiv WHERE coid=@coid_TrescalUS AND subname = 'Houston';
SELECT @subdiv_Philadelphia = subdivid from subdiv WHERE coid=@coid_TrescalUS AND subname = 'Philadelphia';

PRINT @coid_TrescalUS;
PRINT @subdiv_Detroit;
--PRINT @subdiv_Greer;
PRINT @subdiv_Hartland;
PRINT @subdiv_Houston;
PRINT @subdiv_Philadelphia

-- Constraints for deleting subdivs
DELETE FROM userrole 
	WHERE orgid IN (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);
DELETE FROM businesssubdivisionsettings 
	WHERE subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);

-- Delete contacts / users
UPDATE company SET defsubdivid = @subdiv_Detroit 
	WHERE defsubdivid = @subdiv_Hartland;
UPDATE subdiv SET defpersonid = NULL 
	WHERE subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);
DELETE up FROM userpreferences up 
	JOIN contact con ON con.personid = up.contactid 
	WHERE con.subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);
DELETE u FROM users u 
	JOIN contact con ON con.personid = u.personid
	WHERE con.subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);
DELETE FROM contact
	WHERE subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);

-- Delete addresses
DELETE FROM addresssettingsforallocatedsubdiv
	WHERE orgid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);
DELETE settings FROM addresssettingsforallocatedsubdiv settings
	JOIN address a on a.addrid = settings.addressid
	WHERE a.subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);
DELETE settings FROM addresssettingsforallocatedcompany settings
	JOIN address a on a.addrid = settings.addressid
	WHERE a.subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);

UPDATE subdiv SET defaddrid = null
	WHERE subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);
DELETE afn FROM addressfunction afn
	JOIN address a on a.addrid = afn.addressid
	WHERE a.subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);
DELETE FROM address 
	WHERE subdivid in (@subdiv_Hartland, @subdiv_Houston, @subdiv_Philadelphia);

--DELETE FROM subdiv WHERE subdivid = @subdiv_Greer;
DELETE FROM subdiv WHERE subdivid = @subdiv_Hartland;
DELETE FROM subdiv WHERE subdivid = @subdiv_Houston;
DELETE FROM subdiv WHERE subdivid = @subdiv_Philadelphia;

UPDATE subdiv SET subdivcode='HAR', subname='Hartland' WHERE subdivid = @subdiv_Detroit;

ROLLBACK TRAN