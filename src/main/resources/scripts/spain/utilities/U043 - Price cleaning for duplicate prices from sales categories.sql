-- Utility script to remove all duplicate standalone prices with sales category prices
-- Author : Laila MADANI - 2021-09-21
-- Note ROLLBACK at end, change to COMMIT after testing

USE [atlas]

BEGIN TRAN

	DELETE FROM catalogprice WHERE id IN 
		(SELECT standalone.id FROM dbo.catalogprice standalone
			JOIN dbo.instmodel model ON  model.modelid = standalone.modelid
			JOIN dbo.salescategory sales ON sales.id  = model.salescategoryid
			WHERE model.modeltypeid = 1 
			-- same fixed price
			AND standalone.fixedPrice IN (SELECT fixedPrice FROM dbo.catalogprice salesCategory 
			JOIN dbo.instmodel model ON  model.modelid = salesCategory.modelid
			JOIN dbo.salescategory sale ON sale.id  = model.salescategoryid
			WHERE model.modeltypeid =  6 AND sales.id = sale.id)
			-- same business company
			AND standalone.orgid IN (SELECT orgid FROM dbo.catalogprice salesCategory 
			JOIN dbo.instmodel model ON  model.modelid = salesCategory.modelid
			JOIN dbo.salescategory sale ON sale.id  = model.salescategoryid
			WHERE model.modeltypeid =  6 AND sales.id = sale.id)
			-- same calibration type
			AND standalone.caltypeid IN (SELECT caltypeid FROM dbo.catalogprice salesCategory 
			JOIN dbo.instmodel model ON  model.modelid = salesCategory.modelid
			JOIN dbo.salescategory sale ON sale.id  = model.salescategoryid
			WHERE model.modeltypeid =  6 AND sales.id = sale.id)
		)
		
ROLLBACK TRAN