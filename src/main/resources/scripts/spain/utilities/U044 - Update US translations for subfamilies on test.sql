/**
 This utility script updates the description/subfamily translations for US English.
 This has been done on the US and EMEA production (~ July 2021) but not on all the US test or dev systems.
 These differences were extracted from US production on 2021-09-11 (88 descriptions)
 After running this script, the instrument model translation update tool (admin area screen) could be run
 to regenerate US instrument model translations, if desired.
*/

USE [atlas]
GO

BEGIN TRAN

UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ANALYZER OTHER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 42
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ENERGY ANALYZER OTHER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 145
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'THREAD RING GAUGE TAPERED' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 354
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'THREAD RING GAUGE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 355
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'RING GAUGE PLAIN' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 357
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'RING GAUGE SETTING PLAIN' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 358
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'RING GAUGE TAPERED' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 356
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'HORIZONTAL UNIDIRECTIONAL MEASURING INSTRUMENT' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 360
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ANGLE BLOCK SET' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 406
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'THREAD MICROMETER STANDARD' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 363
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ANGLE BLOCK' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 407
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'SURFACE PLATE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 398
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'THREAD WIRE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 432
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'STRAIGHTEDGE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 434
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'THREAD PLUG GAUGE TAPERED' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 447
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'THREAD PLUG GAUGE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 448
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'PLUG GAUGE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 451
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'VECTOR NETWORK ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 136
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'NOISE FIGURE ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 214
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'LOGIC ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 147
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'PANEL METER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 228
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'LOGIC ANALYZER PROBE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 149
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ROTAMETER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 100
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ANALOG PRESSURE GAUGE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 473
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'COLOR ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 481
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'STANDARD COLOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 483
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'DEADWEIGHT TESTER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 597
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'LEAK STANDARD' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 467
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'IMPACT SENSOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 606
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'CMM Probe' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 609
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'IMPACT GENERATOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 22
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ARINC ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 172
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'LF NETWORK ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 134
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'MAINS DISTURBANCE ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 144
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'SEMICONDUCTOR ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 210
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'VIDEO ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 297
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'SIGNAL CONDITIONER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 130
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'HF POWER METER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 308
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'VISCOSITY CUP' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 118
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'DISK' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 15
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'VIBRATION TABLE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 25
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'GAS ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 43
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'FLOWMETER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 105
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'PHASE NOISE ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 132
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'SCALAR NETWORK ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 135
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'SPECTRUM ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 138
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'PROBE FOR SIGNAL ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 142
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'AUDIO ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 162
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'PROTOCOL ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 173
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'SIMULATOR ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 174
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'Transmission and Noise Measuring Set' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 216
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'RADIONAVIGATION ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 252
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'Avionics Simulator' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 253
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'BIT ERROR RATE ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 266
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'TRANSMISSION ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 267
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'PDH ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 268
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'SDH ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 269
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'DIRECTIONAL COUPLER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 279
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'TORQUE TRANSDUCER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 312
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'TORQUE TRANSDUCER SYSTEM' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 313
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'TORQUE GUN' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 319
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'HARDNESS TESTER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 322
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'LOAD CELL' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 324
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'LOAD CELL SYSTEM' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 325
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'FORCE GAUGE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 326
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'CRIMPER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 333
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'MICROMETER STANDARD FLAT ENDED' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 361
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'MICROMETER STANDARD SPERICAL ENDED' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 362
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'STEP BLOCK' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 368
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ANGLE PLATE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 382
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'OUTSIDE MICROMETER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 422
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'BORE GAUGE TWO POINT' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 423
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'BORE GAUGE THREE POINT' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 426
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'TAPE MEASURE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 441
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'TESTING SIEVE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 445
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'TIRE INFLATOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 472
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'OPTICAL SPECTRUM ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 498
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'IR THERMOMETER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 551
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'RESISTOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 170
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'TORQUE CALIBRATOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 645
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ANALOG MODULATION ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 133
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'SIGNAL ANALYZER OTHER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 137
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'OVEN' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 92
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'CHEMISTRY ANALYZER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 641
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'INDUCTOR BOX' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 671
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'DIAL INDICATOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 688
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'DIGITAL INDICATOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 689
UPDATE [dbo].[descriptiontranslation] SET [translation] = 'DUROMETER' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 725

/* Also insert missing translations if not present */

UPDATE [dbo].[descriptiontranslation] SET [translation] = 'LAMINAR FLOW HOOD' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 62
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[descriptiontranslation]
			   ([descriptionid],[locale],[translation])
		 VALUES
			   ((SELECT descriptionid from description where tmlid = 62), 'en_US', 'LAMINAR FLOW HOOD');
END

UPDATE [dbo].[descriptiontranslation] SET [translation] = 'ISOLATOR' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 731
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[descriptiontranslation]
			   ([descriptionid],[locale],[translation])
		 VALUES
			   ((SELECT descriptionid from description where tmlid = 731), 'en_US', 'ISOLATOR');
END

UPDATE [dbo].[descriptiontranslation] SET [translation] = 'HVAC SYSTEM' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 732
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[descriptiontranslation]
			   ([descriptionid],[locale],[translation])
		 VALUES
			   ((SELECT descriptionid from description where tmlid = 732), 'en_US', 'HVAC SYSTEM');
END

UPDATE [dbo].[descriptiontranslation] SET [translation] = 'PHARMACEUTICAL UTILITIES' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 733
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[descriptiontranslation]
			   ([descriptionid],[locale],[translation])
		 VALUES
			   ((SELECT descriptionid from description where tmlid = 733), 'en_US', 'PHARMACEUTICAL UTILITIES');
END

UPDATE [dbo].[descriptiontranslation] SET [translation] = 'IMPULSION MODULE' FROM [descriptiontranslation] dt INNER JOIN description d on dt.descriptionid = d.descriptionid WHERE dt.[locale] = 'en_US' AND d.tmlid = 734
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[descriptiontranslation]
			   ([descriptionid],[locale],[translation])
		 VALUES
			   ((SELECT descriptionid from description where tmlid = 734), 'en_US', 'IMPULSION MODULE');
END

/* Also insert missing units and characteristicdescription if not present (issue on with original backup only, if TML sync hasn't been running)
   This is for the SURFACE PLATE (units) and ELECTRONIC HEIGHT GAGE SETTING MASTER (length characteristic) */

SELECT [id] ,[description] ,[name] ,[symbol] ,[tmlid] ,[ShortName] ,[active] ,[lastModified] ,[lastModifiedBy] FROM [dbo].[uom] where tmlid = 581;
IF @@ROWCOUNT = 0
BEGIN
INSERT INTO [dbo].[uom]
           ([name] ,[symbol] ,[tmlid] ,[ShortName] ,[active] ,[lastModified] ,[lastModifiedBy])
     VALUES
           ('Square feet' ,'ft�' ,581, 'ft�', 1, sysdatetime(), 8740);
END

SELECT [characteristicdescriptionid] ,[active] ,[characteristicType] ,[metadata] ,[name] ,[orderno] ,[required] ,[tmlid] ,[descriptionid]
      ,[uomid] ,[partofkey] ,[shortname] ,[internalname],[lastModified] ,[lastModifiedBy]
  FROM [dbo].[characteristicdescription] where tmlid=36343;
GO
IF @@ROWCOUNT = 0
BEGIN
INSERT INTO [dbo].[characteristicdescription]
           ([active] ,[characteristicType] ,[metadata] ,[name] ,[orderno] ,[required] ,[tmlid], [descriptionid]
           ,[uomid] ,[partofkey] ,[shortname] ,[internalname] ,[lastModified] ,[lastModifiedBy])
     VALUES
           (1 ,14 ,'' ,'Length' ,2 ,0 ,36343 ,67 ,
		   186 ,0 ,'Length' ,'Float01', SYSDATETIME() ,8740)
DECLARE @cdid INT = SCOPE_IDENTITY()
INSERT INTO [dbo].[characteristicdescriptiontranslation]
           ([characteristicdescriptionid] ,[locale] ,[translation])
     VALUES
           (@cdid, 'en_GB', 'Length'),
           (@cdid, 'es_ES', 'Largo'),
           (@cdid, 'fr_FR', 'Longueur');
END

COMMIT TRAN
