DECLARE @subdiv_BAL int;
DECLARE @subdiv_CHI int;
DECLARE @subdiv_CLE int;

SELECT @subdiv_BAL = subdivid FROM subdiv WHERE coid=6682 and subdivcode='BAL';
SELECT @subdiv_CHI = subdivid FROM subdiv WHERE coid=6682 and subdivcode='CHI';
SELECT @subdiv_CLE = subdivid FROM subdiv WHERE coid=6682 and subdivcode='CLE';

PRINT '@subdiv_BAL: '+cast(@subdiv_BAL as varchar)
PRINT '@subdiv_CHI: '+cast(@subdiv_CHI as varchar)
PRINT '@subdiv_CLE: '+cast(@subdiv_CLE as varchar)

SELECT comp.coname as coname
	  ,comp.formerid as c_formerid
	  ,comp.fiscalidentifier as c_fiscalid
	  ,comp.legaladdressid as c_legaladdrid
	  ,comp.corole as c_corole
	  ,(SELECT COUNT(1) FROM job 
	    INNER JOIN contact ON job.personid = contact.personid 
		INNER JOIN subdiv on contact.subdivid = subdiv.subdivid 
		WHERE subdiv.coid = comp.coid) 
		AS c_jobs
	  ,(SELECT COUNT(1) FROM instrument 
	    INNER JOIN contact ON instrument.personid = contact.personid 
		INNER JOIN subdiv on contact.subdivid = subdiv.subdivid 
		WHERE subdiv.coid = comp.coid) 
		AS c_insts
	  ,sub.[subdivid]
      ,sub.[active]
      ,sub.[createdate]
      ,sub.[subname]
      ,sub.[coid]
      ,sub.[defaddrid]
      ,sub.[siretNumber] as s_fiscalid
      ,sub.[formerid] as s_fiscalid
	  ,(SELECT COUNT(1) 
	    FROM job 
		LEFT JOIN contact ON job.personid = contact.personid 
		WHERE contact.subdivid = sub.subdivid) as s_jobs
	  ,(SELECT COUNT(1) FROM instrument 
	    LEFT JOIN contact ON instrument.personid = contact.personid 
		WHERE contact.subdivid = sub.subdivid) as s_insts
	  ,(SELECT contact.firstname + ' ' + contact.lastname
		FROM subdivsettingsforallocatedsubdiv settings
		INNER JOIN contact ON settings.businesscontactid=contact.personid
		WHERE settings.orgid = @subdiv_BAL 
		AND settings.subdivid = sub.subdivid) as csr_bal
	  ,(SELECT contact.firstname+' '+contact.lastname 
		FROM subdivsettingsforallocatedsubdiv settings
		INNER JOIN contact ON settings.businesscontactid=contact.personid
		WHERE settings.orgid = @subdiv_CHI 
		AND settings.subdivid = sub.subdivid) as csr_chi
	  ,(SELECT contact.firstname+' '+contact.lastname 
		FROM subdivsettingsforallocatedsubdiv settings
		INNER JOIN contact ON settings.businesscontactid=contact.personid
		WHERE settings.orgid = @subdiv_CLE
		AND settings.subdivid = sub.subdivid) as csr_cle
	  ,def_addr.addrid as d_addr_id
	  ,def_addr.addr1 as d_addr_1
	  ,def_addr.addr2 as d_addr_2
	  ,def_addr.addr3 as d_addr_3
	  ,def_addr.town as d_addr_city
	  ,def_addr.county as d_addr_state
	  ,def_addr.postcode as d_addr_zip
	  ,def_addr.country as d_addr_country
	  ,(CASE WHEN leg_addr.subdivid = sub.subdivid THEN leg_addr.addrid ELSE '' END) as l_addr_id
	  ,(CASE WHEN leg_addr.subdivid = sub.subdivid THEN leg_addr.addr1 ELSE '' END) as l_addr_1
	  ,(CASE WHEN leg_addr.subdivid = sub.subdivid THEN leg_addr.addr2 ELSE '' END) as l_addr_2
	  ,(CASE WHEN leg_addr.subdivid = sub.subdivid THEN leg_addr.addr3 ELSE '' END) as l_addr_3
	  ,(CASE WHEN leg_addr.subdivid = sub.subdivid THEN leg_addr.town ELSE '' END) as l_addr_city
	  ,(CASE WHEN leg_addr.subdivid = sub.subdivid THEN leg_addr.county ELSE '' END) as l_addr_state
	  ,(CASE WHEN leg_addr.subdivid = sub.subdivid THEN leg_addr.postcode ELSE '' END) as l_addr_zip
	  ,(CASE WHEN leg_addr.subdivid = sub.subdivid THEN leg_addr.country ELSE '' END) as l_addr_country
	  ,(CASE WHEN leg_addr.subdivid <> sub.subdivid THEN '' WHEN leg_addr.validator = 1 THEN 'Y' ELSE 'N' END) as l_addr_validated
  FROM [dbo].[subdiv] sub
  INNER JOIN [company] comp on sub.coid = comp.coid
  LEFT JOIN [address] def_addr on sub.defaddrid = def_addr.addrid
  LEFT JOIN [address] leg_addr on comp.legaladdressid = leg_addr.addrid
  WHERE comp.corole = 1
  ORDER BY comp.corole asc, comp.coname asc, sub.formerid asc
GO


