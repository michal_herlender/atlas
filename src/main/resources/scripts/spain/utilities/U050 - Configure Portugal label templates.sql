-- Inserts 4 new templates for Portugal and changes configuration

BEGIN TRAN

DECLARE @coname varchar(100) = 'TRESCAL PORTUGAL Lda';
DECLARE @coid int;
SELECT @coid = coid from company where coname = @coname and corole = 5;

IF @coid IS NULL
BEGIN
	PRINT 'No business company found for name '+@coname
END
ELSE
BEGIN 
	PRINT @coid;
	-- Delete any existing settings for company
	DELETE FROM [dbo].[alligatorlabeltemplaterule] WHERE coid = @coid;
	-- 
	DECLARE @descriptionRegular varchar(100) = 'Cal_Standard';
	DECLARE @descriptionSmall varchar(100) = 'Cal_Standard_Small';
	DECLARE @templateXmlRegular varchar(max);
	DECLARE @templateXmlSmall varchar(max);

	SELECT @templateXmlRegular = templatexml FROM alligatorlabeltemplate WHERE description=@descriptionRegular
	SELECT @templateXmlSmall = templatexml FROM alligatorlabeltemplate WHERE description=@descriptionSmall

	-- 
	SET IDENTITY_INSERT [alligatorlabeltemplate] ON

	INSERT INTO [dbo].[alligatorlabeltemplate]
           ([id]
		   ,[description]
           ,[templatexml]
           ,[templatetype]
           ,[parameterstyle])
     VALUES
	       (9, @descriptionRegular, @templateXmlRegular, 'ACCREDITED', 'AUTOMATIC')
		   ,(10, @descriptionSmall, @templateXmlSmall, 'ACCREDITED_SMALL', 'AUTOMATIC')
		   ,(11, @descriptionRegular, @templateXmlRegular, 'STANDARD', 'AUTOMATIC')
		   ,(12, @descriptionSmall, @templateXmlSmall, 'STANDARD_SMALL', 'AUTOMATIC')

	SET IDENTITY_INSERT [alligatorlabeltemplate] OFF

	INSERT INTO [dbo].[alligatorlabeltemplaterule]
			   ([coid]
			   ,[templateid])
		 VALUES
			   (@coid, 9),
			   (@coid, 10),
			   (@coid, 11),
			   (@coid, 12);
END

ROLLBACK TRAN
