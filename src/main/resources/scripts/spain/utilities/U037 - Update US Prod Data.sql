-- Script to fix various issues with US production data
-- after migration and before import into Calypso / TAM
-- Updates address line2/line3/county, and other data
-- Note ROLLBACK at end, test before commit

USE [atlas]
GO

BEGIN TRAN

-- Update addresses with bad data

UPDATE [dbo].[address]
   SET [addr2] = ''
 WHERE addr2 like 'Empty_Address%'
GO

UPDATE [dbo].[address]
   SET [addr2] = ''
 WHERE addr2 like 'Unknown_Address%'
GO

-- Update US addresses with bad states (some remain)

UPDATE [dbo].[address] SET [county] = 'WY' WHERE [county] = 'Wy' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'WV' WHERE [county] = 'Wv' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'WI' WHERE [county] = 'Wisconsin' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'WI' WHERE [county] = 'Wi' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'WA' WHERE [county] = 'Wa' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'VT' WHERE [county] = 'Vt' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'VA' WHERE [county] = 'Virginia' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'VA' WHERE [county] = 'Va' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'UT' WHERE [county] = 'Ut' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'TX' WHERE [county] = 'Tx' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'TN' WHERE [county] = 'Tn' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'TX' WHERE [county] = 'Texas' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'SC' WHERE [county] = 'South Carolina' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'SC' WHERE [county] = 'Sc' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'RI' WHERE [county] = 'Ri' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'PA' WHERE [county] = 'Pennsylvania' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'PA' WHERE [county] = 'Pa' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'OR' WHERE [county] = 'Or' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'OK' WHERE [county] = 'Ok' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'OH' WHERE [county] = 'Ohio' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'OH' WHERE [county] = 'Oh' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NY' WHERE [county] = 'Ny' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NV' WHERE [county] = 'Nv' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NC' WHERE [county] = 'North Carolina' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NM' WHERE [county] = 'Nm' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NJ' WHERE [county] = 'Nj' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NH' WHERE [county] = 'Nh' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NY' WHERE [county] = 'New York' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NJ' WHERE [county] = 'New Jersey' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NE' WHERE [county] = 'Ne' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'ND' WHERE [county] = 'Nd' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'NC' WHERE [county] = 'Nc' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MT' WHERE [county] = 'Mt' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MS' WHERE [county] = 'Ms' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MO' WHERE [county] = 'Mo' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MN' WHERE [county] = 'Mn' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MI' WHERE [county] = 'Michigan' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MI' WHERE [county] = 'Mi' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'ME' WHERE [county] = 'Me' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MD' WHERE [county] = 'Md' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MA' WHERE [county] = 'Massachusetts' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MD' WHERE [county] = 'Maryland' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'MA' WHERE [county] = 'Ma' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'LA' WHERE [county] = 'La' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'KY' WHERE [county] = 'Ky' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'KS' WHERE [county] = 'Ks' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'IN' WHERE [county] = 'In' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'IL' WHERE [county] = 'Illinois' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'IL' WHERE [county] = 'Il' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'ID' WHERE [county] = 'Id' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'IA' WHERE [county] = 'Ia' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'HI' WHERE [county] = 'Hi' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'HI' WHERE [county] = 'Hawaii' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'GA' WHERE [county] = 'Georgia' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'GA' WHERE [county] = 'Ga' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'FL' WHERE [county] = 'Florida' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'FL' WHERE [county] = 'Fl' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'DE' WHERE [county] = 'De' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'DC' WHERE [county] = 'Dc' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'CT' WHERE [county] = 'Ct' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'CO' WHERE [county] = 'Co' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'CA' WHERE [county] = 'California' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'CA' WHERE [county] = 'Ca' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'AZ' WHERE [county] = 'Az' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'AZ' WHERE [county] = 'Arizona' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'AR' WHERE [county] = 'Ar' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'AL' WHERE [county] = 'AL' AND countryid = 185;
UPDATE [dbo].[address] SET [county] = 'AK' WHERE [county] = 'Ak' AND countryid = 185;
GO

-- Delete orphaned Trescal Inc subdiv

UPDATE subdiv set defpersonid = null where subdivid = 48036;
DELETE FROM subdivinstructionlink where subdivid = 48036;
DELETE FROM subdivsettingsforallocatedsubdiv where subdivid = 48036;

DELETE FROM contact where personid = 112636;
DELETE FROM subdiv where subdivid = 48036;

-- Update 4 default subdivs with no default address

UPDATE [dbo].[subdiv]
   SET [defaddrid] = (SELECT TOP 1 addr.addrid FROM address addr WHERE addr.subdivid = subdiv.subdivid) WHERE [defaddrid] is null;
GO

COMMIT TRAN