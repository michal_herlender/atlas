-- Copy translations from en_GB to pt_PT (update 58 translation tables) on selected database

BEGIN TRAN

	-- accessorytranslation
	INSERT INTO dbo.accessorytranslation ( accessoryid, translation, locale)
		SELECT tr.accessoryid, tr.translation, 'pt_PT' from dbo.accessorytranslation tr where locale = 'en_GB'

	-- actionoutcometranslation
	INSERT INTO dbo.actionoutcometranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.actionoutcometranslation tr where locale = 'en_GB'

	-- assetstatustranslation
	INSERT INTO dbo.assetstatustranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.assetstatustranslation tr where locale = 'en_GB'

	-- assettypetranslation
	INSERT INTO dbo.assettypetranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.assettypetranslation tr where locale = 'en_GB'

	-- basestatusdescriptiontranslation
	INSERT INTO dbo.basestatusdescriptiontranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.basestatusdescriptiontranslation tr where locale = 'en_GB'

	-- basestatusnametranslation
	INSERT INTO dbo.basestatusnametranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.basestatusnametranslation tr where locale = 'en_GB'

	-- businessareadescriptiontranslation
	INSERT INTO dbo.businessareadescriptiontranslation ( businessareaid, translation, locale)
		SELECT tr.businessareaid, tr.translation, 'pt_PT' from dbo.businessareadescriptiontranslation tr where locale = 'en_GB'

	-- businessareanametranslation
	INSERT INTO dbo.businessareanametranslation ( businessareaid, translation, locale)
		SELECT tr.businessareaid, tr.translation, 'pt_PT' from dbo.businessareanametranslation tr where locale = 'en_GB'

	-- characteristicdescriptiontranslation
	INSERT INTO dbo.characteristicdescriptiontranslation ( characteristicdescriptionid, translation, locale)
		SELECT tr.characteristicdescriptionid, tr.translation, 'pt_PT' from dbo.characteristicdescriptiontranslation tr where locale = 'en_GB'

	-- characteristiclibrarytranslation
	INSERT INTO dbo.characteristiclibrarytranslation ( characteristiclibraryid, translation, locale)
		SELECT tr.characteristiclibraryid, tr.translation, 'pt_PT' from dbo.characteristiclibrarytranslation tr where locale = 'en_GB'

	-- characteristicshortnametranslation
	INSERT INTO dbo.characteristicshortnametranslation ( characteristicdescriptionid, translation, locale)
		SELECT tr.characteristicdescriptionid, tr.translation, 'pt_PT' from dbo.characteristicshortnametranslation tr where locale = 'en_GB'

	-- creditnotestatusdescriptiontranslation
	INSERT INTO dbo.creditnotestatusdescriptiontranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.creditnotestatusdescriptiontranslation tr where locale = 'en_GB'

	-- creditnotestatusnametranslation
	INSERT INTO dbo.creditnotestatusnametranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.creditnotestatusnametranslation tr where locale = 'en_GB'

	-- defaultnotelabeltranslation
	INSERT INTO dbo.defaultnotelabeltranslation ( defaultnoteid, translation, locale)
		SELECT tr.defaultnoteid, tr.translation, 'pt_PT' from dbo.defaultnotelabeltranslation tr where locale = 'en_GB'

	-- defaultnotenotetranslation
	INSERT INTO dbo.defaultnotenotetranslation ( defaultnoteid, translation, locale)
		SELECT tr.defaultnoteid, tr.translation, 'pt_PT' from dbo.defaultnotenotetranslation tr where locale = 'en_GB'

	-- defaultquotationcalibrationconditiontranslation
	INSERT INTO dbo.defaultquotationcalibrationconditiontranslation ( defcalconid, translation, locale)
		SELECT tr.defcalconid, tr.translation, 'pt_PT' from dbo.defaultquotationcalibrationconditiontranslation tr where locale = 'en_GB'

	-- defaultquotationgeneralconditiontranslation
	INSERT INTO dbo.defaultquotationgeneralconditiontranslation ( defgenconid, translation, locale)
		SELECT tr.defgenconid, tr.translation, 'pt_PT' from dbo.defaultquotationgeneralconditiontranslation tr where locale = 'en_GB'

	-- descriptiontranslation
	INSERT INTO dbo.descriptiontranslation ( descriptionid, translation, locale)
		SELECT tr.descriptionid, tr.translation, 'pt_PT' from dbo.descriptiontranslation tr where locale = 'en_GB'

	-- instmodeldomaintranslation
	INSERT INTO dbo.instmodeldomaintranslation ( domainid, translation, locale)
		SELECT tr.domainid, tr.translation, 'pt_PT' from dbo.instmodeldomaintranslation tr where locale = 'en_GB'

	-- instmodelfamilytranslation
	INSERT INTO dbo.instmodelfamilytranslation ( familyid, translation, locale)
		SELECT tr.familyid, tr.translation, 'pt_PT' from dbo.instmodelfamilytranslation tr where locale = 'en_GB'

	-- instmodelsubfamilytranslation
	INSERT INTO dbo.instmodelsubfamilytranslation ( subfamilyid, translation, locale)
		SELECT tr.subfamilyid, tr.translation, 'pt_PT' from dbo.instmodelsubfamilytranslation tr where locale = 'en_GB'

	-- instmodeltranslation
	-- update manually (maybe OK)
--	INSERT INTO dbo.instmodeltranslation ( modelid, translation, locale)
--		SELECT tr.modelid, tr.translation, 'pt_PT' from dbo.instmodeltranslation tr where locale = 'en_GB'

	-- instrumentstoragetypedescriptiontranslation
	INSERT INTO dbo.instrumentstoragetypedescriptiontranslation ( typeid, translation, locale)
		SELECT tr.typeid, tr.translation, 'pt_PT' from dbo.instrumentstoragetypedescriptiontranslation tr where locale = 'en_GB'

	-- instrumentstoragetypenametranslation
	INSERT INTO dbo.instrumentstoragetypenametranslation ( typeid, translation, locale)
		SELECT tr.typeid, tr.translation, 'pt_PT' from dbo.instrumentstoragetypenametranslation tr where locale = 'en_GB'

	-- instrumentusagetypedescriptiontranslation
	INSERT INTO dbo.instrumentusagetypedescriptiontranslation ( typeid, translation, locale)
		SELECT tr.typeid, tr.translation, 'pt_PT' from dbo.instrumentusagetypedescriptiontranslation tr where locale = 'en_GB'

	-- instrumentusagetypenametranslation
	INSERT INTO dbo.instrumentusagetypenametranslation ( typeid, translation, locale)
		SELECT tr.typeid, tr.translation, 'pt_PT' from dbo.instrumentusagetypenametranslation tr where locale = 'en_GB'

	-- invoicefrequencytranslation
	INSERT INTO dbo.invoicefrequencytranslation ( invoicefrequencyid, translation, locale)
		SELECT tr.invoicefrequencyid, tr.translation, 'pt_PT' from dbo.invoicefrequencytranslation tr where locale = 'en_GB'

	-- invoicestatusdescriptiontranslation
	INSERT INTO dbo.invoicestatusdescriptiontranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.invoicestatusdescriptiontranslation tr where locale = 'en_GB'

	-- invoicestatusnametranslation
	INSERT INTO dbo.invoicestatusnametranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.invoicestatusnametranslation tr where locale = 'en_GB'

	-- invoicetypetranslation
	INSERT INTO dbo.invoicetypetranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.invoicetypetranslation tr where locale = 'en_GB'

	-- itemstatetranslation
	INSERT INTO dbo.itemstatetranslation ( stateid, translation, locale)
		SELECT tr.stateid, tr.translation, 'pt_PT' from dbo.itemstatetranslation tr where locale = 'en_GB'

	-- jobstatusdescriptiontranslation
	INSERT INTO dbo.jobstatusdescriptiontranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.jobstatusdescriptiontranslation tr where locale = 'en_GB'

	-- jobstatusnametranslation
	INSERT INTO dbo.jobstatusnametranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.jobstatusnametranslation tr where locale = 'en_GB'

	-- labeltemplateadditionalfieldtranslation
	INSERT INTO dbo.labeltemplateadditionalfieldtranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.labeltemplateadditionalfieldtranslation tr where locale = 'en_GB'

	-- modeltypedesctranslation
	INSERT INTO dbo.modeltypedesctranslation ( instmodeltypeid, translation, locale)
		SELECT tr.instmodeltypeid, tr.translation, 'pt_PT' from dbo.modeltypedesctranslation tr where locale = 'en_GB'

	-- modeltypenametranslation
	INSERT INTO dbo.modeltypenametranslation ( instmodeltypeid, translation, locale)
		SELECT tr.instmodeltypeid, tr.translation, 'pt_PT' from dbo.modeltypenametranslation tr where locale = 'en_GB'

	-- nominalcodetitletranslation
	INSERT INTO dbo.nominalcodetitletranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.nominalcodetitletranslation tr where locale = 'en_GB'

	-- optiontranslation
	INSERT INTO dbo.optiontranslation ( optionid, translation, locale)
		SELECT tr.optionid, tr.translation, 'pt_PT' from dbo.optiontranslation tr where locale = 'en_GB'

	-- paymentmodetranslation
	INSERT INTO dbo.paymentmodetranslation ( paymentmodeid, translation, locale)
		SELECT tr.paymentmodeid, tr.translation, 'pt_PT' from dbo.paymentmodetranslation tr where locale = 'en_GB'

	-- paymenttermstranslation
	INSERT INTO dbo.paymenttermstranslation ( paymenttermsid, translation, locale)
		SELECT tr.paymenttermsid, tr.translation, 'pt_PT' from dbo.paymenttermstranslation tr where locale = 'en_GB'

	-- permissiongrouptranslation
	INSERT INTO dbo.permissiongrouptranslation ( permissiongroup, translation, locale)
		SELECT tr.permissiongroup, tr.translation, 'pt_PT' from dbo.permissiongrouptranslation tr where locale = 'en_GB'

	-- perroletranslation
	INSERT INTO dbo.perroletranslation ( perrole, translation, locale)
		SELECT tr.perrole, tr.translation, 'pt_PT' from dbo.perroletranslation tr where locale = 'en_GB'

	-- presetcommentcategorytranslation
	INSERT INTO dbo.presetcommentcategorytranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.presetcommentcategorytranslation tr where locale = 'en_GB'

	-- purchaseorderstatusdescriptiontranslation
	INSERT INTO dbo.purchaseorderstatusdescriptiontranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.purchaseorderstatusdescriptiontranslation tr where locale = 'en_GB'

	-- purchaseorderstatusnametranslation
	INSERT INTO dbo.purchaseorderstatusnametranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.purchaseorderstatusnametranslation tr where locale = 'en_GB'

	-- recallresponsestatustranslation
	INSERT INTO dbo.recallresponsestatustranslation ( statusid, translation, locale)
		SELECT tr.statusid, tr.translation, 'pt_PT' from dbo.recallresponsestatustranslation tr where locale = 'en_GB'

	-- salescategorytranslation
	INSERT INTO dbo.salescategorytranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.salescategorytranslation tr where locale = 'en_GB'

	-- servicetypelongnametranslation
	INSERT INTO dbo.servicetypelongnametranslation ( servicetypeid, translation, locale)
		SELECT tr.servicetypeid, tr.translation, 'pt_PT' from dbo.servicetypelongnametranslation tr where locale = 'en_GB'

	-- servicetypeshortnametranslation
	INSERT INTO dbo.servicetypeshortnametranslation ( servicetypeid, translation, locale)
		SELECT tr.servicetypeid, tr.translation, 'pt_PT' from dbo.servicetypeshortnametranslation tr where locale = 'en_GB'

	-- systemdefaultdescriptiontranslation
	INSERT INTO dbo.systemdefaultdescriptiontranslation ( defaultid, translation, locale)
		SELECT tr.defaultid, tr.translation, 'pt_PT' from dbo.systemdefaultdescriptiontranslation tr where locale = 'en_GB'

	-- systemdefaultgroupdescriptiontranslation
	INSERT INTO dbo.systemdefaultgroupdescriptiontranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.systemdefaultgroupdescriptiontranslation tr where locale = 'en_GB'

	-- systemdefaultgroupnametranslation
	INSERT INTO dbo.systemdefaultgroupnametranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.systemdefaultgroupnametranslation tr where locale = 'en_GB'

	-- systemdefaultnametranslation
	INSERT INTO dbo.systemdefaultnametranslation ( defaultid, translation, locale)
		SELECT tr.defaultid, tr.translation, 'pt_PT' from dbo.systemdefaultnametranslation tr where locale = 'en_GB'

	-- transportmethodtranslation
	INSERT INTO dbo.transportmethodtranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.transportmethodtranslation tr where locale = 'en_GB'

	-- uomnametranslation
	INSERT INTO dbo.uomnametranslation ( id, translation, locale)
		SELECT tr.id, tr.translation, 'pt_PT' from dbo.uomnametranslation tr where locale = 'en_GB'

	-- usergroupdescriptiontranslation
	INSERT INTO dbo.usergroupdescriptiontranslation ( groupid, translation, locale)
		SELECT tr.groupid, tr.translation, 'pt_PT' from dbo.usergroupdescriptiontranslation tr where locale = 'en_GB'

	-- usergroupnametranslation
	INSERT INTO dbo.usergroupnametranslation ( groupid, translation, locale)
		SELECT tr.groupid, tr.translation, 'pt_PT' from dbo.usergroupnametranslation tr where locale = 'en_GB'

COMMIT TRAN