-- Script to merge information from one business company into another:
-- A+ Metrologie (source company) and 
-- Trescal SA (target company)
-- Galen Beck - 2018-12-17
-- We plan to run on production server 2018-12-31, and not before...

USE [spain]

BEGIN TRAN

-- Declare person that is performing the update
DECLARE @personid_admin int
	SELECT @personid_admin = c.personid FROM dbo.contact c
	WHERE c.firstname = 'Galen' and c.lastname = 'Beck';

print 'Admin person id :'
print @personid_admin

-- Get source 'A+ METROLOGIE' coid
DECLARE @coid_source int
	SELECT @coid_source = c.coid FROM dbo.company c
	WHERE c.coname like 'A+ METROLOGIE' and corole=5;

print 'Source company id :'
print @coid_source

-- Get target 'Trescal SA' coid
DECLARE @coid_target int
	SELECT @coid_target = c.coid FROM dbo.company c
	WHERE c.coname = 'TRESCAL SA' and corole=5;

print 'Target company id :'
print @coid_target

-- Get subdivisions in source company that we are disabling (not merging)

DECLARE @subdivids_disable TABLE(subdivid int);

INSERT INTO @subdivids_disable (subdivid)
	SELECT [subdivid]
	  FROM [dbo].[subdiv] WHERE coid = @coid_source AND subname IN ('Arras', 'Nantes', 'Toulouse');

print 'Source subdiv ids to disable :'
DECLARE @xmltmp xml = (SELECT * FROM @subdivids_disable FOR XML AUTO)
PRINT CONVERT(NVARCHAR(MAX), @xmltmp)

-- Special declarations for subdivisions being disabled (not moved)

DECLARE @subdivid_aplus_arras int
	SELECT @subdivid_aplus_arras = [subdivid]
	  FROM [dbo].[subdiv] WHERE coid = @coid_source AND subname = 'Arras';	

print 'APlus Arras subdiv id :'
print @subdivid_aplus_arras

DECLARE @subdivid_aplus_nantes int
	SELECT @subdivid_aplus_nantes = [subdivid]
	  FROM [dbo].[subdiv] WHERE coid = @coid_source AND subname = 'Nantes';

print 'APlus Nantes subdiv id :'
print @subdivid_aplus_nantes

DECLARE @subdivid_aplus_toulouse int
	SELECT @subdivid_aplus_toulouse = [subdivid]
	  FROM [dbo].[subdiv] WHERE coid = @coid_source AND subname = 'Toulouse';

print 'APlus Toulouse subdiv id :'
print @subdivid_aplus_toulouse

-- Default person / address in new company used when moving instruments from old company in deactivated subdivs

DECLARE @personid_trescal_arras int
	SELECT @personid_trescal_arras = [defpersonid]
	  FROM [subdiv] WHERE coid = @coid_target AND subname = 'Arras';

print 'Trescal Arras default person id :'
print @personid_trescal_arras

DECLARE @personid_trescal_nantes int
	SELECT @personid_trescal_nantes = [defpersonid]
	  FROM [subdiv] WHERE coid = @coid_target AND subname = 'Nantes';

print 'Trescal Nantes default person id :'
print @personid_trescal_nantes

DECLARE @personid_trescal_toulouse int
	SELECT @personid_trescal_toulouse = [defpersonid]
	  FROM [subdiv] WHERE coid = @coid_target AND subname = 'Toulouse';

print 'Trescal Toulouse default person id :'
print @personid_trescal_toulouse

DECLARE @addressid_trescal_arras int
	SELECT @addressid_trescal_arras = [defaddrid]
	  FROM [subdiv] WHERE coid = @coid_target AND subname = 'Arras';

print 'Trescal Arras default address id :'
print @addressid_trescal_arras

DECLARE @addressid_trescal_nantes int
	SELECT @addressid_trescal_nantes = [defaddrid]
	  FROM [subdiv] WHERE coid = @coid_target AND subname = 'Nantes';

print 'Trescal Nantes default address id :'
print @addressid_trescal_nantes

DECLARE @addressid_trescal_toulouse int
	SELECT @addressid_trescal_toulouse = [defaddrid]
	  FROM [subdiv] WHERE coid = @coid_target AND subname = 'Toulouse';

print 'Trescal Toulouse default address id :'
print @addressid_trescal_toulouse

-- Get subdivisions in source company that we are moving (merging)

DECLARE @subdivids_move TABLE(subdivid int);

INSERT INTO @subdivids_move (subdivid)
	SELECT [subdivid]
	  FROM [dbo].[subdiv] WHERE coid = @coid_source AND subname NOT IN ('Arras', 'Nantes', 'Toulouse');

print 'Source subdiv ids to move :'
SELECT @xmltmp = (SELECT * FROM @subdivids_move FOR XML AUTO)
PRINT CONVERT(NVARCHAR(MAX), @xmltmp)

-- Disable the unused subdivisions (note, no data is moved, these subdivisions should be inactive)

UPDATE [subdiv] 
	SET active = 0, deacttime = GETDATE(), deactreason='Merge APlus to Trescal SA', 
	  lastModified = GETDATE(), lastModifiedBy = @personid_admin
	WHERE subdivid in (SELECT subdivid FROM @subdivids_disable);

print 'Disabled unused subdivisions'

-- Update the subdivisions to be moved

UPDATE [subdiv] 
	SET coid = @coid_target, lastModified = GETDATE(), lastModifiedBy = @personid_admin
	WHERE subdivid in (SELECT subdivid FROM @subdivids_move);

print 'Moved remaining subdivisions'

-- Special update of instruments for A+ Arras to Trescal Arras

UPDATE i
	SET i.[addressid] = @addressid_trescal_arras
	FROM [instrument] as i
	INNER JOIN address as a ON i.addressid = a.addrid
	WHERE a.subdivid = @subdivid_aplus_arras

print 'Updated instrument address from APlus Arras to Trescal Arras'

UPDATE i
	SET i.personid = @personid_trescal_arras
	FROM [instrument] as i
	INNER JOIN contact as c ON i.personid = c.personid
	WHERE c.subdivid = @subdivid_aplus_arras

print 'Updated instrument owner from APlus Arras to Trescal Arras'

-- Special update of instruments for A+ Nantes to Trescal Nantes

UPDATE i
	SET i.[addressid] = @addressid_trescal_nantes
	FROM [instrument] as i
	INNER JOIN address as a ON i.addressid = a.addrid
	WHERE a.subdivid = @subdivid_aplus_nantes

print 'Updated instrument address from APlus Nantes to Trescal SA Nantes'

UPDATE i
	SET i.personid = @personid_trescal_nantes
	FROM [instrument] as i
	INNER JOIN contact as c ON i.personid = c.personid
	WHERE c.subdivid = @subdivid_aplus_nantes

print 'Updated instrument owner from APlus Nantes to Trescal SA Nantes'

-- Special update of instruments for A+ Toulouse to Trescal Toulouse

UPDATE i
	SET i.[addressid] = @addressid_trescal_toulouse
	FROM [instrument] as i
	INNER JOIN address as a ON i.addressid = a.addrid
	WHERE a.subdivid = @subdivid_aplus_toulouse

print 'Updated instrument address from APlus Toulouse to Trescal SA Toulouse'

UPDATE i
	SET i.personid = @personid_trescal_toulouse
	FROM [instrument] as i
	INNER JOIN contact as c ON i.personid = c.personid
	WHERE c.subdivid = @subdivid_aplus_toulouse

print 'Updated instrument owner from APlus Toulouse to Trescal SA Toulouse'

-- Update coid of all old company instruments to new company

INSERT INTO [dbo].[instrumenthistory]
           ([addressupdated]
           ,[changedate]
           ,[contactupdated]
           ,[plantnoupdated]
           ,[serialnoupdated]
           ,[statusUpdated]
           ,[changeby]
           ,[plantid]
           ,[companyupdated]
           ,[newcompanyid]
           ,[oldcompanyid])
     SELECT 0
           ,GETDATE()
           ,0
           ,0
           ,0
           ,0
           ,@personid_admin
           ,instrument.plantid
           ,1
           ,@coid_target
           ,@coid_source
	FROM instrument
	WHERE instrument.coid = @coid_source;

print 'Inserted instrument company change record from APlus to Trescal SA'

UPDATE instrument
	SET coid = @coid_target WHERE coid = @coid_source;

print 'Updated instruments from APlus to Trescal SA'

-- Update remaining entities (business data / configuration) which should point to new company

print 'Updating QuickPurchaseOrder'

UPDATE [quickpurchaseorder] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating CompanyInstructionLink'

UPDATE [companyinstructionlink] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating SubdivInstructionLink'

UPDATE [subdivinstructionlink] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating ContactInstructionLink'

UPDATE [contactinstructionlink] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating Recall'

UPDATE [recall] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating QuotationRequest'

UPDATE [quotationrequest] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating DefaultNote'

UPDATE [defaultnote] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating SupplierInvoice'

UPDATE [supplierinvoice] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating (Client) BPO'

UPDATE [bpo] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating (Client) PO'

UPDATE [po] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating (Client) InvoicePO'

UPDATE [invoicepo] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating CustomGeneralCondition'

UPDATE [customquotationgeneralcondition] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating ExchangeFormat - businessCompany'

UPDATE [exchangeformat] SET businessCompanyid = @coid_target WHERE businessCompanyid = @coid_source;

print 'Updating PurchaseOrder'

UPDATE [purchaseorder] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating CreditNote'

UPDATE [creditnote] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating Quotation'

UPDATE [quotation] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating Invoice'

UPDATE [invoice] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating JobCosting'

UPDATE [jobcosting] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating TPQuotation'

UPDATE [tpquotation] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating TPQuoteRequest'

UPDATE [tpquoterequest] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating EmailTemplate - moved subdivs only'

UPDATE [emailtemplate] SET orgid = @coid_target WHERE orgid = @coid_source AND
	subdivid in (SELECT subdivid FROM @subdivids_move); 

ROLLBACK TRAN