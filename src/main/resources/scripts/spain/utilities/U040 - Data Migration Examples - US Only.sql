-- Utility script with some examples for cleaning / updating migrated data on the US system
-- Galen Beck - 2021-08-23

-- Note, change database name as needed if not using default of "atlas"
-- At the end, change ROLLBACK TRAN to COMMIT TRAN in order to update actual data

USE [atlas-us]

BEGIN TRAN

-- Define / look up IDs which would be used for further activities (safer as some IDs may not be the same on all systems...)

DECLARE @coid_TUS int;
DECLARE @subdivid_ATL int;
DECLARE @subdivid_BAL int;
DECLARE @subdivid_CHI int;
DECLARE @subdivid_CLE int;
DECLARE @subdivid_DAL int;
DECLARE @subdivid_HAR int;
DECLARE @subdivid_MOB int;
DECLARE @personid_ModifiedBy int;

SELECT @coid_TUS = coid from company WHERE coname='TRESCAL INC' and corole = 5;
SELECT @subdivid_ATL = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'ATL';
SELECT @subdivid_BAL = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'BAL';
SELECT @subdivid_CHI = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'CHI';
SELECT @subdivid_CLE = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'CLE';
SELECT @subdivid_DAL = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'DAL';
SELECT @subdivid_HAR = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'HAR';
SELECT @subdivid_MOB = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'MOB';
SELECT @personid_ModifiedBy = personid FROM contact WHERE email='eric.christianson@trescal.us';

-- All the data here should exist and not be null...

PRINT '@coid_TUS : ';
PRINT @coid_TUS;

PRINT '@subivid_ATL : ';
PRINT @subdivid_ATL;

PRINT '@subivid_BAL : ';
PRINT @subdivid_BAL;

PRINT '@subivid_CHI : ';
PRINT @subdivid_CHI;

PRINT '@subivid_CLE : ';
PRINT @subdivid_CLE;

PRINT '@subivid_DAL : ';
PRINT @subdivid_DAL;

PRINT '@subivid_HAR : ';
PRINT @subdivid_HAR;

PRINT '@subivid_MOB : ';
PRINT @subdivid_MOB;

PRINT '@personid_ModifiedBy : ';
PRINT @personid_ModifiedBy;

-- ****** Example 1 : Account Reps:

DECLARE @personid_CLE_Carol_White int;
DECLARE @personid_CLE_Cynthia_Duncan int;
DECLARE @personid_HAR_Jill_Laier int;

SELECT @personid_CLE_Carol_White = personid FROM contact WHERE contact.subdivid = @subdivid_CLE AND email='carol.white@trescal.us';
SELECT @personid_CLE_Cynthia_Duncan = personid FROM contact WHERE contact.subdivid = @subdivid_CLE AND email='cynthia.duncan@trescal.us';
SELECT @personid_HAR_Jill_Laier = personid FROM contact WHERE contact.subdivid = @subdivid_HAR AND email='jill.laier@trescal.us';

PRINT '@personid_CLE_Carol_White : ';
PRINT @personid_CLE_Carol_White;

PRINT '@personid_CLE_Cynthia_Duncan :';
PRINT @personid_CLE_Cynthia_Duncan;

PRINT '@personid_HAR_Jill_Laier :';
PRINT @personid_HAR_Jill_Laier;

-- Example client Sample Industries (former ID 240090)

DECLARE @subdivid_client int;
SELECT @subdivid_client = subdivid FROM subdiv WHERE formerid = '240090';
PRINT 'Example - @subdivid_client : ';
PRINT @subdivid_client;

-- Example 1 - Update or insert the record for local business contact 
--             for Sample Industries subdiv for both CLE and HAR

-- Example 1A - Update or insert local business contact for HAR subdivision

PRINT 'Example 1A - Update/Insert for HAR'
UPDATE [dbo].[subdivsettingsforallocatedsubdiv]
   SET [lastModified] = SYSDATETIME()
      ,[lastModifiedBy] = @personid_ModifiedBy
      ,[businesscontactid] = @personid_HAR_Jill_Laier
 WHERE [orgid] = @subdivid_HAR AND [subdivid] = @subdivid_client

IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[subdivsettingsforallocatedsubdiv]
			   ([lastModified]
			   ,[lastModifiedBy]
			   ,[orgid]
			   ,[businesscontactid]
			   ,[subdivid])
		 VALUES
			   (SYSDATETIME()
			   ,@personid_ModifiedBy
			   ,@subdivid_HAR
			   ,@personid_HAR_Jill_Laier
			   ,@subdivid_client)
END

-- Example 1B - Update or insert local business contact for CLE subdivision

PRINT 'Example 1B - Update/Insert for CLE'
UPDATE [dbo].[subdivsettingsforallocatedsubdiv]
   SET [lastModified] = SYSDATETIME()
      ,[lastModifiedBy] = @personid_ModifiedBy
      ,[businesscontactid] = @personid_CLE_Cynthia_Duncan
 WHERE [orgid] = @subdivid_CLE AND [subdivid] = @subdivid_client

IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[subdivsettingsforallocatedsubdiv]
			   ([lastModified]
			   ,[lastModifiedBy]
			   ,[orgid]
			   ,[businesscontactid]
			   ,[subdivid])
		 VALUES
			   (SYSDATETIME()
			   ,@personid_ModifiedBy
			   ,@subdivid_CLE
			   ,@personid_CLE_Cynthia_Duncan
			   ,@subdivid_client)
END

-- ****** Example 2 : Instructions

-- Example - insert instructions of various types for Sample Industries (former ID 240090)
-- this is done at client subdiv level
-- There are 11 types of instructions in Atlas

DECLARE @itype_GENERAL int = 1;
DECLARE @itype_PACKAGING int = 2;
DECLARE @itype_CARRIAGE int = 3;
DECLARE @itype_DISCOUNT int = 4;
DECLARE @itype_CALIBRATION int = 5;
DECLARE @itype_REPAIRANDADJUSTMENT int = 6;
DECLARE @itype_COSTING int = 7;
DECLARE @itype_INVOICE int = 8;
DECLARE @itype_HIRE int = 9;
DECLARE @itype_PURCHASE int = 10;
DECLARE @itype_RECEIPT int = 11;

-- Example of CARRIAGE (aka Transport in USA) instruction if we want to have the instruction appear on
-- both client and supplier delivery notes
-- For other types of instructions we always set the "inludeon" fields to 0
-- [includeondelnote] = 0
-- [includeonsupplierdelnote] = 0
-- We need to create both a baseinstruction and a subdivinstructionlink record

INSERT INTO [dbo].[baseinstruction]
           ([includeondelnote]
           ,[instruction]
           ,[instructiontypeid]
           ,[lastModified]
           ,[lastModifiedBy]
           ,[includeonsupplierdelnote])
     VALUES
           (1
           ,'All transport for Sample Industries is hypothetical as it''s a sample customer only'
           ,@itype_CARRIAGE
           ,SYSDATETIME()
           ,@personid_ModifiedBy
           ,0)

DECLARE @baseinst_id int;
SELECT @baseinst_id = MAX(instructionid) from baseinstruction;
PRINT 'Example 2 - inserted baseinstructionid : '
PRINT @baseinst_id;

-- We can assume that [issubdivinstruction] can be false
-- this means that the instruction will appear for ALL business subdivisions (CLE, DAL, HAR, etc...)
-- and thereby "businesssubdivid" can be left null

INSERT INTO [dbo].[subdivinstructionlink]
           ([lastModified]
           ,[instructionid]
           ,[subdivid]
           ,[orgid]
           ,[lastModifiedBy]
           ,[issubdivinstruction])
     VALUES
           (SYSDATETIME()
           ,@baseinst_id
           ,@subdivid_client
           ,@coid_TUS
           ,@personid_ModifiedBy
           ,0)

-- ****** Example 3 : Receiver Complete

-- Example - insert or update system default for Sample Industries (former ID 240090)
-- this is done at client subdiv level
-- EMS "Receiver Complete" = Yes/true is equivalent to Atlas "Client - Misc" : "Part-Delivery" = No/false
-- Expected to be always ID 5

DECLARE @sysdefid_PART_DELIVERY int;
SELECT @sysdefid_PART_DELIVERY = defaultid FROM systemdefault WHERE defaultname = 'Part-Delivery';

PRINT 'Example 3 - @sysdefid_PART_DELIVERY : ';
PRINT @sysdefid_PART_DELIVERY;

-- Since we are updating / inserting at [subdivid] level the [coid] and [personid] fields are left blank (not shown here)

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'false'
      ,[lastModified] = SYSDATETIME()
      ,[lastModifiedBy] = @personid_ModifiedBy
 WHERE [subdivid] = @subdivid_client AND [defaultid] = @sysdefid_PART_DELIVERY;

IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[systemdefaultapplication]
           ([defaultvalue]
           ,[subdivid]
           ,[defaultid]
           ,[lastModified]
           ,[orgid]
           ,[lastModifiedBy])
     VALUES
           ('false'
           ,@subdivid_client
           ,@sysdefid_PART_DELIVERY
           ,SYSDATETIME()
           ,@coid_TUS
           ,@personid_ModifiedBy)
END

-- ****** Example 4 : Price on Packing List

-- Example - insert or update system default for Sample Industries (former ID 240090)
-- this is done at client subdiv level
-- EMS "Price on Packing List" is equivalent to Atlas "Client - Delivery Note" : "Print Total Price"
-- Expected to be always ID 62

DECLARE @sysdefid_PRINT_TOTAL_PRICE int;
SELECT @sysdefid_PRINT_TOTAL_PRICE = defaultid FROM systemdefault WHERE defaultname = 'Print Total Price';

PRINT 'Example 4 - @sysdefid_PRINT_TOTAL_PRICE : ';
PRINT @sysdefid_PRINT_TOTAL_PRICE;

-- Since we are updating / inserting at [subdivid] level the [coid] and [personid] fields are left blank (not shown here)

UPDATE [dbo].[systemdefaultapplication]
   SET [defaultvalue] = 'true'
      ,[lastModified] = SYSDATETIME()
      ,[lastModifiedBy] = @personid_ModifiedBy
 WHERE [subdivid] = @subdivid_client AND [defaultid] = @sysdefid_PRINT_TOTAL_PRICE;

IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [dbo].[systemdefaultapplication]
           ([defaultvalue]
           ,[subdivid]
           ,[defaultid]
           ,[lastModified]
           ,[orgid]
           ,[lastModifiedBy])
     VALUES
           ('true'
           ,@subdivid_client
           ,@sysdefid_PRINT_TOTAL_PRICE
           ,SYSDATETIME()
           ,@coid_TUS
           ,@personid_ModifiedBy)
END

ROLLBACK TRAN