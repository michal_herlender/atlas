-- Script 029 - This can be run to create (or recreate) the CDC tables.
-- This is being separated from our usual upgrade scripts for several reasons:
-- We would only use this (to start) on US production servers, one US test server, and for some development machines
-- Not all test servers (France-test, Spain-test) support CDC currently.

-- Found that on some restored databases, we may have to set (reset) the database owner before enabling CDC - GB 2021-03-04
--EXEC sp_changedbowner 'sa'
--GO

--Disable and enable before changes in CDC.

EXEC sys.sp_cdc_disable_db  
GO  

EXEC sys.sp_cdc_enable_db  
GO  

-- GROUP 1 - Referential Data ( CDC instance creation queries )

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'mfr',  
@role_name     = N'sa',  
@captured_column_list = '[mfrid],
[active], 
[genericmfr], 
[name], 
[tmlid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'country',  
@role_name     = N'sa',  
@captured_column_list = '[countryid], 
[country], 
[countrycode]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'instmodel',  
@role_name     = N'sa',  
@captured_column_list = '[modelid]
,[descriptionid]
,[mfrid]
,[model]
,[modelmfrtype]
,[modeltypeid]
,[quarantined]
,[tmlid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'instmodeltranslation',  
@role_name     = N'sa',  
@captured_column_list = '[modelid], 
[locale], 
[translation]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'servicetype',  
@role_name     = N'sa',  
@captured_column_list = '[servicetypeid]
,[longname]
,[shortname]
,[canbeperformedbyclient]
,[domaintype]
,[orderby]
,[repair]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'description',  
@role_name     = N'sa',  
@captured_column_list = '[descriptionid]
, [description]
, [tmlid]
, [familyid]
, [active]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'uom',  
@role_name     = N'sa',  
@captured_column_list = '[id]
      ,[name]
      ,[symbol]
      ,[tmlid]
      ,[active]',
@supports_net_changes = 1  
GO

-- GROUP 2 - Company Data ( CDC instance creation queries )

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'company',  
@role_name     = N'sa',  
@captured_column_list = '[coid]
           ,[coname]
           ,[corole]
           ,[countryid]
           ,[companycode]
           ,[legalidentifier]
           ,[fiscalidentifier]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'companysettingsforallocatedcompany',  
@role_name     = N'sa',  
@captured_column_list = '[id]
            ,[orgid]
			,[companyid]
		   ,[active]
           ,[onstop]
           ,[status]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'subdiv',  
@role_name     = N'sa',  
@captured_column_list = '[subdivid]
,[coid]
,[active]
,[subdivcode]
,[subname]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'address',  
@role_name     = N'sa',  
@captured_column_list = '[addrid]
           ,[active]
           ,[addr1]
           ,[addr2]
           ,[addr3]
		   ,[county]
           ,[postcode]
           ,[town]
           ,[countryid]
		   ,[subdivid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'contact',  
@role_name     = N'sa',  
@captured_column_list = '[personid]
,[active]
,[firstname]
,[lastname]
,[email]
,[telephone]
,[fax]
,[mobile]
,[subdivid]
,[defaddress]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'location',  
@role_name     = N'sa',  
@captured_column_list = '[locationid]
           ,[active]
           ,[location]
           ,[addressid] ',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'department',  
@role_name     = N'sa',  
@captured_column_list = '[deptid]
		   ,[name]
           ,[addrid]
           ,[subdivid]
           ,[typeid]
           ,[shortname]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'departmentmember',  
@role_name     = N'sa',  
@captured_column_list = '[id]
           ,[personid]
           ,[deptid]',
@supports_net_changes = 1  
GO

-- GROUP 3 - Instrument Data ( CDC instance creations )
EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'instrument',  
@role_name     = N'sa',  
@captured_column_list = '[plantid]  
		   ,[calfrequency]
		   ,[calintervalunit]
           ,[calibrationStandard]
           ,[recalldate]
           ,[plantno]
		   ,[serialno]
		   ,[statusid]
		   ,[customerdescription]
		   ,[customerspecification]
		   ,[formerbarcode]
		    ,[freeTextLocation]
			,[clientbarcode]
			,[modelname]
           ,[addressid]
           ,[coid]
           ,[personid]
           ,[locationid]
           ,[mfrid]
           ,[modelid]
		   ,[defaultservicetype]
		   ,[usageid]
		   ,[customermanaged]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'instrumentrange',  
@role_name     = N'sa',  
@captured_column_list = '[id]
   ,[endd]
   ,[start]
   ,[maxuomid]
   ,[uomid]
   ,[plantid]
 ',
@supports_net_changes = 1  
GO


EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'instrumentusagetype',  
@role_name     = N'sa',  
@captured_column_list = '[typeid]
   ,[defaulttype]
   ,[fulldescription]
   ,[name]
   ,[recall]
   ,[instrumentstatus]
 ',
@supports_net_changes = 1  
GO


-- GROUP 4 - Job Data ( CDC instance creation queries )

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'itemstate',  
@role_name     = N'sa',  
@captured_column_list = '[type]
		   ,[stateid]
           ,[active]
           ,[description]
           ,[retired]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'job',  
@role_name     = N'sa',  
@captured_column_list = '[jobid]
   ,[clientref]
		 ,[jobno]
		  ,[regdate]
		  ,[datecomplete]
		  ,[personid]
		  ,[statusid]
		  ,[returnto]
		  ,[typeid]
          ,[orgid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'jobitem',  
@role_name     = N'sa',  
@captured_column_list = '[jobitemid]
		   ,[datein]
           ,[duedate]
		   ,[datecomplete]
		    ,[clientreceiptdate]
           ,[itemno]
		   ,[addrid]
		   ,[plantid]
		   ,[jobid]
		   ,[stateid]
		   ,[groupid]
		   ,[clientRef]
		   ,[servicetypeid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'jobitemgroup',  
@role_name     = N'sa',  
@captured_column_list = '[id]
		   ,[calibration]
		   ,[delivery]
		   ,[jobid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'engineerallocation',  
@role_name     = N'sa',  
@captured_column_list = '[id]
		   ,[active]
           ,[allocatedfor]
           ,[allocatedon]
           ,[timeofday]
           ,[allocatedbyid]
           ,[allocatedtoid]
           ,[jobitemid]
           ,[allocateduntil]
           ,[timeofdayuntil]
           ,[deptid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'calreq',  
@role_name     = N'sa',  
@captured_column_list = '[type]
	  ,[id]
      ,[active]
	  ,[publicinstructions]
      ,[instructions]
      ,[jobitemid]
      ,[plantid]
      ,[compid]
      ,[modelid]
      ,[descid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'upcomingwork',  
@role_name     = N'sa',  
@captured_column_list = ' [id]
      ,[active]
      ,[description]
      ,[duedate]
      ,[startdate]
      ,[title]
      ,[coid]
      ,[personid]
      ,[deptid]
      ,[respuserid]
      ,[addedbyid]
      ,[orgid] ',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'upcomingworkjoblink',  
@role_name     = N'sa',  
@captured_column_list = '[id]
      ,[active]
      ,[jobid]
      ,[upcomingworkid]',
@supports_net_changes = 1  
GO


EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'onbehalfitem',  
@role_name     = N'sa',  
@captured_column_list = '[id]
      ,[addressid]
      ,[coid]
      ,[jobitemid]',
@supports_net_changes = 1  
GO

--Group 5 - Instruction Data ( CDC instance creation query )


EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'baseinstruction',  
@role_name     = N'sa',  
@captured_column_list = ' [instructionid]
      ,[instruction]
      ,[instructiontypeid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'companyinstructionlink',  
@role_name     = N'sa',  
@captured_column_list = '[id]
,[instructionid]
		,[coid]
		,[orgid]
		,[issubdivinstruction]	
		,[businesssubdivid]',
@supports_net_changes = 1  
GO


EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'subdivinstructionlink',  
@role_name     = N'sa',  
@captured_column_list = ' [id]
 ,[instructionid]
	,[subdivid]
	,[orgid]
    ,[issubdivinstruction]
	,[businesssubdivid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'contactinstructionlink',  
@role_name     = N'sa',  
@captured_column_list = '[id] 
		,[instructionid]
		,[personid]
		,[orgid]',
@supports_net_changes = 1  
GO



EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'jobinstructionlink',  
@role_name     = N'sa',  
@captured_column_list = '  [id]
,[instructionid]
,[jobid]',
@supports_net_changes = 1  
GO

-- Group 6 - System Default Data

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'systemdefault',  
@role_name     = N'sa',  
@captured_column_list = ' [defaultid]
      ,[defaultdatatype]
      ,[defaultdescription]
      ,[defaultname]
      ,[defaultvalue]
      ,[groupwide]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'systemdefaultapplication',  
@role_name     = N'sa',  
@captured_column_list = ' [id]
 ,[defaultid]
      ,[defaultvalue]
      ,[coid]
	  ,[subdivid]
      ,[personid]
      ,[orgid]',
@supports_net_changes = 1  
GO

--Group 7 - Work Instruction Data 

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'workinstruction',  
@role_name     = N'sa',  
@captured_column_list = '[id]
      ,[name]
      ,[title]
	  ,[descid]
      ,[orgid]',
@supports_net_changes = 1  
GO

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'instrumentworkinstruction',  
@role_name     = N'sa', 
@captured_column_list = '[workInstructionId]
      ,[plantId]',
@supports_net_changes = 0  
GO

--Group 8 - Certificate Data

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'certificate',  
@role_name     = N'sa', 
@captured_column_list = '[certid]
      ,[caldate]
      ,[certdate]
      ,[certno]
      ,[duration]
      ,[thirdcertno]
      ,[type]
	  ,[servicetypeid]
      ,[supplementaryforid]
      ,[thirddiv]
      ,[intervalunit]
      ,[calibrationverificationstatus]
      ,[optimization]
      ,[repair]
      ,[restriction]
      ,[adjustment]
      ,[certstatus]',
@supports_net_changes = 1  
GO
 
 
 EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'instcertlink',  
@role_name     = N'sa', 
@captured_column_list = '[id]
		,[certid]
		,[plantid]',
@supports_net_changes =1  
GO

 EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'certlink',  
@role_name     = N'sa', 
@captured_column_list = '[linkid]
		 ,[certid]
		 ,[jobitemid]',
@supports_net_changes = 1  
GO


 EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'subdivsettingsforallocatedsubdiv',  
@role_name     = N'sa', 
@captured_column_list = '[id]
		 ,[orgid]
		 ,[businesscontactid]
		 ,[subdivid]',
@supports_net_changes = 1  
GO

--Group 9 - Invoice Data

 EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'invoice',  
@role_name     = N'sa', 
@captured_column_list = '[id]
		 ,[issued]
		 ,[issuedate]
		 ,[finalcost]
		 ,[totalcost]
		 ,[vatvalue]
		 ,[invno]
		 ,[addressid]
		 ,[coid]
		 ,[orgid]
		 ,[duedate]',
@supports_net_changes = 1  
GO

 EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'creditnote',  
@role_name     = N'sa', 
@captured_column_list = '[id]
		 ,[issued]
		 ,[issuedate]
		 ,[finalcost]
		 ,[totalcost]
		 ,[vatvalue]
		 ,[creditnoteno]
		 ,[invid]
		 ,[orgid]',
@supports_net_changes = 1  
GO
