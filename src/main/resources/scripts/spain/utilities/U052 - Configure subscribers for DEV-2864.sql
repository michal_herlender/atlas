BEGIN TRAN

-- Adds TAM and TAM Test as subscribers to additional events

declare @sub_TEST int = 1;
declare @sub_CALYPSO int = 2;
declare @sub_TAM int = 3;
declare @sub_TAM_Local int = 4;

declare @id_ON_BEHALF_ITEM int = 37;
declare @id_INSTRUMENT_USAGE_TYPE int = 38;
declare @id_SUBDIV_SETTINGS int = 39;

declare @id_INVOICE int = 40;
declare @id_CREDIT_NOTE int = 41;

-- Test no longer needed (if activated)

DELETE FROM [dbo].[event_subscription] WHERE [subscriberid] = @sub_TEST;

-- Reset all recent subscriptions (will recreate here)

DELETE FROM [dbo].[event_subscription] WHERE [entitytype] in 
    (@id_ON_BEHALF_ITEM,
	 @id_INSTRUMENT_USAGE_TYPE,
	 @id_SUBDIV_SETTINGS,
	 @id_INVOICE,
     @id_CREDIT_NOTE);

-- Calypso

INSERT INTO [dbo].[event_subscription]
           ([entitytype],[subscriberid])
     VALUES
		   (@id_ON_BEHALF_ITEM ,@sub_CALYPSO),
		   (@id_INSTRUMENT_USAGE_TYPE ,@sub_CALYPSO);
-- (@id_SUBDIV_SETTINGS ,@sub_CALYPSO);

-- (@id_INVOICE ,@sub_CALYPSO);
-- (@id_CREDIT_NOTE ,@sub_CALYPSO);

-- TAM

INSERT INTO [dbo].[event_subscription]
           ([entitytype],[subscriberid])
     VALUES
--         (@id_ON_BEHALF_ITEM ,@sub_TAM),
		   (@id_INSTRUMENT_USAGE_TYPE ,@sub_TAM),
		   (@id_SUBDIV_SETTINGS ,@sub_TAM),

		   (@id_INVOICE ,@sub_TAM),
		   (@id_CREDIT_NOTE ,@sub_TAM);

-- TAM Test

INSERT INTO [dbo].[event_subscription]
           ([entitytype],[subscriberid])
     VALUES
--         (@id_ON_BEHALF_ITEM ,@sub_TAM_Local),
		   (@id_INSTRUMENT_USAGE_TYPE ,@sub_TAM_Local),
		   (@id_SUBDIV_SETTINGS ,@sub_TAM_Local),

		   (@id_INVOICE ,@sub_TAM_Local),
		   (@id_CREDIT_NOTE ,@sub_TAM_Local);

ROLLBACK TRAN