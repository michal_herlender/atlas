-- 2021-03-25 : Creates work instructions based on INPUT_instrument table
-- Author : Obed Ortiz (and others)
-- 2021-05-04: Change on search work instruction based on instrument model subfamily instead TML_subfamily

BEGIN TRAN

DECLARE @BusinessCompanyID INT
	,@UserID INT				
-- Get BusinessCompany for which treatment is				
SET @BusinessCompanyID = 6682;

-- Get UserID of the CWMS Auto User
SELECT @UserID = personid
		FROM dbo.contact
		WHERE (lower(firstname) = lower('CWMS Auto User') OR lower(firstname) = lower('ERP Auto User'))
			AND subdivid IN (
				SELECT subdivid
				FROM dbo.subdiv
				WHERE coid = @BusinessCompanyID
				)
IF (@userID IS NULL)
	SELECT TOP 1 @userID = personid
	FROM dbo.contact
	WHERE (lower(firstname) = lower('CWMS Auto User') OR lower(firstname) = lower('ERP Auto User'))
	

-- Insert into Atlas workinstruction based on instrument model Subfamily in input_instrument table
-- localization3 contains model code
-- localization4 contains model name, it is taken the first model name, as per there are several names for one subfamily.

INSERT INTO dbo.workinstruction(
		descid
		,title
		,name
		,lastModified
		,orgid
		,lastModifiedBy
		)
	SELECT distinct d.descriptionid,localization3,
	(select top 1 localization4 from INPUT_Instrument ii2 where ii2.localization3=ii.localization3) "localization4",
	getdate(),@BusinessCompanyID,@UserID
FROM dbo.INPUT_Instrument ii 
inner join instmodel im on im.tmlid=ii.instmodel_tmlid
inner join description d on im.descriptionid=d.descriptionid
WHERE ii.record_toExclude=0 and ii.new_plantid is not null 


	-- Insert the plantid and workinstruction records from input_instrument 
	INSERT INTO dbo.instrumentworkinstruction(
		workInstructionId
		,plantId)
	SELECT 
		wi.id
		,ii.new_plantid /* , ii.plantno,ii.new_subfamilyid,d.descriptionid,ii.localization3,ii.localization4*/
	FROM dbo.INPUT_Instrument ii 
	/*inner join description d on d.tmlid=ii.new_subfamilyid*/
	inner join instmodel im on im.tmlid=ii.instmodel_tmlid
	inner join description d on im.descriptionid=d.descriptionid
	inner join	workinstruction wi on wi.descid=d.descriptionid and wi.title=ii.localization3 
	WHERE  ii.record_toExclude=0 and ii.new_plantid is not null
	
COMMIT TRAN