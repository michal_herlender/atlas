-- Inserts PUSH of current manufacturer data to change queue as an example, and 
-- creates / configures three subscribers to these events
-- This is an example only for development/test system use
-- Galen Beck - 2020-05-26

USE [atlas]
GO

BEGIN TRAN

SET IDENTITY_INSERT [event_subscriber] ON

INSERT INTO [dbo].[event_subscriber]
           ([id],[model])
     VALUES
           (1,'Atlas Dev Test'),
		   (2,'Calypso Test'),
		   (3,'TAM Test')
GO

SET IDENTITY_INSERT [event_subscriber] OFF

-- Insert values for a PUSH of all mfr to change queue
-- Entity types : MFR - 0, MODEL, - 1, SUBFAMILY = 2
-- Change types : PUSH = 0, CREATE = 1, UPDATE = 2

SET IDENTITY_INSERT [change_queue] ON;

INSERT INTO [dbo].[change_queue]
           ([id]
		   ,[entitytype]
           ,[processed])
     SELECT [mfrid], 0 , 0 FROM [dbo].[mfr]
GO

SET IDENTITY_INSERT [change_queue] OFF;

INSERT INTO [dbo].[change_mfr]
           ([changeid]
           ,[changetype]
           ,[active]
           ,[genericmfr]
           ,[mfrid]
           ,[name]
           ,[tmlid])
	SELECT [mfrid]
	      ,0
		  ,[active]
		  ,[genericmfr]
		  ,[mfrid]
		  ,[name]
		  ,[tmlid]
	  FROM [dbo].[mfr]
GO

-- Events for each subscriber


INSERT INTO [dbo].[subscriber_queue]
           ([processed]
           ,[changeid]
           ,[subscriberid])
     SELECT 0
	  ,[id]
      ,1
     FROM [dbo].[change_queue]
GO

INSERT INTO [dbo].[subscriber_queue]
           ([processed]
           ,[changeid]
           ,[subscriberid])
     SELECT 0
	  ,[id]
      ,2
     FROM [dbo].[change_queue]
GO

INSERT INTO [dbo].[subscriber_queue]
           ([processed]
           ,[changeid]
           ,[subscriberid])
     SELECT 0
	  ,[id]
      ,3
     FROM [dbo].[change_queue]
GO

COMMIT TRAN