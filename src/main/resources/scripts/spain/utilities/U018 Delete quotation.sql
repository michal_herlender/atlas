declare @quoteNumber varchar(30) = 'QUOTE_NUMBER'
declare @quoteId int
declare @quoteItemIds table(id int)
declare @calibrationCostIds table(id int)
declare @purchaseCostIds table(id int)

begin tran

/* Determine quotation id */
select @quoteId = quotation.id
from quotation
where qno = @quoteNumber

/* Revert quotation request status */
update quotationrequest set quoteid = null, status = 'REQUESTED'
where quoteid = @quoteId

/* Delete quotation notes */
delete from quotenote
where quoteid = @quoteId

/* Delete prior quotation status */
delete from priorquotestatus
where quoteid = @quoteId

/* Delete all job item links */
delete from jobquotelink
where quoteid = @quoteId

/* Determine all quotation item ids */
insert into @quoteItemIds
select id from quotationitem
where quoteid = @quoteId

/* Determine all calibration cost ids */
insert into @calibrationCostIds
select calcost_id from quotationitem
where quoteid = @quoteId

/* Determine all purchase cost ids */
insert into @purchaseCostIds
select purchasecost_id from quotationitem
where quoteid = @quoteId

/* Delete quotation item notes */
delete from quoteitemnote
where id in (select id from @quoteItemIds)

/* Delete all quotation items */
delete from quotationitem
where quoteid = @quoteId

/* Delete all calibration costs */
delete from costs_calibration
where costid in (select id from @calibrationCostIds)

/* Delete all purchase costs */
delete from costs_purchase
where costid in (select id from @purchaseCostIds)

/* Remove default heading */
update quotation set defaultheading = null
where id = @quoteId

/* Delete quotation headings */
delete from quoteheading
where quoteid = @quoteId

/* Delete quotation */
delete from quotation
where id = @quoteId

rollback tran