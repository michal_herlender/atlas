-- This script clears out all client data from an Atlas database (should be ~ version 757)
-- Author : Galen Beck
-- See ticket DEV-2142 for details
-- For efficiency this script does not use a transaction
-- You may want to run in SIMPLE recovery mode to prevent log from filling up

USE [atlas]
GO

DELETE FROM dbo.additionalcostcontact; 
GO
PRINT 'Deleted dbo.additionalcostcontact';
DELETE [dbo].[addressfunction] FROM [dbo].[addressfunction] af
 JOIN address addr on addr.addrid = af.addressid
 JOIN subdiv sub on sub.subdivid = addr.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE [dbo].[addressplantillassettings] FROM [dbo].[addressplantillassettings] aps
 JOIN address addr on addr.addrid = aps.addressid
 JOIN subdiv sub on sub.subdivid = addr.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE [dbo].[addresssettingsforallocatedcompany] FROM [dbo].[addresssettingsforallocatedcompany] asac
 JOIN address addr on addr.addrid = asac.addressid
 JOIN subdiv sub on sub.subdivid = addr.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE [dbo].[addresssettingsforallocatedsubdiv] FROM [dbo].[addresssettingsforallocatedsubdiv] asas
 JOIN address addr on addr.addrid = asas.addressid
 JOIN subdiv sub on sub.subdivid = addr.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE FROM dbo.advesojobItemactivityqueue; 
GO
PRINT 'Deleted dbo.advesojobItemactivityqueue';
DELETE FROM dbo.asnitemanalysisresult; 
GO
PRINT 'Deleted dbo.asnitemanalysisresult';
DELETE FROM dbo.assetnote; 
GO
PRINT 'Deleted dbo.assetnote';
DELETE FROM dbo.backgroundtask; 
GO
PRINT 'Deleted dbo.backgroundtask';
DELETE FROM dbo.BATCH_STEP_EXECUTION_CONTEXT; 
GO
PRINT 'Deleted dbo.BATCH_STEP_EXECUTION_CONTEXT';
DELETE FROM dbo.BATCH_STEP_EXECUTION_SEQ; 
GO
PRINT 'Deleted dbo.BATCH_STEP_EXECUTION_SEQ';
DELETE FROM dbo.batchcalibration; 
GO
PRINT 'Deleted dbo.batchcalibration';
DELETE [dbo].[businesssubdivisionsettings] 
 FROM [dbo].[businesssubdivisionsettings] bss
 JOIN subdiv sub on sub.subdivid = bss.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5;
GO

DELETE FROM dbo.calibrationactivitylink; 
GO
PRINT 'Deleted dbo.calibrationactivitylink';
DELETE FROM dbo.calibrationpoint; 
GO
PRINT 'Deleted dbo.calibrationpoint';
DELETE FROM dbo.callink; 
GO
PRINT 'Deleted dbo.callink';
DELETE FROM dbo.calloffitem; 
GO
PRINT 'Deleted dbo.calloffitem';
DELETE FROM dbo.calreqhistory; 
GO
PRINT 'Deleted dbo.calreqhistory';
DELETE FROM dbo.capabilityfilter; 
GO
PRINT 'Deleted dbo.capabilityfilter';
DELETE FROM dbo.certaction; 
GO
PRINT 'Deleted dbo.certaction';
DELETE FROM dbo.certificatevalidation; 
GO
PRINT 'Deleted dbo.certificatevalidation';
DELETE FROM dbo.certlink; 
GO
PRINT 'Deleted dbo.certlink';
DELETE FROM dbo.certnote; 
GO
PRINT 'Deleted dbo.certnote';
DELETE FROM dbo.closeddate; 
GO
PRINT 'Deleted dbo.closeddate';
DELETE FROM dbo.collectedinstrument; 
GO
PRINT 'Deleted dbo.collectedinstrument';
DELETE [dbo].[companyaccreditation] FROM [dbo].[companyaccreditation] ca
 JOIN company comp on comp.coid = ca.coid
 WHERE comp.corole <> 5
GO
DELETE [dbo].[companyhistory] FROM [dbo].[companyhistory] ch
 JOIN company comp on comp.coid = ch.coid
 WHERE comp.corole <> 5
GO
DELETE FROM dbo.companyinstructionlink; 
GO
PRINT 'Deleted dbo.companyinstructionlink';
DELETE FROM dbo.companylistmember; 
GO
PRINT 'Deleted dbo.companylistmember';
DELETE FROM dbo.companylistusage; 
GO
PRINT 'Deleted dbo.companylistusage';
DELETE [dbo].[companysettingsforallocatedcompany] FROM [dbo].[companysettingsforallocatedcompany] csac
 JOIN company comp on comp.coid = csac.companyid
 WHERE comp.corole <> 5
GO
DELETE FROM dbo.contactsettingsforallocatedcompany; 
GO
PRINT 'Deleted dbo.contactsettingsforallocatedcompany';
DELETE FROM dbo.contract_instructions; 
GO
PRINT 'Deleted dbo.contract_instructions';
DELETE FROM dbo.contractdemand; 
GO
PRINT 'Deleted dbo.contractdemand';
DELETE FROM dbo.contractreviewlinkedcalibrationcost; 
GO
PRINT 'Deleted dbo.contractreviewlinkedcalibrationcost';
DELETE FROM dbo.contractreviewlinkedpurchasecost; 
GO
PRINT 'Deleted dbo.contractreviewlinkedpurchasecost';
DELETE FROM dbo.contractservicetypesettings; 
GO
PRINT 'Deleted dbo.contractservicetypesettings';
DELETE FROM dbo.creditcard; 
GO
PRINT 'Deleted dbo.creditcard';
DELETE FROM dbo.creditnoteaction; 
GO
PRINT 'Deleted dbo.creditnoteaction';
DELETE FROM dbo.creditnoteitem; 
GO
PRINT 'Deleted dbo.creditnoteitem';
DELETE FROM dbo.creditnotenote; 
GO
PRINT 'Deleted dbo.creditnotenote';
DELETE FROM dbo.customquotationcalibrationcondition; 
GO
PRINT 'Deleted dbo.customquotationcalibrationcondition';
DELETE FROM dbo.customquotationgeneralcondition; 
GO
PRINT 'Deleted dbo.customquotationgeneralcondition';
DELETE FROM dbo.defaultstandard; 
GO
PRINT 'Deleted dbo.defaultstandard';
DELETE FROM dbo.deletedcomponent; 
GO
PRINT 'Deleted dbo.deletedcomponent';
DELETE FROM dbo.deliveryitemaccessory; 
GO
PRINT 'Deleted dbo.deliveryitemaccessory';
DELETE FROM dbo.deliveryitemnote; 
GO
PRINT 'Deleted dbo.deliveryitemnote';
DELETE FROM dbo.deliverynote; 
GO
PRINT 'Deleted dbo.deliverynote';
DELETE FROM dbo.demandmodel; 
GO
PRINT 'Deleted dbo.demandmodel';
DELETE FROM dbo.emailrecipient; 
GO
PRINT 'Deleted dbo.emailrecipient';
DELETE FROM dbo.engineerallocationmessage; 
GO
PRINT 'Deleted dbo.engineerallocationmessage';
DELETE FROM dbo.exception; 
GO
PRINT 'Deleted dbo.exception';
DELETE dbo.exchangeformatfieldnamedetails FROM dbo.exchangeformatfieldnamedetails effnd 
 JOIN exchangeformat ef on ef.id = effnd.exchangeFormatid
 WHERE ef.eftype <> 'GLOBAL'
GO
DELETE FROM dbo.failedLoginAttempt; 
GO
PRINT 'Deleted dbo.failedLoginAttempt';
DELETE FROM dbo.faultreportaction; 
GO
PRINT 'Deleted dbo.faultreportaction';
DELETE FROM dbo.faultreportdescription; 
GO
PRINT 'Deleted dbo.faultreportdescription';
DELETE FROM dbo.faultreportinstruction; 
GO
PRINT 'Deleted dbo.faultreportinstruction';
DELETE FROM dbo.faultreportrecommendation; 
GO
PRINT 'Deleted dbo.faultreportrecommendation';
DELETE FROM dbo.faultreportstate; 
GO
PRINT 'Deleted dbo.faultreportstate';
DELETE FROM dbo.groupaccessory; 
GO
PRINT 'Deleted dbo.groupaccessory';
DELETE FROM dbo.gsodocumentlink; 
GO
PRINT 'Deleted dbo.gsodocumentlink';
DELETE FROM dbo.gsolink; 
GO
PRINT 'Deleted dbo.gsolink';
DELETE FROM dbo.hireitemaccessory; 
GO
PRINT 'Deleted dbo.hireitemaccessory';
DELETE FROM dbo.hirenote; 
GO
PRINT 'Deleted dbo.hirenote';
DELETE FROM dbo.INPUT_Address; 
GO
PRINT 'Deleted dbo.INPUT_Address';
DELETE FROM dbo.INPUT_BusinessCompany; 
GO
PRINT 'Deleted dbo.INPUT_BusinessCompany';
DELETE FROM dbo.INPUT_Certificate; 
GO
PRINT 'Deleted dbo.INPUT_Certificate';
DELETE FROM dbo.INPUT_Company; 
GO
PRINT 'Deleted dbo.INPUT_Company';
DELETE FROM dbo.INPUT_CompanyInstruction; 
GO
PRINT 'Deleted dbo.INPUT_CompanyInstruction';
DELETE FROM dbo.INPUT_Contact; 
GO
PRINT 'Deleted dbo.INPUT_Contact';
DELETE FROM dbo.INPUT_ContactOnly; 
GO
PRINT 'Deleted dbo.INPUT_ContactOnly';
DELETE FROM dbo.INPUT_Courier; 
GO
PRINT 'Deleted dbo.INPUT_Courier';
DELETE FROM dbo.INPUT_CourierDespatchType; 
GO
PRINT 'Deleted dbo.INPUT_CourierDespatchType';
DELETE FROM dbo.INPUT_DataMigration; 
GO
PRINT 'Deleted dbo.INPUT_DataMigration';
DELETE FROM dbo.INPUT_InstrCRIS; 
GO
PRINT 'Deleted dbo.INPUT_InstrCRIS';
DELETE FROM dbo.INPUT_Instrument; 
GO
PRINT 'Deleted dbo.INPUT_Instrument';
DELETE FROM dbo.INPUT_InstrumentOnly; 
GO
PRINT 'Deleted dbo.INPUT_InstrumentOnly';
DELETE FROM dbo.INPUT_Location; 
GO
PRINT 'Deleted dbo.INPUT_Location';
DELETE FROM dbo.INPUT_MatchingBusinessComp; 
GO
PRINT 'Deleted dbo.INPUT_MatchingBusinessComp';
DELETE FROM dbo.INPUT_MatchingFamily; 
GO
PRINT 'Deleted dbo.INPUT_MatchingFamily';
DELETE FROM dbo.INPUT_MatchingMfrModel; 
GO
PRINT 'Deleted dbo.INPUT_MatchingMfrModel';
DELETE FROM dbo.INPUT_MfrAlias; 
GO
PRINT 'Deleted dbo.INPUT_MfrAlias';
DELETE FROM dbo.INPUT_MfrModelAlias; 
GO
PRINT 'Deleted dbo.INPUT_MfrModelAlias';
DELETE FROM dbo.INPUT_Quotation; 
GO
PRINT 'Deleted dbo.INPUT_Quotation';
DELETE FROM dbo.INPUT_Subdivision; 
GO
PRINT 'Deleted dbo.INPUT_Subdivision';
DELETE FROM dbo.INPUT_SubdivisionInstruction; 
GO
PRINT 'Deleted dbo.INPUT_SubdivisionInstruction';
DELETE FROM dbo.INPUT_SubdivisionSettings; 
GO
PRINT 'Deleted dbo.INPUT_SubdivisionSettings';
DELETE FROM dbo.INPUT_ToolsMatchingInstr; 
GO
PRINT 'Deleted dbo.INPUT_ToolsMatchingInstr';
DELETE FROM dbo.INPUT_UpdateInstrModel; 
GO
PRINT 'Deleted dbo.INPUT_UpdateInstrModel';
DELETE FROM dbo.INPUT_UpdateLinkInstrModel; 
GO
PRINT 'Deleted dbo.INPUT_UpdateLinkInstrModel';
DELETE FROM dbo.instcertlink; 
GO
PRINT 'Deleted dbo.instcertlink';
DELETE FROM dbo.instrument_contract; 
GO
PRINT 'Deleted dbo.instrument_contract';
DELETE FROM dbo.instrumentcharacteristic; 
GO
PRINT 'Deleted dbo.instrumentcharacteristic';
DELETE FROM dbo.instrumentcomplementaryfield; 
GO
PRINT 'Deleted dbo.instrumentcomplementaryfield';
DELETE FROM dbo.instrumenthistory; 
GO
PRINT 'Deleted dbo.instrumenthistory';
DELETE FROM dbo.instrumentnote; 
GO
PRINT 'Deleted dbo.instrumentnote';
DELETE FROM dbo.instrumentvalidationaction; 
GO
PRINT 'Deleted dbo.instrumentvalidationaction';
DELETE FROM dbo.instrumentvalidationissue; 
GO
PRINT 'Deleted dbo.instrumentvalidationissue';
DELETE FROM dbo.instrumentvalueboolean; 
GO
PRINT 'Deleted dbo.instrumentvalueboolean';
DELETE FROM dbo.instrumentvaluedatetime; 
GO
PRINT 'Deleted dbo.instrumentvaluedatetime';
DELETE FROM dbo.instrumentvaluenumeric; 
GO
PRINT 'Deleted dbo.instrumentvaluenumeric';
DELETE FROM dbo.instrumentvalueselection; 
GO
PRINT 'Deleted dbo.instrumentvalueselection';
DELETE FROM dbo.instrumentvaluestring; 
GO
PRINT 'Deleted dbo.instrumentvaluestring';
DELETE FROM dbo.instrumentworkinstruction; 
GO
PRINT 'Deleted dbo.instrumentworkinstruction';
DELETE FROM dbo.invoiceactionlist; 
GO
PRINT 'Deleted dbo.invoiceactionlist';
DELETE FROM dbo.invoicedelivery; 
GO
PRINT 'Deleted dbo.invoicedelivery';
DELETE FROM dbo.invoiceitempo; 
GO
PRINT 'Deleted dbo.invoiceitempo';
DELETE FROM dbo.invoicejoblink; 
GO
PRINT 'Deleted dbo.invoicejoblink';
DELETE FROM dbo.invoicenote; 
GO
PRINT 'Deleted dbo.invoicenote';
DELETE FROM dbo.invoicepo; 
GO
PRINT 'Deleted dbo.invoicepo';
DELETE FROM dbo.itemaccessory; 
GO
PRINT 'Deleted dbo.itemaccessory';
DELETE FROM dbo.itemanalysisresult; 
GO
PRINT 'Deleted dbo.itemanalysisresult';
DELETE FROM dbo.jobbpolink; 
GO
PRINT 'Deleted dbo.jobbpolink';
DELETE FROM dbo.jobcostingclientapproval; 
GO
PRINT 'Deleted dbo.jobcostingclientapproval';
DELETE FROM dbo.jobcostingexpenseitem; 
GO
PRINT 'Deleted dbo.jobcostingexpenseitem';
DELETE FROM dbo.jobcostingitemnote; 
GO
PRINT 'Deleted dbo.jobcostingitemnote';
DELETE FROM dbo.jobcostingitemremark; 
GO
PRINT 'Deleted dbo.jobcostingitemremark';
DELETE FROM dbo.jobcostinglinkedadjustmentcost; 
GO
PRINT 'Deleted dbo.jobcostinglinkedadjustmentcost';
DELETE FROM dbo.jobcostinglinkedcalibrationcost; 
GO
PRINT 'Deleted dbo.jobcostinglinkedcalibrationcost';
DELETE FROM dbo.jobcostinglinkedpurchasecost; 
GO
PRINT 'Deleted dbo.jobcostinglinkedpurchasecost';
DELETE FROM dbo.jobcostinglinkedrepaircost; 
GO
PRINT 'Deleted dbo.jobcostinglinkedrepaircost';
DELETE FROM dbo.jobcostingnote; 
GO
PRINT 'Deleted dbo.jobcostingnote';
DELETE FROM dbo.jobcostingviewed; 
GO
PRINT 'Deleted dbo.jobcostingviewed';
DELETE FROM dbo.jobcourierlink; 
GO
PRINT 'Deleted dbo.jobcourierlink';
DELETE FROM dbo.jobexpenseitemnotinvoiced; 
GO
PRINT 'Deleted dbo.jobexpenseitemnotinvoiced';
DELETE FROM dbo.jobexpenseitempo; 
GO
PRINT 'Deleted dbo.jobexpenseitempo';
DELETE FROM dbo.jobinstructionlink; 
GO
PRINT 'Deleted dbo.jobinstructionlink';
DELETE FROM dbo.jobitemaction; 
GO
PRINT 'Deleted dbo.jobitemaction';
DELETE FROM dbo.jobitemdemand; 
GO
PRINT 'Deleted dbo.jobitemdemand';
DELETE FROM dbo.jobitemhistory; 
GO
PRINT 'Deleted dbo.jobitemhistory';
DELETE FROM dbo.jobitemnote; 
GO
PRINT 'Deleted dbo.jobitemnote';
DELETE FROM dbo.jobitemnotinvoiced; 
GO
PRINT 'Deleted dbo.jobitemnotinvoiced';
DELETE FROM dbo.jobitempo; 
GO
PRINT 'Deleted dbo.jobitempo';
DELETE FROM dbo.jobnote; 
GO
PRINT 'Deleted dbo.jobnote';
DELETE FROM dbo.jobquotelink; 
GO
PRINT 'Deleted dbo.jobquotelink';
DELETE FROM dbo.login; 
GO
PRINT 'Deleted dbo.login';
DELETE FROM dbo.mailgroupmember; 
GO
PRINT 'Deleted dbo.mailgroupmember';
DELETE FROM dbo.measurementpossibility; 
GO
PRINT 'Deleted dbo.measurementpossibility';
DELETE FROM dbo.modelnote; 
GO
PRINT 'Deleted dbo.modelnote';
UPDATE dbo.calreq SET rangeid=null WHERE rangeid IS NOT NULL;
DELETE FROM dbo.modelrange WHERE type <> 'model';
GO
PRINT 'Deleted from modelrange';
DELETE FROM dbo.nominalinvoicecost; 
GO
PRINT 'Deleted dbo.nominalinvoicecost';
DELETE FROM dbo.notificationsystemfieldchange; 
GO
PRINT 'Deleted dbo.notificationsystemfieldchange';
DELETE FROM dbo.notificationsystemqueue; 
GO
PRINT 'Deleted dbo.notificationsystemqueue';
DELETE FROM dbo.oauth2accesstoken; 
GO
PRINT 'Deleted dbo.oauth2accesstoken';
DELETE FROM dbo.oauth2clientdetails; 
GO
PRINT 'Deleted dbo.oauth2clientdetails';
DELETE FROM dbo.oauth2refreshtoken; 
GO
PRINT 'Deleted dbo.oauth2refreshtoken';
DELETE FROM dbo.onbehalfitem; 
GO
PRINT 'Deleted dbo.onbehalfitem';
DELETE FROM dbo.pat; 
GO
PRINT 'Deleted dbo.pat';
DELETE FROM dbo.permitedcalibrationextension; 
GO
PRINT 'Deleted dbo.permitedcalibrationextension';
DELETE FROM dbo.pointsettemplate; 
GO
PRINT 'Deleted dbo.pointsettemplate';
UPDATE dbo.po SET prebookingdetailsid = null where prebookingdetailsid is not null;
DELETE FROM dbo.prebookingjobdetails; 
GO
PRINT 'Deleted dbo.prebookingjobdetails';

DELETE FROM dbo.priorquotestatus; 
GO
PRINT 'Deleted dbo.priorquotestatus';
DELETE FROM dbo.procedureaccreditation; 
GO
PRINT 'Deleted dbo.procedureaccreditation';
DELETE FROM dbo.proceduretrainingrecord; 
GO
PRINT 'Deleted dbo.proceduretrainingrecord';
DELETE FROM dbo.purchaseorderactionlist; 
GO
PRINT 'Deleted dbo.purchaseorderactionlist';
DELETE FROM dbo.purchaseorderitemnote; 
GO
PRINT 'Deleted dbo.purchaseorderitemnote';
DELETE FROM dbo.purchaseorderitemprogressaction; 
GO
PRINT 'Deleted dbo.purchaseorderitemprogressaction';
DELETE FROM dbo.purchaseorderjobitem; 
GO
PRINT 'Deleted dbo.purchaseorderjobitem';
DELETE FROM dbo.purchaseordernote; 
GO
PRINT 'Deleted dbo.purchaseordernote';
DELETE FROM dbo.queuedcalibration; 
GO
PRINT 'Deleted dbo.queuedcalibration';
DELETE FROM dbo.quickpurchaseorder; 
GO
PRINT 'Deleted dbo.quickpurchaseorder';
DELETE FROM dbo.quotationdoccustomtext; 
GO
PRINT 'Deleted dbo.quotationdoccustomtext';
DELETE FROM dbo.quotationitempartof; 
GO
PRINT 'Deleted dbo.quotationitempartof';
DELETE FROM dbo.quotationrequest; 
GO
PRINT 'Deleted dbo.quotationrequest';
DELETE FROM dbo.quotecaldefaults; 
GO
PRINT 'Deleted dbo.quotecaldefaults';
DELETE FROM dbo.quotecaltypedefault; 
GO
PRINT 'Deleted dbo.quotecaltypedefault';
DELETE FROM dbo.quoteitemnote; 
GO
PRINT 'Deleted dbo.quoteitemnote';
DELETE FROM dbo.quotenote; 
GO
PRINT 'Deleted dbo.quotenote';
DELETE FROM dbo.recallcompanyconfiguration; 
GO
PRINT 'Deleted dbo.recallcompanyconfiguration';
DELETE FROM dbo.recallitem; 
GO
PRINT 'Deleted dbo.recallitem';
DELETE FROM dbo.recallnote; 
GO
PRINT 'Deleted dbo.recallnote';
DELETE FROM dbo.recallresponsenote; 
GO
PRINT 'Deleted dbo.recallresponsenote';
DELETE FROM dbo.recallrule; 
GO
PRINT 'Deleted dbo.recallrule';
DELETE FROM dbo.repeatschedule; 
GO
PRINT 'Deleted dbo.repeatschedule';
DELETE FROM dbo.requirement; 
GO
PRINT 'Deleted dbo.requirement';
DELETE FROM dbo.saledisc; 
GO
PRINT 'Deleted dbo.saledisc';
DELETE FROM dbo.scheduleddelivery; 
GO
PRINT 'Deleted dbo.scheduleddelivery';
DELETE FROM dbo.scheduledquotationrequestdaterange; 
GO
PRINT 'Deleted dbo.scheduledquotationrequestdaterange';
DELETE FROM dbo.scheduleequipment; 
GO
PRINT 'Deleted dbo.scheduleequipment';
DELETE FROM dbo.schedulenote; 
GO
PRINT 'Deleted dbo.schedulenote';
DELETE FROM dbo.servicecapability; 
GO
PRINT 'Deleted dbo.servicecapability';
DELETE FROM dbo.singletransit; 
GO
PRINT 'Deleted dbo.singletransit';
DELETE FROM dbo.standardused; 
GO
PRINT 'Deleted dbo.standardused';
DELETE FROM dbo.subdiv_contract; 
GO
PRINT 'Deleted dbo.subdiv_contract';
DELETE FROM dbo.subdivinstructionlink; 
GO
PRINT 'Deleted dbo.subdivinstructionlink';
DELETE FROM dbo.subdivsettingsforallocatedcompany; 
GO
PRINT 'Deleted dbo.subdivsettingsforallocatedcompany';
DELETE [dbo].[subdivsettingsforallocatedsubdiv] FROM [dbo].[subdivsettingsforallocatedsubdiv] ssas
 JOIN subdiv sub on sub.subdivid = ssas.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE FROM dbo.subscriber_queue; 
GO
PRINT 'Deleted dbo.subscriber_queue';
UPDATE purchaseorderitem SET supplierinvoiceid = null WHERE supplierinvoiceid IS NOT NULL;
DELETE FROM dbo.supplierinvoice; 
GO
PRINT 'Deleted dbo.supplierinvoice';
DELETE FROM dbo.systemdefaultapplication; 
GO
PRINT 'Deleted dbo.systemdefaultapplication';
DELETE FROM dbo.taggedimage; 
GO
PRINT 'Deleted dbo.taggedimage';
DELETE FROM dbo.templatepoint; 
GO
PRINT 'Deleted dbo.templatepoint';
DELETE FROM dbo.timesheetvalidation; 
GO
PRINT 'Deleted dbo.timesheetvalidation';
DELETE FROM dbo.TML; 
GO
PRINT 'Deleted dbo.TML';
DELETE FROM dbo.TMLFULL_CharacteristicProp; 
GO
PRINT 'Deleted dbo.TMLFULL_CharacteristicProp';
DELETE FROM dbo.TMLFULL_CharacteristicReg; 
GO
PRINT 'Deleted dbo.TMLFULL_CharacteristicReg';
DELETE FROM dbo.TMLFULL_DomainProp; 
GO
PRINT 'Deleted dbo.TMLFULL_DomainProp';
DELETE FROM dbo.TMLFULL_DomainReg; 
GO
PRINT 'Deleted dbo.TMLFULL_DomainReg';
DELETE FROM dbo.TMLFULL_FamilyProp; 
GO
PRINT 'Deleted dbo.TMLFULL_FamilyProp';
DELETE FROM dbo.TMLFULL_FamilyReg; 
GO
PRINT 'Deleted dbo.TMLFULL_FamilyReg';
DELETE FROM dbo.TMLFULL_ManufacturerProp; 
GO
PRINT 'Deleted dbo.TMLFULL_ManufacturerProp';
DELETE FROM dbo.TMLFULL_ModelCharacteristicProp; 
GO
PRINT 'Deleted dbo.TMLFULL_ModelCharacteristicProp';
DELETE FROM dbo.TMLFULL_ModelProp; 
GO
PRINT 'Deleted dbo.TMLFULL_ModelProp';
DELETE FROM dbo.TMLFULL_OptionProp; 
GO
PRINT 'Deleted dbo.TMLFULL_OptionProp';
DELETE FROM dbo.TMLFULL_OptionReg; 
GO
PRINT 'Deleted dbo.TMLFULL_OptionReg';
DELETE FROM dbo.TMLFULL_SubFamilyProp; 
GO
PRINT 'Deleted dbo.TMLFULL_SubFamilyProp';
DELETE FROM dbo.TMLFULL_SubFamilyReg; 
GO
PRINT 'Deleted dbo.TMLFULL_SubFamilyReg';
DELETE FROM dbo.TMLFULL_UnitProp; 
GO
PRINT 'Deleted dbo.TMLFULL_UnitProp';
DELETE FROM dbo.TMLFULL_UnitReg; 
GO
PRINT 'Deleted dbo.TMLFULL_UnitReg';
DELETE FROM dbo.tpinstruction; 
GO
PRINT 'Deleted dbo.tpinstruction';
DELETE FROM dbo.tpquotationcaltypedefault; 
GO
PRINT 'Deleted dbo.tpquotationcaltypedefault';
DELETE FROM dbo.tpquotationdefaultcosttype; 
GO
PRINT 'Deleted dbo.tpquotationdefaultcosttype';
DELETE FROM dbo.tpquoteitemnote; 
GO
PRINT 'Deleted dbo.tpquoteitemnote';
DELETE FROM dbo.tpquotelink; 
GO
PRINT 'Deleted dbo.tpquotelink';
DELETE FROM dbo.tpquotenote; 
GO
PRINT 'Deleted dbo.tpquotenote';
DELETE FROM dbo.tpquoterequestdefaultcosttype; 
GO
PRINT 'Deleted dbo.tpquoterequestdefaultcosttype';
DELETE FROM dbo.tpquoterequestedcosttype; 
GO
PRINT 'Deleted dbo.tpquoterequestedcosttype';
DELETE FROM dbo.tpquoterequestitemnote; 
GO
PRINT 'Deleted dbo.tpquoterequestitemnote';
DELETE FROM dbo.tpquoterequestnote; 
GO
PRINT 'Deleted dbo.tpquoterequestnote';
DELETE FROM dbo.upcomingwork; 
GO
PRINT 'Deleted dbo.upcomingwork';
DELETE [dbo].[usergrouppropertyvalue] FROM [dbo].[usergrouppropertyvalue] ugpv
 JOIN usergroup ug on ug.groupid = ugpv.groupid
 JOIN company comp on comp.coid = ug.coid
 WHERE comp.corole <> 5
GO
DELETE FROM dbo.userinstructiontype; 
GO
PRINT 'Deleted dbo.userinstructiontype';
DELETE [dbo].[userpreferences] FROM [dbo].[userpreferences] up
 JOIN contact con on con.personid = up.contactid
 JOIN subdiv sub on sub.subdivid = con.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE [dbo].[userrole] FROM [dbo].[userrole] ur
 JOIN users users on users.username = ur.userName
 JOIN contact con on con.personid = users.personid
 JOIN subdiv sub on sub.subdivid = con.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE FROM dbo.validatedfreerepaircomponent; 
GO
PRINT 'Deleted dbo.validatedfreerepaircomponent';
DELETE FROM dbo.validatedfreerepairoperation; 
GO
PRINT 'Deleted dbo.validatedfreerepairoperation';
DELETE FROM dbo.asnitem; 
GO
PRINT 'Deleted dbo.asnitem';
DELETE FROM dbo.asset; 
GO
PRINT 'Deleted dbo.asset';
DELETE dbo.bankaccount FROM dbo.bankaccount ba
 JOIN company comp on comp.coid = ba.companyid
 WHERE comp.corole <> 5
GO
DELETE FROM dbo.BATCH_STEP_EXECUTION; 
GO
PRINT 'Deleted dbo.BATCH_STEP_EXECUTION';
UPDATE calreq SET pointsetid = null WHERE pointsetid IS NOT NULL;
DELETE FROM dbo.calibrationpointset; 
GO
PRINT 'Deleted dbo.calibrationpointset';
DELETE FROM dbo.certificate; 
GO
PRINT 'Deleted dbo.certificate';
DELETE FROM dbo.companylist; 
GO
PRINT 'Deleted dbo.companylist';
DELETE FROM dbo.contactinstructionlink; 
GO
PRINT 'Deleted dbo.contactinstructionlink';
DELETE FROM dbo.contractreviewitem; 
GO
PRINT 'Deleted dbo.contractreviewitem';
DELETE FROM dbo.creditnote; 
GO
PRINT 'Deleted dbo.creditnote';
DELETE FROM dbo.deliveryitem; 
GO
PRINT 'Deleted dbo.deliveryitem';
DELETE FROM dbo.email; 
GO
PRINT 'Deleted dbo.email';
DELETE FROM dbo.engineerallocation; 
GO
PRINT 'Deleted dbo.engineerallocation';
UPDATE dbo.jobitem SET faultreportid = NULL WHERE faultreportid IS NOT NULL;
DELETE FROM dbo.faultreport; 
GO
PRINT 'Deleted dbo.faultreport';
DELETE FROM dbo.freerepaircomponent; 
GO
PRINT 'Deleted dbo.freerepaircomponent';
DELETE FROM dbo.freerepairoperation; 
GO
PRINT 'Deleted dbo.freerepairoperation';
DELETE FROM dbo.gsodocument; 
GO
PRINT 'Deleted dbo.gsodocument';
DELETE FROM dbo.hiresuspenditem; 
GO
PRINT 'Deleted dbo.hiresuspenditem';
DELETE FROM dbo.images; 
GO
PRINT 'Deleted dbo.images';
DELETE FROM dbo.instrumentfieldlibrary; 
GO
PRINT 'Deleted dbo.instrumentfieldlibrary';
DELETE FROM dbo.instrumentvalue; 
GO
PRINT 'Deleted dbo.instrumentvalue';
DELETE FROM dbo.invoiceitem; 
GO
PRINT 'Deleted dbo.invoiceitem';
DELETE FROM dbo.jobcostingitem; 
GO
PRINT 'Deleted dbo.jobcostingitem';
DELETE FROM dbo.jobexpenseitem; 
GO
PRINT 'Deleted dbo.jobexpenseitem';
UPDATE dbo.jobitem SET nextworkreqid = null WHERE nextworkreqid IS NOT NULL;
DELETE FROM dbo.jobitemworkrequirement; 
GO
PRINT 'Deleted dbo.jobitemworkrequirement';
UPDATE dbo.job set defaultpoid = null WHERE defaultpoid IS NOT NULL;
DELETE FROM dbo.po; 
GO
PRINT 'Deleted dbo.po';
DELETE FROM dbo.prebookingfile; 
GO
PRINT 'Deleted dbo.prebookingfile';
UPDATE dbo.tpquoterequestitem SET quoteitemid = NULL WHERE quoteitemid IS NOT NULL;
DELETE FROM dbo.quotationitem; 
GO
PRINT 'Deleted dbo.quotationitem';
DELETE FROM dbo.recalldetail; 
GO
PRINT 'Deleted dbo.recalldetail';
DELETE FROM dbo.repairinspectionreport; 
GO
PRINT 'Deleted dbo.repairinspectionreport';
UPDATE dbo.delivery SET scheduleid = null WHERE scheduleid IS NOT NULL;
DELETE FROM dbo.schedule; 
GO
PRINT 'Deleted dbo.schedule';
DELETE FROM dbo.scheduledquotationrequest; 
GO
PRINT 'Deleted dbo.scheduledquotationrequest';
DELETE FROM dbo.timesheetentry; 
GO
PRINT 'Deleted dbo.timesheetentry';
DELETE FROM dbo.tpquotationitem; 
GO
PRINT 'Deleted dbo.tpquotationitem';
DELETE FROM dbo.tpquoterequestitem; 
GO
PRINT 'Deleted dbo.tpquoterequestitem';
DELETE FROM dbo.tprequirement; 
GO
PRINT 'Deleted dbo.tprequirement';
DELETE [dbo].[users] FROM [dbo].[users] users
 JOIN contact con on con.personid = users.personid
 JOIN subdiv sub on sub.subdivid = con.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
UPDATE calibration SET workInstructionId = null WHERE workInstructionId IS NOT NULL;
UPDATE procs SET defworkinstructionid = null WHERE defworkinstructionid IS NOT NULL;
DELETE FROM dbo.workinstruction; 
GO
PRINT 'Deleted dbo.workinstruction';
DELETE FROM dbo.workrequirement; 
GO
PRINT 'Deleted dbo.workrequirement';
DELETE FROM dbo.asn; 
GO
PRINT 'Deleted dbo.asn';
DELETE FROM dbo.baseinstruction; 
GO
PRINT 'Deleted dbo.baseinstruction';
DELETE FROM dbo.BATCH_JOB_EXECUTION_CONTEXT; 
GO
PRINT 'Deleted dbo.BATCH_JOB_EXECUTION_CONTEXT';
DELETE FROM dbo.BATCH_JOB_EXECUTION_PARAMS; 
GO
PRINT 'Deleted dbo.BATCH_JOB_EXECUTION_PARAMS';
DELETE FROM dbo.BATCH_JOB_EXECUTION_SEQ; 
GO
PRINT 'Deleted dbo.BATCH_JOB_EXECUTION_SEQ';
UPDATE dbo.job SET bpoid = null WHERE bpoid IS NOT NULL;
UPDATE dbo.contract SET bpoid = null WHERE bpoid IS NOT NULL;
DELETE FROM dbo.bpo; 
GO
PRINT 'Deleted dbo.bpo';
UPDATE dbo.instrument SET lastcalid = null WHERE lastcalid IS NOT NULL;
DELETE FROM dbo.calibration; 
GO
PRINT 'Deleted dbo.calibration';

UPDATE dbo.jobitem SET contractid = null WHERE contractid IS NOT NULL;
DELETE FROM dbo.contract; 
GO
PRINT 'Deleted dbo.contract';
DELETE FROM dbo.contractreview; 
GO
PRINT 'Deleted dbo.contractreview';
DELETE FROM dbo.delivery; 
GO
PRINT 'Deleted dbo.delivery';
DELETE FROM dbo.exchangeformatfile; 
GO
PRINT 'Deleted dbo.exchangeformatfile';
DELETE FROM dbo.generalserviceoperation; 
GO
PRINT 'Deleted dbo.generalserviceoperation';
DELETE FROM dbo.hireitem; 
GO
PRINT 'Deleted dbo.hireitem';
DELETE FROM dbo.instrumentfielddefinition; 
GO
PRINT 'Deleted dbo.instrumentfielddefinition';
UPDATE purchaseorderitem SET internalinvoiceid = null WHERE internalinvoiceid IS NOT NULL;
DELETE FROM dbo.invoice; 
GO
PRINT 'Deleted dbo.invoice';
DELETE FROM dbo.jobcosting; 
GO
PRINT 'Deleted dbo.jobcosting';
DELETE FROM dbo.purchaseorderitem; 
GO
PRINT 'Deleted dbo.purchaseorderitem';
UPDATE dbo.quotation SET defaultheading=null WHERE defaultheading IS NOT NULL;
DELETE FROM dbo.quoteheading; 
GO
PRINT 'Deleted dbo.quoteheading';
DELETE FROM dbo.recall; 
GO
PRINT 'Deleted dbo.recall';
DELETE FROM dbo.repaircompletionreport; 
GO
PRINT 'Deleted dbo.repaircompletionreport';
DELETE FROM dbo.tpquotation; 
GO
PRINT 'Deleted dbo.tpquotation';
DELETE FROM dbo.tpquoterequest; 
GO
PRINT 'Deleted dbo.tpquoterequest';
DELETE FROM dbo.BATCH_JOB_EXECUTION; 
GO
PRINT 'Deleted dbo.BATCH_JOB_EXECUTION';
DELETE FROM dbo.BATCH_JOB_INSTANCE; 
GO
PRINT 'Deleted dbo.BATCH_JOB_INSTANCE';
DELETE FROM dbo.BATCH_JOB_SEQ; 
GO
PRINT 'Deleted dbo.BATCH_JOB_SEQ';
UPDATE hire SET courierdespatchid = null WHERE courierdespatchid IS NOT NULL;
DELETE FROM dbo.courierdespatch; 
GO
PRINT 'Deleted dbo.courierdespatch';
DELETE dbo.exchangeformat FROM dbo.exchangeformat ef
 WHERE ef.eftype <> 'GLOBAL'
GO
DELETE FROM dbo.freehandcontact; 
GO
PRINT 'Deleted dbo.freehandcontact';
DELETE FROM dbo.hireaccessory; 
GO
PRINT 'Deleted dbo.hireaccessory';
DELETE FROM dbo.hirecategory; 
GO
PRINT 'Deleted dbo.hirecategory';
DELETE FROM dbo.hirecrossitem; 
GO
PRINT 'Deleted dbo.hirecrossitem';
DELETE FROM dbo.hireinstrument; 
GO
PRINT 'Deleted dbo.hireinstrument';
DELETE FROM dbo.hirereplacedinstrument; 
GO
PRINT 'Deleted dbo.hirereplacedinstrument';
DELETE FROM dbo.hiresection; 
GO
PRINT 'Deleted dbo.hiresection';
DELETE FROM dbo.purchaseorder; 
GO
PRINT 'Deleted dbo.purchaseorder';
DELETE FROM dbo.quotation; 
GO
PRINT 'Deleted dbo.quotation';
DELETE FROM dbo.calreq; 
GO
PRINT 'Deleted dbo.calreq';
DELETE FROM dbo.hire; 
GO
PRINT 'Deleted dbo.hire';
DELETE FROM dbo.hiremodel; 
GO
PRINT 'Deleted dbo.hiremodel';
DELETE FROM dbo.hiremodelincategory; 
GO
PRINT 'Deleted dbo.hiremodelincategory';
UPDATE dbo.instrument SET scrappedonji=null WHERE scrappedonji IS NOT NULL;
DELETE FROM dbo.jobitem; 
GO
PRINT 'Deleted dbo.jobitem';
DELETE FROM dbo.jobitemgroup; 
GO
PRINT 'Deleted dbo.jobitemgroup';
DELETE FROM dbo.costs_adjustment; 
GO
PRINT 'Deleted dbo.costs_adjustment';
DELETE FROM dbo.costs_calibration; 
GO
PRINT 'Deleted dbo.costs_calibration';
DELETE FROM dbo.costs_inspection; 
GO
PRINT 'Deleted dbo.costs_inspection';
DELETE FROM dbo.costs_purchase; 
GO
PRINT 'Deleted dbo.costs_purchase';
DELETE FROM dbo.costs_repair; 
GO
PRINT 'Deleted dbo.costs_repair';
DELETE FROM dbo.costs_service; 
GO
PRINT 'Deleted dbo.costs_service';
DELETE FROM dbo.hiremodelcategory; 
GO
PRINT 'Deleted dbo.hiremodelcategory';
DELETE FROM dbo.instrument; 
GO
PRINT 'Deleted dbo.instrument';
DELETE FROM dbo.job; 
GO
PRINT 'Deleted dbo.job';
DELETE [dbo].[location] FROM [dbo].[location] loc
 JOIN address addr on addr.addrid = loc.addressid
 JOIN subdiv sub on sub.subdivid = addr.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
UPDATE [dbo].[subdiv]
 SET [defaddrid] = null
      ,[defpersonid] = null
 FROM [dbo].[subdiv] sub
 JOIN company comp ON sub.coid = comp.coid
 WHERE comp.corole <> 5;
GO
UPDATE [dbo].[contact]
 SET [defaddress] = null
 FROM [dbo].[contact] con
 JOIN address addr on addr.addrid = con.defaddress
 JOIN subdiv sub on sub.subdivid = addr.subdivid
 JOIN company comp ON sub.coid = comp.coid
 WHERE comp.corole <> 5;
GO
UPDATE [dbo].[company]
 SET [legaladdressid] = null
 WHERE corole <> 5;
GO
DELETE [dbo].[address] FROM [dbo].[address] addr
 JOIN subdiv sub on sub.subdivid = addr.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
UPDATE [dbo].[businesssubdivisionsettings]
 SET [defaultbusinesscontact] = 8740
FROM [dbo].[businesssubdivisionsettings] bss
  join contact con on bss.defaultbusinesscontact = con.personid
  join subdiv sub on sub.subdivid = con.subdivid
  join company comp on comp.coid = sub.coid
  where comp.corole <> 5
GO
UPDATE [dbo].[company]
 SET [defaultbusinesscontact] = 8740
 WHERE corole <> 5 AND defaultbusinesscontact IS NOT NULL;
GO
UPDATE [dbo].[numeration]
 SET lastModifiedBy = 8740;
GO
DELETE [dbo].[contact] FROM [dbo].[contact] con
 JOIN subdiv sub on sub.subdivid = con.subdivid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE [dbo].[usergroup] FROM [dbo].[usergroup] ug
 JOIN company comp on comp.coid = ug.coid
 WHERE comp.corole <> 5
GO
DELETE [dbo].[userrole] FROM [dbo].[userrole] ur
 JOIN subdiv sub on sub.subdivid = ur.orgid
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5;
GO
UPDATE [dbo].[company]
 SET [defsubdivid] = null
  FROM [dbo].[company] comp
  JOIN subdiv defsub on defsub.subdivid = comp.defsubdivid
  WHERE defsub.coid <> comp.coid
GO
UPDATE [dbo].[company]
 SET [defsubdivid] = null
 WHERE corole <> 5
GO
DELETE [dbo].[subdiv] FROM [dbo].[subdiv] sub
 JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole <> 5
GO
DELETE [dbo].[numeration] FROM [dbo].[numeration] num
      JOIN company comp on num.orgid = comp.coid
   WHERE comp.corole <> 5
GO
DELETE [dbo].[company] FROM [dbo].[company] comp
 WHERE comp.corole <> 5
GO