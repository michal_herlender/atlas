-- Inserts a "default" US sales tax rate
-- For test only, as prod setup will need different configuration / new entities

USE [spain]
GO

BEGIN TRAN

INSERT INTO [dbo].[vatrate]
           ([vatcode]
           ,[description]
           ,[rate]
           ,[type]
           ,[countryid]
           ,[lastModified]
           ,[lastModifiedBy])
     VALUES
           ('N','US - No Specific VAT Rate',0.000,0,185,'2019-07-24',17280);
GO

COMMIT TRAN