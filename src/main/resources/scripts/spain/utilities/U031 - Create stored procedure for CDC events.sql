-- Script 031 - This can be run to create (or recreate) a stored procedure used for CDC event retrieval.
-- This is being separated from our usual upgrade scripts for several reasons:
-- We would only use this (to start) on US production servers, one US test server, and for some development machines
-- Not all test servers (France-test, Spain-test) support CDC currently.

-- This can be run against the "cwms_test" database, like script U029, select the DB name in SSMS

BEGIN TRAN

DROP PROCEDURE IF EXISTS [dbo].[getAllCdcChangeFromLastProcessedLsn]
GO

-- Stored Procedure getAllCdcChangeFromLastProcessedLsn
CREATE PROCEDURE getAllCdcChangeFromLastProcessedLsn
    @lastprocessedlsn BINARY(10),
    @table_name VARCHAR(50) 
AS   
BEGIN
	DECLARE @function_name VARCHAR(100), @sql_command VARCHAR(200), @op_type varchar(50), 
	@begin_lsn_str VARCHAR(100), @end_lsn_str VARCHAR(100), @from binary(10), @to binary(10), 
	@query_from nvarchar(max), @query_to nvarchar(max),@query_exist nvarchar(max),@exist BINARY(10)

	SELECT @function_name = 'cdc.fn_cdc_get_all_changes_'+@table_name
	SELECT @op_type = 'all'
	SELECT @query_from = 'SELECT @from = MIN(__$start_lsn) FROM  cdc.'+@table_name+'_CT'
	EXEC sp_executesql @query_from, N'@from binary(10) out', @from out
	SELECT @query_to = 'SELECT @to = MAX(__$start_lsn) FROM  cdc.'+@table_name+'_CT'
	EXEC sp_executesql @query_to, N'@to binary(10) out', @to out
	SELECT @query_exist = 'SELECT @exist = __$start_lsn FROM  cdc.'+@table_name+'_CT WHERE __$start_lsn = '+CONVERT(VARCHAR(100), @lastprocessedlsn, 1)
	EXEC sp_executesql @query_exist, N'@exist binary(10) out', @exist out
	SELECT @begin_lsn_str = CASE 
       WHEN @exist IS NULL THEN CONVERT(VARCHAR(100), @from, 1)
       ELSE CONVERT(VARCHAR(100), @lastprocessedlsn, 1)
	   END
	SELECT @end_lsn_str = CONVERT(VARCHAR(100), @to, 1)
	SELECT @sql_command = 'SELECT DISTINCT __$start_lsn FROM ' + @function_name+'('+@begin_lsn_str+','+@end_lsn_str+','''+@op_type+''')'
	EXEC (@sql_command) 
END
GO

COMMIT TRAN