USE atlas

BEGIN TRAN

-- id of the subdivision, for which we'd like to update all addresses (here Bordeaux Pessax)
DECLARE @subdivId INT = 6966;
-- id of the new default address (here 19 Rue Nicolas Leblanc)
DECLARE @defaultAddress INT = 65060;
-- id of subdiv, where we'd like to move contacts from (here Bordeaux Merignac)
DECLARE @fromSubdivId INT = 18473

-- update default address
UPDATE subdiv SET defaddrid = @defaultAddress
WHERE subdivid = @subdivId

-- set new default address for all contacts
UPDATE contact SET defaddress = @defaultAddress
WHERE contact.subdivid = @subdivId

-- move contacts
UPDATE contact SET subdivid = @subdivId, defaddress = @defaultAddress
WHERE contact.subdivid = @fromSubdivId

-- set address for all departments to new default address
UPDATE department SET addrid = @defaultAddress
WHERE subdivid = @subdivId

-- set address for all capabilities to new default address
UPDATE procs SET defaddressid = @defaultAddress
WHERE orgid = @subdivId

-- update current address of job items, which are in the subdivision and the job is not completed
UPDATE jobitem SET addrid = @defaultAddress
FROM jobitem ji
INNER JOIN address addr ON ji.addrid = addr.addrid
INNER JOIN job j ON ji.jobid = j.jobid
INNER JOIN jobstatus js ON j.statusid = js.statusid
WHERE js.complete = 0 AND addr.subdivid = @subdivId

-- update calibration address of job items, whose calibration address is in the subdivision and the job is not completed
UPDATE jobitem SET caladdrid = @defaultAddress
FROM jobitem ji
INNER JOIN address addr ON ji.caladdrid = addr.addrid
INNER JOIN job j ON ji.jobid = j.jobid
INNER JOIN jobstatus js ON j.statusid = js.statusid
WHERE js.complete = 0 AND addr.subdivid = @subdivId

-- update booked-in address of not completed jobs and their items, whose booked-in address is in the subdivision
UPDATE job SET bookedinaddrid = @defaultAddress
FROM job j
INNER JOIN address addr ON j.bookedinaddrid = addr.addrid
INNER JOIN jobstatus js ON j.statusid = js.statusid
WHERE js.complete = 0 AND addr.subdivid = @subdivId

UPDATE jobitem SET bookedinaddrid = @defaultAddress
FROM jobitem ji
INNER JOIN address addr ON ji.bookedinaddrid = addr.addrid
INNER JOIN job j ON ji.jobid = j.jobid
INNER JOIN jobstatus js ON j.statusid = js.statusid
WHERE js.complete = 0 AND addr.subdivid = @subdivId

-- create address settings for allocated subdivision (mainly transport options) in new subdivision from former subdivision,
-- whenever they are not already exist
INSERT INTO addresssettingsforallocatedsubdiv(addressid, lastModified, lastModifiedBy, orgid, transportinid, transportoutid)
SELECT fromSettings.addressid, fromSettings.lastModified, fromSettings.lastModifiedBy, @subdivId, optionIn.id, optionOut.id
FROM addresssettingsforallocatedsubdiv fromSettings
LEFT JOIN transportoption fromOptionIn ON fromSettings.transportinid = fromOptionIn.id
LEFT JOIN transportoption optionIn ON fromOptionIn.transportmethodid = optionIn.transportmethodid AND optionIn.subdivid = @subdivId
LEFT JOIN transportoption fromOptionOut ON fromSettings.transportoutid = fromOptionOut.id
LEFT JOIN transportoption optionOut ON fromOptionOut.transportmethodid = optionOut.transportmethodid AND optionOut.subdivid = @subdivId
WHERE orgid = @fromSubdivId AND
(
	SELECT COUNT(*) FROM addresssettingsforallocatedsubdiv settings
	WHERE settings.orgid = @subdivId AND settings.addressid = fromSettings.addressid
	) = 0

COMMIT TRAN