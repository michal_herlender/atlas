-- Script to undo capability load, targeted at Lyon
-- Galen Beck - 2019-03-20

-- NOTE ROLLBACK TRAN at end, change to COMMIT TRAN once successfully tested
-- Requires running script 417 to create indices, to efficiently delete
-- Takes ~6 seconds for deletion of all data on production (FR - Trescal SA - Lyon)
-- May need updates to handle other cases for other subdivisions

USE [spain]

BEGIN TRAN

-- Déclaration des variables
DECLARE @SubName VARCHAR(255) = 'Lyon'
DECLARE @CompanyCode VARCHAR(50) = '005'
DECLARE @SubdivID INT
-- Existing subcontracting procedure, use for reassignment of job items
DECLARE @STIProcRef VARCHAR(20) = 'FR-LYN-STI'
-- Existing external procedure, keep as not created in capability load
DECLARE @EXTProcRef VARCHAR(20) = 'FR-LYN-EXT'

-- Récupération de l'ID de la subdivision
SELECT @SubdivID = dbo.subdiv.subdivid
FROM dbo.subdiv WITH (NOLOCK)
INNER JOIN dbo.company WITH (NOLOCK)
ON dbo.subdiv.coid = dbo.company.coid
WHERE subname = @SubName
AND companycode = @CompanyCode

PRINT '@SubdivID'
PRINT @SubdivID

DECLARE @STIProcID INT
SELECT @STIProcID = dbo.procs.id from dbo.procs WHERE reference = @STIProcRef;

PRINT '@STIProcID'
PRINT @STIProcID

DECLARE @EXTProcID INT
SELECT @EXTProcID = dbo.procs.id from dbo.procs WHERE reference = @EXTProcRef;

PRINT '@EXTProcID'
PRINT @EXTProcID

-- Undo script 009 Reference ---
-- Nothing to do, it's just setting time on procedures

-- Undo script 008 Skills ---
-- Delete procedure authorization for all procedures in subdiv

PRINT 'DELETE FROM [procedureaccreditation]...'

DELETE FROM dbo.procedureaccreditation WHERE procedureaccreditation.procid IN 
	(SELECT procs.id FROM dbo.procs WHERE procs.orgid = @SubdivID AND procs.id NOT IN (@EXTProcID, @STIProcID));

-- Undo script 007 Arrosage Procedures ---

PRINT 'DELETE FROM [servicecapability]...'

DELETE FROM dbo.servicecapability WHERE servicecapability.procedureid IN
	(SELECT procs.id FROM dbo.procs WHERE procs.orgid = @SubdivID AND procs.id NOT IN (@EXTProcID, @STIProcID));

-- Undo script 006 SubFamily ---
-- Nothing to do

-- Undo script 005 CapabilityFIlter ---

PRINT 'DELETE FROM [capabilityfilter]...'

DELETE FROM dbo.capabilityfilter WHERE capabilityfilter.procedureid IN
	(SELECT procs.id FROM dbo.procs WHERE procs.orgid = @SubdivID AND procs.id NOT IN (@EXTProcID, @STIProcID));

-- Undo script 004 ServiceCapability ---
-- Already done by undo in step 7 (deletes all)

-- Undo script 003 CategorizedProcedures ---

PRINT 'DELETE FROM [categorisedprocedure]...'

-- Delete all (for departments in subdiv) as we will delete all departments in subdiv at end
DELETE FROM dbo.categorisedprocedure WHERE categorisedprocedure.categoryid IN
	(SELECT procedurecategory.id FROM procedurecategory
	 LEFT JOIN department on department.deptid = procedurecategory.departmentid 
	 WHERE department.subdivid = @SubdivID);

-- Undo script 002 Procedures ---

PRINT 'UPDATE [jobitem] procid...'

UPDATE dbo.jobitem SET procid = @STIProcID WHERE jobitem.procid IN 
	(SELECT procs.id FROM dbo.procs WHERE procs.orgid = @SubdivID 
		AND procs.id NOT IN (@EXTProcID, @STIProcID));

UPDATE dbo.workrequirement SET procedureid = @STIProcID WHERE workrequirement.procedureid IN 
	(SELECT procs.id FROM dbo.procs WHERE procs.orgid = @SubdivID 
		AND procs.id NOT IN (@EXTProcID, @STIProcID));

PRINT 'DELETE FROM [procedurecategory]...'

DELETE FROM dbo.procedurecategory WHERE procedurecategory.departmentid IN 
	(SELECT department.deptid FROM department WHERE subdivid = @SubdivID);

PRINT 'DELETE FROM [procs]...'

DELETE FROM dbo.procs WHERE procs.orgid = @SubdivID AND procs.id NOT IN (@EXTProcID, @STIProcID);

--SELECT procs.id, procs.reference FROM procs WHERE procs.orgid = @SubdivID AND procs.id NOT IN (@EXTProcID, @STIProcID);

-- Undo script 001 Departments ---

PRINT 'UPDATE [workrequirement].deptid...'

UPDATE dbo.workrequirement SET workrequirement.deptid = NULL WHERE workrequirement.deptid IN
	(SELECT deptid FROM dbo.department WHERE department.subdivid = @SubdivID);

PRINT 'DELETE FROM [departmentmember]'

DELETE FROM dbo.departmentmember WHERE departmentmember.deptid IN
	(SELECT deptid FROM dbo.department WHERE department.subdivid = @SubdivID);

PRINT 'DELETE FROM [department]...'

DELETE FROM dbo.department WHERE department.subdivid = @SubdivID;

ROLLBACK TRAN