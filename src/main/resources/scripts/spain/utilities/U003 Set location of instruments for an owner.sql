/*** Please, ensure that there is only one result for the subquery ***/

BEGIN TRAN

UPDATE instrument SET instrument.locationid = subquery.locationid, instrument.addressid = subquery.addrid
FROM
(SELECT TOP(1) location.locationid, address.addrid, contact.personid
FROM contact
LEFT JOIN subdiv ON contact.subdivid = subdiv.subdivid
LEFT JOIN company ON subdiv.coid = company.coid,
location
LEFT JOIN address ON location.addressid = address.addrid
WHERE
contact.firstname like 'Alfred%' AND
contact.lastname like 'Gonz%' AND
subdiv.subname like '%' AND
company.coname like 'GE%' AND
location.location like 'Rep%' AND
address.subdivid = subdiv.subdivid) AS subquery
WHERE instrument.personid = subquery.personid

ROLLBACK TRAN