BEGIN TRAN

INSERT INTO capability_filter(lastModified, bestlab, filtertype, capabilityId, servicetypeid, active)
SELECT '2022-02-25', 0, 'YES', c.id, ct.servicetypeid, 1
FROM capability c
         JOIN capability_authorization ca ON c.id = ca.capabilityId
         JOIN calibrationtype ct ON ca.caltypeid = ct.caltypeid
         LEFT JOIN capability_filter cf ON ct.servicetypeid = cf.servicetypeid AND c.id = cf.capabilityId
WHERE c.subfamilyid IS NULL AND cf.id IS NULL
GROUP BY c.id, ct.servicetypeid

COMMIT TRAN