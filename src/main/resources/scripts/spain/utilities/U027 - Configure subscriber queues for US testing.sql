-- Configures subscriber queues for basic testing
-- It's only intended that this is run on the US test server and local dev machines as needed
-- therefore it's not saved as a regular numbered script
-- Galen Beck - 2020-03-15
-- Note, this now does not clear the subscriber queue (as of 2021-05-01) as not all events may have been processed by TAM/Calypso/etc...
-- Note ROLLBACK at end, change local after testing

USE [atlas]
GO

BEGIN TRAN

DELETE FROM [dbo].[event_subscription]
GO

DELETE FROM [dbo].[event_subscriber]
GO

SET IDENTITY_INSERT [event_subscriber] ON

INSERT INTO [dbo].[event_subscriber]
           ([id],[model])
     VALUES
           (1,'Atlas Dev Test'),
		   (2,'Calypso Test'),
		   (3,'TAM Test'),
		   (4,'TAM Local Test')
GO

SET IDENTITY_INSERT [event_subscriber] OFF

declare @sub_TEST int = 1;
declare @sub_CALYPSO int = 2;
declare @sub_TAM int = 3;
declare @sub_TAM_Local int = 4;

-- Definitions matching ordinal mappings in ChangeEntity

declare @id_BRAND int = 0;
declare @id_COUNTRY int = 1;
declare @id_SERVICE_TYPE int = 2;
declare @id_SUB_FAMILY int = 3;
declare @id_INSTRUMENT_MODEL int = 4;

declare @id_INSTRUMENT_MODEL_TRANSLATION int = 5;	
declare @id_COMPANY int = 6;
declare @id_COMPANY_SETTINGS int = 7;
declare @id_SUBDIV int = 8;
declare @id_ADDRESS int = 9;

declare @id_LOCATION int = 10;
declare @id_CONTACT int = 11;
declare @id_DEPARTMENT int = 12;
declare @id_DEPARTMENT_MEMBER int = 13;
declare @id_INSTRUMENT int = 14;
	
declare @id_ITEM_STATE int = 15;
declare @id_JOB int = 16;
declare @id_JOB_ITEM int = 17;
declare @id_ENGINEER_ALLOCATION int = 18;
declare @id_CAL_REQ int = 19;

declare @id_UPCOMING_WORK int = 20;
declare @id_UPCOMING_WORK_JOB_LINK int = 21;
declare @id_BASE_INSTRUCTION int = 22;
declare @id_COMPANY_INSTRUCTION_LINK int = 23;
declare @id_SUBDIV_INSTRUCTION_LINK int = 24;

declare @id_CONTACT_INSTRUCTION_LINK int = 25;
declare @id_JOB_INSTRUCTION_LINK int = 26;
declare @id_SYSTEM_DEFAULT int = 27;
declare @id_SYSTEM_DEFAULT_APPLICATION int = 28;
declare @id_WORK_INSTRUCTION int = 29;

declare @id_INSTRUMENT_WORK_INSTRUCTION int = 30;
declare @id_CERTIFICATE int = 31;
declare @id_INST_CERT_LINK int = 32;
declare @id_CERT_LINK int = 33;
declare @id_UOM int = 34;

declare @id_INSTRUMENT_RANGE int = 35;
declare @id_JOB_ITEM_GROUP int = 36;
declare @id_ON_BEHALF_ITEM int = 37;
declare @id_INSTUMENT_USAGE_TYPE int = 38;

-- Everything for test
INSERT INTO [dbo].[event_subscription]
           ([entitytype],[subscriberid])
     VALUES
           (@id_BRAND ,@sub_TEST),
           (@id_COUNTRY ,@sub_TEST),
           (@id_SERVICE_TYPE ,@sub_TEST),
           (@id_SUB_FAMILY ,@sub_TEST),
           (@id_INSTRUMENT_MODEL ,@sub_TEST),

           (@id_INSTRUMENT_MODEL_TRANSLATION ,@sub_TEST),	
           (@id_COMPANY ,@sub_TEST),
           (@id_COMPANY_SETTINGS ,@sub_TEST),
           (@id_SUBDIV ,@sub_TEST),
           (@id_ADDRESS ,@sub_TEST),

           (@id_LOCATION ,@sub_TEST),
           (@id_CONTACT ,@sub_TEST),
           (@id_DEPARTMENT ,@sub_TEST),
           (@id_DEPARTMENT_MEMBER ,@sub_TEST),
           (@id_INSTRUMENT ,@sub_TEST),
	
           (@id_ITEM_STATE ,@sub_TEST),
           (@id_JOB ,@sub_TEST),
           (@id_JOB_ITEM ,@sub_TEST),
           (@id_ENGINEER_ALLOCATION ,@sub_TEST),
           (@id_CAL_REQ ,@sub_TEST),

           (@id_UPCOMING_WORK ,@sub_TEST),
           (@id_UPCOMING_WORK_JOB_LINK ,@sub_TEST),
           (@id_BASE_INSTRUCTION ,@sub_TEST),
           (@id_COMPANY_INSTRUCTION_LINK ,@sub_TEST),
           (@id_SUBDIV_INSTRUCTION_LINK ,@sub_TEST),

           (@id_CONTACT_INSTRUCTION_LINK ,@sub_TEST),
           (@id_JOB_INSTRUCTION_LINK ,@sub_TEST),
           (@id_SYSTEM_DEFAULT ,@sub_TEST),
           (@id_SYSTEM_DEFAULT_APPLICATION ,@sub_TEST),
           (@id_WORK_INSTRUCTION ,@sub_TEST),

           (@id_INSTRUMENT_WORK_INSTRUCTION ,@sub_TEST),
           (@id_CERTIFICATE ,@sub_TEST),
           (@id_INST_CERT_LINK ,@sub_TEST),
           (@id_CERT_LINK ,@sub_TEST),
           (@id_UOM ,@sub_TEST),

           (@id_INSTRUMENT_RANGE ,@sub_TEST),
		   (@id_JOB_ITEM_GROUP ,@sub_TEST),
		   (@id_ON_BEHALF_ITEM ,@sub_TEST),
		   (@id_INSTUMENT_USAGE_TYPE ,@sub_TEST);

-- Everything for Calypso except Certificates
INSERT INTO [dbo].[event_subscription]
           ([entitytype],[subscriberid])
     VALUES
           (@id_BRAND ,@sub_CALYPSO),
           (@id_COUNTRY ,@sub_CALYPSO),
           (@id_SERVICE_TYPE ,@sub_CALYPSO),
           (@id_SUB_FAMILY ,@sub_CALYPSO),
           (@id_INSTRUMENT_MODEL ,@sub_CALYPSO),

           (@id_INSTRUMENT_MODEL_TRANSLATION ,@sub_CALYPSO),	
           (@id_COMPANY ,@sub_CALYPSO),
           (@id_COMPANY_SETTINGS ,@sub_CALYPSO),
           (@id_SUBDIV ,@sub_CALYPSO),
           (@id_ADDRESS ,@sub_CALYPSO),

           (@id_LOCATION ,@sub_CALYPSO),
           (@id_CONTACT ,@sub_CALYPSO),
           (@id_DEPARTMENT ,@sub_CALYPSO),
           (@id_DEPARTMENT_MEMBER ,@sub_CALYPSO),
           (@id_INSTRUMENT ,@sub_CALYPSO),
	
           (@id_ITEM_STATE ,@sub_CALYPSO),
           (@id_JOB ,@sub_CALYPSO),
           (@id_JOB_ITEM ,@sub_CALYPSO),
           (@id_ENGINEER_ALLOCATION ,@sub_CALYPSO),
           (@id_CAL_REQ ,@sub_CALYPSO),

           (@id_UPCOMING_WORK ,@sub_CALYPSO),
           (@id_UPCOMING_WORK_JOB_LINK ,@sub_CALYPSO),
           (@id_BASE_INSTRUCTION ,@sub_CALYPSO),
           (@id_COMPANY_INSTRUCTION_LINK ,@sub_CALYPSO),
           (@id_SUBDIV_INSTRUCTION_LINK ,@sub_CALYPSO),

           (@id_CONTACT_INSTRUCTION_LINK ,@sub_CALYPSO),
           (@id_JOB_INSTRUCTION_LINK ,@sub_CALYPSO),
           (@id_SYSTEM_DEFAULT ,@sub_CALYPSO),
           (@id_SYSTEM_DEFAULT_APPLICATION ,@sub_CALYPSO),
           (@id_WORK_INSTRUCTION ,@sub_CALYPSO),

           (@id_INSTRUMENT_WORK_INSTRUCTION ,@sub_CALYPSO),
--           (@id_CERTIFICATE ,@sub_CALYPSO),
--           (@id_INST_CERT_LINK ,@sub_CALYPSO),
--           (@id_CERT_LINK ,@sub_CALYPSO),
           (@id_UOM ,@sub_CALYPSO),

           (@id_INSTRUMENT_RANGE ,@sub_CALYPSO),
		   (@id_JOB_ITEM_GROUP ,@sub_CALYPSO),
		   (@id_ON_BEHALF_ITEM ,@sub_TEST),
		   (@id_INSTUMENT_USAGE_TYPE ,@sub_TEST);
		   
-- No workflow / laboratory related stuff for TAM

INSERT INTO [dbo].[event_subscription]
           ([entitytype],[subscriberid])
     VALUES
--          (@id_DEPARTMENT ,@sub_TAM),
--          (@id_DEPARTMENT_MEMBER ,@sub_TAM),
--          (@id_ENGINEER_ALLOCATION ,@sub_TAM)

           (@id_BRAND ,@sub_TAM),
           (@id_COUNTRY ,@sub_TAM),
           (@id_SERVICE_TYPE ,@sub_TAM),
           (@id_SUB_FAMILY ,@sub_TAM),
           (@id_INSTRUMENT_MODEL ,@sub_TAM),

           (@id_INSTRUMENT_MODEL_TRANSLATION ,@sub_TAM),	
           (@id_COMPANY ,@sub_TAM),
           (@id_COMPANY_SETTINGS ,@sub_TAM),
           (@id_SUBDIV ,@sub_TAM),
           (@id_ADDRESS ,@sub_TAM),

           (@id_LOCATION ,@sub_TAM),
           (@id_CONTACT ,@sub_TAM),
--           (@id_DEPARTMENT ,@sub_TAM),
--           (@id_DEPARTMENT_MEMBER ,@sub_TAM),
           (@id_INSTRUMENT ,@sub_TAM),
	
           (@id_ITEM_STATE ,@sub_TAM),
           (@id_JOB ,@sub_TAM),
           (@id_JOB_ITEM ,@sub_TAM),
--          (@id_ENGINEER_ALLOCATION ,@sub_TAM),
           (@id_CAL_REQ ,@sub_TAM),

--           (@id_UPCOMING_WORK ,@sub_TAM),
--           (@id_UPCOMING_WORK_JOB_LINK ,@sub_TAM),
--           (@id_BASE_INSTRUCTION ,@sub_TAM),
--           (@id_COMPANY_INSTRUCTION_LINK ,@sub_TAM),
--           (@id_SUBDIV_INSTRUCTION_LINK ,@sub_TAM),

--           (@id_CONTACT_INSTRUCTION_LINK ,@sub_TAM),
--           (@id_JOB_INSTRUCTION_LINK ,@sub_TAM),
           (@id_SYSTEM_DEFAULT ,@sub_TAM),
           (@id_SYSTEM_DEFAULT_APPLICATION ,@sub_TAM),
           (@id_WORK_INSTRUCTION ,@sub_TAM),

           (@id_INSTRUMENT_WORK_INSTRUCTION ,@sub_TAM),
           (@id_CERTIFICATE ,@sub_TAM),
           (@id_INST_CERT_LINK ,@sub_TAM),
           (@id_CERT_LINK ,@sub_TAM),
           (@id_UOM ,@sub_TAM),

           (@id_INSTRUMENT_RANGE ,@sub_TAM),
           (@id_ON_BEHALF_ITEM ,@sub_TEST),
		   (@id_INSTUMENT_USAGE_TYPE ,@sub_TEST);
--		   (@id_JOB_ITEM_GROUP ,@sub_TAM);
			


INSERT INTO [dbo].[event_subscription]
           ([entitytype],[subscriberid])
     VALUES
--          (@id_DEPARTMENT ,@sub_TAM),
--          (@id_DEPARTMENT_MEMBER ,@sub_TAM),
--          (@id_ENGINEER_ALLOCATION ,@sub_TAM)

           (@id_BRAND ,@sub_TAM_Local),
           (@id_COUNTRY ,@sub_TAM_Local),
           (@id_SERVICE_TYPE ,@sub_TAM_Local),
           (@id_SUB_FAMILY ,@sub_TAM_Local),
           (@id_INSTRUMENT_MODEL ,@sub_TAM_Local),

           (@id_INSTRUMENT_MODEL_TRANSLATION ,@sub_TAM_Local),	
           (@id_COMPANY ,@sub_TAM_Local),
           (@id_COMPANY_SETTINGS ,@sub_TAM_Local),
           (@id_SUBDIV ,@sub_TAM_Local),
           (@id_ADDRESS ,@sub_TAM_Local),

           (@id_LOCATION ,@sub_TAM_Local),
           (@id_CONTACT ,@sub_TAM_Local),
--           (@id_DEPARTMENT ,@sub_TAM),
--           (@id_DEPARTMENT_MEMBER ,@sub_TAM),
           (@id_INSTRUMENT ,@sub_TAM_Local),
	
           (@id_ITEM_STATE ,@sub_TAM_Local),
           (@id_JOB ,@sub_TAM_Local),
           (@id_JOB_ITEM ,@sub_TAM_Local),
--          (@id_ENGINEER_ALLOCATION ,@sub_TAM),
           (@id_CAL_REQ ,@sub_TAM_Local),

--           (@id_UPCOMING_WORK ,@sub_TAM),
--           (@id_UPCOMING_WORK_JOB_LINK ,@sub_TAM),
--           (@id_BASE_INSTRUCTION ,@sub_TAM),
--           (@id_COMPANY_INSTRUCTION_LINK ,@sub_TAM),
--           (@id_SUBDIV_INSTRUCTION_LINK ,@sub_TAM),

--           (@id_CONTACT_INSTRUCTION_LINK ,@sub_TAM),
--           (@id_JOB_INSTRUCTION_LINK ,@sub_TAM),
           (@id_SYSTEM_DEFAULT ,@sub_TAM_Local),
           (@id_SYSTEM_DEFAULT_APPLICATION ,@sub_TAM_Local),
           (@id_WORK_INSTRUCTION ,@sub_TAM_Local),

           (@id_INSTRUMENT_WORK_INSTRUCTION ,@sub_TAM_Local),
           (@id_CERTIFICATE ,@sub_TAM_Local),
           (@id_INST_CERT_LINK ,@sub_TAM_Local),
           (@id_CERT_LINK ,@sub_TAM_Local),
           (@id_UOM ,@sub_TAM_Local),

           (@id_INSTRUMENT_RANGE ,@sub_TAM_Local),
           (@id_ON_BEHALF_ITEM ,@sub_TEST),
		   (@id_INSTUMENT_USAGE_TYPE ,@sub_TEST);
--		   (@id_JOB_ITEM_GROUP ,@sub_TAM);

ROLLBACK TRAN