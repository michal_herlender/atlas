-- Creates a permission group, wither for local use or Spain-Test, 
-- to allow for editing / creating instrument models and manufacturers
-- for evaluation purposes
-- Note ROLLBACK at end, COMMIT once tested locally

-- Galen Beck - 2019-11-19 
-- (updated 2019-11-22 to refelct changes on Spain-test, no need to rerun)

USE [atlas]

BEGIN TRAN

-- Create new role group ROLE_DEMO_TML_OVERRIDE

INSERT INTO [dbo].[perrole]
           ([name])
     VALUES ('ROLE_DEMO_TML_OVERRIDE')
GO

-- Create 2 permission groups for model and brands

INSERT INTO [dbo].[permissiongroup]
           ([name])
     VALUES ('DEMO_INSTRUMENT_MODEL_MANAGEMENT'),
             ('DEMO_BRAND_MANAGEMENT');
GO

DECLARE @roleId INT;
SELECT @roleId = id FROM perrole WHERE name = 'ROLE_DEMO_TML_OVERRIDE';
PRINT @roleId

DECLARE @groupId_Model INT;
SELECT @groupId_Model = id FROM permissiongroup WHERE name = 'DEMO_INSTRUMENT_MODEL_MANAGEMENT';
PRINT @groupId_Model

DECLARE @groupId_Brand INT;
SELECT @groupId_Brand = id FROM permissiongroup WHERE name = 'DEMO_BRAND_MANAGEMENT';
PRINT @groupId_Brand

DECLARE @coId_TUS INT;
SELECT @coId_TUS = coid FROM company WHERE coname = 'Trescal Inc.';
PRINT @coId_TUS

DECLARE @coId_TFR INT;
SELECT @coId_TFR = coid FROM company WHERE coname = 'Trescal SAS';
PRINT @coId_TFR

DECLARE @coId_TUK INT;
SELECT @coId_TUK = coid FROM company WHERE coname = 'TRESCAL Ltd';
PRINT @coId_TUK

DECLARE @subdivId_DTW INT;
SELECT @subdivId_DTW = subdivid FROM subdiv WHERE subname = 'Detroit' and subdiv.coid = @coId_TUS;
PRINT @subdivId_DTW

DECLARE @subdivId_MIL INT;
SELECT @subdivId_MIL = subdivid FROM subdiv WHERE subname = 'Milwaukee' and subdiv.coid = @coId_TUS;
PRINT @subdivId_MIL

DECLARE @subdivId_SCL INT;
SELECT @subdivId_SCL = subdivid FROM subdiv WHERE subname = 'Santa CLara' and subdiv.coid = @coId_TUS;
PRINT @subdivId_SCL

DECLARE @subdivId_RUN INT;
SELECT @subdivId_RUN = subdivid FROM subdiv WHERE subname = 'Rungis' and subdiv.coid = @coId_TFR;
PRINT @subdivId_RUN

DECLARE @subdivId_MTZ INT;
SELECT @subdivId_MTZ = subdivid FROM subdiv WHERE subname = 'Metz' and subdiv.coid = @coId_TFR;
PRINT @subdivId_MTZ

DECLARE @subdivId_YAR INT;
SELECT @subdivId_YAR = subdivid FROM subdiv WHERE subname = 'Great Yarmouth' and subdiv.coid = @coId_TUK;
PRINT @subdivId_YAR

-- Add permissions to role groups

INSERT INTO [dbo].[permission]
           ([groupId],[permission])
     VALUES
           (@groupId_Model,'MANUFACTURER_SEARCH'),
           (@groupId_Model,'MANUFACTURER_EDIT'),
           (@groupId_Model,'MANUFACTURER_VIEW'),
           (@groupId_Model,'MODEL_EDIT_CHARACTERISTICS'),
           (@groupId_Model,'MODEL_EDIT');

-- Add role groups to single permision group

INSERT INTO [dbo].[rolegroup]
           ([roleId]
           ,[groupId])
     VALUES
           (@roleId, @groupId_Model),
		   (@roleId, @groupId_Brand);

-- Provide this to selected demo users for their HOME subdivision only

INSERT INTO [dbo].[userrole]
           ([role],[userName],[lastModified],[lastModifiedBy],[orgid])
     VALUES
--			 For Didier/ Dominique to set up data first, per Didier
--           (@roleId ,'dustin.spero' ,'2019-11-19' ,'8740' ,@subdivId_DTW),
--           (@roleId ,'EricC' ,'2019-11-19' ,'8740' ,@subdivId_MIL),
           (@roleId ,'GalenB' ,'2019-11-19' ,'8740' ,@subdivId_SCL),
           (@roleId ,'bernardd' ,'2019-11-19' ,'8740' ,@subdivId_RUN),
           (@roleId ,'terriend' ,'2019-11-19' ,'8740' ,@subdivId_MTZ),
           (@roleId ,'adrian' ,'2019-11-19' ,'8740' ,@subdivId_YAR)
GO

ROLLBACK TRAN