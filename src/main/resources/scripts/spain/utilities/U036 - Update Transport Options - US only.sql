-- Utility script to update transport options for US and configure for Cleveland
-- Intended just to be run on US servers so is not in the usual upgrade scripts

-- This also removes / inactivates the transport options for non-US business companies as a simplification
-- (which may become stale over time anyways)
-- This inactivates the "Trescal Shuttle 1" to "Trescal Shuttle 5" options and reactivates the generic "Trescal Shuttle"
-- Note ROLLBACK at end, change to COMIT after testing for use

USE [atlas-us-2021-07-01]

BEGIN TRAN

-- Save information belonging to Trescal Inc and ISS business companies only
DECLARE @coid_TUS int;
DECLARE @coid_ISS int;
DECLARE @subdivid_CLE int;
DECLARE @tmethod_shuttle int;
DECLARE @UserId INT;
DECLARE @Now datetime2 = SYSDATETIME();

DECLARE @remainingCount int;

SELECT @coid_TUS = coid from company where coname='TRESCAL INC' and corole=5;
SELECT @coid_ISS = coid from company where coname='INTEGRATED SERVICE SOLUTIONS INC' and corole=5;
SELECT @subdivid_CLE = subdivid from subdiv where coid=@coid_TUS and subdivcode = 'CLE';
SELECT @tmethod_shuttle = id from transportmethod where [method] like ('Trescal Shuttle');
SELECT @UserId= personid FROM dbo.users WHERE username='GalenB';

PRINT @coid_TUS;
PRINT @coid_ISS;
PRINT @subdivid_CLE;
PRINT @tmethod_shuttle;
PRINT @UserId;
-- Expected to be 17280

PRINT 'Deleting address settings (transport option config) for non-US companies'

DELETE [addresssettingsforallocatedsubdiv] FROM [addresssettingsforallocatedsubdiv] settings
	INNER JOIN subdiv sub on sub.subdivid = settings.orgid
      WHERE sub.coid NOT IN (@coid_ISS, @coid_TUS);

SELECT @remainingCount = COUNT(1) from [addresssettingsforallocatedsubdiv];
PRINT @remainingCount;

PRINT 'Deleting transport options for non-US companies'

DELETE [transportoption] FROM [transportoption] toption
	INNER JOIN subdiv sub on sub.subdivid = toption.subdivid
      WHERE sub.coid NOT IN (@coid_ISS, @coid_TUS);

SELECT @remainingCount = COUNT(1) from [transportoption];
PRINT @remainingCount;

PRINT 'Updating transport methods Trescal Shuttle 1-5 to be inactive'

UPDATE [dbo].[transportmethod]
   SET [active] = 0
 WHERE [method] like ('Trescal Shuttle - %');

PRINT 'Updating generic transport method Trescal Shuttle to be active'

UPDATE [dbo].[transportmethod]
   SET [active] = 1
 WHERE [method] like ('Trescal Shuttle');

PRINT 'Deleting old US shuttle transport option schedule rules'

DELETE transportoptionschedulerule FROM transportoptionschedulerule toptrule
 INNER JOIN [dbo].transportoption toption on toption.id = toptrule.transportoptionid
 INNER JOIN [dbo].[transportmethod] tmethod on tmethod.id = toption.transportmethodid
 WHERE [method] like ('Trescal Shuttle - %');
 
SELECT @remainingCount = COUNT(1) from [transportoptionschedulerule];
PRINT @remainingCount;

PRINT 'Deleting old US shuttle transport options'

DELETE transportoption FROM transportoption toption
 INNER JOIN [dbo].[transportmethod] tmethod on tmethod.id = toption.transportmethodid
 WHERE [method] like ('Trescal Shuttle - %');

SELECT @remainingCount = COUNT(1) from [transportoption];
PRINT @remainingCount;

PRINT 'Inserting new US Cleveland shuttle transport options'

INSERT INTO [dbo].[transportoption]
           ([active]
           ,[transportmethodid]
           ,[subdivid]
           ,[lastModified]
           ,[lastModifiedBy]
           ,[localizedname]
           ,[externalref])
     VALUES
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 1 Mon','1'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 2 Mon','2'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 1 Tue','1'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 2 Tue','2'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 1 Wed','1'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 2 Wed','2'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 1 Thu','1'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 2 Thu','2'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 1 Fri','1'),
           (1,@tmethod_shuttle,@subdivid_CLE,@Now,@UserId,'Shuttle 2 Fri','2');

DECLARE @topt_Mon1 int;
DECLARE @topt_Mon2 int;
DECLARE @topt_Tue1 int;
DECLARE @topt_Tue2 int;
DECLARE @topt_Wed1 int;
DECLARE @topt_Wed2 int;
DECLARE @topt_Thu1 int;
DECLARE @topt_Thu2 int;
DECLARE @topt_Fri1 int;
DECLARE @topt_Fri2 int;

SELECT @topt_Mon1 = id FROM transportoption where localizedname = 'Shuttle 1 Mon' and subdivid = @subdivid_CLE;
SELECT @topt_Mon2 = id FROM transportoption where localizedname = 'Shuttle 2 Mon' and subdivid = @subdivid_CLE;
SELECT @topt_Tue1 = id FROM transportoption where localizedname = 'Shuttle 1 Tue' and subdivid = @subdivid_CLE;
SELECT @topt_Tue2 = id FROM transportoption where localizedname = 'Shuttle 2 Tue' and subdivid = @subdivid_CLE;
SELECT @topt_Wed1 = id FROM transportoption where localizedname = 'Shuttle 1 Wed' and subdivid = @subdivid_CLE;
SELECT @topt_Wed2 = id FROM transportoption where localizedname = 'Shuttle 2 Wed' and subdivid = @subdivid_CLE;
SELECT @topt_Thu1 = id FROM transportoption where localizedname = 'Shuttle 1 Thu' and subdivid = @subdivid_CLE;
SELECT @topt_Thu2 = id FROM transportoption where localizedname = 'Shuttle 2 Thu' and subdivid = @subdivid_CLE;
SELECT @topt_Fri1 = id FROM transportoption where localizedname = 'Shuttle 1 Fri' and subdivid = @subdivid_CLE;
SELECT @topt_Fri2 = id FROM transportoption where localizedname = 'Shuttle 2 Fri' and subdivid = @subdivid_CLE;


INSERT INTO [dbo].[transportoptionschedulerule]
           ([transportoptionid]
           ,[cutofftime]
           ,[dayofweek])
     VALUES
           (@topt_Mon1 ,'07:30:00.000' ,'MONDAY'),
           (@topt_Mon2 ,'07:30:00.000' ,'MONDAY'),
           (@topt_Tue1 ,'07:30:00.000' ,'TUESDAY'),
           (@topt_Tue2 ,'07:30:00.000' ,'TUESDAY'),
           (@topt_Wed1 ,'07:30:00.000' ,'WEDNESDAY'),
           (@topt_Wed2 ,'07:30:00.000' ,'WEDNESDAY'),
           (@topt_Thu1 ,'07:30:00.000' ,'THURSDAY'),
           (@topt_Thu2 ,'07:30:00.000' ,'THURSDAY'),
           (@topt_Fri1 ,'07:30:00.000' ,'FRIDAY'),
           (@topt_Fri2 ,'07:30:00.000' ,'FRIDAY');

ROLLBACK TRAN