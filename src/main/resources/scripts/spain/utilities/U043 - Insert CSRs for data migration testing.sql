-- This script is meant for some data configuration on US test environments
-- so that they could match US production (all expected CSRs)

USE [atlas]
GO

BEGIN TRAN

DECLARE @coid_TUS int;

DECLARE @subdivid_ATL int;
DECLARE @subdivid_BAL int;
DECLARE @subdivid_CHI int;
DECLARE @subdivid_CLE int;
DECLARE @subdivid_DAL int;
DECLARE @subdivid_HAR int;
DECLARE @subdivid_MOB int;

DECLARE @addrid_ATL int;
DECLARE @addrid_BAL int;
DECLARE @addrid_CHI int;
DECLARE @addrid_CLE int;
DECLARE @addrid_DAL int;
DECLARE @addrid_HAR int;
DECLARE @addrid_MOB int;

SELECT @coid_TUS = coid from company WHERE coname='TRESCAL INC' and corole = 5;
SELECT @subdivid_ATL = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'ATL';
SELECT @subdivid_BAL = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'BAL';
SELECT @subdivid_CHI = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'CHI';
SELECT @subdivid_CLE = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'CLE';
SELECT @subdivid_DAL = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'DAL';
SELECT @subdivid_HAR = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'HAR';
SELECT @subdivid_MOB = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'MOB';

SELECT @addrid_ATL = addrid from address WHERE subdivid = @subdivid_ATL;
SELECT @addrid_BAL = addrid from address WHERE subdivid = @subdivid_BAL;
SELECT @addrid_CHI = addrid from address WHERE subdivid = @subdivid_CHI;
SELECT @addrid_CLE = addrid from address WHERE subdivid = @subdivid_CLE;
SELECT @addrid_DAL = addrid from address WHERE subdivid = @subdivid_DAL;
SELECT @addrid_HAR = addrid from address WHERE subdivid = @subdivid_HAR;
SELECT @addrid_MOB = addrid from address WHERE subdivid = @subdivid_MOB;

PRINT @addrid_ATL;
PRINT @addrid_BAL;
PRINT @addrid_CHI;
PRINT @addrid_CLE;
PRINT @addrid_DAL;
PRINT @addrid_HAR;
PRINT @addrid_MOB;

UPDATE contact set email = 'lori.reece@trescal.us' WHERE email = 'lreece@trescal.us';

INSERT INTO [dbo].[contact]
           ([active]
           ,[createdate]
           ,[email]
           ,[fax]
           ,[firstname]
           ,[lastname]
           ,[mobile]
           ,[position]
           ,[preference]
           ,[telephone]
           ,[defaddress]
           ,[subdivid]
           ,[title]
           ,[lastModified]
           ,[locale]
           ,[lastModifiedBy])
     VALUES
           (1, SYSDATETIME(), 'tara.phillips@trescal.us', 'N/A', 'Tara', 'Phillips', 'N/A', 'N/A', 'E', 'N/A',  @addrid_HAR, @subdivid_HAR, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'danielle.flair@trescal.us', 'N/A', 'Danielle', 'Flair', 'N/A', 'N/A', 'E', 'N/A',  @addrid_DAL, @subdivid_DAL, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'priscilla.duran@trescal.us', 'N/A', 'Priscilla', 'Duran', 'N/A', 'N/A', 'E', 'N/A',  @addrid_DAL, @subdivid_DAL, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'kevin.sawberger@trescal.us', 'N/A', 'Kevin', 'Sawberger', 'N/A', 'N/A', 'E', 'N/A',  @addrid_DAL, @subdivid_DAL, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'christina.brennan@trescal.us', 'N/A', 'Christina', 'Brennan', 'N/A', 'N/A', 'E', 'N/A',  @addrid_DAL, @subdivid_DAL, '', sysdatetime(), 'en_US', 17280),

           (1, SYSDATETIME(), 'dawn.lowery@trescal.us', 'N/A', 'Dawn', 'Lowery', 'N/A', 'N/A', 'E', 'N/A',  @addrid_DAL, @subdivid_DAL, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'john.deshotel@trescal.us', 'N/A', 'John', 'Deshotel', 'N/A', 'N/A', 'E', 'N/A',  @addrid_MOB, @subdivid_MOB, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'ryan.christie@trescal.us', 'N/A', 'Ryan', 'Christie', 'N/A', 'N/A', 'E', 'N/A',  @addrid_CHI, @subdivid_CHI, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'brittni.weaver@trescal.us', 'N/A', 'Brittni', 'Weaver', 'N/A', 'N/A', 'E', 'N/A',  @addrid_HAR, @subdivid_HAR, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'candice.mckeehan@trescal.us', 'N/A', 'Candice', 'McKeehan', 'N/A', 'N/A', 'E', 'N/A',  @addrid_DAL, @subdivid_DAL, '', sysdatetime(), 'en_US', 17280),

           (1, SYSDATETIME(), 'amanda.wolf@trescal.us', 'N/A', 'Amanda', 'Wolf', 'N/A', 'N/A', 'E', 'N/A',  @addrid_BAL, @subdivid_BAL, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'jordan.santana@trescal.us', 'N/A', 'Jordan', 'Santana', 'N/A', 'N/A', 'E', 'N/A',  @addrid_DAL, @subdivid_DAL, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'kaila.willoughby@trescal.us', 'N/A', 'Kaila', 'Willoughby', 'N/A', 'N/A', 'E', 'N/A',  @addrid_DAL, @subdivid_DAL, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'mellisa.stcharles@trescal.us', 'N/A', 'Mellisa', 'St. Charles', 'N/A', 'N/A', 'E', 'N/A',  @addrid_HAR, @subdivid_HAR, '', sysdatetime(), 'en_US', 17280),
           (1, SYSDATETIME(), 'kenneth.rollins@trescal.us', 'N/A', 'Kenneth', 'Rollins', 'N/A', 'N/A', 'E', 'N/A',  @addrid_ATL, @subdivid_ATL, '', sysdatetime(), 'en_US', 17280);

COMMIT TRAN