-- Utility script U035
-- Identifies job costing entries which would be updated by script 821, can be run before running script 821

USE [atlas]
GO

-- Separately making sure of no null cost entries (count=0 on local testing)
/*
SELECT COUNT(1)
  FROM [dbo].[jobcostingitem] jci
  WHERE jci.calcost_id IS NULL
  OR jci.repaircost_id IS NULL
  OR jci.purchasecost_id IS NULL
  OR jci.adjustmentcost_id IS NULL
*/

-- Information about total number of job costing items, if desired (~834759 on local test)
/*
SELECT COUNT(1)
  FROM [dbo].[jobcostingitem] jci;
*/

SELECT jci.[id]
      ,jci.[finalcost]
      ,jci.[discountrate]
      ,jci.[discountvalue]
      ,jci.[itemno]
      ,jci.[quantity]
      ,jci.[totalcost]
      ,jci.[adjustmentcost_id]
      ,jci.[calcost_id]
      ,jci.[inspectioncost_id]
      ,jci.[costingid]
      ,jci.[jobitemid]
      ,jci.[purchasecost_id]
      ,jci.[repaircost_id]
      ,jci.[lastModified]
      ,jci.[lastModifiedBy]
	  ,jc.ver
	  ,job.jobno
	  ,ji.itemno
	  ,costs_cal.active as costs_cal_active
	  ,costs_cal.totalcost as costs_cal_totalcost
	  ,costs_cal.finalcost as costs_cal_finalcost
	  ,ist.active
	  ,(SELECT COUNT(1) FROM invoiceitem ii WHERE ii.jobitemid = jci.jobitemid) as invoice_item_count
	  ,(SELECT COUNT(1) FROM jobitemnotinvoiced jini WHERE jini.jobitemid = jci.jobitemid) as not_invoiced_count
  FROM [dbo].[jobcostingitem] jci
  INNER JOIN costs_calibration costs_cal ON jci.calcost_id = costs_cal.costid
  INNER JOIN costs_repair costs_rep ON jci.repaircost_id = costs_rep.costid
  INNER JOIN costs_purchase costs_pur ON jci.purchasecost_id = costs_pur.costid
  INNER JOIN costs_adjustment costs_adj ON jci.adjustmentcost_id = costs_adj.costid
  INNER JOIN jobcosting jc on jci.costingid = jc.id
  INNER JOIN jobitem ji on jci.jobitemid = ji.jobitemid
  INNER JOIN itemstate ist on ist.stateid = ji.stateid
  INNER JOIN job on job.jobid = ji.jobid
  WHERE jci.finalcost = 0
  AND costs_cal.active = 0
  AND costs_rep.active = 0
  AND costs_pur.active = 0
  AND costs_adj.active = 0
  AND costs_cal.finalcost = 0
  ORDER by jci.id desc
GO
