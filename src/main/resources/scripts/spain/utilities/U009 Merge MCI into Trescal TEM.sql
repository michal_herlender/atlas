-- Script to merge information from one business company into another:
-- LABORATORIO DE METROLOGIA Y CALIBRACION INDUSTRIAL(source company) and 
-- TRESCAL ESPA�A DE METROLOGIA SL(target company)
-- Galen Beck - 2018-12-17, updated by Obed Ortiz 2019-06-10/12
-- We plan to run on production server 2019-06-30, and not before...
-- Something to take into account, check the legal address afterwards. 

USE [spain]

BEGIN TRAN

-- Declare person that is performing the update
DECLARE @personid_admin int
DECLARE @personName varchar(20)
	SELECT @personid_admin = c.personid, @personName = c.firstname + ' ' + c.lastname FROM dbo.contact c
	WHERE c.firstname = 'Obed' and c.lastname = 'Ortiz Guido';

print 'Admin person id' + 'And name is: ' 
print @personid_admin
print @personName
print char(10)

-- Get source 'MCI-LABORATORIO' coid
DECLARE @coid_source int
	SELECT @coid_source = c.coid FROM dbo.company c
	WHERE c.coname like 'LABORATORIO DE METROLOG�A Y CALIBRACI�N INDUSTRIAL S.A.' and corole=5;

print 'Source company id :'
print @coid_source
print char(13)

-- Get target 'Trescal Espa�a de Metrolog�a' coid
DECLARE @coid_target int
	SELECT @coid_target = c.coid FROM dbo.company c
	WHERE c.coname = 'TRESCAL ESPA�A DE METROLOGIA SL' and corole=5;

print 'Target company id :'
print @coid_target

-- Get subdivisions in source company that we are moving (merging)

DECLARE @subdivids_move TABLE(subdivid int);

INSERT INTO @subdivids_move (subdivid)
	SELECT [subdivid]
	  FROM [dbo].[subdiv] WHERE coid = @coid_source AND subname IN ('Toledo', 'Sevilla');

print 'Source subdiv ids to move :'
DECLARE @xmltmp xml = (SELECT * FROM @subdivids_move FOR XML AUTO)
PRINT CONVERT(NVARCHAR(MAX), @xmltmp)

-- Update the subdivisions to be moved

UPDATE [subdiv] 
	SET coid = @coid_target, lastModified = GETDATE(), lastModifiedBy = @personid_admin
	WHERE subdivid in (SELECT subdivid FROM @subdivids_move);
print 'Moved subdivisions'

INSERT INTO [dbo].[instrumenthistory]
           ([addressupdated]
           ,[changedate]
           ,[contactupdated]
           ,[plantnoupdated]
           ,[serialnoupdated]
           ,[statusUpdated]
           ,[changeby]
           ,[plantid]
           ,[companyupdated]
           ,[newcompanyid]
           ,[oldcompanyid])
     SELECT 0
           ,GETDATE()
           ,0
           ,0
           ,0
           ,0
           ,@personid_admin
           ,instrument.plantid
           ,1
           ,@coid_target
           ,@coid_source
	FROM instrument
	WHERE instrument.coid = @coid_source;

print 'Inserted instrument company change record from Laboratorio MCI'

UPDATE instrument
	SET coid = @coid_target WHERE coid = @coid_source;

print 'Updated instruments from Laboratorio MCI'

-- Update remaining entities (business data / configuration) which should point to new company

print 'Updating QuickPurchaseOrder'

UPDATE [quickpurchaseorder] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating CompanyInstructionLink'

UPDATE [companyinstructionlink] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating SubdivInstructionLink'

UPDATE [subdivinstructionlink] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating ContactInstructionLink'

UPDATE [contactinstructionlink] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating Recall'

UPDATE [recall] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating QuotationRequest'

UPDATE [quotationrequest] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating DefaultNote'

UPDATE [defaultnote] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating SupplierInvoice'

UPDATE [supplierinvoice] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating (Client) BPO'

UPDATE [bpo] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating (Client) PO'

UPDATE [po] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating (Client) InvoicePO'

UPDATE [invoicepo] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating CustomGeneralCondition'

UPDATE [customquotationgeneralcondition] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating ExchangeFormat - businessCompany'

UPDATE [exchangeformat] SET businessCompanyid = @coid_target WHERE businessCompanyid = @coid_source;

print 'Updating PurchaseOrder'

UPDATE [purchaseorder] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating CreditNote'

UPDATE [creditnote] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating Quotation'

UPDATE [quotation] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating Invoice'

UPDATE [invoice] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating JobCosting'

UPDATE [jobcosting] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating TPQuotation'

UPDATE [tpquotation] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating TPQuoteRequest'

UPDATE [tpquoterequest] SET orgid = @coid_target WHERE orgid = @coid_source;

print 'Updating EmailTemplate - moved subdivs only'

UPDATE [emailtemplate] SET orgid = @coid_target WHERE orgid = @coid_source AND
	subdivid in (SELECT subdivid FROM @subdivids_move); 

ROLLBACK TRAN
