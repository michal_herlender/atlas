-- U032 - Update language for US servers only.sql

-- 2021-06-23
-- Script to update migrated data (on US servers only) to set:
-- a) document language for all client companies and supplier companies to en_US
-- b) contact language for all client / supplier contacts to en_US
-- c) document language for any business company in US to en_US
-- d) contact language for any contacts for business company in US to en_US

-- note ROLLBACK TRAN at end, confirm counts are expected and then update

USE [atlas]

BEGIN TRAN

DECLARE @corole_Client INT = 1;
DECLARE @corole_Supplier INT = 2;

UPDATE [dbo].[company]
   SET [documentlanguage] = 'en_US'
 WHERE corole IN (@corole_Client, @corole_Supplier);

UPDATE [dbo].[contact]
   SET [locale] = 'en_US'
  FROM [dbo].[contact] con
  JOIN subdiv sub on sub.subdivid = con.subdivid
  JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole IN (@corole_Client, @corole_Supplier);

GO

DECLARE @corole_Business INT = 5;

DECLARE @countryid_US INT;
SELECT @countryid_US = [countryid] FROM [dbo].[country] where [countrycode] = 'US';
PRINT @countryid_US;

UPDATE [dbo].[company]
   SET [documentlanguage] = 'en_US'
 WHERE corole = @corole_Business AND countryid = @countryid_US;

UPDATE [dbo].[contact]
   SET [locale] = 'en_US'
  FROM [dbo].[contact] con
  JOIN subdiv sub on sub.subdivid = con.subdivid
  JOIN company comp on comp.coid = sub.coid
 WHERE comp.corole = @corole_Business AND countryid = @countryid_US;

GO

ROLLBACK TRAN