-- Script to update various entities after script 833 is run
-- This does mainly:
-- Change AC-EX 

-- Note ROLLBACK TRAN at end, change to COMMIT after testing
-- It's expected that this should run only on US servers so platform is checked accordingly

USE [atlas]
GO

BEGIN TRAN

DECLARE @region varchar(10);
SELECT @region = region FROM dbconfig;
PRINT @region;

IF (@region = 'US')
BEGIN

DECLARE @st_AC_MC_IL int; 
DECLARE @st_AC_MC_OS int;
DECLARE @st_AC_M_IL int;
DECLARE @st_AC_M_OS int;
DECLARE @st_TC_MC_IL int; 
DECLARE @st_TC_M_IL int;
DECLARE @st_TC_M_OS int; 
DECLARE @st_TC_C_IL int;
DECLARE @st_TC_C_OS int; 
DECLARE @st_AC_EX int;
DECLARE @st_STD_EX int; 

SELECT @st_AC_MC_IL = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'AC-MC-IL';
SELECT @st_AC_MC_OS = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'AC-MC-OS';
SELECT @st_AC_M_IL = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'AC-M-IL';
SELECT @st_AC_M_OS = [servicetypeid] FROM [dbo].[servicetype] WHERE shortname = 'AC-M-OS';
SELECT @st_TC_MC_IL = servicetypeid FROM servicetype WHERE shortname = 'TC-MC-IL';

-- TC-M-IL/TC-M-OS was formerly TC-MVO-IL/TC-MVO-OS but was actually being used for "Compliant without data" service
SELECT @st_TC_M_IL = servicetypeid FROM servicetype WHERE shortname = 'TC-M-IL';
SELECT @st_TC_M_OS = servicetypeid FROM servicetype WHERE shortname = 'TC-M-OS';

SELECT @st_TC_C_IL = servicetypeid FROM servicetype WHERE shortname = 'TC-C-IL';
SELECT @st_TC_C_OS = servicetypeid FROM servicetype WHERE shortname = 'TC-C-OS';

SELECT @st_AC_EX = servicetypeid FROM servicetype WHERE shortname = 'AC-EX';
SELECT @st_STD_EX = servicetypeid FROM servicetype WHERE shortname = 'STD-EX';

PRINT '@st_AC_MC_IL : ';
PRINT @st_AC_MC_IL;
PRINT '@st_AC_MC_OS : ';
PRINT @st_AC_MC_OS;
PRINT '@st_AC_M_IL : ';
PRINT @st_AC_M_IL;
PRINT '@st_AC_M_OS : ';
PRINT @st_AC_M_OS;

PRINT '@st_TC_MC_IL : ';
PRINT @st_TC_MC_IL;
PRINT '@st_TC_M_IL : ';
PRINT @st_TC_M_IL;
PRINT '@st_TC_M_OS : ';
PRINT @st_TC_M_OS;
PRINT '@st_TC_C_IL : ';
PRINT @st_TC_C_IL;
PRINT '@st_TC_C_OS : ';
PRINT @st_TC_C_OS;
PRINT '@st_AC_EX : ';
PRINT @st_AC_EX;
PRINT '@st_STD_EX : '
PRINT @st_STD_EX;

DECLARE @ct_AC_MC_IL int; 
DECLARE @ct_AC_MC_OS int;
DECLARE @ct_AC_M_IL int;
DECLARE @ct_AC_M_OS int;
DECLARE @ct_TC_MC_IL int; 
DECLARE @ct_TC_M_IL int;
DECLARE @ct_TC_M_OS int; 
DECLARE @ct_TC_C_IL int;
DECLARE @ct_TC_C_OS int; 
DECLARE @ct_AC_EX int;
DECLARE @ct_STD_EX int; 

SELECT @ct_AC_MC_IL = caltypeid FROM calibrationtype WHERE servicetypeid = @st_AC_MC_IL;
SELECT @ct_AC_MC_OS = caltypeid FROM calibrationtype WHERE servicetypeid = @st_AC_MC_OS;
SELECT @ct_AC_M_IL = caltypeid FROM calibrationtype WHERE servicetypeid = @st_AC_M_IL;
SELECT @ct_AC_M_OS = caltypeid FROM calibrationtype WHERE servicetypeid = @st_AC_M_OS;
SELECT @ct_TC_MC_IL = caltypeid FROM calibrationtype WHERE servicetypeid = @st_TC_MC_IL;
SELECT @ct_TC_M_IL = caltypeid FROM calibrationtype WHERE servicetypeid = @st_TC_M_IL;
SELECT @ct_TC_M_OS = caltypeid FROM calibrationtype WHERE servicetypeid = @st_TC_M_OS;
SELECT @ct_TC_C_IL = caltypeid FROM calibrationtype WHERE servicetypeid = @st_TC_C_IL;
SELECT @ct_TC_C_OS = caltypeid FROM calibrationtype WHERE servicetypeid = @st_TC_C_OS;
SELECT @ct_AC_EX = caltypeid FROM calibrationtype WHERE servicetypeid = @st_AC_EX;
SELECT @ct_STD_EX = caltypeid FROM calibrationtype WHERE servicetypeid = @st_STD_EX;

PRINT '@ct_AC_MC_IL : '; 
PRINT @ct_AC_MC_IL; 
PRINT '@ct_AC_MC_OS : ';
PRINT @ct_AC_MC_OS;
PRINT '@ct_AC_M_IL : ';
PRINT @ct_AC_M_IL;
PRINT '@ct_AC_M_OS : ';
PRINT @ct_AC_M_OS;
PRINT '@ct_TC_MC_IL : '
PRINT @ct_TC_MC_IL;
PRINT '@ct_TC_M_IL : ';
PRINT @ct_TC_M_IL;
PRINT '@ct_TC_M_OS : '; 
PRINT @ct_TC_M_OS; 
PRINT '@ct_TC_C_IL : ';
PRINT @ct_TC_C_IL;
PRINT '@ct_TC_C_OS : '; 
PRINT @ct_TC_C_OS; 
PRINT '@ct_AC_EX : ';
PRINT @ct_AC_EX;
PRINT '@ct_STD_EX : ';
PRINT @ct_STD_EX;

PRINT 'Instrument : default service type : TC-M-IL -> TC-C-IL'
UPDATE instrument SET defaultservicetype = @st_TC_C_IL WHERE defaultservicetype = @st_TC_M_IL;
PRINT 'Instrument : default service type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE instrument SET defaultservicetype = @st_AC_MC_IL WHERE defaultservicetype in (@st_AC_M_IL, @st_AC_EX);
PRINT 'Instrument : default service type : TC-M-OS -> TC-C-OS'
UPDATE instrument SET defaultservicetype = @st_TC_C_OS WHERE defaultservicetype = @st_TC_M_OS;
PRINT 'Instrument : default service type : AC-M-OS -> AC-MC-OS'
UPDATE instrument SET defaultservicetype = @st_AC_MC_OS WHERE defaultservicetype = @st_AC_M_OS;
PRINT 'Instrument : default service type : STD-EX -> TC-MC-IL'
UPDATE instrument SET defaultservicetype = @st_TC_MC_IL WHERE defaultservicetype in (@st_STD_EX);

PRINT 'Work Requirement : default service type : TC-M-IL -> TC-C-IL'
UPDATE workrequirement SET servicetypeid = @st_TC_C_IL WHERE servicetypeid = @st_TC_M_IL;
PRINT 'Work Requirement : default service type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE workrequirement SET servicetypeid = @st_AC_MC_IL WHERE servicetypeid in (@st_AC_M_IL, @st_AC_EX);
PRINT 'Work Requirement : default service type : TC-M-OS -> TC-C-OS'
UPDATE workrequirement SET servicetypeid = @st_TC_C_OS WHERE servicetypeid = @st_TC_M_OS;
PRINT 'Work Requirement : default service type : AC-M-OS -> AC-MC-OS'
UPDATE workrequirement SET servicetypeid = @st_AC_MC_OS WHERE servicetypeid = @st_AC_M_OS;
PRINT 'Work Requirement : default service type : STD-EX -> TC-MC-IL'
UPDATE workrequirement SET servicetypeid = @st_TC_MC_IL WHERE servicetypeid in (@st_STD_EX);

PRINT 'Job Item : service type : TC-M-IL -> TC-C-IL'
UPDATE jobitem SET servicetypeid = @st_TC_C_IL WHERE servicetypeid = @st_TC_M_IL;
PRINT 'Job Item : service type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE jobitem SET servicetypeid = @st_AC_MC_IL WHERE servicetypeid in (@st_AC_M_IL, @st_AC_EX);
PRINT 'Job Item : service type : TC-M-OS -> TC-C-OS'
UPDATE jobitem SET servicetypeid = @st_TC_C_OS WHERE servicetypeid = @st_TC_M_OS;
PRINT 'Job Item : service type : AC-M-OS -> AC-MC-OS'
UPDATE jobitem SET servicetypeid = @st_AC_MC_OS WHERE servicetypeid = @st_AC_M_OS;
PRINT 'Job Item : service type : STD-EX -> TC-MC-IL'
UPDATE jobitem SET servicetypeid = @st_TC_MC_IL WHERE servicetypeid in (@st_STD_EX);

PRINT 'Job Item : cal type : TC-M-IL -> TC-C-IL'
UPDATE jobitem SET caltypeid = @ct_TC_C_IL WHERE caltypeid = @ct_TC_M_IL;
PRINT 'Job Item : cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE jobitem SET caltypeid = @ct_AC_MC_IL WHERE caltypeid in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'Job Item : cal type : TC-M-OS -> TC-C-OS'
UPDATE jobitem SET caltypeid = @ct_TC_C_OS WHERE caltypeid = @ct_TC_M_OS;
PRINT 'Job Item : cal type : AC-M-OS -> AC-MC-OS'
UPDATE jobitem SET caltypeid = @ct_AC_MC_OS WHERE caltypeid = @ct_AC_M_OS;
PRINT 'Job Item : cal type : STD-EX -> TC-MC-IL'
UPDATE jobitem SET caltypeid = @ct_TC_MC_IL WHERE caltypeid in (@ct_STD_EX);

PRINT 'Quotation Item : service type : TC-M-IL -> TC-C-IL'
UPDATE quotationitem SET servicetypeid = @st_TC_C_IL WHERE servicetypeid = @st_TC_M_IL;
PRINT 'Quotation Item : service type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE quotationitem SET servicetypeid = @st_AC_MC_IL WHERE servicetypeid in (@st_AC_M_IL, @st_AC_EX);
PRINT 'Quotation Item : service type : TC-M-OS -> TC-C-OS'
UPDATE quotationitem SET servicetypeid = @st_TC_C_OS WHERE servicetypeid = @st_TC_M_OS;
PRINT 'Quotation Item : service type : AC-M-OS -> AC-MC-OS'
UPDATE quotationitem SET servicetypeid = @st_AC_MC_OS WHERE servicetypeid = @st_AC_M_OS;
PRINT 'Quotation Item : service type : STD-EX -> TC-MC-IL'
UPDATE quotationitem SET servicetypeid = @st_TC_MC_IL WHERE servicetypeid in (@st_STD_EX);

PRINT 'Quotation Item : cal type : TC-M-IL -> TC-C-IL'
UPDATE quotationitem SET caltype_caltypeid = @ct_TC_C_IL WHERE caltype_caltypeid = @ct_TC_M_IL;
PRINT 'Quotation Item : cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE quotationitem SET caltype_caltypeid = @ct_AC_MC_IL WHERE caltype_caltypeid in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'Quotation Item : cal type : TC-M-OS -> TC-C-OS'
UPDATE quotationitem SET caltype_caltypeid = @ct_TC_C_OS WHERE caltype_caltypeid = @ct_TC_M_OS;
PRINT 'Quotation Item : cal type : AC-M-OS -> AC-MC-OS'
UPDATE quotationitem SET caltype_caltypeid = @ct_AC_MC_OS WHERE caltype_caltypeid = @ct_AC_M_OS;
PRINT 'Quotation Item : cal type : STD-EX -> TC-MC-IL'
UPDATE quotationitem SET caltype_caltypeid = @ct_TC_MC_IL WHERE caltype_caltypeid in (@ct_STD_EX);

-- It seems that on some migrated certificates, new servicetype field is not set, so using caltype as reference for updates
PRINT 'Certificate : service type : TC-M-IL -> TC-C-IL'
UPDATE dbo.certificate SET servicetypeid = @st_TC_C_IL WHERE caltype = @ct_TC_M_IL;
PRINT 'Certificate : service type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE dbo.certificate SET servicetypeid = @st_AC_MC_IL WHERE caltype in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'Certificate : service type : TC-M-OS -> TC-C-OS'
UPDATE dbo.certificate SET servicetypeid = @st_TC_C_OS WHERE caltype = @ct_TC_M_OS;
PRINT 'Certificate : service type : AC-M-OS -> AC-MC-OS'
UPDATE dbo.certificate SET servicetypeid = @st_AC_MC_OS WHERE caltype = @ct_AC_M_OS;
PRINT 'Certificate : service type : STD-EX -> TC-MC-IL'
UPDATE dbo.certificate SET servicetypeid = @st_TC_MC_IL WHERE caltype = @ct_STD_EX;

PRINT 'Certificate : cal type : TC-M-IL -> TC-C-IL'
UPDATE dbo.certificate SET caltype = @ct_TC_C_IL WHERE caltype = @ct_TC_M_IL;
PRINT 'Certificate : cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE dbo.certificate SET caltype = @ct_AC_MC_IL WHERE caltype in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'Certificate : cal type : TC-M-OS -> TC-C-OS'
UPDATE dbo.certificate SET caltype = @ct_TC_C_OS WHERE caltype = @ct_TC_M_OS;
PRINT 'Certificate : cal type : AC-M-OS -> AC-MC-OS'
UPDATE dbo.certificate SET caltype = @ct_AC_MC_OS WHERE caltype = @ct_AC_M_OS;
PRINT 'Certificate : cal type : STD-EX -> TC-MC-IL'
UPDATE dbo.certificate SET caltype = @st_TC_MC_IL WHERE caltype = @st_STD_EX;

PRINT 'Calibration : cal type : TC-M-IL -> TC-C-IL'
UPDATE dbo.calibration SET caltypeid = @ct_TC_C_IL WHERE caltypeid = @ct_TC_M_IL;
PRINT 'Calibration : cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE dbo.calibration SET caltypeid = @ct_AC_MC_IL WHERE caltypeid in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'Calibration : cal type : TC-M-OS -> TC-C-OS'
UPDATE dbo.calibration SET caltypeid = @ct_TC_C_OS WHERE caltypeid = @ct_TC_M_OS;
PRINT 'Calibration : cal type : AC-M-OS -> AC-MC-OS'
UPDATE dbo.calibration SET caltypeid = @ct_AC_MC_OS WHERE caltypeid = @ct_AC_M_OS;
PRINT 'Calibration : cal type : STD-EX -> TC-MC-IL'
UPDATE dbo.calibration SET caltypeid = @st_TC_MC_IL WHERE caltypeid = @st_STD_EX;

PRINT 'Job : default cal type : TC-M-IL -> TC-C-IL'
UPDATE dbo.job SET defaultcaltypeid = @ct_TC_C_IL WHERE defaultcaltypeid = @ct_TC_M_IL;
PRINT 'Job : default cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE dbo.job SET defaultcaltypeid = @ct_AC_MC_IL WHERE defaultcaltypeid in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'Job : default cal type : TC-M-OS -> TC-C-OS'
UPDATE dbo.job SET defaultcaltypeid = @ct_TC_C_OS WHERE defaultcaltypeid = @ct_TC_M_OS;
PRINT 'Job : default cal type : AC-M-OS -> AC-MC-OS'
UPDATE dbo.job SET defaultcaltypeid = @ct_AC_MC_OS WHERE defaultcaltypeid = @ct_AC_M_OS;
PRINT 'Job : default cal type : STD-EX -> TC-MC-IL'
UPDATE dbo.job SET defaultcaltypeid = @st_TC_MC_IL WHERE defaultcaltypeid = @st_STD_EX;

PRINT 'Quotation Cal Type Default : cal type : TC-M-IL -> TC-C-IL'
UPDATE quotecaltypedefault SET caltype_caltypeid = @ct_TC_C_IL WHERE caltype_caltypeid = @ct_TC_M_IL;
PRINT 'Quotation Cal Type Default : cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE quotecaltypedefault SET caltype_caltypeid = @ct_AC_MC_IL WHERE caltype_caltypeid in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'Quotation Cal Type Default : cal type : TC-M-OS -> TC-C-OS'
UPDATE quotecaltypedefault SET caltype_caltypeid = @ct_TC_C_OS WHERE caltype_caltypeid = @ct_TC_M_OS;
PRINT 'Quotation Cal Type Default : cal type : AC-M-OS -> AC-MC-OS'
UPDATE quotecaltypedefault SET caltype_caltypeid = @ct_AC_MC_OS WHERE caltype_caltypeid = @ct_AC_M_OS;
PRINT 'Quotation Cal Type Default : cal type : STD-EX -> TC-MC-IL'
UPDATE quotecaltypedefault SET caltype_caltypeid = @ct_TC_MC_IL WHERE caltype_caltypeid in (@ct_STD_EX);

PRINT 'TP Quotation Cal Type Default : cal type : TC-M-IL -> TC-C-IL'
UPDATE tpquotationcaltypedefault SET caltypeid = @ct_TC_C_IL WHERE caltypeid = @ct_TC_M_IL;
PRINT 'TP Quotation Cal Type Default : cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE tpquotationcaltypedefault SET caltypeid = @ct_AC_MC_IL WHERE caltypeid in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'TP Quotation Cal Type Default : cal type : TC-M-OS -> TC-C-OS'
UPDATE tpquotationcaltypedefault SET caltypeid = @ct_TC_C_OS WHERE caltypeid = @ct_TC_M_OS;
PRINT 'TP Quotation Cal Type Default : cal type : AC-M-OS -> AC-MC-OS'
UPDATE tpquotationcaltypedefault SET caltypeid = @ct_AC_MC_OS WHERE caltypeid = @ct_AC_M_OS;
PRINT 'TP Quotation Cal Type Default : cal type : STD-EX -> TC-MC-IL'
UPDATE tpquotationcaltypedefault SET caltypeid = @ct_TC_MC_IL WHERE caltypeid in (@ct_STD_EX);

PRINT 'TP Quotation Item : cal type : TC-M-IL -> TC-C-IL'
UPDATE tpquotationitem SET caltype_caltypeid = @ct_TC_C_IL WHERE caltype_caltypeid = @ct_TC_M_IL;
PRINT 'TP Quotation Item : cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE tpquotationitem SET caltype_caltypeid = @ct_AC_MC_IL WHERE caltype_caltypeid in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'TP Quotation Item : cal type : TC-M-OS -> TC-C-OS'
UPDATE tpquotationitem SET caltype_caltypeid = @ct_TC_C_OS WHERE caltype_caltypeid = @ct_TC_M_OS;
PRINT 'TP Quotation Item : cal type : AC-M-OS -> AC-MC-OS'
UPDATE tpquotationitem SET caltype_caltypeid = @ct_AC_MC_OS WHERE caltype_caltypeid = @ct_AC_M_OS;
PRINT 'TP Quotation Item : cal type : STD-EX -> TC-MC-IL'
UPDATE tpquotationitem SET caltype_caltypeid = @ct_TC_MC_IL WHERE caltype_caltypeid in (@ct_STD_EX);

PRINT 'TP Quote Request Item : cal type : TC-M-IL -> TC-C-IL'
UPDATE tpquoterequestitem SET caltypeid = @ct_TC_C_IL WHERE caltypeid = @ct_TC_M_IL;
PRINT 'TP Quote Request Item : cal type : AC-M-IL/AC-EX -> AC-MC-IL'
UPDATE tpquoterequestitem SET caltypeid = @ct_AC_MC_IL WHERE caltypeid in (@ct_AC_M_IL, @ct_AC_EX);
PRINT 'TP Quote Request Item : cal type : TC-M-OS -> TC-C-OS'
UPDATE tpquoterequestitem SET caltypeid = @ct_TC_C_OS WHERE caltypeid = @ct_TC_M_OS;
PRINT 'TP Quote Request Item : cal type : AC-M-OS -> AC-MC-OS'
UPDATE tpquoterequestitem SET caltypeid = @ct_AC_MC_OS WHERE caltypeid = @ct_AC_M_OS;
PRINT 'TP Quote Request Item : cal type : STD-EX -> TC-MC-IL'
UPDATE tpquoterequestitem SET caltypeid = @ct_TC_MC_IL WHERE caltypeid in (@ct_STD_EX);

END

ELSE
BEGIN
	PRINT 'Warning, region is not US, no action performed'
END

ROLLBACK TRAN