

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'instrumentusagetype',  
@role_name     = N'sa',  
@captured_column_list = '[typeid]
   ,[defaulttype]
   ,[fulldescription]
   ,[name]
   ,[recall]
   ,[instrumentstatus]
 ',
@supports_net_changes = 1  
GO