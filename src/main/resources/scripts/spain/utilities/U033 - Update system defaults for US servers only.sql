-- Utility script for US systems only to update global system defaults for date formats and time zones

USE [atlas]
GO

UPDATE [dbo].[systemdefault]
   SET [defaultvalue] = 'MM/dd/yyyy'
 WHERE [defaultid] = 48;
GO

UPDATE [dbo].[systemdefault]
   SET [defaultvalue] = 'US/Eastern'
 WHERE [defaultid] = 67;
GO