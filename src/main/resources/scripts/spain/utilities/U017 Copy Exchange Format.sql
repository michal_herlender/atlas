-- Utility script to copy an exchange format to a different business company
-- Intended initially for use to copy exchange formats for calibration import from Trescal SAS to Intermes.
-- Note (a) You will have to choose which exchange format to copy (and run three times, once for each) in the SELECT @exchange_format_name
--      (b) Replace ROLLBACK with COMMIT locally after testing (each of the 'ids' should have a value reported
--      (c) You can modify the 'SELECT @person_id' SELECT for your own user name if desired
--      (d) Before running this, you should insert an alias group for the new business company (see script 599 for Intermes)


USE [atlas]
GO

BEGIN TRAN

-- General defintions, can change as needed on SELECT lines
DECLARE @exchange_format_name VARCHAR(100);
DECLARE @username VARCHAR(20);
DECLARE @person_id INT;
DECLARE @source_business_company_id INT;
DECLARE @target_business_company_id INT;
DECLARE @target_alias_group_id INT;
DECLARE @old_exchangeformat_id INT;

SELECT @exchange_format_name = '';
--SELECT @exchange_format_name = 'TLM OP Site Instrument';
--SELECT @exchange_format_name = 'TLM OP Site Prebooking';
--SELECT @exchange_format_name = 'TLM OP Site Calibration';
SELECT @username = 'GalenB';

SELECT @person_id = c.personid from dbo.contact c INNER JOIN dbo.users u ON u.personid = c.personid WHERE u.username = @username;
SELECT @source_business_company_id = coid FROM dbo.company WHERE [coname] = 'Trescal SAS' and corole = 5;
SELECT @target_business_company_id = coid FROM dbo.company WHERE [coname] = 'Intermes' and corole = 5;
SELECT @target_alias_group_id = id FROM dbo.aliasgroup WHERE [orgid] = @target_business_company_id and defaultBusinessCompany = 1;
SELECT @old_exchangeformat_id = id FROM [dbo].[exchangeformat] WHERE name = @exchange_format_name AND [businessCompanyid] = @source_business_company_id;

PRINT 'personid : '
PRINT @person_id;
PRINT 'source_business_company_id : '
PRINT @source_business_company_id;
PRINT 'target_business_company_id : '
PRINT @target_business_company_id;
PRINT 'target_alias_group_id : '
PRINT @target_alias_group_id;
PRINT 'old_exchangeformat_id : '
PRINT @old_exchangeformat_id;

-- Fields not included : [businessSubdivid] (leave null - business company dependent), [id] (autoassigned)

INSERT INTO [dbo].[exchangeformat]
           ([name]
           ,[description]
           ,[clientCompanyid]
           ,[businessCompanyid]
           ,[exchangeFormatLevel]
           ,[dateFormat]
           ,[lastModified]
           ,[lastModifiedBy]
           ,[linestoskip]
           ,[sheetname]
           ,[eftype]
           ,[aliasgroupid]
           ,[deleted])
     SELECT [name]
		  ,[description]
		  ,[clientCompanyid]
		  ,@target_business_company_id
		  ,[exchangeFormatLevel]
		  ,[dateFormat]
		  ,GETDATE()
		  ,@person_id
		  ,[linestoskip]
		  ,[sheetname]
		  ,[eftype]
		  ,@target_alias_group_id
		  ,[deleted]
	  FROM [dbo].[exchangeformat] 
	  WHERE id = @old_exchangeformat_id;

DECLARE @new_exchangeformat_id INT;

SELECT @new_exchangeformat_id = id FROM [dbo].[exchangeformat] 
WHERE name = @exchange_format_name
AND [businessCompanyid] = @target_business_company_id;

PRINT 'new_exchangeformat_id : '
PRINT @new_exchangeformat_id;

INSERT INTO [dbo].[exchangeformatfieldnamedetails]
           ([position]
           ,[mandatory]
           ,[templateName]
           ,[fieldName]
           ,[exchangeFormatid])
    SELECT [position]
      ,[mandatory]
      ,[templateName]
      ,[fieldName]
      ,@new_exchangeformat_id
	FROM [dbo].[exchangeformatfieldnamedetails] WHERE [exchangeformatid] = @old_exchangeformat_id
GO

ROLLBACK TRAN