-- The following will shrink the size of the database, 
-- e.g. after running script U023 to remove client data


USE [atlas]

GO

ALTER DATABASE [atlas] SET RECOVERY SIMPLE WITH NO_WAIT

DBCC SHRINKDATABASE (atlas)

ALTER DATABASE [atlas] SET RECOVERY FULL WITH NO_WAIT

GO
