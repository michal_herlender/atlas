
-- GROUP 4 - Job Data 

EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'onbehalfitem',  
@role_name     = N'sa',  
@captured_column_list = '[id]
      ,[addressid]
      ,[coid]
      ,[jobitemid]',
@supports_net_changes = 1  
GO
