-- Utility script U007

-- This script finds duplicate certificatevalidation records for the same certificate ID
-- See script U008 for a way to delete the duplicates (retaining the newest record)
-- If there are no results, there are no duplicates :-)

-- Galen Beck - 2019-05-14

USE [tempdb]
GO

-- Clean up temporary table, if it exists from a previous failure of this script
IF OBJECT_ID('#duplicate_certval') IS NOT NULL
	DROP TABLE #duplicate_certval
GO

USE [atlas]
GO

-- Create a temporary table summarizing count of validation records for each certid
SELECT certificate.[certid] as cert_id,
(SELECT COUNT(*) FROM certificatevalidation where certificatevalidation.certid = certificate.certid) as val_count,
(SELECT TOP 1 certificatevalidation.id FROM certificatevalidation where certificatevalidation.certid = certificate.certid ORDER BY certificatevalidation.id DESC) as max_certval_id
INTO #duplicate_certval
FROM [dbo].[certificate]
GO

-- Retain just the records that indicate duplicates (0, 1 = no duplicates)
DELETE FROM #duplicate_certval WHERE val_count < 2
GO

-- This finds both the duplicates to keep, and the records to remove.
-- If desired the two commented-out lines below can be uncommented, to show just the records to remove
SELECT [id]
	  ,(SELECT max_certval_id FROM #duplicate_certval WHERE cert_id = certid) AS max_certval
      ,[note]
      ,[responsible]
      ,[log_userid]
      ,[certid]
      ,[document]
      ,[lastModified]
      ,[lastModifiedBy]
      ,[personid]
      ,[validationstatus]
  FROM [dbo].[certificatevalidation] WHERE certid IN 
  (SELECT cert_id FROM #duplicate_certval
	WHERE val_count > 1
  )
--  AND id NOT IN
--  (SELECT max_certval_id FROM #duplicate_certval WHERE cert_id = certid)
  order by certid, lastModified desc;
GO

-- Clear up [tempdb] in case connection left open in SSMS

USE [tempdb]
GO

IF OBJECT_ID('#duplicate_certval') IS NOT NULL
	DROP TABLE #duplicate_certval
	GO
GO