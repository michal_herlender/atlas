-- Contains just the additional tables added in DEV-2864

 EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'subdivsettingsforallocatedsubdiv',  
@role_name     = N'sa', 
@captured_column_list = '[id]
		 ,[orgid]
		 ,[businesscontactid]
		 ,[subdivid]',
@supports_net_changes = 1  
GO

--Group 9 - Invoice Data

 EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'invoice',  
@role_name     = N'sa', 
@captured_column_list = '[id]
		 ,[issued]
		 ,[issuedate]
		 ,[finalcost]
		 ,[totalcost]
		 ,[vatvalue]
		 ,[invno]
		 ,[addressid]
		 ,[coid]
		 ,[orgid]
		 ,[duedate]',
@supports_net_changes = 1  
GO

 EXEC sys.sp_cdc_enable_table  
@source_schema = N'dbo',  
@source_name   = N'creditnote',  
@role_name     = N'sa', 
@captured_column_list = '[id]
		 ,[issued]
		 ,[issuedate]
		 ,[finalcost]
		 ,[totalcost]
		 ,[vatvalue]
		 ,[creditnoteno]
		 ,[invid]
		 ,[orgid]',
@supports_net_changes = 1  
GO