-- Utility to find/report disconnected job item po entities not matching bpos set on jobs
-- Author Galen Beck - 2016-09-27

USE [spain]
GO

SELECT [jobitempo].jobitempoid
      ,[jobitempo].bpoid
      ,[jobitempo].jobitemid
      ,[jobitempo].poid
	  ,[jobitem].jobitemid
	  ,[jobitem].jobid
	  ,[jobitem].itemno
	  ,[job].jobno
	  ,[job].jobid
	  ,[job].bpoid
	  ,[bpo].poid
	  ,[bpo].ponumber
	  ,[bpo].comment
	  ,[bpo].active
  FROM [jobitempo]
  LEFT JOIN [jobitem] ON [jobitempo].jobitemid = [jobitem].jobitemid
  LEFT JOIN [job] ON [jobitem].jobid = [job].jobid
  LEFT JOIN [bpo] ON [bpo].poid = [jobitempo].bpoid
  WHERE ([job].bpoid <> [jobitempo].bpoid) OR 
		([job].bpoid IS NULL AND [jobitempo].bpoid IS NOT NULL)
GO


