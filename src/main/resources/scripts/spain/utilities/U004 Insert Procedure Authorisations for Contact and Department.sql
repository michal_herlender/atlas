-- Script to insert procedure authorisation into all procedures for a department
-- Note ROLLBACK TRAN at end, so we can check the bulk insert before it commits
-- You'll need to modify the contact_id and dept_id variables below

-- Author Galen Beck - 2017-09-15

USE [spain]
GO

BEGIN TRAN

SET NOCOUNT ON; 

DECLARE @contact_id int = 17545,	-- Contact id of the person to insert for (17545 = Marta in Madrid)
		@dept_id int = 25,			-- Department id of the department to insert for (25 = TEM-MAD-Temp/Hum Lab)
		@authorised_id int = 8740,	-- Contact id of the person authorising the procedure (8740=CWMS_AUTO_USER)
		@count_existing int = 0,
		@count_new int = 0;

DECLARE @proc_id int, 
		@proc_name varchar(200), 
		@proc_reference varchar(20),
		@caltype_id int,
		@servicetype_shortname varchar(10),
		@output_text varchar(255);

DECLARE procs_cursor CURSOR LOCAL FOR
	SELECT [procs].[id] AS var_procid
		,[procs].[name]
		,[procs].[reference]
		  FROM [procs] 
		  LEFT JOIN [categorisedprocedure] ON [procs].[id] = [categorisedprocedure].procid
		  LEFT JOIN [procedurecategory] ON [categorisedprocedure].categoryid = [procedurecategory].id
		  WHERE [procedurecategory].departmentid = @dept_id;

OPEN procs_cursor
FETCH NEXT FROM procs_cursor INTO @proc_id, @proc_name, @proc_reference;

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE caltype_cursor CURSOR LOCAL FOR
		SELECT [caltypeid], servicetype.shortname FROM [calibrationtype] 
			LEFT JOIN [servicetype] on [calibrationtype].servicetypeid = [servicetype].servicetypeid
			WHERE servicetype.shortname IN ('AC-IH', 'ST-IH', 'AC-OS', 'ST-OS', 'OTHER', 'ST-IC', 'AC-IC')
	OPEN caltype_cursor 
	FETCH NEXT FROM caltype_cursor INTO @caltype_id, @servicetype_shortname;
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @output_text = 'servicetype:'+@servicetype_shortname+' proc_id:'+CAST(@proc_id AS varchar(10))+' '+@proc_name+' '+@proc_reference;

			IF EXISTS (SELECT 1 FROM [procedureaccreditation] WHERE procid = @proc_id AND accreditationfor = @contact_id AND caltypeid = @caltype_id)
				BEGIN
					PRINT('OK: '+@output_text);
					SET @count_existing = @count_existing + 1;
				END
			ELSE
				BEGIN
					PRINT('NEW: '+@output_text);
					SET @count_new = @count_new + 1;
					INSERT INTO [dbo].[procedureaccreditation]
								([accredlevel]
								,[awarded]
								,[accreditationfor]
								,[awardedby]
								,[caltypeid]
								,[procid]
								,[lastModified]
								,[lastModifiedBy])
							VALUES
								('APPROVE'
								,GETDATE()
								,@contact_id
								,@authorised_id
								,@caltype_id
								,@proc_id
								,GETDATE()
								,@authorised_id)
				END
			FETCH NEXT FROM caltype_cursor INTO @caltype_id, @servicetype_shortname;
		END
	FETCH NEXT FROM procs_cursor INTO @proc_id, @proc_name, @proc_reference;
	CLOSE caltype_cursor
	DEALLOCATE caltype_cursor
END

CLOSE procs_cursor
DEALLOCATE procs_cursor;

PRINT('Summary:');
PRINT('Total OK:'+CAST(@count_existing AS varchar(10)));
PRINT('Total New:'+CAST(@count_new AS varchar(10)));

SET NOCOUNT OFF;

ROLLBACK TRAN