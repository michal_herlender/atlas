-- Script to update capabilities after script 833 is run
-- This does a basic rename of capabilities (and minor reconfiguration)
-- Note ROLLBACK TRAN at end, change to COMMIT after testing

USE [atlas]
GO

BEGIN TRAN

DECLARE @coid_TUS int;
DECLARE @subdivid_CLE int;

SELECT @coid_TUS = coid from company WHERE coname='TRESCAL INC' and corole = 5;
SELECT @subdivid_CLE = subdivid from subdiv WHERE coid=@coid_TUS AND subdivcode = 'CLE';

-- All the data here should exist and not be empty... script 833 must be run first.

PRINT '@coid_TUS : ';
PRINT @coid_TUS;

PRINT '@subivid_CLE : ';
PRINT @subdivid_CLE;

DECLARE @st_TC_M_IL int;
DECLARE @st_TC_M_OS int; 
DECLARE @st_TC_C_IL int;
DECLARE @st_TC_C_OS int; 

-- TC-M-IL/TC-M-OS was formerly TC-MVO-IL/TC-MVO-OS but was actually being used for "Compliant without data" service
SELECT @st_TC_M_IL = servicetypeid FROM servicetype WHERE shortname = 'TC-M-IL';
SELECT @st_TC_M_OS = servicetypeid FROM servicetype WHERE shortname = 'TC-M-OS';

SELECT @st_TC_C_IL = servicetypeid FROM servicetype WHERE shortname = 'TC-C-IL';
SELECT @st_TC_C_OS = servicetypeid FROM servicetype WHERE shortname = 'TC-C-OS';

PRINT '@st_TC_M_IL : ';
PRINT @st_TC_M_IL;
PRINT '@st_TC_M_OS : ';
PRINT @st_TC_M_OS;
PRINT '@st_TC_C_IL : ';
PRINT @st_TC_C_IL;
PRINT '@st_TC_C_OS : ';
PRINT @st_TC_C_OS;

-- Select Cleveland capabilities and iterate through to update

DECLARE @proc_name varchar(200);
DECLARE @proc_id int;
DECLARE @proc_reference varchar(20);
DECLARE @proc_newname varchar(200);

DECLARE csr_procs CURSOR LOCAL FAST_FORWARD READ_ONLY for 
SELECT [name],[id],[reference]
	FROM procs WHERE orgid = @subdivid_CLE AND subfamilyid IS NOT NULL 
OPEN csr_procs
FETCH NEXT FROM csr_procs INTO @proc_name, @proc_id, @proc_reference
WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT @proc_reference;
	-- 8 different replacement patterns; only one (at most) will be used
	SET @proc_newname = REPLACE(@proc_name, 'Accredited calibration with statement of compliance in laboratory',
			'Accredited calibration with measurement results and statement of conformity in laboratory');
	SET @proc_newname = REPLACE(@proc_newname, 'Traceable calibration with statement of compliance in laboratory',
			'Traceable calibration with measurement results and statement of conformity in laboratory');
	SET @proc_newname = REPLACE(@proc_newname, 'Accredited calibration with statement of compliance on site',
			'Accredited calibration with measurement results and statement of conformity on site');
	SET @proc_newname = REPLACE(@proc_newname, 'Traceable calibration with statement of compliance on site',
			'Traceable calibration with measurement results and statement of conformity on site');
	SET @proc_newname = REPLACE(@proc_newname, 'Accredited calibration without statement of compliance in laboratory',
			'Accredited calibration with measurement results in laboratory');
	SET @proc_newname = REPLACE(@proc_newname, 'Traceable calibration without statement of compliance in laboratory',
			'Traceable calibration with statement of conformity in laboratory');
	SET @proc_newname = REPLACE(@proc_newname, 'Accredited calibration without statement of compliance on site',
			'Accredited calibration with measurement results on site');
	SET @proc_newname = REPLACE(@proc_newname, 'Traceable calibration without statement of compliance on site',
			'Traceable calibration with statement of conformity on site');
	PRINT 'Old : ';
	PRINT @proc_name;
	PRINT 'New : ';
	PRINT @proc_newname;
	UPDATE procs SET [name] = @proc_newname WHERE [id] = @proc_id;
	FETCH NEXT FROM csr_procs INTO @proc_name, @proc_id, @proc_reference
END
CLOSE csr_procs
DEALLOCATE csr_procs

-- Update any capability filters using TC-M-IL (was TC-MVO-IL) to TC-C-IL
-- Update any capability filters using TC-M-OS (was TC-MVO-OS) to TC-C-OS

PRINT 'Update TC-M-IL filters (was TC-MVO-IL) to TC-C-IL'
UPDATE capability_servicetype 
	SET servicetypeid = @st_TC_C_IL FROM capability_servicetype cst 
	INNER JOIN procs on procs.id = cst.procedureid 
	WHERE cst.servicetypeid=@st_TC_M_IL AND procs.orgid = @subdivid_CLE;

PRINT 'Update TC-M-OS filters (was TC-MVO-OS) to TC-C-OS'
UPDATE capability_servicetype 
	SET servicetypeid = @st_TC_C_OS FROM capability_servicetype cst 
	INNER JOIN procs on procs.id = cst.procedureid 
	WHERE cst.servicetypeid=@st_TC_M_OS AND procs.orgid = @subdivid_CLE;

ROLLBACK TRAN