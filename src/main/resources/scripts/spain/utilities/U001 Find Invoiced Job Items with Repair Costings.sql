-- Query to find job items which have been billed after 2016-09-15 that have job costings with non-zero repair costs
-- to identify any issues from bug between commit 1965 (release night of 2016-09-15) and 1993 (release night of 2016-09-26)
-- Author Galen Beck 

USE [spain]
GO

SELECT [costs_repair].costtype
      ,[costs_repair].costid
      ,[costs_repair].active
      ,[costs_repair].finalcost
      ,[costs_repair].totalcost
	  ,[jobcostingitem].id
	  ,[jobcostingitem].jobitemid
	  ,[jobcostingitem].repaircost_id
	  ,[invoiceitem].jobitemid
	  ,[invoiceitem].invoiceid
	  ,[invoice].id
	  ,[invoice].regdate
	  ,[invoice].invno
	  ,[jobitem].jobid
	  ,[jobitem].jobitemid
	  ,[jobitem].itemno
	  ,[job].jobid
	  ,[job].jobno
  FROM [costs_repair] 
  
  LEFT JOIN [jobcostingitem] ON [costs_repair].costid = [jobcostingitem].repaircost_id
  LEFT JOIN [invoiceitem] ON [jobcostingitem].jobitemid = [invoiceitem].jobitemid
  LEFT JOIN [invoice] ON [invoiceitem].invoiceid = [invoice].id
  LEFT JOIN [jobitem] ON [jobcostingitem].jobitemid = [jobitem].jobitemid
  LEFT JOIN [job] ON [job].jobid = [jobitem].jobid
 
  WHERE [costs_repair].finalcost > 0 AND 
        [invoice].regdate > '2016-09-15';
GO