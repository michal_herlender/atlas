-- Script to clear the three tables used for Adveso notification / activity queues
-- ROLLBACK is intentional at the end to help verify that this is what we want to do;
--  please manually change to COMMIT when certain

USE [atlas]
GO

BEGIN TRAN

PRINT 'Deleting all from notificationsystemfieldchange'

DELETE FROM [dbo].[notificationsystemfieldchange]
GO

PRINT 'Deleting all from notificationsystemqueue'

DELETE FROM [dbo].[notificationsystemqueue]
GO

PRINT 'Deleting all from advesojobItemactivityqueue'

DELETE FROM [dbo].[advesojobItemactivityqueue]
GO

ROLLBACK TRAN