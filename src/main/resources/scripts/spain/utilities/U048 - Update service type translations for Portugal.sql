-- Updates service types for Portugal, running on STG first
-- Note, script U045 to copy translations should have been run first

BEGIN TRAN

-- Short names

UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'ST-CAR-EL' WHERE locale='pt_PT' and servicetypeid = 16;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'AC-CAR-EL' WHERE locale='pt_PT' and servicetypeid = 15;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'ST-CAR-IS' WHERE locale='pt_PT' and servicetypeid = 18;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'AC-CAR-IS' WHERE locale='pt_PT' and servicetypeid = 17;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'ST-EL' WHERE locale='pt_PT' and servicetypeid = 2;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'AC-EL' WHERE locale='pt_PT' and servicetypeid = 1;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'ST-IS' WHERE locale='pt_PT' and servicetypeid = 5;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'AC-IS' WHERE locale='pt_PT' and servicetypeid = 4;

UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'MP' WHERE locale='pt_PT' and servicetypeid = 27;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'CI-EL' WHERE locale='pt_PT' and servicetypeid = 28;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'CI-IS' WHERE locale='pt_PT' and servicetypeid = 35;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'AC-EX' WHERE locale='pt_PT' and servicetypeid = 7;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'ST-EX' WHERE locale='pt_PT' and servicetypeid = 8;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'CLIENT' WHERE locale='pt_PT' and servicetypeid = 29;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'CLIENT-EX' WHERE locale='pt_PT' and servicetypeid = 30;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'REP' WHERE locale='pt_PT' and servicetypeid = 6;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'OUTROS' WHERE locale='pt_PT' and servicetypeid = 3;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'Servi�o' WHERE locale='pt_PT' and servicetypeid = 31;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'VCF-EL' WHERE locale='pt_PT' and servicetypeid = 32;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'VCF-IS' WHERE locale='pt_PT' and servicetypeid = 36;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'QUAL' WHERE locale='pt_PT' and servicetypeid = 33;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'VAL' WHERE locale='pt_PT' and servicetypeid = 34;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'TA' WHERE locale='pt_PT' and servicetypeid = 37;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'ICB' WHERE locale='pt_PT' and servicetypeid = 38;
UPDATE [dbo].[servicetypeshortnametranslation] SET [translation] = 'TL' WHERE locale='pt_PT' and servicetypeid = 39;

-- Long names

UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o n�o acreditada com verifica��o CAR em laborat�rio' WHERE locale='pt_PT' and servicetypeid = 16;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o acreditada com verifica��o CAR em laborat�rio' WHERE locale='pt_PT' and servicetypeid = 15;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o n�o acreditada com verifica��o CAR in situ' WHERE locale='pt_PT' and servicetypeid = 18;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o acreditada com verifica��o CAR in situ' WHERE locale='pt_PT' and servicetypeid = 17;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o n�o acreditada em laborat�rio' WHERE locale='pt_PT' and servicetypeid = 2;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o acreditada em laborat�rio' WHERE locale='pt_PT' and servicetypeid = 1;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o n�o acreditada in situ' WHERE locale='pt_PT' and servicetypeid = 5;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o n�o acreditada in situ' WHERE locale='pt_PT' and servicetypeid = 4;

UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Manuten��o preventiva' WHERE locale='pt_PT' and servicetypeid = 27;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Controlo interm�dio em laborat�rio' WHERE locale='pt_PT' and servicetypeid = 28;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Controlo interm�dio in situ' WHERE locale='pt_PT' and servicetypeid = 35;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o acreditada Externa' WHERE locale='pt_PT' and servicetypeid = 7;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o n�o acreditada Externa' WHERE locale='pt_PT' and servicetypeid = 8;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o gerida pelo cliente' WHERE locale='pt_PT' and servicetypeid = 29;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Calibra��o externa gerida pelo cliente' WHERE locale='pt_PT' and servicetypeid = 30;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Repara��o' WHERE locale='pt_PT' and servicetypeid = 6;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Outros' WHERE locale='pt_PT' and servicetypeid = 3;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Servi�o' WHERE locale='pt_PT' and servicetypeid = 31;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Verifica��o do correto funcionamento em laborat�rio' WHERE locale='pt_PT' and servicetypeid = 32;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Verifica��o do correto funcionamento in situ' WHERE locale='pt_PT' and servicetypeid = 36;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Qualifica��o' WHERE locale='pt_PT' and servicetypeid = 33;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Valida��o' WHERE locale='pt_PT' and servicetypeid = 34;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Tarefa administrativa' WHERE locale='pt_PT' and servicetypeid = 37;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Interc�mbio' WHERE locale='pt_PT' and servicetypeid = 38;
UPDATE [dbo].[servicetypelongnametranslation] SET [translation] = 'Transfer�ncia log�stica' WHERE locale='pt_PT' and servicetypeid = 39;


ROLLBACK TRAN