-- Updates the global exchange format (mobile application) in the production server
-- Saving as script 
-- Laila Madani - 2020-01-21

USE [atlas]
GO
BEGIN TRAN

DECLARE @exchangeformatId INT

SET @exchangeformatId=(SELECT id FROM dbo.exchangeformat WHERE exchangeFormatLevel LIKE 'GLOBAL' AND eftype LIKE 'PREBOOKING_FROM_MOBILEAPP')

DELETE FROM dbo.exchangeformatfieldnamedetails WHERE exchangeFormatid = @exchangeformatId

UPDATE dbo.exchangeformat SET name = 'ExchangeFormatExternalSystem' WHERE id = @exchangeformatId
UPDATE dbo.exchangeformat SET lastModifiedBy = 17280 WHERE id = @exchangeformatId

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (1, 1, 'idTrescal', 'ID_TRESCAL', @exchangeformatId)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (2, 0, 'domain', 'UNDEFINED', @exchangeformatId)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (3, 0, 'customerDescription', 'CUSTOMER_DESCRIPTION', @exchangeformatId)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (4, 0, 'plantNo', 'PLANT_NO', @exchangeformatId)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (5, 0, 'serialNo', 'SERIAL_NUMBER', @exchangeformatId)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (6, 0, 'clientref', 'CLIENT_REF', @exchangeformatId)

INSERT INTO dbo.exchangeformatfieldnamedetails(position, mandatory, templateName, fieldName, exchangeFormatid)
VALUES (7, 0, 'accessories', 'UNDEFINED', @exchangeformatId)

COMMIT TRAN