-- Utility script to reset CDC related functionality to an empty / clear state
-- Useful more for testing CDC processing locally or to clean an installation on a test machine
-- Normally the two CDC related jobs should be disabled while running this script
-- Afterwards all CDC events which are present would be available for processing; this script does not clear the CDC queues
-- Author : Galen Beck - 2021-03-16


USE [atlas]
GO

BEGIN TRAN

PRINT 'Clear LSNs in [processed_changeentity]';
UPDATE [dbo].[processed_changeentity] SET [lastprocessedlsn] = null WHERE [lastprocessedlsn] IS NOT NULL;
GO

PRINT 'Clear [subscriber_queue]';
DELETE FROM [dbo].[subscriber_queue];
GO

PRINT 'Clear [change_queue]';
DELETE FROM [dbo].[change_queue];
GO

COMMIT TRAN