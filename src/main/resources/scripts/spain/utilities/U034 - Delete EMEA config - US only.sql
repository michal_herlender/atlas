-- Cleanup script for US database to remove some more EMEA business data
-- Note, you must enter / confirm / update the database name below and note ROLLBACK at end to test

-- Removes the following except for the US Trescal and ISS business companies:
-- 1) Department Members
-- 2) Categorized Procedures
-- 3) Procedure Category
-- 4) Department
-- 5) Capability-ServiceType filters
-- 6) Unused CapabilityFilter table
-- 7) Capabilities
-- 8) Catalog Price

USE [atlas-us-2021-07-01]

BEGIN TRAN

-- Save information belonging to Trescal Inc and ISS business companies only
DECLARE @coid_TUS int;
DECLARE @coid_ISS int;
DECLARE @remainingCount int;

SELECT @coid_TUS = coid from company where coname='TRESCAL INC' and corole=5;
SELECT @coid_ISS = coid from company where coname='INTEGRATED SERVICE SOLUTIONS INC' and corole=5;

PRINT @coid_TUS;
PRINT @coid_ISS;

PRINT 'Deleting department members for non-US companies'

DELETE [departmentmember] FROM [dbo].[departmentmember] dm
	INNER JOIN department d on d.deptid = dm.deptid
	INNER JOIN address a on a.addrid = d.addrid
	INNER JOIN subdiv sub on sub.subdivid = a.subdivid
	WHERE sub.coid not in (@coid_ISS, @coid_TUS)

SELECT @remainingCount = COUNT(1) from departmentmember;
PRINT @remainingCount;

PRINT 'Updating purchase order status'

-- Note, we could drop this column (in separate cleanup script for all DBs)
UPDATE [purchaseorderstatus] SET deptid = null WHERE deptid is not null;

PRINT 'Deleting categorised procedures for non-US companies'

DELETE [categorisedprocedure] FROM [categorisedprocedure] cp
	INNER JOIN procedurecategory pc on pc.id = cp.categoryid
	INNER JOIN department d on d.deptid = pc.departmentid
	INNER JOIN address a on a.addrid = d.addrid
	INNER JOIN subdiv sub on sub.subdivid = a.subdivid
	WHERE sub.coid not in (@coid_ISS, @coid_TUS)

SELECT @remainingCount = COUNT(1) from categorisedprocedure;
PRINT @remainingCount;

PRINT 'Deleting procedure categories for non-US companies'

DELETE [procedurecategory] FROM [procedurecategory] pc 
	INNER JOIN department d on d.deptid = pc.departmentid
	INNER JOIN address a on a.addrid = d.addrid
	INNER JOIN subdiv sub on sub.subdivid = a.subdivid
	WHERE sub.coid not in (@coid_ISS, @coid_TUS)

SELECT @remainingCount = COUNT(1) from procedurecategory;
PRINT @remainingCount;

PRINT 'Deleting departments for non-US companies'

DELETE [department] FROM [dbo].[department] d
	INNER JOIN address a on a.addrid = d.addrid
	INNER JOIN subdiv sub on sub.subdivid = a.subdivid
	WHERE sub.coid not in (@coid_ISS, @coid_TUS)

SELECT @remainingCount = COUNT(1) from department;
PRINT @remainingCount;

PRINT 'Deleting capability_servicetype for non-US companies'

DELETE [capability_servicetype] FROM [capability_servicetype] cst
	INNER JOIN [procs] procs on procs.id = cst.procedureid
	INNER JOIN subdiv sub on sub.subdivid = procs.orgid
	WHERE sub.coid not in (@coid_ISS, @coid_TUS)

SELECT @remainingCount = COUNT(1) from capability_servicetype;
PRINT @remainingCount;

PRINT 'Deleting capability_filter for non-US companies'

-- Note, we could delete / drop capability filter table
-- (unused - check if used for loading)
DELETE [capabilityfilter] FROM [capabilityfilter] cf
	INNER JOIN [procs] procs on procs.id = cf.procedureid
	INNER JOIN subdiv sub on sub.subdivid = procs.orgid
	WHERE sub.coid not in (@coid_ISS, @coid_TUS)

SELECT @remainingCount = COUNT(1) from capabilityfilter;
PRINT @remainingCount;

PRINT 'Deleting capabilities for non-US companies'

DELETE [procs] FROM [dbo].[procs] procs
	INNER JOIN subdiv sub on sub.subdivid = procs.orgid
	WHERE sub.coid not in (@coid_ISS, @coid_TUS)

SELECT @remainingCount = COUNT(1) from procs;
PRINT @remainingCount;

PRINT 'Deleting catalog prices for non-US companies'

DELETE [catalogprice] FROM [dbo].[catalogprice] cp
	WHERE cp.orgid not in (@coid_ISS, @coid_TUS)

SELECT @remainingCount = COUNT(1) from catalogprice;
PRINT @remainingCount;

ROLLBACK TRAN