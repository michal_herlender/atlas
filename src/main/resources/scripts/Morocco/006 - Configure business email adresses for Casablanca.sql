-- Script to insert/update email adresses for the subdivision Casablanca for Morocco
-- Author: Tony Provost 2016-10-21

BEGIN TRAN

USE [spain];
GO

DECLARE @BusinessSubdivID INT = 6951

IF NOT EXISTS (SELECT * FROM [dbo].[businessemails] WHERE [orgid] = @BusinessSubdivID)
	INSERT INTO [dbo].[businessemails] ([lastModified], [account], [autoService], [collections], [goodsIn], [quality], [quotations], [recall], [sales], [web], [orgid])
	VALUES (GETDATE(), 'compta.ma030-cas@trescal.com', 'auto-service.ma030-cas@trescal.com', 'transports.ma030-cas@trescal.com', 'logistique.ma030-cas@trescal.com',
		'qse.ma030-cas@trescal.com', 'devis.ma030-cas@trescal.com', 'rappel.ma030-cas@trescal.com', 'crc.ma030-cas@trescal.com', 'web.ma030-cas@trescal.com', @BusinessSubdivID) 
ELSE
	UPDATE [dbo].[businessemails]
	SET [lastModified] = GETDATE(), [account] = 'compta.ma030-cas@trescal.com', [autoService] = 'auto-service.ma030-cas@trescal.com', [collections] = 'transports.ma030-cas@trescal.com',
		[goodsIn] = 'logistique.ma030-cas@trescal.com', [quality] = 'qse.ma030-cas@trescal.com', [quotations] = 'devis.ma030-cas@trescal.com', 
		[recall] = 'rappel.ma030-cas@trescal.com', [sales] = 'crc.ma030-cas@trescal.com', [web] = 'web.ma030-cas@trescal.com'
	WHERE [orgid] = @BusinessSubdivID

COMMIT TRAN