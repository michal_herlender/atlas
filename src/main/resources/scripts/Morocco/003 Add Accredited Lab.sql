BEGIN TRAN
USE [spain];
GO
INSERT INTO [accreditationbody] ( [lastModified] ,[shortName] , [countryid])
VALUES ('2016-04-29' , 'SNIMA' , 117 )
GO


INSERT INTO [accreditedlab] ([description],[labno] ,[watermark], [lastModified],[orgid] ,[accreditationbodyid])
  SELECT 'Casablanca - Calibration', 'lab no',0, '2016-04-29',6951, [id] FROM accreditationbody WHERE [shortName] = 'SNIMA'
GO

ROLLBACK TRAN
GO
