-- Script to insert/update default general condition for Morocco
-- Author: Tony Provost 2016-10-31

BEGIN TRAN

USE [spain];
GO

DECLARE @BusinessCompanyID INT = 6685

DECLARE @ConditionText VARCHAR(MAX) = '<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>

<body style="margin: 0;">

<!-- Page 1 -->
<div id="p1" style="overflow: hidden; position: relative; width: 935px; height: 1210px;">

<!-- Begin shared CSS values -->
<style type="text/css" >
.t {
	-webkit-transform-origin: top left;
	-moz-transform-origin: top left;
	-o-transform-origin: top left;
	-ms-transform-origin: top left;
	-webkit-transform: scale(0.25);
	-moz-transform: scale(0.25);
	-o-transform: scale(0.25);
	-ms-transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:331px;top:50px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2_1{left:751px;top:54px;letter-spacing:-0.1px;}
#t3_1{left:865px;top:54px;}
#t4_1{left:59px;top:1153px;}
#t5_1{left:227px;top:1153px;}
#t6_1{left:850px;top:1153px;word-spacing:0.1px;}
#t7_1{left:57px;top:90px;letter-spacing:-0.1px;word-spacing:0.3px;}
#t8_1{left:57px;top:104px;letter-spacing:-0.1px;word-spacing:19px;}
#t9_1{left:57px;top:118px;word-spacing:18.8px;}
#ta_1{left:57px;top:132px;}
#tb_1{left:57px;top:147px;word-spacing:7.2px;}
#tc_1{left:57px;top:160px;word-spacing:10.4px;}
#td_1{left:57px;top:175px;word-spacing:3.8px;}
#te_1{left:57px;top:189px;}
#tf_1{left:57px;top:203px;letter-spacing:-0.1px;word-spacing:2.6px;}
#tg_1{left:57px;top:217px;word-spacing:1.6px;}
#th_1{left:57px;top:230px;word-spacing:8.1px;}
#ti_1{left:57px;top:245px;word-spacing:0.4px;}
#tj_1{left:57px;top:259px;word-spacing:5.9px;}
#tk_1{left:57px;top:273px;word-spacing:30.5px;}
#tl_1{left:162px;top:273px;word-spacing:30.6px;}
#tm_1{left:57px;top:287px;word-spacing:4.5px;}
#tn_1{left:57px;top:301px;word-spacing:0.4px;}
#to_1{left:57px;top:320px;letter-spacing:-0.1px;word-spacing:0.5px;}
#tp_1{left:57px;top:334px;word-spacing:1px;}
#tq_1{left:57px;top:348px;letter-spacing:-0.1px;word-spacing:0.5px;}
#tr_1{left:57px;top:362px;word-spacing:1.2px;}
#ts_1{left:57px;top:376px;word-spacing:0.2px;}
#tt_1{left:57px;top:395px;word-spacing:0.2px;}
#tu_1{left:57px;top:409px;letter-spacing:-0.1px;word-spacing:0.4px;}
#tv_1{left:57px;top:423px;word-spacing:12.6px;}
#tw_1{left:57px;top:437px;word-spacing:0.4px;}
#tx_1{left:57px;top:451px;letter-spacing:-0.1px;word-spacing:0.3px;}
#ty_1{left:57px;top:465px;word-spacing:5.8px;}
#tz_1{left:57px;top:479px;letter-spacing:-0.1px;word-spacing:0.4px;}
#t10_1{left:57px;top:493px;letter-spacing:-0.1px;word-spacing:22.6px;}
#t11_1{left:57px;top:507px;word-spacing:0.1px;}
#t12_1{left:57px;top:521px;word-spacing:3.1px;}
#t13_1{left:57px;top:535px;word-spacing:6.4px;}
#t14_1{left:57px;top:549px;word-spacing:14.8px;}
#t15_1{left:57px;top:563px;word-spacing:10.5px;}
#t16_1{left:57px;top:577px;word-spacing:6.7px;}
#t17_1{left:57px;top:591px;word-spacing:0.3px;}
#t18_1{left:57px;top:606px;letter-spacing:-0.1px;word-spacing:0.5px;}
#t19_1{left:57px;top:619px;word-spacing:19.6px;}
#t1a_1{left:57px;top:634px;}
#t1b_1{left:57px;top:648px;letter-spacing:0.1px;word-spacing:6.3px;}
#t1c_1{left:57px;top:662px;word-spacing:9.9px;}
#t1d_1{left:57px;top:676px;letter-spacing:-0.1px;}
#t1e_1{left:57px;top:690px;word-spacing:0.9px;}
#t1f_1{left:57px;top:704px;word-spacing:0.1px;}
#t1g_1{left:57px;top:718px;word-spacing:0.3px;}
#t1h_1{left:57px;top:732px;word-spacing:23.8px;}
#t1i_1{left:57px;top:746px;word-spacing:21px;}
#t1j_1{left:57px;top:760px;}
#t1k_1{left:57px;top:774px;word-spacing:1.8px;}
#t1l_1{left:57px;top:788px;word-spacing:4.4px;}
#t1m_1{left:57px;top:802px;letter-spacing:-0.1px;word-spacing:0.6px;}
#t1n_1{left:57px;top:816px;word-spacing:7.2px;}
#t1o_1{left:57px;top:830px;word-spacing:5.6px;}
#t1p_1{left:57px;top:844px;word-spacing:0.5px;}
#t1q_1{left:57px;top:858px;word-spacing:10.4px;}
#t1r_1{left:57px;top:872px;word-spacing:11.4px;}
#t1s_1{left:57px;top:886px;letter-spacing:-0.1px;word-spacing:0.5px;}
#t1t_1{left:57px;top:900px;word-spacing:11.3px;}
#t1u_1{left:57px;top:914px;word-spacing:0.2px;}
#t1v_1{left:57px;top:928px;word-spacing:4.9px;}
#t1w_1{left:57px;top:943px;word-spacing:0.4px;}
#t1x_1{left:57px;top:956px;word-spacing:6.4px;}
#t1y_1{left:57px;top:970px;word-spacing:0.3px;}
#t1z_1{left:57px;top:985px;word-spacing:0.2px;}
#t20_1{left:57px;top:1003px;word-spacing:0.2px;}
#t21_1{left:57px;top:1018px;word-spacing:0.5px;}
#t22_1{left:57px;top:1032px;word-spacing:11.1px;}
#t23_1{left:57px;top:1046px;word-spacing:5.6px;}
#t24_1{left:57px;top:1060px;word-spacing:26.5px;}
#t25_1{left:57px;top:1074px;word-spacing:12.5px;}
#t26_1{left:57px;top:1088px;}
#t27_1{left:57px;top:1102px;word-spacing:16.2px;}
#t28_1{left:57px;top:1116px;word-spacing:-0.4px;}
#t29_1{left:493px;top:90px;word-spacing:8.6px;}
#t2a_1{left:493px;top:104px;word-spacing:0.2px;}
#t2b_1{left:644px;top:104px;}
#t2c_1{left:650px;top:104px;letter-spacing:-0.8px;}
#t2d_1{left:660px;top:104px;}
#t2e_1{left:688px;top:104px;letter-spacing:0.4px;}
#t2f_1{left:493px;top:118px;word-spacing:5.2px;}
#t2g_1{left:493px;top:132px;word-spacing:0.3px;}
#t2h_1{left:493px;top:146px;word-spacing:0.4px;}
#t2i_1{left:493px;top:160px;word-spacing:9.9px;}
#t2j_1{left:493px;top:174px;word-spacing:0.1px;}
#t2k_1{left:493px;top:188px;word-spacing:3.1px;}
#t2l_1{left:493px;top:202px;word-spacing:-0.1px;}
#t2m_1{left:583px;top:202px;letter-spacing:-0.1px;word-spacing:0.2px;}
#t2n_1{left:493px;top:216px;word-spacing:0.2px;}
#t2o_1{left:493px;top:230px;word-spacing:0.3px;}
#t2p_1{left:493px;top:245px;word-spacing:0.1px;}
#t2q_1{left:493px;top:258px;word-spacing:13.1px;}
#t2r_1{left:493px;top:273px;word-spacing:11.3px;}
#t2s_1{left:493px;top:287px;letter-spacing:-0.1px;word-spacing:4.9px;}
#t2t_1{left:493px;top:301px;}
#t2u_1{left:825px;top:301px;}
#t2v_1{left:835px;top:301px;}
#t2w_1{left:862px;top:301px;letter-spacing:0.4px;}
#t2x_1{left:493px;top:315px;word-spacing:0.3px;}
#t2y_1{left:493px;top:329px;}
#t2z_1{left:493px;top:343px;word-spacing:2.4px;}
#t30_1{left:493px;top:357px;word-spacing:0.4px;}
#t31_1{left:493px;top:371px;word-spacing:3.8px;}
#t32_1{left:493px;top:385px;}
#t33_1{left:493px;top:399px;word-spacing:8.6px;}
#t34_1{left:493px;top:413px;word-spacing:0.4px;}
#t35_1{left:493px;top:427px;word-spacing:4.9px;}
#t36_1{left:493px;top:441px;letter-spacing:-0.1px;word-spacing:0.4px;}
#t37_1{left:493px;top:455px;word-spacing:10.6px;}
#t38_1{left:493px;top:469px;word-spacing:14.9px;}
#t39_1{left:493px;top:483px;letter-spacing:-0.1px;word-spacing:0.4px;}
#t3a_1{left:493px;top:497px;word-spacing:4.3px;}
#t3b_1{left:493px;top:511px;word-spacing:6.2px;}
#t3c_1{left:493px;top:525px;}
#t3d_1{left:493px;top:544px;letter-spacing:-0.1px;word-spacing:0.5px;}
#t3e_1{left:493px;top:558px;word-spacing:2.9px;}
#t3f_1{left:493px;top:572px;word-spacing:0.1px;}
#t3g_1{left:493px;top:587px;letter-spacing:-0.1px;word-spacing:0.3px;}
#t3h_1{left:493px;top:600px;letter-spacing:-0.1px;word-spacing:4.3px;}
#t3i_1{left:493px;top:614px;word-spacing:0.3px;}
#t3j_1{left:493px;top:629px;word-spacing:11.5px;}
#t3k_1{left:493px;top:642px;word-spacing:6px;}
#t3l_1{left:493px;top:657px;word-spacing:3px;}
#t3m_1{left:493px;top:671px;letter-spacing:0.1px;word-spacing:5.6px;}
#t3n_1{left:493px;top:685px;word-spacing:25.8px;}
#t3o_1{left:493px;top:699px;}
#t3p_1{left:493px;top:713px;word-spacing:1.6px;}
#t3q_1{left:493px;top:727px;word-spacing:0.9px;}
#t3r_1{left:493px;top:741px;word-spacing:11.5px;}
#t3s_1{left:493px;top:755px;word-spacing:7.1px;}
#t3t_1{left:493px;top:769px;word-spacing:0.1px;}
#t3u_1{left:493px;top:783px;word-spacing:11.5px;}
#t3v_1{left:493px;top:797px;word-spacing:0.9px;}
#t3w_1{left:493px;top:811px;word-spacing:5.1px;}
#t3x_1{left:493px;top:825px;word-spacing:1.6px;}
#t3y_1{left:493px;top:839px;}
#t3z_1{left:541px;top:839px;letter-spacing:0.3px;}
#t40_1{left:572px;top:839px;}
#t41_1{left:653px;top:839px;}
#t42_1{left:714px;top:839px;letter-spacing:0.3px;}
#t43_1{left:739px;top:839px;}
#t44_1{left:820px;top:839px;letter-spacing:0.2px;}
#t45_1{left:850px;top:839px;}
#t46_1{left:493px;top:853px;word-spacing:17.9px;}
#t47_1{left:493px;top:867px;word-spacing:7.8px;}
#t48_1{left:493px;top:881px;word-spacing:11.2px;}
#t49_1{left:493px;top:895px;}
#t4a_1{left:493px;top:914px;word-spacing:0.5px;}
#t4b_1{left:493px;top:928px;word-spacing:29.1px;}
#t4c_1{left:493px;top:942px;word-spacing:18.8px;}
#t4d_1{left:493px;top:956px;word-spacing:4.2px;}
#t4e_1{left:493px;top:970px;word-spacing:9.9px;}
#t4f_1{left:493px;top:984px;word-spacing:10.8px;}
#t4g_1{left:493px;top:998px;word-spacing:3.7px;}
#t4h_1{left:493px;top:1013px;word-spacing:-0.4px;}
#t4i_1{left:493px;top:1026px;word-spacing:10px;}
#t4j_1{left:493px;top:1041px;letter-spacing:-0.1px;word-spacing:16.1px;}
#t4k_1{left:493px;top:1055px;word-spacing:0.2px;}
#t4l_1{left:493px;top:1069px;letter-spacing:-0.1px;word-spacing:6.6px;}
#t4m_1{left:493px;top:1083px;letter-spacing:-0.1px;word-spacing:10.6px;}
#t4n_1{left:493px;top:1097px;word-spacing:0.4px;}
#t4o_1{left:493px;top:1111px;word-spacing:29.2px;}
#t4p_1{left:493px;top:1125px;word-spacing:6px;}

.s1_1{
	FONT-SIZE: 57.2px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s3_1{
	FONT-SIZE: 43.4px;
	FONT-FAMILY: Arial, Helvetica, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s7_1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
}

.s8_1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Arial, Helvetica, sans-serif;
	color: rgb(0,0,0);
}

.s4_1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_i;
	color: rgb(0,0,0);
}

.s10_1{
	FONT-SIZE: 49px;
	FONT-FAMILY: Arial, Helvetica, sans-serif;
	color: rgb(0,0,0);
}

.s11_1{
	FONT-SIZE: 49px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s6_1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s2_1{
	FONT-SIZE: 43.4px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s5_1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_j;
	color: rgb(0,0,0);
}

.s9_1{
	FONT-SIZE: 49px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri_j;
	src: url("fonts/Calibri_j.woff") format("woff");
}

@font-face {
	font-family: Calibri_i;
	src: url("fonts/Calibri_i.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1_1">CONDITIONS GENERALES DE VENTE</div>
<div id="t2_1" class="t s2_1">S06/P01/1,F12-1/B-201</div>
<div id="t3_1" class="t s3_1">6</div>
<div id="t4_1" class="t s4_1">Trescal Maroc - Zone Indusparc - </div>
<div id="t5_1" class="t s4_1">Sidi Moumen - Chemin Tertiaire 1015 - 20400 Casablanca - Maroc</div>
<div id="t6_1" class="t s5_1">1 / 2</div>
<div id="t7_1" class="t s6_1">1 - OBJET ET OPPOSABILITE DES CONDITIONS GENERALES</div>
<div id="t8_1" class="t s7_1">Les pr�sentes conditions g�n�rales ont pour objet de d�finir les </div>
<div id="t9_1" class="t s7_1">conditions applicables aux prestations de services et aux ventes </div>
<div id="ta_1" class="t s7_1">d��quipement, effectu�es en France, par la soci�t� Trescal. </div>
<div id="tb_1" class="t s7_1">Le contrat est constitu� des Conditions G�n�rales et des Conditions </div>
<div id="tc_1" class="t s7_1">Particuli�res (ci-apr�s d�nomm�es la � Proposition Commerciale �) </div>
<div id="td_1" class="t s7_1">qui repr�sentent l�int�gralit� de la convention, et se substituent � tous </div>
<div id="te_1" class="t s7_1">les autres accords oraux ou �crits. </div>
<div id="tf_1" class="t s7_1">Le fait pour un client de passer commande implique l�adh�sion enti�re </div>
<div id="tg_1" class="t s7_1">et sans r�serve aux pr�sentes conditions g�n�rales et � la Proposition </div>
<div id="th_1" class="t s7_1">Commerciale. Le client renonce express�ment � l�application de ses </div>
<div id="ti_1" class="t s7_1">conditions g�n�rales d�achat. </div>
<div id="tj_1" class="t s7_1">Le fait que Trescal ne se pr�vale pas, � un moment donn�, de l�une </div>
<div id="tk_1" class="t s7_1">quelconque des </div>
<div id="tl_1" class="t s7_1">pr�sentes conditions g�n�rales, ne peut �tre </div>
<div id="tm_1" class="t s7_1">interpr�t� comme valant renonciation � se pr�valoir ult�rieurement de </div>
<div id="tn_1" class="t s7_1">l�une quelconque desdites conditions. </div>
<div id="to_1" class="t s6_1">2 - FORMATION DU CONTRAT</div>
<div id="tp_1" class="t s7_1">Les commandes ne sont d�finitives que lorsqu�elles ont �t� confirm�es </div>
<div id="tq_1" class="t s7_1">par �crit par Trescal. </div>
<div id="tr_1" class="t s7_1">Toute modification aux pr�sentes n�aura d�effet que dans la mesure o� </div>
<div id="ts_1" class="t s7_1">Trescal l�aura pr�alablement accept�e par �crit. </div>
<div id="tt_1" class="t s6_1">3- CONDITIONS GENERALES D�EXECUTION </div>
<div id="tu_1" class="t s7_1">3.1 Trescal effectue les prestations : </div>
<div id="tv_1" class="t s7_1">- par r�f�rence aux sp�cifications qu�il a d�finies et dont le Client </div>
<div id="tw_1" class="t s7_1">d�clare avoir pris connaissance </div>
<div id="tx_1" class="t s7_1">- et en vertu des besoins particuliers d�finis par le Client. </div>
<div id="ty_1" class="t s7_1">En aucun cas, Trescal ne peut �tre tenu responsable de la d�finition </div>
<div id="tz_1" class="t s7_1">des tol�rances. </div>
<div id="t10_1" class="t s7_1">La fourniture ne comprend, exactement et uniquement, que les </div>
<div id="t11_1" class="t s7_1">prestations sp�cifi�es sur la Proposition Commerciale. </div>
<div id="t12_1" class="t s7_1">3.2 Les d�lais de r�alisation ou de livraison figurant sur la Proposition </div>
<div id="t13_1" class="t s7_1">Commerciale sont donn�s � titre purement indicatif et sans garantie. </div>
<div id="t14_1" class="t s7_1">De convention expresse, leur inobservation, qui ne donne pas au </div>
<div id="t15_1" class="t s7_1">Client le droit d�annuler la commande ou de refuser la vente ou la </div>
<div id="t16_1" class="t s7_1">prestation, ne peut donner lieu � retenue, compensation, p�nalit� ou </div>
<div id="t17_1" class="t s7_1">dommages et int�r�ts. </div>
<div id="t18_1" class="t s7_1">3.3 Le Client s�engage � : </div>
<div id="t19_1" class="t s7_1">- fournir � Trescal toute information et d�tail utiles relatifs � la </div>
<div id="t1a_1" class="t s7_1">destination de la prestation et � le tenir inform� de tout changement. </div>
<div id="t1b_1" class="t s7_1">- mettre � la disposition de Trescal les moyens d�acc�s sur les lieux </div>
<div id="t1c_1" class="t s7_1">d�ex�cution de la prestation, les �quipements et leur documentation </div>
<div id="t1d_1" class="t s7_1">technique </div>
<div id="t1e_1" class="t s7_1">- prendre toutes les dispositions de s�curit� relatives aux conditions de </div>
<div id="t1f_1" class="t s7_1">travail, site et �quipement </div>
<div id="t1g_1" class="t s7_1">- obtenir toutes les autorisations administratives n�cessaires. </div>
<div id="t1h_1" class="t s7_1">3.4 Toute r�clamation portant sur les produits vendus, sur les </div>
<div id="t1i_1" class="t s7_1">prestations r�alis�es sur le mat�riel doit �tre motiv�e par lettre </div>
<div id="t1j_1" class="t s7_1">recommand�e avec AR dans les trois jours suivant la date de livraison. </div>
<div id="t1k_1" class="t s7_1">Le Client devra fournir par lettre recommand�e avec AR tout justificatif </div>
<div id="t1l_1" class="t s7_1">quant � la r�alit� des d�fauts constat�s dans les cinq jours suivant la </div>
<div id="t1m_1" class="t s7_1">date de r�ception par Trescal de la r�clamation motiv�e. </div>
<div id="t1n_1" class="t s7_1">A compter de la date de r�ception des pr�sents documents, Trescal </div>
<div id="t1o_1" class="t s7_1">disposera alors, d�un d�lai de dix jours pour faire savoir au Client s�il </div>
<div id="t1p_1" class="t s7_1">exige une expertise contradictoire. </div>
<div id="t1q_1" class="t s7_1">Si les conclusions du laboratoire tiers confirment celles de Trescal, </div>
<div id="t1r_1" class="t s7_1">tous les frais occasionn�s du fait de cette contestation seront � la </div>
<div id="t1s_1" class="t s7_1">charge du Client. </div>
<div id="t1t_1" class="t s7_1">Aucune r�clamation ne sera recevable si les produits ont subi des </div>
<div id="t1u_1" class="t s7_1">modifications ou des d�t�riorations du fait du Client. </div>
<div id="t1v_1" class="t s7_1">Ne sont accept�s que les retours d�ment motiv�s, avec l�autorisation </div>
<div id="t1w_1" class="t s7_1">pr�alable et �crite de Trescal. </div>
<div id="t1x_1" class="t s7_1">En aucun cas, le Client ne pourra arguer d�un tel retour pour cesser </div>
<div id="t1y_1" class="t s7_1">tout paiement, ni pour annuler, en totalit� ou en partie, une quelconque </div>
<div id="t1z_1" class="t s7_1">commande en cours d�ex�cution. </div>
<div id="t20_1" class="t s6_1">4 - PRIX ET CONDITIONS DE PAIEMENT</div>
<div id="t21_1" class="t s7_1">4.1 Prix</div>
<div id="t22_1" class="t s7_1">Les prestations et les ventes de mat�riel sont r�alis�es au prix en </div>
<div id="t23_1" class="t s7_1">vigueur au jour de la passation de commande et dont le montant est </div>
<div id="t24_1" class="t s7_1">pr�cis� dans la Proposition Commerciale. Les prestations sont </div>
<div id="t25_1" class="t s7_1">factur�es suivant une p�riodicit� mensuelle au prorata des travaux </div>
<div id="t26_1" class="t s7_1">r�alis�s. </div>
<div id="t27_1" class="t s7_1">Les prix �tablis hors toutes taxes et hors frais de transport sont </div>
<div id="t28_1" class="t s7_1">modifiables sans pr�avis. </div>
<div id="t29_1" class="t s7_1">Toute commande hors transport pass�e aupr�s de Trescal doit �tre </div>
<div id="t2a_1" class="t s7_1">d�un montant minimum de 8</div>
<div id="t2b_1" class="t s8_1">0</div>
<div id="t2c_1" class="t s7_1">0 </div>
<div id="t2d_1" class="t s8_1">MAD</div>
<div id="t2e_1" class="t s7_1">. </div>
<div id="t2f_1" class="t s7_1">Dans tous les cas, les frais de diagnostic et/ou de v�rification restent </div>
<div id="t2g_1" class="t s7_1">acquis � Trescal. </div>
<div id="t2h_1" class="t s7_1">4.2 Conditions de paiement</div>
<div id="t2i_1" class="t s9_1">Les conditions de paiement sont soumises � l�accord pr�alable des </div>
<div id="t2j_1" class="t s9_1">services financiers de Trescal. </div>
<div id="t2k_1" class="t s9_1">Les factures sont payables � 30 jours date de facture, net d�escompte </div>
<div id="t2l_1" class="t s9_1">et major�es des</div>
<div id="t2m_1" class="t s9_1">taxes en vigueur au jour du fait g�n�rateur. </div>
<div id="t2n_1" class="t s9_1">4.3 Retard de paiement</div>
<div id="t2o_1" class="t s9_1">a)Toute somme non pay�e � l��ch�ance figurant sur la facture entra�ne </div>
<div id="t2p_1" class="t s9_1">de plein droit : </div>
<div id="t2q_1" class="t s9_1">(i) Sans qu�un rappel soit n�cessaire, l�application de p�nalit�s de </div>
<div id="t2r_1" class="t s9_1">retard exigibles le jour suivant la date de r�glement figurant sur la </div>
<div id="t2s_1" class="t s9_1">facture et au taux de r�f�rence : BCE + 10 points et le versement de </div>
<div id="t2t_1" class="t s9_1">l�indemnit� l�gale forfaitaire pour frais de recouvrement de 40</div>
<div id="t2u_1" class="t s10_1">0</div>
<div id="t2v_1" class="t s10_1">MAD</div>
<div id="t2w_1" class="t s9_1">. </div>
<div id="t2x_1" class="t s9_1">(ii) Apr�s envoi d�une mise en demeure rest�e infructueuse : </div>
<div id="t2y_1" class="t s9_1">- l�exigibilit� imm�diate de toutes sommes restant dues. </div>
<div id="t2z_1" class="t s9_1">- la suspension de toutes les commandes en cours, sans pr�judice de </div>
<div id="t30_1" class="t s9_1">toute autre voie d�action. </div>
<div id="t31_1" class="t s9_1">- la r�solution de la proposition commerciale si bon semble � Trescal, </div>
<div id="t32_1" class="t s9_1">qui pourra : </div>
<div id="t33_1" class="t s9_1">. demander, en r�f�r�, la restitution des produits, sans pr�judice de </div>
<div id="t34_1" class="t s9_1">tous autres dommages et int�r�ts. </div>
<div id="t35_1" class="t s9_1">. conserver et retenir, jusqu�� complet paiement du prix, tout mat�riel </div>
<div id="t36_1" class="t s9_1">que le Client lui aura confi� et tout livrable. </div>
<div id="t37_1" class="t s9_1">La r�solution frappera non seulement la commande en cause mais </div>
<div id="t38_1" class="t s9_1">aussi toutes les commandes impay�es ant�rieures, qu�elles soient </div>
<div id="t39_1" class="t s9_1">livr�es ou en cours de livraison et que leur paiement soit �chu ou non. </div>
<div id="t3a_1" class="t s9_1">b) En cas de paiement par effet de commerce, le d�faut de retour de </div>
<div id="t3b_1" class="t s9_1">l�effet sera consid�r� comme un refus d�acceptation assimilable � un </div>
<div id="t3c_1" class="t s9_1">d�faut de paiement. </div>
<div id="t3d_1" class="t s11_1">5 - TRANSPORT </div>
<div id="t3e_1" class="t s9_1">5.1 S�agissant de mat�riel confi� � Trescal, les modalit�s du transport </div>
<div id="t3f_1" class="t s9_1">sont r�gies dans la Proposition Commerciale. </div>
<div id="t3g_1" class="t s9_1">Les frais de transport restent toujours � la charge du Client. </div>
<div id="t3h_1" class="t s9_1">En cas d�avarie ou de manquants, le Client doit �mettre des r�serves </div>
<div id="t3i_1" class="t s9_1">sur le bon de livraison et dispose d�un d�lai de 3 jours, non compris les </div>
<div id="t3j_1" class="t s9_1">jours f�ri�s, � compter de la livraison du mat�riel pour notifier ses </div>
<div id="t3k_1" class="t s9_1">protestations motiv�es au transporteur, par lettre recommand�e avec </div>
<div id="t3l_1" class="t s9_1">accus� de r�ception, avec copie adress�e � Trescal. Au-del� du d�lai </div>
<div id="t3m_1" class="t s9_1">de 3 jours, non compris les jours f�ri�s, � compter de la livraison du </div>
<div id="t3n_1" class="t s9_1">mat�riel, les r�clamations �mises par le Client ne seront pas </div>
<div id="t3o_1" class="t s9_1">recevables. </div>
<div id="t3p_1" class="t s9_1">Les frais �ventuels d�entreposage entra�n�s par un d�faut du Client de </div>
<div id="t3q_1" class="t s9_1">reprendre l��quipement sont � sa charge. Tout �quipement d�pos� par </div>
<div id="t3r_1" class="t s9_1">le Client et non repris dans le d�lai d�un an est consid�r� comme </div>
<div id="t3s_1" class="t s9_1">abandonn� et Trescal peut en disposer sans aucune autorisation du </div>
<div id="t3t_1" class="t s9_1">client, ni aucune formalit� vis-�-vis du Client. </div>
<div id="t3u_1" class="t s9_1">5.2 Les �quipements vendus voyagent toujours aux risques et aux </div>
<div id="t3v_1" class="t s9_1">frais du Client. En cas d�avarie ou de manquants, le Client doit �mettre </div>
<div id="t3w_1" class="t s9_1">des r�serves sur le bon de livraison et dispose d�un d�lai de 3 jours, </div>
<div id="t3x_1" class="t s9_1">non compris les jours f�ri�s, � compter de la livraison du mat�riel pour </div>
<div id="t3y_1" class="t s9_1">notifier </div>
<div id="t3z_1" class="t s9_1">ses </div>
<div id="t40_1" class="t s9_1">protestations </div>
<div id="t41_1" class="t s9_1">motiv�es </div>
<div id="t42_1" class="t s9_1">au </div>
<div id="t43_1" class="t s9_1">transporteur, </div>
<div id="t44_1" class="t s9_1">par </div>
<div id="t45_1" class="t s9_1">lettre </div>
<div id="t46_1" class="t s9_1">recommand�e avec accus� de r�ception, avec copie adress�e � </div>
<div id="t47_1" class="t s9_1">Trescal. Au-del� du d�lai de 3 jours, non compris les jours f�ri�s, � </div>
<div id="t48_1" class="t s9_1">compter de la livraison du mat�riel, les r�clamations �mises par le </div>
<div id="t49_1" class="t s9_1">Client ne seront pas recevables </div>
<div id="t4a_1" class="t s11_1">6 � RESPONSABILITE</div>
<div id="t4b_1" class="t s9_1">6-1 Dans aucune circonstance, Trescal ne pourra �tre tenue </div>
<div id="t4c_1" class="t s9_1">responsable de quelque perte, dommage ou r�clamation que ce </div>
<div id="t4d_1" class="t s9_1">soient, li�s � un d�faut apparent et/ou une insuffisance apparente qui </div>
<div id="t4e_1" class="t s9_1">r�sulteraient de ou seraient en lien avec une prestation r�alis�e en </div>
<div id="t4f_1" class="t s9_1">ex�cution de la Proposition Commerciale dans l''hypoth�se o� cette </div>
<div id="t4g_1" class="t s9_1">prestation a �t� accept�e par le Client ou est consid�r�e comme telle </div>
<div id="t4h_1" class="t s9_1">apr�s son ex�cution. </div>
<div id="t4i_1" class="t s9_1">6-2 Toute r�clamation, pour �tre recevable, doit (i) �tre adress�e � </div>
<div id="t4j_1" class="t s9_1">Trescal par le Client et (ii) �tre accompagn�e de l''ensemble des </div>
<div id="t4k_1" class="t s9_1">renseignements et �l�ments de preuves la justifiant. </div>
<div id="t4l_1" class="t s9_1">6-3 Trescal est responsable des dommages directs caus�s au Client </div>
<div id="t4m_1" class="t s9_1">que ce soit par sa n�gligence ou en raison du non-respect de ses </div>
<div id="t4n_1" class="t s9_1">obligations aux termes de la Proposition Commerciale. </div>
<div id="t4o_1" class="t s9_1">6-4 Dans aucune circonstance, Trescal ne pourra �tre tenue </div>
<div id="t4p_1" class="t s9_1">responsable d�un dommage immat�riel, cons�cutif, non cons�cutif ou </div>

<!-- End text definitions -->

<!-- End Page 1 -->
</div>

<!-- Page 2 -->
<div id="p2" style="overflow: hidden; position: relative; width: 935px; height: 1210px;">

<!-- Begin shared CSS values -->
<style type="text/css" >
.t {
	-webkit-transform-origin: top left;
	-moz-transform-origin: top left;
	-o-transform-origin: top left;
	-ms-transform-origin: top left;
	-webkit-transform: scale(0.25);
	-moz-transform: scale(0.25);
	-o-transform: scale(0.25);
	-ms-transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
}
</style>
<!-- End shared CSS values -->

<!-- Begin inline CSS -->
<style type="text/css" >

#t1_2{left:331px;top:50px;letter-spacing:0.2px;word-spacing:-0.1px;}
#t2_2{left:754px;top:54px;letter-spacing:-0.1px;}
#t3_2{left:62px;top:1158px;}
#t4_2{left:229px;top:1158px;}
#t5_2{left:853px;top:1158px;word-spacing:0.1px;}
#t6_2{left:57px;top:80px;word-spacing:13.9px;}
#t7_2{left:57px;top:95px;word-spacing:11.5px;}
#t8_2{left:57px;top:108px;word-spacing:5.1px;}
#t9_2{left:57px;top:123px;}
#ta_2{left:57px;top:136px;word-spacing:1.8px;}
#tb_2{left:57px;top:151px;word-spacing:9.4px;}
#tc_2{left:57px;top:165px;word-spacing:6.5px;}
#td_2{left:57px;top:179px;letter-spacing:-0.1px;word-spacing:0.4px;}
#te_2{left:57px;top:193px;word-spacing:10.5px;}
#tf_2{left:57px;top:207px;word-spacing:6.2px;}
#tg_2{left:57px;top:221px;word-spacing:7px;}
#th_2{left:57px;top:235px;}
#ti_2{left:139px;top:235px;letter-spacing:-0.3px;}
#tj_2{left:170px;top:235px;letter-spacing:-0.3px;}
#tk_2{left:193px;top:235px;letter-spacing:-0.1px;}
#tl_2{left:227px;top:235px;letter-spacing:-0.4px;}
#tm_2{left:254px;top:235px;letter-spacing:0.1px;}
#tn_2{left:345px;top:235px;letter-spacing:-0.4px;}
#to_2{left:372px;top:235px;}
#tp_2{left:57px;top:249px;}
#tq_2{left:57px;top:268px;letter-spacing:-0.1px;word-spacing:0.8px;}
#tr_2{left:57px;top:282px;word-spacing:0.2px;}
#ts_2{left:57px;top:296px;word-spacing:10.1px;}
#tt_2{left:57px;top:310px;word-spacing:9.9px;}
#tu_2{left:57px;top:324px;word-spacing:6.5px;}
#tv_2{left:57px;top:338px;word-spacing:17.4px;}
#tw_2{left:57px;top:352px;letter-spacing:-0.1px;word-spacing:0.3px;}
#tx_2{left:57px;top:366px;word-spacing:8.6px;}
#ty_2{left:57px;top:380px;word-spacing:9px;}
#tz_2{left:57px;top:394px;word-spacing:0.2px;}
#t10_2{left:269px;top:394px;}
#t11_2{left:283px;top:394px;}
#t12_2{left:334px;top:394px;}
#t13_2{left:364px;top:394px;word-spacing:0.4px;}
#t14_2{left:57px;top:408px;word-spacing:0.3px;}
#t15_2{left:57px;top:422px;word-spacing:25.3px;}
#t16_2{left:57px;top:436px;letter-spacing:-0.1px;word-spacing:24.4px;}
#t17_2{left:57px;top:450px;}
#t18_2{left:123px;top:450px;letter-spacing:-0.2px;}
#t19_2{left:145px;top:450px;letter-spacing:0.2px;}
#t1a_2{left:199px;top:450px;letter-spacing:-0.1px;}
#t1b_2{left:263px;top:450px;letter-spacing:0.1px;}
#t1c_2{left:324px;top:450px;letter-spacing:-0.4px;}
#t1d_2{left:350px;top:450px;letter-spacing:0.1px;}
#t1e_2{left:425px;top:450px;letter-spacing:-0.2px;}
#t1f_2{left:57px;top:464px;word-spacing:9.4px;}
#t1g_2{left:57px;top:478px;word-spacing:12.6px;}
#t1h_2{left:57px;top:492px;word-spacing:13.9px;}
#t1i_2{left:57px;top:506px;letter-spacing:-0.1px;word-spacing:13px;}
#t1j_2{left:57px;top:521px;word-spacing:0.4px;}
#t1k_2{left:57px;top:535px;word-spacing:20.2px;}
#t1l_2{left:57px;top:549px;word-spacing:2.2px;}
#t1m_2{left:57px;top:563px;word-spacing:24.8px;}
#t1n_2{left:57px;top:577px;word-spacing:6.4px;}
#t1o_2{left:57px;top:591px;letter-spacing:-0.1px;word-spacing:5.9px;}
#t1p_2{left:57px;top:605px;letter-spacing:-0.1px;word-spacing:15.6px;}
#t1q_2{left:57px;top:619px;word-spacing:0.2px;}
#t1r_2{left:57px;top:633px;word-spacing:0.4px;}
#t1s_2{left:57px;top:647px;word-spacing:4.9px;}
#t1t_2{left:57px;top:661px;word-spacing:0.2px;}
#t1u_2{left:57px;top:680px;letter-spacing:-0.1px;word-spacing:0.5px;}
#t1v_2{left:57px;top:694px;word-spacing:0.2px;}
#t1w_2{left:57px;top:708px;word-spacing:4.3px;}
#t1x_2{left:57px;top:722px;letter-spacing:0.2px;}
#t1y_2{left:85px;top:722px;}
#t1z_2{left:117px;top:722px;}
#t20_2{left:149px;top:722px;letter-spacing:0.1px;}
#t21_2{left:194px;top:722px;letter-spacing:0.1px;}
#t22_2{left:237px;top:722px;letter-spacing:0.1px;}
#t23_2{left:264px;top:722px;}
#t24_2{left:353px;top:722px;letter-spacing:-0.1px;}
#t25_2{left:427px;top:722px;letter-spacing:0.3px;}
#t26_2{left:57px;top:736px;letter-spacing:-0.1px;word-spacing:0.4px;}
#t27_2{left:57px;top:750px;word-spacing:0.2px;}
#t28_2{left:57px;top:764px;word-spacing:5.8px;}
#t29_2{left:57px;top:778px;letter-spacing:-0.1px;word-spacing:0.6px;}
#t2a_2{left:57px;top:792px;letter-spacing:-0.1px;word-spacing:1.9px;}
#t2b_2{left:57px;top:806px;word-spacing:0.1px;}
#t2c_2{left:57px;top:820px;word-spacing:21.1px;}
#t2d_2{left:57px;top:834px;letter-spacing:-0.1px;word-spacing:3.1px;}
#t2e_2{left:57px;top:848px;word-spacing:7.2px;}
#t2f_2{left:57px;top:862px;letter-spacing:-0.1px;word-spacing:8.6px;}
#t2g_2{left:57px;top:876px;word-spacing:1px;}
#t2h_2{left:57px;top:890px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2i_2{left:57px;top:904px;letter-spacing:-0.1px;word-spacing:0.5px;}
#t2j_2{left:57px;top:919px;letter-spacing:-0.1px;word-spacing:0.3px;}
#t2k_2{left:57px;top:932px;word-spacing:8.6px;}
#t2l_2{left:57px;top:947px;word-spacing:6.5px;}
#t2m_2{left:57px;top:960px;word-spacing:13.4px;}
#t2n_2{left:57px;top:974px;word-spacing:-0.4px;}
#t2o_2{left:57px;top:989px;letter-spacing:-0.1px;word-spacing:0.3px;}
#t2p_2{left:57px;top:1003px;word-spacing:12.7px;}
#t2q_2{left:57px;top:1017px;word-spacing:25.7px;}
#t2r_2{left:57px;top:1031px;word-spacing:4.5px;}
#t2s_2{left:57px;top:1045px;word-spacing:1.5px;}
#t2t_2{left:57px;top:1059px;word-spacing:-0.1px;}
#t2u_2{left:57px;top:1087px;letter-spacing:-0.1px;word-spacing:0.5px;}
#t2v_2{left:57px;top:1101px;word-spacing:9.2px;}
#t2w_2{left:57px;top:1115px;word-spacing:23.8px;}
#t2x_2{left:57px;top:1129px;word-spacing:0.1px;}
#t2y_2{left:493px;top:80px;word-spacing:1px;}
#t2z_2{left:493px;top:94px;word-spacing:11.3px;}
#t30_2{left:493px;top:108px;letter-spacing:-0.1px;word-spacing:14.1px;}
#t31_2{left:493px;top:122px;word-spacing:21.7px;}
#t32_2{left:493px;top:136px;letter-spacing:-0.1px;word-spacing:1.1px;}
#t33_2{left:493px;top:150px;word-spacing:0.4px;}
#t34_2{left:493px;top:164px;word-spacing:23.2px;}
#t35_2{left:493px;top:179px;}
#t36_2{left:493px;top:193px;word-spacing:2.2px;}
#t37_2{left:493px;top:207px;letter-spacing:-0.1px;word-spacing:0.3px;}
#t38_2{left:493px;top:221px;word-spacing:0.7px;}
#t39_2{left:493px;top:235px;word-spacing:1.5px;}
#t3a_2{left:493px;top:249px;letter-spacing:-0.1px;word-spacing:0.4px;}
#t3b_2{left:493px;top:263px;word-spacing:16.1px;}
#t3c_2{left:493px;top:277px;letter-spacing:0.1px;}
#t3d_2{left:493px;top:291px;word-spacing:11.2px;}
#t3e_2{left:493px;top:305px;word-spacing:0.2px;}
#t3f_2{left:493px;top:319px;word-spacing:0.3px;}
#t3g_2{left:493px;top:333px;word-spacing:12.7px;}
#t3h_2{left:493px;top:347px;word-spacing:-0.4px;}
#t3i_2{left:493px;top:361px;letter-spacing:-0.1px;}
#t3j_2{left:509px;top:361px;letter-spacing:-0.1px;}
#t3k_2{left:574px;top:361px;letter-spacing:0.2px;}
#t3l_2{left:602px;top:361px;}
#t3m_2{left:658px;top:361px;letter-spacing:-0.2px;}
#t3n_2{left:680px;top:361px;letter-spacing:-0.1px;}
#t3o_2{left:733px;top:361px;letter-spacing:-0.3px;}
#t3p_2{left:754px;top:361px;}
#t3q_2{left:798px;top:361px;letter-spacing:-0.2px;}
#t3r_2{left:833px;top:361px;}
#t3s_2{left:493px;top:375px;word-spacing:0.4px;}
#t3t_2{left:493px;top:389px;word-spacing:21px;}
#t3u_2{left:493px;top:403px;word-spacing:10.9px;}
#t3v_2{left:493px;top:417px;word-spacing:11.2px;}
#t3w_2{left:493px;top:431px;letter-spacing:0.1px;}
#t3x_2{left:493px;top:445px;letter-spacing:-0.1px;}
#t3y_2{left:526px;top:445px;letter-spacing:-0.1px;}
#t3z_2{left:561px;top:445px;}
#t40_2{left:641px;top:445px;letter-spacing:0.1px;}
#t41_2{left:708px;top:445px;letter-spacing:0.1px;}
#t42_2{left:783px;top:445px;}
#t43_2{left:857px;top:445px;letter-spacing:0.2px;}
#t44_2{left:493px;top:459px;word-spacing:9.1px;}
#t45_2{left:493px;top:473px;word-spacing:7.2px;}
#t46_2{left:493px;top:487px;letter-spacing:0.1px;}
#t47_2{left:493px;top:506px;word-spacing:29.1px;}
#t48_2{left:493px;top:520px;letter-spacing:-0.1px;}
#t49_2{left:493px;top:534px;letter-spacing:-0.1px;word-spacing:5.4px;}
#t4a_2{left:493px;top:548px;letter-spacing:-0.1px;word-spacing:8.7px;}
#t4b_2{left:493px;top:563px;word-spacing:0.9px;}
#t4c_2{left:493px;top:576px;word-spacing:27px;}
#t4d_2{left:493px;top:590px;}
#t4e_2{left:618px;top:590px;letter-spacing:-0.1px;}
#t4f_2{left:691px;top:590px;letter-spacing:-0.1px;}
#t4g_2{left:760px;top:590px;letter-spacing:0.2px;}
#t4h_2{left:790px;top:590px;letter-spacing:-0.4px;}
#t4i_2{left:817px;top:590px;}
#t4j_2{left:858px;top:590px;letter-spacing:-0.1px;}
#t4k_2{left:493px;top:605px;word-spacing:5px;}
#t4l_2{left:493px;top:618px;word-spacing:12.1px;}
#t4m_2{left:493px;top:633px;letter-spacing:-0.1px;}
#t4n_2{left:493px;top:651px;}
#t4o_2{left:493px;top:666px;word-spacing:0.9px;}
#t4p_2{left:493px;top:680px;word-spacing:0.3px;}
#t4q_2{left:493px;top:694px;word-spacing:1.2px;}
#t4r_2{left:493px;top:708px;word-spacing:16px;}
#t4s_2{left:493px;top:722px;}
#t4t_2{left:493px;top:736px;word-spacing:15.5px;}
#t4u_2{left:493px;top:750px;}
#t4v_2{left:493px;top:764px;word-spacing:1.2px;}
#t4w_2{left:493px;top:778px;letter-spacing:-0.1px;word-spacing:23.8px;}
#t4x_2{left:493px;top:792px;word-spacing:0.3px;}
#t4y_2{left:493px;top:806px;letter-spacing:-0.1px;word-spacing:0.3px;}
#t4z_2{left:493px;top:825px;}
#t50_2{left:493px;top:839px;letter-spacing:0.1px;word-spacing:1.5px;}
#t51_2{left:493px;top:853px;word-spacing:17.5px;}
#t52_2{left:493px;top:867px;letter-spacing:-0.1px;word-spacing:0.4px;}
#t53_2{left:493px;top:881px;word-spacing:3.5px;}
#t54_2{left:493px;top:895px;word-spacing:3.4px;}
#t55_2{left:493px;top:909px;word-spacing:3.1px;}
#t56_2{left:493px;top:923px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t57_2{left:493px;top:937px;word-spacing:1.7px;}
#t58_2{left:493px;top:951px;word-spacing:20.9px;}
#t59_2{left:493px;top:966px;letter-spacing:-0.1px;word-spacing:2.4px;}
#t5a_2{left:493px;top:980px;word-spacing:1.7px;}
#t5b_2{left:493px;top:994px;word-spacing:7.3px;}
#t5c_2{left:493px;top:1008px;letter-spacing:-0.1px;word-spacing:0.4px;}
#t5d_2{left:493px;top:1026px;word-spacing:0.2px;}
#t5e_2{left:493px;top:1041px;word-spacing:8.6px;}
#t5f_2{left:493px;top:1055px;}
#t5g_2{left:493px;top:1069px;word-spacing:19.7px;}
#t5h_2{left:493px;top:1083px;word-spacing:19.6px;}
#t5i_2{left:493px;top:1097px;word-spacing:18.9px;}
#t5j_2{left:493px;top:1111px;word-spacing:9.9px;}
#t5k_2{left:825px;top:1111px;letter-spacing:0.4px;}
#t5l_2{left:834px;top:1111px;letter-spacing:-0.1px;word-spacing:9.9px;}
#t5m_2{left:493px;top:1125px;letter-spacing:-0.1px;word-spacing:0.4px;}

.s1_2{
	FONT-SIZE: 57.2px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s3_2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_i;
	color: rgb(0,0,0);
}

.s5_2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
}

.s4_2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_j;
	color: rgb(0,0,0);
}

.s9_2{
	FONT-SIZE: 49px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s6_2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s2_2{
	FONT-SIZE: 43.4px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

.s7_2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Arial, Helvetica, sans-serif;
	color: rgb(0,0,0);
}

.s8_2{
	FONT-SIZE: 49px;
	FONT-FAMILY: Helvetica, Arial, sans-serif;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts2" type="text/css" >

@font-face {
	font-family: Calibri_j;
	src: url("fonts/Calibri_j.woff") format("woff");
}

@font-face {
	font-family: Calibri_i;
	src: url("fonts/Calibri_i.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_2" class="t s1_2">CONDITIONS GENERALES DE VENTE</div>
<div id="t2_2" class="t s2_2">S06/P01/1,F12-1/B-2013 </div>
<div id="t3_2" class="t s3_2">Trescal Maroc - Zone Indusparc - </div>
<div id="t4_2" class="t s3_2">Sidi Moumen - Chemin Tertiaire 1015 - 20400 Casablanca - Maroc</div>
<div id="t5_2" class="t s4_2">2 / 2</div>
<div id="t6_2" class="t s5_2">accessoire, incluant de mani�re non limitative les pertes de profit, </div>
<div id="t7_2" class="t s5_2">pertes de revenu, pertes de production ou d�exploitation, pertes de </div>
<div id="t8_2" class="t s5_2">vente ou de contrat, ou dommages-int�r�ts ou p�nalit�s pay�s par le </div>
<div id="t9_2" class="t s5_2">Client. </div>
<div id="ta_2" class="t s5_2">6-5 Le montant total maximum de la responsabilit� de Trescal, dans le </div>
<div id="tb_2" class="t s5_2">cadre de l''ex�cution effective ou envisag�e des prestations pr�vues </div>
<div id="tc_2" class="t s5_2">aux termes de la Proposition Commerciale ne pourra pas exc�der le </div>
<div id="td_2" class="t s5_2">montant HT de la prestation. </div>
<div id="te_2" class="t s5_2">6-6 Sans pr�judice des stipulations de l''article 6-5, le montant total </div>
<div id="tf_2" class="t s5_2">maximum de la responsabilit� de Trescal r�sultant de tout dommage </div>
<div id="tg_2" class="t s5_2">caus� � un �quipement ne saurait d�passer la valeur de march� de </div>
<div id="th_2" class="t s5_2">l''�quipement </div>
<div id="ti_2" class="t s5_2">(ou </div>
<div id="tj_2" class="t s5_2">le </div>
<div id="tk_2" class="t s5_2">prix </div>
<div id="tl_2" class="t s5_2">de </div>
<div id="tm_2" class="t s5_2">remplacement </div>
<div id="tn_2" class="t s5_2">de </div>
<div id="to_2" class="t s5_2">l''�quipement </div>
<div id="tp_2" class="t s5_2">endommag�). </div>
<div id="tq_2" class="t s6_2">7 � ASSURANCES</div>
<div id="tr_2" class="t s5_2">7-1 Sans pr�judice des stipulations de l''articles 6 � Responsabilit� � ci-</div>
<div id="ts_2" class="t s5_2">dessus, Trescal s''engage � souscrire et � maintenir, � ses propres </div>
<div id="tt_2" class="t s5_2">frais, aupr�s d''une compagnie d''assurance notoirement connue, les </div>
<div id="tu_2" class="t s5_2">polices d''assurance n�cessaires pour couvrir ses responsabilit�s aux </div>
<div id="tv_2" class="t s5_2">termes de la Proposition Commerciale et devra communiquer au </div>
<div id="tw_2" class="t s5_2">Client, � sa demande, toute preuve �crite de leur souscription. </div>
<div id="tx_2" class="t s5_2">7.2 Sans pr�judice de toute stipulation contraire dans la Proposition </div>
<div id="ty_2" class="t s5_2">Commerciale, les polices d''assurance g�n�rales responsabilit� civile </div>
<div id="tz_2" class="t s5_2">souscrite par Trescal n''exc�deront pas </div>
<div id="t10_2" class="t s7_2">30</div>
<div id="t11_2" class="t s5_2">.000.000 </div>
<div id="t12_2" class="t s7_2">MAD</div>
<div id="t13_2" class="t s5_2">par an. </div>
<div id="t14_2" class="t s5_2">7.3 Le Client doit, de m�me, souscrire et maintenir, � ses propres frais, </div>
<div id="t15_2" class="t s5_2">aupr�s d�une compagnie d�assurances notoirement connue, des </div>
<div id="t16_2" class="t s5_2">polices d''assurance couvrant de mani�re suffisante toute perte, </div>
<div id="t17_2" class="t s5_2">dommage </div>
<div id="t18_2" class="t s5_2">(y </div>
<div id="t19_2" class="t s5_2">compris </div>
<div id="t1a_2" class="t s5_2">corporel), </div>
<div id="t1b_2" class="t s5_2">pr�judice </div>
<div id="t1c_2" class="t s5_2">ou </div>
<div id="t1d_2" class="t s5_2">r�clamation </div>
<div id="t1e_2" class="t s5_2">qui </div>
<div id="t1f_2" class="t s5_2">r�sulteraient de ou seraient en lien avec l''ex�cution des prestations </div>
<div id="t1g_2" class="t s5_2">pr�vues par la Proposition Commerciale. Il est en particulier de la </div>
<div id="t1h_2" class="t s5_2">responsabilit� du Client de souscrire de telles polices d''assurance </div>
<div id="t1i_2" class="t s5_2">dans l''hypoth�se o� il souhaiterait �tre couvert au-del� du plafond </div>
<div id="t1j_2" class="t s5_2">d''assurance de Trescal. </div>
<div id="t1k_2" class="t s5_2">7.4 Dans le cadre de l''ex�cution des prestations, le Client doit </div>
<div id="t1l_2" class="t s5_2">souscrire une assurance-responsabilit� couvrant de mani�re ad�quate </div>
<div id="t1m_2" class="t s5_2">tous les dommages corporels quels qu�ils soient, subis par le </div>
<div id="t1n_2" class="t s5_2">personnel de Trescal, qui pourraient �tre caus�s par le manquement </div>
<div id="t1o_2" class="t s5_2">du Client � se conformer aux r�glementations applicables en mati�re </div>
<div id="t1p_2" class="t s5_2">de sant� et s�curit� au sein de ses locaux, ou en raison de sa </div>
<div id="t1q_2" class="t s5_2">n�gligence ou de celle de l�un de ses employ�s. </div>
<div id="t1r_2" class="t s5_2">7.5 Le Client renonce et renoncera (et s�engage � ce que son assureur </div>
<div id="t1s_2" class="t s5_2">renonce et renoncera) � tout droit de r�clamer une indemnisation au-</div>
<div id="t1t_2" class="t s5_2">del� des montants susmentionn�s. </div>
<div id="t1u_2" class="t s6_2">8 - GARANTIE</div>
<div id="t1v_2" class="t s5_2">8-1 Dur�e et champ de la garantie</div>
<div id="t1w_2" class="t s5_2">La dur�e de la garantie est de 3 mois pour la maintenance corrective </div>
<div id="t1x_2" class="t s5_2">et </div>
<div id="t1y_2" class="t s5_2">de </div>
<div id="t1z_2" class="t s5_2">10 </div>
<div id="t20_2" class="t s5_2">jours </div>
<div id="t21_2" class="t s5_2">pour </div>
<div id="t22_2" class="t s5_2">la </div>
<div id="t23_2" class="t s5_2">maintenance </div>
<div id="t24_2" class="t s5_2">pr�ventive </div>
<div id="t25_2" class="t s5_2">ou </div>
<div id="t26_2" class="t s5_2">�talonnage/v�rification � partir de la date de signature du bordereau de </div>
<div id="t27_2" class="t s5_2">livraison ou du bordereau de remise. </div>
<div id="t28_2" class="t s5_2">Le remplacement ou la modification de pi�ces pendant la p�riode de </div>
<div id="t29_2" class="t s5_2">garantie ne peut avoir pour effet de la prolonger. </div>
<div id="t2a_2" class="t s5_2">La garantie ne s�applique qu�� la fonctionnalit� et/ou � l�objet ayant fait </div>
<div id="t2b_2" class="t s5_2">l�objet de la prestation. </div>
<div id="t2c_2" class="t s5_2">La garantie couvre la main-d�oeuvre, le co�t de d�montage, de </div>
<div id="t2d_2" class="t s5_2">remplacement ou de r�paration, des essais et du montage des pi�ces </div>
<div id="t2e_2" class="t s5_2">et composants d�fectueux. Le transport aller et retour et l�emballage </div>
<div id="t2f_2" class="t s5_2">incombent au Client. Pour toute intervention sur le site du Client au </div>
<div id="t2g_2" class="t s5_2">titre du pr�sent article, les frais de d�placement, d�h�bergement sont � </div>
<div id="t2h_2" class="t s5_2">la charge du Client. </div>
<div id="t2i_2" class="t s5_2">8-2 Administration de la garantie</div>
<div id="t2j_2" class="t s5_2">En cas de d�faut pr�sum�, le Client adressera dans un d�lai de 5 jours </div>
<div id="t2k_2" class="t s5_2">ouvr�s � Trescal, un recours en garantie contenant les informations </div>
<div id="t2l_2" class="t s5_2">suivantes : d�signation du moyen de mesure, de contr�le et d�essai, </div>
<div id="t2m_2" class="t s5_2">num�ro de s�rie, num�ro de commande, date de livraison initiale, </div>
<div id="t2n_2" class="t s5_2">analyse du d�faut (motif invoqu�). </div>
<div id="t2o_2" class="t s5_2">8-3 Cas d�exclusion de la garantie</div>
<div id="t2p_2" class="t s5_2">Sont exclus de la garantie les appareils modifi�s sans l�accord de </div>
<div id="t2q_2" class="t s5_2">Trescal, les appareils soumis � des contraintes excessives, la </div>
<div id="t2r_2" class="t s5_2">m�connaissance des recommandations du constructeur, les appareils </div>
<div id="t2s_2" class="t s5_2">accident�s, les appareils endommag�s par des intemp�ries naturelles, </div>
<div id="t2t_2" class="t s5_2">les appareils de mesure obsol�tes. </div>
<div id="t2u_2" class="t s6_2">9 - PROPRIETE INDUSTRIELLE</div>
<div id="t2v_2" class="t s8_2">9.1 Tous brevets, droits d�auteur et droits de mod�le relatifs � tous </div>
<div id="t2w_2" class="t s8_2">plans, dessins, sp�cifications, informations et devis que Trescal </div>
<div id="t2x_2" class="t s8_2">fournira au Client, resteront sa propri�t�. </div>
<div id="t2y_2" class="t s8_2">9.2 Au cas o� une action est intent�e contre le Client pour contrefa�on </div>
<div id="t2z_2" class="t s8_2">d�un droit de propri�t� industrielle concernant les produits livr�s au </div>
<div id="t30_2" class="t s8_2">Client, Trescal assurera � son choix et � ses frais sa d�fense et </div>
<div id="t31_2" class="t s8_2">supportera alors le co�t des dommages-int�r�ts et frais que la </div>
<div id="t32_2" class="t s8_2">juridiction saisie pourrait infliger en dernier ressort, sous r�serve que le </div>
<div id="t33_2" class="t s8_2">Client : </div>
<div id="t34_2" class="t s8_2">- notifie � Trescal par �crit et sans d�lai l�existence de cette </div>
<div id="t35_2" class="t s8_2">revendication, </div>
<div id="t36_2" class="t s8_2">- donne � Trescal le contr�le exclusif de la d�fense et le r�glement de </div>
<div id="t37_2" class="t s8_2">la revendication, </div>
<div id="t38_2" class="t s8_2">- fournisse � Trescal, toutes les informations utiles en sa possession et </div>
<div id="t39_2" class="t s8_2">toute l�assistance possible et lui donne tous les pouvoirs afin d�assurer </div>
<div id="t3a_2" class="t s8_2">la d�fense de Trescal, </div>
<div id="t3b_2" class="t s8_2">- n�a pas mis fin � cette revendication par un compromis, ni un </div>
<div id="t3c_2" class="t s8_2">r�glement. </div>
<div id="t3d_2" class="t s8_2">9.3 Si un jugement d�finitif d�favorable �tait rendu � l�encontre de </div>
<div id="t3e_2" class="t s8_2">Trescal, ce dernier choisira l�une des solutions suivantes : </div>
<div id="t3f_2" class="t s8_2">- obtenir le droit pour le Client de continuer � utiliser les produits, </div>
<div id="t3g_2" class="t s8_2">- remplacer ou modifier les produits de sorte qu�ils ne soient plus </div>
<div id="t3h_2" class="t s8_2">contrefaisants, ou </div>
<div id="t3i_2" class="t s8_2">- </div>
<div id="t3j_2" class="t s8_2">reprendre </div>
<div id="t3k_2" class="t s8_2">les </div>
<div id="t3l_2" class="t s8_2">produits </div>
<div id="t3m_2" class="t s8_2">et </div>
<div id="t3n_2" class="t s8_2">cr�diter </div>
<div id="t3o_2" class="t s8_2">le </div>
<div id="t3p_2" class="t s8_2">Client </div>
<div id="t3q_2" class="t s8_2">d�un </div>
<div id="t3r_2" class="t s8_2">montant </div>
<div id="t3s_2" class="t s8_2">correspondant � leur prix d�achat. </div>
<div id="t3t_2" class="t s8_2">9.4 La responsabilit� de Trescal est express�ment exclue si la </div>
<div id="t3u_2" class="t s8_2">contrefa�on r�sulte de la combinaison des produits livr�s avec tout </div>
<div id="t3v_2" class="t s8_2">autre produit ou de toute modification effectu�e autrement que par </div>
<div id="t3w_2" class="t s8_2">Trescal. </div>
<div id="t3x_2" class="t s8_2">9.5 </div>
<div id="t3y_2" class="t s8_2">Les </div>
<div id="t3z_2" class="t s8_2">dispositions </div>
<div id="t40_2" class="t s8_2">ci-dessus </div>
<div id="t41_2" class="t s8_2">constituent </div>
<div id="t42_2" class="t s8_2">l�int�gralit� </div>
<div id="t43_2" class="t s8_2">des </div>
<div id="t44_2" class="t s8_2">engagements de Trescal vis-�-vis du Client dans le cas d��ventuels </div>
<div id="t45_2" class="t s8_2">droits de propri�t� industrielle de tiers relatifs aux produits livr�s par </div>
<div id="t46_2" class="t s8_2">Trescal. </div>
<div id="t47_2" class="t s9_2">10 � EVENEMENTS INDEPENDANTS DE LA VOLONTE DE </div>
<div id="t48_2" class="t s9_2">TRESCAL</div>
<div id="t49_2" class="t s8_2">Trescal ne pourra �tre tenu pour responsable de la non-ex�cution ou </div>
<div id="t4a_2" class="t s8_2">du retard dans l�ex�cution de ses obligations en cas de survenance </div>
<div id="t4b_2" class="t s8_2">d�un �v�nement ind�pendant de sa volont� tel que, notamment, gr�ve, </div>
<div id="t4c_2" class="t s8_2">lock-out, bris de machine, incendie, interruption du r�seau de </div>
<div id="t4d_2" class="t s8_2">t�l�communications, </div>
<div id="t4e_2" class="t s8_2">inondation, </div>
<div id="t4f_2" class="t s8_2">explosion, </div>
<div id="t4g_2" class="t s8_2">fait </div>
<div id="t4h_2" class="t s8_2">de </div>
<div id="t4i_2" class="t s8_2">tiers, </div>
<div id="t4j_2" class="t s8_2">etc. </div>
<div id="t4k_2" class="t s8_2">L�ex�cution des obligations contractuelles de Trescal sera suspendue </div>
<div id="t4l_2" class="t s8_2">pendant la dur�e de l��v�nement et sera prolong�e pour la m�me </div>
<div id="t4m_2" class="t s8_2">dur�e. </div>
<div id="t4n_2" class="t s9_2">11 � RESILIATION</div>
<div id="t4o_2" class="t s8_2">La Proposition Commerciale pourra �tre r�sili�e de plein droit par l�une </div>
<div id="t4p_2" class="t s8_2">ou l�autre des parties dans les cas suivants : </div>
<div id="t4q_2" class="t s8_2">- en cas de manquement grave de l�autre partie � l�une des obligations </div>
<div id="t4r_2" class="t s8_2">contractuelles essentielles mises � sa charge dans la Proposition </div>
<div id="t4s_2" class="t s8_2">Commerciale, si un mois apr�s l�envoi d�une lettre de mise en demeure </div>
<div id="t4t_2" class="t s8_2">adress�e en recommand�e avec avis de r�ception il n�a pas �t� </div>
<div id="t4u_2" class="t s8_2">rem�di� au manquement, </div>
<div id="t4v_2" class="t s8_2">- en cas de redressement judiciaire, conform�ment � l�article L. 622-13 </div>
<div id="t4w_2" class="t s8_2">du Code de Commerce, apr�s mise en demeure adress�e en </div>
<div id="t4x_2" class="t s8_2">recommand� avec avis de r�ception � l�Administrateur rest�e plus d�un </div>
<div id="t4y_2" class="t s8_2">mois sans r�ponse. </div>
<div id="t4z_2" class="t s9_2">12 - CLAUSE DE RESERVE DE PROPRIETE</div>
<div id="t50_2" class="t s8_2">En cas de vente d��quipements, Trescal, en application de la loi du 12 </div>
<div id="t51_2" class="t s8_2">mai 1980, se r�serve la propri�t� du mat�riel jusqu''au paiement </div>
<div id="t52_2" class="t s8_2">int�gral du prix. </div>
<div id="t53_2" class="t s8_2">Toute transformation ou revente du mat�riel par le Client est interdite. </div>
<div id="t54_2" class="t s8_2">Si le Client contrevenait � cette disposition, Trescal serait, apr�s mise </div>
<div id="t55_2" class="t s8_2">en demeure par simple lettre, autoris�e � reprendre les marchandises </div>
<div id="t56_2" class="t s8_2">en stock. </div>
<div id="t57_2" class="t s8_2">Le Client s''engage � assurer le mat�riel au profit de qui il appartiendra </div>
<div id="t58_2" class="t s8_2">contre tous les risques que ce mat�riel pourrait faire courir ou </div>
<div id="t59_2" class="t s8_2">occasionner d�s sa livraison et jusqu''� complet paiement du prix. Si le </div>
<div id="t5a_2" class="t s8_2">Client est locataire des locaux dans lesquels est entrepos� le mat�riel, </div>
<div id="t5b_2" class="t s8_2">il doit informer le bailleur par �crit que ledit mat�riel est grev� d''une </div>
<div id="t5c_2" class="t s8_2">clause de r�serve de propri�t�. </div>
<div id="t5d_2" class="t s9_2">13 - COMPETENCE ET DROIT APPLICABLE</div>
<div id="t5e_2" class="t s8_2">Les parties rechercheront avant toute action contentieuse un accord </div>
<div id="t5f_2" class="t s8_2">amiable. </div>
<div id="t5g_2" class="t s8_2">A d�faut, tout diff�rend relatif � la formation ou l�ex�cution des </div>
<div id="t5h_2" class="t s8_2">pr�sentes conditions g�n�rales de prestations, sera soumis � la </div>
<div id="t5i_2" class="t s8_2">comp�tence du Tribunal de Commerce de PARIS, � moins que </div>
<div id="t5j_2" class="t s8_2">Trescal ne pr�f�re saisir toute autre juridiction comp�tente</div>
<div id="t5k_2" class="t s9_2">. </div>
<div id="t5l_2" class="t s8_2">Le droit </div>
<div id="t5m_2" class="t s8_2">applicable sera la Loi fran�aise </div>

<!-- End text definitions -->

<!-- End Page 2 -->
</div>

</body>
</html>'

IF NOT EXISTS (SELECT * FROM [dbo].[defaultquotationgeneralcondition] WHERE [orgid] = @BusinessCompanyID)
	INSERT INTO [dbo].[defaultquotationgeneralcondition] ([conditiontext], [lastModified], [orgid])
	VALUES (@ConditionText, GETDATE(), @BusinessCompanyID)
ELSE
	UPDATE [dbo].[defaultquotationgeneralcondition] SET [lastModified] = GETDATE(), [conditiontext] = @ConditionText
	WHERE [orgid] = @BusinessCompanyID

COMMIT TRAN