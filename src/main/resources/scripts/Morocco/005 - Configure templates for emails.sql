-- Script to insert/update email templates for Moroco
-- Author: Tony Provost 2016-10-11

BEGIN TRAN

USE [spain];
GO

DECLARE @BusCoidID INT = 6685
DECLARE @BusSubdivID INT = 6951
DECLARE @Locale VARCHAR(10) = 'fr_FR' 

DECLARE @SystemComponent INT
DECLARE @MailSubject NVARCHAR(200)
DECLARE @MailBody NVARCHAR(2000)

----- Jobs -----
SET @SystemComponent = 0
SET @MailSubject = 'Job n� _REPLACE_IDENTIFIER_'
SET @MailBody = '<p>Madame/Monsieur _REPLACE_FIRSTNAME_,</p>

<p>Ceci est un email concernant le Job n� <b>_REPLACE_IDENTIFIER_</b>.</p>

<p>Salutations,</p>

<p>_REPLACE_SENDER_FIRSTNAME_ _REPLACE_SENDER_LASTNAME_</p>

<p>Trescal Maroc</p>
Zone Indusparc - Sidi Moumen<br/>
Chemin tertiaire 1015<br/>
20400 Casablanca<br/>
Maroc<br/>
+212 (0) 5 22 74 09 01<br/>
www.trescal.ma<br/></p>'

IF EXISTS (SELECT * FROM [dbo].[emailtemplate] WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID)
	UPDATE [dbo].[emailtemplate] SET [lastModified] = GETDATE(), [subject] = @MailSubject, [template] = @MailBody WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID
ELSE
	INSERT INTO [dbo].[emailtemplate] ([lastModified], [component], [locale], [subject], [template], [orgid], [subdivid]) VALUES (GETDATE(), @SystemComponent, @Locale, @MailSubject, @MailBody, @BusCoidID, @BusSubdivID)

----- Job Costing -----
SET @SystemComponent = 1
SET @MailSubject = 'Job n� _REPLACE_IDENTIFIER_'
SET @MailBody = '<p>_REPLACE_FIRSTNAME_</p>,

<p>Salutations,</p>

<p>_REPLACE_SENDER_FIRSTNAME_ _REPLACE_SENDER_LASTNAME_</p>

<p>Trescal Maroc</p>
Zone Indusparc - Sidi Moumen<br/>
Chemin tertiaire 1015<br/>
20400 Casablanca<br/>
Maroc<br/>
+212 (0) 5 22 74 09 01<br/>
www.trescal.ma<br/>/</p>'

IF EXISTS (SELECT * FROM [dbo].[emailtemplate] WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID)
	UPDATE [dbo].[emailtemplate] SET [lastModified] = GETDATE(), [subject] = @MailSubject, [template] = @MailBody WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID
ELSE
	INSERT INTO [dbo].[emailtemplate] ([lastModified], [component], [locale], [subject], [template], [orgid], [subdivid]) VALUES (GETDATE(), @SystemComponent, @Locale, @MailSubject, @MailBody, @BusCoidID, @BusSubdivID)
 
----- Quoations -----
SET @SystemComponent = 2
SET @MailSubject = 'Devis n� _REPLACE_IDENTIFIER_'
SET @MailBody = '<p>Madame/Monsieur _REPLACE_FIRSTNAME_</p>,

<p>Veuillez trouver ci-joint votre devis n� <b>_REPLACE_IDENTIFIER_</b>.</p>

<p>En attendant votre r�ponse.</p>

<p>Salutations</p>,

<p>_REPLACE_SENDER_FIRSTNAME_ _REPLACE_SENDER_LASTNAME_<br/>
_REPLACE_SENDER_POSITION_</p>

<p>Trescal Maroc</p>
Zone Indusparc - Sidi Moumen<br/>
Chemin tertiaire 1015<br/>
20400 Casablanca<br/>
Maroc<br/>
+212 (0) 5 22 74 09 01<br/>
www.trescal.ma<br/>/</p>'

IF EXISTS (SELECT * FROM [dbo].[emailtemplate] WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID)
	UPDATE [dbo].[emailtemplate] SET [lastModified] = GETDATE(), [subject] = @MailSubject, [template] = @MailBody WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID
ELSE
	INSERT INTO [dbo].[emailtemplate] ([lastModified], [component], [locale], [subject], [template], [orgid], [subdivid]) VALUES (GETDATE(), @SystemComponent, @Locale, @MailSubject, @MailBody, @BusCoidID, @BusSubdivID)

----- TP Quote Request -----
SET @SystemComponent = 4
SET @MailSubject = 'Demande de devis n� _REPLACE_IDENTIFIER_'
SET @MailBody = '<p>Madame/Monsieur _REPLACE_FIRSTNAME_</p>,

<p>Veuillez trouver ci-joint la demande de devis n� <b>_REPLACE_IDENTIFIER_</b>.</p>

<p>En attendant votre r�ponse.</p>

<p>Salutations</p>,

<p>_REPLACE_SENDER_FIRSTNAME_ _REPLACE_SENDER_LASTNAME_</p>

<p>Trescal Maroc</p>
Zone Indusparc - Sidi Moumen<br/>
Chemin tertiaire 1015<br/>
20400 Casablanca<br/>
Maroc<br/>
+212 (0) 5 22 74 09 01<br/>
www.trescal.ma<br/>/</p>'

IF EXISTS (SELECT * FROM [dbo].[emailtemplate] WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID)
	UPDATE [dbo].[emailtemplate] SET [lastModified] = GETDATE(), [subject] = @MailSubject, [template] = @MailBody WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID
ELSE
	INSERT INTO [dbo].[emailtemplate] ([lastModified], [component], [locale], [subject], [template], [orgid], [subdivid]) VALUES (GETDATE(), @SystemComponent, @Locale, @MailSubject, @MailBody, @BusCoidID, @BusSubdivID)

----- Purchase Order -----
SET @SystemComponent = 15
SET @MailSubject = 'Bon de commande n� _REPLACE_IDENTIFIER_'
SET @MailBody = '<p>Madame/Monsieur _REPLACE_FIRSTNAME_</p>,

<p>Veuillez trouver ci-joint votre bon de commande n� <b>_REPLACE_IDENTIFIER_</b>.</p>

<p>Salutations</p>,

<p>_REPLACE_SENDER_FIRSTNAME_ _REPLACE_SENDER_LASTNAME_</p>

<p>Trescal Maroc</p>
Zone Indusparc - Sidi Moumen<br/>
Chemin tertiaire 1015<br/>
20400 Casablanca<br/>
Maroc<br/>
+212 (0) 5 22 74 09 01<br/>
www.trescal.ma<br/>/</p>'

IF EXISTS (SELECT * FROM [dbo].[emailtemplate] WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID)
	UPDATE [dbo].[emailtemplate] SET [lastModified] = GETDATE(), [subject] = @MailSubject, [template] = @MailBody WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID
ELSE
	INSERT INTO [dbo].[emailtemplate] ([lastModified], [component], [locale], [subject], [template], [orgid], [subdivid]) VALUES (GETDATE(), @SystemComponent, @Locale, @MailSubject, @MailBody, @BusCoidID, @BusSubdivID)

----- Invoices -----
SET @SystemComponent = 18
SET @MailSubject = 'Facture n� _REPLACE_IDENTIFIER_'
SET @MailBody = '<p>Madame/Monsieur _REPLACE_FIRSTNAME_</p>,

<p>Veuillez trouver ci-joint la facture n� <b>_REPLACE_IDENTIFIER_</b> comme demand�.</p>

<p>N''h�sitez pas � me contacter pour tout renseignement compl�mentaire.</p>

<p>Salutations</p>,

<p>_REPLACE_SENDER_FIRSTNAME_ _REPLACE_SENDER_LASTNAME_</p>

<p>Trescal Maroc</p>
Zone Indusparc - Sidi Moumen<br/>
Chemin tertiaire 1015<br/>
20400 Casablanca<br/>
Maroc<br/>
+212 (0) 5 22 74 09 01<br/>
www.trescal.ma<br/>/</p>'

IF EXISTS (SELECT * FROM [dbo].[emailtemplate] WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID)
	UPDATE [dbo].[emailtemplate] SET [lastModified] = GETDATE(), [subject] = @MailSubject, [template] = @MailBody WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID
ELSE
	INSERT INTO [dbo].[emailtemplate] ([lastModified], [component], [locale], [subject], [template], [orgid], [subdivid]) VALUES (GETDATE(), @SystemComponent, @Locale, @MailSubject, @MailBody, @BusCoidID, @BusSubdivID)

----- Credit Notes -----
SET @SystemComponent = 22
SET @MailSubject = 'Note de cr�dit n� _REPLACE_IDENTIFIER_'
SET @MailBody = '<p>Madame/Monsieur _REPLACE_FIRSTNAME_</p>,

<p>Veuillez trouver ci-joint votre note de cr�dit n� <b>_REPLACE_IDENTIFIER_</b>.</p>

<p>N''h�sitez pas � me contacter pour tout renseignement compl�mentaire.</p>

<p>Salutations</p>,

<p>_REPLACE_SENDER_FIRSTNAME_ _REPLACE_SENDER_LASTNAME_</p>

<p>Trescal Maroc</p>
Zone Indusparc - Sidi Moumen<br/>
Chemin tertiaire 1015<br/>
20400 Casablanca<br/>
Maroc<br/>
+212 (0) 5 22 74 09 01<br/>
www.trescal.ma<br/>/</p>'

IF EXISTS (SELECT * FROM [dbo].[emailtemplate] WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID)
	UPDATE [dbo].[emailtemplate] SET [lastModified] = GETDATE(), [subject] = @MailSubject, [template] = @MailBody WHERE [component] = @SystemComponent AND [locale] = @Locale AND [orgid] = @BusCoidID AND [subdivid] = @BusSubdivID
ELSE
	INSERT INTO [dbo].[emailtemplate] ([lastModified], [component], [locale], [subject], [template], [orgid], [subdivid]) VALUES (GETDATE(), @SystemComponent, @Locale, @MailSubject, @MailBody, @BusCoidID, @BusSubdivID)

GO

COMMIT TRAN

