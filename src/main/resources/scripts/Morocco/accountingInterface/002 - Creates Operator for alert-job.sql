USE [msdb]
GO

BEGIN TRANSACTION

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

/* Create category for operator */
IF NOT EXISTS (SELECT * FROM msdb.dbo.syscategories WHERE name=N'Morocco' AND category_class=3)
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'OPERATOR', @type=N'NONE', @name=N'Morocco'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
END

/* Delete operator */
IF EXISTS (SELECT * FROM msdb.dbo.sysoperators WHERE name=N'Morocco Administrator')
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_delete_operator @name=N'Morocco Administrator'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
END

/* Create operator */
EXEC @ReturnCode = msdb.dbo.sp_add_operator @name=N'Morocco Administrator', 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address=N'auto-service.ma030-cas@trescal.com', 
		@category_name=N'Morocco'

COMMIT TRANSACTION

GOTO EndSave

QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION

EndSave:

GO
