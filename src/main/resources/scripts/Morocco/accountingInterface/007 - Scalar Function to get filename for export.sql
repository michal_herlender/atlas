USE [spain]
GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_getFileLedger]')	AND type IN (N'FN'))
	DROP FUNCTION [dbo].[Morocco_getFileLedger]
GO

CREATE FUNCTION dbo.Morocco_getFileLedger
(
	@accountType INT		-- 1 = Invoices / 2 = Credit Notes / 3 = Purchase Order
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	DECLARE @extractionPath NVARCHAR(MAX)
	DECLARE @suffixFile NVARCHAR(MAX)
	DECLARE @extensionFile VARCHAR(5)
	DECLARE @fileName NVARCHAR(MAX)

	SET @extractionPath = 'D:\CWMS\SQLExtract\Morocco\'
	
	SET @extensionFile = '.csv'
	
	SET @suffixFile = '_' + CAST(YEAR(GETDATE()) AS NVARCHAR(MAX)) + CASE 
		WHEN MONTH(GETDATE()) < 10
			THEN '0' + CAST(MONTH(GETDATE()) AS NVARCHAR(MAX))
		ELSE CAST(MONTH(GETDATE()) AS NVARCHAR(MAX))
		END + CASE 
		WHEN DAY(GETDATE()) < 10
			THEN '0' + CAST(DAY(GETDATE()) AS NVARCHAR(MAX))
		ELSE CAST(DAY(GETDATE()) AS NVARCHAR(MAX))
		END

	SET @fileName = CASE 
		-- Invoice accounts
		WHEN @accountType = 1 THEN 'Morocco_InvoiceAccounts'
		-- Credit note accounts		
		WHEN @accountType = 2 THEN 'Morocco_CreditNoteAccounts'
		-- Purchase order accounts		
		WHEN @accountType = 3 THEN 'Morocco_PurchaseOrderAccounts'
		END	+ @suffixFile + @extensionFile

	RETURN @extractionPath + @fileName

END