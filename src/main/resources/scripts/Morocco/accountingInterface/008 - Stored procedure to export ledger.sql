USE [spain]
GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_generateLedger]')	AND type IN (N'P'))
	DROP PROCEDURE [dbo].[Morocco_generateLedger]
GO

CREATE PROCEDURE dbo.Morocco_generateLedger 
(
	@accountType INT		-- 1 = Invoices / 2 = Credit Notes / 3 = Purchase Order
)	
AS
BEGIN
	
	IF (@accountType < 1 OR @accountType > 3)
		RAISERROR(N'The input parameter of the stored procedure is not correct', 16, 1)
	
	DECLARE @databaseName NVARCHAR(200)
	DECLARE @serverName NVARCHAR(200)
	DECLARE @tempTableName NVARCHAR(200)
	DECLARE @ownerName NVARCHAR(200)
	DECLARE @columnList NVARCHAR(MAX)
	DECLARE @columnHeader NVARCHAR(MAX)
	DECLARE @query NVARCHAR(MAX)
	DECLARE @cmd NVARCHAR(4000)
	DECLARE @fileResult NVARCHAR(MAX)
	DECLARE @dirBCP NVARCHAR(200)
	
	-- 1. Construct the query to extract data
	--SET @serverName = 'ES-S-ZARA-DB01\TEST'
	--SET @databaseName = '[spain-test]'
	SET @serverName = 'ES-S-ZARA-DB01'
	SET @databaseName = '[spain]'
	SET @ownerName = 'dbo'	
	
	SET @tempTableName = CASE 
		-- Invoice accounts
		WHEN @accountType = 1 THEN 'MOROCCO_INVOICEACCOUNTS'
		-- Credit note accounts		
		WHEN @accountType = 2 THEN 'MOROCCO_CREDITNOTEACCOUNTS'
		-- Purchase order accounts		
		WHEN @accountType = 3 THEN 'MOROCCO_PURCHASEORDERACCOUNTS'
		END	
		
    SELECT @columnHeader = COALESCE(@columnHeader + ',' ,'') + '''' + column_name + '''' FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tempTableName ORDER BY COLUMNS.ORDINAL_POSITION

	SELECT @columnList = COALESCE(@columnList + ',', '') 
		+ CASE WHEN COLUMNS.DATA_TYPE = 'DATETIME'
				THEN 'CONVERT(VARCHAR,' + column_name + ', 20)'
			ELSE 'CAST(' + column_name + ' AS VARCHAR)'
			END
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tempTableName ORDER BY COLUMNS.ORDINAL_POSITION

	SELECT @query = '"SELECT ' + @columnHeader + ' UNION ALL SELECT ' + @ColumnList + ' FROM ' + @databaseName + '.' + @ownerName + '.' + @tempTableName + ' WHERE accountStatus = ''P''"'
	
	--2.  Generate file result (use option -w instead -c for unicode)
	SELECT @fileResult = dbo.Morocco_getFileLedger(@accountType)
	SET @dirBCP = 'bcp '
	SET @cmd = @dirBCP + @query + ' queryout' + ' "' + @fileResult + '" -w -S ' + @serverName + ' -T'
	EXEC xp_cmdshell @cmd

END
GO
