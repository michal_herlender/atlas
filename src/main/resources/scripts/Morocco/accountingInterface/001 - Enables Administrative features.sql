-- To allow advanced options to be changed.
EXEC sp_configure 'show advanced options', 1
GO
-- To update the currently configured value for advanced options.
RECONFIGURE
GO

-- To enable the features.
EXEC sp_configure 'xp_cmdshell', 1
GO
EXEC sp_configure 'Database Mail XPs', 1
GO

-- To update the currently configured value for this feature.
RECONFIGURE
GO

-- To hide advanced options
EXEC sp_configure 'show advanced options', 0
GO
-- To update the currently configured value for advanced options.
RECONFIGURE
GO

-- To define the max file size to 10 MB
EXEC msdb.dbo.sysmail_configure_sp 'MaxFileSize', '10485760'
GO

