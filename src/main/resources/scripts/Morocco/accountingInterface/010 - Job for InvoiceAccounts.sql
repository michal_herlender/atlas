USE [msdb]
GO

BEGIN TRANSACTION

DECLARE @ReturnCode INT = 0
DECLARE @CatName NVARCHAR(100) = N'Morocco - Accounting Interface'
DECLARE @JobName NVARCHAR(100) = N'Morocco - InvoiceAccounts'
DECLARE @OperatorName NVARCHAR(50) = N'Morocco Administrator'
DECLARE @DatabaseName NVARCHAR(10) = N'spain'
DECLARE @ScheduleName NVARCHAR(100) = N'EveryFirstDay_EveryMonth 2:00'

/* Create specific category*/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=@CatName AND category_class=1)
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@CatName
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
END

/* Delete Job */
IF EXISTS (SELECT * FROM msdb.dbo.sysjobs WHERE name=@JobName)
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_delete_job @job_name=@JobName, @delete_history=1, @delete_unused_schedule=1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
END

/* Create Job */
DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Generating sales ledger (invoices only) for Morocco', 
		@category_name=@CatName, 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=@OperatorName, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

/* Define Step 1 of the Job */
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Get data', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'-- Get invoices to synchronize
EXEC dbo.Morocco_getInvoiceAccounts', 
		@database_name=@DatabaseName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

/* Define Step 2 of the Job */
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Generate file result', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'-- Generate file result for sales ledger (invoices only)
EXEC dbo.Morocco_generateLedger 1', 
		@database_name=@DatabaseName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

/* Create Step 3 of the Job */
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Send file result', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'-- Send file result by mail
EXEC dbo.Morocco_sendLedger 1', 
		@database_name=@DatabaseName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

/* Create Step 4 of the Job */
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Synchronize data', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'-- Update the status of invoices pending synchronization
EXEC dbo.Morocco_syncInvoiceAccounts', 
		@database_name=@DatabaseName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

/* Define schedule of job */
IF (NOT EXISTS (SELECT * FROM msdb.dbo.sysschedules WHERE name = @ScheduleName))
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=@ScheduleName, 
			@enabled=1, 
			@freq_type=32, 
			@freq_interval=8, 
			@freq_subday_type=1, 
			@freq_subday_interval=0, 
			@freq_relative_interval=1, 
			@freq_recurrence_factor=1, 
			@active_start_date=20161220, 
			@active_end_date=99991231, 
			@active_start_time=20000, 
			@active_end_time=235959;
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
END
ELSE
BEGIN
	DECLARE @scheduleid INT
	SELECT TOP 1 @scheduleid = schedule_id FROM msdb.dbo.sysschedules WHERE name = @ScheduleName
	EXEC @ReturnCode = msdb.dbo.sp_attach_schedule @job_id=@jobId, 
			@schedule_id=@scheduleid;
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
END
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

COMMIT TRANSACTION

GOTO EndSave

QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION

EndSave:

GO
