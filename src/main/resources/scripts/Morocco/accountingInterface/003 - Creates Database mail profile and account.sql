-- Create a Database Mail account
EXECUTE msdb.dbo.sysmail_add_account_sp
    @account_name = N'Morocco accounting interface Account',
    @description = N'Mail account for accounting interface for Morocco',
    @email_address = N'auto-service.ma030-cas@trescal.com',
    @display_name = N'Morocco - ERP Accounting interface',
	@replyto_address = N'noreply@trescal.com',
    @mailserver_name = N'172.22.1.47' ;

-- Create a Database Mail profile
EXECUTE msdb.dbo.sysmail_add_profile_sp
    @profile_name = N'Morocco accounting interface Profile',
    @description = N'Profile used for accounting interface for Morocco' ;

-- Add the account to the profile
EXECUTE msdb.dbo.sysmail_add_profileaccount_sp
    @profile_name = N'Morocco accounting interface Profile',
    @account_name = N'Morocco accounting interface Account',
    @sequence_number = 1;