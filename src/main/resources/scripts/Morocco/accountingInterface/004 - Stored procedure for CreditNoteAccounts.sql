USE [spain]
GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_getCreditNoteAccounts]') AND type IN (N'P'))
	DROP PROCEDURE [dbo].[Morocco_getCreditNoteAccounts]
GO

CREATE PROCEDURE dbo.Morocco_getCreditNoteAccounts
AS
BEGIN
	-- Drop temporary table
	IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MOROCCO_CREDITNOTEACCOUNTS]') AND type IN (N'U'))
		DROP TABLE [dbo].[MOROCCO_CREDITNOTEACCOUNTS]
		
	-- Populate temporary table
	SELECT * INTO dbo.MOROCCO_CREDITNOTEACCOUNTS 
	FROM dbo.new_creditnoteaccounts 
	WHERE busID = 6685 AND accountstatus = 'P'
	ORDER BY cnId ASC, itemNo ASC
END
GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_syncCreditNoteAccounts]') AND type IN (N'P'))
	DROP PROCEDURE [dbo].[Morocco_syncCreditNoteAccounts]
GO

CREATE PROCEDURE dbo.Morocco_syncCreditNoteAccounts
AS
BEGIN
	-- Update the status of credit notes pending synchronization : P (pending synchronisation) => S (synchronized)
	UPDATE dbo.new_creditnoteaccounts SET accountstatus = 'S' WHERE accountstatus = 'P' AND cnId IN (SELECT cnId FROM dbo.MOROCCO_CREDITNOTEACCOUNTS)
END
GO