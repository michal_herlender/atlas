USE [spain]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_sendLedger]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Morocco_sendLedger]
GO

CREATE PROCEDURE [dbo].[Morocco_sendLedger]
(
	@accountType INT		-- Type of account : 1 = Invoices / 2 = Credit Notes / 3 = Purchase Order
)
AS
BEGIN

	IF (@accountType < 1 OR @accountType > 3)
		RAISERROR(N'The input parameter of the stored procedure is not correct', 16, 1)

	DECLARE @body NVARCHAR(MAX)
	DECLARE @profileName SYSNAME
	DECLARE @to NVARCHAR(MAX) 
	DECLARE @subject NVARCHAR(MAX)
	DECLARE @cc NVARCHAR(MAX)
	DECLARE @fileAttach NVARCHAR(MAX)
	
	SET @profileName = 'Morocco Accounting interface Profile'
	SET @to = 'assma.benhammou@trescal.com;julien.champin@trescal.com;pierre.bonzon@trescal.com'
	SET @cc = 'jean-luc.richard@trescal.com;christophe.dupriez@trescal.com;yoann.heymann@trescal.com;tony.provost@trescal.com'
		
	SET @subject = CASE
		WHEN @accountType = 1 THEN 'Extraction Journal de ventes (factures)'
		WHEN @accountType = 2 THEN 'Extraction Journal de ventes (notes de crédit)'
		WHEN @accountType = 3 THEN 'Extraction Journal d''achats'
		END

	SET @body = 'Bonjour, ' 
		+ CHAR(13) + CHAR(10) 
		+ CHAR(13) + CHAR(10) 
		+ 'Veuillez trouver ci-joint le fichier '
	SET @body = @body + CASE
		WHEN @accountType = 1 THEN 'journal de ventes (factures seulement)'
		WHEN @accountType = 2 THEN 'journal de ventes (notes de crédit seulement)'
		WHEN @accountType = 3 THEN 'journal d''achats'
		END
	SET @body = @body 
		+ ' pour la période mensuelle écoulée.' 
		+ CHAR(13) + CHAR(10) 
		+ CHAR(13) + CHAR(10) 
		+ 'Cordialement'
	
	SELECT @fileAttach = dbo.Morocco_getFileLedger(@accountType)
		
	EXEC msdb.dbo.sp_send_dbmail @profile_name = @profileName, @recipients = @to, @copy_recipients = @cc, @subject = @subject, @body = @body, @file_attachments=@fileAttach
	
END
GO