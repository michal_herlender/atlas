USE [spain]
GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_getPurchaseOrderAccounts]') AND type IN (N'P'))
	DROP PROCEDURE [dbo].[Morocco_getPurchaseOrderAccounts]
GO

CREATE PROCEDURE dbo.Morocco_getPurchaseOrderAccounts
AS
BEGIN
	-- Drop temporary table
	IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MOROCCO_PURCHASEORDERACCOUNTS]') AND type IN (N'U'))
		DROP TABLE [dbo].[MOROCCO_PURCHASEORDERACCOUNTS]
		
	-- Populate temporary table
	SELECT * INTO dbo.MOROCCO_PURCHASEORDERACCOUNTS 
	FROM dbo.new_purchaseorderaccounts 
	WHERE busID = 6685 AND accountstatus = 'P'
	ORDER BY poId ASC, itemNo ASC
END
GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_syncPurchaseOrderAccounts]') AND type IN (N'P'))
	DROP PROCEDURE [dbo].[Morocco_syncPurchaseOrderAccounts]
GO

CREATE PROCEDURE dbo.Morocco_syncPurchaseOrderAccounts
AS
BEGIN
	-- Update the status of credit notes pending synchronization : P (pending synchronisation) => S (synchronized)
	UPDATE dbo.new_purchaseorderaccounts SET accountstatus = 'S' WHERE accountstatus = 'P' AND poId IN (SELECT poId FROM dbo.MOROCCO_PURCHASEORDERACCOUNTS)
END
GO