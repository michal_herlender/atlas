USE [spain]
GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_getInvoiceAccounts]') AND type IN (N'P'))
	DROP PROCEDURE [dbo].[Morocco_getInvoiceAccounts]
GO

CREATE PROCEDURE dbo.Morocco_getInvoiceAccounts
AS
BEGIN
	-- Drop temporary table
	IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MOROCCO_INVOICEACCOUNTS]') AND type IN (N'U'))
		DROP TABLE [dbo].[MOROCCO_INVOICEACCOUNTS]
		
	-- Populate temporary table
	SELECT * INTO dbo.MOROCCO_INVOICEACCOUNTS 
	FROM dbo.new_invoiceaccounts 
	WHERE busID = 6685 AND accountstatus = 'P'
	ORDER BY invId ASC, itemNo ASC
END
GO

IF EXISTS (SELECT *	FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Morocco_syncInvoiceAccounts]') AND type IN (N'P'))
	DROP PROCEDURE [dbo].[Morocco_syncInvoiceAccounts]
GO

CREATE PROCEDURE dbo.Morocco_syncInvoiceAccounts
AS
BEGIN
	-- Update the status of invoices pending synchronization : P (pending synchronisation) => S (synchronized)
	UPDATE dbo.new_invoiceaccounts SET accountstatus = 'S' WHERE accountstatus = 'P' AND invid IN (SELECT invid FROM dbo.MOROCCO_INVOICEACCOUNTS)
END
GO