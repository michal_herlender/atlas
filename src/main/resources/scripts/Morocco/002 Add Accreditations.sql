BEGIN TRAN
USE [spain];
GO
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'APPROVE' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 1 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'APPROVE' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 2 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'APPROVE' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 3 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'PERFORM' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 1 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'REMOVED' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 1 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'REMOVED' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 2 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'REMOVED' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 3 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SIGN' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 2 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SIGN' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 3 AND [orgid] = 6951;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SUPERVISED' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 1 AND [orgid] = 6951 ;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SUPERVISED' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 2 AND [orgid] = 6951 ;
INSERT INTO [dbo].[calibrationaccreditationlevel] ([accredlevel] ,[caltypeid]) SELECT 'SUPERVISED' , [caltypeid] FROM [dbo].[calibrationtype] WHERE [servicetypeid] = 3 AND [orgid] = 6951 ;
GO
ROLLBACK TRAN