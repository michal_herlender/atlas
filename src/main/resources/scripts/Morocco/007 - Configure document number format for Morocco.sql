-- Script to insert/update document format number for Morocco
-- Author: Tony Provost 2016-10-25

BEGIN TRAN

USE [spain];
GO

DECLARE @BusinessCompanyID INT = 6685
DECLARE @NumberFormat VARCHAR(255)
DECLARE @NumberFormatSubdiv VARCHAR(255) = 'lcstyy000000'
DECLARE @NumberFormatComp VARCHAR(255) = 'lctyy000000'
DECLARE @NumerationType VARCHAR(255)

SET @NumberFormat = @NumberFormatSubdiv
SET @NumerationType = 'DELIVERY_NOTE'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatSubdiv
SET @NumerationType = 'JOB'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatSubdiv
SET @NumerationType = 'SITE_JOB'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatSubdiv
SET @NumerationType = 'VALVE_JOB'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'QUOTATION'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'TPQUOTATION'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatComp	
SET @NumerationType = 'HIRE'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType
	
SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'PURCHASE_ORDER'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType
	
SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'INVOICE'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatComp	
SET @NumerationType = 'ASSET'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType
	
SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'QUOTATION_REQUEST'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType
	
SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'TP_QUOTATION_REQUEST'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType
	
SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'GENERAL_DELIVERY'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType
	
SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'PRICE_LIST'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatComp
SET @NumerationType = 'CREDIT_NOTE'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType
	
SET @NumberFormat = @NumberFormatSubdiv
SET @NumerationType = 'CERTIFICATE'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatSubdiv
SET @NumerationType = 'TP_CERTIFICATE'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatSubdiv
SET @NumerationType = 'CLIENT_CERTIFICATE'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

SET @NumberFormat = @NumberFormatSubdiv
SET @NumerationType = 'IMAGE'
IF NOT EXISTS (SELECT * FROM [dbo].[numberformat] WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType)
	INSERT INTO [dbo].[numberformat] ([format], [numerationType], [lastModified], [global], [universal], [orgid])
	VALUES (@NumberFormat, @NumerationType, GETDATE(), 0, 0, @BusinessCompanyID)
ELSE
	UPDATE [dbo].[numberformat] SET [lastModified] = GETDATE(), [format] = @NumberFormat, [global] = 0, [universal] = 0
	WHERE [orgid] = @BusinessCompanyID AND [numerationType] = @NumerationType

COMMIT TRAN