BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 1 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (1, 'fr_FR', 'Facturation � l''ach�vement du Job') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation � l''ach�vement du Job' WHERE invoicefrequencyid = 1 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 2 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (2, 'fr_FR', 'Facturation r�capitulative fin de mois') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation r�capitulative fin de mois' WHERE invoicefrequencyid = 2 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 3 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (3, 'fr_FR', 'Facturation mensuelle terme � �choir') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation mensuelle terme � �choir' WHERE invoicefrequencyid = 3 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 4 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (4, 'fr_FR', 'Facturation trimetrielle terme � �choir') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation trimetrielle terme � �choir' WHERE invoicefrequencyid = 4 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 5 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (5, 'fr_FR', 'Facturation annuelle au douzi�me') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation annuelle au douzi�me' WHERE invoicefrequencyid = 5 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 6 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (6, 'fr_FR', 'Facturation par commande') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation par commande' WHERE invoicefrequencyid = 6 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 7 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (7, 'fr_FR', 'Facturation par bon de livraison') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation par bon de livraison' WHERE invoicefrequencyid = 7 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 8 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (8, 'fr_FR', 'Facturation r�capitulative fin de trimestre') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation r�capitulative fin de trimestre' WHERE invoicefrequencyid = 8 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.invoicefrequencytranslation WHERE invoicefrequencyid = 9 AND locale = 'fr_FR') 
	INSERT INTO dbo.invoicefrequencytranslation (invoicefrequencyid, locale, translation) VALUES (9, 'fr_FR', 'Facturation � l''avance') 
ELSE 
	UPDATE dbo.invoicefrequencytranslation SET translation = 'Facturation � l''avance' WHERE invoicefrequencyid = 9 AND locale = 'fr_FR';
		
COMMIT TRAN