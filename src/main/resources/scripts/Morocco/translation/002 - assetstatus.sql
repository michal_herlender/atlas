BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.assetstatustranslation WHERE id = 1 AND locale = 'fr_FR') INSERT INTO dbo.assetstatustranslation (id, locale, translation) VALUES (1, 'fr_FR', 'Bien actif') ELSE UPDATE dbo.assetstatustranslation SET translation = 'Bien actif' WHERE id = 1 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.assetstatustranslation WHERE id = 2 AND locale = 'fr_FR') INSERT INTO dbo.assetstatustranslation (id, locale, translation) VALUES (2, 'fr_FR', 'Bien retir� (mais existe toujours)') ELSE UPDATE dbo.assetstatustranslation SET translation = 'Bien retir� (mais existe toujours)' WHERE id = 2 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.assetstatustranslation WHERE id = 3 AND locale = 'fr_FR') INSERT INTO dbo.assetstatustranslation (id, locale, translation) VALUES (3, 'fr_FR', 'Bien r�form�') ELSE UPDATE dbo.assetstatustranslation SET translation = 'Bien r�form�' WHERE id = 3 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.assetstatustranslation WHERE id = 4 AND locale = 'fr_FR') INSERT INTO dbo.assetstatustranslation (id, locale, translation) VALUES (4, 'fr_FR', 'Bien de sauvegarde') ELSE UPDATE dbo.assetstatustranslation SET translation = 'Bien de sauvegarde' WHERE id = 4 AND locale = 'fr_FR';

COMMIT TRAN