BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 1 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (1, 'fr_FR', 'Ch�que') 
ELSE 
	UPDATE dbo.paymentmodetranslation SET translation = 'Ch�que' WHERE paymentmodeid = 1 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 2 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (2, 'fr_FR', 'Virement bancaire') 
ELSE 
	UPDATE dbo.paymentmodetranslation SET translation = 'Virement bancaire' WHERE paymentmodeid = 2 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 3 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (3, 'fr_FR', 'Carte de cr�dit') 
ELSE 
	UPDATE dbo.paymentmodetranslation SET translation = 'Carte de cr�dit' WHERE paymentmodeid = 3 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 4 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (4, 'fr_FR', 'Lettre de change') 
ELSE 
	UPDATE dbo.paymentmodetranslation SET translation = 'Lettre de change' WHERE paymentmodeid = 4 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 5 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (5, 'fr_FR', 'Cr�dit documentaire') 
ELSE 
	UPDATE dbo.paymentmodetranslation SET translation = 'Cr�dit documentaire' WHERE paymentmodeid = 5 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 6 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (6, 'fr_FR', 'Billet � ordre') 
ELSE
	UPDATE dbo.paymentmodetranslation SET translation = 'Billet � ordre' WHERE paymentmodeid = 6 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 7 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (7, 'fr_FR', 'Confirming bank') 
ELSE
	UPDATE dbo.paymentmodetranslation SET translation = 'Confirming bank' WHERE paymentmodeid = 7 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 8 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (8, 'fr_FR', 'Virement garanti par la banque') 
ELSE 
	UPDATE dbo.paymentmodetranslation SET translation = 'Virement garanti par la banque' WHERE paymentmodeid = 8 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymentmodetranslation WHERE paymentmodeid = 9 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymentmodetranslation (paymentmodeid, locale, translation) VALUES (9, 'fr_FR', 'Paiement cash') 
ELSE 
	UPDATE dbo.paymentmodetranslation SET translation = 'Paiement cash' WHERE paymentmodeid = 9 AND locale = 'fr_FR';

COMMIT TRAN