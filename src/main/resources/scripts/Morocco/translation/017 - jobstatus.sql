BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.jobstatusnametranslation WHERE statusid = 1 AND locale = 'fr_FR') INSERT INTO dbo.jobstatusnametranslation (statusid, locale, translation) VALUES (1, 'fr_FR', 'En cours') ELSE UPDATE dbo.jobstatusnametranslation SET translation = 'En cours' WHERE statusid = 1 AND locale = 'dbo.jobstatusnametranslation';
IF NOT EXISTS (SELECT 1 FROM dbo.jobstatusnametranslation WHERE statusid = 2 AND locale = 'fr_FR') INSERT INTO dbo.jobstatusnametranslation (statusid, locale, translation) VALUES (2, 'fr_FR', 'Facture en attente') ELSE UPDATE dbo.jobstatusnametranslation SET translation = 'Facture en attente' WHERE statusid = 2 AND locale = 'dbo.jobstatusnametranslation';
IF NOT EXISTS (SELECT 1 FROM dbo.jobstatusnametranslation WHERE statusid = 3 AND locale = 'fr_FR') INSERT INTO dbo.jobstatusnametranslation (statusid, locale, translation) VALUES (3, 'fr_FR', 'Termin�') ELSE UPDATE dbo.jobstatusnametranslation SET translation = 'Termin�' WHERE statusid = 3 AND locale = 'dbo.jobstatusnametranslation';

COMMIT TRAN