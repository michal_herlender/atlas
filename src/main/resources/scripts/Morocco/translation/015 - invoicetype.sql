BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 1 AND locale = 'fr_FR') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (1, 'fr_FR', '1') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'Vérification' WHERE id = 1 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 3 AND locale = 'fr_FR') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (3, 'fr_FR', '3') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'Ventes' WHERE id = 3 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 4 AND locale = 'fr_FR') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (4, 'fr_FR', '4') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'Location' WHERE id = 4 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.invoicetypetranslation WHERE id = 5 AND locale = 'fr_FR') INSERT INTO dbo.invoicetypetranslation (id, locale, translation) VALUES (5, 'fr_FR', '5') ELSE UPDATE dbo.invoicetypetranslation SET translation = 'Pro-Forma' WHERE id = 5 AND locale = 'fr_FR';

COMMIT TRAN