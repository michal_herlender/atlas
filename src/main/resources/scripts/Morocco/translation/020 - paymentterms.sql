BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 1 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (1, 'fr_FR', '30 jours nets') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '30 jours nets' WHERE paymenttermsid = 1 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 2 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (2, 'fr_FR', '30 jours fin de mois') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '30 jours fin de mois' WHERE paymenttermsid = 2 AND locale = 'fr_FR';
	
IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 3 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (3, 'fr_FR', 'Fin de mois 30 jours') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = 'Fin de mois 30 jours' WHERE paymenttermsid = 3 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 4 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (4, 'fr_FR', '30 jours fin de mois le 10') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '30 jours fin de mois le 10' WHERE paymenttermsid = 4 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 5 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (5, 'fr_FR', '45 jours nets') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '45 jours nets' WHERE paymenttermsid = 5 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 6 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (6, 'fr_FR', '45 jours fin de mois') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '45 jours fin de mois' WHERE paymenttermsid = 6 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 7 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (7, 'fr_FR', 'Fin de mois 45 jours') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = 'Fin de mois 45 jours' WHERE paymenttermsid = 7 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 8 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (8, 'fr_FR', '60 jours nets') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '60 jours nets' WHERE paymenttermsid = 8 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 9 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (9, 'fr_FR', '85 jours nets') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '85 jours nets' WHERE paymenttermsid = 9 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 10 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (10, 'fr_FR', '90 jours nets') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '90 jours nets' WHERE paymenttermsid = 10 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 11 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (11, 'fr_FR', '120 jours nets') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '120 jours nets' WHERE paymenttermsid = 11 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 12 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (12, 'fr_FR', '150 jours nets') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '150 jours nets' WHERE paymenttermsid = 12 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 13 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (13, 'fr_FR', '180 jours nets') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = '180 jours nets' WHERE paymenttermsid = 13 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 14 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (14, 'fr_FR', 'Comptant') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = 'Comptant' WHERE paymenttermsid = 14 AND locale = 'fr_FR';

IF NOT EXISTS (SELECT 1 FROM dbo.paymenttermstranslation WHERE paymenttermsid = 15 AND locale = 'fr_FR') 
	INSERT INTO dbo.paymenttermstranslation (paymenttermsid, locale, translation) VALUES (15, 'fr_FR', 'Prochain mois le 25') 
ELSE 
	UPDATE dbo.paymenttermstranslation SET translation = 'Prochain mois le 25' WHERE paymenttermsid = 15 AND locale = 'fr_FR';

COMMIT TRAN