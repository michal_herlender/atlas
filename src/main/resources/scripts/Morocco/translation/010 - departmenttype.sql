BEGIN TRAN

IF NOT EXISTS (SELECT 1 FROM dbo.departmenttypetranslation WHERE id = 1 AND locale = 'fr_FR') INSERT INTO dbo.departmenttypetranslation (id, locale, translation) VALUES (1, 'fr_FR', 'Service client') ELSE UPDATE dbo.departmenttypetranslation SET translation = 'Service client' WHERE id = 1 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.departmenttypetranslation WHERE id = 2 AND locale = 'fr_FR') INSERT INTO dbo.departmenttypetranslation (id, locale, translation) VALUES (2, 'fr_FR', 'Comptes') ELSE UPDATE dbo.departmenttypetranslation SET translation = 'Comptes' WHERE id = 2 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.departmenttypetranslation WHERE id = 3 AND locale = 'fr_FR') INSERT INTO dbo.departmenttypetranslation (id, locale, translation) VALUES (3, 'fr_FR', 'Laboratoire') ELSE UPDATE dbo.departmenttypetranslation SET translation = 'Laboratoire' WHERE id = 3 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.departmenttypetranslation WHERE id = 4 AND locale = 'fr_FR') INSERT INTO dbo.departmenttypetranslation (id, locale, translation) VALUES (4, 'fr_FR', 'Entr�es / Sorties') ELSE UPDATE dbo.departmenttypetranslation SET translation = 'Entr�es / Sorties' WHERE id = 4 AND locale = 'fr_FR';
IF NOT EXISTS (SELECT 1 FROM dbo.departmenttypetranslation WHERE id = 5 AND locale = 'fr_FR') INSERT INTO dbo.departmenttypetranslation (id, locale, translation) VALUES (5, 'fr_FR', 'Informatique') ELSE UPDATE dbo.departmenttypetranslation SET translation = 'Informatique' WHERE id = 5 AND locale = 'fr_FR';

COMMIT TRAN