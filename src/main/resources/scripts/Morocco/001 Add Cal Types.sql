BEGIN TRAN

USE [spain];
GO

INSERT INTO [dbo].[calibrationtype]
           ([accreditationspecific]
           ,[active]
           ,[certnoresetyearly]
           ,[certprefix]
           ,[firstpagepapertype]
           ,[labeltemplatepath]
           ,[nextcertno]
           ,[orderby]
           ,[pdfcontinuationwatermark]
           ,[pdfheaderwatermark]
           ,[recalldateonlabel]
           ,[restpagepapertype]
           ,[lastModified]
           ,[servicetypeid]
           ,[orgid])
     VALUES
         (1,1,0,'S','CERT_HEADED','/labels/zebra/zebra_label_saccal.vm',10000,1,'SAC_continuation.tif','SACV_header.tif',0,'CERT_CONTINUATION','2016-01-13',1,6951),
         (0,1,1,'','A4_PLAIN','/labels/zebra/zebra_label_stdcal.vm',1,2,NULL,NULL,0,'A4_PLAIN','2016-01-13',2,6951),
         (0,1,0,'C','A4_PLAIN','/labels/zebra/zebra_label_cofccal.vm',1,3,NULL,NULL,0,'A4_PLAIN','2016-01-13',3,6951)
GO

INSERT INTO [dbo].[defaultquotationcalibrationcondition] (conditiontext, islatest, lastModified, caltypeid) SELECT '',1,'2016-01-01',caltypeid FROM [dbo].[calibrationtype] WHERE orgid = 6951;
GO

ROLLBACK TRAN