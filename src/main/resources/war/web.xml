<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xml>
<!-- 
	This is the DEPLOYMENT web.xml: /src/main/resources/war/web.xml
	Note: This file is copied during the "gradle war" process to /WEB-INF/web.xml for WAR builds only
	This allows cache control to be on for production but off for development (and in future other differences)
	The regular /src/main/webapp/WEB-INF/web.xml is used for development purposes only 
-->
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
		 http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">

	<display-name>Workflow Management System</display-name>

	<!--  Note: applicationContext-deploy.xml allows separation of project.properties outside other config files -->
	<context-param>
		<param-name>contextConfigLocation</param-name>
		<param-value>
			classpath:applicationContext-deploy.xml
			classpath:applicationContext-jdbc.xml
			classpath:applicationContext-dao.xml
			classpath:applicationContext-service.xml
			classpath:applicationContext-cache.xml
			classpath:applicationContext-oauth-security.xml
			classpath:applicationContext-spring-security.xml
			classpath:applicationContext-dev.xml
			classpath:applicationContext-schedule.xml
		</param-value>
	</context-param>
	
	<context-param>
		<param-name>build-number</param-name>
		<param-value>@BUILD@</param-value>
	</context-param>

	<listener>
		<listener-class>
			org.trescal.cwms.spring.resolver.JspListener
		</listener-class>
	</listener>
	<listener>
		<listener-class>
			org.springframework.web.context.ContextLoaderListener
		</listener-class>
	</listener>

	<!-- 
		Filter to establish cache control on static resources to prevent repeated GET/304 responses
		DEVELOPMENT: "access plus 0 seconds" so that all resources expire immediately
		DEPLOYMENT: "access plus 12 hours" to accommodate daily updates
	-->	
 	<filter>
		<filter-name>expiresFilter</filter-name>
		<filter-class>org.apache.catalina.filters.ExpiresFilter</filter-class>
		<async-supported>true</async-supported>
		<init-param>
		 <param-name>ExpiresDefault</param-name>
		 <param-value>access plus 12 hours</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>expiresFilter</filter-name>
		<url-pattern>/applets/*</url-pattern>
		<url-pattern>/html/*</url-pattern>
		<url-pattern>/img/*</url-pattern>
		<url-pattern>/labels/*</url-pattern>
		<url-pattern>/locales/*</url-pattern>
		<url-pattern>/script/*</url-pattern>
		<url-pattern>/styles/*</url-pattern>
		<url-pattern>/temp/uploadimages/*</url-pattern>
 		<dispatcher>REQUEST</dispatcher>
 	</filter-mapping>
	
	<filter>
		<filter-name>hibernateFilterChain</filter-name>
		<filter-class>org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter</filter-class>
		<async-supported>true</async-supported>
	</filter>
	<filter-mapping>
		<filter-name>hibernateFilterChain</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<filter-name>charsetFilter</filter-name>
		<filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
		<async-supported>true</async-supported>
		<init-param>
			<param-name>encoding</param-name>
			<param-value>UTF-8</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>charsetFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<display-name>springMultipartFilter</display-name>
		<filter-name>springMultipartFilter</filter-name>
		<filter-class>org.springframework.web.multipart.support.MultipartFilter</filter-class>
		<async-supported>true</async-supported>
		<init-param>
			<param-name>multipartResolverBeanName</param-name>
			<param-value>multipartResolver</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>springMultipartFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	<!-- Spring Security  -->
	<filter>
		<filter-name>springSecurityFilterChain</filter-name>
		<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
		<async-supported>true</async-supported>
	</filter>

	<filter-mapping>
		<filter-name>springSecurityFilterChain</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	<filter>
	    <filter-name>LocaleFilter</filter-name>
	    <filter-class>org.trescal.cwms.core.tools.supportedlocale.LocaleFilter</filter-class>
		<async-supported>true</async-supported>
	</filter>
	<filter-mapping>
	    <filter-name>LocaleFilter</filter-name>
	    <url-pattern>/*</url-pattern>
	</filter-mapping>

	<filter>
		<filter-name>XSS-filter</filter-name>
		<filter-class>org.trescal.cwms.xss.XssFilter</filter-class>
		<async-supported>true</async-supported>
		<init-param>
			<param-name>whiteListedPaths</param-name>
			<param-value>
				/adveso/downloadDocument
				/downloadfile
				/web/downloadfile
				/oauth/authorize
				/oauth/check_token
				/oauth/confirm_access
				/oauth/error
				/oauth/token
				/createemail.htm
			</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>XSS-filter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	<listener>
		<listener-class>org.springframework.web.util.IntrospectorCleanupListener</listener-class>
	</listener>		

	<servlet>
		<servlet-name>cwms</servlet-name>
		<servlet-class>
			org.springframework.web.servlet.DispatcherServlet
		</servlet-class>
	    <init-param>
	        <param-name>contextConfigLocation</param-name>
	            <param-value>classpath:cwms-servlet.xml, classpath:applicationContext-deploy.xml</param-value>
	    </init-param>
		<load-on-startup>1</load-on-startup>
		<async-supported>true</async-supported>
   	</servlet>
	
	<servlet>
		<servlet-name>cwmsweb</servlet-name>
		<servlet-class>
			org.springframework.web.servlet.DispatcherServlet
		</servlet-class>
		<init-param>
	        <param-name>contextConfigLocation</param-name>
	            <param-value>classpath:cwmsweb-servlet.xml, classpath:applicationContext-deploy.xml</param-value>
	    </init-param>
		<load-on-startup>2</load-on-startup>
		<async-supported>true</async-supported>
	</servlet>

	<servlet>
		<servlet-name>dwr-invoker</servlet-name>
		<servlet-class>org.directwebremoting.servlet.DwrServlet</servlet-class>
		<init-param>
			<param-name>debug</param-name>
			<param-value>false</param-value>	<!-- true for development, false for production -->
		</init-param>
		<init-param>
		    <param-name>activeReverseAjaxEnabled</param-name>
		    <param-value>true</param-value>
		</init-param>
		<init-param>
     		<param-name>pollAndCometEnabled</param-name>
     		<param-value>true</param-value>
    	</init-param>
    	<init-param>
     		<param-name>periodCacheableTime</param-name> 
     		<param-value>3600</param-value> <!-- in seconds - 1m/60s for development, 60m/3600s for production -->
    	</init-param>
        <load-on-startup>3</load-on-startup>
		<async-supported>true</async-supported>
	</servlet>
	
	<!-- Separate configuration for RESTful web services calls e.g. Excel calibration data sheet integration -->
	<servlet>
		<servlet-name>rest</servlet-name>
		<servlet-class>
			org.springframework.web.servlet.DispatcherServlet
		</servlet-class>
		<init-param>
	        <param-name>contextConfigLocation</param-name>
	            <param-value>classpath:rest-servlet.xml, classpath:applicationContext-deploy.xml</param-value>
	    </init-param>
		<load-on-startup>4</load-on-startup>
		<async-supported>true</async-supported>
	</servlet>
		
	<!-- Level 1 - static resources are mapped first to the default servlet, so that default cwms servlet does not respond -->
	<servlet-mapping>
		<servlet-name>default</servlet-name>
		<url-pattern>/applets/*</url-pattern>
		<url-pattern>/html/*</url-pattern>
		<url-pattern>/img/*</url-pattern>
		<url-pattern>/labels/*</url-pattern>
		<url-pattern>/locales/*</url-pattern>
		<url-pattern>/script/*</url-pattern>
		<url-pattern>/styles/*</url-pattern>
		<url-pattern>/temp/uploadimages/*</url-pattern>
	</servlet-mapping>
	<!-- Level 2 - Remaining servlets are mapped  -->
	<servlet-mapping>
		<servlet-name>dwr-invoker</servlet-name>
		<url-pattern>/web/dwr/*</url-pattern>
	</servlet-mapping>
	<servlet-mapping>
		<servlet-name>cwmsweb</servlet-name>
		<url-pattern>/web/*</url-pattern>
	</servlet-mapping>
    <servlet-mapping>
        <servlet-name>rest</servlet-name>
        <url-pattern>/rest/*</url-pattern>
    </servlet-mapping>
	<servlet-mapping>
		<servlet-name>dwr-invoker</servlet-name>
		<url-pattern>/dwr/*</url-pattern>
	</servlet-mapping>
	<!-- Level 3 - Finally, the default URL pattern of / is invoked matching all remaining items to CWMS -->
	<servlet-mapping>
		<servlet-name>cwms</servlet-name>
		<url-pattern>/</url-pattern>
	</servlet-mapping>

	<session-config>
    	<session-timeout>120</session-timeout> 
    </session-config>
	
	<welcome-file-list>
		<welcome-file>home.htm</welcome-file>
	</welcome-file-list>

	<jsp-config>
	    <jsp-property-group>
	        <url-pattern>*.jsp</url-pattern>
	        <trim-directive-whitespaces>true</trim-directive-whitespaces>
	    </jsp-property-group>
	</jsp-config>
</web-app>