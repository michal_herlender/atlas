package org.trescal.cwms.nobuild.dataport;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Database
{
	public static void close(Statement stmt)
	{
		try
		{
			Connection conn = stmt.getConnection();
			stmt.close();
			conn.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static Statement getDB2Statement()
	{
		Connection connection;
		Statement stmt;
		try
		{
			Class.forName("COM.ibm.db2.jdbc.app.DB2Driver");
			connection = DriverManager.getConnection("jdbc:db2:cwms07", "db2admin", "Wobbegong13");
			stmt = connection.createStatement();
		}
		catch (Exception e)
		{
			stmt = null;
			e.printStackTrace();
		}
		return stmt;
	}

	public static Statement getMySQLStatement()
	{
		return getMySQLStatement("");
	}

	public static Statement getMySQLStatement(String password)
	{
		Connection connection;
		Statement stmt;

		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost/cwms", "root", password);
			stmt = connection.createStatement();
		}
		catch (Exception e)
		{
			stmt = null;
			e.printStackTrace();
		}

		return stmt;
	}

	public static Statement getMySQLStatement(String dbName, String dbUser, String dbPass)
	{
		Connection connection;
		Statement stmt;

		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(dbName, dbUser, dbPass);
			stmt = connection.createStatement();
		}
		catch (Exception e)
		{
			stmt = null;
			e.printStackTrace();
		}

		return stmt;
	}

	public static Connection getSQLServerConnection(String dbName, String dbUser, String dbPass)
	{
		Connection connection = null;

		try
		{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			connection = DriverManager.getConnection(dbName, dbUser, dbPass);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return connection;
	}

	public static Statement getSQLServerStatement(String dbName, String dbUser, String dbPass)
	{
		Connection connection;
		Statement stmt;

		try
		{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			connection = DriverManager.getConnection("" + dbName, dbUser, dbPass);
			stmt = connection.createStatement();
		}
		catch (Exception e)
		{
			stmt = null;
			e.printStackTrace();
		}
		return stmt;
	}
}
