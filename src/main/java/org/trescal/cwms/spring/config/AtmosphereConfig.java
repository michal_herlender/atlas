package org.trescal.cwms.spring.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.atmosphere.cache.UUIDBroadcasterCache;
import org.atmosphere.client.TrackMessageSizeInterceptor;
import org.atmosphere.cpr.AnnotationProcessor;
import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.AtmosphereHandler;
import org.atmosphere.cpr.AtmosphereInterceptor;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.BroadcasterLifeCyclePolicy.ATMOSPHERE_RESOURCE_POLICY;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.atmosphere.interceptor.HeartbeatInterceptor;
import org.atmosphere.spring.bean.AtmosphereSpringContext;
import org.atmosphere.util.VoidAnnotationProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trescal.cwms.websocket.PrintHandler;

@Configuration
public class AtmosphereConfig {
	@Bean
	public AtmosphereFramework atmosphereFramework()
			throws ServletException, InstantiationException, IllegalAccessException {
		AtmosphereFramework result = new AtmosphereFramework(false, false);
		result.setBroadcasterCacheClassName(UUIDBroadcasterCache.class.getName());
		result.addAtmosphereHandler(PrintHandler.BASE_PATH + "{addr: [0-9]*}", printHandler(), interceptors());
		return result;
	}

	@Bean
	public AtmosphereHandler printHandler() {
		return new PrintHandler();
	}

	private List<AtmosphereInterceptor> interceptors() {
		List<AtmosphereInterceptor> result = new ArrayList<>();
		result.add(new AtmosphereResourceLifecycleInterceptor());
		result.add(new TrackMessageSizeInterceptor());
		result.add(new HeartbeatInterceptor());
		return result;
	}

	@Bean
	public BroadcasterFactory broadcasterFactory()
			throws ServletException, InstantiationException, IllegalAccessException {
		return atmosphereFramework().getAtmosphereConfig().getBroadcasterFactory();
	}
	
	@Bean
	public AtmosphereSpringContext atmosphereSpringContext() {
		AtmosphereSpringContext result = new AtmosphereSpringContext();
		Map<String, String> map = new HashMap<>();
		map.put(ApplicationConfig.BROADCASTER_CLASS, org.atmosphere.cpr.DefaultBroadcaster.class.getName());
		map.put(AtmosphereInterceptor.class.getName(), TrackMessageSizeInterceptor.class.getName());
		map.put(AnnotationProcessor.class.getName(), VoidAnnotationProcessor.class.getName());
		map.put(ApplicationConfig.BROADCASTER_LIFECYCLE_POLICY, ATMOSPHERE_RESOURCE_POLICY.IDLE_DESTROY.toString());
		map.put(ApplicationConfig.ANALYTICS, "false");
		result.setConfig(map);
		return result;
	}
}
