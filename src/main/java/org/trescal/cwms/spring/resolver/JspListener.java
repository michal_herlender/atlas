package org.trescal.cwms.spring.resolver;

import lombok.val;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.jsp.JspFactory;

public class JspListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        val sc = sce.getServletContext();
        val jcs = JspFactory.getDefaultFactory().getJspApplicationContext(sc);
        jcs.addELResolver(new DateFromJavaTimeResolver());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

}
