package org.trescal.cwms.spring.resolver;

import org.springframework.context.i18n.LocaleContextHolder;

import javax.el.ELContext;
import javax.el.TypeConverter;
import java.time.*;
import java.util.Date;

public class DateFromJavaTimeResolver extends TypeConverter {
    @Override
    public Object convertToType(ELContext context, Object obj, Class<?> type) {
        Date date = null;
        if (Date.class.isAssignableFrom(type)) {
            if (obj instanceof Date) {
                date = (Date) obj;
            } else {
                ZonedDateTime zdt = null;
                if (obj instanceof LocalDate) {
                    zdt = ((LocalDate) obj).atStartOfDay(LocaleContextHolder.getTimeZone().toZoneId());
                } else if (obj instanceof LocalDateTime) {
                    zdt = ((LocalDateTime) obj).atZone(LocaleContextHolder.getTimeZone().toZoneId());
                } else if (obj instanceof ZonedDateTime) {
                    zdt = (ZonedDateTime) obj;
                } else  if (obj instanceof  OffsetDateTime) {
                    zdt = ((OffsetDateTime) obj).toZonedDateTime();
                } else if (obj instanceof Instant) {
                    date = Date.from((Instant) obj);
                } else if (obj instanceof Long) {
                    date = new Date((Long) obj);
                }
                if (zdt != null) {
                    date = Date.from(zdt.toInstant());
                }
            }
            context.setPropertyResolved(date != null);
        }
        return date;
    }
}

