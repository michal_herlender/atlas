package org.trescal.cwms.spring.tools.properties;

import org.apache.commons.text.StringEscapeUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Takes a single import file containing the format
 * 
 * filename|messagekey|messagevalue
 * 
 * Imports the specified keys and values into the file, reporting existing value and missing keys
 * 
 * @author galen.beck
 *
 */
public class PropertiesMergeTool {

	private Map<String, Map<String,String>> inputMap;
	private int outputLinesProcessed;
	private int outputLinesNotUpdated;
	private int outputLinesSkipped;
	private int outputLinesUpdated;
	private int outputLinesWritten;
	
	// Change as needed
	private static final String INPUT_FILE = "C:\\cwms\\dbscripts\\properties_input.txt";
	private static final String EDIT_LOCATION_BASE = "C:\\cwms\\src\\main\\resources";
	private static final String DELIMITER = "\\|";
	
	public static void main(String[] args) {
		try {
			(new PropertiesMergeTool()).execute(args);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public PropertiesMergeTool() {
		inputMap = new TreeMap<>();
		outputLinesProcessed = 0;
		outputLinesNotUpdated = 0;
		outputLinesSkipped = 0;
		outputLinesUpdated = 0;
		outputLinesWritten = 0;
	}
	
	public void execute(String[] args) throws IOException {
		step1_readInputFile();
		step2_updatePropertiesFiles();
		step3_reportNotFound();
		step4_reportStats();
	}
	
	/*
	 * First we read the input file
	 */
	private void step1_readInputFile() throws IOException {
		int countInputLines = 0;
		int countInputLinesBlank = 0;
		int countInputLinesTokenMismatch = 0;
		int countInputLinesRead = 0;
		
		// It's expected that the input file will be in the default encoding for properties files (ISO-8859-1), 
		// but we need to accommodate that characters may have been "pasted in" and not \\u escaped.
		// This is dealt with at output time.
		LineNumberReader inputReader = new LineNumberReader(new InputStreamReader(new FileInputStream(INPUT_FILE), StandardCharsets.ISO_8859_1));
		String currentLine = inputReader.readLine();
		while (currentLine != null) {
			countInputLines++;
			String[] tokens = currentLine.split(DELIMITER);
			if (currentLine.trim().isEmpty()) {
				countInputLinesBlank++;
			}
			else if (tokens.length != 3) {
				System.out.println("Warning, expected 3 tokens on line "+countInputLines+" but found "+tokens.length);
				countInputLinesTokenMismatch++;
			}
			else {
				String filename = tokens[0].trim();
				String messageKey = tokens[1].trim();
				String messageValue = tokens[2].trim();
				
				addToInputMap(filename, messageKey, messageValue);
				countInputLinesRead++;
			}
			currentLine = inputReader.readLine();
		}
		inputReader.close();
		
		System.out.println("Total Input Lines : "+countInputLines);
		System.out.println("Blank : "+countInputLinesBlank);
		System.out.println("Token Mismatch : "+countInputLinesTokenMismatch);
		System.out.println("Read : "+countInputLinesRead);
	}
	
	private void addToInputMap(String filename, String messageKey, String messageValue) {
		Map<String,String> fileMap = inputMap.get(filename);
		if (fileMap == null) {
			fileMap = new TreeMap<>();
			inputMap.put(filename, fileMap);
		}
		fileMap.put(messageKey, messageValue);
	}
	
	/*
	 * All the identified properties files are then processed 
	 */
	private void step2_updatePropertiesFiles() throws IOException {
		System.out.println("Distinct properties files : "+inputMap.keySet().size());
		for (String propertiesFileName : inputMap.keySet()) {
			editFile(propertiesFileName, inputMap.get(propertiesFileName));
		}
	}
	
	private void editFile(String propertiesFileName, Map<String, String> propertiesMap) throws IOException {
		String filename = EDIT_LOCATION_BASE+File.separatorChar+propertiesFileName;
		System.out.println("Reading file : "+filename);
		// We read in the properties files using the default encoding for properties files (ISO-8859-1), 
		// also anticipating that characters may have been "pasted in" and not \\u escaped.
		// This is dealt with at output time.
		LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(filename), StandardCharsets.ISO_8859_1));
		ArrayList<String> updateBuffer = new ArrayList<>();
		String currentLine = reader.readLine(); 
		while (currentLine != null) {
			// If current line has key/value pair, check for an update, otherwise write as is
			String[] tokens = currentLine.split("=");
			if (tokens.length == 2) {
				String key = tokens[0];
				String trimmedKey = key.trim();
				
				if (propertiesMap.containsKey(trimmedKey)) {
					String newValue = propertiesMap.get(trimmedKey);
					// Preserve spacing using untrimmed key, and escape NEW value using \\u encoding
					// We don't encode the key, as it may contain tabs...
					updateBuffer.add(key+"= "+StringEscapeUtils.escapeJava(newValue));
					propertiesMap.remove(key.trim());
					outputLinesUpdated++;
				}
				else {
					// Write existing line exactly as it (it might have \\u encoding AND/OR special characters)
//					updateBuffer.add(currentLine);
//					outputLinesNotUpdated++;
					
					// Preserve spacing using untrimmed key, and escape EXISTING value using \\u encoding
					// This converts any existing unenscaped values consistently to proper \\u encoding.
					// We don't encode the key, as it may contain tabs...
					String oldValue = tokens[1];
					String unescapedTrimmedOldValue = StringEscapeUtils.unescapeJava(oldValue.trim());
					updateBuffer.add(key+"= "+StringEscapeUtils.escapeJava(unescapedTrimmedOldValue));
					outputLinesNotUpdated++;
				}
			}
			else {
				// Write exactly as is, without any encoding (tabs, etc...) - it's either comments or a blank line
				updateBuffer.add(currentLine);
				outputLinesSkipped++;
				
			}
			outputLinesProcessed++;
			currentLine = reader.readLine();
		}
		reader.close();
		System.out.println("Writing file : "+filename);
		
		Writer writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename), StandardCharsets.ISO_8859_1));
		
		for (String outputLine : updateBuffer) {
			writer.write(outputLine);
			writer.write("\r\n");
			outputLinesWritten++;
		}
		
		writer.flush();
		writer.close();
	}
	
	/*
	 * Keys not found are remaining in map; report to user as mismatches
	 */
	private void step3_reportNotFound() {
		for (String filename : inputMap.keySet()) {
			Map<String,String> notFound = inputMap.get(filename);
			System.out.println(filename+" : "+notFound.size()+" not found");
			for (String key : notFound.keySet()) {
				String value = notFound.get(key);
				System.out.println(key+" = "+value);
			}
		}
	}

	private void step4_reportStats() {
		System.out.println("outputLinesProcessed : "+outputLinesProcessed);
		System.out.println("outputLinesNotUpdated : "+outputLinesNotUpdated);
		System.out.println("outputLinesSkipped : "+outputLinesSkipped);
		System.out.println("outputLinesUpdated : "+outputLinesUpdated);
		System.out.println("outputLinesWritten : "+outputLinesWritten);
	}
}