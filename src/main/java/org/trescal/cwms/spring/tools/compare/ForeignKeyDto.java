package org.trescal.cwms.spring.tools.compare;

public class ForeignKeyDto {
	private String pkTableName;
	private String pkColumnName;
	private String fkColumnName;
	private String fkTableName;
	private String fkName;
	
	public ForeignKeyDto(String pkTableName, String pkColumnName, String fkColumnName, String fkTableName, String fkName) {
		this.pkTableName = pkTableName;
		this.pkColumnName = pkColumnName;
		this.fkColumnName = fkColumnName;
		this.fkTableName = fkTableName;
		this.fkName = fkName;
	}
	
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("pkTable: "+pkTableName);
		result.append(" pkColumn: "+pkColumnName);
		result.append(" fkTable: "+fkTableName);
		result.append(" fkColumn: "+fkColumnName);
		result.append(" fkName: "+fkName);
		return result.toString();
	}
	
	// Returns true if all fields (not including name - not checked) are equal
	public boolean equivalentTo(ForeignKeyDto otherDto) {
		return this.pkTableName.equals(otherDto.pkTableName) &&
			this.pkColumnName.equals(otherDto.pkColumnName) &&
			this.fkColumnName.equals(otherDto.fkColumnName) &&
			this.fkTableName.equals(otherDto.fkTableName);
	}
	
	public String getPkTableName() {
		return pkTableName;
	}
	public String getPkColumnName() {
		return pkColumnName;
	}
	public String getFkColumnName() {
		return fkColumnName;
	}
	public String getFkTableName() {
		return fkTableName;
	}
	public String getFkName() {
		return fkName;
	}
}
