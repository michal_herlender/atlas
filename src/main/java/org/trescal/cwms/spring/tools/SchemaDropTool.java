package org.trescal.cwms.spring.tools;

import java.util.EnumSet;

import org.hibernate.boot.Metadata;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

public class SchemaDropTool extends SchemaTool {
	
	private static final String FILENAME_PRIMARY = "script-drop.sql"; 
	private static final String FILENAME_SECONDARY = "script-drop-files.sql"; 
	
	public SchemaDropTool() {
		super(true, FILENAME_PRIMARY, FILENAME_SECONDARY);
	}
	
	public static void main(String[] args) {
		(new SchemaDropTool()).execute();
	}
	
	@Override
    protected void performTask(Metadata metadata, String database, String outputFilePath) {
		super.initialiseFile(database, outputFilePath);
	    SchemaExport export = getSchemaExport(outputFilePath);
	    dropDataBase(export, metadata);
	    super.finaliseFile(outputFilePath);
	}
    
   /**
    * TargetType.DATABASE - Execute on Database (not included here, we want to review SQL before execution)
    * TargetType.SCRIPT - Write Script file.
    * TargetType.STDOUT - Write log to Console.
    */
   private void dropDataBase(SchemaExport export, Metadata metadata) {
	   // Note, TargetType.DATABASE is omitted, if included it executes statements directly to database
	   EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.SCRIPT, TargetType.STDOUT);
       export.drop(targetTypes, metadata);
   }
}