package org.trescal.cwms.spring.tools.compare;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.spring.tools.SchemaConstants;
import org.trescal.cwms.spring.tools.SchemaTool;
/**
 * Utility that compares a production database (e.g. "spain") 
 *  with an empty newly created database (e.g. "empty") and then matches up
 *  foreign keys to be renamed. 
 * @author Galen Beck
 *
 */
public class CompareForeignKeys {
	private Connection connectionProductionPrimary;
	private Connection connectionEmptyPrimary;
	private List<ForeignKeyDto> listProductionKeys;
	private List<ForeignKeyDto> listEmptyKeys;
	
	public static final String OUTPUT_FILENAME = "rename-keys.sql";	
	private static final Logger logger = LoggerFactory.getLogger(CompareForeignKeys.class);
	
	public CompareForeignKeys() {
		listProductionKeys = new ArrayList<>();
		listEmptyKeys = new ArrayList<>();
	}
	
	public static void main(String[] args) {
		(new CompareForeignKeys()).main_execute();
	}

	private void main_execute() {
		try {
			execute();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		finally {
			try {
				if (connectionProductionPrimary != null) {
					logger.info("Closing primary production connection");
					connectionProductionPrimary.close();
				}
				if (connectionEmptyPrimary != null) {
					logger.info("Closing primary empty connection");
					connectionEmptyPrimary.close();
				}
			}
			catch (SQLException e) {
				System.out.println("Error closing connection");
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	
	private void execute() throws SQLException, IOException {
		connectionProductionPrimary = getConnection(SchemaConstants.PRODUCTION_DB_PRIMARY);
		connectionEmptyPrimary = getConnection(SchemaConstants.EMPTY_DB_PRIMARY);
		logger.info("Reading foreign keys from production");
		readForeignKeys(connectionProductionPrimary, listProductionKeys);
		logger.info("Reading foreign keys from empty");
		readForeignKeys(connectionEmptyPrimary, listEmptyKeys);
		logger.info("Comparing keys");
		compareKeys();
	}
	
	private Connection getConnection(String databaseName) throws SQLException {
		logger.info("Getting connection to "+databaseName);
		String url = SchemaConstants.JDBC_URL_PREFIX+databaseName;
		String user = SchemaConstants.JDBC_USER;
		String password = SchemaConstants.JDBC_PASSWORD;
		return DriverManager.getConnection(url, user, password);
	}
	
	private void readForeignKeys(Connection connection, List<ForeignKeyDto> listKeys) throws SQLException {
		DatabaseMetaData metaData = connection.getMetaData();
		ResultSet rsTables = metaData.getTables(null, SchemaConstants.SCHEMA_NAME, null, null);
		logger.info("Table count: "+rsTables.getFetchSize());
		boolean validTableRow = rsTables.next();
		while (validTableRow) {
			// DatabaseMetaData.getTables() -- 3: TABLE_NAME String => table name
			String tableName = rsTables.getString(3);
			ResultSet rsExportedKeys = metaData.getExportedKeys(null, SchemaConstants.SCHEMA_NAME, tableName);
			logger.info("Table: "+tableName);
			boolean validKeyRow = rsExportedKeys.next();
			while (validKeyRow) {
				String pkTableName = rsExportedKeys.getString(JdbcMetaDataConstants.EXPORTED_KEYS_PKTABLE_NAME);
				String pkColumnName = rsExportedKeys.getString(JdbcMetaDataConstants.EXPORTED_KEYS_PKCOLUMN_NAME);
				String fkColumnName = rsExportedKeys.getString(JdbcMetaDataConstants.EXPORTED_KEYS_FKCOLUMN_NAME);
				String fkTableName = rsExportedKeys.getString(JdbcMetaDataConstants.EXPORTED_KEYS_FKTABLE_NAME);
				String fkName = rsExportedKeys.getString(JdbcMetaDataConstants.EXPORTED_KEYS_FK_NAME);
						
				ForeignKeyDto dto = new ForeignKeyDto(pkTableName, pkColumnName, fkColumnName, fkTableName, fkName);
				listKeys.add(dto);
				logger.info(dto.toString());
				validKeyRow = rsExportedKeys.next();
			}
			
			validTableRow = rsTables.next();
		}
	}
	
	/*
	 * Iterates through the list of keys in the empty (newly created) database and attempts
	 * to find matches in the production (created over time) database.
	 */
	private void compareKeys() throws IOException {
		String filepath = SchemaConstants.SCRIPT_DIRECTORY+File.separator+OUTPUT_FILENAME;
		SchemaTool.deleteFileIfExists(filepath);

		FileOutputStream os = new FileOutputStream(filepath);
		logger.info("Initialising file: " + filepath);
		PrintWriter pw = new PrintWriter(os, true);
		pw.println("USE [" + SchemaConstants.PRODUCTION_DB_PRIMARY + "]");
		pw.println("BEGIN TRAN");
		
		int countMissing = 0;
		int countSingleMatch = 0;
		int countSingleToRename = 0;
		int countMultiple = 0;
		for (ForeignKeyDto emptyKey : listEmptyKeys) {
			List<ForeignKeyDto> equivalentKeys = findEquivalentKeys(listProductionKeys, emptyKey);
			if (equivalentKeys.isEmpty()) {
				logger.info("Missing key: "+emptyKey.toString());
				countMissing++;
			}
			else if (equivalentKeys.size() > 1) {
				logger.info("Multiple keys for: "+emptyKey.toString());
				for (ForeignKeyDto dto : equivalentKeys) {
					logger.info("Match: "+dto.toString());
				}
				countMultiple++;
			}
			else {
				ForeignKeyDto productionKey = equivalentKeys.get(0);
				if (productionKey.getFkName().equals(emptyKey.getFkName())) {
					countSingleMatch++;
				}
				else {
					countSingleToRename++;
					logger.trace("Name mismatch: "+emptyKey.toString());
					logger.trace("To Rename: "+productionKey.toString());
					pw.println("EXEC sp_rename '"+productionKey.getFkName()+"', '"+emptyKey.getFkName()+"';");
				}
			}
		}
		logger.info("countMissing: "+countMissing);
		logger.info("countSingleMatch: "+countSingleMatch);
		logger.info("countSingleToRename: "+countSingleToRename);
		logger.info("countMultiple: "+countMultiple);

		logger.info("Finalising file: " + filepath);
		pw.println();
		pw.println("COMMIT TRAN");
		os.flush();
		os.close();
	}
	
	/*
	 * Potentially more than one matching key (could delete duplicates?)
	 */
	private List<ForeignKeyDto> findEquivalentKeys(List<ForeignKeyDto> listKeys, ForeignKeyDto dto) {
		List<ForeignKeyDto> results = new ArrayList<>();
		// Linear search through keys (utility, performance not important)
		for (ForeignKeyDto candidate : listKeys) {
			if (candidate.equivalentTo(dto)) {
				results.add(candidate);
			}
		}
		return results;
	}
}
