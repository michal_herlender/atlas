package org.trescal.cwms.spring.tools;

public class SchemaConstants {
	public static final String SCRIPT_DIRECTORY = "C:\\cwms\\dbscripts";
	public static final String PRODUCTION_CONFIG_PRIMARY = "hibernate-standalone.cfg.xml";
	public static final String PRODUCTION_CONFIG_SECONDARY = "hibernate-standalone-files.cfg.xml";
	public static final String TEST_CONFIG_PRIMARY = "hibernate-test.cfg.xml";
	public static final String TEST_CONFIG_SECONDARY = "hibernate-test-files.cfg.xml";
	// Database names are used just for generating script files; 
	// connection in hibernate files
	public static final String PRODUCTION_DB_PRIMARY = "atlas";
	public static final String PRODUCTION_DB_SECONDARY = "atlasfiles";
	public static final String TEST_DB_PRIMARY = "cwms_test";
	public static final String TEST_DB_SECONDARY = "cwmsfiles_test";
	// Empty (newly created from SQL script) database used for foreign key comparisons vs existing DB
	public static final String EMPTY_DB_PRIMARY = "empty";
	public static final String EMPTY_DB_SECONDARY = "emptyfiles";
	// Connection info for JDBC (separate from properties files)
	public static final String JDBC_URL_PREFIX = "jdbc:sqlserver://localhost;database=";
	public static final String JDBC_USER = "sa";
	public static final String JDBC_PASSWORD = "Wobbegong13";
	// Schema name for cwms
	public static final String SCHEMA_NAME = "dbo";
	public static final String CDC_SCHEMA_NAME = "cdc";
	
	public static final String[] CLASSPATH_PRIMARY = new String[] { "org.trescal.cwms.core",
			"org.trescal.cwms.device", "org.trescal.cwms.batch" };
	public static final String[] CLASSPATH_SECONDARY = new String[] { "org.trescal.cwms.files" };

}
