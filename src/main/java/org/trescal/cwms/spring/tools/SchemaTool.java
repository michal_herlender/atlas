package org.trescal.cwms.spring.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.persistence.Entity;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.cdc.entity.EntityCT;

/**
 * Base class for running Hibernate (5.1) schema manipulation tools Intended for
 * command line use (e.g. from Eclipse/IntelliJ) as a development activity only
 * such that the tools can be run before application startup (e.g. to create
 * modification scripts) Normally, the ant task tools, and built in Hibernate
 * tools use XML mappings Here, reflection is used for classpath scanning
 * similar to a live application context
 * 
 * @author Galen Created 2017-08-23
 * 
 * Adapted from following approach:
 * 
 * https://stackoverflow.com/questions/35993598/how-do-i-export-schema-after-upgrading-hibernate-to-5-1-0
 */

public abstract class SchemaTool {
	private boolean production;
	private String primaryOutputFilepath;
	private String secondaryOutputFilepath;
	
	private static final Logger logger = LoggerFactory.getLogger(SchemaTool.class);

	/**
	 * For use without file generation e.g. validation script
	 */
	protected SchemaTool(boolean production) {
		this.production = production;
	}
			
	/**
	 * For use with file generation e.g. create / update / drop scripts
	 */
	protected SchemaTool(boolean production, String primaryOutputFilename, String secondaryOutputFilename) {
		this.production = production;
		this.primaryOutputFilepath = SchemaConstants.SCRIPT_DIRECTORY + File.separator + primaryOutputFilename;
		this.secondaryOutputFilepath = SchemaConstants.SCRIPT_DIRECTORY + File.separator + secondaryOutputFilename;
	}
	
	public static void deleteFileIfExists(String filepath) {
		File file = new File(filepath);
		if (file.exists()) {
			logger.info("Deleting file: " + file.getAbsolutePath());
			file.delete();
		}
	}

	protected void initialiseFile(String database, String filepath) {
		try {
			deleteFileIfExists(filepath);
			FileOutputStream os = new FileOutputStream(filepath);
			logger.info("Initialising file: " + filepath);
			PrintWriter pw = new PrintWriter(os, true);
			pw.println("BEGIN TRAN");
			pw.println("EXEC sp_msforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'");
			pw.println("EXEC sp_msforeachtable 'DROP TABLE ?'");
			pw.println("DROP SEQUENCE IF EXISTS dbo.invoice_po_item_id_sequence");
			pw.println("DROP SEQUENCE IF EXISTS dbo.invoiceitem_id_sequence");
			pw.println("DROP SEQUENCE IF EXISTS dbo.quotation_item_id_sequence");
			os.flush();
			os.close();
		}
		catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	protected void finaliseFile(String filepath) {
		try {
			FileOutputStream os = new FileOutputStream(filepath, true);
			logger.info("Finalising file: " + filepath);
			PrintWriter pw = new PrintWriter(os, true);
			pw.println();
			pw.println("COMMIT TRAN");
			os.flush();
			os.close();
		}
		catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	protected abstract void performTask(Metadata metadata, String database, String outputFilePath);

	protected void execute() {
		String configPrimary = production ? SchemaConstants.PRODUCTION_CONFIG_PRIMARY : SchemaConstants.TEST_CONFIG_PRIMARY;
		String configSecondary = production ? SchemaConstants.PRODUCTION_CONFIG_SECONDARY : SchemaConstants.TEST_CONFIG_SECONDARY;
		String databasePrimary = production ? SchemaConstants.PRODUCTION_DB_PRIMARY : SchemaConstants.TEST_DB_PRIMARY;
		String databaseSecondary = production ? SchemaConstants.PRODUCTION_DB_SECONDARY : SchemaConstants.TEST_DB_SECONDARY;
		   
		executeTask(configPrimary, SchemaConstants.CLASSPATH_PRIMARY, databasePrimary, this.primaryOutputFilepath);
		executeTask(configSecondary, SchemaConstants.CLASSPATH_SECONDARY, databaseSecondary, this.secondaryOutputFilepath);
	}

	private void executeTask(String configFile, String[] classpathArray, String database, String outputFilepath) {
		// Create the ServiceRegistry from hibernate-xxx.cfg.xml
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure(configFile).build();

		// Use a single reflections instance; 
		// Works around bug in reflections 0.9.12 with previous implementation on empty package scan
		// https://github.com/ronmamo/reflections/issues/273
		ConfigurationBuilder builder = new ConfigurationBuilder();
		FilterBuilder filterBuilder = new FilterBuilder();
		for (String classpath : classpathArray) {
			filterBuilder.includePackage(classpath);
		}
		builder.filterInputsBy(filterBuilder);
		builder.setUrls(ClasspathHelper.forClassLoader());
		Reflections reflections = new Reflections(builder);
		
		// Create a metadata sources using the specified service registry.
		// Exclude EntityCT classes optionally present from SQL Server CDC (Change Data Capture) system tables   
		MetadataSources metadataSources = new MetadataSources(serviceRegistry);
		reflections.getTypesAnnotatedWith(Entity.class).stream()
			.filter(clazz -> !EntityCT.class.isAssignableFrom(clazz))
			.forEach(metadataSources::addAnnotatedClass);
		
		logger.info("Annotated classes:");
		logger.info(metadataSources.getAnnotatedClasses().toString());

		Metadata metadata = metadataSources.getMetadataBuilder().build();
		try {
			performTask(metadata, database, outputFilepath);
		} finally {
			// The service registry must be closed automatically;
			StandardServiceRegistryBuilder.destroy(serviceRegistry);
		}
	}

	protected SchemaExport getSchemaExport(String outputFilePath) {
		SchemaExport export = new SchemaExport();
		// Script file.
		File outputFile = new File(outputFilePath);
		logger.info("Export file: " + outputFilePath);

		export.setDelimiter(";");
		export.setOutputFile(outputFile.getAbsolutePath());
		export.setHaltOnError(true);
		export.setFormat(true);
		return export;
	}
}
