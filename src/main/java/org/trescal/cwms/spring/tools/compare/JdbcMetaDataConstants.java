package org.trescal.cwms.spring.tools.compare;

public class JdbcMetaDataConstants {
	// DatabaseMetaData.getExportedKeys() result set mappings - from javadocs
	public static final int EXPORTED_KEYS_PKTABLE_CAT = 1;
	public static final int EXPORTED_KEYS_PKTABLE_SCHEM = 2;
	public static final int EXPORTED_KEYS_PKTABLE_NAME = 3;
	public static final int EXPORTED_KEYS_PKCOLUMN_NAME = 4;
	public static final int EXPORTED_KEYS_FKTABLE_CAT = 5;
	public static final int EXPORTED_KEYS_FKTABLE_SCHEM = 6;
	public static final int EXPORTED_KEYS_FKTABLE_NAME = 7;
	public static final int EXPORTED_KEYS_FKCOLUMN_NAME = 8;
	public static final int EXPORTED_KEYS_KEY_SEQ = 9;
	public static final int EXPORTED_KEYS_UPDATE_RULE = 10;
	public static final int EXPORTED_KEYS_DELETE_RULE  = 11;
	public static final int EXPORTED_KEYS_FK_NAME = 12;
	public static final int EXPORTED_KEYS_PK_NAME = 13;
	public static final int EXPORTED_KEYS_DEFERRABILITY  = 14;
}
