package org.trescal.cwms.spring.tools;

import org.hibernate.boot.Metadata;
import org.hibernate.tool.hbm2ddl.SchemaValidator;

public class SchemaValidatorTool extends SchemaTool {

	public SchemaValidatorTool() {
		// No files generated from validation, but we use the same database configuration as the other tools
		super(true);
	}
	
	@Override
	protected void performTask(Metadata metadata, String database, String outputFilePath) {
		(new SchemaValidator()).validate(metadata);

	}

	public static void main(String[] args) {
		(new SchemaValidatorTool()).execute();
	}

}
