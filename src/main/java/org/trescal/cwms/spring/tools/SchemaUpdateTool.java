package org.trescal.cwms.spring.tools;

import java.util.EnumSet;

import org.hibernate.boot.Metadata;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.hibernate.tool.schema.TargetType;

public class SchemaUpdateTool extends SchemaTool {

	private static final String FILENAME_PRIMARY = "script-update.sql"; 
	private static final String FILENAME_SECONDARY = "script-update-files.sql"; 

	public SchemaUpdateTool() {
		super(true, FILENAME_PRIMARY, FILENAME_SECONDARY);
	}
	
	public static void main(String[] args) {
		(new SchemaUpdateTool()).execute();
	}

   /**
    * TargetType.DATABASE - Execute on Dat@abase (not included here, we want to review SQL)
    * TargetType.SCRIPT - Write Script file.
    * TargetType.STDOUT - Write log to Console.
    */
	@Override
	protected void performTask(Metadata metadata, String database, String outputFilePath) {
		super.initialiseFile(database, outputFilePath);
		// Note, TargetType.DATABASE is omitted, if included it executes statements directly to database
		EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.SCRIPT, TargetType.STDOUT);
		SchemaUpdate schemaUpdate = new SchemaUpdate();
		schemaUpdate.setDelimiter(";");
		schemaUpdate.setFormat(true);
		schemaUpdate.setHaltOnError(true);
		schemaUpdate.setOutputFile(outputFilePath);
		schemaUpdate.execute(targetTypes, metadata);
	    super.finaliseFile(outputFilePath);
	}
}
