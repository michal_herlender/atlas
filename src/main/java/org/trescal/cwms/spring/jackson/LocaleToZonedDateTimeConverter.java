package org.trescal.cwms.spring.jackson;


import com.fasterxml.jackson.databind.util.StdConverter;
import org.springframework.context.i18n.LocaleContextHolder;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class LocaleToZonedDateTimeConverter extends StdConverter<LocalDateTime, ZonedDateTime> {
    @Override
    public ZonedDateTime convert(LocalDateTime value) {
        return value.atZone(LocaleContextHolder.getTimeZone().toZoneId());
    }
}
