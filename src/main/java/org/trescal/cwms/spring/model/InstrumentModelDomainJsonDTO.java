package org.trescal.cwms.spring.model;

public class InstrumentModelDomainJsonDTO {
	
	public void setActive(String active) {
		this.active = active;
	}

	private Integer domainid;

	private String name;

	private String active;

	private Integer tmlid;
	
	public InstrumentModelDomainJsonDTO(Integer domainid,String name,Boolean active,Integer tmlid) {
		this.domainid = domainid;
		this.name = name;
		if(active) {
			this.active = "Yes";
		} else {
			this.active = "No";
		}
		this.tmlid = tmlid;
	}

	public Integer getDomainid() {
		return domainid;
	}

	public String getName() {
		return name;
	}

	public String getActive() {
		return active;
	}

	public Integer getTmlid() {
		return tmlid;
	}
	

}
