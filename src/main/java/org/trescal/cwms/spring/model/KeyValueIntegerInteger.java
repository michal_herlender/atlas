package org.trescal.cwms.spring.model;

import lombok.NonNull;

/**
 * Subclass of KeyValue<Integer,String> allowing direct use in JPA query results
 * (as generic projection results not supported)
 */
public class KeyValueIntegerInteger extends KeyValue<Integer, Integer> {

	public KeyValueIntegerInteger() {
		super();
	}

	public KeyValueIntegerInteger(@NonNull Integer key, @NonNull Integer value) {
		super(key, value);
	}
	
	/**
	 * jQuery autocomplete needs id
	 * @return key
	 */
	public Integer getId() {
		return getKey();
	}
}