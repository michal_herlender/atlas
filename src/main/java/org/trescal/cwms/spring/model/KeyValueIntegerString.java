package org.trescal.cwms.spring.model;

import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

import lombok.NonNull;

/**
 * Subclass of KeyValue<Integer,String> allowing direct use in JPA query results
 * (as generic projection results not supported)
 */
public class KeyValueIntegerString extends KeyValue<Integer, String> {

	public KeyValueIntegerString() {
		super();
	}
	
/*	public KeyValueIntegerString(Invoice invoice, String creditNoteNumber) {
		super(invoice.getId(), creditNoteNumber);
	}*/

	public KeyValueIntegerString(@NonNull Integer key, @NonNull String value) {
		super(key, value);
	}
	
	/**
	 * jQuery autocomplete needs id
	 * @return key
	 */
	public Integer getId() {
		return getKey();
	}
	
	
}