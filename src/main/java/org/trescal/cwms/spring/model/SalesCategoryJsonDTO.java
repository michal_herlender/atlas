package org.trescal.cwms.spring.model;

public class SalesCategoryJsonDTO {
	private String id;
	private String name;
	private String active;
			
	public SalesCategoryJsonDTO(Integer id, String name, Boolean active) {
		this.id = id.toString();
		this.name = name;
		this.active = (active) == true ? "Yes" : "No";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	
}
