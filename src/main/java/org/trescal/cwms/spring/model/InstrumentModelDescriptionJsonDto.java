package org.trescal.cwms.spring.model;

public class InstrumentModelDescriptionJsonDto {
	
	private Integer id;
	private Boolean active;
	private String description;
	private String translation;
	private Integer tmlId;
	
	public InstrumentModelDescriptionJsonDto(Integer id, 
			Boolean active, String description, String translation, Integer tmlId) {
		this.id = id;
		this.active = active;
		this.description = description;
		this.translation = translation;
		this.tmlId = tmlId;
	}

	public Integer getId() {
		return id;
	}

	public Boolean getActive() {
		return active;
	}

	public String getDescription() {
		return description;
	}

	public String getTranslation() {
		return translation;
	}

	public Integer getTmlId() {
		return tmlId;
	}
	
	public String getBestTranslation() {
		return this.translation == null ? this.description : this.translation;
	}
}
