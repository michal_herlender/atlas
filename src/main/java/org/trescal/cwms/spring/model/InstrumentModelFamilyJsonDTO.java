package org.trescal.cwms.spring.model;

public class InstrumentModelFamilyJsonDTO {
	private Integer familyid;

	private String name;

	private String active;

	private Integer tmlid;
	
	public InstrumentModelFamilyJsonDTO(Integer familyid,String name,Boolean active,Integer tmlid) {
		this.familyid = familyid;
		this.name = name;
		if(active) {
			this.active = "Yes";
		} else {
			this.active = "No";
		}
		this.tmlid = tmlid;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Integer getFamilyid() {
		return familyid;
	}

	public String getName() {
		return name;
	}

	public String getActive() {
		return active;
	}

	public Integer getTmlid() {
		return tmlid;
	}
	
}
