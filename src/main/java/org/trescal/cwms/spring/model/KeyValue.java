package org.trescal.cwms.spring.model;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/*
 * Generic tuple pair intended primarily for marshalling / storing simple values in application
 * Tuple pair implements comparable in order to 
 * Comparison is done first by value, and then by key  
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValue<K extends Comparable<K>, V extends Comparable<V>> implements Comparable<KeyValue<K, V>> {

	@NonNull
	@NotNull(message = "Null key not allowed")
	private K key;
	@NonNull
	@NotNull(message = "Null value not allowed")
	private V value;

	@NonNull
	@Override
	public String toString() {
		return key.toString() + ": " + value.toString();
	}

	@Override
	public int compareTo(KeyValue<K, V> other) {
		// Compare first by values, then by keys
		int valueResult = this.value.compareTo(other.value);
		if (valueResult != 0)
			return valueResult;
		return this.key.compareTo(other.key);
	}

	@Override
	public int hashCode() {
		// Derive hashcode from key and value
		int result = value.hashCode();
		result += key.hashCode() * 31;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		try {
			@SuppressWarnings("unchecked")
			KeyValue<K, V> otherKeyValue = (KeyValue<K, V>) obj;
			return this.key.equals(otherKeyValue.key) && this.value.equals(otherKeyValue.value);
		} catch (ClassCastException ex) {
			return false;
		}
	}
}