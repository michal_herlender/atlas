package org.trescal.cwms.spring.model;

/**
 * Enum for transferring null boolean values as a non-null enum.
 * 
 * Specifically used for KeyValue implementations where the "key" cannot be null. 
 * 
 * No messages, translations, etc... are implemented here, as they should be managed per use case.
 *  
 * @author galen
 *
 */
public enum TriState {
	UNDEFINED(null), TRUE(Boolean.TRUE), FALSE(Boolean.FALSE);
	
	private Boolean booleanValue;
	
	private TriState(Boolean booleanValue) {
		this.booleanValue = booleanValue;
	}
	
	public Boolean getBooleanValue() {
		return booleanValue;
	}
}
