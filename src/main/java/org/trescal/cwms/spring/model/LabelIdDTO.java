package org.trescal.cwms.spring.model;

import lombok.Value;

/*
 * Generic label / id model data transfer object for use in marshalling key/id pairs to UI components
 * Intended first for use with JQuery components but expected to use in other application areas too.
 * GB 2015-06-05
 */
@Value
public class LabelIdDTO {
	String label;
	Integer id;
}
