package org.trescal.cwms.spring.model;

public class ImportModelJsonDTO {
	private String family;

	private String manufacturer;

	private String model;
	
	public ImportModelJsonDTO(String family,String manufacturer,String model) {
		this.family = family;
		this.manufacturer = manufacturer;
		this.model = model;
	}

	public String getFamily() {
		return family;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getModel() {
		return model;
	}
}
