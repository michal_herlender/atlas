package org.trescal.cwms.spring.helpers;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RequestMatcher;

/***
 * a naive copy of the original @see org.springframework.security.web.csrf.CsrfFilter.DefaultRequiresCsrfMatcher
 * which we cannot use directly because of it is private.
 * The main reason why we want to use it, is to create a minimal feature split which allow us to safely introduce CSRF protection.
 * Once refactor will be done we will need the feature split (and this class) any longer and it should be deleted.
 **/
public class DefaultRequiresCsrfMatcher implements RequestMatcher {
    private final Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.security.web.util.matcher.RequestMatcher#matches(javax.
     * servlet.http.HttpServletRequest)
     */
    public boolean matches(HttpServletRequest request) {
        return !allowedMethods.matcher(request.getMethod()).matches();
    }
}
