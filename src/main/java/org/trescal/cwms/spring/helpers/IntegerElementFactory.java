package org.trescal.cwms.spring.helpers;

import org.springframework.util.AutoPopulatingList;
import org.springframework.util.AutoPopulatingList.ElementInstantiationException;

/**
 * Necessary for use in AutoPopulatingLists containing integers as there's no Integer() constructor.
 * The defaultValue may be null if required.
 * @author Galen
 *
 */
public class IntegerElementFactory implements AutoPopulatingList.ElementFactory<Integer> {
	
	private Integer defaultValue;
	
	public IntegerElementFactory(Integer defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Override
	public Integer createElement(int arg0) throws ElementInstantiationException {
		return this.defaultValue;
	}

}
