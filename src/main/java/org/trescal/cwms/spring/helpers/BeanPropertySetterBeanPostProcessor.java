package org.trescal.cwms.spring.helpers;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * http://forum.springframework.org/showthread.php?t=35406&highlight=
 * BeanFactoryPostProcessor+circular
 * 
 * @author Richard
 */
@SuppressWarnings("rawtypes")
public class BeanPropertySetterBeanPostProcessor implements BeanFactoryPostProcessor, BeanPostProcessor
{
	private Map config = null;

	private ConfigurableListableBeanFactory factory = null;
	Log log = LogFactory.getLog(BeanPropertySetterBeanPostProcessor.class);

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException
	{
		// log.debug("after: "+bean+", "+beanName);
		// Now set the dependency
		if (this.config == null)
		{
			return bean;
		}
		Map beanConfig = (Map) this.config.get(beanName);
		if (beanConfig != null)
		{
			BeanInfo beanInfo = null;
			try
			{
				beanInfo = Introspector.getBeanInfo(bean.getClass());
				PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
				if (propertyDescriptors != null)
				{
					for (PropertyDescriptor propertyDescriptor : propertyDescriptors)
					{
						String propertyName = propertyDescriptor.getName();
						if (beanConfig.get(propertyName) != null)
						{
							String beanId = (String) beanConfig.get(propertyName);
							if (this.log.isDebugEnabled())
							{
								this.log.debug("Found propertyDescriptor for "
										+ beanName + ": " + propertyName);
							}
							try
							{
								Object targetBean = this.factory.getBean(beanId);
								Method setter = propertyDescriptor.getWriteMethod();
								setter.invoke(bean, targetBean);
							}
							catch (NoSuchBeanDefinitionException nsbde)
							{
								this.log.error("Bean not found", nsbde);
							}
							catch (IllegalAccessException e)
							{
								this.log.error("IllegalAccessException", e);
							}
							catch (InvocationTargetException e)
							{
								this.log.error("InvocationTargetException", e);
							}
						}
					}
				}
			}
			catch (IntrospectionException ie)
			{
				this.log.error("IntrospectionException", ie);
			}
		}
		return bean;
	}

	public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException
	{
		this.factory = factory;
	}

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException
	{
		// log.debug("before: "+bean+", "+beanName);
		return bean;
	}

	public void setConfig(Map config)
	{
		this.config = config;
	}
}
