package org.trescal.cwms.spring.helpers;

import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

@Component
public class CsrfSecurityRequestMatcher implements RequestMatcher {
	
	private final Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
	private final RegexRequestMatcher protectedMatcher = new RegexRequestMatcher(String.join("|",
			"/login",
			"/logout",
			"/addcourier.htm",
			"/viewperson.htm",
			"/addperson.htm",
			"/jobsearch.htm",
			"/addlogin.htm",
			"/web/myprofile.htm"), null);
	
	@Override
	public boolean matches(HttpServletRequest request) {
		if(allowedMethods.matcher(request.getMethod()).matches()) return false;
		return protectedMatcher.matches(request);
	}
}