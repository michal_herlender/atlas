package org.trescal.cwms.spring.oauth2;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.login.entity.oauth2.OAuth2Converter;
import org.trescal.cwms.core.login.entity.oauth2.accesstoken.PersistentAccessToken;
import org.trescal.cwms.core.login.entity.oauth2.accesstoken.db.PersistentAccessTokenService;
import org.trescal.cwms.core.login.entity.oauth2.refreshtoken.PersistentRefreshToken;
import org.trescal.cwms.core.login.entity.oauth2.refreshtoken.db.PersistentRefreshTokenService;

import lombok.extern.slf4j.Slf4j;

/**
 * Created - 2019-09-17 :
 * Spring OAuth2 token store implementation for Atlas
 * Connects to token storage in database via JPA, to allow shared authentication
 * <p>
 * Based on following article / example :
 * https://blog.couchbase.com/custom-token-store-spring-securtiy-oauth2/
 */
@Component
@Slf4j
public class AtlasTokenStore implements TokenStore {

	@Autowired
	private PersistentAccessTokenService accessTokenService;

	@Autowired
	private PersistentRefreshTokenService refreshTokenService;

	private final AuthenticationKeyGenerator authenticationKeyGenerator;


	public AtlasTokenStore() {
		// 2021-07-07 : Changed from DefaultAuthenticationKeyGenerator to generate unique tokens per request
		authenticationKeyGenerator = new UniqueAuthenticationKeyGenerator();
	}

	@Override
	public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
		if (token == null) throw new IllegalArgumentException("token was null");
		return readAuthentication(token.getValue());
	}

	@Override
	public OAuth2Authentication readAuthentication(String token) {
		if (token == null) throw new IllegalArgumentException("token was null");

		String tokenId = extractTokenKey(token);
		if (log.isTraceEnabled())
			log.trace("readAuthentication : tokenId = " + tokenId);
		PersistentAccessToken accessToken = this.accessTokenService.findByTokenId(tokenId);
		OAuth2Authentication result = null;
		if (accessToken != null) {
			result = OAuth2Converter.deserializeOAuth2Authentication(accessToken.getSerializedAuthentication());
			log.trace("readAuthentication : deserialized authentication");
		} else {
			log.trace("readAuthentication : no token found");
		}
		return result;
	}

	@Override
	public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
		if (token == null) throw new IllegalArgumentException("token was null");
		if (authentication == null) throw new IllegalArgumentException("authentication was null");

		String refreshToken = null;
		if (token.getRefreshToken() != null) {
			refreshToken = token.getRefreshToken().getValue();
		}

		String serializedAuthentication = OAuth2Converter.serialize(authentication);
		String serializedToken = OAuth2Converter.serialize(token);

		log.trace("storeAccessToken : serializedAuthentication.length = " + serializedAuthentication.length());
		log.trace("storeAccessToken : serializedToken.length = " + serializedToken.length());

		String tokenId = extractTokenKey(token.getValue());
		PersistentAccessToken persistentToken = this.accessTokenService.findByTokenId(tokenId);
		boolean newToken = (persistentToken == null);
		if (newToken) {
			log.trace("storeAccessToken : no existing token");
			persistentToken = new PersistentAccessToken();
		} else {
			log.trace("storeAccessToken : updating existing token");
		}
		String authenticationId = this.authenticationKeyGenerator.extractKey(authentication);
		
		persistentToken.setAuthenticationId(authenticationId);
		persistentToken.setClientId(authentication.getOAuth2Request().getClientId());
		persistentToken.setRefreshToken(extractTokenKey(refreshToken));
		persistentToken.setSerializedAuthentication(serializedAuthentication);
		persistentToken.setSerializedToken(serializedToken);
		persistentToken.setTokenId(tokenId);
		persistentToken.setUsername(authentication.isClientOnly() ? null : authentication.getName());

		if (newToken) {
			this.accessTokenService.save(persistentToken);
			log.trace("storeAccessToken : saved new token");
		}
	}

	@Override
	public OAuth2AccessToken readAccessToken(String tokenValue) {
		if (tokenValue == null) throw new IllegalArgumentException("tokenValue was null");
		String tokenId = extractTokenKey(tokenValue);
		if (log.isTraceEnabled())
			log.trace("readAccessToken : tokenId = " + tokenId);

		PersistentAccessToken persistentToken = this.accessTokenService.findByTokenId(tokenId);
		OAuth2AccessToken result = null;
		if (persistentToken != null) {
			String serializedToken = persistentToken.getSerializedToken();
			result = OAuth2Converter.deserializeOAuth2AccessToken(serializedToken);
			log.trace("readAccessToken : deserialized token");
		} else {
			log.trace("readAccessToken : no token found");
		}
		
		return result;
	}

	@Override
	public void removeAccessToken(OAuth2AccessToken token) {
		if (token == null) throw new IllegalArgumentException("token was null");
		String tokenId = extractTokenKey(token.getValue());
		if (log.isTraceEnabled())
			log.trace("removeAccessToken : tokenId = " + tokenId);

		PersistentAccessToken persistentToken = this.accessTokenService.findByTokenId(tokenId);
		if (persistentToken != null) {
			this.accessTokenService.delete(persistentToken);
			log.trace("removeAccessToken : deleted token");
		} else {
			log.trace("removeAccessToken : no token found");
		}
	}

	@Override
	public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
		if (refreshToken == null) throw new IllegalArgumentException("refreshToken was null");

		String serializedAuthentication = OAuth2Converter.serialize(authentication);
		String serializedToken = OAuth2Converter.serialize(refreshToken);
		String tokenId = extractTokenKey(refreshToken.getValue());
		if (log.isTraceEnabled())
			log.trace("storeRefreshToken : tokenId = " + tokenId);

		PersistentRefreshToken persistentToken = new PersistentRefreshToken();
		persistentToken.setSerializedAuthentication(serializedAuthentication);
		persistentToken.setSerializedToken(serializedToken);
		persistentToken.setTokenId(tokenId);

		this.refreshTokenService.save(persistentToken);
	}

	@Override
	public OAuth2RefreshToken readRefreshToken(String tokenValue) {
		if (tokenValue == null) throw new IllegalArgumentException("tokenValue was null");
		String tokenId = extractTokenKey(tokenValue);
		log.trace("readRefreshToken : tokenId = " + tokenId);
		PersistentRefreshToken persistentToken = this.refreshTokenService.findByTokenId(tokenId);
		OAuth2RefreshToken result = null;
		if (persistentToken != null) {
			String serializedToken = persistentToken.getSerializedToken();
			result = OAuth2Converter.deserializeOAuth2RefreshToken(serializedToken);
			log.trace("readRefreshToken : deserialized token");
		} else {
			log.trace("readRefreshToken : no token found");
		}
		
		return result;
	}

	@Override
	public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
		if (token == null) throw new IllegalArgumentException("token was null");

		String tokenId = extractTokenKey(token.getValue());
		if (log.isTraceEnabled())
			log.trace("readAuthenticationForRefreshToken : tokenId = " + tokenId);
		PersistentRefreshToken refreshToken = this.refreshTokenService.findByTokenId(tokenId);
		OAuth2Authentication result = null;
		if (refreshToken != null) {
			result = OAuth2Converter.deserializeOAuth2Authentication(refreshToken.getSerializedAuthentication());
			log.trace("readAuthenticationForRefreshToken : deserialized authentication");
		} else {
			log.trace("readAuthenticationForRefreshToken : no token found");
		}
		return result;
	}
	
	@Override
	public void removeRefreshToken(OAuth2RefreshToken token) {
		if (token == null) throw new IllegalArgumentException("token was null");
		String tokenId = extractTokenKey(token.getValue());
		log.trace("removeRefreshToken : tokenId = " + tokenId);
		PersistentRefreshToken persistentToken = this.refreshTokenService.findByTokenId(tokenId);
		if (persistentToken != null) {
			this.refreshTokenService.delete(persistentToken);
			log.trace("removeRefreshToken : deleted token");
		} else {
			log.trace("removeRefreshToken : no token found");
		}
	}

	@Override
	public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
		if (refreshToken == null) throw new IllegalArgumentException("token was null");
		String tokenId = extractTokenKey(refreshToken.getValue());
		log.trace("removeAccessTokenUsingRefreshToken : tokenId = " + tokenId);
		PersistentAccessToken persistentToken = this.accessTokenService.findByRefreshTokenId(tokenId);
		if (persistentToken != null) {
			this.accessTokenService.delete(persistentToken);
			log.trace("removeAccessTokenUsingRefreshToken : deleted token");
		} else {
			log.trace("removeAccessTokenUsingRefreshToken : no token found");
		}
	}

	@Override
	public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
        OAuth2AccessToken accessToken = null;
        String authenticationId = authenticationKeyGenerator.extractKey(authentication);
        PersistentAccessToken persistentToken = accessTokenService.findByAuthenticationId(authenticationId);
        
        if (persistentToken != null) {
			log.trace("getAccessToken : token found");
			accessToken = OAuth2Converter.deserializeOAuth2AccessToken(persistentToken.getSerializedToken());
			OAuth2Authentication persistedAuthentication = this.readAuthentication(accessToken);
			String persistedAuthenticationId = this.authenticationKeyGenerator.extractKey(persistedAuthentication);
			if ((accessToken != null) && !authenticationId.equals(persistedAuthenticationId)) {
				this.removeAccessToken(accessToken);
				this.storeAccessToken(accessToken, authentication);
			}
		}
        else {
			log.trace("getAccessToken : no token found");
		}
        
		return accessToken;
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {
		List<PersistentAccessToken> persistedTokens = this.accessTokenService.findByClientIdAndUserName(clientId, userName);
		log.trace("findTokensByClientIdAndUserName : " + persistedTokens.size() + " tokens found for client id " + clientId + " and user name " + userName);
		return convertResultList(persistedTokens);
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
		List<PersistentAccessToken> persistedTokens = this.accessTokenService.findByClientId(clientId);
		log.trace("findTokensByClientId : " + persistedTokens.size() + " tokens found for client id " + clientId);
		return convertResultList(persistedTokens);
	}

	private List<OAuth2AccessToken> convertResultList(List<PersistentAccessToken> persistedTokens) {
		return persistedTokens.stream()
			.map(token -> OAuth2Converter.deserializeOAuth2AccessToken(token.getSerializedToken()))
			.collect(Collectors.toList());
	}
	
    private String extractTokenKey(String value) {
        if(value == null) {
            return null;
        } else {
			MessageDigest digest;
			try {
				digest = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException var5) {
				throw new IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).");
			}

			byte[] e = digest.digest(value.getBytes(StandardCharsets.UTF_8));
			return String.format("%032x", new BigInteger(1, e));
		}
    }	
}
