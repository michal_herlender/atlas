package org.trescal.cwms.hibernateextension;

import org.hibernate.dialect.SQLServer2012Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LocalDateType;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.StringType;

public class TrescalSQLServerDialect extends SQLServer2012Dialect {

	public TrescalSQLServerDialect() {
		super();
		registerFunction("GetNthCalDate", new StandardSQLFunction("dbo.GetNthCalDate", new DateType()));
		registerFunction("GetMostRecentCertificate",
				new StandardSQLFunction("dbo.GetMostRecentCertificate", new IntegerType()));
		registerFunction("getDeliveryDateFromLastCreatedClientDelivery",
				new StandardSQLFunction("dbo.getDeliveryDateFromLastCreatedClientDelivery", new LocalDateType()));
		registerFunction("string_agg", new SQLFunctionTemplate(StandardBasicTypes.STRING, "string_agg(?1, ?2)"));
		registerFunction("castToVarChar", new SQLFunctionTemplate(StandardBasicTypes.STRING, "convert(varchar, ?1)"));
		registerFunction("castToNVarChar", new SQLFunctionTemplate(StandardBasicTypes.STRING, "convert(nvarchar, ?1)"));
		registerFunction("simplifySerialNo", new StandardSQLFunction("dbo.simplifySerialNo", new StringType()));
	}
}