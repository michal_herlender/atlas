package org.trescal.cwms.hibernateextension;

import java.util.BitSet;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class BitSetJPAConverter implements AttributeConverter<BitSet, Integer> {

	@Override
	public Integer convertToDatabaseColumn(BitSet attribute) {
		if (attribute == null)
			return null;
		else {
			Integer result = 0;
			Integer power = 1;
			for (Integer exp = 0; exp < attribute.length(); exp++) {
				if (attribute.get(exp))
					result += power;
				power *= 2;
			}
			return result;
		}
	}

	@Override
	public BitSet convertToEntityAttribute(Integer dbData) {
		if (dbData == null)
			return null;
		else {
			BitSet result = new BitSet();
			Integer index = 0;
			Integer rest = dbData;
			while (rest > 0) {
				if (rest % 2 == 1)
					result.set(index);
				rest /= 2;
				index += 1;
			}
			return result;
		}
	}
}