package org.trescal.cwms.websocket;

public class PrintMessage {
	private String trackId;
	private byte[] fileContent;
	private int personId;
	private String printerDesc;
	private String template;

	public static enum Status {
		OFFER, ACCEPTED, TRANSFER, RECEIVED
	};

	private Status status;
		
	public String getTrackId() {
		return trackId;
	}

	public void setTrackId(String trackId) {
		this.trackId = trackId;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getPrinterDesc() {
		return printerDesc;
	}

	public void setPrinterDesc(String printerPath) {
		this.printerDesc = printerPath;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
