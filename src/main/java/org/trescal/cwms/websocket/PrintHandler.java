package org.trescal.cwms.websocket;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.IOUtils;
import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.AtmosphereRequest;
import org.atmosphere.cpr.AtmosphereRequestImpl.Body;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.MetaBroadcaster;
import org.atmosphere.handler.AbstractReflectorAtmosphereHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.websocket.PrintMessage.Status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PrintHandler extends AbstractReflectorAtmosphereHandler implements ApplicationContextAware {
	private static final Logger LOG = LoggerFactory.getLogger(PrintHandler.class);

	private Map<String, PrintElement> messages = new ConcurrentHashMap<>();

	public static final String BASE_PATH = "/websocket/print/";

	private MetaBroadcaster metaBroadcaster;

	private ApplicationContext applicationContext;

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public void onRequest(AtmosphereResource resource) throws IOException {
		LOG.info("onRequest {}", resource);

		AtmosphereRequest req = resource.getRequest();
		Body body = req.body();
		if (body.isEmpty()) {
			AtmosphereFramework af = applicationContext.getBean(AtmosphereFramework.class);
			BroadcasterFactory bFactory = af.getBroadcasterFactory();
			String key = req.getRequestURI().substring(req.getContextPath().length());
			Broadcaster broadcaster = bFactory.lookup(key, true);
			resource.suspend();
			broadcaster.addAtmosphereResource(resource);
		} else {
			PrintMessage incoming = mapper.readValue(body.asString(), PrintMessage.class);
			PrintElement pe = messages.get(incoming.getTrackId());

			if (pe != null) {
				switch (incoming.getStatus()) {
				case ACCEPTED:
					pe.lastAttempt = System.currentTimeMillis();
					incoming.setStatus(Status.TRANSFER);
					try (InputStream is = new FileInputStream(pe.file)) {
						incoming.setFileContent(IOUtils.toByteArray(is));
						resource.write(mapper.writeValueAsString(incoming));
					} catch (IOException e) {
						// hmmm, maybe the file was handled by another
						// server
					}

					break;
				case RECEIVED:
					pe.file.delete();
					messages.remove(incoming.getTrackId());
					break;
				default:
					LOG.error("Invalid status received {} for {}", incoming.getStatus(), incoming.getTrackId());
					break;
				}
			}
		}
	}

	@Override
	public void onStateChange(AtmosphereResourceEvent event) throws IOException {
		super.onStateChange(event);

		LOG.info("onStateChange {}", event);
	}

	public void print(LabelPrinter printer, File f) {
		PrintMessage m = new PrintMessage();
		String uuid = UUID.randomUUID().toString();
		m.setStatus(Status.OFFER);
		m.setPrinterDesc(printer.getDescription());
		m.setTrackId(uuid);

		PrintElement pe = new PrintElement();
		pe.message = m;
		pe.file = f;
		pe.addrid = printer.getAddr().getAddrid();
		pe.firstAttempt = System.currentTimeMillis();
		pe.lastAttempt = pe.firstAttempt;

		messages.put(uuid, pe);

		if (checkMetaBroadcaster()) {
			// the DefaultMetaBroadcaster is not thread-safe... joy.
			synchronized (metaBroadcaster) {
				String target = BASE_PATH + pe.addrid;
				try {
					metaBroadcaster.broadcastTo(target, mapper.writeValueAsString(m));
				} catch (JsonProcessingException e) {
					LOG.error("Should not happen", e);
				}
			}
		}
	}

	/**
	 * Can't be directly injected.
	 */
	private boolean checkMetaBroadcaster() {
		if (metaBroadcaster == null) {
			// we don't really care about locking
			AtmosphereFramework af = applicationContext.getBean(AtmosphereFramework.class);
			if (af.initialized()) {
				// initialized when at least one call has been performed
				metaBroadcaster = af.metaBroadcaster();
			}
		}
		return metaBroadcaster != null;
	}

	private class PrintElement {
		int addrid;
		PrintMessage message;
		File file;
		long firstAttempt;
		long lastAttempt;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
