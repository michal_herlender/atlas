package org.trescal.cwms.web.home.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class JobStats
{
	private Long compItems;
	private Long compJobs;
	private Long locItems;
	private Long locJobs;
	private Long persItems;
	private Long persJobs;
	private Long subItems;
	private Long subJobs;

}