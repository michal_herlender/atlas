package org.trescal.cwms.web.home.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.UserScope;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db.UserGroupPropertyValueService;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@ExtranetController
public class WebHomeController {
	@Autowired
	protected SessionUtilsService sessionServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private ScheduleService schServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private UserGroupPropertyValueService userGroupPropertyValueService;

	@RequestMapping(value="/home.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	{
		Map<String, Object> refData = new HashMap<String, Object>();

		Contact contact = this.sessionServ.getCurrentContact();
		UserScope userScope = this.userGroupPropertyValueService.getValue(contact, UserGroupProperty.VIEW_LEVEL);
		
		if(userScope.equals(UserScope.NONE)){
			return new ModelAndView("redirect:/web/certsearch.htm");
		}
		
		Integer subdivid = contact.getSub().getSubdivid();
		Integer addrid = contact.getDefAddress().getAddrid();
		Integer personid = contact.getPersonid();

		
		// get date minus one month for quotation retrieval
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Date date = cal.getTime();

		// get job items requiring attention by the current contact
		refData.put("itemsReqActions", this.jiServ.getContactItemsOnJobInStateGroup(contact.getPersonid(), StateGroup.AWAITINGDECISION));
		// get any collections requested by the current contact
		refData.put("requestedCollections", this.schServ.getSchedulesForContact(contact.getPersonid(), false, ScheduleType.COLLECTION));
		// get any quotations requested by the current contact
		refData.put("requestedQuotations", this.quoteServ.getQuotationsRequestForContactFromReqdate(contact.getPersonid(), DateTools.dateToLocalDate(date), false, false));
		// get job stats
		refData.put("jobStats", this.jobServ.getJobStats(contact.getPersonid(), contact.getSub().getSubdivid(), contact.getSub().getComp().getCoid(), contact.getDefAddress().getAddrid()));
		// get items due to calibration
		refData.put("itemsDueToCalActions", this.instServ.webSearchInstrumentsDueForCalibration(null, LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()), contact.getSub().getComp().getCoid(), subdivid, addrid, personid));
		return new ModelAndView("/home/home", refData);
	}
}