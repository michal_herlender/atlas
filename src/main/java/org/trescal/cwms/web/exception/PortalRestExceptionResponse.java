package org.trescal.cwms.web.exception;

import java.util.Date;

public class PortalRestExceptionResponse {

    private Date timestamp;
    private String message;
    private String details;

    public PortalRestExceptionResponse(Date timestamp, String message, String details) {
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }
}
