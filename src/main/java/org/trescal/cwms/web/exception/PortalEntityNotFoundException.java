package org.trescal.cwms.web.exception;

public class PortalEntityNotFoundException extends RuntimeException {
    public PortalEntityNotFoundException(String message) {
        super(message);
    }
}
