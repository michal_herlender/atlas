package org.trescal.cwms.web.exception;

import java.util.Arrays;
import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.trescal.cwms.core.exception.controller.ExtranetRestController;

@ControllerAdvice(annotations = ExtranetRestController.class)
public class PortalRestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {



    @ExceptionHandler(PortalEntityNotFoundException.class)
    public final ResponseEntity<Object> handleCertValidationNotFoundException(Exception ex, WebRequest request) {
        PortalRestExceptionResponse exceptionResponse =
                new PortalRestExceptionResponse(new Date(), ex.getMessage(),
                        request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        PortalRestExceptionResponse exceptionResponse =
                new PortalRestExceptionResponse(new Date(), ex.getMessage(),
                        ex.getBindingResult().toString());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        PortalRestExceptionResponse exceptionResponse =
                new PortalRestExceptionResponse(new Date(), ex.getCause().getMessage(), null);
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        PortalRestExceptionResponse exceptionResponse =
                new PortalRestExceptionResponse(new Date(), ex.getMessage(),
                        Arrays.toString(ex.getStackTrace()));
        return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
