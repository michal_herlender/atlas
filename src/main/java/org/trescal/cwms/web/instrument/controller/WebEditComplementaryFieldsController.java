package org.trescal.cwms.web.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.WebExternalPermissionException;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.db.InstrumentComplementaryFieldService;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;

import java.util.Date;

@Controller @ExtranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME,Constants.SESSION_ATTRIBUTE_USER_READ_RULE})
@RequestMapping("webeditcomplementaryfields.htm")
public class WebEditComplementaryFieldsController {

    @Autowired
    private InstrumentComplementaryFieldService icfService;
    @Autowired
    private InstrumService instrumService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserGroupService userGroupService;

    @InitBinder("command")
    protected void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(Date.class, new MyCustomDateEditor(DateTools.df, true));
    }

    @ModelAttribute("command")
    protected InstrumentComplementaryField findComplementaryField(@RequestParam(value="plantid") Integer plantId) {

        if(icfService.getComplementaryFieldByPlantid(plantId) != null){
            return icfService.getComplementaryFieldByPlantid(plantId);
        } else{
            InstrumentComplementaryField icf = new InstrumentComplementaryField();
            icf.setInstrument(instrumService.get(plantId));
            return icf;
        }
    }

    @RequestMapping(method=RequestMethod.GET)
    public String displayForm(@RequestParam(value="plantid", required=false, defaultValue="0") Integer plantId,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {

        if(plantId > 0){
            Instrument instrument = this.instrumService.get(plantId);
            Contact userContact = this.userService.get(username).getCon();
            if(!userGroupService.canUserUpdateThisInstrument(userContact, instrument)){
                throw new WebExternalPermissionException(userContact.getPersonid(), userContact.getName(), Instrument.class, "Instrument");
            }
        }
        return "/instrument/webeditcomplementaryfields";
    }

    @RequestMapping(method=RequestMethod.POST)
    public String processUpdate(@Validated @ModelAttribute("command") InstrumentComplementaryField complementaryField, BindingResult result){

        if(result.hasErrors()){
            //result.getAllErrors().forEach(System.out::println);
            return "/instrument/webeditcomplementaryfields";
        }
        complementaryField = icfService.merge(complementaryField);
        return "redirect:viewinstrument.htm?plantid=" + complementaryField.getInstrument().getPlantid();
    }
}