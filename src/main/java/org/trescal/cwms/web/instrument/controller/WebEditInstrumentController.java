package org.trescal.cwms.web.instrument.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.WebExternalPermissionException;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.db.InstrumentComplementaryFieldService;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.db.InstrumentStorageTypeService;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.db.InstrumentUsageTypeService;
import org.trescal.cwms.core.instrument.form.EditInstrumentForm;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CsrEmails;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.web.instrument.dto.WebUpdateInstrumentDTO;
import org.trescal.cwms.web.instrument.form.WebEditInstrumentValidator;

import lombok.extern.slf4j.Slf4j;

@Controller
@ExtranetController
@Slf4j
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_USER_READ_RULE})
public class WebEditInstrumentController {
    @Autowired
    private AddressService addressServ;
    @Autowired
    private ContactService conServ;
    @Autowired
    private InstrumService instServ;
    @Autowired
    private InstrumentStorageTypeService instStorServ;
    @Autowired
    private InstrumentUsageTypeService instUsageServ;
    @Autowired
    private SubdivService subdivServ;
    @Autowired
    private UserService userService;
    @Autowired
    private InstrumentModelService modelServ;
    @Autowired
    private WebEditInstrumentValidator validator;
    @Autowired
    private UserGroupService userGroupService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private CalReqService calReqService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private TemplateEngine templateEngine;
    @Autowired
    private EmailService emailService;
    @Autowired
    private InstrumentComplementaryFieldService complementaryFieldService;
    @Autowired
    private CsrEmails csrEmails;
    @Autowired
    private CalibrationVerification systemDefaultCalibrationVerification;

    @Value("#{props['cwms.config.instrument.defaultcalfrequency']}")
    private Integer defaultCalFrequency;

    @InitBinder("command")
    protected void initBinder(WebDataBinder binder) throws Exception {
        MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
        binder.registerCustomEditor(Date.class, editor);
        binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
        binder.setValidator(validator);
    }

    @ModelAttribute("contact")
    public Contact initializeContact(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        return this.userService.get(username).getCon();
    }

    @ModelAttribute("storage")
    public List<InstrumentStorageType> initializeStorageTypes() {
        return this.instStorServ.getAllInstrumentStorageTypes();
    }

    @ModelAttribute("usage")
    public List<InstrumentUsageType> initializeUsageTypes() {
        return this.instUsageServ.getAllInstrumentUsageTypes();
    }

    @ModelAttribute("units")
    public List<KeyValue<String, String>> initializeRecallIntervalUnits() {
        return IntervalUnit.getUnitsForSelect();
    }

    @ModelAttribute("contactDefSubdiv")
    public Integer initializeCurrentContactSubdiv(
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        return this.userService.get(username).getCon().getSub().getSubdivid();
    }

    @ModelAttribute("instrument")
    public Instrument initializeInstrument(
        @RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId,
        @RequestParam(value = "modelid", required = false, defaultValue = "0") Integer modelId) throws Exception {
        if (plantId > 0) {
            return instServ.get(plantId);
        } else {
            if (modelId == 0) {
                log.error("Attempt to create instrument with no model selected");
                throw new Exception("No model selected");
            } else {
                Instrument instrument = new Instrument();
                instrument.setModel(modelServ.findInstrumentModel(modelId));
                return instrument;
            }
        }
	}

    @ModelAttribute("statuslist")
    public InstrumentStatus[] initializeInstrumentStatusList() {
        return InstrumentStatus.values();
    }

    @ModelAttribute("subdivs")
    public List<Subdiv> initializeSubdivList(
        @RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        if (plantId > 0) {
            Instrument i = this.instServ.get(plantId);
            return subdivServ.getAllActiveCompanySubdivs(i.getComp());
        } else {
            return this.subdivServ
                .getAllActiveCompanySubdivs(this.userService.get(username).getCon().getSub().getComp());
        }
    }

    @ModelAttribute("addresses")
    public List<Address> initializeAddressList(
        @RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        if (plantId > 0) {
            Instrument i = this.instServ.get(plantId);
            return this.addressServ.getAllActiveSubdivAddresses(i.getAdd().getSub(), null);
        } else {
            return this.addressServ.getAllActiveSubdivAddresses(this.userService.get(username).getCon().getSub(), null);
        }
    }

    @ModelAttribute("locations")
    public List<Location> initializeLocationList(
        @RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        if (plantId > 0) {
            Instrument i = this.instServ.get(plantId);
            log.debug("Inst Address Id = " + i.getAdd().getAddrid());
            return this.locationService.getAllAddressLocations(i.getAdd().getAddrid(), true);
        } else {
            return this.locationService
                .getAllAddressLocations(this.userService.get(username).getCon().getDefAddress().getAddrid(), true);
        }
    }

    @ModelAttribute("contacts")
    public List<Contact> initializeContacts(
        @RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        if (plantId > 0) {
            Instrument i = this.instServ.get(plantId);
            return this.conServ.getAllSubdivContacts(i.getAdd().getSub().getSubdivid());
        } else {
            return this.conServ.getAllSubdivContacts(this.userService.get(username).getCon().getSub().getSubdivid());
        }
    }

    @ModelAttribute("verificationStatuses")
	public List<KeyValue<CalibrationVerificationStatus, String>> getCalStatuses() {
        return Arrays.stream(CalibrationVerificationStatus.values()).map(s -> new KeyValue<>(s, s.getName()))
            .collect(Collectors.toList());
    }

    @ModelAttribute("hasCalVerification")
    public Boolean isCalVerificationUsed(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        Company company = this.userService.get(username).getCon().getSub().getComp();
        return this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY, company, null);
    }

    @ModelAttribute("command")
    protected EditInstrumentForm formBackingObject(
        @RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId,
        @RequestParam(value = "modelid", required = false, defaultValue = "0") Integer modelId,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
        EditInstrumentForm form;
        User user = this.userService.get(username);

        if (plantId == 0) {
            if (modelId == 0) {
                log.error("Attempt to create instrument with no model selected");
                throw new Exception("No model selected");
            } else {
                form = new EditInstrumentForm();
                form.setLastModified(new Date());
                form.setCalFrequency(defaultCalFrequency);
                form.setCalFrequencyUnitId(IntervalUnit.MONTH.name());
                form.setTranslatedCalFrequencyUnit(IntervalUnit.MONTH.getName(defaultCalFrequency));
                form.setStatusid(InstrumentStatus.IN_CIRCULATION.ordinal());
                form.setUsageTypeId(null);// this.instUsageServ.findDefaultType(InstrumentUsageType.class).getId());
                form.setStorageTypeId(this.instStorServ.findDefaultType(InstrumentStorageType.class).getId());
                form.setModelid(modelId);
                form.setCalibrationStandard(false);
                form.setUsesSinceLastCalibration(0);
                form.setCalExpiresAfterNumberOfUses(false);
                form.setPermittedNumberOfUses(0);
                form.setHireInstrumentReq(false);
                form.setCustomerManaged(true);
                Company clientCompany = user.getCon().getSub().getComp();
                form.setCoid(clientCompany.getId());
                Company businessCompany = clientCompany.getDefaultBusinessContact().getSub().getComp();
                form.setBusinessCompanyId(businessCompany.getId());
            }
        } else {
            form = instServ.getEditInstrumentForm(plantId);
            Instrument instrument = this.instServ.get(plantId);
            if (instrument == null) {
                log.error("Requested instrument [" + plantId + "] not found");
                throw new Exception("Requested instrument not found");
            } else {
                if (modelId > 0 && instrument.getModel().getModelid() != modelId) {
                    // delete brand and model name of instrument, if the new
                    // instrument model is not generic
                    if (!instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
                        instrument.setMfr(null);
                        instrument.setModelname(null);
                    }
                }
                CalReq calReq = calReqService.findCalReqsForInstrument(instrument);
                if (calReq != null)
                    form.setCalibrationInstructions(calReq.getPublicInstructions());
            }
            form.setCalFrequencyUnitId(instrument.getCalFrequencyUnit().name());
            form.setTranslatedCalFrequencyUnit(instrument.getCalFrequencyUnit().getName(instrument.getCalFrequency()));
            if (instrument.getInstrumentComplementaryField() != null
                && instrument.getInstrumentComplementaryField().getClientRemarks() != null) {
                form.setCustomerRemarks(instrument.getInstrumentComplementaryField().getClientRemarks());
            }
            form.setBusinessCompanyId(instrument.getComp().getDefaultBusinessContact().getSub().getComp().getId());
        }
        return form;
    }

    @RequestMapping(value = "/editinstrument.htm", method = RequestMethod.POST)
    public String onSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                           @Validated @ModelAttribute("command") EditInstrumentForm form, BindingResult bindingResult)
        throws Exception {
        if (bindingResult.hasErrors())
            return "/instrument/webeditinstrument";

        WebUpdateInstrumentDTO updateDTO = null;
        // if no plantid passed then it must be a new instrument being added
        if (form.getPlantId() != null) {
            Instrument origInstrument = instServ.get(form.getPlantId());
            updateDTO = new WebUpdateInstrumentDTO(origInstrument.getNextCalDueDate(), origInstrument.getCalFrequency(),
                origInstrument.getCalFrequencyUnit());
        }
        // save the new details
        Instrument instrument = instServ.saveOrUpdate(form, this.userService.get(username).getCon());

        // update calibration instruction
        if (form.getCalibrationInstructions() != null) {
            InstrumentCalReq calibrationRequirement = calReqService.findInstrumentCalReqForInstrument(instrument);
            if ((calibrationRequirement == null) && !form.getCalibrationInstructions().isEmpty()) {
                // Only create new instructions if text entered
                calibrationRequirement = new InstrumentCalReq();
                calibrationRequirement.setInst(instrument);
                calibrationRequirement.setPublicInstructions(form.getCalibrationInstructions());
                calReqService.insertCalReq(calibrationRequirement);
            } else if (calibrationRequirement != null) {
                // Otherwise update / clear existing calibration instuctions if
                // they exist
                calibrationRequirement.setPublicInstructions(form.getCalibrationInstructions());
                calReqService.updateCalReq(calibrationRequirement);
            }
        }

        InstrumentComplementaryField complementaryFields = complementaryFieldService
            .getComplementaryFieldByPlantid(instrument.getPlantid());
        if (complementaryFields == null)
            complementaryFields = new InstrumentComplementaryField();
        if (form.getCustomerRemarks() != null) {
            complementaryFields.setClientRemarks(form.getCustomerRemarks());
        }
        complementaryFields.setCalibrationStatusOverride(form.getLastCertStatusOverride());
        complementaryFieldService.merge(complementaryFields);

        // if field changes have been made then email the CSR with the details
        if (form.getPlantId() != null && form.getNextCalDueDate() != null && instrument.getNextCalDueDate() != null
            && !isEqual(instrument, updateDTO)) {
            Contact contact = this.userService.get(username).getCon();
            sendNotificationToCSR(instrument, updateDTO, contact);
        }
        return "redirect:/web/viewinstrument.htm?plantid=" + instrument.getPlantid();
    }

    @RequestMapping(value = "/editinstrument.htm", method = RequestMethod.GET)
    protected String displayForm(@ModelAttribute("command") EditInstrumentForm form,
                                 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                 @RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId) throws Exception {
        if (plantId > 0) {
            Instrument instrument = this.instServ.get(plantId);
            Contact userContact = this.userService.get(username).getCon();
            if (!userGroupService.canUserUpdateThisInstrument(userContact, instrument)) {
                throw new WebExternalPermissionException(userContact.getPersonid(), userContact.getName(),
                    Instrument.class, "Instrument");
            }
        }

        return "/instrument/webeditinstrument";
    }

    private void sendNotificationToCSR(Instrument instrument, WebUpdateInstrumentDTO updateDTO, Contact clientContact) {
        Locale locale = LocaleContextHolder.getLocale();
        Contact csr = clientContact.getSub().getComp().getDefaultBusinessContact();
        String toAddr = csr.getEmail();
        String fromAddr = clientContact.getEmail();
        String subject = messageSource.getMessage("web.email.instupdatednotification.subject",
            new String[]{Integer.toString(instrument.getPlantid())}, locale);
        Context context = new Context(locale);
        context.setVariable("client", clientContact);
        context.setVariable("csr", csr);
        context.setVariable("updateDTO", updateDTO);
        context.setVariable("instrument", instrument);
        context.setVariable("translationService", translationService);
        String body = templateEngine.process("email/portal/instrument_updated", context);
        log.debug(body);
        if (fromAddr != null && toAddr != null
            && this.csrEmails.getValueByScope(Scope.COMPANY, instrument.getComp(), null))
            emailService.sendBasicEmail(subject, fromAddr, toAddr, body);
    }

    private Boolean isEqual(Instrument instrument, WebUpdateInstrumentDTO dto) {
        if (dto == null)
            return false;
        return (instrument.getNextCalDueDate().equals(dto.getNextCalDueDate())
            && instrument.getCalFrequency().equals(dto.getCalFrequency())
            && instrument.getCalFrequencyUnit().equals(dto.getCalFrequencyUnit()));
    }
}
