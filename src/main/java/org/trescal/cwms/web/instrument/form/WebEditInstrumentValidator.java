package org.trescal.cwms.web.instrument.form;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.db.InstrumentStorageTypeService;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.db.InstrumentUsageTypeService;
import org.trescal.cwms.core.instrument.form.EditInstrumentForm;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutPlantNo;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutSerialNo;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class WebEditInstrumentValidator extends AbstractBeanValidator {
	/** Logger for this class and subclasses */
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
    private InstrumentUsageTypeService instrumentUsageTypeService;

    @Autowired
    private InstrumentStorageTypeService instrumentStorageTypeService;

    @Autowired
    private InstrumService instrumentService;

    @Autowired
    private AllowInstrumentCreationWithoutPlantNo allowEmptyPlantNo;

    @Autowired
    private AllowInstrumentCreationWithoutSerialNo allowInstrumentCreationWithoutSerialNo;

    @Autowired
    private InstrumentUsageTypeService instUsageTypeService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(EditInstrumentForm.class);
    }

    @Override
	public void validate(Object obj, Errors errors) {
        super.validate(obj, errors);
        EditInstrumentForm aif = (EditInstrumentForm) obj;

        // We treat new instruments created by customer as "customer managed"
        boolean customerManaged = true;
        if (aif.getPlantId() != null && aif.getPlantId() != 0) {
            customerManaged = instrumentService.get(aif.getPlantId()).getCustomerManaged();
        }

        if (!allowInstrumentCreationWithoutSerialNo.getValueByScope(Scope.COMPANY, aif.getBusinessCompanyId(),
            aif.getBusinessCompanyId()) && StringUtils.isBlank(aif.getSerialNo()))
            errors.rejectValue("serialNo", "error.instrument.serialno");

        if (!allowEmptyPlantNo.getValueByScope(Scope.COMPANY, aif.getBusinessCompanyId(),
            aif.getBusinessCompanyId())) {
            if (aif.getPlantNo() == null || aif.getPlantNo().trim().isEmpty()) {
                errors.rejectValue("plantNo", "error.instrument.plantno", null,
                    "A Plant no must be specified for the instrument");
            }
        }
        if ((aif.getNextCalDueDate() == null) && (!aif.getCalExpiresAfterNumberOfUses()) && (customerManaged)
            && (InstrumentStatus.values()[aif.getStatusid()]).equals(InstrumentStatus.IN_CIRCULATION)
            && (instrumentUsageTypeService.findInstrumentUsageType(aif.getUsageTypeId()).getRecall())
            && (instrumentStorageTypeService.findInstrumentStorageType(aif.getStorageTypeId())
            .getRecall())) {
            errors.rejectValue("nextCalDueDate", "", null, "Please assign a calibration due date");
        }
        if ((aif.getSerialNo() == null) || aif.getSerialNo().trim().isEmpty()) {
            errors.rejectValue("serialNo", "A serial no must be specified for the instrument", null,
                "A serial no must be specified for the instrument");
        }

		if (aif.getCoid() == null) {
			errors.rejectValue("coid", "error.instrument.company", null, "");
		}
		if (aif.getModelid() == null) {
			errors.rejectValue("modelid", "error.instrument.nomodel", null, "");
		}
		
		if(aif.getStatus() == null)
			errors.rejectValue("status", "error.instrument.mandatory", null,"Mandatory field");
		if(aif.getUsageTypeId() == null)
			errors.rejectValue("usageTypeId", "error.instrument.mandatory", null,"Mandatory field");
		if(aif.getStatus() != null && aif.getUsageTypeId() != null) {
			InstrumentUsageType usageType = this.instUsageTypeService.findInstrumentUsageType(aif.getUsageTypeId());
			if(usageType != null && !usageType.getInstrumentStatus().equals(aif.getStatus()))
				errors.rejectValue("usageTypeId", "error.instrument.usagetypecorrelation", null,"Usage type doesn't correlate with instrument type");
		}
	}
}