package org.trescal.cwms.web.instrument.controller;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.web.instrument.dto.ReplacementAddedDto;

import java.time.LocalDate;
import java.util.Map;

@RestController
@RequestMapping(path = "/replacementinstrument.json/{plantId}")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class WebReplacementController {

    private final InstrumService instrumentService;
    private final UserService userService;

    public WebReplacementController(InstrumService instrumentService,
                                    UserService userService) {
        this.instrumentService = instrumentService;
        this.userService = userService;
    }

    @PutMapping
    public ReplacementAddedDto addOrUpdateReplacement(@PathVariable("plantId") Integer plantId,
                                                      @RequestBody Map<String,Integer> incoming,
                                                      @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username){

        Contact contact = this.userService.get(username).getCon();

        Instrument instrument = instrumentService.get(plantId);
        Instrument replacementInstrument = instrumentService.get(incoming.get("replacementId"));

        instrument.setReplacement(replacementInstrument);
        instrument.setReplacedBy(contact);
        instrument.setReplacedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        instrumentService.merge(instrument);

        return new ReplacementAddedDto(replacementInstrument, instrument
                ,instrumentService.getCorrectInstrumentModelName(replacementInstrument));
    }

    @DeleteMapping
    public void removeReplacement(@PathVariable("plantId") Integer plantId){

        Instrument instrument = instrumentService.get(plantId);

        instrument.setReplacement(null);
        instrument.setReplacedBy(null);
        instrument.setReplacedOn(null);
        instrumentService.merge(instrument);

    }

}
