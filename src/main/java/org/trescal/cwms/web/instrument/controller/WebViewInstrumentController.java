package org.trescal.cwms.web.instrument.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.documents.images.image.db.ImageService;
import org.trescal.cwms.core.exception.EntityNotFoundException;
import org.trescal.cwms.core.exception.WebExternalPermissionException;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentFieldDTO;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db.PermitedCalibrationExtensionService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.db.CertLinkService;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardType;
import org.trescal.cwms.core.procedure.entity.defaultstandard.db.DefaultStandardService;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplateType;
import org.trescal.cwms.core.system.entity.labeltemplate.db.LabelTemplateService;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.*;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@ExtranetController
@Slf4j
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_USER_CERTVAL_RULE, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE})
public class WebViewInstrumentController {
    @Autowired
    private CertLinkService certLinkServ;
    @Autowired
    private CertificateService certServ;
	@Value("#{props['recall.default.interval']}")
	private Integer defaultRecallInterval;
	@Autowired
	private DefaultStandardService defStandServ;
	@Autowired
	private ImageService imageServ;
	@Autowired
	private InstrumService instrumServ;
	@Autowired
	private RecallRuleService recallRuleServ;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private UserService usrServ;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private LabelTemplateService labelTemplateServ;
	@Autowired
	private CalReqService calReqService;
	@Autowired
	private CalibrationVerification systemDefaultCalibrationVerification;
	@Autowired
	private Deviation systemDefaultDeviation;
	@Autowired
	private CalibrationExtensions systemDefaultCalExtension;
	@Autowired
	private PermitedCalibrationExtensionService calibrationExtensionService;
	@Autowired
	private CertificateClassDefault certificateClassSystemDefault;
	@Autowired
	private CustomerCertificateValidation customerCertValidationSystemDefault;
	@Autowired
    private MessageSource messageSource;
	

	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
        return new ArrayList<>();
    }

    @ModelAttribute("calExtensionAllowed")
    public Boolean isCalExtensionAllowed(@RequestParam(name = "plantid") Integer plantid) {
        Instrument instrument = this.instrumServ.get(plantid);
        return this.systemDefaultCalExtension.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
    }

    @ModelAttribute("calExtensionDateColor")
    public String setExtensionDateColor(@RequestParam(name = "plantid") Integer plantid) {
        Instrument instrument = this.instrumServ.get(plantid);
        PermitedCalibrationExtension extension = this.calibrationExtensionService.getExtensionForInstrument(instrument);
        LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        if (extension != null) {
            return (today.isAfter(instrument.getNextCalDueDate()) && !today.isAfter(extension.getExtensionEndDate()) ?
                "green" : (today.isAfter(extension.getExtensionEndDate()) ? "red" : "grey"));
        } else return null;
    }

    @ModelAttribute("hasCalVerification")
    public Boolean isCalVerificationUsed(@RequestParam(name = "plantid") Integer plantid) {
        Instrument instrument = this.instrumServ.get(plantid);
        return this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
    }

    @ModelAttribute("hasDeviation")
    public Boolean isDeviationAllowed(@RequestParam(name = "plantid") Integer plantid) {
        Instrument instrument = this.instrumServ.get(plantid);
        return this.systemDefaultDeviation.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
    }

    @ModelAttribute("hasCustomeCertificateValidation")
    public Boolean hasCustomerCertificateValidation(@RequestParam(name = "plantid") Integer plantid) {
        Instrument instrument = this.instrumServ.get(plantid);
        return this.customerCertValidationSystemDefault.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
    }


	@ModelAttribute("hasCertClass")
	public Boolean hasCertClass(@RequestParam(name="plantid") Integer plantid) {
		Instrument instrument = this.instrumServ.get(plantid);
		return this.certificateClassSystemDefault.getValueByScope(Scope.COMPANY,instrument.getComp(),null);
	}

    @ModelAttribute("userCanEdit")
    public Boolean canUserEdit(@RequestParam(name = "plantid") Integer plantid, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        Instrument instrument = this.instrumServ.get(plantid);
        User user = usrServ.get(username);
        return userGroupService.canUserUpdateThisInstrument(user.getCon(), instrument);
    }
	
	@ModelAttribute("userCanValidateCert")
	public Boolean canUserValidateCert(@RequestParam(name="plantid",required=false, defaultValue="0") Integer plantid, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username ){
		Instrument instrument = this.instrumServ.get(plantid);
		User user = usrServ.get(username);
		return userGroupService.canUserValidateCertificateForThisInstrument(user.getCon(), instrument);
	}
	
	@ModelAttribute("userCanCreateCert")
	public Boolean canUserAddCert(@RequestParam(name="plantid",required=false, defaultValue="0") Integer plantid, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username ){
		Instrument instrument = this.instrumServ.get(plantid);
		User user = usrServ.get(username);
		if(instrument.getCustomerManaged() != null && instrument.getCustomerManaged()){
			return userGroupService.canUserCreateCalibrationForThisInstrument(user.getCon(), instrument);
		} else {
			return false;
		}
	}

	@ModelAttribute("flexiblefields")
	public Set<InstrumentFieldDTO> initializeFlexibleFields(@RequestParam(name="plantid",required=false, defaultValue="0") Integer plantid){
        Instrument instrument = this.instrumServ.get(plantid);
	    return instrumServ.getFlexibleFields(instrument);
    }

	@ModelAttribute("calibrationinstructions")
	public String getCalibrationInstructions(@RequestParam(name="plantid",required=false, defaultValue="0") Integer plantid){
		String calibrationInstruction = " ";
		if(plantid > 0){
			Instrument instrument = instrumServ.get(plantid);
			CalReq calibrationRequirement = calReqService.findCalReqsForInstrument(instrument);
			if(calibrationRequirement != null){
				calibrationInstruction = calibrationRequirement.getPublicInstructions();
			}
		} 
		return calibrationInstruction;
	}

	@RequestMapping(value="/viewinstrument.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(@RequestParam(name="plantid",required=false, defaultValue="0") Integer plantid, @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception
	{
		Instrument instrument = this.instrumServ.get(plantid);
		User user = usrServ.get(username);
		
		if ((instrument == null) || (plantid == 0)) {
            log.error("Requested instrument " + plantid + " not found");
            throw new EntityNotFoundException("Instrument");
        }

        if (!userGroupService.canUserViewThisInstrument(user.getCon(), instrument)) {
            throw new WebExternalPermissionException(user.getCon().getPersonid(), user.getCon().getName(), Instrument.class, "Instrument");
        }


        Map<String, Object> refData = new HashMap<>();
        refData.put("instrument", instrument);

        refData.put("calextension", calibrationExtensionService.getExtensionForInstrument(instrument));

        // get a list of all certificates for this instrument and set
        // directories
        List<CertLink> cls = this.certLinkServ.searchCertLink(instrument.getPlantid(), false);
        for (CertLink cl : cls) {
            // sets directory
			this.certServ.findCertificate(cl.getCert().getCertid());
		}
		refData.put("certlinks", cls);

		List<InstCertLink> instCertLinks = new ArrayList<>();
		// loads all instrument certificates and sets directories
		for (InstCertLink icl : instrument.getInstCertLinks()) {
			instCertLinks.add(icl);
			this.certServ.findCertificate(icl.getCert().getCertid());
		}
		// To appear newest to oldest
		Collections.reverse(instCertLinks);
		refData.put("instCertLinks", instCertLinks);

		refData.put("validation", instrument.getIssues());

		LocalDate lastCalDate = (instrument.getLastCal() != null) ? instrument.getLastCal().getCalDate() : null;
		refData.put("lastCalDate", lastCalDate);

		// get the next recall month + year
		refData.put("recalldue", DateTools.getRecallPeriod(instrument.getNextCalDueDate()));

		// get the system default recall interval
		refData.put("defaultrecall", this.defaultRecallInterval);

		refData.put("CalRecallInterval",
			this.recallRuleServ.getCurrentRecallIntervalDto(instrument, RecallRequirementType.FULL_CALIBRATION));
		refData.put("currentInterimCalRecallRuleInterval",
				this.recallRuleServ.getCurrentRecallIntervalDto(instrument, RecallRequirementType.INTERIM_CALIBRATION));
		refData.put("currentMaintenanceRecallRuleInterval", 
				this.recallRuleServ.getCurrentRecallIntervalDto(instrument, RecallRequirementType.MAINTENANCE));
		
		// get a list of all recall rules applied to this instrument
		refData.put("activerecallrules", this.recallRuleServ.getAllInstrumentRecallRules(instrument.getPlantid(), true));

		// get a list of all old recall rules applied to this instrument
		refData.put("inactiverecallrules", this.recallRuleServ.getAllInstrumentRecallRules(instrument.getPlantid(), false));

		// get a list of all images that have been assigned to this instrument
		// (via jobitems)
		refData.put("jobItemImages", this.imageServ.getInstrumentImages(plantid));

		refData.put("history", this.instrumServ.getHistory(instrument));

		refData.put("privateOnlyNotes", NoteType.INSTRUMENTNOTE.isPrivateOnly());

		refData.put("imageheight", 500);
		refData.put("imagewidth", 750);

		// load any procedures / workinstructions that this instrument is a
		// standard for
		if (instrument.getCalibrationStandard())
		{
            refData.put("procstandards", this.defStandServ.getAllDefaultStandardsForInstument(plantid, DefaultStandardType.PROCEDURE));
            refData.put("wistandards", this.defStandServ.getAllDefaultStandardsForInstument(plantid, DefaultStandardType.WORKINSTRUCTION));
        }
        //get files in the root directory
        FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.INSTRUMENT, plantid, newFiles);
        refData.put(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.INSTRUMENT));
        refData.put(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
        refData.put(Constants.REFDATA_FILES_NAME, fileBrowserServ.getFilesName(fileBrowserWrapper));

        //get all job certs
        List<Certificate> allCertificates = cls.stream().map(CertLink::getCert).collect(Collectors.toList());
        //add any instrument certs
        allCertificates.addAll(instrument.getInstCertLinks().stream().map(InstCertLink::getCert).collect(Collectors.toList()));
        //find most recent signed cert
        Optional<Certificate> latestCertificate = allCertificates.stream()
            .max(Comparator.comparing(Certificate::getCertDate));

        if (latestCertificate.isPresent()) {
            refData.put("lastCert", latestCertificate.get());
            if (instrument.getInstrumentComplementaryField() != null && instrument.getInstrumentComplementaryField().getCalibrationStatusOverride() != null) {
                refData.put("calVerificationStatus", instrument.getInstrumentComplementaryField().getCalibrationStatusOverride().getName()
                    + " (" + messageSource.getMessage("web.overridden", null, LocaleContextHolder.getLocale()) + ")");
            } else if (latestCertificate.get().getCalibrationVerificationStatus() != null) {
                refData.put("calVerificationStatus", latestCertificate.get().getCalibrationVerificationStatus().getName());
            }
	    }

		refData.put("labeltemplate",labelTemplateServ.getLabelTemplateByLabelTemplateType(LabelTemplateType.STANDARD));
				
		return new ModelAndView("/instrument/webviewinstrument", "command", refData);
		
	}

}
