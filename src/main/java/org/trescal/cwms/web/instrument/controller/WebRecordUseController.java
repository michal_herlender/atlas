package org.trescal.cwms.web.instrument.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.web.instrument.dto.RecordUseResponseDTO;

@RequestMapping(value="/recordinstrmentuse.json")
@RestController
public class WebRecordUseController {

    @Autowired
    InstrumService instrumentService;
    @Autowired
    MessageSource messageSource;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private TemplateEngine templateEngine;
    @Autowired
    private EmailService emailService;


    @RequestMapping(value="{plantid:[0-9]+}",method= RequestMethod.PUT,produces= MediaType.APPLICATION_JSON_UTF8_VALUE)
    RecordUseResponseDTO recordUse(@PathVariable("plantid") Integer plantId){
        RecordUseResponseDTO response = new RecordUseResponseDTO();
        Instrument instrument = instrumentService.get(plantId);
        if (instrument == null) {
            //javascript should stop this ever happening so not bothering with translated message
            response.setSuccess(false);
            response.setMessage("Instrument not found!");
        } else if (!instrument.getCalExpiresAfterNumberOfUses()) {
            //javascript should stop this ever happening so not bothering with translated message
            response.setSuccess(false);
            response.setMessage("Instrument does not support cal expiry based on number of uses!");
        } else if (instrument.getCalExpiresAfterNumberOfUses()
                && (instrument.getUsesSinceLastCalibration() >= instrument.getPermittedNumberOfUses())) {
            response.setSuccess(false);
            response.setMessage(messageSource.getMessage("web.nomoreusespermitted",null, LocaleContextHolder.getLocale()));
        }
        else {
            instrument.setUsesSinceLastCalibration(instrument.getUsesSinceLastCalibration() + 1);
            instrumentService.merge(instrument);
            if(instrument.getUsesSinceLastCalibration() >= (instrument.getPermittedNumberOfUses() - 1)) {
                emailOwner(instrument);
            }
            response.setSuccess(true);
            response.setNumberOfUsesSinceCal(instrument.getUsesSinceLastCalibration());
            response.setPermittedNumberOfUses(instrument.getPermittedNumberOfUses());
        }
        return response;
    }

    private void emailOwner(Instrument instrument) {
        Locale locale = LocaleContextHolder.getLocale();
        Contact owner = instrument.getCon();
        Contact csr = owner.getSub().getComp().getDefaultBusinessContact();
        String toAddr = owner.getEmail();
        String fromAddr = csr.getEmail();;
        String subject = messageSource.getMessage("web.email.instlimiteduses.subject", new String[] {Integer.toString(instrument.getPlantid())},locale);
        Context context = new Context(locale);
        context.setVariable("client", owner);
        context.setVariable("csr", csr);
        context.setVariable("instrument", instrument);
        context.setVariable("translationService", translationService);
        String body = templateEngine.process("email/portal/instrument_limited_uses", context);
        System.out.print(body);
        if ( fromAddr != null && toAddr != null )
            emailService.sendBasicEmail(subject,fromAddr,toAddr,body);
    }

}
