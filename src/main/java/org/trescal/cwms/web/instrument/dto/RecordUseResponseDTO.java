package org.trescal.cwms.web.instrument.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RecordUseResponseDTO {

    Integer permittedNumberOfUses;
    Integer numberOfUsesSinceCal;
    String message;
    Boolean success;

}
