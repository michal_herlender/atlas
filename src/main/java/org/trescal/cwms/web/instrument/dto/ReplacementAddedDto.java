package org.trescal.cwms.web.instrument.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

import java.time.LocalDate;

public class ReplacementAddedDto {

    private Integer barcode;
    private String instrumentName;
    private String replacedBy;
    @JsonFormat(pattern = "yyy-MM-dd")
    private LocalDate replacedOn;


    public ReplacementAddedDto(Instrument replacementInstrument, Instrument instrument, String instrumentName) {

        this.barcode = replacementInstrument.getPlantid();
        this.instrumentName = instrumentName;
        this.replacedBy = instrument.getReplacedBy().getName();
        this.replacedOn = instrument.getReplacedOn();

    }

    public Integer getBarcode() {
        return barcode;
    }

    public void setBarcode(Integer barcode) {
        this.barcode = barcode;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getReplacedBy() {
        return replacedBy;
    }

    public void setReplacedBy(String replacedBy) {
        this.replacedBy = replacedBy;
    }

    public LocalDate getReplacedOn() {
        return replacedOn;
    }

    public void setReplacedOn(LocalDate replacedOn) {
        this.replacedOn = replacedOn;
    }
}
