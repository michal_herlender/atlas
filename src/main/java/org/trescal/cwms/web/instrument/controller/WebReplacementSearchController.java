package org.trescal.cwms.web.instrument.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.web.instrument.dto.ReplacementSearchDto;
import org.trescal.cwms.web.instrument.dto.SimpleInstrumentSearchResultsDto;

@RestController
@RequestMapping(path = "/replacementsearch.json")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class WebReplacementSearchController {

    private final InstrumService instrumentService;
    private final UserService userService;
    private final MfrService mfrService;
    private final NewDescriptionService descriptionService;

    @Autowired
    public WebReplacementSearchController(InstrumService instrumentService,
                                          UserService userService,
                                          MfrService mfrService,
                                          NewDescriptionService descriptionService) {
        this.instrumentService = instrumentService;
        this.userService = userService;
        this.mfrService = mfrService;
        this.descriptionService = descriptionService;
    }

    @PostMapping
    public List<SimpleInstrumentSearchResultsDto> searchForReplacement
            (@RequestBody ReplacementSearchDto searchDto,
             @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        User user = this.userService.get(username);
        Company company = user.getCon().getSub().getComp();
        Description desc = searchDto.getDescId() != null ? descriptionService.findDescription(searchDto.getDescId()) : null;
        Mfr mfr = searchDto.getMfrId() != null ? mfrService.findMfr(searchDto.getMfrId()) : null;
        Boolean outOfCal = searchDto.getInCal() ? false : null;
        List<InstrumentSearchResultsDTO> results = instrumentService
                .searchInstruments(company, searchDto.getPlantNo(), searchDto.getSerialNo(), desc, null,
                        mfr, null, null, null, outOfCal,
                        null, searchDto.getModelText(), null, searchDto.getPlantId(),
                        false, null);

        return results.stream().map(SimpleInstrumentSearchResultsDto::new)
                .collect(Collectors.toList());

    }

}
