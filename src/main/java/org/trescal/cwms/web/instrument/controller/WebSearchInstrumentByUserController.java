package org.trescal.cwms.web.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.dto.SearchInstrumentsByUserDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentsByUserForm;

@Controller @ExtranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
@RequestMapping("/websearchinstrumentbyuser.htm")
public class WebSearchInstrumentByUserController {

	@Autowired
	private UserService userService;
	@Autowired
	private InstrumService instServ;

	public static final String FORM_NAME = "command";

	@ModelAttribute(FORM_NAME)
	public WebSearchInstrumentsByUserForm populateForm(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		WebSearchInstrumentsByUserForm form = new WebSearchInstrumentsByUserForm();
		Contact contact = this.userService.get(username).getCon();
		form.setCoid(contact.getSub().getComp().getId());
		form.setPersonid(contact.getPersonid());
		return form;
	}

	
	@GetMapping
	public String onLoad(@ModelAttribute(FORM_NAME) WebSearchInstrumentsByUserForm form, BindingResult bindingResult)
	{

		PagedResultSet<SearchInstrumentsByUserDTO> rs = this.instServ.searchSortedInstrumentsByUser(form, new PagedResultSet<SearchInstrumentsByUserDTO>(0,0));
		form.setRs(rs);
		return "/instrument/websearchinstrumentbyuser";

	}
	
	@PostMapping
	protected String onSubmit(@ModelAttribute(FORM_NAME) WebSearchInstrumentsByUserForm form)
	{

		PagedResultSet<SearchInstrumentsByUserDTO> rs = this.instServ.searchSortedInstrumentsByUser(form, new PagedResultSet<SearchInstrumentsByUserDTO>(0, form.getPageNo()));
		form.setRs(rs);
		return "/instrument/websearchinstrumentbyuser";

	}
}