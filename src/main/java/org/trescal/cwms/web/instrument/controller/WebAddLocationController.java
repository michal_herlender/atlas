package org.trescal.cwms.web.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.spring.model.KeyValue;

@RestController
public class WebAddLocationController {

	@Autowired
    LocationService locationService;
	@Autowired
	AddressService addressService;

    @RequestMapping(value="/addlocation.json",method=RequestMethod.POST,
    		consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public Integer insertNewLocation(@RequestBody KeyValue<Integer,String> data ){
    	Location location = new Location();
    	location.setAdd(addressService.get(data.getKey()));
    	location.setActive(true);
    	location.setLocation(data.getValue());
        locationService.insertLocation(location);
        return location.getLocationid();
    }
}
