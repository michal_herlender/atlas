package org.trescal.cwms.web.instrument.dto;

import org.trescal.cwms.core.system.enums.IntervalUnit;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter @Setter
public class WebUpdateInstrumentDTO {
    private LocalDate nextCalDueDate;
    private Integer calFrequency;
    private IntervalUnit calFrequencyUnit;

    public WebUpdateInstrumentDTO(LocalDate nextCalDueDate, Integer calFrequency, IntervalUnit calFrequencyUnit) {
        this.nextCalDueDate = nextCalDueDate;
        this.calFrequency = calFrequency;
        this.calFrequencyUnit = calFrequencyUnit;
    }

}
