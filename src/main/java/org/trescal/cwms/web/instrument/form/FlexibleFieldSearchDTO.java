package org.trescal.cwms.web.instrument.form;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlexibleFieldSearchDTO {
	
	Integer fieldDefinitionId;
	Boolean booleanValue;
	String stringValue;
	Double numericValue;
	Date dateValue;
	Integer selectionValueId;
		
}
