package org.trescal.cwms.web.instrument.dto;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleInterval;

public class WebSearchInstrumentsDTO {
	private Instrument instrument;
	private RecallRuleInterval interval;
	
	public WebSearchInstrumentsDTO(Instrument instrument, RecallRuleInterval interval) {
		this.instrument = instrument;
		this.interval = interval;
	}
	public Instrument getInstrument() {
		return instrument;
	}
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
	public RecallRuleInterval getInterval() {
		return interval;
	}
	public void setInterval(RecallRuleInterval interval) {
		this.interval = interval;
	}
	
}
