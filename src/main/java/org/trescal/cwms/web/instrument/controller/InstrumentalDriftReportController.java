package org.trescal.cwms.web.instrument.controller;

import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.tools.BirtEngine;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller @ExtranetController
@RequestMapping(path = "/instrumentaldrift.htm")
public class InstrumentalDriftReportController {

    @Autowired
    BirtEngine birtEngine;

    private static final String REPORT_PATH = "/birt/scripted/instrument/instrumental_drift.rptdesign";

    @GetMapping
    public ResponseEntity<byte[]> produceDocument(@RequestParam("plantid") Integer plantid) throws IOException, EngineException {

        Locale locale = LocaleContextHolder.getLocale();
        String filename = "drift_" + plantid + ".pdf";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID, plantid);

        //Create binary contents
        byte[] pdf = birtEngine.generatePDFAsByteArray(REPORT_PATH, parameters, locale);

        //Create headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        //create response
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
        return response;
    }
}
