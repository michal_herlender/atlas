package org.trescal.cwms.web.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.WebExternalPermissionException;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentFieldDefinitionService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentFieldLibraryValueService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentValueService;
import org.trescal.cwms.core.instrument.form.AddEditInstrumentFieldForm;
import org.trescal.cwms.core.instrument.form.AddEditInstrumentFieldValidator;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Date;
import java.util.Set;

@Controller @ExtranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
@RequestMapping(value = "/addeditflexiblefieldvalue.htm")
public class WebAddEditInstrumentFieldController {

    @Autowired
    private InstrumentFieldLibraryValueService instrumentFieldLibraryValueService;

    @Autowired
    private InstrumentValueService instrumentValueService;
    
    @Autowired
    private InstrumentFieldDefinitionService fieldDefinitionService;

    @Autowired
    private AddEditInstrumentFieldValidator validator;
    
    @Autowired
    private UserGroupService userGroupService;
    
    @Autowired
    private InstrumService instrumentService;

    @Autowired
    private UserService userService;
    
    
    @InitBinder("command")
    protected void initBinder(WebDataBinder binder) {
        MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
        binder.registerCustomEditor(Date.class, editor);
        binder.setValidator(validator);
    }

    @ModelAttribute("selectionvalues")
    public Set<KeyValue<Integer,String>> initializeSelectionValues(@RequestParam(name="fielddefid", required=false, defaultValue="0") Integer fieldDefinitionId,
                                                                   @RequestParam(name="valueid", required=false, defaultValue="0") Integer instrumentFieldValueId) throws Exception{
        if(instrumentFieldValueId == 0 && fieldDefinitionId == 0){
            throw new Exception("fielddefid and valueid both zero");
        }
        return instrumentFieldLibraryValueService.getLibraryValuesForDefinitionAsKeyValue(fieldDefinitionId, instrumentFieldValueId);
    }


    @ModelAttribute("command")
    public AddEditInstrumentFieldForm initializeFormBackingObject(@RequestParam(name="fielddefid", required=false, defaultValue="0") Integer fieldDefinitionId,
                                                                  @RequestParam(name="valueid", required=false, defaultValue="0") Integer instrumentFieldValueId,
                                                                  @RequestParam(name="plantid", required=true) Integer plantId) throws Exception{
        if(instrumentFieldValueId == 0 && fieldDefinitionId == 0){
            throw new Exception("fielddefid and valueid both zero");
        }
        return instrumentValueService.createFormBackingObject(instrumentFieldValueId,fieldDefinitionId,plantId);
    }

    @RequestMapping(method= RequestMethod.GET)
    public String displayForm(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
    		@ModelAttribute("command") AddEditInstrumentFieldForm command) throws Exception{
    	
    	System.out.println("username: " + username);
    	Contact userContact = this.userService.get(username).getCon();
    	Instrument instrument = this.instrumentService.get(command.getPlantId());
    	
    	if(!fieldDefinitionService.get(command.getFieldDefinitionId()).getIsUpdatable() ||
    			!userGroupService.canUserUpdateThisInstrument(userContact, instrument)){
    		
    		throw new WebExternalPermissionException(userContact.getPersonid(), userContact.getName(), InstrumentValue.class, "InstrumentValue");
    	}
    	
        switch (command.getType()) {
            case BOOLEAN:
                return "/instrument/flexiblefields/webaddeditbooleanflexiblefieldvalue";
            case DATETIME:
                return "instrument/flexiblefields/webaddeditdateflexiblefieldvalue";
            case NUMERIC:
                return "instrument/flexiblefields/webaddeditnumericflexiblefieldvalue";
            case SELECTION:
                return "instrument/flexiblefields/webaddeditselectionflexiblefieldvalue";
            case STRING:
                return "instrument/flexiblefields/webaddeditstringflexiblefieldvalue";
            default:
                throw new Exception("unknown field type " + command.getType().toString());
        }
    }

    @RequestMapping(params = "update", method=RequestMethod.POST)
    public String processFormUpdate(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
    		@ModelAttribute("command") @Validated AddEditInstrumentFieldForm command, BindingResult bindingResult) throws Exception{

        if (bindingResult.hasErrors()) return this.displayForm(username,command);

        instrumentValueService.updateFieldValue(command);

        return "redirect:/web/viewinstrument.htm?plantid=" + command.getPlantId();
    }

    @RequestMapping(params = "delete", method=RequestMethod.POST)
    public String processFormDelete(@ModelAttribute("command") @Validated AddEditInstrumentFieldForm command) throws Exception{
        InstrumentValue value = instrumentValueService.get(command.getValueId());
        instrumentValueService.delete(value);
        return "redirect:/web/viewinstrument.htm?plantid=" + command.getPlantId();
    }

}
