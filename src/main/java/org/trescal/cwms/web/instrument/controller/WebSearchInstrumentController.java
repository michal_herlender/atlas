package org.trescal.cwms.web.instrument.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentFieldDefinitionService;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.db.InstrumentUsageTypeService;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;
import org.trescal.cwms.core.instrument.views.InstrumentXlsxView;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.web.instrument.form.WebInstrumentSearchResultsDTO;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentForm;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Web version of search instrument. adapted from searchinstrumentcontroller
 *
 * @author Matt
 */
@Controller
@ExtranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class WebSearchInstrumentController {
	@Autowired
	private AddressService addrServ;
	@Autowired
	private ContactService contServ;
	@Autowired
	private NewDescriptionService descriptionService;
	@Autowired
	private InstrumService instService;
	@Autowired
	private MfrService mfrService;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private UserService userService;
	@Autowired
	private InstrumentFieldDefinitionService instrumentFieldDefinitionService;
	@Autowired
	private InstrumentXlsxView instrumentXlsxView;
	@Autowired
	private InstrumentModelService modelService;
	@Autowired
	private WebSearchInstrumentValidator validator;
	@Autowired
	private CalReqService calReqService;
	@Autowired
	private CalibrationVerification systemDefaultCalibrationVerification;
	@Autowired
	private InstrumentUsageTypeService usageTypeService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private CertificateService certService;

	public static final Logger logger = LoggerFactory.getLogger(WebSearchInstrumentController.class);

	public static final String FORM_NAME = "command";

	@ModelAttribute(FORM_NAME)
	protected WebSearchInstrumentForm formBackingObject() {
		return new WebSearchInstrumentForm();
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@ModelAttribute("hasCalVerification")
	public Boolean isCalVerificationUsed(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		return this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY,
				this.userService.get(username).getCon().getSub().getComp(), null);
	}

	@ModelAttribute("flexiblefields")
	public Set<InstrumentFieldDefinition> initializeFlexibleFieldDefinitions(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		return instrumentFieldDefinitionService
				.getInstrumentFieldDefinitionsForCompany(this.userService.get(username).getCon().getSub().getComp());
	}

	@ModelAttribute("usageTypes")
	public List<KeyValue<Integer, String>> getAllUsageTypes() {
		return usageTypeService.getAllInstrumentUsageTypes()
				.stream().map(ut -> new KeyValue<>(ut.getId(), translationService
						.getCorrectTranslation(ut.getNametranslation(), LocaleContextHolder.getLocale())))
				.collect(Collectors.toList());
	}

	@ModelAttribute("companyId")
	public Integer getCompanyId(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Contact contact = this.userService.get(username).getCon();
		return contact.getSub().getComp().getCoid();
	}

	@RequestMapping(value = "/searchinstrument.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request, HttpSession session,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Validated @ModelAttribute(FORM_NAME) WebSearchInstrumentForm form, BindingResult bindingResult,
			HttpServletResponse response) {
		if (bindingResult.hasErrors())
			return referenceData(username);
		Contact contact = this.userService.get(username).getCon();
		PagedResultSet<InstrumentSearchResultsDTO> rs = instService.searchInstrumentsPaged(
            new PagedResultSet<>(form.getResultsPerPage(), form.getPageNo()),
            contact.getSub().getComp(), form);

		form.setRs(rs);
		Collection<InstrumentSearchResultsDTO> ins = rs.getResults();

		List<WebInstrumentSearchResultsDTO> resultsDTO = new ArrayList<>();

		for (InstrumentSearchResultsDTO dto : ins) {
				Instrument instrument = instService.findEagerInstrum(dto.getPlantId());

				List<Certificate> certs = this.certService.getCertificatesForInstrument(instrument);

				Certificate cert = certs.stream().max(Comparator.comparing(Certificate::getCalDate))
						.filter(cer -> !cer.getStatus().equals(CertStatusEnum.DELETED)
								&& !cer.getStatus().equals(CertStatusEnum.CANCELLED)).orElseGet(Certificate::new);

				CalibrationVerificationStatus overrideStatus = null;
				if (instrument.getInstrumentComplementaryField() != null) {
					overrideStatus = instrument.getInstrumentComplementaryField().getCalibrationStatusOverride();
				}
				WebInstrumentSearchResultsDTO webInstrumentSearchResultsDTO = new WebInstrumentSearchResultsDTO(dto, cert,
						overrideStatus);
				resultsDTO.add(webInstrumentSearchResultsDTO);
		}

		form.setInstruments(ins);
		form.setResults(resultsDTO);
		form.setInstCount(form.getRs().getResultsCount());
		// This overrides spring security's default cache-control setting for
		// this page so that browser back and forward
		// buttons will traverse the paged results without sending another
		// request to the server
		response.setHeader("Cache-Control", "Private");
		return new ModelAndView("/instrument/websearchinstrumentresults", "command", form);
	}

	@RequestMapping(value = "/searchinstrument.htm", method = RequestMethod.POST, params = "export")
	protected ModelAndView onExport(HttpServletRequest request, HttpSession session,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Validated @ModelAttribute(FORM_NAME) WebSearchInstrumentForm form, BindingResult bindingResult) {

		Company company = this.userService.get(username).getCon().getSub().getComp();
		Boolean includeCertInfo = this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY, company,
				null);
		form.setIncludeCertInfo(includeCertInfo);
        Description desc = form.getDescid() == null ? null : descriptionService.findDescription(form.getDescid());
        Mfr mfr = form.getMfrid() == null ? null : mfrService.findMfr(form.getMfrid());
        Contact contact = form.getPersonid() == null ? null : contServ.get(form.getPersonid());
        Subdiv subdiv = form.getSubdivid() == null ? null : subdivServ.get(form.getSubdivid());
        Address address = form.getAddressid() == null ? null : addrServ.get(form.getAddressid());
        InstrumentModel instModel = form.getModelid() == null ? null : modelService.get(form.getModelid());
        List<InstrumentSearchResultsDTO> instruments = instService.searchInstruments(company, form.getPlantno(),
            form.getSerialno(), desc, instModel, mfr, contact, subdiv, address, form.getOutOfCalibration(), null,
            null, form.getCustomerDescription(), null, null, form);

        Map<String, Object> model = new HashMap<>();
        model.put("instruments", instruments);

        model.put("companyRole", company.getCompanyRole());
        model.put("flexibleFields", company.getInstrumentFieldDefinition().stream()
            .map(InstrumentFieldDefinition::getName).sorted().collect(Collectors.toList()));
        List<Integer> plantids = instruments.stream().map(InstrumentSearchResultsDTO::getPlantId)
            .collect(Collectors.toList());
        model.put("calrequirements", calReqService.findInstrumentCalReqsForPlantIds(plantids));
        model.put("includeCertInfo", includeCertInfo);
        return new ModelAndView(instrumentXlsxView, model);

	}

	@RequestMapping(value = "/searchinstrument.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        Contact contact = this.userService.get(username).getCon();
        Map<String, Object> refData = new HashMap<>();
        refData.put("subdivs", this.subdivServ.getAllActiveCompanySubdivs(contact.getSub().getComp()));
        refData.put("addresses", this.addrServ.getAllActiveSubdivAddresses(contact.getSub(), null));
        refData.put("contacts", this.contServ.getAllActiveSubdivContacts(contact.getSub().getSubdivid()));
        refData.put("inststatuss", InstrumentStatus.activeStatus());
        refData.put("userContact", contact);
        return new ModelAndView("/instrument/websearchinstrument", refData);
    }
}
