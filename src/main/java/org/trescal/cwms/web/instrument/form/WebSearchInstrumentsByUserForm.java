package org.trescal.cwms.web.instrument.form;

import org.trescal.cwms.core.instrument.dto.SearchInstrumentsByUserDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

public class WebSearchInstrumentsByUserForm
{
	private Integer coid;
	private int pageNo;
	private Integer personid;
	private PagedResultSet<SearchInstrumentsByUserDTO> rs;

	public Integer getCoid() {
		return coid;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPersonid() {
		return personid;
	}

	public void setPersonid(Integer personid) {
		this.personid = personid;
	}

	public PagedResultSet<SearchInstrumentsByUserDTO> getRs() {
		return rs;
	}

	public void setRs(PagedResultSet<SearchInstrumentsByUserDTO> rs) {
		this.rs = rs;
	}

}