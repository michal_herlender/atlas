package org.trescal.cwms.web.instrument.form;

import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.form.SearchInstrumentForm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WebSearchInstrumentForm extends SearchInstrumentForm
{
	//private Integer addrid;
	private Boolean exactSearch;
	private InstrumentStatus status;
	private List<FlexibleFieldSearchDTO> flexibleFieldSearches;
	private Boolean calDueDateBetween;
	private Boolean lastCalDateBetween;
	private int instCount;
	private Boolean newSearch;
	private Integer remainingUses;
	private Boolean hasReplacement;
	private List<WebInstrumentSearchResultsDTO> results;
	private Integer usageTypeId;
	private Boolean includeCertInfo;
	
}