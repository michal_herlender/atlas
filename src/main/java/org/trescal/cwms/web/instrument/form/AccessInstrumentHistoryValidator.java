package org.trescal.cwms.web.instrument.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AccessInstrumentHistoryValidator extends AbstractBeanValidator implements SmartValidator {

	@Autowired
	private InstrumService instService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(Instrument.class);
	}

	@Override
	public void validate(Object target, Errors errors, Object... validationHints) {

		Instrument inst = (Instrument) target;
		Company comp = (Company) validationHints[0];

		// if the instrument is not found or the user don't belong to the
		// client company return an error message (validation in customer portal
		// level)
		if (inst == null || !instService.checkExists(inst.getPlantno(), null, comp.getCoid(), null)) {
			errors.reject("error.instrument.plantid.notfound", "Instrument could not be found");
		}
		
	}
}
