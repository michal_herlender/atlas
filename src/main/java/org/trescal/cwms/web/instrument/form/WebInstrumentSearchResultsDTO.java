package org.trescal.cwms.web.instrument.form;

import java.time.LocalDate;
import java.util.Date;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WebInstrumentSearchResultsDTO {

    private Integer plantId;
    private String modelName;
    private String customerDescription;
    private Boolean scrapped;
    private InstrumentStatus instrumentStatus;
    private IntervalUnit calibrationIntervalUnit;
    private Integer calibrationInterval;
    private LocalDate calDueDate;
    private String serialNo;
    private String plantNo;
    private String owner;
    private LocalDate  lastCalDate;
    private String location;
    private String certNo;
    private String calStatusName;
    private Integer certId;
    private String usageTypeName;
    private Date lastcertdate;

    public WebInstrumentSearchResultsDTO(InstrumentSearchResultsDTO dto, Certificate latestCert, CalibrationVerificationStatus overrideStatus) {
        this.plantId = dto.getPlantId();
        this.modelName = dto.getModelName();
        this.customerDescription = dto.getCustomerDescription();
        this.scrapped = dto.getScrapped();
        this.instrumentStatus = dto.getInstrumentStatus();
        this.calibrationInterval = dto.getCalibrationInterval();
        this.calDueDate = dto.getCalDueDate();
        this.serialNo = dto.getSerialNo();
        this.plantNo = dto.getPlantNo();
        this.owner = dto.getOwner();
        this.lastCalDate = dto.getLastCalDate();
        this.location = dto.getLocation();
        this.certNo = latestCert != null ? latestCert.getCertno() : "";
        this.calStatusName = overrideStatus == null ?
            latestCert != null && latestCert.getCalibrationVerificationStatus() != null ? latestCert.getCalibrationVerificationStatus().getName() : ""
                : overrideStatus.getName();
        this.certId = latestCert != null ? latestCert.getCertid() : 0;
        this.usageTypeName = dto.getUsageTypeName();
        this.lastcertdate =dto.getCertCalDate();
    }




}
