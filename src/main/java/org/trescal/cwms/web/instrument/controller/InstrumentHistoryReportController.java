package org.trescal.cwms.web.instrument.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.web.instrument.form.AccessInstrumentHistoryValidator;

@Controller
@ExtranetController
@RequestMapping(path = "/instrumenthistorydocument.htm")
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class InstrumentHistoryReportController {

	@Autowired
	private BirtEngine birtEngine;

	@Autowired
	private Deviation deviationSystemDefault;

	@Autowired
	private InstrumService instrumentService;

	@Autowired
	protected UserService userService;

	@Autowired
	private AccessInstrumentHistoryValidator validator;

	private static final String REPORT_PATH = "/birt/scripted/instrument/instrument_history.rptdesign";

	@GetMapping
	public Object produceDocument(@RequestParam("plantid") Integer plantid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, BindingResult bindingResult)
			throws IOException, EngineException {

		Contact contact = this.userService.get(username).getCon();

		Instrument instrument = instrumentService.get(plantid);

		validator.validate(instrument, bindingResult, contact.getSub().getComp());

		if (bindingResult.hasErrors()) {
			// not download the instrument history and redirect to the view
			// instrument page where we can see the error
			// message 'The page you are trying to view is beyond the scope of
			// your viewing permissions!'
			return "redirect:/web/viewinstrument.htm?plantid=" + plantid;
		} else {
			Locale locale = LocaleContextHolder.getLocale();
			String filename = "history_" + plantid + ".pdf";
			Boolean showDeviation = deviationSystemDefault.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
			Map<String, Object> parameters = new HashMap<>();
			parameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID, plantid);
			parameters.put("showdeviation", showDeviation);

			// Create binary contents
			byte[] pdf = birtEngine.generatePDFAsByteArray(REPORT_PATH, parameters, locale);

			// Create headers
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setContentDispositionFormData(filename, filename);
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

			// create response
			ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
			return response;
		}

	}

}
