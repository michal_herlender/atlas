package org.trescal.cwms.web.instrument.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;

import java.time.LocalDate;

@Getter
@Setter
public class SimpleInstrumentSearchResultsDto {

    private Integer plantId;
    private String modelName;
    private String serialNo;
    private String plantNo;
    @JsonFormat(pattern = "yyy-MM-dd")
    private LocalDate nextCalDate;

    public SimpleInstrumentSearchResultsDto(InstrumentSearchResultsDTO bigDto) {
        this.plantId = bigDto.getPlantId();
        this.modelName = bigDto.getModelName();
        this.serialNo = bigDto.getSerialNo();
        this.plantNo = bigDto.getPlantNo();
        this.nextCalDate = bigDto.getCalDueDate();
    }

}
