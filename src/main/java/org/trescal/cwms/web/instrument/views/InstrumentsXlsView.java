package org.trescal.cwms.web.instrument.views;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.web.instrument.dto.WebSearchInstrumentsDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
public class InstrumentsXlsView extends AbstractXlsxView {

	@Autowired
	private LocaleResolver localeResolver;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private TranslationService tranServ;
	
	public static final Logger logger = LoggerFactory.getLogger(InstrumentsXlsView.class);
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		long startTime = System.currentTimeMillis();
		Locale userLocale = localeResolver.resolveLocale(request);
		
		@SuppressWarnings("unchecked")
		List<WebSearchInstrumentsDTO> resultsDTO = (List<WebSearchInstrumentsDTO>) model.get("results");
		
		// create a new Excel sheet
		Sheet sheet = workbook.createSheet(messageSource.getMessage("web.instruments", null, "Instruments", userLocale));
		sheet.setDefaultColumnWidth(25);

		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);

		int cellCount = 0;
		// create header row
		Row header = sheet.createRow(0);
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.barcode", null, "Barcode", userLocale));
		header.getCell(cellCount).setCellStyle(style);
		
		cellCount++;
		header.createCell(cellCount).setCellValue(messageSource.getMessage("instmodelname", null, "Instrument Model Name", userLocale));
		header.getCell(cellCount).setCellStyle(style);

		cellCount++;
		header.createCell(cellCount).setCellValue(messageSource.getMessage("viewinstrument.customerdescription", null, "Customer Description", userLocale));
		header.getCell(cellCount).setCellStyle(style);
		
		cellCount++;
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.serialno", null, "Serial No", userLocale));
		header.getCell(cellCount).setCellStyle(style);
		
		cellCount++;		
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.plantno", null, "Plant No", userLocale));
		header.getCell(cellCount).setCellStyle(style);
		
		cellCount++;		
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.owner", null, "Owner", userLocale));
		header.getCell(cellCount).setCellStyle(style);
		
		cellCount++;
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.address", null, "Address", userLocale));
		header.getCell(cellCount).setCellStyle(style);

		cellCount++;
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.subdivision", null, "Subdivision", userLocale));
		header.getCell(cellCount).setCellStyle(style);

		cellCount++;
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.location", null, "Location", userLocale));
		header.getCell(cellCount).setCellStyle(style);

		cellCount++;
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.nextcaldate", null, "Next Cal Due", userLocale));
		header.getCell(cellCount).setCellStyle(style);

		cellCount++;
		header.createCell(cellCount).setCellValue(messageSource.getMessage("web.interval", null, "Interval", userLocale));
		header.getCell(cellCount).setCellStyle(style);

		response.setHeader("Content-Disposition", "attachment; filename=\"instrument_export.xlsx\"");
		
		int rowCount = 1;
		
		for (WebSearchInstrumentsDTO result : resultsDTO) {
			int cell = 0;
			Row row = sheet.createRow(rowCount++);
			row.createCell(cell++).setCellValue(result.getInstrument().getPlantid());
			// use the customer description if it's populated else use the translated model name.
			row.createCell(cell++).setCellValue(tranServ.getCorrectTranslation(result.getInstrument().getModel().getNameTranslations(),userLocale));	
			row.createCell(cell++).setCellValue(result.getInstrument().getCustomerDescription());
			row.createCell(cell++).setCellValue(result.getInstrument().getSerialno());
			row.createCell(cell++).setCellValue(result.getInstrument().getPlantno());
			//cell++;  Cannot figure this out but with this cell++ in the export is out by 1 column from here onwards! PJW 13/6/17
			if(result.getInstrument().getCon() != null){
				row.createCell(cell).setCellValue(result.getInstrument().getCon().getName());
			}
			cell++;
			if(result.getInstrument().getAdd() != null){
				row.createCell(cell).setCellValue(result.getInstrument().getAdd().getAddr1());
			}
			cell++;
			if(result.getInstrument().getCon().getSub() != null){
				row.createCell(cell).setCellValue(result.getInstrument().getCon().getSub().getSubname());
			}
			cell++;
			if(result.getInstrument().getLoc() != null){
				row.createCell(cell).setCellValue(result.getInstrument().getLoc().getLocation());
			}
			cell++;
			if(result.getInstrument().getNextCalDueDate() != null) {
				row.createCell(cell).setCellValue(DateTools.df.format(result.getInstrument().getNextCalDueDate()));
			}
			cell++;
			if(result.getInterval() != null){
				row.createCell(cell).setCellValue(result.getInterval().getInterval() + " " + result.getInterval().getUnit().getName(result.getInterval().getInterval()));
			}
		}
		long finishTime = System.currentTimeMillis();
		logger.debug("Exported "+(rowCount-1)+" rows in "+(finishTime-startTime)+" ms");
	}
}
