package org.trescal.cwms.web.instrument.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReplacementSearchDto {

    private Integer plantId;
    private String serialNo;
    private String plantNo;
    private Integer descId;
    private Integer mfrId;
    private Boolean inCal;
    private String modelText;

}

