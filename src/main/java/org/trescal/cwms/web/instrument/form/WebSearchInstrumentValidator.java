package org.trescal.cwms.web.instrument.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class WebSearchInstrumentValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(WebSearchInstrumentForm.class);
	}
	
	@Override
	public void validate(Object obj, Errors errors)
	{
		super.validate(obj, errors);
		WebSearchInstrumentForm form = (WebSearchInstrumentForm) obj;
		if (!form.getPlantid().trim().isEmpty()) {
			// check that it converts into a number
			if (!NumberTools.isAnInteger(form.getPlantid().trim())) {
				errors.rejectValue("plantid", "plantid", null, "The barcode given must be a number.");
			}
		}

		if (form.getCalDueDate1() != null && form.getCalDueDate2() != null && form.getCalDueDate1().isAfter(form.getCalDueDate2())) {
			errors.reject("error.instrument.search.firstcalduedatebeforesecond", "The first calibration due date must be before the second calibration due date");
		}

		if (form.getLastCalDate1() != null && form.getLastCalDate2() != null && form.getLastCalDate1().isAfter(form.getLastCalDate2())) {
			errors.reject("error.instrument.search.firstlastcaldatebeforesecond", "The first date must be before the second date when searching last cal date");
		}


	}
}