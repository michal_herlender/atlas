package org.trescal.cwms.web.utilities.formutils;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public class WebRequestFormItemDTO
{
	private CalibrationType caltype;
	private Integer caltypeid;
	private String comment;
	private String commentDesc;
	private Boolean faulty;
	private String faultyDesc;
	private Instrument inst;
	private String itempo;
	private InstrumentModel model;
	private Integer modelid;
	private Integer plantid;
	private String plantno;
	private Integer qty;
	private String quotetype;
	private String serialno;
	private String supportDesc;
	private Boolean supported;
	private String turn;

	public CalibrationType getCaltype()
	{
		return this.caltype;
	}

	public Integer getCaltypeid()
	{
		return this.caltypeid;
	}

	public String getComment()
	{
		return this.comment;
	}

	public String getCommentDesc()
	{
		return this.commentDesc;
	}

	public Boolean getFaulty()
	{
		return this.faulty;
	}

	public String getFaultyDesc()
	{
		return this.faultyDesc;
	}

	public Instrument getInst()
	{
		return this.inst;
	}

	public String getItempo()
	{
		return this.itempo;
	}

	public InstrumentModel getModel()
	{
		return this.model;
	}

	public Integer getModelid()
	{
		return this.modelid;
	}

	public Integer getPlantid()
	{
		return this.plantid;
	}

	public String getPlantno()
	{
		return this.plantno;
	}

	public Integer getQty()
	{
		return this.qty;
	}

	public String getQuotetype()
	{
		return this.quotetype;
	}

	public String getSerialno()
	{
		return this.serialno;
	}

	public String getSupportDesc()
	{
		return this.supportDesc;
	}

	public Boolean getSupported()
	{
		return this.supported;
	}

	public String getTurn()
	{
		return this.turn;
	}

	public void setCaltype(CalibrationType caltype)
	{
		this.caltype = caltype;
	}

	public void setCaltypeid(Integer caltypeid)
	{
		this.caltypeid = caltypeid;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public void setCommentDesc(String commentDesc)
	{
		this.commentDesc = commentDesc;
	}

	public void setFaulty(Boolean faulty)
	{
		this.faulty = faulty;
	}

	public void setFaultyDesc(String faultyDesc)
	{
		this.faultyDesc = faultyDesc;
	}

	public void setInst(Instrument inst)
	{
		this.inst = inst;
	}

	public void setItempo(String itempo)
	{
		this.itempo = itempo;
	}

	public void setModel(InstrumentModel model)
	{
		this.model = model;
	}

	public void setModelid(Integer modelid)
	{
		this.modelid = modelid;
	}

	public void setPlantid(Integer plantid)
	{
		this.plantid = plantid;
	}

	public void setPlantno(String plantno)
	{
		this.plantno = plantno;
	}

	public void setQty(Integer qty)
	{
		this.qty = qty;
	}

	public void setQuotetype(String quotetype)
	{
		this.quotetype = quotetype;
	}

	public void setSerialno(String serialno)
	{
		this.serialno = serialno;
	}

	public void setSupportDesc(String supportDesc)
	{
		this.supportDesc = supportDesc;
	}

	public void setSupported(Boolean supported)
	{
		this.supported = supported;
	}

	public void setTurn(String turn)
	{
		this.turn = turn;
	}

}
