package org.trescal.cwms.web.utilities;

import java.util.List;
import java.util.Set;

import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.WebExternalPermissionException;
import org.trescal.cwms.core.exception.WebInternalPermissionException;
import org.trescal.cwms.core.exception.WebPermissionException;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db.UserGroupPropertyValueService;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

public class WebSecurityChecker {

	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private UserGroupPropertyValueService userGroupPropertyValueService;
	@Autowired
	private CertificateService certService;

	private String viewPermission;
	private Contact contactOwner;
	private Contact currentContact;
	private String identifier;
	private Object returnValue;

	public void doAddressChecks() throws WebPermissionException {
		if (this.returnValue instanceof Job) {
			Job j = (Job) this.returnValue;

			if (this.viewPermission.equalsIgnoreCase("location")
					&& (j.getReturnTo().getAddrid() != this.currentContact.getDefAddress().getAddrid())) {
				throw new WebInternalPermissionException(this.contactOwner.getPersonid(), this.contactOwner.getName(),
						this.returnValue.getClass(), j.getIdentifier());
			}
		} else if (this.returnValue instanceof Instrument) {
			Instrument i = (Instrument) this.returnValue;

			if (this.viewPermission.equalsIgnoreCase("location")
					&& (i.getAdd().getAddrid() != this.currentContact.getDefAddress().getAddrid())) {
				throw new WebInternalPermissionException(this.contactOwner.getPersonid(), this.contactOwner.getName(),
						this.returnValue.getClass(), i.getIdentifier());
			}
		} else if (this.returnValue instanceof Certificate) {
			Certificate c = (Certificate) this.returnValue;

			Set<CertLink> cls = c.getLinks();
			Set<InstCertLink> ils = c.getInstCertLinks();

			if ((cls != null) && (cls.size() > 0)) {
				for (CertLink cl : cls) {
					if (this.viewPermission.equalsIgnoreCase("location") && (cl.getJobItem().getJob().getReturnTo()
							.getAddrid() != this.currentContact.getDefAddress().getAddrid())) {
						throw new WebInternalPermissionException(this.contactOwner.getPersonid(),
								this.contactOwner.getName(), this.returnValue.getClass(), c.getIdentifier());
					}
				}
			} else if ((ils != null) && (ils.size() > 0)) {
				for (InstCertLink il : ils) {
					if (this.viewPermission.equalsIgnoreCase("location")
							&& (il.getInst().getAdd().getAddrid() != this.currentContact.getDefAddress().getAddrid())) {
						throw new WebInternalPermissionException(this.contactOwner.getPersonid(),
								this.contactOwner.getName(), this.returnValue.getClass(), c.getIdentifier());
					}
				}
			}
		}
	}

	public void doContactChecks() throws WebPermissionException {
		// check if contact is hacking into another company
		if (this.contactOwner.getSub().getComp().getCoid() != this.currentContact.getSub().getComp().getCoid()) {
			throw new WebExternalPermissionException(this.contactOwner.getPersonid(), this.contactOwner.getName(),
					this.returnValue.getClass(), this.identifier);
		}

		// check if contact has required permissions within company
		if (this.viewPermission.equalsIgnoreCase("subdiv")
				&& (this.contactOwner.getSub().getSubdivid() != this.currentContact.getSub().getSubdivid())) {
			throw new WebInternalPermissionException(this.contactOwner.getPersonid(), this.contactOwner.getName(),
					this.returnValue.getClass(), this.identifier);
		}
		if (this.viewPermission.equalsIgnoreCase("personal")
				&& (this.contactOwner.getPersonid() != this.currentContact.getPersonid())) {
			throw new WebInternalPermissionException(this.contactOwner.getPersonid(), this.contactOwner.getName(),
					this.returnValue.getClass(), this.identifier);
		}
	}

	public void doSecurityChecks(JoinPoint jp, Object returnValue) throws Exception {
		this.contactOwner = null;
		this.returnValue = returnValue;
		this.currentContact = this.sessionServ.getCurrentContact();
		this.viewPermission = this.userGroupPropertyValueService
				.getValue(this.currentContact, UserGroupProperty.VIEW_LEVEL).name();

		if (this.returnValue != null) {
			// Don't perform security checks if user is trying to access a certificate for a
			// standard
			if (this.returnValue instanceof Certificate) {
				List<Instrument> instruments = certService.getRelatedInstruments((Certificate) this.returnValue);
				if (instruments.stream().anyMatch(i -> Boolean.TRUE.equals(i.getCalibrationStandard()))) {
					return;
				}
			}

			if (this.returnValue instanceof ComponentEntity) {
				this.identifier = ((ComponentEntity) this.returnValue).getIdentifier();
				this.contactOwner = ((ComponentEntity) this.returnValue).getDefaultContact();
			}

			if (this.contactOwner == null) {
				this.contactOwner = this.findContactOwner();
			}

			if (this.contactOwner == null) {
				throw new Exception("Owning contact cannot be found");
			}

			this.doContactChecks();
			this.doAddressChecks();
		}
	}

	public Contact findContactOwner() {
		if (this.returnValue instanceof Certificate) {
			Certificate c = (Certificate) this.returnValue;

			Set<CertLink> cls = c.getLinks();
			Set<InstCertLink> ils = c.getInstCertLinks();

			if ((cls != null) && (cls.size() > 0)) {
				for (CertLink cl : cls) {
					return cl.getJobItem().getInst().getCon();
				}
			} else if ((ils != null) && (ils.size() > 0)) {
				for (InstCertLink il : ils) {
					return il.getInst().getCon();
				}
			}
		}

		return null;
	}

	public void setSessionServ(SessionUtilsService sessionServ) {
		this.sessionServ = sessionServ;
	}

}