package org.trescal.cwms.web.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.login.db.LoginService;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.UserGroupPropertyValue;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;

public class WebLogWriter
{
	@Autowired
	private LoginService loginService;
	
	public void addAjaxCallToLog(String logFilePath, String ajaxCall)
	{
		File file = new File(logFilePath);
		PrintWriter out = null;

		try
		{
			out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
			out.println("    Ajax  : " + ajaxCall);
			out.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (out != null)
				{
					out.close();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public void addURLToLog(String logFilePath, String url, String queryString)
	{
		File file = new File(logFilePath);
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
			out.println("Location  : " + url);
			if ((queryString != null) && !queryString.trim().isEmpty()) {
				String[] params = queryString.split("&");
				for (String param : params) {
					param = param.replaceAll("=", " = ");
					out.println("   Param  : " + param);
				}
			}
			out.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (out != null) out.close();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public File createLog(User user, HttpServletRequest request)
	{
		String webLogFolder = request.getSession().getServletContext().getRealPath("WEB-INF" + File.separator + "temp");
		String readRule = "";
		String updateRule = "";
		UserGroup group = user.getCon().getUsergroup();
		for(UserGroupPropertyValue ugpv : group.getProperties()) {
			if (ugpv.getUserGroupProperty().equals(UserGroupProperty.VIEW_LEVEL)) readRule = ugpv.getScope().name();
			if (ugpv.getUserGroupProperty().equals(UserGroupProperty.UPDATE_INSTRUMENT)) updateRule = ugpv.getScope().name();
		}
		Contact con = user.getCon();
		SimpleDateFormat tf = new SimpleDateFormat("hh.mm.ss a");
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		Calendar cal = new GregorianCalendar();
		int year = cal.get(Calendar.YEAR);
		String monthStr = DateTools.getMonthInWords(cal.get(Calendar.MONTH));
		int month = cal.get(Calendar.MONTH) + 1;
		String monthNum = (month < 10) ? "0" + String.valueOf(month) : String.valueOf(month);
		String ddMMyyyy = df.format(cal.getTime());
		String time = tf.format(cal.getTime());
		String path = String.valueOf(year).concat(File.separator).concat(monthNum
				+ " - " + monthStr).concat(File.separator).concat(ddMMyyyy);
		File dir = new File(webLogFolder.concat(File.separator).concat(path));
		dir.mkdirs();
		File file = new File(dir.getAbsolutePath().concat(File.separator).concat(time
				+ " - " + con.getName()).concat(".txt"));
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
			out.println("Username  : " + user.getUsername());
			out.println("Contact   : " + con.getName() + " (ID: "
					+ con.getPersonid() + ")");
			out.println("Subdiv    : " + con.getSub().getSubname() + " (ID: "
					+ con.getSub().getSubdivid() + ")");
			out.println("Company   : " + con.getSub().getComp().getConame()
					+ " (ID: " + con.getSub().getComp().getCoid() + ")");
			out.println("IP        : " + request.getRemoteAddr());
			out.println("Read      : " + readRule);
			out.println("Update    : " + updateRule);
			out.println();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (out != null) out.close();
				request.getSession().setAttribute(Constants.WEB_LOG_FILEPATH, file.getAbsolutePath());
				loginService.recordWebLogin(user.getCon(), true, file.getAbsolutePath());
			}
			catch (Exception e) {
				e.printStackTrace();
				loginService.recordWebLogin(user.getCon(), false, null);
			}
		}
		return file;
	}
}