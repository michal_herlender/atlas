package org.trescal.cwms.web.utilities.controllers;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Controller
@JsonController
public class SearchJsonTags {
	@Autowired
	private NewDescriptionService descriptionService;
	@Autowired
	private SalesCategoryService salesCatServ;
	@Autowired
	private InstrumentModelFamilyService instModFamServ;
	@Autowired
	private InstrumentModelDomainService instModDomainServ;
	@Autowired
	private MfrService manufacturerService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private AddressService addressService;

	public static int MAX_RESULTS = 20;

	@RequestMapping(value = "/searchdescriptiontags.json", method = RequestMethod.GET)
	public @ResponseBody List<KeyValueIntegerString> getDescriptionTagList(
			@RequestParam("term") String descriptionFragment, Locale userLocale) {
		return descriptionService.getAllKeyValues(descriptionFragment, null, userLocale, MAX_RESULTS);
	}

	@RequestMapping(value = "/searchsalescategorytags.json", method = RequestMethod.GET)
	public @ResponseBody List<LabelIdDTO> getSalesCategoryTagList(@RequestParam("term") String salesCategoryFragment,
			Locale userLocale) {
		return salesCatServ.getLabelIdList(salesCategoryFragment, userLocale, MAX_RESULTS);
	}

	@RequestMapping(value = "/searchfamilytags.json", method = RequestMethod.GET)
	public @ResponseBody List<KeyValueIntegerString> getFamilyTagList(@RequestParam("term") String familyFragment,
			Locale userLocale) {
		return instModFamServ.getAllKeyValues(familyFragment, null, userLocale, MAX_RESULTS);
	}

	@RequestMapping(value = "/searchdomaintags.json", method = RequestMethod.GET)
	public @ResponseBody List<KeyValueIntegerString> getTagList(@RequestParam("term") String domainFragment,
			Locale userLocale) {
		return instModDomainServ.getAllKeyValues(domainFragment, null, userLocale, MAX_RESULTS);
	}

	@RequestMapping(value = "/searchmanufacturertags.json", method = RequestMethod.GET)
	public @ResponseBody List<LabelIdDTO> getTagList(@RequestParam("term") String descriptionFragment) {
		return manufacturerService.getLabelIdList(descriptionFragment, true, MAX_RESULTS);
	}

	@RequestMapping(value = "/searchlocations.json", method = RequestMethod.GET)
	public @ResponseBody List<LabelIdDTO> getLocations(@RequestParam("coid") Integer companyId,
			@RequestParam("subdivid") Integer subdivisionId, @RequestParam("addressid") Integer addressId,
			@RequestParam("term") String searchTerm) {
		Company company = companyId != null ? companyService.get(companyId) : null;
		Subdiv subdivision = subdivisionId != null ? subdivService.get(subdivisionId) : null;
		Address address = addressId != null ? addressService.get(addressId) : null;
		return locationService.getLocationsForAutocomplete(company, subdivision, address, searchTerm);
	}
}