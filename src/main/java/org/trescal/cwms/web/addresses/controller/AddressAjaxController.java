package org.trescal.cwms.web.addresses.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.dto.AddrSearchResultWrapper;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("addressapi")
public class AddressAjaxController {

    private final AddressService addressService;

    private final SubdivService subdivService;

    public AddressAjaxController(AddressService addressService, SubdivService subdivService) {
        this.addressService = addressService;
        this.subdivService = subdivService;
    }


    @GetMapping("/activesubdivaddresses/{subdivid}")
    public List<AddrSearchResultWrapper> getSubdivAddresses(@PathVariable("subdivid") Integer subdivId) {
        return addressService.getAllActiveSubdivAddressesHQL(subdivId, "");
    }

    @GetMapping("/subdivdeliveryaddresslist/{subdivid}")
    public List<KeyValue<Integer, String>> getDeliveryAddressListForSubdiv(@PathVariable("subdivid") Integer subdivid) {
        Subdiv subdiv = subdivService.get(subdivid);
        List<Address> addresses = addressService.getAllActiveSubdivAddresses(subdiv, AddressType.WITHOUT);
        return addresses.stream().map(a -> new KeyValue<>(a.getAddrid(), a.getAddr1()))
                .collect(Collectors.toList());
    }

}
