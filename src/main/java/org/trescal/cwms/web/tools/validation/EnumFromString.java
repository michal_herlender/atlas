package org.trescal.cwms.web.tools.validation;


public class EnumFromString {

    /*
        Static method used by jackson to convert string to enum from json in REST api and provide nicer error message
        than normally would occur if passed string is not valid for the enum.
        thanks to https://www.linkedin.com/pulse/java-enum-restjson-api-ramesh-dara
     */

    public static <T extends Enum<T>> T getEnumFromString(Class<T> enumClass, String value ) {
        if(enumClass == null) {
            throw new IllegalArgumentException("Enum class value can't be null");
        }

        for (Enum<?> enumValue  : enumClass.getEnumConstants()) {
            if (enumValue.toString().equalsIgnoreCase(value)) {
                return (T) enumValue;
            }
        }

        //Create error message
        StringBuilder errorMessage = new StringBuilder();
        boolean bFirstTime = true;
        for (Enum<?> enumValue : enumClass.getEnumConstants()) {
            errorMessage.append(bFirstTime ? "" : ",").append(enumValue);
            bFirstTime = false;
        }
        throw new IllegalArgumentException(value + " is an invalid value. Supported values are " + errorMessage);
    }


}
