package org.trescal.cwms.web.tools.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.FileController;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;

@Controller
@FileController
@JsonController
public class WebDeleteFileController {
	
	@Autowired
	FileBrowserService fileBrowserService;

	@GetMapping(value = "/deletefile.json")
	@ResponseBody
	public Boolean deleteFile(@RequestParam(name = "fileName", required = false) String fileName,
			@RequestParam(name = "subdirectoryName", required = false) String subdirectoryName,
			@RequestParam(name = "scid", required = true) Integer scId,
			@RequestParam(name = "entityid", required = true) Integer entityId) {
		return fileBrowserService.deleteFile(fileName, subdirectoryName, scId, entityId);
	}

}
