package org.trescal.cwms.web.tools.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.EncryptionTools;
import org.trescal.cwms.web.tools.component.WebFileIconResolver;

@Controller @ExtranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class WebDownloadsController
{
	@Autowired
	private CompanyService compServ;
	@Autowired
	private UserService userService;
	
	public static final String SUBDIR_PORTAL_DOWNLOADS = "Portal Downloads";

	@RequestMapping(value="/downloads.htm")
	public String handleRequest(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception
	{
		// get current contact
		Contact contact = this.userService.get(username).getCon();
		// Get path to company directory
		String compDir = this.compServ.get(contact.getSub().getComp().getCoid()).getDirectory().toString();
		// Get path to downloads folder for company
		String downloadPath = compDir.concat(File.separator).concat(SUBDIR_PORTAL_DOWNLOADS);

		// get all files from the downloads path for selected company
		File downloadDir = new File(downloadPath);
		if (!downloadDir.exists())
		{
			downloadDir.mkdirs();
		}
		String[] downFiles = downloadDir.list();

		Map<String,String> files = new TreeMap<>();
		Map<String,String> icons = new TreeMap<>();
		for (String fileName : downFiles)
		{
			String filePath = downloadPath.concat(File.separator).concat(fileName); 
			String encryptedPath = URLEncoder.encode(EncryptionTools.encrypt(filePath), "UTF-8");
			String iconURLFragment = WebFileIconResolver.getIcon(fileName);
			
			files.put(fileName, encryptedPath);
			icons.put(fileName, iconURLFragment);
		}

		model.addAttribute("files", files);
		model.addAttribute("icons", icons);
		model.addAttribute("userContact", contact);

		return "/tools/downloads";
	}
}
