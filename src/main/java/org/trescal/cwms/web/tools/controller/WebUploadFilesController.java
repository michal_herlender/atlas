package org.trescal.cwms.web.tools.controller;

import java.io.File;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateAction;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.db.CertActionService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.fileupload2.service.UploadService;
import org.trescal.cwms.core.tools.fileupload2.utils.UploadInfo;
import org.trescal.cwms.core.tools.fileupload2.utils.UploadListener;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.ValidFileType;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.db.ValidFileTypeService;
import org.trescal.cwms.core.tools.fileupload2.web.ajax.AjaxFileUploadMultipartResolver;
import org.trescal.cwms.core.tools.fileupload2.web.formobjects.DataImportForm;
import org.trescal.cwms.core.tools.fileupload2.web.propertyEditors.MultipartFilePropertyEditor;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

/**
 * Controller called by the AJAX file uploader. When a file is uploaded Spring
 * will now manage it's upload via {@link AjaxFileUploadMultipartResolver},
 * which starts the {@link UploadListener} that updates the progress of the file
 * upload via a session variable called 'uploadInfo' which resolves to an
 * instance of {@link UploadInfo}. {@link UploadInfo} contains the current
 * progress data on the upload (which is updated by the {@link UploadListener})
 * and extra file information (such as the status of the upload and any errors
 * encountered). This controller is only called once the files have been
 * uploaded. It's purpose is to validate that the file uploaded is allowed and
 * then to copy the file to the requested location - a {@link SystemComponent}
 * file location.
 * 
 * same Controller like in package
 * org.trescal.cwms.core.tools.fileupload2.web.controller
 * 
 * @author Gunnar Hillert, extended by Richard Dean.
 */
@Controller
@ExtranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class WebUploadFilesController {
	private static final Logger logger = LoggerFactory.getLogger(WebUploadFilesController.class);

	@Autowired
	private CertActionService certActionServ;
	@Autowired
	private CertificateService certServ;
	@Autowired
	ComponentDirectoryService compDirServ;
	@Autowired
	private SystemComponentService systemCompServ;
	/**
	 * Hold a reference to the service. Used e.g. to save the file.
	 */
	@Autowired
	private UploadService service;
	@Autowired
	private UserService userService;
	@Autowired
	private ValidFileTypeService vftServ;
	@Autowired
	private MessageSource messages;

	/**
	 * Register proerty editor for handling multipart files.
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param binder
	 *            ServletRequestDataBinder
	 * @throws ServletException
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws ServletException {
		binder.registerCustomEditor(MultipartFile.class, new MultipartFilePropertyEditor());
	}

	@ModelAttribute("command")
	protected DataImportForm formBackingObject() {
		return new DataImportForm();
	}

	/**
	 * Spring-MVC method - executed on form submission.
	 * 
	 * @param dataImportForm
	 *            DataImportForm
	 * @param request
	 *            HttpServletRequest
	 * @param locale
	 *            Locale
	 * @throws Exception
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/uploadfile.json", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public final RestResultDTO<String> onSubmitt(HttpServletRequest request,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("command") DataImportForm dataImportForm, Locale locale) throws Exception {
		RestResultDTO<String> res = new RestResultDTO<String>();
		if (request.getParameter("cancel") != null) {
			logger.info("Canceling operation.");
		} else {
			MultipartFile multipartFile = dataImportForm.getFile();
			if (multipartFile == null) {
				// file is empty
				return new RestResultDTO<String>(false, this.messages.getMessage("error.instrument.upload.file.empty",
						null, "The file provided is empty!", locale));
			} else {
				UploadInfo info = new UploadInfo();
				if (request.getSession().getAttribute("uploadInfo") != null) {
					info = (UploadInfo) request.getSession().getAttribute("uploadInfo");
				}
				if (!this.vftServ.isValidFileType(multipartFile, ValidFileType.FileTypeUsage.GENERAL)) {
					// invalid file type
					logger.warn("The file type is invalid.");
					info.setStatus("filetypeerror");
					return new RestResultDTO<String>(false, this.messages.getMessage("errors.upload.extension.invalid",
							null, "invalid file type", locale));
				} else {
					SystemComponent comp = this.systemCompServ
							.findComponent(Integer.parseInt(dataImportForm.getCompid()));
					Integer version = null;
					if (dataImportForm.getCompEntityVersion() != null) {
						try {
							version = Integer.parseInt(dataImportForm.getCompEntityVersion());
						} catch (NumberFormatException ex) {
							logger.error(ex.getMessage());
							;
						}
					}
					File toDirectory = this.compDirServ.getDirectory(comp, dataImportForm.getCompEntityId(), version);
					if (dataImportForm.getSubdirectory() != null) {
						toDirectory = new File(toDirectory.getAbsolutePath().concat(File.separator)
								.concat(dataImportForm.getSubdirectory()));
						toDirectory.mkdirs();
					}
					String newName = multipartFile.getOriginalFilename();
					if ((dataImportForm.getRenameTo() != null) && !dataImportForm.getRenameTo().trim().isEmpty()) {
						newName = dataImportForm.getRenameTo();
						int extChar = multipartFile.getOriginalFilename().lastIndexOf(".");
						newName = newName.concat(multipartFile.getOriginalFilename().substring(extChar,
								multipartFile.getOriginalFilename().length()));
					}
					info.setFileName(newName);
					this.service.saveFile(multipartFile.getInputStream(), toDirectory, newName);
					info.setStatus("done");
					// if a file is being uploaded to a certificate directory
					// AND it
					// begins with a 'T' AND is a PDF file
					if (comp.getComponent().equals(Component.CERTIFICATE) && newName.startsWith("T")
							&& newName.endsWith(".pdf")) {
						Contact contact = userService.get(username).getCon();
						Certificate cert = this.certServ.findCertificateByCertNo(dataImportForm.getRenameTo());
						// it is most likely a TP cert file so log the upload
						// action
						this.certActionServ.logCertAction(cert, CertificateAction.TPUPLOAD, contact);
					}
				}
				request.getSession().setAttribute("uploadInfo", info);
			}
		}
		// no errors found
		res.setSuccess(true);
		res.setMessage(this.messages.getMessage("success.upload", new Object[] {}, locale));
		return res;
	}
}
