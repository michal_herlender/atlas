package org.trescal.cwms.web.tools.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Locale;

import javax.activation.MimetypesFileTypeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.documents.usermanual.entity.UserManual;
import org.trescal.cwms.core.documents.usermanual.entity.db.UserManualService;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Controller @ExtranetController
public class WebUserManualDownload {
	
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@Autowired
	private UserManualService userManualService;
	
	private static final Logger logger = LoggerFactory.getLogger(WebUserManualDownload.class);
	
	@RequestMapping(value = "/downloadusermanual", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Resource> getUserManual(Locale locale) throws IOException {
    	logger.debug("Obtaining user manual for locale "+locale);
    	UserManual userManual = userManualService.getUserManual(locale);
    	if (userManual == null) {
    		Locale primaryLocale = supportedLocaleService.getPrimaryLocale();
    		userManual = userManualService.getUserManual(primaryLocale);
    		if (userManual == null) throw new RuntimeException("No user manual loaded for either primary or user locale");
    	}
    	
    	byte[] fileBinary = userManual.getFileBinary();
    	logger.debug("Returning "+fileBinary.length+" bytes");
    	
    	MimetypesFileTypeMap mimeTypeMap = new MimetypesFileTypeMap();
		return ResponseEntity.ok()
				.header("Content-Disposition", "attachment; filename="+URLEncoder.encode(userManual.getFilename(), "UTF-8"))
				.contentLength(fileBinary.length)
	    		.contentType(MediaType.parseMediaType(mimeTypeMap.getContentType(userManual.getFilename())))
	    		.body(new ByteArrayResource(fileBinary));
	}
}
