package org.trescal.cwms.web.tools.component;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

/**
 * Defines file icons available for downloads and resolves for a provided filename
 */
public class WebFileIconResolver {
	public static String DIRECTORY = "img/doctypes";
	public static String ICON_PDF = "pdf.png"; 
	public static String ICON_DOC = "doc.png"; 
	public static String ICON_HTML = "html.png";
	public static String ICON_XLS = "xls.png";
	public static String ICON_TXT = "txt.png";
	public static String ICON_IMAGE = "image.png";
	public static String ICON_OTHER = "other.png";
	
	public static Map<String,String> extensionsToIcons;
	static {
		extensionsToIcons = new HashMap<>();
		extensionsToIcons.put("pdf",ICON_PDF);
		extensionsToIcons.put("docx",ICON_DOC);
		extensionsToIcons.put("doc",ICON_DOC);
		extensionsToIcons.put("xlsx",ICON_XLS);
		extensionsToIcons.put("xls",ICON_XLS);
		extensionsToIcons.put("html",ICON_HTML);
		extensionsToIcons.put("htm",ICON_HTML);
		extensionsToIcons.put("txt",ICON_TXT);
		extensionsToIcons.put("png",ICON_IMAGE);
		extensionsToIcons.put("gif",ICON_IMAGE);
		extensionsToIcons.put("jpg",ICON_IMAGE);
		extensionsToIcons.put("bmp",ICON_IMAGE);
	}
	
	public static String getIcon(String filename) {
		String extension = FilenameUtils.getExtension(filename).toLowerCase();
		String icon = extensionsToIcons.get(extension);
		if (icon == null) {
			icon = ICON_OTHER;
		}
		String result = DIRECTORY.concat(File.separator).concat(icon);
		return result;
	}
	
}
