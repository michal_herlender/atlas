package org.trescal.cwms.web.tools.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.FileController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.labelprinting.LabelService;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.web.instrument.form.AccessInstrumentHistoryValidator;

@Controller
@FileController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_USERNAME })
public class WebBirtLabelController {

	@Resource(name = "BirtLabelService")
	private LabelService labelService;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private InstrumService instService;
	@Autowired
	private AccessInstrumentHistoryValidator validator;
	@Autowired
	protected UserService userService;

	@RequestMapping(value = "/custcallabel", method = RequestMethod.GET)
	public void downloadCustCalLabel(@RequestParam("certId") int certId, @RequestParam("instrumentId") int instrumentId,
			@RequestParam("templateKey") String templateKey,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyKeyValue,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, HttpServletResponse response,
			BindingResult bindingResult, HttpSession session) throws IOException, EngineException {

		Instrument inst = instService.get(instrumentId);
		Company comp = userService.get(username).getCon().getSub().getComp();
		validator.validate(inst, bindingResult, comp);

		if (bindingResult.hasErrors()) {
			throw new RuntimeException("Instrument not found!!");
		} else {
			ByteArrayOutputStream baos = new ByteArrayOutputStream(10 * 1024);

			if (labelService.printCustCalLabel(certId, templateKey, instrumentId, LocaleContextHolder.getLocale(), baos,
					compServ.get(companyKeyValue.getKey()))) {
				String filename = "callabel_" + templateKey + "_" + certId + ".pdf";
				response.setContentType(URLConnection.guessContentTypeFromName(filename));
				response.setContentLength(baos.size());
				response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

				FileCopyUtils.copy(baos.toByteArray(), response.getOutputStream());
			}
		}
	}
}