package org.trescal.cwms.web.quotations.dto;

public class WebQuoteFieldWrapper
{
	private String poNumber;

	public WebQuoteFieldWrapper(String poNumber)
	{
		this.poNumber = poNumber;
	}

	public String getPoNumber()
	{
		return this.poNumber;
	}

	public void setPoNumber(String poNumber)
	{
		this.poNumber = poNumber;
	}

}