package org.trescal.cwms.web.quotations.form.genericentityvalidator;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.web.quotations.dto.WebQuoteFieldWrapper;

public class WebQuoteValidator implements Validator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(WebQuoteFieldWrapper.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		WebQuoteFieldWrapper quoteFW = (WebQuoteFieldWrapper) target;
		if (errors == null)
		{
			errors = new BindException(quoteFW, "quote");
		}

		// check the purchase order field for length violation
		if (quoteFW.getPoNumber().length() > 60)
		{
			errors.rejectValue("poNumber", null, "Maximum length of purchase order field is 60 characters, please amend");
		}
	}

}
