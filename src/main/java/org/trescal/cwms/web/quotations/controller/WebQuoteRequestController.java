package org.trescal.cwms.web.quotations.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.service.ConvertToQuotationUtils;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.db.RecallDetailService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.web.EmailContent_QuoteRequest;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.SystemTime;
import org.trescal.cwms.web.quotations.form.WebQuoteRequestForm;
import org.trescal.cwms.web.utilities.formutils.WebRequestFormItemDTO;
import org.trescal.cwms.web.utilities.formutils.WebRequestItemDTOFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@ExtranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class WebQuoteRequestController {
	// TODO Refactor - not thread safe!!!
	private String message = null;

	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private ConvertToQuotationUtils convertToQuotationUtils;
	@Autowired
	private EmailContent_QuoteRequest ecQuoteRequest;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private QuotationItemService quotationItemService;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private RecallDetailService rdServ;
	@Autowired
	private StatusService statusService;
	@Autowired
	private UserService	userService;
	@Autowired
	private TaxCalculator taxCalculator;
	
	protected Quotation createAndMailQuoteRequest(WebQuoteRequestForm form, Contact currentContact, Subdiv allocatedSubdiv) throws Exception
	{
		Contact cwmsAutoUser = this.userService.findSystemContact();
		
		// this assumes all of our validation has been done
		Quotation quotation = this.createQuotationFromWebQuoteRequest(form, currentContact, allocatedSubdiv);
		Subdiv subdiv = currentContact.getSub().getComp().getCompanyRole().equals(CompanyRole.BUSINESS) ? currentContact.getSub() : currentContact.getSub().getComp().getDefaultBusinessContact().getSub();
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(subdiv);
		String quoteEmail = bdetails.getQuoteEmail();
		String salesEmail = bdetails.getSalesEmail(); 
		
		Locale conLocale = currentContact.getLocale();
		EmailContentDto internalDto = this.ecQuoteRequest.getContent(quotation, conLocale, EmailContent_QuoteRequest.EmailType.INTERNAL);
		
		// send an internal mail notifying of this quotation request
		// send the e-mail as a systemcomponent so the mail is saved to the new quotation
		this.emailServ.sendEmailAsComponentEmail(Component.QUOTATION, quotation, quotation.getId(), internalDto.getSubject(), quoteEmail, salesEmail, null, internalDto.getBody(), cwmsAutoUser);

		// send a confirmation to the user
		if (!StringUtils.isEmpty(currentContact.getEmail())) {
			// send contact a mail notifying of this quotation request
			EmailContentDto clientDto = this.ecQuoteRequest.getContent(quotation, conLocale, EmailContent_QuoteRequest.EmailType.CLIENT);
			this.emailServ.sendEmailAsComponentEmail(Component.QUOTATION, quotation, quotation.getId(), clientDto.getSubject(), quoteEmail, null, currentContact, clientDto.getBody(), cwmsAutoUser);
		}
		return quotation;
	}
	
	protected Quotation createQuotationFromWebQuoteRequest(WebQuoteRequestForm form, Contact currentContact, Subdiv allocatedSubdiv) throws Exception
	{
		Locale locale = LocaleContextHolder.getLocale();
		Quotation quotation = this.quoteServ.createTemplateNewQuotation(currentContact, currentContact, allocatedSubdiv, allocatedSubdiv.getComp(), locale);
		quotation.setCreatedBy(currentContact);
		quotation.setSourcedBy(currentContact.getSub().getComp().getDefaultBusinessContact());

		if (quotation.getNotes() == null)
		{
			quotation.setNotes(new TreeSet<QuoteNote>(new NoteComparator()));
		}

		// hhmmm hardcoded badness, @TODO. Sort. QuotationStatusGroup maybe?
		quotation.setQuotestatus(((QuoteStatus) this.statusService.findStatusByName("Quote created on web-site, requires approval", QuoteStatus.class)));

		// persist the basic quote first, then we can add items etc next
		this.quoteServ.save(quotation);
		QuoteHeading heading = quotation.getQuoteheadings().iterator().next();

		// add our items
		if (form.getRequestItems() != null)
		{
			int itemcount = 1;
			for (Object rItem : form.getRequestItems())
			{
				WebRequestFormItemDTO requestItem = (WebRequestFormItemDTO) rItem;

				if (requestItem != null)
				{
					JobItem ji = null;
					// plantid supplied?
					if ((requestItem.getPlantid() != null)
							&& (requestItem.getPlantid() != 0))
					{
						requestItem.setInst(this.instServ.get(requestItem.getPlantid()));
						// look for the most recent jobitem for this instrument
						// (there may or
						// may not be a jobitem - instruments can be recalled
						// without having
						// ever appeared on a job)
						ji = this.jobItemServ.findMostRecentJobItem(requestItem.getPlantid());
					}
					// grab a new pre-initialised quote item
					Quotationitem item = this.quotationItemService.createTemplateNewQuotationItem(quotation, heading, requestItem.getInst(), requestItem.getModelid(), requestItem.getCaltypeid(), allocatedSubdiv.getSubdivid());
					// job item not null?
					if (ji != null) {
						if (ji.getCalType().getCalTypeId() == requestItem.getCaltypeid().intValue()) {
							// work out the cost of the item based on it's most
							// recent cal cost (or
							// model defaults if no previous costing)
							this.prepareCalibrationCost(requestItem.getInst(), ji, item.getCalibrationCost(), allocatedSubdiv);
						}
					}
					// update item costs
					CostCalculator.updateItemCostWithRunningTotal(item);

					item.setQuantity(requestItem.getQty());
					item.setItemno(itemcount);
					itemcount++;

					// set comments for this item as item notes - set
					// unpublished
					// and add a label indicating their source
					if (StringUtils.isNotEmpty(requestItem.getComment()))
					{
						String label = "Website Item Comment";
						QuoteItemNote note = this.quotationItemService.createTemplateNewQuotationItemNote(item, true, label, requestItem.getCommentDesc(), false, quotation.getContact());
						item.getNotes().add(note);
					}
					quotation.getQuotationitems().add(item);

				}
			}

            //this assumes only one heading but so does the existing line 174
            //have to create a new list to stop HibernateException: Found shared references to a collection:
            Set<Quotationitem> items = new TreeSet<>(new QuotationItemComparator());
            items.addAll(quotation.getQuotationitems());
            heading.setQuoteitems(items);
		}

		// add supplied po number to quotation notes
		if ((form.getPoNumber() != null) && (!form.getPoNumber().isEmpty()))
		{
			QuoteNote qnote = new QuoteNote();
			qnote.setActive(true);
			qnote.setLabel("Purchase order number supplied");
			qnote.setNote(form.getPoNumber());
			qnote.setPublish(false);
			qnote.setQuotation(quotation);
			qnote.setSetBy(currentContact);
			qnote.setSetOn(new Date());

			quotation.getNotes().add(qnote);
		}

		// sort the items
		this.quotationItemService.sortQuotationItems(quotation);

		// update the quotation costs
		CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
		taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);

		// update our changes
		quotation = this.quoteServ.merge(quotation);

		System.out.println("persisted new quotation " + quotation.getQno()
				+ " with id " + quotation.getId());
		return quotation;
	}

	@ModelAttribute("command")
	protected WebQuoteRequestForm formBackingObject(HttpServletRequest request) throws Exception
	{
		WebQuoteRequestForm form = new WebQuoteRequestForm();

		AutoPopulatingList<WebRequestFormItemDTO> list = new AutoPopulatingList<WebRequestFormItemDTO>(new WebRequestItemDTOFactory());
		form.setRequestItems(list);

		form.setMessage(this.message);
		this.message = null;

		// page requested from recall document?
		int rdid = ServletRequestUtils.getIntParameter(request, "rdid", 0);
		// if we have then load recall details
		if (rdid != 0)
		{
			// find the recall detail
			RecallDetail rdetail = this.rdServ.get(rdid);
			// check recall detail can be loaded
			if (rdetail == null)
			{
				throw new Exception("Recall details could not be found");
			}
			// set recall detail on form
			form.setRdetail(rdetail);
		}

		return form;
	}

	@RequestMapping(value="/quoterequest.htm", method=RequestMethod.POST)
	public ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("command") WebQuoteRequestForm form, 
			BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData(username);
		Contact currentContact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = currentContact.getSub().getComp().getDefaultBusinessContact().getSub();

		try {
			this.createAndMailQuoteRequest(form, currentContact, allocatedSubdiv);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		this.message = "Thank you for your quotation request.<br />Details of your request can be viewed below and a confirmation email has been sent to your mailbox";
		return new ModelAndView(new RedirectView("/web/quoterequest.htm", true));
	}
	
	protected void prepareCalibrationCost(Instrument inst, JobItem ji, QuotationCalibrationCost qCalCost, Subdiv subDiv)
	{
		if (ji != null)
		{
			this.convertToQuotationUtils.prepareCalibrationCostFromJobItem(ji, qCalCost);
		}
		else
		{
			// prepare from instrument
				this.convertToQuotationUtils.prepareCalibrationCostFromModel(inst, qCalCost, subDiv);
		}
	}

	@RequestMapping(value="/quoterequest.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();
		Contact contact = this.userService.get(username).getCon();
		Subdiv businessSubdiv = contact.getSub().getComp().getDefaultBusinessContact().getSub(); 

		refData.put("userContact", contact);
		refData.put("businessCompany", businessSubdiv.getComp().getConame());
		refData.put("businessTel", businessSubdiv.getDefaultAddress() != null ? businessSubdiv.getDefaultAddress().getTelephone() : "");
		refData.put("businessSubdiv", businessSubdiv);
		
		// get date minus one year for quotation retrieval
		Calendar cal = SystemTime.nowCal();
		cal.add(Calendar.YEAR, -1);
		Date date = cal.getTime();

		refData.put("requestedQuotations", this.quoteServ.getQuotationsRequestForContactFromReqdate(contact.getPersonid(), DateTools.dateToLocalDate(date), false, false));
		refData.put("issuedQuotations", this.quoteServ.getIssuedQuotationsForContactFromIssuedate(contact.getPersonid(), DateTools.dateToLocalDate(date), false));
		refData.put("acceptedQuotations", this.quoteServ.getIssuedQuotationsForContactFromIssuedate(contact.getPersonid(), DateTools.dateToLocalDate(date), true));

		return new ModelAndView("/quotations/webcreatequoterequest", refData);
	}
}
