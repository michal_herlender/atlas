package org.trescal.cwms.web.quotations.form;

import java.util.List;

import org.springframework.util.AutoPopulatingList;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;

public class WebQuoteRequestForm
{
	private List<String> caltype;
	private List<String> comment;
	private List<String> commentdesc;
	private String message;
	private List<String> modelid;
	private List<String> plantid;
	private List<String> plantno;
	private String poNumber;
	private List<Integer> qty;
	private List<String> quotetype;
	private RecallDetail rdetail;
	private AutoPopulatingList<?> requestItems;
	private List<String> serialno;

	public List<String> getCaltype()
	{
		return this.caltype;
	}

	public List<String> getComment()
	{
		return this.comment;
	}

	public List<String> getCommentdesc()
	{
		return this.commentdesc;
	}

	public String getMessage()
	{
		return this.message;
	}

	public List<String> getModelid()
	{
		return this.modelid;
	}

	public List<String> getPlantid()
	{
		return this.plantid;
	}

	public List<String> getPlantno()
	{
		return this.plantno;
	}

	public String getPoNumber()
	{
		return this.poNumber;
	}

	public List<Integer> getQty()
	{
		return this.qty;
	}

	public List<String> getQuotetype()
	{
		return this.quotetype;
	}

	public RecallDetail getRdetail()
	{
		return this.rdetail;
	}

	public AutoPopulatingList<?> getRequestItems()
	{
		return this.requestItems;
	}

	public List<String> getSerialno()
	{
		return this.serialno;
	}

	public void setCaltype(List<String> caltype)
	{
		this.caltype = caltype;
	}

	public void setComment(List<String> comment)
	{
		this.comment = comment;
	}

	public void setCommentdesc(List<String> commentdesc)
	{
		this.commentdesc = commentdesc;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setModelid(List<String> modelid)
	{
		this.modelid = modelid;
	}

	public void setPlantid(List<String> plantid)
	{
		this.plantid = plantid;
	}

	public void setPlantno(List<String> plantno)
	{
		this.plantno = plantno;
	}

	public void setPoNumber(String poNumber)
	{
		this.poNumber = poNumber;
	}

	public void setQty(List<Integer> qty)
	{
		this.qty = qty;
	}

	public void setQuotetype(List<String> quotetype)
	{
		this.quotetype = quotetype;
	}

	public void setRdetail(RecallDetail rdetail)
	{
		this.rdetail = rdetail;
	}

	public void setRequestItems(AutoPopulatingList<?> requestItems)
	{
		this.requestItems = requestItems;
	}

	public void setSerialno(List<String> serialno)
	{
		this.serialno = serialno;
	}

}
