package org.trescal.cwms.web.contacts.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactDWRService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.spring.model.KeyValue;

@RestController
@RequestMapping("contactapi")
public class ContactAjaxController {

    ContactDWRService contactDWRService;

    ContactService contactService;

    public ContactAjaxController(ContactDWRService contactDWRService,
                                 ContactService contactService) {
        this.contactDWRService = contactDWRService;
        this.contactService = contactService;
    }

    @GetMapping("/activesubdivcontacts/{subdivid}")
    public List<ContactSearchResultWrapper> getSubdivContacts(@PathVariable("subdivid") Integer subdivId) {
        return contactDWRService.getAllActiveSubdivContactsHQL(subdivId);
    }

    @GetMapping("/subdivcontactlist/{subdivid}")
    public List<KeyValue<Integer,String>> getContactsForSubdiv(@PathVariable("subdivid") Integer subdivid){
        List<Contact> contacts = contactService.getAllSubdivContacts(subdivid);
        return contacts.stream()
                .map(c->new KeyValue<Integer,String>(c.getPersonid(),c.getName()))
                .collect(Collectors.toList());
    }

}
