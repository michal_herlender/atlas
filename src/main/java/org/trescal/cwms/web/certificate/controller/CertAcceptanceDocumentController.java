package org.trescal.cwms.web.certificate.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.exception.WebPermissionException;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.tools.BirtEngine;

@Controller @ExtranetController
@RequestMapping("/certacceptancedoc.htm")
public class CertAcceptanceDocumentController {
	@Autowired
	private BirtEngine birtEngine;
	
	private static final String REPORT_PATH = "/birt/scripted/certificate/cert_acceptance.rptdesign";
	
	@GetMapping
	public ResponseEntity<byte[]> produceDocument(Locale locale,
			@RequestParam("certid") Integer certid) throws IOException, EngineException, WebPermissionException {

	    String filename = certid + "_acceptance.pdf";
	    Map<String, Object> parameters = new HashMap<>();
		parameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID,certid);

		//Create binary contents
		byte[] pdf = birtEngine.generatePDFAsByteArray(REPORT_PATH, parameters, locale);
		
		//Create headers
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.parseMediaType("application/pdf"));
	    headers.setContentDispositionFormData(filename, filename);
	    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		
		//create response
		ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
	    return response;		
	}
}
