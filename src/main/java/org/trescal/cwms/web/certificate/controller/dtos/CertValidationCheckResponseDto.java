package org.trescal.cwms.web.certificate.controller.dtos;

import java.util.List;

public class CertValidationCheckResponseDto {

    List<SimpleCertificateDetailsDto> notValidated;
    List<SimpleCertificateDetailsDto> validated;

    public CertValidationCheckResponseDto(List<SimpleCertificateDetailsDto> notValidated, List<SimpleCertificateDetailsDto> validated) {
        this.notValidated = notValidated;
        this.validated = validated;
    }

    public List<SimpleCertificateDetailsDto> getNotValidated() {
        return notValidated;
    }

    public List<SimpleCertificateDetailsDto> getValidated() {
        return validated;
    }

}
