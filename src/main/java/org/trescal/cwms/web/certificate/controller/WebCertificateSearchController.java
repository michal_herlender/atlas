package org.trescal.cwms.web.certificate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateSearchWrapper;
import org.trescal.cwms.core.jobs.certificate.dto.WebCertificateInstrumentLinkProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.dto.WebCertificateJobItemLinkProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CustomerCertificateValidation;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.web.certificate.form.WebCertificateSearchForm;
import org.trescal.cwms.web.certificate.form.WebCertificateSearchValidator;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@ExtranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USER_CERTVAL_RULE, Constants.SESSION_ATTRIBUTE_USERNAME})
public class WebCertificateSearchController {
    @Autowired
    private AddressService addrServ;
    @Autowired
    private CalibrationTypeService calTypeServ;
    @Autowired
	private CertificateService certServ;
	@Autowired
	private ContactService contServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private WebCertificateSearchValidator validator;
	@Autowired
	private UserService usrServ;
	@Autowired
	private CustomerCertificateValidation certValidationSystemDefault;
	@Autowired
	private CalibrationVerification calVerificationStatusSystemDefault;
	@Autowired
	private Deviation deviationSystemDefault;

	@ModelAttribute("certvalidation")
	protected Boolean showCertValidation(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		User user = usrServ.get(username);
		return certValidationSystemDefault.getValueByScope(Scope.COMPANY, user.getCon().getSub().getComp(), null);
	}

	@ModelAttribute("calstatus")
	protected Boolean showCalStatus(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		User user = usrServ.get(username);
		Company userCompany = user.getCon().getSub().getComp();
        return calVerificationStatusSystemDefault.getValueByScope(Scope.COMPANY, userCompany, null);
	}

	@ModelAttribute("deviation")
	protected Boolean customerRequiresDeviationFigure(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		User user = usrServ.get(username);
		return deviationSystemDefault.getValueByScope(Scope.COMPANY, user.getCon().getSub().getComp(), null);
	}

	@ModelAttribute("certValidationStatuses")
	protected List<KeyValue<String, String>> getAllCertificateValidationStatuses() {
        return Arrays.stream(CertificateValidationStatus.values())
            .map(v -> new KeyValue<>(v.name(), v.getName())).collect(Collectors.toList());
	}
	
	@ModelAttribute("calVerificationStatuses")
	protected List<KeyValue<String, String>> getAllCalVerificationStatuses() {
        return Arrays.stream(CalibrationVerificationStatus.values())
            .map(v -> new KeyValue<>(v.name(), v.getName())).collect(Collectors.toList());
	}

	@ModelAttribute("form")
	protected WebCertificateSearchForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		WebCertificateSearchForm form = new WebCertificateSearchForm();
		// only ever allowed to search for certs belonging to this users company
		User user = usrServ.get(username);
		form.setCoid(user.getCon().getSub().getComp().getCoid());
		return form;
	}

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/certsearch.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit2(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USER_CERTVAL_RULE) String certValRule,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value = "prev", required = false) String prev,
			@RequestParam(value = "next", required = false) String next,
			@RequestParam(value = "hiddenSelectSubmit", required = false) String hiddenSelectSubmit,
			@Validated @ModelAttribute("form") WebCertificateSearchForm form, BindingResult bindingResult,
			HttpServletResponse response, Locale locale) throws Exception {
		if (bindingResult.hasErrors())
			return referenceData(username);

        // sets a hidden field to show if this is the very first page of results or if
        // we have paged forwards or backwards to get here
        // used by javascript to empty list of selected certificates that is used for
        // bulk certificate download and bulk certificate acceptance
        form.setInitialResultsPage(prev == null && next == null && hiddenSelectSubmit == null);

        // only ever allowed to search for certs belonging to this users company
        User user = usrServ.get(username);
        form.setCoid(user.getCon().getSub().getComp().getCoid());
        PagedResultSet<CertificateSearchWrapper> rs = new PagedResultSet<>(
            form.getResultsPerPage(), form.getPageNo());
        this.certServ.searchCertificatesProjectionJPA(form, rs, false, locale);

        // is the user allowed to validate certificates on this instrument?
        for (CertificateSearchWrapper o : rs.getResults()) {
            if (o != null) {
                if (certValRule.equals("NONE"))
                    o.setCreateCertVal(false);
                else if (certValRule.equals("COMPANY")) {
                    o.setCreateCertVal(true);
                } else {
                    o.setCreateCertVal(false);
                    boolean canValidate = o.getCertDto().getJobitemLinkDtos().size() > 0
                        || o.getCertDto().getInstrumentLinkDtos().size() > 0;
                    if (canValidate) {
                        switch (certValRule) {
                            case "ADDRESS":
                                for (WebCertificateJobItemLinkProjectionDTO i : o.getCertDto().getJobitemLinkDtos()) {
                                    if (!i.getInstDto().getAddressId().equals(user.getCon()
                                        .getDefAddress().getAddrid())) {
                                        canValidate = false;
                                        break;
                                    }
                                }
                                if (canValidate) {
                                    for (WebCertificateInstrumentLinkProjectionDTO i : o.getCertDto().getInstrumentLinkDtos()) {
                                        if (!i.getAddressId().equals(user.getCon().getDefAddress()
                                            .getAddrid())) {
                                            canValidate = false;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "CONTACT":
                                for (WebCertificateJobItemLinkProjectionDTO i : o.getCertDto().getJobitemLinkDtos()) {
                                    if (!i.getInstDto().getContactId().equals(user.getCon().getId())) {
                                        canValidate = false;
                                        break;
                                    }
                                }
                                if (canValidate) {
                                    for (WebCertificateInstrumentLinkProjectionDTO i : o.getCertDto().getInstrumentLinkDtos()) {
                                        if (!i.getContactId().equals(user.getCon().getId())) {
                                            canValidate = false;
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "SUBDIV":
                                for (WebCertificateJobItemLinkProjectionDTO i : o.getCertDto().getJobitemLinkDtos()) {
                                    if (!i.getInstDto().getSubdivId().equals(user.getCon()
                                        .getSub().getId())) {
                                        canValidate = false;
                                        break;
                                    }
                                }
                                if (canValidate) {
                                    for (WebCertificateInstrumentLinkProjectionDTO i : o.getCertDto().getInstrumentLinkDtos()) {
                                        if (!i.getSubdivId().equals(user.getCon().getSub()
                                            .getId())) {
                                            canValidate = false;
                                            break;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    o.setCreateCertVal(canValidate);
                }
			}
		}
		form.setRs(rs);

		// This overrides spring security's default cache-control setting for this page
		// so that browser back and forward
		// buttons will traverse the paged results without sending another request to
		// the server
		response.setHeader("Cache-Control", "Private");

		return new ModelAndView("/jobs/certificate/newwebcertsearchresults", "command", form);
	}

	@RequestMapping(value = "/certsearch.htm", method = RequestMethod.GET)
	public ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username)
			throws Exception {
        Map<String, Object> refData = new HashMap<>();
        List<CertStatusEnum> certStatues = new ArrayList<>(Arrays.asList(CertStatusEnum.values()));
		certStatues.remove(CertStatusEnum.DELETED);
		certStatues.remove(CertStatusEnum.CANCELLED);
		refData.put("certstatuses", certStatues);
		Contact contact = usrServ.get(username).getCon();
		refData.put("userContact", contact);
		refData.put("caltypes", this.calTypeServ.getCalTypes());
		refData.put("subdivs", this.subdivServ.getAllActiveCompanySubdivs(contact.getSub().getComp()));
		refData.put("addresses", this.addrServ.getAllActiveSubdivAddresses(contact.getSub(), AddressType.WITHOUT));
		refData.put("contacts", this.contServ.getAllSubdivContacts(contact.getSub().getSubdivid()));
		return new ModelAndView("/jobs/certificate/webcertsearch", refData);
	}
	
}