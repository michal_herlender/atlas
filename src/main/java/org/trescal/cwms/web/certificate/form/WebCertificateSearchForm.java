package org.trescal.cwms.web.certificate.form;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.jobs.certificate.form.CertificateSearchForm;

import java.time.LocalDate;

@Data
public class WebCertificateSearchForm extends CertificateSearchForm {

    private Integer addrid;
    private Integer subdivid;
    private Boolean unAcceptedCertsOnly;
    private Boolean initialResultsPage;
    private Boolean acceptDateBetween;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate acceptDate1;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate acceptDate2;

    public boolean hasSubdivSearch() {
        return subdivid != null;
    }

    public boolean hasAddressSearch() {
        return addrid != null;
    }

    public boolean hasValidationSearch() {
        boolean result = unAcceptedCertsOnly != null && unAcceptedCertsOnly;
        // awaiting validation
        // accept date
        if (acceptDate1 != null) {
            result = true;
        }
        return result;
    }
}