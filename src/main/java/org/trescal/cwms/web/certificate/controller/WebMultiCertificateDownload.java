package org.trescal.cwms.web.certificate.controller;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.WebPermissionException;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


@Controller @ExtranetController
@RequestMapping("/bulkcertdownload.zip")
public class WebMultiCertificateDownload {

    @Autowired
    CertificateService certificateService;

    protected final Log logger = LogFactory.getLog(this.getClass());

    @PostMapping(produces="application/zip")
    public void produceZip(@RequestParam("certids") String certIdsString, HttpServletResponse response) throws IOException, WebPermissionException {

        String[] certIds = certIdsString.split(",");

        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", "attachment; filename \"certificates.zip\"");
        response.setContentType("application/zip");

        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());

        for (String certId : certIds) {
            Certificate certificate = certificateService.findCertificate(Integer.parseInt(certId));
            String filepath = certificate.getCertFilePath();
            
            
            if(filepath != null) {
            	File certFile = new File(filepath);
                zipOutputStream.putNextEntry(new ZipEntry(certFile.getName()));
                FileInputStream fileInputStream = new FileInputStream(certFile);

                IOUtils.copy(fileInputStream, zipOutputStream);

                fileInputStream.close();
                zipOutputStream.closeEntry();
            } else {
                logger.debug("File Not Found: " + filepath);
            }
        }

        zipOutputStream.close();
    }

}
