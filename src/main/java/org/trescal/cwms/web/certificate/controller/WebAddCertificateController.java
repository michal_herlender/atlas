package org.trescal.cwms.web.certificate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.validation.db.CertificateValidationService;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CsrEmails;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.web.certificate.form.WebAddCertificateForm;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Controller
@ExtranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
@RequestMapping("addcertificate.htm")
public class WebAddCertificateController {

	@Autowired
	UserService userService;

	@Autowired
	UserGroupService userGroupService;
	
	@Autowired
	CalibrationTypeService calTypeService;
	
	@Autowired
	CertificateService certService;
	
	@Autowired
	InstrumService instrumentService;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	RecallRuleService recallRuleService;
	
	@Autowired
	CertificateValidationService certificateValidationService;
	
	@Autowired
	private CalibrationVerification systemDefaultCalibrationVerification;

	@Autowired
    private Deviation systemDefaultDeviation;
	
	@Autowired
	private MessageSource messageSource;

	@Autowired
	private TranslationService translationService;

	@Autowired
	private TemplateEngine templateEngine;

	@Autowired
	private EmailService emailService;

	@Autowired
	private CsrEmails csrEmails;

	@InitBinder("command")
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	private final Comparator<CalibrationType> byRequirementType = Comparator.comparing(CalibrationType::getRecallRequirementType);

	@ModelAttribute("command")
	public WebAddCertificateForm initializeForm(@RequestParam(name = "instid", required = false, defaultValue = "0") Integer plantId) {
		WebAddCertificateForm webAddCertificateForm = new WebAddCertificateForm();
		webAddCertificateForm.setInstrumentId(plantId);
		return webAddCertificateForm;
	}

	@ModelAttribute("services")
	public List<CalibrationType> initializeListOfAllServiceTypes(@RequestParam(name = "instid") Integer plantid,
																 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {

		Instrument instrument = this.instrumentService.get(plantid);
		Contact userContact = userService.get(username).getCon();

		if (userGroupService.canUserCreateCalibrationForThisInstrument(userContact, instrument)) {
			return calTypeService.getCalTypes().stream().
				filter(calTypeCanBeUsedByClient()).
				sorted(byRequirementType).
				collect(Collectors.toList());
		} else {
			return calTypeService.getCalTypes().stream().
				filter(calTypeCanBeUsedByClient()).
				filter(calTypeIsFullCalibration().negate()).
				sorted(byRequirementType).
				collect(Collectors.toList());
		}
	}

	@ModelAttribute("units")
	public List<KeyValue<String, String>> getAllUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute("deviationunitsymbol")
	public String getDeviationUnitSymbol(@RequestParam(name = "instid") Integer plantid) {
		Instrument instrument = this.instrumentService.get(plantid);
		return Optional.ofNullable(instrument.getInstrumentComplementaryField())
			.flatMap(f -> Optional.ofNullable(f.getDeviationUnits()))
			.map(UoM::getSymbol).orElse("");
	}

	@ModelAttribute("CalibrationVerificationStatusRequired")
	public Boolean isCalibrationVerifiactionStatusRequired(@ModelAttribute("command") WebAddCertificateForm webAddCertificateForm) {
		Instrument instrument = instrumentService.get(webAddCertificateForm.getInstrumentId());
		return this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("deviationRequired")
	public Boolean isDeviationRequired(@ModelAttribute("command") WebAddCertificateForm webAddCertificateForm) {
		Instrument instrument = instrumentService.get(webAddCertificateForm.getInstrumentId());
		return this.systemDefaultDeviation.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String displayForm(){ return "/instrument/webaddcertificate"; }

	@RequestMapping(method=RequestMethod.POST)
	public String submitForm(@ModelAttribute("command") WebAddCertificateForm webAddCertificateForm,
			BindingResult bindingResult, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws IOException {
		if(bindingResult.hasErrors()) return this.displayForm();

		Contact contact = this.userService.get(username).getCon();
		Instrument instrument = instrumentService.get(webAddCertificateForm.getInstrumentId());
		CalibrationType calType = calTypeService.find(webAddCertificateForm.getCalTypeId());
		IntervalUnit intervalUnit = IntervalUnit.valueOf(webAddCertificateForm.getIntervalUnitId());
		
		
		if(calType.getRecallRequirementType().equals(RecallRequirementType.FULL_CALIBRATION)){
			Certificate certificate = new Certificate();
			certificate.setCertno(webAddCertificateForm.getCertNumber());
			certificate.setServiceType(calType.getServiceType());
			certificate.setType(CertificateType.CERT_CLIENT);
			certificate.setCalDate(webAddCertificateForm.getCalDate());
			certificate.setDuration(webAddCertificateForm.getInterval());
			certificate.setUnit(IntervalUnit.valueOf(webAddCertificateForm.getIntervalUnitId()));
			certificate.setCertDate(webAddCertificateForm.getCertDate());
			certificate.setCalibrationVerificationStatus(webAddCertificateForm.getCalibrationVerificationStatus());
			certificate.setRemarks(webAddCertificateForm.getRemarks());
		
			certificate.setStatus(CertStatusEnum.SIGNED);

			certificate.setDeviation(webAddCertificateForm.getDeviation());
			
			certService.insertCertificate(certificate, contact);
			
			certService.linkCertToInstrument(certificate, instrument);
		
			/*
			 * Validate certificate because as the customer is uploading it we assume they consider it valid
			 */
			CertificateValidation certificateValidation = new CertificateValidation();
			certificateValidation.setCert(certificate);
			certificateValidation.setDatevalidated(LocalDate.now());
			certificateValidation.setStatus(CertificateValidationStatus.ACCEPTED);
			certificateValidationService.save(certificateValidation);
			certificate.setValidation(certificateValidation);
			certService.updateCertificate(certificate);
			
			//set path for certificate file storage
			this.certService.setCertificateDirectory(certificate);

            certService.uploadFile(webAddCertificateForm.getFile(), certificate, contact);

            instrumentService.calculateRecallDateAfterTPCert(instrument, certificate);
			
			recallRuleService.insert( webAddCertificateForm.getInstrumentId(), webAddCertificateForm.getInterval(), 
					null, true, intervalUnit, calType.getRecallRequirementType(), false, null);
		     
            if ( this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY, instrument.getComp(), null)
                    && this.csrEmails.getValueByScope(Scope.COMPANY, instrument.getComp(),null)) {
    			this.sendNotificationToCSR(instrument,certificate,contact);
    		}
    	
		} else {
			LocalDate dueDate = DateTools.datePlusInterval(DateTools.dateToLocalDate(webAddCertificateForm.getCalDate()), webAddCertificateForm.getInterval(), intervalUnit);
			recallRuleService.insert(webAddCertificateForm.getInstrumentId(), webAddCertificateForm.getInterval(),
				dueDate, false, intervalUnit, calType.getRecallRequirementType(), false, null);

		}
        return "redirect:/web/viewinstrument.htm?plantid=" + webAddCertificateForm.getInstrumentId(); 
	}
	
	private void sendNotificationToCSR(Instrument instrument, Certificate certificate, Contact clientContact) {
		Locale locale = LocaleContextHolder.getLocale();
		Contact csr = clientContact.getSub().getComp().getDefaultBusinessContact();
		String toAddr = csr.getEmail();
	    String fromAddr = clientContact.getEmail();
	    String subject = messageSource.getMessage("web.email.certcreatednotification.subject", new String[]{certificate.getCertno()},locale);
        Context context = new Context(locale);
		context.setVariable("client", clientContact);
		context.setVariable("csr", csr);
		context.setVariable("certificate", certificate);
		context.setVariable("instrument", instrument);
		context.setVariable("translationService", translationService);
		String body = templateEngine.process("email/portal/certificate_created", context);
		if ( fromAddr != null && toAddr != null )
			emailService.sendBasicEmail(subject, fromAddr, toAddr, body);
	}

	private Predicate<CalibrationType> calTypeCanBeUsedByClient() {
		return c -> c.getServiceType().getCanBePerformedByClient().equals(true);
	}

	private Predicate<CalibrationType> calTypeIsFullCalibration() {
		return c -> c.getRecallRequirementType().equals(RecallRequirementType.FULL_CALIBRATION);
	}

	@ModelAttribute("verificationstatuses")
	public List<KeyValue<CalibrationVerificationStatus, String>> getVerificationStatuses() {
		return Arrays.stream(CalibrationVerificationStatus.values()).map(s -> new KeyValue<>(s, s.getName())).collect(Collectors.toList());
	}

}
