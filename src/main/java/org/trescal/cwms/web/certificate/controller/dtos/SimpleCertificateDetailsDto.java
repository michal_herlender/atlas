package org.trescal.cwms.web.certificate.controller.dtos;

import org.trescal.cwms.web.instrument.dto.SimpleInstrumentSearchResultsDto;

import java.util.List;

public class SimpleCertificateDetailsDto {

    private String certNo;
    private List<SimpleInstrumentSearchResultsDto> instruments;
    private String calDate;
    private String verificationDate;
    private String verificationStatus;

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public List<SimpleInstrumentSearchResultsDto> getInstruments() {
        return instruments;
    }

    public void setInstruments(List<SimpleInstrumentSearchResultsDto> instruments) {
        this.instruments = instruments;
    }

    public String getCalDate() {
        return calDate;
    }

    public void setCalDate(String calDate) {
        this.calDate = calDate;
    }

    public String getVerificationDate() {
        return verificationDate;
    }

    public void setVerificationDate(String verificationDate) {
        this.verificationDate = verificationDate;
    }

    public void setVerificationStatus(String verificationStatus) {
        this.verificationStatus = verificationStatus;
    }

    public String getVerificationStatus() {
        return verificationStatus;
    }
}
