package org.trescal.cwms.web.certificate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetRestController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.validation.db.CertificateValidationService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CsrEmails;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.web.certificate.controller.dtos.CertValidationAddDto;
import org.trescal.cwms.web.certificate.controller.dtos.CertValidationResponseDto;
import org.trescal.cwms.web.exception.PortalEntityNotFoundException;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@RestController
@ExtranetRestController
@RequestMapping(path = "certificateacceptance")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class WebCertificateAcceptanceController {

	@Autowired
	private CertificateValidationService validationService;

	@Autowired
	private CertificateService certificateService;

	@Autowired
	private UserService userService;

	@Autowired
	private TranslationService translationService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private EmailService emailService;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Autowired
	private CsrEmails csrEmails;

	// Retrieve a certificate validation record
	@GetMapping("/{id}")
	public CertValidationResponseDto getValidation(@PathVariable("id") Integer validationId) {
		CertificateValidation certificateValidation = validationService.get(validationId);
		if (certificateValidation == null) {
			throw new PortalEntityNotFoundException("cert validation id-" + validationId);
		}

		return new CertValidationResponseDto(certificateValidation, LocaleContextHolder.getLocale());

	}

	// Add a certificate validation record for a cert
	@PostMapping("/add")
	public ResponseEntity<Object> createValidation(@RequestBody CertValidationAddDto input,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {

		Contact contact = this.userService.get(username).getCon();
		Certificate certificate = certificateService.findCertificate(input.getCertificateId());
		if (certificate == null) {
			throw new PortalEntityNotFoundException("cert id-" + input.getCertificateId());
		}

		CertificateValidation certValidation = new CertificateValidation();
		certValidation.setCert(certificate);
		certValidation.setDatevalidated(input.getDateValidated());
		certValidation.setNote(input.getNote());
		certValidation.setValidatedBy(contact);
		certValidation.setStatus(input.getStatus());
		validationService.save(certValidation);

		this.sendEmail(certValidation, contact);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().replacePath("certificateacceptance/{id}")
				.buildAndExpand(certValidation.getId()).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", location.toString());
		return new ResponseEntity<>(new CertValidationResponseDto(certValidation, LocaleContextHolder.getLocale()),
				headers, HttpStatus.CREATED);
	}
	
	// Reset a certificate validation records for a cert
	@PostMapping("/reset")
	public ResponseEntity<Object> resetCertificateValidation(@RequestBody Integer certificateId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {

			Certificate certificate = certificateService.findCertificate(certificateId);
			if (certificate == null) {
				throw new PortalEntityNotFoundException("cert id-" + certificateId);
			}

			Contact contact = this.userService.get(username).getCon();
			this.certificateService.resetCertificateValidation(certificate, contact);
			
			return new ResponseEntity<>(HttpStatus.OK, null, HttpStatus.OK);

		}
	
	@PostMapping("/bulkadd")
	public ResponseEntity<Object> createMultipleValidation2(@RequestBody List<CertValidationAddDto> data,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Contact contact = this.userService.get(username).getCon();
		ArrayList<KeyValue<Integer, KeyValue<Integer, Integer>>> acceptedCertIds = new ArrayList<>();
		for (CertValidationAddDto certValidationDto : data) {
			Certificate certificate = certificateService.findCertificate(certValidationDto.getCertificateId());
			if (certificate != null) { 
				CertificateValidation certValidation = new CertificateValidation();
				certValidation.setCert(certificate);
				certValidation.setDatevalidated(certValidationDto.getDateValidated());
				certValidation.setValidatedBy(contact);
				certValidation.setStatus(certValidationDto.getStatus());
				certValidation.setNote(certValidationDto.getNote());
				if(!certificate.validationExist(certValidation)) {
					validationService.save(certValidation);
				certificate.setValidation(certValidation);
				this.sendEmail(certValidation, contact);
				acceptedCertIds.add(new KeyValue<>(certValidationDto.getCertificateId(), new KeyValue<>(certValidation.getId(), certificate.getBeforeLastValidation() != null ? certificate.getBeforeLastValidation().getId():0)));
				}
			}
		}
		return new ResponseEntity<>(acceptedCertIds, HttpStatus.CREATED);
	}


	// Get instrument cal requirement text to display in cert acceptance pop-up
	@GetMapping("/instructions/{certid}")
	public List<String> getInstructions(@PathVariable("certid") Integer certId) {
		return this.certificateService.getRelatedInstrumentsInstructions(certId);
	}

	// Get customer acceptance criteria to display in cert acceptance pop-up
	@GetMapping("/acceptancecriteria/{certid}")
	public List<String> getAcceptanceCriteria(@PathVariable("certid") Integer certId) {
		return this.certificateService.getRelatedInstrumentsAcceptanceCriteria(certId);
	}

	// Get certificate deviation
	@GetMapping("/deviation/{certid}")
	public String getDeviation(@PathVariable("certid") Integer certId) {
		return this.certificateService.getRelatedInstrumentDeviation(certId);
	}

	private void sendEmail(CertificateValidation certValidation, Contact clientContact) {

		if (this.csrEmails.getValueByScope(Scope.COMPANY, clientContact.getSub().getComp(), null)
				&& (certValidation.getStatus().equals(CertificateValidationStatus.REJECTED)
						|| (certValidation.getStatus().equals(CertificateValidationStatus.ACCEPTED)
								&& certValidation.getNote() != null && !certValidation.getNote().isEmpty()))) {

			Locale locale = LocaleContextHolder.getLocale();
			Contact csr = clientContact.getSub().getComp().getDefaultBusinessContact();
			List<Instrument> instruments = this.certificateService.getRelatedInstruments(certValidation.getCert());
			String acceptOrRejectCode = (certValidation.getStatus().equals(CertificateValidationStatus.ACCEPTED)
					? "certificatevalidation.status.accepted"
					: "certificatevalidation.status.rejected");
			String acceptOrRejectString = messageSource.getMessage(acceptOrRejectCode, null, locale);

			String toAddr = csr.getEmail();
			String fromAddr = clientContact.getEmail();
			String subject = messageSource.getMessage("addresstype.certificate", null, locale) + " "
					+ certValidation.getCert().getCertno() + " " + acceptOrRejectString;

			Context context = new Context(locale);
			context.setVariable("client", clientContact);
			context.setVariable("csr", csr);
			context.setVariable("validation", certValidation);
			context.setVariable("instruments", instruments);
			context.setVariable("translationService", translationService);
			String body = templateEngine.process("email/portal/certificate_acceptance", context);
			if (fromAddr != null && toAddr != null)
				emailService.sendBasicEmail(subject, fromAddr, toAddr, body);
		}

	}

}
