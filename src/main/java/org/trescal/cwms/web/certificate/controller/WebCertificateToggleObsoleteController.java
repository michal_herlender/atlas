package org.trescal.cwms.web.certificate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.entity.status.db.StatusService;

@RequestMapping(value = "/certtoggleobsolete.json")
@RestController
public class WebCertificateToggleObsoleteController {

	@Autowired
	CertificateService certificateService;

	@Autowired
	StatusService statusService;

	@RequestMapping(value = "{certId:[0-9]+}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Boolean changeStatus(@PathVariable("certId") Integer certid) {
		Certificate certificate = certificateService.findCertificate(certid);
		if (certificate.getStatus().equals(CertStatusEnum.OBSOLETE)) {
			certificate.setStatus(CertStatusEnum.SIGNED);
		} else {
			certificate.setStatus(CertStatusEnum.OBSOLETE);
		}
		certificateService.updateCertificate(certificate);
		return true;
	}

}
