package org.trescal.cwms.web.certificate.controller.dtos;

import java.time.LocalDate;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

@Data
public class CertValidationAddDto {

    @NotNull
    private Integer certificateId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dateValidated;
    private String note;
    private CertificateValidationStatus status;
}