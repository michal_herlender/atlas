package org.trescal.cwms.web.certificate.controller.dtos;

import lombok.Data;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

@Data
public class CertValidationResponseDto {

    private final Integer certificateId;
    private final String certificateNo;
    private final String approvedDate;
    private final String shortApprovedDate;
    private final String approvedBy;
    private final String note;
    private final String statusName;
    private final boolean rejected;
    private final Integer certValidationId;

    public CertValidationResponseDto(CertificateValidation certificateValidation, Locale locale) {
        final LocalDate dateValidated = certificateValidation.getDatevalidated();
        String approvedDate = dateValidated.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale));
        String shortApprovedDate = dateValidated.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(locale));
        //Get approved by name. For old records validated by is empty name is plain text in responsible
        String approvedBy = certificateValidation.getValidatedBy() != null
                ? certificateValidation.getValidatedBy().getName()
                : certificateValidation.getResponsible() != null
                ? certificateValidation.getResponsible() : "";
        this.certificateNo = certificateValidation.getCert().getCertno();
        this.approvedDate = approvedDate;
        this.shortApprovedDate = shortApprovedDate;
        this.approvedBy = approvedBy;
        this.note = certificateValidation.getNote() != null ? certificateValidation.getNote() : "";
        this.statusName = certificateValidation.getStatus().getName();
        this.rejected = certificateValidation.getStatus().equals(CertificateValidationStatus.REJECTED);
        this.certificateId = certificateValidation.getCert().getCertid();
        this.certValidationId = certificateValidation.getId();
    }
}