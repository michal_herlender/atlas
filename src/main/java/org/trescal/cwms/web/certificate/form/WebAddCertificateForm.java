package org.trescal.cwms.web.certificate.form;


import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class WebAddCertificateForm {

    Integer instrumentId;
    MultipartFile file;
    Integer interval;
    String intervalUnitId;
    Date calDate;
    String certNumber;
    Integer calTypeId;
    String remarks;
    CalibrationVerificationStatus calibrationVerificationStatus;
    BigDecimal deviation;
    Date certDate;

}
