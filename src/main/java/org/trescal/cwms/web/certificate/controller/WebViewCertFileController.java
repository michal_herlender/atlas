package org.trescal.cwms.web.certificate.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.WebPermissionException;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CertificateClassDefault;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CertificateValidationReport;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ObsoleteCertificateFunctionality;
import org.trescal.cwms.core.system.enums.Scope;

@Controller
@ExtranetController
public class WebViewCertFileController {
	@Autowired
	private CertificateService certServ;
	@Autowired
	private CalibrationVerification systemDefaultCalibrationVerification;
	@Autowired
	private CertificateValidationReport systemDefaultCertificateValidation;
	@Autowired
	private CertificateClassDefault systemDefaultCertificateClass;
	@Autowired
	private Deviation systemDefaultDeviation;
	@Autowired
	private ObsoleteCertificateFunctionality obsoleteCertificateFunctionality;

	@ModelAttribute("showCalVerificationStatus")
	public Boolean isCalibrationVerificationRequired(@RequestParam("cert") Integer certno)
			throws WebPermissionException {
		Certificate cert = this.certServ.findWebSafeCertificate(certno);
		// Only need first instrument if there are more than 1 to get client company.
		Instrument instrument = this.certServ.getRelatedInstruments(cert).get(0);
		return this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("canPrintCertAccepptanceDoc")
	protected Boolean canPrintCertAcceptance(@RequestParam("cert") Integer certno) throws WebPermissionException {
		Certificate cert = this.certServ.findWebSafeCertificate(certno);
		// Only need first instrument if there are more than 1 to get client company.
		Instrument instrument = this.certServ.getRelatedInstruments(cert).get(0);
		return this.systemDefaultCertificateValidation.getValueByScope(Scope.COMPANY, instrument.getComp(), null)
				&& cert.getValidation() != null;
	}

	@ModelAttribute("showDeviation")
	public Boolean isDeviationRequired(@RequestParam("cert") Integer certno) throws WebPermissionException {
		Certificate cert = this.certServ.findWebSafeCertificate(certno);
		// Only need first instrument if there are more than 1 to get client company.
		Instrument instrument = this.certServ.getRelatedInstruments(cert).get(0);
		return this.systemDefaultDeviation.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("showClass")
	public Boolean isCertClassRequired(@RequestParam("cert") Integer certno) throws WebPermissionException {
		Certificate cert = this.certServ.findWebSafeCertificate(certno);
		// Only need first instrument if there are more than 1 to get client company.
		Instrument instrument = this.certServ.getRelatedInstruments(cert).get(0);
		return this.systemDefaultCertificateClass.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("obsoleteAllowed")
	public Boolean isObsoleteCertAllowed(@RequestParam("cert") Integer certno) throws WebPermissionException {
		Certificate cert = this.certServ.findWebSafeCertificate(certno);
		// Only need first instrument if there are more than 1 to get client company.
		Instrument instrument = this.certServ.getRelatedInstruments(cert).get(0);
		return this.obsoleteCertificateFunctionality.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("obsolete")
	public Boolean isCertObsolete(@RequestParam("cert") Integer certno) throws WebPermissionException {
		Certificate cert = this.certServ.findWebSafeCertificate(certno);
		return cert.getStatus().equals(CertStatusEnum.OBSOLETE);
	}

	@RequestMapping(value = "/viewcert.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(@RequestParam("cert") Integer certno) throws Exception {

		Certificate cert = null;
		cert = this.certServ.findWebSafeCertificate(certno);

		if (cert == null || cert.getStatus().equals(CertStatusEnum.DELETED)) {
			throw new Exception("Cert with id of " + certno + " cannot be found");
		}

		Map<String, Object> model = new HashMap<>();
		model.put("cert", cert);

		return new ModelAndView("jobs/certificate/webviewcert", "model", model);
	}
}
