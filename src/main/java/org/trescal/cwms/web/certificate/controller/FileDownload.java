package org.trescal.cwms.web.certificate.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.FileController;
import org.trescal.cwms.core.tools.EncryptionTools;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;

/*
 * same controller as in core
 * package org.trescal.cwms.core.documents.files;
 */
@Controller
@FileController
@Slf4j
public class FileDownload {

    @RequestMapping(value = "/downloadfile.htm", method = RequestMethod.GET)
    public void showImage(@RequestParam("file") String filename, HttpServletResponse response) throws IOException {
		String fileencrypt ="";
		try {
			fileencrypt = EncryptionTools.decrypt(filename);
		} catch (Exception e) {
            // can't download
            log.error("encrypt filepath in Web-FileDownload", e);
        }
		if (fileencrypt.length() > 0) {
            File file = new File(fileencrypt);
            if (file.exists() && file.length() < Integer.MAX_VALUE) {
                response.setContentType(URLConnection.guessContentTypeFromName(file.getName()));
                response.setContentLength((int) file.length());
                response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

                log.debug("Download File");
                log.debug(response.getContentType());
                if (response.getContentType() == null) {
                    String fileType = file.getName().substring(file.getName().lastIndexOf(".") + 1);
                    log.debug(fileType);
                    if ("xls".equals(fileType)) {
                        response.setContentType("application/vnd.ms-excel");
                    }
                }
                log.debug(response.getContentType());
                log.debug(file.getName());

                FileCopyUtils.copy(Files.readAllBytes(file.toPath()), response.getOutputStream());
            }
        }
	}
}
