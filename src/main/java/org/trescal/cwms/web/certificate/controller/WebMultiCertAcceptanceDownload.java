package org.trescal.cwms.web.certificate.controller;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.exception.WebPermissionException;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.tools.BirtEngine;

@Controller @ExtranetController
@RequestMapping("/bulkacceptancedownload.zip")
public class WebMultiCertAcceptanceDownload {

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private CertificateService certificateService;

    @Autowired
    private BirtEngine birtEngine;
    
    private static final String REPORT_PATH = "/birt/scripted/certificate/cert_acceptance.rptdesign";

    @PostMapping(produces = "application/zip")
    public void produceZip(Locale locale, @RequestParam("certids") String certIdsString, HttpServletResponse response) throws IOException, WebPermissionException, EngineException {
        File systemTempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
        System.out.println(systemTempDir.getPath());
        Path tempZipDirPath = Files.createTempDirectory(systemTempDir.toPath(),"certacceptance");

        String[] certIds = certIdsString.split(",");

        List<Path> acceptanceDocuments = new ArrayList<>();

        // Generate pdf document for each accepted / rejected cert
        for (String certId : certIds) {
            Certificate certificate = certificateService.findWebSafeCertificate(Integer.parseInt(certId));
            if(certificate.getValidation() != null) {
                List<Instrument> instruments = certificateService.getRelatedInstruments(certificate);
                Integer plantid = instruments.get(0).getPlantid();
                //Create file for document
                String filename = certificate.getCertno() + "_acceptance.pdf";
                Path acceptanceDocument = tempZipDirPath.resolve(filename);
                //Initialize parameters for Birt
                Map<String, Object> parameters = new HashMap<>();
                parameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID,Integer.parseInt(certId));
                //Generate document
                byte[] pdf = birtEngine.generatePDFAsByteArray(REPORT_PATH, parameters, locale);
                Files.write(acceptanceDocument,pdf);
                //Add to list of files to be zipped
                acceptanceDocuments.add(acceptanceDocument);
            }
        }

        // Zip files and stream to browser
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-Disposition", "attachment; filename \"certificates.zip\"");
        response.setContentType("application/zip");
        ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream());
        for (Path acceptanceDoc : acceptanceDocuments) {
            zipOutputStream.putNextEntry(new ZipEntry(acceptanceDoc.getFileName().toString()));
            InputStream fileInputStream = Files.newInputStream(acceptanceDoc);

            IOUtils.copy(fileInputStream, zipOutputStream);

            fileInputStream.close();
            zipOutputStream.closeEntry();
        }

        zipOutputStream.close();
    }

}
