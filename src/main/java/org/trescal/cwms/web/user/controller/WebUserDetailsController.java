package org.trescal.cwms.web.user.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.web.user.form.WebUserDetailsForm;
import org.trescal.cwms.web.user.form.WebValidateUser;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Controller @ExtranetController
public class WebUserDetailsController
{
	@Autowired
	private AddressService addressServ;
	@Autowired
	private ContactService conServ;
	// TODO fix - not threadsafe!
	private String message = null;
	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private WebValidateUser validator;

	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("command")
	protected WebUserDetailsForm formBackingObject() throws Exception
	{
		WebUserDetailsForm form = new WebUserDetailsForm();
		Contact contact = this.sessionServ.getCurrentContact();
		form.setContact(contact);

		if (contact.getDefAddress() != null)
		{
			form.setAddrid(contact.getDefAddress().getAddrid());
		}
		form.setMessage(this.message);
		this.message = null;

		return form;
	}

	@RequestMapping(value="/myprofile.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute("command") @Validated WebUserDetailsForm form, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData();
		Contact contact = form.getContact();
		Address defAddress = this.addressServ.get(form.getAddrid());
		contact.setDefAddress(defAddress);
		this.conServ.merge(contact);

		this.message = "Your profile has been successfully updated";

		return new ModelAndView(new RedirectView("/web/myprofile.htm", true));
	}

	@RequestMapping(value="/myprofile.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception
	{
		Map<String, Object> refData = new HashMap<>();

		Contact contact = this.sessionServ.getCurrentContact();


		val adds = contact.getSub().getAddresses().stream().map(a ->
				AddressDto.builder().addrid(a.getAddrid())
						.addressLine(String.join(", ",a.getAddr1(),a.getTown(),a.getCountry().getLocalizedName())
						).build()
				).collect(Collectors.toList());
		refData.put("addresses", adds);

		return new ModelAndView("/user/webuserprofile", refData);
	}

	@Data
	@AllArgsConstructor
	@Builder
	private static class AddressDto{
		Integer addrid;
		String addressLine;
	}
}
