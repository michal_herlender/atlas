package org.trescal.cwms.web.user.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class WebValidateUser extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(WebUserDetailsForm.class);
	}

	@Override
	public void validate(Object obj, Errors errors)
	{
		WebUserDetailsForm form = (WebUserDetailsForm) obj;
		errors.pushNestedPath("contact");
		super.validate(form.getContact(), errors);
		errors.popNestedPath();

		if (!NumberTools.isValidPhoneNumber(form.getContact().getTelephone()))
		{
			errors.rejectValue("contact.telephone", null, "A forbidden character is present in your telephone number, please amend using numeric values combined with any of the following symbols ('+', ' ', '(', ')')");
		}


		if (!NumberTools.isValidPhoneNumber(form.getContact().getFax()))
		{
			errors.rejectValue("contact.fax", null, "A forbidden character is present in your fax number, please amend using numeric values combined with any of the following symbols ('+', ' ', '(', ')')");
		}

		if (!NumberTools.isValidPhoneNumber(form.getContact().getMobile()))
		{
			errors.rejectValue("contact.mobile", null, "A forbidden character is present in your mobile number, please amend using numeric values combined with any of the following symbols ('+', ' ', '(', ')')");
		}

		
	}
}
