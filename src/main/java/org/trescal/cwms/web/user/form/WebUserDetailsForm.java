package org.trescal.cwms.web.user.form;

import javax.validation.Valid;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WebUserDetailsForm
{
	private int addrid;
	@Valid
	private Contact contact;
	private String message;
	private String partdelivery;
	private int partdeliverysysdefid;

}
