package org.trescal.cwms.web.misc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.SessionUtilsService;

/*
 * This replaces the base handleRequest method in the WebController with an interceptor implementation. 
 * GB 2015-5-1 
 */
public class WebTermsInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	protected SessionUtilsService sessionServ;
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		Contact con = this.sessionServ.getCurrentContact();
		if (!con.getUser().getWebTermsAccepted())
		{
			response.sendRedirect("termsandconditions.htm");
			return false;
		}
		return true;
	}
}
