package org.trescal.cwms.web.misc.form;

import java.util.Locale;

import lombok.Data;

@Data
public class WebLocaleChangeForm {
	private Locale desiredLocale;
	private String refererUrl;
}
