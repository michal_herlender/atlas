package org.trescal.cwms.web.misc.form;

import javax.validation.constraints.Size;

import org.trescal.cwms.core.company.entity.contact.Contact;

public class WebTermsForm
{
	private boolean accept;
	private Contact contact;
	private String contactPreference;
	private String newpassword;
	private String newpasswordConfirm;

	public Contact getContact()
	{
		return this.contact;
	}

	public String getContactPreference()
	{
		return this.contactPreference;
	}

	@Size(min=6, max=20)
	public String getNewpassword()
	{
		return this.newpassword;
	}

	@Size(min=6, max=20)
	public String getNewpasswordConfirm()
	{
		return this.newpasswordConfirm;
	}

	public boolean isAccept()
	{
		return this.accept;
	}

	public void setAccept(boolean accept)
	{
		this.accept = accept;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setContactPreference(String contactPreference)
	{
		this.contactPreference = contactPreference;
	}

	public void setNewpassword(String newpassword)
	{
		this.newpassword = newpassword;
	}

	public void setNewpasswordConfirm(String newpasswordConfirm)
	{
		this.newpasswordConfirm = newpasswordConfirm;
	}

}
