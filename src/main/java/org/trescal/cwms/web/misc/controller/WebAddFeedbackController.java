package org.trescal.cwms.web.misc.controller;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.form.AddFeedbackForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_FeedbackConfirmation;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_FeedbackConfirmation.EmailType;

/**
 * Temporary web feedback form.
 * 
 * @author richard
 */
@Controller @ExtranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class WebAddFeedbackController
{
	@Value("#{props['cwms.admin.email']}")
	private String toAddr;
	@Value("#{props['cwms.config.email.noreply']}")
	private String noreplyAddress;
	
	@Autowired
	private EmailContent_FeedbackConfirmation emailContentFeedbackConfirmation;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private UserService userService;
	@Autowired
	private ServletContext servletContext;
	
	@ModelAttribute("command")
	protected AddFeedbackForm formBackingObject(HttpServletRequest request) throws Exception
	{
		AddFeedbackForm form = new AddFeedbackForm();
		form.setUrl(ServletRequestUtils.getStringParameter(request, "url", ""));

		ServletRequestUtils.getStringParameter(request, "referrer");
		HttpSession session = request.getSession(true);
		if (session.getAttribute("feedbackleft") != null)
		{
			form.setFeedbackLeft(true);
		}
		else
		{
			session.setAttribute("url", form.getUrl());
		}
		session.removeAttribute("feedbackleft");

		return form;
	}

	@RequestMapping(value="/addfeedback.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "/misc/webaddfeedback";
	}

	@RequestMapping(value="/addfeedback.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("command") AddFeedbackForm form, 
			BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) return new ModelAndView(referenceData());
		HttpSession session = request.getSession(true);
		session.setAttribute("feedbackleft", true);

		Contact con = this.userService.get(username).getCon();

		if (form.getUrl() != null)
		{
			if (form.getUrl().indexOf(',') > -1)
			{
				form.setUrl(form.getUrl().substring(0, form.getUrl().indexOf(',')));
			}
		}

		//TODO : get/define the locale to use
		Locale locale = con.getLocale();
		
		String build = servletContext.getInitParameter("build-number");
		String requestURI = request.getRequestURI();
		String userAgent = request.getHeader("user-agent");
		String title = form.getTitle();
		String description = form.getDescription();
				
		// create email text
		EmailContentDto contentDto = this.emailContentFeedbackConfirmation.getContent(con, description, title, build, requestURI, userAgent, locale, EmailType.CLIENT);
		String htmlText = contentDto.getBody();
		String subject = contentDto.getSubject();

		this.emailServ.sendBasicEmail(subject, noreplyAddress, this.toAddr, htmlText);

		return new ModelAndView(new RedirectView("/web/addfeedback.htm", true));
	}
}
