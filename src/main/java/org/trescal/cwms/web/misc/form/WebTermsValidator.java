package org.trescal.cwms.web.misc.form;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class WebTermsValidator extends AbstractBeanValidator implements Validator
{
	/** Logger for this class and subclasses */
	protected final Log logger = LogFactory.getLog(this.getClass());

	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(WebTermsForm.class);
	}

	@Override
	public void validate(Object obj, Errors errors)
	{
		WebTermsForm form = (WebTermsForm) obj;

		String p1 = form.getNewpassword();
		String p2 = form.getNewpasswordConfirm();
		if (p1.length() > 0 || p2.length() > 0) {
			if (form.getNewpassword().indexOf(' ') > -1) errors.rejectValue("password", "error.space.character.notallowed");
			if (form.getNewpasswordConfirm().indexOf(' ') > -1) errors.rejectValue("password2", "error.space.character.notallowed");
			// Changing password, validate form
			super.validate(form, errors);
			// do the new password entries match?
			if (!p1.equals(p2))
			{
				errors.rejectValue("newpasswordConfirm", "error.contact.password.mismatch", "The chosen passwords do not appear to match, please re-enter");
			}
		}
		// have the terms and conditions been accepted
		else if (!form.isAccept())
		{
			errors.rejectValue("accept", "", null, "You have not accepted the terms and conditions, in order to access our client website the terms and conditions must be accepted");
		}
	}
}