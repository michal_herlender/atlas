package org.trescal.cwms.web.misc.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;

@Controller @ExtranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class WebContactUsController
{
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/contact.htm")
	public ModelAndView handleRequest(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();
		// Primary Business Contact of the Client company the logged in web user belongs to
		Contact con = this.userService.get(username).getCon();
		
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(con.getSub().getComp().getDefaultBusinessContact(), null, null);
		refData.put(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);

		return new ModelAndView("/misc/contact", refData);
	}
}
