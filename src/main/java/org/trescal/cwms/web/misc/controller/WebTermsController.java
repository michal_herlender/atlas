package org.trescal.cwms.web.misc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.web.misc.form.WebTermsForm;
import org.trescal.cwms.web.misc.form.WebTermsValidator;

@Controller
@ExtranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class WebTermsController {
	@Autowired
	private ContactService conServ;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private SessionUtilsService sessServ;
	@Autowired
	private UserService userServ;
	@Autowired
	private WebTermsValidator validator;

	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("command")
	protected WebTermsForm formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username)
			throws Exception {
		WebTermsForm form = new WebTermsForm();
		User user = this.userServ.get(username);
		form.setContact(user.getCon());
		form.setAccept(user.getWebTermsAccepted());
		return form;
	}

	@RequestMapping(value = "/termsandconditions.htm", method = RequestMethod.GET)
	protected String referenceData() {
		return "/misc/termsandconditions";
	}

	@RequestMapping(value = "/termsandconditions.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("command") @Validated WebTermsForm form, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors())
			return new ModelAndView(referenceData());
		User user = this.userServ.get(username);
		Contact contact = user.getCon();
		user.setWebTermsAccepted(form.isAccept());
		contact.setPreference(form.getContactPreference());
		if (!form.getNewpassword().isEmpty() && !form.getNewpasswordConfirm().isEmpty()) {
			user.setPassword(passwordEncoder.encode(form.getNewpassword()));
		}
		this.userServ.mergeUser(user);
		contact = this.conServ.merge(contact);
		// Update the contact in the session so that future calls
		// that still use session access have an updated version
		this.sessServ.setCurrentContact(request, contact);
		return new ModelAndView(new RedirectView("/web/home.htm", true));
	}
}