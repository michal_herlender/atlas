package org.trescal.cwms.web.recall.dto;

import java.time.LocalDate;

public class WebRecallDTO {
	private boolean selected;        // Whether this DTO represents the period selected by the user
	private boolean current;        // Whether this is the current peiod e.g. current month
	private boolean past;            // Whether this period is in the past
	private boolean future;            // Whether this period is in the future
	private LocalDate startDate;            // Start date of period, may be null
	private LocalDate finishDate;        // Finish date of period, may be null

	public boolean isSelected() {
		return selected;
	}

	public boolean isCurrent() {
		return current;
	}

	public boolean isPast() {
		return past;
	}

	public boolean isFuture() {
		return future;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

	public void setPast(boolean past) {
		this.past = past;
	}

	public void setFuture(boolean future) {
		this.future = future;
	}

	public LocalDate getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(LocalDate finishDate) {
		this.finishDate = finishDate;
	}
}
