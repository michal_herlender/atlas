package org.trescal.cwms.web.recall.form;

import java.time.LocalDate;

public class WebRecallForm {
	private int addrid;
	private int personid;
	private int subdivid;
	private LocalDate startDate;
	private LocalDate finishDate;

	public int getAddrid() {
		return addrid;
	}

	public int getPersonid() {
		return personid;
	}

	public int getSubdivid() {
		return subdivid;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setAddrid(int addrid) {
		this.addrid = addrid;
	}

	public void setPersonid(int personid) {
		this.personid = personid;
	}

	public void setSubdivid(int subdivid) {
		this.subdivid = subdivid;
	}

	public LocalDate getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(LocalDate finishDate) {
		this.finishDate = finishDate;
	}
}
