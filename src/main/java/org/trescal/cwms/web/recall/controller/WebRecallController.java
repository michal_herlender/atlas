package org.trescal.cwms.web.recall.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.UserScope;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db.UserGroupPropertyValueService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.web.recall.dto.WebRecallDTO;
import org.trescal.cwms.web.recall.form.WebRecallForm;

import javax.servlet.ServletException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@ExtranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
@Slf4j
public class WebRecallController {
    @Autowired
    private AddressService addrServ;
    @Autowired
    private ContactService contServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private UserGroupPropertyValueService userGroupPropertyValueService;
	@Autowired
	private UserService userService;


	@ModelAttribute("form")
	protected WebRecallForm formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username
			) throws Exception
	{
		WebRecallForm form = new WebRecallForm();
		Contact userContact = this.userService.get(username).getCon();
		form.setSubdivid(userContact.getSub().getSubdivid());
		// set the users initial selections based on user rights
		UserScope viewLevel = this.userGroupPropertyValueService.getValue(userContact, UserGroupProperty.VIEW_LEVEL);
		if (viewLevel.equals(UserScope.ADDRESS)) {
			form.setAddrid(userContact.getDefAddress().getAddrid());
			form.setPersonid(0);
        } else if (viewLevel.equals(UserScope.CONTACT)) {
            form.setAddrid(0);
            form.setPersonid(userContact.getPersonid());
        } else {
            form.setAddrid(0);
            form.setPersonid(0);
        }
        // Upon initial load, we want to display instruments due for the current month
        LocalDate now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        form.setStartDate(now.withDayOfMonth(1));
        form.setFinishDate(now.withDayOfMonth(now.lengthOfMonth()));
        return form;
    }

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	private List<WebRecallDTO> initRecallPeriods(WebRecallForm wrf) {
        LocalDate now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        List<WebRecallDTO> result = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            WebRecallDTO dto = new WebRecallDTO();
            if (i == 0) {
                dto.setStartDate(null);
                dto.setFinishDate(now.withDayOfMonth(1).minusDays(1));
                dto.setPast(true);
            } else {
                if (i == 1) {
                    dto.setCurrent(true);
                } else {
                    dto.setFuture(true);
                }
                val date = now.plusMonths(i - 1);
                dto.setStartDate(date.withDayOfMonth(1));
                dto.setFinishDate(date.withDayOfMonth(date.lengthOfMonth()));
            }
			dto.setSelected(isSelected(wrf, dto));
			result.add(dto);
		}
		return result;
	}
	
	/*
	 * Tests if dates match between form and dto on a calendar comparison basis, 
	 * i.e. if current dto should appear as selected recall period
	 */
	private boolean isSelected(WebRecallForm wrf, WebRecallDTO dto) {
        return
            DateTools.isSameLocalDateOrBothNull(wrf.getStartDate(), dto.getStartDate()) &&
                DateTools.isSameLocalDateOrBothNull(wrf.getFinishDate(), dto.getFinishDate());
    }

	@RequestMapping(value="/webrecall.htm")
	public String onSubmit(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("form") WebRecallForm wrf) throws ServletException
	{
		// get the current contact
		Contact userContact = this.userService.get(username).getCon();
		model.addAttribute("userContact", userContact);
		// get the users group rule for viewing instruments
		UserScope viewLevel = this.userGroupPropertyValueService.getValue(userContact, UserGroupProperty.VIEW_LEVEL);
		model.addAttribute("userReadRule", viewLevel);
		// add entity lists to the form using values set on form previously
		model.addAttribute("contacts",this.contServ.getAllSubdivContacts(wrf.getSubdivid()));
        Subdiv subdiv = subdivServ.get(wrf.getSubdivid());
        model.addAttribute("addresses", this.addrServ.getAllActiveSubdivAddresses(subdiv, null));
        model.addAttribute("subdivs", this.subdivServ.getAllActiveCompanySubdivs(userContact.getSub().getComp()));
        model.addAttribute("recallPeriods", initRecallPeriods(wrf));

        // get search values from form for use when searching
        Integer subdivid = wrf.getSubdivid() == 0 ? null : wrf.getSubdivid();
        Integer addrid = wrf.getAddrid() == 0 ? null : wrf.getAddrid();
        Integer personid = wrf.getPersonid() == 0 ? null : wrf.getPersonid();
        List<Instrument> rList = this.instServ.webSearchInstrumentsDueForCalibration(wrf.getStartDate(), wrf.getFinishDate(), userContact.getSub().getComp().getCoid(), subdivid, addrid, personid);
        log.debug("Found " + rList.size());
        // add instruments to form
        model.addAttribute("insts", rList);

        return "/recall/webrecall";
    }
}