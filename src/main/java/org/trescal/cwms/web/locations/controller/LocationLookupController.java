package org.trescal.cwms.web.locations.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.spring.model.KeyValue;

@RestController
public class LocationLookupController {
	
	@Autowired
	LocationService locationService;
	
	@RequestMapping(value="/getaddresslocations.json", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<KeyValue<Integer,String>> getActiveLocationsForAddress(@RequestParam("addrid") Integer addrid){
		List<Location> locations = locationService.getAllAddressLocations(addrid, true);
		return locations.stream()
				.map(l->new KeyValue<Integer,String>(l.getLocationid(),l.getLocation()))
				.collect(Collectors.toList());
		
		
				
	}

}
