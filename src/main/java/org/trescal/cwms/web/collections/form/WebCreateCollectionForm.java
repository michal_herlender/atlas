package org.trescal.cwms.web.collections.form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.AutoPopulatingList;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class WebCreateCollectionForm {
    private Integer addrId;
    private List<String> caltype;
    private List<String> commentDesc;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate customDate;
    private List<Boolean> faulty;
    private List<String> faultyDesc;
    private String furtherInformation;
    private List<String> itempo;
    private List<String> modelid;
    private List<String> plantid;
    private List<String> plantno;
    private String poNumberCal;
    private String poNumberRep;
    private List<Integer> qty;
    private RecallDetail rdetail;
    private AutoPopulatingList<?> requestItems;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate rrDate;
    private Schedule schedule;
    private List<String> serialno;
    private List<String> supportDesc;
    private List<Boolean> supported;
    private List<String> turn;

}
