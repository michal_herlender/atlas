package org.trescal.cwms.web.collections.dto;

public class WebCollectionFieldWrapper
{
	private Integer addrId;
	private String customDate;
	private String furtherInformation;
	private String poNumberCal;
	private String poNumberRep;
	private String rrDate;

	public WebCollectionFieldWrapper(Integer addrId, String rrDate, String customDate, String poNumberCal, String poNumberRep, String furtherInformation)
	{
		this.addrId = addrId;
		this.rrDate = rrDate;
		this.customDate = customDate;
		this.poNumberCal = poNumberCal;
		this.poNumberRep = poNumberRep;
		this.furtherInformation = furtherInformation;
	}

	public Integer getAddrId()
	{
		return this.addrId;
	}

	public String getCustomDate()
	{
		return this.customDate;
	}

	public String getFurtherInformation()
	{
		return this.furtherInformation;
	}

	public String getPoNumberCal()
	{
		return this.poNumberCal;
	}

	public String getPoNumberRep()
	{
		return this.poNumberRep;
	}

	public String getRrDate()
	{
		return this.rrDate;
	}

	public void setAddrId(Integer addrId)
	{
		this.addrId = addrId;
	}

	public void setCustomDate(String customDate)
	{
		this.customDate = customDate;
	}

	public void setFurtherInformation(String furtherInformation)
	{
		this.furtherInformation = furtherInformation;
	}

	public void setPoNumberCal(String poNumberCal)
	{
		this.poNumberCal = poNumberCal;
	}

	public void setPoNumberRep(String poNumberRep)
	{
		this.poNumberRep = poNumberRep;
	}

	public void setRrDate(String rrDate)
	{
		this.rrDate = rrDate;
	}
}