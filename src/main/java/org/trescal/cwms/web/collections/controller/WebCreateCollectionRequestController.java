package org.trescal.cwms.web.collections.controller;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db.UserGroupPropertyValueService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.db.RecallDetailService;
import org.trescal.cwms.core.schedule.entity.ScheduleEquipmentTurn;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipmentComparator;
import org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.ScheduleEquipmentInstrument;
import org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.ScheduleEquipmentModel;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.web.collections.form.WebCreateCollectionForm;
import org.trescal.cwms.web.utilities.formutils.WebRequestFormItemDTO;
import org.trescal.cwms.web.utilities.formutils.WebRequestItemDTOFactory;

@Controller
@ExtranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class WebCreateCollectionRequestController {
	@Autowired
	private AddressService addressServ;
	@Autowired
	private AddressSettingsForAllocatedSubdivService addressSettingsService;
	@Autowired
	private CalibrationTypeService caltypeServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private InstrumentModelService modelServ;
	@Autowired
	private RecallDetailService rdServ;
	@Autowired
	private ScheduleService schServ;
	@Autowired
	private UserService userService;
    @Autowired
    private UserGroupPropertyValueService userGroupPropertyValueService;

	public static final String REQUEST_PARAM_RECALL_DETAILS_ID = "rdid";

	@ModelAttribute("form")
	protected WebCreateCollectionForm formBackingObject(
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
		@RequestParam(name = REQUEST_PARAM_RECALL_DETAILS_ID, required = false, defaultValue = "0") Integer rdid) throws Exception {
		WebCreateCollectionForm form = new WebCreateCollectionForm();
		if (form.getSchedule() == null) {
			Schedule s = new Schedule();
			Contact contact = this.userService.get(username).getCon();
			s.setCreatedBy(contact);
			s.setCreatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));

			s.setSource(ScheduleSource.WEB);
			s.setType(ScheduleType.COLLECTION);

			form.setSchedule(s);
		}

		AutoPopulatingList<WebRequestFormItemDTO> list = new AutoPopulatingList<>(new WebRequestItemDTOFactory());
		form.setRequestItems(list);

		// page requested from recall document?
		// if we have then load recall details
		if (rdid != 0) {
			// find the recall detail
			RecallDetail rdetail = this.rdServ.get(rdid);
			// check recall detail can be loaded
			if (rdetail == null) {
				throw new Exception("Recall details could not be found");
			}
			// set recall detail on form
			form.setRdetail(rdetail);
		}

		return form;
	}

	@RequestMapping(value="/createcollection.htm", method=RequestMethod.POST)
	public ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("form") WebCreateCollectionForm form, 
			BindingResult bindingResult,
			RedirectAttributes redirectAttributes) throws ServletException
	{	
		if (bindingResult.hasErrors()) return referenceData(username);
		Contact contact = this.userService.get(username).getCon();
		/*
		 * At the present time collections are supported to the subdivision of the primary business contact
		 * of the client customer. 
		 */
		Subdiv businessSubdiv = contact.getSub().getComp().getDefaultBusinessContact().getSub(); 
		Address address = this.addressServ.get(form.getAddrId()); 

		Schedule s = form.getSchedule();
		s.setRepeatSchedule(false);
		s.setStatus(ScheduleStatus.PROVISIONAL);
		s.setContact(contact);
		s.setAddress(address);
		// set the scheduled from subdivision using the contact's company (no longer "base address")
		s.setOrganisation(businessSubdiv);
		
		AddressSettingsForAllocatedSubdiv addressSettingsForSubdiv = this.addressSettingsService.getBySubdiv(address, businessSubdiv);
		TransportOption transportIn = null;
		if (addressSettingsForSubdiv != null) {
			transportIn = addressSettingsForSubdiv.getTransportIn(); 
		}

		// If address supplied by round robin is selected, use selected date from drop down list
		if ((transportIn != null) && transportIn.getMethod().isOptionPerDayOfWeek()) {
			if (form.getRrDate() != null) {
				s.setScheduleDate(form.getRrDate());
			}
		}
		// address supplied by other method where any day can be selected, so
		// take date from input
		else {
			if (form.getCustomDate() != null) {
				s.setScheduleDate(form.getCustomDate());
			}
		}

		if (s.getNotes() == null) {
			s.setNotes(new TreeSet<>(new NoteComparator()));
		}

		if ((form.getPoNumberCal() != null)
			&& (!form.getPoNumberCal().isEmpty())) {
			ScheduleNote poNote = new ScheduleNote();
			poNote.setActive(true);
			poNote.setLabel("Calibration Purchase Order Number Supplied");
			poNote.setNote(form.getPoNumberCal());
			poNote.setPublish(false);
			poNote.setSetBy(contact);
			poNote.setSetOn(new Date());
			poNote.setSchedule(s);

			s.getNotes().add(poNote);
		}

		if ((form.getPoNumberRep() != null)
				&& (!form.getPoNumberRep().isEmpty()))
		{
			ScheduleNote poNote = new ScheduleNote();
			poNote.setActive(true);
			poNote.setLabel("Repair Purchase Order Number Supplied");
			poNote.setNote(form.getPoNumberRep());
			poNote.setPublish(false);
			poNote.setSetBy(contact);
			poNote.setSetOn(new Date());
			poNote.setSchedule(s);

			s.getNotes().add(poNote);
		}

		if ((form.getFurtherInformation() != null)
				&& (!form.getFurtherInformation().isEmpty()))
		{
			ScheduleNote fiNote = new ScheduleNote();
			fiNote.setActive(true);
			fiNote.setLabel("Collection Instruction/Info");
			fiNote.setNote(form.getFurtherInformation());
			fiNote.setPublish(false);
			fiNote.setSetBy(contact);
			fiNote.setSetOn(new Date());
			fiNote.setSchedule(s);

			s.getNotes().add(fiNote);
		}

		// arraylist to hold the current list of collection items
		Set<ScheduleEquipment> equipList = new TreeSet<>(new ScheduleEquipmentComparator());

		// add our items
		if (form.getRequestItems() != null) {
			for (Object rItem : form.getRequestItems()) {
				WebRequestFormItemDTO requestItem = (WebRequestFormItemDTO) rItem;

				if (requestItem != null) {
					if (requestItem.getPlantid() != null) {
						ScheduleEquipmentInstrument inst = new ScheduleEquipmentInstrument();
						inst.setEquipInst(this.instServ.findEagerModelInstrum(requestItem.getPlantid()));
						inst.setCaltype(this.caltypeServ.find(requestItem.getCaltypeid()));
						inst.setFaulty(requestItem.getFaulty());
						inst.setFaultDesc(requestItem.getFaultyDesc());
						inst.setCommentDesc(requestItem.getCommentDesc());
						inst.setTurn(ScheduleEquipmentTurn.valueOf(requestItem.getTurn()));
						inst.setSupported(requestItem.getSupported());
						inst.setSupportDesc(requestItem.getSupportDesc());
						inst.setPonumber(requestItem.getItempo());
						inst.setSchedule(s);

						equipList.add(inst);
					}
					else
					{
						ScheduleEquipmentModel model = new ScheduleEquipmentModel();
						model.setEquipModel(this.modelServ.findInstrumentModel(requestItem.getModelid()));
						model.setQty(requestItem.getQty());
						model.setCaltype(this.caltypeServ.find(requestItem.getCaltypeid()));
						model.setFaulty(requestItem.getFaulty());
						model.setFaultDesc(requestItem.getFaultyDesc());
						model.setCommentDesc(requestItem.getCommentDesc());
						model.setTurn(ScheduleEquipmentTurn.valueOf(requestItem.getTurn()));
						model.setSupported(requestItem.getSupported());
						model.setSupportDesc(requestItem.getSupportDesc());
						model.setPonumber(requestItem.getItempo());
						model.setSchedule(s);

						equipList.add(model);
					}
				}
			}
		}

		s.setScheduleequipment(equipList);

		this.schServ.insertSchedule(s);

		String filename = "";
		this.schServ.webRequestSchedule(s, filename, contact);

		redirectAttributes.addFlashAttribute("showSuccessMessage", true);
		return new ModelAndView(new RedirectView("/web/createcollection.htm", true));
	}

	@RequestMapping(value = "/createcollection.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Contact contact = this.userService.get(username).getCon();
		/*
		 * At the present time collections are supported to the subdivision of the primary business contact
		 * of the client customer.
		 */
		Subdiv businessSubdiv = contact.getSub().getComp().getDefaultBusinessContact().getSub();

		Map<String, Object> refData = new HashMap<>();

		refData.put("addresses", this.addressServ.getAllActiveSubdivAddresses(contact.getSub(), AddressType.DELIVERY));
		refData.put("datetoday", new Date());
		refData.put("businessSubdiv", businessSubdiv);
		refData.put("businessCompany", businessSubdiv.getComp().getConame());
		refData.put("businessTel", contact.getSub().getComp().getDefaultBusinessContact().getTelephone());

		String userReadRule = this.userGroupPropertyValueService.getValue(contact, UserGroupProperty.VIEW_LEVEL).name();

		if (userReadRule.equalsIgnoreCase("company")
				|| userReadRule.equalsIgnoreCase("subdiv"))
		{
			refData.put("requestedCollections", this.schServ.getSchedulesForSubdiv(contact.getSub().getSubdivid(), false, ScheduleType.COLLECTION));
		}
		else if (userReadRule.equalsIgnoreCase("location"))
		{
			if (contact.getDefAddress() != null)
			{
				refData.put("requestedCollections", this.schServ.getSchedulesForAddress(contact.getDefAddress().getAddrid(), false, ScheduleType.COLLECTION));
			}
		}
		else
		{
			refData.put("requestedCollections", this.schServ.getSchedulesForContact(contact.getPersonid(), false, ScheduleType.COLLECTION));
		}

		refData.put("turnOpts", ScheduleEquipmentTurn.values());

		return new ModelAndView("/collections/webcreatecollection", refData);
	}
}
