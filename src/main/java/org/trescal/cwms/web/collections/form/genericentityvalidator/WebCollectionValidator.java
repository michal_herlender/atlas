package org.trescal.cwms.web.collections.form.genericentityvalidator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.validation.AbstractEntityValidator;
import org.trescal.cwms.web.collections.dto.WebCollectionFieldWrapper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Slf4j
public class WebCollectionValidator extends AbstractEntityValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(WebCollectionFieldWrapper.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		WebCollectionFieldWrapper colFW = (WebCollectionFieldWrapper) target;

		// check address has been selected
		if (colFW.getAddrId() == null) {
			errors.rejectValue("addrId", null, "An address must be selected to proceed with your collection");
		}

		// check date has been supplied
		if (((colFW.getRrDate() != null) && (!colFW.getRrDate().isEmpty()))
			|| ((colFW.getCustomDate() != null) && (!colFW.getCustomDate().isEmpty()))) {
			if (colFW.getRrDate() != null && !colFW.getRrDate().trim().isEmpty()) {
				try {
					Locale locale = LocaleContextHolder.getLocale();
					if (locale == null) locale = Locale.UK;
					SimpleDateFormat pattern = new SimpleDateFormat("EEE dd MMM yyyy", locale);
					pattern.parse(colFW.getRrDate());
				} catch (Exception e) {
					errors.rejectValue("rrDate", null, "An error occured with the date entered");
				}
			}
			if ((colFW.getCustomDate() != null)
				&& (!colFW.getCustomDate().trim().isEmpty())) {
				try {
					Date date = DateTools.df.parse(colFW.getCustomDate());
					log.debug(date.toString());
				} catch (Exception e) {
					errors.rejectValue("customDate", null, "Please ensure the date selected is in the format 'dd.MM.yyyy'.");
				}
			}
		} else {
			errors.rejectValue("customDate", null, "Please ensure a date is specified for the collection");
		}

		// check the purchase order fields for length violation
		if (colFW.getPoNumberCal().length() > 60) {
			errors.rejectValue("poNumberCal", null, "Maximum length of purchase order field is 60 characters, please amend");
		}
		if (colFW.getPoNumberRep().length() > 60) {
			errors.rejectValue("poNumberRep", null, "Maximum length of purchase order field is 60 characters, please amend");
		}

		// check the further information field does not exceed note length
		if (colFW.getFurtherInformation().length() > 1000) {
			errors.rejectValue("furtherInformation", null, "Maximum length of collection instruction field is 1000 characters, please amend");
		}

		// make the errors available outside of this function
		this.errors = (BindException) errors;
	}

}
