package org.trescal.cwms.web.jobs.jobs.form;

import org.trescal.cwms.core.jobs.job.form.JobSearchForm;

public class WebJobSearchForm extends JobSearchForm
{
	private Integer addrid;
	private Integer subdivid;

	public Integer getAddrid()
	{
		return this.addrid;
	}

	public Integer getSubdivid()
	{
		return this.subdivid;
	}

	public void setAddrid(Integer addrid)
	{
		this.addrid = addrid;
	}

	public void setSubdivid(Integer subdivid)
	{
		this.subdivid = subdivid;
	}

}
