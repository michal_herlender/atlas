package org.trescal.cwms.web.jobs.jobs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedDto;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.db.StandardUsedService;

@JsonController
@Controller
public class StandardsUsedForJobItemController {
	
	@Autowired
	StandardUsedService standardUsedService;

	 @RequestMapping(value="/getstandardsused.json",method=RequestMethod.GET)
	 @ResponseBody
	    public List<StandardUsedDto> getStandardsUsedData(@RequestParam("jobitemid") Integer jobItemId){
	        return standardUsedService.getStandardsUsedForJobItemAjax(jobItemId);
	    }
}