package org.trescal.cwms.web.jobs.jobs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.db.StandardUsedService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.web.jobs.jobs.WebViewJobItemDTO;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Controller
@ExtranetController
public class WebViewJobController {
	@Autowired
	private JobService jobServ;

	@Autowired
	StandardUsedService standardUsedService;

	@ModelAttribute("job")
	public Job getJob(@RequestParam(value="jobid", defaultValue="0") int jobid) throws Exception{
		return this.findJob(jobid);
	}
	
	@ModelAttribute("jobitems")
	public List<WebViewJobItemDTO> getJobItems(@RequestParam(value="jobid", defaultValue="0") int jobid) throws Exception{
		Job job = this.findJob(jobid);
		List<WebViewJobItemDTO> jobItemDtos = new ArrayList<>();
		for (JobItem jobItem : job.getItems()) {
			Boolean showButton = standardUsedService.showStandardsUsedButtonForJobItem(jobItem);
			String consignmentInfo = null;
			JobDeliveryItem mostRecentDeliveryItem = null;
			if (jobItem.getDeliveryItems() != null){
					mostRecentDeliveryItem = jobItem.getDeliveryItems()
						.stream().filter(DeliveryItem::isDespatched)
					.max(Comparator.comparing(DeliveryItem::getDespatchedon)).orElse(null);
			}
			
			if (mostRecentDeliveryItem != null && mostRecentDeliveryItem.getDelivery().getCrdes() != null){
				consignmentInfo = mostRecentDeliveryItem.getDelivery().getCrdes().getCdtype().getCourier().getName()
						+ " " + mostRecentDeliveryItem.getDelivery().getCrdes().getConsignmentno();
			}
			
			WebViewJobItemDTO dto = new WebViewJobItemDTO(jobItem,showButton,consignmentInfo);
			jobItemDtos.add(dto);
		}
		return jobItemDtos;
	}

	@RequestMapping(value="/viewjob.htm", method=RequestMethod.GET)
	public String handleRequest() throws Exception
	{
		return "/jobs/jobs/webviewjob";
	}
	
	private Job findJob(Integer jobid) throws Exception {
		
		Job job = this.jobServ.findWebSafeJob(jobid);

		if (job == null)
		{
			throw new Exception("Job with id of " + jobid + " cannot be found");
		}
		
		return job;
	}
}
