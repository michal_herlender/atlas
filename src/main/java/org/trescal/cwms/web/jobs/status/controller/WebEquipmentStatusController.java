package org.trescal.cwms.web.jobs.status.controller;

import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db.UserGroupPropertyValueService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.web.jobs.jobs.form.OldWebJobSearchForm;

@Controller
@ExtranetController
public class WebEquipmentStatusController {
    @Autowired
    private JobService jobServ;
    @Autowired
    private SessionUtilsService sessionServ;
    @Autowired
    private UserGroupPropertyValueService userGroupPropertyValueService;

    @ModelAttribute("form")
    protected OldWebJobSearchForm formBackingObject(HttpServletRequest request) throws Exception {
        return new OldWebJobSearchForm();
    }

    @RequestMapping(value = "/equipmentstatus.htm", method = RequestMethod.GET)
    public ModelAndView referenceData(@RequestParam(value = "display", required = false) String display,
                                      @ModelAttribute("form") OldWebJobSearchForm wjsf, BindingResult bindingResult) throws ServletException {
        // Check for so called forced post!
        if ((display != null) && !bindingResult.hasErrors()) return onSubmit(display, wjsf, bindingResult);
        return new ModelAndView("/jobs/status/webequipmentstatus");
    }

    @RequestMapping(value = "/equipmentstatus.htm", method = RequestMethod.POST)
    public ModelAndView onSubmit(@RequestParam(value = "display") String display,
                                 @ModelAttribute("form") OldWebJobSearchForm wjsf, BindingResult bindingResult) throws ServletException {
        if (bindingResult.hasErrors()) return referenceData(null, wjsf, bindingResult);

        Contact con = this.sessionServ.getCurrentContact();

        String userReadRule = this.userGroupPropertyValueService.getValue(con, UserGroupProperty.VIEW_LEVEL).name();

        wjsf.setActive(true);

        if (display.equals("default")) {
            if (userReadRule.equals("COMPANY")) {
                wjsf.setCoid(new Integer(con.getSub().getComp().getCoid()).toString());
            }
            if (userReadRule.equals("SUBDIV")) {
                wjsf.setCoid(new Integer(con.getSub().getComp().getCoid()).toString());
                wjsf.setSubdivid(new Integer(con.getSub().getSubdivid()));
            } else if (userReadRule.equals("ADDRESS")) {
                wjsf.setCoid(new Integer(con.getSub().getComp().getCoid()).toString());
                wjsf.setAddrid(new Integer(con.getDefAddress().getAddrid()));
            } else if (userReadRule.equals("CONTACT")) {
                wjsf.setCoid(new Integer(con.getSub().getComp().getCoid()).toString());
                wjsf.setPersonid(new Integer(con.getPersonid()).toString());
            }
        } else if (display.equals("COMPANY")) {
            if (userReadRule.equals("COMPANY")) {
                wjsf.setCoid(new Integer(con.getSub().getComp().getCoid()).toString());
            } else {
                return new ModelAndView("/jobs/status/webequipmentstatus", "form", wjsf);
            }
        } else if (display.equals("SUBDIV")) {
            if (userReadRule.equals("COMPANY") || userReadRule.equals("SUBDIV")) {
                wjsf.setCoid(new Integer(con.getSub().getComp().getCoid()).toString());
                wjsf.setSubdivid(new Integer(con.getSub().getSubdivid()));
            } else {
                return new ModelAndView("/jobs/status/webequipmentstatus", "form", wjsf);
            }
        } else if (display.equals("ADDRESS")) {
            if (!userReadRule.equals("CONTACT")) {
                wjsf.setCoid(new Integer(con.getSub().getComp().getCoid()).toString());
                wjsf.setAddrid(new Integer(con.getDefAddress().getAddrid()));
            } else {
                return new ModelAndView("/jobs/status/webequipmentstatus", "form", wjsf);
            }
        } else {
            wjsf.setCoid(new Integer(con.getSub().getComp().getCoid()).toString());
            wjsf.setPersonid(new Integer(con.getPersonid()).toString());
        }

        PagedResultSet<Job> rs = this.jobServ.queryJobOld(wjsf, new PagedResultSet<Job>(wjsf.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : wjsf.getResultsPerPage(), wjsf.getPageNo() == 0 ? 1 : wjsf.getPageNo()));
        wjsf.setRs(rs);

        wjsf.setJobs((Collection<Job>) rs.getResults());

        wjsf.setJobItemCount(
                new Long(
                        wjsf.getJobs().stream()
                                .flatMap(j -> j.getItems().stream())
                                .filter(ji -> ji.getState().getActive())
                                .count()
                ).intValue()
        );

        return new ModelAndView("/jobs/status/webequipmentstatus", "form", wjsf);

    }


}
