package org.trescal.cwms.web.jobs.jobs;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class WebViewJobItemDTO {
	
	JobItem jobItem;
	Boolean showStandardsUsedButton;
	String consignmentInfo;
	

	public WebViewJobItemDTO(JobItem jobItem, Boolean showStandardsUsedButton, String consignmentInfo) {
		super();
		this.jobItem = jobItem;
		this.showStandardsUsedButton = showStandardsUsedButton;
		this.consignmentInfo = consignmentInfo;
	}

	public JobItem getJobItem() {
		return jobItem;
	}

	public void setJobItem(JobItem jobItem) {
		this.jobItem = jobItem;
	}

	public Boolean getShowStandardsUsedButton() {
		return showStandardsUsedButton;
	}

	public void setShowStandardsUsedButton(Boolean showStandardsUsedButton) {
		this.showStandardsUsedButton = showStandardsUsedButton;
	}
	
	public String getConsignmentInfo() {
		return consignmentInfo;
	}

	public void setConsignmentInfo(String consignmentInfo) {
		this.consignmentInfo = consignmentInfo;
	}

	
	
}
