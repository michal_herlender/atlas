package org.trescal.cwms.web.jobs.jobs.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class WebJobSearchValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(OldWebJobSearchForm.class);
	}
	
	@Override
	public void validate(Object obj, Errors errors)
	{
		super.validate(obj, errors);
		OldWebJobSearchForm form = (OldWebJobSearchForm) obj;
		if (!form.getPlantid().trim().isEmpty())
		{
			// check that it converts into a number
			if (!NumberTools.isAnInteger(form.getPlantid().trim()))
			{
				errors.rejectValue("plantid", "plantid", null, "The barcode given must be a number.");
			}
		}
		
		/**
		 * BUSINESS VALIDATION
		 */
		if (form.isBookedInDateBetween())
		{
			// if first date is given
			if (form.getBookedInDate1() != null)
			{
				// check second date is given
				if (form.getBookedInDate2() == null)
				{
					errors.rejectValue("bookedInDate2", "bookedInDate2", null, "Second booked date has not been specified.");
				}
				else
				{
					// check second date is chronologically after the first date
					if (!form.getBookedInDate2().isAfter(form.getBookedInDate1())) {
						errors.rejectValue("bookedInDate2", "bookedInDate2", null, "Second booked in date must be after the first booked in date.");
					}
				}
			}
		}
		if (form.isCompletedDateBetween())
		{
			// if first date is given
			if (form.getCompletedDate1() != null)
			{
				// check second date is given
				if (form.getCompletedDate2() == null)
				{
					errors.rejectValue("completedDate2", "completedDate2", null, "Second completed date has not been specified.");
				}
				else
				{
					// check second date is chronologically after the first date

					if (!form.getCompletedDate2().isAfter(form.getCompletedDate1())) {
						errors.rejectValue("completedDate2", "completedDate2", null, "Second completed date must be after the first completed date.");
					}
				}
			}
		}	
		
	}
}