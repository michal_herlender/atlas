package org.trescal.cwms.web.jobs.jobs.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.ExtranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.views.OldJobSearchXlsxView;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.web.jobs.jobs.form.OldWebJobSearchForm;
import org.trescal.cwms.web.jobs.jobs.form.WebJobSearchValidator;

@Controller @ExtranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class WebJobSearchController
{
	@Autowired
	private AddressService addrServ;
	@Autowired
	private ContactService contServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private WebJobSearchValidator validator;
	@Autowired
	private OldJobSearchXlsxView jobSearchXlsxView;
	@Autowired
	private UserService userService;
	@Value("${cwms.config.web.oldjobsearch.size.restrictexport}")
	private long sizeRestrictExport;
	
	private Log logger = LogFactory.getLog(this.getClass());

	@ModelAttribute("form")
	protected OldWebJobSearchForm formBackingObject(HttpServletRequest request)
	{
		return new OldWebJobSearchForm();
	}

	@InitBinder("form")
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception
	{
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@RequestMapping(value="/jobs.htm", method=RequestMethod.POST, params="!export")
	public ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			HttpServletRequest request, 
			@ModelAttribute("form") @Validated OldWebJobSearchForm jsf, BindingResult bindingResult) throws Exception
	{
		this.logger.info("Entering SearchJob handleRequest method ....");
		if (bindingResult.hasErrors()) return referenceData(username, "false", request, jsf, bindingResult);

		Contact contact = this.userService.get(username).getCon();
		// only searches for the current contacts company are allowed so set
		// that here
		jsf.setCoid(new Integer(contact.getSub().getComp().getCoid()).toString());

		// you have to lookup and set the modelid separately as if it's set from
		// a url spring will break and try and double submit it
		if (ServletRequestUtils.getIntParameter(request, "mid") != null)
		{
			jsf.setModelid(ServletRequestUtils.getIntParameter(request, "mid"));
		}

		PagedResultSet<Job> rs = this.jobServ.queryJobOld(jsf, new PagedResultSet<Job>(jsf.getResultsPerPage(), jsf.getPageNo()));
		jsf.setRs(rs);
		jsf.setResultsTotal(rs.getResultsCount());		
		jsf.setJobs((Collection<Job>) rs.getResults());

		// Get the total number of job items found in the search
		int jobitems = 0;
		for (Job j : jsf.getJobs())
		{
			jobitems = jobitems + j.getItems().size();
		}
		jsf.setJobItemCount(jobitems);

		return new ModelAndView("/jobs/jobs/webjobresults", "form", jsf);
	}
	
	@RequestMapping(value="/jobs.htm", method=RequestMethod.POST, params="export")
	public ModelAndView onExport(@ModelAttribute("form") OldWebJobSearchForm jsf) throws Exception
	{
		if (jsf.getJobItemCount() >= sizeRestrictExport) {
			throw new RuntimeException("This search has "+jsf.getJobItemCount()+" results and due to temporary performance restrictions, searches with "+sizeRestrictExport+" results or more cannot currently be exported."); 
		}
		long startTime = System.currentTimeMillis();
		PagedResultSet<Job> rs = this.jobServ.queryJobOld(jsf, new PagedResultSet<Job>(jsf.getResultsTotal(), 0));
		Set<Job> jobs = new HashSet<Job>(rs.getResults());
		long finishTime = System.currentTimeMillis();
		logger.debug("Search for "+ jsf.getResultsTotal() +" job items to export performed in "+(finishTime-startTime)+" ms");
		return new ModelAndView(jobSearchXlsxView, "jobs", jobs);
	}


	@RequestMapping(value="/jobs.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value="forced", required=false) String forced, 
			HttpServletRequest request, @ModelAttribute("form") OldWebJobSearchForm form, 
			BindingResult bindingResult) throws Exception
	{
		if ("forced".equals(forced) && !bindingResult.hasErrors()) return onSubmit(username,request, form, bindingResult);
		
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("instruments", this.instServ.getAllContactInstrums(contact.getPersonid()));
		refData.put("subdivs", this.subdivServ.getAllActiveCompanySubdivs(contact.getSub().getComp()));
		refData.put("addresses", this.addrServ.getAllActiveSubdivAddresses(contact.getSub(), null));
		refData.put("contacts", this.contServ.getAllSubdivContacts(contact.getSub().getSubdivid()));
		refData.put("userContact", contact);
		return new ModelAndView("/jobs/jobs/webjobsearch", refData);
	}
	
}
