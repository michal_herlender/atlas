package org.trescal.cwms.batch.config;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.PlatformTransactionManager;
import org.trescal.cwms.batch.jobs.importInstrument.InstrumentImportConfig;
import org.trescal.cwms.batch.jobs.operationsimport.OperationsImportConfig;
import org.trescal.cwms.batch.jobs.quotationimport.QuotationImportConfig;

@Configuration
@EnableBatchProcessing
@Import({ OperationsImportConfig.class, QuotationImportConfig.class,InstrumentImportConfig.class })
public class BatchConfig {

	/* Shared names */
	public static final String ANALYSIS_STEP = "ANALYSIS_STEP";
	public static final String SUBMIT_STEP = "SUBMIT_STEP";
	public static final int TRANSACTION_TIMEOUT = 5000;
	/* Shared Job Params */
	public static final String BATCH_JOB_ID = "BATCH_JOB_ID";
	public static final String EF_ID = "EF_ID";
	public static final String EF_FILE_ID = "EF_FILE_ID";
	public static final String LOCALE = "LOCALE";
	public static final String CLIENT_SUBDIV_ID = "CLIENT_SUBDIV_ID";
	public static final String BUSINESS_SUBDIV_ID = "BUSINESS_SUBDIV_ID";
	public static final String AUTO_SUBMIT_POLICY = "SUBMIT_POLICY";
	public static final String SUBMITTED_BY_ID = "SUBMITTED_BY_ID";
	
	/* Shared Step Execution Context data field names*/
	public static final String STEP_EC_ERROR_COUNT = "STEP_EC_ERROR_COUNT";
	public static final String STEP_EC_WARNING_COUNT = "STEP_EC_WARNING_COUNT";

	/* Shared Job execution context data field names*/
	public static final String JOB_EC_LAST_ANALAYSIS_STEP_ID = "JOB_EC_LAST_ANALAYSIS_STEP_ID";

	@Autowired
	@Qualifier("transactionManager")
	private PlatformTransactionManager transactionManager;
	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager entityManager;

	@Bean
	@Primary
	public BatchConfigurer configurer() {
		return new CwmsBatchConfigurer(transactionManager, entityManager);
	}

	@Bean("JobLauncher")
	@Primary
	public JobLauncher jobLauncher() throws Exception {
		return configurer().getJobLauncher();
	}

	@Bean("JobRepository")
	@Primary
	public JobRepository jobRepository() throws Exception {
		return configurer().getJobRepository();
	}

	@Bean
	@Primary
	public JobExplorer jobExplorer() throws Exception {
		return configurer().getJobExplorer();
	}

}
