package org.trescal.cwms.batch.config.entities.itemanalysisresult;

import java.util.List;

import org.trescal.cwms.batch.controller.AnalysisResultJsonController.AnalysisResultErrorsAndWarnings;
import org.trescal.cwms.core.audit.entity.db.BaseService;

public interface ItemAnalysisResultService extends BaseService<ItemAnalysisResult, Integer> {

	List<ItemAnalysisResult> getAllAnalysisResults(Integer batchStepExecutionId, Integer startIndex, Integer endIndex);

	List<AnalysisResultErrorsAndWarnings> getResultIndexesWithErrorsAndWarnings(Integer batchStepExecutionId);

}
