package org.trescal.cwms.batch.config.entities.batchstepexecution;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Setter;

@Entity
@Table(name = "BATCH_STEP_EXECUTION")
@Setter
public class BatchStepExecution {

	private Integer id;
	private Long version;
	private String stepName;
	private Date startTime;
	private Date endTime;
	private Date lastUpdated;
	private String status;
	private String exitCode;
	private String exitMessage;
	private Integer readCount;
	private Integer filterCount;
	private Integer writeCount;
	private Integer commitCount;
	private Integer readSkipCount;
	private Integer writeSkipCount;
	private Integer processSkipCount;
	private Integer rollbackCount;
	private BatchJobExecution jobExecution;
	private List<ItemAnalysisResult> itemAnalysisResults;

	@Id
	@Column(name = "STEP_EXECUTION_ID")
	public Integer getId() {
		return id;
	}

	@Column(name = "VERSION")
	public Long getVersion() {
		return version;
	}

	@Column(name = "STEP_NAME")
	public String getStepName() {
		return stepName;
	}

	@Column(name = "START_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getStartTime() {
		return startTime;
	}

	@Column(name = "END_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndTime() {
		return endTime;
	}

	@Column(name = "LAST_UPDATED")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastUpdated() {
		return lastUpdated;
	}

	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	@Column(name = "EXIT_CODE")
	public String getExitCode() {
		return exitCode;
	}

	@Column(name = "EXIT_MESSAGE", columnDefinition = "varchar(2500)")
	public String getExitMessage() {
		return exitMessage;
	}

	@Column(name = "READ_COUNT")
	public Integer getReadCount() {
		return readCount;
	}

	@Column(name = "FILTER_COUNT")
	public Integer getFilterCount() {
		return filterCount;
	}

	@Column(name = "WRITE_COUNT")
	public Integer getWriteCount() {
		return writeCount;
	}

	@Column(name = "COMMIT_COUNT")
	public Integer getCommitCount() {
		return commitCount;
	}

	@Column(name = "READ_SKIP_COUNT")
	public Integer getReadSkipCount() {
		return readSkipCount;
	}

	@Column(name = "WRITE_SKIP_COUNT")
	public Integer getWriteSkipCount() {
		return writeSkipCount;
	}

	@Column(name = "PROCESS_SKIP_COUNT")
	public Integer getProcessSkipCount() {
		return processSkipCount;
	}

	@Column(name = "ROLLBACK_COUNT")
	public Integer getRollbackCount() {
		return rollbackCount;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "JOB_EXECUTION_ID")
	public BatchJobExecution getJobExecution() {
		return jobExecution;
	}

	@JsonIgnore
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "stepExecution", fetch = FetchType.LAZY)
	public List<ItemAnalysisResult> getItemAnalysisResults() {
		return itemAnalysisResults;
	}

}
