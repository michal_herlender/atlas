package org.trescal.cwms.batch.config;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum AutoSubmitPolicyEnum {
	
	SUBMIT_IF_ALL_WITHOUT_ERRORS_AND_WITHOUT_WARNINGS("auto.submit.policy.batch.submit.only.noerrors.nowarnings"),
	SUBMIT_IF_ALL_WITHOUT_ERRORS("auto.submit.policy.batch.submit.only.noerrors"),
	SUBMIT_ONLY_ITEMS_WITHOUT_ERRORS_AND_WITHOUT_WARNINGS("auto.submit.policy.batch.submit.only.items.noerrors.nowarnings"),
	SUBMIT_ONLY_ITEMS_WITHOUT_ERRORS("auto.submit.policy.batch.submit.only.items.noerrors"),
	DO_NOT_SUBMIT("auto.submit.policy.batch.not.submit");

	private ReloadableResourceBundleMessageSource messages;
	private String messageCode;
	
	private AutoSubmitPolicyEnum(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getValue() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name(), locale);
		}
		return this.toString();
	}

	public String getName() {
		return this.name();
	}
	
	@Component
	public static class AutoSubmitPolicyMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (AutoSubmitPolicyEnum effn : EnumSet.allOf(AutoSubmitPolicyEnum.class))
            	effn.setMessageSource(messages);
        }
	}

	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
}
