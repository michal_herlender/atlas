package org.trescal.cwms.batch.config.entities.backgroundtask;

import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.entities.BatchJobInstance;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "backgroundtask")
public class BackgroundTask extends Allocated<Subdiv> {

    private Integer id;
    private Contact submittedBy;
    private Date submitedOn;
    private Contact notifiee;
    private Boolean notified;
	private BatchJobInstance jobInstance;
	private AutoSubmitPolicyEnum autoSubmitPolicy;
	private ExchangeFormat ef;
	private ExchangeFormatFile efFile;
	private Subdiv clientSubdiv;

	public BackgroundTask() {
	}

	public BackgroundTask(Contact submittedBy, Date submitedOn, Contact notifiee, AutoSubmitPolicyEnum autoSubmitPolicy,
			Subdiv clientSubdiv, Subdiv businessSubdiv, ExchangeFormat ef) {
		super();
		this.submittedBy = submittedBy;
		this.submitedOn = submitedOn;
		this.notifiee = notifiee;
		this.notified = false;
		this.autoSubmitPolicy = autoSubmitPolicy;
		this.organisation = businessSubdiv;
		this.clientSubdiv = clientSubdiv;
		this.ef = ef;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "submitedby", nullable = false)
	public Contact getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(Contact submitedBy) {
		this.submittedBy = submitedBy;
	}

	@Column(name = "submitedon", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSubmitedOn() {
		return submitedOn;
	}

	public void setSubmitedOn(Date submitedOn) {
		this.submitedOn = submitedOn;
	}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "notifiee")
	public Contact getNotifiee() {
		return notifiee;
	}

	public void setNotifiee(Contact notifiee) {
		this.notifiee = notifiee;
	}

	@Column(name = "notified", nullable = false)
	public Boolean getNotified() {
		return notified;
	}

	public void setNotified(Boolean notified) {
		this.notified = notified;
	}

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jobinstanceid")
	public BatchJobInstance getJobInstance() {
		return jobInstance;
	}

	public void setJobInstance(BatchJobInstance jobInstance) {
		this.jobInstance = jobInstance;
	}

	@Column(name = "autosubmitpolicy", nullable = false)
	@Enumerated(EnumType.STRING)
	public AutoSubmitPolicyEnum getAutoSubmitPolicy() {
		return autoSubmitPolicy;
	}

	public void setAutoSubmitPolicy(AutoSubmitPolicyEnum autoSubmitPolicy) {
		this.autoSubmitPolicy = autoSubmitPolicy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "exchangeformatfileid")
	public ExchangeFormatFile getEfFile() {
		return efFile;
	}

	public void setEfFile(ExchangeFormatFile efFile) {
		this.efFile = efFile;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clientsubdivid")
	public Subdiv getClientSubdiv() {
		return clientSubdiv;
	}

	public void setClientSubdiv(Subdiv clientSubdiv) {
		this.clientSubdiv = clientSubdiv;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "exchangeformatid")
	public ExchangeFormat getEf() {
		return ef;
	}

	public void setEf(ExchangeFormat ef) {
		this.ef = ef;
	}

}
