package org.trescal.cwms.batch.config.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTask;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;

import lombok.Setter;

@Entity
@Setter
@Table(name = "BATCH_JOB_INSTANCE")
public class BatchJobInstance {

	private Integer id;
	private String jobName;
	private BackgroundTask bgTask;
	private String jobKey;
	private Long version;
	private List<BatchJobExecution> jobExecutions;

	@Id
	@Column(name = "JOB_INSTANCE_ID")
	public Integer getId() {
		return id;
	}

	@Column(name = "JOB_NAME")
	public String getJobName() {
		return jobName;
	}

	@OneToOne(mappedBy = "jobInstance")
	public BackgroundTask getBgTask() {
		return bgTask;
	}

	@Column(name = "JOB_KEY")
	public String getJobKey() {
		return jobKey;
	}

	@Column(name = "VERSION")
	public Long getVersion() {
		return version;
	}

	@OneToMany(mappedBy = "jobInstance", fetch = FetchType.LAZY)
	public List<BatchJobExecution> getJobExecutions() {
		return jobExecutions;
	}

}
