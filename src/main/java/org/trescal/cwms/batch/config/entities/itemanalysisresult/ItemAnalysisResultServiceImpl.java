package org.trescal.cwms.batch.config.entities.itemanalysisresult;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.batch.controller.AnalysisResultJsonController.AnalysisResultErrorsAndWarnings;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;

@Service
public class ItemAnalysisResultServiceImpl extends BaseServiceImpl<ItemAnalysisResult, Integer>
		implements ItemAnalysisResultService {

	@Autowired
	private ItemAnalysisResultDao dao;

	@Override
	protected BaseDao<ItemAnalysisResult, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public List<ItemAnalysisResult> getAllAnalysisResults(Integer batchStepExecutionId, Integer startIndex,
			Integer endIndex) {
		return dao.getAllAnalysisResults(batchStepExecutionId, startIndex, endIndex);
	}

	@Override
	public List<AnalysisResultErrorsAndWarnings> getResultIndexesWithErrorsAndWarnings(Integer batchStepExecutionId) {
		return dao.getResultsIndexesWithErrorsAndWarnings(batchStepExecutionId);
	}

}
