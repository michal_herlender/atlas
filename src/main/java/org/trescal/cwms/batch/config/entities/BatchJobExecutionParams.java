package org.trescal.cwms.batch.config.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.batch.config.entities.BatchJobExecutionParams.BatchJobExecutionParamsId;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "BATCH_JOB_EXECUTION_PARAMS")
@IdClass(BatchJobExecutionParamsId.class)
@Setter
public class BatchJobExecutionParams {

	private BatchJobExecution batchJobExecution;
	private String type;
	private String keyName;
	private String stringValue;
	private Date dateValue;
	private Long longValue;
	private Double doubleValue;
	private Character identifiying;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "JOB_EXECUTION_ID")
	public BatchJobExecution getBatchJobExecution() {
		return batchJobExecution;
	}

	@Id
	@Column(name = "TYPE_CD")
	public String getType() {
		return type;
	}

	@Id
	@Column(name = "KEY_NAME")
	public String getKeyName() {
		return keyName;
	}

	@Id
	@Column(name = "STRING_VAL")
	public String getStringValue() {
		return stringValue;
	}

	@Id
	@Column(name = "DATE_VAL")
	public Date getDateValue() {
		return dateValue;
	}

	@Id
	@Column(name = "LONG_VAL")
	public Long getLongValue() {
		return longValue;
	}

	@Id
	@Column(name = "DOUBLE_VAL")
	public Double getDoubleValue() {
		return doubleValue;
	}

	@Column(name = "IDENTIFYING")
	public Character getIdentifiying() {
		return identifiying;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class BatchJobExecutionParamsId implements Serializable {
		private Long batchJobExecution;
		private String type;
		private String keyName;
		private String stringValue;
		private Date dateValue;
		private Long longValue;
		private Double doubleValue;
	}

}
