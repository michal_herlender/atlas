package org.trescal.cwms.batch.config.entities.batchjobexecution;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.batch.config.entities.BatchJobInstance;
import org.trescal.cwms.batch.config.entities.BatchJobInstance_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;

@Repository("BatchJobExecutionDao")
public class BatchJobExecutionDaoImpl extends BaseDaoImpl<BatchJobExecution, Integer> implements BatchJobExecutionDao {

	@Override
	protected Class<BatchJobExecution> getEntity() {
		return BatchJobExecution.class;
	}

	@Override
	public List<BatchJobExecution> fetchAllBatchJobExecutionsAndStepsByBatchJobId(Integer batchJobId) {
		return getResultList(cb -> {
			CriteriaQuery<BatchJobExecution> cq = cb.createQuery(BatchJobExecution.class);
			Root<BatchJobExecution> exec = cq.from(BatchJobExecution.class);
			Join<BatchJobExecution, BatchJobInstance> jobInstance = exec.join(BatchJobExecution_.jobInstance);
			exec.fetch(BatchJobExecution_.stepExecutions);
			cq.where(cb.equal(jobInstance.get(BatchJobInstance_.id), batchJobId));
			cq.distinct(true);
			return cq;
		});
	}

}
