package org.trescal.cwms.batch.config;

import java.util.List;

import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;

public interface ItemAnalysis {
	
	List<ItemAnalysisResult> getItemAnalysisResult();
	Integer getIndex();

}
