package org.trescal.cwms.batch.config.entities.backgroundtask;

import org.trescal.cwms.batch.controller.BgTasksController.BgTaskDto;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface BackgroundTaskDao extends BaseDao<BackgroundTask, Integer> {

	void getBgTaskDtos(PagedResultSet<BgTaskDto> pagedResultSet, Integer submitterContactId,
			Integer submitterContactId2, String jobname, Integer allocatedSubdivId);

	BackgroundTask getByBatchJobInstanceId(Integer batchJobInstanceId);

}
