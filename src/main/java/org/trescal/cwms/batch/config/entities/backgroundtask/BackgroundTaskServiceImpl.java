package org.trescal.cwms.batch.config.entities.backgroundtask;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.BatchJobInstance;
import org.trescal.cwms.batch.controller.BgTasksController.BgTaskDto;
import org.trescal.cwms.batch.jobs.importInstrument.InstrumentImportConfig;
import org.trescal.cwms.batch.jobs.operationsimport.OperationsImportConfig;
import org.trescal.cwms.batch.jobs.quotationimport.QuotationImportConfig;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.instrument.form.ImportInstrumentsForm;
import org.trescal.cwms.core.jobs.calibration.form.ImportCalibrationsForm;
import org.trescal.cwms.core.quotation.form.ImportQuotationItemsForm;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.batch.EmailContent_GenericBackgroundTaskCompletion;
import org.trescal.cwms.core.tools.PagedResultSet;

@Service
public class BackgroundTaskServiceImpl extends BaseServiceImpl<BackgroundTask, Integer>
		implements BackgroundTaskService {

	@Autowired
	private JobLauncher jobLauncher;
	@Autowired
	@Qualifier(OperationsImportConfig.JOB_NAME)
	private Job calibrationImportjob;
	@Autowired
	@Qualifier(QuotationImportConfig.JOB_NAME)
	private Job quotationImportjob;
	@Autowired
	@Qualifier(InstrumentImportConfig.JOB_NAME)
	private Job instrumentImportjob;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private BackgroundTaskDao dao;
	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager entityManager;
	@Autowired
	private ExchangeFormatService exchangeFormatService;
	@Autowired
	private EmailContent_GenericBackgroundTaskCompletion genericEmailContent;
	@Autowired
	private EmailService emailServ;
	@Value("${cwms.config.email.noreply}")
	private String noreplyAddress;

	@Override
	protected BaseDao<BackgroundTask, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public BackgroundTask submitOperationsImportBgTask(ImportCalibrationsForm form, Integer submittedById,
			Integer notifieeId, Integer subdivId, ExchangeFormatFile efFile, String locale)
			throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException,
			JobParametersInvalidException {

		Contact submittedBy = contactService.get(submittedById);
		Contact notifiee = contactService.get(notifieeId);
		Subdiv clientSubdiv = subdivService.get(form.getSubdivid());
		Subdiv businessSubdiv = subdivService.get(subdivId);

		ExchangeFormat ef = exchangeFormatService.get(form.getExchangeFormatId());
		BackgroundTask bgTask = new BackgroundTask(submittedBy, new Date(), notifiee, form.getAutoSubmitPolicy(),
				clientSubdiv, businessSubdiv, ef);

		bgTask.setEfFile(efFile);

		JobParameters params = new JobParametersBuilder()
				.addLong(BatchConfig.BATCH_JOB_ID, System.currentTimeMillis(), true)
				.addLong(OperationsImportConfig.JOB_ID, form.getJobId().longValue())
				.addLong(BatchConfig.CLIENT_SUBDIV_ID, form.getSubdivid().longValue())
				.addLong(OperationsImportConfig.BUSINESS_SUBDIV_ID, subdivId.longValue())
				.addLong(BatchConfig.EF_ID, form.getExchangeFormatId().longValue())
				.addLong(BatchConfig.EF_FILE_ID, efFile.getId().longValue()).addString(BatchConfig.LOCALE, locale)
				.addString(OperationsImportConfig.AUTO_CONTRACT_REVIEW,
						Boolean.toString(form.isAutomaticContractReview()))
				.addString(OperationsImportConfig.AUTO_CREATE_JOBITEMS, Boolean.toString(form.isAutoCreateJobitems()))
				.addString(BatchConfig.AUTO_SUBMIT_POLICY, form.getAutoSubmitPolicy().name()).toJobParameters();
		JobExecution je = jobLauncher.run(calibrationImportjob, params);

		bgTask.setJobInstance(entityManager.getReference(BatchJobInstance.class, je.getId().intValue()));

		this.dao.persist(bgTask);
		return bgTask;
	}

	@Override
	public BackgroundTask submitQuotationItemsImportBgTask(ImportQuotationItemsForm form, Integer submittedById,
			Integer notifieeId, Integer subdivId, ExchangeFormatFile efFile, String locale)
			throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException,
			JobParametersInvalidException {

		Contact submittedBy = contactService.get(submittedById);
		Contact notifiee = contactService.get(notifieeId);
		Subdiv clientSubdiv = subdivService.get(form.getSubdivid());
		Subdiv businessSubdiv = subdivService.get(subdivId);

		ExchangeFormat ef = exchangeFormatService.get(form.getExchangeFormatId());
		BackgroundTask bgTask = new BackgroundTask(submittedBy, new Date(), notifiee, form.getAutoSubmitPolicy(),
				clientSubdiv, businessSubdiv, ef);

		bgTask.setEfFile(efFile);

		JobParameters params = new JobParametersBuilder()
				.addLong(BatchConfig.BATCH_JOB_ID, System.currentTimeMillis(), true)
				.addLong(QuotationImportConfig.QUOTE_ID, form.getQuotationId().longValue())
				.addLong(BatchConfig.CLIENT_SUBDIV_ID, form.getSubdivid().longValue())
				.addLong(BatchConfig.BUSINESS_SUBDIV_ID, subdivId.longValue())
				.addLong(BatchConfig.EF_ID, form.getExchangeFormatId().longValue())
				.addLong(BatchConfig.EF_FILE_ID, efFile.getId().longValue()).addString(BatchConfig.LOCALE, locale)
				.addString(BatchConfig.AUTO_SUBMIT_POLICY, form.getAutoSubmitPolicy().name())
				.addLong(BatchConfig.SUBMITTED_BY_ID, submittedById.longValue())
				.addString(QuotationImportConfig.PRICE_SERVICE_TYPE_FROM_FILE,
						form.getPriceAndServiceTypeFromFile().toString())
				.toJobParameters();

		JobExecution je = jobLauncher.run(quotationImportjob, params);

		bgTask.setJobInstance(entityManager.getReference(BatchJobInstance.class, je.getId().intValue()));

		this.dao.persist(bgTask);
		return bgTask;

	}

	@Override
	public BackgroundTask submitImportInstrumentsBgTask(ImportInstrumentsForm form, Integer submittedById,
			Integer notifieeId, Integer subdivId, ExchangeFormatFile efFile, String locale)
			throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException,
			JobParametersInvalidException {

		Contact submittedBy = contactService.get(submittedById);
		Contact notifiee = contactService.get(notifieeId);
		Subdiv clientSubdiv = subdivService.get(form.getSubdivid());
		Subdiv businessSubdiv = subdivService.get(subdivId);

		ExchangeFormat ef = exchangeFormatService.get(form.getExchangeFormatId());
		BackgroundTask bgTask = new BackgroundTask(submittedBy, new Date(), notifiee, form.getAutoSubmitPolicy(),
				clientSubdiv, businessSubdiv, ef);

		bgTask.setEfFile(efFile);

		JobParameters params = new JobParametersBuilder()
				.addLong(BatchConfig.BATCH_JOB_ID, System.currentTimeMillis(), true)
				.addLong(InstrumentImportConfig.CLIENT_SUBDIV_ID, form.getSubdivid().longValue())
				.addLong(InstrumentImportConfig.BUSINESS_SUBDIV_ID, subdivId.longValue())
				.addLong(InstrumentImportConfig.DEFAULT_SERVICETYPE_ID, form.getDefaultServiceTypeId().longValue())
				.addLong(InstrumentImportConfig.DEFAULT_ADDRESS_ID, form.getAddrid().longValue())
				.addLong(InstrumentImportConfig.DEFAULT_CONTACT_ID, form.getPersonid().longValue())
				.addLong(BatchConfig.EF_ID, form.getExchangeFormatId().longValue())
				.addLong(BatchConfig.EF_FILE_ID, efFile.getId().longValue()).addString(BatchConfig.LOCALE, locale)
				.addString(BatchConfig.AUTO_SUBMIT_POLICY, form.getAutoSubmitPolicy().name())
				.addLong(InstrumentImportConfig.SUBMITED_BY, submittedById.longValue()).toJobParameters();

		JobExecution je = jobLauncher.run(instrumentImportjob, params);

		bgTask.setJobInstance(entityManager.getReference(BatchJobInstance.class, je.getId().intValue()));

		this.dao.persist(bgTask);
		return bgTask;
	}

	@Override
	public PagedResultSet<BgTaskDto> getBgTaskDtos(Integer count, int currentPage, Integer id,
			Integer submitterContactId, String jobname, Integer allocatedSubdivId) {
		PagedResultSet<BgTaskDto> prs = new PagedResultSet<>(count, currentPage);
		dao.getBgTaskDtos(prs, id, submitterContactId, jobname, allocatedSubdivId);
		return prs;
	}

	@Override
	public BackgroundTask getByBatchJobInstanceId(Integer batchJobInstanceId) {
		return dao.getByBatchJobInstanceId(batchJobInstanceId);
	}

	@Override
	public void notifyBatchJobCompletion(JobExecution jobExecution) {

		BackgroundTask bgTask = this.getByBatchJobInstanceId(jobExecution.getJobInstance().getId().intValue());
		if (bgTask != null) {
			Contact notifiee = bgTask.getNotifiee();

			/** get locale */
			// from contact or default to 'en'
			Locale locale = notifiee.getLocale();
			if (locale == null)
				locale = new Locale("en");
			// override from job parameters if it has been set, otherwise use default
			String localeString = jobExecution.getJobParameters().getString(BatchConfig.LOCALE, locale.toString());
			locale = new Locale(localeString);

			try {
				/** get email content */
				// TODO : get email contact by type of batch job
				EmailContentDto emailDto = genericEmailContent.getContent(locale, bgTask, jobExecution);
				/** send email */
				boolean success = emailServ.sendBasicEmail(emailDto.getSubject(), noreplyAddress, notifiee.getEmail(),
						emailDto.getBody());
				bgTask.setNotified(success);
			} catch (MalformedURLException | URISyntaxException e) {
				e.printStackTrace();
			}
		}
	}

}
