package org.trescal.cwms.batch.config.entities.backgroundtask;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.trescal.cwms.batch.controller.BgTasksController.BgTaskDto;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.instrument.form.ImportInstrumentsForm;
import org.trescal.cwms.core.jobs.calibration.form.ImportCalibrationsForm;
import org.trescal.cwms.core.quotation.form.ImportQuotationItemsForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface BackgroundTaskService extends BaseService<BackgroundTask, Integer> {

	PagedResultSet<BgTaskDto> getBgTaskDtos(Integer count, int currentPage, Integer submitterContactId,
			Integer personid, String jobname, Integer allocatedSubdivId);

	BackgroundTask submitOperationsImportBgTask(ImportCalibrationsForm form, Integer submittedById, Integer notifieeId,
			Integer subdivId, ExchangeFormatFile efFile, String locale) throws JobExecutionAlreadyRunningException,
			JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException;

	BackgroundTask submitQuotationItemsImportBgTask(ImportQuotationItemsForm form, Integer submittedById,
			Integer notifieeId, Integer subdivId, ExchangeFormatFile efFile, String locale)
			throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException,
			JobParametersInvalidException;

	BackgroundTask submitImportInstrumentsBgTask(ImportInstrumentsForm form, Integer submittedById, Integer notifieeId,
			Integer subdivId, ExchangeFormatFile efFile, String locale) throws JobExecutionAlreadyRunningException,
			JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException;

	BackgroundTask getByBatchJobInstanceId(Integer batchJobInstanceId);

	void notifyBatchJobCompletion(JobExecution jobExecution);

}
