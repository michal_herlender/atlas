package org.trescal.cwms.batch.config;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.configuration.annotation.BatchConfigurer;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.explore.support.JobExplorerFactoryBean;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.orm.jpa.EntityManagerFactoryInfo;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;

public class CwmsBatchConfigurer implements BatchConfigurer {

	protected final Log logger = LogFactory.getLog(getClass());

	private PlatformTransactionManager transactionManager;
	private EntityManager entityManager;

	private JobRepository jobRepository;
	private JobLauncher jobLauncher;
	private JobExplorer jobExplorer;

	public CwmsBatchConfigurer(PlatformTransactionManager transactionManager, EntityManager entityManager) {
		this.transactionManager = transactionManager;
		this.entityManager = entityManager;
		initialize();
	}

	@Override
	public JobRepository getJobRepository() throws Exception {
		return jobRepository;
	}

	@Override
	public PlatformTransactionManager getTransactionManager() throws Exception {
		return transactionManager;
	}

	@Override
	public JobLauncher getJobLauncher() throws Exception {
		return jobLauncher;
	}

	@Override
	public JobExplorer getJobExplorer() throws Exception {
		return jobExplorer;
	}

	public void initialize() {
		try {
			JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
			factory.setDataSource(getDataSource());
			factory.setTransactionManager(getTransactionManager());
			factory.setValidateTransactionState(false);
			factory.afterPropertiesSet();
			this.jobRepository = factory.getObject();

			this.jobLauncher = createJobLauncher();

			JobExplorerFactoryBean jobExplorerFactory = new JobExplorerFactoryBean();
			jobExplorerFactory.setDataSource(getDataSource());
			jobExplorerFactory.afterPropertiesSet();
			this.jobExplorer = jobExplorerFactory.getObject();

		} catch (Exception ex) {
			throw new IllegalStateException("Unable to initialize Spring Batch", ex);
		}
	}

	private JobLauncher createJobLauncher() {
		SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
		jobLauncher.setJobRepository(jobRepository);
		jobLauncher.setTaskExecutor(threadPoolTaskExecutor());
		return jobLauncher;
	}

	public TaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setMaxPoolSize(12);
		executor.setCorePoolSize(8);
		executor.setQueueCapacity(15);
		executor.setBeanName("BatchThreadPoolTaskExecutor");
		executor.initialize();
		return executor;
	}

	private DataSource getDataSource() {
		EntityManagerFactoryInfo info = (EntityManagerFactoryInfo) this.entityManager.getEntityManagerFactory();
		return info.getDataSource();
	}

}
