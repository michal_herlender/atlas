package org.trescal.cwms.batch.config.entities.batchjobexecution;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.trescal.cwms.batch.config.entities.BatchJobExecutionParams;
import org.trescal.cwms.batch.config.entities.BatchJobInstance;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Setter;

@Entity
@Table(name = "BATCH_JOB_EXECUTION")
@Setter
public class BatchJobExecution {

	private Integer id;
	private Date createTime;
	private Date startTime;
	private Date endTime;
	private Date lastUpdated;
	private String status;
	private String exitCode;
	private String exitMessage;
	private Long version;
	private String jobConfigurationLocation;
	private BatchJobInstance jobInstance;
	private List<BatchStepExecution> stepExecutions;
	private List<BatchJobExecutionParams> params;

	@Id
	@Column(name = "JOB_EXECUTION_ID")
	public Integer getId() {
		return id;
	}

	@Column(name = "CREATE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateTime() {
		return createTime;
	}

	@Column(name = "START_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getStartTime() {
		return startTime;
	}

	@Column(name = "END_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndTime() {
		return endTime;
	}

	@Column(name = "LAST_UPDATED")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastUpdated() {
		return lastUpdated;
	}

	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	@Column(name = "EXIT_CODE")
	public String getExitCode() {
		return exitCode;
	}

	@Column(name = "EXIT_MESSAGE")
	public String getExitMessage() {
		return exitMessage;
	}

	@Column(name = "VERSION")
	public Long getVersion() {
		return version;
	}

	@Column(name = "JOB_CONFIGURATION_LOCATION")
	public String getJobConfigurationLocation() {
		return jobConfigurationLocation;
	}

	@OneToMany(mappedBy = "jobExecution", fetch = FetchType.LAZY)
	public List<BatchStepExecution> getStepExecutions() {
		return stepExecutions;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "JOB_INSTANCE_ID")
	public BatchJobInstance getJobInstance() {
		return jobInstance;
	}

	@OneToMany(mappedBy = "batchJobExecution", fetch = FetchType.LAZY)
	public List<BatchJobExecutionParams> getParams() {
		return params;
	}

}
