package org.trescal.cwms.batch.config.entities.batchjobexecution;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;

public interface BatchJobExecutionDao extends BaseDao<BatchJobExecution, Integer> {

	List<BatchJobExecution> fetchAllBatchJobExecutionsAndStepsByBatchJobId(Integer batchJobId);

}
