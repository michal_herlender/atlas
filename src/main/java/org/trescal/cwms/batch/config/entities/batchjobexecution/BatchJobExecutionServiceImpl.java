package org.trescal.cwms.batch.config.entities.batchjobexecution;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;

@Service
public class BatchJobExecutionServiceImpl extends BaseServiceImpl<BatchJobExecution, Integer>
		implements BatchJobExecutionService {

	@Autowired
	private BatchJobExecutionDao dao;

	@Override
	protected BaseDao<BatchJobExecution, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public List<BatchJobExecution> fetchAllBatchJobExecutionsAndStepsByBatchJobId(Integer batchJobId){
		return dao.fetchAllBatchJobExecutionsAndStepsByBatchJobId(batchJobId);
	}
	
}
