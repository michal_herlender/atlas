package org.trescal.cwms.batch.config.entities.batchjobexecution;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;

public interface BatchJobExecutionService extends BaseService<BatchJobExecution, Integer> {

	List<BatchJobExecution> fetchAllBatchJobExecutionsAndStepsByBatchJobId(Integer batchJobId);

}
