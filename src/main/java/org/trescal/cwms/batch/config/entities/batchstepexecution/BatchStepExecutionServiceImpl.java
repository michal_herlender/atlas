package org.trescal.cwms.batch.config.entities.batchstepexecution;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTask;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResultService;
import org.trescal.cwms.batch.controller.AnalysisResultJsonController.AnalysisResultCellDto;
import org.trescal.cwms.batch.controller.AnalysisResultJsonController.AnalysisResultRowDto;
import org.trescal.cwms.batch.controller.AnalysisResultJsonController.RowsToDisplayEnum;
import org.trescal.cwms.batch.jobs.importInstrument.InstrumentImportConfig;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.db.ExchangeFormatFieldNameDetailsService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BatchStepExecutionServiceImpl extends BaseServiceImpl<BatchStepExecution, Integer>
	implements BatchStepExecutionService {

	@Autowired
	private ExchangeFormatFieldNameDetailsService efFieldNameDetailsService;
	@Autowired
	private ItemAnalysisResultService itemAnalysisResultService;
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private BatchStepExecutionDao dao;
	@Autowired
	private ServletContext servletContext;
	@Autowired
	private JobExplorer jobExplorer;

	@Override
	protected BaseDao<BatchStepExecution, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public PagedResultSet<AnalysisResultRowDto> getFileDataWithAnalysisResults(Integer batchStepExecutionId,
			Integer page, Integer maxResults, RowsToDisplayEnum rowsToDisplay) {

		PagedResultSet<AnalysisResultRowDto> res = new PagedResultSet<>(maxResults, page);
		res.setResults(new ArrayList<>());

		BatchStepExecution stepExe = this.get(batchStepExecutionId);
		BackgroundTask bgTask = stepExe.getJobExecution().getJobInstance().getBgTask();
		ExchangeFormatFile efFile = efFileService.get(bgTask.getEfFile().getId());
		ExchangeFormat ef = bgTask.getEf();

		try {

			File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
			// get file
			File file = this.efFileService.getExcelFile(efFile, tempDir);

			// get content
			Integer startIndex = (maxResults * page) - maxResults + ef.getLinesToSkip() + 1;
			Integer endIndex = startIndex + maxResults - 1;
			Pair<Integer, ArrayList<LinkedCaseInsensitiveMap<String>>> pair = this.excelFileReaderUtil
					.readExcelFile(new FileInputStream(file), null, ef.getLinesToSkip(), startIndex, endIndex);
			ArrayList<LinkedCaseInsensitiveMap<String>> content = pair.getRight();

			// get total rows / calculate number of pages
			Integer totalFileRows = pair.getLeft();
			res.setResultsCount(totalFileRows);

			// get all analysis results for range of indexes
			List<ItemAnalysisResult> iar = itemAnalysisResultService.getAllAnalysisResults(batchStepExecutionId,
					startIndex, endIndex);

			// get ef columns details
			List<ExchangeFormatFieldNameDetails> efColumnsDetails = efFieldNameDetailsService
					.getByExchangeFormat(ef.getId());

			// construct dtos
			for (int i = 0; i < content.size(); i++) {

				int currentIndex = i + startIndex;

				AnalysisResultRowDto row = new AnalysisResultRowDto(currentIndex, new ArrayList<>());

				LinkedCaseInsensitiveMap<String> data = content.get(i);
				for (String key : data.keySet()) {

					// find ef field
					ExchangeFormatFieldNameDetails eFfield = efColumnsDetails.stream().filter(f -> {
						if (StringUtils.isBlank(f.getTemplateName())) {
							return f.getFieldName().getValue().equalsIgnoreCase(key.trim());
						} else {
							return f.getTemplateName().equalsIgnoreCase(key.trim());
						}
					}).findFirst().orElse(null);

					if (eFfield == null)
						continue;

					// find warnings
					Set<String> warnings = iar.stream().filter(ia -> {
						return ia.getIndex().equals(currentIndex) && StringUtils.isNotBlank(ia.getWarning())
								&& ia.getField().equals(eFfield.getFieldName());
					}).map(ia -> ia.getWarning()).collect(Collectors.toSet());

					// find errors
					Set<String> errors = iar.stream().filter(ia -> {
						return ia.getIndex().equals(currentIndex) && StringUtils.isNotBlank(ia.getError())
								&& ia.getField().equals(eFfield.getFieldName());
					}).map(ia -> ia.getError()).collect(Collectors.toSet());

					AnalysisResultCellDto cell = new AnalysisResultCellDto(eFfield.getFieldName(), key, data.get(key),
							warnings, errors);

					row.getData().add(cell);
				}

				/** Choose what rows to display */
				if (rowsToDisplay.equals(RowsToDisplayEnum.ALL))
					res.getResults().add(row);
				else if (rowsToDisplay.equals(RowsToDisplayEnum.ONLY_ROWS_WITH_ERRORS)) {
					boolean rowHasErrors = row.getData().stream()
							.anyMatch(c -> c.getErrors() != null && !c.getErrors().isEmpty());
					if (rowHasErrors)
						res.getResults().add(row);
				} else if (rowsToDisplay.equals(RowsToDisplayEnum.ONLY_ROWS_WITH_WARNINGS)) {
					boolean rowHasWarnings = row.getData().stream()
							.anyMatch(c -> c.getWarnings() != null && !c.getWarnings().isEmpty());
					if (rowHasWarnings)
						res.getResults().add(row);
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		/** update metadata fields */
		updateMetadataFields(bgTask, stepExe, res);

		return res;
	}

	private void updateMetadataFields(BackgroundTask bgTask, BatchStepExecution stepExe,
			PagedResultSet<AnalysisResultRowDto> res) {
		/** if it's an Instrument import job */
		if (bgTask.getJobInstance().getJobName().equals(InstrumentImportConfig.JOB_NAME)) {

			BatchStepExecution submitBatchSe = this.getLastCompletedStep(stepExe.getJobExecution().getId(),
					BatchConfig.SUBMIT_STEP);

			/** if it has a submit step, get the plantids of the added instrument */
			if (submitBatchSe != null) {

				StepExecution se = jobExplorer.getStepExecution(stepExe.getJobExecution().getId().longValue(),
						submitBatchSe.getId().longValue());

				/**
				 * put metadata for the plantid of the added instrument on row
				 */
				@SuppressWarnings("unchecked")
				Map<Integer, Integer> indexToPlantId = (Map<Integer, Integer>) se.getExecutionContext()
						.get(InstrumentImportConfig.JOB_EC_INDEX_TO_PLANTID_MAP);
				res.getResults().forEach(row -> {
					if (indexToPlantId.get(row.getIndex()) != null)
						row.setMetadata(indexToPlantId.get(row.getIndex()).toString());
				});

			}
		}
	}

	@Override
	public BatchStepExecution getLastCompletedStep(Integer lastJobExecutionId, String stepName) {
		return dao.getLastCompletedStep(lastJobExecutionId, stepName);
	}

}
