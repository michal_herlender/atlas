package org.trescal.cwms.batch.config.entities.itemanalysisresult;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution_;
import org.trescal.cwms.batch.controller.AnalysisResultJsonController.AnalysisResultErrorsAndWarnings;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;

@Repository("ItemAnalysisResultDao")
public class ItemAnalysisResultDaoImpl extends BaseDaoImpl<ItemAnalysisResult, Integer>
		implements ItemAnalysisResultDao {

	@Override
	protected Class<ItemAnalysisResult> getEntity() {
		return ItemAnalysisResult.class;
	}

	@Override
	public List<ItemAnalysisResult> getAllAnalysisResults(Integer batchStepExecutionId, Integer startIndex,
			Integer endIndex) {
		return getResultList(cb -> {
			CriteriaQuery<ItemAnalysisResult> cq = cb.createQuery(ItemAnalysisResult.class);
			Root<ItemAnalysisResult> root = cq.from(ItemAnalysisResult.class);
			Join<ItemAnalysisResult, BatchStepExecution> stepJoin = root.join(ItemAnalysisResult_.stepExecution);

			cq.where(cb.and(cb.equal(stepJoin.get(BatchStepExecution_.id), batchStepExecutionId),
					cb.between(root.get(ItemAnalysisResult_.index), startIndex, endIndex)));

			return cq;
		});
	}

	@Override
	public List<AnalysisResultErrorsAndWarnings> getResultsIndexesWithErrorsAndWarnings(Integer batchStepExecutionId) {
		return getResultList(cb -> {
			CriteriaQuery<AnalysisResultErrorsAndWarnings> cq = cb.createQuery(AnalysisResultErrorsAndWarnings.class);
			Root<ItemAnalysisResult> root = cq.from(ItemAnalysisResult.class);
			Join<ItemAnalysisResult, BatchStepExecution> stepJoin = root.join(ItemAnalysisResult_.stepExecution);

			cq.where(cb.and(cb.equal(stepJoin.get(BatchStepExecution_.id), batchStepExecutionId),
					cb.or(cb.isNotNull(root.get(ItemAnalysisResult_.error)),
							cb.isNotNull(root.get(ItemAnalysisResult_.warning)))));

			cq.select(cb.construct(AnalysisResultErrorsAndWarnings.class, root.get(ItemAnalysisResult_.index),
					cb.selectCase().when(root.get(ItemAnalysisResult_.error).isNull(), false).otherwise(true),
					cb.selectCase().when(root.get(ItemAnalysisResult_.warning).isNull(), false).otherwise(true)));

			return cq;
		});
	}

}
