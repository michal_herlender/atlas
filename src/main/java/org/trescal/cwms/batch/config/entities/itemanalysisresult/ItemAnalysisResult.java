package org.trescal.cwms.batch.config.entities.itemanalysisresult;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;

@Entity
@Table(name = "itemanalysisresult")
public class ItemAnalysisResult implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer index;
	private ExchangeFormatFieldNameEnum field;
	private String error;
	private String warning;
	private BatchStepExecution stepExecution;
	
	public ItemAnalysisResult() {
	}

	public ItemAnalysisResult(Integer index, ExchangeFormatFieldNameEnum field, String error, String warning) {
		super();
		this.index = index;
		this.field = field;
		this.error = error;
		this.warning = warning;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Min(0)
	@NotNull
	@Column(name = "itemIndex")
	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	@NotNull
	@Column(name = "field")
	@Enumerated(EnumType.STRING)
	public ExchangeFormatFieldNameEnum getField() {
		return field;
	}

	public void setField(ExchangeFormatFieldNameEnum field) {
		this.field = field;
	}

	@Column(name = "error")
	public String getError() {
		return error;
	}

	public void setError(String errors) {
		this.error = errors;
	}

	@Column(name = "warning")
	public String getWarning() {
		return warning;
	}

	public void setWarning(String warnings) {
		this.warning = warnings;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stepexecutionid")
	public BatchStepExecution getStepExecution() {
		return stepExecution;
	}

	public void setStepExecution(BatchStepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

}
