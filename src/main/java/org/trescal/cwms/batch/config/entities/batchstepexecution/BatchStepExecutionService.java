package org.trescal.cwms.batch.config.entities.batchstepexecution;

import org.trescal.cwms.batch.controller.AnalysisResultJsonController.AnalysisResultRowDto;
import org.trescal.cwms.batch.controller.AnalysisResultJsonController.RowsToDisplayEnum;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface BatchStepExecutionService extends BaseService<BatchStepExecution, Integer> {

	PagedResultSet<AnalysisResultRowDto> getFileDataWithAnalysisResults(Integer batchStepExecutionId, Integer page,
			Integer maxResults, RowsToDisplayEnum rowsToDisplay);

	BatchStepExecution getLastCompletedStep(Integer lastJobExecutionId, String stepName);

}
