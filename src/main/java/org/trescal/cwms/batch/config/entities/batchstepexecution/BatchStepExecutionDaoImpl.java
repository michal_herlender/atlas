package org.trescal.cwms.batch.config.entities.batchstepexecution;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.batch.core.ExitStatus;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;

@Repository("BatchStepExecutionDao")
public class BatchStepExecutionDaoImpl extends BaseDaoImpl<BatchStepExecution, Integer>
		implements BatchStepExecutionDao {

	@Override
	protected Class<BatchStepExecution> getEntity() {
		return BatchStepExecution.class;
	}

	@Override
	public BatchStepExecution getLastCompletedStep(Integer lastJobExecutionId, String stepName) {
		return getFirstResult(cb -> {
			CriteriaQuery<BatchStepExecution> cq = cb.createQuery(BatchStepExecution.class);
			Root<BatchStepExecution> root = cq.from(BatchStepExecution.class);
			Join<BatchStepExecution, BatchJobExecution> jobExecution = root.join(BatchStepExecution_.jobExecution);
			cq.where(cb.and(
					cb.equal(root.get(BatchStepExecution_.status), ExitStatus.COMPLETED.getExitCode()),
					cb.equal(root.get(BatchStepExecution_.stepName), stepName),
					cb.equal(jobExecution.get(BatchJobExecution_.id), lastJobExecutionId)));
			cq.orderBy(cb.desc(root.get(BatchStepExecution_.id)));
			return cq;
		}).orElse(null);
	}

}
