package org.trescal.cwms.batch.config.entities.itemanalysisresult;

import java.util.List;

import org.trescal.cwms.batch.controller.AnalysisResultJsonController.AnalysisResultErrorsAndWarnings;
import org.trescal.cwms.core.audit.entity.db.BaseDao;

public interface ItemAnalysisResultDao extends BaseDao<ItemAnalysisResult, Integer> {

	List<ItemAnalysisResult> getAllAnalysisResults(Integer batchStepExecutionId, Integer startIndex, Integer endIndex);

	List<AnalysisResultErrorsAndWarnings> getResultsIndexesWithErrorsAndWarnings(Integer batchStepExecutionId);

}
