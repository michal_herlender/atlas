package org.trescal.cwms.batch.config.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Setter;

@Entity
@Setter
@Table(name = "BATCH_JOB_EXECUTION_CONTEXT")
public class BatchJobExecutionContext {

	private Long id;
	private String shortContext;
	private String serializedContext;

	@Id
	@Column(name = "JOB_EXECUTION_ID")
	public Long getId() {
		return id;
	}

	@Column(name = "SHORT_CONTEXT")
	public String getShortContext() {
		return shortContext;
	}

	@Column(name = "SERIALIZED_CONTEXT")
	public String getSerializedContext() {
		return serializedContext;
	}

}
