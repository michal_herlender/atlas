package org.trescal.cwms.batch.config.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Setter;

@Entity
@Setter
@Table(name = "BATCH_STEP_EXECUTION_CONTEXT")
public class BatchStepExecutionContext {

	private Long id;
	private String shortContext;
	private String serializedContext;

	@Id
	@Column(name = "STEP_EXECUTION_ID")
	public Long getId() {
		return id;
	}

	@Column(name = "SHORT_CONTEXT", columnDefinition = "varchar(2500)")
	public String getShortContext() {
		return shortContext;
	}

	@Column(name = "SERIALIZED_CONTEXT", columnDefinition = "text")
	public String getSerializedContext() {
		return serializedContext;
	}

}
