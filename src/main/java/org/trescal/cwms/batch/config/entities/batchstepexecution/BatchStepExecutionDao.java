package org.trescal.cwms.batch.config.entities.batchstepexecution;

import org.trescal.cwms.core.audit.entity.db.BaseDao;

public interface BatchStepExecutionDao extends BaseDao<BatchStepExecution, Integer> {

	BatchStepExecution getLastCompletedStep(Integer lastJobExecutionId, String stepName);

}
