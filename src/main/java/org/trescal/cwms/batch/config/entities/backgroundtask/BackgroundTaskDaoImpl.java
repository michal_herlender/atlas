package org.trescal.cwms.batch.config.entities.backgroundtask;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.batch.config.entities.BatchJobInstance;
import org.trescal.cwms.batch.config.entities.BatchJobInstance_;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution_;
import org.trescal.cwms.batch.controller.BgTasksController.BgTaskDto;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat_;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile_;
import org.trescal.cwms.core.tools.PagedResultSet;

@Repository("BackgroundTaskDao")
public class BackgroundTaskDaoImpl extends BaseDaoImpl<BackgroundTask, Integer> implements BackgroundTaskDao {

	@Override
	protected Class<BackgroundTask> getEntity() {
		return BackgroundTask.class;
	}

	@Override
	public void getBgTaskDtos(PagedResultSet<BgTaskDto> pagedResultSet, Integer id, Integer submitterContactId,
			String jobname, Integer allocatedSubdivId) {

		completePagedResultSet(pagedResultSet, BgTaskDto.class, cb -> cq -> {

			Root<BackgroundTask> bgTask = cq.from(BackgroundTask.class);

			Join<BackgroundTask, Contact> submitterJoin = bgTask.join(BackgroundTask_.submittedBy);
			Join<BackgroundTask, ExchangeFormatFile> efFileJoin = bgTask.join(BackgroundTask_.efFile);
			Join<BackgroundTask, ExchangeFormat> efJoin = bgTask.join(BackgroundTask_.ef);
			Join<BackgroundTask, Subdiv> businessSubdivJoin = bgTask.join(BackgroundTask_.organisation.getName());
			Join<BackgroundTask, Subdiv> clientSubdivJoin = bgTask.join(BackgroundTask_.clientSubdiv);
			Join<Subdiv, Company> clientCompanyJoin = clientSubdivJoin.join(Subdiv_.comp);

			Join<BackgroundTask, BatchJobInstance> batchJobJoin = bgTask.join(BackgroundTask_.jobInstance);

			// count executions subquery
			Subquery<Long> countExecutionsSq = cq.subquery(Long.class);
			Root<BatchJobExecution> batchJobExecution = countExecutionsSq.from(BatchJobExecution.class);
			countExecutionsSq.where(cb.equal(batchJobExecution.get(BatchJobExecution_.jobInstance),
					batchJobJoin.get(BatchJobInstance_.id)));
			countExecutionsSq.select(cb.count(batchJobExecution));

			// last executions id subquery
			Subquery<Integer> lastExecutionsIdSq = cq.subquery(Integer.class);
			Root<BatchJobExecution> batchJobLastExecutionId = lastExecutionsIdSq.from(BatchJobExecution.class);
			lastExecutionsIdSq.where(cb.equal(batchJobLastExecutionId.get(BatchJobExecution_.jobInstance),
					batchJobJoin.get(BatchJobInstance_.id)));
			lastExecutionsIdSq.select(cb.max(batchJobLastExecutionId.get(BatchJobExecution_.id)));

			// last execution status subquery
			Subquery<String> lastExecutionsSq = cq.subquery(String.class);
			Root<BatchJobExecution> batchJobLastExecution = lastExecutionsSq.from(BatchJobExecution.class);
			lastExecutionsSq.where(
					cb.equal(batchJobLastExecution.get(BatchJobExecution_.id), lastExecutionsIdSq.getSelection()));
			lastExecutionsSq.select(batchJobLastExecution.get(BatchJobExecution_.status));

			// last execution message subquery
			Subquery<String> lastExecutionsSq2 = cq.subquery(String.class);
			Root<BatchJobExecution> batchJobLastExecution2 = lastExecutionsSq2.from(BatchJobExecution.class);
			lastExecutionsSq2.where(
					cb.equal(batchJobLastExecution2.get(BatchJobExecution_.id), lastExecutionsIdSq.getSelection()));
			lastExecutionsSq2.select(batchJobLastExecution2.get(BatchJobExecution_.exitMessage));

			Predicate clauses = cb.conjunction();
			if (submitterContactId != null) {
				clauses.getExpressions().add(cb.equal(submitterJoin.get(Contact_.personid), submitterContactId));
			}
			if (jobname != null) {
				clauses.getExpressions()
						.add(cb.equal(bgTask.get(BackgroundTask_.jobInstance).get(BatchJobInstance_.jobName), jobname));
			}
			if (id != null) {
				clauses.getExpressions().add(cb.equal(bgTask.get(BackgroundTask_.id), id));
			}
			if (allocatedSubdivId != null) {
				clauses.getExpressions().add(cb.equal(bgTask.get(BackgroundTask_.organisation), allocatedSubdivId));
			}
			cq.where(clauses);
			Selection<BgTaskDto> selection = cb.construct(BgTaskDto.class, bgTask.get(BackgroundTask_.id),
					cb.concat(submitterJoin.get(Contact_.firstName),
							cb.concat(" ", submitterJoin.get(Contact_.lastName))),
					bgTask.get(BackgroundTask_.submitedOn), bgTask.get(BackgroundTask_.autoSubmitPolicy),
					efFileJoin.get(ExchangeFormatFile_.fileName), businessSubdivJoin.get(Subdiv_.subname),
					clientSubdivJoin.get(Subdiv_.subname), clientCompanyJoin.get(Company_.coname),
					clientSubdivJoin.get(Subdiv_.subdivid), clientCompanyJoin.get(Company_.coid),
					batchJobJoin.get(BatchJobInstance_.id), batchJobJoin.get(BatchJobInstance_.jobName),
					countExecutionsSq.getSelection(), lastExecutionsSq.getSelection(), lastExecutionsSq2.getSelection(),
					efJoin.get(ExchangeFormat_.name), lastExecutionsIdSq.getSelection());

			// Most recent
			List<Order> order = new ArrayList<>();
			order.add(cb.desc(bgTask.get(BackgroundTask_.id)));

			return Triple.of(bgTask, selection, order);
		});
	}

	@Override
	public BackgroundTask getByBatchJobInstanceId(Integer batchJobInstanceId) {
		return getFirstResult(cb -> {
			CriteriaQuery<BackgroundTask> cq = cb.createQuery(BackgroundTask.class);
			Root<BackgroundTask> root = cq.from(BackgroundTask.class);
			Join<BackgroundTask, BatchJobInstance> job = root.join(BackgroundTask_.jobInstance);
			cq.where(cb.equal(job.get(BatchJobInstance_.id), batchJobInstanceId));
			return cq;
		}).orElse(null);
	}
}
