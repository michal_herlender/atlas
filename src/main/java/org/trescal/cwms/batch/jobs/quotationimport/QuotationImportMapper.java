package org.trescal.cwms.batch.jobs.quotationimport;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.exchangeformat.EFRowMapper;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;

@Component
@StepScope
public class QuotationImportMapper
		implements EFRowMapper<ImportedQuotationItemsSynthesisRowDTO, LinkedCaseInsensitiveMap<String>> {

	@Autowired
	ExchangeFormatService exchangeFormatService;
	@Autowired
	ServiceTypeService serviceTypeService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private AliasGroupService aliasGroupService;

	private ExchangeFormat ef;
	private Integer efId;

	public QuotationImportMapper(@Value("#{jobParameters['" + BatchConfig.EF_ID + "']}") Integer efId) {
		this.efId = efId;
	}

	@PostConstruct
	public void postConstruct() {
		this.ef = exchangeFormatService.get(efId);
	}

	public ImportedQuotationItemsSynthesisRowDTO mapItem(LinkedCaseInsensitiveMap<String> fileContent, int itemIndex,
			Locale locale) {
		
		if (fileContent == null)
			return null;

		List<ExchangeFormatFieldNameEnum> fieldsWithExceptions = new ArrayList<>();

		ImportedQuotationItemsSynthesisRowDTO dto = new ImportedQuotationItemsSynthesisRowDTO();
		dto.setIndex(itemIndex);

		for (String key : fileContent.keySet()) {
			ExchangeFormatFieldNameEnum efFieldName = ef.getExchangeFormatFieldNameDetails().stream()
					.filter(e -> (e.getTemplateName() != null
							&& e.getTemplateName().trim().toLowerCase().equals(key.trim().toLowerCase()))
							|| (e.getFieldName().getValue().trim().toLowerCase().equals(key.trim().toLowerCase())))
					.map(e -> e.getFieldName()).findFirst().orElse(null);

			String columnValue = fileContent.get(key);

			if (efFieldName != null && StringUtils.isNoneBlank(columnValue))
			  try{
				switch (efFieldName) {

				case ID_TRESCAL:
					if (StringUtils.isNotBlank(columnValue))
						dto.setPlantId(Integer.valueOf(columnValue));
					break;

				case PLANT_NO:
					if (StringUtils.isNotBlank(columnValue))
						dto.setPlantNo(columnValue);
					break;
					
				case INSTRUMENT_MODEL_ID:
					if (StringUtils.isNotBlank(columnValue))
						dto.setModelid(Integer.valueOf(columnValue));
					break;
					
				case TML_INSTRUMENT_MODEL_ID:
					if (StringUtils.isNotBlank(columnValue))
						dto.setTmlid(Integer.valueOf(columnValue));
					break;

				case CATALOG_PRICE:
					dto.setCatalogPrice(Double.valueOf(columnValue));
					break;

				case DISCOUNT_RATE:
					dto.setDiscountRate(Double.valueOf(columnValue));
					break;

				case FINAL_PRICE:
					dto.setFinalPrice(Double.valueOf(columnValue));
					break;

				case PUBLIC_NOTE:
					dto.setPublicNote(columnValue);
					break;

				case PRIVATE_NOTE:
					dto.setPrivateNote(columnValue);
					break;
					
				case EXPECTED_SERVICE:
					dto.setServiceType(columnValue);
					ServiceType st = this.aliasGroupService
							.determineExpectedServiceTypeFromAliasOrValue(ef.getAliasGroup(), columnValue);
					if (st != null)
						dto.setServiceTypeId(st.getServiceTypeId());
					break;
				default:
					break;
				}
			  } catch (Exception ex) {
					fieldsWithExceptions.add(efFieldName);
					String errorMsg = messageSource.getMessage(
							"exchangeformat.import.validation.columnswithinvaliddata",
							new String[] { String.join(",",
									fieldsWithExceptions.stream().map(e -> e.getName()).collect(Collectors.toList())) },
							locale);
					ItemAnalysisResult ias = new ItemAnalysisResult(itemIndex, efFieldName, errorMsg, null);
					dto.getItemAnalysisResult().add(ias);
				}
		}
		
		return dto;
	}

	@Override
	public ImportedQuotationItemsSynthesisRowDTO map(LinkedCaseInsensitiveMap<String> fileContent, int itemIndex,
			Locale locale) {
		return mapItem(fileContent, itemIndex, locale);
	}

}
