package org.trescal.cwms.batch.jobs.operationsimport;

import io.vavr.control.Either;
import io.vavr.control.Option;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatzipfile.OperationsImportZipFile;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatOperationTypeEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemImportInfoDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.tools.function.Consumer4;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;

@Component
@StepScope
public class OperationsImportBusinessValidationProcessor
	implements ItemProcessor<List<ImportedOperationItem>, List<ItemAnalysisResult>> {

	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private InstrumService instrumService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private CertificateService certificateService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private ServiceCapabilityService serviceCapabilityService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private FaultReportService faultReportService;
    @Autowired
    private ExchangeFormatFileService efFileService;
    @Autowired
    private CapabilityService capabilityService;

    private final Integer clientSubdivId;
    private final Integer businessSubdivId;
    private final Integer jobId;
    private final Integer efFileId;
    protected ExchangeFormatFile efFile;
    private final Locale locale;
    private Integer clientCompanyId;
    private List<JobItemImportInfoDto> jobItemsInfo;
    private ExecutionContext jobExecutionContext;
    private final boolean autoContractReview;
    private final boolean autoCreateJobItems;
    private final List<Integer> alreadySelectedWr;
    private final List<String> passedJobItemEquivalentIds;
    private List<String> certificatesFileNames;

    public OperationsImportBusinessValidationProcessor(
            @Value("#{jobParameters['" + BatchConfig.CLIENT_SUBDIV_ID + "']}") Integer clientSubdivId,
            @Value("#{jobParameters['" + OperationsImportConfig.BUSINESS_SUBDIV_ID + "']}") Integer businessSubdivId,
            @Value("#{jobParameters['" + OperationsImportConfig.JOB_ID + "']}") Integer jobId,
            @Value("#{jobParameters['" + BatchConfig.EF_FILE_ID + "']}") Integer efFileId,
            @Value("#{jobParameters['" + OperationsImportConfig.AUTO_CONTRACT_REVIEW + "']}") String autoContractReview,
            @Value("#{jobParameters['" + OperationsImportConfig.AUTO_CREATE_JOBITEMS + "']}") String autoCreateJobItems,
            @Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale) {
		this.locale = new Locale(locale);
		this.clientSubdivId = clientSubdivId;
		this.businessSubdivId = businessSubdivId;
		this.jobId = jobId;
		this.efFileId = efFileId;
		this.autoContractReview = Boolean.parseBoolean(autoContractReview);
		this.autoCreateJobItems = Boolean.parseBoolean(autoCreateJobItems);
		this.alreadySelectedWr = new ArrayList<>();
		this.passedJobItemEquivalentIds = new ArrayList<>();
	}

	@PostConstruct
	public void postConstruct() {
		this.clientCompanyId = companyService.getCompanyId(clientSubdivId);
		this.jobItemsInfo = jobItemService.getJobItemsInfo(jobId);
		this.efFile = efFileService.get(this.efFileId);
	}

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.jobExecutionContext = stepExecution.getJobExecution().getExecutionContext();
		this.certificatesFileNames = (List<String>) this.jobExecutionContext
				.get(OperationsImportConfig.JOB_EC_CERTIFICATES_FILENAMES);
	}

    @Override
    public List<ItemAnalysisResult> process(List<ImportedOperationItem> group) {

        // check all rows inside checked group
        group.forEach(row -> row.setChecked(true));

        List<ItemAnalysisResult> groupAnalysisResults = new ArrayList<>();
        for (int i = 0; i < group.size(); i++) {
            ImportedOperationItem item = group.get(i);
            ImportedOperationItem nextItem = i + 1 < group.size() ? group.get(i + 1) : null;
            List<ItemAnalysisResult> analysisResults = processItem(item, nextItem);
            groupAnalysisResults.addAll(analysisResults);
        }

		/*
		  try to find a valid work requirement, by : matching the service type if it
		  doesn't exist, and checking if it was not already selected by a different
		  group
		 */
        ImportedOperationItem firstRow = group.get(0);

        /** if jobitem is already created */
        if (firstRow.getJobitemId() != null) {
            JobItemImportInfoDto wrInfo = null;
            if (firstRow.getServiceTypeId() == null)
                wrInfo = jobItemsInfo.stream().filter(i -> i.getJobitemId().equals(firstRow.getJobitemId())
                                && i.getJiWrId() != null && !alreadySelectedWr.contains(i.getJiWrId())).findFirst()
                        .orElse(null);
			else
				wrInfo = jobItemsInfo.stream()
						.filter(i -> i.getJobitemId().equals(firstRow.getJobitemId()) && i.getJiWrId() != null
								&& firstRow.getServiceTypeId().equals(i.getServiceTypeId())
								&& !alreadySelectedWr.contains(i.getJiWrId()))
						.findFirst().orElse(null);

			if (wrInfo == null) {
				ItemAnalysisResult ias = addError(firstRow, ExchangeFormatFieldNameEnum.ID_TRESCAL,
						"importedcalibrationssynthesis.novalidworkrequirement", new String[] {});
				groupAnalysisResults.add(ias);
			} else {
				// procedure
				boolean hasProcedure = wrInfo.getProcedureId() != null;
				if (!hasProcedure) {
					ItemAnalysisResult ias = addError(firstRow, ExchangeFormatFieldNameEnum.ID_TRESCAL,
							"error.rest.workrequirement.noprocedure", new String[] {});
					groupAnalysisResults.add(ias);
				}

				// service type match with one provided in the file (if any)
				if (firstRow.getServiceTypeId() != null) {
					boolean matchingServiceType = firstRow.getServiceTypeId().equals(wrInfo.getServiceTypeId());
					if (!matchingServiceType) {
						ItemAnalysisResult ias = addError(firstRow, ExchangeFormatFieldNameEnum.ID_TRESCAL,
								"importedcalibrationssynthesis.servicetypdoesntmatch", new String[] {});
						groupAnalysisResults.add(ias);
					}
				}
				alreadySelectedWr.add(wrInfo.getJiWrId());

			}
		} else {
			// the jobitem is yet to be created
			if (passedJobItemEquivalentIds.contains(firstRow.getJobitemEquivalentId()) && this.autoCreateJobItems) {
				ItemAnalysisResult ias = addError(firstRow, ExchangeFormatFieldNameEnum.ID_TRESCAL,
						"importedcalibrationssynthesis.multiplewrrequired", new String[] {});
				groupAnalysisResults.add(ias);
			}
        }

        passedJobItemEquivalentIds.add(firstRow.getJobitemEquivalentId());

        // no errors or warnings
        if (groupAnalysisResults.isEmpty())
            return null;
        return groupAnalysisResults;
    }

    @lombok.Value(staticConstructor = "of")
    private static class WarningMessageContainer {

        public static WarningMessageContainer ofPlantId(Boolean plantIdError, String plantIdMessage) {
            return WarningMessageContainer.of(plantIdError, plantIdMessage, false, null, false, null);
        }

        public static WarningMessageContainer ofPlantNumber(Boolean plantNumberError, String plantNumberMessage) {
            return WarningMessageContainer.of(false, null,
                    plantNumberError, plantNumberMessage, false, null);
        }

        Boolean plantIdError;
        String plantIdMessage;

        Boolean plantNumberError;
        String plantNumberMessage;

        Boolean serialNumberError;
        String serialNumberMessage;
    }

    private enum MessageType {
        DUPLICATES, NOT_FOUND
    }

    private String warningMessageIfNotEmpty(MessageType messageType, ExchangeFormatFieldNameEnum field, String value, boolean hasNoFieldError) {
        if (StringUtils.isBlank(value)) return "";
        switch (messageType) {
            case DUPLICATES:
                return hasNoFieldError && field == ExchangeFormatFieldNameEnum.PLANT_NO ? "exchangeformat.import.itemstatus.mulitpleinstrumentswithsameplantno" :
                        hasNoFieldError && field == ExchangeFormatFieldNameEnum.SERIAL_NUMBER ? "exchangeformat.import.itemstatus.mulitpleinstrumentswithsameserialno"
                                : "";
            case NOT_FOUND:
                return hasNoFieldError ? "importedcalibrationssynthesis.instrumentnotfound" : "";
        }
        return "Java 8 is wreid";
    }

    private Either<WarningMessageContainer, Integer> obtainIdentyfiedInstrumentByPlantIdPlantNoOrSerialNo(Integer coid, Integer plantId, String plantNo, String serialNo, boolean hasPlandIdNoFieldError, boolean hasPlantNumberNoFieldError, boolean hasSerialNumberNoFieldError) {
        if (plantId == null && StringUtils.isBlank(plantNo) && StringUtils.isBlank(serialNo))
            return Either.left(WarningMessageContainer.ofPlantNumber(true, "instrument.new.plantno.warning"));
        val instruents = instrumService.getByOwningCompany(plantId, coid, plantNo, serialNo);
        if (instruents.size() > 1) {
            val plantNumberMessage = warningMessageIfNotEmpty(MessageType.DUPLICATES, ExchangeFormatFieldNameEnum.PLANT_NO, plantNo, hasPlantNumberNoFieldError);
            val serialNumberMessage = warningMessageIfNotEmpty(MessageType.DUPLICATES, ExchangeFormatFieldNameEnum.SERIAL_NUMBER, serialNo, hasSerialNumberNoFieldError);
            return Either.left(WarningMessageContainer.of(false, null, false, plantNumberMessage, false, serialNumberMessage));
        }
        val indentifiedId = instruents.stream().findFirst().map(Instrument::getPlantid);
        return Option.ofOptional(indentifiedId).toEither(() -> produceNonInstrumentsWarnings(plantId, plantNo, serialNo, hasPlandIdNoFieldError, hasPlantNumberNoFieldError, hasSerialNumberNoFieldError));
    }

    private WarningMessageContainer produceNonInstrumentsWarnings(Integer plantId, String plantNo, String serialNo, boolean hasPlandIdNoFieldError, boolean hasPlantNumberNoFieldError, boolean hasSerialNumberNoFieldError) {
        if (plantId != null && hasPlandIdNoFieldError)
            return WarningMessageContainer.ofPlantId(true, "error.rest.common.instrument.notfound");
        val plantMessage = warningMessageIfNotEmpty(MessageType.NOT_FOUND, ExchangeFormatFieldNameEnum.PLANT_NO, plantNo, hasPlantNumberNoFieldError);
        val serialMessage = warningMessageIfNotEmpty(MessageType.NOT_FOUND, ExchangeFormatFieldNameEnum.SERIAL_NUMBER, serialNo, hasSerialNumberNoFieldError);
        return WarningMessageContainer.of(false, null, StringUtils.isNotBlank(plantMessage), plantMessage, StringUtils.isNotBlank(serialMessage), serialMessage);
    }

    private List<ItemAnalysisResult> processItem(ImportedOperationItem item, ImportedOperationItem nextItem) {
        /* identify instrument */
        Integer identifiedInstrumentPlantId = null;

        val plandIdFieldHasNoErrors = fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.ID_TRESCAL);
        val plandNumberFieldHasNoErrors = fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.PLANT_NO);
        val serialNumberFieldHasNoErrors = fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.SERIAL_NUMBER);


        val identifiedInstument = obtainIdentyfiedInstrumentByPlantIdPlantNoOrSerialNo(clientCompanyId, item.getPlantid(), item.getPlantno(), item.getSerialNo(),
                plandIdFieldHasNoErrors, plandNumberFieldHasNoErrors, serialNumberFieldHasNoErrors
        );
        if (plandIdFieldHasNoErrors || plandNumberFieldHasNoErrors || serialNumberFieldHasNoErrors) {
            identifiedInstrumentPlantId = identifiedInstument.fold(
                    warn -> {
                        if (item.getPlantid() != null)
                            addWarning(item, ExchangeFormatFieldNameEnum.ID_TRESCAL, warn.plantIdMessage, new String[]{item.getPlantid().toString()});
                        Consumer4<ImportedOperationItem, ExchangeFormatFieldNameEnum, String, String[]> plantNoLogger = warn.plantNumberError ? this::addWarning : this::addError;
                        Consumer4<ImportedOperationItem, ExchangeFormatFieldNameEnum, String, String[]> serialNoLogger = warn.plantNumberError ? this::addWarning : this::addError;
                        if (StringUtils.isNotBlank(warn.plantNumberMessage))
                            plantNoLogger.apply(item, ExchangeFormatFieldNameEnum.PLANT_NO, warn.plantNumberMessage, null);
                        if (StringUtils.isNotBlank(warn.serialNumberMessage))
                            serialNoLogger.apply(item, ExchangeFormatFieldNameEnum.SERIAL_NUMBER, warn.serialNumberMessage, null);
                        return null;
                    }, Function.identity()
            );
        }

        if (identifiedInstrumentPlantId == null && identifiedInstument.isLeft()) {
            val warn = identifiedInstument.getLeft();
            if (warn.plantIdError)
                addError(item, ExchangeFormatFieldNameEnum.ID_TRESCAL, "error.rest.common.instrument.notfound",
                        new String[]{item.getPlantid().toString()});
            if (warn.plantNumberError)
                addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "importedcalibrationssynthesis.instrumentnotfound",
                        null);
            if (warn.serialNumberError)
                addError(item, ExchangeFormatFieldNameEnum.SERIAL_NUMBER,
                        "importedcalibrationssynthesis.instrumentnotfound", null);
        }

        // save identidied plantid if the plantid field was not provided in the file
        if (item.getPlantid() == null && identifiedInstrumentPlantId != null) {
            @SuppressWarnings("unchecked")
            Map<Integer, Integer> identifiedInstruments = (Map<Integer, Integer>) this.jobExecutionContext
                    .get(OperationsImportConfig.JOB_EC_IDENTIFIED_INST);
            if (identifiedInstruments == null)
                identifiedInstruments = new HashMap<>();
            identifiedInstruments.put(item.getIndex(), identifiedInstrumentPlantId);
            this.jobExecutionContext.put(OperationsImportConfig.JOB_EC_IDENTIFIED_INST, identifiedInstruments);
		}

        /* try to read the dates */
		// get startdate
		if (fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.OPERATION_START_DATE)
				&& item.getOperationStartDate() == null)
			addError(item, ExchangeFormatFieldNameEnum.OPERATION_START_DATE,
					"importedcalibrationssynthesis.datedoesntexistorpoorlyformatted", null);

		// get operation date
		if (fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.OPERATION_DATE) && item.getOperationDate() == null)
			addError(item, ExchangeFormatFieldNameEnum.OPERATION_DATE,
					"importedcalibrationssynthesis.datedoesntexistorpoorlyformatted", null);

		// get operation validation date
		if (fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.OPERATION_VALIDATED_ON)
				&& item.getOperationValidatedOn() == null)
			addError(item, ExchangeFormatFieldNameEnum.OPERATION_VALIDATED_ON,
					"importedcalibrationssynthesis.datedoesntexistorpoorlyformatted", null);

        /* look for the technician by the hr id */
		if (fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.OPERATION_BY_HRID)
				&& StringUtils.isNotEmpty(item.getOperationByHrId())) {
			Contact technician = contactService.getByHrId(item.getOperationByHrId());
			if (technician == null)
				addError(item, ExchangeFormatFieldNameEnum.OPERATION_BY_HRID,
						"importedcalibrationssynthesis.techniciannotfound", null);
		} else {
			addError(item, ExchangeFormatFieldNameEnum.OPERATION_BY_HRID,
					"importedcalibrationssynthesis.techniciannotfound", null);
		}

        /* look for the technician who validated the operation by the hr id */
		if (fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.OPERATION_VALIDATED_BY_HRID)
				&& StringUtils.isNotEmpty(item.getOperationValidatedByHrId())) {
			Contact technician = contactService.getByHrId(item.getOperationValidatedByHrId());
			if (technician == null)
				addError(item, ExchangeFormatFieldNameEnum.OPERATION_VALIDATED_BY_HRID,
						"importedcalibrationssynthesis.techniciannotfound", null);
		} else {
			if (!item.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.ON_HOLD))
				addError(item, ExchangeFormatFieldNameEnum.OPERATION_VALIDATED_BY_HRID,
						"importedcalibrationssynthesis.techniciannotfound", null);
		}

		/*
		  check if the certificate number is not duplicated within the instrument
		  record
		 */
		if (item.getOperationType() != null && item.getOperationType().equals(ExchangeFormatOperationTypeEnum.CAL)
				&& fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.DOCUMENT_NO)
                && StringUtils.isNotEmpty(item.getDocumentNumber()) && identifiedInstrumentPlantId != null) {
            List<Certificate> certs = certificateService.getByThirdCertNoAndPlantId(item.getDocumentNumber(),
                    identifiedInstrumentPlantId);
            if (CollectionUtils.isNotEmpty(certs)) {
                addError(item, ExchangeFormatFieldNameEnum.DOCUMENT_NO,
                        "error.rest.common.certificate.create.thirdpartycertno.exists",
                        new String[]{item.getDocumentNumber()});
            }
        }

		/*
		  If a zip file is imported, check if for each certificate number that a
		  corresponding file exists.
		 */
        if (efFile.getType().equals(OperationsImportZipFile.class.getSimpleName())
                && StringUtils.isNotEmpty(item.getDocumentNumber())
                && item.getOperationType().equals(ExchangeFormatOperationTypeEnum.CAL)
                && certificatesFileNames != null) {
            boolean fileExists = certificatesFileNames.stream()
                    .anyMatch(c -> c.equalsIgnoreCase(item.getDocumentNumber()));
            if (!fileExists) {
                addError(item, ExchangeFormatFieldNameEnum.DOCUMENT_NO,
                        "importedcalibrationssynthesis.certificatefilenotfound",
						new String[] { item.getDocumentNumber() });
			}
		}

        /* check operation type */
		if (item.getOperationType() == null) {
			addError(item, ExchangeFormatFieldNameEnum.OPERATION_TYPE, "error.rest.data.notfound", new String[] {});
		}

        /* identify jobitems, contract review */
		final Integer finalPlantId = identifiedInstrumentPlantId;
		boolean jobItemExists = jobItemsInfo.stream().anyMatch(i -> i.getPlantId().equals(finalPlantId));
		if (!jobItemExists) {
			if (!autoCreateJobItems)
				addError(item, ExchangeFormatFieldNameEnum.ID_TRESCAL, "error.jobitem.notfound.forbarcode",
						new String[] {});
			else {
				if (identifiedInstrumentPlantId != null) {

					JobItem alreadyActiveJobItem = jobItemService
							.findActiveJobItemFromBarcode(identifiedInstrumentPlantId);

					if (alreadyActiveJobItem != null) {
						addError(item, ExchangeFormatFieldNameEnum.ID_TRESCAL,
								"exchangeformat.import.itemstatus.alreadyactive", null);
					} else {

                        Instrument inst = instrumService.get(finalPlantId);
                        // instrument identified but jobitem is yet to be created automatically
                        // check for a default capability by service type and subdivision
                        val proc = this.capabilityService.getActiveCapabilityUsingCapabilityFilter(inst.getModel().getDescription().getId(),
                            item.getServiceTypeId(), this.businessSubdivId);
                        if (!proc.isPresent()) {
                            ServiceCapability serviceCapability = this.serviceCapabilityService.getCalService(
                                inst.getModel().getModelid(), item.getServiceTypeId(), this.businessSubdivId);
                            if (serviceCapability == null)
                                addWarning(item, ExchangeFormatFieldNameEnum.ID_TRESCAL,
                                    "error.jobitem.defaultprocedurenotfound", new String[]{});
                        }

                    }
					addWarning(item, ExchangeFormatFieldNameEnum.ID_TRESCAL, "error.jobitem.notfound.forbarcode",
							new String[] {});
				}
			}
		} else {

			// put in jobitem id
			Integer jobitemId = jobItemsInfo.stream().filter(i -> i.getPlantId().equals(finalPlantId))
					.map(JobItemImportInfoDto::getJobitemId).findFirst().orElse(null);
			item.setJobitemId(jobitemId);

			// contract reviewed
			boolean isContractReviewed = jobItemsInfo.stream()
					.anyMatch(i -> i.getPlantId().equals(finalPlantId) && i.getContractReviewed());
			if (!isContractReviewed) {
				if (autoContractReview)
					addWarning(item, ExchangeFormatFieldNameEnum.ID_TRESCAL,
							"lookupresultmessage.itemhasbeencontractreviewed.no", new String[] {});
				else
					addError(item, ExchangeFormatFieldNameEnum.ID_TRESCAL,
							"lookupresultmessage.itemhasbeencontractreviewed.no", new String[] {});
			}

		}

        /* check if owning subdiv of the failure report exists */
		if (item.getFrSubdivisionId() != null && fieldHasNoErrors(item, ExchangeFormatFieldNameEnum.FR_SUBDIV_ID)) {
			Subdiv frsubdiv = subdivService.get(item.getFrSubdivisionId());
			if (frsubdiv == null) {
				addError(item, ExchangeFormatFieldNameEnum.FR_SUBDIV_ID, "error.rest.subdiv.notfound", new String[] {});
			}
		}

		boolean operationIsFr = item.getOperationType() != null
				&& item.getOperationType().equals(ExchangeFormatOperationTypeEnum.FR);

        if (operationIsFr) {

            boolean nextDtoExistsAndCheckedAndOnSameJobitem = nextItem != null && nextItem.isChecked()
                    && nextItem.getJobitemEquivalentId().equals(item.getJobitemEquivalentId());

            // reject if the current failure report outcome is a 'REPAIR' or 'THIRD_PARTY' and there are
            // still lines to import on the same jobitem
            FailureReportFinalOutcome finaloutcome;
            if (item.getFrFinalOutcome() != null)
                finaloutcome = item.getFrFinalOutcome();
            else {
				ServiceType nextSt = null;
				if (nextItem != null && nextItem.getServiceTypeId() != null)
					nextSt = serviceTypeService.get(nextItem.getServiceTypeId());
				finaloutcome = faultReportService.deduceFrOutcomeFromRecommendation(item.getFrOperationByTrescal(),
						item.getFrRecommendations(), nextSt, item.getFrClientResponseNeeded(),
						item.getFrClientDecision(), true);
			}
			boolean frOutcomeIsRepairOrTP = FailureReportFinalOutcome.INTERNAL_REPAIR.equals(finaloutcome)
					|| FailureReportFinalOutcome.THIRD_PARTY_REPAIR.equals(finaloutcome)
					|| FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITH_JUDGMENT.equals(finaloutcome)
					|| FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT.equals(finaloutcome);
			if (nextDtoExistsAndCheckedAndOnSameJobitem && frOutcomeIsRepairOrTP) {
				addError(item, ExchangeFormatFieldNameEnum.DOCUMENT_NO,
						"importedcalibrationssynthesis.outcomeisrepairortpwithfollowinglines", new String[] {});
			}

			// reject if we are still expecting the client response (finaloutcome is empty)
			// and also we have still pending lines to import
			boolean expectingClientResponseWithoutFrOutcome = finaloutcome == null
					&& BooleanUtils.isTrue(item.getFrClientResponseNeeded()) && item.getFrClientDecision() == null;
			if (expectingClientResponseWithoutFrOutcome && nextDtoExistsAndCheckedAndOnSameJobitem) {
				addError(item, ExchangeFormatFieldNameEnum.DOCUMENT_NO,
						"importedcalibrationssynthesis.faultreportawaitingclientdecisionwithnextlinestoimport",
						new String[] {});
			}

			// reject if we couldn't deduce the final outcome of the FR (we will go to
			// 'awaiting selection of FR outcome')
			// and still pending calibraiton to import
			boolean couldNotDeduceAndStillExpectingLinesToImport = finaloutcome == null;
			if (couldNotDeduceAndStillExpectingLinesToImport && nextDtoExistsAndCheckedAndOnSameJobitem) {
				addError(item, ExchangeFormatFieldNameEnum.DOCUMENT_NO,
						"importedcalibrationssynthesis.clientrefusedwithlinestoimport", new String[] {});
			}
		}

		return item.getItemAnalysisResult();
	}

    private ItemAnalysisResult addError(ImportedOperationItem item, ExchangeFormatFieldNameEnum field,
                                        String messageCode, Object[] params) {
        ItemAnalysisResult ias = new ItemAnalysisResult(item.getIndex(), field,
                messageSource.getMessage(messageCode, params, locale), null);
        item.getItemAnalysisResult().add(ias);
        return ias;

    }

    private void addWarning(ImportedOperationItem item, ExchangeFormatFieldNameEnum field,
                            String messageCode, Object[] params) {
        ItemAnalysisResult ias = new ItemAnalysisResult(item.getIndex(), field, null,
                messageSource.getMessage(messageCode, params, locale));
        item.getItemAnalysisResult().add(ias);
    }

    private boolean fieldHasNoErrors(ImportedOperationItem item, ExchangeFormatFieldNameEnum field) {
        return item.getItemAnalysisResult().stream().noneMatch(ias -> ias.getField().equals(field));
    }

}
