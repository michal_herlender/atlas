package org.trescal.cwms.batch.jobs.importInstrument;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.repeat.policy.SimpleCompletionPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.exchangeformat.AutoSubmitPolicyDecider;
import org.trescal.cwms.batch.exchangeformat.GlobalJobListener;

@Configuration
public class InstrumentImportConfig {

	public static final String JOB_NAME = "INSTRUMENT_IMPORT_JOB";
	private static final int ANALYSIS_CHUNK_SIZE = 100;
	private static final int SUBMIT_CHUNK_SIZE = 100;
	private static final int TRANSACTION_TIMEOUT = 5000;

	/* Job Params */
	public static final String JOB_ID = "JOB_ID";
	public static final String CLIENT_SUBDIV_ID = "CLIENT_SUBDIV_ID";
	public static final String BUSINESS_SUBDIV_ID = "BUSINESS_SUBDIV_ID";
	public static final String DEFAULT_ADDRESS_ID = "DEFAULT_ADDRESS_ID";
	public static final String DEFAULT_CONTACT_ID = "DEFAULT_CONTACT_ID";
	public static final String DEFAULT_SERVICETYPE_ID = "DEFAULT_SERVICETYPE_ID";
	public static final String SUBMITED_BY = "SUBMITED_BY";

	/* Job execution context */
	public static final String JOB_EC_IDENTIFIED_INST = "JOB_EC_IDENTIFIED_INST";
	public static final String JOB_EC_INDEX_TO_PLANTID_MAP = "JOB_EC_MAPED_INSTRUMENT_TO_INDEX";
	public static final String JOB_EC_INSERTED_INSTRUMENTS = "JOB_EC_INSERTED_INSTRUMENTS";

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private StepBuilderFactory stepsBuilderFactory;
	@Autowired
	private AutoSubmitPolicyDecider autoSubmitPolicyDecider;
	@Autowired
	private GlobalJobListener globalListener;
	@Autowired
	private InstrumentImportReader instrumentImportReader;
	@Autowired
	private InstrumentImportBusinessValidationProcessor instrumentImportBusinessValidationProcessor;
	@Autowired
	private InstrumentImportAnalysisResultWriter instrumentImportAnalysisResultWriter;
	@Autowired
	private InstrumentImportSubmitter instrumentImportSubmitter;
	@Autowired
	private InstrumentImportFilterProcessor instrumentImportFilterProcessor;

	@Bean(JOB_NAME)
	public Job instrumentsImportJob() {
		return this.jobBuilderFactory.get(JOB_NAME).listener(globalListener)
				.start(instrumentsImportAnalysisStep()).next(autoSubmitPolicyDecider).on(AutoSubmitPolicyDecider.EXIT)
				.end().from(autoSubmitPolicyDecider).on("*").to(instrumentsImportSubmitStep()).end().build();
	}

	@Bean
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Step instrumentsImportAnalysisStep() {
		DefaultTransactionAttribute dta = new DefaultTransactionAttribute();
		dta.setTimeout(TRANSACTION_TIMEOUT);
		return stepsBuilderFactory.get(BatchConfig.ANALYSIS_STEP).chunk(new SimpleCompletionPolicy(ANALYSIS_CHUNK_SIZE))
				.reader(instrumentImportReader).processor((ItemProcessor) instrumentImportBusinessValidationProcessor)
				.writer(instrumentImportAnalysisResultWriter).transactionAttribute(dta).build();
	}

	@Bean
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Step instrumentsImportSubmitStep() {
		return stepsBuilderFactory.get(BatchConfig.SUBMIT_STEP).chunk(new SimpleCompletionPolicy(SUBMIT_CHUNK_SIZE))
				.reader(instrumentImportReader).processor((ItemProcessor) instrumentImportFilterProcessor)
				.writer(instrumentImportSubmitter).build();
	}

}
