package org.trescal.cwms.batch.jobs.quotationimport;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;
import org.trescal.cwms.core.quotation.service.AddInstModelToQuotationService;
import org.trescal.cwms.core.quotation.service.AddInstrumentsToQuotationService;

@Component
@StepScope
public class QuotationImportSubmitter implements ItemWriter<ImportedQuotationItemsSynthesisRowDTO> {

	@Autowired
	private AddInstrumentsToQuotationService instAddToQuotationService;
	@Autowired
	private AddInstModelToQuotationService instModelAddToQuotationService;
	@Autowired
	private QuotationService quoteService;
	@Autowired
	private QuotationItemService qiService;
	@Autowired
	private TaxCalculator taxCalculator;

	// shared data
	private Locale locale;
	private Integer businessSubdivId;
	private Integer quoteId;
	private Integer submittedById;
	private ExecutionContext jobExecutionContext;
	private Long oldCountInsertedItems = 0L;
	private Integer modelindex;
	private Integer instindex;
	private Map<Integer, Integer> indentifiedInstruments;
	private Map<Integer, Integer> indentifiedInstrumentModels;
	private Boolean priceAndServiceTypeFromFile;

	public QuotationImportSubmitter(@Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale,
			@Value("#{jobParameters['" + QuotationImportConfig.BUSINESS_SUBDIV_ID + "']}") Integer businessSubdivId,
			@Value("#{jobParameters['" + QuotationImportConfig.QUOTE_ID + "']}") Integer quoteId,
			@Value("#{jobParameters['" + BatchConfig.SUBMITTED_BY_ID + "']}") Integer submittedById,
			@Value("#{jobExecutionContext['" + QuotationImportConfig.JOB_EC_IDENTIFIED_INST
					+ "']}") Map<Integer, Integer> indentifiedInstruments,
			@Value("#{jobExecutionContext['" + QuotationImportConfig.JOB_EC_IDENTIFIED_INST_MODEL
					+ "']}") Map<Integer, Integer> indentifiedInstrumentModels,
			@Value("#{jobParameters['" + QuotationImportConfig.PRICE_SERVICE_TYPE_FROM_FILE
					+ "']}") String priceAndServiceTypeFromFile) {
		this.locale = new Locale(locale);
		this.businessSubdivId = businessSubdivId;
		this.quoteId = quoteId;
		this.submittedById = submittedById;
		this.indentifiedInstruments = indentifiedInstruments;
		this.indentifiedInstrumentModels = indentifiedInstrumentModels;
		this.priceAndServiceTypeFromFile = Boolean.valueOf(priceAndServiceTypeFromFile);
		this.modelindex = 1;
		this.instindex = 1;
	}

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.jobExecutionContext = stepExecution.getJobExecution().getExecutionContext();
	}

	@Override
	public void write(List<? extends ImportedQuotationItemsSynthesisRowDTO> items) throws Exception {

		List<Integer> instIndex = new ArrayList<Integer>();
		List<Integer> instModelIndex = new ArrayList<Integer>();
		Map<Integer, Integer> instModelIds = new HashMap<>();
		List<Integer> plantIds = new ArrayList<Integer>();
		Map<Integer, Integer> instwithindex = new HashMap<>();
		Map<Integer, Integer> instServiceTypeIds = new HashMap<>();
		Map<Integer, BigDecimal> instFinalPrices = new HashMap<>();
		Map<Integer, Integer> instModelServiceTypeIds = new HashMap<>();
		Map<Integer, BigDecimal> instModelFinalPrices = new HashMap<>();
		Map<Integer, BigDecimal> instDiscountPrices = new HashMap<>();
		Map<Integer, BigDecimal> instCatalogPrices = new HashMap<>();
		Map<Integer, String> instPublicNotes = new HashMap<>();
		Map<Integer, String> instPrivateNotes = new HashMap<>();
		Map<Integer, BigDecimal> instModelDiscountPrices = new HashMap<>();
		Map<Integer, BigDecimal> instModelCatalogPrices = new HashMap<>();
		Map<Integer, String> instModelPublicNotes = new HashMap<>();
		Map<Integer, String> instModelPrivateNotes = new HashMap<>();

		items.stream().forEach(row -> {
			// set plant ids / instrument model ids
			setIds(row);

			if (row.getPlantId() != null) {
				setInstLists(row, instIndex, plantIds, instwithindex, instServiceTypeIds, instFinalPrices, instDiscountPrices, instCatalogPrices,
						instPublicNotes, instPrivateNotes, priceAndServiceTypeFromFile);

			} else if (row.getModelid() != null) {
				setInstModelLists(row, instModelIndex , instModelIds, instModelServiceTypeIds, instModelFinalPrices,
						instModelDiscountPrices, instModelCatalogPrices, instModelPublicNotes, instModelPrivateNotes);
			}
		});

		Set<Quotationitem> result = new HashSet<>();

		// add quotation items created from instrument
		if (!instwithindex.isEmpty()) {
			if (priceAndServiceTypeFromFile) {
				// get Prices and Service Types from excel file
				result.addAll(instAddToQuotationService.importQuotationItemsUsingInst_PricesFromFile_BatchMode(businessSubdivId,
						submittedById, instIndex, plantIds, instwithindex, instServiceTypeIds, instFinalPrices, instDiscountPrices,
						instCatalogPrices, instPublicNotes, instPrivateNotes, quoteId, locale));
			} else {
				// get Prices and Service type from last job item
				result.addAll(instAddToQuotationService.importQuotationItemsUsingInst_PricesFromLatestJobItem(
						businessSubdivId, submittedById, plantIds, instDiscountPrices, instCatalogPrices,
						instPublicNotes, instPrivateNotes, quoteId, locale));
			}
		}

		// add quotation items created from instrument model
		if (!instModelIds.isEmpty()) {
			// get Prices and Service Types from excel file
			result.addAll(instModelAddToQuotationService.importQuotationItemsUsingInstModel(businessSubdivId,
					submittedById, instModelIndex, instModelIds, instModelServiceTypeIds, instModelFinalPrices, instModelDiscountPrices,
					instModelCatalogPrices, instModelPublicNotes, instModelPrivateNotes, quoteId, locale));

		}

		Quotation quotation = quoteService.get(quoteId);
		quotation.getQuotationitems().addAll(result);
		oldCountInsertedItems = oldCountInsertedItems + new Long(result.size());
		// update the costs
		CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
		taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
		// save implicitly needed before sort; as sort currently rebuilds
		// quotation item set.
		this.quoteService.save(quotation);
		// update the numbering of the items on the quotation
		this.qiService.sortQuotationItems(quotation);

		this.jobExecutionContext.put(QuotationImportConfig.JOB_EC_INSTERTED_QUOTATION_ITEMS, oldCountInsertedItems);
	}

	/**
	 * Set the value of plant id in the case of instrument and the model id in
	 * the case of instrument model
	 */
	private void setIds(ImportedQuotationItemsSynthesisRowDTO dto) {
		if (dto.getPlantId() == null && !StringUtils.isEmpty(dto.getPlantNo())
				&& !MapUtils.isEmpty(indentifiedInstruments)) {
			Integer plantId = indentifiedInstruments.get(dto.getIndex());
			dto.setPlantId(plantId);
		} else if (dto.getModelid() == null && dto.getTmlid() != null
				&& !MapUtils.isEmpty(indentifiedInstrumentModels)) {
			Integer modelid = indentifiedInstrumentModels.get(dto.getIndex());
			dto.setModelid(modelid);
		}
	}

	/**
	 * Fill in the data relating to the instrument
	 */
	private void setInstLists(ImportedQuotationItemsSynthesisRowDTO dto, List<Integer> instIndex, List<Integer> plantIds,
			Map<Integer, Integer> instWithIndex, Map<Integer, Integer> instServiceTypeIds, Map<Integer, BigDecimal> instFinalPrices,
			Map<Integer, BigDecimal> instDiscountPrices, Map<Integer, BigDecimal> instCatalogPrices,
			Map<Integer, String> instPublicNotes, Map<Integer, String> instPrivateNotes,
			Boolean priceAndServiceTypeFromFile) {

		instWithIndex.put(instindex, dto.getPlantId());
		instIndex.add(instindex);
		plantIds.add(dto.getPlantId());
		if (priceAndServiceTypeFromFile) {
			instServiceTypeIds.put(instindex, dto.getServiceTypeId());
			instFinalPrices.put(instindex, BigDecimal.valueOf(dto.getFinalPrice()));
		}
		if (dto.getDiscountRate() != null)
			instDiscountPrices.put(instindex, BigDecimal.valueOf(dto.getDiscountRate()));
		if (dto.getCatalogPrice() != null)
			instCatalogPrices.put(instindex, BigDecimal.valueOf(dto.getCatalogPrice()));
		if (!StringUtils.isEmpty(dto.getPublicNote()))
			instPublicNotes.put(instindex, dto.getPublicNote());
		if (!StringUtils.isEmpty(dto.getPrivateNote()))
			instPrivateNotes.put(instindex, dto.getPrivateNote());
		
		instindex++;
	}

	/**
	 * Fill in the data relating to the instrument model
	 */
	private void setInstModelLists(ImportedQuotationItemsSynthesisRowDTO dto, List<Integer> instModelIndex,
			Map<Integer, Integer> instModelIds, Map<Integer, Integer> instModelServiceTypeIds,
			Map<Integer, BigDecimal> instModelFinalPrices, Map<Integer, BigDecimal> instModelDiscountPrices,
			Map<Integer, BigDecimal> instModelCatalogPrices, Map<Integer, String> instModelPublicNotes,
			Map<Integer, String> instModelPrivateNotes) {

		instModelIds.put(modelindex, dto.getModelid());
		instModelIndex.add(modelindex);
		instModelServiceTypeIds.put(modelindex, dto.getServiceTypeId());
		instModelFinalPrices.put(modelindex, BigDecimal.valueOf(dto.getFinalPrice()));

		if (dto.getDiscountRate() != null)
			instModelDiscountPrices.put(modelindex, BigDecimal.valueOf(dto.getDiscountRate()));
		if (dto.getCatalogPrice() != null)
			instModelCatalogPrices.put(modelindex, BigDecimal.valueOf(dto.getCatalogPrice()));
		if (!StringUtils.isEmpty(dto.getPublicNote()))
			instModelPublicNotes.put(modelindex, dto.getPublicNote());
		if (!StringUtils.isEmpty(dto.getPrivateNote()))
			instModelPrivateNotes.put(modelindex, dto.getPrivateNote());
		
		modelindex++;
	}
}
