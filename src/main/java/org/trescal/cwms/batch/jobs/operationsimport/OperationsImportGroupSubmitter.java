package org.trescal.cwms.batch.jobs.operationsimport;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemImportInfoDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;

@Component
@StepScope
public class OperationsImportGroupSubmitter implements ItemWriter<List<ImportedOperationItem>> {

	private static final String TLM_IMPORTED = "TLM IMPORTED";
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private CalibrationProcessService calibrationProcessService;
	@Autowired
	private UserService userService;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobItemService jobItemService;

	private Locale locale;
	@Value("#{props['cwms.users.automatedusername']}")
	private String autoUserName;
	private Contact autoUser;
	private Integer businessSubdivId;
	private ExecutionContext jobExecutionContext;
	private Map<Integer, Integer> indentifiedInstruments;
	private Integer jobId;
	private List<JobItemImportInfoDto> jobItemsInfo;
	private Set<Integer> alreadySelectedWr;
	private boolean autoContractReview;
	private boolean autoCreateJobItems;

	public OperationsImportGroupSubmitter(
			@Value("#{jobParameters['" + OperationsImportConfig.JOB_ID + "']}") Integer jobId,
			@Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale,
			@Value("#{jobParameters['" + OperationsImportConfig.BUSINESS_SUBDIV_ID + "']}") Integer businessSubdivId,
			@Value("#{jobExecutionContext['" + OperationsImportConfig.JOB_EC_IDENTIFIED_INST
					+ "']}") Map<Integer, Integer> indentifiedInstruments,
			@Value("#{jobParameters['" + OperationsImportConfig.AUTO_CONTRACT_REVIEW + "']}") String autoContractReview,
			@Value("#{jobParameters['" + OperationsImportConfig.AUTO_CREATE_JOBITEMS
					+ "']}") String autoCreateJobItems) {
		this.locale = new Locale(locale);
		this.businessSubdivId = businessSubdivId;
		this.indentifiedInstruments = indentifiedInstruments;
		this.jobId = jobId;
		this.alreadySelectedWr = new HashSet<>();
		this.autoContractReview = Boolean.parseBoolean(autoContractReview);
		this.autoCreateJobItems = Boolean.parseBoolean(autoCreateJobItems);
	}

	@PostConstruct
	public void init() {
		this.jobItemsInfo = jobItemService.getJobItemsInfo(jobId);
		this.autoUser = this.userService.get(this.autoUserName).getCon();
	}

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.jobExecutionContext = stepExecution.getJobExecution().getExecutionContext();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void write(List<? extends List<ImportedOperationItem>> groups) throws Exception {

		if (groups.isEmpty())
			return;

		/**
		 * set plantids if null (case where the instruments were inddeitified by seralno
		 * or/and plantno)
		 */
		groups.stream().flatMap(gs -> gs.stream()).filter(d -> d.getPlantid() == null)
				.forEach(d -> d.setPlantid(indentifiedInstruments.get(d.getIndex())));

		// ----
		// identify jobitem and workrequirement, and set information : isContractReview,
		// AutoContractReview, ServiceTypeId...
		for (List<ImportedOperationItem> group : groups) {
			ImportedOperationItem firstItem = group.get(0);

			JobItemImportInfoDto wrInfo = null;
			if (firstItem.getServiceTypeId() == null)
				wrInfo = jobItemsInfo.stream().filter(i -> i.getPlantId().equals(firstItem.getPlantid())
						&& !alreadySelectedWr.contains(i.getJiWrId())).findFirst().orElse(null);
			else
				wrInfo = jobItemsInfo.stream()
						.filter(i -> i.getPlantId().equals(firstItem.getPlantid())
								&& firstItem.getServiceTypeId().equals(i.getServiceTypeId())
								&& !alreadySelectedWr.contains(i.getJiWrId()))
						.findFirst().orElse(null);

			if (wrInfo == null && autoCreateJobItems) {
				Map<Integer, Integer> plantIdsAndDefaultSt = new HashMap<>();
				plantIdsAndDefaultSt.put(firstItem.getPlantid(), firstItem.getServiceTypeId());
				Job job = this.jobService.get(jobId);
				// create jobitem
				List<JobItem> res = jobItemService.createJobItems(jobId, plantIdsAndDefaultSt, autoUser.getPersonid(),
						job.getBookedInAddr().getAddrid());
				JobItem newJi = res.get(0);
				for (ImportedOperationItem item : group) {
					item.setJobitemId(newJi.getJobItemId());
					item.setJiWrId(newJi.getNextWorkReq().getId());
					item.setWrServiceTypeId(
							newJi.getNextWorkReq().getWorkRequirement().getServiceType().getServiceTypeId());
					item.setContractReviewed(CollectionUtils.isNotEmpty(newJi.getContactReviewItems()));
					item.setAutomaticContractReview(true);
				}
				alreadySelectedWr.add(newJi.getNextWorkReq().getId());
			} else {
				for (ImportedOperationItem item : group) {
					item.setJobitemId(wrInfo.getJobitemId());
					item.setJiWrId(wrInfo.getJiWrId());
					item.setWrServiceTypeId(wrInfo.getServiceTypeId());
					item.setContractReviewed(wrInfo.getContractReviewed());
					item.setAutomaticContractReview(autoContractReview);
				}
				alreadySelectedWr.add(wrInfo.getJiWrId());
			}
		}

		List<Calibration> createdCalibrations = calibrationService.importOnsiteOperations(
				(List<List<ImportedOperationItem>>) groups, businessSubdivId, autoUser.getPersonid(), locale,
				calibrationProcessService.findByName(TLM_IMPORTED));

		// register how many calibrations were inserted
		Long insertedCalibrations = jobExecutionContext.getLong(OperationsImportConfig.JOB_EC_INSTERTED_CALIBRATIONS,
				0);
		jobExecutionContext.putLong(OperationsImportConfig.JOB_EC_INSTERTED_CALIBRATIONS,
				insertedCalibrations + createdCalibrations.size());
	}

}
