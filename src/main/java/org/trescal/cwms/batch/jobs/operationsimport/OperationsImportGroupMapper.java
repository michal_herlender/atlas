package org.trescal.cwms.batch.jobs.operationsimport;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.exchangeformat.EFRowMapper;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.db.ExchangeFormatFieldNameDetailsService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatOperationTypeEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportStateEnum;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
@StepScope
public class OperationsImportGroupMapper
	implements EFRowMapper<List<ImportedOperationItem>, List<LinkedCaseInsensitiveMap<String>>> {

	@Autowired
	private AliasGroupService aliasGroupService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ExchangeFormatFieldNameDetailsService efFieldNameDetailsService;
	@Autowired
	private ExchangeFormatService exchangeFormatService;

	private ExchangeFormat ef;
	private SimpleDateFormat sdf;
	private final Integer efId;

	public OperationsImportGroupMapper(@Value("#{jobParameters['" + BatchConfig.EF_ID + "']}") Integer efId) {
		this.efId = efId;
	}

	@PostConstruct
	public void postConstruct() {
		this.ef = exchangeFormatService.get(efId);
		this.sdf = new SimpleDateFormat(this.ef.getDateFormat().getValue());
		this.sdf.setLenient(false);
	}

	public ImportedOperationItem mapItem(LinkedCaseInsensitiveMap<String> content, int itemIndex, Locale locale) {
		if (content == null)
			return null;

		List<ExchangeFormatFieldNameEnum> fieldsWithExceptions = new ArrayList<>();

		ImportedOperationItem item = new ImportedOperationItem();
		item.setIndex(itemIndex);

		List<ExchangeFormatFieldNameDetails> efFields = efFieldNameDetailsService.getByExchangeFormat(efId);

		for (String key : content.keySet()) {
			ExchangeFormatFieldNameEnum efFieldName = efFields.stream()
				.filter(e -> (e.getTemplateName() != null
					&& e.getTemplateName().trim().equalsIgnoreCase(key.trim()))
					|| (e.getFieldName().getValue().trim().equalsIgnoreCase(key.trim())))
				.map(ExchangeFormatFieldNameDetails::getFieldName).findFirst().orElse(null);

			String columnValue = content.get(key);
			if (efFieldName != null && StringUtils.isNotBlank(columnValue)) {

				try {

					switch (efFieldName) {
					case ID_TRESCAL:
						item.setPlantid(Integer.valueOf(columnValue));
						break;
					case PLANT_NO:
						item.setPlantno(columnValue);
						break;
					case SERIAL_NUMBER:
						item.setSerialNo(columnValue);
						break;
					case EXPECTED_SERVICE:
						item.setService(columnValue);
						ServiceType st = this.aliasGroupService
								.determineExpectedServiceTypeFromAliasOrValue(ef.getAliasGroup(), columnValue);
						if (st != null)
							item.setServiceTypeId(st.getServiceTypeId());
						break;
					case SERVICETYPE_ID:
						item.setServiceTypeId(Integer.valueOf(columnValue));
						break;
					case OPERATION_DATE:
						if (StringUtils.isNotBlank(columnValue))
							item.setOperationDate(sdf.parse(columnValue));
						break;
					case OPERATION_START_DATE:
						item.setOperationStartDate(sdf.parse(columnValue));
						break;
					case OPERATION_DURATION:
						item.setOperationDuration(Integer.valueOf(columnValue));
						break;
					case OPERATION_BY_HRID:
						item.setOperationByHrId(columnValue);
						break;
					case DOCUMENT_NO:
						item.setDocumentNumber(columnValue);
						break;
					case OPERATION_VALIDATED_BY_HRID:
						item.setOperationValidatedByHrId(columnValue);
						break;
					case OPERATION_VALIDATED_ON:
						if (StringUtils.isNotBlank(columnValue))
							item.setOperationValidatedOn(sdf.parse(columnValue));
						break;
					case BRAND:
						item.setBrand(columnValue);
						break;
					case CUSTOMER_DESCRIPTION:
						item.setCustomerDescription(columnValue);
						break;
					case CUSTOMER_COMMENTS:
						item.setCustomerComments(columnValue);
						break;
					case MODEL:
						item.setModel(columnValue);
						break;
					case JOBITEM_EQUIVALENT_ID:
						item.setJobitemEquivalentId(columnValue);
						break;
					case CALIBRATION_OUTCOME:
						item.setCalibrationOutcome(ActionOutcomeValue_Calibration.valueOf(columnValue));
						break;
					case OPERATION_TYPE:
						item.setOperationType(ExchangeFormatOperationTypeEnum.valueOf(columnValue));
						break;
						case CAL_FREQUENCY:
							item.setCalibrationFrequency(Integer.valueOf(columnValue));
							break;
						case INTERVAL_UNIT:
							item.setIntervalUnit(
								this.aliasGroupService.determineIntervalUnitValue(ef.getAliasGroup(), columnValue));
							break;
						case NEXT_CAL_DUEDATE:
							item.setNextCalDueDate(DateTools.dateToLocalDate(sdf.parse(columnValue)));
							break;
						case CALIBRATION_CLASS:
							item.setCalibrationClass(CalibrationClass.valueOf(columnValue));
							break;
						case CALIBRATION_VERIFICATION_STATUS:
							item.setCalibrationVerificationStatus(CalibrationVerificationStatus.valueOf(columnValue));
							break;
						case ADJUSTMENT:
							item.setAdjustment(Boolean.valueOf(columnValue));
							break;
					case OPTIMIZATION:
						item.setOptimization(Boolean.valueOf(columnValue));
						break;
					case RESTRICTION:
						item.setRestiction(Boolean.valueOf(columnValue));
						break;
					case REPAIR:
						item.setRepair(Boolean.valueOf(columnValue));
						break;
					case FR_STATES:
						if (StringUtils.isNotBlank(columnValue)) {
							List<FailureReportStateEnum> states = Arrays.stream(columnValue.split(","))
								.map(s -> FailureReportStateEnum.valueOf(s.trim())).collect(Collectors.toList());
							item.setFrStates(states);
						}
						break;
					case FR_STATE_OTHER:
						item.setFrOtherState(columnValue);
						break;
					case FR_TECHNICIAN_COMMENTS:
						item.setFrTechnicianComment(columnValue);
						break;
					case FR_RECOMMENDATIONS:
						if (StringUtils.isNotBlank(columnValue)) {
							List<FailureReportRecommendationsEnum> recommendations = Arrays.stream(columnValue.split(","))
								.map(s -> FailureReportRecommendationsEnum.valueOf(s.trim()))
								.collect(Collectors.toList());
							item.setFrRecommendations(recommendations);
						}
						break;
					case FR_DISPENSATION_COMMENTS:
						item.setFrDispensationComment(columnValue);
						break;
					case FR_VALIDATION_COMMENTS:
						item.setFrValidationComment(columnValue);
						break;
					case FR_SEND_TO_CLIENT:
						item.setFrSendToClient(Boolean.valueOf(columnValue));
						break;
					case FR_CLIENT_RESPONSE_NEEDED:
						item.setFrClientResponseNeeded(Boolean.valueOf(columnValue));
						break;
					case FR_FINAL_OUTCOME:
						item.setFrFinalOutcome(FailureReportFinalOutcome.valueOf(columnValue));
						break;
					case FR_CLIENT_APPROVAL_ON:
						item.setFrClientApprovalOn(sdf.parse(columnValue));
						break;
					case FR_CLIENT_DECISION:
						item.setFrClientDecision(Boolean.valueOf(columnValue));
						break;
					case FR_CLIENT_COMMENTS:
						item.setFrClientComments(columnValue);
						break;
					case FR_OPERATION_BY_TRESCAL:
						item.setFrOperationByTrescal(Boolean.valueOf(columnValue));
						break;
					case FR_SUBDIV_ID:
						item.setFrSubdivisionId(Integer.valueOf(columnValue));
						break;
					case FR_ESTIMATED_DELIVERY_TIME:
						item.setFrEstimatedDeliveryTime(Integer.valueOf(columnValue));
						break;
					case FR_CLIENT_ALIAS:
						item.setFrClientAlias(columnValue);
						break;
					default:
						break;

					}
				} catch (Exception ex) {
					fieldsWithExceptions.add(efFieldName);
					String errorMsg = messageSource.getMessage(
						"exchangeformat.import.validation.columnswithinvaliddata",
						new String[]{fieldsWithExceptions.stream().map(ExchangeFormatFieldNameEnum::getName).collect(Collectors.joining(","))},
						locale);
					ItemAnalysisResult ias = new ItemAnalysisResult(itemIndex, efFieldName, errorMsg, null);
					item.getItemAnalysisResult().add(ias);
				}
			}
		}

		return item;
	}

	@Override
	public List<ImportedOperationItem> map(List<LinkedCaseInsensitiveMap<String>> content, int itemIndex,
			Locale locale) {
		return content.stream().map(c -> mapItem(c, itemIndex, locale)).collect(Collectors.toList());
	}

}
