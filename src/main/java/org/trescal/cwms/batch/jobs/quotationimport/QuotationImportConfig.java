package org.trescal.cwms.batch.jobs.quotationimport;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.repeat.policy.SimpleCompletionPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.exchangeformat.AutoSubmitPolicyDecider;
import org.trescal.cwms.batch.exchangeformat.GlobalJobListener;

@Configuration
public class QuotationImportConfig {

	public static final String JOB_NAME = "QUOTATION_IMPORT_JOB";
	private static final int ANALYSIS_CHUNK_SIZE = 100;
	private static final int SUBMIT_CHUNK_SIZE = 100;
	private static final int TRANSACTION_TIMEOUT = 5000;

	/* Job Params */
	public static final String JOB_ID = "JOB_ID";
	public static final String QUOTE_ID = "QUOTE_ID";
	public static final String BUSINESS_SUBDIV_ID = "BUSINESS_SUBDIV_ID";
	public static final String CLIENT_SUBDIV_ID = "CLIENT_SUBDIV_ID";
	public static final String PRICE_SERVICE_TYPE_FROM_FILE = "PRICE_SERVICE_TYPE_FROM_FILE";

	/* Job execution context */
	public static final String JOB_EC_IDENTIFIED_INST = "JOB_EC_IDENTIFIED_INST";
	public static final String JOB_EC_IDENTIFIED_INST_MODEL = "JOB_EC_IDENTIFIED_INST";
	public static final String JOB_EC_INSTERTED_QUOTATION_ITEMS = "JOB_EC_INSTERTED_QUOTATION_ITEMS";

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private StepBuilderFactory stepsBuilderFactory;
	@Autowired
	private GlobalJobListener globaleJobListener;
	@Autowired
	private QuotationImportReader quotationImportReader;
	@Autowired
	private QuotationImportValidationPocessor quotationImportValidationPocessor;
	@Autowired
	private QuotationImportAnalysisResultWriter quotationImportAnalysisResultWriter;
	@Autowired
	private AutoSubmitPolicyDecider autoSubmitPolicyDecider;
	@Autowired
	private QuotationImportFilterProcessor quotationImportFilterProcessor;
	@Autowired
	private QuotationImportSubmitter quotationImportSubmitter;

	@Bean(JOB_NAME)
	public Job quotationsImportJob() {
		return this.jobBuilderFactory.get(JOB_NAME).listener(globaleJobListener).start(quotationsImportAnalysisStep())
				.next(autoSubmitPolicyDecider).on(AutoSubmitPolicyDecider.EXIT).end().from(autoSubmitPolicyDecider)
				.on("*").to(quotationsImportSubmitStep()).end().build();
	}

	@Bean
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Step quotationsImportAnalysisStep() {
		DefaultTransactionAttribute dta = new DefaultTransactionAttribute();
		dta.setTimeout(TRANSACTION_TIMEOUT);
		return stepsBuilderFactory.get(BatchConfig.ANALYSIS_STEP).chunk(new SimpleCompletionPolicy(ANALYSIS_CHUNK_SIZE))
				.reader(quotationImportReader).processor((ItemProcessor) quotationImportValidationPocessor)
				.writer(quotationImportAnalysisResultWriter).transactionAttribute(dta).build();
	}

	@Bean
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Step quotationsImportSubmitStep() {
		return stepsBuilderFactory.get(BatchConfig.SUBMIT_STEP).chunk(new SimpleCompletionPolicy(SUBMIT_CHUNK_SIZE))
				.reader(quotationImportReader).processor((ItemProcessor) quotationImportFilterProcessor)
				.writer(quotationImportSubmitter).build();
	}

}
