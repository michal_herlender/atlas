package org.trescal.cwms.batch.jobs.importInstrument;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.exchangeformat.EFRowMapper;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.db.InstrumentUsageTypeService;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
@StepScope
public class InstrumentImportMapper
    implements EFRowMapper<ImportedInstrumentsSynthesisRowDTO, LinkedCaseInsensitiveMap<String>> {

    @Autowired
    private AliasGroupService aliasGroupService;
    @Autowired
    private ExchangeFormatService exchangeFormatService;
    @Autowired
    private AddressService addrServ;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private InstrumentModelFamilyService familyService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private InstrumentModelService modelServ;
    @Autowired
    private NewDescriptionService descService;
    @Autowired
    private InstrumentUsageTypeService instrumentUsageTypeService;
    @Autowired
    private MessageSource messageSource;

	private ExchangeFormat ef;
	private SimpleDateFormat sdf;
	private final Integer efId;
	private final Integer defaultAddressId;
	private final Integer defaultContactId;
	private final Integer defaultServiceTypeId;
	private final Integer subdivId;
	private Subdiv subdiv;

	public InstrumentImportMapper(@Value("#{jobParameters['" + BatchConfig.EF_ID + "']}") Integer efId,
								  @Value("#{jobParameters['" + InstrumentImportConfig.DEFAULT_ADDRESS_ID + "']}") Integer defaultAddressId,
								  @Value("#{jobParameters['" + InstrumentImportConfig.DEFAULT_CONTACT_ID + "']}") Integer defaultContactId,
								  @Value("#{jobParameters['" + InstrumentImportConfig.DEFAULT_SERVICETYPE_ID
									  + "']}") Integer defaultServiceTypeId,
								  @Value("#{jobParameters['" + InstrumentImportConfig.CLIENT_SUBDIV_ID + "']}") Integer subdivId) {
		this.efId = efId;
		this.defaultAddressId = defaultAddressId;
		this.defaultContactId = defaultContactId;
		this.defaultServiceTypeId = defaultServiceTypeId;
		this.subdivId = subdivId;
	}

	@PostConstruct
	public void postConstruct() {
		this.ef = exchangeFormatService.get(efId);
		this.sdf = new SimpleDateFormat(this.ef.getDateFormat().getValue());
		this.sdf.setLenient(false);
		this.subdiv = subdivService.get(subdivId);
	}

	@Override
	public ImportedInstrumentsSynthesisRowDTO map(LinkedCaseInsensitiveMap<String> fileContent, int itemIndex,
			Locale locale) {

		if (fileContent == null)
			return null;

		List<ExchangeFormatFieldNameEnum> fieldsWithExceptions = new ArrayList<>();

		AliasGroup aliasGroup = ef.getAliasGroup();
		ImportedInstrumentsSynthesisRowDTO dto = new ImportedInstrumentsSynthesisRowDTO();
		dto.setIndex(itemIndex);

		for (String key : fileContent.keySet()) {
			ExchangeFormatFieldNameEnum efFieldName = ef.getExchangeFormatFieldNameDetails().stream()
				.filter(e -> (e.getTemplateName() != null
					&& e.getTemplateName().trim().equalsIgnoreCase(key.trim()))
					|| (e.getFieldName().getValue().trim().equalsIgnoreCase(key.trim())))
				.map(ExchangeFormatFieldNameDetails::getFieldName).findFirst().orElse(null);

			String columnValue = fileContent.get(key);

			if (efFieldName != null && StringUtils.isNoneBlank(columnValue)) {
				try {
					switch (efFieldName) {
					case ADDRESS:
						if (StringUtils.isNotBlank(columnValue)) {
							dto.setAddress(columnValue);
							Address address = addrServ.findCompanyAddress(subdiv.getComp().getCoid(), null, true,
									columnValue);
							if (address != null)
								dto.setAddressId(address.getAddrid());
						} else {
							dto.setAddressId(defaultAddressId);
						}
						break;
					case ADDRESS_ID:
						if (StringUtils.isNotBlank(columnValue))
							dto.setAddressId(Integer.valueOf(columnValue));
						else
							dto.setAddressId(defaultAddressId);
						break;
					// set the service type id
					case SERVICETYPE_ID:
						if (StringUtils.isNotBlank(columnValue)) {
							dto.setDefaultServiceTypeId(Integer.valueOf(columnValue));
						} else {
							dto.setDefaultServiceTypeId(defaultServiceTypeId);
						}

						break;
					case CAL_FREQUENCY:
						dto.setCalFrequency(Integer.valueOf(columnValue));
						break;
					case CONTACT:
						if (StringUtils.isNotBlank(columnValue)) {
							dto.setContact(columnValue);
							Contact contact = contactService.findCompanyContactByFullName(subdiv.getComp().getCoid(),
									columnValue, true);
							if (contact != null)
								dto.setContactId(contact.getId());
						} else {
							dto.setContactId(defaultContactId);
						}
						break;
					case CONTACT_ID:
						if (StringUtils.isNotBlank(columnValue))
							dto.setContactId(Integer.valueOf(columnValue));
						else
							dto.setContactId(defaultContactId);
						break;
					case DEFAULT_PROCEDURE_REFERENCE:
						dto.setDefaultProcedureReference(columnValue);
						if (StringUtils.isNoneBlank(dto.getDefaultProcedureReference())) {
                            capabilityService
                                .getCapabilityByReference(dto.getDefaultProcedureReference(), null, null).stream()
                                .findFirst().ifPresent(procedure -> dto.setDefaultProcedureId(procedure.getId()));
                        }
						break;
					case FAMILY_ID:
						dto.setFamilyId(Integer.valueOf(columnValue));
						break;
					case INSTRUMENT_MODEL_ID:
						dto.setInstrumentModelId(Integer.valueOf(columnValue));
						break;
					case INTERVAL_UNIT:
						dto.setIntervalUnit(this.aliasGroupService.determineIntervalUnitValue(aliasGroup, columnValue));
						break;
					case NEXT_CAL_DUEDATE:
						try {
							dto.setNextCalDueDate(DateTools.dateToLocalDate(sdf.parse(columnValue)));
						} catch (ParseException e1) {
							e1.printStackTrace();
						}
						break;
					case SUBFAMILY_ID:
						dto.setSubFamilyId(Integer.valueOf(columnValue));
						break;
					case TML_BRAND_ID:
						dto.setTmlBrandId(Integer.valueOf(columnValue));
						break;
					case TML_FAMILY_ID:
						if (StringUtils.isNoneBlank(columnValue)) {
							dto.setTmlFamilyId(Integer.valueOf(columnValue));
							// find the instrument model family by TML_FAMILY_ID
							try {
								InstrumentModelFamily family = this.familyService
									.getTMLFamily(Integer.parseInt(columnValue));
								if (family != null) {
									dto.setFamilyId(family.getFamilyid());
									dto.setFamilyName(this.translationService
										.getCorrectTranslation(family.getTranslation(), locale));
								}
							} catch (Exception ignored) {
							}
						}

						break;
					case TML_INSTRUMENT_MODEL_ID:
						if (StringUtils.isNoneBlank(columnValue)) {
							dto.setTmlInstrumentModelId(Integer.valueOf(columnValue));
							try {
								// try to find the instrument model by
								// TML_instrument_id
								InstrumentModel model = this.modelServ
									.getTMLInstrumentModel(Integer.parseInt(columnValue));
								if (model != null) {
									dto.setInstrumentModelId(model.getModelid());
								}
							} catch (Exception ignored) {
							}
						}
						break;
					case TML_SUBFAMILY_ID:
						if (StringUtils.isNoneBlank(columnValue)) {
							dto.setTmlSubFamilyId(Integer.valueOf(columnValue));
							// find the instrument model family by
							// TML_SUBFAMILY_ID
							try {
								Description description = this.descService
									.getTMLDescription(Integer.parseInt(columnValue));
								dto.setSubFamilyId(description.getTmlid());
								dto.setSubFamilyName(this.translationService.getCorrectTranslation(
									description.getTranslations(), LocaleContextHolder.getLocale()));
							} catch (Exception ignored) {

							}
						}

						break;
					case BRAND:
						dto.setBrand(columnValue);
						break;
					case CUSTOMER_DESCRIPTION:
						dto.setDescription(columnValue);
						break;
					case MODEL:
						dto.setModel(columnValue);
						break;
					case PLANT_NO:
						dto.setPlantno(columnValue);
						break;
					case SERIAL_NUMBER:
						dto.setSerialno(columnValue);
						break;
					case EXPECTED_SERVICE:
						dto.setServiceType(columnValue);
						ServiceType st = this.aliasGroupService.determineExpectedServiceTypeFromAliasOrValue(aliasGroup,
								columnValue);
						if (st != null)
							dto.setDefaultServiceTypeId(st.getServiceTypeId());
						break;
					case CLIENT_REF:
						dto.setClientRef(columnValue);
						break;
					case INSTRUMENT_USAGE_TYPE:
						dto.setInstrumentUsageType(columnValue);
						if (StringUtils.isNoneBlank(columnValue)) {
							InstrumentUsageType usageType = instrumentUsageTypeService.getByName(columnValue, locale);
							if (usageType != null)
								dto.setInstrumentUsageTypeId(usageType.getId());
						}
						break;
					default:
						break;
					}
				} catch (Exception e) {
					fieldsWithExceptions.add(efFieldName);

					String errorMsg = messageSource.getMessage(
						"exchangeformat.import.validation.columnswithinvaliddata",
						new String[]{fieldsWithExceptions.stream().map(ExchangeFormatFieldNameEnum::getName)
							.collect(Collectors.joining(","))},
						locale);
					ItemAnalysisResult ias = new ItemAnalysisResult(itemIndex, efFieldName, errorMsg, null);
					dto.getItemAnalysisResult().add(ias);
				}
			}
		}

		return dto;
	}

}
