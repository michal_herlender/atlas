package org.trescal.cwms.batch.jobs.importInstrument;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResultService;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;

@Component
@StepScope
public class InstrumentImportFilterProcessor
		implements ItemProcessor<ImportedInstrumentsSynthesisRowDTO, ImportedInstrumentsSynthesisRowDTO> {

	@Autowired
	private ItemAnalysisResultService iarService;

	private AutoSubmitPolicyEnum autoSubmitPolicy;
	private Integer lastAnalysisStepId;

	public InstrumentImportFilterProcessor(
			@Value("#{jobParameters['" + BatchConfig.AUTO_SUBMIT_POLICY + "']}") String autoSubmitPolicy,
			@Value("#{jobExecutionContext['" + BatchConfig.JOB_EC_LAST_ANALAYSIS_STEP_ID
					+ "']}") Integer lastAnalysisStepId) {

		this.autoSubmitPolicy = AutoSubmitPolicyEnum.valueOf(autoSubmitPolicy);
		this.lastAnalysisStepId = lastAnalysisStepId;
	}

	@Override
	public ImportedInstrumentsSynthesisRowDTO process(ImportedInstrumentsSynthesisRowDTO dto) throws Exception {

		if (dto == null)
			return null;

		List<ItemAnalysisResult> iars = iarService.getAllAnalysisResults(lastAnalysisStepId, dto.getIndex(),
				dto.getIndex());

		if (iars.isEmpty()) {
			return dto;
		}
		boolean hasErrors = iars.stream().anyMatch(i -> StringUtils.isNoneBlank(i.getError()));
		if (hasErrors)
			return null;

		boolean hasWarnings = iars.stream().anyMatch(i -> StringUtils.isNoneBlank(i.getWarning()));

		if (autoSubmitPolicy.equals(AutoSubmitPolicyEnum.SUBMIT_ONLY_ITEMS_WITHOUT_ERRORS_AND_WITHOUT_WARNINGS)
				&& hasWarnings)
			return null;

		return null;
	}

}
