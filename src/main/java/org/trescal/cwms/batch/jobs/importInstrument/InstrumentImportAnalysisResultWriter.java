package org.trescal.cwms.batch.jobs.importInstrument;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Component
@StepScope
public class InstrumentImportAnalysisResultWriter implements ItemWriter<List<ItemAnalysisResult>> {

	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager entityManager;

	private StepExecution stepExecution;

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	@AfterStep
	public void afterStep(StepExecution stepExecution) {
		stepExecution.getJobExecution().getExecutionContext().putLong(BatchConfig.JOB_EC_LAST_ANALAYSIS_STEP_ID,
				stepExecution.getId());
	}

	@Override
	public void write(List<? extends List<ItemAnalysisResult>> lists) throws Exception {

		Long errorCount = 0l;
		Long warningCount = 0l;

		BatchStepExecution bsa = entityManager.find(BatchStepExecution.class, this.stepExecution.getId().intValue());
		if (bsa.getItemAnalysisResults() == null)
			bsa.setItemAnalysisResults(new ArrayList<>());

		for (List<ItemAnalysisResult> list : lists) {
			errorCount += list.stream().filter(i -> StringUtils.isNotBlank(i.getError())).count();
			warningCount += list.stream().filter(i -> StringUtils.isNotBlank(i.getWarning())).count();
			list.forEach(i -> i.setStepExecution(bsa));
			bsa.getItemAnalysisResults().addAll(list);
		}

		// error and warning count
		Long oldErrorCount = stepExecution.getExecutionContext().getLong(BatchConfig.STEP_EC_ERROR_COUNT, 0);
		Long oldWarningCount = stepExecution.getExecutionContext().getLong(BatchConfig.STEP_EC_WARNING_COUNT, 0);

		errorCount += oldErrorCount;

		stepExecution.getExecutionContext().putLong(BatchConfig.STEP_EC_ERROR_COUNT, errorCount);

		warningCount += oldWarningCount;

		stepExecution.getExecutionContext().putLong(BatchConfig.STEP_EC_WARNING_COUNT, warningCount);

		entityManager.persist(bsa);

	}

}
