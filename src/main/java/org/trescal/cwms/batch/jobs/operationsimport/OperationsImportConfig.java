package org.trescal.cwms.batch.jobs.operationsimport;

import java.util.List;

import javax.persistence.OptimisticLockException;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.repeat.policy.SimpleCompletionPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.exchangeformat.AutoSubmitPolicyDecider;
import org.trescal.cwms.batch.exchangeformat.GlobalJobListener;

@Configuration
public class OperationsImportConfig {

	public static final String JOB_NAME = "OPERATION_IMPORT_JOB";
	public static final String ANALYSIS_STEP = "ANALYSIS_STEP";
	public static final String SUBMIT_STEP = "SUBMIT_STEP";
	public static final String CERTIFICATES_FILES_UPLOAD_STEP = "CERTIFICATES_FILES_UPLOAD_STEP";
	private static final int ANALYSIS_CHUNK_SIZE = 5;
	private static final int SUBMIT_CHUNK_SIZE = 5;
	private static final int TRANSACTION_TIMEOUT = 5000;
	/* Job Params */
	public static final String JOB_ID = "JOB_ID";
	public static final String BUSINESS_SUBDIV_ID = "BUSINESS_SUBDIV_ID";
	public static final String AUTO_CONTRACT_REVIEW = "AUTO_CONTRACT_REVIEW";
	public static final String AUTO_CREATE_JOBITEMS = "AUTO_CREATE_JOBITEMS";
	/* Job execution context */
	public static final String JOB_EC_IDENTIFIED_INST = "JOB_EC_IDENTIFIED_INST";
	public static final String JOB_EC_CERTIFICATES_FILENAMES = "JOB_EC_CERTIFICATES_FILENAMES";
	public static final String JOB_EC_INSTERTED_CALIBRATIONS = "JOB_EC_INSTERTED_CALIBRATIONS";
	public static final String JOB_EC_UPLOADED_CERTIFICATE_FILES = "JOB_EC_UPLOADED_CERTIFICATE_FILES";

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private StepBuilderFactory stepsBuilderFactory;

	@Autowired
	private AutoSubmitPolicyDecider autoSubmitPolicyDecider;
	@Autowired
	private GlobalJobListener globalJobListener;
	@Autowired
	private CertificatesFilesImportDecider certificatesFilesImportDecider;
	@Autowired
	private OperationsImportGroupReader operationsImportGroupReader;
	@Autowired
	private OperationsImportBusinessValidationProcessor operationsImportBusinessValidationProcessor;
	@Autowired
	private OperationsImportAnalysisResultWriter operationsImportAnalysisResultWriter;
	@Autowired
	private OperationsImportGroupFilter operationsImportGroupFilter;
	@Autowired
	private OperationsImportGroupSubmitter operationsImportGroupSubmitter;
	@Autowired
	private CertificateFileImporter certificateFileImporter;

	@Bean("OPERATION_IMPORT_JOB")
	public Job operationsImportJob() {
		return this.jobBuilderFactory.get(JOB_NAME).listener(globalJobListener).start(operationsImportAnalysisStep())
				.next(autoSubmitPolicyDecider).on(AutoSubmitPolicyDecider.EXIT).end().from(autoSubmitPolicyDecider)
				.on("*").to(operationsImportSubmitStep()).next(certificatesFilesImportDecider)
				.on(CertificatesFilesImportDecider.EXIT).end().from(certificatesFilesImportDecider).on("*")
				.to(certificatesFilesUploadStep()).end().build();
	}

	@Bean
	public Step operationsImportAnalysisStep() {
		DefaultTransactionAttribute dta = new DefaultTransactionAttribute();
		dta.setTimeout(TRANSACTION_TIMEOUT);
		return stepsBuilderFactory.get(ANALYSIS_STEP)
				.<List<ImportedOperationItem>, List<ItemAnalysisResult>>chunk(
						new SimpleCompletionPolicy(ANALYSIS_CHUNK_SIZE))
				.reader(operationsImportGroupReader).processor(operationsImportBusinessValidationProcessor)
				.writer(operationsImportAnalysisResultWriter).transactionAttribute(dta).build();
	}

	@Bean
	public Step operationsImportSubmitStep() {
		return stepsBuilderFactory.get(SUBMIT_STEP)
				.<List<ImportedOperationItem>, List<ImportedOperationItem>>chunk(
						new SimpleCompletionPolicy(SUBMIT_CHUNK_SIZE))
				.reader(operationsImportGroupReader).processor(operationsImportGroupFilter)
				.writer(operationsImportGroupSubmitter)
				.faultTolerant()
				.retryLimit(2)
				.retry(OptimisticLockException.class)
				.build();
	}

	@Bean
	public Step certificatesFilesUploadStep() {
		return stepsBuilderFactory.get(CERTIFICATES_FILES_UPLOAD_STEP)
				.<List<ImportedOperationItem>, List<ImportedOperationItem>>chunk(
						new SimpleCompletionPolicy(SUBMIT_CHUNK_SIZE))
				.reader(operationsImportGroupReader).processor(operationsImportGroupFilter)
				.writer(certificateFileImporter)
				.build();
	}
	
}