package org.trescal.cwms.batch.jobs.operationsimport;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.batch.config.ItemAnalysis;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatOperationTypeEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportStateEnum;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
public class ImportedOperationItem implements ItemAnalysis {

	// input data
	private Integer plantid;
	private String plantno;
	private String service;
	private Integer serviceTypeId;
	private ExchangeFormatOperationTypeEnum operationType;
	@DateTimeFormat
	private Date operationDate;
	@DateTimeFormat
	private Date operationStartDate;
	private Integer operationDuration;
	private String operationByHrId;
	private String documentNumber;
	private String operationValidatedByHrId;
	@DateTimeFormat
	private Date operationValidatedOn;
	private String customerDescription;
	private String brand;
	private String model;
	private String serialNo;
	private String technicalRequirements;
	private String customerComments;
	private boolean checked;
	@NotNull
	private String jobitemEquivalentId;
	private ActionOutcomeValue_Calibration calibrationOutcome;
	private IntervalUnit intervalUnit;
	private Integer calibrationFrequency;
	@DateTimeFormat
	private LocalDate nextCalDueDate;
	private CalibrationClass calibrationClass;
	private CalibrationVerificationStatus calibrationVerificationStatus;
	private Boolean adjustment;
	private Boolean optimization;
	private Boolean restiction;
	private Boolean repair;
	private List<FailureReportStateEnum> frStates;
	private String frOtherState;
	private String frTechnicianComment;
	private List<FailureReportRecommendationsEnum> frRecommendations;
	private String frDispensationComment;
	private String frValidationComment;
	private Boolean frSendToClient;
	private Boolean frClientResponseNeeded;
	private FailureReportFinalOutcome frFinalOutcome;
	@DateTimeFormat
	private Date frClientApprovalOn;
	private Boolean frClientDecision;
	private String frClientComments;
	private Boolean frOperationByTrescal;
	private Integer frSubdivisionId;
	private Integer frEstimatedDeliveryTime;
	private String frClientAlias;

	private Integer index;
	private List<ItemAnalysisResult> itemAnalysisResult;

	private Integer jobitemId;
	private Integer jiWrId;
	private Integer wrServiceTypeId;
	private Boolean contractReviewed;

	// sync import tool
	private Boolean automaticContractReview;
	private Map<String, String> undefineds;
	private Instrument instrument;
	private JobItem jobitem;
	private Contact operationBy;
	private Contact operationValidatedBy;
	private Certificate certificate;
	private Integer rowspan;
	// should be field if the current operations is FR and the next item is on the
	// same jobitem, used to deduce the outcome of the FR.
	private Integer nextServiceTypeId;

	public ImportedOperationItem() {
        this.itemAnalysisResult = new ArrayList<>();
    }

	@Override
	public String toString() {
		return "ImportedCalibrationItem [plantid=" + plantid + ", plantno=" + plantno + ", jobitemEquivalentId="
				+ jobitemEquivalentId + "]";
	}

}
