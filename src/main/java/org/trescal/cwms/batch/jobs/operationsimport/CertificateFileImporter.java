package org.trescal.cwms.batch.jobs.operationsimport;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;

@Component
@StepScope
public class CertificateFileImporter implements ItemWriter<List<ImportedOperationItem>> {

	@Autowired
	private JobService jobService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private ServletContext servletContext;

	private Integer jobId;
	private Job job;
	private Integer efFileId;
	private ExchangeFormatFile efFile;
	private ExecutionContext jobExecutionContext;

	public CertificateFileImporter(@Value("#{jobParameters['" + OperationsImportConfig.JOB_ID + "']}") Integer jobId,
			@Value("#{jobParameters['" + BatchConfig.EF_FILE_ID + "']}") Integer efFileId) {
		this.jobId = jobId;
		this.efFileId = efFileId;
	}

	@PostConstruct
	public void init() {
		this.job = jobService.get(this.jobId);
		this.efFile = efFileService.get(this.efFileId);
	}

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.jobExecutionContext = stepExecution.getJobExecution().getExecutionContext();
	}

	@Override
	public void write(List<? extends List<ImportedOperationItem>> groups) throws Exception {

		if (groups.isEmpty())
			return;

		File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);

		int uploadedCertificates = 0;
		for (List<ImportedOperationItem> group : groups) {
			
			Set<String> documentNumbers = group.stream().filter(it -> it.getDocumentNumber() != null)
					.map(ImportedOperationItem::getDocumentNumber).collect(Collectors.toSet());

			
			for(String docNumber : documentNumbers){
				
				List<Certificate> certificates = certificateService.getByThirdCertNoAndJobId(docNumber,
						this.jobId);

				// extracted zip folder
				File excelFile = this.efFileService.getExcelFile(this.efFile, tempDir);
				
				File extractedFolder = excelFile.getParentFile();

				// find certificate file
				File file = Arrays.asList(extractedFolder.listFiles(
						(dir, filename) -> FilenameUtils.getBaseName(filename).equalsIgnoreCase(docNumber)))
						.stream().findFirst().orElse(null);
				if( file != null) {
					String fileExtension = FilenameUtils.getExtension(file.getPath());
					certificateService.importCertificateFile(job.getJobno(), fileExtension, FileUtils.readFileToByteArray(file),
							certificates);
					uploadedCertificates++;
				}
			}
			
			
		}

		// register how many certificate file were uploaded
		Long alreadyUploadedCertificates = jobExecutionContext
				.getLong(OperationsImportConfig.JOB_EC_UPLOADED_CERTIFICATE_FILES, 0);

		alreadyUploadedCertificates = alreadyUploadedCertificates + uploadedCertificates;
		
		jobExecutionContext.putLong(OperationsImportConfig.JOB_EC_UPLOADED_CERTIFICATE_FILES,
				alreadyUploadedCertificates);

	}

}
