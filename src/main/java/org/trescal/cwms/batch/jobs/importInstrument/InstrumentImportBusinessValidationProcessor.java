package org.trescal.cwms.batch.jobs.importInstrument;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companygroup.db.CompanyGroupService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutPlantNo;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutSerialNo;
import org.trescal.cwms.core.system.enums.Scope;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Locale;

@Component
@StepScope
public class InstrumentImportBusinessValidationProcessor
	implements ItemProcessor<ImportedInstrumentsSynthesisRowDTO, List<ItemAnalysisResult>> {

	@Autowired
	private InstrumService instrumService;
	@Autowired
	private SubdivService subdivService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private AllowInstrumentCreationWithoutPlantNo allowedInstrCreatwitoutPlantNo;
    @Autowired
    private AllowInstrumentCreationWithoutSerialNo allowedInstrCreatwitoutSerialNo;
    @Autowired
    private MfrService mfrService;
    @Autowired
    private InstrumentModelService instrumentModelService;
    @Autowired
    private NewDescriptionService descriptionService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private CompanyGroupService companyGroupService;

    private Locale locale;
    private Integer subdivId;
    private Integer businessSubdivId;
	private Subdiv subdiv;
	private Subdiv businesSubdiv;
	private boolean isallowedInstrCreatwitoutPlantNo;
	private boolean isallowedInstrCreatwitoutSerialNo;

	public InstrumentImportBusinessValidationProcessor(
		@Value("#{jobParameters['" + InstrumentImportConfig.CLIENT_SUBDIV_ID + "']}") Integer clientSubdivId,
		@Value("#{jobParameters['" + InstrumentImportConfig.BUSINESS_SUBDIV_ID + "']}") Integer businessSubdivId,
		@Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale) {

		this.subdivId = clientSubdivId;
		this.businessSubdivId = businessSubdivId;
		this.locale = new Locale(locale);
	}

	@PostConstruct
	public void postConstruct() {
		this.subdiv = subdivService.get(subdivId);
		this.businesSubdiv = subdivService.get(businessSubdivId);
		this.isallowedInstrCreatwitoutPlantNo = allowedInstrCreatwitoutPlantNo.getValueByScope(Scope.COMPANY,
			businesSubdiv.getComp().getCoid(), businesSubdiv.getComp().getCoid());
		this.isallowedInstrCreatwitoutSerialNo = allowedInstrCreatwitoutSerialNo.getValueByScope(Scope.COMPANY,
			businesSubdiv.getComp().getCoid(), businesSubdiv.getComp().getCoid());
	}

	@Override
	public List<ItemAnalysisResult> process(ImportedInstrumentsSynthesisRowDTO dto) throws Exception {

		/*
		 * check if instrument exists based on : if instrument exists with only the
		 * plantno => warning if instrument exists with only the serialno =>warning if
		 * instrument exists with both the plantno and serialno => error
		 */

		Company clientCompany = subdiv.getComp();
		// get the name of company group here to avoid "could not initialize proxy - no Session" error
		String companyGroupName = clientCompany.getCompanyGroup() != null ?
			(this.companyGroupService.get(clientCompany.getCompanyGroup().getId()) != null ?
				this.companyGroupService.get(clientCompany.getCompanyGroup().getId()).getGroupName() : "") : "";

		if (!isallowedInstrCreatwitoutSerialNo && StringUtils.isBlank(dto.getSerialno()))
			addError(dto, ExchangeFormatFieldNameEnum.SERIAL_NUMBER, "error.instrument.serialno", null);
		if (!isallowedInstrCreatwitoutPlantNo && StringUtils.isBlank(dto.getPlantno()))
			addError(dto, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.plantno", null);

		if (StringUtils.isBlank(dto.getPlantno())) {
			if (instrumService.checkExists(dto.getPlantno(), dto.getSerialno(), clientCompany.getCoid(), null)) {
				// Instrument you are trying to add have potential duplicates the company, please review before adding!
				addWarning(dto, "error.instrument.validate.duplicate.company",
					new String[]{""});
			} else if (clientCompany.getCompanyGroup() != null && instrumService.checkExists(dto.getPlantno(), dto.getSerialno(), null, clientCompany.getCompanyGroup().getId())) {
				// Instrument you are trying to add have potential duplicates the group company, please review before adding!
				addWarning(dto, "error.instrument.validate.duplicate.companygroup",
					new String[]{companyGroupName});
			}
		}
		else if(instrumService.checkExists(dto.getPlantno(), dto.getSerialno(), clientCompany.getCoid(),null)) {
			if(StringUtils.isNoneBlank(dto.getSerialno())){
				addError(dto, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.validate.serialnoandplantno.exists", null);
			} else {
				addWarning(dto, "error.instrument.validate.duplicate.company",
					new String[]{""});
			}
		} 
		else if(clientCompany.getCompanyGroup() != null && instrumService.checkExists(dto.getPlantno(), dto.getSerialno(), null, clientCompany.getCompanyGroup().getId())) {
			addWarning(dto, "error.instrument.validate.duplicate.companygroup",
				new String[]{companyGroupName});
		}
				
		// contact id
		if (dto.getContactId() != null) {
			Contact contact = contactService.get(dto.getContactId());
			if (contact == null)
				addError(dto, ExchangeFormatFieldNameEnum.CONTACT_ID, "error.rest.data.notfound", null);
		}

		// address id
		if (dto.getAddressId() != null) {
			Address address = addressService.get(dto.getAddressId());
			if (address == null)
				addError(dto, ExchangeFormatFieldNameEnum.ADDRESS_ID, "error.rest.data.notfound", null);
		}

		// brand validation --
		if (dto.getTmlBrandId() != null) {
			Mfr mfr = mfrService.findTMLMfr(dto.getTmlBrandId());
			if (mfr == null) {
				addError(dto, ExchangeFormatFieldNameEnum.TML_BRAND_ID, "error.rest.data.notfound", null);
			}
		} else if (StringUtils.isNoneBlank(dto.getBrand())) {
			Mfr mfr = mfrService.findMfrByName(dto.getBrand());
			if (mfr == null) {
				addError(dto, ExchangeFormatFieldNameEnum.BRAND, "error.rest.data.notfound", null);
			}
		}

		// instrument model --
		if (dto.getTmlInstrumentModelId() != null) {
			InstrumentModel instrumentModel = instrumentModelService
					.getTMLInstrumentModel(dto.getTmlInstrumentModelId());
			if (instrumentModel == null) {
				addError(dto, ExchangeFormatFieldNameEnum.TML_INSTRUMENT_MODEL_ID, "error.rest.data.notfound", null);
			}
		} else if (dto.getInstrumentModelId() != null) {
			InstrumentModel instrumentModel = instrumentModelService.get(dto.getInstrumentModelId());
			if (instrumentModel == null) {
				addError(dto, ExchangeFormatFieldNameEnum.INSTRUMENT_MODEL_ID, "error.rest.data.notfound", null);
			}
		}

		if (dto.getInstrumentModelId() == null) {
			addError(dto, ExchangeFormatFieldNameEnum.INSTRUMENT_MODEL_ID, "error.rest.data.notfound", null);
		}

		// Sub-family --
		if (dto.getTmlSubFamilyId() != null) {
			Description description = descriptionService.getTMLDescription(dto.getTmlSubFamilyId());
			if (description == null) {
				addError(dto, ExchangeFormatFieldNameEnum.TML_SUBFAMILY_ID, "error.rest.data.notfound", null);
			}
		} else if (dto.getSubFamilyId() != null) {
			Description description = descriptionService.findDescription(dto.getSubFamilyId());
			if (description == null) {
				addError(dto, ExchangeFormatFieldNameEnum.TML_SUBFAMILY_ID, "error.rest.data.notfound", null);
			}
		}

		// default procedure
        if (StringUtils.isNoneBlank(dto.getDefaultProcedureReference())) {
            Capability capability = capabilityService
                .getCapabilityByReference(dto.getDefaultProcedureReference(), null, null).stream().findFirst()
                .orElse(null);
            if (capability == null) {
                addError(dto, ExchangeFormatFieldNameEnum.DEFAULT_PROCEDURE_REFERENCE, "error.rest.data.notfound",
                    null);
            } else {
                dto.setDefaultProcedureId(capability.getId());
            }
        } else if (dto.getDefaultProcedureId() != null) {
            Capability capability = capabilityService.get(dto.getDefaultProcedureId());
            if (capability == null) {
                addError(dto, ExchangeFormatFieldNameEnum.DEFAULT_PROCEDURE_REFERENCE, "error.rest.data.notfound",
                    null);
            } else {
                dto.setDefaultProcedureReference(capability.getReference());
            }
        }

		// service type
		if (dto.getDefaultServiceTypeId() != null) {
			ServiceType serviceType = serviceTypeService.get(dto.getDefaultServiceTypeId());
			if (serviceType == null) {
				addError(dto, ExchangeFormatFieldNameEnum.SERVICETYPE_ID, "error.rest.data.notfound", null);
			}
		}

		// Calibration Frequency
		if (dto.getCalFrequency() == null)
			addError(dto, ExchangeFormatFieldNameEnum.CAL_FREQUENCY, "error.rest.data.notfound", null);

		// Interval Unit
		if (dto.getIntervalUnit() == null)
			addError(dto, ExchangeFormatFieldNameEnum.INTERVAL_UNIT, "error.rest.data.notfound", null);

		// Usage type
		if (StringUtils.isNoneBlank(dto.getInstrumentUsageType()) && dto.getInstrumentUsageTypeId() == null)
			addError(dto, ExchangeFormatFieldNameEnum.INSTRUMENT_USAGE_TYPE, "error.rest.data.notfound",
					new String[] { dto.getInstrumentUsageType() });

		if (dto.getItemAnalysisResult().isEmpty())
			return null;

		return dto.getItemAnalysisResult();
	}

	private void addError(ImportedInstrumentsSynthesisRowDTO item, ExchangeFormatFieldNameEnum field,
						  String messageCode, Object[] params) {
		ItemAnalysisResult ias = new ItemAnalysisResult(item.getIndex(), field,
			messageSource.getMessage(messageCode, params, locale), null);
		item.getItemAnalysisResult().add(ias);

	}

	private void addWarning(ImportedInstrumentsSynthesisRowDTO item,
							String messageCode, Object[] params) {
		ItemAnalysisResult ias = new ItemAnalysisResult(item.getIndex(), ExchangeFormatFieldNameEnum.PLANT_NO, null,
			messageSource.getMessage(messageCode, params, locale));
		item.getItemAnalysisResult().add(ias);
	}

}
