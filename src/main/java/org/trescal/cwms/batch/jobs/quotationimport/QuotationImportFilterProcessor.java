package org.trescal.cwms.batch.jobs.quotationimport;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResultService;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;

import java.util.List;

@Component
@StepScope
public class QuotationImportFilterProcessor
	implements ItemProcessor<ImportedQuotationItemsSynthesisRowDTO, ImportedQuotationItemsSynthesisRowDTO> {

	@Autowired
	private ItemAnalysisResultService iarService;
	private Integer lastAnalysisStepId;

	public QuotationImportFilterProcessor(@Value("#{jobExecutionContext['" + BatchConfig.JOB_EC_LAST_ANALAYSIS_STEP_ID
			+ "']}") Integer lastAnalysisStepId) {
		this.lastAnalysisStepId = lastAnalysisStepId;
	}

	@Override
	public ImportedQuotationItemsSynthesisRowDTO process(ImportedQuotationItemsSynthesisRowDTO dto) throws Exception {

		List<ItemAnalysisResult> list = iarService.getAllAnalysisResults(lastAnalysisStepId, dto.getIndex(),
				dto.getIndex());

		// decide whether to send to submitter
		boolean hasErrors = list.stream().anyMatch(i -> StringUtils.isNotBlank(i.getError()));

		if (hasErrors)
			return null;

		return dto;
	}

}
