package org.trescal.cwms.batch.jobs.operationsimport;

import javax.annotation.PostConstruct;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatzipfile.OperationsImportZipFile;

@Component
@JobScope
public class CertificatesFilesImportDecider implements JobExecutionDecider {

	public static final String CONTINUE = "CONTINUE";
	public static final String EXIT = "EXIT";

	@Autowired
	private ExchangeFormatFileService efFileService;

	private Integer efFileId;
	private ExchangeFormatFile efFile;

	public CertificatesFilesImportDecider(
			@Value("#{jobParameters['" + BatchConfig.EF_FILE_ID + "']}") Integer efFileId) {
		this.efFileId = efFileId;
	}

	@PostConstruct
	public void postConstruct() {
		this.efFile = efFileService.get(this.efFileId);
	}

	@Override
	public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {

		if (this.efFile.getType().equals(OperationsImportZipFile.class.getSimpleName()))
			return new FlowExecutionStatus(CONTINUE);
		else
			return new FlowExecutionStatus(EXIT);
	}

}
