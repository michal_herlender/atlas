package org.trescal.cwms.batch.jobs.importInstrument;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecutionService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;

@Component
@StepScope
public class InstrumentImportSubmitter implements ItemWriter<ImportedInstrumentsSynthesisRowDTO> {

	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager entityManager;
	@Autowired
	private BatchStepExecutionService bseService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private InstrumService instService;

	private StepExecution stepExecution;
	private ExecutionContext jobExecutionContext;

	private Integer subdivid;
	private Integer defaultAddressid;
	private Integer defaultContactid;
	private Integer defaultServiceTypeId;
	private Contact currentContact;
	private Integer submitedby;
	private Long insertedInstrumentsCount = 0L;

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
		this.jobExecutionContext = stepExecution.getJobExecution().getExecutionContext();
	}

	@AfterStep
	public void afterStep(StepExecution stepExecution) {
		stepExecution.getJobExecution().getExecutionContext().putLong(BatchConfig.JOB_EC_LAST_ANALAYSIS_STEP_ID,
				stepExecution.getId());
	}

	public InstrumentImportSubmitter(
			@Value("#{jobParameters['" + InstrumentImportConfig.DEFAULT_ADDRESS_ID + "']}") Integer defaultAddressId,
			@Value("#{jobParameters['" + InstrumentImportConfig.DEFAULT_CONTACT_ID + "']}") Integer defaultContactId,
			@Value("#{jobParameters['" + InstrumentImportConfig.DEFAULT_SERVICETYPE_ID
					+ "']}") Integer defaultServiceTypeId,
			@Value("#{jobParameters['" + InstrumentImportConfig.CLIENT_SUBDIV_ID + "']}") Integer subdivId,
			@Value("#{jobParameters['" + InstrumentImportConfig.SUBMITED_BY + "']}") Integer submitedby) {

		this.defaultAddressid = defaultAddressId;
		this.defaultContactid = defaultContactId;
		this.defaultServiceTypeId = defaultServiceTypeId;
		this.subdivid = subdivId;
		this.submitedby = submitedby;
	}

	@PostConstruct
	public void init() {
		this.currentContact = contactService.get(submitedby);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void write(List<? extends ImportedInstrumentsSynthesisRowDTO> items) throws Exception {

		BatchStepExecution bsa = bseService.get(this.stepExecution.getId().intValue());

		List<Instrument> addedInstruments = this.instService.importInstruments(
				(List<ImportedInstrumentsSynthesisRowDTO>) items, subdivid, defaultAddressid, defaultContactid,
				defaultServiceTypeId, currentContact.getId());

		/** save a map of items indexes and inserted instruments ids */
		entityManager.flush();
		Map<Integer, Integer> indexToPlantId = new HashMap<>();
		for (int i = 0; i < items.size(); i++) {
			ImportedInstrumentsSynthesisRowDTO item = items.get(i);
			Instrument instrument = addedInstruments.get(i);
			indexToPlantId.put(item.getIndex(), instrument.getPlantid());
		}
		// get old map if it exists
		Map<Integer, Integer> oldindexToPlantId = (Map<Integer, Integer>) stepExecution.getExecutionContext()
				.get(InstrumentImportConfig.JOB_EC_INDEX_TO_PLANTID_MAP);
		if (oldindexToPlantId == null)
			oldindexToPlantId = new HashMap<>();
		// merge the two maps and re-save
		indexToPlantId.putAll(oldindexToPlantId);
		stepExecution.getExecutionContext().put(InstrumentImportConfig.JOB_EC_INDEX_TO_PLANTID_MAP, indexToPlantId);

		/** save the count of the instruments inserted */
		insertedInstrumentsCount += addedInstruments.size();
		jobExecutionContext.putLong(InstrumentImportConfig.JOB_EC_INSERTED_INSTRUMENTS,
				insertedInstrumentsCount);

		bseService.save(bsa);

	}

}
