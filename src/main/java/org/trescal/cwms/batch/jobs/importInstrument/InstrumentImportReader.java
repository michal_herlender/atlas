package org.trescal.cwms.batch.jobs.importInstrument;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.exchangeformat.EFExcelReader;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;

@Component
@StepScope
public class InstrumentImportReader extends EFExcelReader<ImportedInstrumentsSynthesisRowDTO> {

	public InstrumentImportReader(@Value("#{jobParameters['" + BatchConfig.EF_ID + "']}") Integer efId,
			@Value("#{jobParameters['" + BatchConfig.EF_FILE_ID + "']}") Integer efFileId,
			@Autowired InstrumentImportMapper rowMapper,
			@Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale) {
		super(efId, efFileId, rowMapper, locale);
	}

}
