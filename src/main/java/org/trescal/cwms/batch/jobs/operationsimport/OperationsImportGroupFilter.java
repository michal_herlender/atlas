package org.trescal.cwms.batch.jobs.operationsimport;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResultService;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Component
@StepScope
public class OperationsImportGroupFilter
	implements ItemProcessor<List<ImportedOperationItem>, List<ImportedOperationItem>> {

	@Autowired
	private ItemAnalysisResultService iarService;

	private AutoSubmitPolicyEnum autoSubmitPolicy;
	private Integer lastAnalysisStepId;

	public OperationsImportGroupFilter(
			@Value("#{jobParameters['" + BatchConfig.AUTO_SUBMIT_POLICY + "']}") String autoSubmitPolicy,
			@Value("#{jobExecutionContext['" + BatchConfig.JOB_EC_LAST_ANALAYSIS_STEP_ID
					+ "']}") Integer lastAnalysisStepId) {
		this.autoSubmitPolicy = AutoSubmitPolicyEnum.valueOf(autoSubmitPolicy);
		this.lastAnalysisStepId = lastAnalysisStepId;
	}

	@Override
	public List<ImportedOperationItem> process(List<ImportedOperationItem> group) throws Exception {
        if (group.isEmpty())
            return null;

        // get analysis results from past step
        Integer startIndex = group.stream().mapToInt(ImportedOperationItem::getIndex).min().orElseThrow(NoSuchElementException::new);
        Integer endIndex = group.stream().mapToInt(ImportedOperationItem::getIndex).max().orElseThrow(NoSuchElementException::new);
        List<ItemAnalysisResult> iars = iarService.getAllAnalysisResults(lastAnalysisStepId, startIndex, endIndex);
        group.forEach(i -> i.setItemAnalysisResult(
                iars.stream().filter(j -> j.getIndex().equals(i.getIndex())).collect(Collectors.toList())));

        // decide whether to send to submitter
        boolean hasErrors = group.stream().flatMap(i -> i.getItemAnalysisResult().stream())
                .anyMatch(i -> StringUtils.isNotBlank(i.getError()));
        if (hasErrors)
            return null;
        boolean hasWarnings = group.stream().flatMap(i -> i.getItemAnalysisResult().stream())
				.anyMatch(i -> StringUtils.isNotBlank(i.getWarning()));
		if (autoSubmitPolicy.equals(AutoSubmitPolicyEnum.SUBMIT_ONLY_ITEMS_WITHOUT_ERRORS_AND_WITHOUT_WARNINGS)
				&& hasWarnings)
			return null;

		return group;
	}

}
