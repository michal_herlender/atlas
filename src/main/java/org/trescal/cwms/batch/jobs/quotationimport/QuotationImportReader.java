package org.trescal.cwms.batch.jobs.quotationimport;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.exchangeformat.EFExcelReader;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;

@Component
@StepScope
public class QuotationImportReader extends EFExcelReader<ImportedQuotationItemsSynthesisRowDTO> {

	public QuotationImportReader(@Value("#{jobParameters['" + BatchConfig.EF_ID + "']}") Integer efId,
			@Value("#{jobParameters['" + BatchConfig.EF_FILE_ID + "']}") Integer efFileId,
			@Autowired QuotationImportMapper rowMapper,
			@Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale) {
		super(efId, efFileId, rowMapper, locale);
	}

	protected ImportedQuotationItemsSynthesisRowDTO doRead() throws Exception {

		XSSFRow firstRow = sheet.getRow(this.currentRowIndex);
		LinkedCaseInsensitiveMap<String> firstRowContent = readRowContent(firstRow);
		ImportedQuotationItemsSynthesisRowDTO item = ((QuotationImportMapper) this.rowMapper).mapItem(firstRowContent,
				this.currentRowIndex, this.locale);

		if (item == null || firstRow == null) {
			return null;
		}
		this.currentRowIndex++;
		return item;

	}

}
