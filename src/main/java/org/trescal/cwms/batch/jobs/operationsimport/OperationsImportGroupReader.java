package org.trescal.cwms.batch.jobs.operationsimport;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.exchangeformat.EFExcelReader;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatzipfile.OperationsImportZipFile;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatOperationTypeEnum;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@StepScope
public class OperationsImportGroupReader extends EFExcelReader<List<ImportedOperationItem>> {

	private ExecutionContext jobExecutionContext;

	public OperationsImportGroupReader(@Value("#{jobParameters['" + BatchConfig.EF_ID + "']}") Integer efId,
									   @Value("#{jobParameters['" + BatchConfig.EF_FILE_ID + "']}") Integer efFileId,
									   @Autowired OperationsImportGroupMapper rowMapper,
			@Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale) {
		super(efId, efFileId, rowMapper, locale);
	}

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) throws IOException {
		this.jobExecutionContext = stepExecution.getJobExecution().getExecutionContext();

		if (this.efFile.getType().equals(OperationsImportZipFile.class.getSimpleName())) {
			// read all certificate files names (.PDF/.doc/.docx), they should exist in the
			// same folder as the excel file
			File currentFolder = this.resource.getFile().getParentFile();
			List<String> certificatesFileNames = Arrays.asList(currentFolder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String filename) {
					return filename.toLowerCase().endsWith(".pdf") || filename.toLowerCase().endsWith(".doc")
							|| filename.toLowerCase().endsWith(".docx");
				}
			})).stream().map(f -> FilenameUtils.getBaseName(f.getPath())).collect(Collectors.toList());
			this.jobExecutionContext.put(OperationsImportConfig.JOB_EC_CERTIFICATES_FILENAMES, certificatesFileNames);
		}
	}

	@Override
	protected List<ImportedOperationItem> doRead() throws Exception {

		List<ImportedOperationItem> group = new ArrayList<>();

		XSSFRow firstRow = sheet.getRow(this.currentRowIndex);
		LinkedCaseInsensitiveMap<String> firstRowContent = readRowContent(firstRow);
		ImportedOperationItem firstItem = ((OperationsImportGroupMapper) this.rowMapper).mapItem(firstRowContent,
				this.currentRowIndex, this.locale);

		if (firstItem == null)
			return null;

		addToGroup(firstItem, group);

		this.currentRowIndex++;

		ImportedOperationItem item = null;
		boolean newGroup = false;
		do {
			XSSFRow row = sheet.getRow(this.currentRowIndex);
			LinkedCaseInsensitiveMap<String> rowContent = readRowContent(row);
			item = ((OperationsImportGroupMapper) this.rowMapper).mapItem(rowContent, this.currentRowIndex,
					this.locale);

			/**
			 * if row is empty (exit immediately since we cannot do next conditions with a
			 * null row)
			 */
			if (item == null) {
				this.currentRowIndex++;
				return group;
			}

			/** if row is on different jobitem equivalent id */
			boolean differentJobitemEquivalentIds = !item.getJobitemEquivalentId()
					.equals(firstItem.getJobitemEquivalentId());
			if (differentJobitemEquivalentIds)
				return group;

			/**
			 * if documentNo is different than the previous row (both should be not empty)
			 */
			ImportedOperationItem previousItem = group.get(group.size() - 1);
			boolean differentDocumentNos = StringUtils.isNotBlank(previousItem.getDocumentNumber())
					&& StringUtils.isNotBlank(item.getDocumentNumber())
					&& !item.getDocumentNumber().equalsIgnoreCase(previousItem.getDocumentNumber());
			boolean frOperationAndNextOpIsAdjustmentOrRestiction = ExchangeFormatOperationTypeEnum.FR
					.equals(previousItem.getOperationType())
					&& (BooleanUtils.isTrue(item.getAdjustment() || BooleanUtils.isTrue(item.getRestiction())));
			if (differentDocumentNos && !frOperationAndNextOpIsAdjustmentOrRestiction)
				return group;

			addToGroup(item, group);
			this.currentRowIndex++;

		} while (!newGroup);

		return group;
	}

	/**
	 * Adds item to group and sets the next service type id if it is provided and on
	 * the same job item for FR Operations
	 * 
	 */
	private void addToGroup(ImportedOperationItem item, List<ImportedOperationItem> group) {
		if (ExchangeFormatOperationTypeEnum.FR.equals(item.getOperationType())) {
			// read next row and if it is not null, get the service type id
			ImportedOperationItem nextItem = getNextItem();
			if (nextItem != null && nextItem.getJobitemEquivalentId().equals(item.getJobitemEquivalentId()))
				item.setNextServiceTypeId(nextItem.getServiceTypeId());
		}
		group.add(item);
	}

	private ImportedOperationItem getNextItem() {
		XSSFRow row = sheet.getRow(this.currentRowIndex + 1);
		LinkedCaseInsensitiveMap<String> rowContent = readRowContent(row);
		ImportedOperationItem nextItem = ((OperationsImportGroupMapper) this.rowMapper).mapItem(rowContent,
				this.currentRowIndex, this.locale);
		return nextItem;
	}

}
