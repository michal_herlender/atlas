package org.trescal.cwms.batch.jobs.quotationimport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;

@Component
@StepScope
public class QuotationImportValidationPocessor
		implements ItemProcessor<ImportedQuotationItemsSynthesisRowDTO, List<ItemAnalysisResult>> {

	@Autowired
	private QuotationService quoteService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private InstrumentModelService modelServ;
	@Autowired
	private MessageSource messageSource;

	private Locale locale;
	private Integer quoteId;
	private ExecutionContext jobExecutionContext;
	private Set<Integer> seenInstItems = new HashSet<>();
	private List<Integer> getQuoteInstIds = new ArrayList<>();
	private MultiValuedMap<Integer, Integer> seenInstWithSameServiceType_File = new HashSetValuedHashMap<>();
	private MultiValuedMap<Integer, Integer> seenInstWithSameServiceType_Quotation = new HashSetValuedHashMap<>();
	private Boolean priceAndServiceTypeFromFile;

	public QuotationImportValidationPocessor(
			@Value("#{jobParameters['" + QuotationImportConfig.QUOTE_ID + "']}") Integer quoteId,
			@Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale, @Value("#{jobParameters['"
					+ QuotationImportConfig.PRICE_SERVICE_TYPE_FROM_FILE + "']}") String priceAndServiceTypeFromFile) {
		this.locale = new Locale(locale);
		this.quoteId = quoteId;
		this.priceAndServiceTypeFromFile = Boolean.valueOf(priceAndServiceTypeFromFile);
	}

	@PostConstruct
	public void postConstruct() {
		this.getQuoteInstIds = quoteService.getPlantIdsFromQuotation(quoteId);
		this.seenInstWithSameServiceType_Quotation = quoteService.quotationItemsFromInstrument(quoteId);

	}

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.jobExecutionContext = stepExecution.getJobExecution().getExecutionContext();
	}

	@Override
	public List<ItemAnalysisResult> process(ImportedQuotationItemsSynthesisRowDTO item) throws Exception {

		if (item.getPlantId() != null) {
			// get prices and service types from the excel file
			if (priceAndServiceTypeFromFile) {
				validatePriceAndServiceType(item);
			}
			if (instrumentService.get(item.getPlantId()) == null) {
				addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.plantid.notfound",
						new String[] { String.valueOf(item.getPlantId()) });
			} else if (!priceAndServiceTypeFromFile) {
				if (getQuoteInstIds.contains(item.getPlantId())) {
					addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.onquote", new String[] {});
				} else if (seenInstItems.contains(item.getPlantId())) {
					addError(item, ExchangeFormatFieldNameEnum.PLANT_NO,
							"exchangeformat.import.itemstatus.duplicatedintable", new String[] {});
				} else{
					seenInstItems.add(item.getPlantId());
				}
			} else if(item.getServiceTypeId() != null) {
				if (seenInstWithSameServiceType_Quotation.get(item.getPlantId()).contains(item.getServiceTypeId())) {
					addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.onquote", new String[] {});
				} else if (seenInstWithSameServiceType_File.get(item.getPlantId()).contains(item.getServiceTypeId())) {
					addError(item, ExchangeFormatFieldNameEnum.PLANT_NO,
							"exchangeformat.import.itemstatus.duplicatedintable", new String[] {});
				} else {
					seenInstWithSameServiceType_File.put(item.getPlantId(), item.getServiceTypeId());
				}
			}

		} else if (StringUtils.isNoneBlank(item.getPlantNo())) {
			List<Instrument> inst = instrumentService.getByOwningCompany(null, null, item.getPlantNo(),
					null);

			// get prices and service types from the excel file
			if (priceAndServiceTypeFromFile) {
				validatePriceAndServiceType(item);
			}

			if(inst.isEmpty()) {
				addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.plantid.notfound",
						new String[] { String.valueOf(item.getPlantId()) });
			} else if (inst.size() > 1) {
				addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.duplicateplantno",
						new String[] {});
			} else if (!priceAndServiceTypeFromFile && getQuoteInstIds.contains(inst.get(0).getPlantid())) {
					addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.onquote", new String[] {});
			} else if (priceAndServiceTypeFromFile && item.getServiceTypeId() != null && seenInstWithSameServiceType_Quotation
					.get(inst.get(0).getPlantid()).contains(item.getServiceTypeId())){
				addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "error.instrument.onquote", new String[] {});
			}
			// set plant id if no error found
			else {
				@SuppressWarnings("unchecked")
				Map<Integer, Integer> identifiedInstruments = (Map<Integer, Integer>) this.jobExecutionContext
						.get(QuotationImportConfig.JOB_EC_IDENTIFIED_INST);
				if (identifiedInstruments == null)
					identifiedInstruments = new HashMap<>();
				identifiedInstruments.put(item.getIndex(), inst.get(0).getPlantid());
				this.jobExecutionContext.put(QuotationImportConfig.JOB_EC_IDENTIFIED_INST, identifiedInstruments);
				item.setPlantId(inst.get(0).getPlantid());
				if(!priceAndServiceTypeFromFile){
					if (seenInstItems.contains(item.getPlantId())) {
						addError(item, ExchangeFormatFieldNameEnum.PLANT_NO,
								"exchangeformat.import.itemstatus.duplicatedintable", new String[] {});
					} else {
						seenInstItems.add(item.getPlantId());
					}
				}
				else if (item.getServiceTypeId() != null) {
					if (seenInstWithSameServiceType_File.get(item.getPlantId()).contains(item.getServiceTypeId())) {
						addError(item, ExchangeFormatFieldNameEnum.PLANT_NO,
								"exchangeformat.import.itemstatus.duplicatedintable", new String[] {});
					} else
						seenInstWithSameServiceType_File.put(item.getPlantId(), item.getServiceTypeId());
				}
			}
		} else if (item.getModelid() != null) {

			// price and service type should be not null
			validatePriceAndServiceType(item);
			// we should identify the instrument model using the model id
			if (modelServ.findInstrumentModel(item.getModelid()) == null) {
				addError(item, ExchangeFormatFieldNameEnum.INSTRUMENT_MODEL_ID, "error.instrument.model.notfound",
						new String[] {});
			}
		} else if (item.getTmlid() != null) {

			// price and service type should be not null
			validatePriceAndServiceType(item);
			// we should identify the instrument model using the TML id
			InstrumentModel instModel = modelServ.getTMLInstrumentModel(item.getTmlid());
			if (instModel == null) {
				addError(item, ExchangeFormatFieldNameEnum.TML_INSTRUMENT_MODEL_ID, "error.instrument.model.notfound",
						new String[] {});
			} else {
				@SuppressWarnings("unchecked")
				Map<Integer, Integer> indentifiedInstrumentModels = (Map<Integer, Integer>) this.jobExecutionContext
						.get(QuotationImportConfig.JOB_EC_IDENTIFIED_INST_MODEL);
				if (indentifiedInstrumentModels == null)
					indentifiedInstrumentModels = new HashMap<>();
				indentifiedInstrumentModels.put(item.getIndex(), instModel.getModelid());
				this.jobExecutionContext.put(QuotationImportConfig.JOB_EC_IDENTIFIED_INST, indentifiedInstrumentModels);
				item.setModelid(instModel.getModelid());
			}
		} else
			addError(item, ExchangeFormatFieldNameEnum.PLANT_NO, "error.import.quotation.notvalid", new String[] {});

		return item.getItemAnalysisResult();
	}

	/**
	 * add error message
	 */
	private void addError(ImportedQuotationItemsSynthesisRowDTO item, ExchangeFormatFieldNameEnum field,
			String messageCode, Object[] params) {
		ItemAnalysisResult ias = new ItemAnalysisResult(item.getIndex(), field,
				messageSource.getMessage(messageCode, params, locale), null);
		item.getItemAnalysisResult().add(ias);

	}

	/**
	 * final price and service type should be not null in cases : *** import
	 * items using instrument model *** import items using instrument with
	 * priceAndServiceTypeFromFile is true
	 */
	private void validatePriceAndServiceType(ImportedQuotationItemsSynthesisRowDTO item) {
		if (item.getFinalPrice() == null) {
			addError(item, ExchangeFormatFieldNameEnum.FINAL_PRICE, "error.final.price.null",
					new String[] { String.valueOf(item.getFinalPrice()) });
		}
		// the service type should be not null
		if (StringUtils.isEmpty(item.getServiceType())) {
			addError(item, ExchangeFormatFieldNameEnum.EXPECTED_SERVICE, "error.service.type.null",
					new String[] { String.valueOf(item.getServiceType()) });
		}
	}
}
