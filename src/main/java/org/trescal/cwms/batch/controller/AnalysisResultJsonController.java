package org.trescal.cwms.batch.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecutionService;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResultService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.AllArgsConstructor;
import lombok.Data;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE })
public class AnalysisResultJsonController {

	@Autowired
	private ItemAnalysisResultService iarService;
	@Autowired
	private BatchStepExecutionService batchStepExecutionService;

	@GetMapping(value = "/getAllAnalysisResults.json")
	@ResponseBody
	public PagedResultSet<AnalysisResultRowDto> getAllAnalysisResults(
			@RequestParam(required = true) Integer batchStepExecutionId,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "50") Integer maxResults,
			@RequestParam(required = false, defaultValue = "false") Boolean requestPagesInfo,
			@RequestParam(required = false, defaultValue = "ALL") RowsToDisplayEnum rowsToDisplay) {

		PagedResultSet<AnalysisResultRowDto> prs = batchStepExecutionService
				.getFileDataWithAnalysisResults(batchStepExecutionId, page, maxResults, rowsToDisplay);

		if (requestPagesInfo) {
			prs.setMetaData(iarService.getResultIndexesWithErrorsAndWarnings(batchStepExecutionId));
		}

		return prs;
	}

	@Data
	@AllArgsConstructor
	public static class AnalysisResultErrorsAndWarnings {
		Integer index;
		Boolean hasError;
		Boolean hasWarning;
	}

	@Data
	public static class AnalysisResultRowDto {

		public AnalysisResultRowDto(int currentIndex, List<AnalysisResultCellDto> data) {
			this.index = currentIndex;
			this.data = data;
		}

		Integer index;
		List<AnalysisResultCellDto> data;
		String metadata;
	}

	@Data
	public static class AnalysisResultCellDto {

		public AnalysisResultCellDto(ExchangeFormatFieldNameEnum efColumn, String excelColumn, String value,
				Set<String> warnings, Set<String> errors) {
			this.efColumn = efColumn;
			this.excelColumn = excelColumn;
			this.value = value;
			this.warnings = warnings;
			this.errors = errors;
		}

		ExchangeFormatFieldNameEnum efColumn;
		String excelColumn;
		String value;
		Set<String> warnings;
		Set<String> errors;
		String metadata;

	}

	/** values used inside web component, file : cwms-analysis-results.html.js */
	public enum RowsToDisplayEnum {
		ALL, ONLY_ROWS_WITH_ERRORS, ONLY_ROWS_WITH_WARNINGS;
	}

}
