package org.trescal.cwms.batch.controller;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;

import lombok.Getter;
import lombok.Setter;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE })
public class BgTasksController {

	public static final String GET_URL = "/bgtasks.htm";

	@GetMapping(value = GET_URL)
	public String doGet(Model model, @RequestParam(name = "personid", required = false) Integer personid,
			@RequestParam(name = "jobname", required = false) String jobname,
			@RequestParam(required = false) Integer id) {
		model.addAttribute("personid", personid);
		model.addAttribute("jobname", jobname);
		model.addAttribute("id", id);
		return "trescal/core/admin/bgtasks";
	}

	@Getter
	@Setter
	public static class BgTaskDto {

		private Integer bgTaskId;
		private String submittedByFullName;
		private Date submittedOn;
		private AutoSubmitPolicyEnum submitPolicy;
		private String fileName;
		private String businessSubdivName;
		private String clientSubdivName;
		private String clientCompanyName;
		private Integer clientSubdivId;
		private Integer clientCompanyId;
		private String exchangeFormatName;

		private Integer jobInstanceId;
		private String batchJobName;
		private Integer executions;
		private String lastExecutionStatus;
		private String lastExecutionMessage;
		private Integer lastJobExecutionId;

		public BgTaskDto(Integer bgTaskId, String submittedByFullName, Date submittedOn,
				AutoSubmitPolicyEnum submitPolicy, String fileName, String businessSubdivName, String clientSubdivName,
				String clientCompanyName, Integer clientSubdivId, Integer clientCompanyId, Integer jobInstanceId,
				String batchJobName, Long executions, String lastExecutionStatus, String lastExecutionMessage,
				String exchangeFormatName, Integer lastJobExecutionId) {
			super();
			this.bgTaskId = bgTaskId;
			this.submittedByFullName = submittedByFullName;
			this.submittedOn = submittedOn;
			this.submitPolicy = submitPolicy;
			this.fileName = fileName;
			this.businessSubdivName = businessSubdivName;
			this.clientSubdivName = clientSubdivName;
			this.clientCompanyName = clientCompanyName;
			this.clientSubdivId = clientSubdivId;
			this.clientCompanyId = clientCompanyId;
			this.jobInstanceId = jobInstanceId.intValue();
			this.batchJobName = batchJobName;
			this.executions = executions.intValue();
			this.lastExecutionStatus = lastExecutionStatus;
			this.lastExecutionMessage = lastExecutionMessage;
			this.exchangeFormatName = exchangeFormatName;
			this.lastJobExecutionId = lastJobExecutionId;
		}

	}

}
