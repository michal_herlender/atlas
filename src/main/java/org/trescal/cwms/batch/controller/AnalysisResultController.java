package org.trescal.cwms.batch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecutionService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE })
public class AnalysisResultController {

	@Autowired
	private BatchStepExecutionService batchStepExecutionService;

	@RequestMapping(value = "/analysisResults.htm", method = RequestMethod.GET)
	public String doGet(@RequestParam Integer lastJobExecutionId, Model model) {
		BatchStepExecution batchStepExecution = batchStepExecutionService.getLastCompletedStep(lastJobExecutionId,
			BatchConfig.ANALYSIS_STEP);
		model.addAttribute("jobName", batchStepExecution.getJobExecution().getJobInstance().getJobName());
		model.addAttribute("stepExecutionId", batchStepExecution.getId());
		model.addAttribute("efStartIndex",
			batchStepExecution.getJobExecution().getJobInstance().getBgTask().getEf().getLinesToSkip());
		return "trescal/batch/analysisstepresults";
	}

}
