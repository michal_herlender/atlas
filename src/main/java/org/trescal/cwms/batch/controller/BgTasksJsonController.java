package org.trescal.cwms.batch.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTaskService;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecution;
import org.trescal.cwms.batch.config.entities.batchjobexecution.BatchJobExecutionService;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecution;
import org.trescal.cwms.batch.controller.BgTasksController.BgTaskDto;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.Getter;
import lombok.Setter;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE })
public class BgTasksJsonController {

	@Autowired
	private BatchJobExecutionService batchJobExecutionService;
	@Autowired
	private BackgroundTaskService backgroundTaskService;

	@GetMapping(value = "/getAllBgTasks.json")
	@ResponseBody
	public PagedResultSet<BgTaskDto> getJobs(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "25") Integer maxResults, @RequestParam(required = false) Integer id,
			@RequestParam(name = "personid", required = false) Integer personid,
			@RequestParam(name = "jobname", required = false) String jobname,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		return backgroundTaskService.getBgTaskDtos(maxResults, page, id, personid, jobname, subdivDto.getKey());
	}

	@GetMapping(value = "/getAllExecutionsByJobInstance.json")
	@ResponseBody
	public List<BgTasksJsonController.ExecutionsDto> getAllExecutionsByJobInstance(
			@RequestParam(required = true) Integer batchJobId)
			throws IllegalAccessException, InvocationTargetException {

		List<BgTasksJsonController.ExecutionsDto> dtos = new ArrayList<>();

		List<BatchJobExecution> execs = batchJobExecutionService
				.fetchAllBatchJobExecutionsAndStepsByBatchJobId(batchJobId);

		for (BatchJobExecution e : execs) {
			ExecutionsDto dto = new ExecutionsDto();
			BeanUtilsBean.getInstance().copyProperties(dto, e);
			dto.setLevel(0);
			dto.setHasChildren(true);

			dtos.add(dto);

			for (BatchStepExecution se : e.getStepExecutions()) {
				ExecutionsDto subDto = new ExecutionsDto();
				BeanUtilsBean.getInstance().copyProperties(subDto, se);
				subDto.setLevel(1);
				subDto.setHasChildren(true);
				dtos.add(subDto);
				// aggregate
				dto.setReadCount(dto.getReadCount() + subDto.getReadCount());
				dto.setReadSkipCount(dto.getReadSkipCount() + subDto.getReadSkipCount());
				dto.setFilterCount(dto.getFilterCount() + subDto.getFilterCount());
				dto.setProcessSkipCount(dto.getProcessSkipCount() + subDto.getProcessSkipCount());
				dto.setWriteCount(dto.getWriteCount() + subDto.getWriteCount());
				dto.setWriteSkipCount(dto.getWriteSkipCount() + subDto.getWriteSkipCount());
				dto.setCommitCount(dto.getCommitCount() + subDto.getCommitCount());
				dto.setRollbackCount(dto.getRollbackCount() + subDto.getRollbackCount());
			}
		}

		return dtos;
	}

	@Getter
	@Setter
	public static class ExecutionsDto {
		private Integer id;
		private Date startTime;
		private Date endTime;
		private String status;
		private String exitCode;
		private String exitMessage;

		private String stepName;
		private Integer readCount;
		private Integer readSkipCount;
		private Integer filterCount;
		private Integer processSkipCount;
		private Integer writeCount;
		private Integer writeSkipCount;
		private Integer commitCount;
		private Integer rollbackCount;
		private Boolean hasChildren;
		private Integer level;

		public ExecutionsDto() {
			readCount = 0;
			readSkipCount = 0;
			filterCount = 0;
			processSkipCount = 0;
			writeCount = 0;
			writeSkipCount = 0;
			commitCount = 0;
			rollbackCount = 0;
		}
	}

}
