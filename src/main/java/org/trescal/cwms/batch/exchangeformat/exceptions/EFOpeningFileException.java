package org.trescal.cwms.batch.exchangeformat.exceptions;

public class EFOpeningFileException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public EFOpeningFileException(String message) {
        super(message);
    }

}
