package org.trescal.cwms.batch.exchangeformat;

import java.util.Locale;

public interface EFRowMapper<T, P> {

	T map(P fileContent, int itemIndex, Locale locale);

}
