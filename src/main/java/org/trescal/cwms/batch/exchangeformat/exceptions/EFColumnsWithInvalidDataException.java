package org.trescal.cwms.batch.exchangeformat.exceptions;

public class EFColumnsWithInvalidDataException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public EFColumnsWithInvalidDataException(String message) {
        super(message);
    }

}
