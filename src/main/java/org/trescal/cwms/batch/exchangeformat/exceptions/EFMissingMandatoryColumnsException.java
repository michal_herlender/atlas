package org.trescal.cwms.batch.exchangeformat.exceptions;

public class EFMissingMandatoryColumnsException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public EFMissingMandatoryColumnsException(String message) {
        super(message);
    }

}
