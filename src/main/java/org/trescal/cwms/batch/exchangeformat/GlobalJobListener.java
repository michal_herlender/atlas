package org.trescal.cwms.batch.exchangeformat;

import java.util.Locale;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTaskService;
import org.trescal.cwms.batch.config.entities.batchstepexecution.BatchStepExecutionService;
import org.trescal.cwms.batch.jobs.importInstrument.InstrumentImportConfig;
import org.trescal.cwms.batch.jobs.operationsimport.OperationsImportConfig;
import org.trescal.cwms.batch.jobs.quotationimport.QuotationImportConfig;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;

import com.google.common.collect.Iterables;

@Component
public class GlobalJobListener implements JobExecutionListener {

	@Autowired
	private BackgroundTaskService backgroundTaskService;
	@Autowired
	private JobService jobService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private QuotationService quoteService;
	@Autowired
	BatchStepExecutionService batchStepExecutionService;
	@Override
	public void beforeJob(JobExecution jobExecution) {
		// no-op
	}

	@Override
	public void afterJob(JobExecution jobExecution) {

		String localeString = jobExecution.getJobParameters().getString(BatchConfig.LOCALE);
		Locale locale = new Locale(localeString);

		String exitMessage = null;
		/**
		 * Case of batch job failed
		 */
		if (jobExecution.getExitStatus().getExitCode().equals(ExitStatus.FAILED.getExitCode())) {
			if (!jobExecution.getAllFailureExceptions().isEmpty()) {
				// getting the first one
				Throwable th = jobExecution.getAllFailureExceptions().get(0);
				String exceptionMessage = null;
				if (th.getCause() != null)
					exceptionMessage = th.getCause().getMessage();
				else
					exceptionMessage = th.getMessage();

				exitMessage = messageSource.getMessage("calibrationimportation.batch.taskfailedwithexceptions",
						new String[] { exceptionMessage }, locale);
			} else {
				exitMessage = messageSource.getMessage("calibrationimportation.batch.taskfailed", new String[] {},
						locale);
			}
		} else if (jobExecution.getExitStatus().getExitCode().equals(ExitStatus.COMPLETED.getExitCode())) {
			/* operations import job */
			if (jobExecution.getJobInstance().getJobName().equals(OperationsImportConfig.JOB_NAME)) {
				Integer jobId = jobExecution.getJobParameters().getLong(OperationsImportConfig.JOB_ID).intValue();
				// get last executed step
				StepExecution lastStepExecution = Iterables.getLast(jobExecution.getStepExecutions());

				// get job no
				String jobNo = jobService.getJobNo(jobId);

				switch (lastStepExecution.getStepName()) {
				case BatchConfig.ANALYSIS_STEP:

					Long errorsCount = lastStepExecution.getExecutionContext().getLong(BatchConfig.STEP_EC_ERROR_COUNT,
							0l);
					Long warningsCount = lastStepExecution.getExecutionContext()
							.getLong(BatchConfig.STEP_EC_WARNING_COUNT, 0l);

					exitMessage = messageSource.getMessage("calibrationimportation.batch.analysisstepcompletedmessage",
							new String[] { errorsCount.toString(), warningsCount.toString(), jobNo }, locale);

					break;
				case BatchConfig.SUBMIT_STEP:

					Long insertedCalibrations = jobExecution.getExecutionContext()
							.getLong(OperationsImportConfig.JOB_EC_INSTERTED_CALIBRATIONS, 0);

					exitMessage = messageSource.getMessage("calibrationimportation.batch.submitstepcompletedmessage",
							new String[] { insertedCalibrations.toString(), jobNo }, locale);
					break;
				case OperationsImportConfig.CERTIFICATES_FILES_UPLOAD_STEP:

					Long insertedCalibrationsCount = jobExecution.getExecutionContext()
							.getLong(OperationsImportConfig.JOB_EC_INSTERTED_CALIBRATIONS, 0);

					Long uploadedCertificatesFilesCount = jobExecution.getExecutionContext()
							.getLong(OperationsImportConfig.JOB_EC_UPLOADED_CERTIFICATE_FILES, 0);

					exitMessage = messageSource.getMessage(
							"calibrationimportation.batch.certificatesfilesuploadtepcompletedmessage",
							new String[] { insertedCalibrationsCount.toString(), jobNo,
									uploadedCertificatesFilesCount.toString() },
							locale);

					break;
				default:
					break;
				}
			}

			else if (jobExecution.getJobInstance().getJobName().equals(InstrumentImportConfig.JOB_NAME)) {

				// get last executed step
				StepExecution lastStepExecution = Iterables.getLast(jobExecution.getStepExecutions());

				switch (lastStepExecution.getStepName()) {
				case BatchConfig.ANALYSIS_STEP:

					Long errorsCount = lastStepExecution.getExecutionContext().getLong(BatchConfig.STEP_EC_ERROR_COUNT,
							0l);
					Long warningsCount = lastStepExecution.getExecutionContext()
							.getLong(BatchConfig.STEP_EC_WARNING_COUNT, 0l);

					exitMessage = messageSource.getMessage("instrument.batch.analysisstepcompletedmessage",
							new String[] { errorsCount.toString(), warningsCount.toString() }, locale);

					break;
				case BatchConfig.SUBMIT_STEP:

					Long insertedInstruments = jobExecution.getExecutionContext()
							.getLong(InstrumentImportConfig.JOB_EC_INSERTED_INSTRUMENTS, 0);

					exitMessage = messageSource.getMessage("instrument.batch.submitstepcompletedmessage",
							new String[] { insertedInstruments.toString() }, locale);
					break;
				default:
					break;
				}

			}
			/* quotation import job */
			else if (jobExecution.getJobInstance().getJobName().equals(QuotationImportConfig.JOB_NAME)) {
				Integer quoteId = jobExecution.getJobParameters().getLong(QuotationImportConfig.QUOTE_ID).intValue();
				// get last executed step
				StepExecution lastStepExecution = Iterables.getLast(jobExecution.getStepExecutions());
				// get quotation no
				String quotationNo = quoteService.get(quoteId).getQno();
				

				switch (lastStepExecution.getStepName()) {
				case BatchConfig.ANALYSIS_STEP:
					Long errorsCount = lastStepExecution.getExecutionContext().getLong(BatchConfig.STEP_EC_ERROR_COUNT,
							0l);
					exitMessage = messageSource.getMessage("quotation.batch.analysisstepcompletedmessage",
							new String[] { errorsCount.toString() , quotationNo }, locale);

					break;
				case BatchConfig.SUBMIT_STEP:

					Long insertedQuotationItems = jobExecution.getExecutionContext()
							.getLong(QuotationImportConfig.JOB_EC_INSTERTED_QUOTATION_ITEMS, 0);

					exitMessage = messageSource.getMessage("quotation.batch.submitstepcompletedmessage",
							new String[] { insertedQuotationItems.toString(), quotationNo }, locale);
					break;
				default:
					break;
				}

			}
		}

		jobExecution.setExitStatus(new ExitStatus(jobExecution.getExitStatus().getExitCode(), exitMessage));

		// notify completion of job
		backgroundTaskService.notifyBatchJobCompletion(jobExecution);

	}

}
