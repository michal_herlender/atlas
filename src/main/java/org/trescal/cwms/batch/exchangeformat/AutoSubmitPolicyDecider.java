package org.trescal.cwms.batch.exchangeformat;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.BatchConfig;

@Component
@JobScope
public class AutoSubmitPolicyDecider implements JobExecutionDecider {

	public static final String CONTINUE = "CONTINUE";
	public static final String EXIT = "EXIT";
	private AutoSubmitPolicyEnum autoSubmitPolicy;

	public AutoSubmitPolicyDecider(
			@Value("#{jobParameters['" + BatchConfig.AUTO_SUBMIT_POLICY + "']}") String autoSubmitPolicy) {
		this.autoSubmitPolicy = AutoSubmitPolicyEnum.valueOf(autoSubmitPolicy);
	}

	@Override
	public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {

		boolean hasErrors = stepExecution.getExecutionContext().getLong(BatchConfig.STEP_EC_ERROR_COUNT,
				0) != 0;
		boolean hasWarnings = stepExecution.getExecutionContext().getLong(BatchConfig.STEP_EC_WARNING_COUNT,
				0) != 0;

		String status;

		switch (autoSubmitPolicy) {
		case SUBMIT_IF_ALL_WITHOUT_ERRORS:
			if (hasErrors)
				status = EXIT;
			else
				status = CONTINUE;
			break;
		case SUBMIT_IF_ALL_WITHOUT_ERRORS_AND_WITHOUT_WARNINGS:
			if (hasErrors || hasWarnings)
				status = EXIT;
			else
				status = CONTINUE;
			break;
		case DO_NOT_SUBMIT:
			status = EXIT;
			break;
		default:
			status = CONTINUE;
			break;
		}

		return new FlowExecutionStatus(status);
	}

}
