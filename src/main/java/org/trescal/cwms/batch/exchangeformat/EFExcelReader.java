package org.trescal.cwms.batch.exchangeformat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.batch.core.annotation.BeforeRead;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.config.BatchConfig;
import org.trescal.cwms.batch.exchangeformat.exceptions.EFMissingMandatoryColumnsException;
import org.trescal.cwms.batch.exchangeformat.exceptions.EFOpeningFileException;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;

@Component
@StepScope
public class EFExcelReader<T> extends AbstractItemCountingItemStreamItemReader<T> {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private ExchangeFormatService exchangeFormatService;
	@Autowired
	private ServletContext servletContext;

	protected final Log logger = LogFactory.getLog(getClass());
	protected Resource resource;
	private DataFormatter formatter;
	private Integer efId;
	private Integer efFileId;

	protected ExchangeFormat ef;
	protected ExchangeFormatFile efFile;
	protected XSSFWorkbook workbook;
	protected XSSFSheet sheet;
	protected Integer currentRowIndex;
	protected List<String> headers;
	protected Locale locale;
	@SuppressWarnings("rawtypes")
	protected EFRowMapper rowMapper;
	private boolean alreadyValidatedHeaders = false;

	public EFExcelReader(@Value("#{jobParameters['" + BatchConfig.EF_ID + "']}") Integer efId,
			@Value("#{jobParameters['" + BatchConfig.EF_FILE_ID + "']}") Integer efFileId, EFRowMapper<?, ?> rowMapper,
			@Value("#{jobParameters['" + BatchConfig.LOCALE + "']}") String locale) {
		super();
		this.setName(this.getClass().getCanonicalName());
		this.efId = efId;
		this.efFileId = efFileId;
		this.rowMapper = rowMapper;
		this.formatter = new DataFormatter();
		this.locale = new Locale(locale);
	}

	@PostConstruct
	public void postConstruct() throws IOException {
		this.ef = exchangeFormatService.get(efId);
		File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
		this.efFile = efFileService.get(this.efFileId);
		this.resource = new PathResource(efFileService.getExcelFile(efFile, tempDir).getAbsolutePath());
	}

	@BeforeRead
	public void beforeRead() {
		if (!alreadyValidatedHeaders) {
			validateMandatoryColumns();
			this.alreadyValidatedHeaders = true;
		}
	}

	@Override
	protected void doOpen() throws Exception {

		if (this.ef == null)
			this.ef = exchangeFormatService.get(efId);

		if (!this.resource.exists()) {
			// TODO : translate
			throw new EFOpeningFileException("Input resource must exist : " + this.resource);
		}

		if (!this.resource.isReadable()) {
			// TODO : translate
			throw new EFOpeningFileException("Input resource must be readable : " + this.resource);
		}

		this.workbook = new XSSFWorkbook(this.resource.getInputStream());

		if (this.workbook == null)
			throw new EFOpeningFileException(messageSource.getMessage("exchangeformat.import.validation.workbook.null",
					new String[] {}, locale));

		logger.debug("Opened workbook [" + this.resource.getFilename() + "]");

		this.openSheet();

		this.readHeader();

	}

	private void openSheet() {

		logger.debug("Opening sheet");

		if (StringUtils.isNotBlank(this.ef.getSheetName()))
			this.sheet = this.workbook.getSheet(this.ef.getSheetName());
		else
			this.sheet = this.workbook.getSheetAt(0);

		if (this.sheet == null)
			throw new EFOpeningFileException(messageSource.getMessage("exchangeformat.import.validation.sheet.notfound",
					new String[] { ef.getSheetName() }, locale));

		logger.debug("Openend sheet " + this.sheet.getSheetName());
	}

	private void readHeader() {

		// set currentRowIndex
		this.currentRowIndex = this.ef.getLinesToSkip() == 0 ? 0 : (this.ef.getLinesToSkip() - 1);

		// get header row and
		XSSFRow headerRow = this.sheet.getRow(this.currentRowIndex);

		// read header cells
		this.headers = new ArrayList<>();
		int headerCount = headerRow.getLastCellNum();
		for (int i = 0; i < headerCount; i++) {
			DataFormatter formatter = new DataFormatter();
			String val = formatter.formatCellValue(headerRow.getCell(i));
			this.headers.add(val.trim());
		}

		this.currentRowIndex++;
	}

	protected void validateMandatoryColumns() {
		List<String> missingFileColumns = new ArrayList<String>();
		for (ExchangeFormatFieldNameDetails efColumn : exchangeFormatService.getEfFields(ef.getId())) {
			if (efColumn.isMandatory()) {
				boolean fieldExists = this.headers.stream().anyMatch(c -> {
					if (efColumn.getTemplateName() != null && !StringUtils.isBlank(efColumn.getTemplateName()))
						return c.trim().toLowerCase().equals(efColumn.getTemplateName().trim().toLowerCase());
					else
						return c.trim().toLowerCase().equals(efColumn.getFieldName().getValue().trim().toLowerCase());
				});
				if (!fieldExists) {
					if (efColumn.getTemplateName() != null && !StringUtils.isBlank(efColumn.getTemplateName()))
						missingFileColumns.add(efColumn.getTemplateName());
					else
						missingFileColumns.add(efColumn.getFieldName().getValue());
				}
			}
		}

		if (!missingFileColumns.isEmpty()) {
			throw new EFMissingMandatoryColumnsException(
					messageSource.getMessage("exchangeformat.import.validation.mandatorycolumns.missing",
							new String[] { String.join(",", missingFileColumns) }, locale));
		}
	}

	@Override
	protected T doRead() throws Exception {

		XSSFRow row = sheet.getRow(this.currentRowIndex);

		LinkedCaseInsensitiveMap<String> rowContent = readRowContent(row);

		if (rowContent == null)
			return null;

		@SuppressWarnings("unchecked")
		T item = (T) this.rowMapper.map(rowContent, this.currentRowIndex, this.locale);

		if (item != null)
			this.currentRowIndex++;

		return item;
	}

	protected LinkedCaseInsensitiveMap<String> readRowContent(XSSFRow row) {

		// exit marking the final line if row is null
		if (row == null)
			return null;

		// read row content
		LinkedCaseInsensitiveMap<String> rowContent = new LinkedCaseInsensitiveMap<>();
		for (String colName : headers) {

			int colIndex = headers.indexOf(colName);

			XSSFCell c = row.getCell(colIndex, Row.RETURN_NULL_AND_BLANK);
			String val = formatter.formatCellValue(c);

			rowContent.put(colName, val);
		}

		// check if all read cells are not empty
		boolean allCellsEmpty = rowContent.values().stream().allMatch(s -> {
			return StringUtils.isBlank(s);
		});

		if (allCellsEmpty)
			return null;
		else
			return rowContent;
	}

	@Override
	protected void doClose() throws Exception {
		this.resource.getInputStream().close();
		this.currentRowIndex = 0;
		this.headers = null;
		this.workbook = null;
		this.sheet = null;
	}

}
