package org.trescal.cwms.datatables;

import java.util.List;

/**
 * Class to hold request received from Datatables plugin
 */
public class DatatableInput {

    //The draw number used by Datatbles plugin to make sure the each response relates to the correct request
    private Integer draw;
    //The column metadata received from Datatables plugin
    private List<DatatableInputColumn> columns;
    //The column to order by
    private List<DatatatableOrder> order;
    //The record to start the page with
    private Integer start;
    //The number of records in the page
    private Integer length;
    //Global search term
    private DatatableSearch search;

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public List<DatatableInputColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<DatatableInputColumn> columns) {
        this.columns = columns;
    }

    public List<DatatatableOrder> getOrder() {
        return order;
    }

    public void setOrder(List<DatatatableOrder> order) {
        this.order = order;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public DatatableSearch getSearch() {
        return search;
    }

    public void setSearch(DatatableSearch search) {
        this.search = search;
    }
}
