package org.trescal.cwms.datatables;

/**
 * Class to hold column metadata received from Datatables javascript plugin
 */
public class DatatableInputColumn {
    // The datatbase field to return in this column
    private String data;
    // The name of the field (seems to be only needed if you need to refer to a column client side)
    private String name;
    // Is this column searchable by global search.
    private Boolean searchable;
    // Is this column oderable
    private Boolean orderable;
    // Field specific search term
    private DatatableSearch search;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSearchable() {
        return searchable;
    }

    public void setSearchable(Boolean searchable) {
        this.searchable = searchable;
    }

    public Boolean getOrderable() {
        return orderable;
    }

    public void setOrderable(Boolean orderable) {
        this.orderable = orderable;
    }

    public DatatableSearch getSearch() {
        return search;
    }

    public void setSearch(DatatableSearch search) {
        this.search = search;
    }
}
