package org.trescal.cwms.datatables;

public class DatatatableOrder {

    private Integer column;
    private DatatableSortDirection dir;

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public DatatableSortDirection getDir() {
        return dir;
    }

    public void setDir(DatatableSortDirection dir) {
        this.dir = dir;
    }
}
