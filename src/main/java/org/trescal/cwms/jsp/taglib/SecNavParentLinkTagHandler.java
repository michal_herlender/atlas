package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.trescal.cwms.core.userright.enums.Permission;

public class SecNavParentLinkTagHandler extends BodyTagSupport {

	private static final long serialVersionUID = -5330032187434963578L;

	private boolean hasPermission;
	private String startString;
	private boolean hasChildWithPermission;
	private Permission permission;
	private String message;
	private String classAttr;

	@Override
	public int doStartTag() throws JspException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		hasPermission = auth.getAuthorities().contains(permission);
		startString = "<li";
		if (classAttr != null && classAttr != "")
			startString += " class=\"" + classAttr + "\"";
		startString += "><a href=\"";
		if (hasPermission)
			startString += permission.getUrl() + "\"";
		else
			startString += "\" onclick=\"event.preventDefault();\"";
		startString += ">";
		if (!message.trim().isEmpty())
			startString += message;
		startString += "<span class=\"menuoptions\">&nbsp;</span></a><ul>";
		hasChildWithPermission = false;
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public int doEndTag() throws JspException {
		if (hasChildWithPermission) {
			try {
				JspWriter out = this.pageContext.getOut();
				out.print("</ul></li>");
				return EVAL_PAGE;
			} catch (IOException e) {
				e.printStackTrace();
				return SKIP_BODY;
			}
		}
		return EVAL_PAGE;
	}

	public String getStartString() {
		return startString;
	}

	public void setStartString(String startString) {
		this.startString = startString;
	}

	public boolean isHasChildWithPermission() {
		return hasChildWithPermission;
	}

	public void setHasChildWithPermission(boolean hasChildWithPermission) {
		this.hasChildWithPermission = hasChildWithPermission;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getClassAttr() {
		return classAttr;
	}

	public void setClassAttr(String classAttr) {
		this.classAttr = classAttr;
	}
}