package org.trescal.cwms.jsp.taglib;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.trescal.cwms.spring.helpers.ApplicationContextProvider;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.util.Collection;

@Slf4j
public class CollectionToJsonTagHandler extends SimpleTagSupport {
    @Setter
    private Collection<?> collection;

    public void doTag() throws JspException {
        JspWriter out = this.getJspContext().getOut();
        val om = new ObjectMapper();
        if (collection != null) {
            try {
                log.debug(om.writeValueAsString(collection));
                out.print(CollectionToJsonTagHandler.collectionToJson(collection));
            } catch (Exception e) {
                throw new JspException(e.getMessage(), e);
            }
        }
    }

    public static String collectionToJson(Collection<?> collection){
        return CollectionToJsonTagHandler.objectToJson(collection);
    }

    public static String objectToJson(Object object){
        val om = ApplicationContextProvider.getContext().getBean(MappingJackson2HttpMessageConverter.class).getObjectMapper();
        try {
            return StringEscapeUtils.escapeHtml4(om.writeValueAsString(object));
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
    }
}
