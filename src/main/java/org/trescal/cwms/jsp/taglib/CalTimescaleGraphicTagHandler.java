package org.trescal.cwms.jsp.taglib;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentPercentageService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.spring.helpers.ApplicationContextProvider;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.time.LocalDate;

public class CalTimescaleGraphicTagHandler extends SimpleTagSupport {

	protected final Log logger = LogFactory.getLog(this.getClass());

	private LocalDate calDueDate;

	private Integer calInterval;

	private IntervalUnit calIntervalUnit;

	private String hoverMessageBegining;

	private final LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());



	public void doTag() throws JspException, IOException {

        InstrumentPercentageService instrumentPercentageService = ApplicationContextProvider.getContext().getBean(InstrumentPercentageService.class);

        if (calInterval == null) {
            calInterval = 12;
        }

        if (calIntervalUnit == null) {
            calIntervalUnit = IntervalUnit.MONTH;
        }

        StringBuilder output = new StringBuilder();

		if (calDueDate != null && calInterval > 0) {

			LocalDate nextCalDate = calDueDate;

        	//Make graph
	        if (nextCalDate.isBefore(today)) {
				// No need for calculations; instrument out of calibration.
				output.append("<div class=\"calIndicatorBox\" title='").append(hoverMessageBegining).append(" - 100&#37;'>");
				output.append("<div class=\"outOfCalibration\"></div>");
	        } else {

                int totalCalPercentage = instrumentPercentageService.totalCalibrationPercentage(nextCalDate, calIntervalUnit, calInterval);

				String title = hoverMessageBegining + " - " + totalCalPercentage + "&#37;";

                int lastMonthCalPercentage = instrumentPercentageService.finalMonthPercentage(nextCalDate, calIntervalUnit, calInterval);

                int precedingMonthsCalPercentage = instrumentPercentageService.remainingPercentage(nextCalDate, calIntervalUnit, calInterval);

				output.append("<div class=\"calIndicatorBox\" title='").append(title).append("'>");
				output.append("<div class=\"inCalibration\">");
				output.append("<div class=\"inCalibrationLevel\" style=\" width: ").append(precedingMonthsCalPercentage).append("%; \"></div>");
				output.append("</div>");
				output.append("<div class=\"finalMonthOfCalibration\">");
				output.append("<div class=\"finalMonthOfCalibrationLevel\" style=\" width: ").append(lastMonthCalPercentage).append("%; \"></div>");
				output.append("</div>");
	        }
	        output.append("</div>");

		}

		//Write the output to the page
		PageContext pageContext = (PageContext) getJspContext();
		JspWriter out = pageContext.getOut();
		out.write(output.toString());

	}

	public LocalDate getCalDueDate() {
		return calDueDate;
	}

	public void setCalDueDate(LocalDate calDueDate) {
		this.calDueDate = calDueDate;
	}

	public Integer getCalInterval() {
		return calInterval;
	}

	public void setCalInterval(Integer calInterval) {
		this.calInterval = calInterval;
	}

	public IntervalUnit getCalIntervalUnit() {
		return calIntervalUnit;
	}

	public void setCalIntervalUnit(IntervalUnit calIntervalUnit) {
		this.calIntervalUnit = calIntervalUnit;
	}

	public String getHoverMessageBegining() {
		return hoverMessageBegining;
	}

	public void setHoverMessageBegining(String hoverMessageBegining) {
		this.hoverMessageBegining = hoverMessageBegining;
	}

}
