package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;

import lombok.Setter;
import lombok.val;

public class YesNoTagHandler extends SimpleTagSupport {
    @Setter
    private Boolean flag;


    public void doTag() throws IOException {
        val out = getJspContext().getOut();
        val         servletContext = ((PageContext) getJspContext()).getServletContext();
        val context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        val messageSource = (MessageSource) context.getBean("messageSource");
        val locale = LocaleContextHolder.getLocale();
        out.print(flag != null && flag? messageSource.getMessage("yes",null, locale)
                : messageSource.getMessage("no",null,locale)
                );

    }
}
