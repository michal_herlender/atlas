package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.trescal.cwms.core.userright.enums.Permission;

public class SecNavMainLinkTagHandler extends TagSupport {

	private static final long serialVersionUID = -7325965091653252628L;

	private boolean hasPermission;
	private Permission permission;
	private String classAttr;

	@Override
	public int doStartTag() throws JspException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		// Note that authentication may be null, if session is expired, etc...
		if (auth != null)
			hasPermission = auth.getAuthorities().contains(permission);
		JspWriter out = this.pageContext.getOut();
		try {
			if (hasPermission) {
				out.print("<li");
				if (classAttr != null && classAttr != "")
					out.print(" class=\"" + classAttr + "\"");
				out.print("><a href=\"" + permission.getUrl() + "\">");
				return EVAL_BODY_INCLUDE;
			} else
				return SKIP_BODY;
		} catch (IOException ioEx) {
			throw new JspException(ioEx.getMessage(), ioEx);
		}
	}

	@Override
	public int doEndTag() throws JspException {
		JspWriter out = this.pageContext.getOut();
		try {
			if (hasPermission)
				out.print("</a></li>");
			return EVAL_PAGE;
		} catch (IOException ioEx) {
			throw new JspException(ioEx.getMessage(), ioEx);
		}
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public String getClassAttr() {
		return classAttr;
	}

	public void setClassAttr(String classAttr) {
		this.classAttr = classAttr;
	}
}