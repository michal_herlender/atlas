package org.trescal.cwms.jsp.taglib;

import org.apache.commons.text.StringEscapeUtils;
import org.trescal.cwms.core.system.entity.translation.Translation;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import java.io.IOException;
import java.util.Set;

/*
 * Simple tag that returns best translation from a set of translations
 */
public class BestTranslationTagHandler extends TranslationTagSupport {

	private Boolean escapeJavaScript;
	private Set<Translation> translations;

	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();

		if (this.escapeJavaScript != null && this.escapeJavaScript)
			out.write(StringEscapeUtils.escapeEcmaScript(getBestTranslation(translations)));
		else
			out.write(getBestTranslation(translations));
	}

	public Set<Translation> getTranslations() {
		return translations;
	}

	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}

	public void setEscapeJavaScript(Boolean escapeJavaScript) {
		this.escapeJavaScript = escapeJavaScript;
	}
}
