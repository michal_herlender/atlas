package org.trescal.cwms.jsp.taglib;

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;

import javax.servlet.jsp.JspWriter;
import java.io.IOException;

/**
 *
 */
public abstract class ModelProjectionTagSupport extends TranslationTagSupport {
	private Boolean escapeJavascript;
	private Boolean hideTypology;

	private final static Logger logger = LoggerFactory.getLogger(ModelTagSupport.class);
	
	public Boolean getEscapeJavascript() {
		return escapeJavascript;
	}

	public void setEscapeJavascript(Boolean escapeJavascript) {
		this.escapeJavascript = escapeJavascript;
	}
	public Boolean getHideTypology() {
		return hideTypology;
	}

	public void setHideTypology(Boolean hideTypology) {
		this.hideTypology = hideTypology;
	}
	/**
	 * Writes the model portion of the tag
	 */
	public void writeModel(String modelName, SubfamilyTypology subfamilyTypology) throws IOException {
		JspWriter out = getJspContext().getOut();

		if (modelName == null) {
			logger.warn("instrumentmodel was null");
			out.write("null");
		}
		else {
			
			if (this.escapeJavascript != null && this.escapeJavascript) {
				out.write(StringEscapeUtils.escapeEcmaScript(modelName));
			} else {
				out.write(modelName);
			}
			if ((this.hideTypology == null || !this.hideTypology) &&
				!subfamilyTypology.equals(SubfamilyTypology.UNDEFINED)) {
				// Display subfamily typology of BM or SFC, used everyhwere in internal application that's not shown to customers 
				out.write(" (");
				out.write(subfamilyTypology.toString());
				out.write(")");
			}
		}
	}
}
