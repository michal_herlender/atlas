package org.trescal.cwms.jsp.taglib;

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

import javax.servlet.jsp.JspWriter;
import java.io.IOException;

/**
 * Used for InstrumentModel tag display
 * Takes hideTypology as a parameter (for customer web use only) - default false
 * Takes escapeJavascript as a parameter - default false
 * Galen Beck 2018-10-22
 */
public abstract class ModelTagSupport extends TranslationTagSupport {
	private Boolean escapeJavascript;
	private Boolean hideTypology;

	private final static Logger logger = LoggerFactory.getLogger(ModelTagSupport.class);
	
	public Boolean getEscapeJavascript() {
		return escapeJavascript;
	}

	public void setEscapeJavascript(Boolean escapeJavascript) {
		this.escapeJavascript = escapeJavascript;
	}
	public Boolean getHideTypology() {
		return hideTypology;
	}

	public void setHideTypology(Boolean hideTypology) {
		this.hideTypology = hideTypology;
	}
	/**
	 * Writes the model portion of the tag
	 */
	public void writeModel(InstrumentModel model) throws IOException {
		JspWriter out = getJspContext().getOut();

		if (model == null) {
			logger.warn("instrumentmodel was null");
			out.write("null");
		}
		else {
			String modelName = super.getBestTranslation(model.getNameTranslations());
			
			if (this.escapeJavascript != null && this.escapeJavascript) {
				out.write(StringEscapeUtils.escapeEcmaScript(modelName));
			}
			else {
				out.write(modelName);
			}
			if ((this.hideTypology == null || !this.hideTypology) &&
				!model.getDescription().getSubfamilyTypology().equals(SubfamilyTypology.UNDEFINED)) {
				// Display subfamily typology of BM or SFC, used everyhwere in internal application that's not shown to customers 
				out.write(" (");
				out.write(model.getDescription().getSubfamilyTypology().toString());
				out.write(")");
			}
		}
	}
}
