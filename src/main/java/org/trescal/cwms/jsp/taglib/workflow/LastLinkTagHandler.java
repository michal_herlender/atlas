package org.trescal.cwms.jsp.taglib.workflow;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;

/*
 * Implemented in a TagHandler due to need for exact control over content, e.g no spaces in URL
 */
public class LastLinkTagHandler extends SimpleTagSupport {
	
	private ItemActivity lastActivity;
	private ItemStatus lastStatus;
	private LookupInstance lastLookup;
	private Hook lastHook;
	
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		if (lastActivity != null) {
			out.write("&lastActivityId=");
			out.write(String.valueOf(lastActivity.getStateid()));
		}
		if (lastStatus != null) {
			out.write("&lastStatusId=");
			out.write(String.valueOf(lastStatus.getStateid()));
		}
		if (lastLookup != null) {
			out.write("&lastLookupId=");
			out.write(String.valueOf(lastLookup.getId()));
		}
		if (lastHook != null) {
			out.write("&lastHookId=");
			out.write(String.valueOf(lastHook.getId()));
		}
	}

	public ItemActivity getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(ItemActivity lastActivity) {
		this.lastActivity = lastActivity;
	}

	public ItemStatus getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(ItemStatus lastStatus) {
		this.lastStatus = lastStatus;
	}

	public LookupInstance getLastLookup() {
		return lastLookup;
	}

	public void setLastLookup(LookupInstance lastLookup) {
		this.lastLookup = lastLookup;
	}

	public Hook getLastHook() {
		return lastHook;
	}

	public void setLastHook(Hook lastHook) {
		this.lastHook = lastHook;
	}
}
