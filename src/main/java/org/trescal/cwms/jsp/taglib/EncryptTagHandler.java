package org.trescal.cwms.jsp.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.trescal.cwms.core.tools.EncryptionTools;

public class EncryptTagHandler extends TagSupport {

	private static final long serialVersionUID = 1L;
	private String value;
	
	@Override
	public int doEndTag() throws JspException {
		JspWriter out = this.pageContext.getOut();
		if (value != null) {
			try {
				String encrypted = EncryptionTools.encrypt(value);
				out.print(encrypted);
			} catch (Exception e) {
				throw new JspException(e.getMessage(), e);
			}
		}
		return EVAL_PAGE;
	}

}
