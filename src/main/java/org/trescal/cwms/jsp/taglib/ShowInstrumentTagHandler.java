package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;

/*
 * Mandatory - instrument
 * Takes hideTypology as a parameter (for customer web use only) - default false
 * Takes escapeJavascript as a parameter - default false
 * Galen Beck 2018-10-22 written with typology support,
 * to start to replace all the different model tags we have going...
 */
public class ShowInstrumentTagHandler extends ModelTagSupport {

	private Instrument instrument;

	private final static Logger logger = LoggerFactory.getLogger(ShowInstrumentTagHandler.class);
	
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		if (instrument == null) {
			logger.warn("instrument was null");
			out.write("null");
		}
		else {
			// Note, superclass writes typology
			super.writeModel(instrument.getModel());
			// If the instrument model is a MFR_SPECIFIC model (information already set), 
			// we suppress display of any instrument manufacturer or model 
			if (instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
				if ((instrument.getMfr() != null) && !instrument.getMfr().getGenericMfr()) {
					out.write(" ");
					out.write(instrument.getMfr().getName());
				}
				if ((instrument.getModelname() != null)) {
					out.write(" ");
					out.write(instrument.getModelname());
				}
			}
		}
		
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
	
}
