package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;

/*
 *
 */
public class ShowInstrumentProjectionTagHandler extends ModelProjectionTagSupport {

	private String modelName;
	private SubfamilyTypology subfamilyTypology;
	private ModelMfrType modelMfrType;
	private String mfrName;
	private Boolean mfrGeneric;
	private String instModelName;

	private final static Logger logger = LoggerFactory.getLogger(ShowInstrumentProjectionTagHandler.class);
	
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		if (modelName == null) {
			logger.warn("instrument was null");
			out.write("null");
		}
		else {
			// Note, superclass writes typology
			super.writeModel(modelName, subfamilyTypology);
			// If the instrument model is a MFR_SPECIFIC model (information already set), 
			// we suppress display of any instrument manufacturer or model
			// Note, this accommodates poorly migrated data where the 
			// generic mfr is not set on instrument MFR_GENERIC model.
			if (modelMfrType.equals(ModelMfrType.MFR_GENERIC)) {
				if ((mfrName != null) && (mfrGeneric != null) && !mfrGeneric) {
					out.write(" ");
					out.write(mfrName);
				}
				if ((instModelName != null)) {
					out.write(" ");
					out.write(instModelName);
				}
			}
		}
		
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public SubfamilyTypology getSubfamilyTypology() {
		return subfamilyTypology;
	}

	public void setSubfamilyTypology(SubfamilyTypology subfamilyTypology) {
		this.subfamilyTypology = subfamilyTypology;
	}

	public ModelMfrType getModelMfrType() {
		return modelMfrType;
	}

	public void setModelMfrType(ModelMfrType modelMfrType) {
		this.modelMfrType = modelMfrType;
	}

	public String getMfrName() {
		return mfrName;
	}

	public void setMfrName(String mfrName) {
		this.mfrName = mfrName;
	}

	public Boolean getMfrGeneric() {
		return mfrGeneric;
	}

	public void setMfrGeneric(Boolean mfrGeneric) {
		this.mfrGeneric = mfrGeneric;
	}

	public String getInstModelName() {
		return instModelName;
	}

	public void setInstModelName(String instModelName) {
		this.instModelName = instModelName;
	}

}
