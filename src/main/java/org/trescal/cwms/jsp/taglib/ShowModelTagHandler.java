package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

/*
 * Mandatory - instrumentModel
 * Takes hideTypology as a parameter (for customer web use only) - default false
 * Takes escapeJavascript as a parameter - default false
 * Galen Beck 2018-10-22 rewritten with typology support
 */
public class ShowModelTagHandler extends ModelTagSupport {
	
	private InstrumentModel instrumentmodel;
	
	public void doTag() throws JspException, IOException {
		super.writeModel(instrumentmodel);
	}

	public InstrumentModel getInstrumentmodel() {
		return instrumentmodel;
	}

	public void setInstrumentmodel(InstrumentModel instrumentmodel) {
		this.instrumentmodel = instrumentmodel;
	}

}
