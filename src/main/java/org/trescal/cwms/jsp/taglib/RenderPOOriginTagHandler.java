package org.trescal.cwms.jsp.taglib;

import lombok.Setter;
import lombok.val;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.util.UriComponentsBuilder;
import org.trescal.cwms.core.jobs.job.entity.poorigin.OriginKind;
import org.trescal.cwms.core.jobs.job.entity.poorigin.POOrigin;
import org.trescal.cwms.core.jobs.job.entity.poorigin.POOriginBlank;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class RenderPOOriginTagHandler extends SimpleTagSupport {

    @Setter
    private POOrigin<? extends OriginKind> origin;

    private static final POOrigin<PurchaseOrder> defaultOrigin = new POOriginBlank();

    private POOrigin<? extends OriginKind> getOrigin() {
        return origin != null ? origin : defaultOrigin;
    }

    @Override
    public void doTag() throws JspException, IOException {
        val servletContext = WebApplicationContextUtils.getWebApplicationContext(
                ((PageContext) getJspContext()).getServletContext());
        assert servletContext != null;
        val messageSource = servletContext.getBean(MessageSource.class);
        val writter = getJspContext().getOut();
        val kindName = getOrigin().getKindName();
        writter.print(messageSource.getMessage(kindName.messageCode, null, kindName.defaultMessage, LocaleContextHolder.getLocale()));
        writter.print(" ");
        writter.print(printLinkToOrigin(getOrigin()));
    }

    private String printLinkToOrigin(POOrigin<? extends OriginKind> origin) {

        switch (origin.getKindName()) {
            case JOB:
                return "<a class=\"mainlink\" href=\"" + getUrlToJob(origin.getJob().getJobid()) + "\">" + origin.getJob().getJobno() + "</a>";
            case TP_QUOATATION:
                return "<a class=\"mainlink\" href=\"" + getUrlToTPQuote(origin.getTpQuotation().getId()) + "\">" + origin.getTpQuotation().getQno() + "</a>";
            case COPY_CANCEL:
            case COPY_SIMPLE:
                return "<a class=\"mainlink\" href=\"" + getUrlToPurchaseOrder(origin.getPurchaseOrder().getId()) + "\">" + origin.getPurchaseOrder().getPono() + "</a>";
            case BLANK:
                return "";
        }
        //JAVA cannot check for completness of matching
        return "";
    }

    private String getUrlToTPQuote(int id) {
        val builder = UriComponentsBuilder.fromPath("viewtpquote.htm");
        builder.queryParam("id", id);
        return builder.toUriString();
    }

    private String getUrlToJob(int jobid) {
        val builder = UriComponentsBuilder.fromPath("viewjob.htm");
        builder.queryParam("jobid", jobid);
        return builder.toUriString();
    }

    private String getUrlToPurchaseOrder(int id) {
        val builder = UriComponentsBuilder.fromPath("viewpurchaseorder.htm");
        builder.queryParam("id", id);
        return builder.toUriString();
    }
}
