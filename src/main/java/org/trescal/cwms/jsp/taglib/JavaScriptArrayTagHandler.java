package org.trescal.cwms.jsp.taglib;

import org.apache.commons.text.StringEscapeUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Collection;

/**
 * Galen Beck - 2017-07-18
 * Takes an aribitrary collection of elements and outputs a Javascript escaped array 
 * calling the toString() method on each element
 * (may extend this to use reflection on a property name in future) 
 */
public class JavaScriptArrayTagHandler extends SimpleTagSupport {

	private Collection<Object> elements;
	
	@Override
	public void doTag() throws JspException, IOException {
		PageContext pageContext = (PageContext) getJspContext();
		JspWriter out = pageContext.getOut();
		out.write('[');
		boolean onFirst = true;
		for (Object element : elements) {
			if (!onFirst) out.write(", ");
			onFirst = false;
			out.write('\'');
			out.write(StringEscapeUtils.escapeEcmaScript(element.toString()));
			out.write('\'');
		}
		out.write(']');
	}

	public Collection<Object> getElements() {
		return elements;
	}

	public void setElements(Collection<Object> elements) {
		this.elements = elements;
	}
}
