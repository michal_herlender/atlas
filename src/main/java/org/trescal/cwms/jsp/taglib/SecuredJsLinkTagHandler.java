package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.trescal.cwms.core.userright.enums.Permission;

public class SecuredJsLinkTagHandler extends TagSupport {

	private static final long serialVersionUID = -7173958407189326162L;

	private boolean hasPermission;
	private String permission;
	private String classAttr;
	private String id;
	private boolean isDisplayNone;
	private String onClick;
	private boolean collapse;
	
	@Override
	public int doStartTag() throws JspException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Permission perm = null;
	    for (Permission me : Permission.values()) {
	        if (me.name().equalsIgnoreCase(permission))
	        	perm= me;
	    }
		 if(perm != null)
			 hasPermission = auth.getAuthorities().contains(perm);
		 else 
			 hasPermission = auth.getAuthorities().contains(new SimpleGrantedAuthority(permission));
		

		JspWriter out = this.pageContext.getOut();
		try {
			if (hasPermission) {
				out.print(" <a href=\"#\"");
				if(id!=null)
					out.print(" id=\""+id+"\"");
				if (classAttr != null)
					out.print(" class=\"" + classAttr + "\"");
				if(onClick!=null)
					out.print(" onclick=\""+onClick+"\"");
				out.print(">");
			}
			else {
				out.print("<span class=\""+classAttr + "\"");
				String outStr = "style=\"";
				if(collapse)
					outStr+="visibility: collapse;";
				if(isDisplayNone)
					outStr+="display: none;";
				outStr+="\"";
				if(collapse || isDisplayNone)
				out.print(outStr);
				out.print(">");

			}
			return EVAL_BODY_INCLUDE;
		} catch (IOException ioEx) {
			throw new JspException(ioEx.getMessage(), ioEx);
		}
	}

	@Override
	public int doEndTag() throws JspException {
		JspWriter out = this.pageContext.getOut();
		try {
			if (hasPermission)
				out.print("</a>");
			else
				out.print("</span>");
			return EVAL_PAGE;
		} catch (IOException ioEx) {
			throw new JspException(ioEx.getMessage(), ioEx);
		}
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getClassAttr() {
		return classAttr;
	}

	public void setClassAttr(String classAttr) {
		this.classAttr = classAttr;
	}


	public String getid() {
		return id;
	}

	public void setid(String id) {
		this.id = id;
	}

	public String getonClick() {
		return onClick;
	}

	public void setonClick(String onClick) {
		this.onClick = onClick;
	}

	public boolean isDisplayNone() {
		return isDisplayNone;
	}

	public void setIsDisplayNone(boolean isDisplayNone) {
		this.isDisplayNone = isDisplayNone;
	}

	public boolean isCollapse() {
		return collapse;
	}

	public void setCollapse(boolean collapse) {
		this.collapse = collapse;
	}
	

}