package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.trescal.cwms.core.userright.enums.Permission;

public class SecuredLinkTagHandler extends TagSupport {

	private static final long serialVersionUID = -6970440772949309780L;

	private boolean hasPermission;
	private Permission permission;
	private Boolean authorized;
	private String classAttr;
	private String parameter;
	private String id;
	private String title;
	private boolean collapse;

	@Override
	public int doStartTag() throws JspException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (authorized != null)
			hasPermission = authorized;
		else if (permission != null)
			hasPermission = auth.getAuthorities().contains(permission);
		else
			hasPermission = false;
		JspWriter out = this.pageContext.getOut();
		try {
			if (hasPermission) {
				if (parameter == null)
					out.print("<a href=\"" + permission.getUrl() + "\" ");
				else
					out.print("<a href=\"" + permission.getUrl() + parameter + "\" ");
				if (id != null)
					out.print(" id=\"" + id + "\" ");
				if (classAttr != null)
					out.print(" class=\"" + classAttr + "\" ");
				if (title != null)
					out.print(" title=\"" + title + "\" ");
				out.print(">");
			} else {
				out.print("<span class=\"" + classAttr + "\"");
				if (collapse)
					out.print(" style=\"visibility: collapse\"");
				out.print(">");
			}
			return EVAL_BODY_INCLUDE;
		} catch (IOException ioEx) {
			throw new JspException(ioEx.getMessage(), ioEx);
		}
	}

	@Override
	public int doEndTag() throws JspException {
		JspWriter out = this.pageContext.getOut();
		try {
			if (hasPermission)
				out.print("</a>");
			else
				out.print("</span>");
			return EVAL_PAGE;
		} catch (IOException ioEx) {
			throw new JspException(ioEx.getMessage(), ioEx);
		}
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	/**
	 * @return the authorized
	 */
	public Boolean getAuthorized() {
		return authorized;
	}

	/**
	 * @param authorized the authorized to set
	 */
	public void setAuthorized(Boolean authorized) {
		this.authorized = authorized;
	}

	public String getClassAttr() {
		return classAttr;
	}

	public void setClassAttr(String classAttr) {
		this.classAttr = classAttr;
	}

	public String getid() {
		return id;
	}

	public void setid(String id) {
		this.id = id;
	}

	public String getparameter() {
		return parameter;
	}

	public void setparameter(String parameter) {
		this.parameter = parameter;
	}

	public boolean isCollapse() {
		return collapse;
	}

	public void setCollapse(boolean collapse) {
		this.collapse = collapse;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}