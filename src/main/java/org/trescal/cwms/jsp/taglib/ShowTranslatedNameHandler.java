package org.trescal.cwms.jsp.taglib;

import java.io.IOException;
import java.util.Set;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import org.trescal.cwms.core.system.entity.translation.Translation;

public class ShowTranslatedNameHandler extends TranslationTagSupport {
	private String name;
	private Set<Translation> translations;
	
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		String translatedName = getBestTranslation(translations);
		out.write(translatedName.equals(null) || translatedName.equals("") ? name : translatedName);
		return;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public Set<Translation> getTranslations() {
		return translations;
	}

	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
}
