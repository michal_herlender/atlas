package org.trescal.cwms.jsp.taglib;

import java.util.Locale;
import java.util.Set;

import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.Translation;

/*
 * Abstract base class with common functionality to return best translation from set
 * Galen Beck - 2015-09-18
 */
public abstract class TranslationTagSupport extends SimpleTagSupport {
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	protected String getBestTranslation(Set<Translation> translations) {
		if (translations == null) {
			return "(no translations loaded)";
		}
		
		JspContext jspContext = getJspContext();
		Locale defaultLocale = (Locale) jspContext.getAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE, PageContext.SESSION_SCOPE);
		if (defaultLocale == null) {
			logger.error("No default locale available in session");
		}
		Locale locale = (Locale) jspContext.getAttribute(Constants.SESSION_ATTRIBUTE_LOCALE, PageContext.SESSION_SCOPE);
		if (locale == null) {
			logger.error("No current locale available in session");
		}
		
		String defaultTranslation = "(no default translation)";
		for (Translation translation : translations) {
			if (translation.getLocale().equals(locale)) {
				return translation.getTranslation();
			}
			
			if (translation.getLocale().equals(defaultLocale)) {
				defaultTranslation = translation.getTranslation();
			}
		}
		return defaultTranslation;
	}
}
