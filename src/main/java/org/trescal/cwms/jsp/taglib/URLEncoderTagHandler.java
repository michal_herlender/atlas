package org.trescal.cwms.jsp.taglib;

import java.net.URLEncoder;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class URLEncoderTagHandler extends SimpleTagSupport {
	public final static String ENCODING = "UTF-8";
	private String source;
	private Log logger = LogFactory.getLog(this.getClass());

	@Override
	public void doTag() throws javax.servlet.jsp.JspException ,java.io.IOException {
		PageContext pageContext = (PageContext) getJspContext();
		JspWriter out = pageContext.getOut();
		logger.info(URLEncoder.encode(source, ENCODING));
		out.write(URLEncoder.encode(source, ENCODING));
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
