package org.trescal.cwms.jsp.taglib;

import java.io.IOException;
import java.time.Duration;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class FormatDurationTagHandler extends TagSupport {

	private static final long serialVersionUID = -8115285453161808741L;

	private Duration value;

	@Override
	public int doEndTag() throws JspException {
		JspWriter out = this.pageContext.getOut();
		if (value != null) {
			try {
				out.print("<span ");
				if(value.isNegative())
					out.print("style='color: red'");
				out.print(">");
				out.print(value.toHours() + " h ");
				Long minutes = value.toMinutes();
				if(minutes < 0) minutes = -minutes;
				minutes %= 60;
				if(minutes > 0)
					if(minutes < 10)
						out.print("0" + minutes);
					else
						out.print(minutes);
				else out.print("00");
				out.print("</span>");
			} catch (IOException ioEx) {
				throw new JspException(ioEx.getMessage(), ioEx);
			}
		}
		return EVAL_PAGE;
	}

	public Duration getValue() {
		return value;
	}

	public void setValue(Duration value) {
		this.value = value;
	}
}