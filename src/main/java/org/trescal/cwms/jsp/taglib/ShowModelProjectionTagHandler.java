package org.trescal.cwms.jsp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;

/*
 * Mandatory - instrumentModel
 * Takes hideTypology as a parameter (for customer web use only) - default false
 * Takes escapeJavascript as a parameter - default false
 * Galen Beck 2018-10-22 rewritten with typology support
 */
public class ShowModelProjectionTagHandler extends ModelProjectionTagSupport {
	
	private String modelName;
	private SubfamilyTypology subfamilyTypology;
	
	public void doTag() throws JspException, IOException {
		super.writeModel(modelName, subfamilyTypology);
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public void setSubfamilyTypology(SubfamilyTypology subfamilyTypology) {
		this.subfamilyTypology = subfamilyTypology;
	}

	public SubfamilyTypology getSubfamilyTypology() {
		return subfamilyTypology;
	}
}
