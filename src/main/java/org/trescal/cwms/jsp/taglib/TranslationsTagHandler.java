package org.trescal.cwms.jsp.taglib;

import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.trescal.cwms.core.system.entity.translation.Translation;

public class TranslationsTagHandler extends TagSupport {

	private static final long serialVersionUID = 1L;

	private TreeSet<Translation> translations;
	
	private boolean readonly = false;

	@Override
	public int doStartTag() throws JspException {
		try {
			JspWriter out = pageContext.getOut();
			Locale locale = pageContext.getResponse().getLocale();
			Iterator<Translation> i = translations.iterator();
			int count = 0;
			while (i.hasNext()) {
				Translation translation = i.next();
				out.println("<li>");
				out.println("<label>" + translation.getLocale().getDisplayLanguage(locale) + "("
						+ translation.getLocale().getDisplayCountry(locale) + ")</label>");
				if (!readonly){
					out.println("<input value=\"" + translation.getTranslation() + "\" name=\"translations[" + count
						+ "].translation\" type=\"text\"></input>");
				}else{
					out.println("<input value=\"" + translation.getTranslation() + "\" name=\"translations[" + count
							+ "].translation\" type=\"text\" disabled></input>");
				}
				out.println("</li>");
				count++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return SKIP_BODY;

	}

	public TreeSet<Translation> getTranslations() {
		return translations;
	}

	public void setTranslations(TreeSet<Translation> translations) {
		this.translations = translations;
	}

	public boolean isReadonly() {
		return readonly;
	}

	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}

}
