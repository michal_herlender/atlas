package org.trescal.cwms.xss;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import io.vavr.control.Either;
import lombok.val;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * 2020-01-17 (Galen) : I updated this configuration bean to comment out.
 * One of the side effects with the XSS implementation was that all Jackson deserialization was HTML sanitizing all strings.
 * This would convert JSON inputs like email addresses to html encoded entities and cause validation problems which block the users.
 * The Policy Factory is still defined here as a bean for testing, with the associated filter commented out in web.xml.
 */
@Configuration
public class JacksonConfiguration {


    @Bean
    public MappingJackson2HttpMessageConverter cwmsConverter(StringDeserializer deserializer) {
        val c = new MappingJackson2HttpMessageConverter();
        val module = new SimpleModule("cwms converter");
        module.setMixInAnnotation(Either.Left.class, EitherMixin.Left.class)
            .setMixInAnnotation(Either.Right.class, EitherMixin.Right.class)
            .addSerializer(LocalDate.class, new LocalDateSerializer())
                .addDeserializer(String.class,deserializer);
        c.getObjectMapper().registerModule(module);
        return c;
    }

    @Bean
    public static PolicyFactory policyFactory() {
        return new HtmlPolicyBuilder()
                .allowElements("table","col","th","td","tr","img","span")
                .allowAttributes("src","width","height","border","id","alt").onElements("img").allowUrlProtocols("cid")
                .allowAttributes("style").onElements("span","p","td")
            .allowCommonBlockElements()
            .allowCommonInlineFormattingElements()
            .allowStyling()
            .allowStandardUrlProtocols()
            .toFactory();
    }
}

@Component
@JacksonStdImpl
class LocalDateSerializer extends StdSerializer<LocalDate> {

    protected LocalDateSerializer() {
        super(LocalDate.class);
    }

    @Override
    public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(value.format(DateTimeFormatter.ISO_DATE));
    }
}

@Component
@JacksonStdImpl
class StringDeserializer extends StdDeserializer<String> {

    private static final long serialVersionUID = -7157411427837422745L;

    @Autowired
    StringDeserializer(PolicyFactory policy) {
        this();
        this.policy = policy;
    }

    protected StringDeserializer(Class<String> vc) {
        super(vc);
    }

    protected StringDeserializer() {
        this(String.class);
    }

    private PolicyFactory policy;

    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        val codec = p.getCodec();
        val node = codec.readTree(p);
        if (node instanceof TextNode) {
            val tn = (TextNode) node;
            return   policy.sanitize(tn.textValue()).replaceAll("&#64;","@");
        }
        return "";
    }
}

interface EitherMixin {
    @JsonIgnore
    boolean isAsync();

    @JsonIgnore
    boolean isLazy();

    @JsonIgnore
    boolean isSingleValued();

    @JsonIgnore
    boolean isEmpty();

    interface Right<A, B> extends EitherMixin {
        @JsonIgnore
        A getLeft();

        @JsonProperty("value")
        B getOrNull();
    }

    interface Left<A, B> extends EitherMixin {
        @JsonProperty("value")
        A getLeft();

        @JsonIgnore
        B getOrNull();
    }
}