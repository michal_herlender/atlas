package org.trescal.cwms.xss;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Order(1)
@Slf4j
/*
  Filter for cross-site-scripting and general script injection protection

  A whitelist of safer paths (including but not restricted to Base64 encoded content) is
  maintained in filter initialization params (web.xml file).

  Both the "development" and the "war file" web.xml files should contain identical whitelists:
  - src/main/resources/war/web.xml
  - src/main/webapp/WEB-INF/web.xml

@Component
@Order(1)
@Slf4j
/**
 * Filter for cross-site-scripting and general script injection protection
 * 
 * A whitelist of safer paths (including but not restricted to Base64 encoded
 * content) is maintained in filter initialization params (web.xml file).
 * 
 * Both the "development" and the "war file" web.xml files should contain
 * identical whitelists: - src/main/resources/war/web.xml -
 * src/main/webapp/WEB-INF/web.xml
 *
 * The "trace" or "debug" logging can be turned on in log4j.xml for
 * troubleshooting specific cases.
 *
 * Issues to discuss / resolve 2020-06-18:
 * 
 * (a) Whether /rest should be treated differently than other paths (b) Whether
 * static whiteListedPaths needs to be kept (c) Whether String::contains is
 * necessary or whether we could match on whole path (d) Whether to move to a
 * HashSet (preventing a linear search as size grows)
 * 
 */
public class XssFilter implements Filter {

	private static final String INIT_PARAM_WHITE_LISTED_PATHS = "whiteListedPaths";

	@Override
	public void init(FilterConfig filterConfig) {
		String parameter = filterConfig.getInitParameter(INIT_PARAM_WHITE_LISTED_PATHS);
		List<String> parsedParameter = parameter != null
				? Arrays.stream(parameter.split("\\s")).filter(s -> !s.isEmpty()).collect(Collectors.toList())
				: Collections.emptyList();
		if (log.isDebugEnabled()) 
			log.debug("whitelist via init parameters contains "+parsedParameter.size()+" values");
		if (!parsedParameter.isEmpty())
			this.whiteListedPaths = parsedParameter;
		if (log.isDebugEnabled())
			log.debug("whitelist : "+this.whiteListedPaths);
	}

	private List<String> whiteListedPaths = Stream.of("/downloadfile", "/web/downloadfile", "/adveso/downloadDocument")
			.collect(Collectors.toList());

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String path;
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		if (httpRequest.getPathInfo() != null)
			path = httpRequest.getPathInfo();
		else
			path = httpRequest.getServletPath();

		boolean match = whiteListedPaths.stream().anyMatch(path::contains);
		if (log.isTraceEnabled())
			log.trace("match = " + match + " for path : " + path);
		ServletRequest req = match ? request : new XssRequestWrapper(httpRequest);
		chain.doFilter(req, response);
	}

	@Override
	public void destroy() {

	}
}
