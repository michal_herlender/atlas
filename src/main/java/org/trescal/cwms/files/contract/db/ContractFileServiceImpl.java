package org.trescal.cwms.files.contract.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.files.contract.entity.ContractFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class ContractFileServiceImpl implements ContractFileService {

    @PersistenceContext(unitName="entityManagerFactory2")
    private EntityManager entityManager2;
    @Autowired @Qualifier("transactionManager2")
    private PlatformTransactionManager transactionManager2;

    @Override
    public void addOrUpdateDocument(byte[] data, Contract contract) {

        if(data != null) {

            TransactionStatus ts = transactionManager2.getTransaction(null);

            ContractFile contractFile = contract.getDocument();

            if (contractFile == null) {
                contractFile = new ContractFile();
                contractFile.setContractId(contract.getId());
            }

            contractFile.setFileData(data);

            if (entityManager2.find(ContractFile.class, contract.getId()) == null) entityManager2.persist(contractFile);
            else entityManager2.merge(contractFile);
            transactionManager2.commit(ts);
        }
    }

    @Override
    public ResponseEntity<byte[]> downloadFile(Contract contract){
        if(contract.getDocument() != null && contract.getDocument().getFileData() != null) {
            String filename = contract.getContractNumber() + ".pdf";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.setContentDispositionFormData("attachment", filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            return new ResponseEntity<>(contract.getDocument().getFileData(), headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
