package org.trescal.cwms.files.contract.db;

import org.springframework.http.ResponseEntity;
import org.trescal.cwms.core.contract.entity.contract.Contract;

public interface ContractFileService {

    void addOrUpdateDocument(byte[] data, Contract contract);

    ResponseEntity<byte[]> downloadFile(Contract contract);

}
