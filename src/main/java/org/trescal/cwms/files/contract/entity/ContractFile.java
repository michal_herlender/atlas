package org.trescal.cwms.files.contract.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ContractFile {

    private Integer contractId;
    private byte[] fileData;

    @Id
    @Column(insertable=true, updatable=true, unique=true)
    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    @Column(name="filedata", nullable=true, columnDefinition="varbinary(max)")
    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }



}

