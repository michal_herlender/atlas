package org.trescal.cwms.files.images.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ImageThumbData {

	private Integer imageId;
	private byte[] thumbData;
	
	@Id
	@Column(insertable=true, updatable=true, unique=true)
	public Integer getImageId() {
		return imageId;
	}
	
	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}
	
	@Column(name = "thumbdata", nullable = true, columnDefinition="varbinary(max)")
	public byte[] getThumbData() {
		return thumbData;
	}
	
	public void setThumbData(byte[] thumbData) {
		this.thumbData = thumbData;
	}
}