package org.trescal.cwms.files.images.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ImageFileData {

	private Integer imageId;
	private byte[] fileData;
	
	@Id
	@Column(insertable=true, updatable=true, unique=true)
	public Integer getImageId() {
		return imageId;
	}
	
	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}
	
	@Column(name = "filedata", nullable = true, columnDefinition="varbinary(max)")
	public byte[] getFileData() {
		return fileData;
	}
	
	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}
}