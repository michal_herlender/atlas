package org.trescal.cwms.files.exchangeformat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ExchangeFormatFileData")
public class ExchangeFormatFileData {

	private Integer entityId;
	private byte[] fileData;
	
	@Id
	@Column(name="entityId", insertable=true, updatable=true, unique=true)
	public Integer getEntityId() {
		return entityId;
	}
	
	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}
	
	@Column(name = "filedata", nullable = true, columnDefinition="varbinary(max)")
	public byte[] getFileData() {
		return fileData;
	}
	
	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

}