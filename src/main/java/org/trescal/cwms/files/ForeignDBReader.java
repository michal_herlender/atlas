package org.trescal.cwms.files;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Configurable;

@Aspect
@Configurable
public class ForeignDBReader {

	private static final Logger logger = LoggerFactory.getLogger(ForeignDBReader.class); 
	
	@PersistenceContext(unitName="entityManagerFactory2")
	private EntityManager em;
	
	@Before("execution(* *.*(..)) && @annotation(foreignDBField)")
	public void beforeMethod(JoinPoint jp, ForeignDBField foreignDBField) {
		logger.debug("read object from secondary database");
		Object base = jp.getThis();
		MethodSignature signature = (MethodSignature) jp.getSignature();
		try {
			Class<?> clazz = base.getClass();
			Field fk = null;
			while(clazz != null && fk == null) {
				try {
					fk = clazz.getDeclaredField(foreignDBField.fkField());
				}
				catch (NoSuchFieldException e) {}
				clazz = clazz.getSuperclass();
			}
			fk.setAccessible(true);
			Serializable id = (Serializable) fk.get(base);
			logger.debug("fk " + id);
			if(id != null) {
				PropertyDescriptor pd = BeanUtils.findPropertyForMethod(signature.getMethod());
				Field prop = null;
				logger.debug("pd " + pd.getName());
				clazz = base.getClass();
				while(clazz != null && prop == null) {
					try {
						prop = clazz.getDeclaredField(pd.getName());
					}
					catch (NoSuchFieldException e) {}
					clazz = clazz.getSuperclass();
				}
				logger.debug("prop " + prop);
				prop.setAccessible(true);
				if(prop.get(base) == null) {
					logger.debug("write");
					Method setter = pd.getWriteMethod();
					setter.invoke(base, em.find(setter.getParameterTypes()[0], id));
				}
			}
		} catch (Exception e) {
			for(StackTraceElement ste: e.getStackTrace())
				logger.error(ste.toString());
		}
	}
}