package org.trescal.cwms.rest.common.controller.certificate;

import java.util.Locale;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.RestCommonCertificateDTO;
import org.trescal.cwms.rest.common.dto.output.RestCommonCertificateDTOFactory;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestCommonDownloadCertificateController {

	public final static String REQUEST_MAPPING_SESSION = "/common/certificate/download";
	public final static String REQUEST_MAPPING_API = "/api/certificate/download";

	private static final String REQUEST_PARAM_CERT_ID = "id";

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestCommonCertificateDTOFactory dtoFactory;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING_SESSION, REQUEST_MAPPING_API }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<?> downloadCertificateData(@RequestParam(REQUEST_PARAM_CERT_ID) @NotNull Integer id,
			Locale locale) {

		// get certificate
		Certificate certificate = this.certificateService.findCertificate(id);
		if (certificate == null || certificate.getStatus().equals(CertStatusEnum.DELETED)) {
			String message = messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND, new Object[] { id },
					RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND, locale);
			return new RestResultDTO<RestCommonCertificateDTO>(false, message);
		}

		// output dto
		RestCommonCertificateDTO outputDto = dtoFactory.convertSingle(certificate);

		// get file
		String base64Data = certificateService.readCertificateFileIntoBase64Format(certificate);
		if (StringUtils.isEmpty(base64Data)) {
			String message = messageSource.getMessage(RestErrors.CODE_ERROR_READING_FILE, new Object[] {},
					RestErrors.MESSAGE_ERROR_READING_FILE, locale);
			return new RestResultDTO<RestCommonCertificateDTO>(false, message);
		} else if (base64Data.equals(RestErrors.CODE_FILE_SIZE)){
			String message = messageSource.getMessage(RestErrors.CODE_FILE_SIZE, new Object[] {},
					RestErrors.MESSAGE_FILE_SIZE, locale);
			return new RestResultDTO<RestCommonCertificateDTO>(false, message);
		}
		outputDto.setFile(base64Data);

		// convert
		RestPagedResultDTO<RestCommonCertificateDTO> result = new RestPagedResultDTO<RestCommonCertificateDTO>(true, "",
				1, 1);
		result.setResultsCount(1);
		result.addResult(outputDto);
		return result;
	}

}
