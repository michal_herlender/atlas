package org.trescal.cwms.rest.common.dto.output;

/*
 * Generic result class for paged results.
 * Designed for returning response to ERPProxy where data may be paged 
 * Follows same conventions as PagedResultSet
 * Galen Beck - 2017-03-09
 */
public class RestPagedResultDTO<Result> extends RestResultDTO<Result> {
	private int currentPage;		// Indicates current page of results, minimum 1	
	private int pageCount;  		// Indicates total number of pages, minimum 1 (if 1, not paged)
	private int resultsCount;		// Indicates total count of results available 
	private int resultsPerPage;		// Indicates number of results per page
	private int startResultsFrom;	// Indicates starting record count of this page (e.g. 1, 51, etc...)
	
	public RestPagedResultDTO(boolean success, String message, int resultsPerPage, int currentPage) {
		super(success, message);
		if (resultsPerPage < 1) throw new IllegalArgumentException("resultsPerPage must be at least 1");
		if (currentPage < 1) throw new IllegalArgumentException("currentPage must be at least 1");
		this.resultsPerPage = resultsPerPage;
		this.currentPage = currentPage;
	}
	/*
	 * This sets all the other fields except for resultsPerPage and currentPage,
	 * which are initialized in constructor. Same implementation as PagedResultSet. 
	 */
	public void setResultsCount(int resultsCount) {
		this.resultsCount = resultsCount;
		if (this.resultsCount > 0)
		{
			this.pageCount = ((int) Math.ceil(((double) this.resultsCount) / ((double) this.resultsPerPage)));
		}
		else
		{
			this.pageCount = 1;
		}

		// set the row the results should start from to show the correct results
		// for this page
		this.startResultsFrom = this.resultsPerPage * this.currentPage - this.getResultsPerPage();
	}
	
	public int getCurrentPage() {
		return currentPage;
	}
	public int getPageCount() {
		return pageCount;
	}
	public int getResultsCount() {
		return resultsCount;
	}
	public int getResultsPerPage() {
		return resultsPerPage;
	}
	public int getStartResultsFrom() {
		return startResultsFrom;
	}
}
