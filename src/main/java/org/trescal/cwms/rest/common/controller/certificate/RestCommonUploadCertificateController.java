package org.trescal.cwms.rest.common.controller.certificate;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.component.RestCommonUploadCertificateValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.RestCommonCertificateDTO;
import org.trescal.cwms.rest.common.dto.output.RestCommonCertificateDTOFactory;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestCommonUploadCertificateController {

	public final static String REQUEST_MAPPING = "/common/certificate/client/create";

	private static final String FILE_EXTENSION = ".pdf";

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private RestCommonUploadCertificateValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestCommonCertificateDTOFactory dtoFactory;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<?> uploadCertificate(@RequestBody RestCommonCertificateDTO inputDTO,
			BindingResult bindingResult, Locale locale) {

		// validation
		validator.validate(inputDTO, bindingResult);

		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}

		// create new certificate in service
		Certificate certificate = certificateService.createCommonCertificate(inputDTO, FILE_EXTENSION);

		// convert
		RestPagedResultDTO<RestCommonCertificateDTO> result = new RestPagedResultDTO<>(true, "",
				1, 1);
		result.setResultsCount(1);
		result.addResult(dtoFactory.convertSingle(certificate));
		return result;
	}

}
