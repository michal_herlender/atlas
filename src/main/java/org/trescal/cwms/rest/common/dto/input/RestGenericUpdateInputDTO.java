package org.trescal.cwms.rest.common.dto.input;

import java.util.List;

import org.trescal.cwms.spring.model.KeyValue;

/*
 * Generic DTO class for update transactions
 * Author: Galen Beck - 2017-06-03
 */
public class RestGenericUpdateInputDTO {
	private List<KeyValue<String,?>> input;
	private String locale;
	private List<KeyValue<String,?>> lookup;
	
	public List<KeyValue<String,?>> getInput() {
		return input;
	}
	public String getLocale() {
		return locale;
	}
	public List<KeyValue<String, ?>> getLookup() {
		return lookup;
	}
	public void setInput(List<KeyValue<String,?>> input) {
		this.input = input;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public void setLookup(List<KeyValue<String, ?>> lookup) {
		this.lookup = lookup;
	}
}
