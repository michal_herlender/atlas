package org.trescal.cwms.rest.common.controller.instrument;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.dto.input.RestGenericUpdateInputDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

/**
 * Generic instrument update "prototype" created as design with Lazaro and Gerard
 * Not currently in use but retaining until create / update process is finalized 
 * We might... use well definted DTO objects insetad (more type safety)
 *  
 * @author Galen
 *
 */
@Controller @JsonController
public class RestInstrumentUpdateController {
	public static final String REQUEST_MAPPING = "/instruments/update";
	
	/*
	 * No "result" from update other than success
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET,
		consumes = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<Object> performUpdate(@RequestBody RestGenericUpdateInputDTO inputDto) {
		return new RestResultDTO<Object>(true, "");
	}
	
}
