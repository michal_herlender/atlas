package org.trescal.cwms.rest.common.component;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.RestCommonCertificateDTO;

@Component
public class RestCommonUpdateCertificateValidator extends AbstractBeanValidator {

	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private SubdivService subdivService;

	@Override
	public boolean supports(Class<?> arg0) {
		return RestCommonCertificateDTO.class.equals(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {

		super.validate(target, errors, RestCommonCertificateDTO.updateGroup.class);

		RestCommonCertificateDTO inputDTO = (RestCommonCertificateDTO) target;

		// calibration type
		if (inputDTO.getCalTypeId() != null) {
			CalibrationType calibrationType = calibrationTypeService.find(inputDTO.getCalTypeId());
			if (calibrationType == null)
				errors.rejectValue("calTypeId", RestErrors.CODE_DATA_NOT_FOUND, null,
						RestErrors.MESSAGE_DATA_NOT_FOUND);
		}

		// check if the certificate exists
		Certificate certificate = certificateService.findCertificate(inputDTO.getCertId());
		if (certificate == null) {
			errors.rejectValue("certId", RestErrors.CODE_CERTIFICATE_NOT_FOUND, new Object[] { inputDTO.getCertId() },
					RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND);
		}

		// certificate status
		if (StringUtils.isNotBlank(inputDTO.getCertificateStatus())) {
			try {
				CertStatusEnum.valueOf(inputDTO.getCertificateStatus());
			} catch (Exception e) {
				errors.rejectValue("certificateStatus", RestErrors.CODE_DATA_NOT_FOUND, null,
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		// subDivId (thirsDivId)
		if (inputDTO.getThirdDivId() != null) {
			Subdiv subdiv = subdivService.get(inputDTO.getThirdDivId());
			if (subdiv == null) {
				errors.rejectValue("thirdDivId", RestErrors.CODE_SUBDIV_NOT_FOUND, null,
						RestErrors.MESSAGE_SUBDIV_NOT_FOUND);
			}
		}

	}

}
