package org.trescal.cwms.rest.common.dto.output;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*
 * Generic result class used for performing searches or any activity where a list of results is being returned.
 * Serialized to JSON object by controllers
 * Changes: 
 * 2017-06-02 GB : Added List<RestFieldErrorDto> errors;  
 * 
 * Author: Galen Beck - 2016-01-21
 */
public class RestResultDTO<Result> {
	private boolean success;
	private String message;
	private List<Result> results;
	private List<RestFieldErrorDTO> errors;

	public RestResultDTO() {
		this.results = new ArrayList<Result>();
		this.errors = new ArrayList<RestFieldErrorDTO>();
	}

	public RestResultDTO(boolean success, String message) {
		this.success = success;
		this.message = message;
		this.results = new ArrayList<Result>();
		this.errors = new ArrayList<RestFieldErrorDTO>();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Result> getResults() {
		return results;
	}

	public void addResult(Result result) {
		this.results.add(result);
	}

	public List<RestFieldErrorDTO> getErrors() {
		return errors;
	}

	public void addError(RestFieldErrorDTO error) {
		this.errors.add(error);
	}

	public void addResults(Collection<Result> results) {
		this.results.addAll(results);
	}
}
