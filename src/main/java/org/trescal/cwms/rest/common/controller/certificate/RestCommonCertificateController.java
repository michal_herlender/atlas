package org.trescal.cwms.rest.common.controller.certificate;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.RestCommonCertificateDTO;
import org.trescal.cwms.rest.common.dto.output.RestCommonCertificateDTOFactory;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@JsonController
public class RestCommonCertificateController {

	public final static String REQUEST_MAPPING = "/common/certificate";
	private static final String REQUEST_PARAM_INSTRUMENT_ID = "plantId";
	private static final String REQUEST_PARAM_PAGE = "page";

	private static final Integer RESULTS_PER_PAGE = 25;

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestCommonCertificateDTOFactory dtoFactory;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<RestCommonCertificateDTO> getCertificatesByInstrumentId(
			@RequestParam(value = REQUEST_PARAM_INSTRUMENT_ID, required = true) Integer plantId,
			@RequestParam(value = REQUEST_PARAM_PAGE, required = false, defaultValue = "1") int currentPage,
			Locale locale) {
		// check if instrument exists
		Instrument instrument = instrumentService.get(plantId);
		if (instrument == null) {
			String message = messageSource.getMessage(RestErrors.CODE_INSTRUMENT_NOT_FOUND, new Object[] { plantId },
					RestErrors.MESSAGE_INSTRUMENT_NOT_FOUND, locale);
			return new RestResultDTO<RestCommonCertificateDTO>(false, message);
		}
		// get certificates
		PagedResultSet<Certificate> pagedCertificates = certificateService
				.getAllCertificatesByRelatedInstrument(plantId, RESULTS_PER_PAGE, currentPage);
		// convert them & return
		return dtoFactory.convertResults(pagedCertificates);
	}
}