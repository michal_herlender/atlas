package org.trescal.cwms.rest.common.dto.output;

/**
 * Object to provide back specific field errors for
 * 
 * @author Galen
 *
 */
public class RestFieldErrorDTO {

	private String fieldName;
	private String message;

	public RestFieldErrorDTO() {
	}

	public RestFieldErrorDTO(String fieldName, String message) {
		super();
		this.fieldName = fieldName;
		this.message = message;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getMessage() {
		return message;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
