package org.trescal.cwms.rest.common.repository.instrument;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.rest.alligator.dto.output.RestInstrumentDTO;

@Repository
public class RestInstrumentDaoImpl extends BaseDaoImpl<Instrument, Integer> implements RestInstrumentDao {

	@Override
	protected Class<Instrument> getEntity() {
		return Instrument.class;
	}

	@Override
	public List<RestInstrumentDTO> getInstruments(Integer subdivId) {
		return getResultList(cb -> {
			CriteriaQuery<RestInstrumentDTO> cq = cb.createQuery(RestInstrumentDTO.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			Join<Instrument, Contact> owner = instrument.join(Instrument_.con);
			
			Subquery<Integer> jobItemSq = cq.subquery(Integer.class);
			Root<JobItem> jobItem = jobItemSq.from(JobItem.class);
			
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
			Predicate jobItemClauses = cb.conjunction();
			jobItemClauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.inst), instrument));
			jobItemClauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));
			
			jobItemSq.where(jobItemClauses);
			jobItemSq.select(cb.max(jobItem.get(JobItem_.jobItemId)));
			
			Join<Instrument, Mfr> instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Join<InstrumentModel, Description> subfamily = model.join(InstrumentModel_.description);
			Join<InstrumentModel, Mfr> modelMfr = model.join(InstrumentModel_.mfr, JoinType.LEFT);
			cq.where(cb.equal(owner.get(Contact_.sub), subdivId));
			cq.select(cb.construct(RestInstrumentDTO.class, instrument.get(Instrument_.customerDescription),
					instrument.get(Instrument_.formerBarCode), jobItemSq.getSelection(),
					cb.coalesce(instrumentMfr.get(Mfr_.name), modelMfr.get(Mfr_.name)),
					cb.coalesce(instrument.get(Instrument_.modelname), model.get(InstrumentModel_.model)),
					instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
					instrument.get(Instrument_.serialno), subfamily.get(Description_.description)));
			return cq;
		});
	}
}