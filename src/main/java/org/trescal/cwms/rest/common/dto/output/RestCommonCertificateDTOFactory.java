package org.trescal.cwms.rest.common.dto.output;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_PostTPFurtherWork;
import org.trescal.cwms.rest.common.dto.RestCommonCertificateDTO;

@Component
public class RestCommonCertificateDTOFactory {

	public RestPagedResultDTO<RestCommonCertificateDTO> convertResults(PagedResultSet<Certificate> queryResults) {
		RestPagedResultDTO<RestCommonCertificateDTO> resultDto = new RestPagedResultDTO<>(true, "",
				queryResults.getResultsPerPage(), queryResults.getCurrentPage());
		queryResults.getResults().stream().forEach(queryDto -> resultDto.addResult(this.convertSingle(queryDto)));
		resultDto.setResultsCount(queryResults.getResultsCount());
		return resultDto;
	}

	public RestCommonCertificateDTO convertSingle(Certificate certificate) {
		RestCommonCertificateDTO dto = new RestCommonCertificateDTO();

		dto.setCalibrationDate(certificate.getCalDate());
		dto.setCalVerificationStatus(certificate.getCalibrationVerificationStatus());
		if (certificate.getCal() != null)
			dto.setCalibrationId(certificate.getCal().getId());
		if (certificate.getCalType() != null)
			dto.setCalTypeId(certificate.getCalType().getCalTypeId());
		dto.setCertId(certificate.getCertid());
		dto.setCertificateDate(certificate.getCertDate());
		dto.setCertificateNumber(certificate.getCertno());
		dto.setCertificateType(certificate.getType());
		dto.setDuration(certificate.getDuration());
		dto.setIntervalUnit(certificate.getUnit());
		Integer plantid = null;
		// Certificate converted may be newly created, without all sets initialized -
		// allow null safe conversion
		if (certificate.getLinks() != null && !certificate.getLinks().isEmpty()) {
			JobItem jobItem = certificate.getLinks().iterator().next().getJobItem();
			plantid = jobItem.getInst().getPlantid();
			dto.setJobItemId(jobItem.getJobItemId());
		} else if (certificate.getInstCertLinks() != null && !certificate.getInstCertLinks().isEmpty()) {
			plantid = certificate.getInstCertLinks().iterator().next().getInst().getPlantid();
		}
		dto.setPlantId(plantid);
		dto.setRemarks(certificate.getRemarks());
		if (certificate.getStatus() != null)
			dto.setCertificateStatus(certificate.getStatus().name());
		if (certificate.getSupplementaryFor() != null)
			dto.setSupplementaryForId(certificate.getSupplementaryFor().getCertid());
		dto.setThirdCertificateNumber(certificate.getThirdCertNo());
		if (certificate.getThirdDiv() != null)
			dto.setThirdDivId(certificate.getThirdDiv().getId());
		if (certificate.getThirdOutcome() != null
				&& certificate.getThirdOutcome() instanceof ActionOutcome_PostTPFurtherWork) {
			ActionOutcome_PostTPFurtherWork ao = (ActionOutcome_PostTPFurtherWork) certificate.getThirdOutcome();
			dto.setThirdPartyWorkOutcome(ao.getValue());
		}

		return dto;
	}

}
