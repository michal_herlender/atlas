package org.trescal.cwms.rest.common.controller.certificate;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestFieldErrorDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestCommonUploadCertificateFileController {

	public final static String REQUEST_MAPPING = "/common/certificate/upload/file";
	
	@Autowired
	private CertificateService certificateServ;
	@Value("#{props['cwms.config.filebrowser.maxRestFileUploadSize']}")
	private Integer fileSizeLimit;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<Map<String, Object>> DownloadCertificate(
			@RequestParam(name = "certificateId", required = true) Integer certificateId) {
		RestResultDTO<Map<String, Object>> result = new RestResultDTO<Map<String, Object>>(true, "");
		Certificate cert = certificateServ.findCertificate(certificateId);
		if (cert == null || cert.getStatus().equals(CertStatusEnum.DELETED)) {
			result = new RestResultDTO<Map<String, Object>>(false, "");
			RestFieldErrorDTO errorCert = new RestFieldErrorDTO();
			errorCert.setFieldName("Certificate_Id_Incorrect");
			errorCert.setMessage("Certificate id incorrect");
			result.addError(errorCert);
			return result;
		}
		byte[] filebyte = null;
		File directory = cert.getDirectory();
		String fileName = cert.getCertno() + ".pdf";
		File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));
		if (file.exists())
			try (FileInputStream fis = new FileInputStream(file)) {
				// 20MB max
				if(file.length() > fileSizeLimit){
					RestFieldErrorDTO error = new RestFieldErrorDTO();
					error.setFieldName(fileName);
					error.setMessage(RestErrors.MESSAGE_FILE_SIZE);
					result.addError(error);
					return result;
				}else{
					byte[] fileData = new byte[(int) file.length()];
					IOUtils.read(fis, fileData);
					fis.close();
					// to base64
					filebyte = fileData;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		else {
			RestFieldErrorDTO error = new RestFieldErrorDTO();
			error.setFieldName("File_Unfound");
			error.setMessage("No file found");
			result.addError(error);
			return result;
		}

		Map<String, Object> resultMap = new TreeMap<String, Object>();
		resultMap.put("certificateId", certificateId);
		resultMap.put("filebyte", filebyte);
		result.addResult(resultMap);
		return result;
	}

}
