package org.trescal.cwms.rest.common.service.instrument;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.rest.alligator.dto.output.RestInstrumentDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.common.repository.instrument.RestInstrumentDao;

@Service
public class RestInstrumentServiceImpl implements RestInstrumentService {

	@Autowired
	private RestInstrumentDao restInstrumentDao;

	@Override
	public RestResultDTO<RestInstrumentDTO> getInstruments(Integer subdivId) {
		List<RestInstrumentDTO> instruments = restInstrumentDao.getInstruments(subdivId);
		RestResultDTO<RestInstrumentDTO> result = new RestResultDTO<>(true, "");
		result.addResults(instruments);
		return result;
	}
}