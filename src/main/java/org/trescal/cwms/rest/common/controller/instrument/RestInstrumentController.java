package org.trescal.cwms.rest.common.controller.instrument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.dto.output.RestInstrumentDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.common.service.instrument.RestInstrumentService;

@RestController
@RestJsonController
@RequestMapping("/instruments")
public class RestInstrumentController {

	@Autowired
	private RestInstrumentService restInstrumentService;

	@GetMapping(produces = { Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public RestResultDTO<RestInstrumentDTO> getInstruments(
			@RequestParam(name = "subdivId", required = false) Integer subdivId) {
		return restInstrumentService.getInstruments(subdivId);
	}
}