package org.trescal.cwms.rest.common.component;

import java.util.Base64;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.RestCommonCertificateDTO;

@Component
public class RestCommonUploadCertificateValidator extends AbstractBeanValidator {

	public final static Integer FILE_SIZE_LIMIT = 20971520;// 20MB

	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private SubdivService subdivService;

	@Override
	public boolean supports(Class<?> arg0) {
		return RestCommonCertificateDTO.class.equals(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {

		super.validate(target, errors, RestCommonCertificateDTO.createGroup.class);

		RestCommonCertificateDTO inputDTO = (RestCommonCertificateDTO) target;

		// Mandatory, if null prevent NPE here so that validation error (from annotation) can be returned
		if (inputDTO.getFile() != null) {
			// file size
			byte[] fileData = Base64.getDecoder().decode(inputDTO.getFile());
			if (fileData.length > FILE_SIZE_LIMIT) {
				errors.rejectValue("file", RestErrors.CODE_FILE_SIZE, null, RestErrors.MESSAGE_FILE_SIZE);
			}
		}

		// CERT_INHOUSE is not allowed
		if (inputDTO.getCertificateType() != null
				&& inputDTO.getCertificateType().equals(CertificateType.CERT_INHOUSE)) {
			errors.rejectValue("getCertificateType", RestErrors.CODE_DATA_INVALID,
					new Object[] { inputDTO.getCertificateType() }, RestErrors.MESSAGE_DATA_INVALID);
		}

		// Cal Type : optional
		if (inputDTO.getCalTypeId() != null) {
			CalibrationType calibrationType = calibrationTypeService.find(inputDTO.getCalTypeId());
			if (calibrationType == null)
				errors.rejectValue("calTypeId", RestErrors.CODE_DATA_NOT_FOUND,
						new Object[] { inputDTO.getCalTypeId() }, RestErrors.MESSAGE_DATA_NOT_FOUND);
		}

		// subDivId (thirdDivId) : mandatory for CERT_THIRDPARTY only, optional
		// for CERT_CLIENT
		if (CertificateType.CERT_THIRDPARTY.equals(inputDTO.getCertificateType()) && inputDTO.getThirdDivId() == null) {
			errors.rejectValue("thirdDivId", RestErrors.CODE_DATA_REQUIRED, null, RestErrors.MESSAGE_DATA_REQUIRED);
		} else if (CertificateType.CERT_THIRDPARTY.equals(inputDTO.getCertificateType())
				&& inputDTO.getThirdDivId() != null) {
			Subdiv subdiv = subdivService.get(inputDTO.getThirdDivId());
			if (subdiv == null) {
				errors.rejectValue("thirdDivId", RestErrors.CODE_DATA_NOT_FOUND,
						new Object[] { inputDTO.getThirdDivId() }, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		// lookup hrid
		if (inputDTO.getHrid() != null) {
			Contact c = this.contactService.getByHrId(inputDTO.getHrid());
			if (c == null) {
				errors.rejectValue("hrid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { inputDTO.getHrid() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		// check if the number is not duplicated within the instrument record
		if (inputDTO.getThirdCertificateNumber() != null) {
			List<Certificate> certs = certificateService
					.getByThirdCertNoAndPlantId(inputDTO.getThirdCertificateNumber(), inputDTO.getPlantId());
			if (CollectionUtils.isNotEmpty(certs)) {
				errors.rejectValue("thirdCertificateNumber", RestErrors.CODE_THIRDPARTY_CERT_NO_ALREADY_EXISTS,
						new Object[] { inputDTO.getThirdCertificateNumber() },
						RestErrors.MESSAGE_THIRDPARTY_CERT_NO_ALREADY_EXISTS);
			}
		}

		// if jobItemId is set check if certificateType is THIRDPARTY and the jobItem exists
		if (inputDTO.getJobItemId() != null) {
			if (CertificateType.CERT_CLIENT.equals(inputDTO.getCertificateType()))
				errors.rejectValue("jobItemId", RestErrors.CODE_DATA_INVALID, new Object[] { inputDTO.getJobItemId() },
						RestErrors.MESSAGE_DATA_INVALID);
			else {
				JobItem jobItem = jobItemService.findJobItem(inputDTO.getJobItemId());
				if (jobItem == null)
					errors.rejectValue("jobItemId", RestErrors.CODE_DATA_NOT_FOUND,
							new Object[] { inputDTO.getJobItemId() }, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

	}
}