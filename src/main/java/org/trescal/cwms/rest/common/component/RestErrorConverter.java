package org.trescal.cwms.rest.common.component;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestFieldErrorDTO;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

/**
 * Takes BindingResult and returns a RestResultDTO
 * 
 * @author Galen
 *
 */

@Component
public class RestErrorConverter {
	@Autowired
	private MessageSource messages;

	/**
	 * 
	 * @param bindingResult
	 *            the binding result with at least one global error or at least
	 *            one field error
	 * @return newly created error object
	 */
	public <T> RestPagedResultDTO<T> createResultDTO(BindingResult bindingResult, Locale locale) {
		if (bindingResult == null)
			throw new IllegalArgumentException("No binding result provided");
		if (locale == null)
			throw new IllegalArgumentException("No locale provided");
		if (!bindingResult.hasErrors())
			throw new IllegalArgumentException("Cannot create error result, because binding result has no errors!");
		RestPagedResultDTO<T> dto = new RestPagedResultDTO<T>(false,
				this.messages.getMessage(RestErrors.CODE_ERROR_SAVE, null, RestErrors.MESSAGE_ERROR_SAVE, locale), 1,
				1);
		addGlobalErrors(dto, bindingResult, locale);
		addFieldErrors(dto, bindingResult, locale);
		return dto;
	}

	private void addGlobalErrors(RestResultDTO<?> dto, BindingResult bindingResult, Locale locale) {
		for (ObjectError error : bindingResult.getGlobalErrors()) {
			RestFieldErrorDTO errorDto = new RestFieldErrorDTO();
			errorDto.setFieldName(""); // No field name for global errors
			errorDto.setMessage(
					this.messages.getMessage(error.getCode(), error.getArguments(), error.getDefaultMessage(), locale));
			dto.addError(errorDto);
		}
	}

	private void addFieldErrors(RestResultDTO<?> dto, BindingResult bindingResult, Locale locale) {
		for (FieldError error : bindingResult.getFieldErrors()) {
			RestFieldErrorDTO errorDto = new RestFieldErrorDTO();
			errorDto.setFieldName(error.getField());
			errorDto.setMessage(
					this.messages.getMessage(error.getCode(), error.getArguments(), error.getDefaultMessage(), locale));
			dto.addError(errorDto);
		}

	}
}
