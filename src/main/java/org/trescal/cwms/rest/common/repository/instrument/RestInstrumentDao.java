package org.trescal.cwms.rest.common.repository.instrument;

import java.util.List;

import org.trescal.cwms.rest.alligator.dto.output.RestInstrumentDTO;

public interface RestInstrumentDao {

	List<RestInstrumentDTO> getInstruments(Integer subdivId);
}