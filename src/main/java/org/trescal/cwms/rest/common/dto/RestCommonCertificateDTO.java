package org.trescal.cwms.rest.common.dto;

import lombok.Data;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_PostTPFurtherWork;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
public class RestCommonCertificateDTO {

	private Integer calibrationId;
	@NotNull(groups = createGroup.class)
	private Date calibrationDate;
	private Integer calTypeId;
	@NotNull(groups = createGroup.class)
	private Date certificateDate;
	@NotNull(groups = updateGroup.class)
	private Integer certId;
	@Size(max = 60)
	private String certificateNumber;
	private Integer duration;
	private IntervalUnit intervalUnit;
	private String certificateStatus;
	private Integer supplementaryForId;
	@NotNull(groups = createGroup.class)
	private CertificateType certificateType;
	@NotNull(groups = createGroup.class)
	private Integer plantId;
	@Size(max = 1000)
	private String remarks;
	@NotNull(groups = createGroup.class)
	private String file;
	/* third party fields */
	private ActionOutcomeValue_PostTPFurtherWork thirdPartyWorkOutcome;
	private Integer jobItemId;
	@Size(max = 30)
	@NotNull(groups = createGroup.class)
	private String thirdCertificateNumber;
	private CalibrationVerificationStatus calVerificationStatus;
	private Integer thirdDivId;
	private String hrid;
	private Integer timeSpent;
	private Date startDate;
	private Boolean optimization;
	private Boolean restriction;
	private Boolean adjustment;
	private Boolean repair;
	private Integer workRequirementId;

	// create = upload certificate
	public interface createGroup {
	}

	public interface updateGroup {
	}

}
