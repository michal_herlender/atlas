package org.trescal.cwms.rest.common.service.instrument;

import org.trescal.cwms.rest.alligator.dto.output.RestInstrumentDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

public interface RestInstrumentService {

	RestResultDTO<RestInstrumentDTO> getInstruments(Integer subdivId);
}