package org.trescal.cwms.rest.alligator.dto.output;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestCertificateInstrumentDTO {
    private int plantID;
    private int jobItemID;
    private int jobItemNumber;
    private String jobItemClientReference;		// Added 2019-12-19
    private String subFamily;
    private String serialNumber; 
    private String plantNumber; 
    private String formerBarcode;
    private String customerDescription; 
    private String manufacturer;
    private String model;
}