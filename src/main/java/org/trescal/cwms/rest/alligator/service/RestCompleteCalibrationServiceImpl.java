package org.trescal.cwms.rest.alligator.service;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleInterval;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateCertificate;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateCertificate.Input;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationCancelInputDTO;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationCompleteInputDTO;

/**
 * Logic taken mostly from "original" RestCompleteCalibrationController as used by Alligator;
 * but moved to service layer and improved implementation (separated validator) 
 * @author galen
 *
 */
@Service
public class RestCompleteCalibrationServiceImpl implements RestCompleteCalibrationService {
	
	@Autowired
	private ActionOutcomeService aoService;
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private RecallRuleService recallRuleService;
	@Autowired
	private HookInterceptor_CreateCertificate hookInterceptor_CreateCertificate;

	@Override
	public Calibration getCalibrationInStatus(List<JobItem> jobItems, String... statusNames) {
		// TODO currently returns first calibration - support multiple cals / check all job items?
		List<String> statusNamesList = Arrays.asList(statusNames);
		
		Calibration result = null;
		for (JobItem jobItem : jobItems) {
			for (CalLink calLink : jobItem.getCalLinks()) {
				Calibration calibration = calLink.getCal();
				if (statusNamesList.contains(calibration.getStatus().getName())) {
					result = calibration;
					break;
				}
			}
		}
		return result;
	}

	private List<JobItem> getJobItems(List<Integer> jobItemIds) {
		List<JobItem> result = new ArrayList<>();
		for (Integer jobItemId : jobItemIds) {
			JobItem jobItem = this.jobItemService.get(jobItemId);
			// Previously checked to exist via validator
			result.add(jobItem);
		}
		return result;
	}
	
	/**
	 * Method for newer "jobitem" based API
	 * 
	 * Input DTO must be previously validated (successfully) with related validator
	 */
	@Override
	public void completeCalibration(RestApiCalibrationCompleteInputDTO inputDto) {
		
		List<JobItem> jobItems = getJobItems(inputDto.getJobItemIds());
		Calibration calibration = getCalibrationInStatus(jobItems, CalibrationStatus.ON_GOING);
		Set<CalLink> calLinks = calibration.getLinks();
		Contact completedBy = this.contactService.getContactOrLoggedInContact(inputDto.getContactId());
		
		ActionOutcome ao = this.aoService.getByTypeAndValue(ActionOutcomeType.CALIBRATION, inputDto.getActionOutcome());

		Map<Integer, Integer> timeSpents = getTimeSpentMap(calLinks, inputDto.getWorkingTimes());
		Map<Integer, Integer> calLinkOutcomes = getCalLinkOutcomeMap(calLinks, ao);
		String remark = inputDto.getRemarks() != null ? inputDto.getRemarks() : "";
		List<Integer> includeOnCert = getIncudeOnCertList(calLinks);
		RecallRuleInterval certDuration = getCertDuration(calLinks);
		
		// Optional inputs
		if (inputDto.getCalibrationDate() != null) {
			calibration.setCalDate(inputDto.getCalibrationDate());
		}
		if (inputDto.getStartTime() != null) {
			Date startTime = Date.from(inputDto.getStartTime().atZoneSameInstant(ZoneId.systemDefault()).toInstant());
			calibration.setStartTime(startTime);
		}
		
		this.calibrationService.completeCalibration(calibration, completedBy, timeSpents, calLinkOutcomes,
				remark, true, includeOnCert, certDuration.getInterval(), certDuration.getUnit());

		// Optional input - override after completion
		if (inputDto.getCompleteTime() != null) {
			Date completeTime = Date.from(inputDto.getCompleteTime().atZoneSameInstant(ZoneId.systemDefault()).toInstant());
			calibration.setCompleteTime(completeTime);
		}
		
		// Certificate will always exist
		Certificate certificate = calibration.getCerts().get(0);
		
		// Step 10 - Finally run "CreateCertificate" HookInterceptor, as it could
		// not be run during Pre-Calibration status (TODO revisit and create at start?)
		for (CertLink certLink : certificate.getLinks()) {
			this.hookInterceptor_CreateCertificate.recordAction(
				new Input(certLink, HookInterceptor_CreateCertificate.CERTIFICATE_DEFAULT_TIME_SPENT));
		}
	}
	
	/**
	 * Method for Alligator "certificate" based API; some updates still made in controller 
	 */
	@Override
	public void completeCalibrationFromCertificate(Certificate certificate, Map<Integer, Integer> mapJobItemIdsToTimes, ActionOutcome ao, Contact completedBy) {
		Set<CalLink> calLinks = certificate.getCal().getLinks();
		
		// Step 8 - Assemble arguments for calling existing "complete
		// calibration" service method
		Map<Integer, Integer> timeSpents = getTimeSpentMap(calLinks, mapJobItemIdsToTimes);
		Map<Integer, Integer> calLinkOutcomes = getCalLinkOutcomeMap(calLinks, ao);
		String remark = "";
		List<Integer> includeOnCert = getIncudeOnCertList(calLinks);
		RecallRuleInterval certDuration = getCertDuration(calLinks);

		// Step 9 - complete calibration
		this.calibrationService.completeCalibration(certificate.getCal(), completedBy, timeSpents, calLinkOutcomes,
			remark, true, includeOnCert, certDuration.getInterval(), certDuration.getUnit());

		// Step 10 - Finally run "CreateCertificate" HookInterceptor, as it could
		// not be run during Pre-Calibration status
		for (CertLink certLink : certificate.getLinks()) {
			this.hookInterceptor_CreateCertificate.recordAction(
				new Input(certLink, HookInterceptor_CreateCertificate.CERTIFICATE_DEFAULT_TIME_SPENT));
		}
	}
	
	private Map<Integer, Integer> getTimeSpentMap(Set<CalLink> calLinks, Map<Integer, Integer> mapJobItemIdsToTimes) {
		Map<Integer, Integer> calLinksToTimes = new HashMap<>();
		for (CalLink calLink : calLinks) {
			Integer time = 0;
			if (mapJobItemIdsToTimes != null)
				time = mapJobItemIdsToTimes.getOrDefault(calLink.getJi().getJobItemId(), 0);
			calLinksToTimes.put(calLink.getId(), time);
		}
		return calLinksToTimes;
	}

	/*
	 * At present we presume that all cal links have the same outcome
	 */
	private Map<Integer, Integer> getCalLinkOutcomeMap(Set<CalLink> calLinks, ActionOutcome actionOutcome) {
		Map<Integer, Integer> result = new HashMap<>();
		for (CalLink calLink : calLinks) {
			result.put(calLink.getId(), actionOutcome.getId());
		}
		return result;
	}

	/*
	 * At present we presume that all cal links are included on cert
	 */
	private List<Integer> getIncudeOnCertList(Set<CalLink> calLinks) {
		List<Integer> result = new ArrayList<>();
		for (CalLink calLink : calLinks) {
			result.add(calLink.getId());
		}
		return result;
	}

	/*
	 * Obtained from first linked instrument
	 */
	private RecallRuleInterval getCertDuration(Set<CalLink> calLinks) {
		CalLink firstCalLink = calLinks.iterator().next();
		return this.recallRuleService.getCurrentRecallInterval(firstCalLink.getJi().getInst(),
				RecallRequirementType.FULL_CALIBRATION, null);
	}

	@Override
	public void cancelCalibration(RestApiCalibrationCancelInputDTO inputDto) {
		
		List<JobItem> jobItems = getJobItems(inputDto.getJobItemIds());
		Contact completedBy = this.contactService.getContactOrLoggedInContact(inputDto.getContactId()); 
		Calibration calibration = getCalibrationInStatus(jobItems, CalibrationStatus.AWAITING_WORK, CalibrationStatus.ON_GOING);
		
		// cancel calibration
		calibrationService.cancelCalibration(calibration.getId(), completedBy);

		calibration.setCompletedBy(completedBy);
		calibration.setCompleteTime(new Date());

		// cancel linked certificates
		calibration.getCerts().forEach(cert -> {
			cert.setStatus(CertStatusEnum.CANCELLED);
			if (inputDto.getRemarks() != null)
				cert.setRemarks(inputDto.getRemarks());
		});
	}	
}
