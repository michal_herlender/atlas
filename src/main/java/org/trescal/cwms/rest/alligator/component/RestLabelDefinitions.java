package org.trescal.cwms.rest.alligator.component;

public class RestLabelDefinitions {
	// Standard field and value names for label template
	public static String FIELD_1 = "field1";
	public static String FIELD_2 = "field2";
	public static String FIELD_3 = "field3";
	public static String FIELD_4 = "field4";
	public static String FIELD_5 = "field5";
	public static String FIELD_6 = "field6";
	public static String[] FIELDS = new String[] {FIELD_1, FIELD_2, FIELD_3, FIELD_4, FIELD_5, FIELD_6}; 
	
	public static String VALUE_1 = "value1";
	public static String VALUE_2 = "value2";
	public static String VALUE_3 = "value3";
	public static String VALUE_4 = "value4";
	public static String VALUE_5 = "value5";
	public static String VALUE_6 = "value6";
	public static String[] VALUES = new String[] {VALUE_1, VALUE_2, VALUE_3, VALUE_4, VALUE_5, VALUE_6}; 
	
	public static String FOOTER = "footer";
	public static String ACCRED = "accred";
	public static String BARCODE = "barcode";
	
	// Message keys
	public static String KEY_COMMON_CAL_DATE = "alligator.label.common.caldate";
	public static String KEY_COMMON_NEXT_CAL = "alligator.label.common.nextcal";
	public static String KEY_COMMON_SERIAL_NO = "alligator.label.common.serialno";
	public static String KEY_COMMON_CUSTOMER_ID = "alligator.label.common.customerid";

	public static String KEY_CAL_CERT_NO = "alligator.label.cal.certno";
	public static String KEY_CAL_PLANT_ID = "alligator.label.cal.plantid";
	public static String KEY_CAL_TECHNICIAN = "alligator.label.cal.technician";
	
	public static String KEY_TP_TP_CERT_NO = "alligator.label.tp.tpcertno";
	public static String KEY_TP_OUR_CERT_NO = "alligator.label.tp.ourcertno";
	public static String KEY_TP_THIRD_PARTY = "alligator.label.tp.thirdparty";	// Currently unused

	// Default message in case translation not loaded
	public static String MESSAGE_COMMON_CAL_DATE = "Date Cal";
	public static String MESSAGE_COMMON_NEXT_CAL = "Next Cal";
	public static String MESSAGE_COMMON_SERIAL_NO = "Serial No";
	public static String MESSAGE_COMMON_CUSTOMER_ID = "Cust ID";
	public static String MESSAGE_COMMON_TECHNICIAN = "Technician";

	public static String MESSAGE_CAL_CERT_NO = "Cert No";
	public static String MESSAGE_CAL_PLANT_ID = "Trescal ID";
	public static String MESSAGE_CAL_TECHNICIAN = "Technician";

	public static String MESSAGE_TP_TP_CERT_NO = "TP Cert";
	public static String MESSAGE_TP_OUR_CERT_NO = "Cert No";
	public static String MESSAGE_TP_THIRD_PARTY = "Third Party";
}
