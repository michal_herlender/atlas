package org.trescal.cwms.rest.alligator.component;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.misc.entity.numberformat.db.NumberFormatService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelParameter;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplate;
import org.trescal.cwms.core.tools.labelprinting.alligator.xml.XmlAlligatorLabel;
import org.trescal.cwms.core.tools.labelprinting.alligator.xml.XmlAlligatorParameterNameValue;

import java.util.ArrayList;
import java.util.Locale;

@Component
@Slf4j
public class AlligatorLabelFormatterFixed extends AlligatorLabelFormatter {
    @Autowired
    private NumberFormatService numberFormatService;

    /**
     * Assembles label parameters based on the fixed definitions in the template
     *
     * @param accreditedLab - optional
     */
    public XmlAlligatorLabel getLabelDto(AlligatorLabelTemplate template, Certificate certificate, Instrument instrument,
                                         AccreditedLab accreditedLab, Locale locale, int copies) {
        XmlAlligatorLabel label = new XmlAlligatorLabel();
        label.setName(template.getDescription());
        label.setQuantity(copies);
        label.setParameters(new ArrayList<>());

        for (AlligatorLabelParameter templateParam : template.getParameters()) {
            String name = templateParam.getName();
            String value = null;

            switch (templateParam.getValue()) {
                case CERT_ACCREDITATION_NUMBER:
                    if (accreditedLab != null)
                        value = accreditedLab.getLabNo();
                    break;
                case CERT_CAL_DATE:
                    value = super.formatDate(certificate.getCalDate(), templateParam.getFormat(), locale);
                    break;
                case INST_DUE_DATE:
                    value = super.formatLocalDate(instrument.getNextCalDueDate(), templateParam.getFormat(), locale);
                    break;
                case CERT_NUMBER:
                    value = certificate.getCertno();
                    break;
                case CERT_NUMBER_LEFT_PART:
                    value = getCertNoParts(certificate).getLeft();
                    break;
                case CERT_NUMBER_RIGHT_PART:
                    value = getCertNoParts(certificate).getRight();
                    break;
			case CERT_NUMBER_RIGHT_PART_TRIMMED:
				value = getCertNoRightPartTrimmed(certificate);
				break;
			case CERT_TECHNCIAN_NAME:
				if ((certificate.getCal() != null) && certificate.getCal().getCompletedBy() != null) {
					value = certificate.getCal().getCompletedBy().getName();
				}
				break;
			case INST_CLIENT_ID:
				value = instrument.getPlantno();
				break;
			case INST_SERIAL_NUMBER:
				value = instrument.getSerialno();
				break;
			case INST_TRESCAL_ID:
				value = String.valueOf(instrument.getPlantid());
				break;
			case TEXT:
				value = templateParam.getText();
				break;
			case TP_CERT_NUMBER:
				value = certificate.getCertno();
				break;

                default:
                    break;
            }
            if (value == null)
                value = "";
            XmlAlligatorParameterNameValue labelParam = new XmlAlligatorParameterNameValue(name, value);
            label.getParameters().add(labelParam);
        }

        return label;
    }


    private Pair<String, String> getCertNoParts(Certificate certificate) {
        // Note, will work best when there is a cal attached, to determine the company, otherwise assumes default format
        Pair<String, String> result;
        if (certificate.getType().equals(CertificateType.CERT_INHOUSE)) {
            Company company = null;
            Subdiv subdiv = null;
            if (certificate.getCal() != null) {
                company = certificate.getCal().getOrganisation().getComp();
                subdiv = certificate.getCal().getOrganisation();
            }
            result = this.numberFormatService.splitNumber(certificate.getCertno(), NumerationType.CERTIFICATE, company, subdiv);
        } else if (certificate.getType().equals(CertificateType.CERT_CLIENT)) {
			result = this.numberFormatService.splitNumber(certificate.getCertno(), NumerationType.CLIENT_CERTIFICATE, null, null);
		}
		else if (certificate.getType().equals(CertificateType.CERT_THIRDPARTY)) {
			result = this.numberFormatService.splitNumber(certificate.getCertno(), NumerationType.TP_CERTIFICATE, null, null);
        } else {
            log.error("Unexpected cert type " + certificate.getType());
            result = new ImmutablePair<>("", "");
		}
		return result;
	}
	
	public String getCertNoRightPartTrimmed(Certificate certificate) {
		String result = "";
		Pair<String,String> parts = getCertNoParts(certificate);
		String right = parts.getRight();
		// Note, assumes 2 digit year prefix, revisit if needed
		try {
			String yearRemoved = right.substring(2);
			result = Integer.valueOf(yearRemoved).toString();
		}
		catch (Exception e) {
            log.error("Error trimming right part : " + right, e);
        }
		return result;
	}
}
