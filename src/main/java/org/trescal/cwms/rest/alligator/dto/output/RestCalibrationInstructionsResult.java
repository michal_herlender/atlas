package org.trescal.cwms.rest.alligator.dto.output;


public class RestCalibrationInstructionsResult {
	private Boolean globalInstruction;
	private Integer workRequirementId;
	private String text;
	private String description;		// Description of the instrument, when there is a work requirement
	private String procedures;		// Description of the procedure, when there is a work requirement
	public Boolean getGlobalInstruction() {
		return globalInstruction;
	}
	public Integer getWorkRequirementId() {
		return workRequirementId;
	}
	public String getText() {
		return text;
	}
	public void setGlobalInstruction(Boolean globalInstruction) {
		this.globalInstruction = globalInstruction;
	}
	public void setWorkRequirementId(Integer workRequirementId) {
		this.workRequirementId = workRequirementId;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getDescription() {
		return description;
	}
	public String getProcedures() {
		return procedures;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setProcedures(String procedures) {
		this.procedures = procedures;
	}
}
