package org.trescal.cwms.rest.alligator.dto.output;

public class RestAlligatorSettingsDto {
	private String excelLocalTempFolder;
    private Boolean excelSaveTempCopy;
    private Boolean excelRelativeTempFolder;
    private String uploadTempDataFolder;
    private String uploadRawDataFolder;
    private Boolean uploadEnabled;
    private Boolean uploadMoveUponUpload;
    private Boolean uploadUseWindowsCredentials;
    private String uploadWindowsUsername;
    private String uploadWindowsDomain;
    private String uploadWindowsPassword;
    private String description;
    
	public String getExcelLocalTempFolder() {
		return excelLocalTempFolder;
	}
	public Boolean getExcelSaveTempCopy() {
		return excelSaveTempCopy;
	}
	public Boolean getExcelRelativeTempFolder() {
		return excelRelativeTempFolder;
	}
	public String getUploadTempDataFolder() {
		return uploadTempDataFolder;
	}
	public String getUploadRawDataFolder() {
		return uploadRawDataFolder;
	}
	public Boolean getUploadEnabled() {
		return uploadEnabled;
	}
	public Boolean getUploadMoveUponUpload() {
		return uploadMoveUponUpload;
	}
	public Boolean getUploadUseWindowsCredentials() {
		return uploadUseWindowsCredentials;
	}
	public String getUploadWindowsUsername() {
		return uploadWindowsUsername;
	}
	public String getUploadWindowsDomain() {
		return uploadWindowsDomain;
	}
	public String getUploadWindowsPassword() {
		return uploadWindowsPassword;
	}
	public void setExcelLocalTempFolder(String excelLocalTempFolder) {
		this.excelLocalTempFolder = excelLocalTempFolder;
	}
	public void setExcelSaveTempCopy(Boolean excelSaveTempCopy) {
		this.excelSaveTempCopy = excelSaveTempCopy;
	}
	public void setExcelRelativeTempFolder(Boolean excelRelativeTempFolder) {
		this.excelRelativeTempFolder = excelRelativeTempFolder;
	}
	public void setUploadTempDataFolder(String uploadTempDataFolder) {
		this.uploadTempDataFolder = uploadTempDataFolder;
	}
	public void setUploadRawDataFolder(String uploadRawDataFolder) {
		this.uploadRawDataFolder = uploadRawDataFolder;
	}
	public void setUploadEnabled(Boolean uploadEnabled) {
		this.uploadEnabled = uploadEnabled;
	}
	public void setUploadMoveUponUpload(Boolean uploadMoveUponUpload) {
		this.uploadMoveUponUpload = uploadMoveUponUpload;
	}
	public void setUploadUseWindowsCredentials(Boolean uploadUseWindowsCredentials) {
		this.uploadUseWindowsCredentials = uploadUseWindowsCredentials;
	}
	public void setUploadWindowsUsername(String uploadWindowsUsername) {
		this.uploadWindowsUsername = uploadWindowsUsername;
	}
	public void setUploadWindowsDomain(String uploadWindowsDomain) {
		this.uploadWindowsDomain = uploadWindowsDomain;
	}
	public void setUploadWindowsPassword(String uploadWindowsPassword) {
		this.uploadWindowsPassword = uploadWindowsPassword;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
