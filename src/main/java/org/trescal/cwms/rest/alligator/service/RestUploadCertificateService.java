package org.trescal.cwms.rest.alligator.service;

import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.rest.alligator.dto.input.RestUploadCertificateDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

public interface RestUploadCertificateService {
	
	RestResultDTO<?> uploadCertificate(Certificate certificate, RestUploadCertificateDTO inputDTO, String username);
}
