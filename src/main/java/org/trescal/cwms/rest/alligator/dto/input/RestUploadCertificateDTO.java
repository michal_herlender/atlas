package org.trescal.cwms.rest.alligator.dto.input;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.spring.jackson.LocaleToZonedDateTimeConverter;

@Getter @Setter
public class RestUploadCertificateDTO {
	// Only the first three fields are mandatory
	@NotNull
	private String certificateNumber;
	@NotNull
	private String content;
	@NotNull
	private String fileName;

	// All remaining fields are optional
    @JsonFormat(pattern = "yyyy-MM-dd['T'HH:mm:ss.SSS][XXX]")
    private LocalDate issueDate;
    private String thirdCertificateNumber;
	private String hrid;
	private Integer timeSpent;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	@JsonDeserialize(converter = LocaleToZonedDateTimeConverter.class)
	private ZonedDateTime startDate;

	private Boolean optimization;
	private Boolean restriction;
	private Boolean adjustment;
	private Boolean repair;
	private String supplementaryFor;
}
