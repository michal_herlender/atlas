package org.trescal.cwms.rest.alligator.dto.output;

import java.util.Locale;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;

public interface RestWorkRequirementDTOFactory {
	RestWorkRequirementDTO convert(JobItemWorkRequirement jobItemWorkReq, Contact contact, Locale locale);
	public String getDescription(JobItemWorkRequirement jobItemWorkReq);
	public String getProcedures(JobItemWorkRequirement jobItemWorkReq);
	RestWorkRequirementDTO convert(JobItemWorkRequirement jobItemWorkReq, Locale locale);
}
