package org.trescal.cwms.rest.alligator.component;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

import java.util.*;

/*
 * Service class containing specific logic for starting / stopping calibrations.
 */
@Service
public class RestStartCalibrationValidatorImpl implements RestStartCalibrationValidator {

    @Autowired
    private JobItemService jobItemService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CapabilityAuthorizationService capabilityAuthorizationService;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private JobItemActionService jiActionService;

    /*
     * To be able to start a calibration for a specific work requirement, the
     * following conditions must be satisfied:
	 * 
	 * a) The Job Item must have a "state group" of StateGroup.AWAITING_CALIBRATION
	 * 
	 * b) The Work Requirement must have a procedure associated (work instruction
	 * not supported yet)
	 * 
	 * c) The work requirement must not be complete
	 * 
	 * d) The user must be authorized for the associated procedure(s) against the
	 * job item's cal type
	 * 
	 * e) The the job item calibration type must have a recall requirement type of
	 * FULL_CALIBRATION
	 * 
	 * TODO determine what to do about job items that are
	 * "ready to resume calibration" - separate?
	 */
	@Override
	public boolean isCalibrationStartable(JobItemWorkRequirement jobItemWorkReq, Contact contact, boolean bypassAuthorisation) {
        if (!isCalibrationStartable(jobItemWorkReq))
            return false;
        // Check authorization
        return bypassAuthorisation || isCalibrationAuthorized(jobItemWorkReq, contact);
    }

	@Override
	public boolean isCalibrationStartable(JobItemWorkRequirement jobItemWorkReq) {
        if (jobItemWorkReq.getWorkRequirement().getServiceType() == null)
            return false;
        if (jobItemWorkReq.getWorkRequirement().getServiceType().getCalibrationType() == null)
            return false;
        if (!jobItemWorkReq.getWorkRequirement().getServiceType().getCalibrationType().getRecallRequirementType()
            .equals(RecallRequirementType.FULL_CALIBRATION))
            return false;
        if (!jobItemService.isReadyForCalibration(jobItemWorkReq.getJobitem())
            && !jobItemService.isReadyToResumeCalibration(jobItemWorkReq.getJobitem()))
            return false;
        if (jobItemWorkReq.getWorkRequirement().getCapability() == null)
            return false;
        if (jobItemWorkReq.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE) ||
            jobItemWorkReq.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED))
            return false;
        if (jobItemWorkReq.getJobitem().getCurrentAddr() == null) {
            // to deal with certificate cancel and replace case
            JobItemAction lastJia = this.jiActionService.findLastActiveActionForItem(jobItemWorkReq.getJobitem());
            boolean condition6 = jobItemService.stateHasGroupOfKeyName(lastJia.getStartStatus(),
                StateGroup.START_LAB_CERTIFICATE_REPLACEMENT);
            boolean condition7 = jobItemService.stateHasGroupOfKeyName(lastJia.getStartStatus(),
                StateGroup.START_ONSITE_CERTIFICATE_REPLACEMENT);
            return condition6 || condition7;
        }
		return true;
	}

	public boolean isCalibrationAuthorized(JobItemWorkRequirement jobItemWorkReq, Contact contact) {
        CalibrationType calType = jobItemWorkReq.getWorkRequirement().getServiceType().getCalibrationType();
        Capability capability = jobItemWorkReq.getWorkRequirement().getCapability();
        if (capability != null) {
            val status = this.capabilityAuthorizationService.authorizedFor(capability.getId(),
                calType.getCalTypeId(), contact.getPersonid());
            return status.isRight();
        }
        return true;
    }

	@Override
	public boolean isCalibrationStarted(JobItemWorkRequirement jobItemWorkReq) {
        if (!jobItemService.isInCalibration(jobItemWorkReq.getJobitem()))
            return false;
        if (jobItemWorkReq.getJobitem().getCalLinks().isEmpty())
            return false;
        Iterator<CalLink> calLinks = jobItemWorkReq.getJobitem().getCalLinks().iterator();
        CalLink calLink = null;
        while (calLinks.hasNext())
            calLink = calLinks.next();
        return !calLink.getCal().getCerts().isEmpty();
    }

	/*
	 * All of the individual work requirements must be startable, and the job items
	 * must all be on the same job.
	 */
	@Override
	public boolean isCalibrationStartableAsGroup(Collection<JobItemWorkRequirement> jobItemWorkReqs, Contact contact, boolean bypassAuthorisation) {
		boolean result = true;
		for (JobItemWorkRequirement jobItemWorkReq : jobItemWorkReqs) {
			if (!isCalibrationStartable(jobItemWorkReq, contact, bypassAuthorisation))
				result = false;
		}
		if (getUniqueJobCount(jobItemWorkReqs) != 1)
			result = false;
		if (getUniqueCalibrationFrequencyCount(jobItemWorkReqs) != 1)
			return false;

		return result;
	}

	/*
	 * Indicates if calibration is already in progress
	 */
	@Override
	public boolean isCalibrationStartedAsGroup(Collection<JobItemWorkRequirement> jobItemWorkReqs) {
		boolean result = true;
		for (JobItemWorkRequirement jobItemWorkReq : jobItemWorkReqs) {
			if (!isCalibrationStarted(jobItemWorkReq))
				result = false;
		}
		if (getUniqueJobCount(jobItemWorkReqs) != 1)
			result = false;
		if (getUniqueCalibrationFrequencyCount(jobItemWorkReqs) != 1)
			return false;

		return result;
	}

	@Override
	public int getUniqueJobCount(Collection<JobItemWorkRequirement> jobItemWorkReqs) {
        Set<Integer> jobIds = new HashSet<>();
		for (JobItemWorkRequirement jobItemWorkReq : jobItemWorkReqs) {
			jobIds.add(jobItemWorkReq.getJobitem().getJob().getJobid());
		}
		return jobIds.size();
	}

	private int getUniqueCalibrationFrequencyCount(Collection<JobItemWorkRequirement> jobItemWorkReqs) {
        Set<Integer> calibrationFrequencies = new HashSet<>();
		for (JobItemWorkRequirement jobItemWorkReq : jobItemWorkReqs) {
			Integer calFreq = jobItemWorkReq.getJobitem().getInst().getCalFrequency();
			calibrationFrequencies.add(calFreq);
		}
		return calibrationFrequencies.size();
	}

	@Override
	public String getNotStartableReasons(JobItemWorkRequirement jobItemWorkReq, Contact contact, boolean bypassAuthorisation, Locale locale) {
        StringBuilder result = new StringBuilder();
        ServiceType serviceType = jobItemWorkReq.getWorkRequirement().getServiceType();
		CalibrationType calType = serviceType != null ? serviceType.getCalibrationType() : null;

		if (serviceType == null) {
			String message = this.messageSource.getMessage(RestErrors.CODE_WR_NO_SERVICE_TYPE,
                new Integer[]{jobItemWorkReq.getId()}, RestErrors.MESSAGE_WR_NO_SERVICE_TYPE, locale);
            result.append(message);
        }

        if (serviceType != null && ((serviceType.getCalibrationType() == null)
            || !jobItemWorkReq.getWorkRequirement().getServiceType().getCalibrationType().getRecallRequirementType()
            .equals(RecallRequirementType.FULL_CALIBRATION))) {
            if (result.length() > 0)
                result.append("; ");
            String message = this.messageSource.getMessage(RestErrors.CODE_CAL_NOT_FULL_CALIBRATION,
                new Integer[]{jobItemWorkReq.getId()}, RestErrors.MESSAGE_CAL_NOT_FULL_CALIBRATION, locale);
            result.append(message);
        }
        if (!jobItemService.isReadyForCalibration(jobItemWorkReq.getJobitem())) {
            if (result.length() > 0)
                result.append("; ");
            String message = this.messageSource.getMessage(RestErrors.CODE_WR_NOT_AWAITING_CALIBATION,
                new Integer[]{jobItemWorkReq.getId()}, RestErrors.MESSAGE_WR_NOT_AWAITING_CALIBATION, locale);
            result.append(message);
        }
        Capability capability = jobItemWorkReq.getWorkRequirement().getCapability();
        if (capability == null) {
            if (result.length() > 0)
                result.append("; ");
            String message = this.messageSource.getMessage(RestErrors.CODE_WR_NO_PROCEDURE,
                new Integer[]{jobItemWorkReq.getId()}, RestErrors.MESSAGE_WR_NO_PROCEDURE, locale);
            result.append(message);
        }
        if (jobItemWorkReq.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)) {
            if (result.length() > 0)
                result.append("; ");
            String message = this.messageSource.getMessage(RestErrors.CODE_WR_COMPLETE,
                new Integer[]{jobItemWorkReq.getId()}, RestErrors.MESSAGE_WR_COMPLETE, locale);
            result.append(message);
        }
        if (jobItemWorkReq.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED)) {
            if (result.length() > 0)
                result.append("; ");
            String message = this.messageSource.getMessage(RestErrors.CODE_WR_CANCELLED,
                new Integer[]{jobItemWorkReq.getId()}, RestErrors.MESSAGE_WR_CANCELLED, locale);
            result.append(message);
        }
        if (!bypassAuthorisation && capability != null && calType != null) {
            val status = this.capabilityAuthorizationService.authorizedFor(capability.getId(),
                calType.getCalTypeId(), contact.getPersonid()).isRight();
            if (!status) {
                String calTypeShortName = this.translationService.getCorrectTranslation(
                    jobItemWorkReq.getWorkRequirement().getServiceType().getShortnameTranslation(), locale);
                String procedureReference = capability.getReference();
                Object[] errorArgs = new Object[]{contact.getName(), procedureReference, calTypeShortName};
                if (result.length() > 0)
                    result.append("; ");
                String message = this.messageSource.getMessage(RestErrors.CODE_CAL_NOT_AUTHORIZED, errorArgs,
                    RestErrors.MESSAGE_CAL_NOT_AUTHORIZED, locale);
                result.append(message);
            }
        }
        if (jobItemWorkReq.getJobitem().getCurrentAddr() == null) {
            String jobItemNo = jobItemWorkReq.getJobitem().getJob().getJobno() + "." +
                jobItemWorkReq.getJobitem().getItemNo();
            if (result.length() > 0)
                result.append("; ");
            String message = this.messageSource.getMessage(RestErrors.CODE_JOBITEM_CURRENT_ADDRESS_NULL,
                new String[]{jobItemNo}, RestErrors.MESSAGE_JOBITEM_CURRENT_ADDRESS_NULL, locale);
			result.append(message);
		}
		return result.toString();
	}

	@Override
	public String getNotStartableReasons(Collection<JobItemWorkRequirement> jobItemWorkReqs, Contact contact,
			boolean bypassAuthorisation, Locale locale) {
        StringBuilder result = new StringBuilder();
		for (JobItemWorkRequirement jobItemWorkRequirement : jobItemWorkReqs) {
			String message = getNotStartableReasons(jobItemWorkRequirement, contact, bypassAuthorisation, locale);
			if ((result.length() > 0) && (message.length() > 0))
				result.append("; ");
			result.append(message);
		}
		if (getUniqueJobCount(jobItemWorkReqs) != 1) {
			String message = this.messageSource.getMessage(RestErrors.CODE_WR_DIFFERENT_JOBS, null,
					RestErrors.MESSAGE_WR_DIFFERENT_JOBS, locale);
			if ((result.length() > 0))
				result.append("; ");
			result.append(message);
		}
		if (getUniqueCalibrationFrequencyCount(jobItemWorkReqs) != 1) {
			String message = this.messageSource.getMessage(RestErrors.CODE_WR_DIFFERENT_CAL_FREQ, null,
					RestErrors.MESSAGE_WR_DIFFERENT_CAL_FREQ, locale);
			if ((result.length() > 0))
				result.append("; ");
			result.append(message);
		}

		return result.toString();
	}

}
