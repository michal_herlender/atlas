package org.trescal.cwms.rest.alligator.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationToCalLinkComparator;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationStartInputDTO;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationStartOutputDTO;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static org.trescal.cwms.core.tools.DateTools.dateToLocalDate;

/*
 * This is a standalone service that creates a calibration and certificate for a set of work requirements.
 * Desired for this to be an atomic operation used only for web services so it is created in its own service class.
 */

@Service
public class RestStartCalibrationServiceImpl implements RestStartCalibrationService {
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private CalibrationProcessService calibrationProcessService;
	@Autowired
	private JobItemService jobItemService; 
	@Autowired
	private ContactService contactService;

	@Override
	public Calibration startCalibration(Collection<JobItemWorkRequirement> jobItemWorkRequirements, Contact startedBy,
										Date startDate, Integer calProcessId, Integer calClassId) {

        // Get parameters from first job item & instrument
        JobItemWorkRequirement firstJiwr = jobItemWorkRequirements.iterator().next();
        // cal frequency = duration (all previously validated to be on same cal
        // frequency)
        JobItem firstJobItem = firstJiwr.getJobitem();
        int duration = firstJobItem.getInst().getCalFrequency();
        IntervalUnit durationUInit = firstJobItem.getInst().getCalFrequencyUnit();
        CalibrationProcess calProcess;
        if (calProcessId == null)
            calProcess = this.calibrationProcessService.findByName("Excel");
        else
            calProcess = this.calibrationProcessService.get(calProcessId);
        // Previously validated to have valid cal type
        CalibrationType calType = firstJiwr.getWorkRequirement().getServiceType().getCalibrationType();
        Capability capability = firstJiwr.getWorkRequirement().getCapability();
        Subdiv subdiv = capability.getOrganisation();

        boolean signCert = false;
        Calibration calibration = new Calibration();
        calibration.setOrganisation(subdiv);
        calibration.setCalAddress(firstJobItem.getCalAddr());
        if (calClassId == null)
            calibration.setCalClass(CalibrationClass.AS_FOUND_AND_AS_LEFT);
        else
            calibration.setCalClass(CalibrationClass.values()[calClassId]);
        calibration.setCalProcess(calProcess);
        calibration.setCalType(calType);
        calibration.setCalDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        calibration.setCapability(capability);
        // If the work requirement has a work instruction associated
        if (firstJiwr.getWorkRequirement().getWorkInstruction() != null) {
            WorkInstruction workInstruction = firstJiwr.getWorkRequirement().getWorkInstruction();
            calibration.setWorkInstruction(workInstruction);
        }
        Certificate reservedCertificate = null;

        // Create CalLinks linking JobItem(s) and Calibration, also search for
        // reserved certificate
        Set<CalLink> calLinks = new TreeSet<>(new CalibrationToCalLinkComparator());
		for (JobItemWorkRequirement jobItemWorkRequirement : jobItemWorkRequirements) {
			JobItem jobItem = jobItemWorkRequirement.getJobitem();
			Certificate certificate = this.certificateService.getReservedCertificate(jobItem);
			if (certificate != null)
				reservedCertificate = certificate;

			CalLink calLink = new CalLink();
			calLink.setCal(calibration);
			calLink.setJi(jobItem);
			calLink.setMainItem(jobItem.getInst().getModel().getModelType().getModules());
			calLinks.add(calLink);
		}
		calibration.setLinks(calLinks);

		// If there is a reserved certificate, use it; otherwise create a new
		// certificate.
		if (reservedCertificate != null) {
			calibration.setCalDate(dateToLocalDate(reservedCertificate.getCalDate()));
		} else {
			calibration.setCalDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		}

		this.calibrationService.insertAndStartCalibration(calibration, startedBy, startDate, null);

		// If there is a reserved certificate, use it; otherwise create a new
		// certificate.
		if (reservedCertificate != null) {
			reservedCertificate.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
			reservedCertificate.setRegisteredBy(startedBy);
			this.certificateService.linkCertToCal(reservedCertificate, calibration);
		} else {
			this.certificateService.createCertFromCal(startedBy, calibration, duration, signCert, subdiv, durationUInit,
					CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE, null);
		}
		return calibration;
	}
	
	/**
	 * Delegates to logic in above startCalibration(...) method but with job item id inputs
	 * 
	 * RestApiCalibrationInputDTO inputDto must have been previously validated
	 * 
	 */
	public RestApiCalibrationStartOutputDTO startCalibration(RestApiCalibrationStartInputDTO inputDto) {
		// Optimization of job item lookup for multiple job items possible here, typically one job item
		
		List<JobItemWorkRequirement> jiwrs = getJobItemWorkRequirements(inputDto.getJobItemIds());
		Contact startedBy = this.contactService.getContactOrLoggedInContact(inputDto.getContactId());
		// remaining fields are optional (eventually pass ZonedDateTime through)
		Date startTime = inputDto.getStartTime() != null ? Date.from(inputDto.getStartTime().atZoneSameInstant(ZoneId.systemDefault()).toInstant()) : null;
		Integer calProcessId = getCalProcessId(inputDto.getProcessName());
		Integer calClassId = inputDto.getDataContent() != null ? inputDto.getDataContent().ordinal() : null;
		
		Calibration calibration = this.startCalibration(jiwrs, startedBy, startTime, calProcessId, calClassId);
		RestApiCalibrationStartOutputDTO outputDto = new RestApiCalibrationStartOutputDTO();
		outputDto.setCalibrationId(calibration.getId());
		
		// Certificate will always exist after web service calibration start
		Certificate cert = calibration.getCerts().get(0);
		outputDto.setDocumentNumber(cert.getCertno());
		outputDto.setCertificateId(cert.getCertid());
		return outputDto;
	}
	
	private Integer getCalProcessId(String processName) {
		Integer result = null;
		if (processName != null) {
			result = this.calibrationProcessService.findByName(processName).getId();
		}
		return result;
	}
	
	private List<JobItemWorkRequirement> getJobItemWorkRequirements(List<Integer> jobItemIds) {
		List<JobItemWorkRequirement> jiwrs = new ArrayList<>();
		for (Integer jobItemId : jobItemIds) {
			JobItem jobItem = this.jobItemService.get(jobItemId);
			// Previously checked to exist and be startable via validator
			jiwrs.add(jobItem.getNextWorkReq());
		}
		return jiwrs;
	}
	
}