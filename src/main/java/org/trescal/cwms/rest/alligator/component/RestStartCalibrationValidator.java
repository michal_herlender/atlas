package org.trescal.cwms.rest.alligator.component;

import java.util.Collection;
import java.util.Locale;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;

public interface RestStartCalibrationValidator {
	int getUniqueJobCount(Collection<JobItemWorkRequirement> jobItemWorkReqs);
	boolean isCalibrationStarted(JobItemWorkRequirement jobItemWorkReq);
	boolean isCalibrationStartable(JobItemWorkRequirement jobItemWorkReq, Contact contact, boolean bypassAuthorisation);
	boolean isCalibrationStartableAsGroup(Collection<JobItemWorkRequirement> jobItemWorkReqs, Contact contact, boolean bypassAuthorisation);
	boolean isCalibrationStartedAsGroup(Collection<JobItemWorkRequirement> jobItemWorkReqs);
	String getNotStartableReasons(JobItemWorkRequirement jobItemWorkReq, Contact contact, boolean bypassAuthorisation, Locale locale);
	String getNotStartableReasons(Collection<JobItemWorkRequirement> jobItemWorkReqs, Contact contact, boolean bypassAuthorisation, Locale locale);
	boolean isCalibrationStartable(JobItemWorkRequirement jobItemWorkReq);
}
