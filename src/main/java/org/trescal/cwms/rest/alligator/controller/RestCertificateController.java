package org.trescal.cwms.rest.alligator.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.dto.output.RestCertificateDataDTO;
import org.trescal.cwms.rest.alligator.dto.output.RestCertificateDataDTOFactory;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

/**
 * REST web service controller that returns Certificate information either for:
 * 
 * (a) the document specified (one certificate), or 
 * (b) the job item specified (may be 0 to n certifiates)
 */ 
@Controller @RestJsonController
public class RestCertificateController {
	public static final String REQUEST_MAPPING = "/certificate";
	public static final String REQUEST_PARAM_DOCUMENT_NUMBER = "documentnumber";
	public static final String REQUEST_PARAM_JOB_ITEM = "jobitemid";

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private MessageSource messageSource; 
	@Autowired
	private RestCertificateDataDTOFactory dtoFactory;
	
	/**
	 * Method (original) for getting single certificate data
	 * 
	 * @param locale
	 * @param documentNumber
	 * @return DTO for JSON conversion with certificate data 
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, params={REQUEST_PARAM_DOCUMENT_NUMBER}, method = RequestMethod.GET,
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestCertificateDataDTO> getCertificateData(Locale locale,
			@RequestParam(value=REQUEST_PARAM_DOCUMENT_NUMBER) String documentNumber) {
		Certificate certificate = this.certificateService.findCertificateByCertNo(documentNumber);
		if (certificate == null) {
			String message = this.messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND, new Object[] {documentNumber}, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND, locale); 
			return new RestResultDTO<RestCertificateDataDTO>(false, message);
		}
		if (!this.dtoFactory.canConvert(certificate)) {
			String message = this.dtoFactory.getConversionErrorMessage(certificate, locale);
			return new RestResultDTO<RestCertificateDataDTO>(false, message);
		}
		RestResultDTO<RestCertificateDataDTO> result = new RestResultDTO<RestCertificateDataDTO>(true,"");
		result.addResult(dtoFactory.convert(certificate, locale));
		return result;
	}
	
	/**
	 * Method (new) for getting data of any & all certificates connected to job item
	 * 
	 * @param locale
	 * @param jobitemid
	 * @return DTO for JSON conversion with certificate data 
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, params={REQUEST_PARAM_JOB_ITEM}, method = RequestMethod.GET,
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestCertificateDataDTO> getJobItemData(Locale locale,
			@RequestParam(value=REQUEST_PARAM_JOB_ITEM) int jobItemId) {
		JobItem jobItem = this.jobItemService.findEagerJobItem(jobItemId);
		if (jobItem == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, null, RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<RestCertificateDataDTO>(false, message);
		}
		RestResultDTO<RestCertificateDataDTO> result = new RestResultDTO<RestCertificateDataDTO>(true,"");
		for (CertLink certLink : jobItem.getCertLinks()) {
			result.addResult(dtoFactory.convert(certLink.getCert(), locale));
		}
		return result;
	}
}
