package org.trescal.cwms.rest.alligator.component;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationStartInputDTO;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationStartOutputDTO;

import java.util.Collection;
import java.util.Date;

public interface RestStartCalibrationService {

    Calibration startCalibration(Collection<JobItemWorkRequirement> jobItemWorkRequirements, Contact startedBy, Date startDate, Integer calProcessId, Integer calClassId);

    RestApiCalibrationStartOutputDTO startCalibration(RestApiCalibrationStartInputDTO inputDto);
}
