package org.trescal.cwms.rest.alligator.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.RestXmlController;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.db.CertLinkService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.labelprinter.enums.AlligatorLabelType;
import org.trescal.cwms.core.tools.labelprinting.alligator.xml.XmlAlligatorPrintJob;
import org.trescal.cwms.rest.alligator.component.RestLabelPrintJobService;

@Controller @RestXmlController
@SessionAttributes(names={Constants.SESSION_ATTRIBUTE_USERNAME})
public class RestLabelController {
	public static final String REQUEST_MAPPING = "/alligator/label";
	public static final String REQUEST_PARAM_CERTIFICATE = "certificate";
	public static final String REQUEST_PARAM_TYPE = "type";
	public static final String REQUEST_PARAM_COPIES = "copies";
	
	public static final String DEFAULT_TYPE = "REGULAR";
	public static final String DEFAULT_COPIES = "1";
	
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private CertLinkService certLinkService;
	@Autowired
	private RestLabelPrintJobService printJobService;
	@Autowired
	private UserService userService;
	@Value("${cwms.config.printing.certificate.label.allinstruments}")
	private boolean printAllInstruments;
	@Value("${cwms.config.printing.certificate.label.certlinkbased}")
	private boolean labelCertLinkBased;
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET,
		produces = {Constants.MEDIATYPE_APPLICATION_XML_UTF8, Constants.MEDIATYPE_TEXT_XML_UTF8})
	public XmlAlligatorPrintJob getLabel(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(name=REQUEST_PARAM_CERTIFICATE, required=true) List<String> certnos,
			@RequestParam(name=REQUEST_PARAM_COPIES, required=false, defaultValue=DEFAULT_COPIES) Integer copies, 
			@RequestParam(name=REQUEST_PARAM_TYPE, required=false, defaultValue=DEFAULT_TYPE) AlligatorLabelType type) {
		XmlAlligatorPrintJob result = null;

		Contact contact = this.userService.get(username).getCon();
		// Use the contact's current locale, as it may change via regular interface after Alligator login
		Locale locale = contact.getLocale();
		if (contact.getUserPreferences().getLabelPrinter() == null) {
			// TODO localisation of error message
			result = new XmlAlligatorPrintJob(false, "No label printer set in ERP");
		}
		else {
			LabelPrinter labelPrinter = contact.getUserPreferences().getLabelPrinter();
			
			if (labelCertLinkBased) {
				Map<String, CertLink> certLinkMap = findCertLinks(certnos);
				String certLinksMissing = checkCertLinksMissing(certLinkMap);
				if (!certLinksMissing.isEmpty()) {
					result = new XmlAlligatorPrintJob(false, certLinksMissing);
				}
				else {
					result = this.printJobService.getPrintJob(certLinkMap.values(), copies, type, labelPrinter, locale);
				}
			}
			else {
				Map<String, Certificate> certs = findCertificates(certnos);
				String certsMissing = checkCertsMissing(certs);
				if (!certsMissing.isEmpty()) {
					result = new XmlAlligatorPrintJob(false, certsMissing);
				}
				else {
					result = this.printJobService.getPrintJob(certs.values(), copies, type, labelPrinter, locale, printAllInstruments);
				}
			}
		}
		return result;
	}
	
	/**
	 * For use when certlink ids are passed to reference combination of instrument and certificate 
	 */
	private Map<String, CertLink> findCertLinks(List<String> certlinkList) {
		Map<String, CertLink> result = new TreeMap<>();
		for (String certlinkText : certlinkList) {
			Integer linkId = Integer.parseInt(certlinkText);
			result.put(certlinkText, this.certLinkService.findCertLink(linkId));
		}
		
		return result;
	}
	
	/**
	 * For use when certlink ids are passed to reference combination of instrument and certificate 
	 */
	private String checkCertLinksMissing(Map<String, CertLink> certLinkMap) {
		StringBuffer result = new StringBuffer();
		for (Map.Entry<String, CertLink> entry : certLinkMap.entrySet()) {
			if (entry.getValue() == null) {
				if (result.length() == 0)
					result.append("Certlink id(s) not found : ");
				else 
					result.append(", ");
				result.append(entry.getKey());
			}
		}
		return result.toString();
	}
	
	
	private Map<String, Certificate> findCertificates(List<String> certnos) {
		Map<String, Certificate> result = new TreeMap<>();
		for (String certno : certnos) {
			Certificate certificate = this.certificateService.findCertificateByCertNo(certno);
			result.put(certno, certificate);
		}
		return result;
	}
	
	private String checkCertsMissing(Map<String, Certificate> certs) {
		StringBuffer result = new StringBuffer();
		for (String certNo : certs.keySet()) {
			Certificate cert = certs.get(certNo);
			if (cert == null) {
				if (result.length() > 0) result.append(", ");
				result.append("Not found : ");
				result.append(certNo);
			}
			else if (!cert.getType().equals(CertificateType.CERT_INHOUSE) &&
					!cert.getType().equals(CertificateType.CERT_THIRDPARTY)) {
				result.append("Invalid certificate type : ");
				result.append(certNo);
			}
		}
		return result.toString();
	}
}