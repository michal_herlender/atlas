package org.trescal.cwms.rest.alligator.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.dto.output.RestCertificateDataDTO;
import org.trescal.cwms.rest.alligator.dto.output.RestCertificateDataDTOFactory;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.util.Date;
import java.util.Locale;

@Controller
@JsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class RestSupplementaryCertificateController {

	public final static String REQUEST_MAPPING = "/alligator/certificate/supplementary";
	public static final String REQUEST_PARAM_DOCUMENT_NUMBER = "documentnumber";
	public static final String REQUEST_PARAM_HR_ID = "hrid";
	public static final String REQUEST_PARAM_START_DATE = "startDate";

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestCertificateDataDTOFactory dtoFactory;
	@Autowired
	private ContactService contactService;
	@Autowired
	private UserService userService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, params = {
			REQUEST_PARAM_DOCUMENT_NUMBER }, method = RequestMethod.POST, produces = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<RestCertificateDataDTO> createSupplementaryCertificate(Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, 
			@RequestParam(name = REQUEST_PARAM_HR_ID, required=false) String hrid,
			@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) 
			@RequestParam(name = REQUEST_PARAM_START_DATE, required = false) Date startDate,
			@RequestParam(name = REQUEST_PARAM_DOCUMENT_NUMBER, required=true) String documentNumber) {

		Contact currentContact = this.userService.get(username).getCon();
		Certificate originalCertificate = this.certificateService.findCertificateByCertNo(documentNumber);
		if (originalCertificate == null) {
			String message = this.messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND,
					new Object[] { documentNumber }, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND, locale);
			return new RestResultDTO<RestCertificateDataDTO>(false, message);
		}
		if (originalCertificate.getCal() == null) {
			String message = this.messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NO_CAL,
					new Object[] { documentNumber }, RestErrors.MESSAGE_CERTIFICATE_NO_CAL, locale);
			return new RestResultDTO<RestCertificateDataDTO>(false, message);
		}
		Contact technician = null;
		if (StringUtils.isNotEmpty(hrid)) {
			technician = contactService.getByHrId(hrid);
		}

		Certificate supplementaryCertificate = this.certificateService.createSupplementaryCert(
				originalCertificate.getCertid(), technician != null ? technician : currentContact, startDate);

		if (!this.dtoFactory.canConvert(supplementaryCertificate)) {
			String message = this.dtoFactory.getConversionErrorMessage(supplementaryCertificate, locale);
			return new RestResultDTO<RestCertificateDataDTO>(false, message);
		}
		
		RestResultDTO<RestCertificateDataDTO> result = new RestResultDTO<RestCertificateDataDTO>(true, "");
		result.addResult(this.dtoFactory.convert(supplementaryCertificate, locale));
		return result;
	}

}
