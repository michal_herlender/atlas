package org.trescal.cwms.rest.alligator.controller;

import java.util.Map;
import java.util.TreeMap;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.system.Constants;

/*
 * Controller that returns reference data of the possible calibration data contents (As-Found, As-Left etc...),
 * localized to the requested language (falling back to the user's language if none specified)
 * 
 * TODO: CalibrationClass needs to be internationalized / localized
 * 
 * Author: Galen Beck 2016-01-21
 */
@Controller
@RestJsonController
public class RestCalibrationDataContentController {
	public static final String REQUEST_MAPPING = "/calibrationdatacontent";

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public Map<Integer, String> referenceData() {
		Map<Integer, String> result = new TreeMap<>();
		for (CalibrationClass instance : CalibrationClass.values()) {
			result.put(instance.ordinal(), instance.getDesc());
		}
		return result;
	}
}