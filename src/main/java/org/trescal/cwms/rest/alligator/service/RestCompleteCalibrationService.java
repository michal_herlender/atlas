package org.trescal.cwms.rest.alligator.service;

import java.util.List;
import java.util.Map;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationCancelInputDTO;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationCompleteInputDTO;

public interface RestCompleteCalibrationService {
	Calibration getCalibrationInStatus(List<JobItem> jobItems, String... statusNames);
	
	void cancelCalibration(RestApiCalibrationCancelInputDTO inputDto);
	
	void completeCalibration(RestApiCalibrationCompleteInputDTO inputDto);
	
	void completeCalibrationFromCertificate(Certificate certificate, Map<Integer, Integer> mapJobItemIdsToTimes, 
			ActionOutcome ao, Contact completedBy);
}
