package org.trescal.cwms.rest.alligator.controller;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.dto.output.RestInstrumentDTO;
import org.trescal.cwms.rest.alligator.dto.output.RestInstrumentDTOFactoryImpl;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

/*
 * REST instrument search controller that searches by current or former barcode
 * Author: Galen Beck - 2016-01-21
 */
@Controller
@RestJsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class RestInstrumentSearchController {

	public static final String REQUEST_MAPPING = "/instruments";
	public static final String REQUEST_PARAM_BARCODE = "barcode";
	public static final String REQUEST_PARAM_STANDARD_PLANT_NUMBER = "workingStandard_plantNumber";
	public static final String REQUEST_PARAM_JOBITEMID = "jobItemId";

	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private MessageSource messageSource;

	@Autowired
	private RestInstrumentDTOFactoryImpl dtoFactory;

	@Autowired
	private UserService userService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.GET, params = {
			REQUEST_PARAM_BARCODE }, produces = { Constants.MEDIATYPE_APPLICATION_JSON_UTF8,
					Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<RestInstrumentDTO> barcodeSearch(@RequestParam(value = REQUEST_PARAM_BARCODE) String barcode,
			Locale locale) {
		if (barcode.trim().length() != 0) {
			List<Instrument> instrumentList = instrumentService.findInstrumByBarCode(barcode.trim());
			return convertResults(instrumentList, locale);
		} else {
			String message = messageSource.getMessage(RestErrors.CODE_BARCODE_BLANK, null,
					RestErrors.MESSAGE_BARCODE_BLANK, locale);
			return new RestResultDTO<RestInstrumentDTO>(false, message);
		}
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.GET, params = {
			REQUEST_PARAM_JOBITEMID }, produces = { Constants.MEDIATYPE_APPLICATION_JSON_UTF8,
					Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<RestInstrumentDTO> jobItemSearch(
			@RequestParam(value = REQUEST_PARAM_JOBITEMID) Integer jobItemId, Locale locale) {
		if (jobItemId != null && jobItemId != 0) {
			JobItem jobItem = jobItemService.findJobItem(jobItemId);
			RestResultDTO<RestInstrumentDTO> result = new RestResultDTO<>(true, "");
			result.addResult(dtoFactory.convert(jobItem, locale));
			return result;
		} else {
			String message = messageSource.getMessage(RestErrors.CODE_JOBITEMID_BLANK, null,
					RestErrors.MESSAGE_JOBITEMID_BLANK, locale);
			return new RestResultDTO<RestInstrumentDTO>(false, message);
		}
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.GET, params = {
			REQUEST_PARAM_STANDARD_PLANT_NUMBER }, produces = { Constants.MEDIATYPE_APPLICATION_JSON_UTF8,
					Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<RestInstrumentDTO> workingStandardSearch(
			@RequestParam(value = REQUEST_PARAM_STANDARD_PLANT_NUMBER) String plantNumber,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) {
		if (plantNumber.trim().length() == 0) {
			String message = messageSource.getMessage(RestErrors.CODE_PLANT_NUMBER_BLANK, null,
					RestErrors.MESSAGE_PLANT_NUMBER_BLANK, locale);
			return new RestResultDTO<RestInstrumentDTO>(false, message);
		}
		User user = this.userService.get(username);
		
		// TODO - add a CompanyService:getBusinessCompanyIdsForUser() method (Subdiv too, via query rather than stream) 
		List<Integer> companyIds = user.getUserRoles().stream()
			.map(userRole -> userRole.getOrganisation().getComp().getCoid())
			.distinct()
			.collect(Collectors.toList());

		List<Instrument> instrumentList = instrumentService.searchPlantNo(companyIds, plantNumber);
		return convertResults(instrumentList, locale);
	}

	private RestResultDTO<RestInstrumentDTO> convertResults(List<Instrument> instrumentList, Locale locale) {
		RestResultDTO<RestInstrumentDTO> results = new RestResultDTO<>(true, "");
		instrumentList.stream().forEach(instrument -> results.addResult(dtoFactory.convert(instrument, locale)));
		return results;
	}
}