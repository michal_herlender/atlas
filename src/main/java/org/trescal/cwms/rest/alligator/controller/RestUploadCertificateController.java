package org.trescal.cwms.rest.alligator.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.dto.input.RestUploadCertificateDTO;
import org.trescal.cwms.rest.alligator.service.RestUploadCertificateService;
import org.trescal.cwms.rest.alligator.validator.RestUploadCertificateValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

/**
 * Note, controller is used by both the OAuth2 "API" based and session-cookie based namespaces
 *
 */
@Controller
@RestJsonController
public class RestUploadCertificateController {
	public final static String SESSION_REQUEST_MAPPING = "/uploadcertificate";
	public final static String API_REQUEST_MAPPING = "api/certificate/upload";

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private RestUploadCertificateValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestUploadCertificateService restUploadCertificateService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { SESSION_REQUEST_MAPPING, API_REQUEST_MAPPING }, method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<?> uploadCertificate(
			@RequestBody RestUploadCertificateDTO inputDTO, BindingResult bindingResult, Locale locale) {
		// validate
		validator.validate(inputDTO, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		Certificate certificate = this.certificateService.findCertificateByCertNo(inputDTO.getCertificateNumber());
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return this.restUploadCertificateService.uploadCertificate(certificate, inputDTO, username);
	}
}