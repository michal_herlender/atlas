package org.trescal.cwms.rest.alligator.dto.output;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Component
public class RestCertificateTechnicianDTOFactoryImpl implements
		RestCertificateTechnicianDTOFactory {

	@Override
	public List<RestCertificateTechnicianDTO> convert(Certificate certificate) {
		List<RestCertificateTechnicianDTO> result = new ArrayList<>();
		if (certificate.getRegisteredBy() != null) {
			Contact contact = certificate.getRegisteredBy();
			RestCertificateTechnicianDTO dto = new RestCertificateTechnicianDTO();
			dto.setTechnicianName(contact.getName());
			dto.setTechnicianJobTitle(contact.getPosition());
			result.add(dto);
		}
		return result;
	}

}
