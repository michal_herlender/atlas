package org.trescal.cwms.rest.alligator.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.alligatorlabelmedia.AlligatorLabelMedia;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplate;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.db.AlligatorLabelTemplateService;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorTemplateType;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.labelprinter.enums.AlligatorLabelType;
import org.trescal.cwms.core.tools.labelprinting.alligator.xml.*;
import org.trescal.cwms.core.tools.labelprinting.certificate.CertificateLabelSettingsService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.*;

@Component
public class RestLabelPrintJobServiceImpl implements RestLabelPrintJobService {

	@Autowired
	private AlligatorLabelFormatterAutomatic formatterAutomatic;
	@Autowired
	private AlligatorLabelFormatterFixed formatterFixed;
	@Autowired
	private AlligatorLabelTemplateService altService;
	@Autowired
	private CertificateLabelSettingsService settingsService;
	
	private final static Logger logger = LoggerFactory.getLogger(RestLabelPrintJobService.class);

	@Override
	public XmlAlligatorPrintJob getPrintJob(Collection<Certificate> certs, int copies, AlligatorLabelType type, 
			LabelPrinter labelPrinter, Locale locale, boolean printAllInstruments) {
		XmlAlligatorPrintJob result = new XmlAlligatorPrintJob(true, "");
		
		String errorMessage = addToPrintJob(result, certs, copies, type, labelPrinter, locale, printAllInstruments);
		if (!errorMessage.isEmpty()) {
			result.setResult(new XmlAlligatorResult(false, errorMessage));
		}
		else {
			initPrintJob(result, labelPrinter);
		}		
		return result;
	}

	@Override
	public XmlAlligatorPrintJob getPrintJob(Collection<CertLink> certLinks, int copies, AlligatorLabelType type,
			LabelPrinter labelPrinter, Locale locale) {
		XmlAlligatorPrintJob result = new XmlAlligatorPrintJob(true, "");
		
		String errorMessage = addToPrintJob(result, certLinks, copies, type, labelPrinter, locale);
		if (!errorMessage.isEmpty()) {
			result.setResult(new XmlAlligatorResult(false, errorMessage));
		}
		else {
			initPrintJob(result, labelPrinter);
		}		
		return result;
	}	
	
	private void initPrintJob(XmlAlligatorPrintJob printJob, LabelPrinter labelPrinter) {
		printJob.setResult(new XmlAlligatorResult());
		printJob.setMedia(getMediaDto(labelPrinter.getMedia()));
		printJob.setPrinter(getPrinterDto(labelPrinter));
	}

	private String addToPrintJob(XmlAlligatorPrintJob printJob, Collection<CertLink> certLinks, int copies, 
			AlligatorLabelType type, LabelPrinter labelPrinter, Locale locale) {
		StringBuffer errorMessage = new StringBuffer(); 
		Set<Integer> templateIds = new HashSet<>();
		for (CertLink certLink : certLinks) {
			Certificate certificate = certLink.getCert();
			AlligatorLabelTemplate template = getTemplate(certificate, type);
			if (template == null) {
				if (errorMessage.length() > 0) errorMessage.append(", ");
				errorMessage.append("No template : "+certificate.getCertno());
			}
			else {
				XmlAlligatorLabel xmlLabel = getLabelDto(template, certLink, locale, copies); 
				printJob.getLabels().add(xmlLabel);
				if (!templateIds.contains(template.getId())) {
					XmlAlligatorTemplate xmlTemplate = getXmlTemplate(template);
					if (xmlTemplate == null) {
						errorMessage.append("Error converting template : "+template.getDescription());
					}
					else {
						xmlTemplate.setDefaultPrinter(labelPrinter.getDescription());
						printJob.getTemplates().add(xmlTemplate);
					}
					templateIds.add(template.getId());
				}
			}
		}
		return errorMessage.toString();
	}
	
	/**
	 * Changed 2020-08-13 as part of DEV-1816 to print one label per instrument, if 
	 * printAllInstruments is true (replicates alligator bug), false in production.
	 */
	private String addToPrintJob(XmlAlligatorPrintJob printJob, Collection<Certificate> certs, int copies, 
			AlligatorLabelType type, LabelPrinter labelPrinter, Locale locale, boolean printAllInstruments) {
		List<CertLink> certLinks = new ArrayList<>(); 
		for (Certificate certificate : certs) {
			if (printAllInstruments) {
				certLinks.addAll(certificate.getLinks());
			}
			else if (!certificate.getLinks().isEmpty()) {
				CertLink firstCertLink = certificate.getLinks().iterator().next();
				certLinks.add(firstCertLink);
			}
		}
		return addToPrintJob(printJob, certLinks, copies, type, labelPrinter, locale);
	}
	
	private XmlAlligatorTemplate getXmlTemplate(AlligatorLabelTemplate template) {
		XmlAlligatorTemplate xmlTemplate = null;
		try {
			JAXBContext context = JAXBContext.newInstance(XmlAlligatorTemplate.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			StringReader reader = new StringReader(template.getTemplateXml());		
			xmlTemplate = (XmlAlligatorTemplate) unmarshaller.unmarshal(reader);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return xmlTemplate;
	}
	
	private AlligatorLabelTemplate getTemplate(Certificate certificate, AlligatorLabelType labelType) {
		Company businessCompany = getBusinessCompany(certificate);
		AccreditedLab accreditedLab = this.settingsService.getAccreditedLab(certificate); 
		AlligatorTemplateType templateType = null;
		if (accreditedLab != null) {
			if (labelType.equals(AlligatorLabelType.SMALL)) templateType = AlligatorTemplateType.ACCREDITED_SMALL; 
			else templateType = AlligatorTemplateType.ACCREDITED;
		}
		else {
			if (labelType.equals(AlligatorLabelType.SMALL)) templateType = AlligatorTemplateType.STANDARD_SMALL; 
			else templateType = AlligatorTemplateType.STANDARD;
		}
		
		return altService.findLabelTemplate(businessCompany, templateType);
	}
	
	private Company getBusinessCompany(Certificate certificate) {
		Company result = null;
		if (certificate.getCal() != null) {
			// Use business company that performed calibration 
			result = certificate.getCal().getOrganisation().getComp();
		}
		else if (certificate.getLinks() != null && !certificate.getLinks().isEmpty()) {
			// Use business company of job (third party, or in case of reserved certificate)
			result = certificate.getLinks().iterator().next().getJobItem().getJob().getOrganisation().getComp();
		}
		else {
			throw new UnsupportedOperationException("No calibration or job item link for certificate, cannot determine business company"); 
		}
		return result;
	}
	
	/**
	 * @param template
	 * @param certificate
	 * @param locale
	 * @param copies
	 * @return
	 */
	private XmlAlligatorLabel getLabelDto(AlligatorLabelTemplate template, CertLink certLink, Locale locale, int copies) {
		Certificate certificate = certLink.getCert();
		AccreditedLab accreditedLab = this.settingsService.getAccreditedLab(certificate);
		Instrument instrument = certLink.getJobItem().getInst();
		
		XmlAlligatorLabel labelDto = null;
		switch (template.getParameterStyle()) {
            case AUTOMATIC:
                labelDto = this.formatterAutomatic.getLabelDto(template, certificate, instrument, accreditedLab, locale, copies);
                break;
            case FIXED:
                labelDto = this.formatterFixed.getLabelDto(template, certificate, instrument, accreditedLab, locale, copies);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported parameter style " + template.getParameterStyle());
        }
		
		return labelDto;
	}
	
	private XmlAlligatorMedia getMediaDto(AlligatorLabelMedia media) {
		XmlAlligatorMedia mediaDto = new XmlAlligatorMedia();
		mediaDto.setName(media.getName());
		mediaDto.setWidth(media.getWidth().doubleValue());
		mediaDto.setHeight(media.getHeight().doubleValue());
		mediaDto.setRotationAngle(media.getRotationAngle().doubleValue());
		mediaDto.setGap(media.getGap().doubleValue());
		mediaDto.setPaddingLeft(media.getPaddingLeft().doubleValue());
		mediaDto.setPaddingRight(media.getPaddingRight().doubleValue());
		mediaDto.setPaddingTop(media.getPaddingTop().doubleValue());
		mediaDto.setPaddingBottom(media.getPaddingBottom().doubleValue());
		return mediaDto;
	}
	private XmlAlligatorPrinter getPrinterDto(LabelPrinter labelPrinter) {
		XmlAlligatorPrinter printerDto = new XmlAlligatorPrinter();
		printerDto.setName(labelPrinter.getDescription());
		printerDto.setSharedPrinterName(labelPrinter.getPath());
		printerDto.setDarknessLevel(labelPrinter.getDarknessLevel());
		printerDto.setResolution(labelPrinter.getResolution());
		printerDto.setPrinterLanguage(labelPrinter.getLanguage().name());
		printerDto.setSupportedMedia(new XmlAlligatorSupportedMedia(labelPrinter.getMedia().getName(), true));
		printerDto.setIpv4(labelPrinter.getIpv4());
		return printerDto;
	}
}
