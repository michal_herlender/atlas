package org.trescal.cwms.rest.alligator.dto.output;

public class RestCertificateCalibrationLocationDTO {
    private String environmentalTemperature;
    private String environmentalHumidity;
    private String ambientAirPressure;
    private String environmentalLocation;
	public String getEnvironmentalTemperature() {
		return environmentalTemperature;
	}
	public String getEnvironmentalHumidity() {
		return environmentalHumidity;
	}
	public String getAmbientAirPressure() {
		return ambientAirPressure;
	}
	public String getEnvironmentalLocation() {
		return environmentalLocation;
	}
	public void setEnvironmentalTemperature(String environmentalTemperature) {
		this.environmentalTemperature = environmentalTemperature;
	}
	public void setEnvironmentalHumidity(String environmentalHumidity) {
		this.environmentalHumidity = environmentalHumidity;
	}
	public void setAmbientAirPressure(String ambientAirPressure) {
		this.ambientAirPressure = ambientAirPressure;
	}
	public void setEnvironmentalLocation(String environmentalLocation) {
		this.environmentalLocation = environmentalLocation;
	}	
}
