package org.trescal.cwms.rest.alligator.controller;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.component.RestWorkRequirementsComponent;
import org.trescal.cwms.rest.alligator.dto.input.RestCompleteCalibrationDTO;
import org.trescal.cwms.rest.alligator.dto.input.RestWorkRequirementIdTimeDTO;
import org.trescal.cwms.rest.alligator.service.RestCompleteCalibrationService;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RestJsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class RestCompleteCalibrationController {
    public static final String REQUEST_MAPPING = "/completecalibration";

    @Autowired
    private CapabilityAuthorizationService capabilityAuthorizationService;
    @Autowired
    private ActionOutcomeService actionOutcomeService;
    @Autowired
    private CertificateService certificateService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private RestWorkRequirementsComponent restWorkRequirementsComponent;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private UserService userService;
	
	@Autowired
	private RestCompleteCalibrationService rccService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<?> completeCalibration(@RequestBody RestCompleteCalibrationDTO inputDTO, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        // Step 1 - look up certificate
        Certificate certificate = this.certificateService.findCertificateByCertNo(inputDTO.getCertificateNumber());
        if (certificate == null) {
            return new RestResultDTO<>(false,
                this.messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND,
                    new Object[]{inputDTO.getCertificateNumber()}, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND,
                    locale));
        }

        // Step 2 - verify that calibration is completable
        if (!certificate.getCal().getStatus().getName().equals(CalibrationStatus.ON_GOING)) {
            return new RestResultDTO<>(false, this.messageSource.getMessage(RestErrors.CODE_CAL_NOT_IN_PROGRESS,
                null, RestErrors.MESSAGE_CAL_NOT_IN_PROGRESS, locale));
        }

        // Step 3 - verify user authorization to complete calibration
        Contact completedBy = this.userService.get(username).getCon();
        val status = this.capabilityAuthorizationService.authorizedFor(
            certificate.getCal().getCapability().getId(), certificate.getCal().getCalType().getCalTypeId(),
            completedBy.getPersonid()).isLeft();
        if (status) {
            String calTypeShortName = this.translationService.getCorrectTranslation(
                certificate.getCal().getCalType().getServiceType().getShortnameTranslation(), locale);
            String procedureReference = certificate.getCal().getCapability().getReference();
            Object[] errorArgs = new Object[]{completedBy.getName(), procedureReference, calTypeShortName};
            return new RestResultDTO<>(false, this.messageSource.getMessage(RestErrors.CODE_CAL_NOT_AUTHORIZED,
                errorArgs, RestErrors.MESSAGE_CAL_NOT_AUTHORIZED, locale));
        }

        // Step 3 - look up all work requirements (building map of job items to
        // times)
        StringBuffer errorMessage = new StringBuffer();
        Map<Integer, Integer> mapJobItemIdsToTimes = lookupWorkRequirements(inputDTO.getCalibrationTimes(), errorMessage,
            locale);
        if (errorMessage.length() > 0) {
            return new RestResultDTO<>(false, errorMessage.toString());
        }

		// Step 4 - Lookup CalibrationOutcome (verify correctness)
		ActionOutcome ao = this.actionOutcomeService.get(inputDTO.getCalibrationOutcome());
		if (ao == null) {
			return new RestResultDTO<>(false,
				this.messageSource.getMessage(RestErrors.CODE_CALIBRATION_OUTCOME_NOT_DEFINED,
					new Object[]{inputDTO.getCalibrationOutcome()},
					RestErrors.MESSAGE_CALIBRATION_OUTCOME_NOT_DEFINED, locale));
		}

		// Step 5 - Update calibration data content aka CalibrationClass
		CalibrationClass calClass = getCalClass(inputDTO.getCalibrationDataContent());
		if (calClass == null) {
			return new RestResultDTO<>(false,
				this.messageSource.getMessage(RestErrors.CODE_CAL_DATA_NOT_DEFINED,
					new Object[]{inputDTO.getCalibrationDataContent()},
					RestErrors.MESSAGE_CAL_DATA_NOT_DEFINED, locale));
		}
		certificate.getCal().setCalClass(calClass);

		// Step 6 - Update Calibration Date, if supplied by end user (if null,
		// no update - use existing date)
		if (inputDTO.getCalibrationDate() != null) {
			certificate.setCalDate(DateTools.dateFromLocalDate(inputDTO.getCalibrationDate()));
			certificate.getCal().setCalDate(inputDTO.getCalibrationDate());
		}

		// Step 7 - Update Calibration verification status, if supplied (optional field)
		if (inputDTO.getCalibrationVerificationStatus() != null
				&& !inputDTO.getCalibrationVerificationStatus().isEmpty()) {
			CalibrationVerificationStatus calVerStatus = getCalibrationVerificationStatus(
					inputDTO.getCalibrationVerificationStatus());
			if (calVerStatus == null) {
				return new RestResultDTO<>(false,
					this.messageSource.getMessage(RestErrors.CODE_DATA_INVALID,
						new Object[]{inputDTO.getCalibrationVerificationStatus()},
						RestErrors.MESSAGE_DATA_INVALID, locale));
			} else {
				certificate.setCalibrationVerificationStatus(calVerStatus);
			}
		}

		this.rccService.completeCalibrationFromCertificate(certificate, mapJobItemIdsToTimes, ao, completedBy);

		return new RestResultDTO<>(true, "");
	}

	/*
	 * Returns enum or null if not found
	 */
	private CalibrationClass getCalClass(int ordinal) {
		if (ordinal < 0)
			return null;
		if (ordinal >= CalibrationClass.class.getEnumConstants().length)
			return null;
		return CalibrationClass.class.getEnumConstants()[ordinal];
	}

	/*
	 * Returns enum or null if not valid
	 */
	private CalibrationVerificationStatus getCalibrationVerificationStatus(String inputValue) {
		CalibrationVerificationStatus result = null;
		try {
			result = CalibrationVerificationStatus.valueOf(inputValue);
		} catch (IllegalArgumentException e) {
			// Intentionally return null value
		}
		return result;
	}

	/*
	 * Looks up work requirements and assembles Map of Job Item IDs to calibration
	 * times (used later to transform to CalLink map)
	 */
	private Map<Integer, Integer> lookupWorkRequirements(List<RestWorkRequirementIdTimeDTO> timeDTOs,
			StringBuffer errorMessage, Locale locale) {
		Map<Integer, Integer> mapJobItemsToTimes = new HashMap<>();
		List<Integer> workRequirementIds = timeDTOs.stream().map(RestWorkRequirementIdTimeDTO::getWorkRequirementId)
			.collect(Collectors.toList());
		Map<Integer, JobItemWorkRequirement> workRequirements = this.restWorkRequirementsComponent
			.lookup(workRequirementIds, errorMessage, locale);
		// Note, it's possible that the result map is null, and error message provided; 
		// we only proceed with assembling the result if no error message provided, and non-null map. 
		if ((workRequirements != null) && (errorMessage.length() == 0)) {
			for (RestWorkRequirementIdTimeDTO timeDTO : timeDTOs) {
				JobItemWorkRequirement jiwr = workRequirements.get(timeDTO.getWorkRequirementId());
				mapJobItemsToTimes.put(jiwr.getJobitem().getJobItemId(), timeDTO.getCalibrationTime());
			}
		}
		return mapJobItemsToTimes;
	}

}
