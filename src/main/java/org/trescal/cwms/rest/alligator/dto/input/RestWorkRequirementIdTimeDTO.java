package org.trescal.cwms.rest.alligator.dto.input;

public class RestWorkRequirementIdTimeDTO {
	private int workRequirementId;
	private int calibrationTime;
	
	public int getWorkRequirementId() {
		return workRequirementId;
	}
	public int getCalibrationTime() {
		return calibrationTime;
	}
	public void setWorkRequirementId(int workRequirementId) {
		this.workRequirementId = workRequirementId;
	}
	public void setCalibrationTime(int calibrationTime) {
		this.calibrationTime = calibrationTime;
	}
}
