package org.trescal.cwms.rest.alligator.dto.output;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created to match web services spec document.
 * Per Gerard's perspective in the 2016-02-17 meeting, includes fields that are not implemented in the ERP 
 * but are reserved for future use.  Unimplemented fields will return null values.
 * GB 2016-02-17
 */
@Getter @Setter
public class RestCertificateDataDTO {
	private String defaultLanguage;
	private String secondLanguage;
	private String language;
	private int jobID;
	private int jobGroupID;
	private int clientCompanyID;					// Added 2019-12-19
	private int clientSubdivID;						// Added 2019-12-19
	private String jobNumber;
	private String certificateTitle;
	private String certificateNumber;
	private String accreditationNumber;
	private String certificateStatus;				// Locale-independent e.g. RESERVED
	private String certificateStatusTranslation;	// Translation of cert status e.g. Reservado
	
	// Laboratory region - START
	private String laboratoryName;
	private List<String> laboratoryDepartments;
	private List<String> laboratoryAddress;
	private String laboratoryPostalCode;
	private String laboratoryCity;
	private String laboratoryProvinceState;
	private String laboratoryCountry;
	private String laboratoryFax;
	private String laboratoryPhone;
	private String laboratoryEmail;
	private String laboratoryWebsite;
	// Laboratory region - END
	
	private String trescalWebsite;
	private List<RestCertificateInstrumentDTO> instruments;	
	private List<RestWorkRequirementDTO> workRequirements;
	
	// Applicant region - START
	private String applicantName;
	private List<String> applicantAddress;
	private String applicantPostalCode;
	private String applicantCity;
	private String applicantProvinceState;
	private String applicantCountry;
	// Applicant region - END
	
	private Date calibrationStartDate;
	private Date calibrationEndDate;
	private Date calibrationDate;
	private Integer calibrationFrequency;
	private String calibrationFrequencyUnits;
	
	private String calibrationDescription;		// Not used yet
	private List<RestCertificateTechnicianDTO> technicians;
	
	// Signature region - START
	private String signatureName;		// Not used yet
	private Date signatureDateTime;		// Not used yet
	private String signatureTitle;		// Not used yet
	private String signatureImage;		// Base 64 encoded, not used yet
	private String signatureStamp;		// Base 64 encoded, not used yet
	// Signature region - END
	
	private String certificateRemark1;	// Not used yet
	private String certificateRemark2;	// Not used yet
	private String certificateRemark3;	// Not used yet
	private String certificateRemark4;	// Not used yet
	
	private RestCertificateCalibrationLocationDTO locations;	// Not used yet
	
	private List<String> calibrationProcedures;
	private List<String> workingStandards;
	private String certificateUncertaintyStatement;	// Not used yet
	
	// Template region - START
	private String templateCode;	// Not used yet
	private String templateExp;		// Not used yet
	private String templateVersion;	// Not used yet
	// Template region - END
	
	private String certificateContent;
	private Boolean isAdjusted;				// Not used yet
	private Boolean isAccredited;
	private Boolean isAsFound;
	private Boolean isAsLeft;
	
	private String specifications;			// Not used yet
	private String complianceStatement;		// Not used yet
}
