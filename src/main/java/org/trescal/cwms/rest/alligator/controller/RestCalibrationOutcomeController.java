package org.trescal.cwms.rest.alligator.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;

/*
 * Controller that returns reference data of the possible calibration outcomes,
 * localized to the requested language (falling back to the user's language if none specified)
 * 
 * Author: Galen Beck 2016-01-21
 */
@Controller @RestJsonController
public class RestCalibrationOutcomeController {
	public static final String REQUEST_MAPPING = "/calibrationoutcome";
	
	@Autowired
	private ItemStateService itemStateService; 
	@Autowired
	private ActionOutcomeService actionOutcomeService; 
	@Autowired
	private TranslationService translationService;
	
	/**
	 * The calibration outcomes are defined in the Lookup_PostCalibration enum and also have
	 * entries in the ActionOutcome table, however the ActionOutcome IDs are used as a reference 
	 * later so we use the ActionOutcomes to define the potential values. 
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET,
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public Map<Integer, String> referenceData(Locale locale) {
		Map<Integer, String> result = new TreeMap<>();
		ItemActivity itemActivityPreCalibration = itemStateService.findItemActivityByName("Pre-calibration"); 
		List<ActionOutcome> actionOutcomes = this.actionOutcomeService.getActionOutcomesForActivity(itemActivityPreCalibration);
		
		for(ActionOutcome ao: actionOutcomes) {
			String bestTranslation = this.translationService.getCorrectTranslation(ao.getTranslations(), locale);
			result.put(ao.getId(), bestTranslation);
		}
		return result;
	}
}
