package org.trescal.cwms.rest.alligator.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.dto.input.RestUploadCertificateDTO;

import java.util.Base64;

@Component
public class RestUploadCertificateValidator extends AbstractBeanValidator {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ContactService contactService;
	@Value("#{props['cwms.config.filebrowser.maxRestFileUploadSize']}")
	private Integer fileSizeLimit;

	@Override
	public boolean supports(Class<?> clazz) {
		return RestUploadCertificateDTO.class.equals(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		 super.validate(target, errors);
		 
		 RestUploadCertificateDTO dto = (RestUploadCertificateDTO) target;
		 Certificate certificate = this.certificateService.findCertificateByCertNo(dto.getCertificateNumber());
		 
		 if (certificate == null) {
			  errors.rejectValue("certificateNumber", RestErrors.CODE_CERTIFICATE_NOT_FOUND,
					  new Object[] { dto.getCertificateNumber() },
						RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND);
		} else if(certificate.getCal() == null){
			errors.rejectValue("certificateNumber", RestErrors.CODE_CERTIFICATE_NO_CAL,
					  new Object[] { dto.getCertificateNumber() },
						RestErrors.MESSAGE_CERTIFICATE_NO_CAL);
		}
		
		Certificate supplementaryFor = null;
		if (StringUtils.isNotBlank(dto.getSupplementaryFor())) {
			supplementaryFor = this.certificateService.findCertificateByCertNo(dto.getSupplementaryFor());
			if (supplementaryFor == null) {
				errors.rejectValue("supplementaryFor", RestErrors.CODE_CERTIFICATE_NOT_FOUND,
						new Object[] { dto.getSupplementaryFor() },
						RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND);
		    }
		}
		
		if (!dto.getFileName().endsWith("PDF") && !dto.getFileName().endsWith("pdf")) {
			errors.rejectValue("fileName", RestErrors.CODE_SUFFIX_NOT_PDF,
					new Object[] { dto.getFileName() }, RestErrors.MESSAGE_SUFFIX_NOT_PDF);
		}

		Contact technician = null;
		if (StringUtils.isNotEmpty(dto.getHrid())) {
			technician = contactService.getByHrId(dto.getHrid());
			if (technician == null) {
				errors.rejectValue("hrid", RestErrors.CODE_DATA_NOT_FOUND,
						new Object[] { dto.getHrid() }, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}
		
		byte[] fileData = Base64.getDecoder().decode(dto.getContent());
		if(fileData.length > fileSizeLimit){
			errors.rejectValue("content", RestErrors.CODE_FILE_SIZE,
					new Object[] { dto.getContent() }, RestErrors.MESSAGE_FILE_SIZE);
		}
	}
	
}
