package org.trescal.cwms.rest.alligator.dto.output;

/*
 * DTO with specific Instrument information to serialize as part of RESTful responses
 * 
 * Author: Galen Beck - 2016-01-21
 */
public class RestInstrumentDTO {
	private String customerDescription;
	private String formerBarcode;
	private Integer jobItemId;
	private String manufacturer;
	private String model;
	private Integer plantId;
	private String plantNumber;
	private String serialNumber;
	private String subFamily;

	public RestInstrumentDTO() {
		super();
	}

	public RestInstrumentDTO(String customerDescription, String formerBarcode, Integer jobItemId, String manufacturer,
			String model, Integer plantId, String plantNumber, String serialNumber, String subFamily) {
		super();
		this.customerDescription = customerDescription;
		this.formerBarcode = formerBarcode;
		this.jobItemId = jobItemId;
		this.manufacturer = manufacturer;
		this.model = model;
		this.plantId = plantId;
		this.plantNumber = plantNumber;
		this.serialNumber = serialNumber;
		this.subFamily = subFamily;
	}

	public String getCustomerDescription() {
		return customerDescription;
	}

	public String getFormerBarcode() {
		return formerBarcode;
	}

	public Integer getJobItemId() {
		return jobItemId;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getModel() {
		return model;
	}

	public Integer getPlantId() {
		return plantId;
	}

	public String getPlantNumber() {
		return plantNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public String getSubFamily() {
		return subFamily;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	public void setFormerBarcode(String formerBarcode) {
		this.formerBarcode = formerBarcode;
	}

	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}

	public void setPlantNumber(String plantNumber) {
		this.plantNumber = plantNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setSubFamily(String subFamily) {
		this.subFamily = subFamily;
	}
}
