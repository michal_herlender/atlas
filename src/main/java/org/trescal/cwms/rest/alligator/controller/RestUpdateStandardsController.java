package org.trescal.cwms.rest.alligator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentPercentageService;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.db.StandardUsedService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.dto.input.RestCertificateBarcodesDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.util.*;

/**
 * Note, controller is used by both the OAuth2 "API" based and session-cookie based namespaces
 */
@Controller
@RestJsonController
public class RestUpdateStandardsController {
    public static final String SESSION_REQUEST_MAPPING = "/updatestandards";
    public static final String API_REQUEST_MAPPING = "/api/certificate/standards";
	public static final String DELIMITER = "; ";

	@Autowired
	private MessageSource messageSource;

	@Autowired
    private InstrumService instrumentService;

    @Autowired
    private CertificateService certificateService;

    @Autowired
    private StandardUsedService standardUsedService;

    @Autowired
    private InstrumentPercentageService instrumentPercentageService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = {SESSION_REQUEST_MAPPING, API_REQUEST_MAPPING}, method = RequestMethod.POST, produces = {
        Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8}, consumes = {
        Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
    public RestResultDTO<Integer> addStandards(@RequestBody RestCertificateBarcodesDTO barcodeInputDto, Locale locale) {
        StringBuffer errorMessage = new StringBuffer();
        boolean success = true;
        Certificate certificate = this.certificateService
            .findCertificateByCertNo(barcodeInputDto.getCertificateNumber());
        List<Instrument> instruments = new ArrayList<>();

        if (certificate == null) {
            success = false;
            errorMessage.append(messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND,
                new Object[] { barcodeInputDto.getCertificateNumber() }, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND,
                locale));
        } else {
            Company businessCompany = certificate.getCal().getOrganisation().getComp();

            for (String barcode : barcodeInputDto.getBarcodes()) {
                // Note instrument barcode search restricted to just business company issuing
                // cert.
                List<Instrument> instrumentsForBarcode = this.instrumentService.getByBarcode(barcode, businessCompany);
                if (instrumentsForBarcode.size() != 1) {
                    success = false;
                    if (errorMessage.length() > 0)
                        errorMessage.append(DELIMITER);
                    errorMessage.append(messageSource.getMessage(RestErrors.CODE_INSTRUMENTS_FOUND,
                        new Object[] { instrumentsForBarcode.size(), barcode },
                        RestErrors.MESSAGE_INSTRUMENTS_FOUND, locale));
                } else {
                    Instrument instrument = instrumentsForBarcode.get(0);
                    if (!instrument.getComp().getId().equals(businessCompany.getId())) {
                        success = false;
                        System.out.println(instrument.getComp().getId() + "!=" + businessCompany.getId());
                        if (errorMessage.length() > 0)
                            errorMessage.append(DELIMITER);
                        errorMessage.append(messageSource.getMessage(RestErrors.CODE_INSTRUMENT_OTHER_COMPANY,
                            new Object[] { instrument.getPlantid(), barcode },
                            RestErrors.MESSAGE_INSTRUMENT_OTHER_COMPANY, locale));
                    } else {
                        // TODO implement check for calibration interval. internationalize messages in
                        // NewCalibrationController
                        // if (inst.getCalExpiresAfterNumberOfUses()
                        // && (inst.getUsesSinceLastCalibration().intValue() >=
                        // inst.getPermittedNumberOfUses().intValue()))
                        if (!instrument.getCalibrationStandard()) {
                            success = false;
                            if (errorMessage.length() > 0)
                                errorMessage.append(DELIMITER);
                            errorMessage.append(messageSource.getMessage(RestErrors.CODE_NOT_STANDARD,
                                new Object[] { instrument.getPlantid(), barcode }, RestErrors.MESSAGE_NOT_STANDARD,
                                locale));
                        } else if (instrument.getScrapped()) {
                            success = false;
                            if (errorMessage.length() > 0)
                                errorMessage.append(DELIMITER);
                            errorMessage.append(messageSource.getMessage(RestErrors.CODE_INSTRUMENT_SCRAPPED,
                                new Object[] { instrument.getPlantid(), barcode },
                                RestErrors.MESSAGE_INSTRUMENT_SCRAPPED, locale));
                        } else if (instrument.getStatus().equals(InstrumentStatus.BER)) {
                            success = false;
                            if (errorMessage.length() > 0)
                                errorMessage.append(DELIMITER);
                            errorMessage.append(messageSource.getMessage(RestErrors.CODE_INSTRUMENT_BER,
                                new Object[] { instrument.getPlantid(), barcode },
                                RestErrors.MESSAGE_INSTRUMENT_BER, locale));
                        } else if (instrument.isOutOfCalibration()) {
                            success = false;
                            if (errorMessage.length() > 0)
                                errorMessage.append(DELIMITER);
                            errorMessage.append(messageSource.getMessage(RestErrors.CODE_INSTRUMENT_PASTDUE,
                                new Object[] { instrument.getPlantid(), barcode },
                                RestErrors.MESSAGE_INSTRUMENT_PASTDUE, locale));
                        } else {
                            instruments.add(instrument);
                        }
                    }
                }
            }
        }

        if (success) {
            updateStandardsUsed(certificate.getCal(), instruments);
        }
        RestResultDTO<Integer> result = new RestResultDTO<Integer>(success, errorMessage.toString());
        if (certificate != null) {
            addPlantIdsOfStandardsToResult(result, certificate.getCal());
        }
        return result;
    }

	private void updateStandardsUsed(Calibration calibration, List<Instrument> instruments) {
		Set<StandardUsed> toDelete = new HashSet<>(calibration.getStandardsUsed());
		List<Instrument> toAdd = new ArrayList<>();
		for (Instrument instrument : instruments) {
			boolean found = false;
			for (StandardUsed existingStandardUsed : calibration.getStandardsUsed()) {
				if (existingStandardUsed.getInst().getPlantid() == instrument.getPlantid()) {
					// Selected, does not need to be deleted
					toDelete.remove(existingStandardUsed);
					found = true;
				}
			}
			if (!found) {
				toAdd.add(instrument);
			}
		}
		deleteStandardsUsed(calibration, toDelete);
		addStandardsUsed(calibration, toAdd);
	}

	private void deleteStandardsUsed(Calibration calibration, Set<StandardUsed> toDelete) {
		for (StandardUsed standardUsed : toDelete) {
			calibration.getStandardsUsed().remove(standardUsed);
			this.standardUsedService.delete(standardUsed);
		}
	}

	private void addStandardsUsed(Calibration calibration, List<Instrument> toAdd) {
		for (Instrument instrument : toAdd) {
			StandardUsed standardUsed = new StandardUsed();
			standardUsed.setCalibration(calibration);
            standardUsed.setCalPercentage(instrumentPercentageService.totalCalibrationPercentage(instrument.getNextCalDueDate(), instrument.getCalFrequencyUnit(), instrument.getCalFrequency()));
            standardUsed.setInst(instrument);
            standardUsed.setNextCalDueDate(instrument.getNextCalDueDate());
			calibration.getStandardsUsed().add(standardUsed);
			this.standardUsedService.save(standardUsed);
		}
	}

	private void addPlantIdsOfStandardsToResult(RestResultDTO<Integer> result, Calibration calibration) {
		for (StandardUsed standardUsed : calibration.getStandardsUsed()) {
			result.addResult(standardUsed.getInst().getPlantid());
		}
	}
}