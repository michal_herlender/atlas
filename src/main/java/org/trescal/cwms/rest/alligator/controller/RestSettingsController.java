package org.trescal.cwms.rest.alligator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettings;
import org.trescal.cwms.core.system.entity.alligatorsettings.db.AlligatorSettingsService;
import org.trescal.cwms.rest.alligator.dto.output.RestAlligatorSettingsDto;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller @RestJsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class RestSettingsController {

	@Autowired
	private AlligatorSettingsService alligatorSettingsService;
	@Autowired
	private UserService userService;
	@Value("#{props['cwms.config.systemtype']}")
	private String systemType;
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/alligator/settings", method=RequestMethod.GET,
		produces={Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestAlligatorSettingsDto> getSettings(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Contact contact = this.userService.get(username).getCon();
		// Return settings for user (note later, we may need to accommodate travelling users)
		boolean production = systemType.equalsIgnoreCase("production");
		AlligatorSettings settings = this.alligatorSettingsService.getSettings(contact, production);
		RestResultDTO<RestAlligatorSettingsDto> result = null; 
		if (settings == null) {
			// Not expected to occur - default fallback settings loaded in database
			result = new RestResultDTO<>(false, "No alligator settings available");
		}
		else {
			result = new RestResultDTO<>(true, "");
			result.addResult(getDto(settings));
		}
		return result;
	}
	
	private RestAlligatorSettingsDto getDto(AlligatorSettings settings) {
		RestAlligatorSettingsDto dto = new RestAlligatorSettingsDto();
		dto.setExcelLocalTempFolder(settings.getExcelLocalTempFolder());
		dto.setExcelRelativeTempFolder(settings.getExcelRelativeTempFolder());
		dto.setExcelSaveTempCopy(settings.getExcelSaveTempCopy());
		dto.setUploadEnabled(settings.getUploadEnabled());
		dto.setUploadMoveUponUpload(settings.getUploadMoveUponUpload());
		dto.setUploadRawDataFolder(settings.getUploadRawDataFolder());
		dto.setUploadTempDataFolder(settings.getUploadTempDataFolder());
		dto.setUploadUseWindowsCredentials(settings.getUploadUseWindowsCredentials());
		dto.setUploadWindowsDomain(settings.getUploadWindowsDomain());
		dto.setUploadWindowsPassword(settings.getUploadWindowsPassword());
		dto.setUploadWindowsUsername(settings.getUploadWindowsUsername());
		dto.setDescription(settings.getDescription());
		return dto;
	}
}
