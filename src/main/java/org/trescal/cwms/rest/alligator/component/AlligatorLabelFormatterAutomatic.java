package org.trescal.cwms.rest.alligator.component;

import java.util.ArrayList;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplate;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AccreditedLabelCertificateNumber;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AccreditedLabelPlantNumber;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AccreditedLabelRecallDate;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationLabelDateFormat;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationLabelTechnicianName;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.StandardLabelCertificateNumber;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.StandardLabelPlantNumber;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.StandardLabelRecallDate;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.labelprinting.alligator.xml.XmlAlligatorLabel;
import org.trescal.cwms.core.tools.labelprinting.alligator.xml.XmlAlligatorParameterNameValue;

@Component
public class AlligatorLabelFormatterAutomatic extends AlligatorLabelFormatter {
    @Autowired
    private MessageSource messageSource;

    // System defaults
    @Autowired
    private AccreditedLabelCertificateNumber accreditedLabelCertificateNumber;
    @Autowired
    private AccreditedLabelPlantNumber accreditedLabelPlantNumber;
    @Autowired
    private AccreditedLabelRecallDate accreditedLabelRecallDate;
	@Autowired
	private StandardLabelCertificateNumber standardLabelCertificateNumber;
	@Autowired
	private StandardLabelPlantNumber standardLabelPlantNumber; 	
	@Autowired
	private StandardLabelRecallDate standardLabelRecallDate; 
	@Autowired
	private CalibrationLabelTechnicianName labelTechnicianName;
	@Autowired
	private CalibrationLabelDateFormat labelDateFormat;
	
	public XmlAlligatorLabel getLabelDto(AlligatorLabelTemplate template, Certificate certificate, Instrument instrument, 
			AccreditedLab accreditedLab, Locale locale, int copies) {
        Contact contact = certificate.getLinks().iterator().next().getJobItem().getJob().getCon();
        String patternDateFormat = this.labelDateFormat.parseValueHierarchical(Scope.CONTACT, contact, null);

        String valueCalDate = super.formatDate(certificate.getCalDate(), patternDateFormat, locale);
        String valueDueDate = "";
        String valuePlantId = "";
        String valuePlantNumber = "";
        String valueCertNumber = certificate.getIdentifier();
        String valueSerialNumber = "";
        String valueAccreditation = "";
        String valueTPCertNumber = certificate.getThirdCertNo() == null ? "" : certificate.getThirdCertNo();
        String valueTechnicianName = "";

        Boolean printAccreditation = null;
		Boolean printCertNumber = null;
		Boolean printPlantId = false;
		Boolean printPlantNumber = null;
		Boolean printRecallDateField = null;
		Boolean printRecallDateValue = null;
		Boolean printTPCertNumber = false;
		Boolean printTechnicianName = this.labelTechnicianName.parseValueHierarchical(Scope.CONTACT, contact, null);

		if (accreditedLab != null) {
			printAccreditation = true;
			valueAccreditation = accreditedLab.getLabNo();
			printCertNumber = this.accreditedLabelCertificateNumber.parseValueHierarchical(Scope.CONTACT, contact, null);
			printPlantNumber = this.accreditedLabelPlantNumber.parseValueHierarchical(Scope.CONTACT, contact, null);
			String sdRecallDate = this.accreditedLabelRecallDate.parseValueHierarchical(Scope.CONTACT, contact, null);
			printRecallDateField = !AccreditedLabelRecallDate.NO.equals(sdRecallDate);
			printRecallDateValue = AccreditedLabelRecallDate.YES.equals(sdRecallDate);
		}
		else {
			printAccreditation = false;
			printCertNumber = this.standardLabelCertificateNumber.parseValueHierarchical(Scope.CONTACT, contact, null);
			if (printCertNumber && certificate.getType().equals(CertificateType.CERT_THIRDPARTY)) {
				printTPCertNumber = true;
			}
			printPlantNumber = this.standardLabelPlantNumber.parseValueHierarchical(Scope.CONTACT, contact, null);
			String sdRecallDate = this.standardLabelRecallDate.parseValueHierarchical(Scope.CONTACT, contact, null);
			printRecallDateField = !StandardLabelRecallDate.NO.equals(sdRecallDate);
			printRecallDateValue = StandardLabelRecallDate.YES.equals(sdRecallDate);
		}
		// Issue that there can be more fields than template supports, if needed don't print the plant id (appears on footer with barcode)
		int maxFields = RestLabelDefinitions.FIELDS.length;
		int desiredFields = 2; 	// Serial number, and calibration date
		if (printRecallDateField) desiredFields++;
		if (printCertNumber) desiredFields++;
		if (printPlantNumber) desiredFields++;
		if (printTechnicianName) desiredFields++;	// Tech name and TP cert number are mutually exclusive, so at most one will print 
		if (printTPCertNumber) desiredFields++;
		if (desiredFields < maxFields) {
			printPlantId = true;
		}
		
		if (instrument != null) {
			if (instrument.getNextCalDueDate() != null) {
                valueDueDate = super.formatLocalDate(instrument.getNextCalDueDate(), patternDateFormat, locale);
            }
			valuePlantId = String.valueOf(instrument.getPlantid());
			if (instrument.getPlantno() != null) {
				valuePlantNumber = instrument.getPlantno().trim();
			}
			if (instrument.getSerialno() != null) {
				valueSerialNumber = instrument.getSerialno().trim();
			}
			 
		}
		if ((certificate.getCal() != null) && certificate.getCal().getCompletedBy() != null) {
			valueTechnicianName = certificate.getCal().getCompletedBy().getName();
		}
		
		XmlAlligatorLabel label = new XmlAlligatorLabel();
		label.setName(template.getDescription());
		label.setQuantity(copies);
		ArrayList<XmlAlligatorParameterNameValue> parameters = new ArrayList<>();
		label.setParameters(parameters);
		int index = 0;

		String fieldCalDate = this.messageSource.getMessage(RestLabelDefinitions.KEY_COMMON_CAL_DATE, null, RestLabelDefinitions.MESSAGE_COMMON_CAL_DATE, locale);
		parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldCalDate));
		parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valueCalDate));
		index++;
		
		if (printRecallDateField) {
			String fieldDueDate = this.messageSource.getMessage(RestLabelDefinitions.KEY_COMMON_NEXT_CAL, null, RestLabelDefinitions.MESSAGE_COMMON_NEXT_CAL, locale);
			parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldDueDate));
			if (printRecallDateValue) {
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valueDueDate));
			}
			else {
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], ""));
			}
			index++;
		}

		if (printPlantNumber) {
			String fieldPlantNumber = this.messageSource.getMessage(RestLabelDefinitions.KEY_COMMON_CUSTOMER_ID, null, RestLabelDefinitions.MESSAGE_COMMON_CUSTOMER_ID, locale);
			parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldPlantNumber));
			parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valuePlantNumber));
			index++;
		}
		
		String fieldSerialNumber = this.messageSource.getMessage(RestLabelDefinitions.KEY_COMMON_SERIAL_NO, null, RestLabelDefinitions.MESSAGE_COMMON_SERIAL_NO, locale);
		parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldSerialNumber));
		parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valueSerialNumber));
		index++;

		if (printPlantId) {
			String fieldPlantId = this.messageSource.getMessage(RestLabelDefinitions.KEY_CAL_PLANT_ID, null, RestLabelDefinitions.MESSAGE_CAL_PLANT_ID, locale);
			parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldPlantId));
			parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valuePlantId));
			index++;
		}

		if (certificate.getType().equals(CertificateType.CERT_INHOUSE)) {
			if (printCertNumber) {
				String fieldCertNumber = this.messageSource.getMessage(RestLabelDefinitions.KEY_CAL_CERT_NO, null, RestLabelDefinitions.MESSAGE_CAL_CERT_NO, locale);
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldCertNumber));
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valueCertNumber));
				index++;
			}
			if (printAccreditation) {
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.ACCRED, valueAccreditation));
			}
			if (printTechnicianName) {
				String fieldTechnicianName = this.messageSource.getMessage(RestLabelDefinitions.KEY_CAL_TECHNICIAN, null, RestLabelDefinitions.MESSAGE_CAL_TECHNICIAN, locale);
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldTechnicianName));
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valueTechnicianName));
			}
		}
		else if (certificate.getType().equals(CertificateType.CERT_THIRDPARTY)) {
			if (printCertNumber) {
				String fieldCertNumber = this.messageSource.getMessage(RestLabelDefinitions.KEY_TP_OUR_CERT_NO, null, RestLabelDefinitions.MESSAGE_TP_OUR_CERT_NO, locale);
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldCertNumber));
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valueCertNumber));
				index++;
			}
			if (printTPCertNumber) {
				String fieldTPCertNumber = this.messageSource.getMessage(RestLabelDefinitions.KEY_TP_TP_CERT_NO, null, RestLabelDefinitions.MESSAGE_TP_TP_CERT_NO, locale);
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FIELDS[index], fieldTPCertNumber));
				parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.VALUES[index], valueTPCertNumber));
			}
		}

		parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.BARCODE, valuePlantId));
		parameters.add(new XmlAlligatorParameterNameValue(RestLabelDefinitions.FOOTER, valuePlantId));
		
		return label;
	}
	
}
