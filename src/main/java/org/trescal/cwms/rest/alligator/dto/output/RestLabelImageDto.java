package org.trescal.cwms.rest.alligator.dto.output;

import java.util.Date;

public class RestLabelImageDto {
	private String name;
	private String fileName;
	private Date lastModified;
	private String file;
	
	public String getName() {
		return name;
	}
	public String getFileName() {
		return fileName;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public String getFile() {
		return file;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public void setFile(String file) {
		this.file = file;
	}
}
