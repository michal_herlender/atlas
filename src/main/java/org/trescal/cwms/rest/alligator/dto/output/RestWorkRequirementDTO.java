package org.trescal.cwms.rest.alligator.dto.output;

import lombok.Getter;
import lombok.Setter;

/*
 * DTO with specific WorkRequirement information to serialize as part of RESTful responses
 * 
 * Author: Galen Beck - 2016-01-21
 */
@Getter @Setter
public class RestWorkRequirementDTO {

	private int workRequirementId;
	private boolean canStart;
	private String description;
	private String procedures;
	private Integer serviceTypeId;

	private Integer subFamilyTMLId;
	private String calIntervalUnit;
	private Integer calInterval;
	private String requirementText;
	private Integer businessSubdivId;
	private Boolean isNext;

}
