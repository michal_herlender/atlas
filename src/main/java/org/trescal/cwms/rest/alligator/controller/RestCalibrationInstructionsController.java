package org.trescal.cwms.rest.alligator.controller;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirementComparator;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.component.RestStartCalibrationValidator;
import org.trescal.cwms.rest.alligator.component.RestWorkRequirementsComponent;
import org.trescal.cwms.rest.alligator.dto.input.RestWorkRequirementIdsDTO;
import org.trescal.cwms.rest.alligator.dto.output.RestCalibrationInstructionsResult;
import org.trescal.cwms.rest.alligator.dto.output.RestWorkRequirementDTOFactory;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller @RestJsonController
public class RestCalibrationInstructionsController {
	public static final String REQUEST_MAPPING = "/calibrationinstructions";
	
	@Autowired
	private RestWorkRequirementsComponent restWorkRequirements; 

	@Autowired
	private RestWorkRequirementDTOFactory restWorkRequirementDtoFactory;
	
	@Autowired
	private CalReqService calReqService;
	
	@Autowired
	private RestStartCalibrationValidator validator;
	
	@Autowired
	private MessageSource messageSource; 
	
	@Autowired
	private InstructionLinkService instructionLinkService; 
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.POST,
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestCalibrationInstructionsResult> getInstructions(Locale locale,
			@RequestBody RestWorkRequirementIdsDTO inputDTO) {

		// Step 1 - Look up Work Requirements
		StringBuffer errorMessage = new StringBuffer();
		Map<Integer, JobItemWorkRequirement> reqMap = restWorkRequirements.lookup(inputDTO.getWorkRequirementIds(), errorMessage, locale);
		if (errorMessage.length() > 0) {
			return new RestResultDTO<RestCalibrationInstructionsResult>(false, errorMessage.toString());
		}
		Collection<JobItemWorkRequirement> jobItemWorkReqs = reqMap.values();//50
		
		// Step 2 - Ensure that all work requirements are on the same job
		if (validator.getUniqueJobCount(jobItemWorkReqs) > 1) {// check if all job ids.
			String message = messageSource.getMessage(RestErrors.CODE_WR_DIFFERENT_JOBS, null, locale);
			return new RestResultDTO<RestCalibrationInstructionsResult>(false, message);
		}
		
		RestResultDTO<RestCalibrationInstructionsResult> dto = new RestResultDTO<RestCalibrationInstructionsResult>(true, "");
		
		// 50
		// Step 3 - Look up Cal Requirements (CalReq) for all job items and the (older) Requirements connected to Job
		Map<JobItemWorkRequirement, CalReq> mapWItoCalReq = new TreeMap<>(new JobItemWorkRequirementComparator()); 
		for (JobItemWorkRequirement jiwr : jobItemWorkReqs) {
			JobItem ji = jiwr.getJobitem();
			CalReq calReq = calReqService.findCalReqsForJobItem(ji);//50, calreqs are null here
			if (calReq != null) {
				mapWItoCalReq.put(jiwr, calReq);
			}
			addIndividualRequirements(dto, jiwr, ji.getReq());//50
		}
		// Identify if there is a single cal requirement for all job items.
		CalReq singleCalReq = checkForSingleCalReq(jobItemWorkReqs, mapWItoCalReq);
		if (singleCalReq != null) {
			addGlobalCalReq(dto, singleCalReq);
		}
		else {
			addIndividualCalReq(dto, mapWItoCalReq);
		}
		
		// Step 4 - Look up InstructionLinks for Job (all WI already validated to be on same job)
		// jobitem calls getContract and get all instructions 
		// Two types of instructions one from job and other from contract and job doesnt contain contract instructions
		// 50 
		Job job = jobItemWorkReqs.iterator().next().getJobitem().getJob();
		List<InstructionLink<?>> instructionLinks = instructionLinkService.getAllForJob(job);
				
			addGlobalInstructionLinks(dto, instructionLinks);
			jobItemContractInstructions(dto, jobItemWorkReqs);
			
		return dto ;
	}
	
	
	private void jobItemContractInstructions(RestResultDTO<RestCalibrationInstructionsResult> dto, Collection<JobItemWorkRequirement> jobItemWorkReqs){

		//Extraction of all instruction of JobItems on the Same Job.
		for (JobItemWorkRequirement jiwr : jobItemWorkReqs) {
			
			Contract contract = jiwr.getJobitem().getContract();
			if (contract != null) {
				Set<Instruction> inst = instructionLinkService.getAllForJobitem(contract);
	
				//Add condition to extract R and P and Calibration Instructions from contract. 
				for(Instruction a : inst)
				{
					if(a.getInstructiontype().equals(InstructionType.CALIBRATION) ||
							a.getInstructiontype().equals(InstructionType.REPAIRANDADJUSTMENT))
					{
						String text = "";
	
						text =  contract.getContractNumber() 
								+ " : " + a.getInstruction();
	
						
						RestCalibrationInstructionsResult result = new RestCalibrationInstructionsResult();
						result.setGlobalInstruction(false);
						result.setWorkRequirementId(jiwr.getId());
						result.setText(text.toString());
						result.setDescription("");
						result.setProcedures("");
						
						dto.addResult(result);
	
					}	
				}
			}
		}
	
	}
	
	/*
	 * Adds a single CalReq to be displayed as a global requirement for all work requirements
	 * WorkRequirementID is then intentionally null
	 */
	private void addGlobalCalReq(RestResultDTO<RestCalibrationInstructionsResult> dto, CalReq singleCalReq) {
		RestCalibrationInstructionsResult result = new RestCalibrationInstructionsResult();
		result.setGlobalInstruction(true);
		result.setText(StringTools.convertCalReqsToSingleString(singleCalReq));
		result.setDescription("");
		result.setProcedures("");
		dto.addResult(result);
	}
	
	/*
	 * Adds work-requirement-specific CalReqs 
	 */
	private void addIndividualCalReq(RestResultDTO<RestCalibrationInstructionsResult> dto, Map<JobItemWorkRequirement, CalReq> mapWItoCalReq) {
		for (Map.Entry<JobItemWorkRequirement, CalReq> entry : mapWItoCalReq.entrySet()) {
			JobItemWorkRequirement jiwr = entry.getKey();
			CalReq calReq = entry.getValue();			
			
			RestCalibrationInstructionsResult result = new RestCalibrationInstructionsResult();
			result.setGlobalInstruction(false);
			result.setDescription(this.restWorkRequirementDtoFactory.getDescription(jiwr));
			result.setProcedures(this.restWorkRequirementDtoFactory.getProcedures(jiwr));
			result.setText(StringTools.convertCalReqsToSingleString(calReq));
			result.setWorkRequirementId(jiwr.getId());
			dto.addResult(result);
		}
	}
	
	/*
	 * Adds InstructionLink globally as instruction (for InstructionType.CALIBRATION instructions only)
	 */
	private void addGlobalInstructionLinks(RestResultDTO<RestCalibrationInstructionsResult> dto, List<InstructionLink<?>> instructionLinks) {
		
		for (InstructionLink<?> instructionLink : instructionLinks) {
			if (instructionLink.getInstruction().getInstructiontype().equals(InstructionType.CALIBRATION) ) {
				StringBuffer text = new StringBuffer();
				if (instructionLink.getInstructionEntity().equals(InstructionEntity.COMPANY)) {
					Company company = (Company) instructionLink.getEntity();
					text.append(company.getConame());
					text.append(": ");
				}
				else if (instructionLink.getInstructionEntity().equals(InstructionEntity.SUBDIV)) {
					Subdiv subdiv = (Subdiv) instructionLink.getEntity();
					text.append(subdiv.getSubname());
					text.append(": ");
				}
				else if (instructionLink.getInstructionEntity().equals(InstructionEntity.CONTACT)) {
					Contact contact = (Contact) instructionLink.getEntity();
					text.append(contact.getName());
					text.append(": ");
				}
				else if (instructionLink.getInstructionEntity().equals(InstructionEntity.CONTRACT)){
					Contract contract = (Contract) instructionLink.getEntity();
					text.append(contract.getContractNumber());
					text.append(": ");	
				}
				text.append(instructionLink.getInstruction().getInstruction());
				RestCalibrationInstructionsResult result = new RestCalibrationInstructionsResult();
				result.setGlobalInstruction(true);
				result.setText(text.toString());
				result.setDescription("");
				result.setProcedures("");
		
				dto.addResult(result);
			}	
		}
	}
	
	/*
	 * Adds any job item specific requirements as instructions to the result
	 * These can either be from 
	 *   (a) the requirement text of the WorkRequirement, or 
	 *   (b) the simple Requirement class attached to the job item
	 */
	private void addIndividualRequirements(RestResultDTO<RestCalibrationInstructionsResult> dto, JobItemWorkRequirement jiwr, Set<Requirement> requirements) {
		if (jiwr.getWorkRequirement().getRequirement() != null && 
				!jiwr.getWorkRequirement().getRequirement().isEmpty()) {
			RestCalibrationInstructionsResult result = new RestCalibrationInstructionsResult();
			result.setGlobalInstruction(false);
			result.setText(jiwr.getWorkRequirement().getRequirement());
			result.setWorkRequirementId(jiwr.getWorkRequirement().getId());
			result.setDescription(this.restWorkRequirementDtoFactory.getDescription(jiwr));
			result.setProcedures(this.restWorkRequirementDtoFactory.getProcedures(jiwr));
			dto.addResult(result);
		}
		for (Requirement requirement : requirements) {
			if (!requirement.isDeleted()) {
				RestCalibrationInstructionsResult result = new RestCalibrationInstructionsResult();
				result.setGlobalInstruction(false);
				result.setText(requirement.getRequirement());
				result.setWorkRequirementId(jiwr.getWorkRequirement().getId());
				result.setDescription(this.restWorkRequirementDtoFactory.getDescription(jiwr));
				result.setProcedures(this.restWorkRequirementDtoFactory.getProcedures(jiwr));
				dto.addResult(result);
			}
		}
	}
	
	/*
	 * If each and every Job Item has the same CalReq, return it so it can just be displayed once  
	 */
	private CalReq checkForSingleCalReq(Collection<JobItemWorkRequirement> jobItemWorkReqs, Map<JobItemWorkRequirement, CalReq> mapWItoCalReq) {
		boolean foundSingleCalReq = true;
		CalReq singleCalReq = null;
		for (JobItemWorkRequirement jiwr : jobItemWorkReqs) {
			CalReq calReq = mapWItoCalReq.get(jiwr);
			if (calReq == null) {
				foundSingleCalReq = false;
				break;
			}
			else if ((singleCalReq != null) && singleCalReq.getId() != calReq.getId()) {
				foundSingleCalReq = false;
				break;
			}
			else {
				singleCalReq = calReq;
			}
		}
		if (foundSingleCalReq) return singleCalReq;
		return null;
	}
}