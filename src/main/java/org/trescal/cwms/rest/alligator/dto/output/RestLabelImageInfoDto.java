package org.trescal.cwms.rest.alligator.dto.output;

import java.util.Date;

public class RestLabelImageInfoDto {
	private String name;
	private String fileName;
	private Date lastModified;
	
	public String getName() {
		return name;
	}
	public String getFileName() {
		return fileName;
	}
	public Date getLastModified() {
		return lastModified;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
}
