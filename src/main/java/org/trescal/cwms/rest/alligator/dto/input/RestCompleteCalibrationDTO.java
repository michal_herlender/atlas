package org.trescal.cwms.rest.alligator.dto.input;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

@Getter
@Setter
public class RestCompleteCalibrationDTO {
    private String certificateNumber;
    private int calibrationOutcome;
    private int calibrationDataContent;
    private Date calibrationStartDate;
    private Date calibrationEndDate;
    // Note - some systems (e.g. Alligator) provide time and time zone information to all three date fields in this DTO
    @JsonFormat(pattern = "yyyy-MM-dd['T'HH:mm:ss.SSS][XXX]")
    private LocalDate calibrationDate;                // Optional field
    private String calibrationVerificationStatus;    // Optional field
    private List<RestWorkRequirementIdTimeDTO> calibrationTimes;
}
