package org.trescal.cwms.rest.alligator.controller;

import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.documents.images.image.db.ImageService;
import org.trescal.cwms.core.documents.images.labelimage.LabelImage;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.dto.output.RestLabelImageDto;
import org.trescal.cwms.rest.alligator.dto.output.RestLabelImageInfoDto;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller @RestJsonController
public class RestLabelImageController {
	
	public final static String REQUEST_MAPPING_LIST = "/alligator/labelimagelist";
	public final static String REQUEST_MAPPING_GET = "/alligator/labelimage";
	
	@Autowired
	private ImageService imageService;
	@Autowired
	private MessageSource messageSource; 
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING_GET}, method = RequestMethod.GET,
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestLabelImageDto> findLabel(Locale locale,
			@RequestParam(name="name", required=true) String name) {
		LabelImage image = this.imageService.getMostRecentLabelImage(name);
		RestResultDTO<RestLabelImageDto> result = null;
		if (image == null) {
			String errorMessage = messageSource.getMessage(RestErrors.CODE_DATA_NOT_FOUND, null, RestErrors.MESSAGE_DATA_NOT_FOUND, locale);
			result = new RestResultDTO<RestLabelImageDto>(false, errorMessage);
		}
		else {
			result = new RestResultDTO<RestLabelImageDto>(true, "");
			RestLabelImageDto dto = new RestLabelImageDto(); 
			dto.setName(image.getDescription());
			dto.setFileName(image.getFileName());
			dto.setLastModified(image.getLastModified());
			dto.setFile(Base64.getEncoder().encodeToString(image.getFileData()));
			result.addResult(dto);
		}
		return result;
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING_LIST}, method = RequestMethod.GET,
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestLabelImageInfoDto> listLabels() {
		RestResultDTO<RestLabelImageInfoDto> result = new RestResultDTO<RestLabelImageInfoDto>(true, "");
		// Images are sorted by name and then by last modified date in descending order; 
		// we add the first image for each name as the most current version in cae of duplicate
		HashSet<String> names = new HashSet<>();
		List<LabelImage> images = this.imageService.getAllLabelImages();
		for (LabelImage image : images) {
			if (!names.contains(image.getDescription())) {
				RestLabelImageInfoDto dto = new RestLabelImageInfoDto();
				dto.setLastModified(image.getLastModified());
				dto.setFileName(image.getFileName());
				dto.setName(image.getDescription());
				result.addResult(dto);
			}
		}
		return result;
	}
	
}
