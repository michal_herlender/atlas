package org.trescal.cwms.rest.alligator.service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Base64;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.external.adveso.events.UploadCertificateEvent;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.rest.alligator.controller.RestUploadCertificateController;
import org.trescal.cwms.rest.alligator.dto.input.RestUploadCertificateDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Service
public class RestUploadCertificateServiceImpl implements RestUploadCertificateService {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private SystemComponentService systemComponentService;
	@Autowired
	private ComponentDirectoryService componentDirectoryService;
	@Autowired
	private UserService userService;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	public static final Logger logger = LoggerFactory.getLogger(RestUploadCertificateController.class);

	@Override
	public RestResultDTO<?> uploadCertificate(Certificate certificate, RestUploadCertificateDTO inputDTO,
			String username) {
		boolean fileExist = false;
		Certificate supplementaryFor = this.certificateService.findCertificateByCertNo(inputDTO.getSupplementaryFor());
		Contact technician = contactService.getByHrId(inputDTO.getHrid());
		byte[] fileData = Base64.getDecoder().decode(inputDTO.getContent());
		Job job = certificate.getCal().getLinks().iterator().next().getJi().getJob();
		SystemComponent sc = this.systemComponentService.findComponent(Component.CERTIFICATE);

		File directory = componentDirectoryService.getDirectory(sc, job.getJobno());
		String fileName = certificate.getCertno() + ".pdf";
		String errorMessage = null;
		try {
			fileExist = writeFile(directory, fileName, fileData);
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}

		if (errorMessage != null) {
			return new RestResultDTO<>(false, errorMessage);
		}

		Contact contact = this.userService.get(username).getCon();
		certificate.setOptimization(inputDTO.getOptimization());
		certificate.setAdjustment(inputDTO.getAdjustment());
		certificate.setRestriction(inputDTO.getRestriction());
		certificate.setRepair(inputDTO.getRepair());
		if (!certificate.getStatus().equals(CertStatusEnum.SIGNED)) {
			// Issue Date is an optional parameter, allowing issue date to be
			// specified if needed, otherwise uses today's date
			Date issueDate = DateTools.dateFromLocalDate(inputDTO.getIssueDate());
			Date startDate = DateTools.dateFromZonedDateTime(inputDTO.getStartDate()); 
			
			this.certificateService.signRestCertificate(certificate, technician != null ? technician : contact,
					issueDate, inputDTO.getThirdCertificateNumber(), inputDTO.getTimeSpent(),
					startDate);
		}
		else {
			// If issue date has been provided 
			// (but not performing "signing", we want to update the cert date directly)
			Date issueDate = DateTools.dateFromLocalDate(inputDTO.getIssueDate());
			if (issueDate != null) {
				certificate.setCertDate(issueDate);
			}
		}

		// set status 'REPLACED' for old certificate
		boolean isReplaced;
		if (supplementaryFor != null) {
			supplementaryFor.setStatus(CertStatusEnum.REPLACED);
			isReplaced = true;
		} else isReplaced = fileExist;

		// send event for upload certificate
		certificate.getCal().getLinks().stream().map(CalLink::getJi).forEach(ji -> {
			UploadCertificateEvent uploadCertEvent = new UploadCertificateEvent(this, certificate, isReplaced, ji);
			applicationEventPublisher.publishEvent(uploadCertEvent);
		});

		return new RestResultDTO<>(true, "");
	}

	/*
	 * Attempts to write file to destination, returning error message if there
	 * is an exception
	 */
	private boolean writeFile(File directory, String fileName, byte[] fileData) throws Exception {
		File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));

		boolean exist = file.exists();

		FileOutputStream os = new FileOutputStream(file);
		IOUtils.write(fileData, os);
		os.flush();
		os.close();
		logger.info("Wrote " + fileData.length + " bytes to " + file.getAbsolutePath());
		return exist;
	}
}
