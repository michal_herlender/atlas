package org.trescal.cwms.rest.alligator.dto.output;

import java.util.Locale;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

/*
 * Auto-instantiated component that converts Instrument into a DTO and provides localized 
 * information for the specified Locale.
 * 
 * Author: Galen Beck - 2016-01-21
 */
@Component
public class RestInstrumentDTOFactoryImpl implements RestInstrumentDTOFactory {

	@Autowired
	private TranslationService translationService;

	@Override
	public RestInstrumentDTO convert(Instrument instrument, Locale locale) {
		RestInstrumentDTO dto = new RestInstrumentDTO();
		dto.setCustomerDescription(instrument.getCustomerDescription());
		dto.setFormerBarcode(instrument.getFormerBarCode());
		dto.setJobItemId(getJobItemId(instrument));
		dto.setManufacturer(instrument.getDefinitiveMfrName());
		if (instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
	            dto.setModel(instrument.getModelname());
	        } else {
	            dto.setModel(instrument.getModel().getModel());
	        }
		dto.setPlantId(instrument.getPlantid());
		dto.setPlantNumber(instrument.getPlantno());
		dto.setSerialNumber(instrument.getSerialno());
		dto.setSubFamily(getSubFamily(instrument, locale));
		return dto;
	}

	@Override
	public RestInstrumentDTO convert(JobItem jobItem, Locale locale) {
		RestInstrumentDTO dto = new RestInstrumentDTO();
		Instrument instrument = jobItem.getInst();
		dto.setCustomerDescription(instrument.getCustomerDescription());
		dto.setFormerBarcode(instrument.getFormerBarCode());
		dto.setJobItemId(jobItem.getJobItemId());
		dto.setManufacturer(instrument.getDefinitiveMfrName());
		dto.setModel(instrument.getModel().getModel());
		dto.setPlantId(instrument.getPlantid());
		dto.setPlantNumber(instrument.getPlantno());
		dto.setSerialNumber(instrument.getSerialno());
		dto.setSubFamily(getSubFamily(instrument, locale));
		return dto;
	}

	private String getSubFamily(Instrument instrument, Locale locale) {
		Description description = instrument.getModel().getDescription();
		return translationService.getCorrectTranslation(description.getTranslations(), locale);
	}

	/*
	 * Return latest active job item with an active ID
	 */
	private int getJobItemId(Instrument instrument) {
		Set<JobItem> jobItems = instrument.getJobItems();
		int result = 0;
		for (JobItem jobItem : jobItems) {
			if (jobItem.getState().getActive())
				result = jobItem.getJobItemId();
		}
		return result;
	}
}