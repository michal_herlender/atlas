package org.trescal.cwms.rest.alligator.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestStartCalibrationService;
import org.trescal.cwms.rest.alligator.component.RestStartCalibrationValidator;
import org.trescal.cwms.rest.alligator.component.RestWorkRequirementsComponent;
import org.trescal.cwms.rest.alligator.dto.input.RestWorkRequirementIdsDTO;
import org.trescal.cwms.rest.alligator.dto.output.RestStartCalibrationResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.util.*;

@Controller
@RestJsonController
@Slf4j
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class RestStartCalibrationController {
    public static final String REQUEST_MAPPING = "/startcalibration";

    @Autowired
    private RestStartCalibrationValidator validator;

	@Autowired
	private RestStartCalibrationService restStartCalibrationService;

	@Autowired
	private RestWorkRequirementsComponent restWorkRequirements;

	@Autowired
	private UserService userService;
	

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<RestStartCalibrationResultDTO> startCalibration(
			@RequestBody RestWorkRequirementIdsDTO inputDTO,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) {
        log.debug("Start calibration for work requirements: " + inputDTO.getWorkRequirementIds().toString());

        // Step 1 - Look up logged in contact
        Contact startedBy;
        if (inputDTO.getTechnicianUsername() == null)
            startedBy = this.userService.get(username).getCon();
        else
            startedBy = this.userService.get(inputDTO.getTechnicianUsername()).getCon();

        // Step 2 - Lookup Work Requirements from supplied IDs
        StringBuffer errorMessage = new StringBuffer();
		Map<Integer, JobItemWorkRequirement> reqMap = restWorkRequirements.lookup(inputDTO.getWorkRequirementIds(),
				errorMessage, locale);
		if (errorMessage.length() > 0) {
            return new RestResultDTO<>(false, errorMessage.toString());
        }
		Collection<JobItemWorkRequirement> jobItemWorkReqs = reqMap.values();

		// Step 3 - If calibration is already in progress for all work
		// requirements,
		// with appropriate calibration and certificate ready, return its
		// information!
		if (validator.isCalibrationStartedAsGroup(jobItemWorkReqs)) {
			return existingCalibration(jobItemWorkReqs);
		}

		// Step 4 - Validate that Work Requirements are startable (as a group)
		if (!validator.isCalibrationStartableAsGroup(jobItemWorkReqs, startedBy, false)) {
            return new RestResultDTO<>(false,
                validator.getNotStartableReasons(jobItemWorkReqs, startedBy, false, locale));
        }

		// Step 5 - Start Calibration and return result
		return createCalibration(jobItemWorkReqs, startedBy, inputDTO.getStartDate(), inputDTO.getProcessID(),
				inputDTO.getCalDataContent());
	}

	/*
	 * Starts the calibration and returns success object
	 */
	protected RestResultDTO<RestStartCalibrationResultDTO> createCalibration(
			Collection<JobItemWorkRequirement> jobItemWorkRequirements, Contact startedBy, Date startDate,
			Integer processID, Integer calDataContent) {
        RestResultDTO<RestStartCalibrationResultDTO> dto = new RestResultDTO<>(true, "");
        RestStartCalibrationResultDTO result = new RestStartCalibrationResultDTO();

        Calibration calibration = this.restStartCalibrationService.startCalibration(jobItemWorkRequirements, startedBy,
            startDate, processID, calDataContent);

        Certificate certificate = calibration.getCerts().get(0);
        result.setCalibrationId(calibration.getId());
        result.setDocumentNumber(certificate.getCertno());
        dto.addResult(result);
        return dto;
	}

	/*
	 * Returns information about existing started calibration
	 */
	protected RestResultDTO<RestStartCalibrationResultDTO> existingCalibration(
			Collection<JobItemWorkRequirement> jobItemWorkRequirements) {
        RestResultDTO<RestStartCalibrationResultDTO> dto = new RestResultDTO<>(true, "");
        RestStartCalibrationResultDTO result = new RestStartCalibrationResultDTO();

        // Can get open calibration from first work requirement
        JobItemWorkRequirement jiwr = jobItemWorkRequirements.iterator().next();
        // Calibration is the LAST calibration in cal links
        Iterator<CalLink> calLinks = jiwr.getJobitem().getCalLinks().iterator();
        CalLink calLink = null;
        while (calLinks.hasNext())
            calLink = calLinks.next();
        assert calLink != null;
        Calibration calibration = calLink.getCal();

        result.setCalibrationId(calibration.getId());
        result.setDocumentNumber(calibration.getCerts().get(0).getCertno());
        dto.addResult(result);
        return dto;
    }
}
