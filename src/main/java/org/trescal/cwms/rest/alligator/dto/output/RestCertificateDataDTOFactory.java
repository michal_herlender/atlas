package org.trescal.cwms.rest.alligator.dto.output;

import java.util.Locale;

import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

/*
 * Interface for factory component to generate CertificateDataDTO
 * 
 * Author: Galen Beck 2016-01-20
 */
public interface RestCertificateDataDTOFactory {
	RestCertificateDataDTO convert(Certificate certificate, Locale locale);
	boolean canConvert(Certificate certificate);
	String getConversionErrorMessage(Certificate certificate, Locale locale);
}
