package org.trescal.cwms.rest.alligator.component;

/**
 * Message codes and default messages used in Rest Web Services (some shared
 * between classes so added here to prevent duplication)
 */
public class RestErrors {

	public static final String CODE_ERROR_SAVE = "error.save";
	public static final String MESSAGE_ERROR_SAVE = "One or more errors are present, please see below:";

	// Remainder of file sorted by message code

	public static final String CODE_INSTRUMENTS_FOUND = "error.rest.addstandards.instrumentsfound";
	public static final String MESSAGE_INSTRUMENTS_FOUND = "{0} instruments found for barcode {1}";

	public static final String CODE_BARCODE_BLANK = "error.rest.barcode.blank";
	public static final String CODE_JOBITEMID_BLANK = "error.rest.jobitemid.blank";
	public static final String MESSAGE_BARCODE_BLANK = "You must enter a non-blank barcode";
	public static final String MESSAGE_JOBITEMID_BLANK = "You must enter a non-blank and non-zero job item id";

	public static final String CODE_PLANT_NUMBER_BLANK = "error.rest.plantnumber.blank";
	public static final String MESSAGE_PLANT_NUMBER_BLANK = "You must enter a non-blank plant number";

	public static final String CODE_CAL_NOT_FULL_CALIBRATION = "error.rest.calibration.notfullcalibration";
	public static final String MESSAGE_CAL_NOT_FULL_CALIBRATION = "Calibration is not a main calibration";

	public static final String CODE_CAL_NOT_IN_PROGRESS = "error.rest.calibration.notinprogress";
	public static final String MESSAGE_CAL_NOT_IN_PROGRESS = "Calibration is not in progress nor in hold";

	public static final String CODE_CAL_NOT_IN_HOLD = "error.rest.calibration.notinhold";
	public static final String MESSAGE_CAL_NOT_IN_HOLD = "Calibration is not in hold";

	public static final String CODE_CAL_NOT_AUTHORIZED = "error.rest.calibration.notauthorized";
	public static final String MESSAGE_CAL_NOT_AUTHORIZED = "{0} is not authorised for capability {1} and calibration type {2}";

	public static final String CODE_CAL_DATA_NOT_DEFINED = "error.rest.caldatacontent.notdefined";
	public static final String MESSAGE_CAL_DATA_NOT_DEFINED = "Calibration Data Content {0} not defined";

	public static final String CODE_CALIBRATION_OUTCOME_NOT_DEFINED = "error.rest.caloutcome.notdefined";
	public static final String MESSAGE_CALIBRATION_OUTCOME_NOT_DEFINED = "Calibration Outcome {0} not defined";

	public static final String CODE_CERTIFICATE_NOT_FOUND = "error.rest.certificate.notfound";
	public static final String MESSAGE_CERTIFICATE_NOT_FOUND = "Certificate {0} not found";

	public static final String CODE_CERTIFICATE_NO_CAL = "error.rest.certificate.nocal";
	public static final String MESSAGE_CERTIFICATE_NO_CAL = "Certificate has no calibration";

	public static final String CODE_CERTIFICATE_STATUS_NOT_FOUND = "error.rest.certificate.status.notfound";
	public static final String MESSAGE_CERTIFICATE_STATUS_NOT_FOUND = "Certificate Status with name {0} is not found";

	public static final String CODE_CERTIFICATES_EMPTY_LIST = "error.rest.certificates.empty.list";
	public static final String MESSAGE_CERTIFICATES_EMPTY_LIST = "Certificates files list is empty";

	public static final String CODE_SUBDIV_NOT_FOUND = "error.rest.subdiv.notfound";
	public static final String MESSAGE_SUBDIV_NOT_FOUND = "Subdiv not found";

	public static final String CODE_CERTIFICATE_NO_JOB_ITEM = "error.rest.certificate.nojobitem";
	public static final String MESSAGE_NO_JOB_ITEM = "Certificate has no Job Item link(s)";

	public static final String CODE_DATA_NOT_FOUND = "error.rest.data.notfound";
	public static final String MESSAGE_DATA_NOT_FOUND = "Data not found: {0}";

	public static final String CODE_SUBDIV_DEFAULT_CONTACT = "error.rest.subdiv.no_default_contact";
	public static final String MESSAGE_SUBDIV_DEFAULT_CONTACT = "the subdivision with the id {0} has no default contact";

	public static final String CODE_DATA_NOT_AUTHORIZED = "error.rest.data.notauthorized";
	public static final String MESSAGE_DATA_NOT_AUTHORIZED = "Access to data not authorized: {0}";

	public static final String CODE_DATA_INVALID = "error.rest.data.invalid";
	public static final String MESSAGE_DATA_INVALID = "Data invalid: {0}";

	public static final String CODE_DATA_REQUIRED = "error.rest.data.required";
	public static final String MESSAGE_DATA_REQUIRED = "Data required";

	public static final String CODE_CONTACT_NOT_RESOLVED = "error.rest.data.contact_not_resolved";
	public static final String MESSAGE_CONTACT_NOT_RESOLVED = "Logged in contact not resolved";

	public static final String CODE_JOB_ITEM_NOT_FOUND = "error.rest.workrequirement.job_item_not_found";
	public static final String MESSAGE_JOB_ITEM_NOT_FOUND = "Job item {0} not found";

	public static final String CODE_JOB_ITEM_ACTIVE = "error.rest.jobitem.active";
	public static final String MESSAGE_JOB_ITEM_ACTIVE = "Job item {0} is currently active for instrument {1}";

	public static final String CODE_INSTRUMENT_PASTDUE = "error.rest.addstandards.pastdue";
	public static final String MESSAGE_INSTRUMENT_PASTDUE = "Instrument {0} with barcode {1} is past due";

	public static final String CODE_INSTRUMENT_OTHER_COMPANY = "error.rest.addstandards.othercompany";
	public static final String MESSAGE_INSTRUMENT_OTHER_COMPANY = "Instrument {0} with barcode {1} belongs to another company";

	public static final String CODE_NOT_STANDARD = "error.rest.addstandards.notstandard";
	public static final String MESSAGE_NOT_STANDARD = "Instrument {0} with barcode {1} is not a calibration standard";

	public static final String CODE_INSTRUMENT_SCRAPPED = "error.rest.addstandards.scrapped";
	public static final String MESSAGE_INSTRUMENT_SCRAPPED = "Instrument {0} with barcode {1} is scrapped";

	public static final String CODE_INSTRUMENT_BER = "error.rest.addstandards.ber";
	public static final String MESSAGE_INSTRUMENT_BER = "Instrument {0} with barcode {1} has B.E.R. status";

	public static final String CODE_MODEL_IS_MFR_SPECIFIC = "error.rest.model.mfrspecific";
	public static final String MESSAGE_MODEL_IS_MFR_SPECIFIC = "The instrument model is manufacturer specific, so a manufacturer cannot be specified for the instrument";

	public static final String CODE_MODEL_UNSELECTABLE = "error.rest.model.type.unselectable";
	public static final String MESSAGE_MODEL_UNSELECTABLE = "The instrument model type cannot be used on an instrument";

	public static final String CODE_SUBDIV_NO_DEFAULT_CONTACT = "error.rest.subdiv.nodefaultcontact";
	public static final String MESSAGE_SUBDIV_NO_DEFAULT_CONTACT = "The subdivision does not have a default contact";

	public static final String CODE_SUFFIX_NOT_PDF = "error.rest.suffix.notpdf";
	public static final String MESSAGE_SUFFIX_NOT_PDF = "File {0} does not have a PDF or pdf suffix";

	public static final String CODE_FILE_NAME_NOT_VALIDE = "error.rest.file.name.notvalid";
	public static final String MESSAGE_FILE_NAME_NOT_VALIDE = "File Name {0} not valid";

	public static final String CODE_WR_NONE = "error.rest.workrequirements.none";
	public static final String MESSAGE_WR_NONE = "No Work Requirements Specified";

	public static final String CODE_WR_NOT_SET = "error.rest.workrequirements.notset";
	public static final String MESSAGE_WR_NOT_SET = "Next Work Requirement not set for job item id : {0}";

	public static final String CODE_WR_NOT_FOUND = "error.rest.workrequirements.notfound";
	public static final String MESSAGE_WR_NOT_FOUND = "Work Requirement IDs Not Found: {0}";

	public static final String CODE_WR_NOT_AWAITING_CALIBATION = "error.rest.workrequirement.notawaitingcalibration";
	public static final String MESSAGE_WR_NOT_AWAITING_CALIBATION = "Work Requirement {0} not awaiting calibration";

	public static final String CODE_WR_NO_PROCEDURE = "error.rest.workrequirement.noprocedure";
	public static final String MESSAGE_WR_NO_PROCEDURE = "Work Requirement {0} has no capability";

	public static final String CODE_WR_NO_SERVICE_TYPE = "error.rest.workrequirement.noservicetype";
	public static final String MESSAGE_WR_NO_SERVICE_TYPE = "Work requirement {0} has no service type";

	public static final String CODE_WR_COMPLETE = "error.rest.workrequirement.complete";
	public static final String MESSAGE_WR_COMPLETE = "Work Requirement {0} already complete";
	
	public static final String CODE_WR_CANCELLED = "error.rest.workrequirement.cancelled";
	public static final String MESSAGE_WR_CANCELLED = "Work Requirement {0} already cancelled";

	public static final String CODE_WR_DIFFERENT_JOBS = "error.rest.workrequirement.differentjobs";
	public static final String MESSAGE_WR_DIFFERENT_JOBS = "Work Requirements are on different jobs";

	public static final String CODE_WR_DIFFERENT_CAL_FREQ = "error.rest.workrequirement.differentcalfrequencies";
	public static final String MESSAGE_WR_DIFFERENT_CAL_FREQ = "Work Requirements are for instruments with different cal frequencies";

	public static final String CODE_EXTENSION_NOT_VALID = "error.rest.common.uploadcertificate.extensionnotvalid";
	public static final String MESSAGE_EXTENSION_NOT_VALID = "Extension is not valid";

	public static final String CODE_FILE_SIZE = "error.rest.common.uploadcertificate.filesizelimitexceeded";
	public static final String MESSAGE_FILE_SIZE = "File Size is larger than 20Mb";

	public static final String CODE_BUSINESS_COMPANY_SETTINGS_NOT_FOUND = "error.rest.plantillas.client.update.businesssettings.notfound";
	public static final String MESSAGE_BUSINESS_COMPANY_SETTINGS_NOT_FOUND = "Business Settings Not Found";

	public static final String CODE_BUSINESS_PLANTILLAS_SETTINGS_NOT_FOUND = "error.rest.plantillas.client.update.plantillassettings.notfound";
	public static final String MESSAGE_BUSINESS_PLANTILLAS_SETTINGS_NOT_FOUND = "Plantillas Settings Not Found";

	public static final String CODE_FILE_NOT_FOUND = "error.rest.common.file.notfound";
	public static final String MESSAGE_FILE_NOT_FOUND = "File not found";

	public static final String CODE_ERROR_READING_FILE = "error.rest.common.file.errorreading";
	public static final String MESSAGE_ERROR_READING_FILE = "Error reading file";

	public static final String CODE_INSTRUMENT_NOT_FOUND = "error.rest.common.instrument.notfound";
	public static final String MESSAGE_INSTRUMENT_NOT_FOUND = "Instrument with id {0} is not found";

	public static final String CODE_ADVESO_DOCUMENT_TYPE_NOT_SUPPORTED = "error.rest.adveso.document.type.notsupported";
	public static final String MESSAGE_ADVESO_DOCUMENT_TYPE_NOT_SUPPORTED = "Document type: {0} is not supported";

	public static final String CODE_ACTIVITY_NOT_FOUND = "error.rest.adveso.activity.not.found";
	public static final String MESSAGE_ACTIVITY_NOT_FOUND = "Activity in queue with id {0} not found";

	public static final String CODE_NOTIFICATION_NOT_FOUND = "error.rest.adveso.notification.not.found";
	public static final String MESSAGE_NOTIFICATION_NOT_FOUND = "Notification in queue with id {0} not found";

	public static final String CODE_COMPANY_NOT_FOUND = "error.rest.tlm.company.not.found";
	public static final String MESSAGE_COMPANY_NOT_FOUND = "Company not found";

	public static final String CODE_ADDRESS_NOT_FOUND = "error.rest.tlm.address.not.found";
	public static final String MESSAGE_ADDRESS_NOT_FOUND = "Address not found";

	public static final String CODE_WR_NOT_FULLCALIBRATION = "error.rest.tlm.startcalibration.recalltype.not.fullcalibration";
	public static final String MESSAGE_WR_NOT_FULLCALIBRATION = "the recall type of the calibration type doesn't have a FULL_CALIBRTION status, for workrequirement id : {0}";

	public static final String CODE_USERNAME_AND_HRID_EMPTY = "error.rest.tlm.calibration.hridandusername.empty";
	public static final String MESSAGE_USERNAME_AND_HRID_EMPTY = "The username and hrid are both empty, atleast provide one!";

	public static final String CODE_JOB_SUPPORT_FORNOW_JUST_ONE_FR = "error.rest.tlm.fr.only.one.fr.supported";
	public static final String MESSAGE_JOB_SUPPORT_FORNOW_JUST_ONE_FR = "Only one failure report per jobitem is supported for now";

	public static final String CODE_FR_VALIDATED_MISSING_DATA = "error.rest.tlm.fr.validated.missing.data";
	public static final String MESSAGE_FR_VALIDATED_MISSING_DATA = "Failure Report validated, but state or recommandation is null";

	public static final String CODE_NOT_AWAITING_TO_BE_SENT = "error.rest.tlm.fr.jobitem.notawaiting.tobesent";
	public static final String MESSAGE_NOT_AWAITING_TO_BE_SENT = "Jobitem is not in the state 'Awaiting selection of Fault Report outcome'";

	public static final String CODE_FR_DOESNT_RECOMMEND_REPAIR = "error.rest.tlm.fr.doesnt.recommend.repair";
	public static final String MESSAGE_FR_DOESNT_RECOMMEND_REPAIR = "The failure report doesnt recommend a repair";

	public static final String CODE_THIRDPARTY_CERT_NO_ALREADY_EXISTS = "error.rest.common.certificate.create.thirdpartycertno.exists";
	public static final String MESSAGE_THIRDPARTY_CERT_NO_ALREADY_EXISTS = "The certificate already exists with the same third party certNo : {0}";

	public static final String CODE_PREBOOKING_DATA_MISSING = "error.rest.prebooking.validated.missing.data";
	public static final String MESSAGE_PREBOOKING_DATA_MISSING = "Please select Trescal ID and if it is null, select at least model and customer description";

	public static final String CODE_WR_NOT_COMPLETED = "error.rest.workrequirement.notcomplete";
	public static final String MESSAGE_WR_NOT_COMPLETED = "Work requiement {0} should be completed";

	public static final String CODE_JOB_COMPLETED = "error.rest.job.completed";
	public static final String MESSAGE_JOB_COMPLETED = "Job {0} is already completed";

	public static final String CODE_JOBITEM_STATE_COMPLETED = "error.rest.jobitem.state.inactive";
	public static final String MESSAGE_JOBITEM_STATE_COMPLETED = "Jobitem state {0} is inactive";

	public static final String CODE_JOBITEM_IS_IN_TRANSIT = "error.rest.jobitem.state.intransit";
	public static final String MESSAGE_JOBITEM_IS_IN_TRANSIT = "Jobitem {0} is in transit";

	public static final String CODE_CAL_ADDR_DIFF_CURR_ADDR = "error.rest.jobitem.caladdress.different";
	public static final String MESSAGE_CAL_ADDR_DIFF_CURR_ADDR = "Calibration address is different than Current address";

	public static final String CODE_CURRENT_STATE_NOT_VALID = "error.rest.jobitem.currentstate.notvalid";
	public static final String MESSAGE_CURRENT_STATE_NOT_VALID = "Current jobitem state {0} is not valid";

	public static final String CODE_CERTIFICATE_NOT_INHOUSE = "error.rest.certificate.notinhousetype";
	public static final String MESSAGE_CERTIFICATE_NOT_INHOUSE = "The certificate is not of type CERT_INHOUSE";

	public static final String CODE_INSTRUMENT_PLANT_NO_MUST_BE_SPECIFIED = "error.instrument.plantno";
	public static final String MESSAGE_INSTRUMENT_PLANT_NO_MUST_BE_SPECIFIED = "A plant no must be specified for the instrument";

	public static final String CODE_JOBITEM_CURRENT_ADDRESS_NULL = "error.rest.jobitem.currentaddress.null";
	public static final String MESSAGE_JOBITEM_CURRENT_ADDRESS_NULL = "Jobitem {0} has no current address";

	public static final String CODE_INVOICE_NUMBER_CREDIT_NOTE_NULL = "error.rest.invoice.credit.note.null";
	public static final String MESSAGE_INVOICE_NUMBER_CREDIT_NOTE_NULL = "At least one of invoice number or credit note number must be specified";
	
	public static final String CODE_INVOICE_NOT_FOUND = "error.rest.invoice.not.found";
	public static final String MESSAGE_INVOICE_NOT_FOUND = "Invoice not found";
	
	public static final String CODE_INVOICE_UNAUTHORIZED = "error.rest.invoice.unauthorized";
	public static final String MESSAGE_INVOICE_UNAUTHORIZED = "The user is not authorized to receive the invoice";
	
	public static final String CODE_CREDIT_NOTE_NOT_FOUND = "error.rest.credit.note.not.found";
	public static final String MESSAGE_CREDIT_NOTE_NOT_FOUND = "Credit note not found";
	
	public static final String CODE_CREDIT_NOTE_UNAUTHORIZED = "error.rest.credit.note.unauthorized";
	public static final String MESSAGE_CREDIT_NOTE_UNAUTHORIZED = "The user is not authorized to receive the credit note";
	
	public static final String CODE_SUBDIV_ID_NULL = "error.rest.subdivision.subdivid";
	public static final String MESSAGE_SUBDIV_ID_NULL = "At least one of subdivId or subdivFiscalId must be specified";

}
