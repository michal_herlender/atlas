package org.trescal.cwms.rest.alligator.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.dto.output.RestWorkRequirementDTO;
import org.trescal.cwms.rest.alligator.dto.output.RestWorkRequirementDTOFactory;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

/*
 * REST web service controller that returns WorkRequirements for the active job
 * belonging to the instrument specified by the plantid parameter
 * Author: Galen Beck - 2016-01-21
 */
@Controller @RestJsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class RestWorkRequirementsController {
	public static final String REQUEST_MAPPING = "/workrequirements";
	public static final String REQUEST_PARAM_JOBITEM_ID = "jobitemid";
	
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestWorkRequirementDTOFactory dtoFactory;
	@Autowired
	private UserService userService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET,
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestWorkRequirementDTO> findByPlantId(Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, 
			@RequestParam(value=REQUEST_PARAM_JOBITEM_ID) int jobItemId) {
		Contact contact = this.userService.get(username).getCon(); 
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		if (jobItem == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, null, RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<RestWorkRequirementDTO>(false, message);
		}
		RestResultDTO<RestWorkRequirementDTO> result = new RestResultDTO<>(true, "");
		for (JobItemWorkRequirement jobItemWorkReq: jobItem.getWorkRequirements()) {
			RestWorkRequirementDTO dto = dtoFactory.convert(jobItemWorkReq, contact, locale);
			result.addResult(dto);
		}
		
		return result;
	}
}