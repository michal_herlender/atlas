package org.trescal.cwms.rest.alligator.dto.output;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleInterval;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.rest.alligator.component.RestStartCalibrationValidator;

import java.util.Locale;

@Component
public class RestWorkRequirementDTOFactoryImpl implements RestWorkRequirementDTOFactory {

	@Autowired
	private RestStartCalibrationValidator restCalibrationService;
	@Autowired
	private RecallRuleService recallRuleService;

	/**
	 * Conversion method used by regular (Alligator / Generic) calls 
	 */
	@Override
	public RestWorkRequirementDTO convert(JobItemWorkRequirement jobItemWorkReq, Contact contact, Locale locale) {
		RestWorkRequirementDTO dto = new RestWorkRequirementDTO();
		if (contact != null) {
			dto.setCanStart(restCalibrationService.isCalibrationStartable(jobItemWorkReq, contact, false));
		} else {
			dto.setCanStart(false);
		}
		dto.setDescription(getDescription(jobItemWorkReq));
		dto.setProcedures(getProcedures(jobItemWorkReq));
		dto.setWorkRequirementId(jobItemWorkReq.getId());
		return dto;
	}

	/**
	 * Conversion method used by TLM calls where a lot of other information has been added 
	 */
	@Override
	public RestWorkRequirementDTO convert(JobItemWorkRequirement jobItemWorkReq, Locale locale) {
		RestWorkRequirementDTO dto = new RestWorkRequirementDTO();
		dto.setCanStart(restCalibrationService.isCalibrationStartable(jobItemWorkReq));
		dto.setDescription(getDescription(jobItemWorkReq));
		dto.setProcedures(getProcedures(jobItemWorkReq));
		dto.setWorkRequirementId(jobItemWorkReq.getId());

		WorkRequirement wr = jobItemWorkReq.getWorkRequirement();
		if (wr.getServiceType() != null) {
            dto.setServiceTypeId(wr.getServiceType().getServiceTypeId());
            RecallRuleInterval rci = recallRuleService.getCurrentRecallInterval(jobItemWorkReq.getJobitem().getInst(),
                RecallRequirementType.SERVICE_TYPE, jobItemWorkReq.getWorkRequirement().getServiceType());
            if (rci.getInterval() != 0) {
                dto.setCalInterval(rci.getInterval());
                dto.setCalIntervalUnit(rci.getUnit().toString());
            } else {
                Instrument inst = jobItemWorkReq.getJobitem().getInst();
                dto.setCalInterval(inst.getCalFrequency());
                dto.setCalIntervalUnit(inst.getCalFrequencyUnit().toString());
            }
        }

        if (wr.getCapability() != null && wr.getCapability().getSubfamily() != null
            && wr.getCapability().getSubfamily().getTmlid() != null)
            dto.setSubFamilyTMLId(wr.getCapability().getSubfamily().getTmlid());
        if (StringUtils.isNotEmpty(wr.getRequirement()))
            dto.setRequirementText(wr.getRequirement());
        if (wr.getCapability() != null)
            dto.setBusinessSubdivId(wr.getCapability().getOrganisation().getSubdivid());

        if (jobItemWorkReq.getJobitem().getNextWorkReq() != null)
            dto.setIsNext(jobItemWorkReq.getJobitem().getNextWorkReq() == jobItemWorkReq);
        return dto;
    }

	/*
	 * Per the web service requirements document (enough for technician to
	 * identify): Barcode + S/N + Manufacturer Name + Model Number (maybe we should
	 * split into separate fields later?)
	 */
	@Override
	public String getDescription(JobItemWorkRequirement jobItemWorkReq) {
		StringBuffer result = new StringBuffer();
		result.append(jobItemWorkReq.getJobitem().getInst().getPlantid());
		result.append(" ");
		result.append(jobItemWorkReq.getJobitem().getInst().getSerialno());
		result.append(" ");
		result.append(jobItemWorkReq.getJobitem().getInst().getDefinitiveMfrName());
		result.append(" ");
		result.append(jobItemWorkReq.getJobitem().getInst().getModel().getModel());
		return result.toString();
	}

	/*
	 * Per the web service requirements document (enough for technician to
	 * identify): Procedure Number (Reference) + Procedure Name TODO if using work
	 * instruction, separate into different field?
	 */
	@Override
	public String getProcedures(JobItemWorkRequirement jobItemWorkReq) {
        StringBuffer result = new StringBuffer();
        Capability capability = jobItemWorkReq.getWorkRequirement().getCapability();

        if (capability != null) {
            result.append(capability.getReference());
            result.append(" ");
            result.append(capability.getName());
        }
        return result.toString();
    }
}
