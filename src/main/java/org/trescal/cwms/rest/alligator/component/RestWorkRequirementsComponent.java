package org.trescal.cwms.rest.alligator.component;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;


public interface RestWorkRequirementsComponent {
	/**
	 * Looks up specified ID list of work requirements, returning a map of ids to instances.
	 * @param inputList
	 * @param errorMessage
	 * @param locale
	 * @return Map<Integer id to JobItemWorkRequirement instance>
	 */
	Map<Integer, JobItemWorkRequirement> lookup(List<Integer> inputList, StringBuffer errorMessage, Locale locale);
}
