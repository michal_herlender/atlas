package org.trescal.cwms.rest.alligator.dto.output;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.businesssubdivisionsettings.BusinessSubdivisionSettings;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.rest.alligator.component.RestErrors;

import java.util.*;

@Component
public class RestCertificateDataDTOFactoryImpl implements RestCertificateDataDTOFactory {
    // Note - some static fields defined here which may need to become business
    // company / company settings over time.
    public final String DEFAULT_LANGUAGE = "es_ES";
    public final String SECOND_LANGUAGE = "en_GB";
    public final String TRESCAL_WEBSITE = "www.trescal.com";
    public final String CAL_FREQUENCY_YEARS = "YEARS";
    public final String CAL_FREQUENCY_MONTHS = "MONTHS";
    public final String CAL_FREQUENCY_WEEKS = "WEEKS";
    public final String CAL_FREQUENCY_DAYS = "DAYS";
    // Static message codes and default messages
    public final String CODE_CALIBRATION_CERTIFICATE = "calibrationcertificate";
    public final String MESSAGE_CALIBRATION_CERTIFICATE = "Calibration Certificate";

    // private final Logger logger =
    // LoggerFactory.getLogger(RestCertificateDataDTOFactoryImpl.class);

    @Autowired
	private MessageSource messageSource;

	@Autowired
	private RestCertificateInstrumentDTOFactory instrumentDTOFactory;

	@Autowired
	private RestCertificateTechnicianDTOFactory technicianDTOFactory;

	@Autowired
	private RestWorkRequirementDTOFactory wrDTOFactory;

	/*
	 * Indicates if there are any errors preventing conversion
	 */
	@Override
	public boolean canConvert(Certificate certificate) {
		if (getFirstLinkedJobItem(certificate) == null)
			return false;
		return true;
	}

	@Override
	public String getConversionErrorMessage(Certificate certificate, Locale locale) {
		StringBuffer result = new StringBuffer();
		if (getFirstLinkedJobItem(certificate) == null) {
			result.append(messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NO_JOB_ITEM, null,
					RestErrors.CODE_CERTIFICATE_NO_JOB_ITEM, locale));
		}
		return result.toString();
	}

	/*
	 * Note, layout below corresponds to web service document and DTO object
	 */
	@Override
	public RestCertificateDataDTO convert(Certificate certificate, Locale locale) {
		RestCertificateDataDTO result = new RestCertificateDataDTO();
		JobItem jobItem = getFirstLinkedJobItem(certificate);
		if (jobItem != null) {
			result.setJobID(jobItem.getJob().getJobid());
			result.setJobGroupID(getJobGroupID(jobItem));
			result.setJobNumber(jobItem.getJob().getJobno());
			result.setClientCompanyID(jobItem.getJob().getCon().getSub().getComp().getCoid());
			result.setClientSubdivID(jobItem.getJob().getCon().getSub().getSubdivid());
			result.setApplicantName(getApplicantName(jobItem));
			result.setApplicantAddress(getApplicantAddress(jobItem));
			result.setApplicantPostalCode(getApplicantPostalCode(jobItem));
			result.setApplicantCity(getApplicantCity(jobItem));
			result.setApplicantProvinceState(getApplicantProvinceState(jobItem));
			result.setApplicantCountry(getApplicantCountry(jobItem, locale));
		}
		result.setDefaultLanguage(getDefaultLanguage(certificate));
		result.setSecondLanguage(getSecondLanguage(certificate));
		result.setLanguage(locale.toString());

		result.setCertificateTitle(getCertificateTitle(locale));
		result.setCertificateNumber(certificate.getCertno());
		result.setAccreditationNumber(getAccreditationNumber(certificate));
		result.setCertificateStatus(getCertificateStatus(certificate));
		result.setCertificateStatusTranslation(getCertificateStatusTranslation(certificate, locale));

		result.setLaboratoryName(getLaboratoryName(certificate));
		result.setLaboratoryDepartments(getLaboratoryDepartments(certificate));
		result.setLaboratoryAddress(getLaboratoryAddress(certificate));
		result.setLaboratoryPostalCode(getLaboratoryPostalCode(certificate));
		result.setLaboratoryCity(getLaboratoryCity(certificate));
		result.setLaboratoryProvinceState(getLaboratoryProvinceState(certificate));
		result.setLaboratoryCountry(getLaboratoryCountry(certificate, locale));
		result.setLaboratoryFax(getLaboratoryFax(certificate));
		result.setLaboratoryPhone(getLaboratoryPhone(certificate));
		result.setLaboratoryEmail(getLaboratoryEmail(certificate));
		result.setLaboratoryWebsite(getLaboratoryWebsite(certificate));

		result.setTrescalWebsite(getTrescalWebsite());
		result.setInstruments(this.instrumentDTOFactory.convert(certificate, locale));

		result.setCalibrationStartDate(getCalibrationStartDate(certificate));
		result.setCalibrationEndDate(getCalibrationEndDate(certificate));
		result.setCalibrationDate(getCalibrationDate(certificate));
		result.setCalibrationFrequency(certificate.getDuration());
		result.setCalibrationFrequencyUnits(getUnits(certificate.getUnit()));

		result.setTechnicians(this.technicianDTOFactory.convert(certificate));
		result.setCalibrationProcedures(getCalibrationProcedures(certificate));
		result.setWorkingStandards(getWorkingStandards(certificate));
		result.setWorkRequirements(getWorkRequirements(certificate, locale));
		result.setCertificateContent(getCertificateContent(certificate));

		result.setIsAccredited(getIsAccredited(certificate));
		result.setIsAsFound(getIsAsFound(certificate));
		result.setIsAsLeft(getIsAsLeft(certificate));

		return result;
	}

	private JobItem getFirstLinkedJobItem(Certificate certificate) {
		JobItem result = null;
		if (certificate.getLinks() != null) {
			Iterator<CertLink> links = certificate.getLinks().iterator();
			if (links.hasNext()) {
				CertLink certLink = links.next();
				result = certLink.getJobItem();
			}
		}
		return result;
	}

	// Returns a list of WorkRequirementDTOs (incomplete and non cancelled)
	// for WIs relevant to this calibration (for completing calibration)
	private List<RestWorkRequirementDTO> getWorkRequirements(Certificate certificate, Locale locale) {
        List<RestWorkRequirementDTO> result = new ArrayList<>();
        if ((certificate.getCal() != null) && (certificate.getCal().getCapability() != null)) {
            Capability proc = certificate.getCal().getCapability();
            for (CertLink certLink : certificate.getLinks()) {
                JobItemWorkRequirement jiwr = certLink.getJobItem().getNextWorkReq();
                if (jiwr != null && !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
                    && !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED)) {
                    Capability wrProc = jiwr.getWorkRequirement().getCapability();
                    if (wrProc != null) {
                        if (proc.getId() == wrProc.getId()) {
                            RestWorkRequirementDTO dto = wrDTOFactory.convert(jiwr, null, locale);
                            result.add(dto);
                        }
                    }
                }
            }
        }
        return result;
    }

	// Assumed to be the default language of the
	protected String getDefaultLanguage(Certificate certificate) {
		return DEFAULT_LANGUAGE;
	}

	protected String getSecondLanguage(Certificate certificate) {
		return SECOND_LANGUAGE;
	}

	protected int getJobGroupID(JobItem jobItem) {
		int jobGroupId = 0;
		if (jobItem.getGroup() != null) {
			jobGroupId = jobItem.getGroup().getId();
		}
		return jobGroupId;
	}

	protected String getCertificateTitle(Locale locale) {
		return messageSource.getMessage(CODE_CALIBRATION_CERTIFICATE, null, MESSAGE_CALIBRATION_CERTIFICATE, locale);
	}

	protected String getAccreditationNumber(Certificate certificate) {
		// If cal type is an accredited cal type, return via linked procedure,
		// otherwise blank
		String result = "";
		if ((certificate.getCal() != null) && certificate.getCal().getCalType().getAccreditationSpecific()) {
            if (certificate.getCal().getCapability() != null) {
                AccreditedLab lab = certificate.getCal().getCapability().getLabAccreditation();
                if (lab != null) {
                    result = lab.dto().getValue();
                }
            }
        }
		return result;
	}

	protected String getLaboratoryName(Certificate certificate) {
		String result = "";
		if (certificate.getCal() != null) {
			result = certificate.getCal().getCalAddress().getSub().getComp().getConame();
		}
		return result;
	}

	protected List<String> getLaboratoryDepartments(Certificate certificate) {
		List<String> result = new ArrayList<>();
		if (certificate.getCal() != null) {
			result.add(certificate.getCal().getCalAddress().getSub().getSubname());
		}
		return result;
	}

	protected List<String> getLaboratoryAddress(Certificate certificate) {
		List<String> result = null;
		if (certificate.getCal() != null) {
			result = getAddressList(certificate.getCal().getCalAddress());
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	protected String getLaboratoryPostalCode(Certificate certificate) {
		String result = "";
		if (certificate.getCal() != null) {
			result = certificate.getCal().getCalAddress().getPostcode();
		}
		return result;
	}

	protected String getLaboratoryCity(Certificate certificate) {
		String result = "";
		if (certificate.getCal() != null) {
			result = certificate.getCal().getCalAddress().getTown();
		}
		return result;
	}

	protected String getLaboratoryProvinceState(Certificate certificate) {
		String result = "";
		if (certificate.getCal() != null) {
			result = certificate.getCal().getCalAddress().getCounty();
		}
		return result;
	}

	protected String getLaboratoryCountry(Certificate certificate, Locale locale) {
		String result = "";
		if (certificate.getCal() != null) {
			Country country = certificate.getCal().getCalAddress().getCountry();
			result = (new Locale("", country.getCountryCode())).getDisplayCountry(locale);
		}
		return result;
	}

	// TODO need to obtain this
	protected String getLaboratoryFax(Certificate certificate) {
		return "";
	}

	// TODO need to obtain this
	protected String getLaboratoryPhone(Certificate certificate) {
		return "";
	}

	// TODO need to obtain this
	protected String getLaboratoryEmail(Certificate certificate) {
		return "";
	}

	// TODO need to store this (local website)
	protected String getLaboratoryWebsite(Certificate certificate) {
		return "";
	}

	protected String getTrescalWebsite() {
		return TRESCAL_WEBSITE;
	}

	protected List<String> getAddressList(Address address) {
		List<String> result = new ArrayList<>();
		if (address.getAddr1().length() > 0)
			result.add(address.getAddr1());
		if (address.getAddr2().length() > 0)
			result.add(address.getAddr2());
		if (address.getAddr3().length() > 0)
			result.add(address.getAddr3());
		return result;
	}

	protected String getApplicantName(JobItem jobItem) {
		if (jobItem.getOnBehalf() != null) {
			return jobItem.getOnBehalf().getCompany().getConame();
		} else {
			return jobItem.getJob().getCon().getSub().getComp().getConame();
		}
	}

	/**
	 * Certificate address logic : 
	 * 1) Use the on-behalf-of address if set, otherwise
	 * 2) Use the return-to address on the job (this is used as a general "client" address)
	 * Note, the job item "return-to" address is not relevant to the certificate address and must not be used here.
	 */
	private Address getAddress(JobItem jobItem) {
		if ((jobItem.getOnBehalf() != null) && (jobItem.getOnBehalf().getAddress() != null)) {
			return jobItem.getOnBehalf().getAddress();
		} else {
			return jobItem.getJob().getReturnTo();
		}
	}

	protected List<String> getApplicantAddress(JobItem jobItem) {
		return getAddressList(getAddress(jobItem));
	}

	protected String getApplicantPostalCode(JobItem jobItem) {
		return (getAddress(jobItem)).getPostcode();
	}

	protected String getApplicantCity(JobItem jobItem) {
		return (getAddress(jobItem)).getTown();
	}

	protected String getApplicantProvinceState(JobItem jobItem) {
		return (getAddress(jobItem)).getCounty();
	}

	protected String getApplicantCountry(JobItem jobItem, Locale locale) {
		Country country = (getAddress(jobItem)).getCountry();
		return (new Locale("", country.getCountryCode())).getDisplayCountry(locale);
	}

	protected Date getCalibrationStartDate(Certificate certificate) {
		Date result = null;
		if (certificate.getCal() != null) {
			// Note, startTime is stored as a TemporalType.TIMESTAMP in the
			// database.
			result = certificate.getCal().getStartTime();
		}
		return result;
	}

	protected Date getCalibrationEndDate(Certificate certificate) {
		Date result = null;
		if (certificate.getCal() != null) {
			// Note, completeTime is stored as a TemporalType.TIMESTAMP in the
			// database.
			result = certificate.getCal().getCompleteTime();
		}
		return result;
	}

	protected Date getCalibrationDate(Certificate certificate) {
		Date result = null;
		if (certificate.getCal() != null) {
			// Note, calDate is stored as a TemporalType.DATE in the database.
			// Returning as a proper java.util.Date to ensure regular
			// serialization
			result = new Date(certificate.getCalDate().getTime());
		}
		return result;
	}

	protected List<String> getCalibrationProcedures(Certificate certificate) {
		List<String> result = new ArrayList<>();
		if (certificate.getCal() != null) {
            BusinessSubdivisionSettings settings = certificate.getCal().getOrganisation().getBusinessSettings();
            Capability capability = certificate.getCal().getCapability();
            boolean preferWorkInstructionReference = settings != null && settings.getWorkInstructionReference();
            String procedureReference = null;
            if (preferWorkInstructionReference) {
                WorkInstruction wi = certificate.getCal().getWorkInstruction();
                // Use the work instruction (title), if available and set
                if (wi != null) {
                    procedureReference = wi.getTitle();
                }
                // Otherwise, use the default work instruction (title) of the capability, if set
                else if (capability != null && capability.getDefWorkInstruction() != null) {
                    procedureReference = capability.getDefWorkInstruction().getTitle();
				}
			}
			if (procedureReference == null && capability != null) {
				// Otherwise the default is to provide a capability reference;
				procedureReference = capability.getReference();
			}
			if (procedureReference != null) {
				result.add(procedureReference);
			}
		}
		return result;
	}

	/*
	 * TODO: Discuss whether we need more information than just the plantid of the
	 * standard
	 */
	protected List<String> getWorkingStandards(Certificate certificate) {
		List<String> result = new ArrayList<>();
		if (certificate.getCal() != null) {
			for (StandardUsed standard : certificate.getCal().getStandardsUsed()) {
				result.add(String.valueOf(standard.getInst().getPlantid()));
			}
		}
		return result;
	}

	protected String getCertificateContent(Certificate certificate) {
		String result = null;
		if (certificate.getCal() != null) {
			result = certificate.getCal().getCalClass().getDesc();
		}
		return result;
	}

	protected Boolean getIsAccredited(Certificate certificate) {
		boolean result = false;
		if (certificate.getCal() != null) {
			result = certificate.getCal().getCalType().getAccreditationSpecific();
		}
		return result;
	}

	protected Boolean getIsAsFound(Certificate certificate) {
		Boolean result = false;
		if (certificate.getCal() != null) {
			if (certificate.getCal().getCalClass().equals(CalibrationClass.AS_FOUND))
				result = true;
			else if (certificate.getCal().getCalClass().equals(CalibrationClass.AS_FOUND_AND_AS_LEFT))
				result = true;
		}
		return result;
	}

	protected Boolean getIsAsLeft(Certificate certificate) {
		Boolean result = false;
		if (certificate.getCal() != null) {
			if (certificate.getCal().getCalClass().equals(CalibrationClass.AS_LEFT))
				result = true;
			else if (certificate.getCal().getCalClass().equals(CalibrationClass.AS_FOUND_AND_AS_LEFT))
				result = true;
		}
		return result;
	}

	protected String getUnits(IntervalUnit unit) {
		String result = CAL_FREQUENCY_MONTHS;
		if (unit != null) {
			switch (unit) {
			case YEAR:
				result = CAL_FREQUENCY_YEARS;
				break;
			case MONTH:
				result = CAL_FREQUENCY_MONTHS;
				break;
			case WEEK:
				result = CAL_FREQUENCY_WEEKS;
				break;
			case DAY:
				result = CAL_FREQUENCY_DAYS;
				break;
			default:
				break;
			}
		}
		return result;
	}

	protected String getCertificateStatus(Certificate certificate) {
		String result = CertStatusEnum.UNDEFINED.name();
		if (certificate.getStatus() != null) {
			return certificate.getStatus().name();
		}
		return result;
	}

	protected String getCertificateStatusTranslation(Certificate certificate, Locale locale) {
		if (certificate.getStatus() != null)
			return certificate.getStatus().getMessage();
		else
			return "";
	}
}