package org.trescal.cwms.rest.alligator.dto.input;

import java.util.Date;
import java.util.List;

public class RestWorkRequirementIdsDTO {
	List<Integer> workRequirementIds;
	private String technicianUsername;
	private Date startDate;
	private Integer processID;
	private Integer calDataContent;

	public List<Integer> getWorkRequirementIds() {
		return workRequirementIds;
	}

	public void setWorkRequirementIds(List<Integer> workRequirementIds) {
		this.workRequirementIds = workRequirementIds;
	}

	public String getTechnicianUsername() {
		return technicianUsername;
	}

	public void setTechnicianUsername(String technicianUsername) {
		this.technicianUsername = technicianUsername;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getProcessID() {
		return processID;
	}

	public void setProcessID(Integer processID) {
		this.processID = processID;
	}

	public Integer getCalDataContent() {
		return calDataContent;
	}

	public void setCalDataContent(Integer calDataContent) {
		this.calDataContent = calDataContent;
	}

}
