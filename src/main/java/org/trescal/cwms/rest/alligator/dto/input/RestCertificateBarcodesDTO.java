package org.trescal.cwms.rest.alligator.dto.input;

import java.util.List;

public class RestCertificateBarcodesDTO {
	private String certificateNumber; 
	private List<String> barcodes;

	public List<String> getBarcodes() {
		return barcodes;
	}

	public void setBarcodes(List<String> barcodes) {
		this.barcodes = barcodes;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
}
