package org.trescal.cwms.rest.alligator.component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AlligatorLabelFormatter {

    /**
     * A "safe" way to get the SimpleDateFormat as not all date formats may work for all locales,
     * according to the javadocs for SimpleDateFormat.
     *
     * @return SimpleDateFormat or default if problem occurs
     */
    private SimpleDateFormat getSimpleDateFormat(String pattern, Locale locale) {
        try {
            return new SimpleDateFormat(pattern, locale);
        } catch (Exception e) {
            if (locale == null) log.error("Locale was null", e);
            if (pattern == null) log.error("pattern was null", e);
            if (locale != null && pattern != null)
                log.error("Error creating SimpleDateFormat for pattern " + pattern + " and locale " + locale);
            return new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        }
    }

    public String formatLocalDate(LocalDate date, String pattern, Locale locale) {
        String result = "";
        if (date != null) {
            result = date.format(DateTimeFormatter.ofPattern(pattern, locale));
        }
        return result;
    }

    public String formatDate(Date date, String pattern, Locale locale) {
        String result = "";
        if (date != null) {
            SimpleDateFormat df = getSimpleDateFormat(pattern, locale);
            result = df.format(date);
        }
        return result;
    }

}
