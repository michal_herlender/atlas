package org.trescal.cwms.rest.alligator.dto.output;

public class RestStartCalibrationResultDTO {
	private int calibrationId;
	private String documentNumber;
	public int getCalibrationId() {
		return calibrationId;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setCalibrationId(int calibrationId) {
		this.calibrationId = calibrationId;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
}
