package org.trescal.cwms.rest.alligator.dto.output;

import java.util.Locale;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

/*
 * Interface for factory component to generate InstrumentDTO
 * 
 * Author: Galen Beck 2016-01-20
 */
public interface RestInstrumentDTOFactory {

	RestInstrumentDTO convert(Instrument instrument, Locale locale);

	RestInstrumentDTO convert(JobItem jobItem, Locale locale);
}