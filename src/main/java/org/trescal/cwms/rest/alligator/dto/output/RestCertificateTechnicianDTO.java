package org.trescal.cwms.rest.alligator.dto.output;

public class RestCertificateTechnicianDTO {
	private String technicianName;
	private String technicianJobTitle;
	public String getTechnicianName() {
		return technicianName;
	}
	public String getTechnicianJobTitle() {
		return technicianJobTitle;
	}
	public void setTechnicianName(String technicianName) {
		this.technicianName = technicianName;
	}
	public void setTechnicianJobTitle(String technicianJobTitle) {
		this.technicianJobTitle = technicianJobTitle;
	}
}
