package org.trescal.cwms.rest.alligator.dto.output;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

@Component
public class RestCertificateInstrumentDTOFactoryImpl implements RestCertificateInstrumentDTOFactory {

	@Autowired
	private TranslationService translationService;

	@Override
	public List<RestCertificateInstrumentDTO> convert(Certificate certificate, Locale locale) {
		List<RestCertificateInstrumentDTO> result = new ArrayList<>();
		if (certificate.getLinks() != null) {
			for (CertLink link : certificate.getLinks()) {
				result.add(convert(link.getJobItem(), locale));
			}
		}
		return result;
	}

	private RestCertificateInstrumentDTO convert(JobItem jobItem, Locale locale) {
		RestCertificateInstrumentDTO dto = new RestCertificateInstrumentDTO();
		Instrument instrument = jobItem.getInst();

		dto.setCustomerDescription(getEmptyStringIfNull(instrument.getCustomerDescription()));
		dto.setFormerBarcode(getEmptyStringIfNull(instrument.getFormerBarCode()));
		dto.setJobItemClientReference(getEmptyStringIfNull(jobItem.getClientRef()));
		dto.setJobItemID(jobItem.getJobItemId());
		dto.setJobItemNumber(jobItem.getItemNo());
		dto.setJobItemClientReference(getEmptyStringIfNull(jobItem.getClientRef()));
		dto.setManufacturer(instrument.getDefinitiveMfrName());
		dto.setModel(getModelNumber(instrument));
		dto.setPlantID(instrument.getPlantid());
		dto.setPlantNumber(getEmptyStringIfNull(instrument.getPlantno()));
		dto.setSerialNumber(instrument.getSerialno());
		dto.setSubFamily(translationService
				.getCorrectTranslation(instrument.getModel().getDescription().getTranslations(), locale));

		return dto;
	}

	private String getModelNumber(Instrument instrument) {
		if ((instrument.getModelname() != null) && !instrument.getModelname().isEmpty()) {
			return instrument.getModelname();
		} else {
			return instrument.getModel().getModel();
		}
	}

	private String getEmptyStringIfNull(String input) {
		return (input == null) ? "" : input;
	}
}