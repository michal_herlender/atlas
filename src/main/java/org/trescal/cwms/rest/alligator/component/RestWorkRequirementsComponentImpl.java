package org.trescal.cwms.rest.alligator.component;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;

@Component
public class RestWorkRequirementsComponentImpl implements RestWorkRequirementsComponent {

	@Autowired
	private JobItemWorkRequirementService jobItemWorkRequirementService;

	@Autowired
	private MessageSource messageSource;

	/*
	 * Finds specified WorkRequirements by ID and populates into list.
	 * If any are not found an error object is returned (otherwise null = no error)  
	 */
	@Override
	public Map<Integer, JobItemWorkRequirement> lookup(List<Integer> workRequirementIds, 
			StringBuffer errorMessage, Locale locale) {
		if ((workRequirementIds == null) || workRequirementIds.isEmpty()) {
			errorMessage.append(this.messageSource.getMessage(RestErrors.CODE_WR_NONE, null, RestErrors.MESSAGE_WR_NONE, locale));
			return null;
		}
		Map<Integer, JobItemWorkRequirement> workRequirements = new HashMap<>();
		StringBuffer notFoundIds = new StringBuffer();
		for (Integer id : workRequirementIds) {
			JobItemWorkRequirement workRequirement = this.jobItemWorkRequirementService.findJobItemWorkRequirement(id);
			if (workRequirement == null) {
				if (notFoundIds.length() > 0) notFoundIds.append(", ");
				notFoundIds.append(id);
			}
			else {
				workRequirements.put(id, workRequirement);
			}
		}
		if (notFoundIds.length() > 0) {
			errorMessage.append(this.messageSource.getMessage(RestErrors.CODE_WR_NOT_FOUND, new Object[]{notFoundIds.toString()}, RestErrors.CODE_WR_NOT_FOUND, locale));
			return null;
		}
		else {
			return workRequirements;
		}
	}}
