package org.trescal.cwms.rest.alligator.dto.output;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

public interface RestCertificateInstrumentDTOFactory {
	List<RestCertificateInstrumentDTO> convert(Certificate certificate, Locale locale);
}
