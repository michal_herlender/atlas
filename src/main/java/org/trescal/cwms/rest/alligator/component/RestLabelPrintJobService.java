package org.trescal.cwms.rest.alligator.component;

import java.util.Collection;
import java.util.Locale;

import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.labelprinter.enums.AlligatorLabelType;
import org.trescal.cwms.core.tools.labelprinting.alligator.xml.XmlAlligatorPrintJob;

public interface RestLabelPrintJobService {
	XmlAlligatorPrintJob getPrintJob(Collection<Certificate> certs, int copies, AlligatorLabelType type, 
			LabelPrinter labelPrinter, Locale locale, boolean printAllInstruments);
	
	XmlAlligatorPrintJob getPrintJob(Collection<CertLink> certLinks, int copies, AlligatorLabelType type, 
			LabelPrinter labelPrinter, Locale locale);	
}
