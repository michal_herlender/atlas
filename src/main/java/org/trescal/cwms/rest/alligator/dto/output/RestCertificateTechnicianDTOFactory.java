package org.trescal.cwms.rest.alligator.dto.output;

import java.util.List;

import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

public interface RestCertificateTechnicianDTOFactory {
	List<RestCertificateTechnicianDTO> convert(Certificate certificate);
}
