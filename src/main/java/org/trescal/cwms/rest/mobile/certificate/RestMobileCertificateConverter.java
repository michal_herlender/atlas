package org.trescal.cwms.rest.mobile.certificate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.util.Locale;


@Component
public class RestMobileCertificateConverter {
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private MessageSource messageSource;
	
	public RestResultDTO<RestMobileCertificateDto> convertSingle(Certificate certificate, Locale locale) {
        RestResultDTO<RestMobileCertificateDto> result;
        if (this.certificateService.getCertificateFileExists(certificate)) {
            // get file
            String base64Data = certificateService.readCertificateFileIntoBase64Format(certificate);
            if (StringUtils.isEmpty(base64Data)) {
                String message = messageSource.getMessage(RestErrors.CODE_ERROR_READING_FILE, new Object[]{},
                    RestErrors.MESSAGE_ERROR_READING_FILE, locale);
                result = new RestResultDTO<>(false, message);
            } else if (base64Data.equals(RestErrors.CODE_FILE_SIZE)) {
                String message = messageSource.getMessage(RestErrors.CODE_FILE_SIZE, new Object[]{},
						RestErrors.MESSAGE_FILE_SIZE, locale);
				result = new RestResultDTO<>(false, message);
			}
			else {
                String certificateFileName = certificate.getCertno() + ".pdf";

                RestMobileCertificateDto dto = convertWithoutFile(certificate);
                dto.setBase64FileData(base64Data);
                dto.setCertificateFileName(certificateFileName);
                result = new RestResultDTO<>(true, "");
                result.addResult(dto);
            }
		}
		else {
			String message = messageSource.getMessage(RestErrors.CODE_FILE_NOT_FOUND, new Object[] {},
					RestErrors.MESSAGE_FILE_NOT_FOUND, locale);
			result = new RestResultDTO<>(false, message);
		}
		return result;
	}
	
	public RestMobileCertificateDto convertWithoutFile(Certificate certificate) {

        RestMobileCertificateDto dto = new RestMobileCertificateDto();
        dto.setBase64FileData(null);
        dto.setCertificateFileName(null);
        dto.setCertificateId(certificate.getCertid());
        dto.setCertificateNumber(certificate.getCertno());
        dto.setCalDate(certificate.getCalDate());
        return dto;
    }
	
	
}
