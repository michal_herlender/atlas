package org.trescal.cwms.rest.mobile.instrument;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.rest.mobile.certificate.RestMobileCertificateDto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class RestMobileInstrumentDto {

	private Integer plantid;
	private String manufacturer;
	private String modelName;
	private String serialno;
	private String plantno;
	private String customerDescription;
	private Date lastCalDate;
	private LocalDate nextCalDueDate;
	private LocalDate extensionDate;
	private String lastCertNo;
	private Integer lastCertId;
	private Boolean active;
	private String activeText;
	private String calVerificationStatus;
	private String instrumentStatus;
	private String usageType;
	private String customerAcceptanceCriteria;
	private BigDecimal deviation;
	private String deviationUnit;
	private String owner;
	private String subdivisionName;
	private String location;
	private String clientRemarks;
	private List<RestMobileCertificateDto> recentCertificates;

}
