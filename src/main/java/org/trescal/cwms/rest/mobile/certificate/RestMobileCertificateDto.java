package org.trescal.cwms.rest.mobile.certificate;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class RestMobileCertificateDto {

    private Integer certificateId;
    private String certificateNumber;
    private String certificateFileName;
    private String base64FileData;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date calDate;

    public Date getCalDate() {
        return calDate;
    }

    public void setCalDate(Date calDate) {
        this.calDate = calDate;
    }

    public Integer getCertificateId() {
        return certificateId;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public String getCertificateFileName() {
        return certificateFileName;
	}
	public String getBase64FileData() {
		return base64FileData;
	}
	public void setCertificateId(Integer certificateId) {
		this.certificateId = certificateId;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public void setCertificateFileName(String certificateFileName) {
		this.certificateFileName = certificateFileName;
	}
	public void setBase64FileData(String base64FileData) {
		this.base64FileData = base64FileData;
	}
}
