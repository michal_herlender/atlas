package org.trescal.cwms.rest.mobile.instrument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db.PermitedCalibrationExtensionService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.rest.mobile.certificate.RestMobileCertificateConverter;
import org.trescal.cwms.rest.mobile.certificate.RestMobileCertificateDto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;


@Component
public class RestMobileInstrumentConverter {
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private PermitedCalibrationExtensionService calibrationExtensionService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestMobileCertificateConverter certificateConverter;

	public RestMobileInstrumentDto convert(Instrument instrument, Locale locale) {


		List<Certificate> certs = this.certificateService.getCertificatesForInstrument(instrument);
		List<RestMobileCertificateDto> dtoList = new ArrayList<>();

		for (int i = certs.size() - 1; i > certs.size() - 6; i--) {
			if (i < 0)
				break;
			Certificate certificate = certs.get(i);

			RestMobileCertificateDto certDto = certificateConverter.convertWithoutFile(certificate);
			dtoList.add(certDto);
		}
		
		// Note; method will have poor performance on large cert counts; should refactor
				// For now, just retrieve the certificate once to help minimize queries
		Certificate certificate = certificateService.getLatestCertificatesForInstrument(instrument);
		
		RestMobileInstrumentDto dto = new RestMobileInstrumentDto();
		dto.setCustomerDescription(getCustomerDescription(instrument, locale));
		dto.setLastCalDate(getLastCalDate(certificate));
		dto.setManufacturer(formatManufacturer(instrument));
		dto.setModelName(formatModelName(instrument));
		dto.setRecentCertificates(dtoList);
		
		// TODO - discuss how to provide cal extension period? 
		dto.setExtensionDate(getExtensionDate(instrument));
		dto.setNextCalDueDate(instrument.getNextCalDueDate());
		dto.setPlantid(instrument.getPlantid());
		dto.setPlantno(instrument.getPlantno());
		dto.setSerialno(instrument.getSerialno());
		dto.setLastCertNo(getLastCertNo(certificate));
		dto.setLastCertId(getLastCertId(certificate));
		// TODO add indication of whether to display calibration verification status in user property
		dto.setCalVerificationStatus(getCalVerificationStatus(instrument, certificate, locale));
		boolean instrumentActive = getActive(instrument);
		dto.setActive(instrumentActive);
		dto.setActiveText(getActiveText(instrumentActive, locale));

		// TODO discuss whether below three fields should be included?
		dto.setInstrumentStatus( instrument.getStatus().getStatusForLocale(locale) );
		dto.setUsageType(translationService.getCorrectTranslation(instrument.getUsageType().getNametranslation(), locale ));
		dto.setCustomerAcceptanceCriteria(getCustomerAcceptanceCriteria(instrument));
		// TODO discuss whether deviation included in mobile info
		dto.setDeviation(getLastCertDeviation(certificate));
		dto.setDeviationUnit(getDeviationUnit(instrument));
		
		dto.setOwner(instrument.getCon().getFirstName() + " " + instrument.getCon().getLastName());
		dto.setSubdivisionName(instrument.getAdd().getSub().getSubname());
		dto.setLocation(getLocation(instrument));
		dto.setClientRemarks(getRemarks(instrument));
		return dto;
	}

	private String formatManufacturer(Instrument instrument) {
		String result;
		if (!instrument.getModel().getMfr().getGenericMfr()) {
			result = instrument.getModel().getMfr().getName();
		} else {
			result = instrument.getMfr().getName();
		}
		return result;
	}

	private String formatModelName(Instrument instrument) {
		String result;
		if (!instrument.getModel().getModel().equals("/")) {
			result = instrument.getModel().getModel();
		} else if (instrument.getModelname() != null && !instrument.getModelname().equals("/")) {
			result = instrument.getModelname();
		} else {
			result = "";
		}
		return result;
	}
	
	/*
	 * Calculates a notion of "active" based on the status and usage type.  
	 * For the moment we want just In_Circulation status considered as "Active"
	 */
	private boolean getActive(Instrument instrument) {
		boolean active = false;
		if (InstrumentStatus.IN_CIRCULATION.equals(instrument.getStatus())) {
			if (instrument.getUsageType() != null &&
				instrument.getUsageType().getRecall()) {
				active = true;
			}
		}
		return active;
	}
	
	private String getActiveText(boolean active, Locale locale) {
		String result;
		if (active) {
			result = this.messageSource.getMessage("active", null, "Active", locale);
		} else {
			result = this.messageSource.getMessage("inactive", null, "Inactive", locale);
		}
		return result;
	}
	 
	private Date getLastCalDate(Certificate certificate) {
		Date result = null;
		if (certificate != null) {
			result = certificate.getCalDate();
		}
		return result;
	}
	
	private String getLastCertNo(Certificate certificate) {
		String result = null;
		if (certificate != null) {
			result= certificate.getCertno();
		}
		return result;
	}
	
	private Integer getLastCertId(Certificate certificate) {
		Integer result = null;
		if (certificate != null) {
			result = certificate.getCertid();
		}
		return result;
	}
	
	private String getCalVerificationStatus(Instrument instrument, Certificate certificate, Locale locale) {
		// Manages Cal verification status override
		String result = null;
		if (certificate != null) {
			if(instrument.getInstrumentComplementaryField() != null && instrument.getInstrumentComplementaryField().getCalibrationStatusOverride() != null) {
                result = instrument.getInstrumentComplementaryField().getCalibrationStatusOverride().getName();
			} else if (certificate.getCalibrationVerificationStatus() != null) {
				result = certificate.getCalibrationVerificationStatus().getNameForLocale(locale);
			} else {
				result = "";
			}
		}
		return result;
	}

	private LocalDate getExtensionDate(Instrument instrument) {
		LocalDate result = null;
		PermitedCalibrationExtension permitedExtension = calibrationExtensionService.getExtensionForInstrument(instrument);
		if (permitedExtension != null) {
			result = permitedExtension.getExtensionEndDate();
		}

		return result;
	}

	private BigDecimal getLastCertDeviation(Certificate certificate) {
		BigDecimal result = null;
		if (certificate != null) {
			if (certificate.getDeviation() != null) {
				result = certificate.getDeviation();
			}
		}
		return result;
	}
	
	private String getCustomerDescription(Instrument instrument, Locale locale) {
		String result;
		if (instrument.getCustomerDescription() != null && !instrument.getCustomerDescription().isEmpty()) {
			result = instrument.getCustomerDescription();
		} else {
			Set<Translation> translations = instrument.getModel().getDescription().getTranslations();
			result = this.translationService.getCorrectTranslation(translations, locale);
		}
		return result;
	}
	
	private String getDeviationUnit(Instrument instrument) {
		String result = "";
		if (instrument.getInstrumentComplementaryField() != null &&
				instrument.getInstrumentComplementaryField().getDeviationUnits() != null) {
			result = instrument.getInstrumentComplementaryField().getDeviationUnits().getSymbol();
		}
		return result;
	}

	private String getCustomerAcceptanceCriteria(Instrument instrument) {
		String result = "";
		if (instrument.getInstrumentComplementaryField() != null &&
				instrument.getInstrumentComplementaryField().getCustomerAcceptanceCriteria() != null) {
			result = instrument.getInstrumentComplementaryField().getCustomerAcceptanceCriteria(); 
		}
		return result;
	}
	
	private String getLocation(Instrument instrument){
		String result="";
		if(instrument.getLoc() !=null){
			result=instrument.getLoc().getLocation();
			
		}
		return result;
	}
	
	private String getRemarks(Instrument instrument){
		String result="";
		if(instrument.getInstrumentComplementaryField() != null &&
				instrument.getInstrumentComplementaryField().getClientRemarks() != null){
			result=instrument.getInstrumentComplementaryField().getClientRemarks();
		}
		return result;
	}
	
}
