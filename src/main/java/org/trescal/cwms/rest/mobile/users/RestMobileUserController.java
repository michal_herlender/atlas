package org.trescal.cwms.rest.mobile.users;

import java.security.Principal;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestMobileUserController {
	@Autowired
	private UserService userService;
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(path="/api/mobile/users/current")
	@GetMapping(produces = { Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<RestMobileUserDto> getCurentUser(Principal principal) {
		
		User user = this.userService.get(principal.getName());
		Locale locale = user.getCon().getLocale();
		
		RestResultDTO<RestMobileUserDto> result = new RestResultDTO<RestMobileUserDto>(true, "");
		RestMobileUserDto dto = new RestMobileUserDto();
		dto.setContactId(user.getCon().getPersonid());
		dto.setCompanyName(user.getCon().getSub().getComp().getConame());
		dto.setFullName(user.getCon().getName());
		dto.setLocale(locale.toLanguageTag());
		dto.setLanguage(user.getCon().getLocale().getDisplayLanguage(locale));
		dto.setSubdivisionName(user.getCon().getSub().getSubname());
		result.addResult(dto);
		
		return result;
	}
}
