package org.trescal.cwms.rest.mobile.instrument;

import java.security.Principal;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestMobileInstrumentController {
	
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private UserService userService;
	@Autowired
	private RestMobileInstrumentConverter converter;
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(path= "/api/mobile/instruments")
	@GetMapping(produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	
	public RestResultDTO<RestMobileInstrumentDto> getInstrument(
			@RequestParam(name="barcode",required=true) String barcode,
			Principal principal){
		RestResultDTO<RestMobileInstrumentDto> result = null;
		User user = this.userService.get(principal.getName());
		Locale locale = user.getCon().getLocale();
		if (!NumberTools.isAnInteger(barcode)) {
			String errorMessage = this.messageSource.getMessage(RestErrors.CODE_DATA_INVALID, 
					new Object[] {barcode}, RestErrors.MESSAGE_DATA_REQUIRED, locale);
			result = new RestResultDTO<>(false, errorMessage);
		}
		else {
			Integer plantid = Integer.valueOf(barcode);
			Instrument instrument = this.instrumentService.get(plantid);
			if (instrument == null) {
				String errorMessage = this.messageSource.getMessage(RestErrors.CODE_DATA_NOT_FOUND, 
						new Object[] {barcode}, RestErrors.MESSAGE_DATA_NOT_FOUND, locale);
				result = new RestResultDTO<>(false, errorMessage);
			}
			// For customer portal users, the user should be restricted to their own view rights; Trescal employees could see all
			else if (user.getWebUser() && !this.userGroupService.canUserViewThisInstrument(user.getCon(), instrument)) {
				String errorMessage = this.messageSource.getMessage(RestErrors.CODE_DATA_NOT_AUTHORIZED, 
						new Object[] {barcode}, RestErrors.MESSAGE_DATA_NOT_AUTHORIZED, locale);
				result = new RestResultDTO<>(false, errorMessage);
			}
			else {
				result = new RestResultDTO<>(true, "");
				result.addResult(converter.convert(instrument, locale));
			}
		}
		
		return result;
	}
	
	
	
}
