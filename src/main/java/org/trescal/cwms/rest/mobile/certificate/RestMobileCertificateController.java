package org.trescal.cwms.rest.mobile.certificate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@RestController
@RestJsonController
public class RestMobileCertificateController {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestMobileCertificateConverter converter;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private UserService userService;

	private static final Logger logger = LoggerFactory.getLogger(RestMobileCertificateController.class);

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(path = "/api/mobile/certificates/{id}")
	@GetMapping(produces = { Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<RestMobileCertificateDto> getCertificate(Principal principal,
			@PathVariable(name = "id") Integer certificateId) {
		RestResultDTO<RestMobileCertificateDto> result;
		logger.debug("Username : " + principal.getName());
		User user = this.userService.get(principal.getName());
		Locale locale = user.getCon().getLocale();

		// get certificate
		Certificate certificate = this.certificateService.findCertificate(certificateId);
		if (certificate == null || certificate.getStatus().equals(CertStatusEnum.DELETED)) {
			String errorMessage = this.messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND,
				new Object[]{certificateId}, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND, locale);
			result = new RestResultDTO<>(false, errorMessage);
		} 
		else {
			boolean authorized = !user.getWebUser() || getAuthorizedForCertificate(user, certificate);
			if (authorized) {
				// output dto
				result = converter.convertSingle(certificate, locale);
			} else {
				String errorMessage = this.messageSource.getMessage(RestErrors.CODE_DATA_NOT_AUTHORIZED,
					new Object[]{certificateId}, RestErrors.MESSAGE_DATA_NOT_AUTHORIZED, locale);
				result = new RestResultDTO<>(false, errorMessage);
			}
		}
		logger.debug("Returning certificate data for id "+certificateId);
		return result;
	}
	
	private boolean getAuthorizedForCertificate(User user, Certificate certificate) {
		// TODO get instrument IDs, contacts, subdiv, contacts in single
		// query?
		List<Instrument> instruments = new ArrayList<>();
		instruments.addAll(certificate.getLinks().stream().map(cl -> cl.getJobItem().getInst())
			.collect(Collectors.toList()));
		instruments.addAll(
			certificate.getInstCertLinks().stream().map(InstCertLink::getInst).collect(Collectors.toList()));
		// At least one connected instrument should be "authorized" by
		// the user
		boolean authorized = false;
		for (Instrument instrument : instruments) {
			boolean instrumentAuthorized = userGroupService.canUserViewThisInstrument(user.getCon(),
					instrument);
			if (instrumentAuthorized) {
				authorized = true;
				break;
			}
		}
		return authorized;
	}
}
