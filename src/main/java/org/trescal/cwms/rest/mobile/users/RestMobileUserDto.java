package org.trescal.cwms.rest.mobile.users;

public class RestMobileUserDto {
	private Integer contactId;
	private String companyName;
	private String fullName;
	private String subdivisionName;
	private String locale;
	private String language;
	
	public String getCompanyName() {
		return companyName;
	}
	public Integer getContactId() {
		return contactId;
	}
	public String getFullName() {
		return fullName;
	}
	public String getLocale() {
		return locale;
	}
	public String getLanguage() {
		return language;
	}
	public String getSubdivisionName() {
		return subdivisionName;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public void setSubdivisionName(String subdivisionName) {
		this.subdivisionName = subdivisionName;
	}
}
