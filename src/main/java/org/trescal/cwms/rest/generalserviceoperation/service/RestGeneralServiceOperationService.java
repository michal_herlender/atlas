package org.trescal.cwms.rest.generalserviceoperation.service;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationInputDTO;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationOutputDTO;

public interface RestGeneralServiceOperationService extends BaseService<GeneralServiceOperation, Integer> {
	
  RestResultDTO<GeneralServiceOperationOutputDTO> startGeneralServiceOperationForExternalSystem(GeneralServiceOperationInputDTO inputDTO,
			Subdiv allocatedSubdiv);
  
  RestResultDTO<GeneralServiceOperationOutputDTO> putGeneralServiceOperationOnHoldForExternalSystem(GeneralServiceOperationInputDTO inputDTO);

  RestResultDTO<GeneralServiceOperationOutputDTO> resumeGeneralServiceOperationForExternalSystem(
		GeneralServiceOperationInputDTO inputDTO);

  RestResultDTO<GeneralServiceOperationOutputDTO> completeGeneralServiceOperationForExternalSystem(
		GeneralServiceOperationInputDTO inputDTO);

  RestResultDTO<GeneralServiceOperationOutputDTO> cancelGeneralServiceOperationForExternalSystem(
		GeneralServiceOperationInputDTO inputDTO);

  RestResultDTO<GeneralServiceOperationOutputDTO> uploadGeneralServiceOperationDocumentForExternalSystem(
		GeneralServiceOperationInputDTO inputDTO, Subdiv allocatedSubdiv);
  
}
