package org.trescal.cwms.rest.generalserviceoperation.validator;

import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.db.GsoDocumentService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationInputDTO;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UploadGeneralServiceOperationDocumentInputDTOValidator extends AbstractBeanValidator {

    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private GeneralServiceOperationService gsoService;
    @Autowired
    private GsoDocumentService gsoDocumentService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;

    @Override
    public boolean supports(@NotNull Class<?> clazz) {
        return GeneralServiceOperationInputDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);

		GeneralServiceOperationInputDTO dto = (GeneralServiceOperationInputDTO) target;

		GeneralServiceOperation gso = null;
		if (dto.getGsoId() == null) {
			errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "General Service Operation ID" }, "General Service Operation ID is Mandatory");
		} else {

			gso = this.gsoService.get(dto.getGsoId());

			if (gso == null)
				errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.gsonotfound",
						new Object[] { dto.getGsoId() }, "General service operation with ID is not found");
			else if(StringUtils.isNotEmpty(dto.getHrid())){
				Contact con = this.contactService.getByHrId(dto.getHrid());
				if(con != null) {
                    val accreditationStatus = this.procAccServ.authorizedFor(gso.getCapability().getId(), gso.getCalType().getCalTypeId(), con.getPersonid());
                    if (!accreditationStatus.isRight())
                        errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.notaccredited",
                            new Object[]{dto.getGsoId()},
                            "The user is not accredited to perform this action for {0}");
                }
			}
		}

		if (StringUtils.isNotEmpty(dto.getSupplemantaryForNo())) {
			GsoDocument gsoDoc = this.gsoDocumentService.findGsoDocumentByDocumentNumber(dto.getSupplemantaryForNo());
			if(gsoDoc == null)
				errors.rejectValue("supplemantaryForNo", "generalserviceoperation.rest.errors.gsodocumentnotfound",
						new Object[] { dto.getSupplemantaryForNo() }, "Gso Document with Number {0} is not found");
		}

		if (StringUtils.isEmpty(dto.getFileContent()))
			errors.rejectValue("fileContent", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "File content" }, "File content is Mandatory");

		if (StringUtils.isEmpty(dto.getFileName()))
			errors.rejectValue("fileName", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "File name" }, "File name is Mandatory");

		if (CollectionUtils.isEmpty(dto.getJobitemIds())) {
			errors.rejectValue("jobitemIds", "generalserviceoperation.rest.errors.jobitemsempty",
					"Please mention at least one jobitem ID");
		} else if (gso != null) {
            List<Integer> notLinkedJiIds = new ArrayList<>();
            List<Integer> notReadyForDocUpJiIds = new ArrayList<>();
            List<String> invalidsupplementaryFor = new ArrayList<>();
            List<JobItem> linkedJIs = gso.getLinks().stream().map(GsoLink::getJi).collect(Collectors.toList());
            dto.getJobitemIds().forEach(jiId -> {
                JobItem ji = linkedJIs.stream().filter(e -> e.getJobItemId() == jiId).findFirst().orElse(null);
                if (ji == null)
                    notLinkedJiIds.add(jiId);
                else if (!this.jobItemService.isReadyForGeneralServiceOperationDocumentUpload(ji)) {
                    if (StringUtils.isNotEmpty(dto.getSupplemantaryForNo())) {
                        GsoDocument gsoDoc = this.gsoDocumentService.findGsoDocumentByDocumentNumber(dto.getSupplemantaryForNo());
                        if (gsoDoc == null)
                            notReadyForDocUpJiIds.add(jiId);
                        else if (gsoDoc.getLinks().stream().noneMatch(d -> d.getJi().equals(ji)))
                            invalidsupplementaryFor.add(dto.getSupplemantaryForNo());
					}
					else
						notReadyForDocUpJiIds.add(jiId);
				}
			});

			if (!CollectionUtils.isEmpty(notLinkedJiIds))
				errors.rejectValue("jobitemIds", "generalserviceoperation.rest.errors.gso.nolinkeditems",
						new Object[] { notLinkedJiIds }, "JobItem with IDs are not linked");

			if (!CollectionUtils.isEmpty(notReadyForDocUpJiIds))
				errors.rejectValue("jobitemIds",
						"generalserviceoperation.rest.errors.jobitemsnotreadyforuploaddocument",
						new Object[] { notReadyForDocUpJiIds },
						"The following jobitems are not ready for General service operation document upload");
			
			if (!CollectionUtils.isEmpty(invalidsupplementaryFor))
				errors.rejectValue("supplemantaryForNo",
						"generalserviceoperation.rest.errors.invalidsupplementaryfor",
						new Object[] { invalidsupplementaryFor, dto.getJobitemIds().get(0) },
						"The following supplementary for value {0} is invalid for this item {1}");
		}

		if (StringUtils.isEmpty(dto.getHrid()))
			errors.rejectValue("hrid", "generalserviceoperation.rest.errors.mandatory", new Object[] { "Hrid" },
					"hrid is Mandatory");
		else {
			Contact con = this.contactService.getByHrId(dto.getHrid());
			if (con == null)
				errors.rejectValue("hrid", "generalserviceoperation.rest.errors.contactnotfound",
						new Object[] { dto.getHrid() }, "Contact with Hrid not found" + dto.getHrid());
		}

		if (dto.getActionDate() == null) {
			errors.rejectValue("actionDate", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "Action Date" }, "actionDate is Mandatory");
		}
	}

}
