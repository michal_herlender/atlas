package org.trescal.cwms.rest.generalserviceoperation.dto;

import lombok.Data;

@Data
public class GeneralServiceOperationOutputDTO {

	private Integer gsoId;
	private String gsoStatus;
	private String gsoDocumentNumber;
	
}
