package org.trescal.cwms.rest.generalserviceoperation.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationDao;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocumentComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.db.GsoDocumentService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLinkComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLinkComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus.GeneralServiceOperationStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.fileupload2.service.UploadService;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CompleteGeneralServiceOperation;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_StartGeneralServiceOperation;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_UploadGSODocumentCancelAndReplace;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_UploadGeneralServiceOperationDocument;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_GeneralServiceOperation;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationInputDTO;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationItemOutcomeInputDTO;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationOutputDTO;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Base64;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Service("RestGeneralServiceOperationService")
public class RestGeneralServiceOperationServiceImpl extends BaseServiceImpl<GeneralServiceOperation, Integer>
	implements RestGeneralServiceOperationService {
	@Autowired
	private GeneralServiceOperationDao generalServiceOperationDao;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private HookInterceptor_StartGeneralServiceOperation hookInterceptor_startGeneralServiceOperation;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private ActionOutcomeService aoService;
	@Autowired
	private HookInterceptor_CompleteGeneralServiceOperation hookInterceptor_CompleteGSO;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private SystemComponentService systemCompServ;
	@Autowired
	private UploadService uploadService;
	@Autowired
	private HookInterceptor_UploadGeneralServiceOperationDocument hookInterceptor_UploadGSODocument;
	@Autowired
	private JobItemService jiService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private GeneralServiceOperationService gsoService;
	@Autowired
	private GsoDocumentService gsoDocumentService;
	@Autowired
	private JobItemWorkRequirementService jiWorkRequirementService;
	@Autowired
	private HookInterceptor_UploadGSODocumentCancelAndReplace hookInterceptor_UploadGSODocumentCancelAndReplace;
	@Autowired
	private InstrumService instServ;

	protected BaseDao<GeneralServiceOperation, Integer> getBaseDao() {
		return (BaseDao<GeneralServiceOperation, Integer>) this.generalServiceOperationDao;
	}

	public RestResultDTO<GeneralServiceOperationOutputDTO> startGeneralServiceOperationForExternalSystem(GeneralServiceOperationInputDTO inputDTO,
			Subdiv allocatedSubdiv) {

        GeneralServiceOperation gso = new GeneralServiceOperation();
        gso.setOrganisation(allocatedSubdiv);
        gso.setStatus((GeneralServiceOperationStatus) this.statusServ.findStatusByName("On-going",
            GeneralServiceOperationStatus.class));

        gso.setStartedOn(inputDTO.getActionDate());
        gso.setStartedBy(this.contactService.getByHrId(inputDTO.getHrid()));

        // get calibration type and procedure from jobitem workrequirement
        JobItemWorkRequirement firstJiWr = this.jiWorkRequirementService.get(inputDTO.getWorkRequirementIds().get(0));
        CalibrationType calType = firstJiWr != null
            ? firstJiWr.getWorkRequirement().getServiceType().getCalibrationType()
            : null;
        gso.setCalType(calType);
        Capability capability = firstJiWr != null
            ? firstJiWr.getWorkRequirement().getCapability()
            : null;
        gso.setCapability(capability);

        JobItem firstJi = firstJiWr.getJobitem();
        gso.setAddress(firstJi.getCurrentAddr());
        // set GsoLinks
        GsoLink gsoLink = new GsoLink();
        gsoLink.setGso(gso);
        gsoLink.setJi(firstJi);
        Set<GsoLink> gsoLinks = new TreeSet<>(new GsoLinkComparator());
        gsoLinks.add(gsoLink);
        int i = 0;
        for (Integer jiWrId : inputDTO.getWorkRequirementIds()) {
            if ((i++) != 0) {
                JobItem ji = this.jiWorkRequirementService.get(jiWrId).getJobitem();
                gsoLink = new GsoLink();
                gsoLink.setGso(gso);
                gsoLink.setJi(ji);
                gsoLinks.add(gsoLink);
            }
		}
		gso.setLinks(gsoLinks);
		this.generalServiceOperationDao.persist(gso);

		// invoke the startGeneralServiceOperation Hook for creating the 'General service operation' activity
		// on the linked Jobitems
		HookInterceptor_StartGeneralServiceOperation.Input input = new HookInterceptor_StartGeneralServiceOperation.Input(
				gso, false, gso.getStartedBy(), gso.getStartedOn(), gso.getStartedOn());
		this.hookInterceptor_startGeneralServiceOperation.recordAction(input);
		
		//set response result
		RestResultDTO<GeneralServiceOperationOutputDTO> dto = new RestResultDTO<GeneralServiceOperationOutputDTO>(true, "");
		GeneralServiceOperationOutputDTO result = new GeneralServiceOperationOutputDTO();
		result.setGsoId(gso.getId());
		result.setGsoStatus(gso.getStatus().getName());
		dto.addResult(result);
		return dto;
	}
	
	public RestResultDTO<GeneralServiceOperationOutputDTO> putGeneralServiceOperationOnHoldForExternalSystem(GeneralServiceOperationInputDTO inputDTO) {

		GeneralServiceOperation gso = this.get(inputDTO.getGsoId());
		gso.setStatus((GeneralServiceOperationStatus) this.statusServ.findStatusByName("On hold",
				GeneralServiceOperationStatus.class));
		gso.setCompletedOn(inputDTO.getActionDate());
		gso.setCompletedBy(this.contactService.getByHrId(inputDTO.getHrid()));
		// update Gso links
		for (GsoLink link : gso.getLinks()) {
			ActionOutcome ao = this.aoService.getByTypeAndValue(ActionOutcomeType.GENERAL_SERVICE_OPERATION,
					ActionOutcomeValue_GeneralServiceOperation.ON_HOLD);
			link.setOutcome(ao);
			link.setTimeSpent(inputDTO.getTimeSpent());
		}
		this.generalServiceOperationDao.merge(gso);
		// invoking hook CompleteGeneralServiceOperation for completing the General service operation activity
		// with ON-HOLD status
		HookInterceptor_CompleteGeneralServiceOperation.Input input = new HookInterceptor_CompleteGeneralServiceOperation.Input(
				gso, inputDTO.getRemark());
		hookInterceptor_CompleteGSO.recordAction(input);
		
		//set response result
		RestResultDTO<GeneralServiceOperationOutputDTO> dto = new RestResultDTO<GeneralServiceOperationOutputDTO>(true, "");
		GeneralServiceOperationOutputDTO result = new GeneralServiceOperationOutputDTO();
		result.setGsoId(gso.getId());
		result.setGsoStatus(gso.getStatus().getName());
		dto.addResult(result);
		return dto;
	}

	@Override
	public RestResultDTO<GeneralServiceOperationOutputDTO> resumeGeneralServiceOperationForExternalSystem(
			GeneralServiceOperationInputDTO inputDTO) {
		GeneralServiceOperation gso = this.get(inputDTO.getGsoId());
		Contact con = this.contactService.getByHrId(inputDTO.getHrid());
		
		this.gsoService.resumeGeneralServiceOperation(gso, con);
		
		//set response result
		RestResultDTO<GeneralServiceOperationOutputDTO> dto = new RestResultDTO<GeneralServiceOperationOutputDTO>(true, "");
		GeneralServiceOperationOutputDTO result = new GeneralServiceOperationOutputDTO();
		result.setGsoId(gso.getId());
		result.setGsoStatus(gso.getStatus().getName());
		dto.addResult(result);
		return dto;
	}

	@Override
	public RestResultDTO<GeneralServiceOperationOutputDTO> completeGeneralServiceOperationForExternalSystem(
			GeneralServiceOperationInputDTO inputDTO) {
		GeneralServiceOperation gso = this.get(inputDTO.getGsoId());
		// check if all Linked Items are completed before setting the Gso status to complete
		if(inputDTO.getItemsOutcome().size() == gso.getLinks().stream().filter(e -> e.getOutcome() == null).collect(Collectors.counting())) {
			gso.setStatus((GeneralServiceOperationStatus) this.statusServ.findStatusByName("Complete",
					GeneralServiceOperationStatus.class));
			gso.setCompletedOn(inputDTO.getActionDate());
			gso.setCompletedBy(this.contactService.getByHrId(inputDTO.getHrid()));
		}
		gso.setClientDecisionNeeded(inputDTO.getClientDecisionRequired());
		// update Gso links with input Outcomes value
		for (GeneralServiceOperationItemOutcomeInputDTO linkItem : inputDTO.getItemsOutcome()) {
			GsoLink currentLink = gso.getLinks().stream().filter(e -> e.getJi().getJobItemId() == linkItem.getJobitemId()).findFirst().get();
			ActionOutcome ao = this.aoService.getByTypeAndValue(ActionOutcomeType.GENERAL_SERVICE_OPERATION, 
					ActionOutcomeValue_GeneralServiceOperation.valueOf(ActionOutcomeValue_GeneralServiceOperation.class, linkItem.getOutcome()));
			currentLink.setOutcome(ao);
			currentLink.setTimeSpent(linkItem.getTimeSpent());
			// calculate the nextCalDueDate if the outcome is positive
			if(ao.isPositiveOutcome()) {
				// set recall date
				this.instServ.calculateRecallDateAfterGSO(currentLink.getJi().getInst(), gso, inputDTO.getDuration(), inputDTO.getIntervalUnitId(),
				currentLink.getJi().getCalType().getRecallRequirementType());
			}
		}
		this.generalServiceOperationDao.merge(gso);

		// invoking hook CompleteGeneralServiceOperation for completing the General service operation activity
		HookInterceptor_CompleteGeneralServiceOperation.Input input = new HookInterceptor_CompleteGeneralServiceOperation.Input(
				gso, inputDTO.getRemark());
		hookInterceptor_CompleteGSO.recordAction(input);
		
		//set response result
		RestResultDTO<GeneralServiceOperationOutputDTO> dto = new RestResultDTO<GeneralServiceOperationOutputDTO>(true, "");
		GeneralServiceOperationOutputDTO result = new GeneralServiceOperationOutputDTO();
		result.setGsoId(gso.getId());
		result.setGsoStatus(gso.getStatus().getName());
		dto.addResult(result);
		return dto;
	}

	@Override
	public RestResultDTO<GeneralServiceOperationOutputDTO> cancelGeneralServiceOperationForExternalSystem(
			GeneralServiceOperationInputDTO inputDTO) {
		GeneralServiceOperation gso = this.get(inputDTO.getGsoId());
		gso.setStatus((GeneralServiceOperationStatus) this.statusServ
				.findStatusByName(GeneralServiceOperationStatus.CANCELLED, GeneralServiceOperationStatus.class));
		gso.setCompletedOn(inputDTO.getActionDate());
		Contact completedBy = this.contactService.getByHrId(inputDTO.getHrid());
		gso.setCompletedBy(completedBy);
		this.generalServiceOperationDao.merge(gso);
		// erase incomplete gso activity
		this.gsoService.eraseIncompleteGsoActivity(gso, completedBy);
		
		//result
		RestResultDTO<GeneralServiceOperationOutputDTO> dto = new RestResultDTO<GeneralServiceOperationOutputDTO>(true, "");
		GeneralServiceOperationOutputDTO result = new GeneralServiceOperationOutputDTO();
		result.setGsoId(gso.getId());
		result.setGsoStatus(gso.getStatus().getName());
		dto.addResult(result);
		return dto;
	}

	@Override
	public RestResultDTO<GeneralServiceOperationOutputDTO> uploadGeneralServiceOperationDocumentForExternalSystem(
			GeneralServiceOperationInputDTO inputDTO, Subdiv allocatedSubdiv) {
		
		GeneralServiceOperation gso = this.get(inputDTO.getGsoId());
		GsoDocument gsoDoc = new GsoDocument();
		gsoDoc.setDocumentDate(inputDTO.getActionDate());
		// Generate and set a document number
		String gsoDocumentNumber = numerationService.generateNumber(NumerationType.GENERAL_SERVICE_OPERATION,
				allocatedSubdiv.getComp(), allocatedSubdiv);
		gsoDoc.setDocumentNumber(gsoDocumentNumber);
		gsoDoc.setGso(gso);
		if (gso.getGsoDocuments() == null)
			gso.setGsoDocuments(new TreeSet<>(new GsoDocumentComparator()));
		//set supplementary for field
		GsoDocument supplementaryForDoc = null;
		if(StringUtils.isNotEmpty(inputDTO.getSupplemantaryForNo())) {
			supplementaryForDoc = this.gsoDocumentService.findGsoDocumentByDocumentNumber(inputDTO.getSupplemantaryForNo());
			gsoDoc.setSupplementaryFor(supplementaryForDoc);
		}
		gso.getGsoDocuments().add(gsoDoc);
		// add general service operation document links
		Set<GsoDocumentLink> links = new TreeSet<>(new GsoDocumentLinkComparator());
		for (Integer jiId : inputDTO.getJobitemIds()) {
			JobItem ji = this.jiService.findJobItem(jiId);
			GsoDocumentLink gsodoclink = new GsoDocumentLink();
			gsodoclink.setGsoDocument(gsoDoc);
			gsodoclink.setJi(ji);
			links.add(gsodoclink);
		}
		gsoDoc.setLinks(links);
		this.merge(gso);
		
		// save the file
		SystemComponent comp = this.systemCompServ.findComponent(Component.GENERAL_SERVICE_OPERATION);
		JobItem firstJi = gso.getLinks().iterator().next().getJi();
		File toDirectory = this.compDirServ.getDirectory(comp, firstJi.getJob().getJobno());

		String fileName = inputDTO.getFileName();
		String newName = gsoDoc.getIdentifier();
		int extChar = fileName.lastIndexOf(".");
		newName = newName.concat(fileName.substring(extChar, fileName.length()));
		
		// decode file
		byte[] fileData = Base64.getDecoder().decode(inputDTO.getFileContent());
		InputStream targetStream = new ByteArrayInputStream(fileData);
			
		this.uploadService.saveFile(targetStream, toDirectory, newName);
		
		// invoking the UploadGeneralServiceOperationDocument hook
		if(StringUtils.isEmpty(inputDTO.getSupplemantaryForNo())) {
			HookInterceptor_UploadGeneralServiceOperationDocument.Input input = new HookInterceptor_UploadGeneralServiceOperationDocument.Input(
					gsoDoc, newName);
			hookInterceptor_UploadGSODocument.recordAction(input);
		}
		else if(supplementaryForDoc != null){
			HookInterceptor_UploadGSODocumentCancelAndReplace.Input input = new HookInterceptor_UploadGSODocumentCancelAndReplace.Input(
					gsoDoc, newName, supplementaryForDoc.getDocumentNumber().concat(fileName.substring(extChar, fileName.length())));
			hookInterceptor_UploadGSODocumentCancelAndReplace.recordAction(input);
		}
				
		//set response result
		RestResultDTO<GeneralServiceOperationOutputDTO> dto = new RestResultDTO<GeneralServiceOperationOutputDTO>(true, "");
		GeneralServiceOperationOutputDTO result = new GeneralServiceOperationOutputDTO();
		result.setGsoId(gso.getId());
		result.setGsoStatus(gso.getStatus().getName());
		result.setGsoDocumentNumber(gsoDocumentNumber);
		dto.addResult(result);
		return dto;
	}
}