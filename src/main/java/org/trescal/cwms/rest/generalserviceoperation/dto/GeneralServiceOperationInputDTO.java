package org.trescal.cwms.rest.generalserviceoperation.dto;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class GeneralServiceOperationInputDTO {

	private List<Integer> workRequirementIds;
	private List<Integer> jobitemIds;
	private String hrid;
	private Date actionDate;
	private Integer gsoId;
	private Integer timeSpent;
	private String remark;
	private Boolean clientDecisionRequired;
	private List<GeneralServiceOperationItemOutcomeInputDTO> itemsOutcome;
	private String fileName;
	private String fileContent;
	private String supplemantaryForNo;
	private Integer duration;
	private String intervalUnitId;
	
}
