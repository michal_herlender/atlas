package org.trescal.cwms.rest.generalserviceoperation.validator;

import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus.GeneralServiceOperationStatus;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_GeneralServiceOperation;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationInputDTO;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationItemOutcomeInputDTO;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CompleteGeneralServiceOperationInputDTOValidator extends AbstractBeanValidator {

    @Autowired
    private ContactService contactService;
    @Autowired
    private GeneralServiceOperationService gsoService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;

    @Override
    public boolean supports(@NotNull Class<?> clazz) {
        return GeneralServiceOperationInputDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);

		GeneralServiceOperationInputDTO dto = (GeneralServiceOperationInputDTO) target;

		if (dto.getGsoId() == null) {
			errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "General Service Operation ID" }, "General Service Operation ID is Mandatory");
		} else {

			GeneralServiceOperation gso = this.gsoService.get(dto.getGsoId());

			if (gso == null)
				errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.gsonotfound",
						new Object[] { dto.getGsoId() }, "General service operation with ID is not found");
			
			if(StringUtils.isNotEmpty(dto.getHrid())){
				Contact con = this.contactService.getByHrId(dto.getHrid());
				if(con != null) {
                    val accreditationStatus = this.procAccServ.authorizedFor(gso.getCapability().getId(), gso.getCalType().getCalTypeId(), con.getPersonid()).isLeft();
                    if (accreditationStatus)
                        errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.notaccredited",
                            new Object[]{dto.getGsoId()},
                            "The user is not accredited to perform this action for {0}");
                }
			}

			if(gso != null){
				if (!gso.getStatus().getName().equals(GeneralServiceOperationStatus.ON_GOING))
					errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.gsoincorrectstatus.completing",
							new Object[] { dto.getGsoId() },
							"The following General service operation not in the correct status for completing");

				if (CollectionUtils.isEmpty(dto.getItemsOutcome()))
					errors.rejectValue("itemsOutcome", "generalserviceoperation.rest.errors.mandatory",
							new Object[] { "Item Outcomes" }, "itemOutcomes is Mandatory");
				else {
					List<Integer> notLinkedItems = new ArrayList<>();
					List<Integer> duplicateItems = new ArrayList<>();
					List<Integer> linkedItemsWithEmptyOutcome = new ArrayList<>();
					List<Integer> linkedItemsWithInvalideOutcome = new ArrayList<>();
					List<Integer> linkedItemsWithEmptyTimeSpent = new ArrayList<>();
					List<Integer> itemsAlreadyCompleted = new ArrayList<>();
					for (GeneralServiceOperationItemOutcomeInputDTO item : dto.getItemsOutcome()) {
						if(dto.getItemsOutcome().stream().filter(e -> e.getJobitemId().equals(item.getJobitemId())).collect(Collectors.counting()) > 1)
							duplicateItems.add(item.getJobitemId());
						else if (!gso.getLinks().stream().anyMatch(e -> e.getJi().getJobItemId() == item.getJobitemId()))
							notLinkedItems.add(item.getJobitemId());
						else if (StringUtils.isEmpty(item.getOutcome()))
							linkedItemsWithEmptyOutcome.add(item.getJobitemId());
						else if (StringUtils.isNotEmpty(item.getOutcome())
								&& !ActionOutcomeValue_GeneralServiceOperation.COMPLETED_SUCCESSFULLY.name()
										.equals(item.getOutcome())
								&& !ActionOutcomeValue_GeneralServiceOperation.COMPLETED_UNSUCCESSFULLY.name()
										.equals(item.getOutcome()))
							linkedItemsWithInvalideOutcome.add(item.getJobitemId());
						else if (item.getTimeSpent() == null)
							linkedItemsWithEmptyTimeSpent.add(item.getJobitemId());
						else if(gso.getLinks().stream().anyMatch(e -> e.getJi().getJobItemId() == item.getJobitemId() && e.getOutcome() != null))
							itemsAlreadyCompleted.add(item.getJobitemId());
					}
					
					if (!CollectionUtils.isEmpty(duplicateItems))
						errors.rejectValue("itemsOutcome", "generalserviceoperation.rest.errors.gso.duplicateitems",
								new Object[] { notLinkedItems },
								"Duplicate items");
					if (!CollectionUtils.isEmpty(notLinkedItems))
						errors.rejectValue("itemsOutcome", "generalserviceoperation.rest.errors.gso.nolinkeditems",
								new Object[] { notLinkedItems },
								"The following items () are not linked to General service operation");
					if (!CollectionUtils.isEmpty(linkedItemsWithEmptyOutcome))
						errors.rejectValue("itemsOutcome", "generalserviceoperation.rest.errors.gso.nooutcome",
								new Object[] { linkedItemsWithEmptyOutcome },
								"The following items () are linked but without outcome");
					if (!CollectionUtils.isEmpty(linkedItemsWithInvalideOutcome))
						errors.rejectValue("itemsOutcome", "generalserviceoperation.rest.errors.gso.invalideoutcome",
								new Object[] { linkedItemsWithInvalideOutcome },
								"The following items () are linked but with invalide outcome");
					if (!CollectionUtils.isEmpty(linkedItemsWithEmptyTimeSpent))
						errors.rejectValue("itemsOutcome", "generalserviceoperation.rest.errors.gso.notimespent",
								new Object[] { linkedItemsWithEmptyTimeSpent },
								"The following items () are linked but without timespent");
					if (!CollectionUtils.isEmpty(itemsAlreadyCompleted))
						errors.rejectValue("itemsOutcome", "generalserviceoperation.rest.errors.gso.itemsalreadycompleted",
								new Object[] { itemsAlreadyCompleted },
								"The following items {0} are already completed");

				}
			}
		}

		if (StringUtils.isEmpty(dto.getHrid()))
			errors.rejectValue("hrid", "generalserviceoperation.rest.errors.mandatory", new Object[] { "Hrid" },
					"hrid is Mandatory");
		else {
			Contact con = this.contactService.getByHrId(dto.getHrid());
			if (con == null)
				errors.rejectValue("hrid", "generalserviceoperation.rest.errors.contactnotfound",
						new Object[] { dto.getHrid() }, "Contact with Hrid not found" + dto.getHrid());
		}

		if (dto.getActionDate() == null) {
			errors.rejectValue("actionDate", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "Action Date" }, "actionDate is Mandatory");
		}

	}

}
