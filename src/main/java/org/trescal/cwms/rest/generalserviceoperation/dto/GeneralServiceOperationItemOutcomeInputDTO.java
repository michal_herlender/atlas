package org.trescal.cwms.rest.generalserviceoperation.dto;

import lombok.Data;

@Data
public class GeneralServiceOperationItemOutcomeInputDTO {

	private Integer jobitemId;
	private String outcome;
	private Integer timeSpent;
	
}
