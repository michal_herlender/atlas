package org.trescal.cwms.rest.generalserviceoperation.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationInputDTO;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationOutputDTO;
import org.trescal.cwms.rest.generalserviceoperation.service.RestGeneralServiceOperationService;
import org.trescal.cwms.rest.generalserviceoperation.validator.CancelGeneralServiceOperationInputDTOValidator;

@Controller
@JsonController
@RequestMapping("/gso")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV})
public class RestCancelGeneralServiceOperationController {

	@Autowired
	private CancelGeneralServiceOperationInputDTOValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestGeneralServiceOperationService gsoService;


	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/cancel", method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<GeneralServiceOperationOutputDTO> cancelGeneralServiceOperation(
			@RequestBody GeneralServiceOperationInputDTO inputDTO, BindingResult bindingResult, Locale locale) {

		// validation
		validator.validate(inputDTO, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		
		return this.gsoService.cancelGeneralServiceOperationForExternalSystem(inputDTO);
	}

}
