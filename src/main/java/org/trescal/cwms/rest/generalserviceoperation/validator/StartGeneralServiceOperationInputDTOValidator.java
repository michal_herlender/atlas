package org.trescal.cwms.rest.generalserviceoperation.validator;

import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationInputDTO;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class StartGeneralServiceOperationInputDTOValidator extends AbstractBeanValidator {

    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private JobItemWorkRequirementService jiWorkRequirementService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;

    @Override
    public boolean supports(@NotNull Class<?> clazz) {
        return GeneralServiceOperationInputDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);

		GeneralServiceOperationInputDTO dto = (GeneralServiceOperationInputDTO) target;

		if (CollectionUtils.isEmpty(dto.getWorkRequirementIds())) {
			errors.rejectValue("workRequirementIds", "generalserviceoperation.rest.errors.workrequirementsempty",
					"Please mention at least one Jobitem Work requirement ID");
		} 
		else {
            List<Integer> notFoundWrIds = new ArrayList<>();
            List<Integer> notReadyForGSOJiWrIds = new ArrayList<>();
            List<Integer> notAccreditedIds = new ArrayList<>();
            dto.getWorkRequirementIds().forEach(jiWrId -> {
                JobItemWorkRequirement wrji = this.jiWorkRequirementService.get(jiWrId);
                if (wrji == null)
                    notFoundWrIds.add(jiWrId);
                else if (!isJobItemWorkRequirementStartable(wrji))
                    notReadyForGSOJiWrIds.add(jiWrId);
                else {
                    Boolean isAccredited = isAccreditedToPerformGsoAction(wrji, dto.getHrid());
                    if (BooleanUtils.isNotTrue(isAccredited))
                        notAccreditedIds.add(jiWrId);
				}
			});
			if (CollectionUtils.isNotEmpty(notFoundWrIds))
				errors.rejectValue("workRequirementIds", "generalserviceoperation.rest.errors.workrequirementsnotfound",
						new Object[] { notFoundWrIds }, "The following Work requirement IDs {0} are not found");

			if (CollectionUtils.isNotEmpty(notReadyForGSOJiWrIds))
				errors.rejectValue("workRequirementIds", "generalserviceoperation.rest.errors.workrequirementsnotreadyforgso",
						new Object[] { notReadyForGSOJiWrIds },
						"The following Work requirements IDs {0} are not ready for General service operation");
			
			if (CollectionUtils.isNotEmpty(notAccreditedIds))
				errors.rejectValue("workRequirementIds", "generalserviceoperation.rest.errors.notaccredited",
						new Object[] { notAccreditedIds },
						"The user is not accredited to perform this action for {0}");
		}

		if (StringUtils.isEmpty(dto.getHrid()))
			errors.rejectValue("hrid", "generalserviceoperation.rest.errors.mandatory", new Object[] { "Hrid" },
					"hrid is Mandatory");
		else {
			Contact con = this.contactService.getByHrId(dto.getHrid());
			if (con == null)
				errors.rejectValue("hrid", "generalserviceoperation.rest.errors.contactnotfound",
						new Object[] { dto.getHrid() }, "Contact with Hrid not found" + dto.getHrid());
		}

		if (dto.getActionDate() == null) {
			errors.rejectValue("actionDate", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "Action Date" }, "actionDate is Mandatory");
		}

	}

	private boolean isJobItemWorkRequirementStartable(JobItemWorkRequirement jiwr) {

		// can start conditions
		// have a state of AWAITING GENERAL SERVICE OPERATION or RESUME GENERAL SERVICE OPERATION
		boolean condition1 = jobItemService.stateHasGroupOfKeyName(jiwr.getJobitem().getState(),
				StateGroup.AWAITINGGENERALSERVICEOPERATION);
		// the Work Requirement has one procedure at least
        boolean condition2 = jiwr.getWorkRequirement().getCapability() != null;
		// the Work Requirement must not be complete or cancelled
		boolean condition3 = !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
				&& !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED);
		// if the jobitem have a current location
		boolean condition4 = jiwr.getJobitem().getCurrentAddr() != null;

		return condition1 && condition2 && condition3 && condition4;
	}

	private Boolean isAccreditedToPerformGsoAction(JobItemWorkRequirement jiWorkRequirement, String userHrId) {
		if(StringUtils.isNotEmpty(userHrId)){
			Contact con = this.contactService.getByHrId(userHrId);
			if (con != null) {
                val accStat = this.procAccServ.authorizedFor(jiWorkRequirement.getWorkRequirement().getCapability().getId(),
                    jiWorkRequirement.getWorkRequirement().getServiceType() != null ?
                        jiWorkRequirement.getWorkRequirement().getServiceType().getCalibrationType().getCalTypeId() :
                        jiWorkRequirement.getJobitem().getServiceType().getCalibrationType().getCalTypeId(), con.getPersonid());
                return accStat.isRight();
            }
			else return null;
		}
		return null;
	}
}
