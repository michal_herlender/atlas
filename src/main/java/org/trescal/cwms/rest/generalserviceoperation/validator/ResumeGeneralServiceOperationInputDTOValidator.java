package org.trescal.cwms.rest.generalserviceoperation.validator;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus.GeneralServiceOperationStatus;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.generalserviceoperation.dto.GeneralServiceOperationInputDTO;

import javax.validation.constraints.NotNull;

@Component
public class ResumeGeneralServiceOperationInputDTOValidator extends AbstractBeanValidator {

    @Autowired
    private ContactService contactService;
    @Autowired
    private GeneralServiceOperationService gsoService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;

    @Override
    public boolean supports(@NotNull Class<?> clazz) {
        return GeneralServiceOperationInputDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);

		GeneralServiceOperationInputDTO dto = (GeneralServiceOperationInputDTO) target;

		if (dto.getGsoId() == null) {
			errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "General Service Operation ID" }, "General Service Operation ID is Mandatory");
		} else {

			GeneralServiceOperation gso = this.gsoService.get(dto.getGsoId());

			if (gso == null)
				errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.gsonotfound",
						new Object[] { dto.getGsoId() }, "General service operation with ID is not found");

			else if (!gso.getStatus().getName().equals(GeneralServiceOperationStatus.ON_HOLD))
				errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.gsoincorrectstatus.resume",
						new Object[] { dto.getGsoId() },
						"The following General service operation is not in the correct status for resuming");
			else if(StringUtils.isNotEmpty(dto.getHrid())){
				Contact con = this.contactService.getByHrId(dto.getHrid());
				if(con != null) {
                    val accreditationStatus = this.procAccServ.authorizedFor(gso.getCapability().getId(), gso.getCalType().getCalTypeId(), con.getPersonid());
                    if (!accreditationStatus.isRight())
                        errors.rejectValue("gsoId", "generalserviceoperation.rest.errors.notaccredited",
                            new Object[]{dto.getGsoId()},
                            "The user is not accredited to perform this action for {0}");
                }
			}
		}

		if (StringUtils.isEmpty(dto.getHrid()))
			errors.rejectValue("hrid", "generalserviceoperation.rest.errors.mandatory", new Object[] { "Hrid" },
					"hrid is Mandatory");
		else {
			Contact con = this.contactService.getByHrId(dto.getHrid());
			if (con == null)
				errors.rejectValue("hrid", "generalserviceoperation.rest.errors.contactnotfound",
						new Object[] { dto.getHrid() }, "Contact with Hrid not found" + dto.getHrid());
		}

		if (dto.getActionDate() == null) {
			errors.rejectValue("actionDate", "generalserviceoperation.rest.errors.mandatory",
					new Object[] { "Action Date" }, "actionDate is Mandatory");
		}

	}
}
