package org.trescal.cwms.rest.generalserviceoperation.enums;

public enum GeneralServiceOperationAction
{
	START_NEW_GSO, 
	PUT_GSO_ON_HOLD, 
	RESUME_GSO, 
	COMPLETE_GSO, 
	CANCEL_GSO, 
	UPLOAD_GSO_DOCUMENT;
}
