package org.trescal.cwms.rest.security;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestPingController {
	
	/**
	 * This is intended for external providers to test 
	 * their own connection to us after authentication
	 * There are two mappings:
	 *   /ping is for session based connectivity   e.g. /rest/ping
	 *   /api/ping is for OAuth2 connectivity      e.g. /rest/api/ping 
	 * @return
	 */
	@ResponseStatus(value=HttpStatus.OK)
	@RequestMapping(path={"/ping", "/api/ping"}/*, 
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8,Constants.MEDIATYPE_TEXT_JSON_UTF8}*/)
	public @ResponseBody RestResultDTO<Object> handleRequest() {
		return new RestResultDTO<Object>(true, "");
	}
}
