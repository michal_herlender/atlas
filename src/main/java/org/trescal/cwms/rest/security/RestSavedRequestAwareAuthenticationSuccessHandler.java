package org.trescal.cwms.rest.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.util.WebUtils;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/*
 * This implements a request handler that returns a HTTP status code of 200 OK; it is similar to the
 * org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler
 * but with removing the redirect logic to a 301 MOVED_PERMANENTLY
 *
 * Created: Galen Beck 2016-01-20
 *
 * Source: http://www.baeldung.com/2011/10/31/securing-a-restful-web-service-with-spring-security-3-1-part-3/
 */
public class RestSavedRequestAwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private LocaleResolver localeResolver;
	// RequestCache allows saving / restarting request after redirecting to authentication
	private RequestCache requestCache;
	private UserService userService;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public RestSavedRequestAwareAuthenticationSuccessHandler() {
		requestCache = new HttpSessionRequestCache();
	}
    
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, 
      Authentication authentication) throws ServletException, IOException {
    	initializeSession(request, response, authentication);
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        if (savedRequest == null) {
            clearAuthenticationAttributes(request);
            return;
        }
        String targetUrlParam = getTargetUrlParameter();
        if (isAlwaysUseDefaultTargetUrl() || 
          (targetUrlParam != null && 
          StringUtils.hasText(request.getParameter(targetUrlParam)))) {
            this.requestCache.removeRequest(request, response);
            clearAuthenticationAttributes(request);
            return;
        }
 
        clearAuthenticationAttributes(request);
    }
    /*
     * Initializes just the few session attributes needed for REST web services
     * At present time the user's default subdiv and company are deemed active
     */
    private void initializeSession(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {

        String username = authentication.getName();
        logger.debug(authentication.getClass().getName());
        User user = this.userService.get(username);
        HttpSession session = request.getSession();
        session.setAttribute(Constants.SESSION_ATTRIBUTE_USERNAME, username);
        session.setAttribute(Constants.SESSION_ATTRIBUTE_COMPANY, user.getCon().getSub().getComp().dto());
        session.setAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV, user.getCon().getSub().dto());
        initializeLocale(request, response, user);
        logger.debug("Initialized REST session for user " + username);

    }
    private void initializeLocale(HttpServletRequest request, HttpServletResponse response, User user) {
    	if (user.getCon().getLocale() == null) return;
        WebUtils.setSessionAttribute(request, Constants.SESSION_ATTRIBUTE_LOCALE, user.getCon().getLocale());
    }

	public void setLocaleResolver(LocaleResolver localeResolver) {
		this.localeResolver = localeResolver;
	}
	 
    public void setRequestCache(RequestCache requestCache) {
        this.requestCache = requestCache;
    }

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
