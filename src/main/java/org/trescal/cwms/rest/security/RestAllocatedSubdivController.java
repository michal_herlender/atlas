package org.trescal.cwms.rest.security;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.userright.entity.userrole.db.UserRoleDao;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.rest.common.dto.output.RestFieldErrorDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;


@Controller
@RestJsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class RestAllocatedSubdivController {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private BusinessDetailsService businessDetailsService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/allocatedsubdiv/change", method = RequestMethod.POST, produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8,Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<Integer> onChange(@RequestParam int allocatedSubdivid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName, HttpServletRequest request) {
		User user = userService.get(userName);
		RestResultDTO<Integer> result = new RestResultDTO<Integer>(true, "");
		//Security
		List<SubdivKeyValue> subdivAllow = userRoleDao.getUserRoleSubdivs(user);
		if(!subdivAllow.stream().anyMatch(x->x.getKey().equals(allocatedSubdivid))) {
			RestFieldErrorDTO error = new RestFieldErrorDTO();
			error.setFieldName("Err_Subdiv_No_Right");
			error.setMessage("You didn't have any role for this subdiv");
			result.addError(error);
			return result;
		}
		
		Subdiv subdiv = subdivService.get(allocatedSubdivid);

		HttpSession httpSession = request.getSession();
		httpSession.setAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV, new SubdivKeyValue(subdiv));
		httpSession.setAttribute(Constants.SESSION_ATTRIBUTE_COMPANY, new CompanyKeyValue(subdiv.getComp()));
		httpSession.setAttribute(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS,
				businessDetailsService.getAllBusinessDetails(subdiv));
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication authentication = securityContext.getAuthentication();
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		List<Permission> permissions = user.getUserRoles().stream().filter(ur -> ur.getOrganisation().equals(subdiv))
				.flatMap(ur -> ur.getRole().getGroups().stream().flatMap(pg -> pg.getPermissions().stream()))
				.collect(Collectors.toList());
		authorities.addAll(permissions);
		if(!permissions.isEmpty()) authorities.add(new SimpleGrantedAuthority("ROLE_INTERNAL"));
		authorities=authorities.stream().distinct().collect(Collectors.toList());
		securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(authentication.getPrincipal(),
				authentication.getCredentials(), authorities));
		result.addResult(allocatedSubdivid);
		return result;
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/allocatedsubdiv/available", method = RequestMethod.GET, produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8,Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<SubdivKeyValue> getSubdivAvailable(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		User user = userService.get(userName);
		RestResultDTO<SubdivKeyValue> result = new RestResultDTO<SubdivKeyValue>(true, "");
		for(SubdivKeyValue role : userRoleDao.getUserRoleSubdivs(user))
			result.addResult(new SubdivKeyValue(role.getKey(),role.getValue()));
		return result;
	}
}
