package org.trescal.cwms.rest.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.entity.userrole.db.UserRoleDao;
import org.trescal.cwms.core.userright.enums.Permission;

@Controller
@RestJsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class RestCompanyTools {

	@Autowired
	private UserService userService;
	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private CompanyService companyServ;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/authorisedwebservice", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public Boolean IsAllowTool(@RequestParam(name = "webservicearea", required = true) String webservicearea,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		User user = userService.get(userName);
		for (SubdivKeyValue subdivKey : userRoleDao.getUserRoleSubdivs(user))
			if (webservicearea.equals("accounting")
					&& authServ.hasRight(user.getCon(), Permission.ACCOUNTING_WEB_SERVICE, subdivKey.getKey()))
				return true;
		return false;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/authorisedwebservice/companies", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public List<CompanyKeyValue> getSubdivAvailable(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		User user = userService.get(userName);
		List<Integer> subdivs = new ArrayList<Integer>();
		List<CompanyKeyValue> result = new ArrayList<CompanyKeyValue>();
		for (SubdivKeyValue subdivKey : userRoleDao.getUserRoleSubdivs(user))
			if (authServ.hasRight(user.getCon(), Permission.ACCOUNTING_WEB_SERVICE, subdivKey.getKey()))
				subdivs.add(subdivKey.getKey());
		result.addAll(companyServ.getCompaniesFromSubdivs(subdivs));
		return result;
	}

}
