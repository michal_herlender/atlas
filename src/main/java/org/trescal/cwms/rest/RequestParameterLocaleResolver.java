package org.trescal.cwms.rest;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.i18n.AbstractLocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

/*
 * LocaleResolver that first checks for a locale paramater and then falls back to the user's 
 * preferred locale.  Used throughout REST web services.
 * 
 * Not set up as a scanned Component because we only use this in certain configurations
 * Intended for RESTful responses where there may be a locale parameter in request designating
 * a specific language to be returned.
 * 
 * Author: Galen Beck 2016-01-21
 */
public class RequestParameterLocaleResolver extends AbstractLocaleResolver {

	public static String PARAMETER_NAME_LOCALE = "locale";

	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@Override
	public Locale resolveLocale(HttpServletRequest request) {
		String localeText = request.getParameter(PARAMETER_NAME_LOCALE);
		if (localeText == null)
			return getSessionLocale(request);
		if (localeText.indexOf("_") > 0) {
			String[] tokens = localeText.split("_");
			return new Locale(tokens[0], tokens[1]);
		}
		return getSessionLocale(request);
	}

	@Override
	public void setLocale(HttpServletRequest arg0, HttpServletResponse arg1, Locale arg2) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Locale getDefaultLocale() {
		return supportedLocaleService.getPrimaryLocale();
	}

	/*
	 * Returns the, falling back to the default system locale if none is present.
	 */
	private Locale getSessionLocale(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			Locale sessionLocale = (Locale) session.getAttribute(Constants.SESSION_ATTRIBUTE_LOCALE);
			if (sessionLocale != null)
				return sessionLocale;
		}
		return getDefaultLocale();
	}
}