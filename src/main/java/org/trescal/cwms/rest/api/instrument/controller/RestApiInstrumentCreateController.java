package org.trescal.cwms.rest.api.instrument.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentInputDTO;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentOutputDTO;
import org.trescal.cwms.rest.api.instrument.service.RestApiInstrumentService;
import org.trescal.cwms.rest.api.instrument.validator.RestApiInstrumentCreateValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestApiInstrumentCreateController {

	@Autowired
	private RestApiInstrumentCreateValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestApiInstrumentService apiInstService;
	
	public static final String API_REQUEST_MAPPING = "/api/instrument/create";

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { API_REQUEST_MAPPING }, method = RequestMethod.POST)
	public RestResultDTO<RestApiInstrumentOutputDTO> createInstrument(
			@RequestBody RestApiInstrumentInputDTO inputDto, BindingResult bindingResult, Locale locale) {
		this.validator.validate(inputDto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		RestApiInstrumentOutputDTO outputDto = apiInstService.createIntstrument(inputDto);
		RestResultDTO<RestApiInstrumentOutputDTO> result = new RestResultDTO<>(true,"");
		result.addResult(outputDto);
		return result;
	}
	
}
