package org.trescal.cwms.rest.api.calibration.validator;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.rest.alligator.component.RestStartCalibrationValidator;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationStartInputDTO;

@Component
public class RestApiCalibrationStartValidator extends RestApiCalibrationValidator {
	
	// Logic already implemented in other validator using work requirements
	@Autowired
	private RestStartCalibrationValidator internalValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return RestApiCalibrationStartInputDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			RestApiCalibrationStartInputDTO inputDto = (RestApiCalibrationStartInputDTO) target;
			List<JobItemWorkRequirement> jiwrs = super.getJobItemWorkRequirements(inputDto.getJobItemIds(), errors);
			
			Contact contact = super.resolveContact(inputDto.getContactId(), errors);
			
			// Only proceed with further validation, if no errors detected in initial validation stages
			if (!errors.hasErrors()) {
				boolean bypassAuthorisation = (inputDto.getBypassAuthorisation() != null) ? inputDto.getBypassAuthorisation() : false; 
				boolean startable = this.internalValidator.isCalibrationStartableAsGroup(jiwrs, contact, bypassAuthorisation);
				if (!startable) {
					Locale locale = LocaleContextHolder.getLocale();
					// TODO use Errors object in other validator
					String reasons = this.internalValidator.getNotStartableReasons(jiwrs, contact, bypassAuthorisation, locale);
					errors.reject(null, reasons);
				}
			}
		}
	}
	

	
}
