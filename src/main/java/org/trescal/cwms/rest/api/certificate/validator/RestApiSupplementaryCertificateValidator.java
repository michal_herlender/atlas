package org.trescal.cwms.rest.api.certificate.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.api.calibration.validator.RestApiCalibrationValidator;
import org.trescal.cwms.rest.api.certificate.dto.RestApiSupplementaryCertificateInputDto;

@Component
public class RestApiSupplementaryCertificateValidator extends RestApiCalibrationValidator {

	@Autowired
	private CertificateService certService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return RestApiSupplementaryCertificateInputDto.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			RestApiSupplementaryCertificateInputDto dto = (RestApiSupplementaryCertificateInputDto) target;
			Certificate cert = this.certService.findCertificate(dto.getCertificateId());
			if (cert == null) {
				errors.reject(RestErrors.CODE_CERTIFICATE_NOT_FOUND, 
						new Object[] {dto.getCertificateId()}, 
						RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND);
			}
			else if (cert.getCal() == null) {
				errors.reject(RestErrors.CODE_CERTIFICATE_NO_CAL, 
						new Object[] {dto.getCertificateId()}, 
						RestErrors.MESSAGE_CERTIFICATE_NO_CAL);
			}
			super.resolveContact(dto.getContactId(), errors);
		}
	}
	
}
