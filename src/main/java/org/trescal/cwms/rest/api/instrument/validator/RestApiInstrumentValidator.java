package org.trescal.cwms.rest.api.instrument.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentInputDTO;

/**
 * Validator used for both create and update of instruments
 */
public abstract class RestApiInstrumentValidator extends AbstractBeanValidator {

	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private InstrumentModelService instModelService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private MfrService mfrService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return RestApiInstrumentInputDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors, Class<?>... groups) {
		super.validate(target, errors, groups);
		if (!errors.hasErrors()) {
			RestApiInstrumentInputDTO inputDto = (RestApiInstrumentInputDTO) target;
			
			// Ensure that records exist for specified mandatory IDs
			if (this.addressService.getReference(inputDto.getAddressid()) == null) {
				errors.rejectValue("addressid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getAddressid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			if (this.companyService.getReference(inputDto.getCoid()) == null) {
				errors.rejectValue("coid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getCoid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			if (this.contactService.getReference(inputDto.getPersonid()) == null) {
				errors.rejectValue("personid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getPersonid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			if (this.instModelService.getReference(inputDto.getModelid()) == null) {
				errors.rejectValue("modelid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getModelid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			
			// Ensure that records exist for optional IDs
			if ((inputDto.getContactId() != null) && (this.contactService.getReference(inputDto.getContactId()) == null)) {
				errors.rejectValue("contactId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getContactId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			if ((inputDto.getLocationid() != null) && (this.locationService.getReference(inputDto.getLocationid()) == null)) {
				errors.rejectValue("locationid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getLocationid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			if ((inputDto.getMfrid() != null) && (this.mfrService.findMfr(inputDto.getMfrid()) == null)) {
				errors.rejectValue("mfrid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getMfrid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			if ((inputDto.getDefaultservicetypeid() != null) && (this.serviceTypeService.getReference(inputDto.getDefaultservicetypeid()) == null)) {
				errors.rejectValue("defaultservicetypeid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getDefaultservicetypeid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}

			// TODO check consistency of address/contact/company/location
			// TODO do we allow moving instrument to other company once created?
		}
	}


}
