package org.trescal.cwms.rest.api.creditnote.component;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.pricing.creditnote.projection.items.CreditNoteItemProjectionDTO;
import org.trescal.cwms.rest.api.creditnote.dto.RestApiCreditNoteDetailsDTO;
import org.trescal.cwms.rest.api.creditnote.dto.RestApiCreditNoteItemDetailsDTO;

@Component
public class RestApiCreditNoteDetailsConverter {
	public RestApiCreditNoteDetailsDTO convertToCreditNoteDetailsDTO(List<CreditNoteItemProjectionDTO> creditNoteItems) {
		List<RestApiCreditNoteItemDetailsDTO> creditNoteItemDTOs = convertToCreditNoteItemDTOList(creditNoteItems);
		
		return RestApiCreditNoteDetailsDTO.builder()
				.creditNoteItems(creditNoteItemDTOs)
				.build();
	}
	
	private List<RestApiCreditNoteItemDetailsDTO> convertToCreditNoteItemDTOList(List<CreditNoteItemProjectionDTO> creditNoteItems) {
		return creditNoteItems.stream()
				.map(creditNoteItem -> RestApiCreditNoteItemDetailsDTO.builder()
						.itemId(creditNoteItem.getItemId())
						.itemNumber(creditNoteItem.getItemNumber())
						.description(creditNoteItem.getDescription())
						.quantity(creditNoteItem.getQuantity())
						.finalCost(creditNoteItem.getFinalCost())
						.taxable(creditNoteItem.getTaxable())
						.taxAmount(creditNoteItem.getTaxAmount())
						.costType(creditNoteItem.getNominalCostType())
						.build())
				.collect(Collectors.toList());
	}
}
