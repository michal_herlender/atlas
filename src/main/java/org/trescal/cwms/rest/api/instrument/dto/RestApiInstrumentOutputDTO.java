package org.trescal.cwms.rest.api.instrument.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiInstrumentOutputDTO {
	private Integer plantid;
}
