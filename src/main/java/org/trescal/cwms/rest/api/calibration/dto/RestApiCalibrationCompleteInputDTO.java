package org.trescal.cwms.rest.api.calibration.dto;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiCalibrationCompleteInputDTO extends RestApiCalibrationInputDTO {
	@NotNull
	private ActionOutcomeValue_Calibration actionOutcome;	
	// Optional
	private OffsetDateTime startTime;
	// Optional
	private OffsetDateTime completeTime;
	// Optional
	private LocalDate calibrationDate;
	// Optional
	private CalibrationVerificationStatus calibrationVerificationStatus;
	// Optional
	private String remarks;
	// Technically optional (0 used as default)
	private Map<Integer, Integer> workingTimes;	
}
