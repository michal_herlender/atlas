package org.trescal.cwms.rest.api.certificate.service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Base64;
import java.util.HashSet;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateInputDto;
import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateOutputDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RestApiCertificateServiceImpl implements RestApiCertificateService {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ContactService contactService; 
	@Autowired
	private InstrumService instrumentService; 
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private SubdivService subdivService;
	
	private final static String FILE_EXTENSION = ".PDF";
	
	@Override
	public RestApiUploadClientCertificateOutputDto uploadCertificate(RestApiUploadClientCertificateInputDto inputDto) {
		Instrument instrument = this.instrumentService.get(inputDto.getPlantId());
		ServiceType serviceType = inputDto.getServiceTypeId() == null ? null : this.serviceTypeService.get(inputDto.getServiceTypeId());
		Contact currentContact = this.contactService.getContactOrLoggedInContact(inputDto.getContactId());
		Subdiv currentSubdiv = currentContact.getSub();
		Subdiv externalSubdiv = (inputDto.getExternalSubdivId() == null) ? null : this.subdivService.get(inputDto.getExternalSubdivId());
		
		Certificate certificate = new Certificate();
		certificate.setCertno(this.certificateService.findNewCertNo(CertificateType.CERT_CLIENT, currentSubdiv));
		certificate.setRegisteredBy(currentContact);
		certificate.setServiceType(serviceType);
		certificate.setDuration(inputDto.getDuration());
		certificate.setUnit(inputDto.getIntervalUnit());
		certificate.setType(CertificateType.CERT_CLIENT);
		certificate.setStatus(CertStatusEnum.SIGNED);
		certificate.setThirdDiv(externalSubdiv);
		certificate.setThirdCertNo(inputDto.getExternalCertificateNumber());
		certificate.setCalDate(DateTools.dateFromLocalDate(inputDto.getCalibrationDate()));
		certificate.setCertDate(DateTools.dateFromLocalDate(inputDto.getCertificateDate()));
		certificate.setOptimization(inputDto.getOptimization());
		certificate.setRestriction(inputDto.getRestriction());
		certificate.setAdjustment(inputDto.getAdjustment());
		certificate.setRepair(inputDto.getRepair());
		certificate.setInstCertLinks(new HashSet<>());
		
		this.certificateService.insertCertificate(certificate, currentContact);
		
		InstCertLink icl = new InstCertLink();
		icl.setCert(certificate);
		icl.setInst(instrument);
		instrument.getInstCertLinks().add(icl);
		certificate.getInstCertLinks().add(icl);
		this.certificateService.setCertificateDirectory(certificate);
		
		File directory = certificate.getDirectory();
		byte[] fileData = Base64.getDecoder().decode(inputDto.getContent());

		// write file
		try {
			String fileName = certificate.getCertno() + FILE_EXTENSION;
			File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));

			FileOutputStream os = new FileOutputStream(file);
			IOUtils.write(fileData, os);
			os.flush();
			os.close();
		} catch (Exception e) {
			log.error("Error writing file", e);
		}
		
		if (inputDto.getUpdateCalibrationDueDate()) {
			// Revisit if necesary - In case interval different than current interval of instrument
			// recallRuleService.insert(inputDto.getPlantid(), certificate.getDuration(), null, true, certificate.getUnit(),
			//		RecallRequirementType.FULL_CALIBRATION, false, null);
			instrumentService.calculateRecallDateAfterTPCert(instrument, certificate);
		}
		
		RestApiUploadClientCertificateOutputDto outputDto = RestApiUploadClientCertificateOutputDto.builder()
				.certificateId(certificate.getCertid())
				.certificateNumber(certificate.getCertno())
				.build();
		return outputDto;
	}
}
