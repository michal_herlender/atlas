package org.trescal.cwms.rest.api.instrument.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiInstrumentInputDTO {
	
	public static interface RestApiInstrumentCreate {};
	public static interface RestApiInstrumentUpdate {};

	// Optional contactId of person performing creation or update; otherwise service account used
	private Integer contactId;

	// Mandatory for update, must be null for create
	@NotNull(groups={RestApiInstrumentUpdate.class})
	@Null(groups={RestApiInstrumentCreate.class})
	private Integer plantid;

	// Mandatory for update or create
	@NotNull
	private Integer calInterval;

	// Mandatory for update or create
	@NotNull
	private IntervalUnit calIntervalUnit;
	
	// Optional for update or create
	// TODO recall rules?
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate recallDate;
	
	// Mandatory for update or create
	@NotNull
	private Boolean calibrationStandard;
	
	// Mandatory for update or create (can be blank unless required by settings - TODO if using for France)
	@NotNull
	@Size(max=50)
	private String plantno;
	
	// Mandatory for update or create (can be blank unless required by settings - TODO if using for France)
	@NotNull
	@Size(max=50)
	private String serialno;
	
	// Mandatory for update or create
	@NotNull
	private InstrumentStatus status;
	
	// Optional for update or create
	@Size(max=2000)
    private String customerDescription;
    
	// Optional for update or create
	@Size(max=100)
    private String customerSpecification;
    
	// Optional for update or create
	@Size(max=50)
    private String formerBarcode;
    
	// Optional for update or create
    @Size(max=64)
    private String freeTextLocation;
    
	// Optional for update or create
    @Size(max=50)
    private String clientBarcode;
    
	// Optional for update or create
    @Size(max=255)
    private String modelname;
    
	// Mandatory for update or create
    @NotNull
    private Integer addressid;
    
	// Mandatory for update or create
    @NotNull
    private Integer coid;

    // Mandatory for update or create
    @NotNull
    private Integer personid;

    // Optional for update or create
    private Integer locationid;

    // Optional for update or create
    private Integer mfrid;

    // Mandatory for update or create
    @NotNull
    private Integer modelid;

    // Optional for update or create
    private Integer defaultservicetypeid;

    // Optional for update or create
    private Integer usageid;
    
    // Optional for update or create
	private Boolean customerManaged;
    
}
