package org.trescal.cwms.rest.api.calibration.validator;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.service.RestCompleteCalibrationService;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationCompleteInputDTO;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Locale;

@Component
public class RestApiCalibrationCompleteValidator extends RestApiCalibrationValidator {

    @Autowired
    private ActionOutcomeService actionOutcomeService;
    @Autowired
    private CapabilityAuthorizationService capabilityAuthorizationService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private RestCompleteCalibrationService rccService;

    @Override
    public boolean supports(@NotNull Class<?> clazz) {
        return RestApiCalibrationCompleteInputDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			RestApiCalibrationCompleteInputDTO inputDto = (RestApiCalibrationCompleteInputDTO) target;
			// Step 1 - look up job items
			List<JobItem> jobItems = super.getJobItems(inputDto.getJobItemIds(), errors);
			// Step 2 - resolve ongoing calibration
			Calibration calibration = rccService.getCalibrationInStatus(jobItems, CalibrationStatus.ON_GOING);
			if (calibration == null) {
				errors.reject(RestErrors.CODE_CAL_NOT_IN_PROGRESS, null, RestErrors.MESSAGE_CAL_NOT_IN_PROGRESS);
			}
			
			// Step 3 - verify user authorization to complete calibration
			Contact completedBy = super.resolveContact(inputDto.getContactId(), errors);
			boolean bypassAuthorisation = (inputDto.getBypassAuthorisation() != null) ? inputDto.getBypassAuthorisation() : false;
			if (!bypassAuthorisation && calibration != null && completedBy != null) {
                val status = this.capabilityAuthorizationService.authorizedFor(
                    calibration.getCapability().getId(), calibration.getCalType().getCalTypeId(),
                    completedBy.getPersonid());
                if (!status.isRight()) {
                    Locale locale = LocaleContextHolder.getLocale();
                    String calTypeShortName = this.translationService.getCorrectTranslation(
                        calibration.getCalType().getServiceType().getShortnameTranslation(), locale);
                    String procedureReference = calibration.getCapability().getReference();
                    Object[] errorArgs = new Object[]{completedBy.getName(), procedureReference, calTypeShortName};
                    errors.reject(RestErrors.CODE_CAL_NOT_AUTHORIZED, errorArgs, RestErrors.MESSAGE_CAL_NOT_AUTHORIZED);
                }
            }

			// Step 4 - Lookup CalibrationOutcome (verify workflow correctness)
			ActionOutcome ao = this.actionOutcomeService.getByTypeAndValue(ActionOutcomeType.CALIBRATION, inputDto.getActionOutcome());
			if (ao == null) {
				errors.reject(RestErrors.CODE_CALIBRATION_OUTCOME_NOT_DEFINED,
						new Object[]{inputDto.getActionOutcome()},
						RestErrors.MESSAGE_CALIBRATION_OUTCOME_NOT_DEFINED);
			}
		}
	}

}
