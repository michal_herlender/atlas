package org.trescal.cwms.rest.api.certificate.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiSupplementaryCertificateOutputDto {
	String documentNumber;
	Integer certificateId;
}
