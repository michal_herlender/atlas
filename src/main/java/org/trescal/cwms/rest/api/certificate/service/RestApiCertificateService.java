package org.trescal.cwms.rest.api.certificate.service;

import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateInputDto;
import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateOutputDto;

public interface RestApiCertificateService {
	RestApiUploadClientCertificateOutputDto uploadCertificate(RestApiUploadClientCertificateInputDto inputDto);
}
