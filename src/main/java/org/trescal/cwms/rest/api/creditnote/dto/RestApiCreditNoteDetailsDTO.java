package org.trescal.cwms.rest.api.creditnote.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder @NoArgsConstructor @AllArgsConstructor @Getter
public class RestApiCreditNoteDetailsDTO {
	private List<RestApiCreditNoteItemDetailsDTO> creditNoteItems;
}
