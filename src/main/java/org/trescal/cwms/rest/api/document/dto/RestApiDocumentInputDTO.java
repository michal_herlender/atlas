package org.trescal.cwms.rest.api.document.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder @AllArgsConstructor @NoArgsConstructor @Setter @Getter
public class RestApiDocumentInputDTO {
	private Integer id;
	private String number;
}
