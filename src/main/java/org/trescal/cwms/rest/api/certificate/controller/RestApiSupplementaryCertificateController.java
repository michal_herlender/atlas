package org.trescal.cwms.rest.api.certificate.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.rest.api.certificate.dto.RestApiSupplementaryCertificateInputDto;
import org.trescal.cwms.rest.api.certificate.dto.RestApiSupplementaryCertificateOutputDto;
import org.trescal.cwms.rest.api.certificate.validator.RestApiSupplementaryCertificateValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller @RestJsonController
public class RestApiSupplementaryCertificateController {

	@Autowired
	private CertificateService certService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private RestApiSupplementaryCertificateValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	
	public static final String API_REQUEST_MAPPING = "/api/certificate/supplementary";

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { API_REQUEST_MAPPING }, method = RequestMethod.POST)
	public RestResultDTO<?> supplementaryCertificate(
			@RequestBody RestApiSupplementaryCertificateInputDto inputDto, BindingResult bindingResult, Locale locale) {
		this.validator.validate(inputDto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		Contact contact = this.contactService.getContactOrLoggedInContact(inputDto.getContactId());
		Certificate cert = certService.createSupplementaryCert(inputDto.getCertificateId(), contact, null);
		RestResultDTO<RestApiSupplementaryCertificateOutputDto> result = new RestResultDTO<>(true,"");
		RestApiSupplementaryCertificateOutputDto outputDto = new RestApiSupplementaryCertificateOutputDto();
		outputDto.setCertificateId(cert.getCertid());
		outputDto.setDocumentNumber(cert.getCertno());
		result.addResult(outputDto);
		return result;
	}
}