package org.trescal.cwms.rest.api.calibration.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiCalibrationStartOutputDTO {
	Integer calibrationId;
	String documentNumber;
	Integer certificateId;
}
