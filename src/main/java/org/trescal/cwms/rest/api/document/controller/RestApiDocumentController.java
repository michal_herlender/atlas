package org.trescal.cwms.rest.api.document.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentResult;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.api.document.component.RestApiDocumentOutputConverter;
import org.trescal.cwms.rest.api.document.dto.RestApiDocumentInputDTO;
import org.trescal.cwms.rest.api.document.dto.RestApiDocumentOutputDTO;
import org.trescal.cwms.rest.api.document.service.RestApiDocumentService;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestApiDocumentController {
	
	@Autowired
	private RestApiDocumentOutputConverter resultConverter;
	@Autowired
	private RestApiDocumentService documentService;

	@RequestMapping(path = "/api/document/{entityName}", method = RequestMethod.POST,
			produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
		public @ResponseBody
		RestResultDTO<RestApiDocumentOutputDTO> handleRequest(
				@PathVariable(name = "entityName") String entityName,
				@RequestBody RestApiDocumentInputDTO inputDto) {
		
			Integer entityId = inputDto.getId();
			RetrieveDocumentResult documentResult = documentService.getLatestDocumentForEntity(entityName, entityId);
			RestResultDTO<RestApiDocumentOutputDTO> result = this.resultConverter.convert(documentResult);
			
			return result;
	}
}
