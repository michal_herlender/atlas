package org.trescal.cwms.rest.api.invoice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder @NoArgsConstructor @AllArgsConstructor @Getter
public class RestApiInvoiceDetailsDTO {
	private List<RestApiInvoiceItemDetailsDTO> invoiceItems;
}
