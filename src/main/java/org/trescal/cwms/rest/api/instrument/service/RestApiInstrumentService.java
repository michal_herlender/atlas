package org.trescal.cwms.rest.api.instrument.service;

import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentInputDTO;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentOutputDTO;

public interface RestApiInstrumentService {
	
	/**
	 * Creates a new instrument 
	 * @param inputDto command for creating instrument (must be validated without errors) 
	 * @return output DTO with the plantid (primary key) of the instrument created
	 */
	RestApiInstrumentOutputDTO createIntstrument(RestApiInstrumentInputDTO inputDto);
	
	/**
	 * Updates an existing instrument 
	 * @param inputDto command for updating instrument (must be validated without errors)
	 */
	void updateInstrument(RestApiInstrumentInputDTO inputDto);
}
