package org.trescal.cwms.rest.api.calibration.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.alligator.service.RestCompleteCalibrationService;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationCancelInputDTO;

@Component
public class RestApiCalibrationCancelValidator extends RestApiCalibrationValidator {

	@Autowired
	private RestCompleteCalibrationService rccService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return RestApiCalibrationCancelInputDTO.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			RestApiCalibrationCancelInputDTO inputDto = (RestApiCalibrationCancelInputDTO) target;
			
			// Ensure that contact can be resolved
			super.resolveContact(inputDto.getContactId(), errors);
			List<JobItem> jobItems = super.getJobItems(inputDto.getJobItemIds(), errors);
			
			// Each job item should have a calibration which is either in progress or not started.
			// Ensure that a calibration can be found to cancel.
			// TODO evaluate on-hold case
			
			Calibration calibration = rccService.getCalibrationInStatus(jobItems, CalibrationStatus.AWAITING_WORK, CalibrationStatus.ON_GOING);
			if (calibration == null) {
				errors.reject(RestErrors.CODE_CAL_NOT_IN_PROGRESS, null, RestErrors.MESSAGE_CAL_NOT_IN_PROGRESS);
			}
		}
	}

}
