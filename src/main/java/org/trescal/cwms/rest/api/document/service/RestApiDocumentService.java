package org.trescal.cwms.rest.api.document.service;

import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentResult;

public interface RestApiDocumentService {
	RetrieveDocumentResult getLatestDocumentForEntity(String entityName, Integer entityId);
}
