package org.trescal.cwms.rest.api.certificate.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder @NoArgsConstructor @AllArgsConstructor @Getter
public class RestApiUploadClientCertificateOutputDto {
	private String certificateNumber;
	private Integer certificateId;
}
