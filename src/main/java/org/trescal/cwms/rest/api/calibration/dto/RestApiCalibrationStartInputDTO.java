package org.trescal.cwms.rest.api.calibration.dto;

import java.time.OffsetDateTime;

import lombok.Getter;
import lombok.Setter;

/**
 * See superclass for core fields
 */
@Getter @Setter
public class RestApiCalibrationStartInputDTO extends RestApiCalibrationInputDTO {
	// Optional
	private OffsetDateTime startTime;
	// Optional
	private String processName;
}
