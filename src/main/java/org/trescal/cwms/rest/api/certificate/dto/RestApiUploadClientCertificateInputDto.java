package org.trescal.cwms.rest.api.certificate.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.trescal.cwms.core.system.enums.IntervalUnit;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder @NoArgsConstructor @AllArgsConstructor @Getter
public class RestApiUploadClientCertificateInputDto {
	private Integer contactId;
	@NotNull
	private Integer plantId;
	@Size(max=50)
	private String externalCertificateNumber;
	@NotNull
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate calibrationDate;
	@NotNull
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate certificateDate;
	@NotNull
	private Integer duration;
	@NotNull
	private IntervalUnit intervalUnit;
	@NotNull
	private Boolean updateCalibrationDueDate;
	private Integer externalSubdivId;
	private Integer serviceTypeId;
	@Size(max=1000)
	private String remarks;
	@NotNull
	private String content;
	private Boolean optimization;
	private Boolean restriction;
	private Boolean adjustment;
	private Boolean repair;
}
