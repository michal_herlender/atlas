package org.trescal.cwms.rest.api.instrument.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.InstrumentHistory;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.db.InstrumentUsageTypeService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentInputDTO;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentOutputDTO;

@Service
public class RestApiInstrumentServiceImpl implements RestApiInstrumentService {

	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private InstrumService instrumService; 
	@Autowired
	private InstrumentModelService instModelService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private MfrService mfrService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private InstrumentUsageTypeService usageTypeService;
	
	/**
	 * Input must be previously validated with no errors to call this method
	 * 
	 * Simplified IDs are automatically set in instrument accessors
	 * 
	 * Not currently implemented due to absence in ExportInstrumentDto, and assumed not needed:
	 * defaultClientRef
	 * CalExpiresAfterNumberOfUses
	 * firmwareAccessCode
	 * flexibleFields...
	 * instrumentCharacteristics...
	 * complementaryField...
	 * measurementType...
	 * permittedNumberOfUses...
	 * replacement / scrapped stuff...
	 * usesSinceLastCalibration...
	 * 
	 * (same goes for updateInstrument) 
	 * TO DO : instrumentRange, workInstruction links
	 */
	@Override
	public RestApiInstrumentOutputDTO createIntstrument(RestApiInstrumentInputDTO inputDto) {
		
		Instrument inst = new Instrument();
		inst.setAdd(this.addressService.getReference(inputDto.getAddressid()));
		inst.setAddedBy(this.contactService.getContactOrLoggedInContact(inputDto.getContactId()));
		inst.setAddedOn(LocalDate.now());
		inst.setCalExpiresAfterNumberOfUses(false);
		inst.setCalFrequency(inputDto.getCalInterval());
		inst.setCalFrequencyUnit(inputDto.getCalIntervalUnit());
		inst.setCalibrationStandard(inputDto.getCalibrationStandard());
		inst.setClientBarcode(inputDto.getClientBarcode());
		inst.setComp(this.companyService.getReference(inputDto.getCoid()));
		inst.setCon(this.contactService.getReference(inputDto.getPersonid()));
		inst.setCustomerDescription(inputDto.getCustomerDescription());
		inst.setCustomerSpecification(inputDto.getCustomerSpecification());
		if (inst.getCustomerManaged() != null) {
			inst.setCustomerManaged(inputDto.getCustomerManaged());
		}
		inst.setDefaultServiceType(this.serviceTypeService.getReferenceOrNull(inputDto.getDefaultservicetypeid()));
		inst.setFormerBarCode(inputDto.getFormerBarcode());
		inst.setFreeTextLocation(inputDto.getFreeTextLocation());
		inst.setLoc(this.locationService.getReferenceOrNull(inputDto.getLocationid()));
		if (inputDto.getMfrid() != null)
			inst.setMfr(this.mfrService.findMfr(inputDto.getMfrid()));
		inst.setModel(this.instModelService.getReference(inputDto.getModelid()));
		inst.setModelname(inputDto.getModelname());
		inst.setNextCalDueDate(inputDto.getRecallDate());
		inst.setPlantno(inputDto.getPlantno());
		inst.setSerialno(inputDto.getSerialno());
		inst.setStatus(inputDto.getStatus());
		if (inputDto.getUsageid() != null)
			inst.setUsageType(this.usageTypeService.getReferenceOrNull(inputDto.getUsageid()));
		else
			inst.setUsageType(this.usageTypeService.findDefaultType(InstrumentUsageType.class));

		this.instrumService.save(inst);
		RestApiInstrumentOutputDTO result = new RestApiInstrumentOutputDTO(); 
		result.setPlantid(inst.getPlantid());
		return result;
	}

	/**
	 * Input must be previously validated with no errors to call this method
	 * 
	 * It's expected that all values should be provided and not just those which are being updated,
	 * as it's possible to set some things / references to null (location, mfr for example)
	 * 
	 * JPA managed entity in service layer, so it's automatically saved.
	 * As a best practice to only change required values, only changed values are updated.
	 */
	@Override
	public void updateInstrument(RestApiInstrumentInputDTO inputDto) {
		Instrument inst = this.instrumService.get(inputDto.getPlantid());
		InstrumentHistory history = new InstrumentHistory();
		boolean insertHistory = false;
		
		if (!Objects.equals(inputDto.getAddressid(), inst.getAdd().getAddrid())) {
			Address newAddress = this.addressService.getReference(inputDto.getAddressid()); 
			insertHistory = true;
			history.setAddressUpdated(true);
			history.setOldAddress(inst.getAdd());
			history.setNewAddress(newAddress);
			inst.setAdd(newAddress);
		}
		if (!Objects.equals(inputDto.getCalInterval(), inst.getCalFrequency())) {
			inst.setCalFrequency(inputDto.getCalInterval());
		}
		if (!Objects.equals(inputDto.getCalIntervalUnit(), inst.getCalFrequencyUnit())) {
			inst.setCalFrequencyUnit(inputDto.getCalIntervalUnit());
		}
		if (!Objects.equals(inputDto.getCalibrationStandard(), inst.getCalibrationStandard())) {
			inst.setCalibrationStandard(inputDto.getCalibrationStandard());
		}
		if (!Objects.equals(inputDto.getClientBarcode(), inst.getClientBarcode())) {
			inst.setClientBarcode(inputDto.getClientBarcode());
		}
		if (!Objects.equals(inputDto.getCoid(), inst.getComp().getCoid())) {
			Company newCompany = this.companyService.getReference(inputDto.getCoid());
			insertHistory = true;
			history.setCompanyUpdated(true);
			history.setOldCompany(inst.getComp());
			history.setNewCompany(newCompany);
			inst.setComp(newCompany);
		}

		if (!Objects.equals(inputDto.getPersonid(), inst.getCon().getPersonid())) {
			Contact newContact = this.contactService.getReference(inputDto.getPersonid());
			insertHistory = true;
			history.setContactUpdated(true);
			history.setOldContact(inst.getCon());
			history.setNewContact(newContact);
			inst.setCon(newContact);
		}

		if (!Objects.equals(inputDto.getCustomerDescription(), inst.getCustomerDescription())) {
			inst.setCustomerDescription(inputDto.getCustomerDescription());
		}
		
		if (inputDto.getCustomerManaged() != null && !Objects.equals(inputDto.getCustomerManaged(), inst.getCustomerManaged())) {
			inst.setCustomerManaged(inputDto.getCustomerManaged());
		}

		if (!Objects.equals(inputDto.getCustomerSpecification(), inst.getCustomerSpecification())) {
			inst.setCustomerSpecification(inputDto.getCustomerSpecification());
		}

		Integer defaultServiceTypeId = inst.getDefaultServiceType() != null ? inst.getDefaultServiceType().getServiceTypeId() : null; 
		if (!Objects.equals(inputDto.getDefaultservicetypeid(), defaultServiceTypeId)) {
			inst.setDefaultServiceType(this.serviceTypeService.getReferenceOrNull(inputDto.getDefaultservicetypeid()));
		}

		if (!Objects.equals(inputDto.getFormerBarcode(), inst.getFormerBarCode())) {
			inst.setFormerBarCode(inputDto.getFormerBarcode());
		}

		if (!Objects.equals(inputDto.getFreeTextLocation(), inst.getFreeTextLocation())) {
			inst.setFreeTextLocation(inputDto.getFreeTextLocation());
		}

		Integer locationId = inst.getLoc() != null ? inst.getLoc().getLocationid() : null;
		if (!Objects.equals(inputDto.getLocationid(), locationId)) {
			inst.setLoc(this.locationService.getReferenceOrNull(inputDto.getLocationid()));
		}

		Integer mfrid = inst.getMfr() != null ? inst.getMfr().getMfrid() : null;
		if (!Objects.equals(inputDto.getMfrid(), mfrid)) {
			if (inputDto.getMfrid() != null)
				inst.setMfr(this.mfrService.findMfr(inputDto.getMfrid()));
			else
				inst.setMfr(null);
		}

		if (!Objects.equals(inputDto.getModelid(), inst.getModel().getModelid())) {
			inst.setModel(this.instModelService.getReference(inputDto.getModelid()));
		}
		
		if (!Objects.equals(inputDto.getModelname(), inst.getModelname())) {
			inst.setModelname(inputDto.getModelname());
		}

		// TODO evaluate whether to insert recall rule, if required
		if (!Objects.equals(inputDto.getRecallDate(), inst.getNextCalDueDate())) {
			inst.setNextCalDueDate(inputDto.getRecallDate());
		}

		if (!Objects.equals(inputDto.getPlantno(), inst.getPlantno())) {
			insertHistory = true;
			history.setPlantNoUpdated(true);
			history.setOldPlantNo(inst.getPlantno());
			history.setNewPlantNo(inputDto.getPlantno());
			inst.setPlantno(inputDto.getPlantno());
		}

		if (!Objects.equals(inputDto.getSerialno(), inst.getSerialno())) {
			insertHistory = true;
			history.setSerialNoUpdated(true);
			history.setOldSerialNo(inputDto.getSerialno());
			history.setNewSerialNo(inst.getSerialno());
			inst.setSerialno(inputDto.getSerialno());
		}

		if (!Objects.equals(inputDto.getStatus(), inst.getStatus())) {
			inst.setStatus(inputDto.getStatus());
		}
		
		Integer usageId = inst.getUsageType() != null ? inst.getUsageType().getId() : null;
		if (inputDto.getUsageid() != null && !Objects.equals(inputDto.getUsageid(), usageId)) {
			inst.setUsageType(this.usageTypeService.get(inputDto.getUsageid()));
		}
		
		if (insertHistory) {
			// saved via JPA cascade
			history.setChangeBy(this.contactService.getContactOrLoggedInContact(inputDto.getContactId()));
			history.setChangeDate(new Date());
			history.setInstrument(inst);
			inst.getHistory().add(history);
		}
	}

}
