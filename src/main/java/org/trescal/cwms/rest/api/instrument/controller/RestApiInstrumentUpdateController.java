package org.trescal.cwms.rest.api.instrument.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentInputDTO;
import org.trescal.cwms.rest.api.instrument.service.RestApiInstrumentService;
import org.trescal.cwms.rest.api.instrument.validator.RestApiInstrumentUpdateValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestApiInstrumentUpdateController {

	@Autowired
	private RestApiInstrumentUpdateValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestApiInstrumentService apiInstService;
	
	public static final String API_REQUEST_MAPPING = "/api/instrument/update";

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { API_REQUEST_MAPPING }, method = RequestMethod.POST)
	public RestResultDTO<?> updateInstrument(
			@RequestBody RestApiInstrumentInputDTO inputDto, BindingResult bindingResult, Locale locale) {
		this.validator.validate(inputDto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		this.apiInstService.updateInstrument(inputDto);
		RestResultDTO<?> result = new RestResultDTO<>(true,"");
		return result;
	}
	
}
