package org.trescal.cwms.rest.api.calibration.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;

public abstract class RestApiCalibrationValidator extends AbstractBeanValidator {

	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private ContactService contactService;

	public Contact resolveContact(Integer contactId, Errors errors) {
		Contact contact = this.contactService.getContactOrLoggedInContact(contactId);
		if (contact == null && contactId != null) {
			errors.rejectValue("contactId", RestErrors.CODE_DATA_NOT_FOUND, new Integer[] {contactId}, RestErrors.MESSAGE_DATA_NOT_FOUND);
		}
		else if (contact == null) {
			errors.reject(RestErrors.CODE_CONTACT_NOT_RESOLVED, RestErrors.MESSAGE_CONTACT_NOT_RESOLVED);
		}
		return contact;
	}

	public List<JobItem> getJobItems(List<Integer> jobItemIds, Errors errors) {
		List<JobItem> result = new ArrayList<>();
		for (Integer jobItemId : jobItemIds) {
			JobItem jobItem = this.jobItemService.get(jobItemId);
			if (jobItem == null) {
				errors.rejectValue("jobItemIds", RestErrors.CODE_DATA_NOT_FOUND, new Integer[] {jobItemId}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			result.add(jobItem);
		}
		return result;
	}
	
	public List<JobItemWorkRequirement> getJobItemWorkRequirements(List<Integer> jobItemIds, Errors errors) {
		List<JobItemWorkRequirement> jiwrs = new ArrayList<>();
		for (Integer jobItemId : jobItemIds) {
			JobItem jobItem = this.jobItemService.get(jobItemId);
			if (jobItem == null) {
				errors.rejectValue("jobItemIds", RestErrors.CODE_DATA_NOT_FOUND, new Integer[] {jobItemId}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			else if (jobItem.getNextWorkReq() == null) {
				errors.rejectValue("jobItemIds", RestErrors.CODE_WR_NOT_SET, new Integer[] {jobItemId}, RestErrors.MESSAGE_WR_NOT_SET);
			}
			else {
				jiwrs.add(jobItem.getNextWorkReq());
			}
		}
		return jiwrs;
	}
	
}
