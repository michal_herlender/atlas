package org.trescal.cwms.rest.api.jobitem.service;

import org.trescal.cwms.rest.api.jobitem.dto.RestApiJobItemCreateInputDTO;
import org.trescal.cwms.rest.api.jobitem.dto.RestApiJobItemCreateOutputDTO;

public interface RestApiJobItemService {
	
	RestApiJobItemCreateOutputDTO createJobItem(RestApiJobItemCreateInputDTO inputDto);
}
