package org.trescal.cwms.rest.api.calibration.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.rest.alligator.service.RestCompleteCalibrationService;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationCancelInputDTO;
import org.trescal.cwms.rest.api.calibration.validator.RestApiCalibrationCancelValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestApiCalibrationCancelController {
	
	@Autowired
	private RestApiCalibrationCancelValidator validator;
	@Autowired
	private RestCompleteCalibrationService rccService;
	@Autowired
	private RestErrorConverter errorConverter;
	
	public static final String API_REQUEST_MAPPING = "/api/calibration/cancel";

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { API_REQUEST_MAPPING }, method = RequestMethod.POST)
	public RestResultDTO<?> cancelCalibration(
			@RequestBody RestApiCalibrationCancelInputDTO inputDto, BindingResult bindingResult, Locale locale) {
		this.validator.validate(inputDto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		rccService.cancelCalibration(inputDto);
		RestResultDTO<?> result = new RestResultDTO<>(true,"");
		return result;
	}

}
