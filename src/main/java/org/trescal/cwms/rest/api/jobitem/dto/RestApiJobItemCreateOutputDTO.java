package org.trescal.cwms.rest.api.jobitem.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiJobItemCreateOutputDTO {
	public Integer jobItemId;
}
