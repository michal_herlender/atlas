package org.trescal.cwms.rest.api.jobitem.service;

import io.jsonwebtoken.lang.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.rest.api.jobitem.dto.RestApiJobItemCreateInputDTO;
import org.trescal.cwms.rest.api.jobitem.dto.RestApiJobItemCreateOutputDTO;

import java.util.*;

@Service
public class RestApiJobItemServiceImpl implements RestApiJobItemService {

    @Autowired
    private CalibrationService calibrationService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private NewDescriptionService descriptionService;
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private CapabilityService procService;
    @Autowired
    private WorkRequirementService wrService;

    // This external ID of the "EQUIPMENT MISC" subfamily from TML is expected to never change
    private static final int TML_ID_EQUIPMENT_MISC = 591;

    /**
     * inputDto must be externally / previously validated
     */
    @Override
    public RestApiJobItemCreateOutputDTO createJobItem(RestApiJobItemCreateInputDTO inputDto) {

        Integer jobId = inputDto.getJobId();
        Contact contact = this.contactService.getContactOrLoggedInContact(inputDto.getContactId());
        Map<Integer, Integer> plantIdsAndDefaultSt = Collections.singletonMap(inputDto.getPlantId(), inputDto.getServiceTypeId());
        Integer bookedInByContactId = contact.getPersonid();
        Integer bookingInAddrId = inputDto.getAddressId();

        List<JobItem> jobItems = this.jobItemService.createJobItems(jobId, plantIdsAndDefaultSt, bookedInByContactId, bookingInAddrId);
        JobItem jobItem = jobItems.get(0);

        if (Objects.nullSafeEquals(inputDto.getAutomaticCapability(), true)) {
            JobItemWorkRequirement jiwr = jobItem.getWorkRequirements().get(0);
            if (jiwr.getWorkRequirement().getCapability() == null) {
                setAutomaticCapability(jiwr);
            }
        }

        if (Objects.nullSafeEquals(inputDto.getAutomaticContractReview(), true)) {
            Date startDate = new Date();
            Date endDate = new Date();
            this.calibrationService.automaticContractReview(contact, contact.getLocale(), jobItem, startDate, endDate, null);
        }

        RestApiJobItemCreateOutputDTO outputDto = new RestApiJobItemCreateOutputDTO();
        outputDto.setJobItemId(jobItem.getJobItemId());

        return outputDto;
    }

    /**
     * Uses a "fallback" capability from EQUIPMENT_MISC in case none is present.
     * This is intended for use with automated import tools
     * which have already performed the calibration and need to record it
     * in case equipment created / used which does not have a capability set
     */
    private void setAutomaticCapability(JobItemWorkRequirement jiwr) {
        Description subfamily = this.descriptionService.getByTmlId(TML_ID_EQUIPMENT_MISC);
        int serviceTypeId = jiwr.getWorkRequirement().getServiceType().getServiceTypeId();

        Optional<Capability> capability = procService.getActiveCapabilityUsingCapabilityFilter(
            subfamily.getId(), serviceTypeId,
            jiwr.getJobitem().getJob().getOrganisation().getSubdivid());
        if (capability.isPresent()) {
            int wrId = jiwr.getWorkRequirement().getId();
            int procId = capability.get().getId();
            int workInstructionId = 0;

            wrService.updateWorkRequirement(wrId, procId, workInstructionId, serviceTypeId, "");
        }
    }
}
