package org.trescal.cwms.rest.api.document.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentResult;
import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentService;

@Service
public class RestApiDocumentServiceImpl implements RestApiDocumentService, InitializingBean {
	@Autowired
	private List<RetrieveDocumentService<?>> retrieveBeanList;
	// Initialized after autowiring
	private Map<Class<?>, RetrieveDocumentService<?>> retrieveBeanMap;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.retrieveBeanMap = new HashMap<>();
		for (RetrieveDocumentService<?> retrieveBean : retrieveBeanList) {
			this.retrieveBeanMap.put(retrieveBean.getEntityClass(), retrieveBean);
		}
	}

	@Override
	public RetrieveDocumentResult getLatestDocumentForEntity(String entityName, Integer entityId) {
		Class<?> entityClass = RestApiDocumentType.resolveEntityClass(entityName); 
		if (entityClass == null) 
			throw new UnsupportedOperationException("Unsupported entity name : "+entityName);
		RetrieveDocumentService<?> service = this.retrieveBeanMap.get(entityClass);
		return service.getLatestDocumentForEntity(entityId);
	}	
}
