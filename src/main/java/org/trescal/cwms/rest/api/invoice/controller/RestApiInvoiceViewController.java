package org.trescal.cwms.rest.api.invoice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.invoice.projection.invoiceitem.InvoiceItemProjectionDTO;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.api.invoice.component.RestApiInvoiceDetailsConverter;
import org.trescal.cwms.rest.api.invoice.dto.RestApiInvoiceDetailsDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller @RestJsonController
public class RestApiInvoiceViewController {
	
	@Autowired
	private RestApiInvoiceDetailsConverter converter;
	@Autowired
	private InvoiceItemService invoiceItemService;

	@RequestMapping("/api/invoice/details")
	@GetMapping(produces = { Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public RestResultDTO<RestApiInvoiceDetailsDTO> getInvoiceDetails(@RequestParam(name="id", required=true) Integer invoiceId) {
		
		List<InvoiceItemProjectionDTO> invoiceItems = this.invoiceItemService.getInvoiceItemProjectionDTOs(invoiceId);
		RestApiInvoiceDetailsDTO invoiceDto = this.converter.convertToInvoiceDetailsDTO(invoiceItems);
		
		RestResultDTO<RestApiInvoiceDetailsDTO> result = new RestResultDTO<>(true, "");
		result.addResult(invoiceDto);
		
		return result;
	}
}
