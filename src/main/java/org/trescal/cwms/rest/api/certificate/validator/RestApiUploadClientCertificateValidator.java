package org.trescal.cwms.rest.api.certificate.validator;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateInputDto;

@Component
public class RestApiUploadClientCertificateValidator extends AbstractBeanValidator {

	@Autowired
	private ContactService contactService;
	@Autowired
	private InstrumService instrumentService; 
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private SubdivService subdivService;
	@Value("${cwms.config.filebrowser.maxRestFileUploadSize}")
	private Integer maxFileUploadSize;

	@Override
	public boolean supports(Class<?> clazz) {
		return RestApiUploadClientCertificateInputDto.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			RestApiUploadClientCertificateInputDto inputDto = (RestApiUploadClientCertificateInputDto) target;
			
			Instrument instrument = this.instrumentService.get(inputDto.getPlantId());
			if (instrument == null) {
				errors.rejectValue("plantId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getPlantId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			
			if (inputDto.getContactId() != null) {
				Contact contact = this.contactService.get(inputDto.getContactId());
				if (contact == null) {
					errors.rejectValue("contactId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getContactId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
				}
			}
			
			if (inputDto.getServiceTypeId() != null) {
				ServiceType serviceType = this.serviceTypeService.get(inputDto.getServiceTypeId());
				if (serviceType == null) {
					errors.rejectValue("serviceTypeId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getServiceTypeId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
				}
			}
			
			if (inputDto.getExternalSubdivId() != null) {
				Subdiv subdiv = this.subdivService.get(inputDto.getExternalSubdivId());
				if (subdiv == null) {
					errors.rejectValue("externalSubdivId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getExternalSubdivId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
				}
			}
			
			byte[] fileData = Base64.getDecoder().decode(inputDto.getContent());
			if (fileData.length > maxFileUploadSize) {
				errors.rejectValue("file", RestErrors.CODE_FILE_SIZE, null, RestErrors.MESSAGE_FILE_SIZE);
			}
		}
	}

}
