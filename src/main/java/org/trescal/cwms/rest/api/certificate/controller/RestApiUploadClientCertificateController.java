package org.trescal.cwms.rest.api.certificate.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateInputDto;
import org.trescal.cwms.rest.api.certificate.dto.RestApiUploadClientCertificateOutputDto;
import org.trescal.cwms.rest.api.certificate.service.RestApiCertificateService;
import org.trescal.cwms.rest.api.certificate.validator.RestApiUploadClientCertificateValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@RestJsonController @Controller
public class RestApiUploadClientCertificateController {
	@Autowired
	private RestApiCertificateService uploadService;
	@Autowired
	private RestApiUploadClientCertificateValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(path="/api/certificate/createclient", method=RequestMethod.POST)
	public @ResponseBody RestResultDTO<?> uploadCertificate(
			@RequestBody RestApiUploadClientCertificateInputDto inputDto,
			BindingResult bindingResult, Locale locale) {
		this.validator.validate(inputDto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		RestApiUploadClientCertificateOutputDto outputDto = this.uploadService.uploadCertificate(inputDto);
		RestResultDTO<RestApiUploadClientCertificateOutputDto> result = new RestResultDTO<>(true, "");
		result.addResult(outputDto);
		return result;
	}
}
