package org.trescal.cwms.rest.api.jobitem.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.rest.api.jobitem.dto.RestApiJobItemCreateInputDTO;
import org.trescal.cwms.rest.api.jobitem.dto.RestApiJobItemCreateOutputDTO;
import org.trescal.cwms.rest.api.jobitem.service.RestApiJobItemService;
import org.trescal.cwms.rest.api.jobitem.validator.RestApiJobItemValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestApiJobItemCreateController {
	
	@Autowired
	private RestApiJobItemService apiJobItemService;
	@Autowired
	private RestApiJobItemValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	
	public static final String API_REQUEST_MAPPING = "/api/jobitem/create";

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { API_REQUEST_MAPPING }, method = RequestMethod.POST)
	public RestResultDTO<RestApiJobItemCreateOutputDTO> createInstrument(
			@RequestBody RestApiJobItemCreateInputDTO inputDto, BindingResult bindingResult, Locale locale) {
		this.validator.validate(inputDto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		RestApiJobItemCreateOutputDTO outputDto = apiJobItemService.createJobItem(inputDto);
		RestResultDTO<RestApiJobItemCreateOutputDTO> result = new RestResultDTO<>(true,"");
		result.addResult(outputDto);
		return result;
	}
}
