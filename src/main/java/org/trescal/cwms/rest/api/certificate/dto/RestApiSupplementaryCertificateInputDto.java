package org.trescal.cwms.rest.api.certificate.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiSupplementaryCertificateInputDto {
	@NotNull
	private Integer certificateId;
	// Optional
	private Integer contactId;
}
