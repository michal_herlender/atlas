package org.trescal.cwms.rest.api.jobitem.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.api.jobitem.dto.RestApiJobItemCreateInputDTO;

@Component
public class RestApiJobItemValidator extends AbstractBeanValidator {

	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private InstrumService instrumService;
	@Autowired
	private JobService jobService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			RestApiJobItemCreateInputDTO inputDto = (RestApiJobItemCreateInputDTO) target;
			// Verify instrument exists
			Instrument instrument = instrumService.get(inputDto.getPlantId());
			if (instrument == null) {
				errors.rejectValue("plantId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getPlantId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			// For this API, we allow instrument to already be on an active job item; as this API is mainly for synchronization
			// e.g. business case that instrumnt has already been received forrepair/other service after onsite calibration failure which is sync'd 
			// Verify job exists
			Job job = this.jobService.get(inputDto.getJobId());
			if (job == null) {
				errors.rejectValue("jobId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getJobId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			// Verify contact exists, if specified
			if ((inputDto.getContactId() != null) && this.contactService.getReference(inputDto.getContactId()) == null) {
				errors.rejectValue("contactId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getContactId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			// Verify address exists
			Address address = this.addressService.getReference(inputDto.getAddressId());
			if (address == null) {
				errors.rejectValue("addressId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getAddressId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			// Verify service type exists
			ServiceType serviceType = this.serviceTypeService.getReference(inputDto.getServiceTypeId());
			if (serviceType == null) {
				errors.rejectValue("serviceTypeId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getServiceTypeId()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			
			// TODO restrict functionality to site jobs?
		}
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return RestApiJobItemCreateInputDTO.class.isAssignableFrom(clazz);
	}
}
