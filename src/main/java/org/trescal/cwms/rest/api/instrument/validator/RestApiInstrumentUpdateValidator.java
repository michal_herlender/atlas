package org.trescal.cwms.rest.api.instrument.validator;

import javax.validation.groups.Default;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentInputDTO;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentInputDTO.RestApiInstrumentUpdate;

@Component
public class RestApiInstrumentUpdateValidator extends RestApiInstrumentValidator {

	@Autowired
	private InstrumService instrumService; 
		
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors, Default.class, RestApiInstrumentUpdate.class);
		if (!errors.hasErrors()) {
			RestApiInstrumentInputDTO inputDto = (RestApiInstrumentInputDTO) target;
			if (this.instrumService.getReference(inputDto.getPlantid()) == null) {
				errors.rejectValue("plantid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] {inputDto.getPlantid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}
	}
}
