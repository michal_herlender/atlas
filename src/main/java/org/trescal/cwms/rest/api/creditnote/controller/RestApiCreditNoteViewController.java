package org.trescal.cwms.rest.api.creditnote.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.db.CreditNoteItemService;
import org.trescal.cwms.core.pricing.creditnote.projection.items.CreditNoteItemProjectionDTO;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.api.creditnote.component.RestApiCreditNoteDetailsConverter;
import org.trescal.cwms.rest.api.creditnote.dto.RestApiCreditNoteDetailsDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller @RestJsonController
public class RestApiCreditNoteViewController {

	@Autowired
	private RestApiCreditNoteDetailsConverter converter;
	@Autowired
	private CreditNoteItemService creditNoteItemService;

	@RequestMapping("/api/creditnote/details")
	@GetMapping(produces = { Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public RestResultDTO<RestApiCreditNoteDetailsDTO> getCreditNoteDetails(@RequestParam(name="id", required=true) Integer creditNoteId) {
		
		List<CreditNoteItemProjectionDTO> creditNoteItems = this.creditNoteItemService.getCreditNoteItemProjectionDTOs(creditNoteId);
		RestApiCreditNoteDetailsDTO creditNoteDto = this.converter.convertToCreditNoteDetailsDTO(creditNoteItems);
		
		RestResultDTO<RestApiCreditNoteDetailsDTO> result = new RestResultDTO<>(true, "");
		result.addResult(creditNoteDto);
		
		return result;
	}	
}
