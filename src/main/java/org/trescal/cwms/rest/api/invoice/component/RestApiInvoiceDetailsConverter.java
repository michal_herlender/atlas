package org.trescal.cwms.rest.api.invoice.component;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.pricing.invoice.projection.invoiceitem.InvoiceItemProjectionDTO;
import org.trescal.cwms.rest.api.invoice.dto.RestApiInvoiceDetailsDTO;
import org.trescal.cwms.rest.api.invoice.dto.RestApiInvoiceItemDetailsDTO;

@Component
public class RestApiInvoiceDetailsConverter {

	public RestApiInvoiceDetailsDTO convertToInvoiceDetailsDTO(List<InvoiceItemProjectionDTO> invoiceItems) {
		List<RestApiInvoiceItemDetailsDTO> invoiceItemDTOs = convertToInvoiceItemDTOList(invoiceItems);
		
		return RestApiInvoiceDetailsDTO.builder()
				.invoiceItems(invoiceItemDTOs)
				.build();
	}
	
	private List<RestApiInvoiceItemDetailsDTO> convertToInvoiceItemDTOList(List<InvoiceItemProjectionDTO> invoiceItems) {
		return invoiceItems.stream()
				.map(invoiceItem -> RestApiInvoiceItemDetailsDTO.builder()
						.itemId(invoiceItem.getItemId())
						.itemNumber(invoiceItem.getItemNumber())
						.description(invoiceItem.getDescription())
						.quantity(invoiceItem.getQuantity())
						.finalCost(invoiceItem.getFinalCost())
						.taxable(invoiceItem.getTaxable())
						.taxAmount(invoiceItem.getTaxAmount())
						.costType(invoiceItem.getNominalCostType())
						.jobItemId(invoiceItem.getJobItemId())
						.expenseItemNumber(invoiceItem.getExpenseItemNumber())
						.expenseItemModelId(invoiceItem.getExpenseItemModelId())
						.expenseItemJobId(invoiceItem.getExpenseItemJobId())
						.expenseItemClientRef(invoiceItem.getExpenseItemClientRef())
						.build())
				.collect(Collectors.toList());
	}
}