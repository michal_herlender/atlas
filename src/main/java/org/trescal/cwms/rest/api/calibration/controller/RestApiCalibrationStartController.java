package org.trescal.cwms.rest.api.calibration.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.rest.alligator.component.RestStartCalibrationService;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationStartInputDTO;
import org.trescal.cwms.rest.api.calibration.dto.RestApiCalibrationStartOutputDTO;
import org.trescal.cwms.rest.api.calibration.validator.RestApiCalibrationStartValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestApiCalibrationStartController {
	
	@Autowired
	private RestApiCalibrationStartValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestStartCalibrationService rscService;
	
	public static final String API_REQUEST_MAPPING = "/api/calibration/start";

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { API_REQUEST_MAPPING }, method = RequestMethod.POST)
	public RestResultDTO<RestApiCalibrationStartOutputDTO> startCalibration(
			@RequestBody RestApiCalibrationStartInputDTO inputDto, BindingResult bindingResult, Locale locale) {
		this.validator.validate(inputDto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}
		RestApiCalibrationStartOutputDTO outputDto = rscService.startCalibration(inputDto);
		RestResultDTO<RestApiCalibrationStartOutputDTO> result = new RestResultDTO<>(true,"");
		result.addResult(outputDto);
		return result;
	}
}
