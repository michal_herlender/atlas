package org.trescal.cwms.rest.api.document.component;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentResult;
import org.trescal.cwms.rest.api.document.dto.RestApiDocumentOutputDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Component
public class RestApiDocumentOutputConverter {
	
	public RestResultDTO<RestApiDocumentOutputDTO> convert(RetrieveDocumentResult documentResult) {
		RestResultDTO<RestApiDocumentOutputDTO> restResult = new RestResultDTO<>();
		if (documentResult.isSuccess()) {
			RestApiDocumentOutputDTO outputDto = RestApiDocumentOutputDTO.builder()
					.data(documentResult.getData())
					.filename(documentResult.getFilename())
					.build();
			
			restResult.setSuccess(true);
			restResult.setMessage("");
			restResult.addResult(outputDto);
		}
		else {
			restResult.setSuccess(false);
			restResult.setMessage(documentResult.getErrorMessage());
		}
		return restResult;
	}
}
