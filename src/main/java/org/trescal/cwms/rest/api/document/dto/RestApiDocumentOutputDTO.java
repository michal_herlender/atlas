package org.trescal.cwms.rest.api.document.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder @NoArgsConstructor @AllArgsConstructor @Getter
public class RestApiDocumentOutputDTO {
	private String filename;
	private byte[] data;
}
