package org.trescal.cwms.rest.api.jobitem.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiJobItemCreateInputDTO {
    // Optional (otherwise authenticated user used)
    private Integer contactId;
    @NotNull
    private Integer plantId;
    @NotNull
    private Integer jobId;
    @NotNull
    private Integer serviceTypeId;
    @NotNull
    private Integer addressId;
    // Optional (default is false)
    private Boolean automaticContractReview;
    // Optional (default is false)
    private Boolean automaticCapability;
}
