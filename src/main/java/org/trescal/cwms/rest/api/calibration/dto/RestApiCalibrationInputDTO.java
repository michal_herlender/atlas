package org.trescal.cwms.rest.api.calibration.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;

import lombok.Getter;
import lombok.Setter;

/**
 * Contains elements which are common to starting and completing calibration
 *
 */
@Getter @Setter
public abstract class RestApiCalibrationInputDTO {
	@NotNull @Size(min=1)
	private List<Integer> jobItemIds;
	// Optional
	private Integer contactId;
	// Optional
	private CalibrationClass dataContent;
	// Optional
	private Boolean bypassAuthorisation;
}
