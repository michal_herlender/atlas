package org.trescal.cwms.rest.api.creditnote.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder @NoArgsConstructor @AllArgsConstructor @Getter
public class RestApiCreditNoteItemDetailsDTO {
	private Integer itemId;
	private Integer itemNumber;
	private String description;
	private Integer quantity;
	private BigDecimal finalCost;
	private Boolean taxable;
	private BigDecimal taxAmount;
	// Optional
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private CostType costType;
}
