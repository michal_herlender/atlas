package org.trescal.cwms.rest.api.calibration.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestApiCalibrationCancelInputDTO extends RestApiCalibrationInputDTO {
	// Optional
	private String remarks;
}
