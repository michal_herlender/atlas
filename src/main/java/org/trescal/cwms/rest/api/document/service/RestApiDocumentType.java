package org.trescal.cwms.rest.api.document.service;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

public enum RestApiDocumentType {
	INVOICE("invoice", Invoice.class),
	CREDIT_NOTE("creditnote", CreditNote.class);
	
	private final String pathVariable;
	private final Class<?> entityClass;
	private static Map<String, Class<?>> supportedPathVariables;
	
	private RestApiDocumentType(String pathVariable, Class<?> entityClass) {
		this.pathVariable = pathVariable;
		this.entityClass = entityClass;
	}
	
	static {
		supportedPathVariables = new HashMap<>();
		EnumSet.allOf(RestApiDocumentType.class)
			.forEach(type -> supportedPathVariables.put(type.pathVariable, type.entityClass));
	}
	
	public static Class<?> resolveEntityClass(String pathVariable) {
		return supportedPathVariables.get(pathVariable);
	}	
}
