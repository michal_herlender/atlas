package org.trescal.cwms.rest.api.instrument.validator;

import javax.validation.groups.Default;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.rest.api.instrument.dto.RestApiInstrumentInputDTO.RestApiInstrumentCreate;

@Component
public class RestApiInstrumentCreateValidator extends RestApiInstrumentValidator {
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors, Default.class, RestApiInstrumentCreate.class);
	}
}
