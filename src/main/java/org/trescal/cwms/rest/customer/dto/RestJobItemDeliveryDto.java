package org.trescal.cwms.rest.customer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class RestJobItemDeliveryDto {

	private Integer jobItemId;
	private Integer jobItemNo;
	private String jobNo;
	private Integer plantId;
	private String plantNo;
	private String serialNo;
	private String customerDescription;
	private String customerSpecification;
	private String instrumentModelName;
	private String instrumentFamily;
	private String instrumentSubFamily;
	private String manufacturer;
	private String model;
	private LocalDate nextCalDate;
	private Integer calFrequency;
	private IntervalUnit calFrequencyUnit;
	private List<RestCalibrationDto> calibs;
	private List<RestCertificateDto> certifs;

	public RestJobItemDeliveryDto(Integer jobItemId, Integer jobItemNo, String jobNo, Integer plantId, String plantNo,
			String serialNo, String customerDescription, String customerSpecification, String instrumentModelName,
			String instrumentFamily, String instrumentSubFamily, String manufacturer, String model,
			LocalDate nextCalDate, Integer calFrequency, IntervalUnit calFrequencyUnit) {
		this.jobItemId = jobItemId;
		this.jobItemNo = jobItemNo;
		this.jobNo = jobNo;
		this.plantId = plantId;
		this.plantNo = plantNo;
		this.serialNo = serialNo;
		this.customerDescription = customerDescription;
		this.customerSpecification = customerSpecification;
		this.instrumentModelName = instrumentModelName;
		this.instrumentFamily = instrumentFamily;
		this.instrumentSubFamily = instrumentSubFamily;
		this.manufacturer = manufacturer;
		this.model = model;
		this.nextCalDate = nextCalDate;
		this.calFrequency = calFrequency;
		this.calFrequencyUnit = calFrequencyUnit;
		this.calibs = new ArrayList<>();
		this.certifs = new ArrayList<>();
	}

	public RestJobItemDeliveryDto(RestInstrumentFlatDTO instrumentDto) {
		super();
		this.jobItemId = instrumentDto.getJobItemId();
		this.jobItemNo = instrumentDto.getJobItemNo();
		this.jobNo = instrumentDto.getJobNo();
		this.plantId = instrumentDto.getPlantId();
		this.plantNo = instrumentDto.getPlantNo();
		this.serialNo = instrumentDto.getSerialNo();
		this.customerDescription = instrumentDto.getCustomerDescription();
		this.customerSpecification = instrumentDto.getCustomerSpecification();
		this.instrumentModelName = instrumentDto.getInstrModelName();
		this.instrumentFamily = instrumentDto.getFamily();
		this.instrumentSubFamily = instrumentDto.getSubFamily();
		this.manufacturer = instrumentDto.getManufacturer().replace('/', ' ').trim().isEmpty()
				? instrumentDto.getInstrumentMfr()
				: instrumentDto.getManufacturer();
		this.model = instrumentDto.getModel().replace('/', ' ').trim().isEmpty() ? instrumentDto.getInstrumentMfrModel()
				: instrumentDto.getModel();
		this.nextCalDate = instrumentDto.getNextCalDate();
		this.calFrequency = instrumentDto.getCalFrequency();
		this.calFrequencyUnit = instrumentDto.getCalFrequencyUnit();
	}
}