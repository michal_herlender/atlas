package org.trescal.cwms.rest.customer.dto;

import java.util.List;
import java.util.Locale;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestInstrumentRequestDTO {

	private List<Integer> plantIds;
	private Locale locale;
}