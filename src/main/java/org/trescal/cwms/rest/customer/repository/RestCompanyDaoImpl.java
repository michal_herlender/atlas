package org.trescal.cwms.rest.customer.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency_;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode_;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRate_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformation;

@Repository
public class RestCompanyDaoImpl extends BaseDaoImpl<Company, Integer> implements RestCompanyDao {

	@Override
	protected Class<Company> getEntity() {
		return Company.class;
	}

	private void searchCompanyInformation(PagedResultSet<RestCompanyInformation> prs,
			Integer companyId, String legalId, LocalDate modifiedFrom,
			Integer orgId) {
		super.completePagedResultSet(prs, RestCompanyInformation.class, cb -> cq -> {
			Root<Company> company = cq.from(Company.class);
			Join<Company, BusinessArea> businessArea = company.join(Company_.businessarea, JoinType.LEFT);
			Join<Company, CompanyGroup> companyGroup = company.join(Company_.companyGroup, JoinType.LEFT);
			// Inner join, so that we return only company information where a settings object exists
			Join<Company, CompanySettingsForAllocatedCompany> companySettings = 
					company.join(Company_.settingsForAllocatedCompanies, JoinType.INNER);
			companySettings.on(cb.equal(companySettings.get(CompanySettingsForAllocatedCompany_.organisation), orgId));
			Join<CompanySettingsForAllocatedCompany, VatRate> vatRate = 
					companySettings.join(CompanySettingsForAllocatedCompany_.vatrate, JoinType.INNER);
			Join<CompanySettingsForAllocatedCompany, InvoiceFrequency> invoiceFrequency = 
					companySettings.join(CompanySettingsForAllocatedCompany_.invoiceFrequency, JoinType.INNER);
			Join<CompanySettingsForAllocatedCompany, PaymentMode> paymentMode = 
					companySettings.join(CompanySettingsForAllocatedCompany_.paymentMode, JoinType.INNER);
			Join<Company, Address> legalAddress = company.join(Company_.legalAddress, JoinType.LEFT);
			Join<Address, Country> country = legalAddress.join(Address_.country, JoinType.LEFT);
			Join<Company, SupportedCurrency> currency = company.join(Company_.currency, JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			if (companyId != null) {
				clauses.getExpressions().add(cb.equal(company.get(Company_.coid), companyId));
			}
			if (legalId != null) {
				clauses.getExpressions().add(cb.equal(company.get(Company_.legalIdentifier), legalId));
			}
			if (modifiedFrom != null) {
				Date modifiedFromDate = modifiedFrom == null ? null
						: DateTools.dateFromLocalDate(modifiedFrom);
				Predicate disjunction = cb.disjunction();
				
				disjunction.getExpressions().add(cb.greaterThanOrEqualTo(businessArea.get(BusinessArea_.lastModified), modifiedFromDate));
				disjunction.getExpressions().add(cb.greaterThanOrEqualTo(company.get(Company_.lastModified), modifiedFromDate));
				disjunction.getExpressions().add(cb.greaterThanOrEqualTo(companyGroup.get(CompanyGroup_.lastModified), modifiedFromDate));
				disjunction.getExpressions().add(cb.greaterThanOrEqualTo(companySettings.get(CompanySettingsForAllocatedCompany_.lastModified), modifiedFromDate));
				disjunction.getExpressions().add(cb.greaterThanOrEqualTo(legalAddress.get(Address_.lastModified), modifiedFromDate));
				disjunction.getExpressions().add(cb.greaterThanOrEqualTo(vatRate.get(VatRate_.lastModified), modifiedFromDate));
				
				clauses.getExpressions().add(disjunction);
			}
			
			cq.where(clauses);
			CompoundSelection<RestCompanyInformation> selection = cb.construct(RestCompanyInformation.class, 
					company.get(Company_.coid),
					company.get(Company_.coname), 
					company.get(Company_.legalIdentifier),
					company.get(Company_.fiscalIdentifier), 
					companyGroup.get(CompanyGroup_.groupName),
					company.get(Company_.companyRole),
					businessArea.get(BusinessArea_.name),
					vatRate.get(VatRate_.rate), 
					companySettings.get(CompanySettingsForAllocatedCompany_.taxable),
					legalAddress.get(Address_.addr1),
					legalAddress.get(Address_.addr2), 
					legalAddress.get(Address_.addr3), 
					legalAddress.get(Address_.town),
					legalAddress.get(Address_.county), 
					legalAddress.get(Address_.postcode),
					country.get(Country_.countryCode), 
					currency.get(SupportedCurrency_.currencyCode), 
					invoiceFrequency.get(InvoiceFrequency_.code),
					paymentMode.get(PaymentMode_.code), 
					companySettings.get(CompanySettingsForAllocatedCompany_.paymentterm),
					companySettings.get(CompanySettingsForAllocatedCompany_.active),
					companySettings.get(CompanySettingsForAllocatedCompany_.onStop), 
					companySettings.get(CompanySettingsForAllocatedCompany_.invoiceFactoring),
					company.get(Company_.createDate),
					businessArea.get(BusinessArea_.lastModified),
					company.get(Company_.lastModified),
					companyGroup.get(CompanyGroup_.lastModified),
					companySettings.get(CompanySettingsForAllocatedCompany_.lastModified),
					legalAddress.get(Address_.lastModified),
					vatRate.get(VatRate_.lastModified)
			);

			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.asc(company.get(Company_.coid)));

			return Triple.of(company, selection, order);
		});
	}

	@Override
	public void searchCompanyInformationByCompanyId(PagedResultSet<RestCompanyInformation> prs, 
			Integer companyId, Integer orgId) {
		searchCompanyInformation(prs, companyId, null, null, orgId);
	}

	@Override
	public void searchCompanyInformationByLegalId(PagedResultSet<RestCompanyInformation> prs, 
			String legalId, Integer orgId) {
		searchCompanyInformation(prs, null, legalId, null, orgId);
	}

	@Override
	public void searchCompanyInformationByModifiedFrom(PagedResultSet<RestCompanyInformation> prs, 
			LocalDate modifiedFrom, Integer orgId) {
		searchCompanyInformation(prs, null, null, modifiedFrom, orgId);
	}
}