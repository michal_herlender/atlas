package org.trescal.cwms.rest.customer.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.customer.component.RestCompanyInformationDtoConverter;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformation;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformationDto;
import org.trescal.cwms.rest.customer.service.RestCompanyService;

@Controller
@RestJsonController
public class RestCompanyController {

	@Autowired
	private RestCompanyService restCompanyService;
	@Autowired
	private RestCompanyInformationDtoConverter converter;
	
	private static int RESULTS_PER_PAGE = 100;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/customer/company", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestPagedResultDTO<RestCompanyInformationDto> getCompanyInformation(
			@RequestParam(name = "companyId", required = false) Integer companyId,
			@RequestParam(name = "legalId", required = false) String legalId,
			@RequestParam(name = "orgId", required = true) Integer orgId,
			@RequestParam(name = "modifiedFrom", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate modifiedFrom,
			@RequestParam(name = "currentPage", required = false, defaultValue = "1") Integer currentPage) {
		
		PagedResultSet<RestCompanyInformation> prs = new PagedResultSet<>(RESULTS_PER_PAGE, currentPage);
		if (companyId != null)
			restCompanyService.searchCompanyInformationByCompanyId(prs, companyId, orgId);
		else if (legalId != null)
			restCompanyService.searchCompanyInformationByLegalId(prs, legalId, orgId);
		else
			restCompanyService.searchCompanyInformationByModifiedFrom(prs, modifiedFrom, orgId);
		
		RestPagedResultDTO<RestCompanyInformationDto> result = new RestPagedResultDTO<>(true, "", prs.getResultsPerPage(), prs.getCurrentPage());
		result.setResultsCount(prs.getResultsCount());
		prs.getResults().forEach(info -> result.addResult(converter.convert(info)));
		return result;
	}
}