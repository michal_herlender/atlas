package org.trescal.cwms.rest.customer.repository;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.rest.customer.dto.RestInstrumentFlatDTO;

public interface RestJobItemDao {

	List<RestInstrumentFlatDTO> getInstrumentCalibrationDetails(Collection<Integer> plantIds, Locale locale);
}