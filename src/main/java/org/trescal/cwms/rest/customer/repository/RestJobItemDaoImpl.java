package org.trescal.cwms.rest.customer.repository;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AbstractDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.rest.customer.dto.RestInstrumentFlatDTO;

@Repository
public class RestJobItemDaoImpl extends AbstractDaoImpl implements RestJobItemDao {

	@Override
	public List<RestInstrumentFlatDTO> getInstrumentCalibrationDetails(Collection<Integer> plantIds, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<RestInstrumentFlatDTO> cq = cb.createQuery(RestInstrumentFlatDTO.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			Join<Instrument, JobItem> jobItem = instrument.join(Instrument_.jobItems, JoinType.LEFT);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job, JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			Join<InstrumentModel, Description> subFamily = model.join(InstrumentModel_.description);
			Expression<String> subFamilyName = joinTranslation(cb, subFamily, Description_.translations, locale);
			Join<Description, InstrumentModelFamily> family = subFamily.join(Description_.family);
			Expression<String> familyName = joinTranslation(cb, family, InstrumentModelFamily_.translation, locale);
			Join<Instrument, Mfr> instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			Join<InstrumentModel, Mfr> modelMfr = model.join(InstrumentModel_.mfr, JoinType.LEFT);
			Join<JobItem, CertLink> certLinks = jobItem.join(JobItem_.certLinks, JoinType.LEFT);
			Join<CertLink, Certificate> certificate = certLinks.join(CertLink_.cert, JoinType.LEFT);
			cq.where(instrument.get(Instrument_.plantid).in(plantIds));
			cq.select(cb.construct(RestInstrumentFlatDTO.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), job.get(Job_.jobno), instrument.get(Instrument_.plantid),
					instrument.get(Instrument_.plantno), instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.customerDescription), instrument.get(Instrument_.customerSpecification),
					modelName, familyName, subFamilyName, modelMfr.get(Mfr_.name), instrumentMfr.get(Mfr_.name),
					model.get(InstrumentModel_.model), instrument.get(Instrument_.modelname),
					instrument.get(Instrument_.nextCalDueDate), instrument.get(Instrument_.calFrequency),
					instrument.get(Instrument_.calFrequencyUnit), certificate.get(Certificate_.certid),
					certificate.get(Certificate_.certno), certificate.get(Certificate_.certDate),
					certificate.get(Certificate_.calDate),
					certificate.get(Certificate_.calibrationVerificationStatus)));
			return cq;
		});
	}
}