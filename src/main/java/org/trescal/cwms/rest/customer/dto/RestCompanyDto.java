package org.trescal.cwms.rest.customer.dto;

import java.util.List;

import org.trescal.cwms.core.company.dto.SubdivKeyValue;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestCompanyDto {
	
	private int coid;
	private String coname;
	private List<SubdivKeyValue> subdivisions;

	public RestCompanyDto() {
		
	}
	
	public RestCompanyDto(int coid,String coname,List<SubdivKeyValue> subdivisions) {
		this.coid = coid;
		this.coname = coname;
		this.subdivisions = subdivisions;
	}
	
	public int getCoid() {
		return coid;
	}
	public void setCoid(int coid) {
		this.coid = coid;
	}
	public String getConame() {
		return coname;
	}
	public void setConame(String coname) {
		this.coname = coname;
	}
	public List<SubdivKeyValue> getSubdivisions() {
		return subdivisions;
	}
	public void setSubdivisions(List<SubdivKeyValue> subdivisions) {
		this.subdivisions = subdivisions;
	}


}
