package org.trescal.cwms.rest.customer.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.rest.customer.dto.RestCertificateDto;
import org.trescal.cwms.rest.customer.dto.RestInstrumentFlatDTO;
import org.trescal.cwms.rest.customer.dto.RestJobItemDeliveryDto;
import org.trescal.cwms.rest.customer.repository.RestJobItemDao;

@Service
public class RestJobItemServiceImpl implements RestJobItemService {

	/*
	 * There shouldn't be too many plantIds inside the IN-clause of the SQL
	 * statement, hence we split the collection.
	 */
	private static final int SPLIT_INTERVAL = 100;

	@Autowired
	private RestJobItemDao jobItemDao;

	@Override
	public List<RestJobItemDeliveryDto> getJobItemInformation(List<Integer> plantIds, Locale locale) {
		List<List<Integer>> splitPlantIds = new ArrayList<List<Integer>>();
		int maxSplit = plantIds.size() / SPLIT_INTERVAL;
		for (int i = 0; i < maxSplit; i++)
			splitPlantIds.add(plantIds.subList(SPLIT_INTERVAL * i, SPLIT_INTERVAL * (i + 1)));
		splitPlantIds.add(plantIds.subList(maxSplit, plantIds.size()));
		List<RestInstrumentFlatDTO> instrumentFlatDetails = splitPlantIds.parallelStream()
				.flatMap(pids -> jobItemDao.getInstrumentCalibrationDetails(pids, locale).stream())
				.collect(Collectors.toList());
		Map<Integer, List<RestInstrumentFlatDTO>> instrumentMap = instrumentFlatDetails.stream()
				.collect(Collectors.groupingBy(RestInstrumentFlatDTO::getPlantId));
		return instrumentMap.entrySet().stream().<RestJobItemDeliveryDto>flatMap(ikv -> {
			RestInstrumentFlatDTO instrRef = ikv.getValue().get(0);
			if (instrRef.getJobItemId() == null)
				return Stream.of(new RestJobItemDeliveryDto(instrRef));
			else {
				Map<Integer, List<RestInstrumentFlatDTO>> jobItemMap = ikv.getValue().stream()
						.collect(Collectors.groupingBy(RestInstrumentFlatDTO::getJobItemId));
				return jobItemMap.entrySet().stream().map(jikv -> {
					RestInstrumentFlatDTO jobItemRef = jikv.getValue().get(0);
					RestJobItemDeliveryDto jobItem = new RestJobItemDeliveryDto(jobItemRef);
					jobItem.setCertifs(jobItemRef.getCertifId() == null ? Collections.emptyList()
							: jikv.getValue().stream()
									.collect(Collectors.groupingBy(RestInstrumentFlatDTO::getCertifId)).entrySet()
									.stream().map(certkv -> {
										RestInstrumentFlatDTO certRef = certkv.getValue().get(0);
										return new RestCertificateDto(certRef.getCertifId(), certRef.getCertifNo(),
												certRef.getCertDate(), certRef.getCalDate(), null,
												certRef.getCertificateCalibrationStatus());
									}).collect(Collectors.toList()));
					return jobItem;
				});
			}
		}).collect(Collectors.toList());
	}
}