package org.trescal.cwms.rest.customer.dto;

import static java.lang.Math.toIntExact;

import java.time.LocalDate;


public class RestCustomerDeliveryDto {
	
	private int deliveryId;
	private LocalDate deliveryDate;
	private String deliveryNumber;
	private int itemsCount;
	
	public RestCustomerDeliveryDto() {
		
	}
	
	public RestCustomerDeliveryDto(int deliveryId, LocalDate deliveryDate,String deliveryNumber,long itemsCounts) {
		this.deliveryId = deliveryId;
		this.deliveryDate = deliveryDate;
		this.deliveryNumber = deliveryNumber;
		this.itemsCount = toIntExact(itemsCounts);
	}	
	
	public int getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(int deliveryId) {
		this.deliveryId = deliveryId;
	}
	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryNumber() {
		return deliveryNumber;
	}
	public void setDeliveryNumber(String deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}
	public int getItemsCount() {
		return itemsCount;
	}
	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}
	
	
	
}
