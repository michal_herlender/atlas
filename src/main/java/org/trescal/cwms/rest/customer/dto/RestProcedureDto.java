package org.trescal.cwms.rest.customer.dto;

import org.trescal.cwms.spring.model.KeyValue;

public class RestProcedureDto {
	
	private int id;
	private String name;
	private String description;
	private String process;
	private KeyValue<Integer,String> subFamily;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public KeyValue<Integer,String> getSubFamily() {
		return subFamily;
	}
	public void setSubFamily(KeyValue<Integer,String> subFamily) {
		this.subFamily = subFamily;
	}
	
}
