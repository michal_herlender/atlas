package org.trescal.cwms.rest.customer.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RestDeliveryDetailDto {

	private int deliveryId;
	private String deliveryNo;
	private List<RestJobItemDeliveryDto> jobitems;

	public RestDeliveryDetailDto(int deliveryId, String deliveryNo) {
		this.deliveryId = deliveryId;
		this.deliveryNo = deliveryNo;
		this.jobitems = new ArrayList<RestJobItemDeliveryDto>();
	}
}