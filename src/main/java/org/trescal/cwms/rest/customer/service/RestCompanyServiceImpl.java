package org.trescal.cwms.rest.customer.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.customer.dto.RestCompanyDto;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformation;
import org.trescal.cwms.rest.customer.repository.RestCompanyDao;

@Service
public class RestCompanyServiceImpl implements RestCompanyService {

	@Autowired
	private RestCompanyDao restCompanyDao;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private SubdivService subdivServ;

	public List<RestCompanyDto> getCustomerCompanies(Subdiv subdiv, Boolean active) {
		List<CompanyKeyValue> companies = compServ.findCustomerCompaniesBySubdiv(subdiv, active);
		List<RestCompanyDto> companiesRest = new ArrayList<RestCompanyDto>();
		RestCompanyDto company;
		for (CompanyKeyValue co : companies) {
			company = new RestCompanyDto();
			company.setCoid(co.getKey());
			company.setConame(co.getValue());
			company.setSubdivisions(subdivServ.getAllSubdivFromCompany(co.getKey(), active));
			companiesRest.add(company);
		}
		return companiesRest;
	}

	@Override
	public void searchCompanyInformationByCompanyId(PagedResultSet<RestCompanyInformation> prs, Integer companyId, Integer orgId) {
		restCompanyDao.searchCompanyInformationByCompanyId(prs, companyId, orgId);
	}

	@Override
	public void searchCompanyInformationByLegalId(PagedResultSet<RestCompanyInformation> prs, String legalId, Integer orgId) {
		restCompanyDao.searchCompanyInformationByLegalId(prs, legalId, orgId);
	}

	@Override
	public void searchCompanyInformationByModifiedFrom(PagedResultSet<RestCompanyInformation> prs, LocalDate modifiedFrom, Integer orgId) {
		restCompanyDao.searchCompanyInformationByModifiedFrom(prs, modifiedFrom, orgId);
	}
}