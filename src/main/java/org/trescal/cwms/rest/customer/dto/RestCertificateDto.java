package org.trescal.cwms.rest.customer.dto;

import java.util.Date;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RestCertificateDto {

	private int certificateId;
	private String certificateNo;
	private Date certificateDate;
	private Date calibrationDate;
	private Integer certCalId;
	private CalibrationVerificationStatus calibrationStatus;
}