package org.trescal.cwms.rest.customer.service;

import java.time.LocalDate;
import java.util.List;

import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.customer.dto.RestCompanyDto;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformation;

public interface RestCompanyService {

	void searchCompanyInformationByCompanyId(PagedResultSet<RestCompanyInformation> prs, Integer companyId, Integer orgId);

	void searchCompanyInformationByLegalId(PagedResultSet<RestCompanyInformation> prs, String legalId, Integer orgId);

	void searchCompanyInformationByModifiedFrom(PagedResultSet<RestCompanyInformation> prs, LocalDate modifiedFrom, Integer orgId);
	
	List<RestCompanyDto> getCustomerCompanies(Subdiv subdiv, Boolean active);
}