package org.trescal.cwms.rest.customer.service;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.rest.customer.dto.RestJobItemDeliveryDto;

public interface RestJobItemService {

	List<RestJobItemDeliveryDto> getJobItemInformation(List<Integer> plantIds, Locale locale);
}