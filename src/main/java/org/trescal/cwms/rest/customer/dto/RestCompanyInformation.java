package org.trescal.cwms.rest.customer.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class used for repository results
 *
 */
@Builder @Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class RestCompanyInformation {
	// Note order is important so constructor order matches JPA projection usage
	private Integer companyId;
	private String companyName;
	private String legalId;
	private String fiscalId;
	private String groupName;
	private CompanyRole companyRole;
	private String companySector;
	private BigDecimal defaultVATRate;
	private Boolean taxable;
	private String legalAddress1;
	private String legalAddress2;
	private String legalAddress3;
	private String legalAddressTown;
	private String legalAddressCounty;
	private String legalAddressPostCode;
	private String legalAddressCountryCode;
	private String currencyCode;
	private String invoiceFrequency;
	private String paymentMode;
	private PaymentTerm paymentTerm;
	private Boolean active;
	private Boolean onStop;
	private Boolean invoiceFactoring;
	private LocalDate createDate;
	private Date lastModifiedBusinessArea;
	private Date lastModifiedCompany;
	private Date lastModifiedCompanyGroup;
	private Date lastModifiedCompanySettings;
	private Date lastModifiedLegalAddress;
	private Date lastModifiedVatRate;
}
