package org.trescal.cwms.rest.customer.component;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformation;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformationDto;

@Component
public class RestCompanyInformationDtoConverter {
	
	private LocalDate getMostRecentModifiedDate(RestCompanyInformation info) {
		SortedSet<LocalDate> sortedDates = new TreeSet<>();
		convertAndAddIfPresent(sortedDates, info.getLastModifiedBusinessArea());
		convertAndAddIfPresent(sortedDates, info.getLastModifiedCompany());
		convertAndAddIfPresent(sortedDates, info.getLastModifiedCompanyGroup());
		convertAndAddIfPresent(sortedDates, info.getLastModifiedCompanySettings());
		convertAndAddIfPresent(sortedDates, info.getLastModifiedLegalAddress());
		convertAndAddIfPresent(sortedDates, info.getLastModifiedVatRate());
		return sortedDates.isEmpty() ? null : sortedDates.last();
	}
	
	private void convertAndAddIfPresent(Collection<LocalDate> collection, Date date) {
		if (date != null)
			collection.add(DateTools.dateToLocalDate(date));
	}

	public RestCompanyInformationDto convert(RestCompanyInformation info) {
		// Last modified dates are old Date; provide "most recent" last modified date externally
		LocalDate mostRecentModifiedDate = getMostRecentModifiedDate(info);
		
		RestCompanyInformationDto dto = RestCompanyInformationDto.builder()
			.active(info.getActive())
			.companyId(info.getCompanyId())
			.companyName(info.getCompanyName())
			.companyRole(info.getCompanyRole())
			.companySector(info.getCompanySector())
			.createDate(info.getCreateDate())
			.currencyCode(info.getCurrencyCode())
			.defaultVATRate(info.getDefaultVATRate())
			.fiscalId(info.getFiscalId())
			.groupName(info.getGroupName())
			.invoiceFactoring(info.getInvoiceFactoring())
			.invoiceFrequency(info.getInvoiceFrequency())
			.legalId(info.getLegalId())
			.legalAddress1(info.getLegalAddress1())
			.legalAddress2(info.getLegalAddress2())
			.legalAddress3(info.getLegalAddress3())
			.legalAddressCountryCode(info.getLegalAddressCountryCode())
			.legalAddressPostCode(info.getLegalAddressPostCode())
			.legalAddressRegion(info.getLegalAddressCounty())
			.legalAddressTown(info.getLegalAddressTown())
			.lastModified(mostRecentModifiedDate)
			.onStop(info.getOnStop())
			.paymentMode(info.getPaymentMode())
			.paymentTerm(info.getPaymentTerm())
			.taxable(info.getTaxable())
			.build();
		
		return dto;
	}

}
