package org.trescal.cwms.rest.customer.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.customer.dto.RestCompanyDto;
import org.trescal.cwms.rest.customer.service.RestCompanyService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@RestJsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class RestCustomerCompaniesController {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private UserService userService;
	@Autowired
	private RestCompanyService restCompanyService;
	@Autowired
	private SubdivService subdivServ;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/customer/companies", method = RequestMethod.GET, produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8,Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<CompanyKeyValue> getCustomerCompanies(
			@RequestParam(name = "scope", required = false, defaultValue = "COMPANY") Scope scope,
			@RequestParam(name = "active", required = false, defaultValue = "true") Boolean active,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		User user = userService.get(userName);
		RestResultDTO<CompanyKeyValue> result = new RestResultDTO<CompanyKeyValue>(true, "");
		result.addResults(companyService.findCustomerCompanies(user.getCon(), scope, active));
		return result;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/customer/subdivs", method = RequestMethod.GET, produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8,Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestCompanyDto> getCustomerCompaniesSubdivs(
			@RequestParam(name = "active", required = false, defaultValue = "true") Boolean active,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		Subdiv subdiv = subdivServ.get(subdivDto.getKey());
		RestResultDTO<RestCompanyDto> result = new RestResultDTO<RestCompanyDto>(true, "");
		result.addResults(restCompanyService.getCustomerCompanies(subdiv, active));
		return result;
	}
}