package org.trescal.cwms.rest.customer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.Date;

@Data
@AllArgsConstructor
/*
 * Important - the order of the fields must match its use
 * e.g. in DeliveryDaoImpl, test query via web service or
 * other method if changing fields
 */
public class RestDeliveryDetailFlatDto {

	private Integer deliveryId;
	private String deliveryNo;
	private Integer jobItemId;
	private Integer jobItemNo;
	private String jobNo;
	
	private Integer plantId;
	private String plantNo;
	private String serialNo;
	private String customerDescription;
	private String customerSpecification;
	private String instrModelName;

	private String manufacturer;
	private String model;
	private String instrumentMfr;
	private String instrumentMfrModel;
	private String family;

	private String subFamily;
	private LocalDate nextCalDate;
	private Integer calFrequency;
	private IntervalUnit calFrequencyUnit;
	private Integer calId;

	private CalibrationClass calClass;
	private Date startTime;
	private Date completionTime;
	private String calStatus;
	private String serviceDescription;
	
	private Integer certifId;
	private String certifNo;
	private Date certDate;
	private Date calDate;
	private Integer certifCalId;
	private CalibrationVerificationStatus certificateCalibrationStatus;
	
	private Date deliveryLastModification;
	private Date jobItemLastModification;
	private Date jobLastModification;
	private Date calibLastModification;
	private Date certifLastModification;
	private Date instrumentLastModification;
}