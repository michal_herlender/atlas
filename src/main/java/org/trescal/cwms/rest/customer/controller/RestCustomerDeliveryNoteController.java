package org.trescal.cwms.rest.customer.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.customer.dto.RestCustomerDeliveryDto;
import org.trescal.cwms.rest.customer.dto.RestDeliveryDetailDto;
import org.trescal.cwms.rest.customer.dto.RestInstrumentRequestDTO;
import org.trescal.cwms.rest.customer.dto.RestJobItemDeliveryDto;
import org.trescal.cwms.rest.customer.service.RestJobItemServiceImpl;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

@Controller
@RestJsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
@Slf4j
public class RestCustomerDeliveryNoteController {

	@Autowired
	private DeliveryService deliveryServ;
	@Autowired
	private SubdivService SubdivServ;
	@Autowired
	private RestJobItemServiceImpl jobItemService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/customer/deliverynotes", method = RequestMethod.GET, produces = {
		Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestCustomerDeliveryDto> getDeliveryNotesBetween(
		@RequestParam(name = "subdivid") int subdivid,
		@RequestParam(name = "deliveryType") DeliveryType deliveryType,
		@RequestParam(name = "dtstart", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) LocalDate dtStart,
		@RequestParam(name = "dtend", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) LocalDate dtEnd) {
		Subdiv subdiv = SubdivServ.get(subdivid);
		RestResultDTO<RestCustomerDeliveryDto> result = new RestResultDTO<>(true, "");
		if (dtEnd == null)
			dtEnd = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		result.addResults(deliveryServ.findDeliveryDto(subdiv, dtStart, dtEnd, deliveryType));
		return result;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/customer/deliverynotes/details", method = RequestMethod.POST, produces = {
		Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestDeliveryDetailDto> getDeliveryNotesDetailsBetween(
		@RequestBody() List<Integer> lstDeliveryIds, Locale locale) {
		log.info("Locale: " + locale);
		RestResultDTO<RestDeliveryDetailDto> result = new RestResultDTO<>(true, "");
		result.addResults(deliveryServ.findDeliveryDetails(lstDeliveryIds));
		return result;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "customer/instrumentcerts", method = RequestMethod.POST, produces = {
		Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestJobItemDeliveryDto> getInstrumentCertificates(
		@RequestBody() RestInstrumentRequestDTO request) {
		try {
			List<RestJobItemDeliveryDto> results = jobItemService.getJobItemInformation(request.getPlantIds(), request.getLocale());
			RestResultDTO<RestJobItemDeliveryDto> resultDto = new RestResultDTO<>(true, "");
			resultDto.addResults(results);
			return resultDto;
		} catch (Exception e) {
			return new RestResultDTO<>(false, e.getMessage());
		}
	}
}