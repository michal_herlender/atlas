package org.trescal.cwms.rest.customer.dto;

import java.util.Date;

public class RestCalibrationDto {

	private int calId;
	
	private String calClass;
	
	private Date startTime;
	
	private Date completionTime;
	
	private String calStatus;
	
	private String serviceDescription;
	
	
	
	public RestCalibrationDto(int calId, String calClass, Date startTime, Date completionTime, String calStatus,
			String serviceDescription) {
		this.calId = calId;
		this.calClass = calClass;
		this.startTime = startTime;
		this.completionTime = completionTime;
		this.calStatus = calStatus;
		this.serviceDescription = serviceDescription;
	}

	public int getCalId() {
		return calId;
	}

	public void setCalId(int calId) {
		this.calId = calId;
	}

	public String getCalClass() {
		return calClass;
	}

	public void setCalClass(String calClass) {
		this.calClass = calClass;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(Date completionTime) {
		this.completionTime = completionTime;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public String getCalStatus() {
		return calStatus;
	}

	public void setCalStatus(String calStatus) {
		this.calStatus = calStatus;
	}
}
