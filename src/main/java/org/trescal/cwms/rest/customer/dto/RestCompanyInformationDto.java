package org.trescal.cwms.rest.customer.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class RestCompanyInformationDto {

	private Integer companyId;
	private String companyName;
	private String legalId;
	private String fiscalId;
	private String groupName;
	private CompanyRole companyRole;
	private String companySector;
	private BigDecimal defaultVATRate;
	private Boolean taxable;
	private String legalAddress1;
	private String legalAddress2;
	private String legalAddress3;
	private String legalAddressTown;
	private String legalAddressRegion;
	private String legalAddressPostCode;
	private String legalAddressCountryCode;
	private String currencyCode;
	private String invoiceFrequency;
	private String paymentMode;
	private PaymentTerm paymentTerm;
	private Boolean active;
	private Boolean onStop;
	private Boolean invoiceFactoring;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate createDate;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate lastModified;

	
}