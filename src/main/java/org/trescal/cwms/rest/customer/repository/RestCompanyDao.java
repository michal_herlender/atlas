package org.trescal.cwms.rest.customer.repository;

import java.time.LocalDate;

import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.customer.dto.RestCompanyInformation;

public interface RestCompanyDao {

	void searchCompanyInformationByCompanyId(PagedResultSet<RestCompanyInformation> prs, Integer companyId, Integer orgId);

	void searchCompanyInformationByLegalId(PagedResultSet<RestCompanyInformation> prs, String legalId, Integer orgId);

	void searchCompanyInformationByModifiedFrom(PagedResultSet<RestCompanyInformation> prs, LocalDate modifiedFrom, Integer orgId);
}