package org.trescal.cwms.rest.customer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.Date;

@Data
@AllArgsConstructor
public class RestInstrumentFlatDTO {

	/* Job item data */
	private Integer jobItemId;
	private Integer jobItemNo;
	private String jobNo;
	/* Instrument data */ 
	private Integer plantId;
	private String plantNo;
	private String serialNo;
	private String customerDescription;
	private String customerSpecification;
	private String instrModelName;
	private String family;
	private String subFamily;
	private String manufacturer;
	private String instrumentMfr;
	private String model;
	private String instrumentMfrModel;
	private LocalDate nextCalDate;
	private Integer calFrequency;
	private IntervalUnit calFrequencyUnit;
	/* Certificate data */
	private Integer certifId;
	private String certifNo;
	private Date certDate;
	private Date calDate;
	private CalibrationVerificationStatus certificateCalibrationStatus;
	/* Last modification dates
	private Date jobItemLastModification;
	private Date jobLastModification;
	private Date calibLastModification;
	private Date certifLastModification;
	private Date instrumentLastModification;*/
}