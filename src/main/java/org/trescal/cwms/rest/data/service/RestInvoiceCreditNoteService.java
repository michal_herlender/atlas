package org.trescal.cwms.rest.data.service;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

public interface RestInvoiceCreditNoteService {

	Invoice getTrescalSASInvoiceByNo(String invoiceNo);

	CreditNote getTrescalSASCreditNoteByNo(String creditnoteNo);
}
