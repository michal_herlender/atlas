package org.trescal.cwms.rest.data.dto;

import lombok.Data;

@Data
public class RestInvoiceCreditNoteDTO {
	
	private String username;
	private String invoiceNo;
	private String creditNoteNo;

}
