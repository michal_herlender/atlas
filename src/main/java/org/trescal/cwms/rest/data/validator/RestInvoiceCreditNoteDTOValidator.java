package org.trescal.cwms.rest.data.validator;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.data.dto.RestInvoiceCreditNoteDTO;
import org.trescal.cwms.rest.data.service.RestInvoiceCreditNoteService;

@Component
public class RestInvoiceCreditNoteDTOValidator extends AbstractBeanValidator {

	@Autowired
	private RestInvoiceCreditNoteService invoiceService;
	@Autowired
	private UserService userService;

	@Override
	public boolean supports(Class<?> clazz) {
		return RestInvoiceCreditNoteDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		 super.validate(target, errors);

		
		RestInvoiceCreditNoteDTO dto = (RestInvoiceCreditNoteDTO) target;
		User user = userService.get(dto.getUsername());
		
		// validate if at least the invoice number or credit note number are not
		// empty
		if (dto.getInvoiceNo() == null && dto.getCreditNoteNo() == null) {
			errors.rejectValue("invoiceNo", RestErrors.CODE_INVOICE_NUMBER_CREDIT_NOTE_NULL,
					new Object[] { dto.getInvoiceNo(), dto.getCreditNoteNo() },
					RestErrors.MESSAGE_INVOICE_NUMBER_CREDIT_NOTE_NULL);
		}
		// lookup invoice
		if (dto.getInvoiceNo() != null) {
			Invoice invoice = invoiceService.getTrescalSASInvoiceByNo(dto.getInvoiceNo());
			if (invoice == null) {
				errors.rejectValue("invoiceNo", RestErrors.CODE_INVOICE_NOT_FOUND, new Object[] { dto.getInvoiceNo() },
						RestErrors.MESSAGE_INVOICE_NOT_FOUND);
			} else if (!user.getUserRoles().stream()
					.filter(ur -> ur.getOrganisation().getComp().getId() == 6690)
					.flatMap(ur -> ur.getRole().getGroups().stream().flatMap(pg -> pg.getPermissions().stream()))
					.collect(Collectors.toList()).contains(Permission.WEB_SERVICE_DOWNLOAD_INVOICE_OR_CREDIT_NOTE_PDF)) {
				errors.rejectValue("invoiceNo", RestErrors.CODE_INVOICE_UNAUTHORIZED,
						new Object[] { dto.getInvoiceNo() }, RestErrors.MESSAGE_INVOICE_UNAUTHORIZED);
			}
		}
		// lookup credit note
		if (dto.getCreditNoteNo() != null) {
			CreditNote creditNote = invoiceService.getTrescalSASCreditNoteByNo(dto.getCreditNoteNo());
			if (creditNote == null) {
				errors.rejectValue("creditNoteNo", RestErrors.CODE_CREDIT_NOTE_NOT_FOUND,
						new Object[] { dto.getCreditNoteNo() }, RestErrors.MESSAGE_CREDIT_NOTE_NOT_FOUND);
			} else if (!user.getUserRoles().stream()
					.filter(ur -> ur.getOrganisation().getComp().getId() == 6690)
					.flatMap(ur -> ur.getRole().getGroups().stream().flatMap(pg -> pg.getPermissions().stream()))
					.collect(Collectors.toList()).contains(Permission.WEB_SERVICE_DOWNLOAD_INVOICE_OR_CREDIT_NOTE_PDF)) {
				errors.rejectValue("creditNoteNo", RestErrors.CODE_CREDIT_NOTE_UNAUTHORIZED,
						new Object[] { dto.getCreditNoteNo() }, RestErrors.MESSAGE_CREDIT_NOTE_UNAUTHORIZED);
			}
		}
	}

}
