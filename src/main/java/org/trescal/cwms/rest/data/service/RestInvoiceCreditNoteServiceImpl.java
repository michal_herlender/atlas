package org.trescal.cwms.rest.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;

@Service
public class RestInvoiceCreditNoteServiceImpl implements RestInvoiceCreditNoteService {

	public static final int TRESCAL_SAS_ID = 6690;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private CreditNoteService creditNoteService;
	
	public Invoice getTrescalSASInvoiceByNo(String invoiceNo){
		return invoiceService.findByInvoiceNo(invoiceNo, TRESCAL_SAS_ID);
	}
	
	public CreditNote getTrescalSASCreditNoteByNo(String creditnoteNo) {
		return creditNoteService.findByNumber(creditnoteNo, TRESCAL_SAS_ID);
	}
}
