package org.trescal.cwms.rest.data.controller;

import java.io.File;
import java.io.FileInputStream;
import java.security.Principal;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestFieldErrorDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.data.dto.RestInvoiceCreditNoteDTO;
import org.trescal.cwms.rest.data.service.RestInvoiceCreditNoteService;
import org.trescal.cwms.rest.data.validator.RestInvoiceCreditNoteDTOValidator;

@Controller
@RestJsonController
public class RestInvoiceCreditNoteController {

	public static final String REQUEST_MAPPING = "/api/data/invoice";

	@Autowired
	private RestInvoiceCreditNoteDTOValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestInvoiceCreditNoteService invoiceCreditNoteService;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private SystemComponentService scServ;

	@RequestMapping(value = { REQUEST_MAPPING } , method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }
	)
	public @ResponseBody RestResultDTO<Map<String, Object>> getInvoiceCreditNote(
			@RequestBody RestInvoiceCreditNoteDTO dto, BindingResult bindingResult, 
			Principal principal, Locale locale)
			throws Exception {
        
		if (principal != null) {
			dto.setUsername(principal.getName());
		}
		// validate
		validator.validate(dto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}

		RestResultDTO<Map<String, Object>> result = new RestResultDTO<Map<String, Object>>(true, "");
		Map<String, Object> resultMap = new HashMap<>();
		// get invoice file
		if (dto.getInvoiceNo() != null) {
			Invoice invoice = invoiceCreditNoteService.getTrescalSASInvoiceByNo(dto.getInvoiceNo());
			byte[] fileinvoicebyte = null;
			File directory = this.compDirServ.getDirectory(this.scServ.findComponent(Component.INVOICE),
					invoice.getInvno());
			if (directory.listFiles().length > 0) {
				String fileName = directory.listFiles()[directory.listFiles().length - 1].getName();
				File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));
				if (file.exists())
					try (FileInputStream fis = new FileInputStream(file)) {
						// 10MB max
						byte[] fileData = new byte[(int) file.length()];
						IOUtils.read(fis, fileData);
						fis.close();
						// to base64
						fileinvoicebyte = fileData;
						resultMap.put("invoiceId", invoice.getId());
						resultMap.put("fileInvoicebyte", fileinvoicebyte);
					} catch (Exception e) {
						e.printStackTrace();
					}
				else {
					RestFieldErrorDTO error = new RestFieldErrorDTO();
					error.setFieldName("File_Unfound");
					error.setMessage("No file found for this invoice");
					result.addError(error);
					return result;
				}
			} else {

				RestFieldErrorDTO error = new RestFieldErrorDTO();
				error.setFieldName("File_Unfound");
				error.setMessage("No file found for this invoice");
				result.addError(error);
				return result;
			}
		}

		// get credit note file
		if (dto.getCreditNoteNo() != null) {
			CreditNote creditNote = invoiceCreditNoteService.getTrescalSASCreditNoteByNo(dto.getCreditNoteNo());
			byte[] filecreditnotebyte = null;
			File directory = this.compDirServ.getDirectory(this.scServ.findComponent(Component.CREDIT_NOTE),
					creditNote.getCreditNoteNo());
			if (directory.listFiles().length > 0) {
				String fileName = directory.listFiles()[directory.listFiles().length - 1].getName();
				File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));
				if (file.exists()) {
					try (FileInputStream fis = new FileInputStream(file)) {
						// 10MB max
						byte[] fileData = new byte[(int) file.length()];
						IOUtils.read(fis, fileData);
						fis.close();
						// to base64
						filecreditnotebyte = fileData;
						resultMap.put("creditnoteId", creditNote.getId());
						resultMap.put("fileCreditNotebyte", filecreditnotebyte);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					RestFieldErrorDTO error = new RestFieldErrorDTO();
					error.setFieldName("File_Unfound");
					error.setMessage("No file found for this credit note");
					result.addError(error);
					return result;
				}
			} else {
				RestFieldErrorDTO error = new RestFieldErrorDTO();
				error.setFieldName("File_Unfound");
				error.setMessage("No file found for this credit note");
				result.addError(error);
				return result;
			}
		}

		result.addResult(resultMap);
		return result;
	}

}
