package org.trescal.cwms.rest.accounting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.accounting.dto.CreditNoteAccountingDTO;
import org.trescal.cwms.rest.accounting.service.RestCreditNoteService;

@Controller
@RestJsonController
public class RestCreditNoteController {

	@Autowired
	private RestCreditNoteService restCreditNoteService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = {"/accounting/creditnotes", "/api/accounting/creditnotes"}, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public List<CreditNoteAccountingDTO> getCreditNotes(@RequestParam(name = "busId", required = true) Integer busId) {
		return restCreditNoteService.getAllForAccounting(busId);
	}
}