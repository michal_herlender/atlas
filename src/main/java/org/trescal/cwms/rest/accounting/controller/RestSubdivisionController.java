package org.trescal.cwms.rest.accounting.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.accounting.dto.SubdivisionAccountingDTO;
import org.trescal.cwms.rest.accounting.service.RestSubdivisionService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@RestJsonController
public class RestSubdivisionController {

	@Autowired
	private RestSubdivisionService restSubdivisionService;
	@Autowired
	private MessageSource messageSource;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/accounting/subdivisions",
			"/api/accounting/subdivisions" }, method = RequestMethod.GET, produces = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })

	public RestResultDTO<SubdivisionAccountingDTO> getAllSubdivision(
			@RequestParam(name = "busId", required = true) Integer busId,
			@RequestParam(name = "subdivId", required = false) Integer subdivId,
			@RequestParam(name = "subdivFiscalId", required = false) String subdivFiscalId,
			@RequestParam(value = "role", required = true) List<CompanyRole> roles, Locale locale) {

		// at least one of subdivId or subdivFiscalId must be specified
		if (subdivFiscalId == null && subdivId == null) {
			String message = messageSource.getMessage(RestErrors.CODE_SUBDIV_ID_NULL, null,
					RestErrors.MESSAGE_SUBDIV_ID_NULL, locale);
			return new RestResultDTO<SubdivisionAccountingDTO>(false, message);
		}

		RestResultDTO<SubdivisionAccountingDTO> result = new RestResultDTO<>(true, "");
		List<SubdivisionAccountingDTO> mylist = restSubdivisionService.getAllSubdivisionsForAccounting(roles, busId,
				subdivId, subdivFiscalId);
		result.addResults(mylist);
		return result;
	}

}
