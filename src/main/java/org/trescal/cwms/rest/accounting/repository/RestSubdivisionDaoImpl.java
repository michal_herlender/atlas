package org.trescal.cwms.rest.accounting.repository;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.AddressDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.rest.accounting.dto.SubdivisionAccountingDTO;

@Repository
public class RestSubdivisionDaoImpl extends BaseDaoImpl<Subdiv, Integer> implements RestSubdivisionDao {

	@Override
	protected Class<Subdiv> getEntity() {
		return Subdiv.class;
	}

	@Override
	public List<SubdivisionAccountingDTO> getAllSubdivisionsForAccounting(List<CompanyRole> roles, Integer busId,
			Integer subdivId, String subdivFiscalId) {
		return getResultList(cb -> {
			CriteriaQuery<SubdivisionAccountingDTO> cq = cb.createQuery(SubdivisionAccountingDTO.class);
			Root<Subdiv> subdiv = cq.from(Subdiv.class);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			Join<CompanySettingsForAllocatedCompany, Company> businessCompany = settings
					.join(CompanySettingsForAllocatedCompany_.organisation.getName());
			Predicate clauses = cb.conjunction();
			if (subdivId != null) {
				clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), subdivId));
			} else if (subdivFiscalId != null) {
				clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.siretNumber), subdivFiscalId));
			}
			clauses.getExpressions().add(company.get(Company_.companyRole).in(roles));
			clauses.getExpressions().add(cb.equal(businessCompany.get(Company_.coid), busId));
			cq.where(clauses);
			// get results
			cq.select(cb.construct(SubdivisionAccountingDTO.class, company.get(Company_.coid),
					company.get(Company_.fiscalIdentifier), company.get(Company_.coname), subdiv.get(Subdiv_.subdivid),
					subdiv.get(Subdiv_.siretNumber), subdiv.get(Subdiv_.subname),
					settings.get(CompanySettingsForAllocatedCompany_.paymentterm),
					settings.get(CompanySettingsForAllocatedCompany_.paymentMode), businessCompany.get(Company_.coid)));
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public List<AddressDTO> getAllAddressesForAccounting(Integer subdivId) {
		return getResultList(cb -> {
			CriteriaQuery<AddressDTO> cq = cb.createQuery(AddressDTO.class);
			Root<Address> address = cq.from(Address.class);
			Join<Address, Subdiv> subdiv = address.join(Address_.sub);
			Join<Address, Country> country = address.join(Address_.country);
			Join<Subdiv, Address> defaultAddress = subdiv.join(Subdiv_.defaultAddress, JoinType.LEFT);

			Subquery<Long> deliveryTypeSq = cq.subquery(Long.class);
			Root<Address> deliveryTypeSqRoot = deliveryTypeSq.from(Address.class);
			deliveryTypeSq.select(cb.count(deliveryTypeSqRoot));
			deliveryTypeSq.where(
					cb.isMember(AddressType.DELIVERY, deliveryTypeSqRoot.get(Address_.addressType.getName())), cb.equal(
							deliveryTypeSqRoot.get(Address_.addrid.getName()), address.get(Address_.addrid.getName())));

			Subquery<Long> invoiceTypeSq = cq.subquery(Long.class);
			Root<Address> invoiceTypeSqRoot = invoiceTypeSq.from(Address.class);
			invoiceTypeSq.select(cb.count(invoiceTypeSqRoot));
			invoiceTypeSq.where(cb.isMember(AddressType.INVOICE, invoiceTypeSqRoot.get(Address_.addressType.getName())),
					cb.equal(invoiceTypeSqRoot.get(Address_.addrid.getName()), address.get(Address_.addrid.getName())));

			Subquery<Long> legalTypeSq = cq.subquery(Long.class);
			Root<Address> legalTypeSqRoot = legalTypeSq.from(Address.class);
			legalTypeSq.select(cb.count(legalTypeSqRoot));
			legalTypeSq.where(
					cb.isMember(AddressType.LEGAL_REGISTRATION, legalTypeSqRoot.get(Address_.addressType.getName())),
					cb.equal(legalTypeSqRoot.get(Address_.addrid.getName()), address.get(Address_.addrid.getName())));

			cq.where(cb.equal(subdiv.get(Subdiv_.subdivid), subdivId));
			// get results
			cq.select(cb.construct(AddressDTO.class, address.get(Address_.addr1), address.get(Address_.addr2),
					address.get(Address_.addr3), address.get(Address_.town), address.get(Address_.telephone),
					country.get(Country_.countryCode), address.get(Address_.postcode), address.get(Address_.county),
					cb.selectCase()
							.when(cb.equal(defaultAddress, address), cb.literal(true))
							.otherwise(cb.literal(false)),
					deliveryTypeSq.getSelection(), invoiceTypeSq.getSelection(), legalTypeSq.getSelection()));
			cq.distinct(true);
			return cq;
		});
	}
}
