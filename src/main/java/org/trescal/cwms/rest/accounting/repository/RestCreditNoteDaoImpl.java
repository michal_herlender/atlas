package org.trescal.cwms.rest.accounting.repository;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRate_;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote_;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.rest.accounting.dto.CreditNoteAccountingDTO;
import org.trescal.cwms.rest.accounting.dto.CreditNoteItemAccountingDTO;

@Repository
public class RestCreditNoteDaoImpl extends BaseDaoImpl<CreditNote, Integer> implements RestCreditNoteDao {

	@Override
	protected Class<CreditNote> getEntity() {
		return CreditNote.class;
	}

	@Override
	public List<CreditNoteAccountingDTO> getAllForAccounting(Integer busId) {
		return getResultList(cb -> {
			CriteriaQuery<CreditNoteAccountingDTO> cq = cb.createQuery(CreditNoteAccountingDTO.class);
			Root<CreditNote> creditNote = cq.from(CreditNote.class);
			Join<CreditNote, Invoice> invoice = creditNote.join(CreditNote_.invoice);
			Join<Invoice, PaymentMode> paymentMode = invoice.join(Invoice_.paymentMode, JoinType.LEFT);
			Join<Invoice, Company> invoiceCompany = invoice.join(Invoice_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> companySettings = invoiceCompany.join(Company_.settingsForAllocatedCompanies);
			companySettings.on(cb.equal(companySettings.get(CompanySettingsForAllocatedCompany_.organisation), busId));
			Join<Invoice, Address> invoiceAddress = invoice.join(Invoice_.address);
			Join<Address, Subdiv> invoiceSubdiv = invoiceAddress.join(Address_.sub);
			Join<Address, Country> invoiceCountry = invoiceAddress.join(Address_.country);
			Join<Subdiv, Contact> defaultContact = invoiceSubdiv.join(Subdiv_.defaultContact, JoinType.LEFT);
			Join<CreditNote, SupportedCurrency> currency = creditNote.join(CreditNote_.currency);
			Join<CreditNote, VatRate> vatRateEntity = creditNote.join(CreditNote_.vatRateEntity, JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(creditNote.get(CreditNote_.organisation), busId));
			clauses.getExpressions().add(cb.equal(creditNote.get(CreditNote_.accountsStatus), AccountStatus.P));
			cq.where(clauses);
			cq.select(cb.construct(CreditNoteAccountingDTO.class, creditNote.get(CreditNote_.id),
					creditNote.get(CreditNote_.creditNoteNo), invoice.get(Invoice_.id), invoice.get(Invoice_.invno),
					invoice.get(Invoice_.invoiceDate), invoiceCompany.get(Company_.coid),
					invoiceCompany.get(Company_.coname), invoiceCompany.get(Company_.fiscalIdentifier),
					invoiceSubdiv.get(Subdiv_.subdivid), invoiceSubdiv.get(Subdiv_.siretNumber),
					cb.concat(
							cb.concat(cb.concat(invoiceAddress.get(Address_.addr1), " "),
									cb.concat(invoiceAddress.get(Address_.addr2), " ")),
							invoiceAddress.get(Address_.addr3)),
					invoiceAddress.get(Address_.town), invoiceAddress.get(Address_.postcode),
					invoiceAddress.get(Address_.county), invoiceCountry.get(Country_.countryCode),
					cb.concat(cb.concat(defaultContact.get(Contact_.firstName), "" + (char) 32),
							defaultContact.get(Contact_.lastName)),
					defaultContact.get(Contact_.email), creditNote.get(CreditNote_.accountsStatus),
					creditNote.get(CreditNote_.regdate), creditNote.get(CreditNote_.issuedate),
					invoice.get(Invoice_.paymentTerm), paymentMode.get(PaymentMode_.code),
					currency.get(SupportedCurrency_.currencyCode), creditNote.get(CreditNote_.rate),
					creditNote.get(CreditNote_.finalCost), creditNote.get(CreditNote_.totalCost),
					creditNote.get(CreditNote_.vatRate), vatRateEntity.get(VatRate_.vatCode),
					vatRateEntity.get(VatRate_.type), companySettings.get(CompanySettingsForAllocatedCompany_.invoiceFactoring)));
			return cq;
		});
	}

	@Override
	public List<CreditNoteItemAccountingDTO> getAllItemsForAccounting(Integer creditNoteId) {
		return getResultList(cb -> {
			CriteriaQuery<CreditNoteItemAccountingDTO> cq = cb.createQuery(CreditNoteItemAccountingDTO.class);
			Root<CreditNoteItem> item = cq.from(CreditNoteItem.class);
			Join<CreditNoteItem, CreditNote> creditNote = item.join(CreditNoteItem_.creditNote);
			Join<CreditNoteItem, NominalCode> nominalCode = item.join(CreditNoteItem_.nominal, JoinType.LEFT);
			Join<CreditNoteItem, Subdiv> businessSubdiv = item.join(CreditNoteItem_.businessSubdiv);
			Join<CreditNoteItem, Address> shipToAddress = item.join(CreditNoteItem_.shipToAddress);
			Join<Address, Country> shipToCountry = shipToAddress.join(Address_.country);
			cq.where(cb.equal(creditNote.get(CreditNote_.id), creditNoteId));
			// vatRate set afterward in service function, not included here to prevent null literal
			cq.select(cb.construct(CreditNoteItemAccountingDTO.class, item.get(CreditNoteItem_.itemno),
					nominalCode.get(NominalCode_.code), nominalCode.get(NominalCode_.title),
					item.get(CreditNoteItem_.quantity), item.get(CreditNoteItem_.totalCost), 
					item.get(CreditNoteItem_.finalCost), businessSubdiv.get(Subdiv_.analyticalCenter),
					shipToAddress.get(Address_.postcode), shipToCountry.get(Country_.countryCode),
					shipToAddress.get(Address_.county), item.get(CreditNoteItem_.taxable),
					item.get(CreditNoteItem_.taxAmount)));
			return cq;
		});
	}
}