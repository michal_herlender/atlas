package org.trescal.cwms.rest.accounting.repository;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AbstractDaoImpl;
import org.trescal.cwms.rest.accounting.dto.RestCustomerDto;

@Repository
public class RestCustomerDaoImpl extends AbstractDaoImpl implements RestCustomerDao {

	@Override
	public List<RestCustomerDto> getAllCustomers(String fiscalId, Integer busId) {

		Query myquery = getEntityManager()
				.createNativeQuery("SELECT id, fiscalId, name, address, city, telephoneNo, countryCode, postCode, "
						+ "county, paymentTermscode, paymentModeCode, busId " + " FROM new_customeraccounts "
						+ " WHERE fiscalId='" + fiscalId + "'" + " AND busId=" + busId);

		@SuppressWarnings("unchecked")
		List<Object[]> myList = myquery.getResultList();
		return myList.stream().map(result -> {
			RestCustomerDto mydto = new RestCustomerDto();
			mydto.setId((Integer) result[0]);
			mydto.setFiscalId((String) result[1]);
			mydto.setName((String) result[2]);
			mydto.setAddress((String) result[3]);
			mydto.setCity((String) result[4]);
			mydto.setTelephoneNo((String) result[5]);
			mydto.setCountryCode((String) result[6]);
			mydto.setPostCode((String) result[7]);
			mydto.setCounty((String) result[8]);
			mydto.setPaymentTermsCode((String) result[9]);
			mydto.setPaymentModecode((String) result[10]);
			mydto.setBusId((Integer) result[11]);
			return mydto;

		}).collect(Collectors.toList());
	}
}