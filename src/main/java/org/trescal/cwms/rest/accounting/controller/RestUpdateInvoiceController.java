package org.trescal.cwms.rest.accounting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.system.Constants;

@Controller @RestJsonController
public class RestUpdateInvoiceController {

	@Autowired
	private InvoiceService invoiceService;
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={"/accounting/updateinvoice", "/api/accounting/updateinvoice"}, method=RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public boolean updateInvoice(@RequestParam(name="invNo", required=true) String invoiceNo,
			@RequestParam(name="busId", required=true) Integer orgId) {
		Invoice invoice = invoiceService.findByInvoiceNo(invoiceNo, orgId);
		invoice.setAccountsStatus(AccountStatus.S);
		invoiceService.merge(invoice);
		return true;
	}
}