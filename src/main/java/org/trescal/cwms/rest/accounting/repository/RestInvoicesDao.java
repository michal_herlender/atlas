package org.trescal.cwms.rest.accounting.repository;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.InvoiceAccountingDTO;
import org.trescal.cwms.rest.accounting.dto.InvoiceItemAccountingDTO;

public interface RestInvoicesDao {

	List<InvoiceItemAccountingDTO> getAllItemsForAccounting(Integer invoiceId);

	List<InvoiceAccountingDTO> getAllInvoicesForAccounting(Integer busId);
}