package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.trescal.cwms.core.company.dto.AddressDTO;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.rest.accounting.dto.SubdivisionAccountingDTO;

public interface RestSubdivisionService {

	List<AddressDTO> getAllAddressesForAccounting(Integer subdivId);

	/**
	 * Return details of single subdivision
	 * 
	 * @param List
	 *            roles the role can be any of CLIENT, SUPPLIER, UTILITY,
	 *            BUSINESS, PROSPECT
	 * @param int
	 *            busId ID of business company to retrieve business-company
	 *            specific settings against
	 * @param int
	 *            subdivId Atlas ID of subdivision to retrieve details for
	 * @param String
	 *            subdivFiscalId fiscal ID of subdivision to retrieve details
	 *            for
	 * 
	 */
	List<SubdivisionAccountingDTO> getAllSubdivisionsForAccounting(List<CompanyRole> roles, Integer busId,
			Integer subdivId, String subdivFiscalId);
}
