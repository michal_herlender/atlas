package org.trescal.cwms.rest.accounting.dto;

import java.time.LocalDate;
import java.util.List;

import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestPurchaseOrderDto {
	
	private String poNo;
	private Integer payTo_id;
	private String payTo_fiscalId;
	private Integer payTo_subdivId;
	private String payTo_subdivFiscalId;
	private LocalDate invoiceDate;
	private String paymentTermsCode;
	private String payTo_countryCode;
	private String paymentModeCode;
	private Integer busId;
	private String invoiceNo;
	private List<RestPurchaseOrderItemDto> items;
	
	
	public RestPurchaseOrderDto(String poNo, Integer payTo_id, String payTo_fiscalId, Integer payTo_subdivId, String payTo_subdivFiscalId, 
			LocalDate invoiceDate, PaymentTerm paymentTermsCode, String payTo_countryCode, String paymentModeCode, Integer busId,
			String invoiceNo) {
		super();
		this.poNo = poNo;
		this.payTo_id = payTo_id;
		this.payTo_fiscalId = payTo_fiscalId;
		this.payTo_subdivId = payTo_subdivId;
		this.payTo_subdivFiscalId = payTo_subdivFiscalId;
		this.invoiceDate = invoiceDate;
		this.paymentTermsCode = paymentTermsCode != null ? paymentTermsCode.getCode() : "";
		this.payTo_countryCode = payTo_countryCode;
		this.paymentModeCode = paymentModeCode;
		this.busId = busId;
		this.invoiceNo = invoiceNo;
	}


	public RestPurchaseOrderDto() {
		super();
	}
	
}
