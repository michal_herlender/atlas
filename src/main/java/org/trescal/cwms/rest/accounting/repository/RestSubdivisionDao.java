package org.trescal.cwms.rest.accounting.repository;

import java.util.List;

import org.trescal.cwms.core.company.dto.AddressDTO;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.rest.accounting.dto.SubdivisionAccountingDTO;

public interface RestSubdivisionDao {

	List<SubdivisionAccountingDTO> getAllSubdivisionsForAccounting(List<CompanyRole> roles, Integer busId,
			Integer subdivId, String subdivFiscalId);
	
	List<AddressDTO> getAllAddressesForAccounting(Integer subdivId);
}
