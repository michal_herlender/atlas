package org.trescal.cwms.rest.accounting.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import lombok.Data;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.vatrate.VatRateType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Data
public class CreditNoteAccountingDTO {

    private Integer creditNoteId;
    private String creditNoteNo;
    private Integer invoiceId;
    private String invoiceNo;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate invoiceDate;
    private Integer billTo_id;
    private String billTo_name;
    private String billTo_fiscalId;
    private Integer billTo_subdivId;
    private String billTo_subdivFiscalId;
    private String billTo_addr;
    private String billTo_city;
    private String billTo_postCode;
    private String billTo_county;
    private String billTo_countryCode;
    private String billTo_contact;
    private String billTo_email;
    private AccountStatus accountStatus;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate regDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate issueDate;
    private PaymentTerm paymentTermsCode;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate paymentDate;
    private String paymentModeCode;
    private String currencyCode;
    private BigDecimal currencyRate;
    private BigDecimal totalAmountInclTax;
    private BigDecimal totalAmountExclTax;
    private BigDecimal vatRate;
    private String vatRateCode;
    private VatRateType vatRateType;
    private Boolean factoring;
    private List<CreditNoteItemAccountingDTO> items;

    public CreditNoteAccountingDTO(Integer creditNoteId, String creditNoteNo, Integer invoiceId, String invoiceNo,
                                   LocalDate invoiceDate, Integer billIo_id, String billTo_name, String billTo_fiscalId, Integer billTo_subdivId,
                                   String billTo_subdivFiscalId, String billTo_addr, String billTo_city, String billTo_postCode, String billTo_county,
                                   String billTo_countryCode, String billTo_contact, String billTo_email, AccountStatus accountStatus,
                                   LocalDate regDate, LocalDate issueDate, PaymentTerm paymentTermsCode, String paymentModeCode, String currencyCode,
                                   BigDecimal currencyRate, BigDecimal totalAmountInclTax, BigDecimal totalAmountExclTax, BigDecimal vatRate,
                                   String vatRateCode, VatRateType vatRateType, Boolean factoring) {
        super();
        this.creditNoteId = creditNoteId;
        this.creditNoteNo = creditNoteNo;
        this.invoiceId = invoiceId;
        this.invoiceNo = invoiceNo;
        this.invoiceDate = invoiceDate;
        this.billTo_id = billIo_id;
        this.billTo_name = billTo_name;
        this.billTo_fiscalId = billTo_fiscalId;
        this.billTo_subdivFiscalId = billTo_subdivFiscalId;
        this.billTo_subdivId = billTo_subdivId;
        this.billTo_addr = billTo_addr;
        this.billTo_city = billTo_city;
        this.billTo_postCode = billTo_postCode;
        this.billTo_county = billTo_county;
        this.billTo_countryCode = billTo_countryCode;
        this.billTo_contact = billTo_contact;
        this.billTo_email = billTo_email;
        this.setAccountStatus(accountStatus);
        this.regDate = regDate;
        this.issueDate = issueDate;
        this.paymentTermsCode = paymentTermsCode;
        this.paymentDate = paymentTermsCode.computePaymentDate(issueDate);
        this.paymentModeCode = paymentModeCode;
        this.currencyCode = currencyCode;
        this.currencyRate = currencyRate;
        this.totalAmountInclTax = totalAmountInclTax;
        this.totalAmountExclTax = totalAmountExclTax;
        this.vatRate = vatRate;
        this.vatRateCode = vatRateCode;
        this.setVatRateType(vatRateType);
        this.factoring = factoring;
    }
}