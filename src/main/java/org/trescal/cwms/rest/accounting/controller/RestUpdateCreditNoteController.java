package org.trescal.cwms.rest.accounting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.system.Constants;

@Controller
@RestJsonController
public class RestUpdateCreditNoteController {
	
	@Autowired
	private CreditNoteService creditNoteService;
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={"/accounting/updatecreditnote", "/api/accounting/updatecreditnote"}, method=RequestMethod.GET, produces = {
		Constants.MEDIATYPE_APPLICATION_JSON_UTF8,Constants.MEDIATYPE_TEXT_JSON_UTF8 })	
	public boolean updateCreditNote(@RequestParam(name="cnNo", required=true) String cnNo,
		@RequestParam(name="busId", required=true) Integer orgId) {
		return creditNoteService.changeAccountStatus(cnNo, orgId, AccountStatus.S);
	}
	
}

