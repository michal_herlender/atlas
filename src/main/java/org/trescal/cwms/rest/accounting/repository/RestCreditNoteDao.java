package org.trescal.cwms.rest.accounting.repository;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.CreditNoteAccountingDTO;
import org.trescal.cwms.rest.accounting.dto.CreditNoteItemAccountingDTO;

public interface RestCreditNoteDao {

	List<CreditNoteAccountingDTO> getAllForAccounting(Integer busId);

	List<CreditNoteItemAccountingDTO> getAllItemsForAccounting(Integer creditNoteId);
}