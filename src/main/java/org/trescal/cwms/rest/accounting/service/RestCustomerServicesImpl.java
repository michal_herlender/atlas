package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.rest.accounting.dto.RestCustomerDto;
import org.trescal.cwms.rest.accounting.repository.RestCustomerDao;

@Service
public class RestCustomerServicesImpl implements RestCustomerService {

	@Autowired
	private RestCustomerDao restCustomerDao;

	@Override
	public List<RestCustomerDto> getAllCustomers(String fiscalId, Integer busId) {
		List<RestCustomerDto> customers = restCustomerDao.getAllCustomers(fiscalId, busId);
		return customers;

	}
}