package org.trescal.cwms.rest.accounting.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class InvoiceItemAccountingDTO {

    private Integer itemNo;
    private String description;
    private String analyticalCenter;
    private String nominalCode;
    private String nominalTitle;
    private Integer quantity;
    private BigDecimal unitPrice;
    private InvoiceItemCostAccountingDTO calibrationCost;
    private InvoiceItemCostAccountingDTO adjustmentCost;
    private InvoiceItemCostAccountingDTO repairCost;
    private InvoiceItemCostAccountingDTO purchaseCost;
    private InvoiceItemCostAccountingDTO serviceCost;
    private BigDecimal discountRate;
    private BigDecimal discountValue;
    private BigDecimal amountExclTax;
    private BigDecimal amountInclTax;
    private Integer shipTo_id;
    private String shipTo_name;
    private Integer shipTo_subdivId;
    private String shipTo_address;
    private String shipTo_city;
    private String shipTo_postCode;
    private String shipTo_county;
    private String shipTo_countryCode;
    private String shipTo_contact;
    private String shipTo_email;
    private String purchaseOrderNumber;
    private BigDecimal vatRate;
    private String vatRateCode;
    private Boolean taxable;
    private BigDecimal taxAmount;
    private Integer jobItemId;
    private Integer jobServiceId;

    public InvoiceItemAccountingDTO(Integer itemNo, String description, String analyticalCenter, String nominalCode,
                                    String nominalTitle, Integer quantity, BigDecimal unitPrice, String calCostNominalCode,
                                    String calCostNominalTitle, BigDecimal calCostDiscountRate, BigDecimal calCostDiscountValue,
                                    BigDecimal calCostFinalCost, BigDecimal calCostTotalCost, String adjustmentCostNominalCode,
                                    String adjustmentCostNominalTitle, BigDecimal adjustmentCostDiscountRate,
                                    BigDecimal adjustmentCostDiscountValue, BigDecimal adjustmentCostFinalCost,
                                    BigDecimal adjustmentCostTotalCost, String repairCostNominalCode, String repairCostNominalTitle,
                                    BigDecimal repairCostDiscountRate, BigDecimal repairCostDiscountValue, BigDecimal repairCostFinalCost,
                                    BigDecimal repairCostTotalCost, String purchaseCostNominalCode, String purchaseCostNominalTitle,
                                    BigDecimal purchaseCostDiscountRate, BigDecimal purchaseCostDiscountValue, BigDecimal purchaseCostFinalCost,
                                    BigDecimal purchaseCostTotalCost, String serviceCostNominalCode, String serviceCostNominalTitle,
                                    BigDecimal serviceCostDiscountRate, BigDecimal serviceCostDiscountValue, BigDecimal serviceCostFinalCost,
                                    BigDecimal serviceCostTotalCost, BigDecimal discountRate, BigDecimal discountValue,
                                    BigDecimal amountExclTax, Integer shipTo_id, String shipTo_name, Integer shipTo_subdivId,
                                    String shipTo_address, String shipTo_city, String shipTo_postCode, String shipTo_county,
                                    String shipTo_countryCode, Integer bpoId, String bpoNumber,
                                    Integer jobPoId, String jobPoNumber, Integer invoicePoId, String invoicePoNumber, Boolean taxable,
                                    BigDecimal taxAmount, Integer jobItemId, Integer jobServiceId) {
        super();
        this.itemNo = itemNo;
        this.description = description;
        this.analyticalCenter = analyticalCenter;
        this.nominalCode = nominalCode;
        this.nominalTitle = nominalTitle;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.setCalibrationCost(new InvoiceItemCostAccountingDTO(calCostNominalCode, calCostNominalTitle,
                calCostDiscountRate, calCostDiscountValue, calCostFinalCost, calCostTotalCost));
        this.setAdjustmentCost(new InvoiceItemCostAccountingDTO(adjustmentCostNominalCode, adjustmentCostNominalTitle,
                adjustmentCostDiscountRate, adjustmentCostDiscountValue, adjustmentCostFinalCost,
                adjustmentCostTotalCost));
        this.setRepairCost(new InvoiceItemCostAccountingDTO(repairCostNominalCode, repairCostNominalTitle,
                repairCostDiscountRate, repairCostDiscountValue, repairCostFinalCost, repairCostTotalCost));
        this.setPurchaseCost(new InvoiceItemCostAccountingDTO(purchaseCostNominalCode, purchaseCostNominalTitle,
                purchaseCostDiscountRate, purchaseCostDiscountValue, purchaseCostFinalCost, purchaseCostTotalCost));
        this.setServiceCost(new InvoiceItemCostAccountingDTO(serviceCostNominalCode, serviceCostNominalTitle,
                serviceCostDiscountRate, serviceCostDiscountValue, serviceCostFinalCost, serviceCostTotalCost));
        this.discountRate = discountRate;
        this.discountValue = discountValue;
        this.amountExclTax = amountExclTax;
        this.shipTo_id = shipTo_id;
        this.shipTo_name = shipTo_name;
        this.shipTo_subdivId = shipTo_subdivId;
        this.shipTo_address = shipTo_address;
        this.shipTo_city = shipTo_city;
        this.shipTo_postCode = shipTo_postCode;
        this.shipTo_county = shipTo_county;
        this.shipTo_countryCode = shipTo_countryCode;
        this.purchaseOrderNumber = bpoNumber == null ? jobPoNumber == null ? invoicePoNumber : jobPoNumber : bpoNumber;
        this.taxable = taxable;
        this.taxAmount = taxAmount;
        this.jobItemId = jobItemId;
        this.jobServiceId = jobServiceId;
    }
}