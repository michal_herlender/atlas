package org.trescal.cwms.rest.accounting.repository;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRate_;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.pricing.invoice.entity.costs.adjcost.InvoiceAdjustmentCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.adjcost.InvoiceAdjustmentCost_;
import org.trescal.cwms.core.pricing.invoice.entity.costs.calcost.InvoiceCalibrationCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.calcost.InvoiceCalibrationCost_;
import org.trescal.cwms.core.pricing.invoice.entity.costs.purchasecost.InvoicePurchaseCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.purchasecost.InvoicePurchaseCost_;
import org.trescal.cwms.core.pricing.invoice.entity.costs.repcost.InvoiceRepairCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.repcost.InvoiceRepairCost_;
import org.trescal.cwms.core.pricing.invoice.entity.costs.servicecost.InvoiceServiceCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.servicecost.InvoiceServiceCost_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.rest.accounting.dto.InvoiceAccountingDTO;
import org.trescal.cwms.rest.accounting.dto.InvoiceItemAccountingDTO;

import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class RestInvoicesDaoImpl extends BaseDaoImpl<Invoice, Integer> implements RestInvoicesDao {

	@Override
	protected Class<Invoice> getEntity() {
		return Invoice.class;
	}

	@Override
	public List<InvoiceItemAccountingDTO> getAllItemsForAccounting(Integer invoiceId) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceItemAccountingDTO> cq = cb.createQuery(InvoiceItemAccountingDTO.class);
			Root<InvoiceItem> item = cq.from(InvoiceItem.class);
			Join<InvoiceItem, Invoice> invoice = item.join(InvoiceItem_.invoice);
			Join<InvoiceItem, NominalCode> nominal = item.join(InvoiceItem_.nominal, JoinType.LEFT);
			Join<InvoiceItem, Subdiv> itemSubdiv = item.join(InvoiceItem_.businessSubdiv);
			Join<InvoiceItem, InvoiceCalibrationCost> calibrationCost = item.join(InvoiceItem_.calibrationCost,
					JoinType.LEFT);
			calibrationCost.on(calibrationCost.get(InvoiceCalibrationCost_.active));
			Join<InvoiceCalibrationCost, NominalCode> calibrationNominalCode = calibrationCost
					.join(InvoiceCalibrationCost_.nominal, JoinType.LEFT);
			Join<InvoiceItem, InvoiceRepairCost> repairCost = item.join(InvoiceItem_.repairCost, JoinType.LEFT);
			repairCost.on(repairCost.get(InvoiceRepairCost_.active));
			Join<InvoiceRepairCost, NominalCode> repairNominalCode = repairCost.join(InvoiceRepairCost_.nominal,
					JoinType.LEFT);
			Join<InvoiceItem, InvoiceAdjustmentCost> adjustmentCost = item.join(InvoiceItem_.adjustmentCost,
					JoinType.LEFT);
			adjustmentCost.on(adjustmentCost.get(InvoiceAdjustmentCost_.active));
			Join<InvoiceAdjustmentCost, NominalCode> adjustmentNominalCode = adjustmentCost
					.join(InvoiceAdjustmentCost_.nominal, JoinType.LEFT);
			Join<InvoiceItem, InvoicePurchaseCost> purchaseCost = item.join(InvoiceItem_.purchaseCost, JoinType.LEFT);
			purchaseCost.on(purchaseCost.get(InvoicePurchaseCost_.active));
			Join<InvoicePurchaseCost, NominalCode> purchaseNominalCode = purchaseCost.join(InvoicePurchaseCost_.nominal,
					JoinType.LEFT);
			Join<InvoiceItem, InvoiceServiceCost> serviceCost = item.join(InvoiceItem_.serviceCost, JoinType.LEFT);
			serviceCost.on(serviceCost.get(InvoiceServiceCost_.active));
			Join<InvoiceServiceCost, NominalCode> serviceNominalCode = serviceCost.join(InvoiceServiceCost_.nominal,
					JoinType.LEFT);
			Join<InvoiceItem, JobItem> jobItem = item.join(InvoiceItem_.jobItem, JoinType.LEFT);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job, JoinType.LEFT);
			Join<Job, Subdiv> jobSubdiv = job.join("organisation", JoinType.LEFT);
			Join<InvoiceItem, JobExpenseItem> jobExpenseItem = item.join(InvoiceItem_.expenseItem, JoinType.LEFT);
			Join<JobExpenseItem, Job> job2 = jobExpenseItem.join(JobExpenseItem_.job, JoinType.LEFT);
			Join<Job, Subdiv> jobSubdiv2 = job2.join("organisation", JoinType.LEFT);
			Join<InvoiceItem, Address> shipToAddress = item.join(InvoiceItem_.shipToAddress);
			Join<Address, Country> shipToCountry = shipToAddress.join(Address_.country);
			Join<Address, Subdiv> shipToSubdiv = shipToAddress.join(Address_.sub);
			Join<Subdiv, Company> shipToCompany = shipToSubdiv.join(Subdiv_.comp);
			Join<InvoiceItem, InvoiceItemPO> invoiceItemPO = item.join(InvoiceItem_.invItemPOs, JoinType.LEFT);
			invoiceItemPO.on(cb.isNull(invoiceItemPO.get(InvoiceItemPO_.invPO)));
			Join<InvoiceItemPO, BPO> bpo = invoiceItemPO.join(InvoiceItemPO_.bpo, JoinType.LEFT);
			Join<InvoiceItemPO, PO> jobPO = invoiceItemPO.join(InvoiceItemPO_.jobPO, JoinType.LEFT);
			Join<InvoiceItem, InvoicePOItem> invoicePOItem = item.join(InvoiceItem_.invPOsItem, JoinType.LEFT);
			Join<InvoicePOItem, InvoicePO> invoicePO = invoicePOItem.join(InvoicePOItem_.invPO, JoinType.LEFT);
			cq.where(cb.equal(invoice.get(Invoice_.id), invoiceId));
			cq.select(cb.construct(InvoiceItemAccountingDTO.class, item.get(InvoiceItem_.itemno),
					item.get(InvoiceItem_.description),
					cb.coalesce(cb.coalesce(itemSubdiv.get(Subdiv_.analyticalCenter),
							jobSubdiv.get(Subdiv_.analyticalCenter)), jobSubdiv2.get(Subdiv_.analyticalCenter)),
					nominal.get(NominalCode_.code), nominal.get(NominalCode_.title), item.get(InvoiceItem_.quantity),
					item.get(InvoiceItem_.totalCost), calibrationNominalCode.get(NominalCode_.code),
					calibrationNominalCode.get(NominalCode_.title),
					calibrationCost.get(InvoiceCalibrationCost_.discountRate),
					calibrationCost.get(InvoiceCalibrationCost_.discountValue),
					calibrationCost.get(InvoiceCalibrationCost_.finalCost),
					calibrationCost.get(InvoiceCalibrationCost_.totalCost),
					adjustmentNominalCode.get(NominalCode_.code), adjustmentNominalCode.get(NominalCode_.title),
					adjustmentCost.get(InvoiceAdjustmentCost_.discountRate),
					adjustmentCost.get(InvoiceAdjustmentCost_.discountValue),
					adjustmentCost.get(InvoiceAdjustmentCost_.finalCost),
					adjustmentCost.get(InvoiceAdjustmentCost_.totalCost), repairNominalCode.get(NominalCode_.code),
					repairNominalCode.get(NominalCode_.title), repairCost.get(InvoiceRepairCost_.discountRate),
					repairCost.get(InvoiceRepairCost_.discountValue), repairCost.get(InvoiceRepairCost_.finalCost),
					repairCost.get(InvoiceRepairCost_.totalCost), purchaseNominalCode.get(NominalCode_.code),
					purchaseNominalCode.get(NominalCode_.title), purchaseCost.get(InvoiceRepairCost_.discountRate),
					purchaseCost.get(InvoiceRepairCost_.discountValue), purchaseCost.get(InvoiceRepairCost_.finalCost),
					purchaseCost.get(InvoiceRepairCost_.totalCost), serviceNominalCode.get(NominalCode_.code),
					serviceNominalCode.get(NominalCode_.title), serviceCost.get(InvoiceRepairCost_.discountRate),
					serviceCost.get(InvoiceRepairCost_.discountValue), serviceCost.get(InvoiceRepairCost_.finalCost),
					serviceCost.get(InvoiceRepairCost_.totalCost), item.get(InvoiceItem_.generalDiscountRate),
					item.get(InvoiceItem_.generalDiscountValue), item.get(InvoiceItem_.finalCost),
					shipToCompany.get(Company_.coid), shipToCompany.get(Company_.coname),
					shipToSubdiv.get(Subdiv_.subdivid),
					cb.concat(
							cb.concat(cb.concat(shipToAddress.get(Address_.addr1), " "),
									cb.concat(shipToAddress.get(Address_.addr2), " ")),
							shipToAddress.get(Address_.addr3)),
					shipToAddress.get(Address_.town), shipToAddress.get(Address_.postcode),
					shipToAddress.get(Address_.county), shipToCountry.get(Country_.countryCode), bpo.get(BPO_.poId),
					bpo.get(BPO_.poNumber), jobPO.get(PO_.poId), jobPO.get(PO_.poNumber), invoicePO.get(PO_.poId),
					invoicePO.get(InvoicePO_.poNumber), item.get(InvoiceItem_.taxable),
					item.get(InvoiceItem_.taxAmount), jobItem.get(JobItem_.jobItemId), jobExpenseItem.get(JobExpenseItem_.id)));
			return cq;
		});
	}

	@Override
	public List<InvoiceAccountingDTO> getAllInvoicesForAccounting(Integer busId) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceAccountingDTO> cq = cb.createQuery(InvoiceAccountingDTO.class);
			Root<Invoice> invoice = cq.from(Invoice.class);
			Join<Invoice, Company> company = invoice.join(Invoice_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> companySettings = company.join(Company_.settingsForAllocatedCompanies);
			companySettings.on(cb.equal(companySettings.get(CompanySettingsForAllocatedCompany_.organisation), busId));
			Join<Invoice, Address> address = invoice.join(Invoice_.address);
            Join<Address, Subdiv> subdiv = address.join(Address_.sub);
            Join<Subdiv, Contact> defaultContact = subdiv.join(Subdiv_.defaultContact, JoinType.LEFT);
            Join<Address, Country> country = address.join(Address_.country);
            Join<Invoice, SupportedCurrency> currency = invoice.join(Invoice_.currency);
            Join<Invoice, PaymentMode> paymentMode = invoice.join(Invoice_.paymentMode, JoinType.LEFT);
            Join<Invoice, VatRate> vatRate = invoice.join(Invoice_.vatRateEntity, JoinType.LEFT);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(invoice.get(Invoice_.accountsStatus), AccountStatus.P));
            clauses.getExpressions().add(cb.equal(invoice.get(Invoice_.organisation), busId));
            cq.where(clauses);
            cq.select(cb.construct(InvoiceAccountingDTO.class, invoice.get(Invoice_.id), invoice.get(Invoice_.invno),
                    company.get(Company_.coid), company.get(Company_.coid), company.get(Company_.coname),
                    company.get(Company_.fiscalIdentifier), subdiv.get(Subdiv_.subdivid),
                    subdiv.get(Subdiv_.siretNumber),
                    cb.concat(cb.concat(cb.concat(address.get(Address_.addr1), " "),
                            cb.concat(address.get(Address_.addr2), " ")), address.get(Address_.addr3)),
                    address.get(Address_.town), address.get(Address_.postcode), address.get(Address_.county),
                    country.get(Country_.countryCode),
                    cb.concat(cb.concat(defaultContact.get(Contact_.firstName), "" + (char) 32),
                            defaultContact.get(Contact_.lastName)),
                    defaultContact.get(Contact_.email), invoice.get(Invoice_.accountsStatus),
                    invoice.get(Invoice_.regdate), invoice.get(Invoice_.issuedate), invoice.get(Invoice_.invoiceDate),
                    invoice.get(Invoice_.paymentTerm), invoice.get(Invoice_.duedate),
                    paymentMode.get(PaymentMode_.code), invoice.get(Invoice_.financialPeriod),
                    currency.get(SupportedCurrency_.currencyCode), currency.get(SupportedCurrency_.defaultRate),
                    invoice.get(Invoice_.vatRate), vatRate.get(VatRate_.vatCode), vatRate.get(VatRate_.type),
                    invoice.get(Invoice_.totalCost), invoice.get(Invoice_.finalCost),
                    companySettings.get(CompanySettingsForAllocatedCompany_.invoiceFactoring)));
            return cq;
        });
    }
}