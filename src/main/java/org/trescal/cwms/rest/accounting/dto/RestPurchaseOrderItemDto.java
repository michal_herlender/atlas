package org.trescal.cwms.rest.accounting.dto;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RestPurchaseOrderItemDto {
	private String nominalCode;
	private Integer quantity;
	private BigDecimal unitPrice;
	private BigDecimal discountRate;
	private BigDecimal amountExclTax;
	private String description;
	private String analyticalCenter;
	
	public RestPurchaseOrderItemDto(String nominalCode, Integer quantity, BigDecimal unitPrice, BigDecimal discountRate,
			BigDecimal amountExclTax, String description, String analyticalCenter) {
		super();
		this.nominalCode = nominalCode;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.discountRate = discountRate;
		this.amountExclTax = amountExclTax;
		this.description = description;
		this.analyticalCenter = analyticalCenter;
	}

	public RestPurchaseOrderItemDto() {
		super();
	}
	
}
