package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.InvoiceAccountingDTO;

public interface RestInvoicesService {

	List<InvoiceAccountingDTO> getAllInvoicesForAccounting(Integer busId);
}