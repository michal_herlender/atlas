package org.trescal.cwms.rest.accounting.dto;

public class RestCustomerDto {
	
	private Integer id;
	private String fiscalId;
	private String name;
	private String address;
	private String city; 
	private String telephoneNo;
	private String countryCode;
	private String postCode;
	private String county;
	private String paymentTermsCode;
	private String paymentModecode;
	private Integer busId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFiscalId() {
		return fiscalId;
	}
	public void setFiscalId(String fiscalId) {
		this.fiscalId = fiscalId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getTelephoneNo() {
		return telephoneNo;
	}
	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getPaymentTermsCode() {
		return paymentTermsCode;
	}
	public void setPaymentTermsCode(String paymentTermsCode) {
		this.paymentTermsCode = paymentTermsCode;
	}
	public String getPaymentModecode() {
		return paymentModecode;
	}
	public void setPaymentModecode(String paymentModecode) {
		this.paymentModecode = paymentModecode;
	}
	public Integer getBusId() {
		return busId;
	}
	public void setBusId(Integer busId) {
		this.busId = busId;
	}
	
	
	
}
