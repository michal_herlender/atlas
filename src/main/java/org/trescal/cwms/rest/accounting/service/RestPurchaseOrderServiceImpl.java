package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.rest.accounting.dto.RestPurchaseOrderDto;
import org.trescal.cwms.rest.accounting.dto.RestPurchaseOrderItemDto;
import org.trescal.cwms.rest.accounting.repository.RestPurchaseOrderDao;

@Service
public class RestPurchaseOrderServiceImpl implements RestPurchaseOrderService {
	@Autowired
	private RestPurchaseOrderDao RestPurchaseOrderDao;
	
	@Override
	public List<RestPurchaseOrderDto> getAllPurchaseOrders(Integer busId){
		List<RestPurchaseOrderDto> purchaseorders = RestPurchaseOrderDao.getAllPurchaseOrders(busId);
		for(RestPurchaseOrderDto purchaseorder:purchaseorders)
		{
			List<RestPurchaseOrderItemDto> items = RestPurchaseOrderDao.getAllItems(purchaseorder.getPoNo(), busId);
			purchaseorder.setItems(items);
		}
		return purchaseorders;
	}

	@Override
	public List<RestPurchaseOrderDto> getPurchaseOrders(Integer busId) {
		List<RestPurchaseOrderDto> purchaseorders = RestPurchaseOrderDao.getPurchaseOrders(busId);
		for(RestPurchaseOrderDto purchaseorder:purchaseorders)
		{
			List<RestPurchaseOrderItemDto> items = RestPurchaseOrderDao.getPurchaseOrderItems(purchaseorder.getPoNo(), busId);
			purchaseorder.setItems(items);
		}
		return purchaseorders;
	}
}
