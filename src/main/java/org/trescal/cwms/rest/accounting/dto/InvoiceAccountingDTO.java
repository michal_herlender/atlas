package org.trescal.cwms.rest.accounting.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import lombok.Data;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.vatrate.VatRateType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Data
public class InvoiceAccountingDTO {

	private Integer invId;
	private String invNo;
	private Integer sellTo_id;
	private Integer billTo_id;
	private String billTo_name;
	private String billTo_fiscalId;
    private Integer billTo_subdivId;
    private String billTo_subdivFiscalId;
    private String billTo_addr;
    private String billTo_city;
    private String billTo_postCode;
    private String billTo_county;
    private String billTo_countryCode;
    private String billTo_contact;
    private String billTo_email;
    private AccountStatus accountStatus;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate regDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate issueDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate invoiceDate;
    private PaymentTerm paymentTermsCode;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate paymentDate;
    private String paymentModeCode;
    private Integer period;
    private String currencyCode;
    private BigDecimal currencyRate;
    private BigDecimal vatRate;
    private String vatRateCode;
    private VatRateType vatRateType;
    private BigDecimal totalAmountExclTax;
    private BigDecimal totalAmountInclTax;
    private Boolean factoring;
    private List<InvoiceItemAccountingDTO> items;

    public InvoiceAccountingDTO(Integer invId, String invNo, Integer sellTo_id, Integer billTo_id, String billTo_name,
                                String billTo_fiscalId, Integer billTo_subdivId, String billTo_subdivFiscalId, String billTo_addr, String billTo_city,
                                String billTo_postCode, String billTo_county, String billTo_countryCode, String billTo_contact,
                                String billTo_email, AccountStatus accountStatus, LocalDate regDate, LocalDate issueDate, LocalDate invoiceDate,
                                PaymentTerm paymentTermsCode, LocalDate paymentDate, String paymentModeCode, Integer period, String currencyCode,
                                BigDecimal currencyRate, BigDecimal vatRate, String vatRateCode, VatRateType vatRateType,
                                BigDecimal totalAmountExclTax, BigDecimal totalAmountInclTax, Boolean factoring) {
        this.invId = invId;
        this.invNo = invNo;
        this.sellTo_id = sellTo_id;
        this.billTo_id = billTo_id;
        this.billTo_name = billTo_name;
        this.billTo_fiscalId = billTo_fiscalId;
        this.billTo_subdivId = billTo_subdivId;
        this.billTo_subdivFiscalId = billTo_subdivFiscalId;
        this.billTo_addr = billTo_addr;
        this.billTo_city = billTo_city;
        this.billTo_postCode = billTo_postCode;
        this.billTo_county = billTo_county;
        this.billTo_countryCode = billTo_countryCode;
        this.billTo_contact = billTo_contact;
        this.billTo_email = billTo_email;
        this.accountStatus = accountStatus;
        this.regDate = regDate;
        this.issueDate = issueDate;
        this.invoiceDate = invoiceDate;
        this.paymentTermsCode = paymentTermsCode;
        this.paymentDate = paymentDate;
        this.paymentModeCode = paymentModeCode;
        this.period = period;
        this.currencyCode = currencyCode;
        this.currencyRate = currencyRate;
        this.vatRate = vatRate;
        this.vatRateCode = vatRateCode;
        this.vatRateType = vatRateType;
        this.totalAmountExclTax = totalAmountExclTax;
        this.totalAmountInclTax = totalAmountInclTax;
        this.factoring = factoring;
    }
}