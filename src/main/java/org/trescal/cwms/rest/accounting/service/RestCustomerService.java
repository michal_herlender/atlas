package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.RestCustomerDto;

public interface RestCustomerService {

	List<RestCustomerDto> getAllCustomers(String fiscalId, Integer busId);
}