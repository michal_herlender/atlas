package org.trescal.cwms.rest.accounting.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.tools.MathTools;
import org.trescal.cwms.rest.accounting.dto.InvoiceAccountingDTO;
import org.trescal.cwms.rest.accounting.dto.InvoiceItemAccountingDTO;
import org.trescal.cwms.rest.accounting.repository.RestInvoicesDao;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Service
public class RestInvoicesServicesImpl implements RestInvoicesService {

	@Autowired
	private RestInvoicesDao restInvoicesDao;

	@Override
	public List<InvoiceAccountingDTO> getAllInvoicesForAccounting(Integer busId) {
		List<InvoiceAccountingDTO> invoices = restInvoicesDao.getAllInvoicesForAccounting(busId);
		invoices.parallelStream().forEach(invoice -> {
			List<InvoiceItemAccountingDTO> items = restInvoicesDao.getAllItemsForAccounting(invoice.getInvId());
			Map<Integer, InvoiceItemAccountingDTO> itemMap = new TreeMap<>();
			items.forEach(item -> {
				if (itemMap.containsKey(item.getItemNo())) {
					if (item.getPurchaseOrderNumber() != null && !item.getPurchaseOrderNumber().isEmpty()) {
						InvoiceItemAccountingDTO mapItem = itemMap.get(item.getItemNo());
						if (mapItem.getPurchaseOrderNumber() == null || mapItem.getPurchaseOrderNumber().isEmpty())
							mapItem.setPurchaseOrderNumber(item.getPurchaseOrderNumber());
						else
							mapItem.setPurchaseOrderNumber(
								mapItem.getPurchaseOrderNumber() + ", " + item.getPurchaseOrderNumber());
					}
				} else
					itemMap.put(item.getItemNo(), item);
			});
			items = itemMap.values().stream().sorted(Comparator.comparing(InvoiceItemAccountingDTO::getItemNo))
				.collect(Collectors.toList());
			items.forEach(item -> {
				if (item.getTaxAmount() != null)
					// All new generated invoices should be here
					item.setAmountInclTax(item.getAmountExclTax().add(item.getTaxAmount()));
				else if (item.getTaxable() && invoice.getVatRate() != null)
					item.setAmountInclTax(
						invoice.getVatRate().add(BigDecimal.valueOf(100)).multiply(item.getAmountExclTax()).divide(
							BigDecimal.valueOf(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN));
				else
					item.setAmountInclTax(item.getAmountExclTax());
				// Should be replaced, when we have vat rate by item
				item.setVatRate(invoice.getVatRate());
				item.setVatRateCode(invoice.getVatRateCode());
			});
			invoice.setItems(items);
		});
		return invoices;
	}
}