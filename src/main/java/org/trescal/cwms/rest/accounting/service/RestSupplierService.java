package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.RestSupplierDto;

public interface RestSupplierService {
	
	List<RestSupplierDto> getAllSupplier(String fiscalId, Integer busId);
	
	

}
