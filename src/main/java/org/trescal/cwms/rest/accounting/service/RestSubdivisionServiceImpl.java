package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.dto.AddressDTO;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.rest.accounting.dto.SubdivisionAccountingDTO;
import org.trescal.cwms.rest.accounting.repository.RestSubdivisionDao;

@Service
public class RestSubdivisionServiceImpl implements RestSubdivisionService {

	@Autowired
	private RestSubdivisionDao restSubdivisionDao;

	@Override
	public List<AddressDTO> getAllAddressesForAccounting(Integer subdivId) {
		return this.restSubdivisionDao.getAllAddressesForAccounting(subdivId);
	}

	@Override
	public List<SubdivisionAccountingDTO> getAllSubdivisionsForAccounting(List<CompanyRole> roles, Integer busId,
			Integer subdivId, String subdivFiscalId) {
		List<SubdivisionAccountingDTO> subdivs = this.restSubdivisionDao.getAllSubdivisionsForAccounting(roles, busId,
				subdivId, subdivFiscalId);
		for (SubdivisionAccountingDTO dto : subdivs) {
			dto.setAddresses(this.getAllAddressesForAccounting(dto.getSubdivId()));
		}
		return subdivs;
	}
}
