package org.trescal.cwms.rest.accounting.repository;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.RestPurchaseOrderDto;
import org.trescal.cwms.rest.accounting.dto.RestPurchaseOrderItemDto;

public interface RestPurchaseOrderDao {
	
	List<RestPurchaseOrderDto> getAllPurchaseOrders(Integer busId);
	List<RestPurchaseOrderItemDto> getAllItems(String poNo, Integer busId);
	
	List<RestPurchaseOrderDto> getPurchaseOrders(Integer busId);
	
	List<RestPurchaseOrderItemDto> getPurchaseOrderItems(String poNo, Integer busId);
	
}
