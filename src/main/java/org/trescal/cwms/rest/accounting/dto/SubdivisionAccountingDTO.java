package org.trescal.cwms.rest.accounting.dto;

import lombok.Setter;

import java.util.List;

import org.trescal.cwms.core.company.dto.AddressDTO;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;

import lombok.Getter;

@Getter
@Setter
public class SubdivisionAccountingDTO {

	private Integer companyId;
	private String companyFiscalId;
	private String companyName;
	private Integer subdivId;
	private String subdivFiscalId;
	private String subdivName;
	private String paymentTermsCode;
	private String paymentModeCode;
	private Integer busId;
	private List<AddressDTO> addresses;

	public SubdivisionAccountingDTO(Integer companyId, String companyFiscalId, String companyName, Integer subdivId,
			String subdivFiscalId, String subdivName, PaymentTerm paymentTerms, PaymentMode paymentMode,
			Integer busId) {
		super();
		this.companyId = companyId;
		this.companyFiscalId = companyFiscalId;
		this.companyName = companyName;
		this.subdivId = subdivId;
		this.subdivFiscalId = subdivFiscalId;
		this.subdivName = subdivName;
		this.paymentTermsCode = paymentTerms.getCode();
		this.paymentModeCode = paymentMode.getCode();
		this.busId = busId;
	}
}
