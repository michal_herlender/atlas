package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.rest.accounting.dto.RestSupplierDto;
import org.trescal.cwms.rest.accounting.repository.RestSupplierDao;

@Service
public class RestSupplierServiceImpl implements RestSupplierService {
	
	@Autowired
	private RestSupplierDao restSupplierDao;
	
	@Override
	public List<RestSupplierDto> getAllSupplier(String fiscalId, Integer busId){
		
		List<RestSupplierDto> suppliers = restSupplierDao.getAllSupplier(fiscalId, busId);
		return suppliers;
	
	}

}
