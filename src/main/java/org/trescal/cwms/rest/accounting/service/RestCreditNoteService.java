package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.CreditNoteAccountingDTO;

public interface RestCreditNoteService {

	List<CreditNoteAccountingDTO> getAllForAccounting(Integer busId);
}