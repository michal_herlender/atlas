package org.trescal.cwms.rest.accounting.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.Data;

@Data
public class CreditNoteItemAccountingDTO {

	private Integer itemNo;
	private String nominalCode;
	private String nominalTitle;
	private Integer quantity;
	private BigDecimal unitPrice;
	private BigDecimal vatRate;		// Not included in constructor as it may be null (e.g. USA), and null literal not permitted, set afterwards!
	private String vatRateCode;
	private BigDecimal vatValue;	// Set manually afterwards
	private BigDecimal amountExclTax;
	private BigDecimal amountInclTax;
	private String analyticalCenter;
	private String purchaseOrderNumber;
	private String shipTo_postCode;
	private String shipTo_countryCode;
	private String shipTo_county;
	private Boolean taxable;
	private BigDecimal taxAmount;

	public CreditNoteItemAccountingDTO(Integer itemNo, String nominalCode, String nominalTitle, Integer quantity,
			BigDecimal unitPrice, BigDecimal amountExclTax, String analyticalCenter, String shipTo_postCode, 
			String shipTo_countryCode, String shipTo_county, Boolean taxable, BigDecimal taxAmount) {
		super();
		this.itemNo = itemNo;
		this.nominalCode = nominalCode;
		this.nominalTitle = nominalTitle;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.amountExclTax = amountExclTax;
		this.analyticalCenter = analyticalCenter;
		this.shipTo_postCode = shipTo_postCode;
		this.shipTo_countryCode = shipTo_countryCode;
		this.shipTo_county = shipTo_county;
		this.taxable = taxable;
		this.taxAmount = taxAmount;
	}

	/**
	 * Must be called externally after JPA constructor; vatRate from credit note may be null (e.g. for US)
	 * @param vatRate
	 */
	public void setVatRateAndCalculateValue(BigDecimal vatRate) {
		this.vatRate = vatRate;
		if (vatRate != null) {
			this.vatValue = amountExclTax.multiply(vatRate).divide(BigDecimal.valueOf(100)).setScale(2,
					RoundingMode.HALF_UP);
		}
		else {
			this.vatValue = new BigDecimal("0.00"); 
		}
		this.amountInclTax = amountExclTax.add(this.vatValue);
	}

}