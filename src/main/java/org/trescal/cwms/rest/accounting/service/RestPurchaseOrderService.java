package org.trescal.cwms.rest.accounting.service;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.RestPurchaseOrderDto;

public interface RestPurchaseOrderService {
	List<RestPurchaseOrderDto> getAllPurchaseOrders(Integer busId);
	
	List<RestPurchaseOrderDto> getPurchaseOrders(Integer busId);
	

}
