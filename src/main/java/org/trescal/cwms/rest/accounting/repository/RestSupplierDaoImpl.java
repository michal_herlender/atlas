package org.trescal.cwms.rest.accounting.repository;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.rest.accounting.dto.RestSupplierDto;

@Repository
public class RestSupplierDaoImpl implements RestSupplierDao {

	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager entityManager;

	@Override
	public List<RestSupplierDto> getAllSupplier(String fiscalId, Integer busId) {

		Query myquery = entityManager
				.createNativeQuery("SELECT id, fiscalId, name, address, city, telephoneNo,countryCode, postCode, "
						+ "county, paymentTermsCode, paymentModeCode, busId " + "FROM new_supplieraccounts "
						+ "where fiscalId='" + fiscalId + "' AND busId=" + busId);

		@SuppressWarnings("unchecked")
		List<Object[]> mylist = myquery.getResultList();
		return mylist.stream().map(result -> {
			RestSupplierDto mydto = new RestSupplierDto();
			mydto.setId((Integer) result[0]);
			mydto.setFiscalId((String) result[1]);
			mydto.setName((String) result[2]);
			mydto.setAddress((String) result[3]);
			mydto.setCity((String) result[4]);
			mydto.setTelephoneNo((String) result[5]);
			mydto.setCountryCode((String) result[6]);
			mydto.setPostCode((String) result[7]);
			mydto.setCounty((String) result[8]);
			mydto.setPaymentTermsCode((String) result[9]);
			mydto.setPaymentModeCode((String) result[10]);
			mydto.setBusId((Integer) result[11]);
			return mydto;

		}).collect(Collectors.toList());

	}
}
