package org.trescal.cwms.rest.accounting.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.rest.accounting.dto.CreditNoteAccountingDTO;
import org.trescal.cwms.rest.accounting.dto.CreditNoteItemAccountingDTO;
import org.trescal.cwms.rest.accounting.repository.RestCreditNoteDao;

@Service
public class RestCreditNoteServicesImpl implements RestCreditNoteService {

	@Autowired
	private RestCreditNoteDao restCreditNoteDao;

	@Override
	public List<CreditNoteAccountingDTO> getAllForAccounting(Integer busId) {
		List<CreditNoteAccountingDTO> creditNotes = restCreditNoteDao.getAllForAccounting(busId);
		creditNotes.forEach(cn -> {
			List<CreditNoteItemAccountingDTO> items = restCreditNoteDao.getAllItemsForAccounting(cn.getCreditNoteId()); 
			/* Should be replaced, if vat rates are managed at credit note item */
			BigDecimal vatRate = cn.getVatRate();
			String vatRateCode = cn.getVatRateCode();
			items.forEach(item -> {
				item.setVatRateAndCalculateValue(vatRate);
				item.setVatRateCode(vatRateCode);
			});
			cn.setItems(items);
		});
		return creditNotes;
	}
}