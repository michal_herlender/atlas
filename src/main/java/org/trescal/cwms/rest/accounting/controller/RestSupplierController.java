package org.trescal.cwms.rest.accounting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.accounting.dto.RestSupplierDto;
import org.trescal.cwms.rest.accounting.service.RestSupplierService;

@Controller
@RestJsonController
public class RestSupplierController {
	
	@Autowired
	RestSupplierService restSupplierService;
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value= {"/accounting/supplier", "/api/accounting/supplier"}, method = RequestMethod.GET, produces = {
		Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	
	public List<RestSupplierDto> getAllSupplier(@RequestParam(name="fiscalId", required=true) String fiscalId,
			@RequestParam(name="busId",required=true) Integer busId){
		
		List<RestSupplierDto> mylist = restSupplierService.getAllSupplier(fiscalId, busId);
		return mylist;
			
	}

}
