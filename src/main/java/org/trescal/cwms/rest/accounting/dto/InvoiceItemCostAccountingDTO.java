package org.trescal.cwms.rest.accounting.dto;

import java.math.BigDecimal;

public class InvoiceItemCostAccountingDTO {

	private String nominalCode;
	private String nominalTitle;
	private BigDecimal discountRate;
	private BigDecimal discountValue;
	private BigDecimal finalCost;
	private BigDecimal totalCost;

	public InvoiceItemCostAccountingDTO(String nominalCode, String nominalTitle, BigDecimal discountRate,
			BigDecimal discountValue, BigDecimal finalCost, BigDecimal totalCost) {
		super();
		this.nominalCode = nominalCode;
		this.nominalTitle = nominalTitle;
		this.discountRate = discountRate;
		this.discountValue = discountValue;
		this.finalCost = finalCost;
		this.totalCost = totalCost;
	}

	public String getNominalCode() {
		return nominalCode;
	}

	public void setNominalCode(String nominalCode) {
		this.nominalCode = nominalCode;
	}

	public String getNominalTitle() {
		return nominalTitle;
	}

	public void setNominalTitle(String nominalTitle) {
		this.nominalTitle = nominalTitle;
	}

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	public BigDecimal getFinalCost() {
		return finalCost;
	}

	public void setFinalCost(BigDecimal finalCost) {
		this.finalCost = finalCost;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}
}