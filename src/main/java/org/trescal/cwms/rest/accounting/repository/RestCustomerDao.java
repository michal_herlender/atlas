package org.trescal.cwms.rest.accounting.repository;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.RestCustomerDto;

public interface RestCustomerDao {

	List<RestCustomerDto> getAllCustomers(String fiscalId, Integer busId);
}