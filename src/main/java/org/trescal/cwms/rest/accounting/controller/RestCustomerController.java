package org.trescal.cwms.rest.accounting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.accounting.dto.RestCustomerDto;
import org.trescal.cwms.rest.accounting.service.RestCustomerService;

@Controller
@RestJsonController
public class RestCustomerController {
	
	@Autowired
	private RestCustomerService restCustomerService;
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value= {"/accounting/customers", "/api/accounting/customers"}, method = RequestMethod.GET, produces = {
		Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	
	public List<RestCustomerDto> getCustomers(@RequestParam(name="fiscalId",required=true) String fiscalId,
		@RequestParam(name="busId", required=true) Integer busId){
		
		List<RestCustomerDto> myList = restCustomerService.getAllCustomers(fiscalId, busId);
		return myList;
	
	
	}

}
