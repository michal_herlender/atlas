package org.trescal.cwms.rest.accounting.repository;

import java.util.List;

import org.trescal.cwms.rest.accounting.dto.RestSupplierDto;

public interface RestSupplierDao {
	
	List<RestSupplierDto> getAllSupplier(String fiscalId, Integer busId);
	

}
