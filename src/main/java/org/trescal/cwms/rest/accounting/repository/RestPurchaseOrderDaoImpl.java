package org.trescal.cwms.rest.accounting.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem_;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice_;
import org.trescal.cwms.rest.accounting.dto.RestPurchaseOrderDto;
import org.trescal.cwms.rest.accounting.dto.RestPurchaseOrderItemDto;


@Repository
public class RestPurchaseOrderDaoImpl extends BaseDaoImpl<PurchaseOrder, Integer> implements RestPurchaseOrderDao {
	
	@PersistenceContext(unitName="entityManagerFactory")
	private EntityManager entityManager;
	
	@Override
	protected Class<PurchaseOrder> getEntity() {
		return PurchaseOrder.class;
	}
	
	@Override
	public List<RestPurchaseOrderDto> getAllPurchaseOrders(Integer busId){
		
		Query query = entityManager.createNativeQuery("SELECT poNo,MIN(payTo_Id) AS payTo_Id,MIN(payTo_fiscalId) AS payTo_fiscalId, MIN(invoiceDate) AS invoiceDate,MIN(paymentTermsCode) AS paymentTermsCode ," +
		"MIN(payTo_countryCode) AS payTo_countryCode,MIN(paymentModeCode) AS paymentModeCode,MIN(analyticalCenter) AS analyticalCenter, MIN(busId) AS busId,MIN(invoiceNo) AS invoiceNo " +
		"FROM new_purchaseorderaccounts WHERE busId=" + busId + " GROUP BY poNo");
		
		@SuppressWarnings("unchecked")
		List<Object[]> mList = query.getResultList();
		return mList.stream().map(result -> {
			RestPurchaseOrderDto dto= new RestPurchaseOrderDto();
			dto.setPoNo((String) result[0]);
			dto.setPayTo_id((Integer) result[1]);
			dto.setPayTo_fiscalId((String) result[2]);
			dto.setInvoiceDate((LocalDate) result[3]);
			dto.setPaymentTermsCode((String) result[4]);
			dto.setPayTo_countryCode((String) result[5]);
			dto.setPaymentModeCode((String) result[6]);
//			dto.setAnalyticalCenter((String) result[7]);
			dto.setBusId((Integer) result[8]);
			dto.setInvoiceNo((String) result[9]);
			return dto;
			
		}).collect(Collectors.toList());
		
	}

	@Override
	public List<RestPurchaseOrderDto> getPurchaseOrders(Integer busId){
		return getResultList(cb -> {
			CriteriaQuery<RestPurchaseOrderDto> cq = cb.createQuery(RestPurchaseOrderDto.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Join<PurchaseOrder, Contact> contact = root.join(PurchaseOrder_.contact);
			Join<Contact, Subdiv> sub = contact.join(Contact_.sub);
			Join<Subdiv, Company> company = sub.join(Subdiv_.comp, JoinType.LEFT);
			Join<PurchaseOrder, Company> businessCompany = root.join("organisation", JoinType.INNER);
			Join<PurchaseOrder, Address> address = root.join(PurchaseOrder_.address, JoinType.LEFT);
			Join<Address, Country> country = address.join(Address_.country, JoinType.LEFT);
			Join<PurchaseOrder, SupplierInvoice> supplierInvoice = root.join(PurchaseOrder_.supplierInvoices, JoinType.LEFT);
			Join<SupplierInvoice, PaymentMode> paymentMode = supplierInvoice.join(SupplierInvoice_.paymentMode, JoinType.LEFT);
			cq.where(cb.equal(businessCompany.get(Company_.coid), busId));
			cq.select(cb.construct(RestPurchaseOrderDto.class, 
					root.get(PurchaseOrder_.pono),
					company.get(Company_.coid),
					company.get(Company_.fiscalIdentifier),
					sub.get(Subdiv_.subdivid),
					sub.get(Subdiv_.siretNumber),
					supplierInvoice.get(SupplierInvoice_.invoiceDate),
					supplierInvoice.get(SupplierInvoice_.paymentTerm),
					country.get(Country_.countryCode),
					paymentMode.get(PaymentMode_.code),
					businessCompany.get(Company_.coid),
					supplierInvoice.get(SupplierInvoice_.invoiceNumber)
					));
			cq.distinct(true);
			return cq;
		});
	}
	
	@Override
	public List<RestPurchaseOrderItemDto> getAllItems(String poNo, Integer busId) {
		// TODO Auto-generated method stub
		Query query = entityManager.createNativeQuery("SELECT nominalCode,quantity,unitPrice,discountRate,amountExclTax,description " +
		"FROM new_purchaseorderaccounts WHERE poNo='" + poNo + "' AND busId=" + busId);
		
		@SuppressWarnings("unchecked")
		List<Object[]> itemlist = query.getResultList();
		return itemlist.stream().map(result -> {
			RestPurchaseOrderItemDto dto = new RestPurchaseOrderItemDto();
			dto.setNominalCode((String) result[0]);
			dto.setQuantity((Integer) result[1]);
			dto.setUnitPrice((BigDecimal) result[2]);
			dto.setDiscountRate((BigDecimal) result[3]);
			dto.setAmountExclTax((BigDecimal) result[4]);
			dto.setDescription((String) result[5]);
			return dto;
		}).collect(Collectors.toList());
		
	}
	
	@Override
	public List<RestPurchaseOrderItemDto> getPurchaseOrderItems(String poNo, Integer busId){
		return getResultList(cb ->{
			CriteriaQuery<RestPurchaseOrderItemDto> cq = cb.createQuery(RestPurchaseOrderItemDto.class);
			Root<PurchaseOrderItem> root = cq.from(PurchaseOrderItem.class);
			Join<PurchaseOrderItem, PurchaseOrder> po = root.join(PurchaseOrderItem_.order);
			Join<PurchaseOrder, Company> businessCompany = po.join("organisation");
			Join<PurchaseOrderItem, Subdiv> businessSubdiv = root.join(PurchaseOrderItem_.businessSubdiv, JoinType.LEFT);
			Join<PurchaseOrderItem, NominalCode> nominalCode = root.join(PurchaseOrderItem_.nominal, JoinType.LEFT);
			Join<PurchaseOrderItem, PurchaseOrderJobItem> pjobItems = root.join(PurchaseOrderItem_.linkedJobItems, JoinType.LEFT);
			Join<PurchaseOrderJobItem, JobItem> jobItems = pjobItems.join(PurchaseOrderJobItem_.ji, JoinType.LEFT);
			Join<JobItem, Job> job = jobItems.join(JobItem_.job);
			
			Expression<String> forSentence = cb.selectCase()
			.when(cb.equal(root.get(PurchaseOrderItem_.costType),CostType.CALIBRATION), "For Calibration of")
			.when(cb.equal(root.get(PurchaseOrderItem_.costType),CostType.CALIBRATION), "For Adjustment of")
			.when(cb.equal(root.get(PurchaseOrderItem_.costType),CostType.CALIBRATION), "For Repair of")
			.when(cb.equal(root.get(PurchaseOrderItem_.costType),CostType.CALIBRATION), "For Purchase of")
            .when(cb.equal(root.get(PurchaseOrderItem_.costType),CostType.CALIBRATION), "For Inspection of")
			.otherwise(" ").as(String.class);
			
			Expression<String> des = cb.concat(
					cb.concat(cb.concat(forSentence , " Item "),
					cb.concat(jobItems.get(JobItem_.itemNo).as(String.class), " ")),
					cb.concat( "on job ", job.get(Job_.jobno)));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.like(po.get(PurchaseOrder_.pono), poNo));
			clauses.getExpressions().add(cb.equal(businessCompany.get(Company_.coid), busId));
			cq.where(clauses);
			cq.select(cb.construct(RestPurchaseOrderItemDto.class, 
					nominalCode.get(NominalCode_.code),
					root.get(PurchaseOrderItem_.quantity),
					root.get(PurchaseOrderItem_.totalCost),
					root.get(PurchaseOrderItem_.generalDiscountRate),
					root.get(PurchaseOrderItem_.finalCost),
					cb.selectCase().when(cb.isNull(root.get(PurchaseOrderItem_.description)),des)
					.otherwise(root.get(PurchaseOrderItem_.description)),
					businessSubdiv.get(Subdiv_.analyticalCenter)
					));
			cq.distinct(true);
			return cq;
		});
	}
}
