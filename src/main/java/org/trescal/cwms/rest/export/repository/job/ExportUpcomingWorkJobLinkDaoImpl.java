package org.trescal.cwms.rest.export.repository.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLink;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLink_;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.job.ExportUpcomingWorkJobLinkDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportUpcomingWorkJobLinkDaoImpl extends RestExportAbstractDaoImpl<ExportUpcomingWorkJobLinkDTO> {

	private Selection<ExportUpcomingWorkJobLinkDTO> getSelection(CriteriaBuilder cb, Root<UpcomingWorkJobLink> root) {
		Join<UpcomingWorkJobLink, UpcomingWork> upcomingWork = root.join(UpcomingWorkJobLink_.upcomingWork); 
		Join<UpcomingWorkJobLink, Job> job = root.join(UpcomingWorkJobLink_.job);
		
		Selection<ExportUpcomingWorkJobLinkDTO> selection = cb.construct(ExportUpcomingWorkJobLinkDTO.class,
				root.get(UpcomingWorkJobLink_.id),
				root.get(UpcomingWorkJobLink_.active),
				upcomingWork.get(UpcomingWork_.id),
				job.get(Job_.jobid));
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportUpcomingWorkJobLinkDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportUpcomingWorkJobLinkDTO.class, cb -> cq -> {
			Root<UpcomingWorkJobLink> root = cq.from(UpcomingWorkJobLink.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(UpcomingWorkJobLink_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(UpcomingWorkJobLink_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.UPCOMING_WORK_JOB_LINK;
	}

	@Override
	public List<ExportUpcomingWorkJobLinkDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("No lastModified on UpcomingWorkJobLink");
	}	
}
