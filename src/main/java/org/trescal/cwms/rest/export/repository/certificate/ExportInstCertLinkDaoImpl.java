package org.trescal.cwms.rest.export.repository.certificate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.certificate.ExportInstCertLinkDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportInstCertLinkDaoImpl extends RestExportAbstractDaoImpl<ExportInstCertLinkDTO> {

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INST_CERT_LINK;
	}

	private Selection<ExportInstCertLinkDTO> getSelection(CriteriaBuilder cb, Root<InstCertLink> root) {
		
		// Technically nullable in database (currently), hence left join
		Join<InstCertLink, Certificate> certificate = root.join(InstCertLink_.cert, JoinType.LEFT);
		Join<InstCertLink, Instrument> instrument = root.join(InstCertLink_.inst, JoinType.LEFT);
		
		Selection<ExportInstCertLinkDTO> selection = cb.construct(ExportInstCertLinkDTO.class, 
			root.get(InstCertLink_.id),
			certificate.get(Certificate_.certid),
			instrument.get(Instrument_.plantid)
			);
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportInstCertLinkDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportInstCertLinkDTO.class, cb -> cq -> {
			Root<InstCertLink> root = cq.from(InstCertLink.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(InstCertLink_.id), id));
			}

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(InstCertLink_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public List<ExportInstCertLinkDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportInstCertLinkDTO> cq = cb.createQuery(ExportInstCertLinkDTO.class); 
			Root<InstCertLink> root = cq.from(InstCertLink.class);
			cq.where(cb.greaterThan(root.get(InstCertLink_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
	
}
