package org.trescal.cwms.rest.export.repository.instruction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink_;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.instruction.ExportSubdivInstructionLinkDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportSubdivInstructionLinkDaoImpl extends RestExportAbstractDaoImpl<ExportSubdivInstructionLinkDTO> {

	private Selection<ExportSubdivInstructionLinkDTO> getSelection(CriteriaBuilder cb, Root<SubdivInstructionLink> root) {
		
		Join<SubdivInstructionLink, Instruction> instruction = root.join(SubdivInstructionLink_.instruction);
		Join<SubdivInstructionLink, Subdiv> subdiv = root.join(SubdivInstructionLink_.subdiv);
		Join<SubdivInstructionLink, Company> allocatedCompany = root.join(SubdivInstructionLink_.ORGANISATION, JoinType.LEFT);
		Join<SubdivInstructionLink, Subdiv> businessSubdiv = root.join(SubdivInstructionLink_.businessSubdiv, JoinType.LEFT);
		
		// Integer id, Integer instructionid, Integer coid, Integer orgid,
		// Boolean issubdivinstruction, Integer businesssubdivid
		
		Selection<ExportSubdivInstructionLinkDTO> selection = cb.construct(ExportSubdivInstructionLinkDTO.class, 
				root.get(SubdivInstructionLink_.id),
				instruction.get(Instruction_.instructionid),
				subdiv.get(Subdiv_.subdivid),
				allocatedCompany.get(Company_.coid),
				root.get(SubdivInstructionLink_.subdivInstruction),
				businessSubdiv.get(Subdiv_.subdivid)
			); 
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportSubdivInstructionLinkDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportSubdivInstructionLinkDTO.class, cb -> cq -> {
			Root<SubdivInstructionLink> root = cq.from(SubdivInstructionLink.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(SubdivInstructionLink_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(SubdivInstructionLink_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.SUBDIV_INSTRUCTION_LINK;
	}
	
	@Override
	public List<ExportSubdivInstructionLinkDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportSubdivInstructionLinkDTO> cq = cb.createQuery(ExportSubdivInstructionLinkDTO.class); 
			Root<SubdivInstructionLink> root = cq.from(SubdivInstructionLink.class);
			cq.where(cb.greaterThan(root.get(SubdivInstructionLink_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}

}
