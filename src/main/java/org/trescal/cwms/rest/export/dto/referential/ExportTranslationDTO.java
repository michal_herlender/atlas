package org.trescal.cwms.rest.export.dto.referential;

import java.util.Locale;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportTranslationDTO {
	private Integer id;
	private Locale locale;
	private String translation;

	public ExportTranslationDTO(Integer id, Locale locale, String translation) {
		super();
		this.id = id;
		this.locale = locale;
		this.translation = translation;
	}
}
