package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportAddressDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportAddressDaoImpl extends RestExportAbstractDaoImpl<ExportAddressDTO> {

	private Selection<ExportAddressDTO> getSelection(CriteriaBuilder cb, Root<Address> root) {
		// TODO, address -> subdiv is currently nullable, should make not nullable in DB and fix few records
		Join<Address, Subdiv> subdiv = root.join(Address_.sub); 
		Join<Address, Country> country = root.join(Address_.country); 
		
		Selection<ExportAddressDTO> selection = cb.construct(ExportAddressDTO.class, 
				root.get(Address_.addrid),
				root.get(Address_.active),
				root.get(Address_.addr1),
				root.get(Address_.addr2),
				root.get(Address_.addr3),
				root.get(Address_.county),
				root.get(Address_.postcode),
				root.get(Address_.town),
				country.get(Country_.countryid),
				subdiv.get(Subdiv_.subdivid)
				);
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportAddressDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportAddressDTO.class, cb -> cq -> {
			Root<Address> root = cq.from(Address.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Address_.addrid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Address_.addrid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.ADDRESS;
	}

	@Override
	public List<ExportAddressDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportAddressDTO> cq = cb.createQuery(ExportAddressDTO.class); 
			Root<Address> root = cq.from(Address.class);
			cq.where(cb.greaterThan(root.get(Address_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
