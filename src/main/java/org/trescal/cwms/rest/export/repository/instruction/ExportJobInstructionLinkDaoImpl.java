package org.trescal.cwms.rest.export.repository.instruction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink_;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.instruction.ExportJobInstructionLinkDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportJobInstructionLinkDaoImpl extends RestExportAbstractDaoImpl<ExportJobInstructionLinkDTO> {

	private Selection<ExportJobInstructionLinkDTO> getSelection(CriteriaBuilder cb, Root<JobInstructionLink> root) {
		
		Join<JobInstructionLink, Instruction> instruction = root.join(JobInstructionLink_.instruction);
		Join<JobInstructionLink, Job> job = root.join(JobInstructionLink_.job);

		Selection<ExportJobInstructionLinkDTO> selection = cb.construct(ExportJobInstructionLinkDTO.class, 
				root.get(JobInstructionLink_.id),
				instruction.get(Instruction_.instructionid),
				job.get(Job_.jobid)
			);
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportJobInstructionLinkDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportJobInstructionLinkDTO.class, cb -> cq -> {
			Root<JobInstructionLink> root = cq.from(JobInstructionLink.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(JobInstructionLink_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(JobInstructionLink_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.JOB_INSTRUCTION_LINK;
	}

	@Override
	public List<ExportJobInstructionLinkDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportJobInstructionLinkDTO> cq = cb.createQuery(ExportJobInstructionLinkDTO.class); 
			Root<JobInstructionLink> root = cq.from(JobInstructionLink.class);
			cq.where(cb.greaterThan(root.get(JobInstructionLink_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
