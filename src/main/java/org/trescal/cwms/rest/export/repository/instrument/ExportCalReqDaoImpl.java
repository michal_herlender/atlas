package org.trescal.cwms.rest.export.repository.instrument;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq_;
import org.trescal.cwms.rest.export.dto.instrument.ExportCalReqDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractNativeDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportCalReqDaoImpl extends RestExportAbstractNativeDaoImpl<ExportCalReqDTO> {

	private static int INDEX_TYPE = 0;
	private static int INDEX_ID = 1;
	private static int INDEX_ACTIVE = 2;
	private static int INDEX_PUBLIC_TEXT = 3;
	private static int INDEX_PRIVATE_TEXT = 4;
	private static int INDEX_JOBITEMID = 5;
	private static int INDEX_PLANTID = 6;
	private static int INDEX_COMPID = 7;
	private static int INDEX_MODELID = 8;
	private static int INDEX_DESCID = 9;
	
	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.CAL_REQ;
	}
	
	@Override
	public List<ExportCalReqDTO> convert(List<Tuple> nativeResults) {
		List<ExportCalReqDTO> result = new ArrayList<>();
		for (Tuple tuple : nativeResults) {
			ExportCalReqDTO dto = new ExportCalReqDTO();
			
			String type = (String) tuple.get(INDEX_TYPE);
			Integer id = (Integer) tuple.get(INDEX_ID);
			Byte active = (Byte) tuple.get(INDEX_ACTIVE);
			String publicText = (String) tuple.get(INDEX_PUBLIC_TEXT);
			String privateText = (String) tuple.get(INDEX_PRIVATE_TEXT);
			Integer jobitemid = (Integer) tuple.get(INDEX_JOBITEMID);
			Integer plantid = (Integer) tuple.get(INDEX_PLANTID);
			Integer compid = (Integer) tuple.get(INDEX_COMPID);
			Integer modelid = (Integer) tuple.get(INDEX_MODELID);
			Integer descid = (Integer) tuple.get(INDEX_DESCID);

			dto.setType(type);
			dto.setActive(active == 1);
			dto.setId(id);
			dto.setPublicInstructions(publicText);
			dto.setPrivateInstructions(privateText);
			dto.setJobitemid(jobitemid);
			dto.setPlantid(plantid);
			dto.setCompid(compid);
			dto.setModelid(modelid);
			dto.setDescid(descid);
			
			result.add(dto);
		}
		return result;
	}

	@Override
	public Long getResultsCount(Integer id) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<CalReq> root = cq.from(CalReq.class);
			if (id != null) {
				cq.where(cb.equal(root.get(CalReq_.id), id));
			}
			
			cq.select(cb.count(root));
			return cq;
		});
	}
	
	@Override
	public String createSqlResults(Integer id, Integer startResultsFrom, Integer resultsPerPage) {
		if (startResultsFrom == null) throw new IllegalArgumentException("startResultsFrom must be specified");
		if (startResultsFrom < 0) throw new IllegalArgumentException("startResultsFrom must be >= 0");
		if (resultsPerPage == null) throw new IllegalArgumentException("resultsPerPage must be specified");
		if (resultsPerPage < 1) throw new IllegalArgumentException("resultsPerPage must be >= 1");
		
		StringBuffer result = new StringBuffer();
		result.append("SELECT [type],[id],[active],[publicinstructions],[instructions]");
		result.append(",[jobitemid],[plantid],[compid],[modelid],[descid]");
		result.append(" FROM [calreq]");
		if (id != null) {
			result.append(" WHERE [id]="+id.toString());
		}
		result.append(" ORDER BY [id]");
		result.append(" OFFSET "+startResultsFrom.toString()+" ROWS");
		result.append(" FETCH NEXT "+resultsPerPage.toString()+" ROWS ONLY");
		
		return result.toString();
		
	}
	
}
