package org.trescal.cwms.rest.export.repository.referential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.referential.ExportUoMDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportUoMDaoImpl extends RestExportAbstractDaoImpl<ExportUoMDTO> {

	private Selection<ExportUoMDTO> getSelection(CriteriaBuilder cb, Root<UoM> root) {
		Selection<ExportUoMDTO> selection = cb.construct(ExportUoMDTO.class, 
				root.get(UoM_.id),
				root.get(UoM_.name),
				root.get(UoM_.symbol),
				root.get(UoM_.tmlid),
				root.get(UoM_.active)
				);
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportUoMDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportUoMDTO.class, cb -> cq -> {
			Root<UoM> root = cq.from(UoM.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(UoM_.id), id));
			}

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(UoM_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.UNITS_OF_MEASURE;
	}

	@Override
	public List<ExportUoMDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportUoMDTO> cq = cb.createQuery(ExportUoMDTO.class); 
			Root<UoM> root = cq.from(UoM.class);
			cq.where(cb.greaterThan(root.get(UoM_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
