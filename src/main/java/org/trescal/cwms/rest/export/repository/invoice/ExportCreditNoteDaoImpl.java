package org.trescal.cwms.rest.export.repository.invoice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.invoice.ExportCreditNoteDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportCreditNoteDaoImpl extends RestExportAbstractDaoImpl<ExportCreditNoteDTO> {

	private Selection<ExportCreditNoteDTO> getSelection(CriteriaBuilder cb, Root<CreditNote> root) {
		Join<CreditNote, Invoice> invoice = root.join(CreditNote_.invoice, JoinType.LEFT);
		Join<CreditNote, Company> allocatedCompany = root.join(CreditNote_.organisation.getName(), JoinType.LEFT);
		
		return cb.construct(ExportCreditNoteDTO.class, 
			root.get(CreditNote_.id),	
			root.get(CreditNote_.issued),
			root.get(CreditNote_.issuedate),
			root.get(CreditNote_.finalCost),
			root.get(CreditNote_.totalCost),
			root.get(CreditNote_.vatValue),
			root.get(CreditNote_.creditNoteNo),
			invoice.get(Invoice_.id),
			allocatedCompany.get(Company_.coid));
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportCreditNoteDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportCreditNoteDTO.class, cb -> cq -> {
			Root<CreditNote> root = cq.from(CreditNote.class);
			if (id != null) {
				cq.where(cb.equal(root.get(CreditNote_.id), id));
			}
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(CreditNote_.id)));
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.CREDIT_NOTE;
	}

	@Override
	public List<ExportCreditNoteDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException();
	}

}
