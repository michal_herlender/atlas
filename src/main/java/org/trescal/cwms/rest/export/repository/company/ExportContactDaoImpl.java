package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportContactDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportContactDaoImpl extends RestExportAbstractDaoImpl<ExportContactDTO> {

	private Selection<ExportContactDTO> getSelection(CriteriaBuilder cb, Root<Contact> root) {
		
		// TODO add not-null constraint on contact -> subdiv and contact -> defaddress
		Join<Contact, Subdiv> subdiv = root.join(Contact_.sub); 
		Join<Contact, Address> address = root.join(Contact_.defAddress); 
		
		Selection<ExportContactDTO> selection = cb.construct(ExportContactDTO.class, 
				root.get(Contact_.personid),
				root.get(Contact_.active),
				root.get(Contact_.firstName),
				root.get(Contact_.lastName),
				root.get(Contact_.email),
				root.get(Contact_.telephone),
				root.get(Contact_.fax),
				root.get(Contact_.mobile),
				subdiv.get(Subdiv_.subdivid),
				address.get(Address_.addrid)
			);
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportContactDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportContactDTO.class, cb -> cq -> {
			Root<Contact> root = cq.from(Contact.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Contact_.personid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Contact_.personid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.CONTACT;
	}

	@Override
	public List<ExportContactDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportContactDTO> cq = cb.createQuery(ExportContactDTO.class); 
			Root<Contact> root = cq.from(Contact.class);
			cq.where(cb.greaterThan(root.get(Contact_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
