package org.trescal.cwms.rest.export.repository.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.job.ExportUpcomingWorkDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportUpcomingWorkDaoImpl extends RestExportAbstractDaoImpl<ExportUpcomingWorkDTO> {

/*
	public ExportUpcomingWorkDTO(Integer id, Boolean active, String description, Date dueDate, Date startDate,
			String title, Integer coid, Integer personid, Integer deptid, Integer respuserid, Integer addedbyid,
			Integer orgid) {
 */
	
	private Selection<ExportUpcomingWorkDTO> getSelection(CriteriaBuilder cb, Root<UpcomingWork> root) {
		Join<UpcomingWork, Company> clientCompany = root.join(UpcomingWork_.comp);
		Join<UpcomingWork, Contact> clientContact = root.join(UpcomingWork_.contact, JoinType.LEFT);
		Join<UpcomingWork, Department> businessDept = root.join(UpcomingWork_.department);
		Join<UpcomingWork, Contact> businessContactFor = root.join(UpcomingWork_.userResponsible);
		Join<UpcomingWork, Contact> businessContactAddedBy = root.join(UpcomingWork_.addedBy);
		Join<UpcomingWork, Subdiv> businessSubdiv = root.join(UpcomingWork_.ORGANISATION);
		
		Selection<ExportUpcomingWorkDTO> selection = cb.construct(ExportUpcomingWorkDTO.class,
			root.get(UpcomingWork_.id),
			root.get(UpcomingWork_.active),
			root.get(UpcomingWork_.description),
			root.get(UpcomingWork_.dueDate),
			root.get(UpcomingWork_.startDate),
			root.get(UpcomingWork_.title),
			clientCompany.get(Company_.coid),
			clientContact.get(Contact_.personid),
			businessDept.get(Department_.deptid),
			businessContactFor.get(Contact_.personid),
			businessContactAddedBy.get(Contact_.personid),
			businessSubdiv.get(Subdiv_.subdivid)
		);
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportUpcomingWorkDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportUpcomingWorkDTO.class, cb -> cq -> {
			Root<UpcomingWork> root = cq.from(UpcomingWork.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(UpcomingWork_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(UpcomingWork_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.UPCOMING_WORK;
	}
	
	@Override
	public List<ExportUpcomingWorkDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportUpcomingWorkDTO> cq = cb.createQuery(ExportUpcomingWorkDTO.class); 
			Root<UpcomingWork> root = cq.from(UpcomingWork.class);
			cq.where(cb.greaterThan(root.get(UpcomingWork_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
}