package org.trescal.cwms.rest.export.repository.certificate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.certificate.ExportCertLinkDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportCertLinkDaoImpl extends RestExportAbstractDaoImpl<ExportCertLinkDTO> {

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.CERT_LINK;
	}
	
	private Selection<ExportCertLinkDTO> getSelection(CriteriaBuilder cb, Root<CertLink> root) {
		
		// Technically nullable in database (currently), hence left join
		Join<CertLink, Certificate> certificate = root.join(CertLink_.cert, JoinType.LEFT);
		Join<CertLink, JobItem> jobItem = root.join(CertLink_.jobItem, JoinType.LEFT);
		
		Selection<ExportCertLinkDTO> selection = cb.construct(ExportCertLinkDTO.class, 
				root.get(CertLink_.LINK_ID),
				certificate.get(Certificate_.certid),
				jobItem.get(JobItem_.jobItemId)
			);
		
		return selection;
	}
		
	@Override
	public void getPagedResults(PagedResultSet<ExportCertLinkDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportCertLinkDTO.class, cb -> cq -> {
			Root<CertLink> root = cq.from(CertLink.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(CertLink_.linkId), id));
			}

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(CertLink_.linkId)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public List<ExportCertLinkDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportCertLinkDTO> cq = cb.createQuery(ExportCertLinkDTO.class); 
			Root<CertLink> root = cq.from(CertLink.class);
			cq.where(cb.greaterThan(root.get(CertLink_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
	
}
