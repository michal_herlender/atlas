package org.trescal.cwms.rest.export.dto.job;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportUpcomingWorkJobLinkDTO {
	private Integer id;
	private Boolean active;
	private Integer jobid;
	private Integer upcomingworkid;
	
	/**
	 * Constructor for JPA
	 */
	public ExportUpcomingWorkJobLinkDTO(Integer id, Boolean active, Integer jobid, Integer upcomingworkid) {
		super();
		this.id = id;
		this.active = active;
		this.jobid = jobid;
		this.upcomingworkid = upcomingworkid;
	}	
}
