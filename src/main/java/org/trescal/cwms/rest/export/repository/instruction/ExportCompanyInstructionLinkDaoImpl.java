package org.trescal.cwms.rest.export.repository.instruction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.instruction.ExportCompanyInstructionLinkDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportCompanyInstructionLinkDaoImpl extends RestExportAbstractDaoImpl<ExportCompanyInstructionLinkDTO> {

	private Selection<ExportCompanyInstructionLinkDTO> getSelection(CriteriaBuilder cb, Root<CompanyInstructionLink> root) {
		
		Join<CompanyInstructionLink, Instruction> instruction = root.join(CompanyInstructionLink_.instruction);
		Join<CompanyInstructionLink, Company> company = root.join(CompanyInstructionLink_.company);
		Join<CompanyInstructionLink, Company> allocatedCompany = root.join(CompanyInstructionLink_.ORGANISATION, JoinType.LEFT);
		Join<CompanyInstructionLink, Subdiv> businessSubdiv = root.join(CompanyInstructionLink_.businessSubdiv, JoinType.LEFT);
		
		// Integer id, Integer instructionid, Integer coid, Integer orgid,
		// Boolean issubdivinstruction, Integer businesssubdivid
		
		Selection<ExportCompanyInstructionLinkDTO> selection = cb.construct(ExportCompanyInstructionLinkDTO.class, 
				root.get(CompanyInstructionLink_.id),
				instruction.get(Instruction_.instructionid),
				company.get(Company_.coid),
				allocatedCompany.get(Company_.coid),
				root.get(CompanyInstructionLink_.subdivInstruction),
				businessSubdiv.get(Subdiv_.subdivid)
			); 
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportCompanyInstructionLinkDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportCompanyInstructionLinkDTO.class, cb -> cq -> {
			Root<CompanyInstructionLink> root = cq.from(CompanyInstructionLink.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(CompanyInstructionLink_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(CompanyInstructionLink_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.COMPANY_INSTRUCTION_LINK;
	}
	
	@Override
	public List<ExportCompanyInstructionLinkDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportCompanyInstructionLinkDTO> cq = cb.createQuery(ExportCompanyInstructionLinkDTO.class); 
			Root<CompanyInstructionLink> root = cq.from(CompanyInstructionLink.class);
			cq.where(cb.greaterThan(root.get(CompanyInstructionLink_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
}
