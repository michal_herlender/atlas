package org.trescal.cwms.rest.export.repository.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.job.ExportJobDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportJobDaoImpl extends RestExportAbstractDaoImpl<ExportJobDTO> {

	private Selection<ExportJobDTO> getSelection(CriteriaBuilder cb, Root<Job> root) {
		// TODO add not-null to statusid (even make enum), returnto
		
		Join<Job, Contact> contact = root.join(Job_.con);
		Join<Job, JobStatus> status = root.join(Job_.js, JoinType.LEFT);
		Join<Job, Address> returnto = root.join(Job_.returnTo, JoinType.LEFT);
		Join<Job, Subdiv> org = root.join(Job_.ORGANISATION);
		
		Selection<ExportJobDTO> selection = cb.construct(ExportJobDTO.class, 
				root.get(Job_.jobid),
				root.get(Job_.clientRef),
				root.get(Job_.jobno),
				root.get(Job_.regDate),
				root.get(Job_.dateComplete),
				contact.get(Contact_.personid),
				status.get(JobStatus_.statusid),
				returnto.get(Address_.addrid),
				root.get(Job_.type),
				org.get(Subdiv_.subdivid));
				
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportJobDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportJobDTO.class, cb -> cq -> {
			Root<Job> root = cq.from(Job.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Job_.jobid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Job_.jobid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.JOB;
	}
	
	@Override
	public List<ExportJobDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportJobDTO> cq = cb.createQuery(ExportJobDTO.class); 
			Root<Job> root = cq.from(Job.class);
			cq.where(cb.greaterThan(root.get(Job_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}

}