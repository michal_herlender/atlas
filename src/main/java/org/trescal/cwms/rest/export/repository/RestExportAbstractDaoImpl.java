package org.trescal.cwms.rest.export.repository;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.AbstractDaoImpl;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;

public abstract class RestExportAbstractDaoImpl<ExportDTO> extends AbstractDaoImpl implements RestExportAbstractDao<ExportDTO> {

	@Override
	public RestPagedResultDTO<ExportDTO> convert(PagedResultSet<ExportDTO> prs) {
		RestPagedResultDTO<ExportDTO> result = new RestPagedResultDTO<>(true, "", prs.getResultsPerPage(), prs.getCurrentPage());
		result.addResults(prs.getResults());
		result.setResultsCount(prs.getResultsCount());
		return result;
	}
	
	@Override
	public RestPagedResultDTO<ExportDTO> getRestPagedResults(Integer resultsPerPage, Integer currentPage, Integer id, List<Locale> locales) {
		PagedResultSet<ExportDTO> prs = new PagedResultSet<>(resultsPerPage, currentPage);
		this.getPagedResults(prs, id, locales);
		return this.convert(prs);
	}
	
}
