package org.trescal.cwms.rest.export.dto.instruction;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportSubdivInstructionLinkDTO extends ExportInstructionLinkDTO {
	private Integer subdivid;
	private Integer orgid;
	private Boolean issubdivinstruction;
	private Integer businesssubdivid;

	/**
	 * Constructor used in JPA
	 */
	public ExportSubdivInstructionLinkDTO(Integer id, Integer instructionid, Integer subdivid, Integer orgid,
			Boolean issubdivinstruction, Integer businesssubdivid) {
		super(id, instructionid);
		this.subdivid = subdivid;
		this.orgid = orgid;
		this.issubdivinstruction = issubdivinstruction;
		this.businesssubdivid = businesssubdivid;
	}
}
