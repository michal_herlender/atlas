package org.trescal.cwms.rest.export.dto.job;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;

import java.time.LocalDate;

@Getter
@Setter
public class ExportEngineerAllocationDTO {
    private Integer id;
    private Boolean active;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate allocatedFor;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate allocatedOn;
    private TimeOfDay timeofday;
    private Integer allocatedbyid;
    private Integer allocatedtoid;
    private Integer jobitemid;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate allocateduntil;
    private TimeOfDay timeofdayuntil;
    private Integer deptid;

    public ExportEngineerAllocationDTO(Integer id, Boolean active, LocalDate allocatedFor, LocalDate allocatedOn,
                                       TimeOfDay timeofday, Integer allocatedbyid, Integer allocatedtoid, Integer jobitemid, LocalDate allocateduntil,
                                       TimeOfDay timeofdayuntil, Integer deptid) {
        super();
        this.id = id;
        this.active = active;
        this.allocatedFor = allocatedFor;
        this.allocatedOn = allocatedOn;
        this.timeofday = timeofday;
        this.allocatedbyid = allocatedbyid;
        this.allocatedtoid = allocatedtoid;
        this.jobitemid = jobitemid;
        this.allocateduntil = allocateduntil;
        this.timeofdayuntil = timeofdayuntil;
        this.deptid = deptid;
    }
}
