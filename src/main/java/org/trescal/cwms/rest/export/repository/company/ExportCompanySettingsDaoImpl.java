package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportCompanySettingsDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

/**
 * Export DAO class for CompanySettingsForAllocatedCompany
 * @author galen
 *
 */
@Repository
public class ExportCompanySettingsDaoImpl extends RestExportAbstractDaoImpl<ExportCompanySettingsDTO> {

	private Selection<ExportCompanySettingsDTO> getSelection(CriteriaBuilder cb, Root<CompanySettingsForAllocatedCompany> root) {
		Join<CompanySettingsForAllocatedCompany, Company> allocatedCompany = root.join(CompanySettingsForAllocatedCompany_.ORGANISATION);
		Join<CompanySettingsForAllocatedCompany, Company> company = root.join(CompanySettingsForAllocatedCompany_.company);
		
		Selection<ExportCompanySettingsDTO> selection = cb.construct(ExportCompanySettingsDTO.class, 
				root.get(CompanySettingsForAllocatedCompany_.id),
				allocatedCompany.get(Company_.coid),
				company.get(Company_.coid),
				root.get(CompanySettingsForAllocatedCompany_.active),
				root.get(CompanySettingsForAllocatedCompany_.onStop),
				root.get(CompanySettingsForAllocatedCompany_.status)
				);
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportCompanySettingsDTO> prs, Integer id,
			List<Locale> locales) {
		completePagedResultSet(prs, ExportCompanySettingsDTO.class, cb -> cq -> {
			Root<CompanySettingsForAllocatedCompany> root = cq.from(CompanySettingsForAllocatedCompany.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(CompanySettingsForAllocatedCompany_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(CompanySettingsForAllocatedCompany_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.COMPANY_SETTINGS;
	}
	
	@Override
	public List<ExportCompanySettingsDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportCompanySettingsDTO> cq = cb.createQuery(ExportCompanySettingsDTO.class); 
			Root<CompanySettingsForAllocatedCompany> root = cq.from(CompanySettingsForAllocatedCompany.class);
			cq.where(cb.greaterThan(root.get(CompanySettingsForAllocatedCompany_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
}
