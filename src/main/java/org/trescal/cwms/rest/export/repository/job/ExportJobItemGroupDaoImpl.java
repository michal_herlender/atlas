/**
 * 
 */
package org.trescal.cwms.rest.export.repository.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.job.ExportJobItemGroupDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

/**
 * @author shazil.khan
 *
 */
@Repository
public class ExportJobItemGroupDaoImpl extends RestExportAbstractDaoImpl<ExportJobItemGroupDTO> {
	
	private Selection<ExportJobItemGroupDTO> getSelection(CriteriaBuilder cb, Root<JobItemGroup> root) {
		
		Join<JobItemGroup, Job> job = root.join(JobItemGroup_.job,JoinType.LEFT);

		return cb.construct(ExportJobItemGroupDTO.class,
			root.get(JobItemGroup_.id),
			root.get(JobItemGroup_.calibrationGroup),
			root.get(JobItemGroup_.deliveryGroup),
			job.get(Job_.jobid));
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportJobItemGroupDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportJobItemGroupDTO.class, cb -> cq -> {
			Root<JobItemGroup> root = cq.from(JobItemGroup.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(JobItemGroup_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(JobItemGroup_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.JOB_ITEM_GROUP;
	}

	@Override
	public List<ExportJobItemGroupDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportJobItemGroupDTO> cq = cb.createQuery(ExportJobItemGroupDTO.class); 
			Root<JobItemGroup> root = cq.from(JobItemGroup.class);
			cq.where(cb.greaterThan(root.get(JobItemGroup_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
	
}
