package org.trescal.cwms.rest.export.dto.company;

import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportDepartmentDTO {
	private Integer deptid;
	private String name;
	private Integer addressid;
	private Integer subdivid;
	private DepartmentType type;
	private String shortName;

	// JPA constructor
	public ExportDepartmentDTO(Integer deptid, String name, Integer addressid, Integer subdivid, DepartmentType type,
			String shortName) {
		super();
		this.deptid = deptid;
		this.name = name;
		this.addressid = addressid;
		this.subdivid = subdivid;
		this.type = type;
		this.shortName = shortName;
	}
}
