package org.trescal.cwms.rest.export.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.export.service.RestExportEntityType;
import org.trescal.cwms.rest.export.service.RestExportService;

import java.util.Locale;

@Controller
@RestJsonController
public class RestExportController {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestExportService restExportService;

	@RequestMapping(path = "/api/export/{entityType}", method = RequestMethod.GET,
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public @ResponseBody
	RestPagedResultDTO<?> handleRequest(
		@PathVariable(name = "entityType") String pathVariable,
		@RequestParam(name = "id", required = false) Integer id,
		@RequestParam(name = "locale", required = false) String[] locales,
		@RequestParam(name = "currentPage", required = false, defaultValue = "1") Integer currentPage,
		@RequestParam(name = "resultsPerPage", required = false, defaultValue = "100") Integer resultsPerPage) {
		RestPagedResultDTO<?> result;
		RestExportEntityType entityType = RestExportEntityType.resolveEntityType(pathVariable);
		if (entityType == null) {
			Locale locale = LocaleContextHolder.getLocale();
			String message = this.messageSource.getMessage(null, new Object[]{pathVariable}, RestExportMessageDefinitions.ERROR_INVALID_PATH, locale);
			result = new RestPagedResultDTO<>(false, message, currentPage, resultsPerPage);
		} else {
			result = this.restExportService.getResults(entityType, id, locales, resultsPerPage, currentPage);
		}

		return result;
	}
}
