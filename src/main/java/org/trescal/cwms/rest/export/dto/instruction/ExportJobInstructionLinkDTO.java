package org.trescal.cwms.rest.export.dto.instruction;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportJobInstructionLinkDTO extends ExportInstructionLinkDTO {
	private Integer jobid;

	/**
	 * Constructor used in JPA
	 */
	public ExportJobInstructionLinkDTO(Integer id, Integer instructionid, Integer jobid) {
		super(id, instructionid);
		this.jobid = jobid;
	}
}
