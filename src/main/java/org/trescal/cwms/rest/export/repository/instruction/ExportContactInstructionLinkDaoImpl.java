package org.trescal.cwms.rest.export.repository.instruction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink_;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.instruction.ExportContactInstructionLinkDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportContactInstructionLinkDaoImpl extends RestExportAbstractDaoImpl<ExportContactInstructionLinkDTO> {

	private Selection<ExportContactInstructionLinkDTO> getSelection(CriteriaBuilder cb, Root<ContactInstructionLink> root) {
		
		Join<ContactInstructionLink, Instruction> instruction = root.join(ContactInstructionLink_.instruction);
		Join<ContactInstructionLink, Contact> contact = root.join(ContactInstructionLink_.contact);
		Join<ContactInstructionLink, Company> allocatedCompany = root.join(ContactInstructionLink_.ORGANISATION, JoinType.LEFT);
		
		// Integer id, Integer instructionid, Integer personid, Integer orgid,
		
		Selection<ExportContactInstructionLinkDTO> selection = cb.construct(ExportContactInstructionLinkDTO.class, 
				root.get(ContactInstructionLink_.id),
				instruction.get(Instruction_.instructionid),
				contact.get(Contact_.personid),
				allocatedCompany.get(Company_.coid)
			); 
		
		return selection;
	}	
	
	@Override
	public void getPagedResults(PagedResultSet<ExportContactInstructionLinkDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportContactInstructionLinkDTO.class, cb -> cq -> {
			Root<ContactInstructionLink> root = cq.from(ContactInstructionLink.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(ContactInstructionLink_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(ContactInstructionLink_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.CONTACT_INSTRUCTION_LINK;
	}

	@Override
	public List<ExportContactInstructionLinkDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportContactInstructionLinkDTO> cq = cb.createQuery(ExportContactInstructionLinkDTO.class); 
			Root<ContactInstructionLink> root = cq.from(ContactInstructionLink.class);
			cq.where(cb.greaterThan(root.get(ContactInstructionLink_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
