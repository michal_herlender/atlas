package org.trescal.cwms.rest.export.repository.workinstruction;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.Tuple;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.rest.export.dto.workinstruction.ExportInstrumentWorkInstructionDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractNativeDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

/**
 * The table is a ManyToMany relationship so there is no Hibernate entity currently; using native SQL
 *
 */
@Repository
public class ExportInstrumentWorkInstructionDaoImpl extends RestExportAbstractNativeDaoImpl<ExportInstrumentWorkInstructionDTO> {

	private static int INDEX_WORK_INSTUCTION_ID = 0;
	private static int INDEX_PLANT_ID = 1;
	
	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INSTRUMENT_WORK_INSTUCTION;
	}

	@Override
	public Long getResultsCount(Integer id) {
		StringBuffer sqlResultsCount = new StringBuffer();
		sqlResultsCount.append("SELECT COUNT(1) FROM [instrumentworkinstruction]");
		if (id != null) {
			sqlResultsCount.append(" WHERE [plantId] = ");
			sqlResultsCount.append(id.toString());
		}
		
		Query resultsQuery = super.getEntityManager().createNativeQuery(sqlResultsCount.toString());
		Integer result = (Integer) resultsQuery.getSingleResult();
		return result.longValue();
	}

	@Override
	public List<ExportInstrumentWorkInstructionDTO> convert(List<Tuple> nativeResults) {
		List<ExportInstrumentWorkInstructionDTO> result = new ArrayList<>();
		for (Tuple tuple : nativeResults) {
			ExportInstrumentWorkInstructionDTO dto = new ExportInstrumentWorkInstructionDTO();
			
			Integer plantId = (Integer) tuple.get(INDEX_PLANT_ID);
			Integer workInstructionId = (Integer) tuple.get(INDEX_WORK_INSTUCTION_ID);

			dto.setPlantId(plantId);
			dto.setWorkInstructionId(workInstructionId);
			
			result.add(dto);
		}
		return result;
	}

	@Override
	public String createSqlResults(Integer id, Integer startResultsFrom, Integer resultsPerPage) {
		if (startResultsFrom == null) throw new IllegalArgumentException("startResultsFrom must be specified");
		if (startResultsFrom < 0) throw new IllegalArgumentException("startResultsFrom must be >= 0");
		if (resultsPerPage == null) throw new IllegalArgumentException("resultsPerPage must be specified");
		if (resultsPerPage < 1) throw new IllegalArgumentException("resultsPerPage must be >= 1");
		
		StringBuffer result = new StringBuffer();
		result.append("SELECT [workInstructionId],[plantId]");
		result.append(" FROM [instrumentworkinstruction]");
		if (id != null) {
			result.append(" WHERE [plantId]="+id.toString());
		}
		result.append(" ORDER BY [plantId],[workInstructionId]");
		result.append(" OFFSET "+startResultsFrom.toString()+" ROWS");
		result.append(" FETCH NEXT "+resultsPerPage.toString()+" ROWS ONLY");
		
		return result.toString();
	}

}
