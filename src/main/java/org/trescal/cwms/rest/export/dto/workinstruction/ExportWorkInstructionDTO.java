package org.trescal.cwms.rest.export.dto.workinstruction;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportWorkInstructionDTO {
	private Integer id;
	private String name;
	private String title;
	private Integer descid;
	private Integer orgid;
	
	// Constructor for JPA
	public ExportWorkInstructionDTO(Integer id, String name, String title, Integer descid, Integer orgid) {
		super();
		this.id = id;
		this.name = name;
		this.title = title;
		this.descid = descid;
		this.orgid = orgid;
	}	
}