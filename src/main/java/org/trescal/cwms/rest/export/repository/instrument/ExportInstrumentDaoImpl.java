package org.trescal.cwms.rest.export.repository.instrument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.Location_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.instrument.ExportInstrumentDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportInstrumentDaoImpl extends RestExportAbstractDaoImpl<ExportInstrumentDTO> {

	private Selection<ExportInstrumentDTO> getSelection(CriteriaBuilder cb, Root<Instrument> root) {
		// TODO - add not null to address, contact relationships (for now, adding left join)
		
		Join<Instrument, Address> address = root.join(Instrument_.add, JoinType.LEFT);
		Join<Instrument, Company> company = root.join(Instrument_.comp);
		Join<Instrument, Contact> contact = root.join(Instrument_.con, JoinType.LEFT);
		Join<Instrument, Location> location = root.join(Instrument_.loc, JoinType.LEFT);
		Join<Instrument, Mfr> mfr = root.join(Instrument_.mfr, JoinType.LEFT);
		Join<Instrument, InstrumentModel> instmodel = root.join(Instrument_.model, JoinType.LEFT);
		Join<Instrument, ServiceType> servicetype = root.join(Instrument_.defaultServiceType, JoinType.LEFT);
		Join<Instrument, InstrumentUsageType> usage = root.join(Instrument_.usageType, JoinType.LEFT);
		
		
		Selection<ExportInstrumentDTO> selection = cb.construct(ExportInstrumentDTO.class, 
				root.get(Instrument_.plantid),	
				root.get(Instrument_.calFrequency),	
				root.get(Instrument_.calFrequencyUnit),	
				root.get(Instrument_.calibrationStandard),	
				root.get(Instrument_.nextCalDueDate),	
				root.get(Instrument_.plantno),	
				root.get(Instrument_.serialno),	
				root.get(Instrument_.status),	
				root.get(Instrument_.customerDescription),	
				root.get(Instrument_.customerSpecification),
				root.get(Instrument_.freeTextLocation),
				root.get(Instrument_.clientBarcode),
				root.get(Instrument_.formerBarCode),	
				root.get(Instrument_.modelname),	
				address.get(Address_.addrid),
				company.get(Company_.coid),
				contact.get(Contact_.personid),
				location.get(Location_.locationid),
				mfr.get(Mfr_.mfrid),
				instmodel.get(InstrumentModel_.modelid),
				servicetype.get(ServiceType_.serviceTypeId),
				usage.get(InstrumentUsageType_.id),
				root.get(Instrument_.customerManaged)
			);
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportInstrumentDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportInstrumentDTO.class, cb -> cq -> {
			Root<Instrument> root = cq.from(Instrument.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Instrument_.plantid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Instrument_.plantid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INSTRUMENT;
	}
	
	@Override
	public List<ExportInstrumentDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportInstrumentDTO> cq = cb.createQuery(ExportInstrumentDTO.class); 
			Root<Instrument> root = cq.from(Instrument.class);
			cq.where(cb.greaterThan(root.get(Instrument_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}

}
