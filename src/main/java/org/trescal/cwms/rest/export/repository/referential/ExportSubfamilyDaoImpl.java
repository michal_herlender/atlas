package org.trescal.cwms.rest.export.repository.referential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.referential.ExportSubFamilyDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportSubfamilyDaoImpl extends RestExportAbstractDaoImpl<ExportSubFamilyDTO> {

	private Selection<ExportSubFamilyDTO> getSelection(CriteriaBuilder cb, Root<Description> root) {
		
		Join<Description, InstrumentModelFamily> family = root.join(Description_.family, JoinType.INNER);

		Selection<ExportSubFamilyDTO> selection = cb.construct(ExportSubFamilyDTO.class, 
				root.get(Description_.id),
				root.get(Description_.description),
				root.get(Description_.tmlid),
				family.get(InstrumentModelFamily_.familyid),
				root.get(Description_.active)
				);
		
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportSubFamilyDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportSubFamilyDTO.class, cb -> cq -> {
			Root<Description> root = cq.from(Description.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Description_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Description_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.SUB_FAMILY;
	}

	@Override
	public List<ExportSubFamilyDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportSubFamilyDTO> cq = cb.createQuery(ExportSubFamilyDTO.class); 
			Root<Description> root = cq.from(Description.class);
			cq.where(cb.greaterThan(root.get(Description_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
