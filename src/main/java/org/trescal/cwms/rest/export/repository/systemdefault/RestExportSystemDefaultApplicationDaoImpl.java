package org.trescal.cwms.rest.export.repository.systemdefault;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault_;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.systemdefault.ExportSystemDefaultApplicationDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class RestExportSystemDefaultApplicationDaoImpl extends RestExportAbstractDaoImpl<ExportSystemDefaultApplicationDTO> {

	private Selection<ExportSystemDefaultApplicationDTO> getSelection(CriteriaBuilder cb, Root<SystemDefaultApplication> root) {
		
		Join<SystemDefaultApplication, SystemDefault> systemDefault = root.join(SystemDefaultApplication_.systemDefault, JoinType.INNER);
		Join<SystemDefaultApplication, Company> company = root.join(SystemDefaultApplication_.company, JoinType.LEFT);
		Join<SystemDefaultApplication, Subdiv> subdiv = root.join(SystemDefaultApplication_.subdiv, JoinType.LEFT);
		Join<SystemDefaultApplication, Contact> contact = root.join(SystemDefaultApplication_.contact, JoinType.LEFT);
		Join<SystemDefaultApplication, Company> allocatedCompany = root.join(SystemDefaultApplication_.ORGANISATION, JoinType.LEFT);
		
		Selection<ExportSystemDefaultApplicationDTO> selection = cb.construct(ExportSystemDefaultApplicationDTO.class, 
				root.get(SystemDefaultApplication_.id),
				systemDefault.get(SystemDefault_.id),
				root.get(SystemDefaultApplication_.value),
				company.get(Company_.coid),
				subdiv.get(Subdiv_.subdivid),
				contact.get(Contact_.personid),
				allocatedCompany.get(Company_.coid)
			);
		
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportSystemDefaultApplicationDTO> prs, Integer id,
			List<Locale> locales) {
		completePagedResultSet(prs, ExportSystemDefaultApplicationDTO.class, cb -> cq -> {
			Root<SystemDefaultApplication> root = cq.from(SystemDefaultApplication.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(SystemDefaultApplication_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(SystemDefaultApplication_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.SYSTEM_DEFAULT_APPLICATION;
	}

	@Override
	public List<ExportSystemDefaultApplicationDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportSystemDefaultApplicationDTO> cq = cb.createQuery(ExportSystemDefaultApplicationDTO.class); 
			Root<SystemDefaultApplication> root = cq.from(SystemDefaultApplication.class);
			cq.where(cb.greaterThan(root.get(SystemDefaultApplication_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
