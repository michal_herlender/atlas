package org.trescal.cwms.rest.export.dto.instruction;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportContactInstructionLinkDTO extends ExportInstructionLinkDTO {
	private Integer personid;
	private Integer orgid;
	
	/**
	 * Used in JPA
	 */
	public ExportContactInstructionLinkDTO(Integer id, Integer instructionid, Integer personid, Integer orgid) {
		super(id, instructionid);
		this.personid = personid;
		this.orgid = orgid;
	}
}
