package org.trescal.cwms.rest.export.dto.instruction;

import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportBaseInstructionDTO {

	private Integer instructionid;
	private String instruction;
	private InstructionType instructiontype;
	
	/**
	 * Constructor for JPA
	 */
	public ExportBaseInstructionDTO(Integer instructionid, String instruction, InstructionType instructiontype) {
		super();
		this.instructionid = instructionid;
		this.instruction = instruction;
		this.instructiontype = instructiontype;
	}
}
