package org.trescal.cwms.rest.export.dto.instrument;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportCalReqDTO {
	private String type;
	private Integer id;
	private Boolean active;
	private String publicInstructions;
	private String privateInstructions;
	private Integer jobitemid;
	private Integer plantid;
	private Integer compid;
	private Integer modelid;
	private Integer descid;
}
