package org.trescal.cwms.rest.export.dto.referential;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportUoMDTO {
	private Integer id;
	private String name;
	private String symbol;
	private Integer tmlid;
	private Boolean active;

	/**
	 * Used by JPA
	 */
	public ExportUoMDTO(Integer id, String name, String symbol, Integer tmlid, Boolean active) {
		super();
		this.id = id;
		this.name = name;
		this.symbol = symbol;
		this.tmlid = tmlid;
		this.active = active;
	}
}
