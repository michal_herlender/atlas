package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportDepartmentMemberDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportDepartmentMemberDaoImpl extends RestExportAbstractDaoImpl<ExportDepartmentMemberDTO> {

	// Integer id, Integer personid, Integer deptid
	
	private Selection<ExportDepartmentMemberDTO> getSelection(CriteriaBuilder cb, Root<DepartmentMember> root) {
		
		Join<DepartmentMember, Department> department = root.join(DepartmentMember_.department);
		Join<DepartmentMember, Contact> contact = root.join(DepartmentMember_.contact);
		
		Selection<ExportDepartmentMemberDTO> selection = cb.construct(ExportDepartmentMemberDTO.class, 
				root.get(DepartmentMember_.id),
				contact.get(Contact_.personid),
				department.get(Department_.deptid)
			);
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportDepartmentMemberDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportDepartmentMemberDTO.class, cb -> cq -> {
			Root<DepartmentMember> root = cq.from(DepartmentMember.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(DepartmentMember_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(DepartmentMember_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.DEPARTMENT_MEMBER;
	}

	@Override
	public List<ExportDepartmentMemberDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportDepartmentMemberDTO> cq = cb.createQuery(ExportDepartmentMemberDTO.class); 
			Root<DepartmentMember> root = cq.from(DepartmentMember.class);
			cq.where(cb.greaterThan(root.get(DepartmentMember_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
	
}
