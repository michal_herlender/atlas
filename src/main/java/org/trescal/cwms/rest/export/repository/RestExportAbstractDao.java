package org.trescal.cwms.rest.export.repository;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

public interface RestExportAbstractDao<ExportDTO> {
	
	/**
	 * Implemented in RestExportAbstractDaoImpl
	 */
	RestPagedResultDTO<ExportDTO> convert(PagedResultSet<ExportDTO> prs);
	
	/**
	 * Implemented in RestExportAbstractDaoImpl
	 * 
	 * Integer resultsPerPage : Optional size of results, otherwise default of 100 used
	 * Integer currentPage : Optional page of results, otherwise default of 1 used
	 * 
	 * Integer id : Optional id of record (note multiple results could be returned for translations) 
	 * List<Locale> locales : Optionally supported by some implementations to query results based on specific locales
	 */
	RestPagedResultDTO<ExportDTO> getRestPagedResults(Integer resultsPerPage, Integer currentPage, Integer id, List<Locale> locales);
	
	/**
	 * Implemented per subclass  
	 * 
	 * Integer id : Optional id of record (note multiple results could be returned for translations) 
	 * List<Locale> locales : Optionally supported by some implementations to query results based on specific locales
	 */
	void getPagedResults(PagedResultSet<ExportDTO> prs, Integer id, List<Locale> locales);
	
	/**
	 * Implemented per subclass  
	 */
	RestExportEntityType getEntityType();
	
	/**
	 * Optionally implemented per subclass to return results changed since lastModified
	 * Provides a rudimentary way to achieve some event data recovery if CDC accidentally disabled / deleted
	 * 
	 * Date lastModifiedAfter - the desired date after which results should be returned (still a java.util.Date)
	 */
	List<ExportDTO> getLastModifiedResults(Date lastModifiedAfter);
}
