package org.trescal.cwms.rest.export.repository.referential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.referential.ExportBrandDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportBrandDaoImpl extends RestExportAbstractDaoImpl<ExportBrandDTO> {
	
	private Selection<ExportBrandDTO> getSelection(CriteriaBuilder cb, Root<Mfr> root) {
		Selection<ExportBrandDTO> selection = cb.construct(ExportBrandDTO.class, 
				root.get(Mfr_.mfrid),	
				root.get(Mfr_.active),
				root.get(Mfr_.genericMfr),
				root.get(Mfr_.name),
				root.get(Mfr_.tmlid));
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportBrandDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportBrandDTO.class, cb -> cq -> {
			Root<Mfr> root = cq.from(Mfr.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Mfr_.mfrid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Mfr_.mfrid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.BRAND;
	}

	@Override
	public List<ExportBrandDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportBrandDTO> cq = cb.createQuery(ExportBrandDTO.class); 
			Root<Mfr> root = cq.from(Mfr.class);
			cq.where(cb.greaterThan(root.get(Mfr_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
}
