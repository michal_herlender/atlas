package org.trescal.cwms.rest.export.dto.job;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportUpcomingWorkDTO {
	private Integer id;
	private Boolean active;
	private String description;
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate dueDate;
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate startDate;
	private String title;
	private Integer coid;		// of client
	private Integer personid;	// of client
	private Integer deptid;		// of business subdiv
	private Integer respuserid;	// of business subdiv (contact)
	private Integer addedbyid;	// of business subdiv (contact)
	private Integer orgid;		// of business subdiv (subdiv)

	/**
	 * Constructor for JPA
	 */
	public ExportUpcomingWorkDTO(Integer id, Boolean active, String description, LocalDate dueDate, LocalDate startDate,
			String title, Integer coid, Integer personid, Integer deptid, Integer respuserid, Integer addedbyid,
			Integer orgid) {
		super();
		this.id = id;
		this.active = active;
		this.description = description;
		this.dueDate = dueDate;
		this.startDate = startDate;
		this.title = title;
		this.coid = coid;
		this.personid = personid;
		this.deptid = deptid;
		this.respuserid = respuserid;
		this.addedbyid = addedbyid;
		this.orgid = orgid;
	}	
}
