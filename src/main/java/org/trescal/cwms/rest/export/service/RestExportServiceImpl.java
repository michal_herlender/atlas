package org.trescal.cwms.rest.export.service;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDao;

import java.util.*;

@Service
public class RestExportServiceImpl implements RestExportService, InitializingBean {
	@Autowired
	private List<RestExportAbstractDao<?>> daoBeanList;
	// Initialized after autowiring
	private Map<RestExportEntityType, RestExportAbstractDao<?>> daoBeanMap;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.daoBeanMap = new HashMap<>();
		for (RestExportAbstractDao<?> daoBean : daoBeanList) {
			this.daoBeanMap.put(daoBean.getEntityType(), daoBean);
		}
	}

	@Override
	public RestPagedResultDTO<?> getResults(RestExportEntityType entityType, Integer id, String[] locales,
											Integer resultsPerPage, Integer currentPage) {
		if (entityType == null) throw new IllegalArgumentException("entityType must not be null");
		return getResultsPaged(entityType, id, locales, resultsPerPage, currentPage);
	}


	public RestPagedResultDTO<?> getResultsPaged(RestExportEntityType entityType, Integer id, String[] localeTextArray,
												 Integer resultsPerPage, Integer currentPage) {
		if (entityType == null) throw new IllegalArgumentException("entityType must not be null!");
		List<Locale> localeList = getLocaleList(localeTextArray);

		RestPagedResultDTO<?> result;
		RestExportAbstractDao<?> daoBean = this.daoBeanMap.get(entityType);
		if (daoBean == null)
			throw new UnsupportedOperationException("Unsupported entity type : " + entityType.toString());
		result = daoBean.getRestPagedResults(resultsPerPage, currentPage, id, localeList);
		return result;
	}
	
	public List<?> getLastModifiedResults(RestExportEntityType entityType, Date lastModifiedAfter) {
		if (entityType == null) throw new IllegalArgumentException("entityType must not be null!");
		RestExportAbstractDao<?> daoBean = this.daoBeanMap.get(entityType);
		if (daoBean == null)
			throw new UnsupportedOperationException("Unsupported entity type : " + entityType.toString());
		return daoBean.getLastModifiedResults(lastModifiedAfter);
	}
	
	private List<Locale> getLocaleList(String[] localeTextArray) {
		List<Locale> result = new ArrayList<>();
		if (localeTextArray != null) {
			for (String localeText : localeTextArray) {
				// TODO consider testing whether specified locale is supported?
				String languageAndCountry = localeText.trim();
				String[] tokens = languageAndCountry.split("_");
				if (tokens.length > 1)
					result.add(new Locale(tokens[0], tokens[1]));
				else 
					result.add(new Locale(localeText));
				
				Locale locale = Locale.forLanguageTag(localeText);
				result.add(locale);
			}
		}
		return result;
	}
	
}
