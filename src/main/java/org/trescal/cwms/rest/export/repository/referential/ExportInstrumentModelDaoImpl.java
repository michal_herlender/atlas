package org.trescal.cwms.rest.export.repository.referential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.referential.ExportInstrumentModelDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportInstrumentModelDaoImpl extends RestExportAbstractDaoImpl<ExportInstrumentModelDTO> {

	private Selection<ExportInstrumentModelDTO> getSelection(CriteriaBuilder cb, 
			Root<InstrumentModel> root) {
		
		Join<InstrumentModel, Description> subfamily = root.join(InstrumentModel_.description); 
		Join<InstrumentModel, Mfr> mfr = root.join(InstrumentModel_.mfr);
		Join<InstrumentModel, InstrumentModelType> modelType = root.join(InstrumentModel_.modelType);
		
		Selection<ExportInstrumentModelDTO> selection = cb.construct(ExportInstrumentModelDTO.class, 
				root.get(InstrumentModel_.modelid),
				subfamily.get(Description_.id),
				mfr.get(Mfr_.mfrid),
				root.get(InstrumentModel_.model),
				root.get(InstrumentModel_.modelMfrType),
				modelType.get(InstrumentModelType_.instModelTypeId),
				root.get(InstrumentModel_.quarantined),
				root.get(InstrumentModel_.tmlid)
			);
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportInstrumentModelDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportInstrumentModelDTO.class, cb -> cq -> {
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(InstrumentModel_.modelid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(InstrumentModel_.modelid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INSTRUMENT_MODEL;
	}
	
	@Override
	public List<ExportInstrumentModelDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportInstrumentModelDTO> cq = cb.createQuery(ExportInstrumentModelDTO.class); 
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			cq.where(cb.greaterThan(root.get(InstrumentModel_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
}
