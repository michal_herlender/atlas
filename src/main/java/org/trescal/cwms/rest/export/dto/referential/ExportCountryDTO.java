package org.trescal.cwms.rest.export.dto.referential;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportCountryDTO {
	public Integer countryid;
	public String country;
	public String countrycode;

	// Used in JPA results
	public ExportCountryDTO(Integer countryid, String country, String countrycode) {
		super();
		this.countryid = countryid;
		this.country = country;
		this.countrycode = countrycode;
	}
}
