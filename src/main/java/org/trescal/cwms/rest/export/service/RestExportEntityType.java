package org.trescal.cwms.rest.export.service;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum RestExportEntityType {
	// Referential Data (Group 1)
	BRAND("brand"),
	COUNTRY("country"),
	INSTRUMENT_MODEL("instrumentmodel"),
	INSTRUMENT_MODEL_TRANSLATION("instrumentmodeltranslation"),
	SERVICE_TYPE("servicetype"),
	SUB_FAMILY("subfamily"),
	UNITS_OF_MEASURE("uom"),
	// Company Data (Group 2)
	ADDRESS("address"),
	COMPANY("company"),
	COMPANY_SETTINGS("companysettings"),
	CONTACT("contact"),
	LOCATION("location"),
	SUBDIV("subdiv"),
	DEPARTMENT("department"),
	DEPARTMENT_MEMBER("departmentmember"),
	SUBDIV_SETTINGS("subdivsettings"),
	// Instrument Data (Group 3)
	INSTRUMENT("instrument"),
	CAL_REQ("calreq"),
	INSTRUMENT_RANGE("instrumentrange"),
	INSTRUMENT_USAGE_TYPE("instrumentusagetype"),
	// Job Data (group 4)
	ITEM_STATE("itemstate"),
	JOB("job"),
	JOB_ITEM("jobitem"),
	ENGINEER_ALLOCATION("engineerallocation"),
	UPCOMING_WORK("upcomingwork"),
	UPCOMING_WORK_JOB_LINK("upcomingworkjoblink"),
	JOB_ITEM_GROUP("jobitemgroup"),
	ON_BEHALF_ITEM("onbehalfitem"),
	// Instruction Data (group 5)
	INSTRUCTION("instruction"),
	COMPANY_INSTRUCTION_LINK("companyinstructionlink"),
	SUBDIV_INSTRUCTION_LINK("subdivinstructionlink"),
	CONTACT_INSTRUCTION_LINK("contactinstructionlink"),
	JOB_INSTRUCTION_LINK("jobinstructionlink"),
	// System Default Data (group 6)
	SYSTEM_DEFAULT("systemdefault"),
	SYSTEM_DEFAULT_APPLICATION("systemdefaultapplication"),
	// Work Instruction (group 7)
	WORK_INSTRUCTION("workinstruction"),
	INSTRUMENT_WORK_INSTUCTION("instrumentworkinstruction"),
	// Certificate (group 8)
	CERTIFICATE("certificate"),
	CERT_LINK("certlink"),
	INST_CERT_LINK("instcertlink"),
	// Invoice (group 9)
	INVOICE("invoice"),
	CREDIT_NOTE("creditnote");
	
	
	
	private String pathVariable;
	private static Map<String, RestExportEntityType> supportedPathVariables;
	
	private RestExportEntityType(String pathVariable) {
		this.pathVariable = pathVariable;
	}
	
	static {
		supportedPathVariables = new HashMap<>();
		EnumSet.allOf(RestExportEntityType.class)
			.forEach(type -> supportedPathVariables.put(type.pathVariable, type));
	}
	
	public static RestExportEntityType resolveEntityType(String pathVariable) {
		return supportedPathVariables.get(pathVariable);
	}
}
