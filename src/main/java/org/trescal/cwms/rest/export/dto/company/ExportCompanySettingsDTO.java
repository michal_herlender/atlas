package org.trescal.cwms.rest.export.dto.company;

import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * Export class for CompanySettingsForAllocatedCompany
 */
@Getter @Setter
public class ExportCompanySettingsDTO {
	private Integer id;
	private Integer orgid;
	private Integer companyid;
	private Boolean active;
	private Boolean onstop;
	private CompanyStatus status;

	// Used by JPA
	public ExportCompanySettingsDTO(Integer id, Integer orgid, Integer companyid, Boolean active,
			Boolean onstop, CompanyStatus status) {
		super();
		this.id = id;
		this.orgid = orgid;
		this.companyid = companyid;
		this.active = active;
		this.onstop = onstop;
		this.status = status;
	}
}
