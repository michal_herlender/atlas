package org.trescal.cwms.rest.export.dto.job;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;

@Getter
@Setter
public class ExportJobItemDTO {
	private Integer jobitemid;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private ZonedDateTime datein;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate duedate;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private Date datecomplete;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
	private ZonedDateTime clientreceiptdate;
	private Integer itemno;
	private Integer addrid;
	private Integer plantid;
	private Integer jobid;
	private Integer stateid;
	private String clientRef;
	private Integer servicetypeid; 
	private Integer groupid;

	// Constructor for JPA
	public ExportJobItemDTO(Integer jobitemid, ZonedDateTime datein, LocalDate duedate, Date datecomplete, ZonedDateTime clientreceiptdate,
							Integer itemno, Integer addrid, Integer plantid, Integer jobid, Integer stateid, String clientRef,
							Integer servicetypeid, Integer groupid) {
		super();
		this.jobitemid = jobitemid;
		this.datein = datein;
		this.duedate = duedate;
		this.datecomplete = datecomplete;
		this.clientreceiptdate = clientreceiptdate;
		this.itemno = itemno;
		this.addrid = addrid;
		this.plantid = plantid;
		this.jobid = jobid;
		this.stateid = stateid;
		this.clientRef = clientRef;
		this.servicetypeid = servicetypeid;
		this.groupid = groupid;
	}
}