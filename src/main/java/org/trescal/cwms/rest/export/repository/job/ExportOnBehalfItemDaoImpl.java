package org.trescal.cwms.rest.export.repository.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.job.ExportOnBehalfItemDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportOnBehalfItemDaoImpl extends RestExportAbstractDaoImpl<ExportOnBehalfItemDTO> {

	private Selection<ExportOnBehalfItemDTO> getSelection(CriteriaBuilder cb, Root<OnBehalfItem> root) {

		Join<OnBehalfItem, Address> address = root.join(OnBehalfItem_.address, JoinType.LEFT);
		Join<OnBehalfItem, Company> company = root.join(OnBehalfItem_.company, JoinType.LEFT);
		Join<OnBehalfItem, JobItem> jobitem = root.join(OnBehalfItem_.jobitem, JoinType.LEFT);

		return cb.construct(ExportOnBehalfItemDTO.class, root.get(OnBehalfItem_.id),
				address.get(Address_.addrid), company.get(Company_.coid), jobitem.get(JobItem_.jobItemId));
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportOnBehalfItemDTO> prs, Integer id, List<Locale> locales) {
		
		completePagedResultSet(prs, ExportOnBehalfItemDTO.class, cb -> cq -> {
			Root<OnBehalfItem> root = cq.from(OnBehalfItem.class);
			if (id != null) {
				cq.where(cb.equal(root.get(OnBehalfItem_.id), id));
			}
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(OnBehalfItem_.id)));
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.ON_BEHALF_ITEM;
	}

	@Override
	public List<ExportOnBehalfItemDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportOnBehalfItemDTO> cq = cb.createQuery(ExportOnBehalfItemDTO.class); 
			Root<OnBehalfItem> root = cq.from(OnBehalfItem.class);
			cq.where(cb.greaterThan(root.get(OnBehalfItem_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}

}
