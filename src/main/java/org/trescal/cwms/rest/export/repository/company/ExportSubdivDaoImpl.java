package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportSubdivDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportSubdivDaoImpl extends RestExportAbstractDaoImpl<ExportSubdivDTO> {

	private Selection<ExportSubdivDTO> getSelection(CriteriaBuilder cb, Root<Subdiv> root) {
		
		// TODO make subdiv -> company as not null
		Join<Subdiv, Company> company = root.join(Subdiv_.comp);
		
		Selection<ExportSubdivDTO> selection = cb.construct(ExportSubdivDTO.class, 
				root.get(Subdiv_.subdivid),
				company.get(Company_.coid),
				root.get(Subdiv_.active),
				root.get(Subdiv_.subname),
				root.get(Subdiv_.subdivCode)
			);
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportSubdivDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportSubdivDTO.class, cb -> cq -> {
			Root<Subdiv> root = cq.from(Subdiv.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Subdiv_.subdivid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Subdiv_.subdivid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.SUBDIV;
	}

	@Override
	public List<ExportSubdivDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportSubdivDTO> cq = cb.createQuery(ExportSubdivDTO.class); 
			Root<Subdiv> root = cq.from(Subdiv.class);
			cq.where(cb.greaterThan(root.get(Subdiv_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
