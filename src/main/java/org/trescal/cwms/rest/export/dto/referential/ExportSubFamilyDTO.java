package org.trescal.cwms.rest.export.dto.referential;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportSubFamilyDTO {
	private Integer descriptionid;
	private String description;
	private Integer tmlid;
	private Integer familyid;
	private Boolean active;
	
	// Used in JPA results
	public ExportSubFamilyDTO(Integer descriptionid, String description, Integer tmlid, Integer familyid,
			Boolean active) {
		super();
		this.descriptionid = descriptionid;
		this.description = description;
		this.tmlid = tmlid;
		this.familyid = familyid;
		this.active = active;
	}
}
