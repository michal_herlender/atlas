package org.trescal.cwms.rest.export.dto.company;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportSubdivDTO {
	private Integer subdivid;
	private Integer coid;
	private Boolean active;
	private String subname;
	private String subdivcode;

	// Used by JPA
	public ExportSubdivDTO(Integer subdivid, Integer coid, Boolean active, String subname, String subdivcode) {
		super();
		this.subdivid = subdivid;
		this.coid = coid;
		this.active = active;
		this.subname = subname;
		this.subdivcode = subdivcode;
	}
}