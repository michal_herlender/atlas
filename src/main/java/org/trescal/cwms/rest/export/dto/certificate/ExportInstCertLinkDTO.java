package org.trescal.cwms.rest.export.dto.certificate;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportInstCertLinkDTO {
	private Integer id;
	private Integer certid;
	private Integer plantid;
	
	public ExportInstCertLinkDTO(Integer id, Integer certid, Integer plantid) {
		super();
		this.id = id;
		this.certid = certid;
		this.plantid = plantid;
	}
}
