package org.trescal.cwms.rest.export.dto.certificate;

import java.util.Date;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportCertificateDTO {
	private Integer certid;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date caldate;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date certdate;
	private String certno;
	private Integer duration;
	private String thirdcertno;
	private CertificateType type;
	private Integer servicetypeid;
	private Integer supplementaryforid;
	private Integer thirddiv;
	private IntervalUnit intervalunit;
	private CalibrationVerificationStatus calibrationverificationstatus;
	private Boolean optimization;
	private Boolean repair;
	private Boolean restriction;
	private Boolean adjustment;
	private CertStatusEnum certstatus;

	/**
	 * Constructor used by JPA
	 */
	public ExportCertificateDTO(Integer certid, Date caldate, Date certdate, String certno, Integer duration,
			String thirdcertno, CertificateType type, Integer servicetypeid, Integer supplementaryforid,
			Integer thirddiv, IntervalUnit intervalunit, CalibrationVerificationStatus calibrationverificationstatus,
			Boolean optimization, Boolean repair, Boolean restriction, Boolean adjustment, CertStatusEnum certstatus) {
		super();
		this.certid = certid;
		this.caldate = caldate;
		this.certdate = certdate;
		this.certno = certno;
		this.duration = duration;
		this.thirdcertno = thirdcertno;
		this.type = type;
		this.servicetypeid = servicetypeid;
		this.supplementaryforid = supplementaryforid;
		this.thirddiv = thirddiv;
		this.intervalunit = intervalunit;
		this.calibrationverificationstatus = calibrationverificationstatus;
		this.optimization = optimization;
		this.repair = repair;
		this.restriction = restriction;
		this.adjustment = adjustment;
		this.certstatus = certstatus;
	}
}