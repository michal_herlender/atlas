package org.trescal.cwms.rest.export.dto.certificate;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportCertLinkDTO {
	private Integer linkid;
	private Integer certid;
	private Integer jobitemid;

	/**
	 * Constructor for JPA
	 */
	public ExportCertLinkDTO(Integer linkid, Integer certid, Integer jobitemid) {
		super();
		this.linkid = linkid;
		this.certid = certid;
		this.jobitemid = jobitemid;
	}
}
