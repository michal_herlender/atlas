package org.trescal.cwms.rest.export.repository;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.Query;
import javax.persistence.Tuple;

import org.trescal.cwms.core.tools.PagedResultSet;

public abstract class RestExportAbstractNativeDaoImpl<ExportDTO> extends RestExportAbstractDaoImpl<ExportDTO> {
	
	public abstract Long getResultsCount(Integer id);
	public abstract List<ExportDTO> convert(List<Tuple> nativeResults); 
	public abstract String createSqlResults(Integer id, Integer startResultsFrom, Integer resultsPerPage);
	
	@Override
	public void getPagedResults(PagedResultSet<ExportDTO> prs, Integer id, List<Locale> locales) {
		// Special implementation by native SQL query due to the desire to include discriminator column
		
		int resultsCount = getResultsCount(id).intValue();
		prs.setResultsCount(resultsCount);
				
		String sqlResults = createSqlResults(id, prs.getStartResultsFrom(), prs.getResultsPerPage());
		Query resultsQuery = super.getEntityManager().createNativeQuery(sqlResults, Tuple.class);
		@SuppressWarnings("unchecked")
		List<Tuple> nativeResults = resultsQuery.getResultList();
		List<ExportDTO> results = convert(nativeResults);
		prs.setResults(results);
	}
	
	public List<ExportDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("Not implemented for this DTO type");
	}

}
