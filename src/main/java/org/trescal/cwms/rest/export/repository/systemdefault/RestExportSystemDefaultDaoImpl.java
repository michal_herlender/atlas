package org.trescal.cwms.rest.export.repository.systemdefault;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.systemdefault.ExportSystemDefaultDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class RestExportSystemDefaultDaoImpl extends RestExportAbstractDaoImpl<ExportSystemDefaultDTO> {

	private Selection<ExportSystemDefaultDTO> getSelection(CriteriaBuilder cb, Root<SystemDefault> root) {
		Selection<ExportSystemDefaultDTO> selection = cb.construct(ExportSystemDefaultDTO.class, 
				root.get(SystemDefault_.id),
				root.get(SystemDefault_.defaultDataType),
				root.get(SystemDefault_.defaultDescription),
				root.get(SystemDefault_.defaultName),
				root.get(SystemDefault_.defaultValue),
				root.get(SystemDefault_.groupwide)
			);
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportSystemDefaultDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportSystemDefaultDTO.class, cb -> cq -> {
			Root<SystemDefault> root = cq.from(SystemDefault.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(SystemDefault_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(SystemDefault_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.SYSTEM_DEFAULT;
	}

	@Override
	public List<ExportSystemDefaultDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("No lastModified on SystemDefault");
	}
	
}
