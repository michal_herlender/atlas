package org.trescal.cwms.rest.export.repository.job;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.rest.export.dto.job.ExportItemStateDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractNativeDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportItemStateDaoImpl extends RestExportAbstractNativeDaoImpl<ExportItemStateDTO> {
	
	private static int INDEX_TYPE = 0;
	private static int INDEX_STATEID = 1;
	private static int INDEX_ACTIVE = 2;
	private static int INDEX_DESCRIPTION = 3;
	private static int INDEX_RETIRED = 4;
	
	// [type],[stateid],[active],[description],[retired]
	
	@Override
	public List<ExportItemStateDTO> convert(List<Tuple> nativeResults) {
		List<ExportItemStateDTO> result = new ArrayList<>();
		for (Tuple tuple : nativeResults) {
			ExportItemStateDTO dto = new ExportItemStateDTO();
			
			String type = (String) tuple.get(INDEX_TYPE);
			Integer stateid = (Integer) tuple.get(INDEX_STATEID);
			Boolean active = (Boolean) tuple.get(INDEX_ACTIVE);
			String description = (String) tuple.get(INDEX_DESCRIPTION);
			Boolean retired = (Boolean) tuple.get(INDEX_RETIRED);

			dto.setActive(active);
			dto.setStateid(stateid);
			dto.setType(type);
			dto.setDescription(description);
			dto.setRetired(retired);
			
			result.add(dto);
		}
		return result;
	}
	
	@Override
	public Long getResultsCount(Integer id) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<ItemState> root = cq.from(ItemState.class);
			if (id != null) {
				cq.where(cb.equal(root.get(ItemState_.stateid), id));
			}
			
			cq.select(cb.count(root));
			return cq;
		});
	}

	@Override
	public String createSqlResults(Integer id, Integer startResultsFrom, Integer resultsPerPage) {
		if (startResultsFrom == null) throw new IllegalArgumentException("startResultsFrom must be specified");
		if (startResultsFrom < 0) throw new IllegalArgumentException("startResultsFrom must be >= 0");
		if (resultsPerPage == null) throw new IllegalArgumentException("resultsPerPage must be specified");
		if (resultsPerPage < 1) throw new IllegalArgumentException("resultsPerPage must be >= 1");
		
		StringBuffer result = new StringBuffer();
		result.append("SELECT [type],[stateid],[active],[description],[retired]");
		result.append(" FROM [itemstate]");
		if (id != null) {
			result.append(" WHERE [stateid]="+id.toString());
		}
		result.append(" ORDER BY [stateid]");
		result.append(" OFFSET "+startResultsFrom.toString()+" ROWS");
		result.append(" FETCH NEXT "+resultsPerPage.toString()+" ROWS ONLY");
		
		return result.toString();
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.ITEM_STATE;
	}

}
