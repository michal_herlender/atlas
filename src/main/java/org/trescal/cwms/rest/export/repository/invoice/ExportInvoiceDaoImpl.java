package org.trescal.cwms.rest.export.repository.invoice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.invoice.ExportInvoiceDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportInvoiceDaoImpl extends RestExportAbstractDaoImpl<ExportInvoiceDTO> {

	private Selection<ExportInvoiceDTO> getSelection(CriteriaBuilder cb, Root<Invoice> root) {
		Join<Invoice, Address> address = root.join(Invoice_.address, JoinType.LEFT);
		Join<Invoice, Company> company = root.join(Invoice_.comp, JoinType.LEFT);
		Join<Invoice, Company> allocatedCompany = root.join(Invoice_.organisation.getName(), JoinType.LEFT);
		
		return cb.construct(ExportInvoiceDTO.class, 
			root.get(Invoice_.id),	
			root.get(Invoice_.issued),
			root.get(Invoice_.issuedate),
			root.get(Invoice_.finalCost),
			root.get(Invoice_.totalCost),
			root.get(Invoice_.vatValue),
			root.get(Invoice_.invno),
			address.get(Address_.addrid),
			company.get(Company_.coid),
			allocatedCompany.get(Company_.coid),
			root.get(Invoice_.duedate));
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportInvoiceDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportInvoiceDTO.class, cb -> cq -> {
			Root<Invoice> root = cq.from(Invoice.class);
			if (id != null) {
				cq.where(cb.equal(root.get(Invoice_.id), id));
			}
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Invoice_.id)));
			return Triple.of(root, getSelection(cb, root), order);
		});
		
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INVOICE;
	}

	@Override
	public List<ExportInvoiceDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException();
	}

}
