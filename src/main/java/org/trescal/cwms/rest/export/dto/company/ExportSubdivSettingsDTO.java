package org.trescal.cwms.rest.export.dto.company;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportSubdivSettingsDTO {
	private Integer id;
	private Integer orgid;
	private Integer businesscontactid;
	private Integer subdivid;

	public ExportSubdivSettingsDTO(Integer id, Integer orgid, Integer businesscontactid, Integer subdivid) {
		super();
		this.id = id;
		this.orgid = orgid;
		this.businesscontactid = businesscontactid;
		this.subdivid = subdivid;
	}
}
