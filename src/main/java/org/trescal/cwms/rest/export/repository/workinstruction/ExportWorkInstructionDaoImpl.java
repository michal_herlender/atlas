package org.trescal.cwms.rest.export.repository.workinstruction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.workinstruction.ExportWorkInstructionDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportWorkInstructionDaoImpl extends RestExportAbstractDaoImpl<ExportWorkInstructionDTO> {
	private Selection<ExportWorkInstructionDTO> getSelection(CriteriaBuilder cb, Root<WorkInstruction> root) {
		Join<WorkInstruction, Description> subfamily = root.join(WorkInstruction_.description, JoinType.LEFT);
		Join<WorkInstruction, Company> allocatedCompany = root.join(WorkInstruction_.ORGANISATION, JoinType.INNER);
		
		Selection<ExportWorkInstructionDTO> selection = cb.construct(ExportWorkInstructionDTO.class, 
			root.get(WorkInstruction_.id),
			root.get(WorkInstruction_.name),
			root.get(WorkInstruction_.title),
			subfamily.get(Description_.id),
			allocatedCompany.get(Company_.coid)
		);

		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportWorkInstructionDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportWorkInstructionDTO.class, cb -> cq -> {
			Root<WorkInstruction> root = cq.from(WorkInstruction.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(WorkInstruction_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(WorkInstruction_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.WORK_INSTRUCTION;
	}

	@Override
	public List<ExportWorkInstructionDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportWorkInstructionDTO> cq = cb.createQuery(ExportWorkInstructionDTO.class); 
			Root<WorkInstruction> root = cq.from(WorkInstruction.class);
			cq.where(cb.greaterThan(root.get(WorkInstruction_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
}
