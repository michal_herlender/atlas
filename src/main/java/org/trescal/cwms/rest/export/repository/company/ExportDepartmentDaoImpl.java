package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportDepartmentDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportDepartmentDaoImpl extends RestExportAbstractDaoImpl<ExportDepartmentDTO> {

	private Selection<ExportDepartmentDTO> getSelection(CriteriaBuilder cb, Root<Department> root) {
		Join<Department, Address> address = root.join(Department_.address);
		Join<Department, Subdiv> subdiv = root.join(Department_.subdiv);
		
		Selection<ExportDepartmentDTO> selection = cb.construct(ExportDepartmentDTO.class, 
				root.get(Department_.deptid),
				root.get(Department_.name),
				address.get(Address_.addrid),
				subdiv.get(Subdiv_.subdivid),
				root.get(Department_.type),
				root.get(Department_.shortName)
			);
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportDepartmentDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportDepartmentDTO.class, cb -> cq -> {
			Root<Department> root = cq.from(Department.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Department_.deptid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Department_.deptid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.DEPARTMENT;
	}

	@Override
	public List<ExportDepartmentDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("No lastModified on Department");
	}	
}
