package org.trescal.cwms.rest.export.dto.systemdefault;

import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportSystemDefaultApplicationDTO {
	
	private Integer id;
	private Integer defaultid;
	private String defaultvalue;
	private Integer companyid;
	private Integer subdivid;
	private Integer personid;
	private Integer orgid;
	
	/**
	 * Constructor for JPA; note use of SystemDefaultNames.  For CDC we could create another constructor if needed?
	 */
	public ExportSystemDefaultApplicationDTO(Integer id, SystemDefaultNames systemDefault, String defaultvalue, Integer companyid,
			Integer subdivid, Integer personid, Integer orgid) {
		super();
		this.id = id;
		this.defaultid = systemDefault.ordinal();
		this.defaultvalue = defaultvalue;
		this.companyid = companyid;
		this.subdivid = subdivid;
		this.personid = personid;
		this.orgid = orgid;
	}	
}
