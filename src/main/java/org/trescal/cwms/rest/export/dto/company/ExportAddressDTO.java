package org.trescal.cwms.rest.export.dto.company;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportAddressDTO {
	private Integer addressid;
	private Boolean active;
	private String addr1;
	private String addr2;
	private String addr3;
	private String county;
	private String postcode;
	private String town;
	private Integer countryid;
	private Integer subdivid;

	// Used by JPA
	public ExportAddressDTO(Integer addressid, Boolean active, String addr1, String addr2, String addr3, String county,
			String postcode, String town, Integer countryid, Integer subdivid) {
		super();
		this.addressid = addressid;
		this.active = active;
		this.addr1 = addr1;
		this.addr2 = addr2;
		this.addr3 = addr3;
		this.county = county;
		this.postcode = postcode;
		this.town = town;
		this.countryid = countryid;
		this.subdivid = subdivid;
	}
}
