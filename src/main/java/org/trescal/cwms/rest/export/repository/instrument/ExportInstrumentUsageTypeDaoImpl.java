package org.trescal.cwms.rest.export.repository.instrument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.instrument.ExportInstrumentUsageTypeDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportInstrumentUsageTypeDaoImpl extends RestExportAbstractDaoImpl<ExportInstrumentUsageTypeDTO> {

	private Selection<ExportInstrumentUsageTypeDTO> getSelection(CriteriaBuilder cb, Root<InstrumentUsageType> root) {
		return cb.construct(ExportInstrumentUsageTypeDTO.class, root.get(InstrumentUsageType_.id),
				root.get(InstrumentUsageType_.defaultType), root.get(InstrumentUsageType_.fullDescription),
				root.get(InstrumentUsageType_.name), root.get(InstrumentUsageType_.recall),
				root.get(InstrumentUsageType_.instrumentStatus));
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportInstrumentUsageTypeDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportInstrumentUsageTypeDTO.class, cb -> cq -> {
			Root<InstrumentUsageType> root = cq.from(InstrumentUsageType.class);
			if (id != null) {
				cq.where(cb.equal(root.get(InstrumentUsageType_.id), id));
			}
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(InstrumentUsageType_.id)));
			return Triple.of(root, getSelection(cb, root), order);
		});

	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INSTRUMENT_USAGE_TYPE;
	}

	@Override
	public List<ExportInstrumentUsageTypeDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("No lastModified on InstrumentUsageType");
	}

}
