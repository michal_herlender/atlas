package org.trescal.cwms.rest.export.dto.invoice;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExportInvoiceDTO {
	private Integer id;
	private Boolean issued;
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate issuedate;
	private BigDecimal finalcost;
	private BigDecimal totalcost;
	private BigDecimal vatvalue;
	private String invno;
	private Integer addressid;
	private Integer coid;
	private Integer orgid;
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate duedate;

	public ExportInvoiceDTO(Integer id, Boolean issued, LocalDate issuedate, BigDecimal finalcost, BigDecimal totalcost,
			BigDecimal vatvalue, String invno, Integer addressid, Integer coid, Integer orgid, LocalDate duedate) {
		super();
		this.id = id;
		this.issued = issued;
		this.issuedate = issuedate;
		this.finalcost = finalcost;
		this.totalcost = totalcost;
		this.vatvalue = vatvalue;
		this.invno = invno;
		this.addressid = addressid;
		this.coid = coid;
		this.orgid = orgid;
		this.duedate = duedate;
	}
}