package org.trescal.cwms.rest.export.dto.instruction;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportCompanyInstructionLinkDTO extends ExportInstructionLinkDTO {
	private Integer coid;
	private Integer orgid;
	private Boolean issubdivinstruction;
	private Integer businesssubdivid;

	/**
	 * Constructor used in JPA
	 */
	public ExportCompanyInstructionLinkDTO(Integer id, Integer instructionid, Integer coid, Integer orgid,
			Boolean issubdivinstruction, Integer businesssubdivid) {
		super(id, instructionid);
		this.coid = coid;
		this.orgid = orgid;
		this.issubdivinstruction = issubdivinstruction;
		this.businesssubdivid = businesssubdivid;
	}

}
