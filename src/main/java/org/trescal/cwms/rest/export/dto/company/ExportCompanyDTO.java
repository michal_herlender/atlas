package org.trescal.cwms.rest.export.dto.company;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportCompanyDTO {
	private Integer coid;
	private String coname;
	private CompanyRole companyRole;
	private Integer countryid;
	private String companyCode;
	private String legalIdentifier;
	private String fiscalIdentifier;

	// Used by JPA
	public ExportCompanyDTO(Integer coid, String coname, CompanyRole companyRole, Integer countryid, String companyCode,
			String legalIdentifier, String fiscalIdentifier) {
		super();
		this.coid = coid;
		this.coname = coname;
		this.companyRole = companyRole;
		this.countryid = countryid;
		this.companyCode = companyCode;
		this.legalIdentifier = legalIdentifier;
		this.fiscalIdentifier = fiscalIdentifier;
	}
}
