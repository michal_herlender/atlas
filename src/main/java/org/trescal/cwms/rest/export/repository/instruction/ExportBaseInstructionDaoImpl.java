package org.trescal.cwms.rest.export.repository.instruction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.instruction.ExportBaseInstructionDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportBaseInstructionDaoImpl extends RestExportAbstractDaoImpl<ExportBaseInstructionDTO> {

	private Selection<ExportBaseInstructionDTO> getSelection(CriteriaBuilder cb, Root<Instruction> root) {
		// Integer instructionid, String instruction, InstructionType instructiontype
		Selection<ExportBaseInstructionDTO> selection = cb.construct(ExportBaseInstructionDTO.class, 
				root.get(Instruction_.instructionid),
				root.get(Instruction_.instruction),
				root.get(Instruction_.instructiontype)
			);
		
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportBaseInstructionDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportBaseInstructionDTO.class, cb -> cq -> {
			Root<Instruction> root = cq.from(Instruction.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Instruction_.instructionid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Instruction_.instructionid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INSTRUCTION;
	}

	@Override
	public List<ExportBaseInstructionDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportBaseInstructionDTO> cq = cb.createQuery(ExportBaseInstructionDTO.class); 
			Root<Instruction> root = cq.from(Instruction.class);
			cq.where(cb.greaterThan(root.get(Instruction_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
