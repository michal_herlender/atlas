package org.trescal.cwms.rest.export.controller;

/**
 * Contains static error message definitions.
 * 
 * These are not needed to be localized because they're
 *
 */
public class RestExportMessageDefinitions {
	public static String ERROR_INVALID_PATH = "Invalid path : {0}";
	public static String ERROR_ENTITY_NOT_FOUND = "Data not found for entity type and id : {0}, {1}";
}
