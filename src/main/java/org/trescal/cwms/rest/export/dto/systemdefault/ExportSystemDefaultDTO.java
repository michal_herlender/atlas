package org.trescal.cwms.rest.export.dto.systemdefault;

import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportSystemDefaultDTO {
	
	private Integer id;
	private String defaultdatatype;
	private String defaultdescription;
	private String defaultname;
	private String defaultvalue;
	private Boolean groupwide;
	
	/**
	 * Constructor for JPA; note that SystemDefaultNames comes from the Hibernate mapping, but I feel it is simpler / better 
	 * to export the default ID.  For CDC export we could create another constructor if needed, directly with the ID?  
	 */
	public ExportSystemDefaultDTO(SystemDefaultNames systemDefault, String defaultdatatype, String defaultdescription,
			String defaultname, String defaultvalue, Boolean groupwide) {
		super();
		this.id = systemDefault.ordinal();
		this.defaultdatatype = defaultdatatype;
		this.defaultdescription = defaultdescription;
		this.defaultname = defaultname;
		this.defaultvalue = defaultvalue;
		this.groupwide = groupwide;
	}
}
