package org.trescal.cwms.rest.export.dto.instrument;

import lombok.Getter;
import lombok.Setter;

/**
 * Note, other values defined in Range such as:
 *  [maxIsInfinite] ,[maxvalue] ,[minvalue] 
 *  are not used by InstrumentRange so not exported here
 */
@Getter @Setter
public class ExportInstrumentRangeDTO {
	private Integer id;
	private Double end;
	private Double start;
	private Integer uom;
	private Integer maxUom;
	private Integer plantid;

	/**
	 * Constructor used by JPA
	 */
	public ExportInstrumentRangeDTO(Integer id, Double end, Double start, Integer uom, Integer maxUom, Integer plantid) {
		super();
		this.id = id;
		this.end = end;
		this.start = start;
		this.uom = uom;
		this.maxUom = maxUom;
		this.plantid = plantid;
	}
}
