package org.trescal.cwms.rest.export.repository.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.job.ExportEngineerAllocationDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation_;

@Repository
public class ExportEngineerAllocationDaoImpl extends RestExportAbstractDaoImpl<ExportEngineerAllocationDTO> {

	private Selection<ExportEngineerAllocationDTO> getSelection(CriteriaBuilder cb, Root<EngineerAllocation> root) {
		Join<EngineerAllocation, Contact> allocatedby = root.join(EngineerAllocation_.allocatedBy);
		Join<EngineerAllocation, Contact> allocatedto = root.join(EngineerAllocation_.allocatedTo);
		Join<EngineerAllocation, JobItem> jobitem = root.join(EngineerAllocation_.itemAllocated);
		Join<EngineerAllocation, Department> dept = root.join(EngineerAllocation_.dept);
		
		Selection<ExportEngineerAllocationDTO> selection = cb.construct(ExportEngineerAllocationDTO.class, 
				root.get(EngineerAllocation_.id),	
				root.get(EngineerAllocation_.active),
				root.get(EngineerAllocation_.allocatedFor),
				root.get(EngineerAllocation_.allocatedOn),	
				root.get(EngineerAllocation_.timeOfDay),	
				allocatedby.get(Contact_.personid),
				allocatedto.get(Contact_.personid),
				jobitem.get(JobItem_.jobItemId),
				root.get(EngineerAllocation_.allocatedUntil),	
				root.get(EngineerAllocation_.timeOfDayUntil),
				dept.get(Department_.deptid)
			);
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportEngineerAllocationDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportEngineerAllocationDTO.class, cb -> cq -> {
			Root<EngineerAllocation> root = cq.from(EngineerAllocation.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(EngineerAllocation_.id), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(EngineerAllocation_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.ENGINEER_ALLOCATION;
	}
	
	@Override
	public List<ExportEngineerAllocationDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("No lastModified on EngineerAllocation");
	}	
}
