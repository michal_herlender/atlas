package org.trescal.cwms.rest.export.dto.instrument;

import java.time.LocalDate;

import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportInstrumentDTO {
	private Integer plantid;
	private Integer calInterval;
	private IntervalUnit calIntervalUnit;
	private Boolean calibrationStandard;
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate recallDate;
	private String plantno;
	private String serialno;
	private InstrumentStatus status;
	private String customerDescription;
	private String customerSpecification;
	private String freeTextLocation;
	private String clientBarcode;
	private String formerBarcode;
	private String modelname;
	private Integer addressid;
	private Integer coid;
	private Integer personid;
	private Integer locationid;
	private Integer mfrid;
	private Integer modelid;
	private Integer defaultservicetypeid;
	private int usageid;
	private boolean customermanaged;
	
	
	public ExportInstrumentDTO(Integer plantid, Integer calInterval, IntervalUnit calIntervalUnit,
			Boolean calibrationStandard, LocalDate recallDate, String plantno, String serialno, InstrumentStatus status,
			String customerDescription, String customerSpecification, String freeTextLocation, String clientBarcode,
			String formerBarcode, String modelname, Integer addressid, Integer coid, Integer personid,
			Integer locationid, Integer mfrid, Integer modelid, Integer defaultservicetypeid, int usageid,
			boolean customermanaged) {
		super();
		this.plantid = plantid;
		this.calInterval = calInterval;
		this.calIntervalUnit = calIntervalUnit;
		this.calibrationStandard = calibrationStandard;
		this.recallDate = recallDate;
		this.plantno = plantno;
		this.serialno = serialno;
		this.status = status;
		this.customerDescription = customerDescription;
		this.customerSpecification = customerSpecification;
		this.freeTextLocation = freeTextLocation;
		this.clientBarcode = clientBarcode;
		this.formerBarcode = formerBarcode;
		this.modelname = modelname;
		this.addressid = addressid;
		this.coid = coid;
		this.personid = personid;
		this.locationid = locationid;
		this.mfrid = mfrid;
		this.modelid = modelid;
		this.defaultservicetypeid = defaultservicetypeid;
		this.usageid = usageid;
		this.customermanaged = customermanaged;
	}

	// Used in JPA
	
}
