package org.trescal.cwms.rest.export.repository.referential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.referential.ExportTranslationDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportInstrumentModelTranslationDaoImpl extends RestExportAbstractDaoImpl<ExportTranslationDTO> {

	private Selection<ExportTranslationDTO> getSelection(CriteriaBuilder cb, 
			Root<InstrumentModel> root, Join<InstrumentModel, Translation> translation) {
		
		Selection<ExportTranslationDTO> selection = cb.construct(ExportTranslationDTO.class, 
				root.get(InstrumentModel_.modelid),
				translation.get(Translation_.locale),
				translation.get(Translation_.translation)
			);
		return selection;
	}
	
	
	@Override
	public void getPagedResults(PagedResultSet<ExportTranslationDTO> prs, Integer id,
			List<Locale> locales) {
		completePagedResultSet(prs, ExportTranslationDTO.class, cb -> cq -> {
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, Translation> translation = root.join(InstrumentModel_.nameTranslations, JoinType.INNER);
			
			Predicate clauses = cb.conjunction();
			
			if (id != null) {
				clauses.getExpressions().add(
						cb.equal(root.get(InstrumentModel_.modelid), id));
			}
			if ((locales != null) && !locales.isEmpty()) {
				clauses.getExpressions().add(
					translation.get(Translation_.locale).in(locales));
			}
			cq.where(clauses);
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(InstrumentModel_.modelid)));
			order.add(cb.asc(translation.get(Translation_.locale)));
			
			return Triple.of(root, getSelection(cb, root, translation), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INSTRUMENT_MODEL_TRANSLATION;
	}

	@Override
	public List<ExportTranslationDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("No lastModified on translations");
	}	
}
