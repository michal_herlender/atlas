package org.trescal.cwms.rest.export.repository.job;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.rest.export.dto.job.ExportJobItemDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Repository
public class ExportJobItemDaoImpl extends RestExportAbstractDaoImpl<ExportJobItemDTO> {

	private Selection<ExportJobItemDTO> getSelection(CriteriaBuilder cb, Root<JobItem> root) {
		// TODO add not-null to plantid, jobid, stateid (for now left join)

		Join<JobItem, Address> currentAddr = root.join(JobItem_.currentAddr, JoinType.LEFT);
		Join<JobItem, Instrument> instrument = root.join(JobItem_.inst, JoinType.LEFT);
		Join<JobItem, Job> job = root.join(JobItem_.job, JoinType.LEFT);
		Join<JobItem, ItemState> state = root.join(JobItem_.state, JoinType.LEFT);
		Join<JobItem, ServiceType> serviceType = root.join(JobItem_.serviceType);
		Join<JobItem, JobItemGroup> group = root.join(JobItem_.group, JoinType.LEFT);

		return cb.construct(ExportJobItemDTO.class,
			root.get(JobItem_.jobItemId),
			root.get(JobItem_.dateIn),
			root.get(JobItem_.dueDate),
			root.get(JobItem_.dateComplete),
			root.get(JobItem_.clientReceiptDate),
			root.get(JobItem_.itemNo),
			currentAddr.get(Address_.addrid),
			instrument.get(Instrument_.plantid),
			job.get(Job_.jobid),
			state.get(ItemState_.stateid),
			root.get(JobItem_.clientRef),
			serviceType.get(ServiceType_.serviceTypeId),
			group.get(JobItemGroup_.id)
		);
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportJobItemDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportJobItemDTO.class, cb -> cq -> {
			Root<JobItem> root = cq.from(JobItem.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(JobItem_.jobItemId), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(JobItem_.jobItemId)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.JOB_ITEM;
	}

	@Override
	public List<ExportJobItemDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportJobItemDTO> cq = cb.createQuery(ExportJobItemDTO.class); 
			Root<JobItem> root = cq.from(JobItem.class);
			cq.where(cb.greaterThan(root.get(JobItem_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
