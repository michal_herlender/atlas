package org.trescal.cwms.rest.export.dto.instrument;

import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportInstrumentUsageTypeDTO {

	private int typeid;
	private boolean defaultType;
	private String fullDescription;
	private String name;
	private boolean recall; 
	private InstrumentStatus instrumentStatus;
	
	
	
	public ExportInstrumentUsageTypeDTO(int typeid, boolean defaultType, String fullDescription, String name,
			boolean recall, InstrumentStatus instrumentStatus) {
		super();
		this.typeid = typeid;
		this.defaultType = defaultType;
		this.fullDescription = fullDescription;
		this.name = name;
		this.recall = recall;
		this.instrumentStatus = instrumentStatus;
	}
	
	
	
	
	
}
