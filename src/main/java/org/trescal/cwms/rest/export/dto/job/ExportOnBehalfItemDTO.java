package org.trescal.cwms.rest.export.dto.job;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportOnBehalfItemDTO {
	
	private Integer id;
	private Integer addressid;
	private Integer coid;
	private Integer jobitemid;
	
	
	public ExportOnBehalfItemDTO(Integer id, Integer addressid, Integer coid, Integer jobitemid) {
		super();
		this.id = id;
		this.addressid = addressid;
		this.coid = coid;
		this.jobitemid = jobitemid;
	}
	
	

}
