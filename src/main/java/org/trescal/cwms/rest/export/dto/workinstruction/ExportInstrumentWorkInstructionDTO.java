package org.trescal.cwms.rest.export.dto.workinstruction;

import lombok.Getter;
import lombok.Setter;

/**
 * Not queried by JPA as there is no corresponding Hibernate entity; SQL only
 */
@Getter @Setter
public class ExportInstrumentWorkInstructionDTO {
	private Integer workInstructionId; 
	private Integer plantId;
}
