package org.trescal.cwms.rest.export.dto.job;

import java.time.LocalDate;

import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportJobDTO {
	private Integer jobid;
	private String clientref;
	private String jobno;
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate regdate;
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate datecomplete;
	private Integer personid;
	private Integer statusid;
	private Integer returnto;
	private JobType jobType;
	private Integer orgid;
	
	// Constructor for JPA
	public ExportJobDTO(Integer jobid, String clientref, String jobno, LocalDate regdate, LocalDate datecomplete,
			Integer personid, Integer statusid, Integer returnto, JobType jobType, Integer orgid) {
		super();
		this.jobid = jobid;
		this.clientref = clientref;
		this.jobno = jobno;
		this.regdate = regdate;
		this.datecomplete = datecomplete;
		this.personid = personid;
		this.statusid = statusid;
		this.returnto = returnto;
		this.jobType = jobType;
		this.orgid = orgid;
	}
}
