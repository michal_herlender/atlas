package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportSubdivSettingsDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportSubdivSettingsDaoImpl extends RestExportAbstractDaoImpl<ExportSubdivSettingsDTO> {

	private Selection<ExportSubdivSettingsDTO> getSelection(CriteriaBuilder cb, Root<SubdivSettingsForAllocatedSubdiv> root) {
		Join<SubdivSettingsForAllocatedSubdiv, Contact> contact = 
				root.join(SubdivSettingsForAllocatedSubdiv_.defaultBusinessContact, JoinType.LEFT);
		Join<SubdivSettingsForAllocatedSubdiv, Subdiv> subdiv = 
				root.join(SubdivSettingsForAllocatedSubdiv_.subdiv, JoinType.LEFT);
		Join<SubdivSettingsForAllocatedSubdiv, Subdiv> allocatedSubdiv = 
				root.join(SubdivSettingsForAllocatedSubdiv_.organisation.getName(), JoinType.LEFT);
		
		return cb.construct(ExportSubdivSettingsDTO.class, 
			root.get(SubdivSettingsForAllocatedSubdiv_.id),	
			allocatedSubdiv.get(Subdiv_.subdivid),
			contact.get(Contact_.personid),
			subdiv.get(Subdiv_.subdivid));
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportSubdivSettingsDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportSubdivSettingsDTO.class, cb -> cq -> {
			Root<SubdivSettingsForAllocatedSubdiv> root = cq.from(SubdivSettingsForAllocatedSubdiv.class);
			if (id != null) {
				cq.where(cb.equal(root.get(SubdivSettingsForAllocatedSubdiv_.id), id));
			}
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(SubdivSettingsForAllocatedSubdiv_.id)));
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.SUBDIV_SETTINGS;
	}

	@Override
	public List<ExportSubdivSettingsDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException();
	}

}
