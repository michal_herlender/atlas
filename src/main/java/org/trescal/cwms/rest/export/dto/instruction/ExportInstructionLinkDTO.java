package org.trescal.cwms.rest.export.dto.instruction;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class ExportInstructionLinkDTO {
	private Integer id;
	private Integer instructionid;

	public ExportInstructionLinkDTO(Integer id, Integer instructionid) {
		super();
		this.id = id;
		this.instructionid = instructionid;
	}
}
