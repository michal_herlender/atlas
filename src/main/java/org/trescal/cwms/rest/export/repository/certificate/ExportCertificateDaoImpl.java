package org.trescal.cwms.rest.export.repository.certificate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.certificate.ExportCertificateDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportCertificateDaoImpl extends RestExportAbstractDaoImpl<ExportCertificateDTO> {

	private Selection<ExportCertificateDTO> getSelection(CriteriaBuilder cb, Root<Certificate> root) {
		Join<Certificate, ServiceType> serviceType = root.join(Certificate_.serviceType, JoinType.LEFT); 
		Join<Certificate, Certificate> supplementaryFor = root.join(Certificate_.supplementaryFor, JoinType.LEFT);
		Join<Certificate, Subdiv> thirdDiv = root.join(Certificate_.thirdDiv, JoinType.LEFT);
		
		Selection<ExportCertificateDTO> selection = cb.construct(ExportCertificateDTO.class, 
				root.get(Certificate_.certid),
				root.get(Certificate_.calDate),
				root.get(Certificate_.certDate),
				root.get(Certificate_.certno),
				root.get(Certificate_.duration),
				root.get(Certificate_.thirdCertNo),
				root.get(Certificate_.type),
				serviceType.get(ServiceType_.serviceTypeId),
				supplementaryFor.get(Certificate_.certid),
				thirdDiv.get(Subdiv_.subdivid),
				root.get(Certificate_.unit),
				root.get(Certificate_.calibrationVerificationStatus),
				root.get(Certificate_.optimization),
				root.get(Certificate_.repair),
				root.get(Certificate_.restriction),
				root.get(Certificate_.adjustment),
				root.get(Certificate_.status)
				);
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportCertificateDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportCertificateDTO.class, cb -> cq -> {
			Root<Certificate> root = cq.from(Certificate.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Certificate_.certid), id));
			}

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Certificate_.certid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.CERTIFICATE;
	}
	
	@Override
	public List<ExportCertificateDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportCertificateDTO> cq = cb.createQuery(ExportCertificateDTO.class); 
			Root<Certificate> root = cq.from(Certificate.class);
			cq.where(cb.greaterThan(root.get(Certificate_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}

}
