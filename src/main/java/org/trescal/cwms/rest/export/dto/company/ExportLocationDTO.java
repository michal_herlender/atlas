package org.trescal.cwms.rest.export.dto.company;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportLocationDTO {
	private Integer locationid;
	private Integer addressid; 
	private Boolean active;
	private String location;

	// Used by JPA
	public ExportLocationDTO(Integer locationid, Integer addressid, Boolean active, String location) {
		super();
		this.locationid = locationid;
		this.addressid = addressid;
		this.active = active;
		this.location = location;
	}
}
