package org.trescal.cwms.rest.export.dto.invoice;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportCreditNoteDTO {
	private Integer id;
	private Boolean issued;
	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate issuedate;
	private BigDecimal finalcost;
	private BigDecimal totalcost;
	private BigDecimal vatvalue;
	private String creditnoteno;
	private Integer invid;
	private Integer orgid;

	public ExportCreditNoteDTO(Integer id, Boolean issued, LocalDate issuedate, BigDecimal finalcost,
			BigDecimal totalcost, BigDecimal vatvalue, String creditnoteno, Integer invid, Integer orgid) {
		super();
		this.id = id;
		this.issued = issued;
		this.issuedate = issuedate;
		this.finalcost = finalcost;
		this.totalcost = totalcost;
		this.vatvalue = vatvalue;
		this.creditnoteno = creditnoteno;
		this.invid = invid;
		this.orgid = orgid;
	}
}