package org.trescal.cwms.rest.export.dto.job;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportItemStateDTO {
	private String type;
	private Integer stateid;
	private Boolean active;
	private String description;
	private Boolean retired;
	
	// No-args constructor used in DAO
}
