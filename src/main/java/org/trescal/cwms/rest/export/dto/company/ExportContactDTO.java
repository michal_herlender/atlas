package org.trescal.cwms.rest.export.dto.company;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class ExportContactDTO {
	private Integer personid;
	private Boolean active;
	private String firstname;
	private String lastname;
	private String email;
	private String telephone;
	private String fax;
	private String mobile;
	private Integer subdivid;
	private Integer defaddress;

	// Used by JPA
	public ExportContactDTO(Integer personid, Boolean active, String firstname, String lastname, String email,
			String telephone, String fax, String mobile, Integer subdivid, Integer defaddress) {
		super();
		this.personid = personid;
		this.active = active;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.telephone = telephone;
		this.fax = fax;
		this.mobile = mobile;
		this.subdivid = subdivid;
		this.defaddress = defaddress;
	}
}