package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportCompanyDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportCompanyDaoImpl extends RestExportAbstractDaoImpl<ExportCompanyDTO> {

	private Selection<ExportCompanyDTO> getSelection(CriteriaBuilder cb, Root<Company> root) {
		
		// TODO should make company -> country not-nullable
		Join<Company, Country> country = root.join(Company_.country);
		
		Selection<ExportCompanyDTO> selection = cb.construct(ExportCompanyDTO.class, 
				root.get(Company_.coid),
				root.get(Company_.coname),
				root.get(Company_.companyRole),
				country.get(Country_.countryid),
				root.get(Company_.companyCode),
				root.get(Company_.legalIdentifier),
				root.get(Company_.fiscalIdentifier)
				);
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportCompanyDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportCompanyDTO.class, cb -> cq -> {
			Root<Company> root = cq.from(Company.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Company_.coid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Company_.coid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.COMPANY;
	}
	
	@Override
	public List<ExportCompanyDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportCompanyDTO> cq = cb.createQuery(ExportCompanyDTO.class); 
			Root<Company> root = cq.from(Company.class);
			cq.where(cb.greaterThan(root.get(Company_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}

}
