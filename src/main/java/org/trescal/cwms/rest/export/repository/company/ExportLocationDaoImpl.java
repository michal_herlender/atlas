package org.trescal.cwms.rest.export.repository.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.Location_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.company.ExportLocationDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportLocationDaoImpl extends RestExportAbstractDaoImpl<ExportLocationDTO> {

	private Selection<ExportLocationDTO> getSelection(CriteriaBuilder cb, Root<Location> root) {
		Join<Location, Address> address = root.join(Location_.add);
		
		Selection<ExportLocationDTO> selection = cb.construct(ExportLocationDTO.class, 
				root.get(Location_.locationid),
				address.get(Address_.addrid),
				root.get(Location_.active),
				root.get(Location_.location)
			);
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportLocationDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportLocationDTO.class, cb -> cq -> {
			Root<Location> root = cq.from(Location.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Location_.locationid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Location_.locationid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.LOCATION;
	}

	@Override
	public List<ExportLocationDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportLocationDTO> cq = cb.createQuery(ExportLocationDTO.class); 
			Root<Location> root = cq.from(Location.class);
			cq.where(cb.greaterThan(root.get(Location_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}
	
}
