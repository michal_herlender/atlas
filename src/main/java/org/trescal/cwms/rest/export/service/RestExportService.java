package org.trescal.cwms.rest.export.service;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;

public interface RestExportService {
	public RestPagedResultDTO<?> getResults(RestExportEntityType entityType, Integer id, String[] locales, Integer resultsPerPage, Integer currentPage);
	
	public List<?> getLastModifiedResults(RestExportEntityType entityType, Date lastModifiedAfter);
}
