package org.trescal.cwms.rest.export.dto.job;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExportJobItemGroupDTO {
	
	private Integer id;
	private Boolean calibrationGroup;
	private Boolean deliveryGroup;
	private Integer jobid;

	//Constructor for JPA
	public ExportJobItemGroupDTO(Integer id, Boolean calibrationGroup, Boolean deliveryGroup, Integer jobid) {
		super();
		this.id = id;
		this.calibrationGroup = calibrationGroup;
		this.deliveryGroup = deliveryGroup;
		this.jobid = jobid;
	}

}
