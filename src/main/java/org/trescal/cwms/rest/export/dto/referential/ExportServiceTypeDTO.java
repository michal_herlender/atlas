package org.trescal.cwms.rest.export.dto.referential;

import org.trescal.cwms.core.instrumentmodel.DomainType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportServiceTypeDTO {
	public Integer servicetypeid;
	public String longname;
	public String shortname;
	public Boolean canbeperformedbyclient;
	public DomainType domaintype;
	public Integer orderby;
	public Boolean repair;
	
	// Used in JPA results
	public ExportServiceTypeDTO(Integer servicetypeid, String longname, String shortname,
			Boolean canbeperformedbyclient, DomainType domaintype, Integer orderby, Boolean repair) {
		super();
		this.servicetypeid = servicetypeid;
		this.longname = longname;
		this.shortname = shortname;
		this.canbeperformedbyclient = canbeperformedbyclient;
		this.domaintype = domaintype;
		this.orderby = orderby;
		this.repair = repair;
	}
}
