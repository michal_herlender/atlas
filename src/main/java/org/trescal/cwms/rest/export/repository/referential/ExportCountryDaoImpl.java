package org.trescal.cwms.rest.export.repository.referential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.referential.ExportCountryDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportCountryDaoImpl extends RestExportAbstractDaoImpl<ExportCountryDTO> {
	
	private Selection<ExportCountryDTO> getSelection(CriteriaBuilder cb, Root<Country> root) {
		Selection<ExportCountryDTO> selection = cb.construct(ExportCountryDTO.class, 
				root.get(Country_.countryid),
				root.get(Country_.country),
				root.get(Country_.countryCode)
				);
		
		return selection;
	}

	@Override
	public void getPagedResults(PagedResultSet<ExportCountryDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportCountryDTO.class, cb -> cq -> {
			Root<Country> root = cq.from(Country.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(Country_.countryid), id));
			}
			
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(Country_.countryid)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.COUNTRY;
	}

	@Override
	public List<ExportCountryDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("No lastModified on Country");
	}	
}
