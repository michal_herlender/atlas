package org.trescal.cwms.rest.export.dto.referential;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportBrandDTO {
	private Integer mfrid;
	private Boolean active;
	private Boolean genericmfr;
	private String name;
	private Integer tmlid;
	
	// Constructor for JPA results
	public ExportBrandDTO(Integer mfrid, Boolean active, Boolean genericmfr, String name, Integer tmlid) {
		super();
		this.mfrid = mfrid;
		this.active = active;
		this.genericmfr = genericmfr;
		this.name = name;
		this.tmlid = tmlid;
	}
}
