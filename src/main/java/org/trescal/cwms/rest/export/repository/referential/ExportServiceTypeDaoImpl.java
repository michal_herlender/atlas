package org.trescal.cwms.rest.export.repository.referential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.referential.ExportServiceTypeDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportServiceTypeDaoImpl extends RestExportAbstractDaoImpl<ExportServiceTypeDTO> {

	private Selection<ExportServiceTypeDTO> getSelection(CriteriaBuilder cb, Root<ServiceType> root) {
		Selection<ExportServiceTypeDTO> selection = cb.construct(ExportServiceTypeDTO.class, 
				root.get(ServiceType_.serviceTypeId),
				root.get(ServiceType_.longName),
				root.get(ServiceType_.shortName),
				root.get(ServiceType_.canBePerformedByClient),
				root.get(ServiceType_.domainType),
				root.get(ServiceType_.order),
				root.get(ServiceType_.repair)
				);
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportServiceTypeDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportServiceTypeDTO.class, cb -> cq -> {
			Root<ServiceType> root = cq.from(ServiceType.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(ServiceType_.serviceTypeId), id));
			}

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(ServiceType_.serviceTypeId)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.SERVICE_TYPE;
	}
	
	@Override
	public List<ExportServiceTypeDTO> getLastModifiedResults(Date lastModifiedAfter) {
		throw new UnsupportedOperationException("No lastModified on ServiceType");
	}	

}
