package org.trescal.cwms.rest.export.repository.instrument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;

import org.apache.commons.lang3.tuple.Triple;
import org.eclipse.birt.report.model.api.IllegalOperationException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange_;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.export.dto.instrument.ExportInstrumentRangeDTO;
import org.trescal.cwms.rest.export.repository.RestExportAbstractDaoImpl;
import org.trescal.cwms.rest.export.service.RestExportEntityType;

@Repository
public class ExportInstrumentRangeDaoImpl extends RestExportAbstractDaoImpl<ExportInstrumentRangeDTO> {

	private Selection<ExportInstrumentRangeDTO> getSelection(CriteriaBuilder cb, Root<InstrumentRange> root) {
		Join<InstrumentRange, UoM> uom = root.join(InstrumentRange_.uom, JoinType.INNER);
		Join<InstrumentRange, UoM> maxUom = root.join(InstrumentRange_.maxUom, JoinType.LEFT);
		Join<InstrumentRange, Instrument> instrument = root.join(InstrumentRange_.instrument, JoinType.INNER);
		
		Selection<ExportInstrumentRangeDTO> selection = cb.construct(ExportInstrumentRangeDTO.class, 
				root.get(InstrumentRange_.id),
				root.get(InstrumentRange_.end),
				root.get(InstrumentRange_.start),
				uom.get(UoM_.id),
				maxUom.get(UoM_.id),
				instrument.get(Instrument_.plantid)
			);
		
		return selection;
	}
	
	@Override
	public void getPagedResults(PagedResultSet<ExportInstrumentRangeDTO> prs, Integer id, List<Locale> locales) {
		completePagedResultSet(prs, ExportInstrumentRangeDTO.class, cb -> cq -> {
			Root<InstrumentRange> root = cq.from(InstrumentRange.class);
			
			if (id != null) {
				cq.where(cb.equal(root.get(InstrumentRange_.id), id));
			}

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(InstrumentRange_.id)));
			
			return Triple.of(root, getSelection(cb, root), order);
		});
	}

	@Override
	public RestExportEntityType getEntityType() {
		return RestExportEntityType.INSTRUMENT_RANGE;
	}

	@Override
	public List<ExportInstrumentRangeDTO> getLastModifiedResults(Date lastModifiedAfter) {
		if (lastModifiedAfter == null) throw new IllegalOperationException("lastModifiedAfter may not be null!");
		return getResultList(cb -> {
			CriteriaQuery<ExportInstrumentRangeDTO> cq = cb.createQuery(ExportInstrumentRangeDTO.class); 
			Root<InstrumentRange> root = cq.from(InstrumentRange.class);
			cq.where(cb.greaterThan(root.get(InstrumentRange_.lastModified), lastModifiedAfter));
			cq.select(getSelection(cb, root));
			return cq;
		});
	}	
}
