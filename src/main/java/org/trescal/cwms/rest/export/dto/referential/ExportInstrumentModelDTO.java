package org.trescal.cwms.rest.export.dto.referential;

import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportInstrumentModelDTO {
	private Integer modelid;
	private Integer descriptionid;
	private Integer mfrid;
	private String model;
	private ModelMfrType modelmfrtype;
	private Integer modeltypeid;
	private Boolean quarantined;
	private Integer tmlid;
	
	// Used in JPA results
	public ExportInstrumentModelDTO(Integer modelid, Integer descriptionid, Integer mfrid, String model,
			ModelMfrType modelmfrtype, Integer modeltypeid, Boolean quarantined, Integer tmlid) {
		super();
		this.modelid = modelid;
		this.descriptionid = descriptionid;
		this.mfrid = mfrid;
		this.model = model;
		this.modelmfrtype = modelmfrtype;
		this.modeltypeid = modeltypeid;
		this.quarantined = quarantined;
		this.tmlid = tmlid;
	}
}
