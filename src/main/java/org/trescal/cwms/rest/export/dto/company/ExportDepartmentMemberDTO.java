package org.trescal.cwms.rest.export.dto.company;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExportDepartmentMemberDTO {
	private Integer id;
	private Integer personid;
	private Integer deptid;

	// Used in JPA access
	public ExportDepartmentMemberDTO(Integer id, Integer personid, Integer deptid) {
		super();
		this.id = id;
		this.personid = personid;
		this.deptid = deptid;
	}
}
