package org.trescal.cwms.rest.tlm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class TLMRegisterCalibrationTimeInputDTO {

    @NotNull
    private Integer calibrationId;
    private String username;
    private Integer activityType; // TBD
    @NotNull
    private Date startDate;
    @NotNull
    private Integer timeSpent;
    private String remarks;
    private String hrid;

}
