package org.trescal.cwms.rest.tlm.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class TLMNextActivityInputDTO {

	@NotNull
	private Integer jobItemId;
	@NotNull
	private Integer activityId;
	private Integer actionOutcomeId;
	private Integer outcomeStatusId;
	@NotNull
	private String hrid;
	private Date startDate;
	private int timeSpent;
	private String remarks;

	public Integer getJobItemId() {
		return jobItemId;
	}

	public void setJobItemId(Integer jobitemId) {
		this.jobItemId = jobitemId;
	}

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public Integer getActionOutcomeId() {
		return actionOutcomeId;
	}

	public void setActionOutcomeId(Integer actionOutcomeId) {
		this.actionOutcomeId = actionOutcomeId;
	}

	public Integer getOutcomeStatusId() {
		return outcomeStatusId;
	}

	public void setOutcomeStatusId(Integer outcomeStatusId) {
		this.outcomeStatusId = outcomeStatusId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(Integer timeSpent) {
		this.timeSpent = timeSpent;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getHrid() {
		return hrid;
	}

	public void setHrid(String hrid) {
		this.hrid = hrid;
	}

}
