package org.trescal.cwms.rest.tlm.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportStateEnum;

import lombok.Data;

@Data
public class TLMFailureReportInputDTO {

	@NotNull
	private Integer jobItemId;
	private Date issueDate;
	private String trescalRef;
	private Integer estDeliveryTime;
	private Boolean operationByTrescal;
	private Boolean operationInLaboratory;
	private Integer subdivId;

	// technician part
	@NotNull
	private String technicianHrid;
	@NotNull
	private List<FailureReportStateEnum> states;
	private String technicianComments;
	private String otherState;

	// manager part
	private String managerHrid;
	private Boolean managerValidation;
	private String managerComments;
	private Date managerValidationOn;
	private List<FailureReportRecommendationsEnum> recommendations;

	private BigDecimal valueInBusinessCompanyCurrency;

	private Integer estAdjustTime;
	private Integer estRepairTime;
	private Boolean noCostingRequired;
	private Boolean outcomePreselected;
	private String dispensationComments;

	private Boolean sendToClient;
	private Boolean clientResponseNeeded;
	private Boolean clientApproval;
	private Date clientApprovalOn;
	private String clientComments;
	private String clientAlias;

	private Integer spentTime;
	private String remarks;

}
