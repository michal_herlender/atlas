package org.trescal.cwms.rest.tlm.service;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.external.adveso.events.CertificateStatusChangedEvent;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateReplacementType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectNextWorkRequirement;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SignCertificate;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_TakeOffHold;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.*;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Service
public class RestTLMCalibrationServiceImpl implements RestTLMCalibrationService {

	private static final String TECHNICAL_CANCEL_AND_REPLACE_REMARK = "[Technical Cancel and Replace] re-run work requiement : ";
	@Autowired
	private HookInterceptor_TakeOffHold interceptor_takeOffHold;
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private CalibrationProcessService calibrationProcessService;
	@Autowired
	private ActionOutcomeService actionOutcomeServ;
	@Autowired
	private UserService userService;
	@Autowired
	private StatusService statusService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private JobItemWorkRequirementService jobItemWorkRequirementService;
	@Autowired
	private HookInterceptor_SelectNextWorkRequirement hookInterceptor_SelectNextWorkRequirement;
	@Autowired
	private HookInterceptor_SignCertificate interceptor_signCertificate;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@Override
	public RestResultDTO<TLMStartCalibrationOutputDTO> startCalibrationForTLM(TLMStartCalibrationInputDTO inputDTO) {

		// start calibration
		RestResultDTO<TLMStartCalibrationOutputDTO> dto = new RestResultDTO<>(true, "");
		TLMStartCalibrationOutputDTO result = new TLMStartCalibrationOutputDTO();

		Contact startedBy;
		if (inputDTO.getUsername() != null)
			startedBy = this.userService.get(inputDTO.getUsername()).getCon();
		else
			startedBy = this.contactService.getByHrId(inputDTO.getHrid());

		List<JobItemWorkRequirement> jobItemWorkRequirements = inputDTO.getWorkRequirementIds().stream().map(id -> this.jobItemWorkRequirementService
			.findJobItemWorkRequirement(id)).collect(Collectors.toList());

		CalibrationProcess calProcess;
		if (inputDTO.getProcessID() == null)
			calProcess = this.calibrationProcessService.findByName("TLM");
		else
			calProcess = this.calibrationProcessService.get(inputDTO.getProcessID());

		CalibrationClass calClass;
		if (inputDTO.getCalDataContent() == null)
			calClass = CalibrationClass.AS_FOUND_AND_AS_LEFT;
		else
			calClass = CalibrationClass.values()[inputDTO.getCalDataContent()];

		Pair<Calibration, Certificate> res = this.calibrationService.startCalibrationForTLM(jobItemWorkRequirements,
				startedBy, calProcess, calClass, inputDTO.getCertificateSupplementaryFor(), inputDTO.getStartDate(),
				inputDTO.getStartDate(), null);

		result.setCalibrationId(res.getLeft().getId());
		if (res.getRight() != null)
			result.setDocumentNumber(res.getRight().getCertno());
		dto.addResult(result);

		return dto;
	}

	@Override
	public RestResultDTO<String> registerCalibrationTime(TLMRegisterCalibrationTimeInputDTO inputDTO) {

		Calibration calibration = calibrationService.findCalibration(inputDTO.getCalibrationId());
		Date startDate = inputDTO.getStartDate() != null ? inputDTO.getStartDate() : new Date();
		Contact startedBy;
		if (inputDTO.getUsername() != null)
			startedBy = this.userService.get(inputDTO.getUsername()).getCon();
		else
			startedBy = this.contactService.getByHrId(inputDTO.getHrid());

		CalibrationStatus onGoingCalStatus = (CalibrationStatus) this.statusService
				.findStatusByName(CalibrationStatus.ON_GOING, CalibrationStatus.class);

		CalibrationStatus onHoldCalStatus = (CalibrationStatus) statusService
				.findStatusByName(CalibrationStatus.ON_HOLD, CalibrationStatus.class);
		// do register time
		ItemActivity postOnSiteCalibrationActivity = calibrationService.getCalibrationTypeItemActivity(true, false);
		ItemActivity preOnsiteCalibrationActivity = calibrationService.getCalibrationTypeItemActivity(true, true);
		calibrationService.registerCalibrationTime(calibration, startDate, inputDTO.getTimeSpent(), null, startedBy,
				inputDTO.getRemarks(), onHoldCalStatus, onGoingCalStatus, postOnSiteCalibrationActivity,
				preOnsiteCalibrationActivity);

		return new RestResultDTO<>(true, "");
	}

	@Override
	public RestResultDTO<String> completeCalibration(TLMCompleteCalibrationInputDTO inputDTO) {

		/* get technician */
		Contact contact;
		if (inputDTO.getUsername() != null)
			contact = this.userService.get(inputDTO.getUsername()).getCon();
		else
			contact = this.contactService.getByHrId(inputDTO.getHrid());

		Certificate certificate = certificateService.findCertificateByCertNo(inputDTO.getCertificateNumber());
		Calibration calibration = calibrationService.findCalibration(certificate.getCal().getId());

		/* Update certificate */
		if (inputDTO.getCalibrationDate() != null)
			certificate.setCalDate(inputDTO.getCalibrationDate());
		if (StringUtils.isNotBlank(inputDTO.getTlmCertificateNumber()))
			certificate.setThirdCertNo(inputDTO.getTlmCertificateNumber());
		certificate.setRemarks(inputDTO.getCalRemarks());
		certificate.setSignedBy(contact);
		JobItem ji = calibration.getLinks().stream().findFirst().map(CalLink::getJi).orElse(null);
		if (BooleanUtils.isTrue(inputDTO.getCancelCertificate())) {
			CertificateStatusChangedEvent event = new CertificateStatusChangedEvent(this, ji, certificate,
				certificate.getStatus(), CertStatusEnum.TO_BE_REPLACED);
			applicationEventPublisher.publishEvent(event);
			certificate.setStatus(CertStatusEnum.TO_BE_REPLACED);
		} else if (BooleanUtils.isTrue(inputDTO.getIsCertSigned())) {
			CertificateStatusChangedEvent event = new CertificateStatusChangedEvent(this, ji, certificate,
				certificate.getStatus(), CertStatusEnum.SIGNED);
			applicationEventPublisher.publishEvent(event);
			certificate.setStatus(CertStatusEnum.SIGNED);
		}
		certificate.setCalibrationVerificationStatus(
				CalibrationVerificationStatus.valueOf(inputDTO.getCalVerificationStatus()));
		if (inputDTO.getAdjustment() != null)
			certificate.setAdjustment(inputDTO.getAdjustment());
		if (inputDTO.getOptimization() != null)
			certificate.setOptimization(inputDTO.getOptimization());
		if (inputDTO.getRestriction() != null)
			certificate.setRestriction(inputDTO.getRestriction());
		if (inputDTO.getRepair() != null)
			certificate.setRepair(inputDTO.getRepair());

		// certificate notes
		if (CollectionUtils.isNotEmpty(inputDTO.getNotes())) {
			List<CertNote> notes = inputDTO.getNotes().stream().map(n -> {
				CertNote cn = new CertNote();
				cn.setLabel(n.getLabel());
				cn.setNote(n.getNote());
				cn.setCert(certificate);
				cn.setActive(true);
				cn.setPublish(false);
				cn.setSetOn(new Date());
				cn.setSetBy(contact);
				return cn;
			}).collect(Collectors.toList());
			Set<CertNote> notesSet = new TreeSet<>(new NoteComparator());
			notesSet.addAll(notes);
			certificate.setNotes(notesSet);
		}

		// calibration class
		CalibrationClass calClass = getCalClass(inputDTO.getCalibrationDataContent());
		calibration.setCalClass(calClass);

		// complete calibration
		ActionOutcome actionOutcome = this.actionOutcomeServ.get(inputDTO.getCalibrationOutcomeId());
		this.calibrationService.completeCalibrationForTLM(calibration, contact, inputDTO.getCalibrationEndDate(),
			inputDTO.getCalibrationDate(), actionOutcome, inputDTO.getNextCalDate(), false);
		if (inputDTO.getCancelCertificate() == null || !inputDTO.getCancelCertificate()) {
			HookInterceptor_SignCertificate.Input input = new HookInterceptor_SignCertificate.Input(certificate,
				contact, 0, inputDTO.getCalibrationEndDate());
			interceptor_signCertificate.recordAction(input);
		}

		// insert FR if available
		TLMFailureReportInputDTO frDto = inputDTO.getFailureReport();
		if (frDto != null) {
			if (frDto.getJobItemId() == null) {
				assert ji != null;
				frDto.setJobItemId(ji.getJobItemId());
			}
			if (frDto.getTechnicianHrid() == null) {
				frDto.setTechnicianHrid(inputDTO.getHrid());
			}
			if (frDto.getManagerHrid() == null) {
				frDto.setManagerHrid(inputDTO.getHrid());
			}
			faultReportService.createFailureReportForTLMInterface(frDto);
		}

		return new RestResultDTO<>(true, "");
	}

	@Override
	public RestResultDTO<String> cancelCalibration(TLMCancelCalibrationInputDTO inputDTO) {

		Calibration cal = calibrationService.findCalibration(inputDTO.getCalibrationId());
		Contact contact = contactService.getByHrId(inputDTO.getHrid());
		JobItem ji = cal.getLinks().stream().findFirst().map(CalLink::getJi).orElse(null);

		// take if off hold, if it is on hold
		assert ji != null;
		boolean isOnHold = jobItemService.stateHasGroupOfKeyName(ji.getState(), StateGroup.ONHOLD);
		if (isOnHold) {
			HookInterceptor_TakeOffHold.Input input = new HookInterceptor_TakeOffHold.Input(ji, null);
			interceptor_takeOffHold.recordAction(input);
		}

		if (ji.getState().getDescription().equalsIgnoreCase("Pre-calibration on hold")) {
			calibrationService.resumeCalibration(cal, null, null, null, (CalibrationStatus) this.statusService
				.findStatusByName(CalibrationStatus.ON_GOING, CalibrationStatus.class));
		}

		// cancel calibration
		calibrationService.cancelCalibration(inputDTO.getCalibrationId(), contact);

		cal.setCompletedBy(contact);
		cal.setCompleteTime(inputDTO.getCancelDate());
		calibrationService.updateCalibration(cal);

		// cancel linked certificates
		cal.getCerts().forEach(cert -> {
			cert.setStatus(CertStatusEnum.CANCELLED);
			certificateService.updateCertificate(cert);
		});

		return new RestResultDTO<>(true, "");
	}

	private CalibrationClass getCalClass(int ordinal) {
		if (ordinal < 0)
			return null;
		if (ordinal >= CalibrationClass.class.getEnumConstants().length)
			return null;
		return CalibrationClass.class.getEnumConstants()[ordinal];
	}

	@Override
	public JobItemWorkRequirement technicalCancelAndReplace(TLMCancelAndReplaceCertificateInputDTO dto) {

		// duplicate jiwr and wr
		JobItemWorkRequirement jiwr = jobItemWorkRequirementService
			.findJobItemWorkRequirement(dto.getWorkRequirementId());
		JobItemWorkRequirement duplicatedJiwr = jobItemWorkRequirementService.duplicate(jiwr);

		// set it as next wr
		JobItem ji = duplicatedJiwr.getJobitem();
		jobItemService.setNextWorkRequirement(ji, duplicatedJiwr);

		String remark;
		if (StringUtils.isNotBlank(dto.getReplacementComments()))
			remark = dto.getReplacementComments();
		else
			remark = TECHNICAL_CANCEL_AND_REPLACE_REMARK + jiwr.getWorkRequirement().getCapability().getReference();
		org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectNextWorkRequirement.Input input = new org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectNextWorkRequirement.Input(
			ji, remark);
		// go to the next wr in workflow
		hookInterceptor_SelectNextWorkRequirement.recordAction(input);

		Certificate certificate = certificateService.findCertificateByCertNo(dto.getCertificateNumber());
		// set certificate as to_be_replaced
		certificate.setStatus(CertStatusEnum.TO_BE_REPLACED);

		// set replacement comments
		certificate.setSupplementaryCertificateComments(dto.getReplacementComments());

		return duplicatedJiwr;
	}

	@Override
	public JobItemWorkRequirement certificateReplacement(TLMCancelAndReplaceCertificateInputDTO inputDTO) {
		
		JobItemWorkRequirement jiwr = jobItemWorkRequirementService.findJobItemWorkRequirement(inputDTO.getWorkRequirementId());
		Certificate certificate = certificateService.findCertificateByCertNo(inputDTO.getCertificateNumber());
		CertificateReplacementType certType = jiwr.getJobitem().getJob().getType().equals(JobType.STANDARD) ? CertificateReplacementType.LAB_CERT :
																											  CertificateReplacementType.ONSITE_CERT;
		return this.certificateService.performCertificateReplacement(jiwr.getJobitem(), jiwr.getWorkRequirement(), certificate, certType);
	}

}
