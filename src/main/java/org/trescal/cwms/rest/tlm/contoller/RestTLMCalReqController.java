package org.trescal.cwms.rest.tlm.contoller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMCalReqDTO;
import org.trescal.cwms.rest.tlm.dto.transformer.TLMCalReqDTOTransformer;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMCalReqController {

	@Autowired
	private TLMCalReqDTOTransformer transformer;
	@Autowired
	private CalReqService calReqService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private MessageSource messageSource;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/calreq/{id}", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMCalReqDTO> getCalReqById(@PathVariable(required = true) int id, Locale locale) {

		CalReq cr = calReqService.findCalReq(id);
		if (cr == null) {
			return new RestResultDTO<>(false, null);
		}

		TLMCalReqDTO dto = transformer.convertSingle(cr);
		RestResultDTO<TLMCalReqDTO> result = new RestResultDTO<>(true, "");
		result.addResult(dto);

		return result;

	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/calreq", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMCalReqDTO> getCalReq(@RequestParam(required = false) Integer jobitemid,
			@RequestParam(required = false) Integer plantid, Locale locale) {

		if (jobitemid != null) {
			JobItem ji = jobItemService.findJobItem(jobitemid);
			if (ji == null) {
				String message = messageSource.getMessage(RestErrors.CODE_DATA_NOT_FOUND, new Object[] {},
						RestErrors.MESSAGE_DATA_NOT_FOUND, locale);
				return new RestResultDTO<>(false, message);
			}

			CalReq cr = calReqService.findCalReqsForJobItem(ji);
			if (cr == null) {
				return new RestResultDTO<>(true, null);
			}

			TLMCalReqDTO dto = transformer.convertSingle(cr);
			dto.setJobItemId(ji.getJobItemId());
			dto.setJobItemNo(ji.getItemNo());
			dto.setPlantId(ji.getInst().getPlantid());
			dto.setPlantNumber(ji.getInst().getPlantno());
			dto.setSerialNumber(ji.getInst().getSerialno());

			RestResultDTO<TLMCalReqDTO> result = new RestResultDTO<>(true, "");
			result.addResult(dto);

			return result;

		} else if (plantid != null) {
			Instrument inst = instrumentService.get(plantid);
			if (inst == null) {
				String message = messageSource.getMessage(RestErrors.CODE_DATA_NOT_FOUND, new Object[] {},
						RestErrors.MESSAGE_DATA_NOT_FOUND, locale);
				return new RestResultDTO<>(false, message);
			}
			CalReq cr = calReqService.findCalReqsForInstrument(inst);
			if (cr == null) {
				return new RestResultDTO<>(true, null);
			}

			TLMCalReqDTO dto = transformer.convertSingle(cr);
			dto.setPlantId(inst.getPlantid());
			dto.setPlantNumber(inst.getPlantno());
			dto.setSerialNumber(inst.getSerialno());

			RestResultDTO<TLMCalReqDTO> result = new RestResultDTO<>(true, "");
			result.addResult(dto);

			return result;
		} else if (jobitemid == null && plantid == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOBITEMID_BLANK, new Object[] {},
					RestErrors.MESSAGE_JOBITEMID_BLANK, locale);
			return new RestResultDTO<>(false, message);
		}

		return null;
	}

}
