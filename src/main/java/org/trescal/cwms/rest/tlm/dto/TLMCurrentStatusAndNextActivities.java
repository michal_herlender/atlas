package org.trescal.cwms.rest.tlm.dto;

import java.util.List;

public class TLMCurrentStatusAndNextActivities {

	private int currentStatusId;
	private String currentStatus;
	private int jobitemId;
	private List<TLMNextActivityDTO> nextActivities;

	public int getCurrentStatusId() {
		return currentStatusId;
	}

	public void setCurrentStatusId(int currentStatusId) {
		this.currentStatusId = currentStatusId;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public int getJobitemId() {
		return jobitemId;
	}

	public void setJobitemId(int jobitemId) {
		this.jobitemId = jobitemId;
	}

	public List<TLMNextActivityDTO> getNextActivities() {
		return nextActivities;
	}
	
	public void setNextActivities(List<TLMNextActivityDTO> nextActivities) {
		this.nextActivities = nextActivities;
	}
}
