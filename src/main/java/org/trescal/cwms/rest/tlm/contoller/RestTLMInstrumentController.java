package org.trescal.cwms.rest.tlm.contoller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMInstrumentDTO;
import org.trescal.cwms.rest.tlm.dto.transformer.TLMInstrumentDTOTransformer;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMInstrumentController {
	
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private TLMInstrumentDTOTransformer transformer;
	@Autowired
	private MessageSource messageSource;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/instrument/{id}", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMInstrumentDTO> getInstruments(@PathVariable(required=true) int id, Locale locale) {

		// check if instrument exists
		Instrument instrument = instrumentService.get(id);
		if (instrument == null) {
			String message = messageSource.getMessage(RestErrors.CODE_INSTRUMENT_NOT_FOUND, new Object[] {id},
					RestErrors.MESSAGE_INSTRUMENT_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}

		TLMInstrumentDTO dto = transformer.convertSingle(instrument);
		RestResultDTO<TLMInstrumentDTO> result = new RestResultDTO<>(true,"");
		result.addResult(dto);

		return result;
	}

}