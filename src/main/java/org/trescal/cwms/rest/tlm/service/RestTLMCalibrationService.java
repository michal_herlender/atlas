package org.trescal.cwms.rest.tlm.service;

import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMCancelAndReplaceCertificateInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMCancelCalibrationInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMCompleteCalibrationInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMRegisterCalibrationTimeInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMStartCalibrationInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMStartCalibrationOutputDTO;

public interface RestTLMCalibrationService {

	RestResultDTO<String> registerCalibrationTime(TLMRegisterCalibrationTimeInputDTO inputDTO);

	RestResultDTO<String> completeCalibration(TLMCompleteCalibrationInputDTO inputDTO);

	RestResultDTO<String> cancelCalibration(TLMCancelCalibrationInputDTO inputDTO);

	JobItemWorkRequirement technicalCancelAndReplace(TLMCancelAndReplaceCertificateInputDTO inputDTO);

	RestResultDTO<TLMStartCalibrationOutputDTO> startCalibrationForTLM(TLMStartCalibrationInputDTO inputDTO);
	
	JobItemWorkRequirement certificateReplacement(TLMCancelAndReplaceCertificateInputDTO inputDTO);

}
