package org.trescal.cwms.rest.tlm.dto.transformer;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.rest.tlm.dto.TLMEngineerAllocationDTO;
import org.trescal.cwms.rest.tlm.utils.TLMTransformer;

@Component
public class TLMEngineerAllocationDTOTransformer
		implements TLMTransformer<EngineerAllocation, TLMEngineerAllocationDTO> {

	@Override
	public TLMEngineerAllocationDTO convertSingle(EngineerAllocation entity) {
		TLMEngineerAllocationDTO dto = new TLMEngineerAllocationDTO();
		dto.setPlannedCalDate(entity.getAllocatedFor());
		dto.setTechFirstName(entity.getAllocatedTo().getFirstName());
		dto.setTechLastName(entity.getAllocatedTo().getLastName());
		dto.setTechUserName(entity.getAllocatedTo().getUser().getUsername());
		dto.setTechHrId(entity.getAllocatedTo().getHrid());
		return dto;
	}

}
