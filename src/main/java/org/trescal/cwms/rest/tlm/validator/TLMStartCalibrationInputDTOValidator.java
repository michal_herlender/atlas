package org.trescal.cwms.rest.tlm.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.tlm.dto.TLMStartCalibrationInputDTO;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;

@Component
public class TLMStartCalibrationInputDTOValidator extends AbstractBeanValidator {

	@Autowired
	private UserService userService;
	@Autowired
	private JobItemWorkRequirementService jobItemWorkRequirementService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobItemActionService jiActionService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ContactService contactService;

	@Override
	public boolean supports(Class<?> clazz) {
		return TLMStartCalibrationInputDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		TLMStartCalibrationInputDTO dto = (TLMStartCalibrationInputDTO) target;

		if (StringUtils.isEmpty(dto.getUsername()) && StringUtils.isEmpty(dto.getHrid())) {
			errors.reject(RestErrors.CODE_USERNAME_AND_HRID_EMPTY, RestErrors.MESSAGE_USERNAME_AND_HRID_EMPTY);
		}

		// Look up username
		if (StringUtils.isNotEmpty(dto.getUsername())) {
			User u = this.userService.get(dto.getUsername());
			Contact startBy = u.getCon();
			if (u == null || startBy == null) {
				errors.rejectValue("username", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { dto.getHrid() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		// lookup hrid
		if (StringUtils.isNotEmpty(dto.getHrid())) {
			Contact c = this.contactService.getByHrId(dto.getHrid());
			if (c == null) {
				errors.rejectValue("hrid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { dto.getUsername() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		// Lookup Work Requirements
		StringJoiner notFoundWrIdsjoiner = new StringJoiner(", ");
		dto.getWorkRequirementIds().stream().filter(id -> {
			return id != null && id != 0;
		}).forEach(id -> {
			JobItemWorkRequirement jobitemWorkRequirement = this.jobItemWorkRequirementService
					.findJobItemWorkRequirement(id);
			if (jobitemWorkRequirement == null) {
				notFoundWrIdsjoiner.add(id + "");
			}
		});
		if (notFoundWrIdsjoiner.length() != 0) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_WR_NOT_FOUND,
					new Object[] { notFoundWrIdsjoiner.toString() }, RestErrors.MESSAGE_WR_NOT_FOUND);
			return;
		}

		// check if all workrequirements are startable
		dto.getWorkRequirementIds().stream().forEach(id -> {
			JobItemWorkRequirement jobitemWorkRequirement = this.jobItemWorkRequirementService
					.findJobItemWorkRequirement(id);
			if (jobitemWorkRequirement != null && !isJobItemWorkRequirementStartable(jobitemWorkRequirement)) {
				getNotStartableReason(jobitemWorkRequirement, errors);
			}
		});

		// check if all workrequirements are on the same job
		Set<Integer> jobIds = new HashSet<>();
		dto.getWorkRequirementIds().stream().forEach(id -> {
			JobItemWorkRequirement jiwr = this.jobItemWorkRequirementService.findJobItemWorkRequirement(id);
			jobIds.add(jiwr.getJobitem().getJob().getJobid());
		});
		if (jobIds.size() != 1) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_WR_DIFFERENT_JOBS,
					new Object[] { notFoundWrIdsjoiner.toString() }, RestErrors.MESSAGE_WR_DIFFERENT_JOBS);
		}

		// check if all instruments associated with workrequirements have the
		// same cal frequency and unit
		boolean haveSameCalFreqAndUnit = true;
		if (dto.getWorkRequirementIds().size() > 1) {
			Optional<Integer> firstId = dto.getWorkRequirementIds().stream().findFirst();
			JobItemWorkRequirement firstJiwr = this.jobItemWorkRequirementService
					.findJobItemWorkRequirement(firstId.get());
			Instrument firstInst = firstJiwr.getJobitem().getInst();
			int firstCalIntervalInDays = firstInst.getCalFrequencyUnit().toDays(firstInst.getCalFrequency());
			for (Integer id : dto.getWorkRequirementIds()) {
				JobItemWorkRequirement jiwr = this.jobItemWorkRequirementService.findJobItemWorkRequirement(id);
				Instrument inst = jiwr.getJobitem().getInst();
				int calIntervalInDays = inst.getCalFrequencyUnit().toDays(inst.getCalFrequency());
				if (firstCalIntervalInDays != calIntervalInDays) {
					haveSameCalFreqAndUnit = false;
					break;
				}
			}
		}

		if (!haveSameCalFreqAndUnit) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_WR_DIFFERENT_CAL_FREQ,
					new Object[] { notFoundWrIdsjoiner.toString() }, RestErrors.MESSAGE_WR_DIFFERENT_CAL_FREQ);
		}

		// check if jobitem status is stratable
		for (Integer wrid : dto.getWorkRequirementIds()) {
			JobItemWorkRequirement jobitemWorkRequirement = this.jobItemWorkRequirementService
					.findJobItemWorkRequirement(wrid);
			JobItem ji = jobitemWorkRequirement.getJobitem();
		}

		if (StringUtils.isNotBlank(dto.getCertificateSupplementaryFor())) {
			Certificate supplementaryFor = certificateService
					.findCertificateByCertNo(dto.getCertificateSupplementaryFor());
			if (supplementaryFor == null)
				errors.rejectValue("certificateSupplementaryFor", RestErrors.CODE_CERTIFICATE_NOT_FOUND,
						new Object[] { dto.getCertificateSupplementaryFor() },
						RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND);
		}

	}

	public boolean isJobItemWorkRequirementStartable(JobItemWorkRequirement jiwr) {

		// can start conditions
		// have a state of AWAITING CALIBRATION or RESUME CALIBRATION
		boolean condition1 = jobItemService.stateHasGroupOfKeyName(jiwr.getJobitem().getState(),
				StateGroup.AWAITINGCALIBRATION);
		// the Work Requirement has one procedure at least
		boolean condition2 = jiwr.getWorkRequirement().getCapability() != null;
		// the Work Requirement must not be complete or cancelled
		boolean condition3 = !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
				&& !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED);
		// The the job item calibration type must have a recall requirement type
		// of FULL_CALIBRATION
		boolean condition4 = jiwr.getWorkRequirement().getServiceType().getCalibrationType()
				.getRecallRequirementType() == RecallRequirementType.FULL_CALIBRATION;
		// if the jobitem have a current location
		boolean condition5 = jiwr.getJobitem().getCurrentAddr() != null;
		
		// to deal with certificate cancel and replace case
		boolean condition6 = false,condition7 = false;
		if(!condition5) {
			JobItemAction lastJia = this.jiActionService.findLastActiveActionForItem(jiwr.getJobitem());
			condition6 = jobItemService.stateHasGroupOfKeyName(lastJia.getStartStatus(),
					StateGroup.START_LAB_CERTIFICATE_REPLACEMENT);
			condition7 = jobItemService.stateHasGroupOfKeyName(lastJia.getStartStatus(),
					StateGroup.START_ONSITE_CERTIFICATE_REPLACEMENT);
		}

		return condition1 && condition2 && condition3 && condition4 && (condition5 || condition6 || condition7);
	}

	private void getNotStartableReason(JobItemWorkRequirement jiwr, Errors errors) {

		// have a state of AWAITING CALIBRATION or RESUME CALIBRATION condition
		boolean condition1 = jobItemService.stateHasGroupOfKeyName(jiwr.getJobitem().getState(),
				StateGroup.AWAITINGCALIBRATION);
		if (!condition1) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_WR_NOT_AWAITING_CALIBATION,
					new Object[] { jiwr.getId() }, RestErrors.MESSAGE_WR_NOT_AWAITING_CALIBATION);
		}

		// the Work Requirement has one procedure at least condition
        boolean condition2 = jiwr.getWorkRequirement().getCapability() != null;
		if (!condition2) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_WR_NO_PROCEDURE, new Object[] { jiwr.getId() },
					RestErrors.MESSAGE_WR_NO_PROCEDURE);
		}

		// the Work Requirement must not be complete condition
		boolean condition3 = !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE);
		if (!condition3) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_WR_COMPLETE, new Object[] { jiwr.getId() },
					RestErrors.MESSAGE_WR_COMPLETE);
		}

		// The the job item calibration type must have a recall requirement type
		// of FULL_CALIBRATION
		boolean condition4 = jiwr.getWorkRequirement().getServiceType().getCalibrationType()
				.getRecallRequirementType() == RecallRequirementType.FULL_CALIBRATION;
		if (!condition4) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_WR_NOT_FULLCALIBRATION,
					new Object[] { jiwr.getId() }, RestErrors.MESSAGE_WR_NOT_FULLCALIBRATION);
		}

		// if the jobitem have a current location
		boolean condition5 = jiwr.getJobitem().getCurrentAddr() != null;
		if (!condition5) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_JOBITEM_CURRENT_ADDRESS_NULL,
					new Object[] { jiwr.getJobitem().getJobItemId() }, RestErrors.MESSAGE_JOBITEM_CURRENT_ADDRESS_NULL);
		}
		
		// the Work Requirement must not be complete condition
		boolean condition6 = !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED);
		if (!condition6) {
			errors.rejectValue("workRequirementIds", RestErrors.CODE_WR_CANCELLED, new Object[] { jiwr.getId() },
					RestErrors.MESSAGE_WR_CANCELLED);
		}

	}

}
