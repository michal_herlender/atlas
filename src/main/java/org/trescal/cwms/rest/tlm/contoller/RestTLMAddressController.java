package org.trescal.cwms.rest.tlm.contoller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMAddressProjectionDTO;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMAddressController {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private MessageSource messageSource;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/address", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestPagedResultDTO<TLMAddressProjectionDTO> getAddresses(
			@RequestParam(required = false, defaultValue = "1") int page,
			@RequestParam(required = false, defaultValue = "25") int resultsPerPage,
			@RequestParam(required = true) int busCoId,
			@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(required = false) String companyLastModifiedDate,
			@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(required = false) String subdivLastModifiedDate,
			@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(required = false) String addressLastModifiedDate,
			Locale locale) throws ParseException {

		// check if company exists
		Company company = companyService.get(busCoId);
		if (company == null) {
			String message = messageSource.getMessage(RestErrors.CODE_COMPANY_NOT_FOUND, new Object[] {},
					RestErrors.MESSAGE_COMPANY_NOT_FOUND, locale);
			return new RestPagedResultDTO<>(false, message, 1, 1);
		}

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		Timestamp companyLastModifiedDateTime = null;
		if (companyLastModifiedDate != null)
			companyLastModifiedDateTime = new Timestamp(formatter.parse(companyLastModifiedDate).getTime());
		Timestamp subdivLastModifiedDateTime = null;
		if (subdivLastModifiedDate != null)
			subdivLastModifiedDateTime = new Timestamp(formatter.parse(subdivLastModifiedDate).getTime());
		Timestamp addressLastModifiedDateTime = null;
		if (addressLastModifiedDate != null)
			addressLastModifiedDateTime = new Timestamp(formatter.parse(addressLastModifiedDate).getTime());
		
		// get results
		PagedResultSet<TLMAddressProjectionDTO> pagedResults = addressService.getAllCompanyAddresses(company,
				Arrays.asList(CompanyRole.BUSINESS, CompanyRole.CLIENT), companyLastModifiedDateTime,
				subdivLastModifiedDateTime, addressLastModifiedDateTime, resultsPerPage, page);

		// convert and construct response
		RestPagedResultDTO<TLMAddressProjectionDTO> results = new RestPagedResultDTO<>(true, "",
				pagedResults.getResultsPerPage(), pagedResults.getCurrentPage());
		pagedResults.getResults().stream().forEach(a -> {
			results.addResult(a);
		});
		results.setResultsCount(pagedResults.getResultsCount());

		return results;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/address/{addrid}", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMAddressProjectionDTO> getAddresseById(@PathVariable(required = true) int addrid)
			throws ParseException {

		// get results
		TLMAddressProjectionDTO address = addressService.getAddresseById(addrid);

		RestResultDTO<TLMAddressProjectionDTO> res = new RestResultDTO<>(true, "");
		res.addResult(address);

		return res;
	}

}
