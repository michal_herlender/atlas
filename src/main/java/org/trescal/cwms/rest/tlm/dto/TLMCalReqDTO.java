package org.trescal.cwms.rest.tlm.dto;

import java.util.List;

public class TLMCalReqDTO {

	private Integer jobItemId;
	private Integer JobItemNo;
	private Integer plantId;
	private String plantNumber;
	private String serialNumber;
	
	private String privateInstructions;
	private String publicInstructions;
	private List<TLMPointDTO> points;
	private String range;
	private String relatedPointRange;
	private TLMUomDTO relatedUomRange;
	private boolean active;
	
	public Integer getJobItemId() {
		return jobItemId;
	}
	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}
	public Integer getJobItemNo() {
		return JobItemNo;
	}
	public void setJobItemNo(Integer jobItemNo) {
		JobItemNo = jobItemNo;
	}
	public Integer getPlantId() {
		return plantId;
	}
	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}
	public String getPlantNumber() {
		return plantNumber;
	}
	public void setPlantNumber(String plantNumber) {
		this.plantNumber = plantNumber;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getPrivateInstructions() {
		return privateInstructions;
	}
	public void setPrivateInstructions(String privateInstructions) {
		this.privateInstructions = privateInstructions;
	}
	public String getPublicInstructions() {
		return publicInstructions;
	}
	public void setPublicInstructions(String publicInstructions) {
		this.publicInstructions = publicInstructions;
	}
	public List<TLMPointDTO> getPoints() {
		return points;
	}
	public void setPoints(List<TLMPointDTO> points) {
		this.points = points;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public String getRelatedPointRange() {
		return relatedPointRange;
	}
	public void setRelatedPointRange(String relatedPointRange) {
		this.relatedPointRange = relatedPointRange;
	}
	public TLMUomDTO getRelatedUomRange() {
		return relatedUomRange;
	}
	public void setRelatedUomRange(TLMUomDTO relatedUomRange) {
		this.relatedUomRange = relatedUomRange;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}
