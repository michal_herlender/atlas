package org.trescal.cwms.rest.tlm.utils;

import java.util.List;
import java.util.stream.Collectors;

public interface TLMTransformer<ENTITY,DTO> {
	
	public default List<DTO> convertList(List<ENTITY> entities){
		return entities.stream().map(company -> {
			return convertSingle(company);
		}).collect(Collectors.toList());
	}
	
	public DTO convertSingle(ENTITY entity);

}
