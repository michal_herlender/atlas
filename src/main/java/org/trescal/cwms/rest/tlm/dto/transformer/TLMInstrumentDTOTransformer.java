package org.trescal.cwms.rest.tlm.dto.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.rest.tlm.dto.TLMInstrumentDTO;
import org.trescal.cwms.rest.tlm.utils.TLMTransformer;

import java.util.Locale;

@Component
public class TLMInstrumentDTOTransformer implements TLMTransformer<Instrument, TLMInstrumentDTO> {

	@Autowired
	private CalReqService calReqService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private TranslationService translationService;

	@Override
	public TLMInstrumentDTO convertSingle(Instrument instrument) {

		Locale locale = new Locale("FR");

		TLMInstrumentDTO dto = new TLMInstrumentDTO();
		dto.setAddrId(instrument.getAdd().getAddrid());
		dto.setCalInterval(instrument.getCalFrequency());
		dto.setCalIntervalUnit(instrument.getCalFrequencyUnit());
		dto.setCoId(instrument.getAdd().getSub().getComp().getCoid());
		dto.setCustomerDescription(instrument.getCustomerDescription());
		dto.setFormerBarcode(instrument.getFormerBarCode());
		InstrumentCalReq calreq = calReqService.findInstrumentCalReqForInstrument(instrument);
		if (calreq != null) {
			dto.setPublicInstructions(calreq.getPublicInstructions());
			dto.setPrivateInstructions(calreq.getPrivateInstructions());
		}
		JobItem ji = jobItemService.findActiveJobItemFromBarcode(instrument.getPlantid());
		if (ji != null)
			dto.setJobItemId(ji.getJobItemId());
		Calibration calibration = instrument.getLastCal();
		if (calibration != null)
			dto.setLastCalDate(calibration.getCalDate());
		dto.setLastModified(instrument.getLastModified());
		dto.setMfrName(instrument.getDefinitiveMfrName());
		if (instrument.getMfr() != null) {
			dto.setMfrTMLId(instrument.getMfr().getTmlid());
		} else {
            dto.setMfrTMLId(instrument.getModel().getMfr().getTmlid());
        }

        dto.setModelId(instrument.getModel().getModelid());
        dto.setModelMfrType(instrument.getModel().getModelMfrType());
        if (instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
            dto.setModelName(instrument.getModelname());
        } else {
            dto.setModelName(instrument.getModel().getModel());
        }

        dto.setNextCalDate(DateTools.dateFromLocalDate(instrument.getNextCalDueDate()));
        dto.setPlantId(instrument.getPlantid());
        dto.setPlantNumber(instrument.getPlantno());
        dto.setSerialNumber(instrument.getSerialno());
        dto.setSubdivId(instrument.getAdd().getSub().getId());
        dto.setSubFamilyName(translationService
            .getCorrectTranslation(instrument.getModel().getDescription().getTranslations(), locale));
        dto.setSubFamilyTMLId(instrument.getModel().getDescription().getTmlid());
        dto.setSubFamilyTypology(instrument.getModel().getDescription().getTypology());
        if (instrument.getInstrumentComplementaryField() != null)
            dto.setObservations(instrument.getInstrumentComplementaryField().getClientRemarks());
		return dto;
	}

}
