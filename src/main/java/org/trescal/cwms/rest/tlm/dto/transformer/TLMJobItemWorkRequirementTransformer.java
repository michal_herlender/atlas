package org.trescal.cwms.rest.tlm.dto.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.rest.tlm.dto.TLMJobitemWorkRequirementDTO;
import org.trescal.cwms.rest.tlm.utils.TLMTransformer;
import org.trescal.cwms.rest.tlm.validator.TLMStartCalibrationInputDTOValidator;

import java.util.Collections;

@Component
public class TLMJobItemWorkRequirementTransformer
    implements TLMTransformer<JobItemWorkRequirement, TLMJobitemWorkRequirementDTO> {

    @Autowired
    TLMStartCalibrationInputDTOValidator startCalibratoinValidator;

    @Override
    public TLMJobitemWorkRequirementDTO convertSingle(JobItemWorkRequirement entity) {

        TLMJobitemWorkRequirementDTO dto = new TLMJobitemWorkRequirementDTO();
        dto.setWorkRequirementId(entity.getId());
        Capability capability = entity.getWorkRequirement().getCapability();
        if (capability != null) {
            dto.setProcedureName(Collections.singletonList(capability.getName()));
            dto.setProcedureReference(Collections.singletonList(capability.getReference()));
        } else {
            dto.setProcedureName(Collections.emptyList());
            dto.setProcedureReference(Collections.emptyList());
        }

        dto.setCanStart(startCalibratoinValidator.isJobItemWorkRequirementStartable(entity));

        if (entity.getJobitem().getCurrentAddr() != null)
            dto.setSubdivId(entity.getJobitem().getCurrentAddr().getSub().getSubdivid());

        return dto;
    }

	

}
