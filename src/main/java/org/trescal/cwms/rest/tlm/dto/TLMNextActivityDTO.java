package org.trescal.cwms.rest.tlm.dto;

import java.util.Map;

public class TLMNextActivityDTO {

	private int activityId;
	private String activity;
	private Map<Integer, String> actionOutcomes;
	private Map<Integer, String> outcomeStatuses;

	public int getActivityId() {
		return activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Map<Integer, String> getActionOutcomes() {
		return actionOutcomes;
	}

	public void setActionOutcomes(Map<Integer, String> actionOutcomes) {
		this.actionOutcomes = actionOutcomes;
	}

	public Map<Integer, String> getOutcomeStatuses() {
		return outcomeStatuses;
	}

	public void setOutcomeStatuses(Map<Integer, String> outcomeStatuses) {
		this.outcomeStatuses = outcomeStatuses;
	}

}
