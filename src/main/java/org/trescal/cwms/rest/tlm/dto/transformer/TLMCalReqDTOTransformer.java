package org.trescal.cwms.rest.tlm.dto.transformer;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.tools.TranslationUtils;
import org.trescal.cwms.rest.tlm.dto.TLMCalReqDTO;
import org.trescal.cwms.rest.tlm.dto.TLMPointDTO;
import org.trescal.cwms.rest.tlm.dto.TLMUomDTO;
import org.trescal.cwms.rest.tlm.utils.TLMTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
public class TLMCalReqDTOTransformer implements TLMTransformer<CalReq, TLMCalReqDTO> {

    @Override
    public TLMCalReqDTO convertSingle(CalReq entity) {

        TLMCalReqDTO dto = new TLMCalReqDTO();
        Locale locale = LocaleContextHolder.getLocale();

        List<TLMPointDTO> points = new ArrayList<TLMPointDTO>();
        ;
        if (entity.getPointSet() != null && entity.getPointSet().getPoints() != null) {
            entity.getPointSet().getPoints().forEach(e -> {
                TLMPointDTO pDto = new TLMPointDTO();
                pDto.setPoint(e.getFormattedPoint());
                pDto.setRelatedPoint(e.getFormattedRelatedPoint());
                {
                    TLMUomDTO uomDto = uoMtoDto(e.getUom());
                    pDto.setUom(uomDto);
                }
                if (e.getRelatedUom() != null) {
                    TLMUomDTO uomDto = uoMtoDto(e.getRelatedUom());
                    pDto.setRelatedUom(uomDto);
                }

                points.add(pDto);
            });
        }

        dto.setPoints(points);
        dto.setPrivateInstructions(entity.getPrivateInstructions());
        dto.setPublicInstructions(entity.getPublicInstructions());
        if (entity.getRange() != null) {
            dto.setRange(entity.getRange().rangeValue());
            dto.setRelatedPointRange(entity.getRange().getFormattedRelatedPoint());
            if (entity.getRange().getRelatedUom() != null) {
                TLMUomDTO uomDto = uoMtoDto(entity.getRange().getRelatedUom());
                dto.setRelatedUomRange(uomDto);
            }
        }
        dto.setActive(entity.isActive());

        return dto;
    }

    private TLMUomDTO uoMtoDto(UoM uoM) {
        TLMUomDTO uomDto = new TLMUomDTO();
        uomDto.setNameTranslation(TranslationUtils.getBestTranslation(uoM.getNameTranslation()).orElse(null));
        uomDto.setSymbol(uoM.getSymbol());
        uomDto.setTmlId(uoM.getTmlid());
        return uomDto;
    }

}
