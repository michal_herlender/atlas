package org.trescal.cwms.rest.tlm.dto;

import org.apache.commons.lang3.StringUtils;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

import java.util.*;
import java.util.stream.Collectors;

public class TLMAddressProjectionDTO {

	private Integer addrId;
	private Boolean addrActive;
	private Set<AddressType> addrTypes;
	private List<String> addrLines;
	private String town;
	private String county;
	private String postCode;
	private String countryCode;
	private String tel;
	private String fax;
	private Date lastModified;

	private Integer subdivId;
	private Boolean subdivActive;
	private String subName;
	private String facilityIdentifier;
	private Date subdivLastModified;

	private Integer coId;
	private Boolean companyActive;
	private Boolean isBusinessCompany;
	private String legalIdentifier;
	private String fiscalidentifier;
	private String coName;
	private Date companyLastModified;

	public TLMAddressProjectionDTO() {
	}

	public TLMAddressProjectionDTO(Integer addrId, Boolean addrActive, String addr1, String addr2, String addr3,
			String town, String county, String postCode, String countryCode, String tel, String fax, Date lastModified,
			Long isInvoiceType, Long isDeliveryType, Long isCertificateType, Long isPOformType,
			Long isLegalResitrationType, Integer subdivId, Boolean subdivActive, String subName,
			String facilityIdentifier, Date subdivLastModified, Integer coId, CompanyRole role, String legalIdentifier,
			String fiscalidentifier, String coName, Date companyLastModified) {
		super();
		this.addrId = addrId;
		this.addrActive = addrActive;
		Set<AddressType> addressTypes = new HashSet<>();
		if(isInvoiceType==1)
			addressTypes.add(AddressType.INVOICE);
		if(isDeliveryType==1)
			addressTypes.add(AddressType.DELIVERY);
		if(isCertificateType==1)
			addressTypes.add(AddressType.CERTIFICATE);
		if(isPOformType==1)
			addressTypes.add(AddressType.POFROM);
		if(isLegalResitrationType==1)
			addressTypes.add(AddressType.LEGAL_REGISTRATION);
		this.addrTypes = addressTypes;
		this.addrLines = Arrays.asList(addr1, addr2, addr3).stream().filter(addrLine -> {
			return StringUtils.isNotBlank(addrLine);
		}).collect(Collectors.toList());
		this.town = town;
		this.county = county;
		this.postCode = postCode;
		this.countryCode = countryCode;
		this.tel = tel;
		this.fax = fax;
		this.lastModified = lastModified;
		this.subdivId = subdivId;
		this.subdivActive = subdivActive;
		this.subName = subName;
		this.facilityIdentifier = facilityIdentifier;
		this.subdivLastModified = subdivLastModified;
		this.coId = coId;
		this.companyActive = true;
		this.isBusinessCompany = role.equals(CompanyRole.BUSINESS);
		this.legalIdentifier = legalIdentifier;
		this.fiscalidentifier = fiscalidentifier;
		this.coName = coName;
		this.companyLastModified = companyLastModified;
	}

	public Integer getAddrId() {
		return addrId;
	}

	public void setAddrId(Integer addrId) {
		this.addrId = addrId;
	}

	public Boolean getAddrActive() {
		return addrActive;
	}

	public void setAddrActive(Boolean addrActive) {
		this.addrActive = addrActive;
	}

	public Set<AddressType> getAddrTypes() {
		return addrTypes;
	}

	public void setAddrTypes(Set<AddressType> addrTypes) {
		this.addrTypes = addrTypes;
	}

	public List<String> getAddrLines() {
		return addrLines;
	}

	public void setAddrLines(List<String> addrLines) {
		this.addrLines = addrLines;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public Integer getSubdivId() {
		return subdivId;
	}

	public void setSubdivId(Integer subdivId) {
		this.subdivId = subdivId;
	}

	public Boolean getSubdivActive() {
		return subdivActive;
	}

	public void setSubdivActive(Boolean subdivActive) {
		this.subdivActive = subdivActive;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public String getFacilityIdentifier() {
		return facilityIdentifier;
	}

	public void setFacilityIdentifier(String facilityIdentifier) {
		this.facilityIdentifier = facilityIdentifier;
	}

	public Date getSubdivLastModified() {
		return subdivLastModified;
	}

	public void setSubdivLastModified(Date subdivLastModified) {
		this.subdivLastModified = subdivLastModified;
	}

	public Integer getCoId() {
		return coId;
	}

	public void setCoId(Integer coId) {
		this.coId = coId;
	}

	public Boolean getCompanyActive() {
		return companyActive;
	}

	public void setCompanyActive(Boolean companyActive) {
		this.companyActive = companyActive;
	}

	public Boolean getIsBusinessCompany() {
		return isBusinessCompany;
	}

	public void setIsBusinessCompany(Boolean isBusinessCompany) {
		this.isBusinessCompany = isBusinessCompany;
	}

	public String getLegalIdentifier() {
		return legalIdentifier;
	}

	public void setLegalIdentifier(String legalIdentifier) {
		this.legalIdentifier = legalIdentifier;
	}

	public String getFiscalidentifier() {
		return fiscalidentifier;
	}

	public void setFiscalidentifier(String fiscalidentifier) {
		this.fiscalidentifier = fiscalidentifier;
	}

	public String getCoName() {
		return coName;
	}

	public void setCoName(String coName) {
		this.coName = coName;
	}

	public Date getCompanyLastModified() {
		return companyLastModified;
	}

	public void setCompanyLastModified(Date companyLastModified) {
		this.companyLastModified = companyLastModified;
	}

}
