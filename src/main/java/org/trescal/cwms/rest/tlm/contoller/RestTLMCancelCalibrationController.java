package org.trescal.cwms.rest.tlm.contoller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMCancelCalibrationInputDTO;
import org.trescal.cwms.rest.tlm.service.RestTLMCalibrationService;
import org.trescal.cwms.rest.tlm.validator.TLMCancelCalibrationInputDTOValidator;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMCancelCalibrationController {

	@Autowired
	private RestTLMCalibrationService restTLMCalibrationService;
	@Autowired
	private TLMCancelCalibrationInputDTOValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/cancelcalibration", method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<String> completeCalibration(@RequestBody TLMCancelCalibrationInputDTO inputDTO,
			BindingResult bindingResult, Locale locale) {

		// validation
		validator.validate(inputDTO, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}

		return restTLMCalibrationService.cancelCalibration(inputDTO);
	}

}
