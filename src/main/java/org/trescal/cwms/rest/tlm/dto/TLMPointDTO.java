package org.trescal.cwms.rest.tlm.dto;

public class TLMPointDTO {

	private String point;
	private String relatedPoint;
	private TLMUomDTO uom;
	private TLMUomDTO relatedUom;
	
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getRelatedPoint() {
		return relatedPoint;
	}
	public void setRelatedPoint(String relatedPoint) {
		this.relatedPoint = relatedPoint;
	}
	public TLMUomDTO getUom() {
		return uom;
	}
	public void setUom(TLMUomDTO uom) {
		this.uom = uom;
	}
	public TLMUomDTO getRelatedUom() {
		return relatedUom;
	}
	public void setRelatedUom(TLMUomDTO relatedUom) {
		this.relatedUom = relatedUom;
	}
}
