package org.trescal.cwms.rest.tlm.dto;

public class TLMUomDTO {

	private String symbol;
	private String nameTranslation;
	private Integer tmlId;
	
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getNameTranslation() {
		return nameTranslation;
	}
	public void setNameTranslation(String nameTranslation) {
		this.nameTranslation = nameTranslation;
	}
	public Integer getTmlId() {
		return tmlId;
	}
	public void setTmlId(Integer tmlId) {
		this.tmlId = tmlId;
	}
	
	
}
