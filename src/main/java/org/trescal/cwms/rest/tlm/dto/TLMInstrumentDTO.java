package org.trescal.cwms.rest.tlm.dto;

import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.Date;

public class TLMInstrumentDTO {

	private int plantId;
	private String plantNumber;
	private String serialNumber;
	private String formerBarcode;
	private String customerDescription;
	private Integer calInterval;
	private IntervalUnit calIntervalUnit;
	private LocalDate lastCalDate;
	private Date nextCalDate;
	private Date lastModified;
	private ModelMfrType modelMfrType;
	private int subFamilyTMLId;
	private int mfrTMLId;
	private int modelId;
	private String modelName;
	private String mfrName;
	private String subFamilyName;
	private Integer subFamilyTypology;
	private int addrId;
	private int subdivId;
	private int coId;
	private Integer jobItemId;
	private String observations;
	private String publicInstructions;
	private String privateInstructions;

	public int getPlantId() {
		return plantId;
	}

	public void setPlantId(int plantid) {
		this.plantId = plantid;
	}

	public String getPlantNumber() {
		return plantNumber;
	}

	public void setPlantNumber(String plantNumber) {
		this.plantNumber = plantNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getFormerBarcode() {
		return formerBarcode;
	}

	public void setFormerBarcode(String formerBarcode) {
		this.formerBarcode = formerBarcode;
	}

	public String getCustomerDescription() {
		return customerDescription;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	public Integer getCalInterval() {
		return calInterval;
	}

	public void setCalInterval(Integer calInterval) {
		this.calInterval = calInterval;
	}

	public IntervalUnit getCalIntervalUnit() {
		return calIntervalUnit;
	}

	public void setCalIntervalUnit(IntervalUnit calIntervalUnit) {
		this.calIntervalUnit = calIntervalUnit;
	}

	public LocalDate getLastCalDate() {
		return lastCalDate;
	}

	public void setLastCalDate(LocalDate lastCalDate) {
		this.lastCalDate = lastCalDate;
	}

	public Date getNextCalDate() {
		return nextCalDate;
	}

	public void setNextCalDate(Date nextCalDate) {
		this.nextCalDate = nextCalDate;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public ModelMfrType getModelMfrType() {
		return modelMfrType;
	}

	public void setModelMfrType(ModelMfrType modelMfrType) {
		this.modelMfrType = modelMfrType;
	}

	public int getSubFamilyTMLId() {
		return subFamilyTMLId;
	}

	public void setSubFamilyTMLId(int subFamilyTMLId) {
		this.subFamilyTMLId = subFamilyTMLId;
	}

	public int getMfrTMLId() {
		return mfrTMLId;
	}

	public void setMfrTMLId(int mfrTMLId) {
		this.mfrTMLId = mfrTMLId;
	}

	public int getModelId() {
		return modelId;
	}

	public void setModelId(int modelId) {
		this.modelId = modelId;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getMfrName() {
		return mfrName;
	}

	public void setMfrName(String mfrName) {
		this.mfrName = mfrName;
	}

	public String getSubFamilyName() {
		return subFamilyName;
	}

	public void setSubFamilyName(String subFamilyName) {
		this.subFamilyName = subFamilyName;
	}

	public int getSubFamilyTypology() {
		return subFamilyTypology;
	}

	public void setSubFamilyTypology(Integer subFamilyTypology) {
		this.subFamilyTypology = subFamilyTypology;
	}

	public int getAddrId() {
		return addrId;
	}

	public void setAddrId(int addrId) {
		this.addrId = addrId;
	}

	public int getSubdivId() {
		return subdivId;
	}

	public void setSubdivId(int subdivId) {
		this.subdivId = subdivId;
	}

	public int getCoId() {
		return coId;
	}

	public void setCoId(int coId) {
		this.coId = coId;
	}

	public Integer getJobItemId() {
		return jobItemId;
	}

	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public String getPublicInstructions() {
		return publicInstructions;
	}

	public void setPublicInstructions(String publicInstructions) {
		this.publicInstructions = publicInstructions;
	}

	public String getPrivateInstructions() {
		return privateInstructions;
	}

	public void setPrivateInstructions(String privateInstructions) {
		this.privateInstructions = privateInstructions;
	}

}
