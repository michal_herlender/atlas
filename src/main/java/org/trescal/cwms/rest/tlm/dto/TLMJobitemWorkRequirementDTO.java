package org.trescal.cwms.rest.tlm.dto;

import java.util.List;

public class TLMJobitemWorkRequirementDTO {

	private int workRequirementId;
	private boolean canStart;
	private List<String> procedureReference;
	private List<String> procedureName;
	private int SubdivId;

	public int getWorkRequirementId() {
		return workRequirementId;
	}

	public void setWorkRequirementId(int workRequirementId) {
		this.workRequirementId = workRequirementId;
	}

	public boolean isCanStart() {
		return canStart;
	}

	public void setCanStart(boolean canStart) {
		this.canStart = canStart;
	}

	public List<String> getProcedureReference() {
		return procedureReference;
	}

	public void setProcedureReference(List<String> procedureReference) {
		this.procedureReference = procedureReference;
	}

	public List<String> getProcedureName() {
		return procedureName;
	}

	public void setProcedureName(List<String> procedureName) {
		this.procedureName = procedureName;
	}

	public int getSubdivId() {
		return SubdivId;
	}

	public void setSubdivId(int subdivId) {
		SubdivId = subdivId;
	}

}
