package org.trescal.cwms.rest.tlm.contoller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMJobitemWorkRequirementDTO;
import org.trescal.cwms.rest.tlm.dto.transformer.TLMJobItemWorkRequirementTransformer;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMWorkRequirementController {

	@Autowired
	private JobItemService jobItemService;
	@Autowired
	TLMJobItemWorkRequirementTransformer transformer;
	@Autowired
	private MessageSource messageSource;

	/* NOT USED */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/workrequirement", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMJobitemWorkRequirementDTO> getWorkRequirementByJobItemId(
			@RequestParam(required = true) int jobitemId,Locale locale) {

		JobItem ji = jobItemService.findJobItem(jobitemId);
		if (ji == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, new Object[] { jobitemId },
					RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}

		List<TLMJobitemWorkRequirementDTO> dtos = transformer
				.convertList(new ArrayList<>(ji.getWorkRequirements()));

		RestResultDTO<TLMJobitemWorkRequirementDTO> result = new RestResultDTO<>(true, "");
		dtos.forEach(dto -> {
			result.addResult(dto);
		});

		return result;

	}

}
