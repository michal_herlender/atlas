package org.trescal.cwms.rest.tlm.contoller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMCancelAndReplaceCertificateInputDTO;
import org.trescal.cwms.rest.tlm.service.RestTLMCalibrationService;
import org.trescal.cwms.rest.tlm.validator.TLMCandelAndReplaceCertificateValidator;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMCancelAndReplaceCertificate {

	private static final String WORK_REQUIREMENT_ID = "workRequirementId";
	@Autowired
	private RestTLMCalibrationService restTLMCalibrationService;
	@Autowired
	private TLMCandelAndReplaceCertificateValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/technicalCancelAndReplace", method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<Map<String, Integer>> cancelAndReplaceCertificate(
			@RequestBody TLMCancelAndReplaceCertificateInputDTO inputDTO, BindingResult bindingResult, Locale locale) {

		// validation
		validator.validate(inputDTO, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}

		JobItemWorkRequirement duplicatedJiwr = restTLMCalibrationService.technicalCancelAndReplace(inputDTO);

		Map<String, Integer> map = new HashMap<>();
		map.put(WORK_REQUIREMENT_ID, duplicatedJiwr.getId());

		RestResultDTO<Map<String, Integer>> result = new RestResultDTO<>(true, null);
		result.addResult(map);
		return result;
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/certificatereplacement", method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<Map<String, Integer>> certificateReplacement(
			@RequestBody TLMCancelAndReplaceCertificateInputDTO inputDTO, BindingResult bindingResult, Locale locale) {

		validator.validate(inputDTO, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}

		JobItemWorkRequirement duplicatedJiwr = this.restTLMCalibrationService.certificateReplacement(inputDTO);

		Map<String, Integer> map = new HashMap<>();
		map.put(WORK_REQUIREMENT_ID, duplicatedJiwr.getId());

		RestResultDTO<Map<String, Integer>> result = new RestResultDTO<>(true, null);
		result.addResult(map);
		return result;
	}

}
