package org.trescal.cwms.rest.tlm.dto;

public class TLMNoteDTO {

	private String label;
	private String note;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
