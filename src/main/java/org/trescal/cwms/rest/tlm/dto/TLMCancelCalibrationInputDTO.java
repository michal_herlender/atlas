package org.trescal.cwms.rest.tlm.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class TLMCancelCalibrationInputDTO {

	@NotNull
	private Integer calibrationId;
	@NotNull
	private Date cancelDate;
	@NotNull
	private String hrid;
	private String remarks;

	public Integer getCalibrationId() {
		return calibrationId;
	}

	public void setCalibrationId(Integer calibrationId) {
		this.calibrationId = calibrationId;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getHrid() {
		return hrid;
	}

	public void setHrid(String hrId) {
		this.hrid = hrId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
