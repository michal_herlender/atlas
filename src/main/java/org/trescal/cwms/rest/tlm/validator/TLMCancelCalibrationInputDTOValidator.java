package org.trescal.cwms.rest.tlm.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.tlm.dto.TLMCancelCalibrationInputDTO;

@Component
public class TLMCancelCalibrationInputDTOValidator extends AbstractBeanValidator {

	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private ContactService contactService;

	@Override
	public boolean supports(Class<?> clazz) {
		return TLMCancelCalibrationInputDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		TLMCancelCalibrationInputDTO dto = (TLMCancelCalibrationInputDTO) target;

		// lookup hrid
		if (StringUtils.isNotEmpty(dto.getHrid())) {
			Contact c = this.contactService.getByHrId(dto.getHrid());
			if (c == null) {
				errors.rejectValue("hrid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { dto.getHrid() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		// check if calibration exists
		Calibration calibration = calibrationService.findCalibration(dto.getCalibrationId());
		if (calibration == null) {
			errors.rejectValue("calibrationId", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { dto.getCalibrationId() },
					RestErrors.MESSAGE_DATA_NOT_FOUND);
		}

		// check if calibration is still not completed
		// check if calibration has the status 'on-going' or 'on-hold'
		if (calibration != null && !calibration.getStatus().getName().equals(CalibrationStatus.ON_GOING)
				&& !calibration.getStatus().getName().equals(CalibrationStatus.ON_HOLD)) {
			errors.rejectValue("calibrationId", RestErrors.CODE_CAL_NOT_IN_PROGRESS, new Object[] {},
					RestErrors.MESSAGE_CAL_NOT_IN_PROGRESS);
		}

		
	}

}
