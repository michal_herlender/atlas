package org.trescal.cwms.rest.tlm.contoller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMStartCalibrationInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMStartCalibrationOutputDTO;
import org.trescal.cwms.rest.tlm.service.RestTLMCalibrationServiceImpl;
import org.trescal.cwms.rest.tlm.validator.TLMStartCalibrationInputDTOValidator;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMStartCalibrationController {

	@Autowired
	private TLMStartCalibrationInputDTOValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestTLMCalibrationServiceImpl restTLMCalibrationService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/startcalibration", method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMStartCalibrationOutputDTO> startCalibration(
			@RequestBody TLMStartCalibrationInputDTO inputDTO, BindingResult bindingResult, Locale locale) {

		// validation
		validator.validate(inputDTO, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}

		return this.restTLMCalibrationService.startCalibrationForTLM(inputDTO);
	}

}
