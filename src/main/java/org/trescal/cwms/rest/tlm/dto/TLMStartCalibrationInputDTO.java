package org.trescal.cwms.rest.tlm.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
public class TLMStartCalibrationInputDTO {

	@NotNull
	@Size(min = 1)
	private List<Integer> workRequirementIds;
	private String username;
	@NotNull
	private Date startDate;
	private Integer processID;
	private Integer calDataContent;
	private String hrid;
	private String certificateSupplementaryFor;

}
