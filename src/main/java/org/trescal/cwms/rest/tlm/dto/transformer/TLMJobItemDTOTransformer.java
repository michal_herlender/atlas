package org.trescal.cwms.rest.tlm.dto.transformer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.companyinstructionlink.db.CompanyInstructionLinkService;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.db.SubdivInstructionLinkService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.db.JobInstructionLinkService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.alligator.dto.output.RestWorkRequirementDTOFactory;
import org.trescal.cwms.rest.tlm.dto.TLMEngineerAllocationDTO;
import org.trescal.cwms.rest.tlm.dto.TLMJobItemDTO;
import org.trescal.cwms.rest.tlm.utils.TLMTransformer;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class TLMJobItemDTOTransformer implements TLMTransformer<JobItem, TLMJobItemDTO> {

	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private TLMEngineerAllocationDTOTransformer tlmEngineerAllocationDTOTransformer;
	@Autowired
	private CalReqService calreqService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private JobInstructionLinkService jobInstructionLinkService;
	@Autowired
	private CompanyInstructionLinkService companyInstructionLinkService;
	@Autowired
	private SubdivInstructionLinkService subdivInstructionLinkService;
	@Autowired
	private RestWorkRequirementDTOFactory wrDtoFactory;
	@Autowired
	private TLMCalReqDTOTransformer calReqDTOTransformer;

	@Override
	public TLMJobItemDTO convertSingle(JobItem entity) {

		Locale locale = new Locale("FR");

		TLMJobItemDTO dto = new TLMJobItemDTO();

		List<TLMEngineerAllocationDTO> allocations = tlmEngineerAllocationDTOTransformer
			.convertList(entity.getEngineerAllocations());
		dto.setAllocations(allocations);

		Address addr;
		if (entity.getOnBehalf() != null && entity.getOnBehalf().getAddress() != null) {
			addr = entity.getOnBehalf().getAddress();
		} else {
			addr = entity.getJob().getReturnTo();
		}

		// set address lines
		dto.setApplicantAddrLine(
			Stream.of(addr.getAddr1(), addr.getAddr2(), addr.getAddr3()).filter(StringUtils::isNotBlank).collect(Collectors.toList()));
		dto.setApplicantCountryCode(addr.getCountry().getCountryCode());
		dto.setApplicantCounty(addr.getCounty());
		dto.setApplicantPostCode(addr.getPostcode());
		dto.setApplicantSubName(addr.getSub().getSubname());
		dto.setApplicantTown(addr.getTown());
		dto.setApplicantCoName(entity.getJob().getReturnTo().getSub().getComp().getConame());

		// set certificate address
		Address certAddr = null;
		if (entity.getInst().getAdd() != null && entity.getInst().getAdd().getAddressType() != null && entity.getInst()
			.getAdd().getAddressType().stream().anyMatch(AddressType.CERTIFICATE::equals)) {
			certAddr = entity.getInst().getAdd();
		}
		if (certAddr != null) {
			dto.setCertAddrId(certAddr.getAddrid());
			dto.setCertAddrLine(Stream.of(certAddr.getAddr1(), certAddr.getAddr2(), certAddr.getAddr3())
				.filter(StringUtils::isNotBlank).collect(Collectors.toList()));
			dto.setCertAddrCountryCode(certAddr.getCountry().getCountryCode());
			dto.setCertAddrCounty(certAddr.getCounty());
			dto.setCertAddrPostCode(certAddr.getPostcode());
			dto.setCertAddrSubName(certAddr.getSub().getSubname());
			dto.setCertAddrTown(certAddr.getTown());
			dto.setCertAddrCoName(certAddr.getSub().getComp().getConame());
		}

		if (entity.getOnBehalf() != null)
			dto.setApplicantCoName(entity.getOnBehalf().getCompany().getConame());

		dto.setReturnAddrId(entity.getJob().getReturnTo().getAddrid());
		CalReq calReq = calreqService.findCalReqsForJobItem(entity);
		if (calReq != null)
			dto.setCalreq(calReqDTOTransformer.convertSingle(calReq));
		dto.setCanResumeCalibration(jobItemService.isReadyToResumeCalibration(entity));
		dto.setCollectDate(null);
		dto.setIsAccredited(entity.getServiceType().getCalibrationType().getAccreditationSpecific());
		dto.setIsAwaitingCalibration(
			jobItemService.stateHasGroupOfKeyName(entity.getState(), StateGroup.AWAITINGCALIBRATION));
		dto.setIsInProgressCalibration(jobItemService.isInCalibration(entity));
		dto.setIsOnBehalf(entity.getOnBehalf() != null);
		dto.setIsOnSite(entity.getJob().getType() == JobType.SITE);
		dto.setJobCreatedBySubdivId(entity.getJob().getOrganisation().getSubdivid());
		dto.setJobId(entity.getJob().getJobid());
		dto.setJobItemId(entity.getJobItemId());
		dto.setJobItemNo(entity.getItemCode());
		dto.setJobNo(entity.getJob().getJobno());
		dto.setLastModified(entity.getLastModified());
		dto.setPlannedDeliveryDate(entity.getAgreedDelDate());
		dto.setPlannedDueDate(entity.getDueDate());
		dto.setPlantId(entity.getInst().getPlantid());
		dto.setReceiptDate(entity.getDateIn());
		dto.setStateId(entity.getState().getStateid());
		dto.setStateName(translationService.getCorrectTranslation(entity.getState().getTranslations(), locale));
		dto.setContactEmail(entity.getJob().getCon().getEmail());
		dto.setContactFirstName(entity.getJob().getCon().getFirstName());
		dto.setContactLastName(entity.getJob().getCon().getLastName());
		dto.setContactTel(entity.getJob().getCon().getTelephone());
		dto.setContactFax(entity.getJob().getCon().getFax());
		if (entity.getCalAddr() != null)
			dto.setSubdivIdToProcessCal(entity.getCalAddr().getSub().getSubdivid());

		// workrequirement
		dto.setWorkRequirements(entity.getWorkRequirements().stream().map(wr -> wrDtoFactory.convert(wr, locale)).collect(Collectors.toList()));
		// job instructions
		dto.setJobInstructions(jobInstructionLinkService.getAllForJob(entity.getJob()).stream().filter(jil -> jil.getInstruction().getInstructiontype() == InstructionType.CALIBRATION
			|| jil.getInstruction().getInstructiontype() == InstructionType.REPAIRANDADJUSTMENT).map(jil -> jil.getInstruction().getInstruction()).collect(Collectors.toList()));
		// company instructions
		dto.setCompanyInstructions(companyInstructionLinkService
			.getForCompanyAndTypes(entity.getJob().getCon().getSub().getComp(), entity.getJob().getOrganisation(),
				InstructionType.CALIBRATION, InstructionType.REPAIRANDADJUSTMENT)
			.stream().map(i -> i.getInstruction().getInstruction()).collect(Collectors.toList()));
		// subdiv instructions
		dto.setSubdivInstructions(subdivInstructionLinkService.getForSubdivAndTypes(entity.getJob().getCon().getSub(),
			entity.getJob().getOrganisation(), InstructionType.CALIBRATION, InstructionType.REPAIRANDADJUSTMENT)
			.stream().map(i -> i.getInstruction().getInstruction()).collect(Collectors.toList()));

		dto.setCalibrationWithJudgement(entity.getServiceType().getCalibrationType().getCalibrationWithJudgement());
		dto.setServiceTypeId(entity.getServiceType().getServiceTypeId());

		return dto;
	}

}
