package org.trescal.cwms.rest.tlm.dto.transformer;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;
import org.trescal.cwms.core.workflow.entity.outcomestatus.db.OutcomeStatusService;
import org.trescal.cwms.rest.tlm.dto.TLMNextActivityDTO;
import org.trescal.cwms.rest.tlm.utils.TLMTransformer;

@Component
public class TLMNextActivitiesDTOTransformer implements TLMTransformer<NextActivity, TLMNextActivityDTO> {

	@Autowired
	private TranslationService tranServ;
	@Autowired
	private OutcomeStatusService outcomeStatusServ;

	@Override
	public TLMNextActivityDTO convertSingle(NextActivity entity) {

		Locale locale = new Locale("fr-FR");
		TLMNextActivityDTO dto = new TLMNextActivityDTO();

		dto.setActivityId(entity.getActivity().getStateid());
		dto.setActivity(tranServ.getCorrectTranslation(entity.getActivity().getTranslations(), locale));
		dto.setActionOutcomes(
				entity.getActivity().getActionOutcomes().stream().collect(Collectors.toMap(ActionOutcome::getId, ao -> {
					return tranServ.getCorrectTranslation(ao.getTranslations(), locale);
				})));
		List<OutcomeStatus> allowedOutcomes = outcomeStatusServ
				.getOutcomeStatusesForActivity(entity.getActivity().getStateid(), false);
		dto.setOutcomeStatuses(allowedOutcomes.stream().collect(Collectors.toMap(os->{
			return os.getStatus().getStateid();
		}, os -> {
			return tranServ.getCorrectTranslation(os.getStatus().getTranslations(), locale);
		})));

		return dto;
	}

}
