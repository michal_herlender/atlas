package org.trescal.cwms.rest.tlm.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.rest.alligator.dto.output.RestWorkRequirementDTO;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class TLMJobItemDTO {

	private int jobItemId;
	private String JobItemNo;
	private int plantId;
	private String plantNumber;
	private String serialNumber;
	private String formerBarcode;
	private int stateId;
	private String stateName;
	private String customerDescription;
	private int modelId;
	private String modelName;
	private String mfrName;
	private String subFamilyName;
	private String companyName;
	private String subdivName;

	private int jobId;
	private String jobNo;
	private Boolean isOnSite;
	private Boolean isAccredited;
	private Date lastModified;
	private Boolean canResumeCalibration;
	private Boolean isAwaitingCalibration;
	private Boolean isInProgressCalibration;
	private Boolean isOnBehalf;
	private int returnAddrId;
	private String applicantCoName;
	private String applicantSubName;
	private List<String> applicantAddrLine;
	private String applicantPostCode;
	private String applicantTown;
	private String applicantCounty;
	private String applicantCountryCode;
	private List<TLMEngineerAllocationDTO> allocations;
	private LocalDate plannedDueDate;
	private Date collectDate;
	private ZonedDateTime receiptDate;
	private LocalDate plannedDeliveryDate;
	private int jobCreatedBySubdivId;
	private List<RestWorkRequirementDTO> workRequirements;
	private Boolean calibrationWithJudgement;
	private Integer serviceTypeId;
	private String contactFirstName;
	private String contactLastName;
	private String contactTel;
	private String contactFax;
	private String contactEmail;
	private Integer subdivIdToProcessCal;

	private List<String> observations;
	private List<String> jobInstructions;
	private List<String> companyInstructions;
	private List<String> subdivInstructions;

	private TLMCalReqDTO calreq;

	//certificate address
	private int certAddrId;
	private String certAddrCoName;
	private String certAddrSubName;
	private List<String> certAddrLine;
	private String certAddrPostCode;
	private String certAddrTown;
	private String certAddrCounty;
	private String certAddrCountryCode;

	public TLMJobItemDTO() {
	}

	public TLMJobItemDTO(int jobItemId, String jobItemNo, int plantId, String plantno, String serialno,
						 String formerBarcode, int stateId, String stateName, String customerDescription, int modelId,
						 String modelName, String mfrName, String subFamilyName, String companyName, String subdivName) {
		super();
		this.jobItemId = jobItemId;
		this.JobItemNo = jobItemNo;
		this.plantId = plantId;
		this.plantNumber = plantno;
		this.serialNumber = serialno;
		this.formerBarcode = formerBarcode;
		this.stateId = stateId;
		this.stateName = stateName;
		this.customerDescription = customerDescription;
		this.modelId = modelId;
		this.modelName = modelName;
		this.mfrName = mfrName;
		this.subFamilyName = subFamilyName;
		this.companyName = companyName;
		this.subdivName = subdivName;
	}

}
