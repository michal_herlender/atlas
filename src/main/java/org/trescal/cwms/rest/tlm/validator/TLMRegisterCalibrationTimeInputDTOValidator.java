package org.trescal.cwms.rest.tlm.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.tlm.dto.TLMRegisterCalibrationTimeInputDTO;

@Component
public class TLMRegisterCalibrationTimeInputDTOValidator extends AbstractBeanValidator {

	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private UserService userService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private ContactService contactService;

	@Override
	public boolean supports(Class<?> clazz) {
		return TLMRegisterCalibrationTimeInputDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		TLMRegisterCalibrationTimeInputDTO inputDTO = (TLMRegisterCalibrationTimeInputDTO) target;

		// check if calibration exists
		Calibration calibration = calibrationService.findCalibration(inputDTO.getCalibrationId());
		if (calibration == null) {
			errors.rejectValue("calibrationId", RestErrors.CODE_DATA_NOT_FOUND,
					new Object[] { inputDTO.getCalibrationId() }, RestErrors.MESSAGE_DATA_NOT_FOUND);
		}

		// check if calibration has the status 'on-going' or 'on-hold'
		if (!calibration.getStatus().getName().equals(CalibrationStatus.ON_GOING)
				&& !calibration.getStatus().getName().equals(CalibrationStatus.ON_HOLD)) {
			errors.rejectValue("calibrationId", RestErrors.CODE_CAL_NOT_IN_PROGRESS, new Object[] {},
					RestErrors.MESSAGE_CAL_NOT_IN_PROGRESS);
		}

		// check if all jobitems linked to the calibration are on the activity
		// 'pre/post-calibration' or status 'pre/post-calibration on hold' or
		// also for onsite
		boolean allJobItemsReadyForCalibraiton = calibration.getLinks().stream().allMatch(cl -> {
			return jobItemService.isReadyForCalibration(cl.getJi())
					|| jobItemService.isReadyToResumeCalibration(cl.getJi())
					|| jobItemService.isInCalibration(cl.getJi());
		});
		if (!allJobItemsReadyForCalibraiton) {
			errors.rejectValue("calibrationId", RestErrors.CODE_CAL_NOT_IN_PROGRESS, new Object[] {},
					RestErrors.MESSAGE_CAL_NOT_IN_PROGRESS);
		}

		Contact startBy = null;
		// check username
		if (inputDTO.getUsername() != null) {
			User u = this.userService.get(inputDTO.getUsername());
			startBy = u.getCon();
			if (u == null || startBy == null) {
				errors.rejectValue("username", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { inputDTO.getUsername() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);

			}
		}

		// lookup hrid
		if (inputDTO.getHrid() != null) {
			Contact c = this.contactService.getByHrId(inputDTO.getHrid());
			if (c == null) {
				errors.rejectValue("hrid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { inputDTO.getUsername() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		if (StringUtils.isEmpty(inputDTO.getUsername()) && StringUtils.isEmpty(inputDTO.getHrid())) {
			errors.reject(RestErrors.CODE_USERNAME_AND_HRID_EMPTY, RestErrors.MESSAGE_USERNAME_AND_HRID_EMPTY);
		}

	}

}
