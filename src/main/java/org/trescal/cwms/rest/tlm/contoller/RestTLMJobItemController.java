package org.trescal.cwms.rest.tlm.contoller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMJobItemDTO;
import org.trescal.cwms.rest.tlm.dto.transformer.TLMJobItemDTOTransformer;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMJobItemController {

	@Autowired
	private TLMJobItemDTOTransformer transformer;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private MessageSource messageSource;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/jobitem/{id}", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMJobItemDTO> getJobItemById(@PathVariable(required = true) int id, Locale locale) {

		JobItem ji = jobItemService.findJobItem(id);
		if (ji == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, new Object[] { id },
					RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}

		TLMJobItemDTO dto = transformer.convertSingle(ji);
		RestResultDTO<TLMJobItemDTO> result = new RestResultDTO<>(true, "");
		result.addResult(dto);

		return result;

	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/jobitemByNo/{number:.+}", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMJobItemDTO> getJobItemByJobItemNo(
			@PathVariable(name = "number", required = true) String itemCode, Locale locale) {

		JobItem ji = jobItemService.getJobItemByItemCode(itemCode);
		if (ji == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, new Object[] {},
					RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}

		TLMJobItemDTO dto = transformer.convertSingle(ji);
		RestResultDTO<TLMJobItemDTO> result = new RestResultDTO<>(true, "");
		result.addResult(dto);

		return result;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/jobitem", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public PagedResultSet<TLMJobItemDTO> searchJobitems(@RequestParam(defaultValue = "true") boolean active,
			@RequestParam(required = false) Integer plantId, @RequestParam(required = false) String plantno,
			@RequestParam(required = false) String serialno, @RequestParam(required = false) String formerBarcode,
			@RequestParam(required = false) Integer calibrationSubdiv, @RequestParam(required = false) String jobitemno,
			@RequestParam(required = false, defaultValue = "en_GB") String locale,
			@RequestParam(required = false, defaultValue = "1") int currentPage,
			@RequestParam(required = false, defaultValue = "25") int resultsPerPage) {
		
		Locale localeFromRequest = StringUtils.parseLocaleString(locale);

		PagedResultSet<TLMJobItemDTO> result = jobItemService.searchJobitems(active, plantId, plantno, serialno,
				formerBarcode, calibrationSubdiv, jobitemno, currentPage, resultsPerPage, localeFromRequest);

		return result;
	}

}
