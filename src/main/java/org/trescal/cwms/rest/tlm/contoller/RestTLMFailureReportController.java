package org.trescal.cwms.rest.tlm.contoller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMFailureReportInputDTO;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMFailureReportController {

	@Autowired
	private ContactService contactService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private MessageSource messageSource;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/createfailurereport", method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<Map<String, String>> createFailureReport(@RequestBody TLMFailureReportInputDTO inputDTO,
			BindingResult bindingResult, Locale locale) {

		JobItem ji = jobItemService.findJobItem(inputDTO.getJobItemId());
		if (ji == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND,
					new Object[] { inputDTO.getJobItemId() }, RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}

		Contact technician = null;
		if (StringUtils.isNotEmpty(inputDTO.getTechnicianHrid())) {
			technician = contactService.getByHrId(inputDTO.getTechnicianHrid());
			if (technician == null) {
				String message = messageSource.getMessage(RestErrors.CODE_DATA_NOT_FOUND,
						new Object[] { inputDTO.getTechnicianHrid() }, RestErrors.MESSAGE_DATA_NOT_FOUND, locale);
				return new RestResultDTO<>(false, message);
			}
		}

		// create fr
		FaultReport fr = faultReportService.createFailureReportForTLMInterface(inputDTO);
		String frNumber = fr.getFaultReportNumber();

		Map<String, String> res = new HashMap<>();
		res.put("number", frNumber);

		RestResultDTO<Map<String, String>> dto = new RestResultDTO<>(true, null);
		dto.addResult(res);

		return dto;
	}
}
