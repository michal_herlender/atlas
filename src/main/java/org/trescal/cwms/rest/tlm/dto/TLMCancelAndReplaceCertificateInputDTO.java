package org.trescal.cwms.rest.tlm.dto;

import javax.validation.constraints.NotNull;

public class TLMCancelAndReplaceCertificateInputDTO {

	@NotNull
	private Integer workRequirementId;
	@NotNull
	private String certificateNumber;
	private String replacementComments;

	public Integer getWorkRequirementId() {
		return workRequirementId;
	}

	public void setWorkRequirementId(Integer workRequirementId) {
		this.workRequirementId = workRequirementId;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNo) {
		this.certificateNumber = certificateNo;
	}

	public String getReplacementComments() {
		return replacementComments;
	}

	public void setReplacementComments(String replacementComments) {
		this.replacementComments = replacementComments;
	}

}
