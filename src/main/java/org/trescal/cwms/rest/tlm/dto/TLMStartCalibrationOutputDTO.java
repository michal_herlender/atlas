package org.trescal.cwms.rest.tlm.dto;

public class TLMStartCalibrationOutputDTO {
	
	private int calibrationId;
	private String documentNumber;

	public int getCalibrationId() {
		return calibrationId;
	}

	public void setCalibrationId(int calibrationId) {
		this.calibrationId = calibrationId;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

}
