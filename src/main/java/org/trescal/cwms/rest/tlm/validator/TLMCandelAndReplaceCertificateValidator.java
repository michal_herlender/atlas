package org.trescal.cwms.rest.tlm.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.tlm.dto.TLMCancelAndReplaceCertificateInputDTO;

@Component
public class TLMCandelAndReplaceCertificateValidator extends AbstractBeanValidator {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobItemWorkRequirementService jobItemWorkRequirementService;

	@Override
	public boolean supports(Class<?> clazz) {
		return TLMCancelAndReplaceCertificateInputDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		super.validate(target, errors);
		TLMCancelAndReplaceCertificateInputDTO dto = (TLMCancelAndReplaceCertificateInputDTO) target;

		// the work requirement exists
		if (dto.getWorkRequirementId() != null) {
			JobItemWorkRequirement jiwr = jobItemWorkRequirementService
					.findJobItemWorkRequirement(dto.getWorkRequirementId());
			if (jiwr == null) {
				errors.rejectValue("workRequirementId", RestErrors.CODE_DATA_NOT_FOUND,
						new Object[] { dto.getWorkRequirementId() }, RestErrors.MESSAGE_DATA_NOT_FOUND);
				return;
			}

			JobItem ji = jiwr.getJobitem();

			// jobitem instrument should be at current address
			// there's some situation when we need to perform a certificate cancel and replace 
			// even if the current address is different from the calibration address
			if (ji.getCurrentAddr() != null && ji.getCalAddr() != ji.getCurrentAddr())
				errors.rejectValue("workRequirementId", RestErrors.CODE_CAL_ADDR_DIFF_CURR_ADDR, new Object[] {},
						RestErrors.MESSAGE_CAL_ADDR_DIFF_CURR_ADDR);

			// jobitem should not be in calibration, calibration on hold or
			// awaiting some decision
			if((ji.getJob().getType().equals(JobType.STANDARD) && !this.jobItemService.stateHasGroupOfKeyName(ji.getState(), StateGroup.START_LAB_CERTIFICATE_REPLACEMENT)) ||
					ji.getJob().getType().equals(JobType.SITE) && !this.jobItemService.stateHasGroupOfKeyName(ji.getState(), StateGroup.START_ONSITE_CERTIFICATE_REPLACEMENT)) {
				errors.rejectValue("workRequirementId", "certificatereplacement.validation.itemnotready", new Object[] {},
						"JobItem not in the right status to perform certificate replacement");
			}

			// the wr should be completed
			if (!jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)) {
				errors.rejectValue("workRequirementId", RestErrors.CODE_WR_NOT_COMPLETED,
						new Object[] { dto.getWorkRequirementId() }, RestErrors.MESSAGE_WR_NOT_COMPLETED);
			}
		}

		// certificate should exist
		Certificate certificate = this.certificateService.findCertificateByCertNo(dto.getCertificateNumber());
		if (certificate == null) {
			errors.rejectValue("certificateNumber", RestErrors.CODE_CERTIFICATE_NOT_FOUND,
					new Object[] { dto.getCertificateNumber() }, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND);
			return;
		}
		else
		{
			this.certificateService.setCertificateDirectory(certificate);
		}

		// the certificat should be of type CERT_INHOUSE
		if (!certificate.getType().equals(CertificateType.CERT_INHOUSE))
			errors.rejectValue("certificateNumber", RestErrors.CODE_CERTIFICATE_NOT_INHOUSE,
					new Object[] { dto.getCertificateNumber() }, RestErrors.MESSAGE_CERTIFICATE_NOT_INHOUSE);

		// the certificate should has a file attached
		if(!certificate.getHasCertFile()) {
			errors.rejectValue("certificateNumber", "certificatereplacement.validation.certnofile",
					new Object[] { dto.getCertificateNumber() }, "Certificate has no file");
		}
		// TODO certificate must belong the same jobitem that the wr belongs to

	}

}
