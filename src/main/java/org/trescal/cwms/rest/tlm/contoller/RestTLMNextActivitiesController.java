package org.trescal.cwms.rest.tlm.contoller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;
import org.trescal.cwms.core.workflow.entity.nextactivity.db.NextActivityService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.tlm.dto.TLMCurrentStatusAndNextActivities;
import org.trescal.cwms.rest.tlm.dto.TLMNextActivityDTO;
import org.trescal.cwms.rest.tlm.dto.TLMNextActivityInputDTO;
import org.trescal.cwms.rest.tlm.dto.transformer.TLMNextActivitiesDTOTransformer;

import java.util.Date;
import java.util.List;
import java.util.Locale;

@Controller
@JsonController
@RequestMapping("/tlm")
public class RestTLMNextActivitiesController {

	@Autowired
	private NextActivityService nextActServ;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private TLMNextActivitiesDTOTransformer transformer;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private JobItemActivityService jobitemactivityservice;
	@Autowired
	private TranslationService tranServ;
	@Autowired
	private ContactService contactService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/nextactivity", method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<TLMCurrentStatusAndNextActivities> getNextActivities(
			@RequestParam(required = true) int jobitemId) {

		Locale locale = new Locale("fr-FR");

		JobItem ji = jobItemService.findJobItem(jobitemId);
		if (ji == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, new Object[] { jobitemId },
					RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}

		List<NextActivity> nextActs = this.nextActServ.getNextActivitiesForStatus(ji.getState().getStateid(), true);
		List<TLMNextActivityDTO> list = transformer.convertList(nextActs);

		TLMCurrentStatusAndNextActivities dto = new TLMCurrentStatusAndNextActivities();
		dto.setJobitemId(jobitemId);
		dto.setCurrentStatusId(ji.getState().getStateid());
		dto.setCurrentStatus(tranServ.getCorrectTranslation(ji.getState().getTranslations(), locale));
		dto.setNextActivities(list);

		RestResultDTO<TLMCurrentStatusAndNextActivities> res = new RestResultDTO<>(true, "");
		res.getResults().add(dto);

		return res;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/nextactivity", method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<String> setNextActivity(@RequestBody TLMNextActivityInputDTO inputDTO, Locale locale) {

		if (inputDTO.getStartDate() == null)
			inputDTO.setStartDate(new Date());

		JobItem ji = jobItemService.findJobItem(inputDTO.getJobItemId());
		if (ji == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND,
					new Object[] { inputDTO.getJobItemId() }, RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}

		Contact contact = null;
		if (StringUtils.isNotEmpty(inputDTO.getHrid())) {
			contact = contactService.getByHrId(inputDTO.getHrid());
			if (contact == null) {
				String message = messageSource.getMessage(RestErrors.CODE_DATA_NOT_FOUND,
						new Object[] { inputDTO.getHrid() }, RestErrors.MESSAGE_DATA_NOT_FOUND, locale);
				return new RestResultDTO<>(false, message);
			}
		}

		ResultWrapper rw = jobitemactivityservice.newCompletedActivity(inputDTO.getJobItemId(),
				inputDTO.getActivityId(), inputDTO.getActionOutcomeId(), inputDTO.getOutcomeStatusId(),
				inputDTO.getRemarks(), inputDTO.getTimeSpent(), inputDTO.getStartDate(), contact, null, null);

		if (rw.isSuccess())
			return new RestResultDTO<>(true, null);
		else {
			StringBuilder errors = new StringBuilder();
			rw.getErrorList().forEach(errors::append);
			return new RestResultDTO<>(false, errors.toString());
		}
	}

}
