package org.trescal.cwms.rest.tlm.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.tlm.dto.TLMCompleteCalibrationInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMFailureReportInputDTO;

@Component
public class TLMCompleteCalibrationInputDTOValidator extends AbstractBeanValidator {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private UserService userService;
	@Autowired
	private ActionOutcomeService actionOutcomeService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private JobItemService jobItemService;

	@Override
	public boolean supports(Class<?> clazz) {
		return TLMCompleteCalibrationInputDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		TLMCompleteCalibrationInputDTO inputDTO = (TLMCompleteCalibrationInputDTO) target;

		// look up certificate
		Certificate certificate = certificateService.findCertificateByCertNo(inputDTO.getCertificateNumber());
		if (certificate == null) {
			errors.rejectValue("certificateNumber", RestErrors.CODE_CERTIFICATE_NOT_FOUND,
					new Object[] { inputDTO.getCertificateNumber() }, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND);
		}

		// verify that calibration is completable
		if (!certificate.getCal().getStatus().getName().equals(CalibrationStatus.ON_HOLD)) {
			errors.rejectValue("certificateNumber", RestErrors.CODE_CAL_NOT_IN_PROGRESS, new Object[] {},
					RestErrors.MESSAGE_CAL_NOT_IN_PROGRESS);
		}

		// check if all jobitems linked to the calibration are status
		// 'pre-calibration on hold'
		boolean allJobItemsOnPreCalibraiton = certificate.getCal().getLinks().stream().allMatch(cl -> {
			return jobItemService.isReadyToResumeCalibration(cl.getJi()) || jobItemService.isInCalibration(cl.getJi());
		});
		if (!allJobItemsOnPreCalibraiton) {
			errors.rejectValue("certificateNumber", RestErrors.CODE_CAL_NOT_IN_PROGRESS, new Object[] {},
					RestErrors.MESSAGE_CAL_NOT_IN_PROGRESS);
		}

		ActionOutcome ao = this.actionOutcomeService.get(inputDTO.getCalibrationOutcomeId());
		if (ao == null) {
			errors.rejectValue("calibrationOutcomeId", RestErrors.CODE_CALIBRATION_OUTCOME_NOT_DEFINED, new Object[] {},
					RestErrors.MESSAGE_CALIBRATION_OUTCOME_NOT_DEFINED);
		}

		CalibrationClass calClass = getCalClass(inputDTO.getCalibrationDataContent());
		if (calClass == null) {
			errors.rejectValue("calibrationDataContent", RestErrors.CODE_CAL_DATA_NOT_DEFINED, new Object[] {},
					RestErrors.MESSAGE_CAL_DATA_NOT_DEFINED);
		}

		if (StringUtils.isEmpty(inputDTO.getUsername()) && StringUtils.isEmpty(inputDTO.getHrid())) {
			errors.reject(RestErrors.CODE_USERNAME_AND_HRID_EMPTY, RestErrors.MESSAGE_USERNAME_AND_HRID_EMPTY);
		}

		Contact user = null;
		// check username
		if (inputDTO.getUsername() != null) {
			User u = this.userService.get(inputDTO.getUsername());
			user = u.getCon();
			if (u == null || user == null) {
				errors.rejectValue("username", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { inputDTO.getUsername() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}
		// lookup hrid
		if (inputDTO.getHrid() != null) {
			Contact c = this.contactService.getByHrId(inputDTO.getHrid());
			if (c == null) {
				errors.rejectValue("hrid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { inputDTO.getUsername() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		// if failure report is validated, check if all necessary fields are
		// there
		if (inputDTO.getFailureReport() != null && inputDTO.getFailureReport().getManagerValidation() != null
				&& inputDTO.getFailureReport().getManagerValidation()) {
			TLMFailureReportInputDTO frDTO = inputDTO.getFailureReport();
			if (frDTO.getStates() == null || frDTO.getStates().isEmpty() || frDTO.getRecommendations() == null
					|| frDTO.getRecommendations().isEmpty()) {
				errors.rejectValue("failureReport", RestErrors.CODE_FR_VALIDATED_MISSING_DATA, new Object[] {},
						RestErrors.MESSAGE_FR_VALIDATED_MISSING_DATA);
			}
		}

	}

	private CalibrationClass getCalClass(int ordinal) {
		if (ordinal < 0)
			return null;
		if (ordinal >= CalibrationClass.class.getEnumConstants().length)
			return null;
		return CalibrationClass.class.getEnumConstants()[ordinal];
	}

}
