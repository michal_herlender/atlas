package org.trescal.cwms.rest.tlm.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class TLMEngineerAllocationDTO {

	private String techUserName;
	private String techFirstName;
	private String techLastName;
	private LocalDate plannedCalDate;
	private String techHrId;

}
