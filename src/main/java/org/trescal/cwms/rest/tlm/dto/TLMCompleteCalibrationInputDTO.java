package org.trescal.cwms.rest.tlm.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
public class TLMCompleteCalibrationInputDTO {

	@NotNull
	private String certificateNumber;
	private String tlmCertificateNumber;
	@NotNull
	private Integer calibrationOutcomeId;
	@NotNull // calibration class enum
	private Integer calibrationDataContent;
	@NotNull
	private Date calibrationEndDate;
	@NotNull
	private Date calibrationDate;
	@NotNull
	private String calVerificationStatus;
	private String calRemarks;
	private String username;
	private LocalDate nextCalDate;
	private Boolean isCertSigned;
	private List<TLMNoteDTO> notes;
	private String hrid;
	private TLMFailureReportInputDTO failureReport;
	private Boolean cancelCertificate;
	private Boolean optimization;
	private Boolean restriction;
	private Boolean adjustment;
	private Boolean repair;

}
