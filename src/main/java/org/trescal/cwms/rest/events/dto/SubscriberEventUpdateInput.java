package org.trescal.cwms.rest.events.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SubscriberEventUpdateInput {
	@NotNull
	private Integer subscriberId;
	@NotNull
	private Boolean processed;
	@NotNull
    @Size(min = 1, max = 100)
    private List<Integer> eventIds;
}
