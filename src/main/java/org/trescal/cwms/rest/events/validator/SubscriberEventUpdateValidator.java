package org.trescal.cwms.rest.events.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.events.service.EventSubscriberService;
import org.trescal.cwms.core.events.service.SubscriberQueueService;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;
import org.trescal.cwms.core.events.subscriber.entity.SubscriberQueue;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.events.dto.SubscriberEventUpdateInput;

@Component
public class SubscriberEventUpdateValidator extends AbstractBeanValidator {

	@Autowired
	private EventSubscriberService subscriberService;
	@Autowired
	private SubscriberQueueService subscriberQueueService;
	
	@Override
	public boolean supports(Class<?> arg0) {
		return SubscriberEventUpdateInput.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO consider validating whether specified events all exist
		// TODO Also consider rejecting non-unique event IDs
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			// Only proceed with further validation if basic JPA validation of the input succeeds;
			// this safeguards against large incorrect lists of event IDs being provided by caller
			// and similar incorrect calls to the API resulting in expensive database calls
			SubscriberEventUpdateInput input = (SubscriberEventUpdateInput) target;

			EventSubscriber es = this.subscriberService.get(input.getSubscriberId());
			if (es == null)
				errors.rejectValue("subscriberId", "", "Subscriber not found");
			List<SubscriberQueue> events = this.subscriberQueueService.getSubscriberQueueEventsById(input.getEventIds());
			// TODO rewrite as a single query to check for incorrect subscriber id
			boolean isAllMatch = events != null && events.stream().allMatch(e -> e.getSubscriber().getId().equals(input.getSubscriberId()));
			if (!isAllMatch)
				errors.rejectValue("eventIds", "", "subscriberID doesn't matches events specified");

		}
	}
}
