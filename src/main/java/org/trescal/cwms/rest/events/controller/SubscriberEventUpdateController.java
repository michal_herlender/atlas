package org.trescal.cwms.rest.events.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.events.service.SubscriberQueueService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.events.dto.SubscriberEventUpdateInput;
import org.trescal.cwms.rest.events.validator.SubscriberEventUpdateValidator;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class SubscriberEventUpdateController {

    @Autowired
    private RestErrorConverter errorConverter;
    @Autowired
    private SubscriberQueueService subscriberQueueService;
    @Autowired
	private SubscriberEventUpdateValidator validator;
	@Autowired
	private SupportedLocaleService localeService;
	
	@RequestMapping(path="/api/events/update", method=RequestMethod.POST)
	@ResponseBody
	public RestResultDTO<Object> updateEvents(
			@RequestBody SubscriberEventUpdateInput inputDto, BindingResult bindingResult) {
        long startTime = System.currentTimeMillis();
        if (log.isDebugEnabled()) {
            // Debugging to help troubleshoot incorrect external calls to event handler
            String subscriberId = inputDto.getSubscriberId() == null ? "null" : String.valueOf(inputDto.getSubscriberId());
            String eventIds = inputDto.getEventIds() == null ? "null" : String.valueOf(inputDto.getEventIds().size());
            String processed = inputDto.getProcessed() == null ? "null" : String.valueOf(inputDto.getProcessed());

            log.debug("eventIds.size() : " + eventIds + " for subscriber : " + subscriberId + " and processed : " + processed);
        }

        this.validator.validate(inputDto, bindingResult);
        if (bindingResult.hasErrors()) {
            // Primary locale (English) should be OK for web service errors
            Locale primaryLocale = localeService.getPrimaryLocale();
            if (log.isDebugEnabled()) {
                log.debug("validation error count : " + bindingResult.getErrorCount());
            }
            return this.errorConverter.createResultDTO(bindingResult, primaryLocale);
        }

        this.subscriberQueueService.performUpdate(inputDto);
        if (log.isDebugEnabled()) {
            long finishTime = System.currentTimeMillis();
            log.debug("updated in " + (finishTime - startTime) + " ms");
        }

        RestResultDTO<Object> result = new RestResultDTO<Object>(true, "");
        return result;
    }

}
