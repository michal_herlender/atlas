package org.trescal.cwms.rest.events.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.events.service.SubscriberQueueService;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberEventDTO;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;

@Slf4j
@Controller
public class SubscriberEventRetrieveController {

	@Autowired
	private SubscriberQueueService subscriberQueueService;

	@RequestMapping(path = "/api/events/retrieve", method = RequestMethod.GET)
	@ResponseBody
	public RestPagedResultDTO<SubscriberEventDTO> getEvents(
		@RequestParam(name = "subscriberId") Integer subscriberId) {
		long startTime = System.currentTimeMillis();
		if (log.isDebugEnabled())
			log.debug("Returning events for subscriberId " + subscriberId);

		RestPagedResultDTO<SubscriberEventDTO> result = new RestPagedResultDTO<>(true, "", 100, 1);
		boolean processed = false;
		PagedResultSet<SubscriberEventDTO> prs = new PagedResultSet<>(100, 1);
		subscriberQueueService.getEvents(subscriberId, processed, prs);

		prs.getResults().forEach(result::addResult);
		result.setResultsCount(prs.getResultsCount());

		if (log.isDebugEnabled()) {
			long finishTime = System.currentTimeMillis();
			log.debug("Found " + result.getResultsCount() + " events in " + (finishTime - startTime) + " ms");
		}

		return result;
	}
}
