package org.trescal.cwms.rest.adveso.dto.transformer;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportStateEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.tools.EntityDtoTransformer;
import org.trescal.cwms.rest.adveso.dto.AdvesoFailureReportDTO;

@Component
public class FailureReportTransformer implements EntityDtoTransformer<FaultReport, AdvesoFailureReportDTO> {

	@Override
	public FaultReport transform(AdvesoFailureReportDTO dto) {
		// TODO if is needed
		return null;
	}

	@Override
	public AdvesoFailureReportDTO transform(FaultReport entity) {

		AdvesoFailureReportDTO dto = new AdvesoFailureReportDTO();

		dto.setFailureRepId(entity.getFaultRepId());
		// set STATE
		dto.setOutOfSpecification(false);
		dto.setUnknownMPEs(false);
		dto.setOutOfService(false);
		dto.setEquipmentQualityInadequate(false);
		dto.setFailureNotCommunicated(false);
		dto.setMissingAccessories(false);
		dto.setSafetyDefault(false);
		dto.setOther(false);

		for (FailureReportStateEnum state : entity.getStates()) {
			switch (state) {
			case OUT_OF_SPECIFICATION:
				dto.setOutOfSpecification(true);
				break;
			case UNKNOWN_MPES:
				dto.setUnknownMPEs(true);
				break;
			case OUT_OF_SERVICE:
				dto.setOutOfService(true);
				break;
			case EQUIPMENT_QUALITY_INADEQUATE:
				dto.setEquipmentQualityInadequate(true);
				break;
			case FAILURE_NOT_COMMUNICATED:
				dto.setFailureNotCommunicated(true);
				break;
			case MISSING_ACCESSORIES:
				dto.setMissingAccessories(true);
				break;
			case SAFETY_DEFAULT:
				dto.setSafetyDefault(true);
				break;
			case OTHER:
				dto.setOther(true);
				break;
			default:
				break;
			}
		}

		// TODO to check with sothy
		dto.setIndeterminate(entity.getStates().stream().anyMatch(s -> s.isIndeterminate()));
		dto.setNotCompliant(!dto.getIndeterminate());
		dto.setTechnicianRemarks(entity.getTechnicianComments());

		dto.setCalWithJudgment(false);
		dto.setCalWithoutJudgment(false);
		dto.setAdjustment(false);
		dto.setRepair(false);
		dto.setScrappingProposal(false);
		dto.setRestriction(false);

		for (FailureReportRecommendationsEnum value : entity.getRecommendations()) {
			switch (value) {
			// switch (entity.getRecommendation()) {
			case CALIBRATION_WITH_JUDGMENT:
				dto.setCalWithJudgment(true);
				break;
			case CALIBRATION_WITHOUT_JUDGMENT:
				dto.setCalWithoutJudgment(true);
				break;
			case ADJUSTMENT:
				dto.setAdjustment(true);
				break;
			case REPAIR:
				dto.setRepair(true);
				break;
			case SCRAPPING_PROPOSAL:
				dto.setScrappingProposal(true);
				break;
			case RESTRICTION:
				dto.setRestriction(true);
				break;
			default:
				break;
			}
		}

		dto.setCustomerAdviceNecessary(entity.getClientResponseNeeded());
		dto.setManagerComments(entity.getManagerComments());
		dto.setManagerValidationDate(entity.getManagerValidationOn());
		dto.setEstimatedDeliveryTime(entity.getEstDeliveryTime());
		dto.setIssueDate(entity.getIssueDate());

		if (entity.getClientResponseNeeded()) {
			dto.setApprobationDate(entity.getClientApprovalOn());
			dto.setApproved(entity.getClientApproval());
			String approver = null;
			if (entity.getApprovalType() != null)
				approver = entity.getApprovalType().name();
			else if (entity.getClientAlias() != null)
				approver = entity.getClientAlias();
			dto.setApprover(approver);
			dto.setCustomerRemarks(entity.getClientComments());
		}

		dto.setReceived(entity.getClientApproval() != null);
		dto.setDispensation(entity.getDispensation());
		// set FR final outcome
		dto.setFinalOutcome(entity.getFinalOutcome());

		return dto;
	}

}
