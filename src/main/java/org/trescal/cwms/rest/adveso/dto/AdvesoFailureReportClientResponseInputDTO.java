package org.trescal.cwms.rest.adveso.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;

public class AdvesoFailureReportClientResponseInputDTO {

	private Integer failureReportId;
	private Boolean clientApproval;
	private String clientApprovalBy;
	private Date clientApprovalOn;
	private String clientComments;
	private FailureReportRecommendationsEnum clientOutcome;
	@NotNull
	private Integer timeSpent;

	public Integer getFailureReportId() {
		return failureReportId;
	}

	public void setFailureReportId(Integer failureReportId) {
		this.failureReportId = failureReportId;
	}

	public Boolean getClientApproval() {
		return clientApproval;
	}

	public void setClientApproval(Boolean clientApproval) {
		this.clientApproval = clientApproval;
	}

	public String getClientApprovalBy() {
		return clientApprovalBy;
	}

	public void setClientApprovalBy(String clientApprovalBy) {
		this.clientApprovalBy = clientApprovalBy;
	}

	public Date getClientApprovalOn() {
		return clientApprovalOn;
	}

	public void setClientApprovalOn(Date clientApprovalOn) {
		this.clientApprovalOn = clientApprovalOn;
	}

	public String getClientComments() {
		return clientComments;
	}

	public void setClientComments(String clientComments) {
		this.clientComments = clientComments;
	}

	public FailureReportRecommendationsEnum getClientOutcome() {
		return clientOutcome;
	}

	public void setClientOutcome(FailureReportRecommendationsEnum clientOutcome) {
		this.clientOutcome = clientOutcome;
	}

	public Integer getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(Integer timeSpent) {
		this.timeSpent = timeSpent;
	}

}
