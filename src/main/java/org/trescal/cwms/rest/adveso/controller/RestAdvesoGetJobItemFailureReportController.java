package org.trescal.cwms.rest.adveso.controller;

import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.failurereport.enums.FailureReportApprovalTypeEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.adveso.dto.AdvesoFailureReportDTO;
import org.trescal.cwms.rest.adveso.dto.transformer.FailureReportTransformer;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@JsonController
@RequestMapping("/adveso")
public class RestAdvesoGetJobItemFailureReportController {

	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private FailureReportTransformer failureReportTransformer;
	@Autowired
	private FaultReportService faultReportService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/failurereport" }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<AdvesoFailureReportDTO> getLatestFailureReport(
			@RequestParam(name = "jobitemid", required = true) Integer jobitemid, Locale locale) {

		// check if jobitem ha
		RestResultDTO<AdvesoFailureReportDTO> result = new RestResultDTO<>(true, "");
		JobItem jobItem = jobItemService.findJobItem(jobitemid);
		// TODO : confirm this
		// get last failure report
		FaultReport faultRep = jobItem.getFailureReports().stream()
				.sorted(Comparator.nullsLast((fr1, fr2) -> fr2.getIssueDate().compareTo(fr1.getIssueDate())))
				.findFirst().orElse(null);
		if (faultRep != null) {
			AdvesoFailureReportDTO dto = failureReportTransformer.transform(faultRep);
			result.addResult(dto);
		} else {
			result = new RestResultDTO<>(false, "");
		}

		// get result
		return result;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/failurereport/clientresponse" }, method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<Object> failureReportClientResponse(
			@RequestParam(name = "failurerepid", required = true) Integer failurerepid,
			@RequestParam(name = "clientresponse", required = true) String clientresponse,
			@RequestParam(name = "clientfistname", required = true) String clientfirstname,
			@RequestParam(name = "clientlastname", required = true) String clientlastname,
			@RequestParam(name = "companyname", required = true) String companyname,
			@RequestParam(name = "subdivname", required = false) String subdivisionname,
			@RequestParam(name = "dateresponse", required = true) Date dateresponse,
			@RequestParam(name = "clientcomment", required = true) String clientcomment, Locale locale) {

		FaultReport fr = faultReportService.findFaultReport(failurerepid);
		if (fr != null) {
			fr.setClientOutcome(FailureReportRecommendationsEnum.valueOf(clientresponse));
			if (fr.getRecommendations().contains(fr.getClientOutcome()))
				fr.setClientApproval(true);
			else if (fr.getClientOutcome() != null)
				fr.setClientApproval(false);

			if (fr.getClientApproval() != null) {
				fr.setApprovalType(FailureReportApprovalTypeEnum.ADVESO);

				// get contact by name TODO
				// subdivService.
				// Contact approvedBy = null;
				// fr.setClientApprovalBy();
				fr.setClientApprovalOn(dateresponse);
				fr.setClientComments(clientcomment);

				faultReportService.updateFaultReport(fr);
			}

		}

		// get result
		RestResultDTO<Object> result = new RestResultDTO<>(true, "");
		return result;
	}

}
