package org.trescal.cwms.rest.adveso.dto;

import java.math.BigDecimal;

public class AdvesoJobCostingItemDTO {

	private Integer id;
	private BigDecimal adjustmentcost;
	private BigDecimal calcost;
	private BigDecimal inspectioncost;
	private BigDecimal purchasecost;
	private BigDecimal repaircost;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public BigDecimal getAdjustmentcost() {
		return adjustmentcost;
	}
	public void setAdjustmentcost(BigDecimal adjustmentcost) {
		this.adjustmentcost = adjustmentcost;
	}
	public BigDecimal getCalcost() {
		return calcost;
	}
	public void setCalcost(BigDecimal calcost) {
		this.calcost = calcost;
	}
	public BigDecimal getInspectioncost() {
		return inspectioncost;
	}
	public void setInspectioncost(BigDecimal inspectioncost) {
		this.inspectioncost = inspectioncost;
	}
	public BigDecimal getPurchasecost() {
		return purchasecost;
	}
	public void setPurchasecost(BigDecimal purchasecost) {
		this.purchasecost = purchasecost;
	}
	public BigDecimal getRepaircost() {
		return repaircost;
	}
	public void setRepaircost(BigDecimal repaircost) {
		this.repaircost = repaircost;
	}
	
	public AdvesoJobCostingItemDTO() {
		super();
	}
	
	public AdvesoJobCostingItemDTO(Integer id, BigDecimal adjustmentcost, BigDecimal calcost, BigDecimal inspectioncost,
			BigDecimal purchasecost, BigDecimal repaircost) {
		super();
		this.id = id;
		this.adjustmentcost = adjustmentcost;
		this.calcost = calcost;
		this.inspectioncost = inspectioncost;
		this.purchasecost = purchasecost;
		this.repaircost = repaircost;
	}
}
