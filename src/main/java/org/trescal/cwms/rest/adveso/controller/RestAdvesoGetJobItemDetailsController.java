package org.trescal.cwms.rest.adveso.controller;

import java.util.Locale;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobItemDetailsDTO;
import org.trescal.cwms.rest.adveso.service.RestAdvesoJobitemServiceImpl;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@JsonController
@RequestMapping("/adveso")
public class RestAdvesoGetJobItemDetailsController {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private RestAdvesoJobitemServiceImpl restJobitemService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/jobItem" }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<AdvesoJobItemDetailsDTO> getJobItemDetails(
			@RequestParam(name = "jobitemnumber", required = true) String jobitemnumber, 
			@RequestParam(name = "language", required = false) String language, Locale locale) {

		// validation
		JobItem jobitem = jobItemService.findJobItem(jobitemnumber);
		if (jobitem == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, new Object[] { jobitemnumber },
					RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<AdvesoJobItemDetailsDTO>(false, message);
		}

		//get jobitemdetails
		Locale loc = null; 
		if(language != null && !language.isEmpty())
		{
			String[] strLocale = language.split("_");
			if(strLocale.length == 2)
			loc = new Locale(strLocale[0], strLocale[1]);
		}
		else
			loc = locale;
		return restJobitemService.getJobItemDetails(jobitemnumber, loc);
	}
	
}
