package org.trescal.cwms.rest.adveso.dto;

import java.io.Serializable;
import java.util.Date;

import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AdvesoFailureReportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer failureRepId;
	private Boolean notCompliant;
	private Boolean indeterminate;
	private Boolean outOfSpecification;
	private Boolean unknownMPEs;
	private Boolean outOfService;
	private Boolean equipmentQualityInadequate;
	private Boolean failureNotCommunicated;
	private Boolean missingAccessories;
	private Boolean safetyDefault;
	private Boolean other;
	private String technicianRemarks;
	private Boolean customerAdviceNecessary;
	private Boolean calWithJudgment;
	private Boolean calWithoutJudgment;
	private Boolean adjustment;
	private Boolean repair;
	private Boolean scrappingProposal;
	private Boolean restriction;
	private String managerComments;
	private Date issueDate;
	private Date managerValidationDate;
	private Integer estimatedDeliveryTime;
	private Boolean received;
	private Boolean approved;
	private String customerRemarks;
	private Date approbationDate;
	private String approver;
	private Boolean dispensation;
	private FailureReportFinalOutcome finalOutcome;

}