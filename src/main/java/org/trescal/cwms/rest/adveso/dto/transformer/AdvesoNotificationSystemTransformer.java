package org.trescal.cwms.rest.adveso.dto.transformer;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.external.adveso.dto.AdvesoNotificationEventDTO;
import org.trescal.cwms.core.external.adveso.dto.NotificationSystemClientModel;
import org.trescal.cwms.core.external.adveso.dto.NotificationSystemFieldChangeDTO;
import org.trescal.cwms.core.external.adveso.dto.NotificationSystemFieldDTO;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory;
import org.trescal.cwms.core.jobs.jobitemhistory.db.JobItemHistoryService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.EntityDtoTransformer;

import java.util.ArrayList;
import java.util.List;

@Component
public class AdvesoNotificationSystemTransformer
	implements EntityDtoTransformer<NotificationSystemModel, AdvesoNotificationEventDTO> {

	@Autowired
	private JobItemActionService jobItemActionService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobItemHistoryService jobItemHistoryService;

	@Override
	public NotificationSystemModel transform(AdvesoNotificationEventDTO dto) {
		return null;
	}

	@Override
	public AdvesoNotificationEventDTO transform(NotificationSystemModel entity) {
		AdvesoNotificationEventDTO dto = new AdvesoNotificationEventDTO();

		dto.setId(entity.getId());
		dto.setEntityId(new NotificationSystemFieldDTO(entity.getEntityIdFieldName(), entity.getEntityIdFieldType(),
				entity.getEntityIdFieldValue(), entity.getEntityClass()));
		if (entity.getFieldChange() != null)
			dto.setEntityChange(new NotificationSystemFieldChangeDTO(entity.getFieldChange()));
		dto.setOperationType(entity.getOperationType());
		if (entity.getLastModified() != null)
			dto.setLastModified(DateTools.dtf_files.format(entity.getLastModified()));
		if (entity.getCreatedOn() != null) {
			dto.setCreatedOn(DateTools.dtf_files.format(entity.getCreatedOn()));
			dto.setCreatedOnNonFormatted(entity.getCreatedOn());
		}

		JobItem ji = null;
		Instrument instrument = null;
		// get NOTIFICATION link with JOBITEM and INSTRUMENT 
		// used to fill PARENT part on Result DTO
		if (NotificationSystemEntityClassEnum.JobItemAction.equals(dto.getEntityId().getEntityClass())) {
			JobItemAction jia = jobItemActionService.get(Integer.valueOf(dto.getEntityId().getFieldValue()));
			if (jia != null)
				ji = (JobItem) Hibernate.unproxy(jia.getJobItem());
			if (ji != null)
				instrument = (Instrument) Hibernate.unproxy(ji.getInst());
		} else if (NotificationSystemEntityClassEnum.JobItem.equals(dto.getEntityId().getEntityClass())
				&& NotificationSystemOperationTypeEnum.DELETE.equals(dto.getOperationType())) {
			instrument = (Instrument) Hibernate.unproxy(entity.getInstrument());
		} else if (NotificationSystemEntityClassEnum.JobItem.equals(dto.getEntityId().getEntityClass())
				&& (NotificationSystemOperationTypeEnum.UPDATE.equals(dto.getOperationType())
						|| NotificationSystemOperationTypeEnum.CALL_OFF.equals(dto.getOperationType())
						|| NotificationSystemOperationTypeEnum.COMPLETE.equals(dto.getOperationType()))) {
			ji = jobItemService.findJobItem(Integer.parseInt(dto.getEntityId().getFieldValue()));
			if (ji != null)
				instrument = (Instrument) Hibernate.unproxy(ji.getInst());
			// get change comment
			if (entity.getFieldChange() != null) {
				JobItemHistory jih = jobItemHistoryService
					.getJobItemHistoryByChangeDate(entity.getFieldChange().getChangeDate());
				if (jih != null)
					dto.getEntityChange().setComment(jih.getComment());
			}
		}

		if(ji == null)
			ji = entity.getJobItem();

			dto.setParent(new ArrayList<>());
			if (ji != null) {
				dto.getParent().add(new NotificationSystemFieldDTO("jobItemId", NotificationSystemFieldTypeEnum.Integer,
					String.valueOf(ji.getJobItemId()), NotificationSystemEntityClassEnum.JobItem));
				instrument = ji.getInst();
			}
			if (instrument != null) {
				dto.getParent().add(new NotificationSystemFieldDTO("plantid", NotificationSystemFieldTypeEnum.Integer,
					String.valueOf(instrument.getPlantid()), NotificationSystemEntityClassEnum.Instrument));
				dto.getParent().add(new NotificationSystemFieldDTO("plantno", NotificationSystemFieldTypeEnum.String,
					instrument.getPlantno(), NotificationSystemEntityClassEnum.Instrument));
				dto.getParent()
					.add(new NotificationSystemFieldDTO("formerBarCode", NotificationSystemFieldTypeEnum.String,
						instrument.getFormerBarCode(),
						NotificationSystemEntityClassEnum.Instrument));
			}

			// 2018-08-18 : For consistency, recommend using instrument contact's
			// subdiv / company
			// (as we're synchronizing job items against an instrument)
			// 2020-12-16 : Update the dto client field to use job contact informations instead of 
			// instrument contact
			if(ji != null || entity.getJobContact() != null) {
				Contact contact = (Contact) Hibernate.unproxy(ji != null ? ji.getJob().getCon() : entity.getJobContact());
				Subdiv subdiv = (Subdiv) Hibernate.unproxy(contact.getSub());
				Company company = (Company) Hibernate.unproxy(contact.getSub().getComp());

				List<NotificationSystemFieldDTO> parent = new ArrayList<>();
				parent.add(new NotificationSystemFieldDTO("coid", NotificationSystemFieldTypeEnum.Integer,
					String.valueOf(company.getCoid()), NotificationSystemEntityClassEnum.Company));
				NotificationSystemFieldDTO entityId = new NotificationSystemFieldDTO("subdivid",
					NotificationSystemFieldTypeEnum.Integer, String.valueOf(subdiv.getSubdivid()),
					NotificationSystemEntityClassEnum.Subdiv);
				NotificationSystemClientModel client = new NotificationSystemClientModel(entityId, parent);
				dto.setClient(client);
			}
		

		return dto;
	}

}
