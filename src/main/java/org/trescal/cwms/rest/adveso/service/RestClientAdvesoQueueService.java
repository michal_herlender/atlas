package org.trescal.cwms.rest.adveso.service;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.external.adveso.dto.AdvesoEventWrapperDTO;

public interface RestClientAdvesoQueueService {

	void sendEvents(List<AdvesoEventWrapperDTO> events);

	void sendNotifications(List<Integer> notificationIds);

	void sendActivities(List<Integer> resendIds);

	Date getCreatedOnDate(AdvesoEventWrapperDTO e);

}
