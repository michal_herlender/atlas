package org.trescal.cwms.rest.adveso.service;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;

public interface AdvesoEventService {

	public static final String METHOD_UPDATE = "update";
	public static final String METHOD_SAVE = "save";

	void publishJobItemActionEvent(JobItemAction action, String method, Boolean jobItemComplete);

	void publishJobItemDeletionEvent(JobItem ji);
	
	void publishJobItemCallOffItemEvent(JobItem ji);
}
