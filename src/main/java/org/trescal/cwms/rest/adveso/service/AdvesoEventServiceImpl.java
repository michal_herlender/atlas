package org.trescal.cwms.rest.adveso.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.external.adveso.events.DeleteJobItemActionEvent;
import org.trescal.cwms.core.external.adveso.events.DeleteJobItemEvent;
import org.trescal.cwms.core.external.adveso.events.JobItemActionCreateOrUpdateEvent;
import org.trescal.cwms.core.external.adveso.events.JobItemCallOffItemEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;

@Service
public class AdvesoEventServiceImpl implements AdvesoEventService {

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;

	@Override
	public void publishJobItemActionEvent(JobItemAction action, String actionMethod, Boolean jobItemComplete) {

		if (action != null && actionMethod.equals(METHOD_SAVE)) {
			JobItemActionCreateOrUpdateEvent createEvent = new JobItemActionCreateOrUpdateEvent(this, action,
					jobItemComplete);
			applicationEventPublisher.publishEvent(createEvent);
		} else if (actionMethod.equals(METHOD_UPDATE)) {
			if (action != null && action.isDeleted()) {
				DeleteJobItemActionEvent deleteEvent = new DeleteJobItemActionEvent(this, action);
				applicationEventPublisher.publishEvent(deleteEvent);
			} else {
				// case of update : timespent changes...
				JobItemActionCreateOrUpdateEvent createEvent = new JobItemActionCreateOrUpdateEvent(this, action,
						jobItemComplete);
				applicationEventPublisher.publishEvent(createEvent);
			}
		}

	}

	@Override
	public void publishJobItemDeletionEvent(JobItem ji) {

		DeleteJobItemEvent deleteEvent = new DeleteJobItemEvent(this, ji);
		applicationEventPublisher.publishEvent(deleteEvent);

	}

	@Override
	public void publishJobItemCallOffItemEvent(JobItem ji) {

		boolean islinkToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
				ji.getJob().getCon().getSub().getSubdivid(),
				ji.getJob().getOrganisation().getComp().getCoid(),  new Date());
		
		if(islinkToAdveso) {
			JobItemCallOffItemEvent deleteEvent = new JobItemCallOffItemEvent(this, ji);
			applicationEventPublisher.publishEvent(deleteEvent);
		}
	}

}
