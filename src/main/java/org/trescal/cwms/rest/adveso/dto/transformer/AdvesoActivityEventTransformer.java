package org.trescal.cwms.rest.adveso.dto.transformer;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.dto.AdvesoActivityEventDTO;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.EntityDtoTransformer;
import org.trescal.cwms.core.tools.filebrowser.FileWrapper;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivityType;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobCostingItemDTO;

import java.time.ZoneId;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Component
public class AdvesoActivityEventTransformer
	implements EntityDtoTransformer<AdvesoJobItemActivity, AdvesoActivityEventDTO> {

	@Autowired
	private JobCostingItemService jciService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private JobCostingService jobCostingService;
	@Value("${cwms.advesointerface.sharedservices.zoneid}")
  	private String europeParisZoneIdValue;

	@Override
	public AdvesoJobItemActivity transform(AdvesoActivityEventDTO dto) {
		return null;
	}

	@Override
	public AdvesoActivityEventDTO transform(AdvesoJobItemActivity entity) {
		AdvesoActivityEventDTO dto = new AdvesoActivityEventDTO();
		ZoneId europeParisZoneId = ZoneId.of(europeParisZoneIdValue);

		JobItemAction jobItemAction = entity.getJobItemAction();
		if (jobItemAction == null) {
			return null;
		}

		if (entity.getCreatedOn() != null) {
			dto.setCreatedOn(DateTools.dateToLocalDateTime(entity.getCreatedOn(), europeParisZoneId));
			dto.setCreatedOnNonFormatted(entity.getCreatedOn());
		}
		
		if(jobItemAction.getEndStatus() != null) {
			Locale localeEnglish = new Locale("en", "EN");
			// end status description
			dto.setEndStatusDescription(this.translationService.getCorrectTranslation(
					jobItemAction.getEndStatus().getTranslations(), localeEnglish));
			// end status id
			dto.setEndStatusId(jobItemAction.getEndStatus().getStateid());
		}

		dto.setQueueId(entity.getId());
		// job item action id
		dto.setJobItemActionId(jobItemAction.getId());
		// is action deleted
		dto.setIsActionDeleted(jobItemAction.isDeleted());
		// model
		// activity id
		dto.setActivityID(jobItemAction.getActivity().getStateid());
		// activity name
		dto.setActivityName(jobItemAction.getActivityDesc());

		JobItem jobitem = jobItemAction.getJobItem();
		if (jobitem != null) {
			// set jobItemNumber
			dto.setJobItemNumber(jobitem.getItemCode());
			// dto.setModel(jobitem.getInst().getModel().getModel());
//			dto.setModel(instrumentService.getCorrectInstrumentModelName(jobitem.getInst()));
			// plantNo
			dto.setPlantNo(jobitem.getInst().getPlantno());
			// plantId
			dto.setPlantId(jobitem.getInst().getPlantid());
			// manufacturer
//			dto.setManufacturer(jobitem.getInst().getDefinitiveMfrName());
			// approved delivery date
			if (jobitem.getJob().getAgreedDelDate() != null) {
				if (jobitem.getAgreedDelDate() != null)
					dto.setApprovedDeliveryDate(jobitem.getAgreedDelDate());
				else
					dto.setApprovedDeliveryDate(jobitem.getJob().getAgreedDelDate());
			}

			// planned delivery date
			if (jobitem.getDueDate() != null)
				dto.setPlannedDeliveryDate(jobitem.getDueDate());
			// real delivery date
			if (jobitem.getClientReceiptDate() != null)
				dto.setRealDeliveryDate(jobitem.getClientReceiptDate().withZoneSameInstant(europeParisZoneId).toLocalDateTime());
			else if (jobitem.getDateComplete() != null)
				dto.setRealDeliveryDate(DateTools.dateToLocalDateTime(jobitem.getDateComplete(),europeParisZoneId));
			// is on site
			dto.setIsOnSite(jobitem.getJob().getType().equals(JobType.SITE));
			// itemNo
			dto.setItemNo(jobitem.getJobItemId() + "");
			// receipt date
			if (jobitem.getDateIn() != null)
				dto.setReceiptDate(jobitem.getDateIn().withZoneSameInstant(europeParisZoneId).toLocalDateTime());
			else if (jobitem.getJob().getReceiptDate() != null)
				dto.setReceiptDate(jobitem.getJob().getReceiptDate().withZoneSameInstant(europeParisZoneId).toLocalDateTime());
			// jobitem notes

			if (!jobitem.getCalLinks().isEmpty()) {

				// TODO : here return the CalibrationVerificationStatus enum
				// from Certificate instead
				// last calibration status
				long count = jobitem.getCalLinks().size();

				val clOpt =
					(entity.getLinkedEntityType() != null && entity.getLinkedEntityType().equals("Calibaration") ? Optional.of(jobitem.getCalLinks().stream()
						.filter(e -> e.getCal().getId() == entity.getLinkedEntityId()).findFirst()) : Optional.<Optional<CalLink>>empty())
						.orElseGet(() -> jobitem.getCalLinks().stream().skip(count - 1).findFirst());
				// get calibration data from linked entity

				clOpt.ifPresent(cl -> {
					if (cl.getOutcome() != null) {

						if (cl.getCal().getCerts() != null && cl.getCal().getCerts().size() > 0) {
							cl.getCal().getCerts().stream().max(Comparator.comparing(Certificate::getCertDate)).ifPresent(cert -> dto.setCalVerificationStatus(cert.getCalibrationVerificationStatus() != null
								? cert.getCalibrationVerificationStatus().toString()
								: null));
						}
					}
				});
			}
			// treat the case when activity is linked to third party certificate without any calibration entity
			else if(!jobitem.getCertLinks().isEmpty()) {
				int count = jobitem.getCertLinks().size();
				jobitem.getCertLinks().stream().skip(count - 1).findFirst()
					.map(CertLink::getCert).map(Certificate::getCalibrationVerificationStatus)
					.map(CalibrationVerificationStatus::toString)
					.ifPresent(dto::setCalVerificationStatus);
			}

			if (jobitem.getNextWorkReq() != null && jobitem.getNextWorkReq().getWorkRequirement().getServiceType() != null) {
				Locale localeEnglish = new Locale("en", "EN");
				// service type
				dto.setServiceType(this.translationService.getCorrectTranslation(
					jobitem.getNextWorkReq().getWorkRequirement().getServiceType().getShortnameTranslation(), localeEnglish));
				// service type id
				dto.setServicetypeId(jobitem.getNextWorkReq().getWorkRequirement().getServiceType().getServiceTypeId());
			} else if (jobitem.getServiceType().getCalibrationType() != null) {
				Locale localeEnglish = new Locale("en", "EN");
				// service type
				dto.setServiceType(this.translationService.getCorrectTranslation(
					jobitem.getServiceType().getShortnameTranslation(), localeEnglish));
				// service type id
				dto.setServicetypeId(jobitem.getServiceType().getServiceTypeId());
			}
			//
			if (jobitem.getJob().getCon().getSub() != null) {
				// subdiv id
				dto.setSubdivID(jobitem.getJob().getCon().getSub().getSubdivid());
			}
			//
			if (jobitem.getInst().getInstrumentComplementaryField() != null)
				dto.setFormerID(jobitem.getInst().getFormerBarCode());

			dto.setIsDeleted(false);
			// get jobItem Costing
			List<AdvesoJobCostingItemDTO> jobCostingItems = jciService
				.findIssuedJobCostingItemByJobItemID(jobitem.getJobItemId());
			dto.setJobCostingItems(jobCostingItems);
			JobItemAction goodsInJiAct = jobitem.getActions().iterator().next();
			if (ItemActivityType.TRANSIT_ACTIVITY.equals(jobItemAction.getActivity().getType())
				&& goodsInJiAct.equals(jobItemAction)
				&& jobitem.getServiceType().getShortName().equals("REP")) {
				String activityRemark = jobItemAction.getRemark();
				if (activityRemark != null) {

					String[] strTabs = activityRemark.split(":");
					if (strTabs.length >= 2) {
						String temp = strTabs[1].trim().replaceAll("<.*?>", "");
						dto.setOldJobItemReference(temp);
					}
				}
			}
		} else
			dto.setIsDeleted(true);

		// start date
		if (jobItemAction.getStartStamp() != null)
			dto.setStartDate(DateTools.dateToLocalDateTime(jobItemAction.getStartStamp(), europeParisZoneId));
		// remarks
		dto.setRemarks(jobItemAction.getRemark());


		// set actionOutcome
		if (jobItemAction instanceof JobItemActivity) {
			JobItemActivity jia = (JobItemActivity) jobItemAction;
			if (jia.getOutcome() != null) {
				dto.setActionOutComeID(jia.getOutcome().getId());
				dto.setActionOutComeName(jia.getOutcome().getGenericValue().getDescription());
			}

			if (jia.getCompletedBy() != null) {
				dto.setTechnicienName(jia.getCompletedBy().getName());
			}
		} else if (jobItemAction.getStartedBy() != null) {
			dto.setTechnicienName(jobItemAction.getStartedBy().getName());
		}

		if (entity.getLinkedEntityType() != null) {
			// set certificate

			// set jobcosting
			if (entity.getLinkedEntityType().equals("JobCosting")) {
				JobCosting jobCosting = jobCostingService.findJobCosting(entity.getLinkedEntityId());
				if (jobCosting != null) {
					setJobCostingDetails(dto, jobCosting);
				}
			}
		}

		// if JobCostingFileId is not set
		if (StringUtils.isBlank(dto.getJobCostingFileId())
				&& StringUtils.isNotBlank(entity.getJobCostingFileName())) {
			String[] fileNameSplit = entity.getJobCostingFileName().split(" ");
			if (fileNameSplit.length >= 4) {
				Integer jobCostingVersion = Integer.valueOf(fileNameSplit[3]);
				JobCosting jobCosting = jobCostingService.getJobCosting(jobItemAction.getJobItem().getJobItemId(),
					jobCostingVersion);
				if (jobCosting != null) {
					setJobCostingDetails(dto, jobCosting);
				}
			}
		}

		return dto;
	}

	private void setJobCostingDetails(AdvesoActivityEventDTO dto,
									  JobCosting jobCosting) {
		FileWrapper file = this.jobCostingService.getLastFileRevision(jobCosting);
		if (file != null)
			dto.setJobCostingFileId(file.getFilePathEncrypted());

		dto.setJobCostingType(jobCosting.getType().name());
	}

}
