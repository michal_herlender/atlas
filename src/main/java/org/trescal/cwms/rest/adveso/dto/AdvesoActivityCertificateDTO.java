package org.trescal.cwms.rest.adveso.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AdvesoActivityCertificateDTO {

	private String plantNo;
	private int plantId;
	private String subdivName;
	private String itemNo;
	private String calibrationStatus;
	private String serviceType;
	private String remarks;
	private int subdivId;
	private List<String> contactEmails;
	private String formerId;
	private int calibrationStatusId;
	private String jobItemNumber;
	private int serviceTypeId;
	private String calibrationVerificationStatus;
	private String actionOutComeId;
	private String actionOutComeName;
	private String technicienName;
	private Date interventionDate;
	private Date documentDate;	
	private String externalDocumentNumber;	
	private String documentNumber;	
	private String documentId;	
	private String technicienHrId;	
	private byte[] documentBinary;
	private String documentTitle;
	private Integer certificateid;
	private String certificateStatus;
	private Integer replacementfor;
	private String replacementComments;
	//additional informations from tlm
    private Boolean optimization;
    private Boolean restriction;
    private Boolean adjustment;
    private Boolean repair;
	
}
