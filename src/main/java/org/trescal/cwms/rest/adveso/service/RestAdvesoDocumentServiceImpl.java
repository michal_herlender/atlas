package org.trescal.cwms.rest.adveso.service;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.tools.EncryptionTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.tools.filebrowser.FileWrapper;
import org.trescal.cwms.rest.adveso.dto.AdvesoDocumentDTO;
import org.trescal.cwms.rest.adveso.dto.AdvesoDownloadDocumentDTO;
import org.trescal.cwms.rest.adveso.utils.AdvesoDocumentTypeEnum;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RestAdvesoDocumentServiceImpl implements RestAdvesoDocumentService {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private JobCostingService jobCostingService;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private RepairInspectionReportService repairInspectionReportService;
	@Autowired
	private MessageSource messageSource;

	@Override
	public RestResultDTO<List<AdvesoDocumentDTO>> getDocumentList(Integer jobitemid, String documenttype,
			Integer subdivId) {
		Set<AdvesoDocumentDTO> resultList = new LinkedHashSet<>();
		if (documenttype == null) {
			/*- get all document for the job item- */

			// certificates files
			List<Certificate> certificates = certificateService.getCertificateListByJobitemId(jobitemid);
			certificates = certificates.stream().filter(c -> CertStatusEnum.SIGNED.equals(c.getStatus()))
				.collect(Collectors.toList());
			for (Certificate certificate : certificates) {
				FileBrowserWrapper rw = fileBrowserServ
					.getFilesForComponentRoot(Component.CERTIFICATE, certificate.getCertid(), null);
				List<AdvesoDocumentDTO> dtos = convertCertificateDocumentToDocumentDTO(rw.getFiles(),
					certificate.getCertno(), certificate.getThirdCertNo());
				resultList.addAll(dtos);
			}

			// JobCosting files
			List<JobCosting> jobscosting = jobCostingService.getJobCostingListByJobitemId(jobitemid);
			for (JobCosting jobCosting : jobscosting) {
				List<AdvesoDocumentDTO> dtos = convertJobCostingDocumentToDocumentDTO(jobCosting);
				resultList.addAll(dtos);
			}

			// DeliveryNote files
			List<ClientDelivery> deliveriesnote = deliveryService.getClientDeliveryListByJobitemId(jobitemid);
			for (ClientDelivery deliveryNote : deliveriesnote) {
				FileBrowserWrapper rw = fileBrowserServ
					.getFilesForComponentRoot(Component.DELIVERY, deliveryNote.getDeliveryid(), null);
				List<AdvesoDocumentDTO> dtos = convertDeliveryNoteDocumentToDocumentDTO(rw.getFiles(),
					deliveryNote.getDeliveryno());
				resultList.addAll(dtos);
			}

			// fault reports
			FaultReport faultReport = faultReportService.getLastFailureReportByJobitemId(jobitemid, true);
			if (faultReport != null) {
				FileBrowserWrapper rw = fileBrowserServ
					.getFilesForComponentRoot(Component.FAILURE_REPORT, faultReport.getFaultRepId(), null);
				List<AdvesoDocumentDTO> dtos = convertFailureReportDocumentToDocumentDTO(rw.getFiles(),
					faultReport.getFaultReportNumber(), null);
				resultList.addAll(dtos);
			}

			// repair reports
			List<RepairInspectionReport> rirs = repairInspectionReportService.getAllRirByJobItemId(jobitemid);
			if (rirs != null) {
				for (RepairInspectionReport rir : rirs) {
					FileBrowserWrapper rw = fileBrowserServ
						.getFilesForComponentRoot(Component.REPAIR_INSPECTION_REPORT, rir.getRirId(), null);
					List<AdvesoDocumentDTO> dtos = convertRepairDocumentToDocumentDTO(rw.getFiles(), null,
						rir.getRirNumber(),
						rir.getRepairCompletionReport() != null ? rir.getRepairCompletionReport().getRcrNumber()
							: null);
					resultList.addAll(dtos);
				}
			}

		} else {
			if (AdvesoDocumentTypeEnum.CERTIFICATE.toString().equalsIgnoreCase(documenttype)) {
				List<Certificate> certificates = certificateService.getCertificateListByJobitemId(jobitemid);
				certificates = certificates.stream().filter(c -> CertStatusEnum.SIGNED.equals(c.getStatus()))
						.collect(Collectors.toList());
				for (Certificate certificate : certificates) {
					FileBrowserWrapper rw = fileBrowserServ
						.getFilesForComponentRoot(Component.CERTIFICATE, certificate.getCertid(), null);
					List<AdvesoDocumentDTO> dtos = convertCertificateDocumentToDocumentDTO(rw.getFiles(),
						certificate.getCertno(), certificate.getThirdCertNo());
					resultList.addAll(dtos);
				}
			} else if (AdvesoDocumentTypeEnum.JOB_COSTING.toString().equalsIgnoreCase(documenttype)) {

				List<JobCosting> jobsCosting = jobCostingService.getJobCostingListByJobitemId(jobitemid);
				for (JobCosting jobCosting : jobsCosting) {
					List<AdvesoDocumentDTO> dtos = convertJobCostingDocumentToDocumentDTO(jobCosting);
					resultList.addAll(dtos);
				}
			} else if (AdvesoDocumentTypeEnum.DELIVERY_NOTE.toString().equalsIgnoreCase(documenttype)) {
				List<ClientDelivery> deliveriesnote = deliveryService.getClientDeliveryListByJobitemId(jobitemid);
				for (ClientDelivery deliveryNote : deliveriesnote) {
					FileBrowserWrapper rw = fileBrowserServ
						.getFilesForComponentRoot(Component.DELIVERY, deliveryNote.getDeliveryid(), null);
					List<AdvesoDocumentDTO> dtos = convertDeliveryNoteDocumentToDocumentDTO(rw.getFiles(),
						deliveryNote.getDeliveryno());
					resultList.addAll(dtos);
				}
			} else if (AdvesoDocumentTypeEnum.FAILURE_REPORT_WITH_CLIENT_RESPONSE.toString()
					.equalsIgnoreCase(documenttype)
					|| AdvesoDocumentTypeEnum.FAILURE_REPORT_WITHOUT_CLIENT_RESPONSE.toString()
							.equalsIgnoreCase(documenttype)) {
				FaultReport faultReport = faultReportService.getLastFailureReportByJobitemId(jobitemid, true);
				if (faultReport != null) {
					FileBrowserWrapper rw = fileBrowserServ
						.getFilesForComponentRoot(Component.FAILURE_REPORT, faultReport.getFaultRepId(), null);
					List<AdvesoDocumentDTO> dtos = convertFailureReportDocumentToDocumentDTO(rw.getFiles(),
						faultReport.getFaultReportNumber(), documenttype);
					resultList.addAll(dtos);
				}
			} else if (AdvesoDocumentTypeEnum.REPAIR_EVALUATION_REPORT.toString().equalsIgnoreCase(documenttype)) {
				List<RepairInspectionReport> rirs = null;
				if (subdivId != null) {
					RepairInspectionReport rir = repairInspectionReportService.getRirByJobItemIdAndSubdivId(jobitemid,
							subdivId);
					if (rir != null) {
						rirs = new ArrayList<>();
						rirs.add(rir);
					}
				} else
					rirs = repairInspectionReportService.getAllRirByJobItemId(jobitemid);
				if (rirs != null)
					for (RepairInspectionReport rir : rirs) {
						FileBrowserWrapper rw = fileBrowserServ
							.getFilesForComponentRoot(Component.REPAIR_INSPECTION_REPORT, rir.getRirId(), null);
						List<AdvesoDocumentDTO> dtos = convertRepairDocumentToDocumentDTO(rw.getFiles(),
							RepairInspectionReport.repairInspectionReportIdentifierPrefix, rir.getRirNumber(),
							rir.getRepairCompletionReport() != null ? rir.getRepairCompletionReport().getRcrNumber()
								: null);
						resultList.addAll(dtos);
					}
			} else if (AdvesoDocumentTypeEnum.REPAIR_COMPLETION_REPORT.toString().equalsIgnoreCase(documenttype)) {
				List<RepairInspectionReport> rirs = repairInspectionReportService.getAllRirByJobItemId(jobitemid);
				if (rirs != null && !rirs.isEmpty()) {
					FileBrowserWrapper rw = fileBrowserServ
						.getFilesForComponentRoot(Component.REPAIR_INSPECTION_REPORT, rirs.get(0).getRirId(), null);
					List<AdvesoDocumentDTO> dtos = convertRepairDocumentToDocumentDTO(rw.getFiles(),
						RepairCompletionReport.repairCompletionReportIdentifierPrefix, rirs.get(0).getRirNumber(),
						rirs.get(0).getRepairCompletionReport() != null
							? rirs.get(0).getRepairCompletionReport().getRcrNumber()
							: null);
					resultList.addAll(dtos);
				}
			}

		}

		RestPagedResultDTO<List<AdvesoDocumentDTO>> result = new RestPagedResultDTO<>(true, "",
			1000, 1);
		result.setResultsCount(resultList.size());
		result.addResult(new ArrayList<>(resultList));
		return result;

	}

	@Override
	public RestResultDTO<AdvesoDownloadDocumentDTO> downloadDocument(String documentid, Locale locale) {

		// documentid is the encrypted path for the file
		AdvesoDownloadDocumentDTO dto = new AdvesoDownloadDocumentDTO();

		String decryptedFileNamePath;

		try {
			// decrypt file
			decryptedFileNamePath = EncryptionTools.decrypt(documentid);

			// read file
			File file = new File(decryptedFileNamePath);
			dto.setDocumentId(documentid);
			dto.setDocumentTitle(file.getName());
			dto.setSize(FileUtils.byteCountToDisplaySize(file.length()));
			dto.setData(FileUtils.readFileToByteArray(file));

		} catch (Exception e) {
			e.printStackTrace();
			String message = messageSource.getMessage(RestErrors.CODE_ERROR_READING_FILE, new Object[]{},
				RestErrors.MESSAGE_ERROR_READING_FILE, locale);
			return new RestResultDTO<>(false, message);
		}

		RestPagedResultDTO<AdvesoDownloadDocumentDTO> result = new RestPagedResultDTO<>(true,
			"", 1000, 1);
		result.setResultsCount(1);
		result.addResult(dto);
		return result;
	}

	private List<AdvesoDocumentDTO> convertJobCostingDocumentToDocumentDTO(JobCosting jobCosting) {

		List<AdvesoDocumentDTO> dtos = new ArrayList<>();

		FileWrapper fw = this.jobCostingService.getLastFileRevision(jobCosting);
	
		if (fw != null) {
			AdvesoDocumentDTO dto = new AdvesoDocumentDTO();
			dto.setDocumentType(AdvesoDocumentTypeEnum.JOB_COSTING);
			dto.setDocumentTitle(fw.getFileName());
			dto.setDocumentExtension(fw.getExtension());
			dto.setDocumentId(fw.getFilePathEncrypted());
			dto.setDocumentName(jobCosting.getJob().getJobno());
			dto.setDocumentLastModified(fw.getLastModified());
			dtos.add(dto);
		}
			
		return dtos;
	}

	private List<AdvesoDocumentDTO> convertCertificateDocumentToDocumentDTO(List<FileWrapper> files,
			String certificateNumber, String thirdCetNumber) {

		List<AdvesoDocumentDTO> dtos = new ArrayList<>();

		if (files != null) {
			for (FileWrapper file : files) {
				if (!file.isDirectory() && file.getFileName().contains(certificateNumber)
						&& file.getFileName().endsWith(".pdf")) {
					AdvesoDocumentDTO dto = new AdvesoDocumentDTO();
					dto.setDocumentType(AdvesoDocumentTypeEnum.CERTIFICATE);
					dto.setDocumentTitle(file.getFileName());
					dto.setDocumentExtension(file.getExtension());
					dto.setDocumentId(file.getFilePathEncrypted());
					dto.setDocumentExternalReference(thirdCetNumber);
					dto.setDocumentName(certificateNumber);
					dto.setDocumentLastModified(file.getLastModified());
					dtos.add(dto);
				}
			}
		}

		return dtos;
	}

	private List<AdvesoDocumentDTO> convertDeliveryNoteDocumentToDocumentDTO(List<FileWrapper> files,
			String deliveryNo) {

		List<AdvesoDocumentDTO> dtos = new ArrayList<>();

		if (files != null) {
			for (FileWrapper file : files) {
				if (!file.isDirectory() && file.getExtension().equals("pdf")) {
					AdvesoDocumentDTO dto = new AdvesoDocumentDTO();
					dto.setDocumentType(AdvesoDocumentTypeEnum.DELIVERY_NOTE);
					dto.setDocumentTitle(file.getFileName());
					dto.setDocumentExtension(file.getExtension());
					dto.setDocumentId(file.getFilePathEncrypted());
					dto.setDocumentName(deliveryNo);
					dto.setDocumentLastModified(file.getLastModified());
					dtos.add(dto);
				}
			}
		}

		return dtos;
	}

	private List<AdvesoDocumentDTO> convertRepairDocumentToDocumentDTO(List<FileWrapper> files, String documentPrefix,
			String rirNumber, String rcrNumber) {

		List<AdvesoDocumentDTO> dtos = new ArrayList<>();

		if (files != null) {
			for (FileWrapper file : files) {
				if (!file.isDirectory() && file.getExtension().equals("pdf")
						&& ((documentPrefix == null && (file.getFileName().startsWith(
								RepairInspectionReport.repairInspectionReportIdentifierPrefix + rirNumber + " - ")
								|| file.getFileName()
										.startsWith(RepairCompletionReport.repairCompletionReportIdentifierPrefix
												+ rcrNumber + " - ")
								|| file.getFileName()
										.startsWith(RepairCompletionReport.repairCompletionReportAppendixIdentifierPrefix
												+ rcrNumber)))
								|| (documentPrefix != null
										&& (file.getFileName().startsWith(documentPrefix + rirNumber + " - ") || file
												.getFileName().startsWith(documentPrefix + rcrNumber + " - "))))) {
					AdvesoDocumentDTO dto = new AdvesoDocumentDTO();
					if (file.getFileName().startsWith(
							RepairInspectionReport.repairInspectionReportIdentifierPrefix + rirNumber + " - ")) {
						dto.setDocumentType(AdvesoDocumentTypeEnum.REPAIR_EVALUATION_REPORT);
						dto.setDocumentName(rirNumber);
					} else if (file.getFileName().startsWith(
						RepairCompletionReport.repairCompletionReportIdentifierPrefix + rcrNumber + " - ") ||
						file.getFileName().startsWith(
							RepairCompletionReport.repairCompletionReportAppendixIdentifierPrefix + rcrNumber)) {
						dto.setDocumentType(AdvesoDocumentTypeEnum.REPAIR_COMPLETION_REPORT);
						dto.setDocumentName(rcrNumber);
					}
					dto.setDocumentTitle(file.getFileName());
					dto.setDocumentExtension(file.getExtension());
					dto.setDocumentId(file.getFilePathEncrypted());
					dto.setDocumentLastModified(file.getLastModified());
					dtos.add(dto);
				}
			}
		}

		return dtos;
	}

	private List<AdvesoDocumentDTO> convertFailureReportDocumentToDocumentDTO(List<FileWrapper> files,
			String faultReportNumber, String documentType) {

		List<AdvesoDocumentDTO> dtos = new ArrayList<>();

		if (files != null) {
			for (FileWrapper file : files) {
				if (!file.isDirectory() && faultReportNumber != null && file.getFileName() != null
						&& file.getFileName().contains(faultReportNumber) && file.getExtension().equals("pdf")) {

					String FR_WITH_CR = "With Client Response";
					if ((documentType == null || AdvesoDocumentTypeEnum.FAILURE_REPORT_WITH_CLIENT_RESPONSE.toString()
						.equals(documentType)) && file.getFileName().contains(FR_WITH_CR)) {
						AdvesoDocumentDTO dto = new AdvesoDocumentDTO();
						dto.setDocumentType(AdvesoDocumentTypeEnum.FAILURE_REPORT_WITH_CLIENT_RESPONSE);
						dto.setDocumentTitle(file.getFileName());
						dto.setDocumentExtension(file.getExtension());
						dto.setDocumentId(file.getFilePathEncrypted());
						dto.setDocumentName(faultReportNumber);
						dto.setDocumentLastModified(file.getLastModified());
						dtos.add(dto);
					}

					String FR_WITHOUT_CR = "Without Client Response";
					if ((documentType == null || AdvesoDocumentTypeEnum.FAILURE_REPORT_WITHOUT_CLIENT_RESPONSE
						.toString().equals(documentType)) && file.getFileName().contains(FR_WITHOUT_CR)) {
						AdvesoDocumentDTO dto = new AdvesoDocumentDTO();
						dto.setDocumentType(AdvesoDocumentTypeEnum.FAILURE_REPORT_WITHOUT_CLIENT_RESPONSE);
						dto.setDocumentTitle(file.getFileName());
						dto.setDocumentExtension(file.getExtension());
						dto.setDocumentId(file.getFilePathEncrypted());
						dto.setDocumentName(faultReportNumber);
						dto.setDocumentLastModified(file.getLastModified());
						dtos.add(dto);
					}
				}
			}
		}

		return dtos;
	}

}
