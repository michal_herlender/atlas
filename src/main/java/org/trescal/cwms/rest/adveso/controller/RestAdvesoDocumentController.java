package org.trescal.cwms.rest.adveso.controller;

import java.util.List;
import java.util.Locale;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.adveso.dto.AdvesoDocumentDTO;
import org.trescal.cwms.rest.adveso.dto.AdvesoDownloadDocumentDTO;
import org.trescal.cwms.rest.adveso.service.RestAdvesoDocumentServiceImpl;
import org.trescal.cwms.rest.adveso.utils.AdvesoDocumentTypeEnum;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@JsonController
@RequestMapping("/adveso")
public class RestAdvesoDocumentController {

	@Autowired
	private RestAdvesoDocumentServiceImpl advesoDocumentService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private MessageSource messageSource;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/getDocumentList" }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<List<AdvesoDocumentDTO>> getDocumentList(
			@RequestParam(required = true) @NotNull Integer jobitemid,
			@RequestParam(required = false) String documenttype,
			@RequestParam(required = false) @NotNull Integer subdivid,
			Locale locale) {

		// validate jobitem existence
		JobItem jobitem = jobItemService.findJobItem(jobitemid);
		if (jobitem == null) {
			String message = messageSource.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, new Object[] { jobitemid },
					RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new RestResultDTO<List<AdvesoDocumentDTO>>(false, message);
		}

		// validate documenttype if it is part of the enum
		if (StringUtils.isNoneBlank(documenttype)
				&& !EnumUtils.isValidEnum(AdvesoDocumentTypeEnum.class, documenttype.toUpperCase())) {
			String message = messageSource.getMessage(RestErrors.CODE_ADVESO_DOCUMENT_TYPE_NOT_SUPPORTED,
					new Object[] { documenttype }, RestErrors.MESSAGE_ADVESO_DOCUMENT_TYPE_NOT_SUPPORTED, locale);
			return new RestResultDTO<List<AdvesoDocumentDTO>>(false, message);
		}

		return advesoDocumentService.getDocumentList(jobitemid, documenttype, subdivid);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/downloadDocument" }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<AdvesoDownloadDocumentDTO> downloadDocument(@RequestParam @NotNull String documentid, Locale locale) {
		return advesoDocumentService.downloadDocument(documentid, locale);
	}

}
