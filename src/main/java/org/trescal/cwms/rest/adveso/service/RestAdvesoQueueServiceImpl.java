package org.trescal.cwms.rest.adveso.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Service
public class RestAdvesoQueueServiceImpl implements RestAdvesoQueueService {

	@Autowired
	private AdvesoJobItemActivityService advesoJobItemActivityService;
	
	@Autowired
	private NotificationSystemModelService notificationSystemModelService;

	@Override
	public RestResultDTO<String> updateReceiptState(AdvesoJobItemActivity jia, Boolean isSuccess, String errorMessage) {

		jia.setIsReceived(isSuccess);
		if (isSuccess) {
			jia.setReceivedOn(new Date());
		} else {
			jia.setLastErrorMessage(errorMessage);
			jia.setLastErrorMessageOn(new Date());
		}
		advesoJobItemActivityService.update(jia);

		RestPagedResultDTO<String> result = new RestPagedResultDTO<>(true, "", 1, 1);
		result.setResultsCount(1);
		return result;

	}
	
	@Override
	public RestResultDTO<String> updateTreatementState(AdvesoJobItemActivity jia, Boolean isSuccess, String errorMessage) {

		jia.setIsTreated(isSuccess);
		if (isSuccess) {
			jia.setTreatedOn(new Date());
		} else {
			jia.setLastErrorMessage(errorMessage);
			jia.setLastErrorMessageOn(new Date());
		}
		advesoJobItemActivityService.update(jia);

		RestPagedResultDTO<String> result = new RestPagedResultDTO<>(true, "", 1, 1);
		result.setResultsCount(1);
		return result;

	}
	
	@Override
	public RestResultDTO<String> updateNotificationReceiptState(NotificationSystemModel nsm, Boolean isSuccess, String errorMessage) {

		nsm.setIsReceived(isSuccess);
		if (isSuccess) {
			nsm.setReceivedOn(new Date());
		} else {
			nsm.setLastErrorMessage(errorMessage);
			nsm.setLastErrorMessageOn(new Date());
		}
		notificationSystemModelService.update(nsm);

		RestPagedResultDTO<String> result = new RestPagedResultDTO<>(true, "", 1, 1);
		result.setResultsCount(1);
		return result;

	}

}
