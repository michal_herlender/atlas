package org.trescal.cwms.rest.adveso.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.adveso.dto.AdvesoFailureReportClientResponseInputDTO;
import org.trescal.cwms.rest.alligator.component.RestErrors;

@Component
public class AdvesoFailureReportClientResponseValidator extends AbstractBeanValidator {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public boolean supports(Class<?> clazz) {
		return AdvesoFailureReportClientResponseInputDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		AdvesoFailureReportClientResponseInputDTO inputDTO = (AdvesoFailureReportClientResponseInputDTO) target;

		// look up failure report
		if (inputDTO.getFailureReportId() == null) {
			errors.rejectValue("failureReportId", RestErrors.CODE_DATA_INVALID, new Object[] { inputDTO.getFailureReportId() },
					RestErrors.MESSAGE_DATA_INVALID);
		} else {
			FaultReport fr = faultReportService.findFaultReport(inputDTO.getFailureReportId());
			if (fr == null) {
				errors.rejectValue("failureReportId", RestErrors.CODE_DATA_NOT_FOUND,
						new Object[] { inputDTO.getFailureReportId() }, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}

		// check all mandatory fields
		if (inputDTO.getClientApproval() == null) {
			errors.rejectValue("clientApproval", RestErrors.CODE_DATA_INVALID, new Object[] { inputDTO.getClientApproval() },
					RestErrors.MESSAGE_DATA_INVALID);
		}

		if (StringUtils.isBlank(inputDTO.getClientApprovalBy())) {
			errors.rejectValue("clientApprovalBy", RestErrors.CODE_DATA_INVALID, new Object[] { inputDTO.getClientApprovalBy() },
					RestErrors.MESSAGE_DATA_INVALID);
		}

		if (inputDTO.getClientApprovalOn() == null) {
			errors.rejectValue("clientApprovalOn", RestErrors.CODE_DATA_INVALID, new Object[] { inputDTO.getClientApprovalOn() },
					RestErrors.MESSAGE_DATA_INVALID);
		}

	}

}
