package org.trescal.cwms.rest.adveso.dto;


public class AdvesoActivityDTO {

	private int id;
	private String name;
	private boolean isTransferable;
	private boolean isDisplayed;
	private Integer subdivID;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isTransferable() {
		return isTransferable;
	}
	public void setTransferable(boolean isTransferable) {
		this.isTransferable = isTransferable;
	}
	public boolean isDisplayed() {
		return isDisplayed;
	}
	public void setDisplayed(boolean isDisplayed) {
		this.isDisplayed = isDisplayed;
	}
	public Integer getSubdivID() {
		return subdivID;
	}
	public void setSubdivID(Integer subdivID) {
		this.subdivID = subdivID;
	}
	
	public AdvesoActivityDTO(int id, String name, boolean isTransferable, boolean isDisplayed, Integer subdivID) {
		super();
		this.id = id;
		this.name = name;
		this.isTransferable = isTransferable;
		this.isDisplayed = isDisplayed;
		this.subdivID = subdivID;
	}
	
	public AdvesoActivityDTO(int id, String name, boolean isTransferable, boolean isDisplayed) {
		super();
		this.id = id;
		this.name = name;
		this.isTransferable = isTransferable;
		this.isDisplayed = isDisplayed;
	}
	
	@Override
	public boolean equals(Object arg0) {
		return (arg0 instanceof AdvesoActivityDTO && this.id == ((AdvesoActivityDTO) arg0).id);
	}
	
}
