package org.trescal.cwms.rest.adveso.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdvesoJobItemDetailsDTO {

    private String model;
    private String plantNo;
    private int plantId;
    private String serialNo;
    private int subdivId;
    private int calFrequency;
    private String subdivName;
    private List<String> procedureReference;
    private String lastActivityName;
    private int lastActivityId;
    private String technicianFullName;
    private String manufacturer;
    private ZonedDateTime collectDate;
    private LocalDate plannedDeliveryDate;
    private LocalDateTime realDeliveryDate;
    private LocalDate lastCalibrationDate;
    private List<String> jobItemNotes;
    private String jobItemNumber;
    private ZonedDateTime receiptDate;
    private List<AdvesoActivityWrapper> activityList;
    private String lastCalibrationStatus;
    private String lastCalibrationProcess;
    private String serviceType;
    private String serviceTypeDescription;

}
