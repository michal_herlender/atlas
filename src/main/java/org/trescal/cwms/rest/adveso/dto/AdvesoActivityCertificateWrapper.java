package org.trescal.cwms.rest.adveso.dto;

public class AdvesoActivityCertificateWrapper {

	//Token used by Shared Services to identify service providers
	public final static String AK = "216FA9C8-9267-476C-A93D-E5BBA9FA75C0";
	
	private AdvesoActivityCertificateDTO dto;
	private String accessKey;

	public AdvesoActivityCertificateDTO getDto() {
		return dto;
	}

	public void setDto(AdvesoActivityCertificateDTO dto) {
		this.dto = dto;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public AdvesoActivityCertificateWrapper() {
		super();
		this.accessKey = this.AK;
	}

}
