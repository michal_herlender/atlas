package org.trescal.cwms.rest.adveso.service;

import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

public interface RestAdvesoQueueService {

	RestResultDTO<String> updateReceiptState(AdvesoJobItemActivity jia, Boolean isSuccess, String errorMessage);

	RestResultDTO<String> updateTreatementState(AdvesoJobItemActivity jia, Boolean isSuccess, String errorMessage);

	RestResultDTO<String> updateNotificationReceiptState(NotificationSystemModel nsm, Boolean isSuccess,
			String errorMessage);

}
