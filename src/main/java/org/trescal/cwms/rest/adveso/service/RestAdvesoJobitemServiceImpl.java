package org.trescal.cwms.rest.adveso.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.db.AdvesoOverriddenActivitySettingService;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db.AdvesoTransferableActivityService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityWrapper;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobItemDetailsDTO;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.trescal.cwms.core.tools.TranslationUtils.getBestTranslation;

@Service
public class RestAdvesoJobitemServiceImpl implements RestAdvesoJobitemService {

	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobItemActionService jobItemActionService;
	@Autowired
	private EngineerAllocationService engineerAllocationService;
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private AdvesoOverriddenActivitySettingService advesoOverriddenActivitySettingService;
	@Autowired
	private AdvesoTransferableActivityService advesoTransferableActivityService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private AdvesoJobItemActivityService advesoJobItemActivityService;
	@Autowired
	private CertificateService certificateService;
	@Value("${cwms.advesointerface.sharedservices.zoneid}")
  	private String europeParisZoneIdValue;

	@Override
	public RestResultDTO<AdvesoJobItemDetailsDTO> getJobItemDetails(String jobitemnumber, Locale locale) {

		AdvesoJobItemDetailsDTO dto = new AdvesoJobItemDetailsDTO();
		ZoneId europeParisZoneId = ZoneId.of(europeParisZoneIdValue);

		JobItem jobitem = jobItemService.findJobItem(jobitemnumber);

		// model
		dto.setModel(getBestTranslation(jobitem.getInst().getModel().getNameTranslations()).orElse(""));

		// plantNo
		dto.setPlantNo(jobitem.getInst().getPlantno());
		// plantId
		dto.setPlantId(jobitem.getInst().getPlantid());
		// serialNo
		dto.setSerialNo(jobitem.getInst().getSerialno());
		// subdiv id
		dto.setSubdivId(jobitem.getJob().getCon().getSub().getSubdivid());
		// calibration frequency in days
		int calFrequencyDays = jobitem.getInst().getCalFrequencyUnit().toDays(jobitem.getInst().getCalFrequency());
		dto.setCalFrequency(calFrequencyDays);
		// subdiv name
		dto.setSubdivName(jobitem.getJob().getCon().getSub().getSubname());
		// procedure reference (from workrequirement list)
		dto.setProcedureReference(jobitem.getWorkRequirements().stream().map(JobItemWorkRequirement::getWorkRequirement)
            .filter(wr -> wr.getCapability() != null).map(wr -> wr.getCapability().getReference())
			.collect(Collectors.toList()));

		JobItemAction jobitemAction = jobItemActionService.findLastActiveActionForItem(jobitem);
		// last activity name
		// get translation
		if (jobitemAction != null) {
			getBestTranslation(jobitemAction.getActivity().getTranslations())
				.ifPresent(dto::setLastActivityName);
			// last activity id
			dto.setLastActivityId(jobitemAction.getId());
		}

		// TBD technician full name
		EngineerAllocation ea = engineerAllocationService.findActiveEngineerAllocationForItem(jobitem.getJobItemId());
		if (ea != null)
			dto.setTechnicianFullName(ea.getAllocatedTo().getFirstName() + " " + ea.getAllocatedTo().getLastName());
		// manufacturer
		dto.setManufacturer(jobitem.getInst().getDefinitiveMfrName());
		// collect date
		dto.setCollectDate(jobitem.getDateIn());
		// planned delivery date
		if (jobitem.getJob().getAgreedDelDate() != null) {
            if (jobitem.getAgreedDelDate() != null)
                dto.setPlannedDeliveryDate(jobitem.getAgreedDelDate());
            else
                dto.setPlannedDeliveryDate(jobitem.getJob().getAgreedDelDate());
        } else
            dto.setPlannedDeliveryDate(jobitem.getDueDate());

        // real delivery date
        if (jobitem.getClientReceiptDate() != null)
            dto.setRealDeliveryDate(jobitem.getClientReceiptDate().withZoneSameInstant(europeParisZoneId).toLocalDateTime());
        else if (jobitem.getDateComplete() != null)
            dto.setRealDeliveryDate(DateTools.dateToLocalDateTime(jobitem.getDateComplete(), europeParisZoneId));
        // last calibration date
        Calibration lastCalibration = calibrationService.findLastCalibrationForJobitem(jobitem.getJobItemId());
        if (lastCalibration != null) {
            dto.setLastCalibrationDate(lastCalibration.getCalDate());
            // TBD : service type : from last completed calibration

        }

        dto.setServiceType(getBestTranslation(jobitem.getServiceType().getShortnameTranslation()).orElse(null));
        dto.setServiceTypeDescription(getBestTranslation(jobitem.getServiceType().getLongnameTranslation()).orElse(null));
        // TBD last calibration status
        Optional<CalLink> clOpt = Optional.ofNullable(jobitem.getCalLinks())
            .map(Collection::stream).flatMap(s -> s.reduce((a, e) -> e));

        clOpt.ifPresent(cl -> {
            if (cl.getOutcome() != null)
                dto.setLastCalibrationStatus(
                    cl.getOutcome().getGenericValue().getTranslation());

            // set last calibration process
            dto.setLastCalibrationProcess(
                cl.getCal().getCalProcess().getProcess());
        });

        // jobitem notes
        dto.setJobItemNotes(jobitem.getNotes().stream().map(Note::getNote).collect(Collectors.toList()));
        // jobitem no
        dto.setJobItemNumber(jobitem.getItemCode());
		// receipt date
        dto.setReceiptDate(jobitem.getDateIn().withZoneSameInstant(europeParisZoneId));
		// activity list

		// get displayed
		List<AdvesoActivityDTO> defaultActs = advesoTransferableActivityService.getDefaultActivities();
		List<AdvesoActivityDTO> subdivLevelNotDispActs = advesoOverriddenActivitySettingService
			.getNotDisplayedSubdivLevelActivitiesBySubdiv(dto.getSubdivId());
		List<AdvesoActivityDTO> subdivLevelDispActs = advesoOverriddenActivitySettingService
			.getDisplayedSubdivLevelActivitiesBySubdiv(dto.getSubdivId());

		List<AdvesoActivityDTO> rs = new ArrayList<>(defaultActs);
		rs.removeAll(subdivLevelNotDispActs);
		rs.removeAll(subdivLevelDispActs);
		rs.addAll(subdivLevelDispActs);
		final List<Integer> ids = rs.stream().filter(AdvesoActivityDTO::isDisplayed).map(AdvesoActivityDTO::getId)
			.collect(Collectors.toList());
		//

		// TODO: get the right translation :

		// TransaltionService.getCorrectTranslation
		AtomicInteger counter = new AtomicInteger(0);
		List<AdvesoActivityWrapper> myList = jobitem.getActions().stream().filter(a -> !a.isDeleted()).map(a -> {
			Locale messageLocale = jobitem.getJob().getOrganisation().getComp().getDocumentLanguage();
			String costingFileMessage = this.messageSource.getMessage("costing.file", null, "Costing File",
				messageLocale) + " : ";
			if (activityIsDisplayed(a, ids, costingFileMessage)) {
				AdvesoActivityWrapper aaw = new AdvesoActivityWrapper();
				aaw.setOrderNumber(counter.incrementAndGet());
				if (a.getActivity() != null && a.getActivity().getDescription().equals("Unit received at goods in"))
					aaw.setActivityStartDate(DateTools.dateFromZonedDateTime(jobitem.getJob().getReceiptDate()));
				else
					aaw.setActivityStartDate(a.getStartStamp());
				// aaw.setActivityName(a.getActivity().getDescription());
				// get translation
				ActionOutcome ao = null;
				if (a instanceof JobItemActivity)
					ao = ((JobItemActivity) a).getOutcome();
				String aoTrans = ao != null ? " (" + ao.getGenericValue().getTranslation() + ")" : "";
				getBestTranslation(a.getActivity().getTranslations())
					.ifPresent(aaw::setActivityName);
				aaw.setActivityName(aaw.getActivityName().concat(aoTrans));
				// set remark
				AdvesoJobItemActivity ajia = this.advesoJobItemActivityService
					.getCertificateAdvesoJobItemActivityByJIAction(a);
				if (ajia != null) {
					Certificate cert = this.certificateService.findCertificate(ajia.getLinkedEntityId());
					if (cert != null && cert.getSupplementaryFor() != null
						&& !cert.getStatus().equals(CertStatusEnum.DELETED)) {
						if (StringUtils.isNotBlank(cert.getSupplementaryFor().getThirdCertNo()))
							aaw.setActivityRemark(this.messageSource.getMessage(
									"certificatejobitemaction.cancelreplace",
									new Object[] { cert.getSupplementaryFor().getThirdCertNo() },
									"Cancel and replace the document " + cert.getSupplementaryFor().getThirdCertNo(),
									locale));
						else
							aaw.setActivityRemark(
								this.messageSource.getMessage("certificatejobitemaction.cancelreplace",
									new Object[]{cert.getSupplementaryFor().getCertno()},
									"Cancel and replace the document " + cert.getSupplementaryFor().getCertno(),
									locale));
					}
				}
				return aaw;
			} else
				return null;

		}).filter(Objects::nonNull).collect(Collectors.toList());

		dto.setActivityList(
			myList.stream().sorted(Comparator.comparing(AdvesoActivityWrapper::getActivityStartDate))
				.collect(Collectors.toList()));

		RestPagedResultDTO<AdvesoJobItemDetailsDTO> result = new RestPagedResultDTO<>(true, "",
			1000, 1);
		result.setResultsCount(1);
		result.addResult(dto);
		return result;
	}

	private boolean activityIsDisplayed(JobItemAction action, List<Integer> displayedIds,
										String costingFileMessage) {
		boolean isJobCostingActivity = jobItemService.stateHasGroupOfKeyName(action.getActivity(),
			StateGroup.COSTING_ISSUED);
		boolean isSingleJobItemActivity = StringUtils.isNotBlank(action.getRemark())
			&& action.getRemark().contains(costingFileMessage);
		return displayedIds.contains(action.getActivity().getStateid()) && (!isJobCostingActivity || isSingleJobItemActivity);
	}

}
