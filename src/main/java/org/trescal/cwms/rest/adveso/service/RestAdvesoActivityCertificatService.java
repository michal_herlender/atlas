package org.trescal.cwms.rest.adveso.service;


import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityCertificateDTO;

public interface RestAdvesoActivityCertificatService {
	AdvesoActivityCertificateDTO getAdvesoActivityCertificate(Certificate certificate, Integer jobitemid);
}
