package org.trescal.cwms.rest.adveso.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityCertificateDTO;
import org.trescal.cwms.rest.adveso.service.RestAdvesoActivityCertificatService;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import javax.validation.constraints.NotNull;
import java.util.Locale;

@Controller
@JsonController
@RequestMapping("/adveso")
public class RestAdvesoActivityCertificateController {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestAdvesoActivityCertificatService restAdvesoActivityCertificatService;
	@Autowired
	private CertificateService certificatService;
	@Autowired
	private CalibrationService calibrationService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = {"/getCertificate"}, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8}, consumes = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<AdvesoActivityCertificateDTO> getActivityCertificate(
			@RequestParam(name = "certificateid") @NotNull Integer certificateid,
			@RequestParam(name = "jobitemid") @NotNull Integer jobitemid, Locale locale) {

		// validate advesojobitemactivity existence
		Certificate certificate = certificatService.findCertificate(certificateid);
		if (certificate != null && !certificate.getStatus().equals(CertStatusEnum.DELETED)) {
			AdvesoActivityCertificateDTO dto = restAdvesoActivityCertificatService
					.getAdvesoActivityCertificate(certificate, jobitemid);
			RestResultDTO<AdvesoActivityCertificateDTO> result = new RestResultDTO<>(true,
					null);
			result.addResult(dto);
			return result;
		} else {
			String message = messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND,
					new Object[] { certificateid }, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = {"/getCalibrationCertificates"}, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8}, consumes = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<AdvesoActivityCertificateDTO> getCalibrationCertificates(
			@RequestParam(name = "calibrationdid") @NotNull Integer calibrationdid,
			@RequestParam(name = "jobitemid") @NotNull Integer jobitemid, Locale locale) {

		Calibration cal = calibrationService.findCalibration(calibrationdid);

		if (cal == null) {
			String message = messageSource.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND, new Object[]{},
					RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND, locale);
			return new RestResultDTO<>(false, message);
		}

		RestResultDTO<AdvesoActivityCertificateDTO> result = new RestResultDTO<>(true, null);
		cal.getCerts().stream().filter(cert -> !cert.getStatus().equals(CertStatusEnum.DELETED)).forEach(cert -> {
			AdvesoActivityCertificateDTO dto = restAdvesoActivityCertificatService.getAdvesoActivityCertificate(cert,
					jobitemid);
			result.addResult(dto);
		});
		return result;

	}

}
