package org.trescal.cwms.rest.adveso.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.trescal.cwms.core.external.adveso.dto.AdvesoEventWrapperDTO;
import org.trescal.cwms.core.external.adveso.dto.AdvesoEventsDTO;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.enums.AdvesoEventTypeEnum;
import org.trescal.cwms.rest.adveso.dto.transformer.AdvesoActivityEventTransformer;
import org.trescal.cwms.rest.adveso.dto.transformer.AdvesoNotificationSystemTransformer;

import java.net.SocketTimeoutException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RestClientAdvesoQueueServiceImpl implements RestClientAdvesoQueueService {

    private static final String SERVER_ERROR = "the server responded with a http status of ";
    private static final String SERVER_TIMEDOUT = "Server Connection Timeout";

    @Value("#{props['cwms.advesointerface.sharedservices.server']}")
    private String server;

    //for testing purpose on adveso, will keep the comments below.
    //@Value("#{props['cwms.advesointerface.sharedservices.serverstg']}")
    //private String server2 =  "https://stg-shared.adveso.trescal.com";

    @Value("#{props['cwms.advesointerface.sharedservices.registerevent']}")
    private String webServiceEventsPath;

    @Autowired
    private AdvesoJobItemActivityService jobItemActivityService;
    @Autowired
    private NotificationSystemModelService notificationService;
    @Autowired
    private NotificationSystemModelService notificationSystemModelService;
    @Autowired
	private AdvesoNotificationSystemTransformer notificationEventransformer;
	@Autowired
	private AdvesoActivityEventTransformer activityEventTransformer;

	@Override
	synchronized public void sendEvents(List<AdvesoEventWrapperDTO> events) {

		AdvesoEventsDTO dto = new AdvesoEventsDTO(events);

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<AdvesoEventsDTO> request = new HttpEntity<>(dto, requestHeaders);

		boolean success = false;
		String errorMessage = "";
		try {

			ResponseEntity<String> response = restTemplate.postForEntity(server + webServiceEventsPath, request,
					String.class);

			// http code 200: Ok
			if (response.getStatusCodeValue() == 200)
				success = true;
			else
                errorMessage = SERVER_ERROR + response.getStatusCodeValue() + ", "
                    + StringEscapeUtils.escapeHtml4(response.getBody());

		} catch (RestClientException e) {
			if (e.getRootCause() instanceof SocketTimeoutException
					|| e.getRootCause() instanceof ConnectTimeoutException) {
				// timedout : probably the server will be down
				errorMessage = SERVER_TIMEDOUT;
			} else {
				// Some other exception
				errorMessage = e.getLocalizedMessage();
			}
		} finally {

            Date sentOn = new Date();

            log.debug("Adveso Notifs and Activities sent on: " + sentOn + ", list: " + events);

            List<Integer> advesoActivityIds = events.stream()
                .filter(e -> e.getType().equals(AdvesoEventTypeEnum.ACTIVITY))
                .map(e -> e.getActivity().getQueueId()).collect(Collectors.toList());

            jobItemActivityService.updateAfterSending(success, sentOn, errorMessage, advesoActivityIds);

            List<Integer> advesoNotificationIds = events.stream()
                .filter(e -> e.getType().equals(AdvesoEventTypeEnum.NOTIFICATION))
                .map(e -> e.getNotification().getId()).collect(Collectors.toList());
            notificationService.updateAfterSending(success, sentOn, errorMessage, advesoNotificationIds);
        }

	}

	@Override
	public void sendActivities(List<Integer> activitiyIds) {

        // get never sent activities
        List<AdvesoJobItemActivity> neverSentActivities = jobItemActivityService.getByIds(activitiyIds);
        List<AdvesoEventWrapperDTO> events = activityEventTransformer.convertList(neverSentActivities).stream()
            .map(AdvesoEventWrapperDTO::new).collect(Collectors.toList());

        // order events
        Comparator<AdvesoEventWrapperDTO> comparator = Comparator.nullsFirst(Comparator.comparing(this::getCreatedOnDate));

        // sort
        events.sort(comparator);

        // set ordre no
        MutableInt cnt = new MutableInt(0);
        events.forEach(e -> {
            cnt.increment();
            e.setOrderNo(cnt.intValue());
        });

        // send events
        this.sendEvents(events);

    }

	@Override
	public void sendNotifications(List<Integer> notificationIds) {

        // get never sent notifications
        List<NotificationSystemModel> neverSentNotifications = notificationSystemModelService.getByIds(notificationIds);

        List<AdvesoEventWrapperDTO> events = notificationEventransformer.convertList(neverSentNotifications).stream()
            .map(AdvesoEventWrapperDTO::new).collect(Collectors.toList());

        // order events
        Comparator<AdvesoEventWrapperDTO> comparator = Comparator.nullsFirst(Comparator.comparing(this::getCreatedOnDate));

        // sort
        events.sort(comparator);

        // set ordre no
        MutableInt cnt = new MutableInt(0);
        events.forEach(e -> {
            cnt.increment();
            e.setOrderNo(cnt.intValue());
        });

        // send events
        this.sendEvents(events);
    }

	@Override
	public Date getCreatedOnDate(AdvesoEventWrapperDTO e) {
		if (e.getType().equals(AdvesoEventTypeEnum.ACTIVITY))
			return e.getActivity().getCreatedOnNonFormatted();
		else if (e.getType().equals(AdvesoEventTypeEnum.NOTIFICATION))
			return e.getNotification().getCreatedOnNonFormatted();
		return null;
	}

}
