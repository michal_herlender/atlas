package org.trescal.cwms.rest.adveso.service;

import java.util.Locale;

import org.trescal.cwms.rest.adveso.dto.AdvesoJobItemDetailsDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

public interface RestAdvesoJobitemService {

	RestResultDTO<AdvesoJobItemDetailsDTO> getJobItemDetails(String jobitemnumber, Locale locale);

}
