package org.trescal.cwms.rest.adveso.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.external.adveso.dto.AdvesoNotificationEventDTO;

public class AdvesoNotificationSystemWrapper implements Serializable {

	private static final long serialVersionUID = 1L;

	// Token used by Notification System to identify service providers
	public final static String AK = "216FA9C8-9267-476C-A93D-E5BBA9FA75C0";

	private List<AdvesoNotificationEventDTO> dtos;
	private String accessKey;

	public List<AdvesoNotificationEventDTO> getDtos() {
		return dtos;
	}

	public void setDtos(List<AdvesoNotificationEventDTO> dto) {
		this.dtos = dtos;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public AdvesoNotificationSystemWrapper() {
		super();
		this.accessKey = AK;
		this.dtos = new ArrayList<AdvesoNotificationEventDTO>();
	}

	public AdvesoNotificationSystemWrapper(List<AdvesoNotificationEventDTO> dto, String accessKey) {
		super();
		this.dtos = dto;
		this.accessKey = AK;
	}

}
