package org.trescal.cwms.rest.adveso.service;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.tools.filebrowser.FileWrapper;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityCertificateDTO;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Service
public class RestAdvesoActivityCertificatServiceImpl implements RestAdvesoActivityCertificatService {

	@Autowired
	private TranslationService translationService;
	@Autowired
	private FileBrowserService fileBrowserService;
	@Autowired
	private JobItemService jobItemService;

	private FileWrapper convertCertificateDocumentToDocumentDTO(List<FileWrapper> files, String certificateNumber) {
		if (files != null) {
			Optional<FileWrapper> file = files.stream().filter(f -> !f.isDirectory()
				&& f.getFileName().contains(certificateNumber) && f.getFileName().endsWith(".pdf"))
				.max((o1, o2) -> {
					// TODO Auto-generated method stub
					Date d1 = o1.getLastModified();
					Date d2 = o2.getLastModified();
					return d1.compareTo(d2);
				});
			return file.orElse(null);
		}
		return null;
	}

	@Override
	public AdvesoActivityCertificateDTO getAdvesoActivityCertificate(Certificate certificate, Integer jobitemid) {
		AdvesoActivityCertificateDTO dto = new AdvesoActivityCertificateDTO();

		JobItem jobitem = jobItemService.findJobItem(jobitemid);
		if (jobitem != null) {
			// set jobItemNumber
			dto.setJobItemNumber(jobitem.getItemCode());
			// itemNo
			dto.setItemNo(String.valueOf(jobitem.getItemNo()));

			// data from instrument
			if (jobitem.getInst() != null) {
				// plantNo
				dto.setPlantNo(jobitem.getInst().getPlantno());
				// plantId
				dto.setPlantId(jobitem.getInst().getPlantid());
				// former bar code
				dto.setFormerId(jobitem.getInst().getFormerBarCode());
			}

			// set subdivision name, id and emails contact
			if (jobitem.getJob().getCon().getSub() != null) {
				// subdiv name
				dto.setSubdivName(jobitem.getJob().getCon().getSub().getSubname());
				dto.setSubdivId(jobitem.getJob().getCon().getSub().getSubdivid());
				// contact email
				List<String> emails = new ArrayList<>();
				if (jobitem.getJobCostingItems() != null) {
					// JobCosting case
					for (JobCostingItem jci : jobitem.getJobCostingItems()) {
						String email = jci.getJobCosting().getLastUpdateBy().getEmail();
						if (!emails.contains(email))
							emails.add(email);
					}
				} else if (jobitem.getJob().getCon().getSub().getSubdivSettings() != null) {
					SubdivSettingsForAllocatedSubdiv ssas = jobitem.getJob().getCon().getSub().getSubdivSettings().stream().
						filter(e -> e.getOrganisation().getSubdivid() == jobitem.getJob().getOrganisation().getSubdivid())
							.findFirst().orElse(null);
					if(ssas != null && ssas.getDefaultBusinessContact() != null)
						emails.add(ssas.getDefaultBusinessContact().getEmail());
					else if(jobitem.getJob().getOrganisation().getBusinessSettings() != null && jobitem.getJob().getOrganisation().getBusinessSettings().getDefaultBusinessContact() != null)
						emails.add(jobitem.getJob().getOrganisation().getBusinessSettings().getDefaultBusinessContact().getEmail());
				} else if (jobitem.getJob().getOrganisation().getBusinessSettings() != null && jobitem.getJob().getOrganisation().getBusinessSettings().getDefaultBusinessContact() != null) {
					emails.add(jobitem.getJob().getOrganisation().getBusinessSettings().getDefaultBusinessContact().getEmail());
				} else {
					emails.add(jobitem.getJob().getCon().getSub().getComp().getDefaultBusinessContact().getEmail());
				}
				dto.setContactEmails(emails);
			}
		}

		if (certificate.getRegisteredBy() != null) {
			dto.setTechnicienHrId(certificate.getRegisteredBy().getHrid());
			dto.setTechnicienName(certificate.getRegisteredBy().getName());
		}

		FileBrowserWrapper rw = fileBrowserService.getFilesForComponentRoot(Component.CERTIFICATE,
			certificate.getCertid(), null);
		FileWrapper file = convertCertificateDocumentToDocumentDTO(rw.getFiles(), certificate.getCertno()
		);

		if (certificate.getCalibrationVerificationStatus() != null)
			dto.setCalibrationVerificationStatus(certificate.getCalibrationVerificationStatus().name());

		if (certificate.getCal() != null && certificate.getCal().getStatus() != null) {
			dto.setCalibrationStatus(certificate.getCal().getStatus().getName());
			dto.setCalibrationStatusId(certificate.getCal().getStatus().getStatusid());
		}

		dto.setInterventionDate(certificate.getCalDate());
		dto.setDocumentDate(new Date()); // Date d'émission du document
		// Stockage de l'information
		if (certificate.getServiceType() != null) {
			Locale localeEnglish = new Locale("en", "EN");
			// service type
			dto.setServiceType(this.translationService.getCorrectTranslation(
				certificate.getServiceType().getShortnameTranslation(), localeEnglish));
			// service type id
			dto.setServiceTypeId(certificate.getServiceType().getServiceTypeId());
		}

		dto.setExternalDocumentNumber(certificate.getThirdCertNo()); // N°
		// externe
																		// du
																		// document
																		// Stockage
																		// du
																		// lien
																		// vers
																		// le
																		// document
																		// ERP
		dto.setDocumentNumber(certificate.getCertno()); // N° du
														// document
														// ERP Stockage
														// du
														// lien vers le
														// document ERP
		if (file != null && !file.isDirectory()) {
			dto.setDocumentId(file.getFilePathEncrypted()); // ID du
															// document
															// Stockage
															// du
															// lien vers
															// le
															// document
															// ERP
			File fileData = new File(file.getFilePath());
			try {
				dto.setDocumentBinary(FileUtils.readFileToByteArray(fileData));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // Binaires
				// de
				// documents
				// Stockage
				// de
				// l'information
			dto.setDocumentTitle(file.getFileName()); // Nom du fichier
														// Stockage de
														// l'information
		}
		dto.setCertificateid(certificate.getCertid());
		dto.setCertificateStatus(certificate.getStatus().name());
		if (certificate.getSupplementaryFor() != null) {
			dto.setReplacementfor(certificate.getSupplementaryFor().getCertid());
			dto.setReplacementComments(certificate.getSupplementaryCertificateComments());
		}
		dto.setAdjustment(certificate.getAdjustment());
		dto.setOptimization(certificate.getOptimization());
		dto.setRepair(certificate.getRepair());
		dto.setRestriction(certificate.getRestriction());

		return dto;
	}

}
