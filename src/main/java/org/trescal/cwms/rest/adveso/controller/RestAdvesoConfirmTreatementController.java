package org.trescal.cwms.rest.adveso.controller;

import java.util.Locale;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.adveso.service.RestAdvesoQueueServiceImpl;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@JsonController
@RequestMapping("/adveso")
public class RestAdvesoConfirmTreatementController {
	
	@Autowired
	private AdvesoJobItemActivityService advesoJobItemActivityService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestAdvesoQueueServiceImpl restAdvesoQueueService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/confirmTreatement/{queueId}" }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<String> updateTreatementState(@PathVariable(required = true) @NotNull Integer queueId,
			@RequestParam(required = true) @NotNull Boolean isSuccess,
			@RequestParam(required = false) String errorMessage, Locale locale) {
		
		//validate queueId exists
		AdvesoJobItemActivity jia = advesoJobItemActivityService.get(queueId);
		if(jia==null){
			String message = messageSource.getMessage(RestErrors.CODE_ACTIVITY_NOT_FOUND, new Object[] { queueId },
					RestErrors.MESSAGE_ACTIVITY_NOT_FOUND, locale);
			return new RestResultDTO<String>(false, message);
		}
		
		//validate errorMessage is not blank when isSucess is false
		if(!isSuccess && StringUtils.isBlank(errorMessage)){
			String message = messageSource.getMessage(RestErrors.CODE_DATA_NOT_FOUND, new Object[] { "Error Message" },
					RestErrors.MESSAGE_DATA_NOT_FOUND, locale);
			return new RestResultDTO<String>(false, message);
		}

		return restAdvesoQueueService.updateTreatementState(jia, isSuccess, errorMessage);
	}

}
