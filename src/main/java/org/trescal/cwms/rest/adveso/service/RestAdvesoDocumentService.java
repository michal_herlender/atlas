package org.trescal.cwms.rest.adveso.service;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.rest.adveso.dto.AdvesoDocumentDTO;
import org.trescal.cwms.rest.adveso.dto.AdvesoDownloadDocumentDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

public interface RestAdvesoDocumentService {

	RestResultDTO<List<AdvesoDocumentDTO>> getDocumentList(Integer jobitemid, String documenttype, Integer subdivId);

	RestResultDTO<AdvesoDownloadDocumentDTO> downloadDocument(String documentid, Locale locale);

}
