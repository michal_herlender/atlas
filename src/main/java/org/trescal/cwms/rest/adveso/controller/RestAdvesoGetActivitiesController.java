package org.trescal.cwms.rest.adveso.controller;

import java.util.List;
import java.util.Locale;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@JsonController
@RequestMapping("/adveso")
public class RestAdvesoGetActivitiesController {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private AdvesoJobItemActivityService advesoJobItemActivityService;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/activities" }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<List<AdvesoActivityDTO>> getActivities(
			@RequestParam(name="subdivid", required = true) Integer subdivid, Locale locale) {

		// validation
		Subdiv subdivision = subdivService.get(subdivid);
		if (subdivision == null) {
			String message = messageSource.getMessage(RestErrors.CODE_SUBDIV_NOT_FOUND, new Object[] { subdivid },
					RestErrors.MESSAGE_SUBDIV_NOT_FOUND, locale);
			return new RestResultDTO<List<AdvesoActivityDTO>>(false, message);
		}

		List<AdvesoActivityDTO> result = advesoJobItemActivityService.getSubdivActivities(subdivid);
		RestResultDTO<List<AdvesoActivityDTO>> ob = new RestResultDTO<>(true, "");
		ob.addResult(result);
		return ob;
	}

}
