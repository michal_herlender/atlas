package org.trescal.cwms.rest.adveso.dto;

import java.util.Date;

import org.trescal.cwms.rest.adveso.utils.AdvesoDocumentTypeEnum;

public class AdvesoDocumentDTO {

	private String documentTitle;
	private String documentId;
	private String documentExtension;
	private AdvesoDocumentTypeEnum documentType;
	private Date documentLastModified;
	private String documentExternalReference;
	private String documentName;

	public String getDocumentTitle() {
		return documentTitle;
	}

	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentExtension() {
		return documentExtension;
	}

	public void setDocumentExtension(String documentExtension) {
		this.documentExtension = documentExtension;
	}

	public AdvesoDocumentTypeEnum getDocumentType() {
		return documentType;
	}

	public void setDocumentType(AdvesoDocumentTypeEnum documentType) {
		this.documentType = documentType;
	}

	public Date getDocumentLastModified() {
		return documentLastModified;
	}

	public void setDocumentLastModified(Date documentLastModified) {
		this.documentLastModified = documentLastModified;
	}

	public String getDocumentExternalReference() {
		return documentExternalReference;
	}

	public void setDocumentExternalReference(String documentExternalReference) {
		this.documentExternalReference = documentExternalReference;
	}
	
	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((documentId == null) ? 0 : documentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdvesoDocumentDTO other = (AdvesoDocumentDTO) obj;
		if (documentId == null) {
			if (other.documentId != null)
				return false;
		} else if (!documentId.equals(other.documentId))
			return false;
		return true;
	}

}
