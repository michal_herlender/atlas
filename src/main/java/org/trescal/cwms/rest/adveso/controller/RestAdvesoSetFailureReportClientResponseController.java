package org.trescal.cwms.rest.adveso.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.adveso.dto.AdvesoFailureReportClientResponseInputDTO;
import org.trescal.cwms.rest.adveso.validator.AdvesoFailureReportClientResponseValidator;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@JsonController
@RequestMapping("/adveso")
public class RestAdvesoSetFailureReportClientResponseController {

	@Autowired
	private FaultReportService faultReportServ;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private AdvesoFailureReportClientResponseValidator validator;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/frclientresponse", method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<String> setClientResponse(@RequestBody AdvesoFailureReportClientResponseInputDTO inputDTO,
			BindingResult bindingResult, Locale locale) {

		// validation
		validator.validate(inputDTO, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}

		FaultReport fr = this.faultReportServ.findFaultReport(inputDTO.getFailureReportId());
		if (fr != null && fr.getClientResponseNeeded() && !fr.getOutcomePreselected()) {
			this.faultReportServ.setFailureReportClientResponseFromAdveso(fr, inputDTO);

			return new RestResultDTO<>(true, "");
		} else
			return new RestResultDTO<>(false, "");

	}

}
