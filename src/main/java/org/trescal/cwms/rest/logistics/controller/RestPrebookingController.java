package org.trescal.cwms.rest.logistics.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.logistics.dto.RestPrebookingDTO;
import org.trescal.cwms.rest.logistics.service.RestPrebookingService;
import org.trescal.cwms.rest.logistics.validator.RestPrebookingDTOValidator;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Controller
@RestJsonController
public class RestPrebookingController {
	
	public static final String REQUEST_MAPPING = "/api/logistics/addprebooking";

	@Autowired
	RestPrebookingService restPrebookingService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private RestPrebookingDTOValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private SubdivService subService;
	@Autowired
	private ContactService conService;

	@RequestMapping(value = {REQUEST_MAPPING}, method = RequestMethod.POST, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, consumes = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public @ResponseBody RestResultDTO<AsnItemFinalSynthesisDTO> createPrebooking(@RequestBody RestPrebookingDTO dto,
			BindingResult bindingResult, Locale locale) throws Exception {

		// validation
		validator.validate(dto, bindingResult);
		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, locale);
		}

		String items = new ObjectMapper().writeValueAsString(dto.getItems());

		// string to date of pickupDate value
		DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss.SSSX", Locale.US);
		Date pickupDate = null;
		if (dto.getPickupDate() != null)
			pickupDate = inputFormat.parse(dto.getPickupDate());

		Subdiv subdivClient = subService.get(dto.getSubdivid());

		Company companyClient = subdivClient.getComp();
		Contact contactClient = null;

		// get the contact using the id if it is provided, if not search the
		// contact using lastName, firstName and subdivid
		// if no contact found, get the default contact of the subdivision
		if (!ObjectUtils.isEmpty(dto.getContactId())) {
			contactClient = conService.get(dto.getContactId());
		}
		if (contactClient == null) {
			contactClient = conService.getContactByFirstnameAndLastname(dto.getSubdivid(), dto.getFirstName(),
					dto.getLastName());
		}
		if (contactClient == null) {
			contactClient = subdivClient.getDefaultContact();
		}
		Contact contactLogistician = contactService.get(dto.getLogisticianContactId());
		Subdiv subdivBusiness = contactLogistician.getSub();

		// convert data to asnItemDto
		List<AsnItemFinalSynthesisDTO> aiFinalSynthDtoListe = restPrebookingService
				.convertDataToAsnItemDTO(dto.getItems());

		// analyze data
		List<AsnItemFinalSynthesisDTO> asnItemsDTOListe = restPrebookingService.analyseData(aiFinalSynthDtoListe,
				companyClient.getCoid(), subdivClient.getSubdivid(), locale);

		if (dto.getEstCarriageOut() == null) {
			dto.setEstCarriageOut(new BigDecimal("0.00"));
		}
		if (dto.getBookedInAddrId() == null) {
			dto.setBookedInAddrId(subdivBusiness.getDefaultAddress().getAddrid());
		}
		// create prebooking
		PrebookingJobForm pjf = new PrebookingJobForm(subdivBusiness.getDefaultAddress().getAddrid(),
				dto.getBookedInAddrId(), dto.getBookedInLocId(), dto.getBpo(), dto.getClientRef(),
				companyClient.getCurrency().getCurrencyCode(), dto.getEstCarriageOut(), JobType.STANDARD,
				contactClient.getPersonid(), dto.getPoCommentList(), dto.getPoNumberList(), subdivClient.getSubdivid(),
				dto.getDefaultPO(), pickupDate);
		Asn asn = restPrebookingService.savePrebooking(pjf, asnItemsDTOListe, contactClient, contactLogistician,
				items);
		RestResultDTO<AsnItemFinalSynthesisDTO> result = new RestResultDTO<>(true, String.valueOf(asn.getId()));
		result.getResults().addAll(asnItemsDTOListe);

		return (result);
	}
}