package org.trescal.cwms.rest.logistics.service;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnDao;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.logistics.utils.AsnItemStatusEnum;
import org.trescal.cwms.core.logistics.utils.SourceAPIEnum;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RestPrebookingServiceImpl implements RestPrebookingService {

	public static final Logger logger = LoggerFactory.getLogger(RestPrebookingServiceImpl.class);
	private static final Integer BER_ID = 1;
	@Autowired
	private InstrumService instService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyService;
	@Autowired
	private POService poService;
	@Autowired
	private BPOService bpoService;
	@Autowired
	private AsnDao dao;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private ServiceTypeService servicetype;
	@Autowired
	private ExchangeFormatService exchangeFormatService;

	@Override
	public List<AsnItemFinalSynthesisDTO> analyseData(List<AsnItemFinalSynthesisDTO> data, Integer contactId,
			Integer subdivId, Locale locale) {
		// get plantid, plantno and serialno IDs liste for query
		List<Integer> plantids = new ArrayList<>();
		List<String> plantnos = new ArrayList<>();
		List<String> serialnos = new ArrayList<>();
		setItemsFieldId(data, plantids, plantnos, serialnos);

		// get allpossibleinstrument from db
		List<PossibleInstrumentDTO> allpossibleInstrumentDto = instService.lookupPossibleInstruments(contactId,
				subdivId != null ? subdivId : 0, 0, locale, plantids, plantnos, serialnos);

		// init asnItemIndex with first index
		int asnItemIndex = 1;

		List<AsnItemFinalSynthesisDTO> asnItemsliste = new ArrayList<>();

		// iterate fileContent to retrieve identified and non identified Items
		for (AsnItemFinalSynthesisDTO dtoo : data) {

			// if prebooking get asnItems else create a new
			if (dtoo.getAsnItemId() == null || dtoo.getIndex() == null) {
				dtoo.setIndex(asnItemIndex++);
			}

			// to get plantid, plantno and serialno of every row to use it to
			// retrieve the item in allpossibleinstrumentdto list
			Integer plantid = null;
			String plantno = null;
			String serialno = null;
			String ponumber = null;
			if (dtoo.getIdTrescal() != null)
				plantid = dtoo.getIdTrescal();
			if (StringUtils.isNotBlank(dtoo.getPlantNo()))
				plantno = dtoo.getPlantNo();
			if (StringUtils.isNotBlank(dtoo.getSerialNo()))
				serialno = dtoo.getSerialNo();

			// get possibleInstrument
			List<PossibleInstrumentDTO> possibleInstruments = getPossibleInstrumentForFieldsId(allpossibleInstrumentDto,
					plantid, plantno, serialno);

			// filter identified items
			List<AsnItemFinalSynthesisDTO> identifiedJobItemsFinalSynthDTO = asnItemsliste.stream()
					.filter(d -> d.getStatus() != null && d.getStatus().equals(AsnItemStatusEnum.IDENTIFIED))
					.collect(Collectors.toList());

			// check the possibleInstruments status
			checkCurrentItem(possibleInstruments, identifiedJobItemsFinalSynthDTO, dtoo, plantid, plantno, serialno,
					ponumber, contactId);

			asnItemsliste.add(dtoo);
		}

		return asnItemsliste;
	}

	private void checkCurrentItem(List<PossibleInstrumentDTO> possibleInstruments,
			List<AsnItemFinalSynthesisDTO> identifiedAsnItemsFinalSynth, AsnItemFinalSynthesisDTO dto, Integer plantid,
			String plantno, String serialno, String ponumber, Integer clientId) {

		if (CollectionUtils.isEmpty(possibleInstruments)) {
			// TODO to be changed to NOT_IDENTIFIED_IN_COMPANY
			dto.setStatus(AsnItemStatusEnum.NOT_IDENTIFIED);
		} else { // not emtpy
			if (possibleInstruments.size() == 1) {
				validateIdentifiedAsnItemAndUpdateDto(dto, identifiedAsnItemsFinalSynth, possibleInstruments.get(0),
						clientId, ponumber);
			} else { // multiple instrument identified
				// try to filter
				// we might get this case, because we're using an OR while
				// fetching instrument (plantid or plantno or serialno)
				if (plantid != null) {
					final int fplantid = plantid;
					possibleInstruments = possibleInstruments.stream().filter(i -> fplantid == i.getPlantid()).collect(Collectors.toList());

					// since it is the plantid (id), we're sure that the result
					// is unique
					if (possibleInstruments.size() == 1) {
						validateIdentifiedAsnItemAndUpdateDto(dto, identifiedAsnItemsFinalSynth,
							possibleInstruments.get(0), clientId, ponumber);
					}

				} else if (StringUtils.isNoneEmpty(plantno)) {
					final String fplantno = plantno;
					possibleInstruments = possibleInstruments.stream().filter(i -> fplantno.equalsIgnoreCase(i.getPlantno())).collect(Collectors.toList());
					// check again if we succeded getting one instrument
					List<PossibleInstrumentDTO> possibleInstrumentWithoutBER = possibleInstruments.stream()
						.filter(pi -> !pi.getStatus().equals(BER_ID)).collect(Collectors.toList());
					if (possibleInstrumentWithoutBER.size() == 1) {
						validateIdentifiedAsnItemAndUpdateDto(dto, identifiedAsnItemsFinalSynth,
							possibleInstruments.get(0), clientId, ponumber);
					} else {
						// couldn't filter
						dto.setStatus(AsnItemStatusEnum.MULTIPLE_INSTRUMENTS_WITH_SAME_PLANTNO);
						dto.setStatusData(new String[]{plantno});
					}
				} else {
					List<PossibleInstrumentDTO> possibleInstrumentWithoutBER = possibleInstruments.stream()
						.filter(pi -> !pi.getStatus().equals(BER_ID)).collect(Collectors.toList());

					if (possibleInstrumentWithoutBER.size() == 1) {
						validateIdentifiedAsnItemAndUpdateDto(dto, identifiedAsnItemsFinalSynth,
								possibleInstruments.get(0), clientId, ponumber);
					} else {
						// couldn't filter
						dto.setStatus(AsnItemStatusEnum.MULTIPLE_INSTRUMENTS_WITH_SAME_SERIALNO);
						dto.setStatusData(new String[] { serialno });
					}
				}
			}
		}
	}

	private void validateIdentifiedAsnItemAndUpdateDto(AsnItemFinalSynthesisDTO dtoPre,
			List<AsnItemFinalSynthesisDTO> dto, PossibleInstrumentDTO possibleInstruments, int clientCompanyId,
			String ponumber) {

		dtoPre.setPossibleInstrument(possibleInstruments);

		// test if already in identified items
		AsnItemFinalSynthesisDTO existDto = dto.stream()
				.filter(e -> e.getPossibleInstrument().getPlantid().equals(possibleInstruments.getPlantid()))
				.findFirst().orElse(null);
		if (existDto != null) {
			dtoPre.setStatus(AsnItemStatusEnum.DUPLICATED_IN_TABLE);
			int indexOfDuplicatedElement = existDto.getIndex();
			int indexOfDuplicatedElementInEFfile = existDto.getIndex();
			dtoPre.setStatusData(new String[] { indexOfDuplicatedElement + "", indexOfDuplicatedElementInEFfile + "" });

		} else if (possibleInstruments.getAsnid() != null) {
			// instrument identified but currently in a prebooking job
			dtoPre.setStatus(AsnItemStatusEnum.ALREADY_ONPREBOOKING);
			dtoPre.setStatusData(new String[] { possibleInstruments.getAsnid() + "" });
		} else if (possibleInstruments.getJobitemid() != null) {
			// instrument identified but currently in a job
			dtoPre.setStatus(AsnItemStatusEnum.ALREADY_ONJOB);
			dtoPre.setStatusData(
				new String[]{possibleInstruments.getJobitemid() + "", possibleInstruments.getJobitemviewcode()});
		} else if (possibleInstruments.getStatus().equals(InstrumentStatus.BER.ordinal())) {
			// BER
			dtoPre.setStatus(AsnItemStatusEnum.BER);
		} else if (possibleInstruments.getCoid() != clientCompanyId) {
			// instrument identified
			dtoPre.setStatus(AsnItemStatusEnum.IDENTIFIED_ON_OTHER_COMPANY_IN_SAME_GROUP);
		} else {
			dtoPre.setIdTrescal(possibleInstruments.getPlantid());
			dtoPre.setStatus(AsnItemStatusEnum.IDENTIFIED);
			if (ponumber != null && !ponumber.isEmpty())
				dtoPre.setPoNumber(ponumber);
		}
	}

	private void setItemsFieldId(List<AsnItemFinalSynthesisDTO> aiFinalSynthDtoListe, List<Integer> plantids,
			List<String> plantnos, List<String> serialnos) {
		for (AsnItemFinalSynthesisDTO dto : aiFinalSynthDtoListe) {
			if (dto.getIdTrescal() != null)
				plantids.add(dto.getIdTrescal());
			if (StringUtils.isNotBlank(dto.getPlantNo()) && !dto.getPlantNo().equals("/"))
				plantnos.add(dto.getPlantNo());
			if (StringUtils.isNotBlank(dto.getSerialNo()) && !dto.getSerialNo().equals("/"))
				serialnos.add(dto.getSerialNo());
		}
	}

	private List<PossibleInstrumentDTO> getPossibleInstrumentForFieldsId(
			List<PossibleInstrumentDTO> allpossibleInstrumentDto, Integer plantid, String plantno, String serialno) {
		// search the possibleInstrumentDto
		List<PossibleInstrumentDTO> possibleInstruments = new ArrayList<>();
		if (plantid != null) {
			for (PossibleInstrumentDTO i : allpossibleInstrumentDto) {
				if (i.getPlantid().equals(plantid)) {
					possibleInstruments.add(i);
				}
			}
		}
		// if no possibleInstruments found using plantId and plantNo is set,
		// get all possibleInstrumentDto with the same plantNo
		if (CollectionUtils.isEmpty(possibleInstruments) && StringUtils.isNotBlank(plantno) && !plantno.equals("/")) {
			for (PossibleInstrumentDTO i : allpossibleInstrumentDto) {
				if (i.getPlantno().equalsIgnoreCase(plantno)) {
					possibleInstruments.add(i);
				}
			}
		}
		// if no possibleInstruments found using plantId or plantNo or multiple
		// instruments are found by plantno then filter
		if (possibleInstruments.size() != 1 && StringUtils.isNotBlank(serialno) && !serialno.equals("/")) {
			if (possibleInstruments.size() > 1) {
				// in this case we're sure that the plantno search resulted
				// multiple possible instruments,
				// we should try filter the possible instruments with the
				// serial no
				possibleInstruments = possibleInstruments.stream()
						.filter(pi -> pi.getSerialno().equalsIgnoreCase(serialno)).collect(Collectors.toList());
			} else {
				for (PossibleInstrumentDTO i : allpossibleInstrumentDto) {
					if (i.getSerialno().equalsIgnoreCase(serialno)) {
						possibleInstruments.add(i);
					}
				}
			}
		}
		return possibleInstruments;
	}

	@Override
	public List<AsnItemFinalSynthesisDTO> convertDataToAsnItemDTO(List<Map<String, String>> content)
			throws ParseException {
		List<AsnItemFinalSynthesisDTO> list = new ArrayList<>();
		HashMap<String, String> undefineds = new HashMap<>();
		ExchangeFormat ef = exchangeFormatService.getExchangeFormatByLevelAndType(
			ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP, ExchangeFormatLevelEnum.GLOBAL);
		String idTrescal = ef.getExchangeFormatFieldNameDetails().stream().filter(efd -> efd.getFieldName().equals(ExchangeFormatFieldNameEnum.ID_TRESCAL)).findFirst().map(ExchangeFormatFieldNameDetails::getTemplateName).orElse("");

		String customerDescription = ef.getExchangeFormatFieldNameDetails().stream().filter(efd -> efd.getFieldName().equals(ExchangeFormatFieldNameEnum.CUSTOMER_DESCRIPTION)).findFirst().map(ExchangeFormatFieldNameDetails::getTemplateName).orElse("");

		String plantNo = ef.getExchangeFormatFieldNameDetails().stream().filter(efd -> efd.getFieldName().equals(ExchangeFormatFieldNameEnum.PLANT_NO)).findFirst().map(ExchangeFormatFieldNameDetails::getTemplateName).orElse("");

		String serialNo = ef.getExchangeFormatFieldNameDetails().stream().filter(efd -> efd.getFieldName().equals(ExchangeFormatFieldNameEnum.SERIAL_NUMBER)).findFirst().map(ExchangeFormatFieldNameDetails::getTemplateName).orElse("");

		String ClientRef = ef.getExchangeFormatFieldNameDetails().stream().filter(efd -> efd.getFieldName().equals(ExchangeFormatFieldNameEnum.CLIENT_REF)).findFirst().map(ExchangeFormatFieldNameDetails::getTemplateName).orElse("");

		List<ExchangeFormatFieldNameDetails> undefinedList = ef.getExchangeFormatFieldNameDetails().stream()
			.filter(efd -> efd.getFieldName().equals(ExchangeFormatFieldNameEnum.UNDEFINED)).collect(Collectors.toList());

		for (Map<String, String> data : content) {
			AsnItemFinalSynthesisDTO dto = new AsnItemFinalSynthesisDTO();
			for (String key : data.keySet()) {
				if (key.equals(idTrescal)) {
					if (data.get(key) != null)
						dto.setIdTrescal(Integer.parseInt(data.get(key)));
				} else if (key.equals(customerDescription)) {
					dto.setCustomerDescription(data.get(key));
				} else if (key.equals(plantNo)) {
					dto.setPlantNo(data.get(key));
				} else if (key.equals(serialNo)) {
					dto.setSerialNo(data.get(key));
				} else if (key.equals(ClientRef)) {
					dto.setJobitemClientRef(data.get(key));
				} else {
					for (ExchangeFormatFieldNameDetails undef : undefinedList)
						if (key.equals(undef.getTemplateName())) {
							undefineds.put(key, data.get(key));
						}
				}
				dto.setUndefineds(undefineds);
			}
			list.add(dto);
		}
		return (list);
	}

	@Override
	public Asn savePrebooking(PrebookingJobForm dto, List<AsnItemFinalSynthesisDTO> list, Contact contactClient,
			Contact contactLogistician, String items) {
		List<Integer> trescalIds = list.stream().map(AsnItemFinalSynthesisDTO::getIdTrescal)
			.filter(Objects::nonNull).collect(Collectors.toList());
		List<Instrument> allInstruments = null;
		if (CollectionUtils.isNotEmpty(trescalIds))
			allInstruments = instrumentService.getInstruments(trescalIds);

		List<AsnItem> asnItemList = new ArrayList<>();
		List<Instrument> instrumentsListe = allInstruments;
		list.forEach(a -> {
			AsnItem ai = new AsnItem(a);
			if (a.getServicetypeid() != null)
				ai.setServicetype(servicetype.get(a.getServicetypeid()));
			if (a.getIdTrescal() != null)
				ai.setInstrument(instrumentsListe.stream().filter(i -> i.getPlantid() == a.getIdTrescal()).findFirst()
						.orElse(null));
			// add adveso data (traitement)
			asnItemList.add(ai);
		});

		// asn
		Asn asn = new Asn();
		asn.setExchangeFormat(exchangeFormatService.getExchangeFormatByLevelAndType(
				ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP, ExchangeFormatLevelEnum.GLOBAL));
		asn.setItemsFromJson(items);

		asn.setJobType(dto.getJobType());
		asn.setSourceApi(SourceAPIEnum.MOBILE_APP);
		asn.setCreatedBy(contactLogistician);
		asn.setCreatedOn(new Timestamp(new Date().getTime()));

		// PrebookingJobDetails
		if (asn.getId() == null) {
			asn.setPrebookingJobDetails(new PrebookingJobDetails());
			asn.getPrebookingJobDetails().setAsn(asn);
			if (dto.getBookedInAddrId() != null)
				asn.getPrebookingJobDetails().setBookedInAddr(addressService.get(dto.getBookedInAddrId()));
			if (dto.getBookedInLocId() != null)
				asn.getPrebookingJobDetails().setBookedInLoc(locationService.get(dto.getBookedInLocId()));
			asn.getPrebookingJobDetails().setClientRef(dto.getClientref());
			if (dto.getPersonid() != null)
				asn.getPrebookingJobDetails().setCon(contactClient);
			if (dto.getCurrencyCode() != null)
				asn.getPrebookingJobDetails()
					.setCurrency(supportedCurrencyService.findCurrencyByCode(dto.getCurrencyCode()));

			if (dto.getPickupDate() != null)
				asn.getPrebookingJobDetails().setPickupDate(dto.getPickupDate());

			if (dto.getDefaultPO() != null)
				poService.addPOsToPrebookingJob(dto.getPoNumberList(), dto.getPoCommentList(), asn, dto.getDefaultPO(),
					contactClient.getSub().getComp());
			asn.getPrebookingJobDetails().setEstCarriageOut(dto.getEstCarriageOut());
		}

		if (CollectionUtils.isNotEmpty(asn.getAsnItems())) {
			Iterator<AsnItem> asnItemIterator = asnItemList.iterator();
			while (asnItemIterator.hasNext()) {
				AsnItem currentAsnItem = asnItemIterator.next();
				AsnItem asnItemToUpdate = null;
				if (asn.getAsnItems() != null)
					asnItemToUpdate = asn.getAsnItems().stream().filter(i -> i.getId().equals(currentAsnItem.getId()))
							.findFirst().orElse(null);
				if (asnItemToUpdate != null) {
					asnItemToUpdate.setStatus(currentAsnItem.getStatus());
					asnItemToUpdate.setStatusData(currentAsnItem.getStatusData());
					asnItemToUpdate.setServicetype(currentAsnItem.getServicetype());
					asnItemToUpdate.setServiceTypeSource(currentAsnItem.getServiceTypeSource());
					asnItemToUpdate.setInstrument(currentAsnItem.getInstrument());
					asnItemToUpdate.setPoNumber(currentAsnItem.getPoNumber());
					asnItemToUpdate.setClientRef(currentAsnItem.getClientRef());
					asnItemIterator.remove();
				}
			}
			if (CollectionUtils.isNotEmpty(asnItemList))
				asn.getAsnItems().addAll(asnItemList);
		} else {
			asn.setAsnItems(new ArrayList<>());
			asn.getAsnItems().addAll(asnItemList);
		}

		asn.getAsnItems().forEach(a -> a.setAsn(asn));

		// Add BPO
		if (dto.getBpo() != null)
			asn.getPrebookingJobDetails().setBpo(bpoService.get(dto.getBpo()));

		/* add operation */
		if (asn.getId() == null) {
			dao.persist(asn);
		}

		/* add Po */
		List<String> posNumber = asnItemList.stream().map(AsnItem::getPoNumber).distinct()
			.filter(StringUtils::isNoneBlank).collect(Collectors.toList());
		if (posNumber.size() > 0)
			poService.addPOsToPrebookingJob(posNumber, null, asn, 1, contactClient.getSub().getComp());

		/* call update every method invocation to save data */
		dao.merge(asn);

		return asn;

	}

}
