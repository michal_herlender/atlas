package org.trescal.cwms.rest.logistics.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.logistics.dto.RestPrebookingDTO;

import java.util.Map;

@Component
public class RestPrebookingDTOValidator extends AbstractBeanValidator {

	@Autowired
	private ContactService contactService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ExchangeFormatService exchangeFormatService;
	@Autowired
	private ContactService conService;

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return RestPrebookingDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		RestPrebookingDTO dto = (RestPrebookingDTO) target;

		
		//NullMandatoryFields
		
		Subdiv sub =null;
		Contact conClient = null ;
		if(!ObjectUtils.isEmpty(dto.getContactId()))
			conClient= this.conService.get(dto.getContactId());
		// lookup subdivision name
		if(!ObjectUtils.isEmpty(dto.getSubdivid())){
			sub = this.subdivService.get(dto.getSubdivid());
			if (sub == null) {
				errors.rejectValue("subdivid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { dto.getSubdivid() },
						RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			// lookup the default contact of the subdivision if no contact found
			else if (conClient == null) {
				conClient = this.conService.getContactByFirstnameAndLastname(dto.getSubdivid(), dto.getFirstName(),
						dto.getLastName());
				if (conClient == null) {
					if (ObjectUtils.isEmpty(sub.getDefaultContact())) {
						errors.rejectValue("subdivid", RestErrors.CODE_SUBDIV_DEFAULT_CONTACT,
								new Object[] { dto.getSubdivid() }, RestErrors.MESSAGE_SUBDIV_DEFAULT_CONTACT);
					}
				}
			}
		}
		
		// lookup logistician contact
		Contact con=null;
		if(!ObjectUtils.isEmpty(dto.getLogisticianContactId())){
			 con = this.contactService.get(dto.getLogisticianContactId());
			 if (con == null) {
					errors.rejectValue("logisticianContactId", RestErrors.CODE_DATA_NOT_FOUND,
							new Object[] { dto.getLogisticianContactId() }, RestErrors.MESSAGE_DATA_NOT_FOUND);
				}
		}
		

		// check the mandatory data
		if(dto.getItems()!=null){
			int validation = 0;
			boolean idTrescalNull = false;
			ExchangeFormat ef = exchangeFormatService.getExchangeFormatByLevelAndType(
					ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP, ExchangeFormatLevelEnum.GLOBAL);
			String idTrescal = ef.getExchangeFormatFieldNameDetails().stream().filter(efd -> {
				return efd.getFieldName().equals(ExchangeFormatFieldNameEnum.ID_TRESCAL);
			}).findFirst().get().getTemplateName();
			String customerDescription = ef.getExchangeFormatFieldNameDetails().stream().filter(efd -> {
				return efd.getFieldName().equals(ExchangeFormatFieldNameEnum.CUSTOMER_DESCRIPTION);
			}).findFirst().get().getTemplateName();
			for (Map<String, String> data : dto.getItems()) {
				for (String key : data.keySet()) {
					if (key.equals(idTrescal)) {
						if (data.get(key) == null) {
							idTrescalNull = true;
						} else
							validation++;
					}
					if (idTrescalNull && key.equals(customerDescription) && !StringUtils.isEmpty(data.get(key))) {
						validation++;
					}
				}
				if (validation == 0) {
					errors.rejectValue("items", RestErrors.CODE_PREBOOKING_DATA_MISSING, new Object[] { dto.getItems() },
							RestErrors.MESSAGE_PREBOOKING_DATA_MISSING);
				} else {
					validation = 0;
					idTrescalNull = false;
				}
			}
		}
	}
}