package org.trescal.cwms.rest.logistics.service;

import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;

public interface RestPrebookingService {
	public List<AsnItemFinalSynthesisDTO> analyseData(List<AsnItemFinalSynthesisDTO> data, Integer contactId,
			Integer subdivId, Locale locale);

	public List<AsnItemFinalSynthesisDTO> convertDataToAsnItemDTO(List<Map<String, String>> data) throws ParseException;

	public Asn savePrebooking(PrebookingJobForm dto, List<AsnItemFinalSynthesisDTO> list,
			Contact contactClient, Contact contactLogistician, String items);
}