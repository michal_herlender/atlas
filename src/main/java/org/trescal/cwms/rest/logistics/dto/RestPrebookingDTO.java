package org.trescal.cwms.rest.logistics.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class RestPrebookingDTO {

	@NotNull
	private Integer subdivid;
	@NotNull
	private Integer logisticianContactId;
	private String pickupDate;
	private String clientRef;
	private BigDecimal estCarriageOut;
	private Integer bookedInAddrId;
	private Integer bookedInLocId;
	@NotNull
	private List<Map<String, String>> items;

	private Integer bpo;
	private List<String> poCommentList;
	private List<String> poNumberList;
	private Integer defaultPO;

	private Integer contactId;
	private String firstName;
	private String lastName;

	public Integer getSubdivid() {
		return subdivid;
	}

	public Integer getLogisticianContactId() {
		return logisticianContactId;
	}

	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	public void setLogisticianContactId(Integer logisticianContactId) {
		this.logisticianContactId = logisticianContactId;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	public Integer getBpo() {
		return bpo;
	}

	public void setBpo(Integer bpo) {
		this.bpo = bpo;
	}

	public List<String> getPoCommentList() {
		return poCommentList;
	}

	public void setPoCommentList(List<String> poCommentList) {
		this.poCommentList = poCommentList;
	}

	public List<String> getPoNumberList() {
		return poNumberList;
	}

	public void setPoNumberList(List<String> poNumberList) {
		this.poNumberList = poNumberList;
	}

	public Integer getDefaultPO() {
		return defaultPO;
	}

	public void setDefaultPO(Integer defaultPO) {
		this.defaultPO = defaultPO;
	}

	public String getClientRef() {
		return clientRef;
	}

	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	public BigDecimal getEstCarriageOut() {
		return estCarriageOut;
	}

	public void setEstCarriageOut(BigDecimal estCarriageOut) {
		this.estCarriageOut = estCarriageOut;
	}

	public Integer getBookedInAddrId() {
		return bookedInAddrId;
	}

	public void setBookedInAddrId(Integer bookedInAddrId) {
		this.bookedInAddrId = bookedInAddrId;
	}

	public Integer getBookedInLocId() {
		return bookedInLocId;
	}

	public void setBookedInLocId(Integer bookedInLocId) {
		this.bookedInLocId = bookedInLocId;
	}

	public List<Map<String, String>> getItems() {
		return items;
	}

	public void setItems(List<Map<String, String>> items) {
		this.items = items;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
}