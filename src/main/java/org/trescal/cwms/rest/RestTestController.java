package org.trescal.cwms.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;

@RestController
public class RestTestController {

	@Autowired
	private JobItemService jobItemService;
	
	@GetMapping("jobitem.json")
	public String getJobItem(@RequestParam(name="id", required = true) Integer id) {
		return jobItemService.get(id).getItemCode();
	}
}