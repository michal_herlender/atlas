package org.trescal.cwms.rest.plantillas.controller;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrument.dto.InstrumentPlantillasDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasInstrumentDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasInstrumentDTOFactory;

/**
 * REST client search for Plantillas that searches by lastModified to return 
 *   recently changed instruments
 * Functionality is Plantillas - specific for the Zaragoza, Spain calibration tool
 *   and any changes must be co-ordinated with the synchronization interface for this tool
 * @author Galen
 * 2017-03-04
 */
@Controller @JsonController
public class RestPlantillasInstrumentController extends RestPlantillasController {
	public static final String REQUEST_MAPPING = "/plantillas/instrument";
	public static final String REQUEST_PARAM_ADDRID = "addrid";
	public static final String REQUEST_PARAM_PLANTNO = "plantno";
	public static final int RESULTS_PER_PAGE = 1000;
	public static final String REQUEST_PARAM_SUBDIVID = "businessSubdivId";
	
	@Autowired
	private InstrumService instrumentService;
	
	@Autowired
	private RestPlantillasInstrumentDTOFactory dtoFactory;
	
	private Logger logger = LoggerFactory.getLogger(RestPlantillasInstrumentController.class);
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={NOT_REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasInstrumentDTO> searchInstruments(
			@RequestParam(value=REQUEST_PARAM_PAGE, required=false, defaultValue="1") int currentPage,
			@RequestParam(value=REQUEST_PARAM_LAST_MODIFIED, required=false, defaultValue="") String lastModifiedText,
			@RequestParam(value=REQUEST_PARAM_ADDRID, required=false, defaultValue="") String addridText,
			@RequestParam(value=REQUEST_PARAM_PLANTNO, required=false, defaultValue="") String plantno,
			@RequestParam(value=REQUEST_PARAM_SUBDIVID, required=false, defaultValue="0") int businessSubdivId) 
			throws ParseException {
		long startTime = System.currentTimeMillis();
		logger.debug("searching lastModified > "+lastModifiedText);
		Date lastModifiedDate = super.parseDateIfNotEmpty(lastModifiedText);
		Integer addrid = super.parseIntegerIfNotEmpty(addridText);
		PagedResultSet<InstrumentPlantillasDTO> results = this.instrumentService.getPlantillasInstruments(allocatedCompanyId, lastModifiedDate, addrid, plantno, businessSubdivId, RESULTS_PER_PAGE, currentPage);
		long queryTime = System.currentTimeMillis();

		logger.debug("resultsCount: "+results.getResultsCount());
		logger.debug("t(query): "+String.valueOf(queryTime-startTime)+" ms");
		RestPagedResultDTO<RestPlantillasInstrumentDTO> resultDto = this.dtoFactory.convertResults(results);
		long resultTime = System.currentTimeMillis();

		logger.debug("t(results): "+String.valueOf(resultTime-queryTime)+" ms");
		logger.debug("t(total): "+String.valueOf(resultTime-startTime)+" ms");
		return resultDto;
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasInstrumentDTO> searchById(
			@RequestParam(value=REQUEST_PARAM_ID, required=true) int plantId) {
		long startTime = System.currentTimeMillis();
		logger.debug("searching plantId = "+plantId);
		long queryTime = System.currentTimeMillis();
		InstrumentPlantillasDTO dto = this.instrumentService.getPlantillasInstrument(plantId);
		long resultTime = System.currentTimeMillis();

		logger.debug("t(results): "+String.valueOf(resultTime-queryTime)+" ms");
		logger.debug("t(total): "+String.valueOf(resultTime-startTime)+" ms");
		
		return this.dtoFactory.convertSingle(plantId, dto);
	}
}
