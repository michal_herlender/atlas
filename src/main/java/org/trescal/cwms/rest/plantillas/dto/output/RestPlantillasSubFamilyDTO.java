package org.trescal.cwms.rest.plantillas.dto.output;

import java.util.Date;

public class RestPlantillasSubFamilyDTO {
	private Date lastModified;
	private Boolean active;
	private Integer subfamilyid;
	private Integer familyid;
	private String translation_en;
	private String translation_fr;
	private String translation_es;

	public Date getLastModified() {
		return lastModified;
	}

	public Boolean getActive() {
		return active;
	}

	public Integer getSubfamilyid() {
		return subfamilyid;
	}

	public Integer getFamilyid() {
		return familyid;
	}

	public String getTranslation_en() {
		return translation_en;
	}

	public String getTranslation_fr() {
		return translation_fr;
	}

	public String getTranslation_es() {
		return translation_es;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setSubfamilyid(Integer subfamilyid) {
		this.subfamilyid = subfamilyid;
	}

	public void setFamilyid(Integer familyid) {
		this.familyid = familyid;
	}

	public void setTranslation_en(String translation_en) {
		this.translation_en = translation_en;
	}

	public void setTranslation_fr(String translation_fr) {
		this.translation_fr = translation_fr;
	}

	public void setTranslation_es(String translation_es) {
		this.translation_es = translation_es;
	}
}
