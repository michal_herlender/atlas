package org.trescal.cwms.rest.plantillas.dto.output;

import org.trescal.cwms.core.instrumentmodel.dto.DescriptionPlantillasDTO;

public interface RestPlantillasSubFamilyDTOFactory extends RestPlantillasDTOFactory<DescriptionPlantillasDTO, RestPlantillasSubFamilyDTO> {

}
