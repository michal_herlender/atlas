package org.trescal.cwms.rest.plantillas.dto.output;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class RestPlantillasClientDTO {
	private Boolean deleted;
	private Date lastModified;
	// From address, subdiv, and company
	private int addrid;
	private int subdivid;
	private int coid;
	private String legalIdentifier;
	// Whether address, subdiv, and company are active in ERP
	private Boolean addrActive;
	private Boolean companyActive;
	private Boolean subdivActive;
	// From address, subdiv, and company
	private String coname;
	private String subname;
	private String addr1;
	private String addr2;
	private String addr3;
	private String town;
	private String county;
	private String countryCode;
	private String postcode;
	// From contact
	private String email;
	private String firstname;
	private String lastname;
	// Additional settings
	private Integer formerCustomerId;
	private Integer managedByAddrId;
	private Integer certificateAddrId;
	private String textUncertaintyFailure;
	private String textToleranceFailure;
	private Boolean sendByFtp;
	private String ftpServer;
	private String ftpUser;
	private String ftpPassword;
	private Boolean metraClient;
	private Boolean contract;
	private Boolean syncToPlantillas;
	private Boolean nextDateOnCertificate;

	// Validation Group for "update Plantillas client" web service
	public static interface UpdateGroup {
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public Date getLastModified() {
		return lastModified;
	}

	@NotNull(groups = UpdateGroup.class)
	public int getAddrid() {
		return addrid;
	}

	public int getSubdivid() {
		return subdivid;
	}

	public int getCoid() {
		return coid;
	}

	public String getLegalIdentifier() {
		return legalIdentifier;
	}

	public Boolean getAddrActive() {
		return addrActive;
	}

	public Boolean getCompanyActive() {
		return companyActive;
	}

	public Boolean getSubdivActive() {
		return subdivActive;
	}

	public String getConame() {
		return coname;
	}

	public String getSubname() {
		return subname;
	}

	public String getAddr1() {
		return addr1;
	}

	public String getAddr2() {
		return addr2;
	}

	public String getAddr3() {
		return addr3;
	}

	public String getTown() {
		return town;
	}

	public String getCounty() {
		return county;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	@NotNull(groups = UpdateGroup.class)
	public Integer getFormerCustomerId() {
		return formerCustomerId;
	}

	public Integer getManagedByAddrId() {
		return managedByAddrId;
	}

	public Integer getCertificateAddrId() {
		return certificateAddrId;
	}

	public String getTextUncertaintyFailure() {
		return textUncertaintyFailure;
	}

	public String getTextToleranceFailure() {
		return textToleranceFailure;
	}

	public Boolean getSendByFtp() {
		return sendByFtp;
	}

	public String getFtpServer() {
		return ftpServer;
	}

	public String getFtpUser() {
		return ftpUser;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public Boolean getMetraClient() {
		return metraClient;
	}

	public Boolean getContract() {
		return contract;
	}

	@NotNull(groups = UpdateGroup.class)
	public Boolean getSyncToPlantillas() {
		return syncToPlantillas;
	}

	public Boolean getNextDateOnCertificate() {
		return nextDateOnCertificate;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public void setAddrid(int addrid) {
		this.addrid = addrid;
	}

	public void setSubdivid(int subdivid) {
		this.subdivid = subdivid;
	}

	public void setCoid(int coid) {
		this.coid = coid;
	}

	public void setLegalIdentifier(String legalIdentifier) {
		this.legalIdentifier = legalIdentifier;
	}

	public void setAddrActive(Boolean addrActive) {
		this.addrActive = addrActive;
	}

	public void setCompanyActive(Boolean companyActive) {
		this.companyActive = companyActive;
	}

	public void setSubdivActive(Boolean subdivActive) {
		this.subdivActive = subdivActive;
	}

	public void setConame(String coname) {
		this.coname = coname;
	}

	public void setSubname(String subname) {
		this.subname = subname;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setFormerCustomerId(Integer formerCustomerId) {
		this.formerCustomerId = formerCustomerId;
	}

	public void setManagedByAddrId(Integer managedByAddrId) {
		this.managedByAddrId = managedByAddrId;
	}

	public void setCertificateAddrId(Integer certificateAddrId) {
		this.certificateAddrId = certificateAddrId;
	}

	public void setTextUncertaintyFailure(String textUncertaintyFailure) {
		this.textUncertaintyFailure = textUncertaintyFailure;
	}

	public void setTextToleranceFailure(String textToleranceFailure) {
		this.textToleranceFailure = textToleranceFailure;
	}

	public void setSendByFtp(Boolean sendByFtp) {
		this.sendByFtp = sendByFtp;
	}

	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}

	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	public void setMetraClient(Boolean metraClient) {
		this.metraClient = metraClient;
	}

	public void setContract(Boolean contract) {
		this.contract = contract;
	}

	public void setSyncToPlantillas(Boolean syncToPlantillas) {
		this.syncToPlantillas = syncToPlantillas;
	}

	public void setNextDateOnCertificate(Boolean nextDateOnCertificate) {
		this.nextDateOnCertificate = nextDateOnCertificate;
	}
}
