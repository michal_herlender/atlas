package org.trescal.cwms.rest.plantillas.dto.output;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

public class RestPlantillasInstrumentDTO {
	private Boolean deleted;
	private Date lastModified;
	// References
	private Integer addrid;
	private Integer subdivid;
	private Integer coid;

	private Integer plantid;
	private Integer modelid;
	// Identification
	private String manufacturer;
	private String modelName;
	private String serialno;
	private String plantno;
	private String location;
	private String contactName;
    private String formerBarCode;
    private String customerDescription;
    // private List<RestPlantillasCharacteristicDTO> characteristics;
    // private List<RestPlantillasMeasurementPossibilityDTO>
    // measurementPossibilities;
    // Service information
    private Boolean onsite;
    private Boolean receivesCalibration;
    private Boolean customermanaged;
    private Boolean outOfService;
    private LocalDate addedOn;
    private LocalDate lastCalDate;
    private Integer calFrequency;
    private String calFrequencyUnits;
    private LocalDate nextCalDueDate;
    private String observations;
    private String instructions;

    private Integer businessCompanyId;

    // Validation Group for "create instrument" web service
	public interface CreateGroup {
	}

	// Validation Group for "update instrument" web service
	public interface UpdateGroup {
	}

	public Boolean getDeleted() {
		return deleted;
	}

	@NotNull(groups = CreateGroup.class)
	public Integer getAddrid() {
		return addrid;
	}

	public Integer getSubdivid() {
		return subdivid;
	}

	public Integer getCoid() {
		return coid;
	}

	@NotNull(groups = UpdateGroup.class)
	public Integer getPlantid() {
		return plantid;
	}

	public Date getLastModified() {
		return lastModified;
	}

    @Length(max = 50, groups = {CreateGroup.class, UpdateGroup.class})
    public String getPlantno() {
        return plantno;
    }

    public Boolean getOnsite() {
        return onsite;
    }

    public LocalDate getAddedOn() {
        return addedOn;
    }

    public LocalDate getLastCalDate() {
        return lastCalDate;
    }

    @NotNull(groups = CreateGroup.class)
    @Min(value = 1, groups = {CreateGroup.class, UpdateGroup.class})
    public Integer getCalFrequency() {
		return calFrequency;
	}

	@NotNull(groups = CreateGroup.class)
	public String getCalFrequencyUnits() {
		return calFrequencyUnits;
	}

	// TODO may reduce size, matching database currently
	@Length(max = 2000, groups = { CreateGroup.class, UpdateGroup.class })
	public String getCustomerDescription() {
		return customerDescription;
	}

	@NotNull(groups = CreateGroup.class)
	public Integer getModelid() {
		return modelid;
	}

	@Length(max = 255, groups = {CreateGroup.class, UpdateGroup.class})
	public String getModelName() {
		return modelName;
	}

	public LocalDate getNextCalDueDate() {
		return nextCalDueDate;
	}

	@Length(max = 100, groups = {CreateGroup.class, UpdateGroup.class})
	public String getManufacturer() {
		return manufacturer;
	}

	@Length(max = 50, groups = {CreateGroup.class, UpdateGroup.class})
	public String getSerialno() {
		return serialno;
	}

	@Length(max = 100, groups = { CreateGroup.class, UpdateGroup.class })
	public String getLocation() {
		return location;
	}

	public String getContactName() {
		return contactName;
	}

	public Boolean getReceivesCalibration() {
		return receivesCalibration;
	}

	// TODO needs constraint on size once implemented
	public String getObservations() {
		return observations;
	}

	// TODO needs constraint on size once implemented
	public String getInstructions() {
		return instructions;
	}

	public Boolean getCustomermanaged() {
		return customermanaged;
	}

	public Boolean getOutOfService() {
		return outOfService;
	}

	@Length(max = 255, groups = {CreateGroup.class,UpdateGroup.class})
	public String getFormerBarCode() {
		return formerBarCode;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public void setAddrid(Integer addrid) {
		this.addrid = addrid;
	}

	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}

	public void setPlantid(Integer plantid) {
		this.plantid = plantid;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
    }

    public void setPlantno(String plantno) {
        this.plantno = plantno;
    }

    public void setOnsite(Boolean onsite) {
        this.onsite = onsite;
    }

    public void setAddedOn(LocalDate addedOn) {
        this.addedOn = addedOn;
    }

    public void setLastCalDate(LocalDate lastCalDate) {
        this.lastCalDate = lastCalDate;
    }

    public void setCalFrequency(Integer calFrequency) {
        this.calFrequency = calFrequency;
    }

	public void setNextCalDueDate(LocalDate nextCalDueDate) {
		this.nextCalDueDate = nextCalDueDate;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public void setReceivesCalibration(Boolean receivesCalibration) {
		this.receivesCalibration = receivesCalibration;
	}

	public void setObservations(String observations) {
		this.observations = observations;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public void setCustomermanaged(Boolean customermanaged) {
		this.customermanaged = customermanaged;
	}

	public void setOutOfService(Boolean outOfService) {
		this.outOfService = outOfService;
	}

	public void setFormerBarCode(String formerBarCode) {
		this.formerBarCode = formerBarCode;
	}

	public void setCalFrequencyUnits(String calFrequencyUnits) {
		this.calFrequencyUnits = calFrequencyUnits;
	}

	public void setModelid(Integer modelid) {
		this.modelid = modelid;
	}

	public void setCustomerDescription(String clientDescription) {
		this.customerDescription = clientDescription;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Integer getBusinessCompanyId() {
		return businessCompanyId;
	}

	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}
	
	
}
