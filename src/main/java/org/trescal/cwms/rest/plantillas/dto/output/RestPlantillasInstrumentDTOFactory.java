package org.trescal.cwms.rest.plantillas.dto.output;

import org.trescal.cwms.core.instrument.dto.InstrumentPlantillasDTO;

public interface RestPlantillasInstrumentDTOFactory 
	extends RestPlantillasDTOFactory<InstrumentPlantillasDTO, RestPlantillasInstrumentDTO> {
}
