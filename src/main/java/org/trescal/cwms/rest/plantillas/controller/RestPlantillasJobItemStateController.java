package org.trescal.cwms.rest.plantillas.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.dto.PlantillasItemStateDTO;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasJobItemStateDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasJobItemStateDTOFactory;

@Controller @JsonController
public class RestPlantillasJobItemStateController extends RestPlantillasController {
	public static final String REQUEST_MAPPING = "/plantillas/jobitemstate";
	
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private RestPlantillasJobItemStateDTOFactory dtoFactory;
	
	public static final Logger logger = LoggerFactory.getLogger(PlantillasItemStateDTO.class);
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={NOT_REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestResultDTO<RestPlantillasJobItemStateDTO> getAllItemStates() {
		List<PlantillasItemStateDTO> queryResults =  this.itemStateService.getPlantillasItemStates(plantillasLocale);
		List<ItemState> itemStateAwaitingCalibration = this.itemStateService.getAll(StateGroup.AWAITINGCALIBRATION); 
		List<ItemState> itemStateCalibration = this.itemStateService.getAll(StateGroup.CALIBRATION);
		List<ItemState> itemStateResumeCalibration = this.itemStateService.getAll(StateGroup.RESUMECALIBRATION);
		RestResultDTO<RestPlantillasJobItemStateDTO> result = new RestResultDTO<>(true, "");
		queryResults.stream().forEach(queryDto -> result.addResult(this.dtoFactory.convertDeprecated(queryDto, itemStateAwaitingCalibration, itemStateCalibration, itemStateResumeCalibration)));
		return result;
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasJobItemStateDTO> searchById(
			@RequestParam(value=REQUEST_PARAM_ID, required=true) int stateId) {
		long startTime = System.currentTimeMillis();
		logger.debug("searching stateId = "+stateId);
		PlantillasItemStateDTO dto = this.itemStateService.getPlantillasItemState(plantillasLocale, stateId);
		long queryTime = System.currentTimeMillis();
		logger.debug("t(query): "+String.valueOf(queryTime-startTime)+" ms");
		
		RestPagedResultDTO<RestPlantillasJobItemStateDTO> result = this.dtoFactory.convertSingle(stateId, dto);  
		long resultTime = System.currentTimeMillis();

		logger.debug("t(results): "+String.valueOf(resultTime-queryTime)+" ms");
		logger.debug("t(total): "+String.valueOf(resultTime-startTime)+" ms");
		return result;
	}

}
