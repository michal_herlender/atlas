package org.trescal.cwms.rest.plantillas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.instrument.dto.InstrumentPlantillasDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.plantillas.component.RestPlantillasInstrumentUpdateValidator;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasInstrumentDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasInstrumentDTOFactory;

@Controller @RestJsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class RestPlantillasInstrumentUpdateController extends RestPlantillasController {

	public static final String REQUEST_MAPPING = "/plantillas/instrument/update";

	@Autowired
	private InstrumService instrumentService;

	@Autowired
	private UserService userService;

	@Autowired
	private RestPlantillasInstrumentUpdateValidator validator;

	@Autowired
	private RestPlantillasInstrumentDTOFactory dtoFactory;

	@Autowired
	private RestErrorConverter errorConverter;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.POST, consumes = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, produces = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	RestPagedResultDTO<RestPlantillasInstrumentDTO> updateInstrument(@RequestBody RestPlantillasInstrumentDTO inputDto,
			BindingResult bindingResult, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		
		Contact updatedBy = this.userService.get(username).getCon();
		inputDto.setBusinessCompanyId(updatedBy.getSub().getComp().getId());
		
		// Manual validation, because input RequestBody is not a ModelAttribute.
		validator.validate(inputDto, bindingResult);

		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, super.plantillasLocale);
		}

		this.instrumentService.updatePlantillasInstrument(inputDto, updatedBy);
		InstrumentPlantillasDTO plantillasDto = this.instrumentService.getPlantillasInstrument(inputDto.getPlantid());

		return this.dtoFactory.convertSingle(inputDto.getPlantid(), plantillasDto);
	}

}
