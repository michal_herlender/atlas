package org.trescal.cwms.rest.plantillas.dto.output;

public class RestPlantillasJobItemStateDTO {
	private Integer stateid;
	private Boolean active;
	private Boolean retired;
	private String description;
	private Boolean awaitingCalibration;
	private Boolean calibration;
	private Boolean resumeCalibration;
	
	public Integer getStateid() {
		return stateid;
	}
	public Boolean getActive() {
		return active;
	}
	public Boolean getRetired() {
		return retired;
	}
	public String getDescription() {
		return description;
	}
	public Boolean getAwaitingCalibration() {
		return awaitingCalibration;
	}
	public Boolean getCalibration() {
		return calibration;
	}
	public Boolean getResumeCalibration() {
		return resumeCalibration;
	}
	public void setStateid(Integer stateid) {
		this.stateid = stateid;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public void setRetired(Boolean retired) {
		this.retired = retired;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setAwaitingCalibration(Boolean awaitingCalibration) {
		this.awaitingCalibration = awaitingCalibration;
	}
	public void setCalibration(Boolean calibration) {
		this.calibration = calibration;
	}
	public void setResumeCalibration(Boolean resumeCalibration) {
		this.resumeCalibration = resumeCalibration;
	}
}
