package org.trescal.cwms.rest.plantillas.controller;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.company.dto.AddressPlantillasDTO;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasClientDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasClientDTOFactory;

/**
 * REST client search for Plantillas that searches by lastModified to return 
 *   recently changed buisness and client companies (both considered as Plantillas clients)
 * Functionality is Plantillas - specific for the Zaragoza, Spain calibration tool
 *   and any changes must be co-ordinated with the synchronization interface for this tool
 * ParseException is intentionally thrown if bad date format used
 * @author Galen
 * 2017-03-02
 */
@Controller @RestJsonController
public class RestPlantillasClientController extends RestPlantillasController {
	public static final String REQUEST_MAPPING = "/plantillas/client";
	public static final int RESULTS_PER_PAGE = 1000;
	public static final String REQUEST_PARAM_FORMER_CUSTOMER_ID = "formerCustomerId";
	public static final String REQUEST_PARAM_SUBDIVID = "businessSubdivId";
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private RestPlantillasClientDTOFactory dtoFactory;
	
	private Logger logger = LoggerFactory.getLogger(RestPlantillasClientController.class);
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={NOT_REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasClientDTO> searchClients(
			@RequestParam(value=REQUEST_PARAM_PAGE, required=false, defaultValue="1") int currentPage,
			@RequestParam(value=REQUEST_PARAM_LAST_MODIFIED, required=false, defaultValue="") String lastModifiedText,
			@RequestParam(value=REQUEST_PARAM_FORMER_CUSTOMER_ID, required=false, defaultValue="") String formerCustomerIdText,
			@RequestParam(value=REQUEST_PARAM_SUBDIVID, required=false, defaultValue="0") int businessSubdivId
			) throws ParseException {
		long startTime = System.currentTimeMillis();
		logger.debug("searching lastModified > "+lastModifiedText+" and formerCustomerId = "+formerCustomerIdText);
		Date lastModifiedDate = super.parseDateIfNotEmpty(lastModifiedText);
		Integer formerCustomerId = super.parseIntegerIfNotEmpty(formerCustomerIdText);
		PagedResultSet<AddressPlantillasDTO> results = this.addressService.getPlantillasAddresses(allocatedCompanyId, lastModifiedDate, formerCustomerId, businessSubdivId, RESULTS_PER_PAGE, currentPage);
		long queryTime = System.currentTimeMillis();

		logger.debug("resultsCount: "+results.getResultsCount());
		logger.debug("t(query): "+String.valueOf(queryTime-startTime)+" ms");
		RestPagedResultDTO<RestPlantillasClientDTO> resultDto = this.dtoFactory.convertResults(results);
		long resultTime = System.currentTimeMillis();

		logger.debug("t(results): "+String.valueOf(resultTime-queryTime)+" ms");
		logger.debug("t(total): "+String.valueOf(resultTime-startTime)+" ms");
		return resultDto;
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasClientDTO> searchById(
			@RequestParam(value=REQUEST_PARAM_ID, required=true) int addressId) {
		AddressPlantillasDTO dto = this.addressService.getPlantillasAddress(allocatedCompanyId, addressId);
		RestPagedResultDTO<RestPlantillasClientDTO> result = this.dtoFactory.convertSingle(addressId, dto);  
		return result;
	}
}
