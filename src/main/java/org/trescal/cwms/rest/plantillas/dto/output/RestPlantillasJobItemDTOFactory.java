package org.trescal.cwms.rest.plantillas.dto.output;

import org.trescal.cwms.core.jobs.jobitem.dto.JobItemPlantillasDTO;

public interface RestPlantillasJobItemDTOFactory 
	extends RestPlantillasDTOFactory<JobItemPlantillasDTO, RestPlantillasJobItemDTO> {

}
