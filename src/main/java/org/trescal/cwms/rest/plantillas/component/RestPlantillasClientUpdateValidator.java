package org.trescal.cwms.rest.plantillas.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasClientDTO;

@Component
public class RestPlantillasClientUpdateValidator extends AbstractBeanValidator {
	
	@Autowired
	private AddressService addressService;

	@Override
	public boolean supports(Class<?> clazz) {
		return RestPlantillasClientDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors, RestPlantillasClientDTO.UpdateGroup.class);
		RestPlantillasClientDTO dto = (RestPlantillasClientDTO) target;

		//ensure address exists
		Address address = addressService.get(dto.getAddrid());
		if (address == null) {
			errors.rejectValue("addrid", RestErrors.CODE_DATA_NOT_FOUND, new Object[] { dto.getAddrid() },
					RestErrors.MESSAGE_DATA_NOT_FOUND);
		}		
	}

}
