package org.trescal.cwms.rest.plantillas.dto.output;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.dto.AddressPlantillasDTO;
import org.trescal.cwms.core.tools.DateTools;

@Component
public class RestPlantillasClientDTOFactoryImpl 
	extends AbstractRestPlantillasDTOFactory<AddressPlantillasDTO, RestPlantillasClientDTO> 
	implements RestPlantillasClientDTOFactory {

	/**
	 * Null safe checks used
	 * (CompanySettingsForAllocatedCompany won't exist for many inactive companies)
	 * (AddressPlantillasSettings may not exist)
	 */ 
	@Override
	public RestPlantillasClientDTO convert(AddressPlantillasDTO dto) {
		RestPlantillasClientDTO restDto = new RestPlantillasClientDTO();
		restDto.setAddr1(dto.getAddr1());
		restDto.setAddr2(dto.getAddr2());
		restDto.setAddr3(dto.getAddr3());
		restDto.setAddrActive(dto.getAddrActive());
		restDto.setAddrid(dto.getAddrid());
		restDto.setCertificateAddrId(nullSafe(dto.getCertificateAddrId()));
		restDto.setCoid(dto.getCoid());
		restDto.setCompanyActive(nullSafe(dto.getCompanyActive(), false));
		restDto.setConame(dto.getConame());
		restDto.setContract(nullSafe(dto.getContract(), false));
		restDto.setCountryCode(dto.getCountrycode());
		restDto.setCounty(dto.getCounty());
		restDto.setDeleted(false);
		restDto.setEmail(dto.getEmail());
		restDto.setFirstname(dto.getFirstname());
		restDto.setFormerCustomerId(nullSafe(dto.getFormerCustomerId()));
		restDto.setFtpPassword(nullSafe(dto.getFtpPassword()));
		restDto.setFtpServer(nullSafe(dto.getFtpServer()));
		restDto.setFtpUser(nullSafe(dto.getFtpUser()));
		restDto.setLastModified(getLatestLastModified(dto));
		restDto.setLastname(dto.getLastname());
		restDto.setLegalIdentifier(dto.getLegalIdentifier());
		restDto.setManagedByAddrId(nullSafe(dto.getManagedByAddrId()));
		restDto.setMetraClient(nullSafe(dto.getMetraClient(), false));
		restDto.setNextDateOnCertificate(nullSafe(dto.getNextDateOnCertificate(), false));
		restDto.setPostcode(dto.getPostcode());
		restDto.setSendByFtp(nullSafe(dto.getSendByFTP(), false));
		restDto.setSubdivActive(nullSafe(dto.getSubdivActive(), false));
		restDto.setSubdivid(dto.getSubdivid());
		restDto.setSubname(dto.getSubname());
		restDto.setSyncToPlantillas(nullSafe(dto.getSyncToPlantillas(), false));
		restDto.setTextUncertaintyFailure(nullSafe(dto.getTextUncertaintyFailure()));
		restDto.setTextToleranceFailure(nullSafe(dto.getTextToleranceFailure()));
		restDto.setTown(dto.getTown());
		return restDto;
	}
	
	/**
	 * Returns the latest of any of the lastModified dates
	 * The contact lastModified date may be null if there is no default contact. 
	 */
	public Date getLatestLastModified(AddressPlantillasDTO dto) {
		Date result = dto.getLastModifiedAddress();
		result = DateTools.getLatest(result, dto.getLastModifiedCompany());
		result = DateTools.getLatest(result, dto.getLastModifiedCompanySettings());
		result = DateTools.getLatest(result, dto.getLastModifiedContact());
		result = DateTools.getLatest(result, dto.getLastModifiedPlantillasSettings());
		result = DateTools.getLatest(result, dto.getLastModifiedSubdiv());
				
		return result;
	}
}
