package org.trescal.cwms.rest.plantillas.dto.output;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrumentmodel.dto.DescriptionPlantillasDTO;

@Component
public class RestPlantillasSubFamilyDTOFactoryImpl 
	extends AbstractRestPlantillasDTOFactory<DescriptionPlantillasDTO, RestPlantillasSubFamilyDTO>
	implements RestPlantillasSubFamilyDTOFactory {

	@Override
	public RestPlantillasSubFamilyDTO convert(DescriptionPlantillasDTO dto) {
		RestPlantillasSubFamilyDTO result = new RestPlantillasSubFamilyDTO();
		result.setActive(dto.getActive());
		result.setFamilyid(dto.getFamilyid());
		result.setLastModified(dto.getLastModified());
		result.setSubfamilyid(dto.getSubfamilyid());
		result.setTranslation_en(dto.getTranslation_en());
		result.setTranslation_es(dto.getTranslation_es());
		result.setTranslation_fr(dto.getTranslation_fr());
		return result;
	}
}
