package org.trescal.cwms.rest.plantillas.dto.output;

import org.trescal.cwms.core.company.dto.AddressPlantillasDTO;

public interface RestPlantillasClientDTOFactory 
	extends RestPlantillasDTOFactory<AddressPlantillasDTO, RestPlantillasClientDTO> {
}
