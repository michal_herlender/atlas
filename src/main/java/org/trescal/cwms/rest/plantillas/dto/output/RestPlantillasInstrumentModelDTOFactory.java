package org.trescal.cwms.rest.plantillas.dto.output;

import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelPlantillasDTO;

public interface RestPlantillasInstrumentModelDTOFactory 
	extends RestPlantillasDTOFactory<InstrumentModelPlantillasDTO, RestPlantillasInstrumentModelDTO> {

}
