package org.trescal.cwms.rest.plantillas.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;
import org.trescal.cwms.core.company.entity.addressplantillassettings.db.AddressPlantillasSettingsService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasClientDTO;

@Service
public class RestPlantillasClientUpdateServiceImpl implements RestPlantillasClientUpdateService {

	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private AddressPlantillasSettingsService addressPlantillasSettingsService;
	@Value("${cwms.config.plantillas.businesscompany.coid}")
	protected int allocatedCompanyId;

	@Override
	public void updateClient(RestPlantillasClientDTO inputDto) {
		Address address = this.addressService.get(inputDto.getAddrid());
		Company allocatedCompany = this.companyService.get(allocatedCompanyId);
		Company company = address.getSub().getComp();
		// former customer
		AddressPlantillasSettings plantillasSettings = this.addressPlantillasSettingsService
				.getForAddress(address.getAddrid(), allocatedCompanyId);
		boolean newPlantillasSettings = plantillasSettings == null; 
		if (newPlantillasSettings) {
			plantillasSettings = this.addressPlantillasSettingsService.createAndInitilazeNewEntity();
			plantillasSettings.setAddress(address);
			plantillasSettings.setOrganisation(allocatedCompany);
		}
		plantillasSettings.setFormerCustomerId(inputDto.getFormerCustomerId());
		if (newPlantillasSettings) {
			this.addressPlantillasSettingsService.save(plantillasSettings);
		}
		if (inputDto.getSyncToPlantillas() != null) {
			CompanySettingsForAllocatedCompany settings = this.companySettingsService.getByCompany(company,
					allocatedCompany);
			boolean newCompanySettings = settings == null; 
			if (newCompanySettings) {
				settings = this.companySettingsService.initializeForCompany(company, allocatedCompany, false);
			}
			settings.setSyncToPlantillas(inputDto.getSyncToPlantillas());
			if (newCompanySettings) {
				this.companySettingsService.save(settings);
			}
		}
	}
}