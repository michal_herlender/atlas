package org.trescal.cwms.rest.plantillas.controller;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.dto.DescriptionPlantillasDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasSubFamilyDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasSubFamilyDTOFactory;

@Controller @JsonController
public class RestPlantillasSubFamilyController extends RestPlantillasController {

	@Autowired
	private NewDescriptionService descriptionService;
	
	@Autowired
	private RestPlantillasSubFamilyDTOFactory dtoFactory;
	
	public static final String REQUEST_MAPPING = "/plantillas/subfamily";
	public static final int RESULTS_PER_PAGE = 1000;

	public static final Logger logger = LoggerFactory.getLogger(RestPlantillasSubFamilyController.class);
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={NOT_REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasSubFamilyDTO> searchByLastModified(
			@RequestParam(value=REQUEST_PARAM_PAGE, required=false, defaultValue="1") int currentPage,
			@RequestParam(value=REQUEST_PARAM_LAST_MODIFIED, required=false, defaultValue="") String lastModifiedText) 
			throws ParseException {
		long startTime = System.currentTimeMillis();
		logger.debug("searching lastModified > "+lastModifiedText);
		Date lastModifiedDate = super.parseDateIfNotEmpty(lastModifiedText);
		PagedResultSet<DescriptionPlantillasDTO> results = this.descriptionService.getPlantillasDescriptions(lastModifiedDate, RESULTS_PER_PAGE, currentPage);
		long queryTime = System.currentTimeMillis();

		logger.debug("resultsCount: "+results.getResultsCount());
		logger.debug("t(query): "+String.valueOf(queryTime-startTime)+" ms");
		RestPagedResultDTO<RestPlantillasSubFamilyDTO> resultDto = this.dtoFactory.convertResults(results);
		long resultTime = System.currentTimeMillis();

		logger.debug("t(results): "+String.valueOf(resultTime-queryTime)+" ms");
		logger.debug("t(total): "+String.valueOf(resultTime-startTime)+" ms");
		return resultDto;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasSubFamilyDTO> searchById(
			@RequestParam(value=REQUEST_PARAM_ID, required=true) int descriptionId) {
		long startTime = System.currentTimeMillis();
		logger.debug("searching descriptionId = "+descriptionId);
		DescriptionPlantillasDTO dto = this.descriptionService.getPlantillasDescription(descriptionId);
		long queryTime = System.currentTimeMillis();
		
		RestPagedResultDTO<RestPlantillasSubFamilyDTO> result = this.dtoFactory.convertSingle(descriptionId, dto);  
		long resultTime = System.currentTimeMillis();

		logger.debug("t(results): "+String.valueOf(resultTime-queryTime)+" ms");
		logger.debug("t(total): "+String.valueOf(resultTime-startTime)+" ms");
		return result;
	}	
}
