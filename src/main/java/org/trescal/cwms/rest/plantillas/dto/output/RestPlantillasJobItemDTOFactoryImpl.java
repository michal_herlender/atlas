package org.trescal.cwms.rest.plantillas.dto.output;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemPlantillasDTO;
import org.trescal.cwms.core.tools.DateTools;

@Component
public class RestPlantillasJobItemDTOFactoryImpl 
	extends AbstractRestPlantillasDTOFactory<JobItemPlantillasDTO, RestPlantillasJobItemDTO>
	implements RestPlantillasJobItemDTOFactory {
	public RestPlantillasJobItemDTO convert(JobItemPlantillasDTO queryDto) {
		RestPlantillasJobItemDTO restDto = new RestPlantillasJobItemDTO();
		restDto.setAccredited(queryDto.getAccredited());
		restDto.setAddrid(queryDto.getAddrid());
		restDto.setCalibrationPrice(queryDto.getCalCost());
		restDto.setCurrencyCode(queryDto.getCurrencyCode());
		restDto.setDateIn(DateTools.dateFromZonedDateTime(queryDto.getDateIn()));

		restDto.setDeleted(false);
		restDto.setInstructions("");            // TODO
		restDto.setJobid(queryDto.getJobid());
		restDto.setJobitemid(queryDto.getJobitemid());
		restDto.setJobitemno(queryDto.getJobitemno());
		restDto.setJobno(queryDto.getJobno());
		restDto.setLastModified(DateTools.getLatest(queryDto.getLastModifiedJob(), queryDto.getLastModifiedJobItem()));
		restDto.setLastTransitDateIn(queryDto.getLastTransitDateIn());
		restDto.setObservations("");
		restDto.setOnsite(JobType.SITE.equals(queryDto.getJobType()));
		restDto.setPlantid(queryDto.getPlantid());
		restDto.setStateid(queryDto.getStateid());
		restDto.setOnBehalfOfAddressId(queryDto.getOnBehalfOfAddrId());
		restDto.setOnBehalfOfCompanyId(queryDto.getOnBehalfOfCoId());
		
		return restDto;
	}
}
