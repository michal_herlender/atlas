package org.trescal.cwms.rest.plantillas.dto.output;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.workflow.dto.PlantillasItemStateDTO;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

@Component
public class RestPlantillasJobItemStateDTOFactory extends AbstractRestPlantillasDTOFactory<PlantillasItemStateDTO, RestPlantillasJobItemStateDTO>{
	
	/*
	 * TODO: Move to do with CriteriaBuilder once tested for individual.
	 */
	@Deprecated
	public RestPlantillasJobItemStateDTO convertDeprecated(PlantillasItemStateDTO queryDto, 
			List<ItemState> itemStateAwaitingCalibration, List<ItemState> itemStateCalibration, List<ItemState> itemStateResumeCalibration) {
		Set<Integer> itemStateIdsAwaitingCalibration = 
				itemStateAwaitingCalibration.stream().map(itemState -> itemState.getStateid()).distinct().collect(Collectors.toSet());
		Set<Integer> itemStateIdsCalibration = 
				itemStateCalibration.stream().map(itemState -> itemState.getStateid()).distinct().collect(Collectors.toSet());
		Set<Integer> itemStateIdsResumeCalibration =
				itemStateResumeCalibration.stream().map(itemState -> itemState.getStateid()).distinct().collect(Collectors.toSet());

		RestPlantillasJobItemStateDTO restDto = convert(queryDto);
		restDto.setCalibration(itemStateIdsCalibration.contains(queryDto.getStateid()));
		restDto.setAwaitingCalibration(itemStateIdsAwaitingCalibration.contains(queryDto.getStateid()));
		restDto.setResumeCalibration(itemStateIdsResumeCalibration.contains(queryDto.getStateid()));
		
		return restDto;
	}

	@Override
	public RestPlantillasJobItemStateDTO convert(PlantillasItemStateDTO queryDto) {
		RestPlantillasJobItemStateDTO restDto = new RestPlantillasJobItemStateDTO();
		restDto.setActive(queryDto.getActive());
		restDto.setAwaitingCalibration(queryDto.getSglAwaitingCalibration() != null);
		restDto.setCalibration(queryDto.getSglCalibration() != null);
		if ((queryDto.getDescription() != null) && !queryDto.getDescription().isEmpty()) {
			restDto.setDescription(queryDto.getDescription());
		}
		else {
			restDto.setDescription(queryDto.getDescriptionFallback());
		}
		
		restDto.setResumeCalibration(queryDto.getSglResumeCalibration() != null);
		restDto.setRetired(queryDto.getRetired());
		restDto.setStateid(queryDto.getStateid());
		return restDto;
	}
}
