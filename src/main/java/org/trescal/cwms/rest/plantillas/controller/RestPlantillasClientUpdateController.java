package org.trescal.cwms.rest.plantillas.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.company.dto.AddressPlantillasDTO;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.exception.controller.RestJsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.plantillas.component.RestPlantillasClientUpdateService;
import org.trescal.cwms.rest.plantillas.component.RestPlantillasClientUpdateValidator;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasClientDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasClientDTOFactory;

@Controller
@RestJsonController
public class RestPlantillasClientUpdateController extends RestPlantillasController {

	public static final String REQUEST_MAPPING = "/plantillas/client/update";

	@Autowired
	private AddressService addressService;
	@Autowired
	private RestPlantillasClientUpdateValidator validator;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private RestPlantillasClientUpdateService restPlantillasClientUpdateService;
	@Autowired
	private RestPlantillasClientDTOFactory dtoFactory;

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { REQUEST_MAPPING }, method = RequestMethod.POST, consumes = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 }, produces = {
					Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestPagedResultDTO<RestPlantillasClientDTO> updateClient(@RequestBody RestPlantillasClientDTO inputDto,
			BindingResult bindingResult) throws ParseException {

		validator.validate(inputDto, bindingResult);

		if (bindingResult.hasErrors()) {
			return this.errorConverter.createResultDTO(bindingResult, super.plantillasLocale);
		}

		restPlantillasClientUpdateService.updateClient(inputDto);
		AddressPlantillasDTO addressPlantillasDTO = this.addressService.getPlantillasAddress(allocatedCompanyId, inputDto.getAddrid());

		RestPagedResultDTO<RestPlantillasClientDTO> result = new RestPagedResultDTO<>(true, "",1,1);
		result.setResultsCount(1);
		result.addResult(dtoFactory.convert(addressPlantillasDTO));
		return result;
	}

}
