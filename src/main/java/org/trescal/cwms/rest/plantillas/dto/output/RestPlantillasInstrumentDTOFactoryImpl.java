package org.trescal.cwms.rest.plantillas.dto.output;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrument.dto.InstrumentPlantillasDTO;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Component
public class RestPlantillasInstrumentDTOFactoryImpl
		extends AbstractRestPlantillasDTOFactory<InstrumentPlantillasDTO, RestPlantillasInstrumentDTO>
		implements RestPlantillasInstrumentDTOFactory {

	@Override
	public RestPlantillasInstrumentDTO convert(InstrumentPlantillasDTO queryDto) {
		RestPlantillasInstrumentDTO restDto = new RestPlantillasInstrumentDTO();
		restDto.setDeleted(false);
		restDto.setLastModified(queryDto.getLastModifiedInstrument());
		// References
		restDto.setAddrid(queryDto.getAddrid());
		restDto.setSubdivid(queryDto.getSubdivid());
		restDto.setCoid(queryDto.getCoid());
		restDto.setPlantid(queryDto.getPlantid());
		// Changed from modelId to modelid 2017-09-05 GB
		restDto.setModelid(queryDto.getModelid());
		// Identification
		restDto.setManufacturer(queryDto.getInstMfr());
		restDto.setModelName(queryDto.getModelname());
		restDto.setSerialno(queryDto.getSerialno());
		restDto.setPlantno(queryDto.getPlantno());
		restDto.setLocation(queryDto.getLocation());
		restDto.setContactName(getContactName(queryDto));
		restDto.setFormerBarCode(queryDto.getFormerBarCode());
		restDto.setCustomerDescription(queryDto.getCustomerDescription());
		// Service information
		restDto.setOnsite(false); // TODO once in ERP
		restDto.setReceivesCalibration(queryDto.getInstrumentUsageTypeRecall());
		restDto.setCustomermanaged(queryDto.getCustomermanaged());
		restDto.setOutOfService(getOutOfService(queryDto));
		restDto.setAddedOn(queryDto.getAddedOn());
		restDto.setLastCalDate(queryDto.getLastCalDate());
		restDto.setCalFrequency(queryDto.getCalFrequency());
		restDto.setCalFrequencyUnits(getCalFrequencyUnits(queryDto.getCalFrequencyUnit()));
		restDto.setNextCalDueDate(queryDto.getNextCalDueDate());
		restDto.setObservations(""); // TODO
		restDto.setInstructions(""); // TODO
		return restDto;
	}

	private boolean getOutOfService(InstrumentPlantillasDTO queryDto) {
		if (queryDto.getStatus() != null) {
			switch (queryDto.getStatus()) {
			case BER:
				return true;
			case TRANSFERRED:
				return true;
			default:
				return false;
			}
		}
		return false;
	}

	private String getContactName(InstrumentPlantillasDTO queryDto) {
		return queryDto.getContactFirstName() + " " + queryDto.getContactLastName();
	}

	private String getCalFrequencyUnits(IntervalUnit unit) {
		String result = IntervalUnit.MONTH.name();
		if (unit != null) {
			result = unit.name();
		}
		return result;
	}
}
