package org.trescal.cwms.rest.plantillas.component;

import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasClientDTO;

public interface RestPlantillasClientUpdateService {
	
	public void updateClient(RestPlantillasClientDTO inputDto);

}
