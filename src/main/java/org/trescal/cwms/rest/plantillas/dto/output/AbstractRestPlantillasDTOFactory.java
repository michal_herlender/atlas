package org.trescal.cwms.rest.plantillas.dto.output;

import java.util.Locale;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;

public abstract class AbstractRestPlantillasDTOFactory<InputDTO, OutputDTO> implements 
	RestPlantillasDTOFactory<InputDTO, OutputDTO>, InitializingBean {

	@Autowired
	private MessageSource messageSource;
	@Value("${cwms.config.plantillas.locale.country}")
	private String country;
	@Value("${cwms.config.plantillas.locale.lang}")
	private String lang;
	// Initialized
	private Locale plantillasLocale;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.plantillasLocale = new Locale(lang, country);
	}
	
	@Override
	public RestPagedResultDTO<OutputDTO> convertResults(PagedResultSet<InputDTO> queryResults) {
		RestPagedResultDTO<OutputDTO> resultDto = new RestPagedResultDTO<>(true, "", queryResults.getResultsPerPage(), queryResults.getCurrentPage());
		queryResults.getResults().stream().forEach(queryDto -> resultDto.addResult(this.convert(queryDto)));
		resultDto.setResultsCount(queryResults.getResultsCount());
		return resultDto;
	}
	
	@Override
	public RestPagedResultDTO<OutputDTO> convertSingle(int id, InputDTO singleResult) {
		RestPagedResultDTO<OutputDTO> resultDto = null;
		if (singleResult != null) {
			resultDto = new RestPagedResultDTO<>(true, "", 1, 1);
			resultDto.addResult(this.convert(singleResult));
			resultDto.setResultsCount(1);
		}
		else {
			String messageNotFound = this.messageSource.getMessage(RestErrors.CODE_DATA_NOT_FOUND, new Object[]{id}, RestErrors.MESSAGE_DATA_NOT_FOUND, plantillasLocale);
			resultDto = new RestPagedResultDTO<>(false, messageNotFound, 1,1);
		}
		return resultDto;
	}

	/**
	 * Null safe check for value (if dto value may be null from left join results)
	 */
	protected Boolean nullSafe(Boolean value, boolean defaultValue) {
		return value != null ? value : defaultValue;
	}
	
	/**
	 * Null safe check for value (if dto value may be null from left join results)
	 */
	protected String nullSafe(String value) {
		return value != null ? value : "";
	}
	
	/**
	 * Null safe check for value (if dto value may be null from left join results)
	 */
	protected Integer nullSafe(Integer value) {
		return value != null ? value : 0;
	}
	
}
