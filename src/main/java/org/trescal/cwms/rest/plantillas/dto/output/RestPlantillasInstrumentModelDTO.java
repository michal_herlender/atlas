package org.trescal.cwms.rest.plantillas.dto.output;

import java.util.Date;

public class RestPlantillasInstrumentModelDTO {
	private Boolean deleted;
	private Date lastModified;
	// References
	private Integer modelid;
	private Integer subfamilyid;
	// Identification
	private Boolean canSelect;
	private Boolean manufacturerSpecific;
	private String manufacturer;
	private String model;
	private String salesCategory;
	private String domain;
	private String family;
	private String subFamily;
	private String type;
	
	public Boolean getDeleted() {
		return deleted;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public Integer getModelid() {
		return modelid;
	}
	public Boolean getManufacturerSpecific() {
		return manufacturerSpecific;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public String getSalesCategory() {
		return salesCategory;
	}
	public String getDomain() {
		return domain;
	}
	public String getFamily() {
		return family;
	}
	public String getModel() {
		return model;
	}
	public String getSubFamily() {
		return subFamily;
	}
	public Integer getSubfamilyid() {
		return subfamilyid;
	}
	public Boolean getCanSelect() {
		return canSelect;
	}
	public String getType() {
		return type;
	}
	
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public void setModelid(Integer modelid) {
		this.modelid = modelid;
	}
	public void setManufacturerSpecific(Boolean manufacturerSpecific) {
		this.manufacturerSpecific = manufacturerSpecific;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public void setSalesCategory(String salesCategory) {
		this.salesCategory = salesCategory;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public void setSubFamily(String subFamily) {
		this.subFamily = subFamily;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public void setSubfamilyid(Integer subfamilyid) {
		this.subfamilyid = subfamilyid;
	}
	public void setCanSelect(Boolean canSelect) {
		this.canSelect = canSelect;
	}
	public void setType(String type) {
		this.type = type;
	}
}
