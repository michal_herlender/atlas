package org.trescal.cwms.rest.plantillas.dto.output;

import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;

public interface RestPlantillasDTOFactory<InputDTO, OutputDTO> {
	public OutputDTO convert(InputDTO dto);
	public RestPagedResultDTO<OutputDTO> convertResults(PagedResultSet<InputDTO> queryResults);
	public RestPagedResultDTO<OutputDTO> convertSingle(int id, InputDTO singleResult);
}
