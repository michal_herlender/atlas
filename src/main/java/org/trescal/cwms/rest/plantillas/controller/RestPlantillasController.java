package org.trescal.cwms.rest.plantillas.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.trescal.cwms.core.tools.DateTools;

public abstract class RestPlantillasController implements InitializingBean {
	
	@Value("${cwms.config.plantillas.locale.country}")
	private String country;
	@Value("${cwms.config.plantillas.locale.lang}")
	private String lang;
	@Value("${cwms.config.plantillas.businesscompany.coid}")
	protected int allocatedCompanyId;
	@Value("${cwms.config.plantillas.businessaddress.addrid}")
	protected int allocatedAddressId;

	protected Locale plantillasLocale;

	public static final String REQUEST_PARAM_LAST_MODIFIED = "lastModified";
	public static final String REQUEST_PARAM_PAGE = "page";
	public static final String REQUEST_PARAM_ID = "id";
	public static final String NOT_REQUEST_PARAM_ID = "!id";

	@Override
	public void afterPropertiesSet() throws Exception {
		this.plantillasLocale = new Locale(lang, country);
	}

	/**
	 * Specialized date handling for Plantillas interface If string is empty,
	 * return null, otherwise parse date
	 * 
	 * @param String text
	 * @return Date date
	 * @throws ParseException if not in RESTful date format
	 */
	protected Date parseDateIfNotEmpty(String text) throws ParseException {
		Date result = null;
		if (text != null && !text.isEmpty()) {
			result = DateTools.rest_dtf.parse(text);
		}
		return result;
	}

	/**
	 * Specialized integer handling for Plantillas interface If string is empty,
	 * return null, otherwise parse integer
	 * 
	 * @param String text
	 * @return Integer result
	 * @throws ParseException if not empty but not in integer
	 */
	protected Integer parseIntegerIfNotEmpty(String text) throws NumberFormatException {
		Integer result = null;
		if (text != null && !text.isEmpty()) {
			result = Integer.valueOf(text);
		}
		return result;
	}
}
