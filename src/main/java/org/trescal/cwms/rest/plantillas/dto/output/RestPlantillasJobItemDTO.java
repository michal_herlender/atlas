package org.trescal.cwms.rest.plantillas.dto.output;

import java.math.BigDecimal;
import java.util.Date;

public class RestPlantillasJobItemDTO {
	private Boolean deleted;
	private Date lastModified;
	private Integer addrid;			// Client address we are returning the job to 
	private Integer plantid;
	private Integer jobitemid;
	private Integer jobid;
	private String jobno;
	private Integer jobitemno;		// Number within the job
	private Integer stateid;		// Relating to job item state
	private Boolean onsite;
	private Boolean accredited;
	private Date dateIn;			// Date job item created
	private Date lastTransitDateIn;	// Last date of job item transit into in local facility (if any)
	private BigDecimal calibrationPrice;
	private String currencyCode;	// Human readable acronym
	private String observations;
	private String instructions;
	private Integer onBehalfOfAddressId;			// On behalf of address (if any)
	private Integer onBehalfOfCompanyId;			// On behalf of company (if any)
	
	
	public Boolean getDeleted() {
		return deleted;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public Integer getAddrid() {
		return addrid;
	}
	public Integer getPlantid() {
		return plantid;
	}
	public Integer getJobitemid() {
		return jobitemid;
	}
	public Integer getJobid() {
		return jobid;
	}
	public String getJobno() {
		return jobno;
	}
	public Integer getJobitemno() {
		return jobitemno;
	}
	public Integer getStateid() {
		return stateid;
	}
	public Boolean getOnsite() {
		return onsite;
	}
	public Boolean getAccredited() {
		return accredited;
	}
	public BigDecimal getCalibrationPrice() {
		return calibrationPrice;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public String getObservations() {
		return observations;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public void setAddrid(Integer addrid) {
		this.addrid = addrid;
	}
	public void setPlantid(Integer plantid) {
		this.plantid = plantid;
	}
	public void setJobitemid(Integer jobitemid) {
		this.jobitemid = jobitemid;
	}
	public void setJobid(Integer jobid) {
		this.jobid = jobid;
	}
	public void setJobno(String jobno) {
		this.jobno = jobno;
	}
	public void setJobitemno(Integer jobitemno) {
		this.jobitemno = jobitemno;
	}
	public void setStateid(Integer stateid) {
		this.stateid = stateid;
	}
	public void setOnsite(Boolean onsite) {
		this.onsite = onsite;
	}
	public void setAccredited(Boolean accredited) {
		this.accredited = accredited;
	}
	public void setCalibrationPrice(BigDecimal calibrationPrice) {
		this.calibrationPrice = calibrationPrice;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public void setObservations(String observations) {
		this.observations = observations;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public Date getDateIn() {
		return dateIn;
	}
	public Date getLastTransitDateIn() {
		return lastTransitDateIn;
	}
	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}
	public void setLastTransitDateIn(Date lastTransitDateIn) {
		this.lastTransitDateIn = lastTransitDateIn;
	}
	public Integer getOnBehalfOfAddressId() {
		return onBehalfOfAddressId;
	}
	public void setOnBehalfOfAddressId(Integer onBehalfOfAddressId) {
		this.onBehalfOfAddressId = onBehalfOfAddressId;
	}
	public Integer getOnBehalfOfCompanyId() {
		return onBehalfOfCompanyId;
	}
	public void setOnBehalfOfCompanyId(Integer onBehalfOfCompanyId) {
		this.onBehalfOfCompanyId = onBehalfOfCompanyId;
	}
}
