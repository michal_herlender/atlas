package org.trescal.cwms.rest.plantillas.component;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutPlantNo;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutSerialNo;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasInstrumentDTO;

import javax.validation.constraints.NotNull;

@Component
public class RestPlantillasInstrumentCreateValidator extends AbstractBeanValidator {

	@Autowired
	private AddressService addressService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private MfrService mfrService;
	@Autowired
	private AllowInstrumentCreationWithoutPlantNo alloweInstCreat;
	@Autowired
	private AllowInstrumentCreationWithoutSerialNo allowInstrumentCreationWithoutSerialNo;

	@Override
	public boolean supports(@NotNull Class<?> arg0) {
		return RestPlantillasInstrumentDTO.class.equals(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors, RestPlantillasInstrumentDTO.CreateGroup.class);
		RestPlantillasInstrumentDTO dto = (RestPlantillasInstrumentDTO) target; 
		// If address specified (mandatory), look up and ensure validity
		if (dto.getAddrid() != null) {
			Address address = this.addressService.get(dto.getAddrid());
			if (address == null) {
				errors.rejectValue("addrid", RestErrors.CODE_DATA_NOT_FOUND, new Object[]{dto.getAddrid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			// If location specified (optional), and address exists, look up location and ensure validity
			if ((dto.getLocation() != null) && !dto.getLocation().isEmpty()) {
				Location location = this.locationService.findLocation(dto.getAddrid(), dto.getLocation());
				if (location == null) {
					errors.rejectValue("location", RestErrors.CODE_DATA_NOT_FOUND, new Object[]{dto.getLocation()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
				}
			}
			// Ensure subdiv for address has a default contact
			if ((address != null) && (address.getSub().getDefaultContact() == null)) {
				errors.rejectValue("addrid", RestErrors.CODE_SUBDIV_NO_DEFAULT_CONTACT, null, RestErrors.MESSAGE_SUBDIV_NO_DEFAULT_CONTACT);
			}
		}
		// If model specified (mandatory), look up and ensure validity
		InstrumentModel model = null;
		if (dto.getModelid() != null) {
			model = this.instrumentModelService.get(dto.getModelid());
			if (model == null) {
				errors.rejectValue("modelid", RestErrors.CODE_DATA_NOT_FOUND, new Object[]{dto.getModelid()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
			if (!model.getModelType().getCanSelect()) {
				errors.rejectValue("modelid", RestErrors.CODE_MODEL_UNSELECTABLE, null, RestErrors.MESSAGE_MODEL_UNSELECTABLE);
			}
		}		
		// If manufacturer specified (optional), look up and ensure validity
		if ((dto.getManufacturer() != null) && !dto.getManufacturer().isEmpty()) {
			if ((model != null) && model.getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC)) {
				errors.rejectValue("manufacturer", RestErrors.CODE_MODEL_IS_MFR_SPECIFIC, null, RestErrors.MESSAGE_MODEL_IS_MFR_SPECIFIC);
			}
			
			Mfr mfr = this.mfrService.findMfrByName(dto.getManufacturer());
			if (mfr == null) {
				errors.rejectValue("manufacturer", RestErrors.CODE_DATA_NOT_FOUND, new Object[]{dto.getManufacturer()}, RestErrors.MESSAGE_DATA_NOT_FOUND);
			}
		}
		// If cal frequency units specified (mandatory), ensure validity
		if (dto.getCalFrequencyUnits() != null) {
			try {
				IntervalUnit.valueOf(dto.getCalFrequencyUnits());
			} catch (IllegalArgumentException e) {
				errors.rejectValue("calFrequencyUnits", RestErrors.CODE_DATA_INVALID, new Object[]{dto.getCalFrequencyUnits()}, RestErrors.MESSAGE_DATA_INVALID);
			}
		}

		if (!allowInstrumentCreationWithoutSerialNo.getValueByScope(Scope.COMPANY, dto.getBusinessCompanyId(), dto.getBusinessCompanyId()) && StringUtils.isBlank(dto.getSerialno()))
			errors.rejectValue("serialno", "error.instrument.serialno");

		if (!alloweInstCreat.getValueByScope(Scope.COMPANY, dto.getBusinessCompanyId(), dto.getBusinessCompanyId())) {
			if (StringUtils.isBlank(dto.getPlantno())) {
				errors.rejectValue("plantno", RestErrors.CODE_INSTRUMENT_PLANT_NO_MUST_BE_SPECIFIED, new Object[]{dto.getPlantno()}, RestErrors.MESSAGE_INSTRUMENT_PLANT_NO_MUST_BE_SPECIFIED);
			}
		}
	}
}