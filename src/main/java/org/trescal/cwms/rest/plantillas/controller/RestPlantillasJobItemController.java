package org.trescal.cwms.rest.plantillas.controller;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemPlantillasDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasJobItemDTO;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasJobItemDTOFactory;

@Controller @JsonController
public class RestPlantillasJobItemController extends RestPlantillasController {
	public static final String REQUEST_MAPPING = "/plantillas/jobitem";
	public static final int RESULTS_PER_PAGE = 1000;
	public static final String REQUEST_PARAM_ADDRID = "addrid";
	public static final String REQUEST_PARAM_SUBDIVID = "businessSubdivId";

	@Autowired
	private JobItemService jobItemService;
	
	@Autowired
	private RestPlantillasJobItemDTOFactory dtoFactory;
	
	private Logger logger = LoggerFactory.getLogger(RestPlantillasJobItemController.class);
	
	/**
	 * Returns data for all synchronized job items matching criteria
	 * @param currentPage - optional page number (defaults to 1, first page)
	 * @param lastModifiedText - optional date string
	 * @param addrid - optional addressId of client address 
	 * @return
	 * @throws ParseException
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={NOT_REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasJobItemDTO> searchJobItems(
			@RequestParam(value=REQUEST_PARAM_PAGE, required=false, defaultValue="1") int currentPage,
			@RequestParam(value=REQUEST_PARAM_LAST_MODIFIED, required=false, defaultValue="") String lastModifiedText,
			@RequestParam(value=REQUEST_PARAM_ADDRID, required=false, defaultValue="") String addressIdText,
			@RequestParam(value=REQUEST_PARAM_SUBDIVID, required=false, defaultValue="0") int businessSubdivId) 
			throws ParseException {
		long startTime = System.currentTimeMillis();
		Date lastModifiedDate = super.parseDateIfNotEmpty(lastModifiedText);
		Integer clientAddressId = super.parseIntegerIfNotEmpty(addressIdText);
		PagedResultSet<JobItemPlantillasDTO> results = this.jobItemService.getPlantillasJobItems(super.allocatedCompanyId, super.allocatedAddressId, lastModifiedDate, clientAddressId, businessSubdivId, RESULTS_PER_PAGE, currentPage);
		long queryTime = System.currentTimeMillis();
		RestPagedResultDTO<RestPlantillasJobItemDTO> resultDto = this.dtoFactory.convertResults(results);
		long resultTime = System.currentTimeMillis();

		if (logger.isDebugEnabled()) {
			logger.debug("paremeters: lastModified > "+lastModifiedText+", page = "+currentPage+", addressId = "+addressIdText);
			logger.debug("resultsCount: "+results.getResultsCount());
			logger.debug("t(query): "+String.valueOf(queryTime-startTime)+" ms");
			logger.debug("t(results): "+String.valueOf(resultTime-queryTime)+" ms");
			logger.debug("t(total): "+String.valueOf(resultTime-startTime)+" ms");
		}

		return resultDto;
	}

	/**
	 * Returns data for a specific single job item ID
	 * @param jobitemId
	 * @return
	 */
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value={REQUEST_MAPPING}, method = RequestMethod.GET, params={REQUEST_PARAM_ID},
		produces = {Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
	public RestPagedResultDTO<RestPlantillasJobItemDTO> searchByJobItemId(
			@RequestParam(value=REQUEST_PARAM_ID, required=true) int jobitemId) {
		long startTime = System.currentTimeMillis();
		JobItemPlantillasDTO dto = this.jobItemService.getPlantillasJobItem(jobitemId, super.allocatedAddressId);
		long queryTime = System.currentTimeMillis();
		
		RestPagedResultDTO<RestPlantillasJobItemDTO> result = this.dtoFactory.convertSingle(jobitemId, dto);  
		long resultTime = System.currentTimeMillis();

		if (logger.isDebugEnabled()) {
			logger.debug("searching jobitemId = "+jobitemId);
			logger.debug("t(results): "+String.valueOf(resultTime-queryTime)+" ms");
			logger.debug("t(total): "+String.valueOf(resultTime-startTime)+" ms");
		}
		return result;
	}	
}
