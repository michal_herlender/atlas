package org.trescal.cwms.rest.plantillas.dto.output;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelPlantillasDTO;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.tools.DateTools;

@Component
public class RestPlantillasInstrumentModelDTOFactoryImpl 
	extends AbstractRestPlantillasDTOFactory<InstrumentModelPlantillasDTO, RestPlantillasInstrumentModelDTO>
	implements RestPlantillasInstrumentModelDTOFactory {

	@Override
	public RestPlantillasInstrumentModelDTO convert(InstrumentModelPlantillasDTO queryDto) {
		RestPlantillasInstrumentModelDTO restDto = new RestPlantillasInstrumentModelDTO();
		restDto.setDeleted(false);
		restDto.setLastModified(queryDto.getLastModifiedInstrumentModel());
		// References
		restDto.setModelid(queryDto.getModelId());
		restDto.setSubfamilyid(queryDto.getDescriptionId());
		// Identification
		restDto.setCanSelect(queryDto.getCanSelect());
		restDto.setManufacturerSpecific(getManufacturerSpecific(queryDto.getModelMfrType()));
		restDto.setManufacturer(queryDto.getModelMfr());
		restDto.setModel(queryDto.getModel());
		restDto.setSalesCategory(queryDto.getSalesCategory());
		restDto.setDomain(queryDto.getDomain());
		restDto.setFamily(queryDto.getFamily());
		restDto.setSubFamily(queryDto.getSubFamily());
		restDto.setType(queryDto.getType());
		
		return restDto;
	}
	
	/**
	 * Returns the latest of any of the lastModified dates
	 * The contact lastModified date may be null if there is no default contact. 
	 */
	public Date getLatestLastModified(InstrumentModelPlantillasDTO dto) {
		Date result = dto.getLastModifiedInstrumentModel();
		result = DateTools.getLatest(result, dto.getLastModifiedDescription());
		result = DateTools.getLatest(result, dto.getLastModifiedDomain());
		result = DateTools.getLatest(result, dto.getLastModifiedFamily());
		result = DateTools.getLatest(result, dto.getLastModifiedSalesCategory());
		return result;
	}
	
	private boolean getManufacturerSpecific(ModelMfrType modelMfrType) {
		return ModelMfrType.MFR_SPECIFIC.equals(modelMfrType);
	}
}
