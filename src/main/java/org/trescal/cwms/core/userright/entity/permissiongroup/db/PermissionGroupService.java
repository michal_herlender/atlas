package org.trescal.cwms.core.userright.entity.permissiongroup.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;

public interface PermissionGroupService extends BaseService<PermissionGroup, Integer> {
	
	public boolean remove(Integer id);
}