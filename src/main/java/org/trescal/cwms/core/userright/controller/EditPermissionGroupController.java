package org.trescal.cwms.core.userright.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;
import org.trescal.cwms.core.userright.entity.permissiongroup.db.PermissionGroupService;
import org.trescal.cwms.core.userright.enums.Permission;

@Controller
@IntranetController
public class EditPermissionGroupController {

	@Autowired
	private PermissionGroupService permissionGroupService;

	@ModelAttribute("permissionGroup")
	public PermissionGroup permissionGroup(@RequestParam(name = "groupId", required = true) Integer groupId) {
		return permissionGroupService.get(groupId);
	}
	
	@ModelAttribute("permissionDto")
	public List<PermissionDto> permissionsDto(@RequestParam(name = "groupId", required = true) Integer groupId) {
		PermissionGroup perm = permissionGroupService.get(groupId);
		List<PermissionDto> dto = new ArrayList<PermissionDto>();
		for(Permission permission : Permission.values())
		{
			boolean owned=false;
			if(perm.getPermissions().contains(permission))
				owned=true;
			dto.add(new PermissionDto(permission.name(), permission.getArea(), permission.getUrl(), permission.getDescription(), owned));
		}
		return dto;
	}

	@RequestMapping(value = "/editpermissiongroup.htm", method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('PERMISSION_GROUPS_EDIT')")
	public String onRequest() {
		return "trescal/core/userright/editpermissiongroup";
	}

	@RequestMapping(value = "/editpermissiongroup.htm", method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('PERMISSION_GROUPS_EDIT')")
	public String onSubmit(@Validated @ModelAttribute("permissionGroup") PermissionGroup permissionGroup,
			BindingResult bindingResult) {
		if(!bindingResult.hasErrors())
			permissionGroupService.merge(permissionGroup);
		return "trescal/core/userright/editpermissiongroup";
	}
}