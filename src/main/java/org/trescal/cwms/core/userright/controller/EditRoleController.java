package org.trescal.cwms.core.userright.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;
import org.trescal.cwms.core.userright.entity.permissiongroup.db.PermissionGroupService;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;
import org.trescal.cwms.core.userright.entity.role.db.RoleService;

@Controller
@IntranetController
public class EditRoleController {

	@Autowired
	private PermissionGroupService groupService;
	@Autowired
	private RoleService roleService;
	
	@ModelAttribute("role")
	public UserRightRole getRole(@RequestParam(name="roleId", required=true) Integer roleId) {
		return roleService.get(roleId);
	}
	
	@ModelAttribute("groups")
	public List<PermissionGroup> getPermissionGroups() {
		return groupService.getAll();
	}
	
	@RequestMapping(value = "/editrole.htm", method = RequestMethod.GET)
	public String onRequest() {
		return "trescal/core/userright/editrole";
	}
	
	@RequestMapping(value = "/editrole.htm", method = RequestMethod.POST)
	public String onSubmit(@Validated @ModelAttribute("role") UserRightRole role,
			BindingResult bindingResult) {
		return "trescal/core/userright/editpermissiongroup";
	}
	
	@RequestMapping(value = "/editrole.htm/deleterole", method = RequestMethod.POST)
	public String deleteRole(@RequestParam(value="roleId",required = true) int roleId) {
		roleService.removeRole(roleId);
		return "redirect:/userroles.htm";
	}
}