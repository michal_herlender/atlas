package org.trescal.cwms.core.userright.entity.permissiongroup.db;


import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup_;

@Repository
public class PermissionGroupDaoImpl extends BaseDaoImpl<PermissionGroup, Integer> implements PermissionGroupDao {

	@Override
	protected Class<PermissionGroup> getEntity() {
		return PermissionGroup.class;
	}

	@Override
	public Boolean exist(String groupName) {
		
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<PermissionGroup> permissionGroup = cq.from(PermissionGroup.class);
			cq.where(cb.equal(permissionGroup.get(PermissionGroup_.name), groupName));
			return cq.select(cb.count(permissionGroup));
		}) > 0;
	}

	@Override
	public Integer getId(String groupName) {
		return getSingleResult(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<PermissionGroup> permissionGroup = cq.from(PermissionGroup.class);
			cq.where(cb.equal(permissionGroup.get(PermissionGroup_.name), groupName));
			return cq.select(permissionGroup.get(PermissionGroup_.id));
		});		
	}
}