package org.trescal.cwms.core.userright.entity.role;

import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;

@Entity
@Table(name = "perrole")
public class UserRightRole {

	private Integer id;
	private String name;
	private Set<Translation> descriptionTranslations;
	private List<PermissionGroup> groups;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Size(min = 1)
	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "perroletranslation", joinColumns = @JoinColumn(name = "perrole"), foreignKey = @ForeignKey(name = "FK_perroletranslation_perrole"))
	public Set<Translation> getDescriptionTranslations() {
		return descriptionTranslations;
	}

	public void setDescriptionTranslations(Set<Translation> descriptionTranslations) {
		this.descriptionTranslations = descriptionTranslations;
	}

	@ManyToMany
	@JoinTable(name = "rolegroup", joinColumns = @JoinColumn(name = "roleId", foreignKey = @ForeignKey(name = "FK_rolegroup_role")), inverseJoinColumns = @JoinColumn(name = "groupId", foreignKey = @ForeignKey(name = "FK_rolegroup_group")))
	public List<PermissionGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<PermissionGroup> groups) {
		this.groups = groups;
	}
}