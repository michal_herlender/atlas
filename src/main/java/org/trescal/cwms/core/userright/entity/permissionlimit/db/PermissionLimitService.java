package org.trescal.cwms.core.userright.entity.permissionlimit.db;


import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit;

public interface PermissionLimitService extends BaseService<PermissionLimit,Integer>{
	

}
