package org.trescal.cwms.core.userright.entity.userrole.db;

import java.util.List;
import java.util.SortedSet;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.userright.dto.UserRoleDTO;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;
import org.trescal.cwms.core.userright.enums.Permission;

public interface UserRoleDao extends BaseDao<UserRole, Integer> {

	List<UserRoleDTO> findUserRoles(User user);
	
	SortedSet<Permission> getAllPermission(org.trescal.cwms.core.userright.entity.userrole.UserRole userRole);

	public List<SubdivKeyValue> getUserRoleSubdivs(User user);
	
}