package org.trescal.cwms.core.userright.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;
import org.trescal.cwms.core.userright.entity.permissiongroup.db.PermissionGroupService;
import org.trescal.cwms.core.userright.enums.Permission;

@Controller
@JsonController
public class PermissionController {

	@Autowired
	private PermissionGroupService permissionGroupService;

	@RequestMapping("addPermission.json")
	public ModelAndView addPermission(@RequestParam(name = "permission", required = true) Permission permission,
			@RequestParam(name = "groupId", required = true) Integer groupId) {
		PermissionGroup permissionGroup = permissionGroupService.get(groupId);
		if (!permissionGroup.getPermissions().contains(permission)) {
			permissionGroup.getPermissions().add(permission);
			permissionGroupService.merge(permissionGroup);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("permission", permission);
			model.put("groupId", groupId);
			return new ModelAndView("trescal/core/userright/permissionline", model);
		} else
			throw new RuntimeException("Don't add a permission twice!");
	}

	@RequestMapping("removePermission.json")
	@ResponseBody
	public void removePermission(@RequestParam(name = "permission", required = true) Permission permission,
			@RequestParam(name = "groupId", required = true) Integer groupId) {
		PermissionGroup permissionGroup = permissionGroupService.get(groupId);
		if (permissionGroup.getPermissions().contains(permission)) {
			permissionGroup.getPermissions().remove(permission);
			permissionGroupService.merge(permissionGroup);
		}
	}

	@RequestMapping("editPermissionGroupName.json")
	@ResponseBody
	public void editPermissionGroupName(@RequestParam(name = "groupId", required = true) Integer groupId,
			@RequestParam(name = "groupName", required = true) String groupName) {
		PermissionGroup group = permissionGroupService.get(groupId);
		group.setName(groupName);
		permissionGroupService.merge(group);
	}
}