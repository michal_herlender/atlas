package org.trescal.cwms.core.userright.entity.role.db;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;

@Service
public class RoleServiceImpl extends BaseServiceImpl<UserRightRole, Integer> implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Override
	protected BaseDao<UserRightRole, Integer> getBaseDao() {
		return roleDao;
	}

	@Override
	public List<String> getAllRoleName() {
		List<String> lst = new ArrayList<String>();
		this.getAll().forEach(x->lst.add(x.getName()));
		return lst;
	}

	@Override
	public Boolean addRole(String roleName) {
		if(!roleDao.exist(roleName)) {
			UserRightRole userRightRole = new UserRightRole();
			userRightRole.setName(roleName);
			getBaseDao().persist(userRightRole);
			return true;
		}
		return false;
	}

	@Override
	public void removeRole(int roleId) {
		UserRightRole userRightRole = roleDao.find(roleId);
		getBaseDao().remove(userRightRole);	
	}
}