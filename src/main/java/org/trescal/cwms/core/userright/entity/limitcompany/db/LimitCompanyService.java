package org.trescal.cwms.core.userright.entity.limitcompany.db;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.userright.dto.PoLimitConfig;
import org.trescal.cwms.core.userright.entity.limitcompany.LimitCompany;
import org.trescal.cwms.spring.model.KeyValue;

public interface LimitCompanyService extends BaseService<LimitCompany, Integer> {

	/**
	 * Check permission and then check if the contact is under limit expense for his
	 * purchase order
	 *
	 * @param con {@link Contact} who want to approved or constructed this po
	 * @param po  {@link PurchaseOrder} to check different CAPEX GENEX AND OPEX
	 *            expense
	 * @return {@link boolean} : true = Under limit and has right, false = not under
	 *         limit or has not right
	 */
	public boolean getPosUnderLimit(PurchaseOrder po, PoLimitConfig poLimitConfig, Company company);

	/**
	 * Check every {@link PurchaseOrder} which an user has right do a status update
	 *
	 * @param List {@link PurchaseOrder} to check which one can be update by an user
	 * @param con  {@link Contact} who want to update status
	 * @return List of {@link PurchaseOrder} which are in the limit
	 */
	public List<Integer> getPosUnderLimit(List<PurchaseOrder> pos, PoLimitConfig poLimitConfig, Company company);

	/**
	 * get limit from an user on a {@link PurchaseOrder}
	 *
	 * @param con {@link Contact} which we want to check his limits
	 * @param po  {@link PurchaseOrder} reference right
	 * @return {@link PoLimitConfig} stock every limit that an user has
	 */
	public PoLimitConfig getPoUserConfigLimit(Collection<? extends GrantedAuthority> auth, Company comp);

	List<Integer> getQuotesDTOUnderLimit(Collection<QuotationProjectionDTO> collection, Collection<? extends GrantedAuthority> auth,
			Company comp);

	KeyValue<Boolean, BigDecimal> getQuotationLimit(Collection<? extends GrantedAuthority> auth,
			Company comp);

	Boolean getQuotationUnderlimit(int quoteCompanyId, BigDecimal quoteFinalCost, Collection<? extends GrantedAuthority> auth, Company comp);
	
	public List<Integer> getPosUnderLimit(List<PurchaseOrder> pos, PoLimitConfig poLimitConfig, Integer companyId);
	
	public PoLimitConfig getPoUserConfigLimit(Collection<? extends GrantedAuthority> auth, Integer companyId);
}
