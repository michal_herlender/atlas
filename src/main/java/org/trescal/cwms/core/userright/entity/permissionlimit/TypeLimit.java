package org.trescal.cwms.core.userright.entity.permissionlimit;

public enum TypeLimit {

    PURCHASE_OPEX,
    PURCHASE_GENEX,
    PURCHASE_CAPEX,
    QUOTATION;
}