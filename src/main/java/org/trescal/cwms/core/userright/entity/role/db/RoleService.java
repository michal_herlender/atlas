package org.trescal.cwms.core.userright.entity.role.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;

public interface RoleService extends BaseService<UserRightRole, Integer> {
	
	public List<String> getAllRoleName();
	
	public Boolean addRole(String roleName);

	public void removeRole(int roleId);

}