package org.trescal.cwms.core.userright.entity.limitcompany.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.account.NominalCodeType;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.MultiCurrencyUtils;
import org.trescal.cwms.core.userright.dto.PoLimitConfig;
import org.trescal.cwms.core.userright.entity.limitcompany.LimitCompany;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit;
import org.trescal.cwms.core.userright.entity.permissionlimit.TypeLimit;
import org.trescal.cwms.core.userright.entity.permissionlimit.db.PermissionLimitDao;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class LimitCompanyServiceImpl extends BaseServiceImpl<LimitCompany, Integer> implements LimitCompanyService {
	@Autowired
	private LimitCompanyDao limitCompanyDao;
	@Autowired
	private PermissionLimitDao permissionLimitDao;

	@Override
	protected BaseDao<LimitCompany, Integer> getBaseDao() {
		return this.limitCompanyDao;
	}

	private Boolean getPoUnderPurchaseLimit(PurchaseOrder po, PoLimitConfig poLimitConfig) {
		// Calculate final price by purchase type
		// OPEX
		BigDecimal poOpexFinalPrice = new BigDecimal(0);
		for (PurchaseOrderItem item : po.getItems().stream().filter(x -> x.getNominal() != null)
				.filter(x -> x.getNominal().getNominalCodeType() == NominalCodeType.OPEX).collect(Collectors.toList()))
			poOpexFinalPrice = poOpexFinalPrice.add(item.getFinalCost());
		// CAPEX
		BigDecimal poCapexFinalPrice = new BigDecimal(0);
		for (PurchaseOrderItem item : po.getItems().stream().filter(x -> x.getNominal() != null)
				.filter(x -> x.getNominal().getNominalCodeType() == NominalCodeType.CAPEX).collect(Collectors.toList()))
			poCapexFinalPrice = poCapexFinalPrice.add(item.getFinalCost());
		// GENEX
		BigDecimal poGenexFinalPrice = new BigDecimal(0);
		for (PurchaseOrderItem item : po.getItems().stream().filter(x -> x.getNominal() != null)
				.filter(x -> x.getNominal().getNominalCodeType() == NominalCodeType.GENEX).collect(Collectors.toList()))
			poGenexFinalPrice = poGenexFinalPrice.add(item.getFinalCost());

		// Verification and convert sum of money if currency is different
		if (po.getCurrency() != po.getOrganisation().getCurrency()) {
			// Get business company's currency
			SupportedCurrency sourceCurrency = po.getCurrency();
			// Get required currency
			SupportedCurrency requiredCurrency = po.getOrganisation().getCurrency();
			// update value
			poOpexFinalPrice = MultiCurrencyUtils.convertCurrencyValue(poOpexFinalPrice, sourceCurrency,
				requiredCurrency);
			poCapexFinalPrice = MultiCurrencyUtils.convertCurrencyValue(poCapexFinalPrice, sourceCurrency,
				requiredCurrency);
			poGenexFinalPrice = MultiCurrencyUtils.convertCurrencyValue(poGenexFinalPrice, sourceCurrency,
				requiredCurrency);
		}
		return (poLimitConfig.getOpexRightMax().compareTo(poOpexFinalPrice) >= 0 || poLimitConfig.isOpexLimitless())
			&& (poLimitConfig.getCapexRightMax().compareTo(poCapexFinalPrice) >= 0
			|| poLimitConfig.isCapexLimitless())
			&& (poLimitConfig.getGenexRightMax().compareTo(poGenexFinalPrice) >= 0
			|| poLimitConfig.isGenexLimitless());
	}

	@Override
	public PoLimitConfig getPoUserConfigLimit(Collection<? extends GrantedAuthority> auth, Company comp) {
		PoLimitConfig poLimitConfig = new PoLimitConfig();
		// Get Purchase limit permission
		List<PermissionLimit> refRights = Stream
				.of(permissionLimitDao.getPermissionLimitFromType(TypeLimit.PURCHASE_CAPEX),
						permissionLimitDao.getPermissionLimitFromType(TypeLimit.PURCHASE_OPEX),
						permissionLimitDao.getPermissionLimitFromType(TypeLimit.PURCHASE_GENEX))
				.flatMap(Collection::stream).collect(Collectors.toList());
		List<PermissionLimit> userLimitPermission = getLimitRight(auth, refRights);

		for (PermissionLimit perm : userLimitPermission) {
			if (perm.getType() == TypeLimit.PURCHASE_CAPEX)
				poLimitConfig.setCapexRightMax(limitCompanyDao.getMaximumFromComp(perm.getPermission(), comp));
			if (perm.getType() == TypeLimit.PURCHASE_OPEX)
				poLimitConfig.setOpexRightMax(limitCompanyDao.getMaximumFromComp(perm.getPermission(), comp));
			if (perm.getType() == TypeLimit.PURCHASE_GENEX)
				poLimitConfig.setGenexRightMax(limitCompanyDao.getMaximumFromComp(perm.getPermission(), comp));
		}

		poLimitConfig.setCapexLimitless(auth.contains(Permission.PURCHASE_ORDER_CAPEX_LIMITLESS));
		poLimitConfig.setGenexLimitless(auth.contains(Permission.PURCHASE_ORDER_GENEX_LIMITLESS));
		poLimitConfig.setOpexLimitless(auth.contains(Permission.PURCHASE_ORDER_OPEX_LIMITLESS));
		return poLimitConfig;
	}

	@Override
	public boolean getPosUnderLimit(PurchaseOrder po, PoLimitConfig poLimitConfig, Company company) {
		if (po.getOrganisation().getCoid() == company.getCoid())
			return getPoUnderPurchaseLimit(po, poLimitConfig);
		return false;
	}

	@Override
	public List<Integer> getPosUnderLimit(List<PurchaseOrder> pos, PoLimitConfig poLimitConfig, Company company) {
		List<Integer> posId = new ArrayList<>();
		for (PurchaseOrder po : pos)
			if (po.getOrganisation().getCoid() == company.getCoid() && getPoUnderPurchaseLimit(po, poLimitConfig))
				posId.add(po.getId());
		return posId;
	}

	@Override
	public List<Integer> getQuotesDTOUnderLimit(Collection<QuotationProjectionDTO> quotes, Collection<? extends GrantedAuthority> auth,
			Company comp) {
		List<Integer> quotesId = new ArrayList<>();
		for (QuotationProjectionDTO quote : quotes)
			if (quote.getOrganisationId() == comp.getCoid() && getQuotationUnderlimit(quote.getOrganisationId(), quote.getFinalCost(), auth, comp))
				quotesId.add(quote.getId());
		return quotesId;
	}

	@Override
	public Boolean getQuotationUnderlimit(int quoteCompanyId, BigDecimal quoteFinalCost, Collection<? extends GrantedAuthority> auth,
			Company comp) {
		if (quoteCompanyId != comp.getCoid())
			return false;
		if (auth.contains(Permission.QUOTATION_CUSTOMER_LIMITLESS_LIMIT))
			return true;
		List<PermissionLimit> refRights = permissionLimitDao.getPermissionLimitFromType(TypeLimit.QUOTATION);
		List<PermissionLimit> rightOwn = getLimitRight(auth, refRights);
		if (!rightOwn.isEmpty()) {
			PermissionLimit perm = rightOwn.get(0);
			BigDecimal maxRightValue = limitCompanyDao.getMaximumFromComp(perm.getPermission(), comp);
			return maxRightValue.compareTo(quoteFinalCost) >= 0;
		}
		return false;
	}

	@Override
	public KeyValue<Boolean, BigDecimal> getQuotationLimit(Collection<? extends GrantedAuthority> auth, Company comp) {
		List<PermissionLimit> refRights = permissionLimitDao.getPermissionLimitFromType(TypeLimit.QUOTATION);
		List<PermissionLimit> rightOwn = getLimitRight(auth, refRights);

		Boolean limitless = auth.contains(Permission.QUOTATION_CUSTOMER_LIMITLESS_LIMIT);
		BigDecimal value = new BigDecimal("0.00");

		if (rightOwn.size() > 0)
			value = limitCompanyDao.getMaximumFromComp(rightOwn.get(0).getPermission(), comp);
		return new KeyValue<>(limitless, value);
	}

	/**
	 * Checks if the given {@link Contact} has one of the given rights for the
	 * Subdiv.
	 * 
	 * @return true if the {@link Contact} has the permission, false otherwise.
	 */
	private List<PermissionLimit> getLimitRight(Collection<? extends GrantedAuthority> auth,
			List<PermissionLimit> refRights) {
		List<PermissionLimit> userRight = new ArrayList<>();
		// Algorithm that get best purchase permission of one contact for a subdiv
		if ((refRights != null)) {
			for (PermissionLimit right : refRights)
				if (auth.contains(right.getPermission())) {
					isUpperLimitLevel(right, userRight);
				}
		}
		return userRight;
	}

	private void isUpperLimitLevel(PermissionLimit target, List<PermissionLimit> currentRights) {
		PermissionLimit currentRight = currentRights.stream().filter(x -> x.getType().equals(target.getType()))
			.findFirst().orElse(null);
		if (currentRight == null)
			currentRights.add(target);
		else if (target.getLevel() > currentRight.getLevel()) {
			currentRights.remove(currentRight);
			currentRights.add(target);
		}
	}

	@Override
	public List<Integer> getPosUnderLimit(List<PurchaseOrder> pos, PoLimitConfig poLimitConfig, Integer companyId) {
		List<Integer> posId = new ArrayList<>();
		for (PurchaseOrder po : pos)
			if (po.getOrganisation().getCoid() == companyId && getPoUnderPurchaseLimit(po, poLimitConfig))
				posId.add(po.getId());
		return posId;
	}

	@Override
	public PoLimitConfig getPoUserConfigLimit(Collection<? extends GrantedAuthority> auth, Integer companyId) {
		PoLimitConfig poLimitConfig = new PoLimitConfig();
		// Get Purchase limit permission
		List<PermissionLimit> refRights = Stream
				.of(permissionLimitDao.getPermissionLimitFromType(TypeLimit.PURCHASE_CAPEX),
						permissionLimitDao.getPermissionLimitFromType(TypeLimit.PURCHASE_OPEX),
						permissionLimitDao.getPermissionLimitFromType(TypeLimit.PURCHASE_GENEX))
				.flatMap(Collection::stream).collect(Collectors.toList());
		List<PermissionLimit> userLimitPermission = getLimitRight(auth, refRights);

		for (PermissionLimit perm : userLimitPermission) {
			if (perm.getType() == TypeLimit.PURCHASE_CAPEX)
				poLimitConfig.setCapexRightMax(limitCompanyDao.getMaximumFromComp(perm.getPermission(), companyId));
			if (perm.getType() == TypeLimit.PURCHASE_OPEX)
				poLimitConfig.setOpexRightMax(limitCompanyDao.getMaximumFromComp(perm.getPermission(), companyId));
			if (perm.getType() == TypeLimit.PURCHASE_GENEX)
				poLimitConfig.setGenexRightMax(limitCompanyDao.getMaximumFromComp(perm.getPermission(), companyId));
		}

		poLimitConfig.setCapexLimitless(auth.contains(Permission.PURCHASE_ORDER_CAPEX_LIMITLESS));
		poLimitConfig.setGenexLimitless(auth.contains(Permission.PURCHASE_ORDER_GENEX_LIMITLESS));
		poLimitConfig.setOpexLimitless(auth.contains(Permission.PURCHASE_ORDER_OPEX_LIMITLESS));
		return poLimitConfig;
	}
	
}
