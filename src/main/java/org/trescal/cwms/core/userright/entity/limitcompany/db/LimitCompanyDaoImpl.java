package org.trescal.cwms.core.userright.entity.limitcompany.db;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.userright.entity.limitcompany.LimitCompany;
import org.trescal.cwms.core.userright.entity.limitcompany.LimitCompany_;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit_;
import org.trescal.cwms.core.userright.entity.permissionlimit.TypeLimit;
import org.trescal.cwms.core.userright.enums.Permission;


@Repository
public class LimitCompanyDaoImpl extends AllocatedToCompanyDaoImpl<LimitCompany, Integer>  implements LimitCompanyDao {

	@Override
	protected Class<LimitCompany> getEntity() {
		return LimitCompany.class;
	}
	
	@Override
	public BigDecimal getMaximumFromComp(Permission permission,Company company) {
		return getSingleResult(cb -> {
			CriteriaQuery<BigDecimal> cq = cb.createQuery(BigDecimal.class);
			Root<LimitCompany> limitCompany = cq.from(LimitCompany.class);
			Join<LimitCompany,PermissionLimit> permLimit = limitCompany.join(LimitCompany_.permissionLimit);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(limitCompany.get(LimitCompany_.organisation),company));
			clauses.getExpressions().add(cb.equal(permLimit.get(PermissionLimit_.permission),permission));
			cq.where(clauses);
			cq.select(limitCompany.get(LimitCompany_.maximum));
			return cq;
		});
	}
	public List<LimitCompany> getAllByCompAndType(Company company,TypeLimit typeLimit) {
		return getResultList(cb ->{
			CriteriaQuery<LimitCompany> cq = cb.createQuery(LimitCompany.class);
			Root<LimitCompany> limitCompany = cq.from(LimitCompany.class);
			Join<LimitCompany,PermissionLimit> permLimit = limitCompany.join(LimitCompany_.permissionLimit);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(limitCompany.get(LimitCompany_.organisation),company));
			clauses.getExpressions().add(cb.equal(permLimit.get(PermissionLimit_.type),typeLimit));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public BigDecimal getMaximumFromComp(Permission permission, Integer companyId) {
		return getSingleResult(cb -> {
			CriteriaQuery<BigDecimal> cq = cb.createQuery(BigDecimal.class);
			Root<LimitCompany> limitCompany = cq.from(LimitCompany.class);
			Join<LimitCompany,PermissionLimit> permLimit = limitCompany.join(LimitCompany_.permissionLimit);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(limitCompany.get(LimitCompany_.organisation),companyId));
			clauses.getExpressions().add(cb.equal(permLimit.get(PermissionLimit_.permission),permission));
			cq.where(clauses);
			cq.select(limitCompany.get(LimitCompany_.maximum));
			return cq;
		});
	}
}
