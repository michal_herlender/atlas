package org.trescal.cwms.core.userright.entity.permissiongroup.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;

public interface PermissionGroupDao extends BaseDao<PermissionGroup, Integer> {
	
	public Boolean exist(String GroupName);
	
	public Integer getId(String GroupName);
	
}