package org.trescal.cwms.core.userright.entity.permissiongroup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;

@Service
public class PermissionGroupServiceImpl extends BaseServiceImpl<PermissionGroup, Integer> implements PermissionGroupService {

	@Autowired
	private PermissionGroupDao permissionGroupDao;
	
	@Override
	protected BaseDao<PermissionGroup, Integer> getBaseDao() {
		return permissionGroupDao;
	}

	@Override
	public boolean remove(Integer id) {
		try {
			PermissionGroup permissionGroup = permissionGroupDao.find(id);
			permissionGroupDao.remove(permissionGroup);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
}