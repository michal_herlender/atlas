package org.trescal.cwms.core.userright.entity.permissionlimit.db;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit;

@Service
public class PermissionLimitServiceImpl extends BaseServiceImpl<PermissionLimit, Integer>
		implements PermissionLimitService {

	@Autowired
	PermissionLimitDao permissionLimitDao;
	@Override
	protected BaseDao<PermissionLimit, Integer> getBaseDao() {
		return permissionLimitDao;
	}




}
