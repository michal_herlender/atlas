package org.trescal.cwms.core.userright.entity.userrole;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;

@Entity(name = "userroles")
@Table(name = "userrole", uniqueConstraints = @UniqueConstraint(columnNames = { "userName",
		"role", "orgid" }, name = "UK_userrole_userName_role_orgid"))
public class UserRole extends Allocated<Subdiv> {

	private Integer id;
	private User user;
	private UserRightRole role;

	public UserRole() {
	}

	public UserRole(User user, UserRightRole role, Subdiv allocatedSubdiv) {
		this.user = user;
		this.role = role;
		this.setOrganisation(allocatedSubdiv);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userName", nullable = false, foreignKey = @ForeignKey(name = "FK_userrole_userName"))
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role", nullable = false, foreignKey = @ForeignKey(name = "FK_userrole_role"))
	public UserRightRole getRole() {
		return role;
	}

	public void setRole(UserRightRole role) {
		this.role = role;
	}
}