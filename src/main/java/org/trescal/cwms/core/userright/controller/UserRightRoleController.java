package org.trescal.cwms.core.userright.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;
import org.trescal.cwms.core.userright.entity.permissiongroup.db.PermissionGroupService;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;
import org.trescal.cwms.core.userright.entity.role.db.RoleService;

@Controller
@JsonController
public class UserRightRoleController {

	@Autowired
	private PermissionGroupService groupService;
	@Autowired
	private RoleService roleService;

	@RequestMapping("addGroupToRole.json")
	public ModelAndView addUserRightRole(@RequestParam(name = "roleId", required = true) Integer roleId,
			@RequestParam(name = "groupId", required = true) Integer groupId) {
		UserRightRole role = roleService.get(roleId);
		PermissionGroup group = groupService.get(groupId);
		if (!role.getGroups().contains(group)) {
			role.getGroups().add(group);
			role = roleService.merge(role);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("group", group);
			model.put("roleId", roleId);
			return new ModelAndView("trescal/core/userright/permissiongroupline", model);
		} else
			throw new RuntimeException("Don't add a permission group twice!");
	}

	@RequestMapping("removeGroupFromRole.json")
	@ResponseBody
	public void removePermission(@RequestParam(name = "groupId", required = true) Integer groupId,
			@RequestParam(name = "roleId", required = true) Integer roleId) {
		UserRightRole role = roleService.get(roleId);
		PermissionGroup permissionGroup = groupService.get(groupId);
		if (role.getGroups().contains(permissionGroup)) {
			role.getGroups().remove(permissionGroup);
			roleService.merge(role);
		}
	}

	@RequestMapping("editUserRightRoleName.json")
	@ResponseBody
	public void editUserRightRoleName(@RequestParam(name = "roleId", required = true) Integer roleId,
			@RequestParam(name = "roleName", required = true) String roleName) {
		UserRightRole role = roleService.get(roleId);
		role.setName(roleName);
		roleService.merge(role);
	}
}