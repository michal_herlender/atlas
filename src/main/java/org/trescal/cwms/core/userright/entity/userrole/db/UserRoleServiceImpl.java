package org.trescal.cwms.core.userright.entity.userrole.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.userright.dto.UserRoleDTO;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;

@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRole, Integer> implements UserRoleService {

	@Autowired
	private UserRoleDao userRoleDao;

	@Override
	protected BaseDao<UserRole, Integer> getBaseDao() {
		return userRoleDao;
	}

	@Override
	public List<UserRoleDTO> findUserRoles(User user) {
		return userRoleDao.findUserRoles(user);
	}

	@Override
	public boolean remove(Integer id) {
		try {
			UserRole userRole = userRoleDao.find(id);
			userRoleDao.remove(userRole);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
}