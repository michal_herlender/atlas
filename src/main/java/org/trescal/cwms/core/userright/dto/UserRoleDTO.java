package org.trescal.cwms.core.userright.dto;

public class UserRoleDTO {

	private Integer userRoleId;
	private String roleName;
	private String companyName;
	private String subdivName;

	public UserRoleDTO() {
	}

	public UserRoleDTO(Integer userRoleId, String roleName, String companyName, String subdivName) {
		this.userRoleId = userRoleId;
		this.roleName = roleName;
		this.companyName = companyName;
		this.subdivName = subdivName;
	}

	public Integer getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getSubdivName() {
		return subdivName;
	}

	public void setSubdivName(String subdivName) {
		this.subdivName = subdivName;
	}
}