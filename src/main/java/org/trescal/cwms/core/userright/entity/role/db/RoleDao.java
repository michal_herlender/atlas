package org.trescal.cwms.core.userright.entity.role.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;

public interface RoleDao extends BaseDao<UserRightRole, Integer> {
	
	public UserRightRole get(String roleName);
	
	public Boolean exist(String roleName);

	public Integer getId(String roleName);
	
}