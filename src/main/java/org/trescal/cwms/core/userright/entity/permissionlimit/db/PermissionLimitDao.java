package org.trescal.cwms.core.userright.entity.permissionlimit.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit;
import org.trescal.cwms.core.userright.entity.permissionlimit.TypeLimit;
import org.trescal.cwms.core.userright.enums.Permission;

public interface PermissionLimitDao extends BaseDao<PermissionLimit, Integer> {
		
	public List<Permission> getPermissionFromType(TypeLimit type);

	public List<PermissionLimit> getPermissionLimitFromType(TypeLimit type);

	PermissionLimit find(Permission permission);
	
	
}
