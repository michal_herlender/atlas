package org.trescal.cwms.core.userright.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;
import org.trescal.cwms.core.userright.entity.permissiongroup.db.PermissionGroupDao;
import org.trescal.cwms.core.userright.entity.permissiongroup.db.PermissionGroupService;

@Controller
@IntranetController
public class PermissionGroupsController {

	@Autowired
	private PermissionGroupService permissionGroupService;
	@Autowired
	private PermissionGroupDao permissionGroupDao;

	@RequestMapping(value="addpermissiongroup.json")
	public ModelAndView addPermissionGroup() {
		String defaultGroupName="DEFAULT_GROUP";
		if(!permissionGroupDao.exist(defaultGroupName))
		{		
			PermissionGroup permissionGroup = new PermissionGroup();
			permissionGroup.setName(defaultGroupName);
			permissionGroupService.merge(permissionGroup);
			Map<String, Object> model = new HashMap<>();
			model.put("groupName", defaultGroupName);			
			model.put("groupId", permissionGroupDao.getId(defaultGroupName));
			model.put("translation","(no default translation)");
			return new ModelAndView("trescal/core/userright/permissiongroupsline", model);
		} else
			throw new RuntimeException("A default group already exist!");
	}
		
	@ModelAttribute("permissionGroups")
	public List<PermissionGroup> permissionGroups() {
		return permissionGroupService.getAll();
	}

	@PreAuthorize("hasAuthority('PERMISSION_GROUPS')")
	@RequestMapping(value = "/permissiongroups.htm", method = RequestMethod.GET)
	public String onRequest() {
		return "trescal/core/userright/permissiongroups";
	}
		
	@PreAuthorize("hasAuthority('PERMISSION_GROUPS')")
	@RequestMapping(value = "/permissiongroups.htm/delete", method = RequestMethod.POST)
	public String onRequestDelete(@RequestParam(name = "groupId", required = true) Integer groupId) {
		permissionGroupService.remove(groupId);
		return "redirect:/permissiongroups.htm";
	}
}