package org.trescal.cwms.core.userright.entity.userrole.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.userright.dto.UserRoleDTO;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;

public interface UserRoleService extends BaseService<UserRole, Integer> {

	List<UserRoleDTO> findUserRoles(User user);
	
	boolean remove(Integer id);
}