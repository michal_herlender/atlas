package org.trescal.cwms.core.userright.controller;

import org.trescal.cwms.core.userright.enums.PermissionDomain;

public class PermissionDto {
	private String name;
	private PermissionDomain area;
	private String url;
	private String description;
	private boolean owned;
	
	public PermissionDto(String name, PermissionDomain area, String url, String description, boolean owned) {
		super();
		this.name = name;
		this.area = area;
		this.url = url;
		this.description = description;
		this.owned = owned;
	}

	public PermissionDto() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PermissionDomain getArea() {
		return area;
	}
	public void setArea(PermissionDomain area) {
		this.area = area;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean getOwned() {
		return owned;
	}
	public void setOwned(boolean owned) {
		this.owned = owned;
	}
	
	
}
