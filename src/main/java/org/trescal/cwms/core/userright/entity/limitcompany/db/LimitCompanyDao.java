package org.trescal.cwms.core.userright.entity.limitcompany.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.userright.entity.limitcompany.LimitCompany;
import org.trescal.cwms.core.userright.entity.permissionlimit.TypeLimit;
import org.trescal.cwms.core.userright.enums.Permission;

public interface LimitCompanyDao extends AllocatedDao<Company, LimitCompany, Integer> {

	java.math.BigDecimal getMaximumFromComp(Permission permission, Company company);
		
	public List<LimitCompany> getAllByCompAndType(Company company,TypeLimit typeLimit);
	
	java.math.BigDecimal getMaximumFromComp(Permission permission, Integer companyId);
}
