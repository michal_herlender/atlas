package org.trescal.cwms.core.userright.controller;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.userright.entity.limitcompany.LimitCompany;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyDao;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;
import org.trescal.cwms.core.userright.entity.permissionlimit.TypeLimit;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class EditPermissionLimitController {

    private CompanyService companyService;
    private LimitCompanyService limitCompanyServ;
    private LimitCompanyDao limitCompanyDao;

    public EditPermissionLimitController(CompanyService companyService, LimitCompanyService limitCompanyServ, LimitCompanyDao limitCompanyDao) {
        this.companyService = companyService;
        this.limitCompanyServ = limitCompanyServ;
        this.limitCompanyDao = limitCompanyDao;
    }

    @ModelAttribute("permissionLimitQuotes")
    public List<LimitCompany> getPermissionLimitQuotes(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
        Company comp = companyService.get(companyDto.getKey());
        return limitCompanyDao.getAllByCompAndType(comp, TypeLimit.QUOTATION);
    }

    @ModelAttribute("permissionLimitPos")
    public List<LimitCompany> getPermissionLimitPos(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
        Company comp = companyService.get(companyDto.getKey());
        List<LimitCompany> refRights = Stream
                .of(limitCompanyDao.getAllByCompAndType(comp, TypeLimit.PURCHASE_CAPEX),
                        limitCompanyDao.getAllByCompAndType(comp, TypeLimit.PURCHASE_GENEX),
                        limitCompanyDao.getAllByCompAndType(comp, TypeLimit.PURCHASE_OPEX))
                .flatMap(Collection::stream).collect(Collectors.toList());
        return refRights;
    }

    @ModelAttribute("companyCurrency")
    public String companyCurrency(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
        Company comp = companyService.get(companyDto.getKey());
        return comp.getCurrency().getCurrencySymbol();
    }

    @RequestMapping(value = "/editpermissionlimit.htm", method = RequestMethod.GET)
    public String onRequest() {
        return "trescal/core/userright/editpermissionlimit";
    }

    @RequestMapping("editmaxpermissionlimit.json")
    @ResponseBody
    public void editPurchaseMaxValue(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                                     @RequestParam(name = "id", required = true) int id, @RequestParam(name = "max", required = true) BigDecimal max) {
        Company comp = companyService.get(companyDto.getKey());
        LimitCompany limitCompany = limitCompanyServ.get(id);
        if (limitCompany != null && limitCompany.getOrganisation().getCoid() == comp.getCoid()) {
            limitCompany.setMaximum(max);
            limitCompanyServ.save(limitCompany);
        }
    }
}