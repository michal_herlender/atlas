package org.trescal.cwms.core.userright.entity.permissionlimit.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit_;
import org.trescal.cwms.core.userright.entity.permissionlimit.TypeLimit;
import org.trescal.cwms.core.userright.enums.Permission;

@Repository
public class PermissionLimitDaoImpl extends BaseDaoImpl<PermissionLimit, Integer>  implements PermissionLimitDao {

	@Override
	protected Class<PermissionLimit> getEntity() {
		return PermissionLimit.class;
	}

	@Override
	public List<Permission> getPermissionFromType(TypeLimit type) {
		return getResultList(cb -> {
			CriteriaQuery<Permission> cq = cb.createQuery(Permission.class);
			Root<PermissionLimit> permissionLimit = cq.from(PermissionLimit.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(permissionLimit.get(PermissionLimit_.type), type));
			cq.where(clauses);
			cq.select(permissionLimit.get(PermissionLimit_.permission));
			return cq;
		});
	}
	
	@Override
	public List<PermissionLimit> getPermissionLimitFromType(TypeLimit type) {
		return getResultList(cb -> {
			CriteriaQuery<PermissionLimit> cq = cb.createQuery(PermissionLimit.class);
			Root<PermissionLimit> permissionLimit = cq.from(PermissionLimit.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(permissionLimit.get(PermissionLimit_.type), type));
			cq.where(clauses);
			return cq;
		});
	}
	
	@Override
	public PermissionLimit find(Permission permission) {
		return getSingleResult(cb -> {
			CriteriaQuery<PermissionLimit> cq = cb.createQuery(PermissionLimit.class);
			Root<PermissionLimit> permissionLimit = cq.from(PermissionLimit.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(permissionLimit.get(PermissionLimit_.permission), permission));
			cq.where(clauses);
			return cq;
		});
	}
}