package org.trescal.cwms.core.userright.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;
import org.trescal.cwms.core.userright.entity.role.db.RoleDao;
import org.trescal.cwms.core.userright.entity.role.db.RoleService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@IntranetController
@Slf4j
public class RolesController {

	@Autowired
	private RoleService roleService;
	@Autowired
	private RoleDao roleDao;
	
	
	@ModelAttribute("roles")
	public List<UserRightRole> roles() {
		List<UserRightRole> roles = roleService.getAll();
		roles.forEach(r -> log.debug(r.getName()));
		return roles;
	}

	@RequestMapping(value = "userroles.htm", method = RequestMethod.GET)
	public String onRequest() {
		return "trescal/core/userright/userroles";
	}
	
	@RequestMapping(value = "adduserrole.json")
	public ModelAndView addUserRole() {
		//Add default role
		String defaultRoleName = "DEFAULT_ROLE";
		if (!roleService.addRole(defaultRoleName))
			throw new RuntimeException("Role already exist");
		//Line Role Inject
		Map<String, Object> model = new HashMap<>();
		model.put("roleId", roleDao.getId(defaultRoleName));
		model.put("roleName", defaultRoleName);
		model.put("roleDesc", "(no default translation)");
		return new ModelAndView("trescal/core/userright/userrightroleline", model);
	}
	
}