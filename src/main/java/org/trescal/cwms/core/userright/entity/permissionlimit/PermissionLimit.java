package org.trescal.cwms.core.userright.entity.permissionlimit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Setter;
import org.trescal.cwms.core.userright.enums.Permission;

@Entity
@Table(name = "permissionlimit")
@Setter
public class PermissionLimit {

    private int id;
    private Permission permission;
    private int level;
    private TypeLimit type;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    @Column(name = "typePermission", nullable = false)
    @Enumerated(EnumType.STRING)
    public TypeLimit getType() {
        return type;
    }

    @Column(name = "permissionName", nullable = false)
    @Enumerated(EnumType.STRING)
    public Permission getPermission() {
        return permission;
    }

    @Column(name = "levelPermission", nullable = false)
    public int getLevel() {
        return level;
    }
}