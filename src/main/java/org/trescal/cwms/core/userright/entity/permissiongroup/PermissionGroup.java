package org.trescal.cwms.core.userright.entity.permissiongroup;

import java.util.Set;
import java.util.SortedSet;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.SortNatural;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.userright.enums.Permission;

@Entity
@Table(name = "permissiongroup")
public class PermissionGroup {

	private Integer id;
	private String name;
	private Set<Translation> descriptionTranslations;
	private SortedSet<Permission> permissions;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Size(min = 1)
	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "permissiongrouptranslation", joinColumns = @JoinColumn(name = "permissiongroup"), foreignKey = @ForeignKey(name = "FK_permissiongrouptranslation_permissiongroup"))
	public Set<Translation> getDescriptionTranslations() {
		return descriptionTranslations;
	}

	public void setDescriptionTranslations(Set<Translation> descriptionTranslations) {
		this.descriptionTranslations = descriptionTranslations;
	}

	@ElementCollection(targetClass = Permission.class)
	@JoinTable(name = "permission", joinColumns = @JoinColumn(name = "groupId"), foreignKey = @ForeignKey(name = "FK_permission_permissiongroup"))
	@Column(name = "permission")
	@Enumerated(EnumType.STRING)
	@SortNatural
	public SortedSet<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(SortedSet<Permission> permissions) {
		this.permissions = permissions;
	}
}