package org.trescal.cwms.core.userright.entity.userrole.db;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.userright.dto.UserRoleDTO;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;
import org.trescal.cwms.core.userright.entity.role.UserRightRole_;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;
import org.trescal.cwms.core.userright.entity.userrole.UserRole_;
import org.trescal.cwms.core.userright.enums.Permission;

@Repository
public class UserRoleDaoImpl extends BaseDaoImpl<UserRole, Integer> implements UserRoleDao {

	@Override
	protected Class<UserRole> getEntity() {
		return UserRole.class;
	}

	@Override
	public List<UserRoleDTO> findUserRoles(User user) {
		return getResultList(cb -> {
			CriteriaQuery<UserRoleDTO> cq = cb.createQuery(UserRoleDTO.class);
			Root<UserRole> userRole = cq.from(UserRole.class);
			Join<UserRole, UserRightRole> role = userRole.join(UserRole_.role);
			Join<UserRole, Subdiv> subdiv = userRole.join("organisation");
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			cq.where(cb.equal(userRole.get(UserRole_.user), user));
			cq.select(cb.construct(UserRoleDTO.class, userRole.get(UserRole_.id), role.get(UserRightRole_.name),
					company.get(Company_.coname), subdiv.get(Subdiv_.subname)));
			cq.orderBy(cb.asc(company.get(Company_.coname)), cb.asc(subdiv.get(Subdiv_.subname)),
					cb.asc(role.get(UserRightRole_.name)));
			return cq;
		});
	}
	// To move in service 
	@Override
	public SortedSet<Permission> getAllPermission(org.trescal.cwms.core.userright.entity.userrole.UserRole userRole) {
		SortedSet<Permission> lst_Permissions = new TreeSet<Permission>();
		for(PermissionGroup Group : userRole.getRole().getGroups()){
			lst_Permissions.addAll(Group.getPermissions());

		}
		return lst_Permissions;
	}

	@Override
	public List<SubdivKeyValue> getUserRoleSubdivs(User user) {
		return getResultList(cb -> {
			CriteriaQuery<SubdivKeyValue> cq = cb.createQuery(SubdivKeyValue.class);
			Root<UserRole> userRole = cq.from(UserRole.class);
			Join<UserRole, Subdiv> subdiv = userRole.join("organisation");
			cq.where(cb.equal(userRole.get(UserRole_.user), user));
			cq.select(cb.construct(SubdivKeyValue.class, subdiv.get(Subdiv_.subdivid),subdiv.get(Subdiv_.subname)));
			return cq;
		});
	}
	
}