package org.trescal.cwms.core.userright.entity.role.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;
import org.trescal.cwms.core.userright.entity.role.UserRightRole_;

@Repository
public class RoleDaoImpl extends BaseDaoImpl<UserRightRole, Integer> implements RoleDao {

	@Override
	protected Class<UserRightRole> getEntity() {
		return UserRightRole.class;
	}

	@Override
	public UserRightRole get(String roleName) {
		return getSingleResult(cb -> {
			CriteriaQuery<UserRightRole> cq = cb.createQuery(UserRightRole.class);
			Root<UserRightRole> role = cq.from(UserRightRole.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(role.get(UserRightRole_.name), roleName));
			cq.where(clauses);
			cq.select(role);
			return cq;
		});
	}

	@Override
	public Boolean exist(String roleName) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<UserRightRole> userRightRole = cq.from(UserRightRole.class);
			cq.where(cb.equal(userRightRole.get(UserRightRole_.name), roleName));
			return cq.select(cb.count(userRightRole));
		}) > 0;
	}

	@Override
	public Integer getId(String roleName) {
		return getSingleResult(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<UserRightRole> role = cq.from(UserRightRole.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(role.get(UserRightRole_.name), roleName));
			cq.where(clauses);
			cq.select(role.get(UserRightRole_.id));
			return cq;
		});
	}
}