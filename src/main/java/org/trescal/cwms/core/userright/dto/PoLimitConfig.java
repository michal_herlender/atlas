package org.trescal.cwms.core.userright.dto;

import java.math.BigDecimal;

public class PoLimitConfig {
	private BigDecimal opexRightMax;
	private boolean opexLimitless;
	private BigDecimal capexRightMax ;
	private boolean capexLimitless ;
	private BigDecimal genexRightMax ;
	private boolean genexLimitless ;
	
	
	public PoLimitConfig() {
		opexRightMax=new BigDecimal(0);
		opexLimitless=false;
		capexRightMax=new BigDecimal(0);
		capexLimitless=false;
		genexRightMax=new BigDecimal(0);
		genexLimitless=false;
	}
	
	public BigDecimal getOpexRightMax() {
		return opexRightMax;
	}
	public void setOpexRightMax(BigDecimal opexRightMax) {
		this.opexRightMax = opexRightMax;
	}
	public boolean isOpexLimitless() {
		return opexLimitless;
	}
	public void setOpexLimitless(boolean opexLimitless) {
		this.opexLimitless = opexLimitless;
	}
	public BigDecimal getCapexRightMax() {
		return capexRightMax;
	}
	public void setCapexRightMax(BigDecimal capexRightMax) {
		this.capexRightMax = capexRightMax;
	}
	public boolean isCapexLimitless() {
		return capexLimitless;
	}
	public void setCapexLimitless(boolean capexLimitless) {
		this.capexLimitless = capexLimitless;
	}
	public BigDecimal getGenexRightMax() {
		return genexRightMax;
	}
	public void setGenexRightMax(BigDecimal genexRightMax) {
		this.genexRightMax = genexRightMax;
	}
	public boolean isGenexLimitless() {
		return genexLimitless;
	}
	public void setGenexLimitless(boolean genexLimitless) {
		this.genexLimitless = genexLimitless;
	}


	
	
}