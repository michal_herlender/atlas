package org.trescal.cwms.core.userright.entity.limitcompany;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Setter;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.userright.entity.permissionlimit.PermissionLimit;

@Entity
@Table(name = "limitCompany")
@Setter
public class LimitCompany extends Allocated<Company> {

    private int id;
    private PermissionLimit permissionLimit;
    private BigDecimal maximum;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "permLimitId")
    public PermissionLimit getPermissionLimit() {
        return permissionLimit;
    }

    @Column(name = "maximum", nullable = false)
    public BigDecimal getMaximum() {
        return maximum;
    }
}