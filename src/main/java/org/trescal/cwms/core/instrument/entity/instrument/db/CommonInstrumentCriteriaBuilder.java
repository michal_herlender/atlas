package org.trescal.cwms.core.instrument.entity.instrument.db;

import org.hibernate.criterion.MatchMode;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;

import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.JpaUtils.checkIfNullOrZero;
import static org.trescal.cwms.core.tools.StringJpaUtils.checkIfNullOrEmpty;
import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;

public class CommonInstrumentCriteriaBuilder {


	public static List<Predicate> companyPredicate(CriteriaBuilder cb, final Root<Instrument> instrument, Integer compId 
			) {
		if (!checkIfNullOrZero(compId))
			return Collections.singletonList(cb.equal(instrument.get(Instrument_.comp), compId));
		else return Collections.emptyList();
	}



	public static Stream<Predicate> modelPredicate(CriteriaBuilder cb, Join<Instrument, InstrumentModel> model, String modelName){
		if (!checkIfNullOrEmpty(modelName))
			return Stream.of(ilike(cb, model.get(InstrumentModel_.model), modelName, MatchMode.ANYWHERE));
		return Stream.empty();
	}

	public static <T> List<Predicate> listInPredicate(CriteriaBuilder cb, Expression<T> exp, List<T> list ){
		if(list.isEmpty()) return Collections.emptyList();
		return Collections.singletonList(exp.in(list));
	}



}
