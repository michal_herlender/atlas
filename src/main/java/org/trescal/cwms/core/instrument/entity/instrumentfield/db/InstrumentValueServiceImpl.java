package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueBoolean;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueDateTime;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueNumeric;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueSelection;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueString;
import org.trescal.cwms.core.instrument.form.AddEditInstrumentFieldForm;
import org.trescal.cwms.web.instrument.form.FlexibleFieldSearchDTO;

@Service
public class InstrumentValueServiceImpl extends BaseServiceImpl<InstrumentValue, Integer> implements InstrumentValueService {

	@Autowired
	InstrumentFieldDefinitionService fieldDefinitionService;

    @Autowired
    InstrumService instrumentService;

    @Autowired
    InstrumentFieldLibraryValueService instrumentFieldLibraryValueService;

	@Autowired
	InstrumentValueDao dao;
	
	@Autowired
	InstrumentValueBooleanDao booleanDao;
	
	@Autowired
	InstrumentValueDateTimeDao dateDao;
	
	@Autowired
	InstrumentValueNumericDao numericDao;
	
	@Autowired
	InstrumentValueSelectionDao selectionDao;
	
	@Autowired
	InstrumentValueStringDao stringDao;
	
	@Autowired
	MessageSource messageSource;
	
	@Override
	public String getValueAsText(InstrumentValue instrumentValue) {
		
		switch (instrumentValue.getInstrumentFieldDefinition().getFieldType()) {
		
		case BOOLEAN:
			if(instrumentValue instanceof InstrumentValueBoolean){
				InstrumentValueBoolean instrumentValueBoolean = (InstrumentValueBoolean) instrumentValue;
				return instrumentValueBoolean.getValue() ? 
						messageSource.getMessage("yes",null, LocaleContextHolder.getLocale()) : 
							messageSource.getMessage("no",null, LocaleContextHolder.getLocale());
			} else {
				throw new RuntimeException("Incorrect field type, InstrumentValue " + instrumentValue.getInstrumentFieldValueid() + "should be Boolean");
			}
					
		case STRING:
			if(instrumentValue instanceof InstrumentValueString){
				InstrumentValueString instrumentValueString = (InstrumentValueString) instrumentValue;
				return instrumentValueString.getValue();
			} else {
				throw new RuntimeException("Incorrect field type, InstrumentValue " + instrumentValue.getInstrumentFieldValueid() + "should be String");
			}
		
		case NUMERIC:
			if(instrumentValue instanceof InstrumentValueNumeric){
				InstrumentValueNumeric instrumentValueNumeric = (InstrumentValueNumeric) instrumentValue;
				return String.valueOf(instrumentValueNumeric.getValue());
			} else {
				throw new RuntimeException("Incorrect field type, InstrumentValue " + instrumentValue.getInstrumentFieldValueid() + "should be Numeric");
			}
			
		case DATETIME:
			if(instrumentValue instanceof InstrumentValueDateTime){
				InstrumentValueDateTime instrumentValueDateTime = (InstrumentValueDateTime) instrumentValue;
				DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT, LocaleContextHolder.getLocale());
				return dateFormatter.format(instrumentValueDateTime.getValue());
			} else {
				throw new RuntimeException("Incorrect field type, InstrumentValue " + instrumentValue.getInstrumentFieldValueid() + "should be Date");
			}
			
		case SELECTION:
			if(instrumentValue instanceof InstrumentValueSelection){
				InstrumentValueSelection instrumentValueSelection = (InstrumentValueSelection) instrumentValue;
				return instrumentValueSelection.getValue();
			}
			

		default:
			throw new RuntimeException("undefined field type for InstrumentValue " + instrumentValue.getInstrumentFieldValueid());
		}
		
	}

    @Override
    public AddEditInstrumentFieldForm createFormBackingObject(Integer instrumentFieldValueId, Integer fieldDefinitionId, Integer plantId) {
        AddEditInstrumentFieldForm command = new AddEditInstrumentFieldForm();
        if(instrumentFieldValueId > 0){
            InstrumentFieldDefinition fieldDefinition = this.dao.find(instrumentFieldValueId).getInstrumentFieldDefinition();
            command.setType(fieldDefinition.getFieldType());
            command.setFieldName(fieldDefinition.getName());
            command.setFieldDefinitionId(fieldDefinition.getInstrumentFieldDefinitionid());
            //get value for correct type
            switch (command.getType()) {
                case BOOLEAN:
                    command.setBooleanValue(((InstrumentValueBoolean) this.dao.find(instrumentFieldValueId)).getValue());
                    break;
                case DATETIME:
                    command.setDateValue(((InstrumentValueDateTime) this.dao.find(instrumentFieldValueId)).getValue());
                    break;
                case NUMERIC:
                    command.setNumericValue(((InstrumentValueNumeric) this.dao.find(instrumentFieldValueId)).getValue());
                    break;
                case SELECTION:
                    command.setSelectValueId(((InstrumentValueSelection) this.dao.find(instrumentFieldValueId)).getInstrumentFieldLibraryValue().getInstrumentFieldLibraryid());
                    break;
                case STRING:
                    command.setStringValue(((InstrumentValueString) this.dao.find(instrumentFieldValueId)).getValue());
                    break;
            }

        } else {
            InstrumentFieldDefinition fieldDefinition = fieldDefinitionService.get(fieldDefinitionId);
            command.setType(fieldDefinition.getFieldType());
            command.setFieldName(fieldDefinition.getName());
            command.setFieldDefinitionId(fieldDefinitionId);
        }
        command.setPlantId(plantId);
        command.setValueId(instrumentFieldValueId);
        return command;
    }

    @Override
	public void updateFieldValue(AddEditInstrumentFieldForm command) throws Exception {
		InstrumentFieldDefinition fieldDefinition = fieldDefinitionService.get(command.getFieldDefinitionId());
		// create new value of correct type
		switch (command.getType()) {
			case BOOLEAN:
				InstrumentValueBoolean booleanValue;
				if(command.getValueId() == 0){
					//new value
					booleanValue = new InstrumentValueBoolean();
					booleanValue.setInstrument(instrumentService.get(command.getPlantId()));
					booleanValue.setInstrumentFieldDefinition(fieldDefinition);
				} else {
					//existing value
					booleanValue = (InstrumentValueBoolean) this.dao.find(command.getValueId());
				}
				booleanValue.setValue(command.getBooleanValue());
				this.dao.saveOrUpdate(booleanValue);
				break;
			case DATETIME:
				InstrumentValueDateTime dateValue;
				if(command.getValueId() == 0){
					//new value
					dateValue = new InstrumentValueDateTime();
					dateValue.setInstrument(instrumentService.get(command.getPlantId()));
					dateValue.setInstrumentFieldDefinition(fieldDefinition);
				} else {
					//existing value
					dateValue = (InstrumentValueDateTime) this.dao.find(command.getValueId());
				}
				dateValue.setValue(command.getDateValue());
				this.dao.saveOrUpdate(dateValue);
				break;
			case NUMERIC:
				InstrumentValueNumeric numericValue;
				if(command.getValueId() == 0){
					//new value
					numericValue = new InstrumentValueNumeric();
					numericValue.setInstrument(instrumentService.get(command.getPlantId()));
					numericValue.setInstrumentFieldDefinition(fieldDefinition);
				} else {
					//existing value
					numericValue = (InstrumentValueNumeric) this.dao.find(command.getValueId());
				}
				numericValue.setValue(command.getNumericValue());
				this.dao.saveOrUpdate(numericValue);
				break;
			case SELECTION:
				InstrumentValueSelection selectionValue;
				if(command.getValueId() == 0){
					//new value
					selectionValue = new InstrumentValueSelection();
					selectionValue.setInstrument(instrumentService.get(command.getPlantId()));
					selectionValue.setInstrumentFieldDefinition(fieldDefinition);
				} else {
					//existing value
					selectionValue = (InstrumentValueSelection) this.dao.find(command.getValueId());
				}
				selectionValue.setInstrumentFieldLibraryValue(instrumentFieldLibraryValueService.get(command.getSelectValueId()));
				this.dao.saveOrUpdate(selectionValue);
				break;
			case STRING:
				InstrumentValueString stringValue;
				if(command.getValueId() == 0){
					//new value
					stringValue = new InstrumentValueString();
					stringValue.setInstrument(instrumentService.get(command.getPlantId()));
					stringValue.setInstrumentFieldDefinition(fieldDefinition);
				} else {
					//existing value
					stringValue = (InstrumentValueString) this.dao.find(command.getValueId());
				}
				stringValue.setValue(command.getStringValue());
				this.dao.saveOrUpdate(stringValue);
				break;
			default:
				throw new Exception("unknown field type " + command.getType().toString());
		}
	}

	@Override
	protected BaseDao<InstrumentValue, Integer> getBaseDao() {
		return this.dao;
	}

	@Override
	public List<Integer> getInstrumentIdsForFlexibleFieldSearch(List<FlexibleFieldSearchDTO> searchParameters) {
		List<Integer> instrumentIds = new ArrayList<>();
		Boolean searchPerformed = false;
		for (FlexibleFieldSearchDTO searchParameter : searchParameters) {
			InstrumentFieldDefinition fieldDefinition = fieldDefinitionService.get(searchParameter.getFieldDefinitionId());
			switch (fieldDefinition.getFieldType()) {
			case BOOLEAN:
				if(searchParameter.getBooleanValue() != null){
					searchPerformed = true;
					instrumentIds.addAll(booleanDao.getMatchingInstruments(fieldDefinition, searchParameter.getBooleanValue()).
							stream().map(i -> i.getPlantid()).collect(Collectors.toList()));
				}
				break;
			case DATETIME:
				if(searchParameter.getDateValue() != null){
					searchPerformed = true;
					instrumentIds.addAll(dateDao.getMatchingInstruments(fieldDefinition, searchParameter.getDateValue()).
							stream().map(i -> i.getPlantid()).collect(Collectors.toList()));
				}
				break;
			case NUMERIC:
				if(searchParameter.getNumericValue() != null){
					searchPerformed = true;
					instrumentIds.addAll(numericDao.getMatchingInstruments(fieldDefinition, searchParameter.getNumericValue()).
							stream().map(i -> i.getPlantid()).collect(Collectors.toList()));
				}
				break;
			case SELECTION:
				InstrumentFieldLibraryValue libraryValue = instrumentFieldLibraryValueService.get(searchParameter.getSelectionValueId());
				if(libraryValue != null){
					searchPerformed = true;
					instrumentIds.addAll(selectionDao.getMatchingInstruments(fieldDefinition, libraryValue).
							stream().map(i -> i.getPlantid()).collect(Collectors.toList()));
				}
				break;
			case STRING:
				if(searchParameter.getStringValue() != null && !searchParameter.getStringValue().isEmpty()){
					searchPerformed = true;
					instrumentIds.addAll(stringDao.getMatchingInstruments(fieldDefinition, searchParameter.getStringValue()).
							stream().map(i -> i.getPlantid()).collect(Collectors.toList()));
				}
		 		break;
			default:
				throw new RuntimeException("Invalid flexible field type");
			}
		}
		
		return searchPerformed ? instrumentIds : null;
	}



	
}
