package org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationAction;

@Repository("InstrumentValidationActionDao")
public class InstrumentValidationActionDaoImpl extends BaseDaoImpl<InstrumentValidationAction, Integer> implements InstrumentValidationActionDao {
	
	@Override
	protected Class<InstrumentValidationAction> getEntity() {
		return InstrumentValidationAction.class;
	}
}