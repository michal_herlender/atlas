package org.trescal.cwms.core.instrument.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.ImportInstrumentsForm;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm;
import org.trescal.cwms.core.instrument.form.validator.ImportedInstrumentsSynthesisFormValidator;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
		Constants.SESSION_ATTRIBUTE_COMPANY })
public class ImportedInstrumentsSynthesisController {

	public static final String FORM = "form";
	@Autowired
	private ImportedInstrumentsSynthesisFormValidator validator;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private UserService userService;

	@InitBinder(FORM)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("serviceTypes")
	public List<KeyValue<Integer, String>> initializeServiceTypes(Locale locale) {
		return this.serviceTypeService.getAllByDomainType(DomainType.INSTRUMENTMODEL).stream().map(st -> {
			return new KeyValue<Integer, String>(st.getServiceTypeId(),
					this.translationService.getCorrectTranslation(st.getShortnameTranslation(), locale));
		}).collect(Collectors.toList());
	}

	@ModelAttribute(FORM)
	protected ImportedInstrumentSynthesisForm formBackingObject(
			@ModelAttribute("importInstrumentsForm") ImportInstrumentsForm inputForm,
			@ModelAttribute(FORM) ImportedInstrumentSynthesisForm form,
			@ModelAttribute("fileContent") ArrayList<LinkedCaseInsensitiveMap<String>> fileContent, Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws ParseException {

		if (form.getSubdivId() == null) {
			form.setDefaultAddressId(inputForm.getAddrid());
			form.setDefaultContactId(inputForm.getPersonid());
			form.setDefaultServiceTypeId(inputForm.getDefaultServiceTypeId());
			form.setExchangeFormatId(inputForm.getExchangeFormatId());
			form.setSubdivId(inputForm.getSubdivid());
			form.setRows(instrumentService.convertFileContentToInstrumentDTO(form.getExchangeFormatId(), fileContent,
					form.getDefaultAddressId(), form.getDefaultContactId(), form.getDefaultServiceTypeId(),
					form.getSubdivId()));
		}

		form.setBusinessCompanyId(this.userService.get(username).getCon().getSub().getComp().getId());
		Contact con = contactService.get(form.getDefaultContactId());
		Address addr = addressService.get(form.getDefaultAddressId());
		model.addAttribute("contact", con);
		model.addAttribute("address", addr);
		model.addAttribute("subdiv", addr.getSub());
		model.addAttribute("company", addr.getSub().getComp());
		model.addAttribute("serviceType", serviceTypeService.get(form.getDefaultServiceTypeId()));

		return form;
	}

	@RequestMapping(value = "/importedinstrumentssynthesis.htm", method = RequestMethod.GET)
	public String doGet(Model model) {
		return "trescal/core/instrument/importedinstrumentssynthesis";
	}

	@RequestMapping(value = { "/importedinstrumentssynthesis.htm" }, params = { "!save",
			"newanalysis" }, method = RequestMethod.POST)
	protected ModelAndView newanalysis(@Valid @ModelAttribute(FORM) ImportedInstrumentSynthesisForm form,
			BindingResult result, Model model) throws Exception {
		return new ModelAndView("trescal/core/instrument/importedinstrumentssynthesis");
	}

	@RequestMapping(value = { "/importedinstrumentssynthesis.htm" }, params = { "save",
			"!newanalysis" }, method = RequestMethod.POST)
	protected ModelAndView submit(@Valid @ModelAttribute(FORM) ImportedInstrumentSynthesisForm form,
			BindingResult result, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Model model,
			Locale locale) throws Exception {

		if (result.hasErrors())
			return newanalysis(form, result, model);
		User user = this.userService.get(username);
		Contact currentContact = user.getCon();
		instrumentService.importInstruments(form.getRows(), form.getSubdivId(), form.getDefaultAddressId(),
				form.getDefaultContactId(), form.getDefaultServiceTypeId(), currentContact.getId());

		return new ModelAndView(new RedirectView("importinstruments.htm"));
	}

}
