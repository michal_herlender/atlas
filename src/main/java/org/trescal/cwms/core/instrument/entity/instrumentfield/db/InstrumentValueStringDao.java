package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueString;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentValueStringDTO;

public interface InstrumentValueStringDao extends BaseDao<InstrumentValueString, Integer> {
	
	List<InstrumentValueStringDTO> getAll(InstrumentFieldDefinition fieldDefinition);
	
	List<Instrument> getMatchingInstruments(InstrumentFieldDefinition fieldDefinition, String value);
}