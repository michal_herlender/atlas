package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@PrimaryKeyJoinColumn(name="instrumentFieldValueid")  
@Table(name = "instrumentvalueboolean")
public class InstrumentValueBoolean extends InstrumentValue {
	
	private Boolean value;
	
	@NotNull
	public Boolean getValue() {
		return value;
	}
	
	public void setValue(Boolean value) {
		this.value = value;
	}
}
