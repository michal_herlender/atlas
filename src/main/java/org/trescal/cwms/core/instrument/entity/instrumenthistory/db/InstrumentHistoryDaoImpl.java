package org.trescal.cwms.core.instrument.entity.instrumenthistory.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.InstrumentHistory;

@Repository("InstrumentHistoryDao")
public class InstrumentHistoryDaoImpl extends BaseDaoImpl<InstrumentHistory, Integer> implements InstrumentHistoryDao {
	
	@Override
	protected Class<InstrumentHistory> getEntity() {
		return InstrumentHistory.class;
	}
}