package org.trescal.cwms.core.instrument.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.util.AutoPopulatingList.ElementFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.db.InstrumentStorageTypeService;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.db.InstrumentUsageTypeService;
import org.trescal.cwms.core.instrument.form.EditInstrumentForm;
import org.trescal.cwms.core.instrument.form.EditInstrumentValidator;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db.ThreadService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db.ThreadTypeService;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DisallowSamePlantNumberDefaultType;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller used to edit an {@link Instrument}. Attention should be paid to
 * the use of an {@link AutoPopulatingList} in the formBackingObject to allow
 * for dynamic generation of new {@link InstrumentRange}s via the use of an
 * {@link ElementFactory} implementation (InstrumentRangeElementFactory} ).
 */
@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY})
public class EditInstrumentController {
	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private AddressService addressServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private InstrumentStorageTypeService instStorServ;
	@Autowired
	private InstrumentUsageTypeService instUsageServ;
	@Autowired
	private InstrumentModelService modelServ;
	@Autowired
	private RecallRuleService recallRuleServ;
	@Autowired
	private UserService userService;
	@Autowired
	private ThreadService threadServ;
	@Autowired
	private ThreadTypeService threadTypeServ;
	@Autowired
	private UoMService uomServ;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private EditInstrumentValidator validator;

	@Value("#{props['cwms.config.instrument.defaultcalfrequency']}")
	private Integer defaultCalFrequency;
	@Autowired
	private DisallowSamePlantNumberDefaultType disallowSamePlantNumber;

	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new MyCustomDateEditor(DateTools.df_ISO8601, true));
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@ModelAttribute("units")
	public List<KeyValue<String, String>> initializeRecallIntervalUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute("serviceTypes")
	public List<KeyValue<Integer, String>> initializeServiceTypes(Locale locale) {
		return this.serviceTypeService.getAllByDomainType(DomainType.INSTRUMENTMODEL).stream().map(st -> new KeyValue<>(st.getServiceTypeId(),
			this.translationService.getCorrectTranslation(st.getShortnameTranslation(), locale) + " - "
				+ this.translationService.getCorrectTranslation(st.getLongnameTranslation(), locale))).collect(Collectors.toList());
	}

	@ModelAttribute("command")
	protected EditInstrumentForm formBackingObject(
			@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId,
			@RequestParam(value = "modelid", required = false, defaultValue = "0") Integer modelId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		EditInstrumentForm form;
		if (plantId == 0) {
			if (modelId == 0) {
				this.logger.error("Attempt to create instrument with no model selected");
				throw new RuntimeException("No model selected");
			} else {
				InstrumentModel model = modelServ.findInstrumentModel(modelId);
				form = new EditInstrumentForm();
				form.setLastModified(new Date());
				form.setCalFrequency(defaultCalFrequency);
				form.setCalFrequencyUnitId(IntervalUnit.MONTH.name());
				form.setStatus(InstrumentStatus.IN_CIRCULATION);
				form.setUsageTypeId(null);//this.instUsageServ.findDefaultType(InstrumentUsageType.class).getId());
				form.setStorageTypeId(this.instStorServ.findDefaultType(InstrumentStorageType.class).getId());
				form.setModelid(modelId);
				form.setCalibrationStandard(false);
				form.setUsesSinceLastCalibration(0);
				form.setCalExpiresAfterNumberOfUses(false);
				form.setPermittedNumberOfUses(0);
				form.setHireInstrumentReq(false);
				form.setCharacteristicMap(new HashMap<>());
				form.setCustomerManaged(false);

				for (CharacteristicDescription characteristic : model.getDescription()
					.getCharacteristicDescriptions()) {
					InstrumentCharacteristic instrChar = new InstrumentCharacteristic();
					instrChar.setCharacteristic(characteristic);
					if (characteristic.getCharacteristicType() == CharacteristicType.LIBRARY) {
						instrChar.setLibraryValues(model.getLibraryCharacteristics().get(characteristic));
						if (instrChar.getLibraryValues() == null || instrChar.getLibraryValues().size() == 0)
							instrChar.setLibraryValues(characteristic.getLibrary());
					} else {
						ModelRange modelRange = model.getRanges().stream()
								.filter(r -> r.getCharacteristicDescription().equals(characteristic)).findFirst()
								.orElse(null);
						instrChar.setModelRange(modelRange);
						instrChar.setUomid(
							modelRange == null ? null : modelRange.getUom().getId());
					}
					form.getCharacteristicMap().put(characteristic.getCharacteristicDescriptionId(), instrChar);
				}
			}
		} else {
			Instrument instrument = this.instServ.get(plantId);
			if (instrument == null) {
				this.logger.error("Requested instrument [" + plantId + "] not found");
				throw new RuntimeException("Requested instrument not found");
			} else {
				form = instServ.getEditInstrumentForm(plantId);

				if (modelId > 0 && instrument.getModel().getModelid() != modelId) {
					// instrument model changed -> remove characteristics
					instrument.setModel(modelServ.findLazyInstrumentModel(modelId));
					instrument.getCharacteristics().clear();
					// delete brand and model name of instrument, if the new
					// instrument model is not generic
					if (!instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
						instrument.setMfr(null);
						instrument.setModelname(null);
					}
					// instrument model will be changed immediately (coming back
					// from addinstrumentmodelsearch)
					instrument = instServ.merge(instrument);
					// update lastModified to prevent
					// HibernateStateStolenException
					form.setLastModified(instrument.getLastModified());
				}
				if (instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
					// add key characteristics
					form.setCharacteristicMap(new HashMap<>());
					for (CharacteristicDescription characteristic : instrument.getModel().getDescription()
						.getCharacteristicDescriptions()) {
						InstrumentCharacteristic instrChar = instrument.getCharacteristics().stream()
							.filter(c -> c.getCharacteristic().equals(characteristic)).findFirst().orElse(null);
						if (instrChar == null) {
							instrChar = new InstrumentCharacteristic();
							instrChar.setInstrument(instrument);
							instrChar.setCharacteristic(characteristic);
							instrument.getCharacteristics().add(instrChar);
						}
						if (characteristic.getCharacteristicType() == CharacteristicType.LIBRARY) {
							instrChar.setLibraryValues(
									instrument.getModel().getLibraryCharacteristics().get(characteristic));
							if (instrChar.getLibraryValues() == null || instrChar.getLibraryValues().size() == 0)
								instrChar.setLibraryValues(characteristic.getLibrary());
							if (instrChar.getLibraryValue() != null)
								instrChar.setLibraryValueId(instrChar.getLibraryValue().getCharacteristiclibraryid());
						} else {
							ModelRange modelRange = instrument.getModel().getRanges().stream()
								.filter(r -> r.getCharacteristicDescription().equals(characteristic)).findFirst()
                                .orElse(null);
                            instrChar.setUomid(modelRange == null ? null
                                : modelRange.getUom().getId());
                            if (instrChar.getUom() != null)
                                instrChar.setUomid(instrChar.getUom().getId());
                            instrChar.setModelRange(modelRange);
                        }
                        form.getCharacteristicMap().put(characteristic.getCharacteristicDescriptionId(), instrChar);
                    }
                }
                form.setModelid(instrument.getModel().getModelid());
                form.setProcid(instrument.getCapability() == null ? null : instrument.getCapability().getId());
                form.setWiid(instrument.getWorkInstructions() == null || instrument.getWorkInstructions().isEmpty()
                    ? null : instrument.getWorkInstructions().get(0).getId());
                // copy the history enabled settings into copy variables
                form.setPersonid(instrument.getCon().getPersonid());
                form.setStatusid(instrument.getStatus().ordinal());
                form.setUsageTypeId(instrument.getUsageType() == null ? null : instrument.getUsageType().getId());
                form.setFirmwareAccessCode(instrument.getFirmwareAccessCode());
                form.setCustomerDescription(instrument.getCustomerDescription());
                form.setCustomerManaged(instrument.getCustomerManaged());
                form.setCalFrequencyUnitId(instrument.getCalFrequencyUnit().name());
                form.setDefaultClientRef(instrument.getDefaultClientRef());
                // default service type
				if (instrument.getDefaultServiceType() != null)
					form.setDefaultServiceTypeId(instrument.getDefaultServiceType().getServiceTypeId());
				// check for hire instrument (currently single record supported)
				form.setHireInstrumentReq(false);
			}
		}
		form.setBusinessCompanyId(companyDto.getKey());
		return form;
	}

	@RequestMapping(value = "/editinstrument.htm", method = RequestMethod.POST)
	public String onSubmit(Model model,
			@RequestParam(value = "addressid", required = false, defaultValue = "0") Integer addrid,
			@RequestParam(value = "contactid", required = false, defaultValue = "0") Integer personid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Validated @ModelAttribute("command") EditInstrumentForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, addrid, personid, form);
		} else {
			Contact currentContact = this.userService.get(username).getCon();
			Instrument instrument = instServ.saveOrUpdate(form, currentContact);
			model.asMap().clear();
			return "redirect:viewinstrument.htm?plantid=" + instrument.getPlantid();
		}
	}

	public List<CharacteristicDescription> getSortedCharacteristicDescriptions(InstrumentModel instModel) {
		// Options have null characteristic description, excluded
		List<CharacteristicDescription> allCharacteristicDescriptions = instModel.getRanges().stream()
			.map(ModelRange::getCharacteristicDescription).filter(Objects::nonNull)
			.collect(Collectors.toList());
		allCharacteristicDescriptions.addAll(instModel.getLibraryCharacteristics().keySet());
		return allCharacteristicDescriptions.stream()
			.sorted(Comparator.comparing(CharacteristicDescription::getOrderno)).collect(Collectors.toList());
	}

	public List<KeyValue<Integer, ModelRange>> getRangeKeyValues(InstrumentModel instModel) {
		// Options have null characteristic description, excluded
		return instModel.getRanges().stream()
			.filter(r -> r.getCharacteristicDescription() != null)
			.map(r -> new KeyValue<>(
				r.getCharacteristicDescription().getCharacteristicDescriptionId(), r))
			.collect(Collectors.toList());
	}

	@RequestMapping(value = "/editinstrument.htm", method = RequestMethod.GET)
	protected String referenceData(Model model,
			@RequestParam(value = "addressid", required = false, defaultValue = "0") Integer addrid,
			@RequestParam(value = "contactid", required = false, defaultValue = "0") Integer personid,
			@ModelAttribute("command") EditInstrumentForm form) {
		InstrumentModel instModel = modelServ.findInstrumentModel(form.getModelid());
		model.addAttribute("model", instModel);
		model.addAttribute("sortedCharacteristicDescriptions", getSortedCharacteristicDescriptions(instModel));
		model.addAttribute("rangeKeyValues", getRangeKeyValues(instModel));

		List<CharacteristicDescription> libraryCharacteristics = instModel.getLibraryCharacteristics().keySet().stream()
			.sorted(Comparator.comparing(CharacteristicDescription::getOrderno)).collect(Collectors.toList());
		model.addAttribute("libraryCharacteristics", libraryCharacteristics);

		model.addAttribute("storage", this.instStorServ.getAllInstrumentStorageTypes());
		model.addAttribute("usageTypes", this.instUsageServ.getAllInstrumentUsageTypes());
		model.addAttribute("threads", this.threadServ.getAll());
		// SL : Gets only the active uom
		model.addAttribute("uomlist", this.uomServ.getAllActiveUoMs());
		model.addAttribute("threadtypes", this.threadTypeServ.getAll());
		// load any recall rules currently applied to the instrument if this is
		// a current instrument
		if (form.getPlantId() != null && form.getPlantId() != 0) {
			model.addAttribute("onActiveJob",
					instServ.get(form.getPlantId()).getJobItems().stream().anyMatch(ji -> ji.getState().getActive()));
			model.addAttribute("activerecallrules",
					this.recallRuleServ.getAllInstrumentRecallRules(form.getPlantId(), true));
			Instrument instrument = instServ.get(form.getPlantId());
			model.addAttribute("instrument", instrument);
			model.addAttribute("calExist", instrument.getLastCal() != null
					|| (instrument.getInstCertLinks() != null && instrument.getInstCertLinks().size() > 0));
			model.addAttribute("disallowSamePlantNumber",
					disallowSamePlantNumber.getValueByScope(Scope.COMPANY, instrument.getComp(), null));
		}
		// pre-populate cascading search plugin if new instrument for specific
		// company - DO NOT CHANGE THE NAMES OF THESE REQUEST PARAMETERS! -->
		// make them constants somewhere!
		Contact con = (personid == 0) ? null : this.conServ.get(personid);
		Address addr = (addrid == 0) ? null : this.addressServ.get(addrid);
		if ((con != null) && (addr != null)) {
			model.addAttribute("casc_coid", con.getSub().getComp().getCoid());
			model.addAttribute("casc_subdivid", con.getSub().getSubdivid());
			model.addAttribute("casc_personid", con.getPersonid());
			model.addAttribute("casc_addrid", addr.getAddrid());
		}
		// get available statuss
		model.addAttribute("statuslist", InstrumentStatus.activeStatus());
		// default & do not recall status used for new instruments
		String DEFAULT_STATUS = "defaultstatus";
		model.addAttribute(DEFAULT_STATUS, InstrumentStatus.IN_CIRCULATION);
		String DO_NOT_RECALL_STATUS = "donotrecallstatus";
		model.addAttribute(DO_NOT_RECALL_STATUS, InstrumentStatus.DO_NOT_RECALL);
		return "trescal/core/instrument/editinstrument";
	}
}