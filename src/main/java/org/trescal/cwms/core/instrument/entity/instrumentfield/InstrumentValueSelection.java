package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.persistence.Transient;

@Entity
@PrimaryKeyJoinColumn(name="instrumentFieldValueid")  
@Table(name = "instrumentvalueselection")
public class InstrumentValueSelection extends InstrumentValue
{
	private InstrumentFieldLibraryValue instrumentFieldLibraryValue;
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "instrumentFieldLibraryid", nullable = false)
	public InstrumentFieldLibraryValue getInstrumentFieldLibraryValue() {
		return instrumentFieldLibraryValue;
	}
	
	public void setInstrumentFieldLibraryValue(InstrumentFieldLibraryValue instrumentFieldLibrary) {
		this.instrumentFieldLibraryValue = instrumentFieldLibrary;
	}
	
	@Transient
	public String getValue() {
		return this.getInstrumentFieldLibraryValue() != null ? this.getInstrumentFieldLibraryValue().getName() : "";
	}
}