package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentValueDateTimeDTO;

public interface InstrumentValueDateTimeDao {

	List<InstrumentValueDateTimeDTO> getAll(InstrumentFieldDefinition fieldDefinition);
	
	List<Instrument> getMatchingInstruments(InstrumentFieldDefinition fieldDefinition, Date value);
}