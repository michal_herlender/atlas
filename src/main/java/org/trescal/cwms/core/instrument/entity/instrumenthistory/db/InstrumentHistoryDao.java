package org.trescal.cwms.core.instrument.entity.instrumenthistory.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.InstrumentHistory;

public interface InstrumentHistoryDao extends BaseDao<InstrumentHistory, Integer> {}