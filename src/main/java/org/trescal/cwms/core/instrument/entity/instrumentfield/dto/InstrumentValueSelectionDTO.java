package org.trescal.cwms.core.instrument.entity.instrumentfield.dto;

import org.trescal.cwms.core.tools.FieldType;

public class InstrumentValueSelectionDTO extends InstrumentValueDTO {

	private String value;
	
	public InstrumentValueSelectionDTO(Integer plantId, String fieldName, String value) {
		super(plantId, fieldName);
		this.value = value;
	}
	
	@Override
	public FieldType getFieldType() {
		return FieldType.SELECTION;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}