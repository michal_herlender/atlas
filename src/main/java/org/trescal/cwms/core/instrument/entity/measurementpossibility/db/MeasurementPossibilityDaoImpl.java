package org.trescal.cwms.core.instrument.entity.measurementpossibility.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.measurementpossibility.MeasurementPossibility;

@Repository("MeasurementPossibilityDao")
public class MeasurementPossibilityDaoImpl extends BaseDaoImpl<MeasurementPossibility, Integer> implements MeasurementPossibilityDao
{
	@Override
	protected Class<MeasurementPossibility> getEntity() {
		return MeasurementPossibility.class;
	}
}