package org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;

/**
 * Created by paul.weston on 22/02/2017.
 */
public interface InstrumentComplementaryFieldService extends BaseService<InstrumentComplementaryField, Integer> {

    InstrumentComplementaryField getComplementaryFieldByPlantid(int plantid);

}
