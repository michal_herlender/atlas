package org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;

public interface InstrumentValidationStatusDao extends BaseDao<InstrumentValidationStatus, Integer> {}