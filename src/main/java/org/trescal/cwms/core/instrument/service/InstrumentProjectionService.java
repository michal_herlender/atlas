package org.trescal.cwms.core.instrument.service;

import java.util.List;

import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;

public interface InstrumentProjectionService {
	public void loadInstrumentProjections(List<InstrumentProjectionDTO> instDtos, InstrumentProjectionCommand command);
}
