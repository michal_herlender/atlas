package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension;

public interface PermitedCalibrationExtensionDao extends BaseDao<PermitedCalibrationExtension, Integer> {

	PermitedCalibrationExtension getExtensionForInstrument(Instrument instrument);
}
