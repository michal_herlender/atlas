package org.trescal.cwms.core.instrument.entity.hireinstrument.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.dto.HireInstrumentDto;
import org.trescal.cwms.core.hire.dto.HireInstrumentRevenueWrapper;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

import java.util.List;
import java.util.Optional;

public interface HireInstrumentDao extends BaseDao<HireInstrument, Integer> {

	HireInstrument findHireInstrumentDWR(int id);

	HireInstrument findHireInstrumentUsingBarcodeDWR(int plantid);

	int getHireInstrumentCount();

	List<HireInstrument> getHireInstruments(List<Integer> plantIds);
	
	List<HireInstrumentRevenueWrapper> searchHireInstrumentsRevenueHQL(Integer mfrid, String mfrname, String modelname, Integer descid, String descname, String barcode, String plantno, String serialno);

    List<HireInstrumentDto> searchHireInstruments(Integer companyId, String plantId, String plantNo, String serialNo, String model, String manufacturer, String subFamily);

    Optional<HireInstrument> getHireInstrumentByPlantId(Integer plantId);

}