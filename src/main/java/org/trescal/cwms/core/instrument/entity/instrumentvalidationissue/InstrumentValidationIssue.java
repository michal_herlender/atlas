package org.trescal.cwms.core.instrument.entity.instrumentvalidationissue;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationAction;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationActionComparator;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "instrumentvalidationissue")
public class InstrumentValidationIssue extends Auditable {
	private Set<InstrumentValidationAction> actions;
	private int id;
	private Instrument instrument;
	private String issue;
	private Contact raisedBy;
	private LocalDate raisedOn;
	private Contact resolvedBy;
	private LocalDate resolvedOn;
	private InstrumentValidationStatus status;

	@OneToMany(mappedBy = "issue", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(InstrumentValidationActionComparator.class)
	public Set<InstrumentValidationAction> getActions() {
		return this.actions;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable = false)
	public Instrument getInstrument() {
		return this.instrument;
	}

	@Length(max = 200)
	@Column(name = "issue", length = 200)
	public String getIssue() {
        return this.issue;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "raisedby", nullable = false)
    public Contact getRaisedBy() {
        return this.raisedBy;
    }

    @Column(name = "raisedon", nullable = false, columnDefinition = "date")
    public LocalDate getRaisedOn() {
        return this.raisedOn;
    }

    public void setRaisedOn(LocalDate raisedOn) {
        this.raisedOn = raisedOn;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "resolvedby")
	public Contact getResolvedBy() {
		return this.resolvedBy;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = false)
	public InstrumentValidationStatus getStatus()
	{
		return this.status;
	}

	public void setActions(Set<InstrumentValidationAction> actions)
	{
		this.actions = actions;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInstrument(Instrument instrument)
	{
		this.instrument = instrument;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public void setRaisedBy(Contact raisedBy) {
        this.raisedBy = raisedBy;
    }

    @Column(name = "resolvedon", columnDefinition = "date")
    public LocalDate getResolvedOn() {
        return this.resolvedOn;
    }

    public void setResolvedBy(Contact resolvedBy) {
        this.resolvedBy = resolvedBy;
    }

    public void setResolvedOn(LocalDate resolvedOn) {
        this.resolvedOn = resolvedOn;
	}

	public void setStatus(InstrumentValidationStatus status) {
		this.status = status;
	}
}