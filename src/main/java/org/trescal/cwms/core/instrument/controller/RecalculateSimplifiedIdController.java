package org.trescal.cwms.core.instrument.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller
@IntranetController
public class RecalculateSimplifiedIdController {
    @GetMapping("rebuildIndex.htm")
    public String rebuildIndex(){
        return "trescal/core/instrument/recalculateSimplifiedIds";
    }
}
