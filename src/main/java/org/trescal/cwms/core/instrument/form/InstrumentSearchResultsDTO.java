package org.trescal.cwms.core.instrument.form;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;

import lombok.*;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Data
public class InstrumentSearchResultsDTO {

    private final Integer plantId;
    private final String mfr;
    private final String instMfr;
    private final String model;
    private final String instModel;
    private final String subfamily;
    private final String modelName;
    private final String customerDescription;
    private final String owner;
    private final String plantNo;
    private final String serialNo;
    private final LocalDate calDueDate;
    private final LocalDate calExtensionEndDate;
    private final LocalDate lastCalDate;
    private final Integer calibrationInterval;
    private final IntervalUnit calibrationIntervalUnit;
    private final InstrumentStatus instrumentStatus;
    private final String usageTypeName;
    private final String storageTypeName;
    private final String subdivName;
    private final Boolean scrapped;
    private final String addressLine1;
    private final String addressLine2;
    private final String addressLine3;
    private final String town;
    private final String postcode;
    private final String location;
    private final Boolean isCalStandard;
    private final ModelMfrType modelMfrType;
    private final String ownerPosition;
    private final String defaultClientRef;
    private final ZonedDateTime lastReceived;
    private final Date penultimateCalDate;
    private final Date antepenultimateCalDate;
    private final String serviceTypeName;
    private final CalibrationVerificationStatus lastCertStatus;
    private final BigDecimal lastCertDeviation;
    private final String CAR;
    private final Date certCalDate;

    private SortedMap<String, String> flexibleFields = new TreeMap<>();



    

}