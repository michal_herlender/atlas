package org.trescal.cwms.core.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrument.dto.DefaultTransportOptionsDTO;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV })
public class TransportOptionJsonController {

	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private SubdivService subdivService;

	@RequestMapping(value = "/getDefaulTransportOptions.json", method = RequestMethod.GET)
	@ResponseBody
	public DefaultTransportOptionsDTO getDefaulTransportOptions(@RequestParam(required = true) Integer addrid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {

		DefaultTransportOptionsDTO dto = new DefaultTransportOptionsDTO();

		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Address addr = addressService.get(addrid);

		if (addr != null) {
			TransportOption transportOptionIn = this.transportOptionService.getTransportOptionIn(addr, allocatedSubdiv);
			TransportOption transportOptionOut = this.transportOptionService.getTransportOptionOut(addr,
					allocatedSubdiv);

			if (transportOptionIn != null)
				dto.setTransportOptionIn(transportOptionIn.getId());
			if (transportOptionOut != null)
				dto.setTransportOptionOut(transportOptionOut.getId());
		}

		return dto;
	}

}
