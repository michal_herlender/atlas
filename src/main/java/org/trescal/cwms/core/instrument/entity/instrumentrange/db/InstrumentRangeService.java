package org.trescal.cwms.core.instrument.entity.instrumentrange.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;

public interface InstrumentRangeService extends BaseService<InstrumentRange, Integer>
{
}