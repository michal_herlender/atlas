package org.trescal.cwms.core.instrument.form;

import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;
import org.springframework.validation.Validator;

public class InstrumentIssueValidator implements Validator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(InstrumentValidationIssue.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		InstrumentValidationIssue iss = (InstrumentValidationIssue) target;

		if (iss.getIssue().length() > 200)
		{
			errors.rejectValue("issue", null, "Exceeds maximum length");
		}
	}
}
