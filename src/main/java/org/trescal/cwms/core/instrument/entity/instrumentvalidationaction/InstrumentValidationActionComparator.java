package org.trescal.cwms.core.instrument.entity.instrumentvalidationaction;

import java.util.Comparator;

/**
 * Comparator class which sorts {@link InstrumentValidationAction} by the date
 * they took place or by id if the date is identical.
 * 
 * @author richard
 */
public class InstrumentValidationActionComparator implements Comparator<InstrumentValidationAction>
{
	@Override
	public int compare(InstrumentValidationAction o1, InstrumentValidationAction o2)
	{
		if (o1.getUnderTakenOn().compareTo(o2.getUnderTakenOn()) == 0)
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
		else
		{
			return o2.getUnderTakenOn().compareTo(o1.getUnderTakenOn());
		}
	}
}
