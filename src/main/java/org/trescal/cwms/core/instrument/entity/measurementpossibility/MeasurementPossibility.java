package org.trescal.cwms.core.instrument.entity.measurementpossibility;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Entity
@Table(name = "measurementpossibility")
public class MeasurementPossibility {
	private int measurementpossibilityid;
	private String name;
	private Double minimum;
	private Double maximum;
	private String scalederiva;
	private String quality;
	private String lmax;
	private Instrument instrument;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getMeasurementpossibilityid() {
		return measurementpossibilityid;
	}
	
	@Column(name = "name", nullable = false,columnDefinition = "nvarchar(2000)")
	public String getName() {
		return name;
	}
	
	@Column(name = "minimum", nullable = true)
	public Double getMinimum() {
		return minimum;
	}

	@Column(name = "maximum", nullable = true)
	public Double getMaximum() {
		return maximum;
	}
	
	@Column(name = "scalederiva", nullable = true,columnDefinition = "nvarchar(2000)")
	public String getScalederiva() {
		return scalederiva;
	}
	
	@Column(name = "quality", nullable = true,columnDefinition = "nvarchar(2000)")
	public String getQuality() {
		return quality;
	}
	
	@Column(name = "lmax", nullable = true,columnDefinition = "nvarchar(2000)")
	public String getLmax() {
		return lmax;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable = false)
	public Instrument getInstrument() {
		return instrument;
	}
	
	public void setMeasurementpossibilityid(int measurementpossibilityid) {
		this.measurementpossibilityid = measurementpossibilityid;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}
	
	public void setMaximum(Double maximum) {
		this.maximum = maximum;
	}
	
	public void setScalederiva(String scalederiva) {
		this.scalederiva = scalederiva;
	}
	
	public void setQuality(String quality) {
		this.quality = quality;
	}
	
	public void setLmax(String lmax) {
		this.lmax = lmax;
	}
	
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
}
