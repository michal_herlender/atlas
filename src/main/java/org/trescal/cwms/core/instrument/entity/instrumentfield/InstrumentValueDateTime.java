package org.trescal.cwms.core.instrument.entity.instrumentfield;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@PrimaryKeyJoinColumn(name="instrumentFieldValueid")  
@Table(name = "instrumentvaluedatetime")
public class InstrumentValueDateTime extends InstrumentValue {
	
	private Date value;
	
	@NotNull
	@DateTimeFormat(iso=DateTimeFormat.ISO.DATE_TIME)
	public Date getValue() {
		return value;
	}
	
	public void setValue(Date value) {
		this.value = value;
	}
}