package org.trescal.cwms.core.instrument.form.validator;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutPlantNo;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutSerialNo;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ImportedInstrumentsSynthesisFormValidator extends AbstractBeanValidator {

	@Autowired
	private ImportedInstrumentsSynthesisRowDTOValidator rowValidator;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContactService contactService;
	@Autowired
    private AddressService addressService;
    @Autowired
    private ServiceCapabilityService serviceCapabilityService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private InstrumentModelService instrumentModelService;
    @Autowired
    private AllowInstrumentCreationWithoutPlantNo alloweInstCreat;
    @Autowired
    private AllowInstrumentCreationWithoutSerialNo allowInstrumentCreationWithoutSerialNo;
    @Autowired
    private CapabilityService capabilityService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(ImportedInstrumentSynthesisForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {

        super.validate(target, errors);

        ImportedInstrumentSynthesisForm form = (ImportedInstrumentSynthesisForm) target;

        Subdiv subdiv = subdivService.get(form.getSubdivId());
        int coid = subdiv.getComp().getCoid();

        boolean isAllowedToCreateInstrument = alloweInstCreat.getValueByScope(Scope.COMPANY, form.getBusinessCompanyId(), form.getBusinessCompanyId());

        boolean isAllowedToCreateIntrumentWithoutSerialNo = allowInstrumentCreationWithoutSerialNo.getValueByScope(Scope.COMPANY, form.getBusinessCompanyId(), form.getBusinessCompanyId());

        // validate rows
        for (int i = 0; i < form.getRows().size(); i++) {
            ImportedInstrumentsSynthesisRowDTO row = form.getRows().get(i);

            try {
                errors.pushNestedPath("rows[" + i + "]");
                ValidationUtils.invokeValidator(this.rowValidator, row, errors, subdiv, isAllowedToCreateInstrument, isAllowedToCreateIntrumentWithoutSerialNo);

                /*
                 * validation done here because we need subdiv that exist in
                 * this level
                 */
                // address
                if (StringUtils.isNoneBlank(row.getAddress())) {
                    Address address = addressService.findCompanyAddress(coid, null, true, row.getAddress());
                    if (address == null) {
                        errors.rejectValue("address", "error.rest.data.notfound", new String[]{row.getAddress()},
								null);
					}
				}
				// contact
				if (StringUtils.isNoneBlank(row.getContact())) {
					Contact contact = contactService.findCompanyContactByFullName(coid, row.getContact(), true);
					if (contact == null) {
						errors.rejectValue("contact", "error.rest.data.notfound", new String[] { row.getContact() },
								null);
					}
				}
				// try to get service capability if required info are there
				if ((row.getDefaultProcedureId() == null) && row.getDefaultServiceTypeId() != null
						&& row.getInstrumentModelId() != null) {
					ServiceType st = serviceTypeService.get(row.getDefaultServiceTypeId());
					InstrumentModel model = instrumentModelService.getReferenceOrNull(row.getInstrumentModelId());
					if (model != null) {
                        val proc = capabilityService.getActiveCapabilityUsingCapabilityFilter(model.getDescription().getId(),
                            st.getCalibrationType().getServiceType().getServiceTypeId(), subdiv.getSubdivid());
                        if (proc.isPresent()) {
                            val p = proc.get();
                            row.setDefaultProcedureId(p.getId());
                            row.setDefaultProcedureReference(p.getReference());
                        } else {
                            ServiceCapability sc = serviceCapabilityService.getCalService(model, st.getCalibrationType(),
                                subdiv);
                            if (sc != null) {
                                row.setDefaultProcedureId(sc.getCapability().getId());
                                row.setDefaultProcedureReference(sc.getCapability().getReference());
                            }
						}
					}

				}

			} finally {
				errors.popNestedPath();
			}

		}
	}

}
