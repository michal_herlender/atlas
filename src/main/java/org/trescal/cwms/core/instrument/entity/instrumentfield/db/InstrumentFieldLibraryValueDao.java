package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue;

public interface InstrumentFieldLibraryValueDao extends BaseDao<InstrumentFieldLibraryValue,Integer> {

}
