package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension_;

@Repository("PermitedCalibrationExtensionDao")
public class PermitedCalibrationExtensionDaoImpl
        extends BaseDaoImpl<PermitedCalibrationExtension, Integer>
        implements PermitedCalibrationExtensionDao {
	
    @Override
    protected Class<PermitedCalibrationExtension> getEntity() {
        return PermitedCalibrationExtension.class;
    }

	@Override
	public PermitedCalibrationExtension getExtensionForInstrument(Instrument instrument) {
		
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<PermitedCalibrationExtension> query = builder.createQuery(PermitedCalibrationExtension.class);

		Root<PermitedCalibrationExtension> extensionRoot = query.from(PermitedCalibrationExtension.class);
		query.where(builder.equal(extensionRoot.get(PermitedCalibrationExtension_.instrument), instrument));
		try{
			return getEntityManager().createQuery(query).getSingleResult();
		}
		catch(NoResultException e) {
			return null;
		}
	}


}
