package org.trescal.cwms.core.instrument.entity.instrumentusagetype.db;

import java.util.Locale;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrument.entity.instrumenttype.db.InstrumentTypeDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType_;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository("InstrumentUsageTypeDao")
public class InstrumentUsageTypeDaoImpl extends InstrumentTypeDaoImpl<InstrumentUsageType>
		implements InstrumentUsageTypeDao {

	@Override
	protected Class<InstrumentUsageType> getEntity() {
		return InstrumentUsageType.class;
	}

	@Override
	public InstrumentUsageType getByName(String name, Locale locale) {
		
		Criteria crt = getSession().createCriteria(InstrumentUsageType.class);
		Criteria nametranslation = crt.createCriteria(InstrumentUsageType_.nametranslation.getName());
		
		nametranslation.add(Restrictions.like(Translation_.translation.getName(), name));
		nametranslation.add(Restrictions.like(Translation_.locale.getName(), locale));
		
		return (InstrumentUsageType) crt.uniqueResult();
	}
}