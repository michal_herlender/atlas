package org.trescal.cwms.core.instrument.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.IntervalUnitJsonConverter;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@Builder
public class InstrumentCheckDuplicatesDto {

	private Integer plantId;
	private String plantNo;
	private String simplifiedPlantNo;
	private String serialNo;
	private String simplifiedSerialNo;
	private String formerBarcode;
	private Integer subdivId;
	private String subdivName;
	private String customerGroup;
	private String subFamily;

	private Integer coid;
	private String companyName;
	private Integer modelId;
	private String description;
	private ZonedDateTime lastJobInDate;
	private LocalDate lastCalibrationDate;
	private LocalDate nextCalibrationDate;
	private Boolean isActive;
	private Double score;
	private Integer calFrequency;
	@JsonSerialize(using = IntervalUnitJsonConverter.class)
	private IntervalUnit calFrequencyUnit;

}
