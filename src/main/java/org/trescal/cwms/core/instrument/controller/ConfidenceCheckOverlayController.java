package org.trescal.cwms.core.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.checkout.db.CheckOutService;
import org.trescal.cwms.core.instrument.entity.confidencecheck.ConfidenceCheck;
import org.trescal.cwms.core.instrument.entity.confidencecheck.confidencecheckresultenum.ConfidenceCheckResultEnum;
import org.trescal.cwms.core.instrument.entity.confidencecheck.db.ConfidenceCheckService;
import org.trescal.cwms.core.instrument.entity.confidencecheck.dto.ConfidenceCheckDto;
import org.trescal.cwms.core.system.Constants;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class ConfidenceCheckOverlayController {

	@Autowired
	private CheckOutService checkOutService;
	@Autowired
	private ConfidenceCheckService confidenceCheckService;

	public static final String VIEW_NAME = "trescal/core/instrument/Confidencecheck/confidenceCheckOverlay";

	@ModelAttribute("confidenceCheckResults")
	public ConfidenceCheckResultEnum[] results() {
		return ConfidenceCheckResultEnum.values();
	}

	@RequestMapping(method = RequestMethod.GET, value = "confidenceCheckOverlay.htm")
	public String getPage(Model model, @RequestParam(value = "id", required = false) Integer id,
			@RequestParam(value = "action", required = false) String action,
			@ModelAttribute("command") ConfidenceCheckDto dto) {
		
		model.addAttribute("checkoutid", id);
		model.addAttribute("action", action);
		model.addAttribute("checkOut", this.checkOutService.get(id));

		return VIEW_NAME;
	}

	@RequestMapping(method = RequestMethod.POST, value = "confidenceCheckOverlay.htm")
	public String performConfidenceCheck(Model model, @ModelAttribute("command") ConfidenceCheckDto dto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		model.addAttribute("checkOut", this.checkOutService.get(dto.getCheckoutId()));
		model.addAttribute("checkoutid", dto.getCheckoutId());
		ConfidenceCheck confCheck = this.confidenceCheckService.performeConfidenceCheck(dto,
				this.checkOutService.get(dto.getCheckoutId()), username);
		model.addAttribute("confCheck", confCheck);
		return VIEW_NAME;
	}

}
