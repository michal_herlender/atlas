package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;

public interface InstrumentFieldDefinitionService extends BaseService<InstrumentFieldDefinition, Integer> {
	
	public Set<InstrumentFieldDefinition> getInstrumentFieldDefinitionsForCompany(Company company);
	public InstrumentFieldDefinition getForCompanyAndName(Company company, String name);
}
