package org.trescal.cwms.core.instrument.entity.confidencecheck;

import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.confidencecheck.confidencecheckresultenum.ConfidenceCheckResultEnum;

import lombok.Setter;

@Entity
@Table(name = "confidencecheck")
@Setter
public class ConfidenceCheck {

	private Integer id;
	private Contact performedBy;
	private Date performedOn;
	private String description;
	private List<CheckOut> checkOuts;
	private ConfidenceCheckResultEnum result;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "techid", nullable = false)
	public Contact getPerformedBy() {
		return performedBy;
	}

	@NotNull
	@Column(name = "performedOn")
	public Date getPerformedOn() {
		return performedOn;
	}

	@Nullable
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "checkout_confidencecheck", joinColumns = @JoinColumn(name = "confidencecheckid", foreignKey = @ForeignKey(name = "FK_checkout_confidencecheck_linktable_confidence")), inverseJoinColumns = @JoinColumn(name = "checkoutid", foreignKey = @ForeignKey(name = "FK_checkout_confidencecheck__checkout")))
	public List<CheckOut> getCheckOuts() {
		return checkOuts;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "result", length = 10)
	public ConfidenceCheckResultEnum getResult() {
		return result;
	}

}
