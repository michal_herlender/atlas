package org.trescal.cwms.core.instrument.entity.instrument.db;

import lombok.val;
import org.hibernate.criterion.MatchMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.audit.entity.db.CriteriaBuilderEffect;
import org.trescal.cwms.core.audit.entity.db.RootSelectRestrictionOrder;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.NumberTools;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.trescal.cwms.core.instrument.entity.instrument.db.CommonInstrumentCriteriaBuilder.modelPredicate;
import static org.trescal.cwms.core.tools.JpaUtils.checkIfNullOrFalse;
import static org.trescal.cwms.core.tools.JpaUtils.checkIfNullOrZero;
import static org.trescal.cwms.core.tools.StringJpaUtils.checkIfNullOrEmpty;
import static org.trescal.cwms.core.tools.StringJpaUtils.ilikePredicateStream;

class SearchInstrumentPagedCriteriaBuilder {
	
	static CriteriaBuilderEffect<RootSelectRestrictionOrder<Instrument, Instrument>> searchInstrumentPageEffect(
			InstrumentAndModelSearchForm<Instrument> form, Integer subdivid, Integer personid, Integer addressid,
			Boolean outOfCal, NewQuoteInstSortType sortType, boolean ascOrder, Locale locale)  {
		return CriteriaBuilderEffect.lift((cb,cq) -> {
			val instrument = cq.from(Instrument.class);
			val model = instrument.join(Instrument_.model);
			val description = model.join(InstrumentModel_.description);
			val desc_translation = description.join(Description_.translations);
			desc_translation.on(cb.equal(desc_translation.get(Translation_.locale), locale));

			val clauses = cb.and(
			  Stream.of(
			  		  searchAllCompaniesRestriction(cb,instrument,form.getSearchAllCompanies(),form.getCompId(),personid,subdivid,addressid),
					  mfrPredicate(cb,instrument,form.getMfrId()),
                      modelPredicate(cb, model, form.getModel()),
					  (checkIfNullOrZero(form.getDescId()))? descriptionPredicate(cb,model.join(InstrumentModel_.description),form.getDescId())
							  : ilikePredicateStream(cb, desc_translation.get(Translation_.translation), form.getDescNm(), MatchMode.ANYWHERE ) ,
					  descriptionPredicate(cb,description,form.getDescId()),
					  ilikePredicateStream(cb,instrument.get(Instrument_.serialno),form.getSerialNo(),MatchMode.ANYWHERE),
					  ilikePredicateStream(cb,instrument.get(Instrument_.plantno),form.getPlantNo(),MatchMode.ANYWHERE),
					  barcodePredicate(cb,instrument,form.getBarcode()),
					  ilikePredicateStream(cb,instrument.get(Instrument_.customerDescription),form.getCustomerDescription(),MatchMode.ANYWHERE),
					  outOfCallPredicate(cb,instrument,outOfCal)
			  ).flatMap(Function.identity()).toArray(Predicate[]::new)
            );
			val order = sortField(instrument,sortType).map(p -> ascOrder?cb.asc(p):cb.desc(p))
					.map(Collections::singletonList).orElse(Collections.emptyList());
			return RootSelectRestrictionOrder.of(instrument, instrument, clauses,order);
		});
	}

	private static Stream<Predicate> searchAllCompaniesRestriction(CriteriaBuilder cb, Root<Instrument> instrument, Boolean searchAllCompanies, Integer coid, Integer personId, Integer subdivId, Integer addressId) {
		if(!checkIfNullOrFalse(searchAllCompanies))
			return Stream.empty();
		val comp = instrument.join(Instrument_.comp);
		return Stream.of(
				Stream.of(cb.equal(comp,coid)),
				!checkIfNullOrZero(personId)? contactPredicate(cb,instrument,personId): subdivPredicate(cb,instrument,subdivId),
				addressPredicate(cb,instrument,addressId)).flatMap(Function.identity());
	}

	private static Stream<Predicate> contactPredicate(CriteriaBuilder cb, Root<Instrument> instrument, Integer personId){
		if(checkIfNullOrZero(personId))
			return Stream.empty();
		return Stream.of(cb.equal(instrument.join(Instrument_.con),personId));
	}

	private static Stream<Predicate> subdivPredicate(CriteriaBuilder cb, Root<Instrument> instrument, Integer subdivId) {
		if (checkIfNullOrZero(subdivId))
			return Stream.empty();
		return Stream.of(cb.equal(instrument.join(Instrument_.con).join(Contact_.sub), subdivId));
	}

	private static Stream<Predicate> addressPredicate(CriteriaBuilder cb, Root<Instrument> instrument, Integer address) {
		if (checkIfNullOrZero(address))
			return Stream.empty();
		return Stream.of(cb.equal(instrument.join(Instrument_.add), address));
	}

	private static Stream<Predicate> mfrPredicate(CriteriaBuilder cb, Root<Instrument> instrument, Integer mfrId) {

		if (checkIfNullOrZero(mfrId))
			return Stream.empty();
		val instMfr = instrument.join(Instrument_.mfr,javax.persistence.criteria.JoinType.LEFT);
		val model = instrument.join(Instrument_.model);
		val modelMfr = model.join(InstrumentModel_.mfr,javax.persistence.criteria.JoinType.LEFT);
		return Stream.of(cb.or(cb.equal(instMfr, mfrId), cb.equal(modelMfr, mfrId)));
	}

	private static Stream<Predicate> descriptionPredicate(CriteriaBuilder cb, Join<InstrumentModel, Description> description	, Integer descriptionId) {
		if (checkIfNullOrZero(descriptionId))
			return Stream.empty();
		return Stream.of(cb.equal(description, descriptionId));
	}

	private static Stream<Predicate> barcodePredicate(CriteriaBuilder cb, Root<Instrument> instrument, String barcode) {
		if (!checkIfNullOrEmpty(barcode) && NumberTools.isAnInteger(barcode))
			return Stream.of(cb.equal(instrument, Integer.parseInt(barcode)));
		else
			return Stream.empty();
	}

	private static Stream<Predicate> outOfCallPredicate(CriteriaBuilder cb, Root<Instrument> instrument, Boolean outOfCal) {
        if (outOfCal == null)
            return Stream.empty();
        val nexCall = instrument.get(Instrument_.nextCalDueDate);
        val date = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        return Stream.of(cb.or(cb.isNull(nexCall),
            outOfCal ? cb.lessThan(nexCall, date) : cb.greaterThan(nexCall, date)
        ));
    }


	private static Optional<Path<?>> sortField(Root<Instrument> instrument, NewQuoteInstSortType sortType) {
		switch (sortType) {
			case BARCODE:
				return Optional.of(instrument.get(Instrument_.plantid));
			case CALDUEDATE:
				return Optional.of(instrument.get(Instrument_.nextCalDueDate));
			case PLANTNO:
				return Optional.of(instrument.get(Instrument_.plantno));
			case SERIALNO:
				return Optional.of(instrument.get(Instrument_.serialno));
		}
		return Optional.empty();
	}
}
