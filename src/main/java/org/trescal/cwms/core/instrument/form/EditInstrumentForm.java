package org.trescal.cwms.core.instrument.form;

import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;
import java.util.Map;

public class EditInstrumentForm {
	private Integer addrid;
	private Integer calFrequency;
	private String calFrequencyUnitId;
	private String translatedCalFrequencyUnit;

	private Integer coid;
	private CompanyRole companyRole;
	private Date lastModified;
	private Integer locid;
	private Integer mfrid;
	private String mfrname;
	private Integer modelid;
	private String modelName;
	private ModelMfrType modelMfrType;
	private LocalDate nextCalDueDate;
	private Integer personid;
	private Integer plantId;
	private String plantNo;
	private Integer procid;
	private String procedureReference;
	private String instStartRange;
	private String instEndRange;
	private Integer instUomId;

	// Hire instrument fields 
	private boolean hireInstrumentReq;
	private String serialNo;
	// instrument history tracked variables
	private Integer statusid;
	private InstrumentStatus status;
	private Integer storageTypeId;
	private Integer threadid;
	private Integer usageTypeId;
	private Integer wiid;
	private String workInstructionTitle;
	private Map<Integer, InstrumentCharacteristic> characteristicMap;
	private Boolean calExpiresAfterNumberOfUses;
	private Integer permittedNumberOfUses;
	private Integer usesSinceLastCalibration;
	private Boolean calibrationStandard;
	private String firmwareAccessCode;
	private String customerDescription;
    private String customerSpecification;
    private String freeTextLocation;
    private String clientBarcode;
	private boolean updateOnBehalf;
	private Boolean customerManaged;
	private String calibrationInstructions;
	private Integer defaultServiceTypeId;
    private String customerRemarks;
    private CalibrationVerificationStatus lastCertStatusOverride;
       
    private Integer businessCompanyId;
    private String 	defaultClientRef;

    
    
	public EditInstrumentForm() {
	}

	public EditInstrumentForm(Integer addrid, Integer calFrequency, Integer coid, CompanyRole companyRole,
							  Date lastModified, Integer locid, Integer mfrid, String mfrname, Integer modelid, String modelName,
							  ModelMfrType modelMfrType, LocalDate nextCalDueDate, Integer personid, Integer plantId, String plantNo,
							  Integer procid, String procedureReference, String serialNo, InstrumentStatus status, Integer storageTypeId,
							  Integer usageTypeId, Boolean calExpiresAfterNumberOfUses, Integer permittedNumberOfUses,
							  Integer usesSinceLastCalibration, Boolean calibrationStandard, String firmwareAccessCode,
							  String customerDescription, String customerSpecification, String freeTextLocation, String clientBarcode,
							  Boolean customerManaged) {
		super();
		this.addrid = addrid;
		this.calFrequency = calFrequency;
		this.coid = coid;
		this.companyRole = companyRole;
		this.lastModified = lastModified;
		this.locid = locid;
		this.mfrid = mfrid;
		this.mfrname = mfrname;
		this.modelid = modelid;
		this.modelName = modelName;
		this.modelMfrType = modelMfrType;
		this.nextCalDueDate = nextCalDueDate;
		this.personid = personid;
		this.plantId = plantId;
		this.plantNo = plantNo;
		this.procid = procid;
		this.procedureReference = procedureReference;
		this.serialNo = serialNo;
		this.status = status;
		this.storageTypeId = storageTypeId;
		this.usageTypeId = usageTypeId;
		this.calExpiresAfterNumberOfUses = calExpiresAfterNumberOfUses;
		this.permittedNumberOfUses = permittedNumberOfUses;
		this.usesSinceLastCalibration = usesSinceLastCalibration;
		this.calibrationStandard = calibrationStandard;
		this.firmwareAccessCode = firmwareAccessCode;
		this.customerDescription = customerDescription;
		this.customerSpecification = customerSpecification;
		this.freeTextLocation = freeTextLocation;
		this.clientBarcode = clientBarcode;
		this.customerManaged = customerManaged;
	}

	@NotNull(message = "error.instrument.address.notnull")
	@Min(value=1, message = "error.instrument.address.notnull")
	public Integer getAddrid() {
		return this.addrid;
	}

	@NotNull(message = "error.instrument.company.notnull")
	@Min(value=1, message = "error.instrument.company.notnull")
	public Integer getCoid() {
		return this.coid;
	}

	public Integer getLocid() {
		return this.locid;
	}

	public Integer getMfrid() {
		return this.mfrid;
	}

	public String getMfrname() {
		return this.mfrname;
	}

	@NotNull(message = "error.instrument.nomodel")
	public Integer getModelid() {
		return this.modelid;
	}

	@NotNull(message = "error.instrument.contact.notnull")
	@Min(value=1, message = "error.instrument.contact.notnull")
	public Integer getPersonid() {
		return this.personid;
	}

	public Integer getPlantId() {
		return plantId;
	}

	public String getPlantNo() {
		return this.plantNo;
	}

	public Integer getProcid() {
		return this.procid;
	}

	public String getSerialNo() {
		return this.serialNo;
	}

	public Integer getStatusid() {
		return this.statusid;
	}

	public Integer getStorageTypeId() {
		return this.storageTypeId;
	}

	public Integer getThreadid() {
		return this.threadid;
	}

	public Integer getUsageTypeId() {
		return this.usageTypeId;
	}

	public Integer getWiid() {
		return this.wiid;
	}

	public boolean isHireInstrumentReq() {
		return this.hireInstrumentReq;
	}

	public void setAddrid(Integer addrid) {
		this.addrid = addrid;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}

	public void setHireInstrumentReq(boolean hireInstrumentReq) {
		this.hireInstrumentReq = hireInstrumentReq;
	}

	public void setLocid(Integer locid) {
		this.locid = locid;
	}

	public void setMfrid(Integer mfrid) {
		this.mfrid = mfrid;
	}

	public void setMfrname(String mfrname) {
		this.mfrname = mfrname;
	}

	public void setModelid(Integer modelid) {
		this.modelid = modelid;
	}

	public void setPersonid(Integer personid) {
		this.personid = personid;
	}

	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}

	public void setPlantNo(String plantNo) {
		this.plantNo = plantNo;
	}

	public void setProcid(Integer procid) {
		this.procid = procid;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public void setStatusid(Integer statusid) {
		this.statusid = statusid;
		this.status = InstrumentStatus.values()[statusid];
	}

	public void setStorageTypeId(Integer storageTypeId) {
		this.storageTypeId = storageTypeId;
	}

	public void setThreadid(Integer threadid) {
		this.threadid = threadid;
	}

	public void setUsageTypeId(Integer usageTypeId) {
		this.usageTypeId = usageTypeId;
	}

	public void setWiid(Integer wiid) {
		this.wiid = wiid;
	}

	public Map<Integer, InstrumentCharacteristic> getCharacteristicMap() {
		return characteristicMap;
	}

	public void setCharacteristicMap(Map<Integer, InstrumentCharacteristic> characteristicMap) {
		this.characteristicMap = characteristicMap;
	}

	public Boolean getCalExpiresAfterNumberOfUses() {
		return calExpiresAfterNumberOfUses;
	}

	public void setCalExpiresAfterNumberOfUses(Boolean calExpiresAfterNumberOfUses) {
		this.calExpiresAfterNumberOfUses = calExpiresAfterNumberOfUses;
	}

	public Integer getPermittedNumberOfUses() {
		return permittedNumberOfUses;
	}

	public void setPermittedNumberOfUses(Integer permittedNumberOfUses) {
		this.permittedNumberOfUses = permittedNumberOfUses;
	}

	public Integer getUsesSinceLastCalibration() {
		return usesSinceLastCalibration;
	}

	public void setUsesSinceLastCalibration(Integer usesSinceLastCalibration) {
		this.usesSinceLastCalibration = usesSinceLastCalibration;
	}

	public Boolean getCalibrationStandard() {
		return calibrationStandard;
	}

	public void setCalibrationStandard(Boolean calibrationStandard) {
		this.calibrationStandard = calibrationStandard;
	}

	public String getFirmwareAccessCode() {
		return firmwareAccessCode;
	}

	public void setFirmwareAccessCode(String firmwareAccessCode) {
		this.firmwareAccessCode = firmwareAccessCode;
	}

	public String getCustomerDescription() {
		return customerDescription;
	}

	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public ModelMfrType getModelMfrType() {
		return modelMfrType;
	}

	public void setModelMfrType(ModelMfrType modelMfrType) {
		this.modelMfrType = modelMfrType;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public LocalDate getNextCalDueDate() {
		return nextCalDueDate;
	}

	public void setNextCalDueDate(LocalDate nextCalDueDate) {
		this.nextCalDueDate = nextCalDueDate;
	}

	@NotNull(message = "Calibration frequency must be a number of months, e.g. 12")
	public Integer getCalFrequency() {
		return calFrequency;
	}

	public void setCalFrequency(Integer calFrequency) {
		this.calFrequency = calFrequency;
	}

	public String getWorkInstructionTitle() {
		return workInstructionTitle;
	}

	public void setWorkInstructionTitle(String workInstructionTitle) {
		this.workInstructionTitle = workInstructionTitle;
	}

	public String getProcedureReference() {
		return procedureReference;
	}

	public void setProcedureReference(String procedureReference) {
		this.procedureReference = procedureReference;
	}

	public CompanyRole getCompanyRole() {
		return companyRole;
	}

	public void setCompanyRole(CompanyRole companyRole) {
		this.companyRole = companyRole;
	}

	public boolean getUpdateOnBehalf() {
		return updateOnBehalf;
	}

	public void setUpdateOnBehalf(boolean updateOnBehalf) {
		this.updateOnBehalf = updateOnBehalf;
	}

	public Boolean getCustomerManaged() {
		return customerManaged;
	}

	public void setCustomerManaged(Boolean customerManaged) {
		this.customerManaged = customerManaged;
	}

	public InstrumentStatus getStatus() {
		return status;
	}

	public void setStatus(InstrumentStatus status) {
		this.status = status;
		this.statusid = status.ordinal();
	}

	public String getCalibrationInstructions() {
		return calibrationInstructions;
	}

	public void setCalibrationInstructions(String specialInstructions) {
		this.calibrationInstructions = specialInstructions;
	}

	public String getCalFrequencyUnitId() {
		return calFrequencyUnitId;
	}

	public void setCalFrequencyUnitId(String calFrequencyUnitId) {
		this.calFrequencyUnitId = calFrequencyUnitId;
	}

	public String getTranslatedCalFrequencyUnit() {
		return translatedCalFrequencyUnit;
	}

	public void setTranslatedCalFrequencyUnit(String translatedCalFrequencyUnit) {
		this.translatedCalFrequencyUnit = translatedCalFrequencyUnit;
	}

	public Integer getDefaultServiceTypeId() {
		return defaultServiceTypeId;
	}

	public void setDefaultServiceTypeId(Integer defaultServiceTypeId) {
		this.defaultServiceTypeId = defaultServiceTypeId;
	}

    public String getCustomerRemarks() {
        return customerRemarks;
    }

    public void setCustomerRemarks(String customerRemarks) {
        this.customerRemarks = customerRemarks;
    }

    public boolean isUpdateOnBehalf() {
        return updateOnBehalf;
    }

    public CalibrationVerificationStatus getLastCertStatusOverride() {
        return lastCertStatusOverride;
    }

    public void setLastCertStatusOverride(CalibrationVerificationStatus lastCertStatusOverride) {
        this.lastCertStatusOverride = lastCertStatusOverride;
    }

	public Integer getBusinessCompanyId() {
		return businessCompanyId;
	}

	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}
	
	@Size(max=30)
	public String getDefaultClientRef() {
		return defaultClientRef;
	}

	public void setDefaultClientRef(String defaultClientRef) {
		this.defaultClientRef = defaultClientRef;
	}

	@Size(max=100)
	public String getCustomerSpecification() {
		return customerSpecification;
	}

	@Size(max=64)
	public String getFreeTextLocation() {
		return freeTextLocation;
	}

	@Size(max=50)
	public String getClientBarcode() {
		return clientBarcode;
	}

	public void setCustomerSpecification(String customerSpecification) {
		this.customerSpecification = customerSpecification;
	}

	public void setFreeTextLocation(String freeTextLocation) {
		this.freeTextLocation = freeTextLocation;
	}

	public void setClientBarcode(String clientBarcode) {
		this.clientBarcode = clientBarcode;
	}

	public String getInstStartRange() {
		return instStartRange;
	}

	public void setInstStartRange(String instStartRange) {
		this.instStartRange = instStartRange;
	}

	public String getInstEndRange() {
		return instEndRange;
	}

	public void setInstEndRange(String instEndRange) {
		this.instEndRange = instEndRange;
	}

	public Integer getInstUomId() {
		return instUomId;
	}

	public void setInstUomId(Integer instUomId) {
		this.instUomId = instUomId;
	}
	
}