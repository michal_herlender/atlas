package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@PrimaryKeyJoinColumn(name="instrumentFieldValueid")  
@Table(name = "instrumentvaluestring")
public class InstrumentValueString extends InstrumentValue {
	
	private String value;
	
	@NotNull
	@Column(name = "value", nullable = false, columnDefinition = "nvarchar(2000)")
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}