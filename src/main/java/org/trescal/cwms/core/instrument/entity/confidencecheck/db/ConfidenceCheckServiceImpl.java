package org.trescal.cwms.core.instrument.entity.confidencecheck.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.confidencecheck.ConfidenceCheck;
import org.trescal.cwms.core.instrument.entity.confidencecheck.confidencecheckresultenum.ConfidenceCheckResultEnum;
import org.trescal.cwms.core.instrument.entity.confidencecheck.dto.ConfidenceCheckDto;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.CollectedInstrument;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db.CollectedInstrumentService;
import org.trescal.cwms.core.login.entity.user.db.UserService;

@Service
public class ConfidenceCheckServiceImpl extends BaseServiceImpl<ConfidenceCheck, Integer>
		implements ConfidenceCheckService {

	@Autowired
	ConfidenceCheckDao dao;
	@Autowired
	UserService userservice;
	@Autowired
	CollectedInstrumentService colInstserv;

	@Override
	protected BaseDao<ConfidenceCheck, Integer> getBaseDao() {
		return this.dao;
	}

	@Override
	public ConfidenceCheck performeConfidenceCheck(ConfidenceCheckDto dto, CheckOut checkout, String username) {

		ConfidenceCheck confCheck = new ConfidenceCheck();
		confCheck.setDescription(dto.getDescription());
		confCheck.setPerformedOn(new Date());
		confCheck.setPerformedBy(this.userservice.get(username).getCon());
		confCheck.setResult(dto.getResult());

		List<CheckOut> ch = new ArrayList<CheckOut>();
		ch.add(checkout);

		confCheck.setCheckOuts(ch);

		this.merge(confCheck);

		if (dto.getResult().equals(ConfidenceCheckResultEnum.FAIL)) {
			CollectedInstrument colInst = new CollectedInstrument();
			colInst.setCollectedBy(this.userservice.get(username).getCon());
			colInst.setCollectStamp(new Date());
			colInst.setInst(checkout.getInstrument());
			colInst.setModel(checkout.getInstrument().getModel());
			colInst.setNumItems(1);

			this.colInstserv.insertCollectedInstrument(colInst);

		}
		
		return confCheck;
	}

}
