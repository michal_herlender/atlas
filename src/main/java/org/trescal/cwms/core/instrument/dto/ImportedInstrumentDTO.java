package org.trescal.cwms.core.instrument.dto;

import java.util.Date;

import org.trescal.cwms.core.system.enums.IntervalUnit;

public class ImportedInstrumentDTO {

	private String importedSerialNo;
	private String importedPlantNo;
	private String importedModel;
	private String importedManufacturer;
	
	private String importedFrequency;
	private Integer	frequency;
	private String importedFrequencyunit;
	private IntervalUnit calFrequencyUnit;
	private Date importedNextcaldate;
	
	private String importedServicetype;
	private Integer defaultServiceTypeId;
	private String importedUsagetype;
	private Integer usageTypeId;
	
	private String importedContact;
	private Integer contactId;
	private String importedAddress;
	private Integer addressId;
	private String importedCustomermanaged;
	private boolean customerManaged;
	private String importedDescription;
	
	private String importedDefaultClientRef;

	public String getImportedSerialNo() {
		return importedSerialNo;
	}

	public void setImportedSerialNo(String importedSerialNo) {
		this.importedSerialNo = importedSerialNo;
	}

	public String getImportedPlantNo() {
		return importedPlantNo;
	}

	public void setImportedPlantNo(String importedPlantNo) {
		this.importedPlantNo = importedPlantNo;
	}

	public String getImportedModel() {
		return importedModel;
	}

	public void setImportedModel(String importedModel) {
		this.importedModel = importedModel;
	}

	public String getImportedManufacturer() {
		return importedManufacturer;
	}

	public void setImportedManufacturer(String manufacturer) {
		importedManufacturer = manufacturer;
	}

	public String getImportedFrequency() {
		return importedFrequency;
	}

	public void setImportedFrequency(String importedFrequency) {
		this.importedFrequency = importedFrequency;
	}

	public String getImportedFrequencyunit() {
		return importedFrequencyunit;
	}

	public void setImportedFrequencyunit(String importedFrequencyunit) {
		this.importedFrequencyunit = importedFrequencyunit;
	}

	public Date getImportedNextcaldate() {
		return importedNextcaldate;
	}

	public void setImportedNextcaldate(Date importedNextcaldate) {
		this.importedNextcaldate = importedNextcaldate;
	}

	public String getImportedServicetype() {
		return importedServicetype;
	}

	public void setImportedServicetype(String importedServicetype) {
		this.importedServicetype = importedServicetype;
	}

	public String getImportedUsagetype() {
		return importedUsagetype;
	}

	public void setImportedUsagetype(String importedUsagetype) {
		this.importedUsagetype = importedUsagetype;
	}

	public String getImportedContact() {
		return importedContact;
	}

	public void setImportedContact(String importedContact) {
		this.importedContact = importedContact;
	}

	public String getImportedAddress() {
		return importedAddress;
	}

	public void setImportedAddress(String importedAddress) {
		this.importedAddress = importedAddress;
	}

	public String getImportedCustomermanaged() {
		return importedCustomermanaged;
	}

	public void setImportedCustomermanaged(String importedCustomermanaged) {
		this.importedCustomermanaged = importedCustomermanaged;
	}

	public String getImportedDescription() {
		return importedDescription;
	}

	public void setImportedDescription(String importedDescription) {
		this.importedDescription = importedDescription;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public IntervalUnit getCalFrequencyUnit() {
		return calFrequencyUnit;
	}
	
	public void setCalFrequencyUnit(IntervalUnit calFrequencyUnit) {
		this.calFrequencyUnit = calFrequencyUnit;
	}

	public Integer getDefaultServiceTypeId() {
		return defaultServiceTypeId;
	}

	public void setDefaultServiceTypeId(Integer defaultServiceTypeId) {
		this.defaultServiceTypeId = defaultServiceTypeId;
	}

	public Integer getUsageTypeId() {
		return usageTypeId;
	}

	public void setUsageTypeId(Integer usageType) {
		this.usageTypeId = usageType;
	}

	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public Boolean getCustomerManaged() {
		return customerManaged;
	}

	public void setCustomerManaged(Boolean customerManaged) {
		this.customerManaged = customerManaged;
	}

	public String getImportedDefaultClientRef() {
		return importedDefaultClientRef;
	}

	public void setImportedDefaultClientRef(String importedDefaultClientRef) {
		this.importedDefaultClientRef = importedDefaultClientRef;
	}
	
}
