package org.trescal.cwms.core.instrument.dto;

import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.Getter;
import lombok.Setter;

/**
 * Designed to hold relevant identifying information for an instrument
 * Referenced in JobItemProjectionDTO to create projection graph
 */

@Getter @Setter
public class InstrumentProjectionDTO {
	// Below result fields always populated by query
	private Integer plantid;
	private String plantno;
	private String formerBarcode;
	private String serialno;
	private Boolean scrapped;
	
	private Boolean calibrationStandard;
	private Integer modelid;						// From instrumentmodel
	private String modelname;						// From instrumentmodel (model field)
	private String instrumentModelTranslation;		// From instrumentmodel (translated)
	private ModelMfrType modelMfrType;				// From instrumentmodel
	
	private SubfamilyTypology subfamilyTypology;	// From instrumentmodel
	private Integer instrumentMfrId;							// From instrument.mfr
	private Boolean instrumentMfrGeneric;			// From instrument.mfr
	private String instrumentMfrName;				// From instrument.mfr
	private String instrumentModelName;				// From instrument,modelname
	
	private String customerDescription;
	private Integer addressId;
	private Integer contactId;
	private Integer locationId;
	
	// Optional references populated after DTO creation
	private AddressProjectionDTO address;
	private ContactProjectionDTO contact;
	private KeyValueIntegerString location;

	public InstrumentProjectionDTO(Integer plantid, String plantno, String formerBarcode, String serialno, Boolean scrapped, 
			Boolean calibrationStandard, Integer modelid, String modelname, String instrumentModelTranslation, ModelMfrType modelMfrType, 
			Integer typology, Integer instrumentMfrId, Boolean instrumentMfrGeneric, String instrumentMfrName, String instrumentModelName,
			String customerDescription, Integer addressId, Integer contactId, Integer locationId) {
		super();
		this.plantid = plantid;
		this.plantno = plantno;
		this.formerBarcode = formerBarcode;
		this.serialno = serialno;
		this.scrapped = scrapped;
		
		this.calibrationStandard = calibrationStandard;
		this.modelid = modelid;
		this.modelname = modelname;
		this.instrumentModelTranslation = instrumentModelTranslation;
		this.modelMfrType = modelMfrType;
		
		this.subfamilyTypology = SubfamilyTypology.convertFromInteger(typology);
		this.instrumentMfrId = instrumentMfrId;
		this.instrumentMfrGeneric = instrumentMfrGeneric;
		this.instrumentMfrName = instrumentMfrName;
		this.instrumentModelName = instrumentModelName;
		
		this.customerDescription = customerDescription;
		this.addressId = addressId;
		this.contactId = contactId;
		this.locationId = locationId;
	}
	
	public InstrumentProjectionDTO(Integer plantid, String plantno, String serialno, 
			String customerDescription, Boolean calibrationStandard, Boolean scrapped, 
			Integer modelid, String instrumentModelTranslation, ModelMfrType modelMfrType, Integer subfamilyTypology,
			Boolean instrumentMfrGeneric, String instrumentMfrName, String instrumentModelName) {
		super();
		this.plantid = plantid;
		this.plantno = plantno;
		this.serialno = serialno;
		this.scrapped = scrapped;
		this.calibrationStandard = calibrationStandard;
		this.modelid = modelid;
		this.instrumentModelTranslation = instrumentModelTranslation;
		this.customerDescription = customerDescription;
		if(subfamilyTypology != null)
			this.subfamilyTypology = SubfamilyTypology.convertFromInteger(subfamilyTypology);
		this.instrumentMfrGeneric = instrumentMfrGeneric;
		this.instrumentMfrName = instrumentMfrName;
		this.instrumentModelName = instrumentModelName;
		this.modelMfrType = modelMfrType;
	}

	public String getInstrumentModelNameViaFields() {
		return getInstrumentModelNameViaFields(false);
	}
	public String getInstrumentModelNameViaFields(boolean hideTypology) {
		return InstModelTools.instrumentModelNameViaFields(instrumentModelTranslation, subfamilyTypology, modelMfrType, instrumentMfrName, instrumentMfrGeneric, 
				modelname, instrumentModelName, hideTypology);
	}
}
