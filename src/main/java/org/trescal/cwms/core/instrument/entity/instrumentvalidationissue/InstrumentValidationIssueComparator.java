package org.trescal.cwms.core.instrument.entity.instrumentvalidationissue;

import java.util.Comparator;

/**
 * Comparator for {@link InstrumentValidationIssue} which sorts issues by the
 * date they were raised or by id if the dates are identical.
 * 
 * @author richard
 */
public class InstrumentValidationIssueComparator implements Comparator<InstrumentValidationIssue>
{
	@Override
	public int compare(InstrumentValidationIssue o1, InstrumentValidationIssue o2)
	{
		if (o1.getRaisedOn().equals(o2.getRaisedOn()))
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
		else
		{
			return o2.getRaisedOn().compareTo(o1.getRaisedOn());
		}
	}
}
