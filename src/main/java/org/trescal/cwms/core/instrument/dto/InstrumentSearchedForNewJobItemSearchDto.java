package org.trescal.cwms.core.instrument.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class InstrumentSearchedForNewJobItemSearchDto {

    private final Integer plantId;
    private final String formerBarcode;
    private final Integer modelId;
    private final String modelName;
    private final String customerDescription;
    private final InstrumentStatus status;
    private final Boolean scrapped;
    private final String serialNo;
    private final String plantNo;
    private final String defaultServiceType;
    private final Integer defaultServiceTypeId;
    private final Boolean isOnJob;
    private final Boolean isOnCurrentJob;
    private final LocalDate nextCalibrationDueDate;
    private final LocalDate lastCalibrationDate;
    private final LocalDate addedOnDate;
    private final String companyName;
    private final Integer companyId;
    private Map<Integer,String> availableQuotations = Collections.emptyMap();


}
