package org.trescal.cwms.core.instrument.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ViewCompanyInstrumentValidator implements Validator
{
	public static final String ERROR_CODE = "error.cleaning.barcodenotrecognised";
	public static final String DEFAULT_MESSAGE = "Barcode {0} not recognized as an active item on a jobb";
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(ViewCompanyInstrumentForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		ViewCompanyInstrumentForm vcif = (ViewCompanyInstrumentForm) obj;

		if (vcif == null)
		{
			errors.reject("error.nullpointer", "Null data received");
		}
		else
		{
			if ((vcif.getSubmitType().equalsIgnoreCase("createquoteinsts") ||
				vcif.getSubmitType().equalsIgnoreCase("updatecompinsts"))
					&& ((vcif.getBasketIds() == null) || (vcif.getBasketIds().size() == 0)))
			{
				errors.rejectValue("basketIds", null, "Please select at least one company instrument");
			}

			if ((vcif.getSubmitType().equalsIgnoreCase("createquoteinsts"))
					&& ((vcif.getQuoteContact() == null) || (vcif.getQuoteContact() == 0)))
			{
				errors.rejectValue("error.quoteContact", "quoteContact You have not selected a contact for your quotation");
			}
		}
	}
}
