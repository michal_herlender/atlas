package org.trescal.cwms.core.instrument.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;

@Controller
@IntranetController
@RequestMapping(value = "/addinstrumentmodelsearch.htm")
public class AddInstrumentModelSearchController {

	@Autowired
	private InstrumentModelService instModelServ;

	@ModelAttribute("addinstrumentmodelsearchform")
	public InstrumentAndModelSearchForm<InstrumentModel> createFormBackingObject(Locale locale,
			@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId) {
		InstrumentAndModelSearchForm<InstrumentModel> form = new InstrumentAndModelSearchForm<>();
		form.setLocale(locale);
		if (plantId > 0) {
			form.setPlantId(plantId);
		} else {
			form.setPlantId(0);
		}
		return form;
	}

	@GetMapping
	public String displayForm() {
		return "trescal/core/instrument/AddInstrumentModelSearch";
	}

	@PostMapping
	public String processForm(
			@ModelAttribute("addinstrumentmodelsearchform") InstrumentAndModelSearchForm<InstrumentModel> instrumentAndModelSearchForm) {
		// Only 'real' model types can become an instrument..
		instrumentAndModelSearchForm.setExcludeCapabilityModelTypes(true);
		instrumentAndModelSearchForm.setExcludeSalesCategoryModelTypes(true);
		instrumentAndModelSearchForm.setExcludeQuarantined(true);
		// Get results
		PagedResultSet<InstrumentModel> rs = this.instModelServ.searchInstrumentModelsPaged(new PagedResultSet<>(
				instrumentAndModelSearchForm.getResultsPerPage() == null
						|| instrumentAndModelSearchForm.getResultsPerPage() == 0
								? Constants.RESULTS_PER_PAGE
								: instrumentAndModelSearchForm.getResultsPerPage(),
				instrumentAndModelSearchForm.getPageNo() == null || instrumentAndModelSearchForm.getPageNo() == 0 ? 1
						: instrumentAndModelSearchForm.getPageNo()),
				instrumentAndModelSearchForm);
		instrumentAndModelSearchForm.setRs(rs);
		return "trescal/core/instrument/AddInstrumentModelSearchResults";
	}
}