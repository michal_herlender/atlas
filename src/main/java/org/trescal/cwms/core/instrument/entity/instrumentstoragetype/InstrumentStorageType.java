package org.trescal.cwms.core.instrument.entity.instrumentstoragetype;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumenttype.InstrumentType;
import org.trescal.cwms.core.system.entity.translation.Translation;

/**
 * Defines the different storage types that can be applied to an
 * {@link Instrument}, for example Stores, Personally held etc.
 * 
 * @author Richard
 */
@Entity
@Table(name = "instrumentstoragetype")
public class InstrumentStorageType extends InstrumentType
{
	private Set<Instrument> instruments;
	private Set<Translation> descriptiontranslation;
	private Set<Translation> nametranslation;
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="instrumentstoragetypedescriptiontranslation", joinColumns=@JoinColumn(name="typeid"))
	public Set<Translation> getDescriptiontranslation() {
		return descriptiontranslation;
	}

	public void setDescriptiontranslation(Set<Translation> descriptiontranslation) {
		this.descriptiontranslation = descriptiontranslation;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="instrumentstoragetypenametranslation", joinColumns=@JoinColumn(name="typeid"))
	public Set<Translation> getNametranslation() {
		return nametranslation;
	}

	public void setNametranslation(Set<Translation> nametranslation) {
		this.nametranslation = nametranslation;
	}

	@OneToMany(mappedBy = "storageType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Instrument> getInstruments()
	{
		return this.instruments;
	}

	public void setInstruments(Set<Instrument> instruments)
	{
		this.instruments = instruments;
	}
}
