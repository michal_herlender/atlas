package org.trescal.cwms.core.instrument.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTask;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTaskService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.instrument.form.ImportInstrumentsForm;
import org.trescal.cwms.core.instrument.form.ImportInstrumentsFormValidator;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class BatchImportInstrumentsController {

	public static final String FORM = "form";
	@Autowired
	private BackgroundTaskService bgTaskService;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private ImportInstrumentsFormValidator validator;
	@Autowired
	private TranslationService translationService;

	@InitBinder(FORM)
	protected void initBinder(ServletRequestDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM)
	protected ImportInstrumentsForm getImportInstrumentsForm() {
		return new ImportInstrumentsForm();
	}

	@ModelAttribute("autoSubmitPolicies")
	protected List<KeyValue<String, String>> getAutoSubmitPolicies() {
		return Arrays.asList(AutoSubmitPolicyEnum.values()).stream().map(v -> new KeyValue<>(v.name(), v.getValue()))
				.collect(Collectors.toList());
	}

	@ModelAttribute("serviceTypes")
	public List<KeyValue<Integer, String>> initializeServiceTypes(Locale locale) {
		return this.serviceTypeService.getAll().stream()
				.map(st -> new KeyValue<>(st.getServiceTypeId(),
						this.translationService.getCorrectTranslation(st.getShortnameTranslation(), locale)))
				.collect(Collectors.toList());
	}

	@GetMapping(value = "/importinstrumentsbatch.htm")
	public String doGet(Locale locale, Model model) {
		return "trescal/core/instrument/batchImportInstruments";
	}

	@PostMapping(value = "/importinstrumentsbatch.htm")
	public String submitForms(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@Valid @ModelAttribute(FORM) ImportInstrumentsForm form, BindingResult bindingResult, Locale locale,
			Model model) throws IOException {

		if (bindingResult.hasErrors()) {
			return doGet(locale, model);
		}

		User user = userService.get(username);
		BackgroundTask bgTask = null;

		try {

			ExchangeFormatFile efFile = efFileService.createEfFile(user.getCon(), form.getFile(),
					new ExchangeFormatFile());
			bgTask = bgTaskService.submitImportInstrumentsBgTask(form, user.getCon().getId(), user.getCon().getId(),
					subdivDto.getKey(), efFile, locale.toString());
			model.addAttribute("success", true);
			model.addAttribute("message", (messageSource.getMessage("importcalibrations.bgtasksubmitted",
					new Object[] { bgTask.getId() }, null, locale)));
			model.addAttribute("jobId", bgTask.getId());

		} catch (JobExecutionAlreadyRunningException e) {
			model.addAttribute("success", false);
			model.addAttribute("message",
					(messageSource.getMessage("importcalibrations.jobalreadyrunning", new Object[] {}, null, locale)));

		} catch (JobRestartException e) {
			model.addAttribute("success", false);
			model.addAttribute("message",
					(messageSource.getMessage("importcalibrations.jobrestarterror", new Object[] {}, null, locale)));

		} catch (JobInstanceAlreadyCompleteException e) {
			model.addAttribute("success", false);
			model.addAttribute("message", (messageSource.getMessage("importcalibrations.jobalreadycompleted",
					new Object[] {}, null, locale)));

		} catch (JobParametersInvalidException e) {
			model.addAttribute("success", false);
			model.addAttribute("message", (messageSource.getMessage("importcalibrations.invalidjobparametres",
					new Object[] {}, null, locale)));
		}

		model.addAttribute("personid", user.getCon().getPersonid());
		return "trescal/core/instrument/batchImportInstruments";
	}

}
