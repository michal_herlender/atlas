package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueDateTime;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueDateTime_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentValueDateTimeDTO;

@Repository
public class InstrumentValueDateTimeDaoImpl extends BaseDaoImpl<InstrumentValueDateTime, Integer> implements InstrumentValueDateTimeDao {
	
	@Override
	protected Class<InstrumentValueDateTime> getEntity() {
		return InstrumentValueDateTime.class;
	}
	
	@Override
	public List<InstrumentValueDateTimeDTO> getAll(InstrumentFieldDefinition fieldDefinition) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<InstrumentValueDateTimeDTO> cq = cb.createQuery(InstrumentValueDateTimeDTO.class);
		Root<InstrumentValueDateTime> val = cq.from(InstrumentValueDateTime.class);
		Join<InstrumentValueDateTime, Instrument> instr = val.join(InstrumentValueDateTime_.instrument);
		Join<InstrumentValueDateTime, InstrumentFieldDefinition> field = val.join(InstrumentValueDateTime_.instrumentFieldDefinition);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(val.get(InstrumentValueDateTime_.instrumentFieldDefinition), fieldDefinition));
		cq.select(cb.construct(InstrumentValueDateTimeDTO.class,
				instr.get(Instrument_.plantid),
				field.get(InstrumentFieldDefinition_.name),
				val.get(InstrumentValueDateTime_.value)));
		cq.where(clauses);
		TypedQuery<InstrumentValueDateTimeDTO> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Instrument> getMatchingInstruments(InstrumentFieldDefinition fieldDefinition, Date value) {
		Criteria criteria = getSession()
				.createCriteria(InstrumentValueDateTime.class).
				add(Restrictions.eq("instrumentFieldDefinition",fieldDefinition)).
				add(Restrictions.eq("value", value)).
				setProjection(Projections.property("instrument"));
		return criteria.list();
	}
}