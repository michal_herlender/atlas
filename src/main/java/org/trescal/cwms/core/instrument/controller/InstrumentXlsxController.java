package org.trescal.cwms.core.instrument.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;
import org.trescal.cwms.core.instrument.views.InstrumentXlsxView;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;

@Controller
@IntranetController
public class InstrumentXlsxController {

	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private NewDescriptionService descriptionService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private MfrService mfrService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CalReqService calReqService;
	@Autowired
	private InstrumentXlsxView instrumentXlsxView;
	

	@RequestMapping(value = "instrumentexport.xlsx", method = RequestMethod.GET)
	public ModelAndView instrumentXlsxExport(@RequestParam(name = "coid", required = true) Integer coId,
			@RequestParam(name = "plantNo", required = false) String plantNo,
			@RequestParam(name = "serialNo", required = false) String serialNo,
			@RequestParam(name = "descId", required = false) Integer descId,
			@RequestParam(name = "mfrId", required = false) Integer mfrId,
			@RequestParam(name = "personId", required = false) Integer personId,
			@RequestParam(name = "subdivId", required = false) Integer subdivId,
			@RequestParam(name = "addressId", required = false) Integer addressId,
			@RequestParam(name = "outOfCal", required = false) Boolean outOfCal,
			@RequestParam(name = "plantId", required = false) Integer plantId,
			@RequestParam(name = "modelText", required = false) String modelText) {
		Company company = companyService.get(coId);
		Description desc = descId == null ? null : descriptionService.findDescription(descId);
		Mfr mfr = mfrId == null ? null : mfrService.findMfr(mfrId);
		Contact contact = personId == null ? null : contactService.get(personId);
		Subdiv subdiv = subdivId == null ? null : subdivService.get(subdivId);
		Address address = addressId == null ? null : addressService.get(addressId);
		List<InstrumentSearchResultsDTO> instruments = instrumentService.searchInstruments(company, plantNo, serialNo, desc,
				null, mfr, contact, subdiv, address, outOfCal, null, modelText, null, plantId, null, null);
		List<Integer> plantids = instruments.stream().map(InstrumentSearchResultsDTO::getPlantId).collect(Collectors.toList());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("instruments", instruments);
		model.put("companyRole", company.getCompanyRole());
		model.put("flexibleFields", company.getInstrumentFieldDefinition().stream()
				.map(InstrumentFieldDefinition::getName).sorted().collect(Collectors.toList()));
		model.put("calrequirements", calReqService.findInstrumentCalReqsForPlantIds(plantids));
		/*model.put("calrequirements", instruments.stream()
				.collect(Collectors.toMap(
						i -> i.getPlantId(), 
						i -> { 
							InstrumentCalReq calReq = calReqService.findInstrumentCalReqForInstrument(i.getPlantId());
							return (calReq != null && calReq.getPrivateInstructions() != null  
									? calReq.getPrivateInstructions()
									: "");
						}))); */
		return new ModelAndView(instrumentXlsxView, model);
	}
}