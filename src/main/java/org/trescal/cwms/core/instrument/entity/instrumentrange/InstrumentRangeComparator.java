package org.trescal.cwms.core.instrument.entity.instrumentrange;

import java.util.Comparator;
import java.util.Set;

/**
 * Comparator used for comparing a {@link Set} of {@link InstrumentRange}.
 * 
 * @author Stuart
 */
public class InstrumentRangeComparator implements Comparator<InstrumentRange>
{
	@Override
	public int compare(InstrumentRange r1, InstrumentRange r2)
	{
		return -1;
	}
}
