package org.trescal.cwms.core.instrument.entity.instrumentusagetype.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;

@Service("InstrumentUsageTypeService")
public class InstrumentUsageTypeServiceImpl extends BaseServiceImpl<InstrumentUsageType, Integer> 
	implements InstrumentUsageTypeService {
	
	@Autowired
	private InstrumentUsageTypeDao baseDao;

	@Override
	public InstrumentUsageType findDefaultType(Class<?> clazz) {
		return this.baseDao.findDefaultType();
	}

	@Override
	public InstrumentUsageType findInstrumentUsageType(int id) {
		return this.baseDao.find(id);
	}

	@Override
	public List<InstrumentUsageType> getAllInstrumentUsageTypes() {
		return this.baseDao.findAll();
	}

	@Override
	public InstrumentUsageType getByName(String name, Locale locale) {
		return this.baseDao.getByName(name, locale);
	}

	@Override
	protected BaseDao<InstrumentUsageType, Integer> getBaseDao() {
		return baseDao;
	}
}