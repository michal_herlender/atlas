package org.trescal.cwms.core.instrument.service;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class InstrumentProjectionServiceImpl implements InstrumentProjectionService {
	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private LocationService locationService;

	@Override
	public void loadInstrumentProjections(List<InstrumentProjectionDTO> instDtos, InstrumentProjectionCommand command) {
		if (command.getLoadAddresses())
			loadAddresses(instDtos, command);
		if (command.getLoadContacts())
			loadContacts(instDtos, command);
		if (command.getLoadLocations())
			loadLocations(instDtos);
	}

	private void loadAddresses(List<InstrumentProjectionDTO> instDtos, InstrumentProjectionCommand command) {
		MultiValuedMap<Integer, InstrumentProjectionDTO> mapByAddressId = new HashSetValuedHashMap<>();
		instDtos.forEach(instDto -> mapByAddressId.put(instDto.getAddressId(), instDto));
		Collection<Integer> addressIds = mapByAddressId.keySet();
		Integer allocatedCompanyId = command.getAllocatedCompanyId();
		List<AddressProjectionDTO> addresses = this.addressService.getAddressProjectionDTOs(addressIds, allocatedCompanyId);
		if (log.isDebugEnabled()) log.debug("addresses.size() : "+addresses.size()+" for "+addressIds.size()+" addressIds");
		for (AddressProjectionDTO address : addresses) {
			mapByAddressId.get(address.getAddressId())
				.forEach(instDto -> instDto.setAddress(address)); 
		}
	}

	private void loadContacts(List<InstrumentProjectionDTO> instDtos, InstrumentProjectionCommand command) {
		MultiValuedMap<Integer, InstrumentProjectionDTO> mapByContactId = new HashSetValuedHashMap<>();
		instDtos.forEach(instDto -> mapByContactId.put(instDto.getContactId(), instDto));
		Collection<Integer> contactIds = mapByContactId.keySet();
		Integer allocatedCompanyId = command.getAllocatedCompanyId();
		List<ContactProjectionDTO> contacts = this.contactService.getContactProjectionDTOs(contactIds, allocatedCompanyId);
		if (log.isDebugEnabled()) log.debug("contacts.size() : "+contacts.size()+" for "+contactIds.size()+" contactIds");
		for (ContactProjectionDTO contact : contacts) {
			mapByContactId.get(contact.getPersonid())
				.forEach(instDto -> instDto.setContact(contact));
		}
	}

	private void loadLocations(List<InstrumentProjectionDTO> instDtos) {
		MultiValuedMap<Integer, InstrumentProjectionDTO> mapByLocationId = new HashSetValuedHashMap<>();
		instDtos.stream()
			.filter(instDto -> instDto.getLocationId() != null)
			.forEach(instDto -> mapByLocationId.put(instDto.getLocationId(), instDto));
		Collection<Integer> locationIds = mapByLocationId.keySet();
		List<KeyValueIntegerString> locations = this.locationService.getLocationDtos(locationIds);
		if (log.isDebugEnabled()) log.debug("locations.size() : "+locations.size()+" for "+locationIds.size()+" locationIds");
		for (KeyValueIntegerString location : locations) {
			mapByLocationId.get(location.getKey())
				.forEach(instDto -> instDto.setLocation(location));
		}
	}
}
