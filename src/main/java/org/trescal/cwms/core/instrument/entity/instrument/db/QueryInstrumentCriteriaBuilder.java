package org.trescal.cwms.core.instrument.entity.instrument.db;

import lombok.val;
import org.hibernate.criterion.MatchMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.audit.entity.db.CriteriaBuilderEffect;
import org.trescal.cwms.core.audit.entity.db.RootSelectRestrictionOrder;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.NumberTools;

import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.trescal.cwms.core.instrument.entity.instrument.db.CommonInstrumentCriteriaBuilder.companyPredicate;
import static org.trescal.cwms.core.tools.JpaUtils.checkIfNullOrZero;
import static org.trescal.cwms.core.tools.StringJpaUtils.*;

public class QueryInstrumentCriteriaBuilder {

	public static CriteriaBuilderEffect<RootSelectRestrictionOrder<Instrument, Instrument>> buildInstrumentCriteria(
			InstrumentAndModelSearchForm<?> form, Boolean orderByModels) {
		return CriteriaBuilderEffect.lift((cb,cq) -> {
			val locale = LocaleContextHolder.getLocale();
			val instrument = cq.from(Instrument.class);
			val model = instrument.join(Instrument_.model);
			val modelName = model.join(InstrumentModel_.nameTranslations);
			val mfr = model.join(InstrumentModel_.mfr, JoinType.LEFT);
			val description = model.join(InstrumentModel_.description);
			val family = description.join(Description_.family);
			val domain = family.join(InstrumentModelFamily_.domain);

		
			Predicate[] predicates =  Stream.of(
			Collections.singletonList(cb.equal(modelName.get(Translation_.locale),locale )),
			companyPredicate(cb, instrument, form.getCompId()),
			mfrPredicate(cb, instrument, mfr, form.getMfrId(), form.getMfrNm()),
			modelPredicate(cb, model, form.getModel()),
			descriptionPredicate(cb, description, form.getDescNm(), form.getDescId()),
			domainPredicate(cb, domain, form.getDomainNm(), form.getDomainId(), form.getLocale()),
			familyPredicate(cb, family, form.getFamilyNm(), form.getFamilyId(), form.getLocale()),
			salesCategoryPredicate(cb, model, form.getSalesCat(), form.getSalesCatId(), form.getLocale()),
			ilikePredicateList(cb, instrument.get(Instrument_.plantno), form.getPlantNo(), MatchMode.ANYWHERE),
			ilikePredicateList(cb, instrument.get(Instrument_.serialno), form.getSerialNo(), MatchMode.ANYWHERE),
			barcodePredicate(cb, instrument, form.getBarcode())
			).flatMap(List::stream).toArray(Predicate[]::new);
			val clauses = cb.and(predicates);
			
			if (orderByModels) {
				val order = Stream.of(cb.asc(modelName.get(Translation_.translation)),
						cb.asc(description.get(Description_.description)), cb.asc(mfr.get(Mfr_.name)),
						cb.asc(model.get(InstrumentModel_.model))).collect(Collectors.toList());
				return RootSelectRestrictionOrder.of(instrument, instrument, clauses, order);
			}
			return RootSelectRestrictionOrder.of(instrument, instrument, clauses, Collections.emptyList());
		});
	}
	
	private static List<Predicate> barcodePredicate(CriteriaBuilder cb, Root<Instrument> instrument, String barcode) {
		if (!checkIfNullOrEmpty(barcode) && NumberTools.isAnInteger(barcode))
			return Collections.singletonList(cb.equal(instrument, Integer.parseInt(barcode)));
		return Collections.emptyList(); 
	}

	private static List<Predicate> salesCategoryPredicate(CriteriaBuilder cb,
			Join<Instrument, InstrumentModel> model, String salesCat, Integer salesCatId, Locale locale) {
		if (checkIfNullOrEmpty(salesCat))
			return Collections.emptyList();
		val salesCategory = model.join(InstrumentModel_.salesCategory);
		if (!checkIfNullOrZero(salesCatId))
			return Collections.singletonList(cb.equal(salesCategory, salesCatId));
		else
			return translatedPredicate(cb, salesCategory.join(SalesCategory_.translations), salesCat, locale);
	}

	private static List<Predicate> familyPredicate(CriteriaBuilder cb,
			Join<Description, InstrumentModelFamily> family, String familyNm, Integer familyId, Locale locale) {
		if (checkIfNullOrEmpty(familyNm))
			return Collections.emptyList();
		if (!checkIfNullOrZero(familyId))
			return Collections.singletonList(cb.equal(family, familyId));
		else
			return translatedPredicate(cb, family.join(InstrumentModelFamily_.translation), familyNm, locale);
	}

	private static List<Predicate> translatedPredicate(CriteriaBuilder cb, Join<?, Translation> translation, String text, Locale locale) {
		return Collections.singletonList(cb.and(cb.equal(translation.get(Translation_.locale), locale),
				ilike(cb, translation.get(Translation_.translation), text, MatchMode.ANYWHERE)));
	}

	private static List<Predicate> domainPredicate(CriteriaBuilder cb,
			Join<InstrumentModelFamily, InstrumentModelDomain> domain, String domainNm, Integer domainId, Locale locale) {
		if (checkIfNullOrEmpty(domainNm))
			return Collections.emptyList();
		if (!checkIfNullOrZero(domainId))
			return Collections.singletonList(cb.equal(domain, domainId));
		else
			return translatedPredicate(cb, domain.join(InstrumentModelDomain_.translation), domainNm, locale);
	}

	private static List<Predicate> descriptionPredicate(CriteriaBuilder cb,
			Join<InstrumentModel, Description> description, String descNm, Integer descId) {
		if (checkIfNullOrEmpty(descNm))
			return Collections.emptyList();
		if (!checkIfNullOrZero(descId))
			return Collections.singletonList(cb.equal(description, descId));
		else
			return Collections.singletonList(ilike(cb, description.get(Description_.description), descNm, MatchMode.ANYWHERE));
	}


	private static List<Predicate> modelPredicate(CriteriaBuilder cb, Join<Instrument, InstrumentModel> model, String modelName){
		if (!checkIfNullOrEmpty(modelName))
			return Collections.singletonList(ilike(cb, model.get(InstrumentModel_.model), modelName, MatchMode.ANYWHERE));
		return Collections.emptyList();
	}

	private static List<Predicate> mfrPredicate(CriteriaBuilder cb, Root<Instrument> instrument,
			Join<InstrumentModel, Mfr> mfr, Integer mfrId, String mfrName) {
		if (checkIfNullOrEmpty(mfrName))
			return Collections.emptyList();
		if (!checkIfNullOrZero(mfrId)) {
			val instMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			val c = cb.and(cb.or(cb.equal(instMfr, mfrId)), cb.isNull(instMfr),
					cb.or(cb.equal(mfr, mfrId), cb.and(cb.notEqual(mfr, mfrId), cb.isTrue(mfr.get(Mfr_.genericMfr)))));
			return Collections.singletonList(c);
		} else {
			val instMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			val pattern = "%" + mfrName + "%";
			val c = cb.and(cb.or(ilike(cb, instMfr.get(Mfr_.name), pattern), cb.isNull(instMfr)),

					cb.or(ilike(cb, mfr.get(Mfr_.name), pattern),
							cb.and(notIlike(cb, mfr.get(Mfr_.name), pattern), cb.isTrue(mfr.get(Mfr_.genericMfr)))));
			return Collections.singletonList(c);
		}
	}

}
