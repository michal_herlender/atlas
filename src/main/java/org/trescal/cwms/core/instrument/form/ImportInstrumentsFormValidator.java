package org.trescal.cwms.core.instrument.form;

import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ImportInstrumentsFormValidator extends AbstractBeanValidator {

	@Autowired
	private ExchangeFormatService efService;
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;

	// 50MB
	private static final long MAX_IN_BYTES = 52428800;
	private static final String EXCEL_EXTENSION_1 = "xlsx";
	private static final String EXCEL_EXTENSION_2 = "xls";

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ImportInstrumentsForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ImportInstrumentsForm form = (ImportInstrumentsForm) target;
		super.validate(form, errors);

		MultipartFile file = form.getFile();

		// validate file
		if (file.isEmpty()) {
			errors.rejectValue("file", "error.instrument.upload.file.empty");
		} else if (!EXCEL_EXTENSION_1.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))
				&& !EXCEL_EXTENSION_2.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))) {
			errors.rejectValue("file", "error.instrument.upload.file.notexcel");
			return;
		} else {
			// try to open the file
			try {
				// For .xlsx
				if (POIXMLDocument.hasOOXMLHeader(file.getInputStream())) {
					XSSFWorkbook wb = new XSSFWorkbook(file.getInputStream());
				}
				// For .xls
				else if (POIFSFileSystem.hasPOIFSHeader(file.getInputStream())) {
					HSSFWorkbook wb = new HSSFWorkbook(file.getInputStream());
				} else {
					errors.rejectValue("file", "error.instrument.upload.file.cannotbeopened");
				}
			} catch (IOException e) {
				e.printStackTrace();
				errors.rejectValue("file", "error.instrument.upload.file.cannotbeopened");
				return;
			}
		}

		// check max file size
		if (file.getSize() > MAX_IN_BYTES) {
			errors.rejectValue("file", "error.instrument.upload.file.maxsizeexceeded");
		}

		// validate exchange Format
		if (form.getExchangeFormatId() == null) {
			errors.rejectValue("exchangeFormatId", "error.instrument.exchangeformat.notnull");
			return;
		} else {
			ExchangeFormat ef = efService.get(form.getExchangeFormatId());
			if (ef == null) {
				errors.rejectValue("exchangeFormatId", "error.instrument.exchangeformat.notnull");
				return;
			}
		}

		ExchangeFormat ef = efService.get(form.getExchangeFormatId());
		// can read file ?
		try {
			if (!excelFileReaderUtil.canReadFile(file.getInputStream(), ef.getSheetName(), ef.getLinesToSkip())) {
				errors.rejectValue("file", "error.instrument.upload.file.cannotbeopened");
			}
		} catch (IOException e) {
			e.printStackTrace();
			errors.rejectValue("file", "error.instrument.upload.file.cannotbeopened");
		}

		// validate address
		if (form.getAddrid() == null) {
			errors.rejectValue("addrid", "error.instrument.address.notnull");
		}

		// validate contact
		if (form.getPersonid() == null) {
			errors.rejectValue("personid", "error.instrument.contact.notnull");
		}

	}

}
