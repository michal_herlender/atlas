package org.trescal.cwms.core.instrument.entity.hireinstrument.db;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireInstrumentDto;
import org.trescal.cwms.core.hire.dto.HireInstrumentRevenueWrapper;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

import java.util.List;
import java.util.Optional;

public interface HireInstrumentService {
	void deleteHireInstrument(HireInstrument hi);

	HireInstrument findHireInstrument(int id);

	HireInstrument findHireInstrumentDWR(int id);

	ResultWrapper findReplacementHireInstrumentDWR(int plantid);

	List<HireInstrument> getAllHireInstruments();

	int getHireInstrumentCount();

	List<HireInstrument> getHireInstruments(List<Integer> plantIds);

	void insertHireInstrument(HireInstrument hi);

	void saveOrUpdateHireInstrument(HireInstrument hi);

	List<HireInstrumentRevenueWrapper> searchHireInstrumentsRevenueHQL(Integer mfrid, String mfrname, String modelname, Integer descid, String descname, String barcode, String plantno, String serialno);

	void updateHireInstrument(HireInstrument hi);

    List<HireInstrumentDto> searchHireInstruments(Integer companyId, String plantId, String plantNo, String serialNo, String model, String manufacturer, String subFamily);

	Optional<HireInstrument> getHireInstrumentByPlantId(Integer plantId);

}