package org.trescal.cwms.core.instrument.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.system.Constants;

@Component @IntranetController
public class EditInstrumentInterceptor implements HandlerInterceptor
{
    @Autowired
    private InstrumService instServ;

    @Value("#{props['cwms.config.instrument.standardsauthentication']}")
    private Boolean authenticateBusinessStandards;

    protected final Log logger = LogFactory.getLog(this.getClass());

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        int procid = ServletRequestUtils.getIntParameter(request, "plantid", 0);
        // if there is no plantid then this is a request for a new instrument or
        // an invalid request so the controller can handle this
        if ((procid == 0) || !this.authenticateBusinessStandards)
        {
            return true;
        }
        // this is an existing instrument, check that the current contact is
        // authenticated to update this
        ResultWrapper results = this.instServ.userCanEditInstrument(request);
        if (!results.isSuccess())
        {
            HttpSession session = request.getSession(true);
            session.setAttribute(Constants.SYSTEM_AUTHENTICATION_ERROR_MESSAGE, results.getMessage());
            response.sendRedirect("/cwms/accessdenied.htm");
            return false;
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {
    }
}