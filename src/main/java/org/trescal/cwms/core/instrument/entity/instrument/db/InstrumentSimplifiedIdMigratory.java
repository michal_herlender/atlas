package org.trescal.cwms.core.instrument.entity.instrument.db;

import lombok.Value;
import lombok.val;
import lombok.var;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

@Slf4j
@Component
public class InstrumentSimplifiedIdMigratory {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void migrateInstruments(Integer coid, boolean nullsOnly, SseEmitter emitter) {
        val batchSize = 50000;
        val query = nullsOnly == false ? "Select plantid,plantno,serialno from instrument" :
        	"Select plantid,plantno,serialno from instrument where simplifiedPlantNo is null or simplifiedSerialNo is null" ;
        val rowSet = coid == null || coid == 0 ? jdbcTemplate.queryForRowSet(query) :
                jdbcTemplate.queryForRowSet(query + " where coid = ?", Collections.singleton(coid).toArray());
        val updateQuery = "update instrument set simplifiedPlantNo = ?, simplifiedSerialNo = ? where plantid = ?";
        var args = new ArrayList<Argument>();
        while (rowSet.next()) {
            val simplyPlant = InstrumentUtils.simplifyId(rowSet.getString("plantno"));
            val simplySerial = InstrumentUtils.simplifyId(rowSet.getString("serialno"));
            val plantId = rowSet.getInt("plantid");
            args.add(Argument.of(plantId, simplySerial, simplyPlant));
            if (rowSet.getRow() % batchSize == 0 || rowSet.isLast()) {
            	try {
                    jdbcTemplate.batchUpdate(updateQuery, args, args.size(), (ps, argument) -> {
                        ps.setString(1, argument.getPlantNo());
                        ps.setString(2, argument.getSerialNo());
                        ps.setInt(3, argument.getPlantId());
                    });
                    emitOrError(emitter, SseEmitter
                            .event()
                            .data(String.format("%d instruments were processed", rowSet.getRow())));
            	}
            	catch (Exception e) {
            		log.error("Exception during batch update", e);
            		emitOrError(emitter, SseEmitter.event().data("Exception during batch update : "+e.getMessage()));
            		break;
            	}
                args = new ArrayList<>();
            }
        }
    }

    private void emitOrError(SseEmitter emitter, SseEmitter.SseEventBuilder event) {
        try {
            emitter.send(event);
        } catch (IOException e) {
            emitter.completeWithError(e);
        }
    }

    @Value(staticConstructor = "of")
    private static class Argument {
        Integer plantId;
        String serialNo, plantNo;
    }
}


@Configuration
class DBConfig {

    @Bean
    JdbcTemplate template(DataSource dataSource) {
        val jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setFetchSize(50000);
        return jdbcTemplate;
    }
}