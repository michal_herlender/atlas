package org.trescal.cwms.core.instrument.form;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.batch.config.ItemAnalysis;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class ImportedInstrumentSynthesisForm {

	private Integer subdivId;
	private Integer defaultContactId;
	private Integer defaultAddressId;
	private Integer defaultServiceTypeId;
	private Integer exchangeFormatId;
	private Integer businessCompanyId;
	@Valid
	private List<ImportedInstrumentsSynthesisRowDTO> rows;

	@Data
	public static class ImportedInstrumentsSynthesisRowDTO implements ItemAnalysis {

		/* input data */
		// not null
		@NotNull
		private String plantno;
		private String serialno;

		private String brand;
		private Integer tmlBrandId;

		private String model;

		// not null or instrumentModelId not null
		private Integer tmlInstrumentModelId;
		// not null or tmlInstrumentModelId not null
		private Integer instrumentModelId;

		private String description;

		private Integer tmlFamilyId;
		private Integer familyId;
		private String familyName;

		private Integer tmlSubFamilyId;
		private Integer subFamilyId;
		private String subFamilyName;

		@DateTimeFormat
		private LocalDate nextCalDueDate;

		// not null
		private Integer calFrequency;
		// not null
		private IntervalUnit intervalUnit;

		private String defaultProcedureReference;
		private Integer defaultProcedureId;

		private String contact;
		private Integer contactId;

		private String address;
		private Integer addressId;

		private String serviceType;
		private Integer defaultServiceTypeId;

		private String clientRef;

		private String instrumentUsageType;
		private Integer instrumentUsageTypeId;

		private List<String> warningMessages;
		private List<ItemAnalysisResult> itemAnalysisResults;
		private Integer index;

		public ImportedInstrumentsSynthesisRowDTO() {
			this.itemAnalysisResults = new ArrayList<>();
		}

		@Override
		public List<ItemAnalysisResult> getItemAnalysisResult() {
			return this.itemAnalysisResults;
		}

	}

}
