package org.trescal.cwms.core.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.dto.InstrumentComplementaryFieldDto;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.db.InstrumentComplementaryFieldService;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CertificateClassDefault;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;
import java.util.stream.Collectors;

@Controller @IntranetController
@RequestMapping("editcomplementaryfields.htm")
public class EditComplementaryFieldsController {

	@Autowired
	private InstrumentComplementaryFieldService icfService;
	@Autowired
	private UoMService uoMService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private Deviation deviationSystemDefault;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private CertificateClassDefault certificateClassSystemDefault;
	@Autowired
	private CalibrationVerification calibrationVerificationSystemDefault;

	@ModelAttribute("units")
	public List<KeyValue<Integer,String>> getUnits() {
		Locale locale = LocaleContextHolder.getLocale();
		List<UoM> allUoMs = uoMService.getAllActiveUoMs();
		return allUoMs.stream()
				.map(u -> new KeyValue<>(u.getId(),u.getNameTranslation() != null ? translationService.getCorrectTranslation(u.getNameTranslation(),locale) : u.getName()))
				.collect(Collectors.toList());
	}

	@ModelAttribute("hasdeviation")
	protected Boolean isDeviationPossible(@RequestParam(value="plantid") Integer plantId) {
		Instrument instrument = instrumentService.get(plantId);
		return deviationSystemDefault.getValueByScope(Scope.COMPANY,instrument.getComp(),null);
	}

	@ModelAttribute("hascertclass")
	public Boolean isCertClassAllowed(@RequestParam(value="plantid") Integer plantId) {
		Instrument instrument = instrumentService.get(plantId);
		return certificateClassSystemDefault.getValueByScope(Scope.COMPANY,instrument.getComp(),null);
	}

	@ModelAttribute("certclasses")
    public List<KeyValue<String,String>> getCertClasses() {
	    return Arrays.stream(CertificateClass.values())
				.sorted(Comparator.comparing(CertificateClass::getOrder))
				.map(c -> new KeyValue<>(c.name(),c.getDescription()))
				.collect(Collectors.toList());
    }
	
	@InitBinder("command")
	protected void initBinder(WebDataBinder binder){
		binder.registerCustomEditor(Date.class, new MyCustomDateEditor(DateTools.df_ISO8601, true));
	}

    @ModelAttribute("hasverificationstatus")
    public Boolean clientHasCertValidation(@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId) {
        Instrument instrument = this.instrumentService.get(plantId);
        return this.calibrationVerificationSystemDefault.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
    }

    @ModelAttribute("verificationStatuses")
    public List<KeyValue<CalibrationVerificationStatus, String>> getCalStatuses() {
        return Arrays.asList(CalibrationVerificationStatus.values()).stream()
                .map(s -> new KeyValue<>(s,s.getName())).collect(Collectors.toList());
    }

	@ModelAttribute("command")
	protected InstrumentComplementaryFieldDto findComplementaryField(@RequestParam(value="plantid") Integer plantId) {
		InstrumentComplementaryFieldDto dto = new InstrumentComplementaryFieldDto();
		InstrumentComplementaryField complementaryField = icfService.getComplementaryFieldByPlantid(plantId);
		dto.setComplementaryField(complementaryField);
		if ( complementaryField.getDeviationUnits() != null ) {
			dto.setUnitId(complementaryField.getDeviationUnits().getId());
		}
		if ( complementaryField.getCertificateClass() != null ) {
		    dto.setCertClass(complementaryField.getCertificateClass().name());
        }
		return dto;
	}

	@RequestMapping(method=RequestMethod.GET)
	public String displayForm(){
		return "trescal/core/instrument/editcomplementaryfields";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String processUpdate(@Validated @ModelAttribute("command") InstrumentComplementaryFieldDto complementaryFieldDto, BindingResult result){
		if(result.hasErrors()){
			//result.getAllErrors().forEach(System.out::println);
			return "trescal/core/instrument/editcomplementaryfields";
		}
		InstrumentComplementaryField complementaryField = complementaryFieldDto.getComplementaryField();
		if ( complementaryFieldDto.getUnitId() != null && complementaryFieldDto.getUnitId() > 0 ) {
			UoM unit = uoMService.findUoM(complementaryFieldDto.getUnitId());
			complementaryField.setDeviationUnits(unit);
		}
		if ( complementaryFieldDto.getCertClass() != null ) {
                complementaryField.setCertificateClass(CertificateClass.valueOf(complementaryFieldDto.getCertClass()));
        }
		complementaryField = icfService.merge(complementaryField);
		return "redirect:viewinstrument.htm?plantid=" + complementaryField.getInstrument().getPlantid();
	}
}