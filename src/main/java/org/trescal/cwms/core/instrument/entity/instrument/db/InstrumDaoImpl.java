package org.trescal.cwms.core.instrument.entity.instrument.db;

import io.vavr.Function4;
import io.vavr.Tuple;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.xmlbeans.impl.common.Levenshtein;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.asset.entity.asset.PhysicalAsset_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.audit.entity.db.CriteriaBuilderEffect;
import org.trescal.cwms.core.audit.entity.db.CriteriaQueryGenerator;
import org.trescal.cwms.core.audit.entity.db.RootSelectRestrictionOrder;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.Location_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.dto.*;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.*;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentFieldDefinitionService;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType_;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType_;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension_;
import org.trescal.cwms.core.instrument.form.EditInstrumentForm;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrument.form.InstrumentInGroupLookUpForm;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard_;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread_;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType_;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM_;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory_;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability_;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink_;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink_;
import org.trescal.cwms.core.jobs.jobitem.entity.enums.NewJobItemSearchOrderByEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemBaseSearchForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.Asn_;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem_;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequestDateRange;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.StringJpaUtils;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.web.instrument.form.FlexibleFieldSearchDTO;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentForm;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Types;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.vavr.API.*;
import static org.trescal.cwms.core.instrument.entity.instrument.db.CommonInstrumentCriteriaBuilder.companyPredicate;
import static org.trescal.cwms.core.tools.JpaUtils.*;
import static org.trescal.cwms.core.tools.StringJpaUtils.*;
import static org.trescal.cwms.core.tools.jpa.NewJobItemSearchUtils.searchByCompanyOrGroupOrEveryWherePredicate;

@Repository
public class InstrumDaoImpl extends BaseDaoImpl<Instrument, Integer> implements InstrumDao {

	@Value("#{props['cwms.config.instrument.duplicates.similarity_planno_weight']}")
	private Double plantNoWeight;
	@Value("#{props['cwms.config.instrument.duplicates.similarity_plantno_simply_weight']}")
	private Double simplifiedPlantNoWeight;
	@Value("#{props['cwms.config.instrument.duplicates.similarity_serialno_weight']}")
	private Double serialNoWeight;
	@Value("#{props['cwms.config.instrument.duplicates.similarity_serialno_simply_weight']}")
	private Double simplifiedSerialNoWeight;
	@Autowired
	private InstrumentRepository instrumentRepository;
	@Autowired
	private InstrumentFieldDefinitionService instrumentFieldDefinitionService;

	@Override
	protected Class<Instrument> getEntity() {
		return Instrument.class;
	}

	@Override
	public Integer batchUpdateOwnership(Contact oldContact, Contact newContact, Address newAddress,
			Company newCompany) {
		if ((newContact == null) && (newAddress == null)) {
			throw new IllegalArgumentException("Either one of newContact or newAddress must not be null");
		}
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaUpdate<Instrument> cu = cb.createCriteriaUpdate(Instrument.class);
		Root<Instrument> root = cu.from(Instrument.class);
		cu.where(cb.equal(root.get(Instrument_.con), oldContact));
		if (newContact != null)
			cu.set(Instrument_.con, newContact);
		if (newAddress != null) {
			cu.set(Instrument_.add, newAddress);
			cu.set(Instrument_.loc, cb.nullLiteral(Location.class));
		}
		if (newCompany != null)
			cu.set(Instrument_.comp, newCompany);
		return getEntityManager().createQuery(cu).executeUpdate();
	}

	@Override
	public Optional<Integer> checkExists(String plantno, String serialno, Integer coid, Integer companyGroupId) {
		if ((plantno == null || plantno.isEmpty()) && (serialno == null || serialno.isEmpty()))
			return Optional.empty();
		return getFirstResult(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<Instrument> instrumentRoot = cq.from(Instrument.class);
			Join<Instrument, Company> company = instrumentRoot.join(Instrument_.comp);

			Predicate clauses = cb.conjunction();
			if (plantno != null && !plantno.isEmpty()) {
				Predicate plantnoPredicate = cb.equal(instrumentRoot.get(Instrument_.plantno), plantno);
				clauses.getExpressions().add(plantnoPredicate);
			}
			if (serialno != null && !serialno.isEmpty()) {
				Predicate serialNoPredicate = cb.equal(instrumentRoot.get(Instrument_.serialno), serialno);
				clauses.getExpressions().add(serialNoPredicate);
			}

			Predicate orClauses = cb.disjunction();
			if (coid != null) {
				orClauses.getExpressions().add(cb.equal(company.get(Company_.coid), coid));
			}
			if (companyGroupId != null) {
				Join<Company, CompanyGroup> companyGroup = company.join(Company_.companyGroup, JoinType.LEFT);
				orClauses.getExpressions().add(cb.equal(companyGroup.get(CompanyGroup_.id), companyGroupId));
			}

			clauses.getExpressions().add(orClauses);

			cq.select(instrumentRoot.get(Instrument_.plantid));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public Long countByAddr(Integer addrid) {
		return instrumentRepository.countByLocAddAddrid(addrid);
	}

	@Override
	public Long countByContact(Integer contactid) {
		return instrumentRepository.countByConPersonid(contactid);
	}

	@Override

	public Long countByLocation(Integer locationid) {
		return instrumentRepository.countByLocLocationid(locationid);
	}

	@Override
	public Long countBySubdiv(Integer subdivid) {
		return instrumentRepository.countByAddSubSubdivid(subdivid);
	}

	@Override
	public Instrument findEagerInstrum(int id) {
		return getSingleResult(cb -> {
            val cq = cb.createQuery(Instrument.class);
            val instrument = cq.from(Instrument.class);
            cq.where(cb.equal(instrument, id));
            instrument.fetch(Instrument_.comp);
            instrument.fetch(Instrument_.con);
            instrument.fetch(Instrument_.add, JoinType.LEFT);
            instrument.fetch(Instrument_.mfr, JoinType.LEFT);
            instrument.fetch(Instrument_.capability, JoinType.LEFT);
            instrument.fetch(Instrument_.workInstructions, JoinType.LEFT);
            val model = instrument.fetch(Instrument_.model);
            model.fetch(InstrumentModel_.mfr, JoinType.LEFT);
            model.fetch(InstrumentModel_.description, JoinType.LEFT);
            model.fetch(InstrumentModel_.modelType, JoinType.LEFT);

            val items = instrument.fetch(Instrument_.jobItems, JoinType.LEFT);
            items.fetch(JobItem_.calType, JoinType.LEFT).fetch(CalibrationType_.serviceType, JoinType.LEFT);
            items.fetch(JobItem_.jobCostingItems, JoinType.LEFT).fetch(JobCostingItem_.jobCosting, JoinType.LEFT)
                .fetch(JobCosting_.currency, JoinType.LEFT);
            items.fetch(JobItem_.capability, JoinType.LEFT);
            items.fetch(JobItem_.job, JoinType.LEFT);
            items.fetch(JobItem_.certLinks, JoinType.LEFT).fetch(CertLink_.cert, JoinType.LEFT);
            instrument.fetch(Instrument_.defaultServiceType, JoinType.LEFT).fetch(ServiceType_.calibrationType,
                JoinType.LEFT);

            return cq;
        });
	}

	@Override
	public Instrument findEagerModelInstrum(int plantid) {
		return getSingleResult(cb -> {
			CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			instrument.fetch(Instrument_.mfr, JoinType.LEFT);
			instrument.fetch(Instrument_.comp);
			instrument.fetch(Instrument_.ranges, JoinType.LEFT);
			Fetch<Instrument, InstrumentModel> model = instrument.fetch(Instrument_.model);
			model.fetch(InstrumentModel_.description);
			model.fetch(InstrumentModel_.mfr, JoinType.LEFT);
			model.fetch(InstrumentModel_.ranges, JoinType.LEFT);
			model.fetch(InstrumentModel_.nameTranslations, JoinType.LEFT);
			cq.where(cb.equal(instrument.get(Instrument_.plantid), plantid));
			return cq;
		});
	}

	@Override
	public Instrument findEagerModelInstrumWithModules(int plantid) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			cq.where(cb.equal(instrument, plantid));
			instrument.fetch(Instrument_.mfr, JoinType.LEFT);
			val model = instrument.fetch(Instrument_.model);
			model.fetch(InstrumentModel_.mfr, JoinType.LEFT);
			model.fetch(InstrumentModel_.description, JoinType.LEFT);
			val modules = instrument.fetch(Instrument_.modules, JoinType.LEFT);
			val moduelsModel = modules.fetch(Instrument_.model);
			modules.fetch(Instrument_.comp, JoinType.LEFT);
			moduelsModel.fetch(InstrumentModel_.mfr);
			moduelsModel.fetch(InstrumentModel_.description, JoinType.LEFT);
			moduelsModel.fetch(InstrumentModel_.modelMfrType, JoinType.LEFT);
			return cq;
		});
	}

	@Override
	public List<Instrument> findInstrumByBarCode(String barcode) {
		return getResultList(cb -> {
			CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			instrument.fetch(Instrument_.con).fetch(Contact_.sub);
			instrument.fetch(Instrument_.mfr, javax.persistence.criteria.JoinType.LEFT);
			Predicate disjunction = cb.disjunction();
			disjunction.getExpressions().add(cb.equal(instrument.get(Instrument_.formerBarCode), barcode));
			if (NumberTools.isAnInteger(barcode))
				disjunction.getExpressions().add(cb.equal(instrument, Integer.parseInt(barcode)));
			cq.where(disjunction);
			return cq;
		});
	}

	@Override
	public List<Instrument> getAll(Company comp, Subdiv sub, Contact con, ScheduledQuoteRequestType type,
			ScheduledQuotationRequestDateRange dateRange, boolean activeOnly) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);
		Root<Instrument> instrument = cq.from(Instrument.class);

		Predicate clauses = cb.conjunction();
		if (con != null)
			clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.con), con));
		else if (sub != null) {
			Join<Instrument, Contact> contact = instrument.join(Instrument_.con);
			clauses.getExpressions().add(cb.equal(contact.get(Contact_.sub), sub));
		} else if (comp != null) {
			Join<Instrument, Contact> contact = instrument.join(Instrument_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.comp), comp));
		}
		switch (type) {
		case ALL_OUT_OF_CAL_INSTRUMENTS:
			clauses.getExpressions()
					.add(cb.lessThan(instrument.get(Instrument_.nextCalDueDate), cb.currentDate().as(LocalDate.class)));
			clauses.getExpressions().add(cb.isFalse(instrument.get(Instrument_.calExpiresAfterNumberOfUses)));
			break;
		case DATE_RANGE_INSTRUMENTS:
			if (dateRange != null && (dateRange.getStartDate() != null || dateRange.getFinishDate() != null)) {
				List<Expression<Boolean>> calDateExpr = new ArrayList<>();
				if (dateRange.getStartDate() != null)
					calDateExpr.add(cb.greaterThanOrEqualTo(instrument.get(Instrument_.nextCalDueDate),
							dateRange.getStartDate()));
				if (dateRange.getFinishDate() != null)
					calDateExpr.add(cb.lessThanOrEqualTo(instrument.get(Instrument_.nextCalDueDate),
							dateRange.getFinishDate()));
				if (dateRange.getIncludeEmptyDates()) {
					Predicate calDateDisjunction = cb.disjunction();
					calDateDisjunction.getExpressions().add(cb.isNull(instrument.get(Instrument_.nextCalDueDate)));
					Predicate calDateConjunction = cb.conjunction();
					calDateConjunction.getExpressions().addAll(calDateExpr);
					calDateDisjunction.getExpressions().add(calDateConjunction);
				} else
					clauses.getExpressions().addAll(calDateExpr);
			} else if (dateRange != null && !dateRange.getIncludeEmptyDates())
				clauses.getExpressions().add(cb.isNotNull(instrument.get(Instrument_.nextCalDueDate)));
			break;
		default:
			break;
		}
		if (activeOnly)
			clauses.getExpressions().add(instrument.get(Instrument_.status).in(InstrumentStatus.activeStatus()));
		cq.where(clauses);
		TypedQuery<Instrument> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<Instrument> getByBarcode(String barcode, Company company) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			cq.where(cb.and(cb.equal(instrument.get(Instrument_.comp), company),
					NumberTools.isAnInteger(barcode)
							? cb.or(cb.equal(instrument.get(Instrument_.plantid), Integer.parseInt(barcode)),
									cb.equal(instrument.get(Instrument_.formerBarCode), barcode))
							: cb.equal(instrument.get(Instrument_.formerBarCode), barcode)));
			return cq;
		});
	}

	@Override
	public List<Instrument> getAllContactInstrums(int personid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			val con = instrument.join(Instrument_.con);
			cq.where(cb.equal(con.get(Contact_.personid), personid));
			return cq;
		});
	}

	@Override
	public List<Instrument> getAllInstrumentsOutOfCalibration(Company comp, Subdiv sub, Contact con) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			val predicates = Stream.of(
					Stream.of(
							cb.or(cb.notEqual(instrument.get(Instrument_.status), InstrumentStatus.BER),
									cb.notEqual(instrument.get(Instrument_.status), InstrumentStatus.DO_NOT_RECALL)),
							cb.isTrue(instrument.join(Instrument_.usageType).get(InstrumentUsageType_.recall)),
							cb.isTrue(instrument.join(Instrument_.storageType).get(InstrumentStorageType_.recall)),
							cb.lessThanOrEqualTo(instrument.get(Instrument_.nextCalDueDate),
									LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))),
					con != null ? Stream.of(cb.equal(instrument.get(Instrument_.con), con)) : Stream.<Predicate>empty(),
					sub != null ? Stream.of(cb.equal(instrument.get(Instrument_.con).get(Contact_.sub), sub))
							: Stream.<Predicate>empty(),
					comp != null ? Stream.of(cb.equal(instrument.get(Instrument_.comp), comp))
							: Stream.<Predicate>empty())
					.flatMap(Function.identity());
			cq.where(predicates.toArray(Predicate[]::new));
			cq.orderBy(cb.asc(instrument.get(Instrument_.nextCalDueDate)));
			cq.orderBy(cb.asc(instrument.get(Instrument_.serialno)));
			return cq;
		});
	}

	@Override
	public List<Instrument> getAvailableBaseUnits(int coid, int modelid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			val comp = instrument.join(Instrument_.comp);
			val model = instrument.join(Instrument_.model);
			val modelType = model.join(InstrumentModel_.modelType);
			val baseUnit = model.join(InstrumentModel_.thisBaseUnitsModules);
			val unitModule = baseUnit.join(InstrumentModelPartOf_.module);
			model.join(InstrumentModel_.mfr, JoinType.LEFT);
			model.join(InstrumentModel_.description, JoinType.LEFT);
			cq.where(cb.and(cb.equal(comp, coid), cb.isTrue(modelType.get(InstrumentModelType_.modules)),
					cb.equal(unitModule, modelid)));
			cq.orderBy(cb.asc(instrument.get(Instrument_.plantid)));
			return cq;
		});
	}

	@Override
	public List<Instrument> getAvailableModules(int coid, int modelid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			val comp = instrument.join(Instrument_.comp);
			val model = instrument.join(Instrument_.model);
			val modelType = model.join(InstrumentModel_.modelType);
			val baseUnit = model.join(InstrumentModel_.thisBaseUnitsModules);
			val unitBase = baseUnit.join(InstrumentModelPartOf_.base);
			model.join(InstrumentModel_.mfr, JoinType.LEFT);
			model.join(InstrumentModel_.description, JoinType.LEFT);
			cq.where(cb.and(cb.equal(comp, coid), cb.isTrue(modelType.get(InstrumentModelType_.baseUnits)),
					cb.equal(unitBase, modelid), cb.isNull(instrument.get(Instrument_.baseUnit))));
			cq.orderBy(cb.asc(instrument.get(Instrument_.plantid)));
			return cq;
		});
	}

	@Override
	public List<Integer> getBarcodesOnJob(int jobId) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Integer.class);
			val instrument = cq.from(Instrument.class);
			val job = instrument.join(Instrument_.jobItems).join(JobItem_.job);
			cq.where(cb.equal(job, jobId));
			cq.distinct(true);
			cq.select(instrument.get(Instrument_.plantid));
			cq.orderBy(cb.asc(instrument.get(Instrument_.plantid)));
			return cq;
		});
	}

	@Override
	public int getCompanyInstrumentCount(int coid) {
		return instrumentRepository.countByCompCoid(coid).intValue();
	}

	@Override
	public List<Instrument> getCompanyInstrumentsFromModelIds(Integer compId, List<Integer> modelIds) {
		return instrumentRepository.getByCompCoidAndModelModelidIn(compId, modelIds);
	}

	@Override
	public EditInstrumentForm getEditInstrumentForm(Integer plantId) {
		return getSingleResult(cb -> {
            val cq = cb.createQuery(EditInstrumentForm.class);
            val instrument = cq.from(Instrument.class);
            val address = instrument.join(Instrument_.add, JoinType.LEFT);
            val company = instrument.join(Instrument_.comp, JoinType.LEFT);
            val contact = instrument.join(Instrument_.con, JoinType.LEFT);
            val location = instrument.join(Instrument_.loc, JoinType.LEFT);
            val mfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
            val model = instrument.join(Instrument_.model, JoinType.LEFT);
            val procedure = instrument.join(Instrument_.capability, JoinType.LEFT);
            val storeageType = instrument.join(Instrument_.storageType, JoinType.LEFT);
            val usageType = instrument.join(Instrument_.usageType, JoinType.LEFT);

            cq.select(cb.construct(EditInstrumentForm.class, address.get(Address_.addrid),
                instrument.get(Instrument_.calFrequency), company.get(Company_.coid),
                company.get(Company_.companyRole), instrument.get(Instrument_.lastModified),
                location.get(Location_.locationid), mfr.get(Mfr_.mfrid), mfr.get(Mfr_.name),
                model.get(InstrumentModel_.modelid), instrument.get(Instrument_.modelname),
                model.get(InstrumentModel_.modelMfrType), instrument.get(Instrument_.nextCalDueDate),
                contact.get(Contact_.personid), instrument.get(Instrument_.plantid),
                instrument.get(Instrument_.plantno), procedure.get(Capability_.id),
                procedure.get(Capability_.reference), instrument.get(Instrument_.serialno),
                instrument.get(Instrument_.status), storeageType.get(InstrumentStorageType_.id),
                usageType.get(InstrumentUsageType_.id), instrument.get(Instrument_.calExpiresAfterNumberOfUses),
                instrument.get(Instrument_.permittedNumberOfUses),
                instrument.get(Instrument_.usesSinceLastCalibration),
                instrument.get(Instrument_.calibrationStandard), instrument.get(Instrument_.firmwareAccessCode),
                instrument.get(Instrument_.customerDescription), instrument.get(Instrument_.customerSpecification),
                instrument.get(Instrument_.freeTextLocation), instrument.get(Instrument_.clientBarcode),
                instrument.get(Instrument_.customerManaged)));
            cq.where(cb.equal(instrument.get(Instrument_.plantid), plantId));
			return cq;
		});
	}

	@Override
	public List<Calibration> getHistory(Instrument instrument) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Calibration.class);
			val calibration = cq.from(Calibration.class);
			val inst = calibration.join(Calibration_.links).join(CalLink_.ji).join(JobItem_.inst);
			cq.where(cb.equal(inst, instrument));
			cq.orderBy(cb.desc(calibration.get(Calibration_.startTime)));
			return cq;
		});
	}

	@Override
	public List<Instrument> getInstruments(List<Integer> plantIds) {
		return instrumentRepository.findAllByPlantidIn(plantIds);
	}

	@Override
	public List<InstrumentProjectionDTO> getInstrumentProjectionDTOs(Locale locale, Collection<Integer> plantIds) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentProjectionDTO> cq = cb.createQuery(InstrumentProjectionDTO.class);
			Root<Instrument> root = cq.from(Instrument.class);
			Join<Instrument, Mfr> instMfr = root.join(Instrument_.mfr, javax.persistence.criteria.JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = root.join(Instrument_.model,
					javax.persistence.criteria.JoinType.INNER);
			Join<InstrumentModel, Description> subfamily = model.join(InstrumentModel_.description,
					javax.persistence.criteria.JoinType.INNER);
			Join<InstrumentModel, Translation> modelTranslation = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelTranslation.on(cb.equal(modelTranslation.get(Translation_.locale), locale));
			Join<Instrument, Address> address = root.join(Instrument_.add, javax.persistence.criteria.JoinType.LEFT);
			Join<Instrument, Contact> contact = root.join(Instrument_.con, javax.persistence.criteria.JoinType.LEFT);
			Join<Instrument, Location> location = root.join(Instrument_.loc, javax.persistence.criteria.JoinType.LEFT);

			CompoundSelection<InstrumentProjectionDTO> selection = cb.construct(InstrumentProjectionDTO.class,
					root.get(Instrument_.plantid), root.get(Instrument_.plantno), root.get(Instrument_.formerBarCode),
					root.get(Instrument_.serialno), root.get(Instrument_.scrapped),
					root.get(Instrument_.calibrationStandard), model.get(InstrumentModel_.modelid),
					model.get(InstrumentModel_.model), modelTranslation.get(Translation_.translation),
					model.get(InstrumentModel_.modelMfrType), subfamily.get(Description_.typology),
					instMfr.get(Mfr_.mfrid), instMfr.get(Mfr_.genericMfr), instMfr.get(Mfr_.name),
					root.get(Instrument_.modelname), root.get(Instrument_.customerDescription),
					address.get(Address_.addrid), contact.get(Contact_.personid), location.get(Location_.locationid));

			cq.where(root.get(Instrument_.plantid).in(plantIds));
			cq.select(selection);

			return cq;
		});
	}

	public List<Instrument> getInstStandardsWithNoRecallDate() {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			val con = instrument.join(Instrument_.con);
			val model = instrument.join(Instrument_.model);
			val mfr = model.join(InstrumentModel_.mfr);
			val description = model.join(InstrumentModel_.description);
			cq.where(cb.and(cb.isTrue(instrument.get(Instrument_.calibrationStandard)),
					cb.isFalse(instrument.get(Instrument_.scrapped)),
					cb.isNull(instrument.get(Instrument_.nextCalDueDate))));
			cq.orderBy(cb.asc(con.get(Contact_.lastName)), cb.asc(con.get(Contact_.firstName)),
					cb.asc(mfr.get(Mfr_.name)), cb.asc(model.get(InstrumentModel_.model)),
					cb.asc(description.get(Description_.description)), cb.asc(instrument.get(Instrument_.plantid)));
			return cq;
		});
	}

	public List<Instrument> getInstsWithMismatchingOwnerAndAddress() {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			val comp = instrument.join(Instrument_.comp);
			val con = instrument.join(Instrument_.con);
			val conSub = con.join(Contact_.sub);
			val addSub = instrument.join(Instrument_.add).join(Address_.sub);
			cq.where(cb.notEqual(conSub, addSub));
			cq.orderBy(cb.asc(comp.get(Company_.coname)), cb.asc(con.get(Contact_.lastName)),
					cb.asc(con.get(Contact_.firstName)), cb.asc(instrument.get(Instrument_.plantid)));
			return cq;
		});
	}

	public List<Integer> getInstrumentIds(Integer oldContactId, Integer oldAddressId) {
		if ((oldContactId == null) && (oldAddressId == null))
			throw new IllegalArgumentException("At least one of contact id or address id must be specified");
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<Instrument> root = cq.from(Instrument.class);
			Join<Instrument, Contact> join = root.join(Instrument_.con);
			cq.where(cb.equal(join.get(Contact_.personid), oldContactId));
			cq.select(root.get(Instrument_.plantid));
			return cq;
		});
	}

	/**
	 * @param allocatedCompanyId
	 *            - if specified, restrict results to companies synced with
	 *            Plantillas (ignored if null)
	 * @param addrid
	 *            - if specified, restrict results to address indicated
	 */
	private CriteriaBuilderEffect<RootSelectRestrictionOrder<Instrument, InstrumentPlantillasDTO>> basePlantillasCriteria(
			Integer allocatedCompanyId, Integer addrid, int businessSubdivId) {
		return CriteriaBuilderEffect.lift((cb, cq) -> {
			val instrument = cq.from(Instrument.class);
			val model = instrument.join(Instrument_.model);
			val add = instrument.join(Instrument_.add, JoinType.LEFT);
			val con = instrument.join(Instrument_.con, JoinType.LEFT);
			val subdiv = con.join(Contact_.sub, JoinType.LEFT);
			val comp = instrument.join(Instrument_.comp);
			val loc = instrument.join(Instrument_.loc, JoinType.LEFT);
			val complementary = instrument.join(Instrument_.instrumentComplementaryFields, JoinType.LEFT);
			val mfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			val usageType = instrument.join(Instrument_.usageType, JoinType.LEFT);
			val lastCal = instrument.join(Instrument_.lastCal, JoinType.LEFT);
			val clauses = cb.conjunction();
			if (businessSubdivId != 0) {
				val subdivSetting = subdiv.join(Subdiv_.subdivSettings, JoinType.LEFT);
				val subdivSettingSubdiv = subdivSetting.join(SubdivSettingsForAllocatedSubdiv_.organisation.getName(),
						JoinType.LEFT);
				clauses.getExpressions()
						.add(cb.equal(subdivSettingSubdiv.get(Subdiv_.subdivid.getName()), businessSubdivId));
			}
			if (addrid != null)
				clauses.getExpressions().add(cb.equal(add, addrid));
			if (allocatedCompanyId != null) {
				val settings = comp.join(Company_.settingsForAllocatedCompanies);

				clauses.getExpressions()
						.add(cb.isTrue(settings.get(CompanySettingsForAllocatedCompany_.syncToPlantillas)));
				clauses.getExpressions().add(
						cb.equal(settings.join(CompanySettingsForAllocatedCompany_.organisation), allocatedCompanyId));
			}
			val selection = cb.construct(InstrumentPlantillasDTO.class, instrument.get(Instrument_.lastModified),
					add.get(Address_.addrid), subdiv.get(Subdiv_.subdivid), comp.get(Company_.coid),
					instrument.get(Instrument_.plantid), model.get(InstrumentModel_.modelid), mfr.get(Mfr_.name),
					instrument.get(Instrument_.modelname), instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.plantno), loc.get(Location_.location), con.get(Contact_.firstName),
					con.get(Contact_.lastName), complementary.get(InstrumentComplementaryField_.formerBarCode),
					instrument.get(Instrument_.customerDescription), usageType.get(InstrumentUsageType_.recall),
					instrument.get(Instrument_.customerManaged), instrument.get(Instrument_.status),
					instrument.get(Instrument_.addedOn), lastCal.get(Calibration_.calDate),
					instrument.get(Instrument_.calFrequency), instrument.get(Instrument_.calFrequencyUnit),
					instrument.get(Instrument_.nextCalDueDate));
			return RootSelectRestrictionOrder.of(instrument, selection, clauses, Collections.emptyList());
		});
	}

	@Override
	public InstrumentPlantillasDTO getPlantillasInstrument(int plantId) {
		CriteriaBuilderEffect<RootSelectRestrictionOrder<Instrument, InstrumentPlantillasDTO>> criteriaQuery = basePlantillasCriteria(
				null, null, 0).flatMap(t -> CriteriaBuilderEffect.lift((cb, cq) -> {
					val clauses = t.getPredicate();
					clauses.getExpressions().add(cb.equal(t.getRoot().get(Instrument_.plantid), plantId));
					return t.withPredicate(clauses);
				}));

		return getSingleResult(cb -> {
			val cq = cb.createQuery(InstrumentPlantillasDTO.class);
			val tuple = criteriaQuery.unsafeApply(cb, cq);
			cq.where(tuple.getPredicate());
			cq.select(tuple.getSelect());
			return cq;
		});
	}

	/**
	 * lastModified parameter is optional, remaining are mandatory
	 */
	@Override
	public PagedResultSet<InstrumentPlantillasDTO> getPlantillasInstruments(int allocatedCompanyId,
			Date afterLastModified, Integer addrid, String plantno, int businessSubdivId, int resultsPerPage,
			int currentPage) {
		PagedResultSet<InstrumentPlantillasDTO> prs = new PagedResultSet<>(resultsPerPage, currentPage);
		CriteriaBuilderEffect<Triple<Expression<?>, Selection<InstrumentPlantillasDTO>, List<Order>>> eff = basePlantillasCriteria(
				allocatedCompanyId, addrid, businessSubdivId).flatMap(t -> CriteriaBuilderEffect.lift((cb, cq) -> {
					val instrument = t.getRoot();
					val clauses = t.getPredicate();
					if (afterLastModified != null)
						clauses.getExpressions()
								.add(cb.greaterThan(instrument.get(Instrument_.lastModified), afterLastModified));
					if (!checkIfNullOrEmpty(plantno))
						clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.plantno), plantno));
					return t.withPredicate(clauses);
				})).flatMap(RootSelectRestrictionOrder::toCriteriaGenerator);
		CriteriaQueryGenerator<InstrumentPlantillasDTO> cqg = cb -> cq -> eff.unsafeApply(cb, cq);
		this.completePagedResultSet(prs, InstrumentPlantillasDTO.class, cqg);
		return prs;
	}

	public List<InstrumentCheckDuplicatesDto> getSimilarIns(int coid, String plantNo, String serialNo, Integer modelId,
			Boolean mfrReqForModel, Boolean searchModelMatches) {
		val simplyPlant = simplifyNo(plantNo);
		val simplySerial = simplifyNo(serialNo);
		if (simplyPlant.isEmpty() && simplySerial.isEmpty())
			return Collections.emptyList();
		CriteriaBuilderEffect<RootSelectRestrictionOrder<Instrument, InstrumentCheckDuplicatesDto>> builder = DuplicationSearchForCriteriaBuilder
				.buildCriteria(coid).flatMap(root -> CriteriaBuilderEffect.lift((cb, cq) -> {
					val instrument = root.getRoot();
					val modelPredicate = (modelId != null) && (mfrReqForModel != null) && (mfrReqForModel)
							&& (searchModelMatches) ? Stream.of(cb.equal(instrument.get(Instrument_.model), modelId))
									: Stream.<Predicate>empty();
					val predicates = cb.and(Stream.of(Stream.of(root.getPredicate()), modelPredicate)
							.flatMap(Function.identity()).toArray(Predicate[]::new));
					return root.withPredicate(predicates);
				}));
		CriteriaBuilderEffect<RootSelectRestrictionOrder<Instrument, InstrumentCheckDuplicatesDto>> bothBuilder = builder
				.flatMap(root -> CriteriaBuilderEffect.lift((cb, cq) -> {
					val instrument = root.getRoot();
					val predicates = root.getPredicate();
					val plantPredicate = cb.equal(instrument.get(Instrument_.simplifiedPlantNo), simplyPlant);
					val serialPredicate = cb.equal(instrument.get(Instrument_.simplifiedSerialNo), simplySerial);
					val newPredicate = simplyPlant.isEmpty() ? serialPredicate
							: simplySerial.isEmpty() ? plantPredicate : cb.or(plantPredicate, serialPredicate);
					return root.withPredicate(cb.and(newPredicate, predicates));
				}));
		val calculateDistance = calculateDistanceGenerator(plantNo != null ? plantNo.toLowerCase() : plantNo,
				simplyPlant, serialNo != null ? serialNo.toLowerCase() : serialNo, simplySerial);
		return produceResultsForInstrumentCheckDuplicatesDto(bothBuilder)
				.peek(i -> i.setScore(calculateDistance.apply(i.getPlantNo().toLowerCase(), i.getSimplifiedPlantNo(),
						i.getSerialNo().toLowerCase(), i.getSimplifiedSerialNo())))
				.sorted(Comparator.comparing(InstrumentCheckDuplicatesDto::getScore).reversed())
				.collect(Collectors.toList());
	}

	private Stream<InstrumentCheckDuplicatesDto> produceResultsForInstrumentCheckDuplicatesDto(
			CriteriaBuilderEffect<RootSelectRestrictionOrder<Instrument, InstrumentCheckDuplicatesDto>> builder) {
		return getResultList(cb -> {
			val cq = cb.createQuery(InstrumentCheckDuplicatesDto.class);
			val t = builder.unsafeApply(cb, cq);
			cq.select(t.getSelect());
			cq.where(t.getPredicate());
			cq.orderBy(t.getOrder());
			return cq;
		}).stream();
	}

	public Function4<String, String, String, String, Double> calculateDistanceGenerator(String plantIn,
			String simplyPlantIn, String serialIn, String simplySerialIn) {
		return (plantNo, simplyPlantNo, serialNo, simplySerialNo) -> {
			if (plantNo.length() + serialNo.length() == 0)
				return 0.0;
			val plantInDistance = similarityFactor(plantIn, plantNo, plantNoWeight);
			val serialInDistance = similarityFactor(serialIn, serialNo, serialNoWeight);
			val simplyPlantDistance = similarityFactor(simplyPlantIn, simplyPlantNo, simplifiedPlantNoWeight);
			val simplySerialDistance = similarityFactor(simplySerialIn, simplySerialNo, simplifiedSerialNoWeight);
			val denominator = plantNoWeight + simplifiedPlantNoWeight + serialNoWeight + simplifiedSerialNoWeight + 4;
			val numerator = Stream.of(Tuple.of(plantInDistance, plantNoWeight),
					Tuple.of(simplyPlantDistance, simplifiedPlantNoWeight), Tuple.of(serialInDistance, serialNoWeight),
					Tuple.of(simplySerialDistance, simplifiedSerialNoWeight))
					.reduce(Tuple.of(0d, 0d), (p1, p2) -> Tuple.of(p1._1() + p2._1(), p1._2() + p2._2()));
			return numerator._1 / denominator;
		};
	}

	public Double similarityFactor(String s1, String s2, Double weight) {
		if (StringUtils.isEmpty(s1) || StringUtils.isEmpty(s1))
			return 0d;
		return (1 - 1.0 * Levenshtein.distance(s1.toLowerCase(), s2.toLowerCase()) / Math.max(s1.length(), s2.length()))
				+ (s1.equalsIgnoreCase(s2) ? weight : 0d);
	}

	private String simplifyNo(String value) {
		return InstrumentUtils.simplifyId(value);
	}

	@Override
	public boolean instrumOnJob(int instrumId, int jobId) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Boolean.class);
			val ji = cq.from(JobItem.class);
			cq.where(cb.and(cb.equal(ji.get(JobItem_.inst), instrumId), cb.equal(ji.get(JobItem_.job), jobId)));
			cq.select(cb.literal(true));
			return cq;
		}).orElse(false);
	}

	@Override
	public PagedResultSet<Instrument> queryInstruments(PagedResultSet<Instrument> prs,
			InstrumentAndModelSearchForm<?> form, NewJobItemSearchOrderByEnum orderBy) {

		CriteriaBuilderEffect<Triple<Expression<?>, Selection<Instrument>, List<Order>>> effect = QueryInstrumentCriteriaBuilder
				.buildInstrumentCriteria(form, false).flatMap(tuple -> CriteriaBuilderEffect.lift((cb, cq) -> {
					val order = orderBy.equals(NewJobItemSearchOrderByEnum.SERIAL_NO)
							? cb.asc(tuple.getRoot().get(Instrument_.serialno))
							: orderBy.equals(NewJobItemSearchOrderByEnum.BARCODE)
									? cb.asc(tuple.getRoot().get(Instrument_.plantid))
									: cb.asc(tuple.getRoot().get(Instrument_.plantno));
					return tuple.withOrder(Collections.singletonList(order));
				})).flatMap(RootSelectRestrictionOrder::toCriteriaGenerator);
		CriteriaQueryGenerator<Instrument> cqg = cb -> cq -> effect.unsafeApply(cb, cq);
		this.completePagedResultSet(prs, Instrument.class, cqg);
		return prs;
	}

	public PagedResultSet<Instrument> queryInstrumentsPaged(PagedResultSet<Instrument> prs,
			InstrumentAndModelSearchForm<?> form, NewJobItemSearchOrderByEnum orderBy, Locale locale) {
		return newJobItemInstrumentSearch(prs, form, locale, orderBy);
	}

	@Override
	public PagedResultSet<InstrumentModel> queryDistinctModelsFromInstruments(PagedResultSet<InstrumentModel> prs,
			InstrumentAndModelSearchForm<?> form) {
		val effect = QueryInstrumentCriteriaBuilder.buildInstrumentCriteria(form, true).map(t -> {
			val model = t.getRoot().get(Instrument_.model);
			return t.withSelect2(model);
		}).flatMap(t -> CriteriaBuilderEffect.lift((cb, cq) -> {
			cq.distinct(true);
			return t.withOrder(Collections.emptyList());
		})).<Triple<Expression<?>, Selection<InstrumentModel>, List<Order>>>flatMap(
				r -> CriteriaBuilderEffect.lift((cb, cq) -> {
					cq.where(r.getPredicate());
					return Triple.of(r.getRoot().get(Instrument_.model), r.getSelect(), r.getOrder());
				}));
		CriteriaQueryGenerator<InstrumentModel> cqg = cb -> cq -> effect.unsafeApply(cb, cq);

		this.completePagedResultSet(prs, InstrumentModel.class, cqg);
		return prs;
	}

	public PagedResultSet<InstrumentModel> queryDistinctModelsFromInstrumentsPaged(PagedResultSet<InstrumentModel> prs,
			PagedResultSet<Instrument> prsInst, InstrumentAndModelSearchForm<?> form, Locale locale,
			NewJobItemSearchOrderByEnum orderBy) {
		PagedResultSet<Instrument> instrus = queryInstrumentsPaged(prsInst, form, orderBy, locale);
		List<Instrument> instruments = (List<Instrument>) instrus.getResults();
		List<InstrumentModel> models = new ArrayList<>();
		// get the distinct models from this list
		for (Instrument i : instruments) {
			if (!models.contains(i.getModel())) {
				models.add(i.getModel());
			}
		}
		// get count of results
		prs.setResultsCount(models.size());
		// update the query so it will return the correct number
		// of rows
		// from the correct start point
		if ((prs.getResultsPerPage() + prs.getStartResultsFrom()) >= models.size()) {
			models = models.subList(prs.getStartResultsFrom(), (models.size()));
		} else {
			models = models.subList(prs.getStartResultsFrom(), (prs.getStartResultsFrom() + prs.getResultsPerPage()));
		}
		// get results
		prs.setResults(models);
		return prs;
	}

	private Function<CriteriaBuilder, CriteriaQuery<Instrument>> getRecallCriteria(Integer coid, LocalDate dateTo) {
		return cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			val usageType = instrument.join(Instrument_.usageType);
			val storageType = instrument.join(Instrument_.storageType);
			val clauses = cb.and(cb.equal(instrument.get(Instrument_.status), InstrumentStatus.IN_CIRCULATION),
					cb.isTrue(usageType.get(InstrumentUsageType_.recall)),
					cb.isTrue(storageType.get(InstrumentStorageType_.recall)),
					cb.or(cb.lessThan(instrument.get(Instrument_.nextCalDueDate), dateTo),
							cb.and(cb.isTrue(instrument.get(Instrument_.calExpiresAfterNumberOfUses)),
									cb.greaterThanOrEqualTo(instrument.get(Instrument_.usesSinceLastCalibration),
											instrument.get(Instrument_.permittedNumberOfUses)))),
					cb.equal(instrument.join(Instrument_.comp), coid));
			cq.where(clauses);
			return cq;
		};
	}

	@Override
	public List<Instrument> queryForRecall(Company company, LocalDate dateTo) {
		if (company == null)
			throw new IllegalArgumentException("company must be specified but was null");
		return getResultList(this.getRecallCriteria(company.getCoid(), dateTo));
	}

	@Override
	public List<Integer> queryForRecallWithActiveJobItems(Company company, LocalDate dateTo) {
		if (company == null)
			throw new IllegalArgumentException("company must be specified but was null");
		return getResultList(cb -> {
			val cq = cb.createQuery(Integer.class);
			val instrument = cq.from(Instrument.class);
			val comp = instrument.join(Instrument_.comp);
			val jobItemsState = instrument.join(Instrument_.jobItems).join(JobItem_.state);
			val caluses = cb.and(cb.equal(comp, company.getId()), cb.isTrue(jobItemsState.get(ItemState_.active)));
			cq.where(caluses);
			cq.distinct(true);
			cq.select(instrument.get(Instrument_.plantid));
			return cq;
		});
	}

	@Override
	public List<InstrumentAssetSearchResultWrapper> searchBusinessInstrumentsForAssetsHQL(int coid, boolean standards,
			String mfr, String model, String desc, String plantid, String plantno, String serialno) {
		return getResultList(buildQueryForSearchBusinessInstrumentsForAssets(coid, standards, mfr, model, desc, plantid,
				plantno, serialno));
	}

	private Function<CriteriaBuilder, CriteriaQuery<InstrumentAssetSearchResultWrapper>> buildQueryForSearchBusinessInstrumentsForAssets(
			int coid, boolean standards, String mfr, String model, String desc, String plantid, String plantno,
			String serialno) {
		return cb -> {
			val cq = cb.createQuery(InstrumentAssetSearchResultWrapper.class);
			val instrument = cq.from(Instrument.class);
			val modelJ = instrument.join(Instrument_.model);
			val description = modelJ.join(InstrumentModel_.description);
			val modelMfr = modelJ.join(InstrumentModel_.mfr, JoinType.LEFT);
			val insMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			val asset = instrument.join(Instrument_.asset, JoinType.LEFT);

			List<Predicate> dis = !checkIfNullOrEmpty(mfr)
					? Collections.singletonList(cb.or(Stream
							.of(ilikePredicateList(cb, modelMfr.get(Mfr_.name), mfr, MatchMode.START),
									ilikePredicateList(cb, insMfr.get(Mfr_.name), mfr, MatchMode.START))
							.flatMap(List::stream).toArray(Predicate[]::new)))
					: Collections.emptyList();

			val stream = Stream.of(dis, companyPredicate(cb, instrument, coid),
					ilikePredicateList(cb, modelJ.get(InstrumentModel_.model), model, MatchMode.START),
					ilikePredicateList(cb, description.get(Description_.description), desc, MatchMode.START),
					ilikePredicateList(cb, instrument.get(Instrument_.serialno), serialno, MatchMode.START),
					ilikePredicateList(cb, instrument.get(Instrument_.plantno), plantno, MatchMode.START),
					ilikePredicateList(cb, instrument.get(Instrument_.plantid).as(String.class), plantid,
							MatchMode.START),
					Collections.singletonList(cb.equal(instrument.get(Instrument_.calibrationStandard), standards)));
			val caluses = cb.and(stream.flatMap(List::stream).toArray(Predicate[]::new));

			cq.where(caluses);

			val select = cb.construct(InstrumentAssetSearchResultWrapper.class, modelJ.get(InstrumentModel_.modelid),
					cb.selectCase().when(cb.isTrue(modelMfr.get(Mfr_.genericMfr)), insMfr.get(Mfr_.name))
							.otherwise(modelMfr.get(Mfr_.name)),
					modelJ.get(InstrumentModel_.model), description.get(Description_.description),
					instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
					instrument.get(Instrument_.serialno), instrument.get(Instrument_.scrapped),
					instrument.get(Instrument_.calibrationStandard),
					cb.selectCase().when(cb.isNull(asset), cb.literal(false)).otherwise(cb.literal(true)),
					asset.get(PhysicalAsset_.assetId), asset.get(PhysicalAsset_.assetNo));
			cq.select(select);
			return cq;

		};
	}

	private CriteriaQuery<Long> instrumentCountQuery(Company company, String plantNo, String serialNo, Description desc,
			InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv, Boolean outOfCal,
			InstrumentModelFamily family, String modelText, String customerDescription, Integer plantId,
			Boolean calStandard, WebSearchInstrumentForm form) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		return instrumentQuery(cb.createQuery(Long.class), cb, company, plantNo, serialNo, desc, instModel, mfr,
				instrumentOwner, subdiv, outOfCal, family, modelText, customerDescription, plantId, calStandard, form,
				Long.class);

	}

	private CriteriaQuery<InstrumentSearchResultsDTO> instrumentSearchQuery(Company company, String plantNo,
			String serialNo, Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner,
			Subdiv subdiv, Boolean outOfCal, InstrumentModelFamily family, String modelText, String customerDescription,
			Integer plantId, Boolean calStandard, WebSearchInstrumentForm form) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		return instrumentQuery(cb.createQuery(InstrumentSearchResultsDTO.class), cb, company, plantNo, serialNo, desc,
				instModel, mfr, instrumentOwner, subdiv, outOfCal, family, modelText, customerDescription, plantId,
				calStandard, form, InstrumentSearchResultsDTO.class);
	}

	private <T> CriteriaQuery<T> instrumentQuery(CriteriaQuery<T> cq, CriteriaBuilder cb, Company company,
			String plantNo, String serialNo, Description desc, InstrumentModel instModel, Mfr mfr,
			Contact instrumentOwner, Subdiv subdiv, Boolean outOfCal, InstrumentModelFamily family, String modelText,
			String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form,
			Class<T> clazz) {
		Locale locale = LocaleContextHolder.getLocale();

		Root<Instrument> instrument = cq.from(Instrument.class);

		Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);

		Join<InstrumentModel, Mfr> manufacturer = model.join(InstrumentModel_.mfr,
				javax.persistence.criteria.JoinType.LEFT);

		Join<Instrument, Mfr> instMfr = instrument.join(Instrument_.mfr, javax.persistence.criteria.JoinType.LEFT);

		Join<InstrumentModel, Translation> modelNames = model.join(InstrumentModel_.nameTranslations,
				javax.persistence.criteria.JoinType.LEFT);
		Predicate modelNameJoinRestriction = cb.conjunction();
		modelNameJoinRestriction.getExpressions().add(cb.equal(modelNames.get(Translation_.locale), locale));
		modelNames.on(modelNameJoinRestriction);

		Join<InstrumentModel, Description> subfamily = model.join(InstrumentModel_.description);
		Join<Description, Translation> subfamilyNames = subfamily.join(Description_.translations,
				javax.persistence.criteria.JoinType.LEFT);
		Predicate subfamilyNameJoinRestriction = cb.conjunction();
		subfamilyNameJoinRestriction.getExpressions().add(cb.equal(subfamilyNames.get(Translation_.locale), locale));
		subfamilyNames.on(subfamilyNameJoinRestriction);

		Join<Instrument, Contact> owner = instrument.join(Instrument_.con);

		Join<Instrument, Calibration> lastCalibration = instrument.join(Instrument_.lastCal,
				javax.persistence.criteria.JoinType.LEFT);

		Join<Instrument, InstrumentStorageType> storageType = instrument.join(Instrument_.storageType,
				javax.persistence.criteria.JoinType.LEFT);
		Join<InstrumentStorageType, Translation> storageTypeName = storageType
				.join(InstrumentStorageType_.nametranslation, javax.persistence.criteria.JoinType.LEFT);
		Predicate storageTypeNameJoinRestriction = cb.conjunction();
		storageTypeNameJoinRestriction.getExpressions().add(cb.equal(storageTypeName.get(Translation_.locale), locale));
		storageTypeName.on(storageTypeNameJoinRestriction);

		Join<Instrument, InstrumentUsageType> usageType = instrument.join(Instrument_.usageType,
				javax.persistence.criteria.JoinType.LEFT);
		Join<InstrumentUsageType, Translation> usageTypeName = usageType.join(InstrumentUsageType_.nametranslation,
				javax.persistence.criteria.JoinType.LEFT);
		Predicate usageTypeNameJoinRestriction = cb.conjunction();
		usageTypeNameJoinRestriction.getExpressions().add(cb.equal(usageTypeName.get(Translation_.locale), locale));
		usageTypeName.on(usageTypeNameJoinRestriction);

		Join<Instrument, Address> joinedAddress = instrument.join(Instrument_.add);

		Join<Contact, Subdiv> subDiv = owner.join(Contact_.sub);

		Join<Instrument, Location> loc = instrument.join(Instrument_.loc, javax.persistence.criteria.JoinType.LEFT);

		Join<Instrument, ServiceType> serviceType = instrument.join(Instrument_.defaultServiceType,
				javax.persistence.criteria.JoinType.LEFT);
		Join<ServiceType, Translation> serviceTypeName = serviceType.join(ServiceType_.shortnameTranslation,
				javax.persistence.criteria.JoinType.LEFT);
		Predicate serviceTypeNameJoinRestriction = cb.conjunction();
		serviceTypeNameJoinRestriction.getExpressions().add(cb.equal(serviceTypeName.get(Translation_.locale), locale));
		serviceTypeName.on(serviceTypeNameJoinRestriction);

		Join<Instrument, InstrumentComplementaryField> complementaryFields = instrument
				.join(Instrument_.instrumentComplementaryFields, javax.persistence.criteria.JoinType.LEFT);

		Subquery<CalibrationVerificationStatus> sqLastCalVerStatus = cq.subquery(CalibrationVerificationStatus.class);
		Root<Certificate> rootLastCalVerStatus = sqLastCalVerStatus.from(Certificate.class);
		sqLastCalVerStatus.where(cb.equal(rootLastCalVerStatus.get(Certificate_.certid),
				cb.function("GetMostRecentCertificate", Integer.class, instrument.get(Instrument_.plantid))));
		sqLastCalVerStatus.select(rootLastCalVerStatus.get(Certificate_.calibrationVerificationStatus));

		Subquery<BigDecimal> sqLastDeviation = cq.subquery(BigDecimal.class);
		Root<Certificate> rootLastDeviation = sqLastDeviation.from(Certificate.class);
		sqLastDeviation.where(cb.equal(rootLastDeviation.get(Certificate_.certid),
				cb.function("GetMostRecentCertificate", Integer.class, instrument.get(Instrument_.plantid))));
		sqLastDeviation.select(rootLastDeviation.get(Certificate_.deviation));

		Subquery<Date> sqlgetcertLastcaldate = cq.subquery(Date.class);
		Root<Certificate> rootgetcertLastcaldate = sqlgetcertLastcaldate.from(Certificate.class);
		sqlgetcertLastcaldate.where(cb.equal(rootgetcertLastcaldate.get(Certificate_.certid),
				cb.function("GetMostRecentCertificate", Integer.class, instrument.get(Instrument_.plantid))));
		sqlgetcertLastcaldate.select(rootgetcertLastcaldate.get(Certificate_.calDate));

		Predicate clauses = cb.conjunction();

		if (company != null)
			clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.comp), company));
		if (!StringUtils.isEmpty(plantNo))
			clauses.getExpressions().add(cb.like(instrument.get(Instrument_.plantno), plantNo));
		if (!StringUtils.isEmpty(serialNo))
			clauses.getExpressions().add(cb.like(instrument.get(Instrument_.serialno), serialNo));
		if (desc != null)
			clauses.getExpressions().add(cb.equal(model.get(InstrumentModel_.description), desc));
		if (instModel != null)
			clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.model), instModel));
		if (family != null) {
			clauses.getExpressions().add(cb.equal(subfamily.get(Description_.family), family));
		}

		if (instrumentOwner != null) {
			clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.con), instrumentOwner));
		}
		if (mfr != null) {
			Predicate mfrPred = cb.disjunction();
			mfrPred.getExpressions().add(cb.equal(model.get(InstrumentModel_.mfr), mfr));
			mfrPred.getExpressions().add(cb.equal(instrument.get(Instrument_.mfr), mfr));
			clauses.getExpressions().add(mfrPred);
		} else if (subdiv != null)
			clauses.getExpressions().add(cb.equal(owner.get(Contact_.sub), subdiv));
		if (outOfCal != null) {

			if (outOfCal)
				clauses.getExpressions()
						.add(cb.or(cb.isNull(instrument.get(Instrument_.nextCalDueDate)),
								cb.lessThanOrEqualTo(instrument.get(Instrument_.nextCalDueDate),
										LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))));
			else
				clauses.getExpressions().add(cb.greaterThan(instrument.get(Instrument_.nextCalDueDate),
						LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())));
		}
		if (!StringUtils.isEmpty(modelText)) {
			Predicate modelTextPred = cb.disjunction();
			modelTextPred.getExpressions().add(cb.like(instrument.get(Instrument_.modelname), '%' + modelText + '%'));
			modelTextPred.getExpressions().add(cb.like(model.get(InstrumentModel_.model), '%' + modelText + '%'));
			clauses.getExpressions().add(modelTextPred);
			// or instrumentmodel's model text
		}
		if (!StringUtils.isEmpty(customerDescription))
			clauses.getExpressions()
					.add(cb.like(instrument.get(Instrument_.customerDescription), '%' + customerDescription + '%'));
		if (plantId != null && plantId > 0)
			clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.plantid), plantId));
		if (calStandard != null)
			clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.calibrationStandard), calStandard));
		// if a form is present then it's a web search so add some extra search
		// criteria if required.
		if (form != null) {

			String location = form.getLocation();
			if (form.getLocationId() != null && form.getLocationId() > 0) {
				clauses.getExpressions().add(cb.equal(loc.get(Location_.locationid), form.getLocationId()));
			} else if (!StringUtils.isEmpty(location)) {
				clauses.getExpressions().add(cb.like(loc.get(Location_.location), '%' + location + '%'));
			}

			if (form.getStatus() != null)
				clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.status), form.getStatus()));
			if (form.getUsageTypeId() > 0)
				clauses.getExpressions().add(cb.equal(usageType.get(InstrumentUsageType_.id), form.getUsageTypeId()));
			if (form.getCustomerManaged() != null)
				clauses.getExpressions()
						.add(cb.equal(instrument.get(Instrument_.customerManaged), form.getCustomerManaged()));
			// limit by next cal date between or on depending on instrument web
			// form selection
			if (form.getCalDueDate1() != null && form.getCalDueDate2() != null)
				clauses.getExpressions().add(cb.between(instrument.get(Instrument_.nextCalDueDate),
						form.getCalDueDate1(), form.getCalDueDate2()));
			else if (form.getCalDueDate1() != null)
				clauses.getExpressions()
						.add(cb.equal(instrument.get(Instrument_.nextCalDueDate), form.getCalDueDate1()));

			if (form.getLastCalDate1() != null && form.getLastCalDate2() != null)
				clauses.getExpressions().add(cb.between(lastCalibration.get(Calibration_.calDate),
						form.getLastCalDate1(), form.getLastCalDate2()));
			else if (form.getLastCalDate1() != null)
				clauses.getExpressions()
						.add(cb.equal(lastCalibration.get(Calibration_.calDate), form.getLastCalDate1()));

			// limit by number of remaining uses if entered
			if (form.getRemainingUses() != null && form.getRemainingUses() > 0) {
				clauses.getExpressions().add(cb.isTrue(instrument.get(Instrument_.calExpiresAfterNumberOfUses)));
				// instrument.permittedNumberOfUses - usesSinceLastCalibration
				// <= form.remainingUses
				clauses.getExpressions()
						.add(cb.lessThanOrEqualTo(
								cb.diff(instrument.get(Instrument_.permittedNumberOfUses),
										instrument.get(Instrument_.usesSinceLastCalibration)),
								form.getRemainingUses()));
			}

			// check the hasReplacement criteria
			if (form.getHasReplacement() != null && form.getHasReplacement()) {
				clauses.getExpressions().add(cb.isNotNull(instrument.get(Instrument_.replacement)));
			} else if (form.getHasReplacement() != null) {
				clauses.getExpressions().add(cb.isNull(instrument.get(Instrument_.replacement)));
			}

			if (form.getFlexibleFieldSearches() != null && form.getFlexibleFieldSearches().size() > 0) {
				for (FlexibleFieldSearchDTO flexibleFieldDTO : form.getFlexibleFieldSearches()) {
					InstrumentFieldDefinition instrumentFieldDefinition = instrumentFieldDefinitionService
							.get(flexibleFieldDTO.getFieldDefinitionId());
					switch (instrumentFieldDefinition.getFieldType()) {
					case STRING:
						if (!StringUtils.isEmpty(flexibleFieldDTO.getStringValue())) {
							Subquery<Instrument> sq = cq.subquery(Instrument.class);
							Root<InstrumentValueString> instValueString = sq.from(InstrumentValueString.class);
							Join<InstrumentValueString, Instrument> instrumentFromStringValue = instValueString
									.join(InstrumentValueString_.instrument);
							Predicate stringFieldCriteria = cb.conjunction();
							stringFieldCriteria.getExpressions()
									.add(cb.equal(instValueString.get(InstrumentValueString_.instrumentFieldDefinition),
											instrumentFieldDefinition));
							stringFieldCriteria.getExpressions()
									.add(cb.equal(instValueString.get(InstrumentValueString_.value),
											flexibleFieldDTO.getStringValue().trim()));
							sq.select(instrumentFromStringValue).where(stringFieldCriteria);
							clauses.getExpressions().add(cb.in(instrument).value(sq));
						}
						break;
					case DATETIME:
						if (flexibleFieldDTO.getDateValue() != null) {
							Subquery<Instrument> sq = cq.subquery(Instrument.class);
							Root<InstrumentValueDateTime> instValueDateTime = sq.from(InstrumentValueDateTime.class);
							Join<InstrumentValueDateTime, Instrument> instrumentFromDateTimeValue = instValueDateTime
									.join(InstrumentValueDateTime_.instrument);
							Predicate dateTimeFieldCriteria = cb.conjunction();
							dateTimeFieldCriteria.getExpressions()
									.add(cb.equal(
											instValueDateTime.get(InstrumentValueDateTime_.instrumentFieldDefinition),
											instrumentFieldDefinition));
							dateTimeFieldCriteria.getExpressions()
									.add(cb.equal(instValueDateTime.get(InstrumentValueDateTime_.value),
											flexibleFieldDTO.getDateValue()));
							sq.select(instrumentFromDateTimeValue).where(dateTimeFieldCriteria);
							clauses.getExpressions().add(cb.in(instrument).value(sq));
						}
						break;
					case BOOLEAN:
						if (flexibleFieldDTO.getBooleanValue() != null) {
							Subquery<Instrument> sq = cq.subquery(Instrument.class);
							Root<InstrumentValueBoolean> instValueBoolean = sq.from(InstrumentValueBoolean.class);
							Join<InstrumentValueBoolean, Instrument> instrumentFromBooleanValue = instValueBoolean
									.join(InstrumentValueBoolean_.instrument);
							Predicate booleanFieldCriteria = cb.conjunction();
							booleanFieldCriteria.getExpressions()
									.add(cb.equal(
											instValueBoolean.get(InstrumentValueBoolean_.instrumentFieldDefinition),
											instrumentFieldDefinition));
							booleanFieldCriteria.getExpressions()
									.add(cb.equal(instValueBoolean.get(InstrumentValueBoolean_.value),
											flexibleFieldDTO.getBooleanValue()));
							sq.select(instrumentFromBooleanValue).where(booleanFieldCriteria);
							clauses.getExpressions().add(cb.in(instrument).value(sq));
						}
						break;
					case SELECTION:
						if (flexibleFieldDTO.getSelectionValueId() != null
								&& flexibleFieldDTO.getSelectionValueId() > 0) {
							Subquery<Instrument> sq = cq.subquery(Instrument.class);
							Root<InstrumentValueSelection> instValueSelection = sq.from(InstrumentValueSelection.class);
							Join<InstrumentValueSelection, Instrument> instrumentFromSelectionValue = instValueSelection
									.join(InstrumentValueSelection_.instrument);
							Predicate selectionFieldCriteria = cb.conjunction();
							selectionFieldCriteria.getExpressions()
									.add(cb.equal(
											instValueSelection.get(InstrumentValueSelection_.instrumentFieldDefinition),
											instrumentFieldDefinition));

							selectionFieldCriteria.getExpressions()
									.add(cb.equal(
											instValueSelection
													.get(InstrumentValueSelection_.instrumentFieldLibraryValue),
											flexibleFieldDTO.getSelectionValueId()));
							sq.select(instrumentFromSelectionValue).where(selectionFieldCriteria);
							clauses.getExpressions().add(cb.in(instrument).value(sq));
						}
						break;
					case NUMERIC:
						if (flexibleFieldDTO.getNumericValue() != null && flexibleFieldDTO.getNumericValue() > 0) {
							Subquery<Instrument> sq = cq.subquery(Instrument.class);
							Root<InstrumentValueNumeric> instValueNumeric = sq.from(InstrumentValueNumeric.class);
							Join<InstrumentValueNumeric, Instrument> instrumentFromNumericValue = instValueNumeric
									.join(InstrumentValueNumeric_.instrument);
							Predicate numericFieldCriteria = cb.conjunction();
							numericFieldCriteria.getExpressions()
									.add(cb.equal(
											instValueNumeric.get(InstrumentValueNumeric_.instrumentFieldDefinition),
											instrumentFieldDefinition));
							numericFieldCriteria.getExpressions()
									.add(cb.equal(instValueNumeric.get(InstrumentValueNumeric_.value),
											flexibleFieldDTO.getNumericValue()));
							sq.select(instrumentFromNumericValue).where(numericFieldCriteria);
							clauses.getExpressions().add(cb.in(instrument).value(sq));
						}
						break;
					default:
						break;
					}
				}
			}
		}

		if (clazz.equals(InstrumentSearchResultsDTO.class)) {

			// Subquery to obtain last received date
			Subquery<ZonedDateTime> lastDateInSubquery = cq.subquery(ZonedDateTime.class);
			Root<JobItem> relatedJobItems = lastDateInSubquery.from(JobItem.class);
			lastDateInSubquery.select(cb.greatest(relatedJobItems.get(JobItem_.dateIn)));
			lastDateInSubquery.where(cb.equal(relatedJobItems.get(JobItem_.inst), instrument));

			// Subquery to obtain cal extension end date if any
			Subquery<LocalDate> extensionEndDateSubquery = cq.subquery(LocalDate.class);
			Root<PermitedCalibrationExtension> extension = extensionEndDateSubquery.from(PermitedCalibrationExtension.class);
			extensionEndDateSubquery.select(extension.get(PermitedCalibrationExtension_.extensionEndDate));
			extensionEndDateSubquery.where(cb.equal(extension.get(PermitedCalibrationExtension_.instrument), instrument));

			cq.select(cb.construct(clazz, instrument.get(Instrument_.plantid), manufacturer.get(Mfr_.name),
					instMfr.get(Mfr_.name), model.get(InstrumentModel_.model), instrument.get(Instrument_.modelname),
					subfamilyNames.get(Translation_.translation), modelNames.get(Translation_.translation),
					instrument.get(Instrument_.customerDescription),
					cb.concat(cb.concat(owner.get(Contact_.firstName), " "), owner.get(Contact_.lastName)),
					instrument.get(Instrument_.plantno), instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.nextCalDueDate), extensionEndDateSubquery.getSelection(),
					lastCalibration.get(Calibration_.calDate), instrument.get(Instrument_.calFrequency),
					instrument.get(Instrument_.calFrequencyUnit), instrument.get(Instrument_.status),
					usageTypeName.get(Translation_.translation), storageTypeName.get(Translation_.translation),
					subDiv.get(Subdiv_.subname), instrument.get(Instrument_.scrapped),
					joinedAddress.get(Address_.addr1), joinedAddress.get(Address_.addr2),
					joinedAddress.get(Address_.addr3), joinedAddress.get(Address_.town),
					joinedAddress.get(Address_.postcode), loc.get(Location_.location),
					instrument.get(Instrument_.calibrationStandard), model.get(InstrumentModel_.modelMfrType),
					owner.get(Contact_.position), instrument.get(Instrument_.defaultClientRef),
					lastDateInSubquery.getSelection(),
					cb.function("GetNthCalDate", Date.class, instrument.get(Instrument_.plantid), cb.literal(2)),
					cb.function("GetNthCalDate", Date.class, instrument.get(Instrument_.plantid), cb.literal(3)),
					serviceTypeName.get(Translation_.translation),
					// lastCertificate.get(MostRecentCertificateView_.calibrationVerificationStatus),
					// lastCertificate.get(MostRecentCertificateView_.deviation),
					sqLastCalVerStatus.getSelection(), sqLastDeviation.getSelection(),
					complementaryFields.get(InstrumentComplementaryField_.customerAcceptanceCriteria),sqlgetcertLastcaldate.getSelection()));

			cq.orderBy(cb.asc(instrument.get(Instrument_.plantid)));
		} else if (clazz.equals(Long.class)) {
			cq.multiselect(cb.count(instrument));
		}

		cq.where(clauses);

		return cq;
	}

	@Override
	public List<InstrumentSearchResultsDTO> searchInstruments(Company company, String plantNo, String serialNo,
			Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv,
			Address address, Boolean outOfCal, InstrumentModelFamily family, String modelText,
			String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form) {
		CriteriaQuery<InstrumentSearchResultsDTO> cq = instrumentSearchQuery(company, plantNo, serialNo, desc,
				instModel, mfr, instrumentOwner, subdiv, outOfCal, family, modelText, customerDescription, plantId,
				calStandard, form);
		TypedQuery<InstrumentSearchResultsDTO> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public PagedResultSet<InstrumentSearchResultsDTO> searchInstrumentsPaged(
			PagedResultSet<InstrumentSearchResultsDTO> pagedResultSet, Company company, String plantNo, String serialNo,
			Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv,
			Address address, Boolean outOfCal, InstrumentModelFamily family, String modelText,
			String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form) {
		CriteriaQuery<Long> ccq = instrumentCountQuery(company, plantNo, serialNo, desc, instModel, mfr,
				instrumentOwner, subdiv, outOfCal, family, modelText, customerDescription, plantId, calStandard, form);
		TypedQuery<Long> countquery = getEntityManager().createQuery(ccq);
		pagedResultSet.setResultsCount(countquery.getSingleResult().intValue());
		CriteriaQuery<InstrumentSearchResultsDTO> cq = instrumentSearchQuery(company, plantNo, serialNo, desc,
				instModel, mfr, instrumentOwner, subdiv, outOfCal, family, modelText, customerDescription, plantId,
				calStandard, form);
		TypedQuery<InstrumentSearchResultsDTO> query = getEntityManager().createQuery(cq);
		query.setFirstResult(pagedResultSet.getStartResultsFrom());
		query.setMaxResults(pagedResultSet.getResultsPerPage());
		pagedResultSet.setResults(query.getResultList());
		return pagedResultSet;
	}

	@Override
	public PagedResultSet<Instrument> searchInstrumentsPaged(PagedResultSet<Instrument> prs,
			InstrumentAndModelSearchForm<Instrument> form, Integer subdivid, Integer personid, Integer addressid,
			Boolean outOfCal, NewQuoteInstSortType sortType, boolean ascOrder, Locale locale) {
		val effect = SearchInstrumentPagedCriteriaBuilder
				.searchInstrumentPageEffect(form, subdivid, personid, addressid, outOfCal, sortType, ascOrder, locale)
				.flatMap(RootSelectRestrictionOrder::toCriteriaGenerator);
		CriteriaQueryGenerator<Instrument> cqg = cb -> cq -> effect.unsafeApply(cb, cq);
		completePagedResultSet(prs, Instrument.class, cqg);
		return prs;
	}

	@Override
	public List<Instrument> searchPlantNo(Collection<Integer> companyIds, String plantno) {
		return getResultList(cb -> {
			CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			Join<Instrument, Company> company = instrument.join(Instrument_.comp, JoinType.LEFT);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(ilike(cb, instrument.get(Instrument_.plantno), plantno.toLowerCase() + "%"));
			clauses.getExpressions().add(company.get(Company_.coid).in(companyIds));
			cq.where(clauses);
			return cq;
		});
	}

	@Deprecated
	private Criteria getBasicRecallCriterion() {
		Criteria crit = getSession().createCriteria(Instrument.class);

		crit.add(Restrictions.conjunction().add(Restrictions.ne("status", InstrumentStatus.BER))
				.add(Restrictions.ne("status", InstrumentStatus.DO_NOT_RECALL)));

		// filter out any other properties that prevent instruments being recall
		crit.createCriteria("usageType").add(Restrictions.eq("recall", true));
		crit.createCriteria("storageType").add(Restrictions.eq("recall", true));

		return crit;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Instrument> webSearchInstrumentsDueForCalibration(LocalDate dateFrom, LocalDate dateTo, Integer coid,
			Integer subdivid, Integer addrid, Integer personid) {
		Criteria crit = getBasicRecallCriterion();
		Criteria addCrit = null;

		crit.createCriteria("comp").add(Restrictions.idEq(coid));
		if (personid != null) {
			crit.createCriteria("con").add(Restrictions.idEq(personid));
		}
		if (addrid != null) {
			addCrit = crit.createCriteria("add");
			addCrit.add(Restrictions.idEq(addrid));
		}
		if (subdivid != null) {
			if (addCrit == null) {
				addCrit = crit.createCriteria("add");
			}
			addCrit.createCriteria("sub").add(Restrictions.idEq(subdivid));
		}

		Conjunction dueDateConj = Restrictions.conjunction();
		if (dateFrom != null) {
			dueDateConj.add(Restrictions.ge("nextCalDueDate", dateFrom));
		}
		if (dateTo != null) {
			dueDateConj.add(Restrictions.le("nextCalDueDate", dateTo));
		}

		Conjunction numberOfUsesConj = Restrictions.conjunction();
		numberOfUsesConj.add(Restrictions.eq("calExpiresAfterNumberOfUses", true));
		numberOfUsesConj.add(Restrictions.geProperty("usesSinceLastCalibration", "permittedNumberOfUses"));

		Disjunction disj = Restrictions.disjunction();
		disj.add(dueDateConj);
		disj.add(numberOfUsesConj);

		crit.add(disj);

		return crit.list();
	}

	@Override
	public List<InstrumentSearchResultWrapper> websearchInstruments(int coid, String mfr, String model, String desc,
			Integer descid, String plantid, String plantno, String serialno, Locale userLocale, Locale primaryLocale) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentSearchResultWrapper> cq = cb.createQuery(InstrumentSearchResultWrapper.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			Join<Instrument, InstrumentModel> modelJ = instrument.join(Instrument_.model);
			Join<InstrumentModel, Description> description = modelJ.join(InstrumentModel_.description);
			Join<InstrumentModel, Translation> model_tr_user = modelJ.join(InstrumentModel_.nameTranslations,
					JoinType.LEFT);
			Join<InstrumentModel, Translation> model_tr_primary = modelJ.join(InstrumentModel_.nameTranslations,
					JoinType.LEFT);

			Join<Instrument, Company> comp = instrument.join(Instrument_.comp);
			Join<InstrumentModel, Mfr> mfrmod = modelJ.join(InstrumentModel_.mfr, JoinType.LEFT);
			Join<Instrument, Mfr> mfrins = instrument.join(Instrument_.mfr, JoinType.LEFT);

			Predicate clauses = cb.and(
					cb.or(cb.like(mfrmod.get(Mfr_.name), mfr + "%"), cb.like(mfrins.get(Mfr_.name), mfr + "%")),
					cb.equal(comp.get(Company_.coid), coid), cb.like(modelJ.get(InstrumentModel_.model), model + "%"),
					cb.like(instrument.get(Instrument_.plantno), plantno + "%"),
					cb.like(instrument.get(Instrument_.serialno), serialno + "%"),
					descid != null && descid > 0 ? cb.equal(description.get(Description_.id), descid)
							: cb.like(description.get(Description_.description), desc + "%"),
					cb.or(cb.like(instrument.get(Instrument_.plantid).as(String.class), plantid + "%"),
							cb.like(instrument.get(Instrument_.plantid).as(String.class), plantid + "%")),
					cb.equal(model_tr_user.get(Translation_.locale), userLocale),
					cb.equal(model_tr_primary.get(Translation_.locale), primaryLocale));
			cq.distinct(true); // In case of duplicate translation
			cq.select(cb.construct(InstrumentSearchResultWrapper.class, modelJ.get(InstrumentModel_.modelid),
					cb.selectCase().when(cb.equal(mfrmod.get(Mfr_.genericMfr), 1), mfrins.get(Mfr_.name))
							.otherwise(mfrmod.get(Mfr_.name)),
					modelJ.get(InstrumentModel_.model), description.get(Description_.description),
					instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
					instrument.get(Instrument_.serialno), instrument.get(Instrument_.scrapped),
					model_tr_user.get(Translation_.translation), model_tr_primary.get(Translation_.translation)));
			cq.where(clauses);
			return cq;
		});
	}

	/**
	 * Note, replaced previous Hibernate criteria with direct function call
	 * Previous implementation would result in a recurring infinite loop SELECT
	 * function in some cases 2018-10-16 GB
	 */
	@Override
	public Date getLastCalDate(Instrument instrument) {
		Session session = getEntityManager().unwrap(Session.class);
		return session.doReturningWork(connection -> {
			try (CallableStatement function = connection.prepareCall("{ ? = call GetNthCalDate(?,?) }")) {
				function.registerOutParameter(1, Types.DATE);
				function.setInt(2, instrument.getPlantid());
				function.setInt(3, 1);
				function.execute();
				return function.getDate(1);
			}
		});
	}

	public List<PossibleInstrumentDTO> lookupPossibleInstruments(int clientCompanyId, int subdivid, int asnid,
			Locale locale, List<Integer> plantids, List<String> plantnos, List<String> serialnos) {
		return getResultList(cb -> {
			val cq = cb.createQuery(PossibleInstrumentDTO.class);
			val instrument = cq.from(Instrument.class);
			val subdiv = instrument.join(Instrument_.add).join(Address_.sub);
			val model = instrument.join(Instrument_.model);
			val description = model.join(InstrumentModel_.description);
			val translation = joinTranslation(cb, description, Description_.translations, locale);
			val serviceType = instrument.join(Instrument_.defaultServiceType, JoinType.LEFT);
			val calibrationType = serviceType.join(ServiceType_.calibrationType, JoinType.LEFT);
			val serviceCapabilities = model.join(InstrumentModel_.serviceCapabilities, JoinType.LEFT);
            serviceCapabilities.on(cb.and(
                cb.equal(instrument.get(Instrument_.capability),
                    serviceCapabilities.get(ServiceCapability_.capability)),
                cb.equal(serviceCapabilities.get(ServiceCapability_.organisation), subdivid)));

			val modelCalibrationType = serviceCapabilities.join(ServiceCapability_.calibrationType, JoinType.LEFT);

			val predicatesArray = Stream.of(
					!plantids.isEmpty() ? Stream.of(instrument.get(Instrument_.plantid).in(plantids))
							: Stream.<Predicate>empty(),
					!plantnos.isEmpty() ? Stream.of(instrument.get(Instrument_.plantno).in(plantnos))
							: Stream.<Predicate>empty(),
					!serialnos.isEmpty() ? Stream.of(instrument.get(Instrument_.serialno).in(serialnos))
							: Stream.<Predicate>empty())
					.flatMap(Function.identity()).toArray(Predicate[]::new);

			cq.where(cb.equal(subdiv.get(Subdiv_.comp), clientCompanyId), cb.or(predicatesArray));

			val select = cb.construct(PossibleInstrumentDTO.class, instrument.get(Instrument_.plantid),
					instrument.get(Instrument_.plantno), instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.status), subdiv.get(Subdiv_.comp).get(Company_.coid), translation,
					description.get(Description_.id), quotationItemsCalTypeId(cb, cq, instrument).getSelection(),
					jobItemCalTypeSubquery(cb, cq, instrument).getSelection(),
					calibrationType.get(CalibrationType_.serviceType).get(ServiceType_.serviceTypeId),
					modelCalibrationType.get(CalibrationType_.serviceType).get(ServiceType_.serviceTypeId),
					asnidSubquery(cb, cq, instrument, asnid).getSelection(),
					jobItemIdSubquery(cb, cq, instrument).getSelection(),
					jobItemViewCodeSubquery(cb, cq, instrument).getSelection());
			cq.select(select);
			return cq;
		});
	}

	private Subquery<String> jobItemViewCodeSubquery(CriteriaBuilder cb, CriteriaQuery<PossibleInstrumentDTO> cq,
			Root<Instrument> instrument) {
		val sq = cq.subquery(String.class);
		val ji = sq.from(JobItem.class);
		sq.select(StringJpaUtils.trimAndConcatWithSeparator(ji.join(JobItem_.job, JoinType.LEFT).get(Job_.jobno),
				ji.get(JobItem_.itemNo).as(String.class), ".").apply(cb));

		val ssq = sq.subquery(Integer.class);
		val inst = ssq.from(Instrument.class);
		val sji = inst.join(Instrument_.jobItems, JoinType.LEFT);
		val ist = sji.join(JobItem_.state, JoinType.LEFT);
		ssq.where(cb.equal(inst, instrument), cb.isTrue(ist.get(ItemState_.active)));
		ssq.select(cb.max(sji.get(JobItem_.jobItemId)));

		sq.where(cb.equal(ji, ssq.getSelection()));
		return sq;
	}

	private Subquery<Integer> asnidSubquery(CriteriaBuilder cb, CriteriaQuery<PossibleInstrumentDTO> cq,
			Root<Instrument> instrument, Integer asnId) {
		val sq = cq.subquery(Integer.class);
		val asn = sq.from(Asn.class);
		val asnItem = asn.join(Asn_.asnItems);
		sq.where(cb.equal(asnItem.get(AsnItem_.instrument), instrument), cb.notEqual(asn.get(Asn_.id), asnId),
				cb.isNull(asn.get(Asn_.job)));
		sq.select(cb.max(asn.get(Asn_.id)));
		return sq;
	}

	private Subquery<Integer> jobItemIdSubquery(CriteriaBuilder cb, CriteriaQuery<PossibleInstrumentDTO> cq,
			Root<Instrument> instrument) {
		val sq = cq.subquery(Integer.class);
		val inst = sq.from(Instrument.class);
		val ji = inst.join(Instrument_.jobItems, JoinType.LEFT);
		val ist = ji.join(JobItem_.state, JoinType.LEFT);
		sq.where(cb.equal(inst, instrument), cb.isTrue(ist.get(ItemState_.active)));
		sq.select(cb.max(ji.get(JobItem_.jobItemId)));
		return sq;
	}

	private Subquery<Integer> jobItemCalTypeSubquery(CriteriaBuilder cb, CriteriaQuery<PossibleInstrumentDTO> cq,
			Root<Instrument> instrument) {
		val sq = cq.subquery(Integer.class);
		val ji = sq.from(JobItem.class);
		val ct = ji.join(JobItem_.calType);
		sq.select(ct.get(CalibrationType_.serviceType).get(ServiceType_.serviceTypeId));
		sq.distinct(true);

		val ssq = sq.subquery(Integer.class);
		val sqi = ssq.from(JobItem.class);
		ssq.select(cb.max(sqi.get(JobItem_.jobItemId)));
		ssq.where(cb.equal(sqi.get(JobItem_.inst), instrument));
		sq.where(cb.equal(ji, ssq.getSelection()));
		return sq;
	}

	private Subquery<Integer> quotationItemsCalTypeId(CriteriaBuilder cb, CriteriaQuery<PossibleInstrumentDTO> cq,
			Root<Instrument> instrument) {
		val sq = cq.subquery(Integer.class);
		val quotationItem = sq.from(Quotationitem.class);
		val serviceType = quotationItem.join(Quotationitem_.serviceType);
		serviceType.join(ServiceType_.calibrationType);
		sq.select(serviceType.get(ServiceType_.serviceTypeId));

		val ssq = sq.subquery(Integer.class);
		val sqi = ssq.from(Quotationitem.class);
		ssq.select(cb.max(sqi.get(Quotationitem_.id)));
		ssq.where(cb.equal(sqi.join(Quotationitem_.inst), instrument));

		sq.where(cb.equal(quotationItem, ssq.getSelection()));
		return sq;
	}

	@Override
	public Map<Integer, Instrument> fetchInstrumentsForJobItemCreation(List<Integer> plantids) {
		return getResultList(cb -> {
			CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);

			Root<Instrument> instrument = cq.from(Instrument.class);

			instrument.fetch(Instrument_.comp);
			instrument.fetch(Instrument_.model);

			Fetch<Instrument, Contact> contact = instrument.fetch(Instrument_.con);
			contact.fetch(Contact_.defAddress, javax.persistence.criteria.JoinType.LEFT);

			cq.where(instrument.get(Instrument_.plantid).in(plantids));
			return cq;
		}).stream().collect(Collectors.toMap(Instrument::getPlantid, i -> i));
	}

	@Override
	public List<Instrument> getByOwningCompany(Integer plantid, Integer coid, String plantno, String serialno) {
		return getResultList(cb -> {

            CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);
            Root<Instrument> instrument = cq.from(Instrument.class);

            Join<Instrument, Company> company = instrument.join(Instrument_.comp);

            val predicates = plantid != null ? Stream.of(cb.equal(instrument.get(Instrument_.plantid), plantid)) :
                    Stream.of(equalStringsIfNotBlankPredicate(cb, instrument.get(Instrument_.plantno), plantno)
                                    , equalStringsIfNotBlankPredicate(cb, instrument.get(Instrument_.serialno), serialno))
                            .flatMap(Function.identity());

            cq.where(Stream.concat(predicates, entityByIdPredicate(cb, company, coid)).toArray(Predicate[]::new));

            cq.select(instrument);
            return cq;
        });
	}

	private PagedResultSet<Instrument> newJobItemInstrumentSearch(PagedResultSet<Instrument> prs,
			InstrumentAndModelSearchForm<?> form, Locale locale, NewJobItemSearchOrderByEnum orderByEnum) {

		completePagedResultSet(prs, Instrument.class, cb -> cq -> {

			Root<Instrument> instrument = cq.from(Instrument.class);
			Join<Instrument, Company> company = instrument.join(Instrument_.comp);
			Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model);
			Join<InstrumentModel, Translation> instModelTranslation = instrumentModel
					.join(InstrumentModel_.nameTranslations);
			instModelTranslation.on(cb.equal(instModelTranslation.get(Translation_.locale), locale));
			Join<InstrumentModel, Mfr> mfr = instrumentModel.join(InstrumentModel_.mfr);
			Join<InstrumentModel, Description> description = instrumentModel.join(InstrumentModel_.description);
			Join<Description, InstrumentModelFamily> family = description.join(Description_.family);
			Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain);

			Predicate clauses = cb.conjunction();

			if (form.getCompId() != null) {
				clauses.getExpressions().add(cb.equal(company.get(Company_.coid), form.getCompId()));
			}
			if (form.getMfrNm() != null && !form.getMfrNm().trim().equals("")) {
				if (form.getMfrId() != null && form.getMfrId() != 0) {
					clauses.getExpressions().add(cb.equal(mfr.get(Mfr_.mfrid), form.getMfrId()));
				} else if (form.getMfrNm() != null && form.getMfrNm().trim().equals("")) {
					clauses.getExpressions().add(cb.equal(mfr.get(Mfr_.name), form.getMfrNm()));
				}
			}
			if (form.getModel() != null && !form.getModel().trim().equals("")) {
				Predicate modelPred = cb.disjunction();
				modelPred.getExpressions()
						.add(cb.like(instrument.get(Instrument_.modelname), '%' + form.getModel() + '%'));
				modelPred.getExpressions()
						.add(cb.like(instrumentModel.get(InstrumentModel_.model), '%' + form.getModel() + '%'));
				clauses.getExpressions().add(modelPred);
			}

			if (form.getDescNm() != null && !form.getDescNm().trim().equals("")) {
				if (form.getDescId() != null && form.getDescId() != 0) {
					clauses.getExpressions().add(cb.equal(description.get(Description_.id), form.getDescId()));
				} else if (form.getDescNm() != null && form.getDescNm().trim().equals("")) {
					clauses.getExpressions().add(cb.like(description.get(Description_.description), form.getDescNm()));
				}
			}

			if (form.getDomainNm() != null && !form.getDomainNm().trim().equals("")) {
				if (form.getDomainId() != null && form.getDomainId() != 0) {
					clauses.getExpressions()
							.add(cb.equal(domain.get(InstrumentModelDomain_.domainid), form.getDomainId()));
				} else if (form.getDomainNm() != null && form.getDomainNm().trim().equals("")) {

					Join<InstrumentModelDomain, Translation> domainTranslation = domain
							.join(InstrumentModelDomain_.translation);
					domainTranslation.on(cb.equal(domainTranslation.get(Translation_.locale), locale));

					clauses.getExpressions()
							.add(cb.like(domainTranslation.get(Translation_.translation), form.getDomainNm()));
				}
			}

			if (form.getFamilyNm() != null && !form.getFamilyNm().trim().equals("")) {
				if (form.getFamilyId() != null) {
					clauses.getExpressions()
							.add(cb.equal(family.get(InstrumentModelFamily_.familyid), form.getFamilyId()));
				} else if (form.getFamilyNm() != null && form.getFamilyNm().trim().equals("")) {

					Join<InstrumentModelFamily, Translation> familyTranslation = family
							.join(InstrumentModelFamily_.translation);
					familyTranslation.on(cb.equal(familyTranslation.get(Translation_.locale), locale));

					clauses.getExpressions()
							.add(cb.like(familyTranslation.get(Translation_.translation), form.getFamilyNm()));
				}
			}

			if (form.getSalesCat() != null && !form.getSalesCat().trim().equals("")) {

				Join<InstrumentModel, SalesCategory> sales = instrumentModel.join(InstrumentModel_.salesCategory);
				Join<SalesCategory, Translation> salesTranslation = sales.join(SalesCategory_.translations);
				salesTranslation.on(cb.equal(salesTranslation.get(Translation_.locale), locale));

				if (form.getSalesCatId() != null && form.getSalesCatId() != 0) {
					clauses.getExpressions().add(cb.equal(sales.get(SalesCategory_.id), form.getSalesCatId()));
				} else if (form.getSalesCat() != null && !form.getSalesCat().trim().equals("")) {
					clauses.getExpressions()
							.add(cb.like(salesTranslation.get(Translation_.translation), form.getSalesCat()));
				}
			}

			if ((form.getSerialNo() != null) && !form.getSerialNo().trim().equals("")) {
				clauses.getExpressions().add(cb.like(instrument.get(Instrument_.serialno), form.getSerialNo()));
			}

			if ((form.getPlantNo() != null) && !form.getPlantNo().trim().equals("")) {
				clauses.getExpressions().add(cb.like(instrument.get(Instrument_.plantno), form.getPlantNo()));
			}
			if ((form.getBarcode() != null) && !form.getBarcode().trim().equals("")) {
				try {
					clauses.getExpressions()
							.add(cb.equal(instrument.get(Instrument_.plantid), Integer.parseInt(form.getBarcode())));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			if (form.getFormerbarcode() != null && !form.getFormerbarcode().trim().equals("")) {
				clauses.getExpressions()
						.add(cb.like(instrument.get(Instrument_.formerBarCode), form.getFormerbarcode()));
			}
			if (form.getCustomerDescription() != null && !form.getCustomerDescription().trim().equals("")) {
				clauses.getExpressions().add(cb.like(instrument.get(Instrument_.customerDescription),
						"%" + form.getCustomerDescription() + "%"));
			}

			List<javax.persistence.criteria.Order> order = new ArrayList<>();

			if (orderByEnum != null) {
				if (orderByEnum.equals(NewJobItemSearchOrderByEnum.PLANT_NO)) {
					order.add(cb.asc(instrument.get(Instrument_.plantno)));
				} else if (orderByEnum.equals(NewJobItemSearchOrderByEnum.SERIAL_NO)) {
					order.add(cb.asc(instrument.get(Instrument_.serialno)));
				} else if (orderByEnum.equals(NewJobItemSearchOrderByEnum.BARCODE)) {
					order.add(cb.asc(instrument.get(Instrument_.plantid)));
				}
			}

			cq.where(clauses);
			return Triple.of(instrument, null, order);
		});

		return prs;
	}

	@Override
	public Instrument getInstrumentByPlantNoAndSerialNoForCompany(Integer coid, String plantno, String serialno) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);
		Root<Instrument> instrumentRoot = cq.from(Instrument.class);

		Predicate clauses = cb.conjunction();
		if (plantno != null) {
			Predicate plantnoPredicate = cb.equal(instrumentRoot.get(Instrument_.plantno), plantno);
			clauses.getExpressions().add(plantnoPredicate);
		}
		if (serialno != null) {
			Predicate serialNoPredicate = cb.equal(instrumentRoot.get(Instrument_.serialno), serialno);
			clauses.getExpressions().add(serialNoPredicate);
		}

		Predicate companyid = cb.equal(instrumentRoot.get(Instrument_.comp), coid);
		clauses.getExpressions().add(companyid);

		cq.select(instrumentRoot);
		cq.where(clauses);

		TypedQuery<Instrument> query = getEntityManager().createQuery(cq);
		return query.getSingleResult();
	}

	@Override
	public PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> searchInstrumentsForNewJobItem(
			NewJobItemSearchInstrumentForm form) {
		val prs = new PagedResultSet<InstrumentSearchedForNewJobItemSearchDto>(form.getResultsPerPage(),
				form.getInstrumentsPageNo());
		completePagedResultSet(prs, InstrumentSearchedForNewJobItemSearchDto.class, cb -> cq -> {
			val instrument = cq.from(Instrument.class);
			val instrumentModel = instrument.join(Instrument_.model);
			val description = instrumentModel.join(InstrumentModel_.description);
			val family = description.join(Description_.family);
			val domain = family.join(InstrumentModelFamily_.domain);
			val serviceType = instrument.join(Instrument_.defaultServiceType, JoinType.LEFT);
			val company = instrument.join(Instrument_.comp);

			val predicates = Stream
					.of(searchByCompanyOrGroupOrEveryWherePredicate(cb, instrument, form),
							mfrPredicate(cb, instrument, instrumentModel, form.getMfrId()),
							entityByIdPredicate(cb, description, form.getDescId()),
							entityByIdPredicate(cb, domain, form.getDomainId()),
							entityByIdPredicate(cb, family, form.getFamilyId()),
							entityByIdPredicate(cb, instrumentModel.get(InstrumentModel_.salesCategory),
									form.getSalesCatId()),
							entityByIdPredicate(cb, instrument, form.getBarcode()),
							ilikePredicateList(cb, instrument.get(Instrument_.simplifiedPlantNo),
									simplifyNo(form.getPlantNo()), MatchMode.EXACT).stream(),
							ilikePredicateList(cb, instrument.get(Instrument_.formerBarCode), form.getFormerBarcode(),
									MatchMode.EXACT).stream(),
							ilikePredicateList(cb, instrument.get(Instrument_.simplifiedSerialNo),
									simplifyNo(form.getSerialNo()), MatchMode.EXACT).stream(),
							ilikePredicateList(cb, instrument.get(Instrument_.customerDescription),
									form.getCustomerDescription(), MatchMode.ANYWHERE).stream(),
							termInOneOfTwoFieldPredicate(cb, instrument.get(Instrument_.modelname),
									instrumentModel.get(InstrumentModel_.model), form.getModel()))
					.flatMap(Function.identity()).toArray(Predicate[]::new);

			cq.where(cb.and(predicates));

			val orderBy = Match(form.getOrderBy()).of(
					Case($(NewJobItemSearchOrderByEnum.BARCODE), cb.asc(instrument.get(Instrument_.plantid))),
					Case($(NewJobItemSearchOrderByEnum.PLANT_NO), cb.asc(instrument.get(Instrument_.plantno))),
					Case($(), cb.asc(instrument.get(Instrument_.serialno)))

			);
			val select = getInstrumentSearchedForNewJobItemSearchDtoCompoundSelection(form, cb, cq, instrument,
					instrumentModel, serviceType, company);
			return Triple.of(instrument, select, Collections.singletonList(orderBy));
		});
		return prs;
	}

	private static Stream<Predicate> mfrPredicate(CriteriaBuilder cb, Root<Instrument> instrument,
			Join<?, InstrumentModel> model, Integer mfrId) {

		if (checkIfNullOrZero(mfrId))
			return Stream.empty();
		val instMfr = instrument.join(Instrument_.mfr, javax.persistence.criteria.JoinType.LEFT);
		val modelMfr = model.join(InstrumentModel_.mfr, javax.persistence.criteria.JoinType.LEFT);
		return Stream.of(cb.or(cb.equal(instMfr, mfrId), cb.equal(modelMfr, mfrId)));
	}

	private CompoundSelection<InstrumentSearchedForNewJobItemSearchDto> getInstrumentSearchedForNewJobItemSearchDtoCompoundSelection(
			NewJobItemBaseSearchForm form, CriteriaBuilder cb, CriteriaQuery<?> cq, Root<Instrument> instrument,
			Join<Instrument, InstrumentModel> instrumentModel, Join<Instrument, ServiceType> serviceType,
			Join<Instrument, Company> company) {
		return cb.construct(InstrumentSearchedForNewJobItemSearchDto.class, instrument.get(Instrument_.plantid),
				instrument.get(Instrument_.formerBarCode), instrumentModel.get(InstrumentModel_.modelid),
				DuplicationSearchForCriteriaBuilder.generateDescription(instrument, instrumentModel).apply(cb),
				instrument.get(Instrument_.customerDescription), instrument.get(Instrument_.status),
				instrument.get(Instrument_.scrapped), instrument.get(Instrument_.serialno),
				instrument.get(Instrument_.plantno),
				joinTranslation(cb, serviceType, ServiceType_.shortnameTranslation, LocaleContextHolder.getLocale()),
				serviceType.get(ServiceType_.serviceTypeId), isActiveOnJobSubquery(cb, cq, instrument, 0),
				isActiveOnJobSubquery(cb, cq, instrument, form.getJobId()), instrument.get(Instrument_.nextCalDueDate),
				instrument.join(Instrument_.lastCal, JoinType.LEFT).get(Calibration_.calDate),
				instrument.get(Instrument_.addedOn), company.get(Company_.coname), company.get(Company_.coid));
	}

	@Override
	public PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> findInstrumentsFromJobQuotes(
			NewJobItemSearchByLinkedQuotationForm form) {
		val prs = new PagedResultSet<InstrumentSearchedForNewJobItemSearchDto>(form.getResultsPerPage(),
				form.getInstrumentsPageNo());
		completePagedResultSet(prs, InstrumentSearchedForNewJobItemSearchDto.class, cb -> cq -> {
			val instrument = cq.from(Instrument.class);
			val quoteItems = instrument.join(Instrument_.quoteItems, JoinType.LEFT);
			val quotation = quoteItems.join(Quotationitem_.quotation, JoinType.LEFT);
			val instrumentModel = instrument.join(Instrument_.model);
			val serviceType = instrument.join(Instrument_.defaultServiceType, JoinType.LEFT);
			val linkedJobs = quotation.join(Quotation_.linkedJobs, JoinType.LEFT);
			val company = instrument.join(Instrument_.comp);

			if (form.getJobQuoteLinkId() != null && form.getJobQuoteLinkId() != 0) {
				cq.where(cb.equal(linkedJobs.get(JobQuoteLink_.id), form.getJobQuoteLinkId()));
			} else {
				cq.where(cb.equal(linkedJobs.get(JobQuoteLink_.job), form.getJobId()));
			}
			val select = getInstrumentSearchedForNewJobItemSearchDtoCompoundSelection(form, cb, cq, instrument,
					instrumentModel, serviceType, company);
			return Triple.of(instrument, select, Collections.emptyList());
		});
		return prs;
	}

	private Subquery<Boolean> isActiveOnJobSubquery(CriteriaBuilder cb, CriteriaQuery<?> cq,
			From<?, Instrument> instrument, Integer jobId) {
		val sq = cq.subquery(Boolean.class);
		val jobItem = sq.from(JobItem.class);
		val itemState = jobItem.join(JobItem_.state);
		sq.select(cb.max(itemState.get(ItemState_.active).as(Integer.class)).as(Boolean.class));

		if (jobId != null && jobId > 0)
			sq.where(cb.equal(jobItem.get(JobItem_.inst), instrument), cb.equal(jobItem.get(JobItem_.job), jobId));
		else
			sq.where(cb.equal(jobItem.get(JobItem_.inst), instrument));
		return sq;
	}

	@Override
	public Optional<InstrumentTooltipDto> loadInstrumentForTooltip(Integer plantId) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(InstrumentTooltipDto.class);
			val instrument = cq.from(Instrument.class);
			val instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			val instrumentModel = instrument.join(Instrument_.model);
			val defaultServiceType = instrument.join(Instrument_.defaultServiceType, JoinType.LEFT);
			val company = instrument.join(Instrument_.comp);
			val contact = instrument.join(Instrument_.con);
			val address = instrument.join(Instrument_.add, JoinType.LEFT);

			val thread = instrument.join(Instrument_.thread, JoinType.LEFT);
			val threadType = thread.join(Thread_.type, JoinType.LEFT);
			val threadWire = thread.join(Thread_.wire, JoinType.LEFT);

			cq.where(cb.equal(instrument, plantId));
			cq.select(cb.construct(InstrumentTooltipDto.class, instrument.get(Instrument_.plantid),
					DuplicationSearchForCriteriaBuilder.generateDescription(instrument, instrumentModel).apply(cb),
					instrument.get(Instrument_.customerDescription), instrumentModel.get(InstrumentModel_.modelMfrType),
					instrumentMfr.get(Mfr_.name), instrument.get(Instrument_.modelname),
					trimAndConcatWithSeparator(
							joinTranslation(cb, defaultServiceType, ServiceType_.shortnameTranslation,
									LocaleContextHolder.getLocale()),
							joinTranslation(cb, defaultServiceType, ServiceType_.longnameTranslation,
									LocaleContextHolder.getLocale()),
							"-").apply(cb),
					company.get(Company_.coname),
					trimAndConcatWithWhitespace(contact.get(Contact_.firstName), contact.get(Contact_.lastName))
							.apply(cb),
					trimAndConcatWithWhitespace(address.get(Address_.addr1), address.get(Address_.town)).apply(cb),
					instrument.get(Instrument_.plantno), instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.calFrequency), instrument.get(Instrument_.calFrequencyUnit),
					instrument.get(Instrument_.nextCalDueDate), thread.get(Thread_.size),
					threadType.get(ThreadType_.type), threadWire.get(Wire_.designation),
					thread.get(Thread_.cylindricalStandard).get(CylindricalStandard_.id),
					thread.join(Thread_.uom, JoinType.LEFT).get(ThreadUoM_.description)));
			return cq;
		});

	}

	@Override
	public PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> findPagedCompanyInstrumentsFromModelIds(
			NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds) {
		val prs = new PagedResultSet<InstrumentSearchedForNewJobItemSearchDto>(form.getResultsPerPage(),
				form.getInstrumentsPageNo());
		completePagedResultSet(prs, InstrumentSearchedForNewJobItemSearchDto.class, cb -> cq -> {
			val instrument = cq.from(Instrument.class);
			val instrumentModel = instrument.join(Instrument_.model);
			val serviceType = instrument.join(Instrument_.defaultServiceType, JoinType.LEFT);
			val company = instrument.join(Instrument_.comp);

			cq.where(instrumentModel.in(modelIds), cb.equal(instrument.get(Instrument_.comp), form.getCompanyId()));

			val select = getInstrumentSearchedForNewJobItemSearchDtoCompoundSelection(form, cb, cq, instrument,
					instrumentModel, serviceType, company);
			return Triple.of(instrument, select, Collections.emptyList());
		});
		return prs;
	}

	@Override
	public List<InstrumentCheckDuplicatesDto> lookupForInstrumentsInGroup(InstrumentInGroupLookUpForm form) {
		return getResultList(cb -> {
			val cq = cb.createQuery(InstrumentCheckDuplicatesDto.class);
			val instrument = cq.from(Instrument.class);
			val company = instrument.join(Instrument_.comp);
			val subdiv = instrument.join(Instrument_.con).join(Contact_.sub);
			val model = instrument.join(Instrument_.model);
			val calibration = instrument.join(Instrument_.lastCal, JoinType.LEFT);

			val sq = cq.subquery(ZonedDateTime.class);
			val sqRoot = sq.from(JobItem.class);
			sq.where(cb.equal(sqRoot.get(JobItem_.inst), instrument));
			sq.select(cb.greatest(sqRoot.get(JobItem_.dateIn)));

			val predicates = Stream.of(cb.equal(company.get(Company_.companyGroup), form.getGroupId()), cb.or(Stream.of(
					entityByIdPredicate(cb, instrument, form.getPlantId()),
					ilikePredicateStream(cb, instrument.get(Instrument_.plantno), form.getPlantNo(), MatchMode.EXACT),
					ilikePredicateStream(cb, instrument.get(Instrument_.serialno), form.getSerialNo(), MatchMode.EXACT))
					.flatMap(Function.identity()).toArray(Predicate[]::new)));
			cq.where(predicates.toArray(Predicate[]::new));

			cq.select(DuplicationSearchForCriteriaBuilder.getSelection(cb, instrument, subdiv, company, model,
					calibration, sq, DuplicationSearchForCriteriaBuilder.checkIfActive(cq, cb, instrument)));

			return cq;
		});
	}

}
