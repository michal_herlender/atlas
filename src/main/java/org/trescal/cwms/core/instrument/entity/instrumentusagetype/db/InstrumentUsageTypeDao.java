package org.trescal.cwms.core.instrument.entity.instrumentusagetype.db;

import java.util.Locale;

import org.trescal.cwms.core.instrument.entity.instrumenttype.db.InstrumentTypeDao;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;

public interface InstrumentUsageTypeDao extends InstrumentTypeDao<InstrumentUsageType> {

	InstrumentUsageType getByName(String name, Locale locale);}