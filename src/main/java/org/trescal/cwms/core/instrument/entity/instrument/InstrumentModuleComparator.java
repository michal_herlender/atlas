package org.trescal.cwms.core.instrument.entity.instrument;

import java.util.Comparator;
import java.util.List;

/**
 * Simple comparator used for comparing a {@link List} of modules, simply
 * returns the list in order of plantid.
 * 
 * @author Richard
 */
public class InstrumentModuleComparator implements Comparator<Instrument>
{
	@Override
	public int compare(Instrument o1, Instrument o2)
	{
		return ((Integer) o1.getPlantid()).compareTo(o2.getPlantid());
	}
}
