package org.trescal.cwms.core.instrument.entity.instrumentfield.dto;

import java.util.Date;

import org.trescal.cwms.core.tools.FieldType;

public class InstrumentValueDateTimeDTO extends InstrumentValueDTO {

	private Date value;
	
	public InstrumentValueDateTimeDTO(Integer plantId, String fieldName, Date value) {
		super(plantId, fieldName);
		this.setValue(value);
	}
	
	@Override
	public FieldType getFieldType() {
		return FieldType.DATETIME;
	}
	
	public Date getValue() {
		return value;
	}
	
	public void setValue(Date value) {
		this.value = value;
	}
}