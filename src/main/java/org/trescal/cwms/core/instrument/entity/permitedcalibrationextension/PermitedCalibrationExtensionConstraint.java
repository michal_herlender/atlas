package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = PermitedCalibrationExtenionValidator.class)
@Target({ TYPE })
@Retention(RUNTIME)
public @interface PermitedCalibrationExtensionConstraint {
    //If this validation fails something has gone very wrong so translating the message didn't seem necessary
    String message() default "Calibration extension has incorrect format possibly time type with no time units";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    PermitedCalibrationExtensionType[] value() default { PermitedCalibrationExtensionType.PERCENTAGE, PermitedCalibrationExtensionType.TIME };

}
