package org.trescal.cwms.core.instrument.entity.hireinstrument;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessoryComparator;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.HireItemIdComparator;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Change history:
 *
 * 2019-02-05 GB : Removed unused fields (available in InstrumentComplementaryField) :
 *  poNumber, purchaseDate, purchaseNo, supplier
 *  TODO in future add active / inactive status, Allocated<Company>
 *  (indicating that a specific business company manages instrument as a hire instrument, 
 *   independent of instrument ownership by client, other company)
 * 
 */
@Entity
@Table(name = "hireinstrument")
public class HireInstrument extends Auditable
{
	private Set<HireAccessory> hireInstAccessories;
	private Set<HireItem> hireItems;
	private int id;
	private Instrument inst;

	@OneToMany(mappedBy = "hireInst", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(HireAccessoryComparator.class)
	public Set<HireAccessory> getHireInstAccessories()
	{
		return this.hireInstAccessories;
	}

	@OneToMany(mappedBy = "hireInst", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(HireItemIdComparator.class)
	public Set<HireItem> getHireItems()
	{
		return this.hireItems;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid")
	public Instrument getInst()
	{
		return this.inst;
	}

	public void setHireInstAccessories(Set<HireAccessory> hireInstAccessories)
	{
		this.hireInstAccessories = hireInstAccessories;
	}

	public void setHireItems(Set<HireItem> hireItems)
	{
		this.hireItems = hireItems;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInst(Instrument inst)
	{
		this.inst = inst;
	}
}
