package org.trescal.cwms.core.instrument.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class SearchInstrumentsByUserDTO {

	Integer plantid;
	String instrumentModelName;
	String serialNo;
	String plantNo;
	String status;
	String jobStatus;
	LocalDate lastCalDate;
	LocalDate nextCalDue;
	String lastCertificateNumber;
	Integer lastCertId;
	String owner;
	String subdivision;
	String onBehalfOf;
	Boolean scrapped;
	Boolean certFileAvailable;

}
