package org.trescal.cwms.core.instrument.entity.checkout.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.checkout.dto.CheckOutDto;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

public interface CheckOutService extends BaseService<CheckOut, Integer> {
	
	public CheckOut checkIfInstrumentIsCheckOut(Instrument inst, Job job);
	
	public List<CheckOut> getCheckOutInstrumentsByTechnicien(Contact technicien);
	
	public List<CheckOut> getCheckInInstrumentsByTechnicienWithNoConfidenceCheck(Contact technicien);
	
	public ResultWrapper checkInMultipalInstruments(String BarCodes, Locale locale);
	
	public ResultWrapper performeCheckOut(String barcode,String jobno,String username,Locale locale);
	
	public List<CheckOut> searchCheckOutRecords(CheckOutDto dto);
	
	public List<CheckOut> getCheckOutRecordsByInstrument(Instrument inst);
	
	
	
}
