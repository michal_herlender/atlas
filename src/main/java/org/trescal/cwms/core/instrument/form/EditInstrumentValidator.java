package org.trescal.cwms.core.instrument.form;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.db.InstrumentUsageTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutPlantNo;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutSerialNo;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditInstrumentValidator extends AbstractBeanValidator {
    /**
     * Logger for this class and subclasses
     */
    protected final Log logger = LogFactory.getLog(this.getClass());


    @Autowired
    private AllowInstrumentCreationWithoutPlantNo allowInstrumentCreationWithoutPlantNo;

    @Autowired
    private AllowInstrumentCreationWithoutSerialNo allowInstrumentCreationWithoutSerialNo;

    @Autowired
    private InstrumentUsageTypeService instUsageTypeService;

    @Autowired
    private ContactService contactService;

	@Autowired
	private AddressService addressService;
	
	@Autowired
	private LocationService locationService;

	public boolean supports(Class<?> clazz) {
		return clazz.equals(EditInstrumentForm.class);
	}

	public void validate(Object obj, Errors errors) {
		EditInstrumentForm aif = (EditInstrumentForm) obj;
        super.validate(obj, errors);

        if (aif.getCalExpiresAfterNumberOfUses()) {
            if (aif.getPermittedNumberOfUses() == null || aif.getPermittedNumberOfUses() <= 0)
                errors.rejectValue("permittedNumberOfUses", "error.calibrationsinterval.empty", null,
                    "The instrument permitted usage must be greater than 0");
            if (aif.getUsesSinceLastCalibration() == null)
                errors.rejectValue("usesSinceLastCalibration", "error.calibrationssincelastcal.empty", null,
                    "The instrument usage count must have a value");
        }
        if (!allowInstrumentCreationWithoutPlantNo.getValueByScope(Scope.COMPANY, aif.getBusinessCompanyId(), aif.getBusinessCompanyId())) {
            if (aif.getPlantNo() == null || aif.getPlantNo().trim().isEmpty()) {
                errors.rejectValue("plantNo", "error.instrument.plantno", null, "A plant No must be specified for the instrument");
            }
        }

        if (!allowInstrumentCreationWithoutSerialNo.getValueByScope(Scope.COMPANY, aif.getBusinessCompanyId(), aif.getBusinessCompanyId())) {
            if (aif.getSerialNo() == null || aif.getSerialNo().trim().isEmpty())
                errors.rejectValue("serialNo", "error.instrument.serialno", null, "A serial no must be specified for the instrument");
        }

        if (aif.getStatus() == null)
            errors.rejectValue("status", "error.instrument.mandatory", null, "Mandatory field");
        if (aif.getUsageTypeId() == null)
            errors.rejectValue("usageTypeId", "error.instrument.mandatory", null, "Mandatory field");
        if (aif.getStatus() != null && aif.getUsageTypeId() != null) {
            InstrumentUsageType usageType = this.instUsageTypeService.findInstrumentUsageType(aif.getUsageTypeId());
            if (usageType != null && !usageType.getInstrumentStatus().equals(aif.getStatus()))
                errors.rejectValue("usageTypeId", "error.instrument.usagetypecorrelation", null, "Usage type doesn't correlate with instrument type");
        }

        //Validations for Data to Server.
        Contact contact = contactService.get(aif.getPersonid());

        if (contact.getSub().getComp().getCoid() != aif.getCoid()) {
            errors.rejectValue("personid", "error.instrument.contact", null, "Contact doesnot match the Company");
        }

        Address address = addressService.get(aif.getAddrid());
        if (address.getSub().getComp().getCoid() != aif.getCoid()) {
            errors.rejectValue("addrid", "error.instrument.address", null, "Address doesnot match the Company");
        }


        if (aif.getLocid() != null) {
            Location location = locationService.get(aif.getLocid());

            if (location.getAdd().getSub().getComp().getCoid() != aif.getCoid()) {
                errors.rejectValue("locid", "error.instrument.location", null, "Location doesnot match the Company");
		}
			}
		
	}
	
}