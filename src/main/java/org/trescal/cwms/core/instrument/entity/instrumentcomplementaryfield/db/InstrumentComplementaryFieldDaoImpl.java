package org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;

/**
 * Created by paul.weston on 22/02/2017.
 */
@Repository("InstrumentComplementaryFieldDao")
public class InstrumentComplementaryFieldDaoImpl extends BaseDaoImpl<InstrumentComplementaryField, Integer> implements InstrumentComplementaryFieldDao  {

    @Override
    protected Class<InstrumentComplementaryField> getEntity() { return InstrumentComplementaryField.class; }

    @Override
    public InstrumentComplementaryField getComplementaryFieldByPlantid(int plantid) {
        Criteria complementaryFieldCriteria = getSession().createCriteria(InstrumentComplementaryField.class);
        complementaryFieldCriteria.createCriteria("instrument").add(Restrictions.eq("plantid", plantid));
        return (InstrumentComplementaryField) complementaryFieldCriteria.uniqueResult();
    }
}
