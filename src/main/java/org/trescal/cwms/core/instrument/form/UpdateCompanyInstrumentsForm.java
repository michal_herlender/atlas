package org.trescal.cwms.core.instrument.form;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class UpdateCompanyInstrumentsForm
{
	private Integer addressid;
	private Integer companyid;					// (Potentially new) company ID selected by user
	private Integer instrumentCompanyid;		// Current company ID of instruments
	private String formid;
	private Integer personid;
	private List<Integer> plantids;
	private Integer subdivid;

	// May be null or 0 if not selected, depending on view 
	public Integer getAddressid()
	{
		return this.addressid;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}")
	public Integer getCompanyid()
	{
		return this.companyid;
	}

	@NotNull
	public String getFormid()
	{
		return this.formid;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}")
	public Integer getInstrumentCompanyid()
	{
		return instrumentCompanyid;
	}

	// May be null or 0 if not selected, depending on view
	public Integer getPersonid()
	{
		return this.personid;
	}

	public List<Integer> getPlantids()
	{
		return this.plantids;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}")
	public Integer getSubdivid()
	{
		return this.subdivid;
	}

	public void setAddressid(Integer addressid)
	{
		this.addressid = addressid;
	}

	public void setCompanyid(Integer companyid)
	{
		this.companyid = companyid;
	}

	public void setFormid(String formid)
	{
		this.formid = formid;
	}

	public void setInstrumentCompanyid(Integer instrumentCompanyid)
	{
		this.instrumentCompanyid = instrumentCompanyid;
	}

	public void setPersonid(Integer personid)
	{
		this.personid = personid;
	}

	public void setPlantids(List<Integer> plantids)
	{
		this.plantids = plantids;
	}

	public void setSubdivid(Integer subdivid)
	{
		this.subdivid = subdivid;
	}
}
