package org.trescal.cwms.core.instrument.entity.instrumentrange.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;

@Service
public class InstrumentRangeServiceImpl extends BaseServiceImpl<InstrumentRange, Integer> implements InstrumentRangeService
{
	@Autowired
	private InstrumentRangeDao instrumentRangeDao;

	@Override
	protected BaseDao<InstrumentRange, Integer> getBaseDao() {
		return instrumentRangeDao;
	}
}