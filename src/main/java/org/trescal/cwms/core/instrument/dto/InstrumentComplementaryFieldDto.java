package org.trescal.cwms.core.instrument.dto;

import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;

public class InstrumentComplementaryFieldDto {
    InstrumentComplementaryField complementaryField;
    Integer unitId;
    String certClass;

    public InstrumentComplementaryField getComplementaryField() {
        return complementaryField;
    }

    public void setComplementaryField(InstrumentComplementaryField complementaryField) {
        this.complementaryField = complementaryField;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public String getCertClass() { return certClass; }

    public void setCertClass(String certClass) { this.certClass = certClass; }
}
