package org.trescal.cwms.core.instrument.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.exchangeformat.dto.ExchangeFormatDTO;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.logistics.dto.AsnDTO;
import org.trescal.cwms.core.logistics.dto.PoDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class ExchangeFormatJsonController {

	@Autowired
	private ExchangeFormatService exchangeFormatService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private AsnService asnService;
	@Autowired
	private POService poService;

	@RequestMapping(value = "/getExchangeFormats.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<ExchangeFormatDTO> getPrebookingForAddJob(@RequestParam(required = false) Integer coid,
			@RequestParam(required = false) ExchangeFormatTypeEnum type,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {

		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		List<ExchangeFormat> list = new ArrayList<>();
		List<ExchangeFormatDTO> myList = new ArrayList<>();

		Company company = null;
		if (coid != null) {
			company = companyService.get(coid);
		}
		list = exchangeFormatService.getByTypeAndClientCompany(type, company, allocatedSubdiv);
		list.forEach(e -> myList.add(new ExchangeFormatDTO(e)));

		return myList;
	}

	@RequestMapping(value = "/getPrebookingExchangeFormatForFileSelection.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<ExchangeFormatDTO> getPrebookingExchangeFormatForFileSelection(
			@RequestParam(name = "clientcompanyid", required = true) Integer clientcompanyid) throws Exception {

		Company company = companyService.get(clientcompanyid);
		List<ExchangeFormatDTO> efds = new ArrayList<>();
		if (company != null) {
			List<ExchangeFormat> efs = exchangeFormatService
					.getByTypeAndClientCompany(ExchangeFormatTypeEnum.PREBOOKING, company);
			for (ExchangeFormat exchangeFormat : efs) {
				efds.add(new ExchangeFormatDTO(exchangeFormat));
			}
		}
		return efds;
	}

	@RequestMapping(value = "/getPrebookingForAddJob.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<AsnDTO> getPrebookingForAddJob(@RequestParam(name = "subdivid", required = false) Integer subdivid,
			@RequestParam(name = "plant_id", required = false) Integer plantid,
			@RequestParam(name = "plant_no", required = false) String plantno) throws Exception {

		List<AsnDTO> list = new ArrayList<>();

		if (subdivid != null)
			list = asnService.findAsn(subdivid, plantid, plantno);

		return list;
	}

	@RequestMapping(value = "/getPOsForSelectedPrebooking.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<PoDTO> getPOsForSelectedPrebooking(
			@RequestParam(name = "prebookingid", required = false) Integer prebookingid) throws Exception {

		Asn asn = asnService.get(prebookingid);
		List<PoDTO> list = new ArrayList<>();
		if (asn != null)
			list = poService.getPOsByPrebooking(asn.getPrebookingJobDetails().getId());

		return list;
	}

	@RequestMapping(value = "/showExchangeFormats.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public List<ExchangeFormatDTO> initializeExchangeFormatsList(
			@RequestParam(value = "coid", required = false) Integer coid,
			@RequestParam(value = "subdivid", required = false) Integer subdivid) {

		List<ExchangeFormatDTO> efdtos = new ArrayList<>();
		if (coid != null) {
			Company company = companyService.get(coid);

			if (company.getCompanyRole().equals(CompanyRole.CLIENT)) {
				efdtos.addAll(this.exchangeFormatService.getExchangeFormats(null, null, null, company));
			} else {
				efdtos.addAll(this.exchangeFormatService.getExchangeFormats(null, company, null, null));
			}
		} else {
			Subdiv subdiv = this.subdivService.get(subdivid);
			efdtos.addAll(this.exchangeFormatService.getExchangeFormats(null, null, subdiv, null));
		}

		return efdtos;
	}

}
