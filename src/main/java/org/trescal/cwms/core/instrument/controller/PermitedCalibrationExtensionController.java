package org.trescal.cwms.core.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db.PermitedCalibrationExtensionService;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("calextension")
public class PermitedCalibrationExtensionController {

    @Autowired
    PermitedCalibrationExtensionService extensionService;
    
    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = "addorupdate.json", method = RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,produces= MediaType.APPLICATION_JSON_VALUE)
    public CalExtensionDto addOrUpdateCalExtension(@RequestBody CalExtensionDto calExtensionDto){
        try {
            return extensionService.insertOrUpdateAjax(calExtensionDto);
        }
        catch(ConstraintViolationException e){
            calExtensionDto.setSuccess(false);
            calExtensionDto.setMessage(e.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            return calExtensionDto;
        }
    }
    
    @RequestMapping(value = "get.json", method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public CalExtensionDto getCalExtension(@RequestParam("plantid") Integer plantId) {
    	return extensionService.getAjax(plantId);
    }

    @RequestMapping(value = "delete.json", method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public CalExtensionDto deleteCalExtension(@RequestParam("plantid") Integer plantId) {
    	return extensionService.deleteAjax(plantId);
    }

}