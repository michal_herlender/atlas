package org.trescal.cwms.core.instrument.dto;

import lombok.Value;

import java.util.Date;

@Value(staticConstructor = "of")
public class InstrumentValidationActionDto {

    Integer id;
    String action;
    String underTakenBy;
    Date underTakenOn;
}
