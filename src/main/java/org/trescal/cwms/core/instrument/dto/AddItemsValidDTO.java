package org.trescal.cwms.core.instrument.dto;

import java.util.List;

import org.trescal.cwms.spring.model.KeyValue;

import lombok.Data;

@Data
public class AddItemsValidDTO {

	private QuickAddItemsValidDTO instrument;
	private List<KeyValue<Integer, String>> serviceTypes;
	private List<KeyValue<Integer, String>> bookingInAddresses;
	private Integer jobDefaultCalTypeId;
	private Integer defAddrId;
	private Integer coid;
	private Integer jobId;
	private Integer defaultTurnAround;
}
