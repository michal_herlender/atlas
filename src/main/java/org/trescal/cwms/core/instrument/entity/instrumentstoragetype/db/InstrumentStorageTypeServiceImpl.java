package org.trescal.cwms.core.instrument.entity.instrumentstoragetype.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;

@Service("InstrumentStorageTypeService")
public class InstrumentStorageTypeServiceImpl implements InstrumentStorageTypeService
{
	@Autowired
	InstrumentStorageTypeDao InstrumentStorageTypeDao;

	public void deleteInstrumentStorageType(InstrumentStorageType instrumentstoragetype)
	{
		this.InstrumentStorageTypeDao.remove(instrumentstoragetype);
	}

	public InstrumentStorageType findDefaultType(Class<?> clazz)
	{
		return this.InstrumentStorageTypeDao.findDefaultType();
	}

	public InstrumentStorageType findInstrumentStorageType(int id)
	{
		return this.InstrumentStorageTypeDao.find(id);
	}

	public List<InstrumentStorageType> getAllInstrumentStorageTypes()
	{
		return this.InstrumentStorageTypeDao.findAll();
	}

	public void insertInstrumentStorageType(InstrumentStorageType InstrumentStorageType)
	{
		this.InstrumentStorageTypeDao.persist(InstrumentStorageType);
	}

	public void saveOrUpdateInstrumentStorageType(InstrumentStorageType instrumentstoragetype)
	{
		this.InstrumentStorageTypeDao.saveOrUpdate(instrumentstoragetype);
	}

	public void setInstrumentStorageTypeDao(InstrumentStorageTypeDao InstrumentStorageTypeDao)
	{
		this.InstrumentStorageTypeDao = InstrumentStorageTypeDao;
	}

	public void updateInstrumentStorageType(InstrumentStorageType InstrumentStorageType)
	{
		this.InstrumentStorageTypeDao.update(InstrumentStorageType);
	}
}