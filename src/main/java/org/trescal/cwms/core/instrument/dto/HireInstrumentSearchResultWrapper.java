package org.trescal.cwms.core.instrument.dto;

public class HireInstrumentSearchResultWrapper
{
	private String description;
	private String hirecost;
	private String mfr;
	private String model;
	private int modelid;
	private Double mrend;
	private Double mrstart;
	private Boolean offhired;
	private int plantid;
	private String plantno;
	private Boolean scrapped;
	private String serialno;
	private String uom;
	private String fullInstrumentModelName;

	public HireInstrumentSearchResultWrapper()
	{

	}

	public String getDescription()
	{
		return this.description;
	}

	public String getHirecost()
	{
		return this.hirecost;
	}

	public String getMfr()
	{
		return this.mfr;
	}

	public String getModel()
	{
		return this.model;
	}

	public int getModelid()
	{
		return this.modelid;
	}

	public Double getMrend()
	{
		return this.mrend;
	}

	public Double getMrstart()
	{
		return this.mrstart;
	}

	public Boolean getOffhired()
	{
		return this.offhired;
	}

	public int getPlantid()
	{
		return this.plantid;
	}

	public String getPlantno()
	{
		return this.plantno;
	}

	public Boolean getScrapped()
	{
		return this.scrapped;
	}

	public String getSerialno()
	{
		return this.serialno;
	}

	public String getUom()
	{
		return this.uom;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setHirecost(String hirecost)
	{
		this.hirecost = hirecost;
	}

	public void setMfr(String mfr)
	{
		this.mfr = mfr;
	}

	public void setModel(String model)
	{
		this.model = model;
	}

	public void setModelid(int modelid)
	{
		this.modelid = modelid;
	}

	public void setMrend(Double mrend)
	{
		this.mrend = mrend;
	}

	public void setMrstart(Double mrstart)
	{
		this.mrstart = mrstart;
	}

	public void setOffhired(Boolean offhired)
	{
		this.offhired = offhired;
	}

	public void setPlantid(int plantid)
	{
		this.plantid = plantid;
	}

	public void setPlantno(String plantno)
	{
		this.plantno = plantno;
	}

	public void setScrapped(Boolean scrapped)
	{
		this.scrapped = scrapped;
	}

	public void setSerialno(String serialno)
	{
		this.serialno = serialno;
	}

	public void setUom(String uom)
	{
		this.uom = uom;
	}

	public String getFullInstrumentModelName() {
		return fullInstrumentModelName;
	}

	public void setFullInstrumentModelName(String fullInstrumentModelName) {
		this.fullInstrumentModelName = fullInstrumentModelName;
	}

}
