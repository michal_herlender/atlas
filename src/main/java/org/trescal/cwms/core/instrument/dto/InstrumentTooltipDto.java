package org.trescal.cwms.core.instrument.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InstrumentTooltipDto {

    private Integer plantId;
    private String modelName;
    private String customerDescription;
    private ModelMfrType modelMfrType;
    private String mfrName;
    private String model;
    private String defaultServiceType;
    private String companyName;
    private String contactName;
    private String address;
    private String plantNo;
    private String serialNo;
    private Integer calFrequency;
    private IntervalUnit calFrequencyUnit;
    private LocalDate nextCalibrationDate;

    private String threadSize;
    private String threadType;
    private String threadWire;
    private Double threadStandardId;
    private String threadUom;



}
