package org.trescal.cwms.core.instrument.entity.instrumentfield.dto;

public class InstrumentFieldDTO implements Comparable<InstrumentFieldDTO> {
	
	Integer fieldDefinitionId;
	String fieldName;
	String fieldType;
	Integer valueId;
	String value;
	Boolean isEditable;
	
	
	public Boolean getIsEditable() {
		return isEditable;
	}
	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}
	public Integer getFieldDefinitionId() {
		return fieldDefinitionId;
	}
	public void setFieldDefinitionId(Integer fieldDefinitionId) {
		this.fieldDefinitionId = fieldDefinitionId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public Integer getValueId() {
		return valueId;
	}
	public void setValueId(Integer valueId) {
		this.valueId = valueId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int compareTo(InstrumentFieldDTO other) {
		return this.fieldName.compareToIgnoreCase(other.fieldName);
	}
	
	
}
