package org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.db;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationAction;

public interface InstrumentValidationActionService
{
	void deleteInstrumentValidationAction(InstrumentValidationAction instrumentvalidationaction);

	InstrumentValidationAction findInstrumentValidationAction(int id);

	List<InstrumentValidationAction> getAllInstrumentValidationActions();

	ResultWrapper insert(Integer issueId, String action, Integer statusId, HttpSession session);

	void insertInstrumentValidationAction(InstrumentValidationAction instrumentvalidationaction);

	void saveOrUpdateInstrumentValidationAction(InstrumentValidationAction instrumentvalidationaction);

	void updateInstrumentValidationAction(InstrumentValidationAction instrumentvalidationaction);
}