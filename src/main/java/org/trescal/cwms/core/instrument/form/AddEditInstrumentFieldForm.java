package org.trescal.cwms.core.instrument.form;

import java.util.Date;

import org.trescal.cwms.core.tools.FieldType;

public class AddEditInstrumentFieldForm {
	
	Integer plantId;
	Integer valueId;
	Integer fieldDefinitionId;
	String fieldName;
	String stringValue;
	Boolean booleanValue;
	Double numericValue;
	Date dateValue;
	Integer selectValueId;
	FieldType type;
	
	public Integer getPlantId() {
		return plantId;
	}
	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}
	public Integer getValueId() {
		return valueId;
	}
	public void setValueId(Integer valueId) {
		this.valueId = valueId;
	}
	public Integer getFieldDefinitionId() {
		return fieldDefinitionId;
	}
	public void setFieldDefinitionId(Integer fieldDefinitionId) {
		this.fieldDefinitionId = fieldDefinitionId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public Boolean getBooleanValue() {
		return booleanValue;
	}
	public void setBooleanValue(Boolean booleanValue) {
		this.booleanValue = booleanValue;
	}
	public Double getNumericValue() {
		return numericValue;
	}
	public void setNumericValue(Double numericValue) {
		this.numericValue = numericValue;
	}
	public Date getDateValue() {
		return dateValue;
	}
	public void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}
	public Integer getSelectValueId() {
		return selectValueId;
	}
	public void setSelectValueId(Integer selectValueId) {
		this.selectValueId = selectValueId;
	}
	public FieldType getType() {
		return type;
	}
	public void setType(FieldType type) {
		this.type = type;
	}
}
