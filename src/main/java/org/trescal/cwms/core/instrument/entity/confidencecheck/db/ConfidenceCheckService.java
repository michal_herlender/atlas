package org.trescal.cwms.core.instrument.entity.confidencecheck.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.confidencecheck.ConfidenceCheck;
import org.trescal.cwms.core.instrument.entity.confidencecheck.dto.ConfidenceCheckDto;

public interface ConfidenceCheckService extends BaseService<ConfidenceCheck, Integer> {

	public ConfidenceCheck performeConfidenceCheck(ConfidenceCheckDto dto, CheckOut checkout, String username);

}
