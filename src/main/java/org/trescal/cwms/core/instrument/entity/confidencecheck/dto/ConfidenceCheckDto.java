package org.trescal.cwms.core.instrument.entity.confidencecheck.dto;

import org.trescal.cwms.core.instrument.entity.confidencecheck.confidencecheckresultenum.ConfidenceCheckResultEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfidenceCheckDto {

	private Integer checkoutId;
	private String description;
	private ConfidenceCheckResultEnum result;
}
