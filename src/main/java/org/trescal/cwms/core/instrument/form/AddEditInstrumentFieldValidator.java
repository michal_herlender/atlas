package org.trescal.cwms.core.instrument.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AddEditInstrumentFieldValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(AddEditInstrumentFieldForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		AddEditInstrumentFieldForm form = (AddEditInstrumentFieldForm) obj;
		
		switch (form.getType()) {
		case BOOLEAN:
			if(form.getBooleanValue() == null){
				errors.rejectValue(null, "error.flexiblefields.booleanvalue", null, "Please select yes or no");
			}
			break;
		case DATETIME:
			if(form.getDateValue() == null){
				errors.rejectValue(null, "error.flexiblefields.datevalue", null, "Please enter a date");
			}
			break;
		case NUMERIC:
			if(form.getNumericValue() == null){
				errors.rejectValue(null, "error.flexiblefields.numericvalue", null, "Please enter a number");
			}
			break;
		case SELECTION:
			if(form.getSelectValueId() == null){
				errors.rejectValue(null, "error.flexiblefields.selectvalue", null, "Please make a selection");
			}
			break;
		case STRING:
			if(form.getStringValue() == null){
				errors.rejectValue(null, "error.flexiblefields.stringvalue", null, "Please enter some text");
			}
		}
		
		
		
	}
}
