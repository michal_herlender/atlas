package org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;

@Repository("InstrumentValidationStatusDao")
public class InstrumentValidationStatusDaoImpl extends BaseDaoImpl<InstrumentValidationStatus, Integer> implements InstrumentValidationStatusDao {
	
	@Override
	protected Class<InstrumentValidationStatus> getEntity() {
		return InstrumentValidationStatus.class;
	}
}