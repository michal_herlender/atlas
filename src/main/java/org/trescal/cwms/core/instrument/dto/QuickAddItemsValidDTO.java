package org.trescal.cwms.core.instrument.dto;

import lombok.Data;

@Data
public class QuickAddItemsValidDTO {
	
	private Integer plantid;
	private String serialNo;
	private String plantNo;
	private String instModel;
	private Integer instModelId;
	private Integer procId;
	private String procReference;
	private Integer workIstructionId;
	private Integer defaultWorkInstructionId;
	private String defaultWorkInstructionTitle;
	private String workInstructionTitle;
	private Boolean isInstrumentCalTypeCompatibleWithJobType;
	private Integer serviceTypeSelected;
	
}
