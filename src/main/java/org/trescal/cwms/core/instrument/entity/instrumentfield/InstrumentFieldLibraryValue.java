package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@Table(name = "instrumentfieldlibrary")
public class InstrumentFieldLibraryValue {
	private int instrumentFieldLibraryid;
	private InstrumentFieldDefinition instrumentFieldDefinition;
	private String name;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getInstrumentFieldLibraryid() {
		return instrumentFieldLibraryid;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "instrumentFieldDefinitionid", nullable = false)
	public InstrumentFieldDefinition getInstrumentFieldDefinition() {
		return instrumentFieldDefinition;
	}
	
	@Column(name = "name", nullable = false, columnDefinition = "nvarchar(2000)")
	public String getName() {
		return name;
	}
	
	public void setInstrumentFieldLibraryid(int instrumentFieldLibraryid) {
		this.instrumentFieldLibraryid = instrumentFieldLibraryid;
	}
	
	public void setInstrumentFieldDefinition(InstrumentFieldDefinition instrumentFieldDefinition) {
		this.instrumentFieldDefinition = instrumentFieldDefinition;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}