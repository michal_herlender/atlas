package org.trescal.cwms.core.instrument.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.checkout.db.CheckOutService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class PerformCheckOutCheckInController {

	@Autowired
	private UserService userService;
	@Autowired
	private CheckOutService checkOutService;

	public static final String VIEW_NAME = "trescal/core/instrument/checkinout/checkInOut";

	@RequestMapping(method = RequestMethod.GET, value = "checkinout.htm")
	public String manageCheckOutCheckIn(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {

		model.addAttribute("checkoutInsts",
				this.checkOutService.getCheckOutInstrumentsByTechnicien(this.userService.get(username).getCon()));

		model.addAttribute("checkedInInstWithNoConfidenceCheck", this.checkOutService
				.getCheckInInstrumentsByTechnicienWithNoConfidenceCheck(this.userService.get(username).getCon()));

		return VIEW_NAME;
	}

	@RequestMapping(value = "/performCheckOut.json", method = RequestMethod.GET)
	public @ResponseBody ResultWrapper performCheckOut(@RequestParam(value = "barcode", required = true) String barcode,
			@RequestParam(value = "jobno", required = false) String jobno,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) {

		return this.checkOutService.performeCheckOut(barcode, jobno, username, locale);
	}

	@RequestMapping(value = "/performCheckIn.json", method = RequestMethod.POST)
	public @ResponseBody ResultWrapper performCheckIn(@RequestParam(value = "barcode", required = true) String barcode,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) {

		return this.checkOutService.checkInMultipalInstruments(barcode,locale);
	}

}
