package org.trescal.cwms.core.instrument.service;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.form.UpdateCompanyInstrumentsForm;

public interface UpdateCompanyInstrumentsService {
	void performUpdate(Contact currentContact, UpdateCompanyInstrumentsForm form);
}
