package org.trescal.cwms.core.instrument.entity.instrument.db;

import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.Clock;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.function.IntBinaryOperator;

import static java.lang.Math.max;
import static java.lang.Math.min;

@Service
public class InstrumentPercentageService {
    private final Clock clock;

    /**
     * ci = calibration interval in days
     * rc - days remaining in current calibration interval
     * Returns days elapsed in entire period of calibration
     */
    private static final IntBinaryOperator TOTAL_CAL_PERCENTAGE_FUNCTION =
        (int ci, int rc) -> max(((ci - rc) * 100) / ci, 0);

    /**
     * Returns days used within final month of calibration
     * (note, may look strange for intervals < 30 days)
     */
    private static final IntBinaryOperator FINAL_MONTH_PERCENTAGE_FUNCTION =
        (int ci, int rc) -> max(((30 - rc) * 100) / 30, 0);

    /**
     * Operation is only valid for calibration interval of 31+ days, otherwise there are no
     * months preceding the final month.
     */
    private static final IntBinaryOperator REMAINING_MONTHS_PERCENTAGE_FUNCTION =
        (int ci, int rc) -> ci <= 30 ? 0 : min(((ci - rc) * 100) / (ci - 30), 100);


    public InstrumentPercentageService(Clock clock) {
        this.clock = clock;
    }

    public Integer totalCalibrationPercentage(LocalDate nextCalibrationDueDate, IntervalUnit intervalUnit, Integer interval) {
        return calculatePercentage(nextCalibrationDueDate, intervalUnit, interval, TOTAL_CAL_PERCENTAGE_FUNCTION);
    }

    public Integer finalMonthPercentage(LocalDate nextCalibrationDueDate, IntervalUnit intervalUnit, Integer interval) {
        return calculatePercentage(nextCalibrationDueDate, intervalUnit, interval, FINAL_MONTH_PERCENTAGE_FUNCTION);
    }


    public Integer remainingPercentage(LocalDate nextCalibrationDueDate, IntervalUnit intervalUnit, Integer interval) {
        return calculatePercentage(nextCalibrationDueDate, intervalUnit, interval, REMAINING_MONTHS_PERCENTAGE_FUNCTION);
    }

    private Integer calculatePercentage(LocalDate nextCalibrationDueDate, IntervalUnit intervalUnit, Integer interval, IntBinaryOperator operator) {
        val today = LocalDate.now(clock).atStartOfDay(LocaleContextHolder.getTimeZone().toZoneId()).toLocalDate();
        if ((nextCalibrationDueDate == null) || nextCalibrationDueDate.isBefore(today)) return 100;
        val calIntervalInDays = intervalUnit.toDays(interval);
        val remainingCalDays = ChronoUnit.DAYS.between(today, nextCalibrationDueDate);
        return operator.applyAsInt(calIntervalInDays, (int) remainingCalDays);
    }
}
