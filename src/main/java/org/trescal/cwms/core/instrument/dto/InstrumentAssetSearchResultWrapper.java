package org.trescal.cwms.core.instrument.dto;

public class InstrumentAssetSearchResultWrapper
{
	private Boolean alreadyAsset;
	private Integer assetid;
	private String assetno;
	private Boolean calibrationStandard;
	private String description;
	private String mfr;
	private String model;
	private int modelid;
	private int plantid;
	private String plantno;
	private Boolean scrapped;
	private String serialno;

	public InstrumentAssetSearchResultWrapper(int modelid, String mfr, String model, String description, int plantid, String plantno, String serialno, Boolean scrapped, Boolean calibrationStandard, Boolean alreadyAsset, Integer assetid, String assetno)
	{
		this.modelid = modelid;
		this.mfr = mfr;
		this.model = model;
		this.description = description;
		this.plantid = plantid;
		this.plantno = plantno;
		this.serialno = serialno;
		this.scrapped = scrapped;
		this.calibrationStandard = calibrationStandard;
		this.alreadyAsset = alreadyAsset;
		this.assetid = assetid;
		this.assetno = assetno;
	}

	public Boolean getAlreadyAsset()
	{
		return this.alreadyAsset;
	}

	public Integer getAssetid()
	{
		return this.assetid;
	}

	public String getAssetno()
	{
		return this.assetno;
	}

	public Boolean getCalibrationStandard()
	{
		return this.calibrationStandard;
	}

	public String getDescription()
	{
		return this.description;
	}

	public String getMfr()
	{
		return this.mfr;
	}

	public String getModel()
	{
		return this.model;
	}

	public int getModelid()
	{
		return this.modelid;
	}

	public int getPlantid()
	{
		return this.plantid;
	}

	public String getPlantno()
	{
		return this.plantno;
	}

	public Boolean getScrapped()
	{
		return this.scrapped;
	}

	public String getSerialno()
	{
		return this.serialno;
	}

	public void setAlreadyAsset(Boolean alreadyAsset)
	{
		this.alreadyAsset = alreadyAsset;
	}

	public void setAssetid(Integer assetid)
	{
		this.assetid = assetid;
	}

	public void setAssetno(String assetno)
	{
		this.assetno = assetno;
	}

	public void setCalibrationStandard(Boolean calibrationStandard)
	{
		this.calibrationStandard = calibrationStandard;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setMfr(String mfr)
	{
		this.mfr = mfr;
	}

	public void setModel(String model)
	{
		this.model = model;
	}

	public void setModelid(int modelid)
	{
		this.modelid = modelid;
	}

	public void setPlantid(int plantid)
	{
		this.plantid = plantid;
	}

	public void setPlantno(String plantno)
	{
		this.plantno = plantno;
	}

	public void setScrapped(Boolean scrapped)
	{
		this.scrapped = scrapped;
	}

	public void setSerialno(String serialno)
	{
		this.serialno = serialno;
	}

}
