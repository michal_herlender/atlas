package org.trescal.cwms.core.instrument.entity.hireinstrument.db;

import lombok.val;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.dto.HireInstrumentDto;
import org.trescal.cwms.core.hire.dto.HireInstrumentRevenueWrapper;
import org.trescal.cwms.core.hire.entity.hire.Hire_;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem_;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.tools.StringJpaUtils;

import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilikePredicateList;
import static org.trescal.cwms.core.tools.StringJpaUtils.ilikePredicateStream;

@Repository("HireInstrumentDao")
public class HireInstrumentDaoImpl extends BaseDaoImpl<HireInstrument, Integer> implements HireInstrumentDao {

	@Override
	protected Class<HireInstrument> getEntity() {
		return HireInstrument.class;
	}
	
	public HireInstrument findHireInstrumentDWR(int id) {
		Criteria criteria = this.getSession().createCriteria(HireInstrument.class);
		criteria.setFetchMode("hireInstAccessories", FetchMode.JOIN);
		criteria.setFetchMode("hireInstAccessories.status", FetchMode.JOIN);
		criteria.add(Restrictions.idEq(id));
		return (HireInstrument) criteria.uniqueResult();
	}
	
	public HireInstrument findHireInstrumentUsingBarcodeDWR(int plantid) {
		Criteria criteria = this.getSession().createCriteria(HireInstrument.class);
		criteria.setFetchMode("hireInstAccessories", FetchMode.JOIN);
		criteria.setFetchMode("hireInstAccessories.status", FetchMode.JOIN);
		Criteria instCrit = criteria.createCriteria("inst");
		instCrit.add(Restrictions.idEq(plantid));
		return (HireInstrument) criteria.uniqueResult();
	}
	
	@Override
	public int getHireInstrumentCount() {
		Criteria criteria = this.getSession().createCriteria(HireInstrument.class);
		criteria.setProjection(Projections.rowCount());
		return (Integer) criteria.list().get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HireInstrument> getHireInstruments(List<Integer> plantIds) {
		Criteria crit = this.getSession().createCriteria(HireInstrument.class);
		crit.createCriteria("inst").add(Restrictions.in("plantid", plantIds));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<HireInstrumentRevenueWrapper> searchHireInstrumentsRevenueHQL(Integer mfrid, String mfrname, String modelname, Integer descid, String descname, String barcode, String plantno, String serialno) {
		String hql = "select new org.trescal.cwms.core.hire.dto.HireInstrumentRevenueWrapper(sum(hitem.offHireDays), count(*), hi, sum(hitem.finalCost)) "
				+ "from HireItem hitem "
				+ "left join hitem.hire as h "
				+ "left join hitem.hireInst as hi "
				+ "left join hi.inst as inst "
				+ "left join inst.model as model "
				+ "left join model.mfr as mfrmod "
				+ "left join inst.mfr as mfrins "
				+ "left join model.description as d "
				+ "where inst.plantno is not null "
				+ "and (h.status.statusid = 55 or h.status.statusid = 56) ";
		if ((mfrid != null) && (mfrid != 0))
		{
			hql += "and ((mfrmod.mfrid = :mfrid) or (mfrins.mfrid = :mfrid)) ";
		}
		else if ((mfrname != null) && (mfrname != ""))
		{
			hql += "and ((mfrmod.name like :mfr) or (mfrins.name like :mfr)) ";
		}
		hql += "and model.model like :model ";
		if ((descid != null) && (descid != 0))
		{
			hql += "and d.id = :descid ";
		}
		else if ((descname != null) && (descname != ""))
		{
			hql += "and d.description like :desc ";
		}
		hql += "and cast(inst.plantid as string) like :plantid "
				+ "and inst.plantno like :plantno "
				+ "and inst.serialno like :serialno "
				+ "group by hi.id, inst.plantno order by inst.plantno asc";
		Query query = this.getSession().createQuery(hql);
		if ((mfrid != null) && (mfrid != 0))
		{
			query.setInteger("mfrid", mfrid);
		}
		else if ((mfrname != null) && (mfrname != ""))
		{
			query.setString("mfr", mfrname + "%");
		}
		query.setString("model", modelname + "%");
		if ((descid != null) && (descid != 0))
		{
			query.setInteger("descid", descid);
		}
		else if ((descname != null) && (descname != ""))
		{
			query.setString("desc", descname + "%");
		}
		query.setString("plantid", barcode + "%");
		query.setString("plantno", plantno + "%");
		query.setString("serialno", serialno + "%");
		return (List<HireInstrumentRevenueWrapper>) query.list();
	}

	@Override
	public List<HireInstrumentDto> searchHireInstruments(Integer companyId, String plantId, String plantNo, String serialNo, String model, String manufacturer, String subFamily) {
		return getResultList(cb -> {
			val cq = cb.createQuery(HireInstrumentDto.class);
			val hireInstrument = cq.from(HireInstrument.class);
			val instrument = hireInstrument.join(HireInstrument_.inst);
			val instMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			val instModel = instrument.join(Instrument_.model);
			val modelDescription = instModel.join(InstrumentModel_.description);
			val modelMfr = instModel.join(InstrumentModel_.mfr,JoinType.LEFT);
			val hireModel = instModel.join(InstrumentModel_.hireModels);
			hireModel.on(cb.equal(hireModel.get(HireModel_.organisation),companyId));
			cq.select(cb.construct(HireInstrumentDto.class,
					instrument.get(Instrument_.plantid),
					instrument.get(Instrument_.plantno),
					instrument.get(Instrument_.serialno),
					getFullInstrumentModelName(cb,instModel,modelDescription,instMfr,instrument.get(Instrument_.modelname)),
					hireModel.get(HireModel_.hireCost).as(String.class),
					instrument.get(Instrument_.scrapped),
					offHiredQuery(cb,cq,instrument)
					));
			val mfrPredicatesList =
					Stream.of(
							ilikePredicateList(cb,instMfr.get(Mfr_.name),manufacturer,MatchMode.START),
							ilikePredicateList(cb,modelMfr.get(Mfr_.name),manufacturer,MatchMode.START)
					).flatMap(Collection::stream).toArray(Predicate[]::new);
			val mfrPredicate = mfrPredicatesList.length==0?Collections.<Predicate>emptyList():Collections.singletonList(cb.or(mfrPredicatesList));
			val modelPredicatesList = Stream.of(ilikePredicateStream(cb,instrument.get(Instrument_.modelname),model,MatchMode.START),
					ilikePredicateStream(cb,instModel.get(InstrumentModel_.model),model,MatchMode.START)
					).flatMap(Function.identity()).toArray(Predicate[]::new);
			val modelPredicates = modelPredicatesList.length==0?Collections.<Predicate>emptyList():
					Collections.singletonList(cb.or(modelPredicatesList));

			val predicates = Stream.of(
					ilikePredicateList(cb,instrument.get(Instrument_.plantid).as(String.class),plantId, MatchMode.START),
					ilikePredicateList(cb,instrument.get(Instrument_.plantno),plantNo,MatchMode.START),
					ilikePredicateList(cb,instrument.get(Instrument_.serialno),serialNo,MatchMode.START),
					modelPredicates	,
					ilikePredicateList(cb,modelDescription.get(Description_.description),subFamily,MatchMode.START),
					ilikePredicateList(cb,instrument.get(Instrument_.plantno),plantNo,MatchMode.START),
					mfrPredicate
			).flatMap(Collection::stream).toArray(Predicate[]::new);
			cq.where(cb.and(predicates));
			return cq;
		});
	}

    @Override
	public Optional<HireInstrument> getHireInstrumentByPlantId(Integer plantId) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(HireInstrument.class);
			val hireInstrument = cq.from(HireInstrument.class);
			val instrument = hireInstrument.join(HireInstrument_.inst);
			cq.where(cb.equal(instrument, plantId));
			return cq;
		});
	}

	private Expression<Boolean> offHiredQuery(CriteriaBuilder cb, CriteriaQuery<HireInstrumentDto> cq, Path<Instrument> instrument) {
		val maxHireItem = cq.subquery(Integer.class);
		val subHireItem = maxHireItem.from(HireItem.class);
		val subHire = subHireItem.join(HireItem_.hire);
		maxHireItem.select(cb.max(subHireItem.get(HireItem_.id)));
		maxHireItem.where(cb.and(
			cb.equal(subHireItem.join(HireItem_.hireInst).get(HireInstrument_.inst), instrument),
			cb.isFalse(subHire.get(Hire_.enquiry))
		));

		val offHire = cq.subquery(Boolean.class);
		val hireItem = offHire.from(HireItem.class);
		offHire.where(cb.equal(hireItem,maxHireItem));
		offHire.select(hireItem.get(HireItem_.offHire));
		return offHire;
	}



	private Expression<String> getFullInstrumentModelName(CriteriaBuilder cb, Join<Instrument, InstrumentModel> model, Join<InstrumentModel, Description> modelDescription,
														  Join<Instrument, Mfr> instrumentMfr, Path<String> instrumentModelName)  {
		val modelNameTranslation  = joinTranslation(cb,model, InstrumentModel_.nameTranslations, LocaleContextHolder.getLocale());
		val typology = modelDescription.get(Description_.typology);
		val convertedTypology =  cb.<String>selectCase().when(cb.equal(typology,cb.literal(1)),cb.literal("(BM)"))
				.when(cb.equal(typology,cb.literal("2")),cb.literal("(SFC)"))
				.otherwise("");
		val genericPredicate = cb.and(cb.equal(model.get(InstrumentModel_.modelMfrType), ModelMfrType.MFR_GENERIC)
							,cb.isNotNull(instrumentMfr.get(Mfr_.name)),cb.isFalse(instrumentMfr.get(Mfr_.genericMfr)));
		val generic = cb.<String>selectCase().when(genericPredicate,instrumentMfr.get(Mfr_.name)).otherwise("");
		val modelName= model.get(InstrumentModel_.model);
		val modelNamePredicate = cb.and(cb.or(cb.isNull(modelName),cb.equal(modelName,"/")),cb.isNotNull(instrumentModelName),cb.notEqual(instrumentModelName,"/"));
		val generatedModelName = cb.<String>selectCase().when(modelNamePredicate,instrumentModelName).otherwise("");
		return StringJpaUtils.trimAndConcatWithWhitespace(modelNameTranslation,convertedTypology)
				.append(generic)
				.append(generatedModelName).apply(cb);
	}
}