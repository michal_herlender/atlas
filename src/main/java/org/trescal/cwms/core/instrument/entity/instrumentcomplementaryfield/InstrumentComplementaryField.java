package org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "instrumentcomplementaryfield")
public class InstrumentComplementaryField {
	private int instrumentComplementaryFieldId;
	private Instrument instrument;
	private Date purchaseDate;
	private String deliveryStatus;
	private Date firstUseDate;
	private BigDecimal purchaseCost;
	private String accreditation;
	private String formerBarCode;
	private String formerLocalDescription;
	private String formerName;
	private String formerFamily;
	private String formerCharacteristics;
	private String formerManufacturer;
	private String formerModel;
	private String clientRemarks;
	private String provider;
	private Date nextMaintenanceDate;
	private Date nextInterimCalibrationDate;
	private UoM deviationUnits;
	private CertificateClass certificateClass;
	private BigDecimal nominalValue;
	private String customerAcceptanceCriteria;
	private CalibrationVerificationStatus calibrationStatusOverride;
	



	@Id
	@Column(name = "instrumentComplementaryFieldId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getInstrumentComplementaryFieldId() {
		return instrumentComplementaryFieldId;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable = false)
	public Instrument getInstrument() {
		return instrument;
	}

	//@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name = "purchaseDate")
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	@Size(max=255)
	@Column(name = "deliveryStatus")
	public String getDeliveryStatus() {
		return deliveryStatus;
	}
	@Column(name = "firstUseDate")
	public Date getFirstUseDate() {
		return firstUseDate;
	}
	@Column(name = "purchaseCost")
	public BigDecimal getPurchaseCost() {
		return purchaseCost;
	}
	@Size(max=255)
	@Column(name = "accreditation")
	public String getAccreditation() {
		return accreditation;
	}
	/**
	 * @deprecated use dedicated formerBarCode field on instrument
	 * TODO - all references to this field should be replaced with instrument formerBarCode 
	 */
	@Deprecated
	@Size(max=255)
	@Column(name = "formerBarCode")
	public String getFormerBarCode() {
		return formerBarCode;
	}
	@Size(max=255)
	@Column(name = "formerLocalDescription")
	public String getFormerLocalDescription() {
		return formerLocalDescription;
	}
	@Size(max=255)
	@Column(name = "formerName")
	public String getFormerName() {
		return formerName;
	}
	@Size(max=255)
	@Column(name = "formerFamily")
	public String getFormerFamily() {
		return formerFamily;
	}
	@Size(max=255)
	@Column(name = "formerCharacteristics")
	public String getFormerCharacteristics() {
		return formerCharacteristics;
	}
	@Size(max=255)
	@Column(name = "formerManufacturer")
	public String getFormerManufacturer() {
		return formerManufacturer;
	}
	@Size(max=255)
	@Column(name = "formerModel")
	public String getFormerModel() {
		return formerModel;
	}
	@Size(max=255)
	@Column(name = "remarks")
	public String getClientRemarks() {
		return clientRemarks;
	}
	@Size(max=255)
	@Column(name = "provider")
	public String getProvider() { return provider; }
	@Column(name="nextmaintenancedate")
	public Date getNextMaintenanceDate() {
		return nextMaintenanceDate;
	}

	@Column(name="nextinterimcalibrationdate")
	public Date getNextInterimCalibrationDate() {
		return nextInterimCalibrationDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uomid")
	public UoM getDeviationUnits() {
		return deviationUnits;
	}

	@Column(name = "certclass")
	@Enumerated(EnumType.STRING)
	public CertificateClass getCertificateClass() {
		return certificateClass;
	}

	@Column(name = "nominalValue")
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	@Column(name = "calibrationstatusoverride")
	@Enumerated(EnumType.STRING)
	public CalibrationVerificationStatus getCalibrationStatusOverride() {
		return calibrationStatusOverride;
	}

	@Column(name = "customeracceptancecriteria")
	public String getCustomerAcceptanceCriteria() {
		return customerAcceptanceCriteria;
	}

	public void setCustomerAcceptanceCriteria(String customerAcceptanceCriteria) {
		this.customerAcceptanceCriteria = customerAcceptanceCriteria;
	}

	public void setCalibrationStatusOverride(CalibrationVerificationStatus calibrationStatusOverride) {
		this.calibrationStatusOverride = calibrationStatusOverride;
	}

	public void setCertificateClass(CertificateClass certificateClass) { this.certificateClass = certificateClass; }

    public void setInstrumentComplementaryFieldId(int instrumentComplementaryFieldId) {
		this.instrumentComplementaryFieldId = instrumentComplementaryFieldId;
	}	
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public void setFirstUseDate(Date firstUseDate) {
		this.firstUseDate = firstUseDate;
	}
	public void setPurchaseCost(BigDecimal purchaseCost) {
		this.purchaseCost = purchaseCost;
	}
	public void setAccreditation(String accreditation) {
		this.accreditation = accreditation;
	}
	public void setFormerBarCode(String formerBarCode) {
		this.formerBarCode = formerBarCode;
	}
	public void setFormerLocalDescription(String formerLocalDescription) {
		this.formerLocalDescription = formerLocalDescription;
	}
	public void setFormerName(String formerName) {
		this.formerName = formerName;
	}
	public void setFormerFamily(String formerFamily) {
		this.formerFamily = formerFamily;
	}
	public void setFormerCharacteristics(String formerCharacteristics) {
		this.formerCharacteristics = formerCharacteristics;
	}
	public void setFormerManufacturer(String formerManufacturer) {
		this.formerManufacturer = formerManufacturer;
	}
	public void setFormerModel(String formerModel) {
		this.formerModel = formerModel;
	}
	public void setClientRemarks(String remarks) {
		this.clientRemarks = remarks;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public void setNextMaintenanceDate(Date nextMaintenanceDate) {
		this.nextMaintenanceDate = nextMaintenanceDate;
	}
	
	public void setNextInterimCalibrationDate(Date nextInterimCalibrationDate) {
		this.nextInterimCalibrationDate = nextInterimCalibrationDate;
	}
	public void setDeviationUnits(UoM deviationUnits) { this.deviationUnits = deviationUnits; }

	public void setNominalValue(BigDecimal nominalValue) { this.nominalValue = nominalValue; }


}
