package org.trescal.cwms.core.instrument.entity.checkout.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckOutDto {

	private String barcode;
	private String jobno;

	private String checkoutdate1;
	private String checkoutdate2;

	private String checkindate1;
	private String checkindate2;

	private boolean checkoutcomplet;

}
