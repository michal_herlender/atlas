package org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;

/**
 * Created by paul.weston on 22/02/2017.
 */
@Service("InstrumentComplementaryFieldService")
public class InstrumentComplementaryFieldServiceImpl extends BaseServiceImpl<InstrumentComplementaryField, Integer> implements InstrumentComplementaryFieldService {

    @Autowired
    InstrumentComplementaryFieldDao icfDao;
    
    @Autowired
    InstrumService instrumService;

    @Override
    public InstrumentComplementaryField getComplementaryFieldByPlantid(int plantid) {
    	InstrumentComplementaryField complementaryFields = icfDao.getComplementaryFieldByPlantid(plantid);
    	if(complementaryFields == null){
    		Instrument instrument = this.instrumService.get(plantid);
    		complementaryFields = new InstrumentComplementaryField();
    		complementaryFields.setInstrument(instrument);
    		icfDao.persist(complementaryFields);
    	}
    	return complementaryFields;
    }

	@Override
    protected BaseDao<InstrumentComplementaryField,Integer> getBaseDao() {
        return icfDao;
    }
}
