package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;

@Service
public class InstrumentFieldDefinitionServiceImpl extends BaseServiceImpl<InstrumentFieldDefinition, Integer>implements InstrumentFieldDefinitionService {

	@Autowired
	InstrumentFieldDefinitionDao dao;
	
	@Override
	protected BaseDao<InstrumentFieldDefinition, Integer> getBaseDao() {
		return this.dao;
	}

	@Override
	public Set<InstrumentFieldDefinition> getInstrumentFieldDefinitionsForCompany(Company company) {
		return new TreeSet<InstrumentFieldDefinition>(dao.getInstrumentFieldDefinitionsForCompany(company));
	}
	
	@Override
	public InstrumentFieldDefinition getForCompanyAndName(Company company, String name) {
		return dao.getForCompanyAndName(company, name);
	}

}
