package org.trescal.cwms.core.instrument.entity.instrument.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.dto.*;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrument.form.EditInstrumentForm;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrument.form.InstrumentInGroupLookUpForm;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.jobitem.entity.enums.NewJobItemSearchOrderByEnum;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequestDateRange;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentForm;

import java.time.LocalDate;
import java.util.*;

public interface InstrumDao extends BaseDao<Instrument, Integer> {
    Integer batchUpdateOwnership(Contact oldContact, Contact newContact, Address newAddress, Company newCompany);

    Optional<Integer> checkExists(String plantno, String serialno, Integer coid, Integer companyGroupId);

    Long countByAddr(Integer addrid);

    Long countByContact(Integer contactid);

    Long countByLocation(Integer locid);

    Long countBySubdiv(Integer subdivid);

    Instrument findEagerInstrum(int id);

    Instrument findEagerModelInstrum(int plantid);

    Instrument findEagerModelInstrumWithModules(int plantid);

    List<Instrument> findInstrumByBarCode(String barcode);

    List<Instrument> getAll(Company comp, Subdiv sub, Contact con, ScheduledQuoteRequestType type,
                            ScheduledQuotationRequestDateRange dateRange, boolean activeOnly);

    List<Instrument> getByBarcode(String barcode, Company company);

    List<Instrument> getAllContactInstrums(int personid);

    List<Instrument> getAllInstrumentsOutOfCalibration(Company comp, Subdiv sub, Contact con);

    List<Instrument> getAvailableBaseUnits(int coid, int modelid);

    List<Instrument> getAvailableModules(int coid, int modelid);

    List<Integer> getBarcodesOnJob(int jobId);

    int getCompanyInstrumentCount(int coid);

    List<Instrument> getCompanyInstrumentsFromModelIds(Integer compId, List<Integer> modelIds);

    /**
     * At least one parameter must be specified
     *
     * @return list of instrument ID
     */
    List<Integer> getInstrumentIds(Integer contactId, Integer addressId);

    EditInstrumentForm getEditInstrumentForm(Integer id);

    List<Calibration> getHistory(Instrument instrument);

    List<Instrument> getInstruments(List<Integer> plantIds);

    List<InstrumentProjectionDTO> getInstrumentProjectionDTOs(Locale locale, Collection<Integer> plantIds);

    List<Instrument> getInstStandardsWithNoRecallDate();

    List<Instrument> getInstsWithMismatchingOwnerAndAddress();

    InstrumentPlantillasDTO getPlantillasInstrument(int plantId);

    PagedResultSet<InstrumentPlantillasDTO> getPlantillasInstruments(int allocatedCompanyId, Date afterLastModified, Integer addrid, 
    		String plantno, int businessSubdivId, int resultsPerPage, int currentPage);

    List<InstrumentCheckDuplicatesDto> getSimilarIns(int coid, String plantno, String serialno, Integer modelId, Boolean mfrReqForModel,
                                                     Boolean searchModelMatches);

    boolean instrumOnJob(int instrumId, int jobId);

    PagedResultSet<Instrument> queryInstruments(PagedResultSet<Instrument> prs,
                                                InstrumentAndModelSearchForm<?> form, NewJobItemSearchOrderByEnum orderBy);

    PagedResultSet<Instrument> queryInstrumentsPaged(PagedResultSet<Instrument> prs,
                                                     InstrumentAndModelSearchForm<?> form, NewJobItemSearchOrderByEnum orderBy, Locale locale);

    PagedResultSet<InstrumentModel> queryDistinctModelsFromInstruments(PagedResultSet<InstrumentModel> prs,
                                                                       InstrumentAndModelSearchForm<?> form);

    PagedResultSet<InstrumentModel> queryDistinctModelsFromInstrumentsPaged(PagedResultSet<InstrumentModel> prs,
                                                                            PagedResultSet<Instrument> prsInst, InstrumentAndModelSearchForm<?> form, Locale locale, NewJobItemSearchOrderByEnum orderBy);

    List<Instrument> queryForRecall(Company company, LocalDate dateTo);

    List<Integer> queryForRecallWithActiveJobItems(Company company, LocalDate dateTo);

    List<InstrumentAssetSearchResultWrapper> searchBusinessInstrumentsForAssetsHQL(int coid, boolean standards, String mfr,
                                                                                   String model, String desc, String plantid, String plantno, String serialno);

    List<InstrumentSearchResultsDTO> searchInstruments(Company company, String plantNo, String serialNo,
                                                       Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv,
                                                       Address address, Boolean outOfCal, InstrumentModelFamily family, String modelText,
                                                       String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form);

    PagedResultSet<InstrumentSearchResultsDTO> searchInstrumentsPaged(
            PagedResultSet<InstrumentSearchResultsDTO> pagedResultSet, Company company, String plantNo, String serialNo,
            Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv,
            Address address, Boolean outOfCal, InstrumentModelFamily family, String modelText,
            String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form);

    PagedResultSet<Instrument> searchInstrumentsPaged(PagedResultSet<Instrument> prs,
                                                      InstrumentAndModelSearchForm<Instrument> form, Integer subdivid, Integer personid, Integer addressid,
                                                      Boolean outOfCal, NewQuoteInstSortType sortType, boolean ascOrder, Locale locale);

    List<Instrument> searchPlantNo(Collection<Integer> companyIds, String plantno);

    List<Instrument> webSearchInstrumentsDueForCalibration(LocalDate dateFrom, LocalDate dateTo, Integer coid, Integer subdivid,
                                                           Integer addrid, Integer personid);

    List<InstrumentSearchResultWrapper> websearchInstruments(int coid, String mfr, String model, String desc,
                                                             Integer descid, String plantid, String plantno, String serialno,
                                                             Locale userLocale, Locale primaryLocale);

    Date getLastCalDate(Instrument instrument);

    List<PossibleInstrumentDTO> lookupPossibleInstruments(int clientCompanyId, int subdivid, int asnid, Locale locale,
                                                          List<Integer> plantids, List<String> plantnos, List<String> serialnos);


    Map<Integer, Instrument> fetchInstrumentsForJobItemCreation(List<Integer> plantids);

    List<Instrument> getByOwningCompany(Integer plantid, Integer coid, String plantno, String serialno);
   
    Instrument getInstrumentByPlantNoAndSerialNoForCompany(Integer coid,String plantno, String serialno);

    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> searchInstrumentsForNewJobItem(NewJobItemSearchInstrumentForm form);

    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> findInstrumentsFromJobQuotes(NewJobItemSearchByLinkedQuotationForm form);

    Optional<InstrumentTooltipDto> loadInstrumentForTooltip(Integer plantId);

    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> findPagedCompanyInstrumentsFromModelIds(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds);

    List<InstrumentCheckDuplicatesDto> lookupForInstrumentsInGroup(InstrumentInGroupLookUpForm form);
}