package org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;

@Service("InstrumentValidationStatusService")
public class InstrumentValidationStatusServiceImpl implements InstrumentValidationStatusService
{
	@Autowired
	InstrumentValidationStatusDao InstrumentValidationStatusDao;

	public InstrumentValidationStatus findInstrumentValidationStatus(int id)
	{
		return this.InstrumentValidationStatusDao.find(id);
	}

	public void insertInstrumentValidationStatus(InstrumentValidationStatus InstrumentValidationStatus)
	{
		this.InstrumentValidationStatusDao.persist(InstrumentValidationStatus);
	}

	public void updateInstrumentValidationStatus(InstrumentValidationStatus InstrumentValidationStatus)
	{
		this.InstrumentValidationStatusDao.update(InstrumentValidationStatus);
	}

	public List<InstrumentValidationStatus> getAllInstrumentValidationStatuss()
	{
		return this.InstrumentValidationStatusDao.findAll();
	}

	public void setInstrumentValidationStatusDao(InstrumentValidationStatusDao InstrumentValidationStatusDao)
	{
		this.InstrumentValidationStatusDao = InstrumentValidationStatusDao;
	}

	public void deleteInstrumentValidationStatus(InstrumentValidationStatus instrumentvalidationstatus)
	{
		this.InstrumentValidationStatusDao.remove(instrumentvalidationstatus);
	}

	public void saveOrUpdateInstrumentValidationStatus(InstrumentValidationStatus instrumentvalidationstatus)
	{
		this.InstrumentValidationStatusDao.saveOrUpdate(instrumentvalidationstatus);
	}
}