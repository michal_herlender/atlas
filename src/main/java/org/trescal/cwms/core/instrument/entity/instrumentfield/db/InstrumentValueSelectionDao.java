package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentValueSelectionDTO;

public interface InstrumentValueSelectionDao {
	
	List<InstrumentValueSelectionDTO> getAll(InstrumentFieldDefinition fieldDefinition);
	
	List<Instrument> getMatchingInstruments(InstrumentFieldDefinition fieldDefinition, InstrumentFieldLibraryValue value);
}