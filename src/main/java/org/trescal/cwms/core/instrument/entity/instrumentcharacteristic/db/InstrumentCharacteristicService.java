package org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.db;

import java.util.UUID;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;

public interface InstrumentCharacteristicService extends BaseService<InstrumentCharacteristic, UUID> {}