package org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationAction;

public interface InstrumentValidationActionDao extends BaseDao<InstrumentValidationAction, Integer> {}