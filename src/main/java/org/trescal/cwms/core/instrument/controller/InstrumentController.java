package org.trescal.cwms.core.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;

@Controller @JsonController
public class InstrumentController {

	@Value("#{props['cwms.config.instrument.use_threads']}")
	private boolean useThreads;
	@Autowired
	private InstrumService instrumentService;
	
	@RequestMapping(path="instrumenttooltip.json", method=RequestMethod.GET)
	public String getInstrumentToolTip(
			@RequestParam(value="plantId", required=true) Integer plantId,
			Model model) {
		Instrument instrument = instrumentService.get(plantId);
		model.addAttribute("instrument", instrument);
		model.addAttribute("useThreads", useThreads);
		model.addAttribute("certLength",
				instrument.getJobItems().stream()
				.flatMap(ji -> ji.getCertLinks().stream()).distinct().count());
		return "trescal/core/instrument/instrumenttooltip";
	}
}