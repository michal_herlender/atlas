package org.trescal.cwms.core.instrument.views;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.confidencecheck.ConfidenceCheck;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

@Component
public class CheckOutXlsxView extends AbstractXlsxStreamingView {

	@Autowired
	private MessageSource messageSource;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		Sheet sheet = workbook.createSheet("Data");
		XlsxViewDecorator decorator = new XlsxViewDecorator(sheet, null);

		CellStyle boldStyle = decorator.getStyleHolder().getHeaderStyle();

		Locale locale = LocaleContextHolder.getLocale();

		int colIndex = 0;

		decorator.createCell(0, colIndex++, messageSource.getMessage("barcode", null, locale), boldStyle);
		decorator.createCell(0, colIndex++, messageSource.getMessage("instmodelname", null, locale), boldStyle);
		decorator.createCell(0, colIndex++, messageSource.getMessage("instrument.checkoutdate", null, locale),
				boldStyle);
		decorator.createCell(0, colIndex++, messageSource.getMessage("instrument.checkindate", null, locale),
				boldStyle);
		decorator.createCell(0, colIndex++, messageSource.getMessage("jobno", null, locale), boldStyle);
		decorator.createCell(0, colIndex++, messageSource.getMessage("instrument.checkoutcomplet", null, locale),
				boldStyle);
		decorator.createCell(0, colIndex++, messageSource.getMessage("company.hrid", null, locale), boldStyle);

		decorator.createCell(0, colIndex++, messageSource.getMessage("instrument.confidencecheckresult", null, locale),
				boldStyle);
		decorator.createCell(0, colIndex++,
				messageSource.getMessage("instrument.confidencecheckdescription", null, locale), boldStyle);

		@SuppressWarnings("unchecked")
		List<CheckOut> results = (List<CheckOut>) model.get("results");

		int rowIndex = 1;

		for (CheckOut dto : results) {
			colIndex = 0;
			decorator.createCell(rowIndex, colIndex++, dto.getInstrument().getPlantid(), null);
			decorator.createCell(rowIndex, colIndex++, dto.getInstrument().getModelname(), null);
			decorator.createCell(rowIndex, colIndex++, dto.getCheckOutDate(), null);
			decorator.createCell(rowIndex, colIndex++, dto.getCheckInDate(), null);
			if (dto.getJob() != null)
				decorator.createCell(rowIndex, colIndex++, dto.getJob().getJobno(), null);
			else
				decorator.createCell(rowIndex, colIndex++, " ", null);

			decorator.createCell(rowIndex, colIndex++, dto.getCheckOutComplet().toString(), null);
			decorator.createCell(rowIndex, colIndex++, dto.getTechnicien().getHrid(), null);

			if (!dto.getConfidenceChecks().isEmpty()){
				ConfidenceCheck confidenceChecks =dto.getConfidenceChecks().get(dto.getConfidenceChecks().size() - 1);
				decorator.createCell(rowIndex, colIndex++, confidenceChecks.getResult().getDescription(), null);
				decorator.createCell(rowIndex, colIndex++, confidenceChecks.getDescription(), null);
			}else{
				decorator.createCell(rowIndex, colIndex++, " ", null);
				decorator.createCell(rowIndex, colIndex++, " ", null);
			}
				
			rowIndex++;
		}

		decorator.autoSizeColumns();
		String fileName = "Check Out Export.xlsx";
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

	}

}
