package org.trescal.cwms.core.instrument.entity.measurementpossibility.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.measurementpossibility.MeasurementPossibility;

public interface MeasurementPossibilityDao extends BaseDao<MeasurementPossibility, Integer> {}