package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentValueNumericDTO;

public interface InstrumentValueNumericDao {
	
	List<InstrumentValueNumericDTO> getAll(InstrumentFieldDefinition fieldDefinition);
	
	List<Instrument> getMatchingInstruments(InstrumentFieldDefinition fieldDefinition, Double value);
}