package org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.db;

import java.util.UUID;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;

public interface InstrumentCharacteristicDao extends BaseDao<InstrumentCharacteristic, UUID> {}