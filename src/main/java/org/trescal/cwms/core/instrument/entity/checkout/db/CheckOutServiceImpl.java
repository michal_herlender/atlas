package org.trescal.cwms.core.instrument.entity.checkout.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.checkout.dto.CheckOutDto;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.db.UserService;

@Service
public class CheckOutServiceImpl extends BaseServiceImpl<CheckOut, Integer> implements CheckOutService {

	@Autowired
	CheckOutDao dao;
	@Autowired
	private InstrumService instService;
	@Autowired
	private UserService userService;
	@Autowired
	private JobService jobService;
	@Autowired
	private MessageSource messages;

	@Override
	protected BaseDao<CheckOut, Integer> getBaseDao() {
		return this.dao;
	}

	@Override
	public CheckOut checkIfInstrumentIsCheckOut(Instrument inst, Job job) {
		return this.dao.checkIfInstrumentIsCheckOut(inst, job);
	}

	@Override
	public List<CheckOut> getCheckOutInstrumentsByTechnicien(Contact technicien) {
		return this.dao.getCheckOutInstrumentsByTechnicien(technicien);
	}

	@Override
	public List<CheckOut> getCheckInInstrumentsByTechnicienWithNoConfidenceCheck(Contact technicien) {
		return this.dao.getCheckInInstrumentsByTechnicienWithNoConfidenceCheck(technicien);
	}

	@Override
	public ResultWrapper checkInMultipalInstruments(String BarCodes, Locale locale) {

		ResultWrapper rw = new ResultWrapper();

		if (BarCodes == null) {
			rw.setSuccess(false);
			rw.setMessage(messages.getMessage("error.rest.barcode.blank", null, locale));
		} else {

			String[] instStringBarcodes = BarCodes.split(",");

			for (String barcode : instStringBarcodes) {
				Instrument instr = this.instService.findIntSafeInstrument(barcode);

				CheckOut existCheckOut = this.checkIfInstrumentIsCheckOut(instr, null);

				existCheckOut.setCheckInDate(new Date());
				existCheckOut.setCheckOutComplet(true);

				this.merge(existCheckOut);
			}

			rw.setSuccess(true);
			rw.setMessage("ok");
		}

		return rw;

	}

	@Override
	public ResultWrapper performeCheckOut(String barcode, String jobno, String username, Locale locale) {
		ResultWrapper rw = new ResultWrapper();

		if (barcode == null) {
			rw.setSuccess(false);
			rw.setMessage(messages.getMessage("error.rest.barcode.blank", null, locale));
		} else {
			Instrument instr = this.instService.findIntSafeInstrument(barcode);
			if (instr == null) {
				rw.setSuccess(false);
				rw.setMessage(messages.getMessage("report.instrument.noresult", null, locale));
			} else {

				Job job = this.jobService.findByJobNo(jobno);

				CheckOut existCheckOut = this.checkIfInstrumentIsCheckOut(instr, job);

				if (existCheckOut != null && !existCheckOut.getCheckOutComplet()) {

					rw.setSuccess(false);
					rw.setMessage(messages.getMessage("instrument.checkout.already", null, locale));

				} else {

					CheckOut obj = new CheckOut();
					obj.setCheckOutDate(new Date());
					obj.setInstrument(instr);
					obj.setCheckOutComplet(false);
					obj.setTechnicien(this.userService.get(username).getCon());
					if (jobno != null) {
						if (job != null) {
							obj.setJob(job);
						}
					}

					this.merge(obj);

					rw.setSuccess(true);
					rw.setMessage("ok");
				}

			}
		}

		return rw;
	}

	@Override
	public List<CheckOut> searchCheckOutRecords(CheckOutDto dto) {
		return this.dao.searchCheckOutRecords(dto, this.instService.findIntSafeInstrument(dto.getBarcode()),
				this.jobService.findByJobNo(dto.getJobno()));
	}

	@Override
	public List<CheckOut> getCheckOutRecordsByInstrument(Instrument inst) {
		return this.dao.getCheckOutRecordsByInstrument(inst);
	}

}
