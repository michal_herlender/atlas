package org.trescal.cwms.core.instrument.entity.checkout;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.confidencecheck.ConfidenceCheck;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import lombok.Setter;

@Entity
@Table(name = "checkout")
@Setter
public class CheckOut {

	private Integer id;
	private Instrument instrument;
	private Date checkOutDate;
	private Boolean checkOutComplet;
	private Date checkInDate;
	private Contact technicien;
	private Job job;
	private List<ConfidenceCheck> confidenceChecks;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable = false)
	public Instrument getInstrument() {
		return instrument;
	}

	@NotNull
	@Column(name = "checkOutDate")
	public Date getCheckOutDate() {
		return checkOutDate;
	}

	@NotNull
	@Column(name = "checkOutComplet")
	public Boolean getCheckOutComplet() {
		return checkOutComplet;
	}
	

	@Column(name = "checkInDate")
	public Date getCheckInDate() {
		return checkInDate;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "techid", nullable = false)
	public Contact getTechnicien() {
		return technicien;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", nullable = true)
	public Job getJob() {
		return job;
	}

	@ManyToMany(mappedBy = "checkOuts", cascade = { CascadeType.ALL })
	public List<ConfidenceCheck> getConfidenceChecks() {
		return confidenceChecks;
	}

}
