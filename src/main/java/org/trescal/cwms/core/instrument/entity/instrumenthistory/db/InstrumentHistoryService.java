package org.trescal.cwms.core.instrument.entity.instrumenthistory.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.InstrumentHistory;

public interface InstrumentHistoryService
{
	void deleteInstrumentHistory(InstrumentHistory instrumenthistory);

	InstrumentHistory findInstrumentHistory(int id);

	List<InstrumentHistory> getAllInstrumentHistorys();

	void insertInstrumentHistory(InstrumentHistory instrumenthistory);

	InstrumentHistory prepareNewAddressChangeItem(Contact currentContact, Address oldAddress, Address newAddress, Instrument instrument);

	InstrumentHistory prepareNewContactChangeItem(Contact currentContact, Contact oldContact, Contact newContact, Instrument instrument);

	InstrumentHistory prepareNewCompanyChangeItem(Contact currentContact, Company oldCompany, Company newCompany, Instrument instrument);
	
	void saveOrUpdateAllInstrumentHistory(Collection<InstrumentHistory> items);

	void saveOrUpdateInstrumentHistory(InstrumentHistory instrumenthistory);

	void updateInstrumentHistory(InstrumentHistory instrumenthistory);
}