package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class InstrumentFieldLibraryValueServiceImpl extends BaseServiceImpl<InstrumentFieldLibraryValue,Integer> implements InstrumentFieldLibraryValueService {

	@Autowired
	InstrumentFieldLibraryValueDao dao;

	@Autowired
    InstrumentValueService instrumentValueService;

	@Autowired
    InstrumentFieldDefinitionService fieldDefinitionService;
	
	@Override
	protected BaseDao<InstrumentFieldLibraryValue, Integer> getBaseDao() {
		return this.dao;
	}

	@Override
	public Set<KeyValue<Integer, String>> getLibraryValuesForDefinitionAsKeyValue(Integer fieldDefinitionId, Integer instrumentFieldValueId) {
		InstrumentFieldDefinition fieldDefinition;
		if(fieldDefinitionId == 0) fieldDefinition = instrumentValueService.get(instrumentFieldValueId).getInstrumentFieldDefinition();
		else fieldDefinition = fieldDefinitionService.get(fieldDefinitionId);
		Set<InstrumentFieldLibraryValue> libraryValues = fieldDefinition.getInstrumentFieldLibraryValues();
		return libraryValues.stream().map(v -> new KeyValue<Integer,String>(v.getInstrumentFieldLibraryid(),v.getName())).collect(Collectors.toSet());
	}
}
