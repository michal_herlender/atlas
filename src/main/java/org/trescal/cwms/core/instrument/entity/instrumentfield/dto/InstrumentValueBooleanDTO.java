package org.trescal.cwms.core.instrument.entity.instrumentfield.dto;

import org.trescal.cwms.core.tools.FieldType;

public class InstrumentValueBooleanDTO extends InstrumentValueDTO {

	private Boolean value;
	
	public Boolean getValue() {
		return value;
	}
	
	public InstrumentValueBooleanDTO(Integer plantId, String fieldName, Boolean value) {
		super(plantId, fieldName);
		this.value = value;
	}
	
	public void setValue(Boolean value) {
		this.value = value;
	}
	
	@Override
	public FieldType getFieldType() {
		return FieldType.BOOLEAN;
	}
}