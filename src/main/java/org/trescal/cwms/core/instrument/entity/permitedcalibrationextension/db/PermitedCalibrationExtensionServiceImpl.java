package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.controller.CalExtensionDto;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtensionType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.DateTools;

import java.time.LocalDate;

@Service("PermitedCalibrationExtensionService")
public class PermitedCalibrationExtensionServiceImpl extends BaseServiceImpl<PermitedCalibrationExtension, Integer>
		implements PermitedCalibrationExtensionService {

	@Autowired
	private PermitedCalibrationExtensionDao calibrationExtensionDao;

	@Autowired
	private InstrumService instrumentService;

	@Autowired
	private MessageSource messageSource;


	@Override
	protected BaseDao<PermitedCalibrationExtension, Integer> getBaseDao() {

		return this.calibrationExtensionDao;
	}



	@Override
	public CalExtensionDto insertOrUpdateAjax(CalExtensionDto calExtensionDto) {

		Instrument instrument = instrumentService.get(calExtensionDto.getPlantid());

		PermitedCalibrationExtensionType incomingType = PermitedCalibrationExtensionType.valueOf(calExtensionDto.getType());

		IntervalUnit incomingTimeUnit = IntervalUnit.valueOf(calExtensionDto.getTimeUnit());

		PermitedCalibrationExtension extension = this.calibrationExtensionDao.getExtensionForInstrument(instrument);

		if (extension == null) {
			extension = new PermitedCalibrationExtension();
			extension.setInstrument(instrument);
		}
		extension.setType(incomingType);
		extension.setValue(calExtensionDto.getValue());
		if (calExtensionDto.getTimeUnit() != null && incomingType.equals(PermitedCalibrationExtensionType.TIME)) {
			extension.setTimeUnit(incomingTimeUnit);
		}
		if (instrument.getNextCalDueDate() != null) {
			if (incomingType.equals(PermitedCalibrationExtensionType.TIME)) {
				extension.setExtensionEndDate(
					DateTools.datePlusInterval(instrument.getNextCalDueDate(),
						calExtensionDto.getValue(), incomingTimeUnit));
			} else if (incomingType.equals(PermitedCalibrationExtensionType.PERCENTAGE)) {
				Integer calIntervalInDays = instrument.getCalFrequencyUnit().toDays(instrument.getCalFrequency());
				Integer extensionDays = calIntervalInDays * extension.getValue() / 100;
				extension.setExtensionEndDate(DateTools.datePlusInterval(instrument.getNextCalDueDate(), extensionDays, IntervalUnit.DAY));
			} else {
				//just in case a new type ever gets added to enum without anything here to handle it.
				throw new RuntimeException("Invalid cal extension type");
			}
		}
        extension = this.merge(extension);
		calExtensionDto.setExtensionId(extension.getExtensionId());
		calExtensionDto.setSuccess(true);
		return calExtensionDto;
	}

	@Override
    public PermitedCalibrationExtension getExtensionForInstrument(Instrument instrument) {
        return this.calibrationExtensionDao.getExtensionForInstrument(instrument);
    }

	@Override
	public CalExtensionDto getAjax(Integer plantId) {

		Instrument instrument = instrumentService.get(plantId);
		PermitedCalibrationExtension extension = this.getExtensionForInstrument(instrument);
		CalExtensionDto calExtensionDto = new CalExtensionDto();
		val today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());

		if (extension != null) {
			calExtensionDto.setExtensionId(extension.getExtensionId());
			calExtensionDto.setPlantid(extension.getInstrument().getPlantid());
			calExtensionDto.setTimeUnit(extension.getTimeUnit() != null ? extension.getTimeUnit().name() : "");
			calExtensionDto.setTimeUnitText(extension.getTimeUnit() != null ? extension.getTimeUnit().getName(extension.getValue()) : "");
			calExtensionDto.setType(extension.getType().name());
			calExtensionDto.setValue(extension.getValue());
			calExtensionDto.setExtensionEndDate(extension.getExtensionEndDate());
			calExtensionDto.setInstrumentPreExtension(!today.isAfter(instrument.getNextCalDueDate()));
			calExtensionDto.setInstrumentInExtension(today.isAfter(instrument.getNextCalDueDate()) && !today.isAfter(extension.getExtensionEndDate()));
			calExtensionDto.setInstrumentPostExtension(today.isAfter(extension.getExtensionEndDate()));
			calExtensionDto.setSuccess(true);
		} else {
			calExtensionDto.setSuccess(false);
			calExtensionDto.setMessage(messageSource.getMessage("viewinstrument.extensionnotfound", null, LocaleContextHolder.getLocale()));
		}

		return calExtensionDto;
	}
	
	@Override
	public CalExtensionDto deleteAjax(Integer plantid) {

	    Instrument instrument = instrumentService.get(plantid);
		
		PermitedCalibrationExtension extension = this.calibrationExtensionDao.getExtensionForInstrument(instrument);

		if(extension == null) {
			String message = (messageSource.getMessage("viewinstrument.extensionnotfound", null, LocaleContextHolder.getLocale()));
			return new CalExtensionDto(false, message, plantid);
		}
		else {
			calibrationExtensionDao.remove(extension);
			return new CalExtensionDto(true,null,plantid);
		}
	}

	@Override
    public void refreshExtensionEndDate(Instrument instrument) {

		PermitedCalibrationExtension extension = this.calibrationExtensionDao.getExtensionForInstrument(instrument);

		if (extension != null && instrument.getNextCalDueDate() != null) {
			if (extension.getType().equals(PermitedCalibrationExtensionType.TIME)) {
				extension.setExtensionEndDate(
					DateTools.datePlusInterval(instrument.getNextCalDueDate(),
						extension.getValue(), extension.getTimeUnit()));
			} else if (extension.getType().equals(PermitedCalibrationExtensionType.PERCENTAGE)) {
				Integer calIntervalInDays = instrument.getCalFrequencyUnit().toDays(instrument.getCalFrequency());
				Integer extensionDays = calIntervalInDays * extension.getValue() / 100;
				extension.setExtensionEndDate(
					DateTools.datePlusInterval(instrument.getNextCalDueDate(), extensionDays, IntervalUnit.DAY));
			} else {
				throw new RuntimeException("Invalid cal extension type");
			}
			this.calibrationExtensionDao.merge(extension);
		}
	}
}
