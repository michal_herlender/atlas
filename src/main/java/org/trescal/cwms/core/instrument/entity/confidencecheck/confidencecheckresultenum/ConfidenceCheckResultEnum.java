package org.trescal.cwms.core.instrument.entity.confidencecheck.confidencecheckresultenum;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum ConfidenceCheckResultEnum {

	PASS("PASSED"), FAIL("FAILED");

	private String description;
	private ReloadableResourceBundleMessageSource messages;

	private ConfidenceCheckResultEnum(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Component
	public static class ConfidenceCheckResulMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (ConfidenceCheckResultEnum rs : EnumSet.allOf(ConfidenceCheckResultEnum.class))
				rs.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	public String getMessage() {
		if (messages != null)
			return messages.getMessage(description, null, this.toString(), LocaleContextHolder.getLocale());
		else
			return description;
	}

}
