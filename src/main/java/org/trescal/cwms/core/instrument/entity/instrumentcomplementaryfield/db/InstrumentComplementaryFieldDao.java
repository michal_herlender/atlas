package org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;

/**
 * Created by paul.weston on 22/02/2017.
 */
public interface InstrumentComplementaryFieldDao extends BaseDao<InstrumentComplementaryField, Integer> {
    InstrumentComplementaryField getComplementaryFieldByPlantid(int plantid);
}
