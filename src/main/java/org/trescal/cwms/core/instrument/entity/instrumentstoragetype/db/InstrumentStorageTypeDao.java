package org.trescal.cwms.core.instrument.entity.instrumentstoragetype.db;

import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;
import org.trescal.cwms.core.instrument.entity.instrumenttype.db.InstrumentTypeDao;

public interface InstrumentStorageTypeDao extends InstrumentTypeDao<InstrumentStorageType> {}