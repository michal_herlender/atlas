package org.trescal.cwms.core.instrument.service;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import lombok.Getter;

@Getter
public class InstrumentProjectionCommand {
	// Mandatory values set in constructor
	private Locale locale;
	private Integer allocatedCompanyId;
	// Optional values set via options (default false)
	private Boolean loadAddresses;
	private Boolean loadContacts;
	private Boolean loadLocations;
	
	public static interface INST_LOAD_ADDRESSES {};
	public static interface INST_LOAD_CONTACTS {};
	public static interface INST_LOAD_LOCATIONS {};
	
	public InstrumentProjectionCommand(Locale locale, Integer allocatedCompanyId, 
			Class<?>... options) {
		if (locale == null) throw new IllegalArgumentException("locale must not be null");
		if (allocatedCompanyId == null) throw new IllegalArgumentException("allocatedCompanyId must not be null");
		this.locale = locale;
		this.allocatedCompanyId = allocatedCompanyId;
		List<Class<?>> args = Arrays.asList(options);
		this.loadAddresses = args.contains(INST_LOAD_ADDRESSES.class);
		this.loadContacts = args.contains(INST_LOAD_CONTACTS.class);
		this.loadLocations = args.contains(INST_LOAD_LOCATIONS.class);
	}
}
