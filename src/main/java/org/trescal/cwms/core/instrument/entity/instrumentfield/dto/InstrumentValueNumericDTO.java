package org.trescal.cwms.core.instrument.entity.instrumentfield.dto;

import org.trescal.cwms.core.tools.FieldType;

public class InstrumentValueNumericDTO extends InstrumentValueDTO {

	private Double value;
	
	public InstrumentValueNumericDTO(Integer plantId, String fieldName, Double value) {
		super(plantId, fieldName);
		this.setValue(value);
	}
	
	@Override
	public FieldType getFieldType() {
		return FieldType.NUMERIC;
	}
	
	public Double getValue() {
		return value;
	}
	
	public void setValue(Double value) {
		this.value = value;
	}
}