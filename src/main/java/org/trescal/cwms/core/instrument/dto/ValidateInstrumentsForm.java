package org.trescal.cwms.core.instrument.dto;

import java.util.List;

public class ValidateInstrumentsForm {
	
	private Integer clientCompanyId;
	private List<ImportedInstrumentDTO> importedInstrumentsList;
	
	public Integer getClientCompanyId() {
		return clientCompanyId;
	}
	
	public void setClientCompanyId(Integer clientCompanyId) {
		this.clientCompanyId = clientCompanyId;
	}

	public List<ImportedInstrumentDTO> getImportedInstrumentsList() {
		return importedInstrumentsList;
	}

	public void setImportedInstrumentsList(List<ImportedInstrumentDTO> importedInstrumentsList) {
		this.importedInstrumentsList = importedInstrumentsList;
	}

}
