package org.trescal.cwms.core.instrument.entity.instrumentvalidationaction;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Entity representing an action that can be undertaken to resolve an
 * {@link InstrumentValidationIssue}.
 * 
 * @author richard
 */
@Entity
@Table(name = "instrumentvalidationaction")
public class InstrumentValidationAction extends Auditable
{
	private String action;
	private int id;
	private InstrumentValidationIssue issue;
	private Contact underTakenBy;

	private Date underTakenOn;

	@NotNull
	@Length(min = 1, max = 500)
	@Column(name = "action", length = 500, nullable = false)
	public String getAction()
	{
		return this.action;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "issueid", nullable = false)
	public InstrumentValidationIssue getIssue()
	{
		return this.issue;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable = false)
	public Contact getUnderTakenBy()
	{
		return this.underTakenBy;
	}

	@NotNull
	@Column(name = "undertakenon", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getUnderTakenOn()
	{
		return this.underTakenOn;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setIssue(InstrumentValidationIssue issue)
	{
		this.issue = issue;
	}

	public void setUnderTakenBy(Contact underTakenBy)
	{
		this.underTakenBy = underTakenBy;
	}

	public void setUnderTakenOn(Date underTakenOn)
	{
		this.underTakenOn = underTakenOn;
	}
}