package org.trescal.cwms.core.instrument.controller;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentFieldLibraryValueService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentValueService;
import org.trescal.cwms.core.instrument.form.AddEditInstrumentFieldForm;
import org.trescal.cwms.core.instrument.form.AddEditInstrumentFieldValidator;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@RequestMapping(value="addeditflexiblefieldvalue.htm")
public class AddEditInstrumentFieldController {
	
	@Autowired
	private InstrumentFieldLibraryValueService instrumentFieldLibraryValueService;
	@Autowired
	private InstrumentValueService instrumentValueService;
	@Autowired
	private AddEditInstrumentFieldValidator validator;
	
	@InitBinder("command")
    protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.setValidator(validator);
    }
	
	@ModelAttribute("selectionvalues")
	public Set<KeyValue<Integer,String>> initializeSelectionValues(@RequestParam(name="fielddefid", required=false, defaultValue="0") Integer fieldDefinitionId,
			@RequestParam(name="valueid", required=false, defaultValue="0") Integer instrumentFieldValueId) throws Exception{
		if(instrumentFieldValueId == 0 && fieldDefinitionId == 0){
			throw new Exception("fielddefid and valueid both zero");
		}
        return instrumentFieldLibraryValueService.getLibraryValuesForDefinitionAsKeyValue(fieldDefinitionId, instrumentFieldValueId);
	}
	
	@ModelAttribute("command")
	public AddEditInstrumentFieldForm initializeFormBackingObject(@RequestParam(name="fielddefid", required=false, defaultValue="0") Integer fieldDefinitionId,
			@RequestParam(name="valueid", required=false, defaultValue="0") Integer instrumentFieldValueId,
			@RequestParam(name="plantid", required=true) Integer plantId) throws Exception{
		if(instrumentFieldValueId == 0 && fieldDefinitionId == 0){
			throw new Exception("fielddefid and valueid both zero");
		}
		return instrumentValueService.createFormBackingObject(instrumentFieldValueId,fieldDefinitionId,plantId);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String displayForm(@ModelAttribute("command") AddEditInstrumentFieldForm command) throws Exception{
		switch (command.getType()) {
		case BOOLEAN:
			return "/trescal/core/instrument/flexiblefields/addeditbooleanflexiblefieldvalue";
		case DATETIME:
			return "/trescal/core/instrument/flexiblefields/addeditdateflexiblefieldvalue";
		case NUMERIC:
			return "/trescal/core/instrument/flexiblefields/addeditnumericflexiblefieldvalue";
		case SELECTION:
			return "/trescal/core/instrument/flexiblefields/addeditselectionflexiblefieldvalue";
		case STRING:
			return "/trescal/core/instrument/flexiblefields/addeditstringflexiblefieldvalue";
		default:
			throw new Exception("unknown field type " + command.getType().toString());
		}
	}
	
	@RequestMapping(params = "update", method=RequestMethod.POST)
	public String processFormUpdate(@ModelAttribute("command") @Validated AddEditInstrumentFieldForm command, BindingResult bindingResult) throws Exception{
		
		if (bindingResult.hasErrors()) return this.displayForm(command);
		
		instrumentValueService.updateFieldValue(command);
		
		return "redirect:/viewinstrument.htm?plantid=" + command.getPlantId();
	}
	
	@RequestMapping(params = "delete", method=RequestMethod.POST)
	public String processFormDelete(@ModelAttribute("command") @Validated AddEditInstrumentFieldForm command) throws Exception{
		InstrumentValue value = instrumentValueService.get(command.getValueId());
		instrumentValueService.delete(value);
		return "redirect:/viewinstrument.htm?plantid=" + command.getPlantId();
	}
}