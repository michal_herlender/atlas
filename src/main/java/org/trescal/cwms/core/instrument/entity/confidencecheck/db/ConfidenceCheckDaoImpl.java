package org.trescal.cwms.core.instrument.entity.confidencecheck.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.confidencecheck.ConfidenceCheck;

@Repository("ConfidenceCheckDao")
public class ConfidenceCheckDaoImpl extends BaseDaoImpl<ConfidenceCheck, Integer> implements ConfidenceCheckDao {

	@Override
	protected Class<ConfidenceCheck> getEntity() {
		return ConfidenceCheck.class;
	}

}
