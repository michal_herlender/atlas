package org.trescal.cwms.core.instrument.form;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;

import lombok.Data;

@Data
public class ImportInstrumentsForm {

	private MultipartFile file;
	private Integer subdivid;
	private Integer personid;
	private Integer addrid;
	private Integer defaultServiceTypeId;
	private Integer exchangeFormatId;
	private AutoSubmitPolicyEnum autoSubmitPolicy;

}
