package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class PermitedCalibrationExtenionValidator implements ConstraintValidator<PermitedCalibrationExtensionConstraint, PermitedCalibrationExtension> {

    private List<PermitedCalibrationExtensionType> allowedTypes = null;

    @Override
    public void initialize(PermitedCalibrationExtensionConstraint constraintAnnotation) {
        allowedTypes = Arrays.asList(constraintAnnotation.value());
    }

    @Override
    public boolean isValid(PermitedCalibrationExtension value, ConstraintValidatorContext context) {

        if(allowedTypes.contains(value.getType())) {
            //Check that a time unit is present if extension is a time type
            if(value.getType() == PermitedCalibrationExtensionType.TIME &&
                    value.getTimeUnit() != null) {
                return true;
            }
            else if(value.getType() == PermitedCalibrationExtensionType.PERCENTAGE) {
                return true;
            }  else {
                return false;
            }

        } else {
            //Type is invalid
            return false;
        }

    }
}
