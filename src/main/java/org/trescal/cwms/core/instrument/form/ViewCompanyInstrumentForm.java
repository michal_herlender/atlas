package org.trescal.cwms.core.instrument.form;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ViewCompanyInstrumentForm {
	
	private List<Address> addlist;
	private Integer addressid;
	private boolean ascOrder;
	private List<String> basketDefInsts;
	private List<Integer> basketIds;
	private List<String> basketPlants;
	private List<String> basketSerials;
	private int coid;
	private Company company;
	private List<Contact> compconlist;
	private List<Contact> conlist;
	private Boolean outOfCal;
	private Integer personid;
	private Integer quoteContact;
	private InstrumentAndModelSearchForm<Instrument> search;
	private NewQuoteInstSortType sortType;
	private Collection<NewQuoteInstSortType> sortTypes;
	private Integer subdivid;
	private List<Subdiv> sublist;
	private String submitType;
	private Integer totalinstrumentcount;
	private Integer selectedSubdivid;

}