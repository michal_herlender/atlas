package org.trescal.cwms.core.instrument.entity.instrumentcharacteristic;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;

@Entity
@Table(name="instrumentcharacteristic")
public class InstrumentCharacteristic implements Comparable<InstrumentCharacteristic> {

	private UUID id;
	private CharacteristicDescription characteristic;
	private Instrument instrument;
	private Double value;
	private String freeTextValue;
	private CharacteristicLibrary libraryValue;
	private Integer libraryValueId;
	private UoM uom;
	private Integer uomid;
	private ModelRange modelRange;
	private List<CharacteristicLibrary> libraryValues;
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="characteristic")
	public CharacteristicDescription getCharacteristic() {
		return characteristic;
	}
	
	public void setCharacteristic(CharacteristicDescription characteristic) {
		this.characteristic = characteristic;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="instrument")
	public Instrument getInstrument() {
		return instrument;
	}
	
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
	
	public Double getValue() {
		return value;
	}
	
	public void setValue(Double value) {
		this.value = value;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="uom")
	public UoM getUom() {
		return uom;
	}

	public void setUom(UoM uom) {
		this.uom = uom;
	}
	
	@Transient
	public Integer getUomid() {
		return uomid;
	}

	public void setUomid(Integer uomid) {
		this.uomid = uomid;
	}
	
	@Transient
	public ModelRange getModelRange() {
		return modelRange;
	}
	
	public void setModelRange(ModelRange modelRange) {
		this.modelRange = modelRange;
	}
	
	@Override
	public int compareTo(InstrumentCharacteristic o) {
		int result = this.getCharacteristic().getOrderno() - o.getCharacteristic().getOrderno();
		if(result == 0) {
			result = this.getCharacteristic().getCharacteristicDescriptionId() - o.getCharacteristic().getCharacteristicDescriptionId();
		}
		return result;
	}
	
	public String getFreeTextValue() {
		return freeTextValue;
	}
	
	public void setFreeTextValue(String freeTextValue) {
		this.freeTextValue = freeTextValue;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="libraryvalue")
	public CharacteristicLibrary getLibraryValue() {
		return libraryValue;
	}
	
	public void setLibraryValue(CharacteristicLibrary libraryValue) {
		this.libraryValue = libraryValue;
	}
	
	@Transient
	public List<CharacteristicLibrary> getLibraryValues() {
		return libraryValues;
	}
	
	public void setLibraryValues(List<CharacteristicLibrary> libraryValues) {
		this.libraryValues = libraryValues;
	}
	
	@Transient
	public Integer getLibraryValueId() {
		return libraryValueId;
	}

	public void setLibraryValueId(Integer libraryValueId) {
		this.libraryValueId = libraryValueId;
	}
}