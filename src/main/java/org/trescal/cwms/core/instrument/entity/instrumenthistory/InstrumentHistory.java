package org.trescal.cwms.core.instrument.entity.instrumenthistory;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.Date;

@Entity
@Table(name = "instrumenthistory")
public class InstrumentHistory {

	public static class InstrumentHistoryComparator implements Comparator<InstrumentHistory> {
		@Override
		public int compare(InstrumentHistory ih1, InstrumentHistory ih2) {
			if ((ih1.getId() == 0) && (ih2.getId() == 0)) {
				return 1;
			} else {
				return ((Integer) ih1.getId()).compareTo(ih2.getId());
			}
		}
	}

	private Contact changeBy;
	private Date changeDate;

	private int id;
	private Instrument instrument;

	private Company newCompany;
	private Company oldCompany;
	private boolean companyUpdated;

	private Contact newContact;
	private Contact oldContact;
	private boolean contactUpdated;

	private String newPlantNo;
	private String oldPlantNo;
	private boolean plantNoUpdated;

	private Address newAddress;
	private Address oldAddress;
	private boolean addressUpdated;

	private String newSerialNo;
	private String oldSerialNo;
	private boolean serialNoUpdated;

	private InstrumentStatus newStatus;
	private InstrumentStatus oldStatus;
	private boolean statusUpdated;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "changeby", nullable = false)
	public Contact getChangeBy() {
		return this.changeBy;
	}

	@NotNull
	@Column(name = "changedate", nullable = false, columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getChangeDate() {
		return this.changeDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable = false)
	public Instrument getInstrument() {
		return this.instrument;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "newcompanyid", nullable = true, foreignKey=@ForeignKey(name="FK_instrumenthistory_newCompany"))
	public Company getNewCompany() {
		return newCompany;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "newaddressid", nullable = true)
	public Address getNewAddress() {
		return this.newAddress;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "newpersonid", nullable = true)
	public Contact getNewContact() {
		return this.newContact;
	}

	@Length(max = 100)
	@Column(name = "newplantno", nullable = true, length = 100)
	public String getNewPlantNo() {
		return this.newPlantNo;
	}

	@Length(max = 100)
	@Column(name = "newserialno", nullable = true, length = 100)
	public String getNewSerialNo() {
		return this.newSerialNo;
	}

	@Column(name = "newstatusid", nullable = true)
	@Enumerated(EnumType.ORDINAL)
	public InstrumentStatus getNewStatus() {
		return this.newStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "oldcompanyid", nullable = true, foreignKey=@ForeignKey(name="FK_instrumenthistory_oldCompany"))
	public Company getOldCompany() {
		return oldCompany;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "oldaddressid", nullable = true)
	public Address getOldAddress() {
		return this.oldAddress;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "oldpersonid", nullable = true)
	public Contact getOldContact() {
		return this.oldContact;
	}

	@Length(max = 100)
	@Column(name = "oldplantno", nullable = true, length = 100)
	public String getOldPlantNo() {
		return this.oldPlantNo;
	}

	@Length(max = 100)
	@Column(name = "oldserialno", nullable = true, length = 100)
	public String getOldSerialNo() {
		return this.oldSerialNo;
	}

	@Column(name = "oldstatusid", nullable = true)
	@Enumerated(EnumType.ORDINAL)
	public InstrumentStatus getOldStatus() {
		return this.oldStatus;
	}

	@NotNull
	@Column(name = "companyupdated", nullable = false, columnDefinition = "tinyint")
	public boolean isCompanyUpdated() {
		return companyUpdated;
	}

	@NotNull
	@Column(name = "addressupdated", nullable = false, columnDefinition = "tinyint")
	public boolean isAddressUpdated() {
		return this.addressUpdated;
	}

	@Transient
	public boolean isChanges() {
		return this.companyUpdated || this.addressUpdated || this.contactUpdated 
				|| this.statusUpdated || this.serialNoUpdated || this.plantNoUpdated;
	}

	@NotNull
	@Column(name = "contactupdated", nullable = false, columnDefinition = "tinyint")
	public boolean isContactUpdated() {
		return this.contactUpdated;
	}

	@NotNull
	@Column(name = "plantnoupdated", nullable = false, columnDefinition = "tinyint")
	public boolean isPlantNoUpdated() {
		return this.plantNoUpdated;
	}

	@NotNull
	@Column(name = "serialnoupdated", nullable = false, columnDefinition = "tinyint")
	public boolean isSerialNoUpdated() {
		return this.serialNoUpdated;
	}

	@NotNull
	@Column(name = "statusUpdated", nullable = false, columnDefinition = "tinyint")
	public boolean isStatusUpdated() {
		return this.statusUpdated;
	}

	public void setNewCompany(Company newCompany) {
		this.newCompany = newCompany;
	}

	public void setOldCompany(Company oldCompany) {
		this.oldCompany = oldCompany;
	}

	public void setCompanyUpdated(boolean companyUpdated) {
		this.companyUpdated = companyUpdated;
	}

	public void setAddressUpdated(boolean addressUpdated) {
		this.addressUpdated = addressUpdated;
	}

	public void setChangeBy(Contact changeBy) {
		this.changeBy = changeBy;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public void setContactUpdated(boolean contactUpdated) {
		this.contactUpdated = contactUpdated;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	public void setNewAddress(Address newAddress) {
		this.newAddress = newAddress;
	}

	public void setNewContact(Contact newContact) {
		this.newContact = newContact;
	}

	public void setNewPlantNo(String newPlantNo) {
		this.newPlantNo = newPlantNo;
	}

	public void setNewSerialNo(String newSerialNo) {
		this.newSerialNo = newSerialNo;
	}

	public void setNewStatus(InstrumentStatus newStatus) {
		this.newStatus = newStatus;
	}

	public void setOldAddress(Address oldAddress) {
		this.oldAddress = oldAddress;
	}

	public void setOldContact(Contact oldContact) {
		this.oldContact = oldContact;
	}

	public void setOldPlantNo(String oldPlantNo) {
		this.oldPlantNo = oldPlantNo;
	}

	public void setOldSerialNo(String oldSerialNo) {
		this.oldSerialNo = oldSerialNo;
	}

	public void setOldStatus(InstrumentStatus oldStatus) {
		this.oldStatus = oldStatus;
	}

	public void setPlantNoUpdated(boolean plantNoUpdated) {
		this.plantNoUpdated = plantNoUpdated;

	}

	public void setSerialNoUpdated(boolean serialNoUpdated) {
		this.serialNoUpdated = serialNoUpdated;

	}

	public void setStatusUpdated(boolean statusUpdated) {
		this.statusUpdated = statusUpdated;
	}

	public void updateChangedStatuss() {
		Integer oldCompanyId = this.oldCompany == null ? null : this.oldCompany.getCoid();
		Integer newCompanyId = this.newCompany == null ? null : this.newCompany.getCoid();
		if (oldCompanyId != null && newCompanyId != null) {
			this.setCompanyUpdated(!oldCompanyId.equals(newCompanyId));
		}
		Integer oldAddId = this.oldAddress == null ? null : this.oldAddress.getAddrid();
		Integer newAddId = this.newAddress == null ? null : this.newAddress.getAddrid();
		if (oldAddId != null && newAddId != null) {
			this.setAddressUpdated(!oldAddId.equals(newAddId));
		}
		Integer oldPerId = this.oldContact == null ? null : this.oldContact.getPersonid();
		Integer newPerId = this.newContact == null ? null : this.newContact.getPersonid();
		if (oldPerId != null && newPerId != null) {
			this.setContactUpdated(!oldPerId.equals(newPerId));
		}
		this.setStatusUpdated(!oldStatus.equals(newStatus));
		this.setPlantNoUpdated(!StringUtils.equalsIgnoreCase(this.oldPlantNo, this.newPlantNo));
		this.setSerialNoUpdated(!StringUtils.equalsIgnoreCase(this.oldSerialNo, this.newSerialNo));
	}
}