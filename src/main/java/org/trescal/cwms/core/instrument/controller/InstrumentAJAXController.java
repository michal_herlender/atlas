package org.trescal.cwms.core.instrument.controller;

import lombok.Value;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.trescal.cwms.core.dwr.ModernResultWrapper;
import org.trescal.cwms.core.instrument.dto.AddNewInstrumentToJobDto;
import org.trescal.cwms.core.instrument.dto.InstrumentCheckDuplicatesDto;
import org.trescal.cwms.core.instrument.entity.instrument.db.CheckInstrumentForDuplicatesOut;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentUtils;
import org.trescal.cwms.core.instrument.form.InstrumentInGroupLookUpForm;

import java.util.List;
import java.util.concurrent.Executors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("instrument/")
public class InstrumentAJAXController {

    @Autowired
    private InstrumDaoImpl dao;

    @Autowired
    private InstrumService instrumentService;

    @org.springframework.beans.factory.annotation.Value("#{props['cwms.config.instrument.duplicates.similarity_planno_weight']}")
    private Double plantNoWeight;
    @org.springframework.beans.factory.annotation.Value("#{props['cwms.config.instrument.duplicates.similarity_plantno_simply_weight']}")
    private Double simplifiedPlantNoWeight;
    @org.springframework.beans.factory.annotation.Value("#{props['cwms.config.instrument.duplicates.similarity_serialno_weight']}")
    private Double serialNoWeight;
    @org.springframework.beans.factory.annotation.Value("#{props['cwms.config.instrument.duplicates.similarity_serialno_simply_weight']}")
    private Double simplifiedSerialNoWeight;

    @RequestMapping(path="checkDuplicatesForInstrument.json", method = {GET})
    public ModernResultWrapper<CheckInstrumentForDuplicatesOut> checkDuplicatesForInstrument( AddNewInstrumentToJobDto instrument) {
        return instrumentService.checkInstrumentForDuplicates(instrument);
    }

    @GetMapping("buildIndex.do")
    public SseEmitter buildIndex(@RequestParam(value = "coid", required = false) Integer coid,
    		@RequestParam(value = "nullsOnly", required = false, defaultValue="false") boolean nullsOnly) {
        val emitter = new SseEmitter(3600000L);
        Executors.newCachedThreadPool()
                .execute(() -> {
                    instrumentService.simplifyIds(coid, nullsOnly, emitter);
                    emitter.complete();
                });
        return emitter;
    }

    @GetMapping("score.json")
    public ScoreResult score(@RequestParam String serialNo, @RequestParam String plantNo, @RequestParam String inPlant, @RequestParam String inSerial) {
        return ScoreResult.of(dao.similarityFactor(plantNo, inPlant, plantNoWeight),
            dao.similarityFactor(InstrumentUtils.simplifyId(inPlant), InstrumentUtils.simplifyId(plantNo), simplifiedPlantNoWeight),
            dao.similarityFactor(serialNo, inSerial, serialNoWeight),
            dao.similarityFactor(InstrumentUtils.simplifyId(serialNo), InstrumentUtils.simplifyId(inSerial), simplifiedSerialNoWeight),
            dao.calculateDistanceGenerator(inPlant, InstrumentUtils.simplifyId(inPlant), inSerial, InstrumentUtils.simplifyId(inSerial)).apply(plantNo, InstrumentUtils.simplifyId(plantNo), serialNo, InstrumentUtils.simplifyId(serialNo))
        );
    }


    @GetMapping("lookForInstrumentInCompanyGroup.json")
    List<InstrumentCheckDuplicatesDto> lookForInstrumentInCompanyGroup(InstrumentInGroupLookUpForm form) {
        return instrumentService.lookupForInstrumentsInGroup(form);
    }

}

@Value(staticConstructor = "of")
class InstrumentExactMatchOut {
    Integer barcode;
    String plantNo;
    String serialNo;
    Integer coid;
}

@Value(staticConstructor = "of")
class ScoreResult {
    Double plantNoSimilarity;
    Double simplyPlantSimilarity;
    Double serialNoSimilarity;
    Double simplySerialSimilarity;
    Double score;
}