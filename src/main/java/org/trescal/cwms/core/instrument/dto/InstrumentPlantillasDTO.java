package org.trescal.cwms.core.instrument.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.Date;
@Data @AllArgsConstructor @NoArgsConstructor
public class InstrumentPlantillasDTO {
	// Last modified information
	private Date lastModifiedInstrument;
	// ID information for connected entities
	private Integer	addrid;
	private Integer	subdivid;
	private Integer	coid;
	private Integer	plantid;
	private Integer modelid;
	// Instrument identification / classification
	private String instMfr;
	private String modelname;
	private String serialno;
	private String plantno;
	private String location;
	private String contactFirstName;
	private String contactLastName;
	private String formerBarCode;
	private String customerDescription;
	private Boolean instrumentUsageTypeRecall;    // Used to derive receivesCalibration
	private Boolean customermanaged;
	private InstrumentStatus status;            // Used to derive outOfService field
	private LocalDate addedOn;
	private LocalDate lastCalDate;
	private Integer calFrequency;
	private IntervalUnit calFrequencyUnit;
	private LocalDate nextCalDueDate;
}
