package org.trescal.cwms.core.instrument.entity.instrumentfield.dto;

import org.trescal.cwms.core.tools.FieldType;

public class InstrumentValueStringDTO extends InstrumentValueDTO {

	private String value;
	
	public InstrumentValueStringDTO(Integer plantId, String fieldName, String value) {
		super(plantId, fieldName);
		this.value = value;
	}
	
	@Override
	public FieldType getFieldType() {
		return FieldType.STRING;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}