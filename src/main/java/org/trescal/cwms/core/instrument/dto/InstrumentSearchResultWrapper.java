package org.trescal.cwms.core.instrument.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * This class is ONLY used in the customer portal search tool for building a quotation or collection request
 * 
 * If the web portal functionality is refactored to use lit-element then this could be removed.
 */
@Getter @Setter
public class InstrumentSearchResultWrapper
{
	private String description;
	private String mfr;
	private String model;
	private int modelid;
	private int plantid;
	private String plantno;
	private Boolean scrapped;
	private String serialno;
	private String translatedModelName;	// In local language
	private String primaryModelName;	// In primary system language

	public InstrumentSearchResultWrapper(int modelid, String mfr, String model, String description, int plantid, String plantno, 
			String serialno, Boolean scrapped, String translatedModelName, String primaryModelName)
	{
		this.modelid = modelid;
		this.mfr = mfr;
		this.model = model;
		this.description = description;
		this.plantid = plantid;
		this.plantno = plantno;
		this.serialno = serialno;
		this.scrapped = scrapped;
		this.translatedModelName = translatedModelName;
		this.primaryModelName = primaryModelName;
	}

	public String getFullModelName() {
		String result = "";
		if (this.translatedModelName != null)
			result = this.translatedModelName;
		else if (this.primaryModelName != null)
			result = this.translatedModelName;
		return result;
	}

}
