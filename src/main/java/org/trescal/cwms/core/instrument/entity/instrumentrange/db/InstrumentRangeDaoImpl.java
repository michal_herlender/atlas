package org.trescal.cwms.core.instrument.entity.instrumentrange.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;

@Repository("InstrumentRangeDao")
public class InstrumentRangeDaoImpl extends BaseDaoImpl<InstrumentRange, Integer> implements InstrumentRangeDao {
	
	@Override
	protected Class<InstrumentRange> getEntity() {
		return InstrumentRange.class;
	}
}