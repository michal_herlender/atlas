package org.trescal.cwms.core.instrument.dto;

import lombok.Value;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationAction;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;
import org.trescal.cwms.core.tools.TranslationUtils;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

@Value(staticConstructor = "of")
public class InstrumentValidationIssueDto {
    Integer id;
    String issue;
    String raisedBy;
    LocalDate raisedOn;
    String resolvedBy;
    LocalDate resolvedOn;
    ValidationStatus status;
    Set<InstrumentValidationActionDto> actions;

    public static InstrumentValidationIssueDto of(Integer id, String issue, String raisedBy, LocalDate raisedOn, String resolvedBy, LocalDate resolvedOn, InstrumentValidationStatus status, Set<InstrumentValidationAction> actions) {
        return new InstrumentValidationIssueDto(id, issue, raisedBy, raisedOn, resolvedBy, resolvedOn, ValidationStatus.fromInstrumentValidationStatus(status),
            actions.stream().map(a -> InstrumentValidationActionDto.of(
                a.getId(), a.getAction(), a.getUnderTakenBy().getName(), a.getUnderTakenOn()))
                .collect(Collectors.toSet())
        );
    }

    @Value
    public static class ValidationStatus {

        Integer id;
        String name;
        Boolean requiredAttention;

        public static ValidationStatus fromInstrumentValidationStatus(InstrumentValidationStatus status) {
            return new ValidationStatus(status.getStatusid(),
                TranslationUtils.getBestTranslation(status.getNametranslations()).orElse(""),
                status.getRequiringAttention());
        }
    }
}

