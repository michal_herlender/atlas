package org.trescal.cwms.core.instrument.form;

import java.util.List;

public class QuoteCompanyInstrumentsForm
{
	private List<Integer> plantids;

	public List<Integer> getPlantids()
	{
		return this.plantids;
	}

	public void setPlantids(List<Integer> plantids)
	{
		this.plantids = plantids;
	}
}
