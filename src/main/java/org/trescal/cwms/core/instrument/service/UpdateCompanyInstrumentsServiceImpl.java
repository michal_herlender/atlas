package org.trescal.cwms.core.instrument.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.InstrumentHistory;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.db.InstrumentHistoryService;
import org.trescal.cwms.core.instrument.form.UpdateCompanyInstrumentsForm;

@Service
public class UpdateCompanyInstrumentsServiceImpl implements UpdateCompanyInstrumentsService {
	@Autowired
	private AddressService addServ;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private InstrumService instrumentServ;
	@Autowired
	private InstrumentHistoryService instHistServ;

	@Override
	public void performUpdate(Contact currentContact, UpdateCompanyInstrumentsForm form) {
		Company newCompany = form.getCompanyid() != null ? this.companyService.get(form.getCompanyid()) : null; 
		Address newInstAdd = form.getAddressid() != null ? this.addServ.get(form.getAddressid()) : null;
		Contact newInstCon = form.getPersonid() != null ? this.conServ.get(form.getPersonid()) : null;
		//
		for (Integer plantid: form.getPlantids())
		{
			Instrument inst = this.instrumentServ.get(plantid);
			// new instrument history object
			InstrumentHistory ih = null;
			// new contact chosen?
			if (newInstCon != null)
			{
				// old contact for instrument
				Contact oldInstCon = inst.getCon();
				// contact different from existing?
				if (oldInstCon.getPersonid() != newInstCon.getPersonid())
				{
					// update contact
					inst.setCon(newInstCon);
					// create history row
					ih = this.instHistServ.prepareNewContactChangeItem(currentContact, oldInstCon, newInstCon, inst);
					// saved through cascade
					inst.getHistory().add(ih);
				}
			}
			// new address chosen?
			if (newInstAdd != null)
			{
				// old address for instrument
				Address oldInstAdd = inst.getAdd();
				// address different from existing?
				if (oldInstAdd.getAddrid() != newInstAdd.getAddrid())
				{
					// update address
					inst.setAdd(newInstAdd);
					// has instrument history been created already?
					if (ih != null)
					{
						// add to current history for address change
						ih.setAddressUpdated(true);
						ih.setOldAddress(oldInstAdd);
						ih.setNewAddress(newInstAdd);
					}
					else
					{
						// create history row
						ih = this.instHistServ.prepareNewAddressChangeItem(currentContact, oldInstAdd, newInstAdd, inst);
						// saved through cascade
						inst.getHistory().add(ih);
					}
				}
			}
			if (newCompany != null) {
				// old company for instrument
				Company oldCompany = inst.getComp();
				// company different from existing?
				if (newCompany.getCoid() != oldCompany.getCoid()) {
					// update company
					inst.setComp(newCompany);
					if (ih != null) {
						ih.setCompanyUpdated(true);
						ih.setOldCompany(oldCompany);
						ih.setNewCompany(newCompany);
					}
					else {
						// create history row
						ih = this.instHistServ.prepareNewCompanyChangeItem(currentContact, oldCompany, newCompany, inst);
						// saved through cascade
						inst.getHistory().add(ih);
					}
				}
			}
		}
	}
}
