package org.trescal.cwms.core.instrument.form;

import lombok.Data;

@Data
public class InstrumentInGroupLookUpForm {
    private Integer groupId,plantId;
    private String plantNo, serialNo;
}
