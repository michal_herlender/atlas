package org.trescal.cwms.core.instrument.views;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReqDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;
import static org.trescal.cwms.core.tools.DateTools.dateFromZonedDateTime;

@Component
@Slf4j
public class InstrumentXlsxView extends AbstractXlsxStreamingView {

	@Autowired
	private MessageSource messageSource;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook wb, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		long startTime = System.currentTimeMillis();
		SXSSFWorkbook workbook = (SXSSFWorkbook) wb;
		
		@SuppressWarnings("unchecked")
		Collection<InstrumentSearchResultsDTO> instruments = (Collection<InstrumentSearchResultsDTO>) model.get("instruments");
		@SuppressWarnings("unchecked")
		List<String> flexibleFields = (List<String>) model.get("flexibleFields");
		CompanyRole companyRole = (CompanyRole) model.get("companyRole");
		@SuppressWarnings("unchecked")
		Map<Integer,InstrumentCalReqDto> requirements = (Map<Integer,InstrumentCalReqDto>) model.get("calrequirements");
		Boolean includeCertInfo = (Boolean) model.get("includeCertInfo");
		Locale locale = LocaleContextHolder.getLocale();
		// create a new Excel sheet
		Sheet sheet = workbook.createSheet(messageSource.getMessage("instruments", null, "Instruments", locale));
		sheet.setDefaultColumnWidth(25);
		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);
		// create style for date cells
		DataFormat dateFormat = wb.createDataFormat();
		CellStyle dateStyle = wb.createCellStyle();
		dateStyle.setDataFormat(dateFormat.getFormat("d/m/yyyy"));

		// create header row
		Row header = sheet.createRow(0);
		int cellNo = 0;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("barcode", null, "Barcode", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("mfr", null, "Mfr", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("model", null, "Model", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("instmodelname", null, "Instrument Model Name", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("customerdescription", null, "Customer Description", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("owner", null, "Owner", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("serialno", null, "Serial No", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("plantno", null, "Plant No", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("viewinstrument.defaultservicetype", null, "Default service type", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("instr.defaultClientRef", null, "Default Customer reference", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("lastcalibrated", null, "Last Calibrated", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("nextcaldue", null, "Next Cal Due", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("calextensiondate", null, "Cal Extension End Date", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("calinterval", null, "Cal Interval", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("calintervalunit", null, "Cal Interval Unit", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("status", null, "Status", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("editinstr.usagetype", null, "Usage Type", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("editinstr.storagetype", null, "Storage Type", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("address", null, "Address", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("subdivision", null, "Subdivision", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("viewinstrument.location", null, "Location", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("ownerposition", null, "Owner Position", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		String privateCalReqHeader =  messageSource.getMessage("calrequirement", null, "Calibration Requirement", locale) + " - " + messageSource.getMessage("private", null, "Private", locale);
		header.createCell(cellNo).setCellValue(privateCalReqHeader);
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		String publicCalReqHeader =  messageSource.getMessage("calrequirement", null, "Calibration Requirement", locale) + " - " + messageSource.getMessage("public", null, "Public", locale);
		header.createCell(cellNo).setCellValue(publicCalReqHeader);
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("lastdatein", null, "Last In", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("penultimatecal", null, "Penultimate Calibration", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		header.createCell(cellNo).setCellValue(messageSource.getMessage("antepenultimatecal", null, "Antepenultimate Calibration", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		if(companyRole.equals(CompanyRole.BUSINESS)){
			header.createCell(cellNo).setCellValue(messageSource.getMessage("editinstr.calibrationstandard", null, "Calibration Standard", locale));
			header.getCell(cellNo).setCellStyle(style);
			cellNo++;
		}
		if(includeCertInfo != null && includeCertInfo) {
			header.createCell(cellNo).setCellValue(messageSource.getMessage("certacceptance.status", null, "Status", locale));
			header.getCell(cellNo).setCellStyle(style);
			cellNo++;
			header.createCell(cellNo).setCellValue(messageSource.getMessage("certificate.deviation", null, "Deviation", locale));
			header.getCell(cellNo).setCellStyle(style);
			cellNo++;
			header.createCell(cellNo).setCellValue(messageSource.getMessage("instrument.customeracceptancecriteria", null, "Customer Acceptance Criteria", locale));
			header.getCell(cellNo).setCellStyle(style);
			cellNo++;
		}

		for (String field : flexibleFields) {
			header.createCell(cellNo).setCellValue(field);
			header.getCell(cellNo).setCellStyle(style);
			cellNo++;
		}
		long startSegmentTime = System.currentTimeMillis();        // To time export in chunks/batches
		log.debug("Initialized sheet in " + (startSegmentTime - startTime));
		response.setHeader("Content-Disposition", "attachment; filename=\"instrument_export.xlsx\"");
		int rowCount = 1;
		for (InstrumentSearchResultsDTO instrument : instruments) {
			Row row = sheet.createRow(rowCount++);
			cellNo = 0;
			row.createCell(cellNo).setCellValue(instrument.getPlantId());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getMfr());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getModel());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getModelName());
			cellNo++;
			if (instrument.getCustomerDescription() != null && !instrument.getCustomerDescription().isEmpty()) {
				row.createCell(cellNo).setCellValue(instrument.getCustomerDescription());
			}
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getOwner());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getSerialNo());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getPlantNo());
			cellNo++;
			if(instrument.getServiceTypeName() != null && !instrument.getServiceTypeName().isEmpty()) {
				row.createCell(cellNo).setCellValue(instrument.getServiceTypeName());
			}
			cellNo++;
			if(instrument.getDefaultClientRef() != null){
				row.createCell(cellNo).setCellValue(instrument.getDefaultClientRef());
			}
			cellNo++;
			if(instrument.getCertCalDate() != null) {
				Cell cell = row.createCell(cellNo);
				cell.setCellValue(instrument.getCertCalDate().toString());
				cell.setCellStyle(dateStyle);
			}
			cellNo++;
			if(instrument.getCalDueDate() != null) {
				Cell cell = row.createCell(cellNo);
				cell.setCellValue(dateFromLocalDate(instrument.getCalDueDate()));
				cell.setCellStyle(dateStyle);
			}
			cellNo++;
			if(instrument.getCalExtensionEndDate() != null) {
				Cell cell = row.createCell(cellNo);
				cell.setCellValue(dateFromLocalDate(instrument.getCalExtensionEndDate()));
				cell.setCellStyle(dateStyle);
			}
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getCalibrationInterval());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getCalibrationIntervalUnit().getName(instrument.getCalibrationInterval()));
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getInstrumentStatus().getStatus());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getUsageTypeName());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getStorageTypeName());
			cellNo++;
            row.createCell(cellNo).setCellValue(String.format("%s, %s", instrument.getAddressLine1(), instrument.getTown()));
            cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getSubdivName());
			cellNo++;
			row.createCell(cellNo).setCellValue(instrument.getLocation());
			cellNo++;
			row.createCell((cellNo)).setCellValue(instrument.getOwnerPosition());
			cellNo++;
			InstrumentCalReqDto calReqDto = requirements.get(instrument.getPlantId());
			if ((calReqDto != null) && calReqDto.getPrivateInstructions() != null && !calReqDto.getPrivateInstructions().isEmpty()) {
				row.createCell((cellNo)).setCellValue(calReqDto.getPrivateInstructions());
			}
			cellNo++;
			if ((calReqDto != null) && calReqDto.getPublicInstructions() != null && !calReqDto.getPublicInstructions().isEmpty()) {
				row.createCell((cellNo)).setCellValue(calReqDto.getPublicInstructions());
			}
			cellNo++;
			if(instrument.getLastReceived() != null) {
				Cell cell = row.createCell(cellNo);
				cell.setCellValue(dateFromZonedDateTime(instrument.getLastReceived()));
				cell.setCellStyle(dateStyle);
			}
			cellNo++;
			if(instrument.getPenultimateCalDate() != null) {
				Cell cell = row.createCell(cellNo);
				cell.setCellValue(instrument.getPenultimateCalDate());
				cell.setCellStyle(dateStyle);
			}
			cellNo++;
			if(instrument.getAntepenultimateCalDate() != null) {
				Cell cell = row.createCell(cellNo);
				cell.setCellValue(instrument.getAntepenultimateCalDate());
				cell.setCellStyle(dateStyle);
			}
			cellNo++;
			if(companyRole.equals(CompanyRole.BUSINESS)){
				Cell cell = row.createCell(cellNo);
				cell.setCellValue(instrument.getIsCalStandard());
				cell.setCellStyle(dateStyle);
				cellNo++;
			}
			if(includeCertInfo != null && includeCertInfo) {
				if(instrument.getLastCertStatus() != null) {
					row.createCell(cellNo).setCellValue(instrument.getLastCertStatus().getName());
				}
				cellNo++;
				if(instrument.getLastCertDeviation() != null) {
					row.createCell(cellNo).setCellValue(instrument.getLastCertDeviation().toPlainString());
				}
				cellNo++;
				row.createCell(cellNo).setCellValue(instrument.getCAR());
				cellNo++;
			}
			for(String field: flexibleFields) {
				String value = instrument.getFlexibleFields().get(field);
				if (!value.isEmpty()) {
					row.createCell(cellNo).setCellValue(value);
				}
				cellNo++;
			}
			if (rowCount % 1000 == 0) {
				long endSegmentTime = System.currentTimeMillis();
				log.debug("Exported to row " + rowCount + " in " + (endSegmentTime - startSegmentTime) + " ms ");
				startSegmentTime = endSegmentTime;
			}
		}
		long endSegmentTime = System.currentTimeMillis();
		log.debug("Exported to row " + rowCount + " in " + (endSegmentTime - startSegmentTime) + " ms ");
		log.debug("Created workbook in " + (endSegmentTime - startTime) + " ms ");
	}
}
