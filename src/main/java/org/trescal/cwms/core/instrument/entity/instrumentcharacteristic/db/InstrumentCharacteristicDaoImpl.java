package org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.db;

import java.util.UUID;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;

@Repository
public class InstrumentCharacteristicDaoImpl extends BaseDaoImpl<InstrumentCharacteristic, UUID>
		implements InstrumentCharacteristicDao {
	
	@Override
	protected Class<InstrumentCharacteristic> getEntity() {
		return InstrumentCharacteristic.class;
	}
}