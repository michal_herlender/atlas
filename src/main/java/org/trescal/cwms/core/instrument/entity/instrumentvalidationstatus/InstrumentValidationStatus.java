package org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;
import org.trescal.cwms.core.system.entity.status.Status;

/**
 * Entity representing the different status's that an InstrumentValidationIssue
 * can be set to.
 * 
 * @author Richard
 */
@Entity
@DiscriminatorValue("instrumentvalidation")
public class InstrumentValidationStatus extends Status
{
	private Set<InstrumentValidationIssue> issues;

	@OneToMany(mappedBy = "status", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<InstrumentValidationIssue> getIssues()
	{
		return this.issues;
	}

	public void setIssues(Set<InstrumentValidationIssue> issues)
	{
		this.issues = issues;
	}
}
