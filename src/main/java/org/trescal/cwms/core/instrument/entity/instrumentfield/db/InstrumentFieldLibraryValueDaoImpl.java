package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue;

@Repository
public class InstrumentFieldLibraryValueDaoImpl extends BaseDaoImpl<InstrumentFieldLibraryValue,Integer> implements InstrumentFieldLibraryValueDao {

	@Override
	protected Class<InstrumentFieldLibraryValue> getEntity() {
		return InstrumentFieldLibraryValue.class;
	}

}
