package org.trescal.cwms.core.instrument.entity.instrument.db;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

public interface InstrumentRepository extends CrudRepository<Instrument,Integer> {
	
	Long countByLocAddAddrid(Integer addrid);
	Long countByConPersonid(Integer contactid);
	Long countByLocLocationid(Integer locationid);
	Long countByAddSubSubdivid(Integer subdivid);
	Long countByCompCoid(Integer coid);

	Page<Instrument> findByCompCoid(Integer coid, Pageable pageable);
	Page<Instrument> findAll(Pageable pageable);
	
	List<Instrument> getByCompCoidAndModelModelidIn(Integer coid, List<Integer> modelids);
	List<Instrument> findAllByPlantidIn(List<Integer> ids);
	
}
