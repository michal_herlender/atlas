package org.trescal.cwms.core.instrument.entity.measurementpossibility.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.entity.measurementpossibility.MeasurementPossibility;

public interface MeasurementPossibilityService extends BaseService<MeasurementPossibility, Integer> {}