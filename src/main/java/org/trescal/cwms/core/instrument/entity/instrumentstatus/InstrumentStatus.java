package org.trescal.cwms.core.instrument.entity.instrumentstatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

/**
 * Entity representing the different available status's for an
 * {@link Instrument}.
 */
public enum InstrumentStatus {
	IN_CIRCULATION("instrumentstatus.incirculation", "instrumentstatus.incirculationdesc", true),
	BER("instrumentstatus.ber", "instrumentstatus.berdesc", true),
	DO_NOT_RECALL("instrumentstatus.donotrecall", "instrumentstatus.donotrecalldesc", true),
	TRANSFERRED("instrumentstatus.transferred", "instrumentstatus.transferreddesc", false);

	private final String nameCode;
	private final String descriptionCode;
	private final boolean active;

	InstrumentStatus(String nameCode, String descriptionCode, boolean active) {
		this.nameCode = nameCode;
		this.descriptionCode = descriptionCode;
		this.active = active;
	}

	private MessageSource messageSource;

	public static List<InstrumentStatus> activeStatus() {
		List<InstrumentStatus> result = new ArrayList<>();
		for (InstrumentStatus status : values())
			if (status.active) result.add(status);
		return result;
	}

	@Component
	public static class InstrumentStatusMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;
		
		@PostConstruct
        public void postConstruct() {
            for (InstrumentStatus status : EnumSet.allOf(InstrumentStatus.class))
               status.setMessageSource(messageSource);
        }
	}
	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getStatus() {
		return getStatusForLocale(LocaleContextHolder.getLocale());
	}
	public String getStatusForLocale(Locale locale) {
		return messageSource.getMessage(nameCode, null, locale);
	}
	
	public String getDescription() {
		return getDescriptionForLocale(LocaleContextHolder.getLocale());
	}
	public String getDescriptionForLocale(Locale locale) {
		return messageSource.getMessage(descriptionCode, null, locale);
	}
	
	public boolean getActive() {
		return active;
	}
}