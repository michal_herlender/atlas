package org.trescal.cwms.core.instrument.entity.instrument.db;

import org.trescal.cwms.core.instrument.dto.InstrumentCheckDuplicatesDto;

import java.util.List;

@lombok.Value(staticConstructor = "of")
public class CheckInstrumentForDuplicatesOut{
	List<InstrumentCheckDuplicatesDto> companyInstruments;
	List<InstrumentCheckDuplicatesDto> residualGroupInstruments;
	String groupName;

	public Boolean isEmpty(){
		return companyInstruments.isEmpty()&&residualGroupInstruments.isEmpty();
	}
}
