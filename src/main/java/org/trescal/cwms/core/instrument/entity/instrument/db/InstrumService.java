package org.trescal.cwms.core.instrument.entity.instrument.db;

import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ModernResultWrapper;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.WebPermissionException;
import org.trescal.cwms.core.instrument.dto.*;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentFieldDTO;
import org.trescal.cwms.core.instrument.form.*;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ItemBasketWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequestDateRange;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasInstrumentDTO;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentForm;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentsByUserForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;

public interface InstrumService extends BaseService<Instrument, Integer> {

	/**
	 * Calculates and save's an {@link Instrument}'s recall date based on it's last
	 * certificate cal date and the specified calibration interval. If no past
	 * certificates are found then the {@link Instrument} is not updated.
	 * 
	 * @param plantid  the id of the {@link Instrument} to operate on.
	 * @param interval the calibration interval (in months).
	 * @return the {@link Instrument} entity,
	 */
	Instrument calculateAndUpdateRecallDate(int plantid, int interval, IntervalUnit unit);

	/**
	 * Note: Should only be used if a recall date needs to be calculated but NOT set
     * on the instrument, i.e. for estimation purposes. In normal cases where the
     * recall date is to be incremented - after a calibration, for example - use
     * calculateAndSetRecallDate which takes in the same parameters.
     *
     * @param inst          the {@link Instrument} to calculate the recall date for
     * @param completeStamp the {@link Date} the last calibration was performed
     * @param duration      the duration in months that the calibration is valid
     *                      for. If null, this will be calculated automatically
     * @return the estimate recall {@link Date}
     */
    LocalDate calculateRecallDate(Instrument inst, LocalDate completeStamp, Integer duration, IntervalUnit intervalUnit,
                                  RecallRequirementType requirementType);

	/**
	 * Calculates the next calibration due date for an {@link Instrument} based on
	 * either the instrument's recall rule or default interval and the
	 * {@link Calibration} that's just been undertaken. Note this function does not
	 * persist any updates to the {@link Instrument}, it is up to the callee to
	 * handle this.
	 * 
	 * @param inst            the {@link Instrument} to update.
	 * @param calibration     the {@link Calibration} that has just been undertaken.
	 * @param duration        the duration in months that the
	 *                        calibration/maintenance is valid for. If null, the
	 *                        default frequency of the instrument will be taken.
	 * @param unit            the time units that the duration is measured in
	 * @param requirementType the requirement type of the 'calibration'
	 *
	 */
	Instrument calculateRecallDateAfterCalibration(Instrument inst, Calibration calibration, Integer duration,
			IntervalUnit unit, RecallRequirementType requirementType);

	Instrument calculateRecallDateAfterEditingCert(Instrument inst, Certificate cert);

	/**
	 * Calculates the next calibration due date for an {@link Instrument} based on
	 * the cal date of the certificate that has just been logged plus either the cal
	 * interval of any recal rule or the cal interval of the certificate. Note this
	 * function does not persist any updates to the {@link Instrument}, it is up to
	 * the Caller to handle this.
	 * 
	 * @param inst the {@link Instrument} to update.
	 * @param cert the Third Party {@link Certificate} that has just been logged.
	 */
	Instrument calculateRecallDateAfterTPCert(Instrument inst, Certificate cert);

	Optional<Integer> getIfExists(String plantno, String serialno, Integer coid, Integer companyGroupId);

	Boolean checkExists(String plantno, String serialno, Integer coid, Integer companyGroupId);

	/**
	 * Performs a series of checks on all instruments in the database to make sure
	 * data integrity is maintained, etc.
	 */
	void checkInstrumentsForValidity();

    Long countByAddr(Integer addrid);

	Long countByContact(Integer contactid);

	Long countByLocation(Integer locid);

	Long countBySubdiv(Integer subdivid);

	/**
	 * Creates a Plantillas instrument given the specified input parameters The
	 * inputDto should have already been validated to catch any issues preventing
	 * creation (see RestPlantillasInstrumentCreateValidator)
	 */
	Integer createPlantillasInstrument(RestPlantillasInstrumentDTO inputDto, Contact addedBy);

	void updatePlantillasInstrument(RestPlantillasInstrumentDTO inputDto, Contact addedBy);

	/**
	 * Creates instruments with the specified information (skipping deletes) and
	 * returns Map of index to instruments created Creates instruments with the
	 * specified information (skipping deletes) and returns Map of index to
	 * instruments created
	 *
	 */
	Map<Integer, Instrument> createInstruments(List<String> serialNos, List<String> plantNos, List<Integer> personIds,
			List<Integer> addrIds, List<Integer> mfrIds, List<Integer> calFreqs, List<IntervalUnit> calFreqsUnit,
			List<Integer> procIds, List<Integer> workInstIds, List<String> rangeStarts, List<String> rangeEnds,
			List<Integer> uomIds, List<Integer> threadIds, List<Boolean> instDeletes, List<String> instModelNames,
			List<Integer> defaultServiceTypeIds, List<ItemBasketWrapper> basketItems, List<String> instCustomerDescs,
			Contact addedBy);

	Instrument findEagerInstrum(int id);

	void setDirectory(Instrument i);

	/**
	 * Returns the {@link Instrument} identified by the given id with {@link Mfr},
	 * {@link InstrumentModel} and {@link Description} information eagerly loaded,
	 * null if not found.
	 * 
	 * @param plantid id of the {@link Instrument}.
	 * @return {@link Instrument} or null if not found.
	 */
	Instrument findEagerModelInstrum(int plantid);

	/**
	 * Returns the {@link Instrument} identified by the given id with {@link Mfr},
	 * {@link InstrumentModel} and {@link Description} information eagerly loaded,
	 * for base units and modules of that base unit, null if not found.
	 * 
	 * @param plantid id of the {@link Instrument}.
	 * @return {@link Instrument} or null if not found.
	 */
	Instrument findEagerModelInstrumWithModules(int plantid);

	ResultWrapper findInstrumAsStandard(int plantid);

	/**
	 * Takes the string parameter and attempts to convert into an integer
	 * {@link Instrument} plantid to search for. If the string passed will not
	 * safely convert to an int then a null is returned.
	 * 
	 * @param plantid the id of the {@link Instrument}.
	 * @return {@link Instrument} or null if no matching {@link Instrument} is found
	 *         or if the id cannot be converted into an int.
	 */
	Instrument findIntSafeInstrument(String plantid);

	Instrument findWebSafeInstrum(int id) throws WebPermissionException;

	List<Instrument> findInstrumByBarCode(String barcode);

	List<Instrument> getAll(Company comp, Subdiv sub, Contact con, ScheduledQuoteRequestType type,
			ScheduledQuotationRequestDateRange dateRange, boolean activeOnly);

	/*
	 * Searches by barcode for the specified company only, NOT using the
	 * "former barcode" intentionally, to prevent duplicate records
	 */
	List<Instrument> getByBarcode(String barcode, Company company);

	List<Instrument> getAllContactInstrums(int personid);

	/**
	 * Gets a list of all instruments that are out of calibration for the
	 * company/subdiv/contact - tries most specific first (contact) and works its
	 * way up. If all three entities are null then an exception is thrown.
	 */
	List<Instrument> getAllInstrumentsOutOfCalibration(Company comp, Subdiv sub, Contact con);

	/**
	 * Returns a {@link List} of all {@link Instrument} entities beloging to the
	 * {@link Company} identified by the given id, which are available to act as
	 * base units for the given model type.
	 * 
	 * @param coid    the id of the {@link Company} to which the base units must
	 *                belong.
	 * @param modelid the id of the module {@link InstrumentModel}.
	 * @return {@link List} ot matching {@link Instrument} entities.
	 */
	List<Instrument> getAvailableBaseUnits(int coid, int modelid);

	/**
	 * Returns a {@link List} of all {@link Instrument} entities belonging to the
	 * {@link Company} identified by the given id, which are available to add as
	 * modules to any base unit with the given model id.
	 * 
	 * @param coid    the id of the {@link Company} that the modules must belong to.
	 * @param modelid the modelid of the base unit {@link InstrumentModel}.
	 * @return {@link List} of matching {@link Instrument} entities.
	 */
	List<Instrument> getAvailableModules(int coid, int modelid);

	/**
	 * Returns a {@link List} of all {@link Instrument} barcodes that are
	 * {@link JobItem}s on the {@link Job} with the given ID.
	 * 
	 * @param jobId the {@link Job} ID.
	 * @return the {@link List} of {@link Integer} barcodes.
	 */
	List<Integer> getBarcodesOnJob(int jobId);

	/**
	 * Returns a count of the number of {@link Instrument}s belonging to the
	 * {@link Company} identified by the given id.
	 * 
	 * @param coid the id of the {@link Company}.
	 * @return count of all {@link Instrument}s.
	 */
	int getCompanyInstrumentCount(int coid);

	PagedResultSet<Instrument> getCompanyInstrumentsFromModelIds(PagedResultSet<Instrument> prs, Integer compId,
			List<Integer> modelIds);

	PagedResultSet<InstrumentModel> getCompanyDistinctModelsFromModelIds(PagedResultSet<InstrumentModel> prs,
			Integer compId, List<Integer> modelIds);

	EditInstrumentForm getEditInstrumentForm(Integer plantId);

	/**
	 * Returns all flexible fields defined for the customer that owns this
	 * instrument plus values for any that are populated;
	 */
	Set<InstrumentFieldDTO> getFlexibleFields(Instrument instrument);

	List<Calibration> getHistory(Instrument instrument);

	List<Instrument> getInstruments(List<Integer> plantIds);

	List<InstrumentProjectionDTO> getInstrumentProjectionDTOs(Locale locale, Collection<Integer> plantIds);

	/**
	 * Returns a {@link List} of {@link Instrument}s that are listed as calibration
	 * standards in the database but have no explicit recall date specified
	 * 
	 * @return the {@link List} of {@link Instrument}s
	 */
	List<Instrument> getInstStandardsWithNoRecallDate();

	/**
	 * Returns a {@link List} of {@link Instrument}s owned by a {@link Contact} in a
	 * {@link Subdiv} different to that of the {@link Instrument}'s home
	 * {@link Address}'s {@link Subdiv}
	 * 
	 * @return the {@link List} of {@link Instrument}s
	 */
	List<Instrument> getInstsWithMismatchingOwnerAndAddress();

	InstrumentPlantillasDTO getPlantillasInstrument(int plantId);

	PagedResultSet<InstrumentPlantillasDTO> getPlantillasInstruments(int allocatedCompanyId, Date afterLastModified,
			Integer addrid, String plantno, int businessSubdivId, int resultsPerPage, int currentPage);

	/**
	 * this method gets a list of instruments whose plant or serial number match for
	 * either any {@link Instrument}s that belong to the company with the specified
	 * {@link Company} id or any other {@link Instrument} that has the same
	 * {@link InstrumentModel} and is of a mfr specific type.
	 * 
	 * @param coid               coid of the company to search for
	 * @param plantno            string used to check for duplicates
	 * @param serialno           string used to check for duplicates
	 * @param modelId            id of the model we want to search for duplicates
	 *                           against
	 * @param mfrReqForModel     boolean to indicate if this is a mfr specific model
	 *                           (or null if needs loading from model)
	 * @param searchModelMatches boolean to indicate if we want to search models
	 *                           (this is not required on the website)
	 * @return List {@link Instrument}
	 */
	List<InstrumentCheckDuplicatesDto> getSimilarIns(int coid, String plantno, String serialno, Integer modelId,
			Boolean mfrReqForModel, Boolean searchModelMatches);

	boolean isInstrumentOnJob(int instrumId, int jobId);

	/**
	 * Checks whether the {@link Instrument} with the given ID belongs to the
	 * {@link Company} with the given ID.
	 * 
	 * @param instrumId the {@link Instrument} ID.
	 * @param coid      the {@link Company} ID.
	 * @return true if the {@link Company} owns the {@link Instrument}, false if
	 *         not, and null if no {@link Instrument} with the given ID exists.
	 */
	boolean isInstrumentOwnedByCompany(int instrumId, int coid);

	boolean isInstrumentOnCurrentJob(int plantid);

	void moveToSubdivisionReferences(Integer oldContactId, Integer newAddressId, Contact userContact);

    /**
     * Search instruments for (updated - 2017 first for Iberia) recall query
     *
     * @param company the company to search for
     * @param dateTo  the date to include up to
     */

    List<Instrument> queryForRecall(Company company, LocalDate dateTo);

	/**
     * Search instruments (returning ids) matching recall query that have active job
     * items
     *
     * @param company the company to search for
     * @param dateTo
     */
    List<Integer> queryForRecallWithActiveJobItems(Company company, LocalDate dateTo);

	/**
	 * Update's the module {@link Instrument} identified by the given plantid and
	 * removes it from it's current base unit {@link Instrument}.
	 * 
	 * @param mPlantid the id of the module {@link Instrument} to remove.
	 * @return {@link ResultWrapper}.
	 */
	ResultWrapper removeModule(int mPlantid);

	Instrument saveOrUpdate(EditInstrumentForm form, Contact currentContact);

	void saveOrUpdateAll(Collection<Instrument> instrums);

	/**
	 * this method scraps (set unit status to B.E.R. and scrapped boolean to true)
	 * on the {@link Instrument} identified by the plantid given. As part of this a
	 * history row is also added to the instrument
	 * 
	 * @param plantid id of the instrument to scrap
	 * @param jiid    id of the job item the instrument is being scrapped from if
	 *                this is the case
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper scrapInstrument(Integer plantid, Integer jiid, HttpSession session);

	ResultWrapper scrapInstrumentByContact(Integer plantid, Integer jiid, Contact contact);

	List<InstrumentAssetSearchResultWrapper> searchBusinessInstrumentsForAssetsHQL(int coid, boolean standards,
			String mfr, String model, String desc, String plantid, String plantno, String serialno);

	List<InstrumentSearchResultsDTO> searchInstruments(Company company, String plantNo, String serialNo,
			Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv,
			Address address, Boolean outOfCal, InstrumentModelFamily family, String modelText,
			String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form);

	PagedResultSet<InstrumentSearchResultsDTO> searchInstrumentsPaged(
			PagedResultSet<InstrumentSearchResultsDTO> pagedResultSet, Company company, String plantNo, String serialNo,
			Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv,
			Address address, Boolean outOfCal, InstrumentModelFamily family, String modelText,
			String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form);

	/**
	 * Returns a list of {@link Instrument} entities matching the given criteria.
	 * 
	 * @param subdivid  subdivision id, null or 0 to ignore.
	 * @param addressid address id, null or 0 to ignore.
	 * @param personid  contact id, null or 0 to ignore.
	 * @param form      instance of an {@link InstrumentAndModelSearchForm}
	 * @param outOfCal  true if units returned should be out of calibration, false
	 *                  if in calibration, null if either.
	 * @param sortType  enum {@link NewQuoteInstSortType} sort by which heading?
	 * @param ascOrder  boolean indicating if the results should be listed in asc or
	 *                  desc order
	 * @return {@link List} of matching {@link Instrument} entities
	 */
	PagedResultSet<Instrument> searchInstrumentsPaged(PagedResultSet<Instrument> prs,
			InstrumentAndModelSearchForm<Instrument> form, Integer subdivid, Integer personid, Integer addressid,
			Boolean outOfCal, NewQuoteInstSortType sortType, boolean ascOrder, Locale locale);

	/**
	 * Performs a search by plant number for multiple companies specified. Primarily
	 * used by REST web service for Spain calibration interface.
	 */
	List<Instrument> searchPlantNo(Collection<Integer> companyIds, String plantno);

	/**
	 * this method un-scraps (set unit status to 'In circulation' and scrapped
	 * boolean to false) on the {@link Instrument} identified by the plantid given.
	 * As part of this a history row is also added to the instrument
	 * 
	 * @param plantid id of the instrument to un-scrap
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper unscrapInstrument(Integer plantid, HttpSession session);

	/**
	 * Updates all {@link Instrument}s owned by the oldContact {@link Contact} and
	 * makes their owner the newContact {@link Contact}.
	 * 
	 * @param userContact the {@link Contact} performing the update, not null

	 * Updated to not hang on large (~4000+) instrument counts
	 */
	void updateInstrumentOwnerReferences(Integer oldContactId, Integer newContactId, Contact userContact);

	/**
	 * Tests if the {@link Instrument} has been received on a {@link Job} since it's
	 * most recent {@link Recall}.
	 * 
	 */
	void updateInstrumentRecalledNotReceived(int plantid);

	/**
	 * Validates that the currently logged in user is allowed to edit an
	 * {@link Instrument} - this is a wrapper method for
	 * userCanEditInstrument(Instrument)}.
	 * 
	 * @param request {@link HttpServletRequest}.
	 * @return {@link ResultWrapper} indicating the sucess of the operation.
	 */
	ResultWrapper userCanEditInstrument(HttpServletRequest request);

	/**
	 * Validates that the editor {@link Contact} is allowed to edit the given
	 * {@link Instrument} - only managers / admins can edit a business instrument if
	 * it is set as a standard.
	 * 
	 * @param instrument {@link Instrument}.
	 */
	ResultWrapper userCanEditInstrument(Instrument instrument);

	/**
	 * Create a new instrument model request from website, submits new mfr, model
	 * and description text to be added via an email request.
     *
     * @param manufacturer string of new manufacturer
     * @param modelname    string of new model
     * @param description  string of new description
     * @param req          {@link HttpServletRequest}.
     * @return {@link ResultWrapper}
     */
    ResultWrapper webRequestModel(String manufacturer, String modelname, String description,
                                  HttpServletRequest req);

    List<Instrument> webSearchInstrumentsDueForCalibration(LocalDate dateFrom, LocalDate dateTo, Integer coid, Integer subdivid,
                                                           Integer addrid, Integer personid);

    /**
     * Search instruments method for web client
     */
    List<InstrumentSearchResultWrapper> websearchInstrumentsHQL(int coid, String mfr, String model, String desc,
                                                                Integer descid, String plantid, String plantno, String serialno);

    PagedResultSet<SearchInstrumentsByUserDTO> searchSortedInstrumentsByUser(WebSearchInstrumentsByUserForm form,
                                                                             PagedResultSet<SearchInstrumentsByUserDTO> rs);

	PagedResultSet<InstrumentSearchResultsDTO> searchInstrumentsPaged(
			PagedResultSet<InstrumentSearchResultsDTO> pagedResultSet, Company comp, WebSearchInstrumentForm form);

	PagedResultSet<InstrumentSearchResultsDTO> searchInstrumentsPaged(
			PagedResultSet<InstrumentSearchResultsDTO> instrumentPagedResultSet, SearchInstrumentForm form);

	String getCorrectInstrumentModelName(Instrument instrument);

	Date getLastCalDate(Instrument instrument);

	List<ImportedInstrumentsSynthesisRowDTO> convertFileContentToInstrumentDTO(Integer exchangeFormat,
			List<LinkedCaseInsensitiveMap<String>> fileContent, Integer defaultAddress, Integer defaultContact,
			Integer defaultServiceType, Integer subdivid) throws ParseException;

	List<PossibleInstrumentDTO> lookupPossibleInstruments(int clientCompanyId, int allocatedsubdivid, int asnid,
			Locale locale, List<Integer> trescalIds, List<String> plantno, List<String> serialNos);

	Map<Integer, Instrument> fetchInstrumentsForJobItemCreation(List<Integer> plantids);

	Date getLastCalDateById(Integer plantId);

	ModernResultWrapper<CheckInstrumentForDuplicatesOut> checkInstrumentForDuplicates(
			AddNewInstrumentToJobDto instrument);
	
	CheckInstrumentForDuplicatesOut checkInstrumentForDuplicatesOut(AddNewInstrumentToJobDto instrument);

	void simplifyIds(Integer coid, boolean nullsOnly, SseEmitter emitter);

	List<Instrument> getByOwningCompany(Integer plantid, Integer coid, String plantno, String serialno);

	QuickAddItemsValidDTO isQuickAddItemsValid(int subdivid, int plantid, int coid, int jobid, Locale locale);

	List<Instrument> importInstruments(List<ImportedInstrumentsSynthesisRowDTO> dtos, Integer subdivid,
			Integer defaultAddressid, Integer defaultContactid, Integer defaultServiceTypeId, Integer currentContactid);

    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> searchInstrumentsForNewJobItem(NewJobItemSearchInstrumentForm form);

    Optional<InstrumentTooltipDto> loadInstrumentForTooltip(Integer plantId);

    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> findInstrumentsFromJobQuotes(NewJobItemSearchByLinkedQuotationForm form);

    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> searchCompanyInstrumentsFromModelIds(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds);

    List<InstrumentCheckDuplicatesDto> lookupForInstrumentsInGroup(InstrumentInGroupLookUpForm form);

	void calculateRecallDateAfterGSO(Instrument inst, GeneralServiceOperation gso, Integer duration,
			String intervalUnitId, RecallRequirementType recallRequirementType);

}
