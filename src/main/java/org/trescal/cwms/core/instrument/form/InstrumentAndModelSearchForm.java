package org.trescal.cwms.core.instrument.form;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Data;

/*
 * T should be either Instrument or InstrumentModel
 */
@Data
public class InstrumentAndModelSearchForm<T>
{
	private String barcode;
	private Boolean searchAllCompanies;
	private Integer compId;
	private Integer descId;
	private String descNm;
	private String domainNm;
	private Integer domainId;
	private String familyNm;
	private Integer familyId;
	private boolean hireOnlyModels;
	private Locale locale;
	private Integer mfrId;
	private String mfrNm;
	private String mfrType;
	private String model;
	private ModelMfrType modelMfrType;
	private String plantNo;
	private String salesCat;
	private Integer salesCatId;
	private String serialNo;
	private List<Integer> modelTypeIds;
	private Boolean excludeCapabilityModelTypes;
	private Boolean excludeSalesCategoryModelTypes;
	private Boolean excludeStndardModelTypes;
	private Boolean excludeQuarantined;
	private PagedResultSet<T> rs;
	private Integer plantId;
	private Integer resultsPerPage;
	private Integer pageNo;
	private String mfrtext;
	private Map<Integer, Integer> charLibraryValues;
	private String formerbarcode;
	private String customerDescription;
	private String sortBy;
}