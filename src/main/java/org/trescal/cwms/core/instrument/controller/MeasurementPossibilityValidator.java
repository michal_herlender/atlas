package org.trescal.cwms.core.instrument.controller;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.measurementpossibility.MeasurementPossibility;

@Component @IntranetController
public class MeasurementPossibilityValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(MeasurementPossibility.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		MeasurementPossibility mp = (MeasurementPossibility)target;
		if (mp.getName().equals(null) || mp.getName().equals(""))
		{
			errors.reject("error.measurementpossibility.name");
		}
		if (mp.getMaximum() != null)
		{
//			if (Double.)
//			DoubleValidator.getInstance().isValid(value)
//			errors.reject("error.measurementpossibility.decimal");
		}
		if (mp.getMinimum() != null)
		{
//			errors.reject("error.measurementpossibility.decimal");
		}
		if (mp.getMaximum() != null && mp.getMinimum() != null)
		{
			if (mp.getMinimum() > mp.getMaximum())
			{
				errors.reject("error.measurementpossibility.mingreaterthanmax");
			}
		}
	}
}