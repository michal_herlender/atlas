package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@PrimaryKeyJoinColumn(name="instrumentFieldValueid")  
@Table(name = "instrumentvaluenumeric")
public class InstrumentValueNumeric extends InstrumentValue {
	
	private Double value;

	@NotNull
	public Double getValue() {
		return value;
	}
	
	public void setValue(Double value) {
		this.value = value;
	}
}
