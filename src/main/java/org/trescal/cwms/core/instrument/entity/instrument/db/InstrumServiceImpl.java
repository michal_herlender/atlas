package org.trescal.cwms.core.instrument.entity.instrument.db;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ModernResultWrapper;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.instrument.dto.*;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.db.InstrumentCharacteristicService;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.*;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.*;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.InstrumentHistory;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.db.InstrumentHistoryService;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.db.InstrumentStorageTypeService;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.db.InstrumentUsageTypeService;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db.PermitedCalibrationExtensionService;
import org.trescal.cwms.core.instrument.form.*;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db.ThreadService;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.db.CharacteristicLibraryService;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.entity.range.RangeComparator;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ItemBasketWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.db.OnBehalfItemService;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequestDateRange;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;
import org.trescal.cwms.core.recall.entity.recall.db.RecallService;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleInterval;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.rest.plantillas.dto.output.RestPlantillasInstrumentDTO;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentForm;
import org.trescal.cwms.web.instrument.form.WebSearchInstrumentsByUserForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;
import static org.trescal.cwms.core.tools.DateTools.dateToLocalDate;

@Service("InstrumentService")
public class InstrumServiceImpl extends BaseServiceImpl<Instrument, Integer> implements InstrumService {

	protected static final Log logger = LogFactory.getLog(InstrumServiceImpl.class);

	@Autowired
	InstrumentSimplifiedIdMigratory migratory;
	@Autowired
	private AddressService addrServ;

	@Value("#{props['cwms.config.instrument.duplicates.similarity_threshold']}")
	private Double similarityThreshold;

	@Value("#{props['cwms.admin.email']}")
	private String adminEmailAddress;
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private CharacteristicLibraryService libraryService;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private InstrumDao instDao;
	@Autowired
	private JobService jobService;
	@Autowired
	private InstrumentValueBooleanDao instrumentValueBooleanDao;
	@Autowired
	private InstrumentValueDateTimeDao instrumentValueDateTimeDao;
	@Autowired
	private InstrumentValueNumericDao instrumentValueNumericDao;
	@Autowired
	private InstrumentValueSelectionDao instrumentValueSelectionDao;
	@Autowired
	private InstrumentValueStringDao instrumentValueStringDao;
	@Autowired
	private InstrumentHistoryService instrumentHistoryService;
	@Autowired
	private InstrumentStorageTypeService instStorageServ;
	@Autowired
	private InstrumentUsageTypeService instUsageServ;
	@Autowired
    private JobItemService jobItemService;
    @Autowired
    private LocationService locationService;
    @Qualifier("messageSource")
    @Autowired
    private MessageSource messages;
    @Autowired
    private MfrService mfrServ;
    @Autowired
    private InstrumentModelService modelServ;
    @Autowired
    private OnBehalfItemService onBehalfItemService;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private RecallRuleService recallRuleServ;
    @Autowired
    private RecallService recallService;
    @Autowired
    private ScheduledTaskService schTaskServ;
    @Autowired
    private SystemComponentService scServ;
    @Autowired
    private UserService userService;
    @Autowired
    private ThreadService threadServ;
	@Autowired
	private InstrumentFieldDefinitionService instrumentFieldDefinitionService;
	@Autowired
	private UoMService uomServ;
	@Autowired
	private WorkInstructionService wiServ;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private InstrumentValueService instrumentValueService;
	@Autowired
	private NewDescriptionService descService;
	@Autowired
	private InstrumentModelFamilyService familyService;
	@Autowired
	private TranslationService translationService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private CalibrationTypeService calibrationTypeService;
    @Autowired
    private PermitedCalibrationExtensionService extensionService;
    @Autowired
    private ExchangeFormatService exchangeFormatService;
    @Value("#{props['cwms.config.model.default.genericmodelname']}")
    private String defaultGenericManufacturerName;
    @Autowired
    private ContactService contactService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private InstrumentUsageTypeService instrumentUsageTypeService;
    @Autowired
    private AliasGroupService aliasGroupService;
    @Autowired
    private QuotationItemService quotationItemService;
    @Autowired
    private SessionUtilsService sessionUtilsService;
    @Autowired
    private InstrumentCharacteristicService instrumentCharacteristicService;

    @Override
	protected BaseDao<Instrument, Integer> getBaseDao() {
		return instDao;
	}

	public void calculateAndSetRecallDate(Instrument inst, LocalDate completeStamp, Integer duration, IntervalUnit unit,
										  RecallRequirementType requirementType) {
		// delegate to calculation method to calculate next recall date based on
		// passed duration/unit
		// or recall rule for requirement type if no duration passed.
		LocalDate recallDate = this.calculateRecallDate(inst, completeStamp, duration, unit, requirementType);
		switch (requirementType) {
			case FULL_CALIBRATION:
				// set the next cal due date (recall date)
				inst.setNextCalDueDate(recallDate);
				break;
			case INTERIM_CALIBRATION:
				// set the next interim calibration date
				if(inst.getInstrumentComplementaryField() != null)
					inst.getInstrumentComplementaryField().setNextInterimCalibrationDate(dateFromLocalDate(recallDate));
				break;
			case MAINTENANCE:
				// set the next maintenance date
				if(inst.getInstrumentComplementaryField() != null)
					inst.getInstrumentComplementaryField().setNextMaintenanceDate(dateFromLocalDate(recallDate));
				break;
			case SERVICE_TYPE:
				break;
		}
	}

	@Override
	public Instrument calculateAndUpdateRecallDate(int plantid, int interval, IntervalUnit unit) {
		Instrument i = this.get(plantid);
		Certificate cert = certServ.getMostRecentCertForInstrument(i, null);
		if (cert != null && interval > 0) {
			Date lastCalDate = cert.getCalDate();
			i.setNextCalDueDate(dateToLocalDate(DateTools.datePlusInterval(lastCalDate, interval, unit)));
			this.merge(i);
		} else if (interval == 0) {
			i.setNextCalDueDate(null);
		}
		return i;
	}

	@Override
	public LocalDate calculateRecallDate(Instrument inst, LocalDate completeStamp, Integer duration, IntervalUnit unit,
										 RecallRequirementType requirementType) {
		// if duration is not passed, check if there are any active recall
		// rules applied to this instrument (these will override the
		// instrument's specific interval)

		RecallRuleInterval recallRuleInterval = this.recallRuleServ.getCurrentRecallInterval(inst, requirementType,
			null);
		Integer intervalFromNow = (duration != null) ? duration : recallRuleInterval.getInterval();
		IntervalUnit intervalUnit = (unit != null) ? unit : recallRuleInterval.getUnit();

		// add the interval onto the calibration date
		return DateTools.datePlusInterval(completeStamp, intervalFromNow, intervalUnit);
	}

	@Override
	public Instrument calculateRecallDateAfterCalibration(Instrument inst, Calibration calibration,
			Integer certDuration, IntervalUnit intervalUnit, RecallRequirementType requirementType) {
		this.calculateAndSetRecallDate(inst, calibration.getCalDate(), certDuration, intervalUnit, requirementType);

		// set the calibration as the most recent calibration
		if (requirementType.equals(RecallRequirementType.FULL_CALIBRATION)) {
			inst.setLastCal(calibration);
		}

		return inst;
	}

	@Override
	public Instrument calculateRecallDateAfterEditingCert(Instrument inst, Certificate cert) {
		this.calculateAndSetRecallDate(inst, dateToLocalDate(cert.getCalDate()), cert.getDuration(), cert.getUnit(),
			RecallRequirementType.FULL_CALIBRATION);

		// set the calibration as the most recent calibration
		inst.setLastCal(cert.getCal());

		return inst;
	}

	@Override
	public Instrument calculateRecallDateAfterTPCert(Instrument inst, Certificate cert) {
		this.calculateAndSetRecallDate(inst, dateToLocalDate(cert.getCalDate()), cert.getDuration(), cert.getUnit(),
			RecallRequirementType.FULL_CALIBRATION);

		return inst;
	}

	@Override
	public Optional<Integer> getIfExists(String plantno, String serialno, Integer coid, Integer companyGroupId) {
		return this.instDao.checkExists(plantno, serialno, coid, companyGroupId);
	}

	@Override
	public Boolean checkExists(String plantno, String serialno, Integer coid, Integer companyGroupId) {
		return getIfExists(plantno, serialno, coid, companyGroupId).isPresent();
	}

	public void checkInstrumentsForValidity() {
		if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(),
				java.lang.Thread.currentThread().getStackTrace())) {
			// get instruments with a different contact subdiv to address subdiv
			List<Instrument> mismatchInsts = this.getInstsWithMismatchingOwnerAndAddress();
			// get instrument standards with no recall date
			List<Instrument> stdsWithoutRecallDate = this.getInstStandardsWithNoRecallDate();

			// find total amount of invalid instruments
			int totalInvalidInsts = mismatchInsts.size() + stdsWithoutRecallDate.size();

			Locale locale = Locale.getDefault();

			boolean warningToBeSent = totalInvalidInsts > 0;
			boolean warningSentSuccessfully = false;

			// if there are instruments to report about
			if (warningToBeSent) {
				String body = "TODO - Migrate email/instrumentvaliditycheck.vm to Thymeleaf";

				// send e-mail to admin
				String subject = this.messages.getMessage("email.instrumentvaliditycheck.subject", null,
						"Invalid Instruments Found", locale);
				warningSentSuccessfully = this.emailServ.sendBasicEmail(subject, this.adminEmailAddress,
						this.adminEmailAddress, body);
			}

			// prepare message to report
			String message = totalInvalidInsts + " instruments found to be invalid: ";

			if (warningToBeSent) {
				if (warningSentSuccessfully) {
					message = message + "Warning e-mail successfully sent to admin.";
				} else {
					message = message + "Failed sending warning e-mail to admin!";
				}
			} else {
				message = message + "No warning e-mail required.";
			}

			// report results of task run
			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(),
					java.lang.Thread.currentThread().getStackTrace(), true, message);
		} else {
			logger.info("Auto-Service Suppliers not running: scheduled task cannot be found or is turned off");
		}
	}

	@Override
	public Long countByAddr(Integer addrid) {
		return instDao.countByAddr(addrid);
	}

	@Override
	public Long countByContact(Integer contactid) {
		return instDao.countByContact(contactid);
	}

	@Override
	public Long countByLocation(Integer locid) {
		return instDao.countByLocation(locid);
	}

	@Override
	public Long countBySubdiv(Integer subdivid) {
		return instDao.countBySubdiv(subdivid);
	}

	/**
	 * Note, DTO must be successfully validated for creation before passing to this
	 * method
	 */
	public Integer createPlantillasInstrument(RestPlantillasInstrumentDTO inputDto, Contact addedBy) {
		Address address = this.addrServ.get(inputDto.getAddrid());

		Instrument instrument = new Instrument();
		instrument.setAdd(address);
		instrument.setAddedBy(addedBy);
		instrument.setAddedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		instrument.setCalFrequency(inputDto.getCalFrequency());
		instrument.setCalFrequencyUnit(IntervalUnit.valueOf(inputDto.getCalFrequencyUnits()));
		instrument.setComp(address.getSub().getComp());
		instrument.setCon(address.getSub().getDefaultContact());
		instrument.setCustomerDescription(
			inputDto.getCustomerDescription() == null ? "" : inputDto.getCustomerDescription());
		instrument.setCustomerManaged(inputDto.getCustomermanaged() != null && inputDto.getCustomermanaged());
		if ((inputDto.getLocation() != null) && !inputDto.getLocation().isEmpty()) {
			instrument.setLoc(this.locationService.findLocation(inputDto.getAddrid(), inputDto.getLocation()));
		}
		if ((inputDto.getManufacturer() != null) && !inputDto.getManufacturer().isEmpty()) {
			instrument.setMfr(this.mfrServ.findMfrByName(inputDto.getManufacturer()));
		}
		instrument.setModelname(inputDto.getModelName() == null ? "" : inputDto.getModelName());
		instrument.setModel(this.modelServ.get(inputDto.getModelid()));
		instrument.setNextCalDueDate(inputDto.getNextCalDueDate());
		instrument.setPlantno(inputDto.getPlantno() == null ? "" : inputDto.getPlantno());
		instrument.setSerialno(inputDto.getSerialno() == null ? "" : inputDto.getSerialno());
		
		instrument.setStorageType(this.instStorageServ.findDefaultType(InstrumentStorageType.class));
		
		// TODO resolve usage type when inputDto.getReceivesCalibration() is
		// false
		InstrumentUsageType defaultUsageType = this.instUsageServ.findDefaultType(InstrumentUsageType.class);
		instrument.setUsageType(defaultUsageType);
		if(defaultUsageType != null)
			instrument.setStatus(defaultUsageType.getInstrumentStatus());
		else if ((inputDto.getOutOfService() == null) || !inputDto.getOutOfService()) {
			instrument.setStatus(InstrumentStatus.IN_CIRCULATION);
		} else {
			instrument.setStatus(InstrumentStatus.BER);
		}

		if ((inputDto.getFormerBarCode() != null) && !inputDto.getFormerBarCode().isEmpty()) {
			InstrumentComplementaryField instrumentComplementaryField = new InstrumentComplementaryField();
			instrumentComplementaryField.setFormerBarCode(inputDto.getFormerBarCode());
			instrumentComplementaryField.setInstrument(instrument);
			instrument.setInstrumentComplementaryField(instrumentComplementaryField);
			instrument.setFormerBarCode(inputDto.getFormerBarCode());
		}
		// TODO observations and calibration instructions
		// Also saves complementary field by cascade
		save(instrument);
		return instrument.getPlantid();
	}

	public void updatePlantillasInstrument(RestPlantillasInstrumentDTO inputDto, Contact updatedBy) {
		Instrument instrument = instDao.find(inputDto.getPlantid());
		InstrumentHistory instrumentHistory = new InstrumentHistory();
		boolean isUpdated = false;

		// address
		if (inputDto.getAddrid() != null && !Objects.equals(inputDto.getAddrid(),instrument.getAdd().getAddrid())) {
			isUpdated = true;
			Address address = addrServ.get(inputDto.getAddrid());
			instrumentHistory.setOldAddress(instrument.getAdd());
			instrumentHistory.setNewAddress(address);
			instrumentHistory.setAddressUpdated(true);
			instrument.setAdd(address);

			// for contact update
			instrumentHistory.setContactUpdated(true);
			instrumentHistory.setOldContact(instrument.getAdd().getSub().getDefaultContact());
			Contact con = address.getSub().getDefaultContact();
			instrument.setCon(con);
			instrumentHistory.setNewContact(con);

		}

		// for location update
		if (inputDto.getLocation() != null && !inputDto.getLocation().isEmpty()) {
			Location location = locationService.findLocation(inputDto.getAddrid(), inputDto.getLocation());
			instrument.setLoc(location);
		}

		// InstrumentModel
		if ((inputDto.getModelid() != null) && !Objects.equals(inputDto.getModelid(),instrument.getModel().getModelid())) {
			isUpdated = true;
			InstrumentModel model = modelServ.get(inputDto.getModelid());
			instrument.setModel(model);
		}

		// Manufacturer (optional field on instrument, existing value may be
		// null)
		if (!StringUtils.isEmpty(inputDto.getManufacturer()) && ((instrument.getMfr() == null)
				|| !inputDto.getManufacturer().equals(instrument.getMfr().getName()))) {
			isUpdated = true;
			Mfr mfr = mfrServ.findMfrByName(inputDto.getManufacturer());
			instrument.setMfr(mfr);
		}

		// Modelname
		if ((inputDto.getModelName() != null) && !inputDto.getModelName().equals(instrument.getModelname())) {
			isUpdated = true;
			instrument.setModelname(inputDto.getModelName());
		}

		// Serialno
		if ((inputDto.getSerialno() != null) && !inputDto.getSerialno().equals(instrument.getSerialno())) {
			isUpdated = true;
			instrumentHistory.setOldSerialNo(instrument.getSerialno());
			instrument.setSerialno(inputDto.getSerialno());
			instrumentHistory.setNewSerialNo(inputDto.getModelName());
			instrumentHistory.setSerialNoUpdated(true);
		}

		// Plantno
		if ((inputDto.getPlantno() != null) && !inputDto.getPlantno().equals(instrument.getPlantno())) {
			isUpdated = true;
			instrumentHistory.setOldPlantNo(instrument.getPlantno());
			instrument.setPlantno(inputDto.getPlantno());
			instrumentHistory.setNewPlantNo(inputDto.getPlantno());
			instrumentHistory.setPlantNoUpdated(true);
		}

		// FormerBarCode (set on both inst and compfield currently, until
		// compfield fully removed from use)
		if ((inputDto.getFormerBarCode() != null)) {
			InstrumentComplementaryField instrumentComplementaryField = instrument.getInstrumentComplementaryField();
			if (instrumentComplementaryField == null) {
				instrumentComplementaryField = new InstrumentComplementaryField();
				instrument.setInstrumentComplementaryField(instrumentComplementaryField);
				instrumentComplementaryField.setInstrument(instrument);
			}
			instrumentComplementaryField.setFormerBarCode(inputDto.getFormerBarCode());
			instrument.setFormerBarCode(inputDto.getFormerBarCode());
		}

		// CustomerDescription
		if (inputDto.getCustomerDescription() != null) {
			instrument.setCustomerDescription(inputDto.getCustomerDescription());
		}


		// isCustomerManaged (only update if provided)
		if (inputDto.getCustomermanaged() != null) {
			instrument.setCustomerManaged(inputDto.getCustomermanaged());
		}

		// status
		if (inputDto.getOutOfService() != null) {

			boolean oldValue;
			switch (instrument.getStatus()) {
			case BER:
				case TRANSFERRED:
					oldValue = true;
				break;
				default:
				oldValue = false;
			}

			if (inputDto.getOutOfService() != oldValue) {
				instrumentHistory.setStatusUpdated(true);
				instrumentHistory.setOldStatus(instrument.getStatus());
				if (!inputDto.getOutOfService()) {
					instrument.setStatus(InstrumentStatus.IN_CIRCULATION);
				} else {
					instrument.setStatus(InstrumentStatus.BER);
				}
				instrumentHistory.setNewStatus(instrument.getStatus());
			}
		}

		// CalFrequency
		if (Objects.nonNull(inputDto.getCalFrequency())) {
			instrument.setCalFrequency(inputDto.getCalFrequency());
		}
		// CalFrequencyUnits
		if (!StringUtils.isEmpty(inputDto.getCalFrequencyUnits())) {
			instrument.setCalFrequencyUnit(IntervalUnit.valueOf(inputDto.getCalFrequencyUnits()));
		}

		// NextCalDueDate
		if (inputDto.getNextCalDueDate() != null) {
			instrument.setNextCalDueDate(inputDto.getNextCalDueDate());
		}

		// instrumentHistory (saved via JPA cascade)
		if (isUpdated) {
			instrumentHistory.setInstrument(instrument);
			instrumentHistory.setChangeDate(new Date());
			instrumentHistory.setChangeBy(updatedBy);
			instrument.getHistory().add(instrumentHistory);
		}
	}

	@Override
	public Map<Integer, Instrument> createInstruments(List<String> serialNos, List<String> plantNos,
			List<Integer> personIds, List<Integer> addrIds, List<Integer> mfrIds, List<Integer> calFreqs,
			List<IntervalUnit> calFreqsUnit, List<Integer> procIds, List<Integer> workInstIds, List<String> rangeStarts,
			List<String> rangeEnds, List<Integer> uomIds, List<Integer> threadIds, List<Boolean> instDeletes,
			List<String> instModelNames, List<Integer> defaultServiceTypeIds, List<ItemBasketWrapper> basketItems,
			List<String> instCustomerDescs, Contact addedBy) {
		Map<Integer, Instrument> createdInsts = new HashMap<>();


		int i = 0;
		for (ItemBasketWrapper ibw : basketItems) {
			if (ibw.getModel() != null) {
				// this item has not been deleted
				if (!instDeletes.get(i)) {
					Contact con = this.conServ.get(personIds.get(i));
					Address addr = this.addrServ.get(addrIds.get(i));

					Instrument inst = new Instrument();
					inst.setAdd(addr);
					inst.setAddedBy(addedBy);
					inst.setAddedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
					inst.setCalFrequency(12);
					inst.setCalFrequencyUnit(IntervalUnit.MONTH);
					inst.setCalibrationStandard(false);
					inst.setComp(con.getSub().getComp());
					inst.setCon(con);
                    inst.setCustomerManaged(false);
                    inst.setModel(ibw.getModel());
                    // check, whether there are i'th elements - TODO: better use
                    // maps, ensure there is a serial number, ...
                    if (i < plantNos.size() && plantNos.get(i) != null)
                        inst.setPlantno(plantNos.get(i));
                    else
                        inst.setPlantno("");
                    if (i < serialNos.size() && serialNos.get(i) != null)
                        inst.setSerialno(serialNos.get(i));
                    else
                        inst.setSerialno("");
                    InstrumentUsageType defaultUsageType = this.instUsageServ.findDefaultType(InstrumentUsageType.class);
                    inst.setUsageType(defaultUsageType);
                    if (defaultUsageType != null)
                        inst.setStatus(defaultUsageType.getInstrumentStatus());
                    else
                        inst.setStatus(InstrumentStatus.IN_CIRCULATION);
                    inst.setStorageType(this.instStorageServ.findDefaultType(InstrumentStorageType.class));

                    Capability capability = (procIds.get(i) == null) ? null : this.procServ.get(procIds.get(i));
                    inst.setCapability(capability);
                    WorkInstruction wi = (workInstIds.get(i) == null) ? null : this.wiServ.get(workInstIds.get(i));
                    List<WorkInstruction> workInstructions = new ArrayList<>();
                    if (wi != null) {
                        workInstructions.add(wi);
                        if (wi.getInstruments() != null) {
                            wi.setInstruments(new ArrayList<>());
                            wi.getInstruments().add(inst);
                        }
                        this.wiServ.save(wi);
                    }
                    inst.setWorkInstructions(workInstructions);

                    if (threadIds != null) {
                        Thread t = (threadIds.get(i) == null) ? null : this.threadServ.get(threadIds.get(i));
                        inst.setThread(t);
                    }
                    if (calFreqs != null) {
                        inst.setCalFrequency(calFreqs.get(i));
                    }

					if (calFreqsUnit != null) {
						inst.setCalFrequencyUnit(calFreqsUnit.get(i));
					}

					if (defaultServiceTypeIds != null && defaultServiceTypeIds.get(i) != null) {
						inst.setDefaultServiceType(serviceTypeService.get(defaultServiceTypeIds.get(i)));
					}

					if (rangeStarts != null) {
						if ((rangeStarts.get(i) != null) && !rangeStarts.get(i).trim().isEmpty()
								&& NumberUtils.isNumber(rangeStarts.get(i))) {
							String start = rangeStarts.get(i);
							String end = rangeEnds.get(i);

							InstrumentRange ir = new InstrumentRange();

							ir.setStart(Double.parseDouble(start));

							if ((end != null) && !end.trim().isEmpty() && NumberUtils.isNumber(end)) {
								ir.setEnd(Double.parseDouble(end));
							}

							ir.setUom(this.uomServ.findUoM(uomIds.get(i)));
							ir.setInstrument(inst);

							inst.setRanges(new TreeSet<>(new RangeComparator()));
							inst.getRanges().add(ir);
						}
					}

					// if this instrument requires a mfr (i.e. MFR_GENERIC model
					// type), set it here
					if (inst.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
						Mfr mfr = (mfrIds.size() == 0 || mfrIds.get(i) == null)
								? this.mfrServ.findMfrByName(defaultGenericManufacturerName)
								: this.mfrServ.findMfr(mfrIds.get(i));
						inst.setMfr(mfr);
						inst.setModelname(instModelNames.get(i));
					}


					if (instCustomerDescs != null) {
						inst.setCustomerDescription(instCustomerDescs.get(i));
					}

					this.save(inst);
					createdInsts.put(i, inst);
				}
			}
			// Index must be incremented for all, we skip over deleted
			// instruments
			i++;
		}

		return createdInsts;
	}

	@Override
	public Instrument findEagerInstrum(int id) {
		return this.instDao.findEagerInstrum(id);
	}

	@Override
	public Instrument findEagerModelInstrum(int plantid) {
		return this.instDao.findEagerModelInstrum(plantid);
	}

	@Override
	public Instrument findEagerModelInstrumWithModules(int plantid) {
		return this.instDao.findEagerModelInstrumWithModules(plantid);
	}

	@Override
	public Instrument get(Integer id) {
		return super.get(id);
	}

	@Override
	public void setDirectory(Instrument i) {
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (i != null && i.getDirectory() == null) {
			i.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.INSTRUMENT),
					String.valueOf(i.getPlantid())));
		}
	}

	@Override
	public List<Instrument> findInstrumByBarCode(String barcode) {
		return this.instDao.findInstrumByBarCode(barcode);
	}

	@Override
	public List<Instrument> getAll(Company company, Subdiv subdiv, Contact contact, ScheduledQuoteRequestType type,
			ScheduledQuotationRequestDateRange dateRange, boolean activeOnly) {
		return instDao.getAll(company, subdiv, contact, type, dateRange, activeOnly);
	}

	@Override
	public List<Instrument> getByBarcode(String barcode, Company company) {
		return this.instDao.getByBarcode(barcode, company);
	}

	@Override
	public ResultWrapper findInstrumAsStandard(int plantid) {
		Instrument i = this.findEagerInstrum(plantid);
		ResultWrapper resWrap = new ResultWrapper();
		Locale locale = LocaleContextHolder.getLocale();
		if (i == null) {
			resWrap.setSuccess(false);

			String msg1 = this.messages.getMessage("viewprocedure.addstandards.msg1", new Object[] { plantid },
					"The barcode " + plantid + " is not recognised.", locale);

			resWrap.setMessage(msg1);
		} else if (!i.getCalibrationStandard()) {
			resWrap.setSuccess(false);

			String msg2 = this.messages.getMessage("viewprocedure.addstandards.msg2", new Object[] { plantid },
					"The instrumentttt with barcode " + plantid + " is not listed as a Calibration Standard.", locale);
			resWrap.setMessage(msg2);
		} else {
			resWrap.setSuccess(true);
			resWrap.setResults(i);
		}

		return resWrap;
	}

	@Override
	public Instrument findIntSafeInstrument(String plantid) {
		if (plantid == null) {
			return null;
		} else if (!NumberTools.isAnInteger(plantid.trim())) {
			return null;
		} else {
			return this.findEagerModelInstrum(Integer.parseInt(plantid.trim()));
		}
	}

	@Override
	public Instrument findWebSafeInstrum(int id) {
		return this.get(id);
	}

	@Override
	public List<Instrument> getAllContactInstrums(int personid) {
		return this.instDao.getAllContactInstrums(personid);
	}

	@Override
	public List<Instrument> getAllInstrumentsOutOfCalibration(Company comp, Subdiv sub, Contact con) {
		return this.instDao.getAllInstrumentsOutOfCalibration(comp, sub, con);
	}

	@Override
	public List<Instrument> getAvailableBaseUnits(int coid, int modelid) {
		return this.instDao.getAvailableBaseUnits(coid, modelid);
	}

	@Override
	public List<Instrument> getAvailableModules(int coid, int modelid) {
		return this.instDao.getAvailableModules(coid, modelid);
	}

	@Override
	public List<Integer> getBarcodesOnJob(int jobId) {
		return this.instDao.getBarcodesOnJob(jobId);
	}

	@Override
	public int getCompanyInstrumentCount(int coid) {
		return this.instDao.getCompanyInstrumentCount(coid);
	}

	@Override
	public PagedResultSet<Instrument> getCompanyInstrumentsFromModelIds(PagedResultSet<Instrument> prs, Integer compId,
			List<Integer> modelIds) {
		List<Instrument> insts = this.instDao.getCompanyInstrumentsFromModelIds(compId, modelIds);
		// get count of results
		prs.setResultsCount(insts.size());
		// update the query so it will return the correct number of rows
		// from the correct start point
		if ((prs.getResultsPerPage() + prs.getStartResultsFrom()) >= insts.size()) {
			insts = insts.subList(prs.getStartResultsFrom(), (insts.size()));
		} else {
			insts = insts.subList(prs.getStartResultsFrom(), (prs.getStartResultsFrom() + prs.getResultsPerPage()));
		}
		// get results
		prs.setResults(insts);
		return prs;
	}

	@Override
	public PagedResultSet<InstrumentModel> getCompanyDistinctModelsFromModelIds(PagedResultSet<InstrumentModel> prs,
			Integer compId, List<Integer> modelIds) {
		List<Instrument> insts = this.instDao.getCompanyInstrumentsFromModelIds(compId, modelIds);
		List<InstrumentModel> models = new ArrayList<>();
		// get the distinct models from this list
		for (Instrument i : insts) {
			if (!models.contains(i.getModel())) {
				models.add(i.getModel());
			}
		}
		// get count of results
		prs.setResultsCount(models.size());
		// update the query so it will return the correct number of rows
		// from the correct start point
		if ((prs.getResultsPerPage() + prs.getStartResultsFrom()) >= models.size()) {
			models = models.subList(prs.getStartResultsFrom(), (models.size()));
		} else {
			models = models.subList(prs.getStartResultsFrom(), (prs.getStartResultsFrom() + prs.getResultsPerPage()));
		}
		// get results
		prs.setResults(models);
		return prs;
	}

	@Override
	public EditInstrumentForm getEditInstrumentForm(Integer id) {
		return instDao.getEditInstrumentForm(id);
	}

	@Override
	public List<Calibration> getHistory(Instrument instrument) {
		return this.instDao.getHistory(instrument);
	}

	public List<Instrument> getInstruments(List<Integer> plantIds) {
		return this.instDao.getInstruments(plantIds);
	}

	@Override
	public List<InstrumentProjectionDTO> getInstrumentProjectionDTOs(Locale locale, Collection<Integer> plantIds) {
		List<InstrumentProjectionDTO> result = Collections.emptyList();
		if (!plantIds.isEmpty()) {
			result = this.instDao.getInstrumentProjectionDTOs(locale, plantIds);
		}
		return result;
	}

	public List<Instrument> getInstStandardsWithNoRecallDate() {
		return this.instDao.getInstStandardsWithNoRecallDate();
	}

	public List<Instrument> getInstsWithMismatchingOwnerAndAddress() {
		return this.instDao.getInstsWithMismatchingOwnerAndAddress();
	}

	@Override
	public InstrumentPlantillasDTO getPlantillasInstrument(int plantId) {
		return this.instDao.getPlantillasInstrument(plantId);
	}

	@Override
	public PagedResultSet<InstrumentPlantillasDTO> getPlantillasInstruments(int allocatedCompanyId,
			Date afterLastModified, Integer addrid, String plantno, int businessSubdivId, int resultsPerPage, int currentPage) {
		return this.instDao.getPlantillasInstruments(allocatedCompanyId, afterLastModified, addrid, plantno,
				businessSubdivId, resultsPerPage, currentPage);
	}

	@Override
	public List<InstrumentCheckDuplicatesDto> getSimilarIns(int coid, String plantno, String serialno, Integer modelId,
			Boolean mfrReqForModel, Boolean searchModelMatches) {
		return Collections.emptyList();
	}

	@Override
	public boolean isInstrumentOnJob(int instrumId, int jobId) {
		return this.instDao.instrumOnJob(instrumId, jobId);
	}

	@Override
	public boolean isInstrumentOwnedByCompany(int instrumId, int coid) {
		Instrument inst = this.instDao.find(instrumId);
		if (inst == null) {
			return false;
		} else {
			return inst.getComp().getCoid() == coid;
		}
	}

	@Override
	public boolean isInstrumentOnCurrentJob(int plantid) {
		// get active job items for instrument
		JobItem ji = this.jobItemService.findActiveJobItemFromBarcode(plantid);
		// active job items returned?
		return (ji != null);
	}

	@Override
	public void moveToSubdivisionReferences(Integer oldContactId, Integer newAddressId, Contact userContact) {
		logger.info("Moving equipment for contact " + oldContactId + " to new address " + newAddressId);
		// Get list of distinct address ids used by contact's equipment (need
		// for change record)
		List<Integer> oldAddressIds = this.addrServ.getAddressIdsForContactInstruments(oldContactId);
		Map<Integer, List<Integer>> mapInstrumentIdsByAddress = new HashMap<>();
		// Get lists of instrument plant ids used by contact's equipment for
		// each address
		for (Integer oldAddressId : oldAddressIds) {
			List<Integer> instrumentIds = this.instDao.getInstrumentIds(oldContactId, oldAddressId);
			mapInstrumentIdsByAddress.put(oldAddressId, instrumentIds);
		}
		// Check if moving company
		Contact oldContact = this.conServ.get(oldContactId);
		Address newAddress = this.addrServ.get(newAddressId);
		Company oldCompany = null;
		Company newCompany = null;
		if (newAddress.getSub().getComp().getCoid() != oldContact.getSub().getComp().getCoid()) {
			oldCompany = oldContact.getSub().getComp();
			newCompany = newAddress.getSub().getComp();
		}

		// Perform update
		int updateCount = this.instDao.batchUpdateOwnership(oldContact, null, newAddress, newCompany);
		logger.info("Updated " + updateCount + " instruments");

		// Insert change records
		for (Integer oldAddressId : oldAddressIds) {
			Address oldAddress = this.addrServ.getReference(oldAddressId);
			List<Integer> instrumentIds = mapInstrumentIdsByAddress.get(oldAddressId);
			for (Integer plantId : instrumentIds) {
				Instrument instrument = getReference(plantId);
				InstrumentHistory contactHistory = this.instrumentHistoryService
						.prepareNewAddressChangeItem(userContact, oldAddress, newAddress, instrument);
				if (newCompany != null) {
					contactHistory.setCompanyUpdated(true);
					contactHistory.setOldCompany(oldCompany);
					contactHistory.setNewCompany(newCompany);
				}
				this.instrumentHistoryService.insertInstrumentHistory(contactHistory);
			}
			logger.info(
					"Inserted " + instrumentIds.size() + " instrument history records for address id " + oldAddressId);
		}
	}

	@Override
	public List<Instrument> queryForRecall(Company company, LocalDate dateTo) {
		return this.instDao.queryForRecall(company, dateTo);
	}

	@Override
	public List<Integer> queryForRecallWithActiveJobItems(Company company, LocalDate dateTo) {
		return this.instDao.queryForRecallWithActiveJobItems(company, dateTo);
	}

	@Override
	public QuickAddItemsValidDTO isQuickAddItemsValid(int subdivid, int plantid, int coid, int jobid, Locale locale) {

		QuickAddItemsValidDTO dto = new QuickAddItemsValidDTO();
		Instrument inst = this.get(plantid);
		Job job = jobService.get(jobid);
		ServiceType st = job.getDefaultCalType().getServiceType();
		ServiceType defaultServiceTypeFromInstrument = inst.getDefaultServiceType();

		// add values to dto
		dto.setPlantid(inst.getPlantid());
		dto.setPlantNo(inst.getPlantno());
		dto.setSerialNo(inst.getSerialno());
		dto.setInstModel(InstModelTools.modelNameViaTranslations(inst.getModel(), locale,
				supportedLocaleService.getPrimaryLocale()));
		dto.setInstModelId(inst.getModel().getModelid());
		// add the default work instruction from the instrument
		if(!inst.getWorkInstructions().isEmpty()){
			dto.setDefaultWorkInstructionId(inst.getWorkInstructions().get(0).getId());
			dto.setDefaultWorkInstructionTitle(inst.getWorkInstructions().get(0).getTitle());
		}
		else {
			dto.setDefaultWorkInstructionId(0);
			dto.setDefaultWorkInstructionTitle("");
		}

		if (defaultServiceTypeFromInstrument != null) {
			Boolean isCompatible = calibrationTypeService
					.isCompatibleWithJobType(defaultServiceTypeFromInstrument.getCalibrationType(), job.getType());
			dto.setIsInstrumentCalTypeCompatibleWithJobType(isCompatible);
			if (!isCompatible) {
                dto.setServiceTypeSelected(st.getServiceTypeId());
            } else
                st = defaultServiceTypeFromInstrument;
            dto.setServiceTypeSelected(st.getServiceTypeId());
        } else {
            dto.setServiceTypeSelected(st.getServiceTypeId());
            dto.setIsInstrumentCalTypeCompatibleWithJobType(false);
        }

        // Lookup the default procedure using capabilityServiceType filters
        val proc = this.capabilityService.getActiveCapabilityUsingCapabilityFilter
            (inst.getModel().getDescription().getId(), dto.getServiceTypeSelected(), job.getOrganisation().getSubdivid());
        proc.ifPresent(p ->
        {
            dto.setProcId(p.getId());
            dto.setProcReference(p.getReference());
        });

        return dto;
    }

	@Override
	public ResultWrapper removeModule(int plantid) {
		Instrument i = this.get(plantid);
		if (i == null) {
			return new ResultWrapper(false, "Could not find instrument with barcode '" + plantid + "'", null, null);
		} else {
			i.setBaseUnit(null);
			this.merge(i);
			return new ResultWrapper(true, "", null, null);
		}
	}

	@Override
	public List<Instrument> importInstruments(List<ImportedInstrumentsSynthesisRowDTO> dtos, Integer subdivid,
			Integer defaultAddressid, Integer defaultContactid, Integer defaultServiceTypeId,
			Integer currentContactid) {

		Subdiv subdiv = subdivService.get(subdivid);
		Company company = subdiv.getComp();
		Address defaultAddress = addrServ.get(defaultAddressid);
		Contact defaultContact = contactService.get(defaultContactid);
		ServiceType defaultServiceType = serviceTypeService.get(defaultServiceTypeId);
		Contact currentContact = contactService.get(currentContactid);

		List<Instrument> newInstruments = new ArrayList<>();

		for (ImportedInstrumentsSynthesisRowDTO row : dtos) {
			Instrument instrument = new Instrument();

			instrument.setComp(company);
			instrument.setAddedBy(currentContact);
			instrument.setAddedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			instrument.setCalFrequency(row.getCalFrequency());
			instrument.setCalFrequencyUnit(row.getIntervalUnit());
			instrument.setCustomerDescription(row.getDescription());
			instrument.setModelname(row.getModel());
			instrument.setNextCalDueDate(row.getNextCalDueDate());
			instrument.setPlantno(row.getPlantno());
			instrument.setSerialno(row.getSerialno());
			

			if (row.getAddressId() != null)
				instrument.setAdd(addrServ.get(row.getAddressId()));
			else
				instrument.setAdd(defaultAddress);

			if (row.getContactId() != null)
				instrument.setCon(contactService.get(row.getContactId()));
			else
				instrument.setCon(defaultContact);

			if (row.getTmlInstrumentModelId() != null)
				instrument.setModel(modelServ.getTMLInstrumentModel(row.getTmlInstrumentModelId()));
			else
				instrument.setModel(modelServ.get(row.getInstrumentModelId()));

			if (row.getDefaultServiceTypeId() != null)
				instrument.setDefaultServiceType(serviceTypeService.get(row.getDefaultServiceTypeId()));
			else
				instrument.setDefaultServiceType(defaultServiceType);

			if (row.getDefaultProcedureId() != null)
                instrument.setCapability(capabilityService.get(row.getDefaultProcedureId()));

			if (instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
				instrument.setMfr(mfrServ.findMfrByName(defaultGenericManufacturerName));

				Mfr mfr = null;
				if (row.getTmlBrandId() != null) {
					mfr = mfrServ.findTMLMfr(row.getTmlBrandId());
				} else if (StringUtils.isNoneBlank(row.getBrand())) {
					mfr = mfrServ.findMfrByName(row.getBrand());
				}
				if (mfr != null)
					instrument.setMfr(mfr);

			} else
				instrument.setMfr(instrument.getModel().getMfr());

			if (row.getClientRef() != null)
				instrument.setDefaultClientRef(row.getClientRef());

			if (row.getInstrumentUsageTypeId() != null) {
				InstrumentUsageType usageType =  instrumentUsageTypeService.findInstrumentUsageType(row.getInstrumentUsageTypeId());
				instrument.setUsageType(usageType);
				instrument.setStatus(usageType.getInstrumentStatus());
			}
			else
				instrument.setStatus(InstrumentStatus.IN_CIRCULATION);

			newInstruments.add(instrument);
		}

		company.getInstrums().addAll(newInstruments);

		companyService.update(company);

		return newInstruments;
	}

    @Override
    public PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> searchInstrumentsForNewJobItem(NewJobItemSearchInstrumentForm form) {
        val prs = instDao.searchInstrumentsForNewJobItem(form);
		unsafeUpdateAvailableQuotations(form.getJobId(), prs);
		return prs;
    }

    @Override
    public Optional<InstrumentTooltipDto> loadInstrumentForTooltip(Integer plantId) {
		return instDao.loadInstrumentForTooltip(plantId);
    }

    @Override
    public PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> findInstrumentsFromJobQuotes(NewJobItemSearchByLinkedQuotationForm form) {
	    val prs  = instDao.findInstrumentsFromJobQuotes(form);
		unsafeUpdateAvailableQuotations(form.getJobId(), prs);
		return prs;
    }

    @Override
    public PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> searchCompanyInstrumentsFromModelIds(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds) {
        val prs = instDao.findPagedCompanyInstrumentsFromModelIds(form,modelIds);
		unsafeUpdateAvailableQuotations(form.getJobId(), prs);
		return prs;
    }

    @Override
    public List<InstrumentCheckDuplicatesDto> lookupForInstrumentsInGroup(InstrumentInGroupLookUpForm form) {
        return instDao.lookupForInstrumentsInGroup(form);
    }

    private void unsafeUpdateAvailableQuotations(Integer jobId , PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> prs) {
		val plantIds = prs.getResults().stream().map(InstrumentSearchedForNewJobItemSearchDto::getPlantId).collect(Collectors.toList());
		val options = quotationItemService.findQuotationItemsOptionsForPlantIdsJobIdAndDefaultUsage(plantIds, jobId,sessionUtilsService.getCurrentContact().getUserPreferences().getDefaultServiceTypeUsage());
		prs.getResults().forEach(i -> i.setAvailableQuotations(options.get(i.getPlantId())));
	}

	@Override
	public Instrument saveOrUpdate(EditInstrumentForm form, Contact currentContact) {
        Instrument instrument;
        Address address = addrServ.get(form.getAddrid());
        Company company = companyService.get(form.getCoid());
        Contact contact = conServ.get(form.getPersonid());
        ServiceType defaultServiceType = form.getDefaultServiceTypeId() == null ? null
            : this.serviceTypeService.get(form.getDefaultServiceTypeId());
        InstrumentStatus status = form.getStatusid() == null ? null : InstrumentStatus.values()[form.getStatusid()];
        Location location = form.getLocid() == null ? null : this.locationService.get(form.getLocid());
        InstrumentModel model = this.modelServ.findInstrumentModel(form.getModelid());
        Capability capability = form.getProcid() == null ? null : procServ.get(form.getProcid());
        InstrumentStorageType storageType = form.getStorageTypeId() == null ? null
            : instStorageServ.findInstrumentStorageType(form.getStorageTypeId());
        InstrumentUsageType usageType = form.getUsageTypeId() == null ? null
            : instUsageServ.findInstrumentUsageType(form.getUsageTypeId());
        WorkInstruction workInstruction = form.getWiid() == null ? null : wiServ.get(form.getWiid());
        Thread thread = form.getThreadid() == null ? null : this.threadServ.get(form.getThreadid());
        IntervalUnit calFrequencyUnit = IntervalUnit.valueOf(form.getCalFrequencyUnitId());
        if (form.getPlantId() == null || form.getPlantId() == 0) {
            // this is a new instrument so record the date added + who by
            instrument = new Instrument();
            instrument.setAddedBy(currentContact);
            instrument.setAddedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		} else {
			// get existing instrument
			instrument = get(form.getPlantId());
			// create InstrumentHistory
			InstrumentHistory history = new InstrumentHistory();
			history.setOldAddress(instrument.getAdd());
			history.setNewAddress(address);
			history.setOldCompany(instrument.getComp());
			history.setNewCompany(company);
			history.setOldContact(instrument.getCon());
			history.setNewContact(contact);
			history.setOldPlantNo(instrument.getPlantno());
			history.setNewPlantNo(form.getPlantNo());
			history.setOldSerialNo(instrument.getSerialno());
			history.setNewSerialNo(form.getSerialNo());
			history.setOldStatus(instrument.getStatus());
			history.setNewStatus(status);
			history.updateChangedStatuss();
			if (history.isStatusUpdated()) {
				if (status != null && status.equals(InstrumentStatus.BER)) {
					// instrument status has been changed to B.E.R.
					// set scrapped, scrapped by, scrapped on
					instrument.setScrapped(true);
					instrument.setScrappedBy(currentContact);
					instrument.setScrappedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
					// set scrapped on this job item to job item of incomplete
					// job (unique, if exists)
					instrument.getJobItems().stream().filter(ji -> ji.getJob().getJs().isComplete())
						.forEach(instrument::setScrappedOnJI);
				} else {
					// null all scrapped fields
					instrument.setScrapped(false);
					instrument.setScrappedBy(null);
					instrument.setScrappedOn(null);
					instrument.setScrappedOnJI(null);
				}
			}
			if (history.isChanges()) {
				history.setInstrument(instrument);
				history.setChangeBy(currentContact);
				history.setChangeDate(new Date());
				// history will be stored together with instrument
				instrument.getHistory().add(history);
			}
			// remove flexible field values if changing company
			if (history.isCompanyUpdated() && !instrument.getFlexibleFields().isEmpty()) {
				List<InstrumentValue> deletions = new ArrayList<>(instrument.getFlexibleFields());
				for (InstrumentValue value : deletions) {
					this.instrumentValueService.delete(value);
				}
				instrument.getFlexibleFields().clear();
			}
		}

		// update instrument
		instrument.setAdd(address);
		instrument.setCalFrequency(form.getCalFrequency());
		instrument.setCalFrequencyUnit(calFrequencyUnit);
		instrument.setPermittedNumberOfUses(form.getPermittedNumberOfUses());
		instrument.setCalExpiresAfterNumberOfUses(form.getCalExpiresAfterNumberOfUses());
		instrument.setUsesSinceLastCalibration(form.getUsesSinceLastCalibration());
		instrument.setCalibrationStandard(form.getCalibrationStandard());
		instrument.setClientBarcode(form.getClientBarcode());
		instrument.setComp(company);
		instrument.setCon(contact);
		instrument.setCustomerDescription(form.getCustomerDescription());
		instrument.setCustomerSpecification(form.getCustomerSpecification());
        instrument.setFirmwareAccessCode(form.getFirmwareAccessCode());
        instrument.setFreeTextLocation(form.getFreeTextLocation());
        instrument.setLoc(location);
        instrument.setMfr(model != null && model.getModelMfrType() == ModelMfrType.MFR_GENERIC
            ? form.getMfrid() == null ? mfrServ.findMfrByName(defaultGenericManufacturerName)
            : mfrServ.findMfr(form.getMfrid())
            : null);
        instrument.setModel(model);
        instrument.setModelname(form.getModelName());
        instrument.setNextCalDueDate(form.getNextCalDueDate());
        instrument.setPlantno(form.getPlantNo());
        instrument.setCapability(capability);
        instrument.setSerialno(form.getSerialNo());
        instrument.setStatus(status);
        instrument.setStorageType(storageType);
        instrument.setUsageType(usageType);
        instrument.setThread(thread);
        instrument.setDefaultClientRef(form.getDefaultClientRef());
        instrument.setCustomerManaged(form.getCustomerManaged());
        instrument.setDefaultServiceType(defaultServiceType);
        // update characteristics
        if (form.getCharacteristicMap() != null)
            for (Integer charId : form.getCharacteristicMap().keySet()) {
				InstrumentCharacteristic c = form.getCharacteristicMap().get(charId);
				if (c.getUomid() != null)
					c.setUom(uomServ.findUoM(c.getUomid()));
				if (c.getLibraryValueId() != null)
					c.setLibraryValue(libraryService.get(c.getLibraryValueId()));
				c.setInstrument(instrument);
				this.instrumentCharacteristicService.save(c);
			}
		// update range/size
		String start = form.getInstStartRange();
		String end = form.getInstEndRange();
		if(!StringUtils.isEmpty(start) && !StringUtils.isEmpty(start)
				&& form.getInstUomId() != null) {
			InstrumentRange ir = new InstrumentRange();
			if (NumberUtils.isNumber(start) && NumberUtils.isNumber(end)) {
				ir.setStart(Double.parseDouble(start));
				ir.setEnd(Double.parseDouble(end));
			}
			ir.setUom(this.uomServ.findUoM(form.getInstUomId()));
			ir.setInstrument(instrument);
			if (instrument.getRanges() == null) {
				instrument.setRanges(new TreeSet<>(new RangeComparator()));
			} else {
				instrument.getRanges().clear();
			}
			instrument.getRanges().add(ir);
		}
		
		// hire instrument required?
		if (form.isHireInstrumentReq()) {
			if (instrument.getHireInstruments() == null) {
				instrument.setHireInstruments(new HashSet<>());
			}
			if (instrument.getHireInstruments().isEmpty()) {
				HireInstrument hi = new HireInstrument();
				hi.setInst(instrument);
				instrument.getHireInstruments().add(hi);
			}
		} else if (instrument.getHireInstruments() != null) {
			// Existing HireInstrument removed by cascade (if no HireItem,
			// HireAccessory)
			// (TODO - inactivate HireInstrument by business company, to retain
			// HireItem information
			instrument.getHireInstruments().clear();
		}

		if (form.getUpdateOnBehalf()) {
			JobItem jobItem = instrument.getJobItems().stream().filter(ji -> ji.getState().getActive()).findAny()
					.orElse(null);
			if (jobItem != null) {
				OnBehalfItem onBehalfItem = jobItem.getOnBehalf();
				if (jobItem.getJob().getCon().getSub().getComp().equals(company)) {
					// new instrument owner company coincides with company of
					// job -> delete existing onBehalfItem
					if (onBehalfItem != null)
						onBehalfItemService.delete(onBehalfItem);
				} else {
					// create or overwrite onBehalfItem
					if (onBehalfItem == null) {
						onBehalfItem = new OnBehalfItem();
						onBehalfItem.setJobitem(jobItem);
					}
					onBehalfItem.setCompany(company);
					onBehalfItem.setAddress(address);
					onBehalfItem.setSetOn(new Date());
					onBehalfItem.setSetBy(currentContact);
					onBehalfItemService.merge(onBehalfItem);
				}
			}
		}
		this.save(instrument);
		this.extensionService.refreshExtensionEndDate(instrument);
		// before add the work instruction selected, we should first clear 
		// all work instructions exist for this instrument
		if (instrument.getWorkInstructions() != null) {
			instrument.getWorkInstructions().forEach(wi -> {
				wi.getInstruments().remove(instrument);
				this.wiServ.save(wi);
			});
			instrument.getWorkInstructions().clear();
		}
		// add the default work instruction selected to the instrument
		if (workInstruction != null) {
			if (workInstruction.getInstruments() == null) {
				workInstruction.setInstruments(new ArrayList<>());
			}
			if (!workInstruction.getInstruments().contains(instrument)) {
				workInstruction.getInstruments().add(instrument);
			}
			this.wiServ.save(workInstruction);
		}
		
		return instrument;
	}

	@Override
	public void saveOrUpdateAll(Collection<Instrument> instrums) {
		instrums.forEach(this::merge);
	}

	@Override
	public ResultWrapper scrapInstrument(Integer plantid, Integer jiid, HttpSession session) {
		// get the current contact
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.getEagerLoad(username).getCon();
		return scrapInstrumentByContact(plantid, jiid, contact);
	}

	@Override
	public ResultWrapper scrapInstrumentByContact(Integer plantid, Integer jiid, Contact contact) {
		// first find instrument
		Instrument inst = this.instDao.find(plantid);
		// not null?
		if (inst != null) {

			// create new instrument history object
			InstrumentHistory ih = new InstrumentHistory();
			// set old status
			ih.setOldStatus(inst.getStatus());
			// get B.E.R. status
			InstrumentStatus berStatus = InstrumentStatus.BER;
			// set instrument status to B.E.R.
			inst.setStatus(berStatus);
			// set new status, status updated, changed by, changed date and
			// instrument in history
			ih.setNewStatus(berStatus);
			ih.setStatusUpdated(true);
			ih.setChangeBy(contact);
			ih.setChangeDate(new Date());
			ih.setInstrument(inst);
			// save the instrument history
			this.instrumentHistoryService.saveOrUpdateInstrumentHistory(ih);
			// set scrapped, scrapped by and scrapped on fields
			inst.setScrapped(true);
			inst.setScrappedBy(contact);
			inst.setScrappedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			// set job item object to null
			JobItem jobItem = null;
			// has job item id been passed to method?
			if (jiid != null) {
				// now look for the job item using id
				jobItem = this.jobItemService.findJobItem(jiid);
			}
			// not null?
			if (jobItem != null) {
				// set scrapped on this job item
				inst.setScrappedOnJI(jobItem);
			} else {
				// look to see if this instrument is on any active job items
				for (JobItem ji : inst.getJobItems()) {
					// job status not complete?
					if (!ji.getJob().getJs().isComplete()) {
						// set scrapped on this job item to this job item
						inst.setScrappedOnJI(ji);
						// exit loop
						break;
					}
				}
			}
			// return successful wrapper
			return new ResultWrapper(true, "", inst, null);
		} else {
			// return un-successful wrapper with message
			return new ResultWrapper(false, "The instrument could not be found", null, null);
		}
	}

	@Override
	public List<InstrumentAssetSearchResultWrapper> searchBusinessInstrumentsForAssetsHQL(int coid, boolean standards,
			String mfr, String model, String desc, String plantid, String plantno, String serialno) {
		return this.instDao.searchBusinessInstrumentsForAssetsHQL(coid, standards, mfr, model, desc, plantid, plantno,
				serialno);
	}

	@Override
	public List<InstrumentSearchResultsDTO> searchInstruments(Company company, String plantNo, String serialNo,
			Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv,
			Address address, Boolean outOfCal, InstrumentModelFamily family, String modelText,
			String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form) {
		List<InstrumentSearchResultsDTO> instruments = instDao.searchInstruments(company, plantNo, serialNo, desc,
				instModel, mfr, instrumentOwner, subdiv, address, outOfCal, family, modelText, customerDescription,
				plantId, calStandard, form);
		Locale locale = LocaleContextHolder.getLocale();
		// add flexible fields
		for (InstrumentFieldDefinition field : company.getInstrumentFieldDefinition()) {
			switch (field.getFieldType()) {
			case BOOLEAN:
				List<InstrumentValueBooleanDTO> boolValList = instrumentValueBooleanDao.getAll(field);
				SortedMap<Integer, InstrumentValueBooleanDTO> boolValMap = new TreeMap<>();
				for (InstrumentValueBooleanDTO value : boolValList)
					boolValMap.put(value.getPlantId(), value);
				for (InstrumentSearchResultsDTO instr : instruments) {
					InstrumentValueBooleanDTO value = boolValMap.get(instr.getPlantId());
					instr.getFlexibleFields().put(field.getName(),
							value == null || value.getValue() == null ? ""
									: value.getValue() ? messages.getMessage("yes", null, "yes", locale)
											: messages.getMessage("no", null, "no", locale));
				}
				break;
			case DATETIME:
				List<InstrumentValueDateTimeDTO> dateValList = instrumentValueDateTimeDao.getAll(field);
				SortedMap<Integer, InstrumentValueDateTimeDTO> dateValMap = new TreeMap<>();
				for (InstrumentValueDateTimeDTO value : dateValList)
					dateValMap.put(value.getPlantId(), value);
				for (InstrumentSearchResultsDTO instr : instruments) {
					InstrumentValueDateTimeDTO value = dateValMap.get(instr.getPlantId());
					instr.getFlexibleFields().put(field.getName(), value == null || value.getValue() == null ? ""
							: (new SimpleDateFormat("dd.MM.yyyy")).format(value.getValue()));
				}
				break;
			case NUMERIC:
				List<InstrumentValueNumericDTO> numericValList = instrumentValueNumericDao.getAll(field);
				SortedMap<Integer, InstrumentValueNumericDTO> numericValMap = new TreeMap<>();
				for (InstrumentValueNumericDTO value : numericValList)
					numericValMap.put(value.getPlantId(), value);
				for (InstrumentSearchResultsDTO instr : instruments) {
					InstrumentValueNumericDTO value = numericValMap.get(instr.getPlantId());
					instr.getFlexibleFields().put(field.getName(),
							value == null || value.getValue() == null ? "" : value.getValue().toString());
				}
				break;
			case SELECTION:
				List<InstrumentValueSelectionDTO> selectionValList = instrumentValueSelectionDao.getAll(field);
				SortedMap<Integer, InstrumentValueSelectionDTO> selectionValMap = new TreeMap<>();
				for (InstrumentValueSelectionDTO value : selectionValList)
					selectionValMap.put(value.getPlantId(), value);
				for (InstrumentSearchResultsDTO instr : instruments) {
					InstrumentValueSelectionDTO value = selectionValMap.get(instr.getPlantId());
					instr.getFlexibleFields().put(field.getName(),
							value == null || value.getValue() == null ? "" : value.getValue());
				}
				break;
			case STRING:
				List<InstrumentValueStringDTO> stringValList = instrumentValueStringDao.getAll(field);
				SortedMap<Integer, InstrumentValueStringDTO> stringValMap = new TreeMap<>();
				for (InstrumentValueStringDTO value : stringValList)
					stringValMap.put(value.getPlantId(), value);
				for (InstrumentSearchResultsDTO instr : instruments) {
					InstrumentValueStringDTO value = stringValMap.get(instr.getPlantId());
					instr.getFlexibleFields().put(field.getName(),
							value == null || value.getValue() == null ? "" : value.getValue());
				}
				break;
			default:
				break;
			}
		}
		return instruments;
	}

	@Override
	public PagedResultSet<InstrumentSearchResultsDTO> searchInstrumentsPaged(
			PagedResultSet<InstrumentSearchResultsDTO> pagedResultSet, Company company, String plantNo, String serialNo,
			Description desc, InstrumentModel instModel, Mfr mfr, Contact instrumentOwner, Subdiv subdiv,
			Address address, Boolean outOfCal, InstrumentModelFamily family, String modelText,
			String customerDescription, Integer plantId, Boolean calStandard, WebSearchInstrumentForm form) {
		return instDao.searchInstrumentsPaged(pagedResultSet, company, plantNo, serialNo, desc, instModel, mfr,
				instrumentOwner, subdiv, address, outOfCal, family, modelText, customerDescription, plantId,
				calStandard, form);
	}

	@Override
	public PagedResultSet<InstrumentSearchResultsDTO> searchInstrumentsPaged(
			PagedResultSet<InstrumentSearchResultsDTO> pagedResultSet, Company comp, WebSearchInstrumentForm form) {

		Contact instrumentOwner = null;
		Subdiv subdiv = null;
		Address address = null;
		Description desc = null;
		Mfr mfr = null;
		Integer plantId = null;
		InstrumentModel instModel = null;

		if (form.getPersonid() != null && form.getPersonid() > 0) {
			instrumentOwner = this.conServ.get(form.getPersonid());
		}

		if (form.getSubdivid() != null && form.getSubdivid() > 0) {
			subdiv = this.subdivService.get(form.getSubdivid());
		}

		if (form.getAddressid() != null && form.getAddressid() > 0) {
			address = this.addrServ.get(form.getAddressid());
		}

		if (form.getDescid() != null && form.getDescid() > 0) {
			desc = this.descService.findDescription(form.getDescid());
		}

		if (form.getMfrid() != null && form.getMfrid() > 0) {
			mfr = this.mfrServ.findMfr(form.getMfrid());
		}

		if (form.getPlantid() != null && StringUtils.isNoneBlank(form.getPlantid())) {
			plantId = Integer.valueOf(form.getPlantid());
		}
		if (form.getModelid() != null && form.getModelid() > 0) {
			instModel = modelServ.get(form.getModelid());
		}

		return this.searchInstrumentsPaged(pagedResultSet, comp, form.getPlantno(), form.getSerialno(), desc, instModel,
				mfr, instrumentOwner, subdiv, address, form.getOutOfCalibration(), null, form.getModel(),
				form.getCustomerDescription(), plantId, null, form);
	}

	@Override
	public PagedResultSet<InstrumentSearchResultsDTO> searchInstrumentsPaged(
			PagedResultSet<InstrumentSearchResultsDTO> pagedResultSet, SearchInstrumentForm form) {
		Contact instrumentOwner = null;
		Subdiv subdiv = null;
		Address address = null;
		Description desc = null;
		Mfr mfr = null;
		Company comp = null;
		InstrumentModelFamily family = null;
		Integer plantID = null;
		InstrumentModel instModel = null;

		if (form.getPersonid() != null && form.getPersonid() > 0) {
			instrumentOwner = this.conServ.get(form.getPersonid());
		}

		if (form.getSubdivid() != null && form.getSubdivid() > 0) {
			subdiv = this.subdivService.get(form.getSubdivid());
		}

		if (form.getAddressid() != null && form.getAddressid() > 0) {
			address = this.addrServ.get(form.getAddressid());
		}

		if (form.getDescid() != null && form.getDescid() > 0) {
			desc = this.descService.findDescription(form.getDescid());
		}

		if (form.getMfrid() != null && form.getMfrid() > 0) {
			mfr = this.mfrServ.findMfr(form.getMfrid());
		}

		if (form.getCoid() != null && form.getCoid() > 0) {
			comp = this.companyService.get(form.getCoid());
		}

		if (form.getFamilyid() != null && form.getFamilyid() > 0) {
			family = this.familyService.getFamily(form.getFamilyid());
		}

		if (form.getPlantid() != null && !form.getPlantid().trim().isEmpty()) {
			plantID = Integer.valueOf(form.getPlantid().trim());
		}

		if (form.getModelid() != null) {
			instModel = modelServ.get(form.getModelid());
		}
		return this.searchInstrumentsPaged(pagedResultSet, comp,
				form.getPlantno() == null ? null : form.getPlantno().trim(),
				form.getSerialno() == null ? null : form.getSerialno().trim(), desc, instModel, mfr, instrumentOwner,
				subdiv, address, form.getOutOfCalibration(), family, form.getModel(), form.getCustomerDescription(),
				plantID, form.getStandard(), null);
	}

	@Override
	public String getCorrectInstrumentModelName(Instrument instrument) {
		String modelName = translationService.getCorrectTranslation(instrument.getModel().getNameTranslations(),
				LocaleContextHolder.getLocale());
		if (instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC))
			modelName = modelName + (instrument.getMfr() != null && !instrument.getMfr().getName().equals("/")
					? " " + instrument.getMfr().getName()
					: "");
		modelName = modelName + (instrument.getModelname() != null && !instrument.getModelname().equals("/")
				? " " + instrument.getModelname()
				: "");
		return modelName;
	}

	@Override
	public Date getLastCalDate(Instrument instrument) {
		return this.instDao.getLastCalDate(instrument);
	}

	public Date getLastCalDateById(Integer plantId) {
		return getLastCalDate(instDao.find(plantId));
	}

	@Override
	public PagedResultSet<Instrument> searchInstrumentsPaged(PagedResultSet<Instrument> prs,
			InstrumentAndModelSearchForm<Instrument> form, Integer subdivid, Integer personid, Integer addressid,
			Boolean outOfCal, NewQuoteInstSortType sortType, boolean ascOrder, Locale locale) {
		return this.instDao.searchInstrumentsPaged(prs, form, subdivid, personid, addressid, outOfCal, sortType,
				ascOrder, locale);
	}

	@Override
	public List<Instrument> searchPlantNo(Collection<Integer> companyIds, String plantno) {
		List<Instrument> result = Collections.emptyList();

		if (!companyIds.isEmpty()) {
			result = this.instDao.searchPlantNo(companyIds, plantno);
		}
		return result;
	}

	@Override
	public ResultWrapper unscrapInstrument(Integer plantid, HttpSession session) {
		// first find instrument
		Instrument inst = this.instDao.find(plantid);
		// not null?
		if (inst != null) {
			// get the current contact
			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact contact = this.userService.getEagerLoad(username).getCon();
			// create new instrument history object
			InstrumentHistory ih = new InstrumentHistory();
			// set old status
			ih.setOldStatus(inst.getStatus());
			// get 'In circulation' status
			InstrumentStatus incircStatus = InstrumentStatus.IN_CIRCULATION;
			// set instrument status to 'In circulation'
			inst.setStatus(incircStatus);
			// set new status, status updated, changed by, changed date and
			// instrument in history
			ih.setNewStatus(incircStatus);
			ih.setStatusUpdated(true);
			ih.setChangeBy(contact);
			ih.setChangeDate(new Date());
			ih.setInstrument(inst);
			// save the instrument history
			this.instrumentHistoryService.saveOrUpdateInstrumentHistory(ih);
			// set scrapped, scrapped by and scrapped on fields
			inst.setScrapped(false);
			inst.setScrappedBy(null);
			inst.setScrappedOn(null);
			inst.setScrappedOnJI(null);
			// return successful wrapper
			return new ResultWrapper(true, "", inst, null);
		} else {
			// return un-successful wrapper with message
			return new ResultWrapper(false, "The instrument could not be found", null, null);
		}
	}

	@Override
	public void updateInstrumentOwnerReferences(Integer oldContactId, Integer newContactId, Contact userContact) {
		logger.info("Updating instrument contact from " + oldContactId + " to " + newContactId);
		if (Objects.equals(oldContactId, newContactId))
			throw new IllegalArgumentException("Old and new contacts are the same");
		// Get list of all contact instrument IDs (at any address)
		List<Integer> instrumentIds = this.instDao.getInstrumentIds(oldContactId, null);
		Contact oldContact = this.conServ.getReference(oldContactId);
		Contact newContact = this.conServ.getReference(newContactId);
		Company oldCompany = null;
		Company newCompany = null;
		if (newContact.getSub().getComp().getCoid() != oldContact.getSub().getComp().getCoid()) {
			oldCompany = oldContact.getSub().getComp();
			newCompany = newContact.getSub().getComp();
		}
		int updateCount = this.instDao.batchUpdateOwnership(oldContact, newContact, null, newCompany);
		logger.info("Updated " + updateCount + " instruments");
		for (Integer plantId : instrumentIds) {
			// TODO replace with batch inserts
			Instrument instrument = getReference(plantId);
			InstrumentHistory contactHistory = this.instrumentHistoryService.prepareNewContactChangeItem(userContact,
					oldContact, newContact, instrument);
			if (newCompany != null) {
				contactHistory.setCompanyUpdated(true);
				contactHistory.setOldCompany(oldCompany);
				contactHistory.setNewCompany(newCompany);
			}
			this.instrumentHistoryService.insertInstrumentHistory(contactHistory);
		}
		logger.info("Inserted " + instrumentIds.size() + " instrument history records");
	}

	@Override
	public void updateInstrumentRecalledNotReceived(int plantid) {
		Instrument instrument = this.get(plantid);
		if (instrument != null) {
			// get most recent job
			JobItem jItem = this.jobItemService.getMostRecentJobItemForInstrument(instrument);
			// get most recent recall
			RecallItem rItem = this.recallService.getMostRecentRecallItemForInstrument(plantid);

			// work out if the most recent recall was before or after the most
			// recent job for the instrument
			boolean recalledNotReceived = false;
			if ((jItem == null) && (rItem != null)) {
				recalledNotReceived = true;
			} else if (jItem != null && rItem != null && jItem.getDateIn().isBefore(rItem.getRecall().getDate().atStartOfDay(LocaleContextHolder.getTimeZone().toZoneId()))) {
				recalledNotReceived = true;
			}
			instrument.setRecalledNotReceived(recalledNotReceived);
			this.merge(instrument);
		}

	}

	@Override
	public ResultWrapper userCanEditInstrument(HttpServletRequest request) {
		int plantid = ServletRequestUtils.getIntParameter(request, "plantid", 0);
		Instrument inst = this.instDao.find(plantid);

		if (inst == null) {
			return new ResultWrapper(false, this.messages.getMessage("error.instrument.plantid.notfound",
					new Object[] { String.valueOf(plantid) }, "Instrument could not be found", null));
		} else {
			return this.userCanEditInstrument(inst);
		}
	}

	@Override
	public ResultWrapper userCanEditInstrument(Instrument instrument) {
		boolean proceed = false;
		String message = "";

		// this is not a business instrument so just proceed or not a standard
		if (instrument.getComp().getCompanyRole() != CompanyRole.BUSINESS || !instrument.getCalibrationStandard()) {
			proceed = true;
		} else {
			Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication()
					.getAuthorities();
			for (GrantedAuthority a : auth) {
				// if the user has either ADMIN, DIRECTOR or MANAGER rights
				// then proceed without any more checks
				if (a.getAuthority().equals(Permission.BUSINESS_INSTRUMENT_EDIT.toString())) {
					proceed = true;
					break;
				} else {
					message = "User is attempting to update a instrument standard without sufficient privileges.";
				}
			}

		}

		return new ResultWrapper(proceed, message, null, null);
	}

	@Override
	public ResultWrapper webRequestModel(String manufacturer, String modelname, String description,
			HttpServletRequest req) {
		String fromAddr = null;
		String username = (String) WebContextFactory.get().getSession()
				.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.getEagerLoad(username).getCon();
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(contact, null, null);
		if ((contact.getEmail() != null) && !contact.getEmail().equals("")) {
			fromAddr = contact.getEmail();
		}

		if (fromAddr != null) {
			String contactComp = contact.getSub().getComp().getConame();

			Locale contactLocale = contact.getLocale() != null ? contact.getLocale() : Locale.getDefault();

			String subjecttext = this.messages.getMessage("email.modelrequest.subject",
					new Object[] { contact.getName(), contactComp },
					"New Instrument Model Request from " + contact.getName() + " at " + contactComp, contactLocale);


			// create email text
			// TODO - migrate Velocity to Thymeleaf if reactivated - this
			// feature currently not used
			String htmlText = "TODO - Migrate email/web/modelrequest.vm to Thymeleaf";

			boolean success = this.emailServ.sendAdvancedEmail(subjecttext, fromAddr,
					Arrays.asList(bdetails.getWebEmail(), bdetails.getSalesEmail()), Collections.emptyList(),
					Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), htmlText,
					true, false);

			if (success) {
				return new ResultWrapper(true, "", null, null);
			} else {
				return new ResultWrapper(false, "Error occured sending email new instrument model request", null, null);
			}
		} else {
			return new ResultWrapper(false,
					"A valid email address could not be found<br />Please check your profile and add a valid email address so we can notify when your new instrument model has been added to the database",
					null, null);
		}
	}

	@Override
	public List<Instrument> webSearchInstrumentsDueForCalibration(LocalDate dateFrom, LocalDate dateTo, Integer coid,
																  Integer subdivid, Integer addrid, Integer personid) {
		return this.instDao.webSearchInstrumentsDueForCalibration(dateFrom, dateTo, coid, subdivid, addrid, personid);
	}

	@Override
	public List<InstrumentSearchResultWrapper> websearchInstrumentsHQL(int coid, String mfr, String model, String desc,
			Integer descid, String plantid, String plantno, String serialno) {

		Locale userLocale = LocaleContextHolder.getLocale();
		Locale primaryLocale = this.supportedLocaleService.getPrimaryLocale();

		return this.instDao.websearchInstruments(coid, mfr, model, desc, descid, plantid, plantno, serialno, userLocale, primaryLocale);
	}

	@Override
	public PagedResultSet<SearchInstrumentsByUserDTO> searchSortedInstrumentsByUser(WebSearchInstrumentsByUserForm form,
			PagedResultSet<SearchInstrumentsByUserDTO> rs) {
		Company company = companyService.get(form.getCoid());
		Contact contact = conServ.get(form.getPersonid());
		PagedResultSet<InstrumentSearchResultsDTO> seed = new PagedResultSet<>(rs.getResultsPerPage(),
				rs.getCurrentPage());
		PagedResultSet<InstrumentSearchResultsDTO> resultSet = this.instDao.searchInstrumentsPaged(seed, company, null,
				null, null, null, null, contact, null, null, null, null, null, null, null, null, null);
		List<SearchInstrumentsByUserDTO> userResults = new ArrayList<>();
		for (InstrumentSearchResultsDTO result : resultSet.getResults()) {
			Instrument instrument = this.findEagerInstrum(result.getPlantId());
			Certificate cert = certServ.getLatestCertificatesForInstrument(instrument);
			JobItem mostRecentJob = jobItemService.findMostRecentJobItem(result.getPlantId());
			SearchInstrumentsByUserDTO dto = new SearchInstrumentsByUserDTO();
			dto.setPlantid(result.getPlantId());
			dto.setInstrumentModelName(result.getModelName());
			dto.setSerialNo(result.getSerialNo());
			dto.setPlantNo(result.getPlantNo());
			dto.setStatus(result.getInstrumentStatus().getStatus());
			dto.setOwner(result.getOwner());
			dto.setScrapped(result.getScrapped());
			dto.setSubdivision(result.getSubdivName());
			if (result.getLastCalDate() != null) {
				dto.setLastCalDate(result.getLastCalDate());
			}
			if (cert != null) {
				dto.setLastCertId(cert.getCertid());
				dto.setLastCertificateNumber(cert.getCertno());
				dto.setCertFileAvailable(cert.getHasCertFile());
			}
			if (result.getCalDueDate() != null) {
				dto.setNextCalDue(result.getCalDueDate());
			}
			if (mostRecentJob != null) {
				dto.setJobStatus(translationService.getCorrectTranslation(mostRecentJob.getState().getTranslations(),
						LocaleContextHolder.getLocale()));
				if (mostRecentJob.getOnBehalf() != null) {
					dto.setOnBehalfOf(mostRecentJob.getOnBehalf().getCompany().getConame());
				}
			}
			userResults.add(dto);
		}
		PagedResultSet<SearchInstrumentsByUserDTO> returnResultSet = new PagedResultSet<>(0,
				resultSet.getCurrentPage());
		returnResultSet.setResultsCount(resultSet.getResultsCount());
		returnResultSet.setPageCount(resultSet.getPageCount());
		returnResultSet.setResults(userResults);
		return returnResultSet;
	}

	@Override
	public Set<InstrumentFieldDTO> getFlexibleFields(Instrument instrument) {
		Set<InstrumentFieldDefinition> fieldDefinitions = instrumentFieldDefinitionService
				.getInstrumentFieldDefinitionsForCompany(instrument.getComp());
		Set<InstrumentFieldDTO> fields = new TreeSet<>();
		for (InstrumentFieldDefinition fieldDefinition : fieldDefinitions) {
			InstrumentFieldDTO dto = new InstrumentFieldDTO();
			dto.setFieldName(fieldDefinition.getName());
			dto.setFieldDefinitionId(fieldDefinition.getInstrumentFieldDefinitionid());
			dto.setFieldType(fieldDefinition.getFieldType().getName());
			dto.setIsEditable(fieldDefinition.getIsUpdatable());

			// get value assigned to this field for this instrument if it is
			// set.
			if (instrument.getFlexibleFields() != null
					&& instrument.getFlexibleFields().stream().anyMatch(isValueForThisField(fieldDefinition))) {
				InstrumentValue instrumentValue = instrument.getFlexibleFields().stream()
						.filter(isValueForThisField(fieldDefinition)).collect(Collectors.toList()).get(0);
				dto.setValue(instrumentValueService.getValueAsText(instrumentValue));
				dto.setValueId(instrumentValue.getInstrumentFieldValueid());
			}
			fields.add(dto);
		}
		return fields;
	}

	@Override
	public Instrument merge(Instrument instrument) {
		instrument = super.merge(instrument);
		this.extensionService.refreshExtensionEndDate(instrument);
		return instrument;
	}

	private static Predicate<InstrumentValue> isValueForThisField(InstrumentFieldDefinition fieldDefinition) {
		return v -> v.getInstrumentFieldDefinition().equals(fieldDefinition);
	}

	@Override
	public void save(Instrument instrument) {
		this.instDao.persist(instrument);
	}

	@Override
	public List<PossibleInstrumentDTO> lookupPossibleInstruments(int clientCompanyId, int allocatedsubdivid, int asnid,
			Locale locale, List<Integer> trescalIds, List<String> plantnos, List<String> serialNos) {
		return instDao.lookupPossibleInstruments(clientCompanyId, allocatedsubdivid, asnid, locale, trescalIds,
				plantnos, serialNos);
	}

	@Override
	public List<ImportedInstrumentsSynthesisRowDTO> convertFileContentToInstrumentDTO(Integer exchangeFormatId,
			List<LinkedCaseInsensitiveMap<String>> fileContent, Integer defaultAddressId, Integer defaultContactId,
			Integer defaultServiceTypeId, Integer subdivid) throws ParseException {

		ExchangeFormat exchangeFormat = exchangeFormatService.get(exchangeFormatId);
		AliasGroup aliasGroup = exchangeFormat.getAliasGroup();
		SimpleDateFormat sdf = new SimpleDateFormat(exchangeFormat.getDateFormat().getValue());
		Subdiv subdiv = subdivService.get(subdivid);

		List<ImportedInstrumentsSynthesisRowDTO> myList = new ArrayList<>();
		for (LinkedCaseInsensitiveMap<String> row : fileContent) {
			ImportedInstrumentsSynthesisRowDTO dto = new ImportedInstrumentsSynthesisRowDTO();
			for (String key : row.keySet()) {
				ExchangeFormatFieldNameEnum efFieldName = exchangeFormat.getExchangeFormatFieldNameDetails().stream()
						.filter(e -> (e.getTemplateName() != null
							&& e.getTemplateName().trim().equalsIgnoreCase(key.trim()))
							|| (e.getFieldName().getValue().trim().equalsIgnoreCase(key.trim())))
						.map(ExchangeFormatFieldNameDetails::getFieldName).findFirst().orElse(null);
				if (efFieldName != null && StringUtils.isNoneBlank(row.get(key))) {
					String columnValue = row.get(key);
					switch (efFieldName) {
					case ADDRESS:
						if (StringUtils.isNotBlank(columnValue)) {
							dto.setAddress(columnValue);
							Address address = addrServ.findCompanyAddress(subdiv.getComp().getCoid(), null, true,
									columnValue);
							if (address != null)
								dto.setAddressId(address.getAddrid());
						} else {
							dto.setAddressId(defaultAddressId);
						}
						break;
					case ADDRESS_ID:
						if (StringUtils.isNotBlank(columnValue))
							dto.setAddressId(Integer.valueOf(columnValue));
						else
							dto.setAddressId(defaultAddressId);
						break;
					// set the service type id
					case SERVICETYPE_ID:
						if (StringUtils.isNotBlank(columnValue)) {
							dto.setDefaultServiceTypeId(Integer.valueOf(columnValue));
						} else {
							dto.setDefaultServiceTypeId(defaultServiceTypeId);
						}

						break;
					case CAL_FREQUENCY:
						dto.setCalFrequency(Integer.valueOf(columnValue));
						break;
					case CONTACT:
						if (StringUtils.isNotBlank(columnValue)) {
							dto.setContact(columnValue);
							Contact contact = contactService.findCompanyContactByFullName(subdiv.getComp().getCoid(),
									columnValue, true);
							if (contact != null)
								dto.setContactId(contact.getId());
						} else {
							dto.setContactId(defaultContactId);
						}
						break;
					case CONTACT_ID:
						if (StringUtils.isNotBlank(columnValue))
							dto.setContactId(Integer.valueOf(columnValue));
						else
							dto.setContactId(defaultContactId);
						break;
					case DEFAULT_PROCEDURE_REFERENCE:
						dto.setDefaultProcedureReference(columnValue);
						if (StringUtils.isNoneBlank(dto.getDefaultProcedureReference())) {
                            capabilityService
                                .getCapabilityByReference(dto.getDefaultProcedureReference(), null, null).stream()
                                .findFirst().ifPresent(procedure -> dto.setDefaultProcedureId(procedure.getId()));
                        }
						break;
						case FAMILY_ID:
							dto.setFamilyId(Integer.valueOf(columnValue));
							break;
						case INSTRUMENT_MODEL_ID:
							dto.setInstrumentModelId(Integer.valueOf(columnValue));
							break;
						case INTERVAL_UNIT:
							dto.setIntervalUnit(this.aliasGroupService.determineIntervalUnitValue(aliasGroup, columnValue));
							break;
						case NEXT_CAL_DUEDATE:
							dto.setNextCalDueDate(dateToLocalDate(sdf.parse(columnValue)));
							break;
						case SUBFAMILY_ID:
							dto.setSubFamilyId(Integer.valueOf(columnValue));
							break;
						case TML_BRAND_ID:
							dto.setTmlBrandId(Integer.valueOf(columnValue));
							break;
						case TML_FAMILY_ID:
							if (StringUtils.isNoneBlank(columnValue)) {
								dto.setTmlFamilyId(Integer.valueOf(columnValue));
							// find the instrument model family by TML_FAMILY_ID
							try {
								InstrumentModelFamily family = this.familyService
										.getTMLFamily(Integer.parseInt(columnValue));
								if (family != null) {
									dto.setFamilyId(family.getFamilyid());
									dto.setFamilyName(this.translationService.getCorrectTranslation(
											family.getTranslation(), LocaleContextHolder.getLocale()));
								}
							} catch (Exception ignored) {
							}
						}

						break;
					case TML_INSTRUMENT_MODEL_ID:
						if (StringUtils.isNoneBlank(columnValue)) {
							dto.setTmlInstrumentModelId(Integer.valueOf(columnValue));
							try {
								// try to find the instrument model by
								// TML_instrument_id
								InstrumentModel model = this.modelServ
										.getTMLInstrumentModel(Integer.parseInt(columnValue));
								if (model != null) {
									dto.setInstrumentModelId(model.getModelid());
								}
							} catch (Exception ignored) {
							}
						}
						break;
					case TML_SUBFAMILY_ID:
						if (StringUtils.isNoneBlank(columnValue)) {
							dto.setTmlSubFamilyId(Integer.valueOf(columnValue));
							// find the instrument model family by
							// TML_SUBFAMILY_ID
							try {
								Description description = this.descService
										.getTMLDescription(Integer.parseInt(columnValue));
								dto.setSubFamilyId(description.getTmlid());
								dto.setSubFamilyName(this.translationService.getCorrectTranslation(
										description.getTranslations(), LocaleContextHolder.getLocale()));
							} catch (Exception ignored) {

							}
						}

						break;
					case BRAND:
						dto.setBrand(columnValue);
						break;
					case CUSTOMER_DESCRIPTION:
						dto.setDescription(columnValue);
						break;
					case MODEL:
						dto.setModel(columnValue);
						break;
					case PLANT_NO:
						dto.setPlantno(columnValue);
						break;
					case SERIAL_NUMBER:
						dto.setSerialno(columnValue);
						break;
					case EXPECTED_SERVICE:
						dto.setServiceType(columnValue);
						ServiceType st = this.aliasGroupService.determineExpectedServiceTypeFromAliasOrValue(aliasGroup,
								columnValue);
						if (st != null)
							dto.setDefaultServiceTypeId(st.getServiceTypeId());
						break;
					case CLIENT_REF:
						dto.setClientRef(columnValue);
						break;
					case INSTRUMENT_USAGE_TYPE:
						dto.setInstrumentUsageType(columnValue);
						if (StringUtils.isNoneBlank(columnValue)) {
							InstrumentUsageType usageType = instrumentUsageTypeService.getByName(columnValue,
									LocaleContextHolder.getLocale());
							if (usageType != null)
								dto.setInstrumentUsageTypeId(usageType.getId());
						}
						break;
					default:
						break;
					}
				}
			}
			myList.add(dto);
		}
		return myList;
	}

	@Override
	public Map<Integer, Instrument> fetchInstrumentsForJobItemCreation(List<Integer> plantids) {
		return instDao.fetchInstrumentsForJobItemCreation(plantids);
	}

	@Override
	public List<Instrument> getByOwningCompany(Integer plantid, Integer coid, String plantno, String serialno) {
		return instDao.getByOwningCompany(plantid, coid, plantno, serialno);
	}

	@Override
	public ModernResultWrapper<CheckInstrumentForDuplicatesOut> checkInstrumentForDuplicates(
			AddNewInstrumentToJobDto instrument) {
		val outResult = this.checkInstrumentForDuplicatesOut(instrument);
		return outResult.isEmpty() ? ModernResultWrapper.failure("No similar instruments found")
				: ModernResultWrapper.success(outResult);

	}
	
	@Override
	public CheckInstrumentForDuplicatesOut checkInstrumentForDuplicatesOut(
			AddNewInstrumentToJobDto instrument) {
        List<InstrumentCheckDuplicatesDto> searchResults = instDao.getSimilarIns(instrument.getCoid(),
            instrument.getPlantNo(), instrument.getSerialNo(), instrument.getModelId(),
            instrument.getMfrReqForModel(), instrument.getSearchModelMatches());
        val groupName = Optional.ofNullable(instrument.getCoid()).filter(i1 -> i1 > 0).map(companyService::get)
            .map(Company::getCompanyGroup).map(CompanyGroup::getGroupName).orElse("");
        val split = searchResults.stream().filter(i -> i.getScore() > similarityThreshold)
            .collect(Collectors.partitioningBy(i -> i.getCoid().equals(instrument.getCoid())));
        return CheckInstrumentForDuplicatesOut.of(
            split.get(true).stream().limit(10).collect(Collectors.toList()),
            split.get(false).stream().limit(5).collect(Collectors.toList()),
            groupName);
    }

	@Override
	public void simplifyIds(Integer coid, boolean nullsOnly, SseEmitter emitter) {
		emitOrError(emitter, SseEmitter.event().data("start"));
		migratory.migrateInstruments(coid, nullsOnly, emitter);
		emitOrError(emitter, SseEmitter.event().data("Company processed").name("company_done"));
	}

	private void emitOrError(SseEmitter emitter, SseEmitter.SseEventBuilder event) {
		try {
			emitter.send(event);
		} catch (IOException e) {
			emitter.completeWithError(e);
		}
	}

	@Override
	public void calculateRecallDateAfterGSO(Instrument inst, GeneralServiceOperation gso, Integer duration,
			String intervalUnitId, RecallRequirementType recallRequirementType) {
		IntervalUnit intervalUnit = IntervalUnit.valueOf(intervalUnitId);
		this.calculateAndSetRecallDate(inst, dateToLocalDate(gso.getCompletedOn()), duration, intervalUnit, recallRequirementType);

		this.merge(inst);
	}

}