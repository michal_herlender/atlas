package org.trescal.cwms.core.instrument.entity.instrumentrange;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.range.Range;

@Entity
@Table(name = "instrumentrange")
public class InstrumentRange extends Range
{
	private Instrument instrument;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable=false, foreignKey=@ForeignKey(name="FK_instrumentrange_instrument"))
	public Instrument getInstrument()
	{
		return this.instrument;
	}

	public void setInstrument(Instrument instrument)
	{
		this.instrument = instrument;
	}
}