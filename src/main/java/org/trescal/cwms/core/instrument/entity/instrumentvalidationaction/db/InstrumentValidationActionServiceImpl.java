package org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationAction;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.db.InstrumentValidationIssueService;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.db.InstrumentValidationStatusService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.validation.BeanValidator;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service("InstrumentValidationActionService")
public class InstrumentValidationActionServiceImpl implements InstrumentValidationActionService {
	@Autowired
	private InstrumentValidationActionDao InstrumentValidationActionDao;
	@Autowired
	private InstrumentValidationIssueService InstrumentValidationIssueServ;
	@Autowired
	private InstrumentValidationStatusService InstrumentValidationStatusServ;
	@Autowired
	private BeanValidator validator;
	@Autowired
	private UserService userService;
	
	public void deleteInstrumentValidationAction(InstrumentValidationAction instrumentvalidationaction)
	{
		this.InstrumentValidationActionDao.remove(instrumentvalidationaction);
	}

	public InstrumentValidationAction findInstrumentValidationAction(int id)
	{
		return this.InstrumentValidationActionDao.find(id);
	}

	public List<InstrumentValidationAction> getAllInstrumentValidationActions()
	{
		return this.InstrumentValidationActionDao.findAll();
	}

	@Override
	public ResultWrapper insert(Integer issueId, String action, Integer statusId, HttpSession session) {
		InstrumentValidationIssue issue = this.InstrumentValidationIssueServ.findInstrumentValidationIssue(issueId);
		InstrumentValidationAction newAction;
		if (issue == null) return new ResultWrapper(false, "The issue could not be found", null, null);
		newAction = new InstrumentValidationAction();
		newAction.setAction(action);
		newAction.setIssue(issue);
		newAction.setUnderTakenOn(new Date());
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact currentUser = userService.get(username).getCon();
		newAction.setUnderTakenBy(currentUser);
		BindException bindException = new BindException(newAction, "action");
		this.validator.validate(newAction, bindException);
		if (!bindException.hasErrors()) {
			this.insertInstrumentValidationAction(newAction);
			if (statusId != null) {
				InstrumentValidationStatus ivs = this.InstrumentValidationStatusServ.findInstrumentValidationStatus(statusId);
				if (!ivs.getRequiringAttention()) {
					issue.setResolvedBy(currentUser);
					issue.setResolvedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
				}
				issue.setStatus(ivs);
				this.InstrumentValidationIssueServ.saveOrUpdateInstrumentValidationIssue(issue);
			}
		}
		return new ResultWrapper(bindException, newAction);
	}
	
	public void insertInstrumentValidationAction(InstrumentValidationAction InstrumentValidationAction)
	{
		this.InstrumentValidationActionDao.persist(InstrumentValidationAction);
	}

	public void saveOrUpdateInstrumentValidationAction(InstrumentValidationAction instrumentvalidationaction) {
		this.InstrumentValidationActionDao.persist(instrumentvalidationaction);
	}
	
	public void updateInstrumentValidationAction(InstrumentValidationAction InstrumentValidationAction) {
		this.InstrumentValidationActionDao.merge(InstrumentValidationAction);
	}
}