package org.trescal.cwms.core.instrument.entity.instrument;

import lombok.*;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;

@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Getter
@Setter
public class PossibleInstrumentDTO {

    @EqualsAndHashCode.Include
    private Integer plantid;
    private String plantno;
    private String serialno;
    private Integer status;
    private Integer coid;
    private String modelDescription;
    private Integer modelDescriptionId;
    private Integer asnid;
    private Integer jobitemid;
    private String jobitemviewcode;
    private Integer quotationServiceTypeId;
    private Integer jiServiceTypeId;
    private Integer instrumentServiceTypeId;
    private Integer instrumentModelServiceTypeId;

    public PossibleInstrumentDTO(Integer plantid,
                                 String plantno,
                                 String serialno,
                                 InstrumentStatus status,
                                 Integer coid,
                                 String modelDescription,
                                 Integer modelDescriptionId,
                                 Integer quotationItemsCalTypeId,
                                 Integer jisCalTypeId,
                                 Integer instrumentCalTypeId,
                                 Integer instrumentModelCalTypeId,
                                 Integer asnid,
                                 Integer jobitemid,
                                 String jobitemviewcode) {
        this(plantid, plantno, serialno, status.ordinal(), coid, modelDescription, modelDescriptionId, quotationItemsCalTypeId, jisCalTypeId, instrumentCalTypeId,
                instrumentModelCalTypeId, asnid, jobitemid, jobitemviewcode);
    }

    public PossibleInstrumentDTO(Integer plantid, String plantno, String serialno, Integer status, Integer coid,
                                 String modelDescription, Integer modelDescriptionId, Integer quotationServiceTypeId, Integer jiServiceTypeId, Integer instrumentServiceTypeId,
                                 Integer instrumentModelServiceTypeId, Integer asnid, Integer jobitemid, String jobitemviewcode) {
        this.plantid = plantid;
        this.plantno = plantno;
        this.serialno = serialno;
        this.status = status;
        this.coid = coid;
        this.modelDescription = modelDescription;
        this.modelDescriptionId = modelDescriptionId;
        this.asnid = asnid;
        this.jobitemid = jobitemid;
        this.jobitemviewcode = jobitemviewcode;

        this.quotationServiceTypeId = quotationServiceTypeId;
        this.jiServiceTypeId = jiServiceTypeId;
        this.instrumentServiceTypeId = instrumentServiceTypeId;
        this.instrumentModelServiceTypeId = instrumentModelServiceTypeId;
    }

}
