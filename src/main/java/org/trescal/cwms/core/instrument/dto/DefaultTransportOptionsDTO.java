package org.trescal.cwms.core.instrument.dto;

public class DefaultTransportOptionsDTO {

	Integer transportOptionIn;
	Integer transportOptionOut;

	public Integer getTransportOptionIn() {
		return transportOptionIn;
	}

	public void setTransportOptionIn(Integer transportOptionIn) {
		this.transportOptionIn = transportOptionIn;
	}

	public Integer getTransportOptionOut() {
		return transportOptionOut;
	}

	public void setTransportOptionOut(Integer transportOptionOut) {
		this.transportOptionOut = transportOptionOut;
	}

}
