package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;

@Repository
public class InstrumentFieldDefinitionDaoImpl extends BaseDaoImpl<InstrumentFieldDefinition,Integer>implements InstrumentFieldDefinitionDao {

	@Override
	protected Class<InstrumentFieldDefinition> getEntity() {
		return InstrumentFieldDefinition.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InstrumentFieldDefinition> getInstrumentFieldDefinitionsForCompany(Company company) {
		Criteria fieldDefinitionCriteria = getSession().createCriteria(InstrumentFieldDefinition.class);
		fieldDefinitionCriteria.add(Restrictions.eq("client", company));
		return fieldDefinitionCriteria.list();
	}
	
	@Override
	public InstrumentFieldDefinition getForCompanyAndName(Company company, String name) {
		Criteria fieldDefinitionCriteria = getSession().createCriteria(InstrumentFieldDefinition.class);
		fieldDefinitionCriteria.add(Restrictions.eq("client", company));
		fieldDefinitionCriteria.add(Restrictions.eq("name", name));
		InstrumentFieldDefinition result = (InstrumentFieldDefinition) fieldDefinitionCriteria.uniqueResult();
		return result;
	}

}
