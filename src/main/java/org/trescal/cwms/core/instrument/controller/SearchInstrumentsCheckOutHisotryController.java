package org.trescal.cwms.core.instrument.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.checkout.db.CheckOutService;
import org.trescal.cwms.core.instrument.entity.checkout.dto.CheckOutDto;
import org.trescal.cwms.core.instrument.views.CheckOutXlsxView;
import org.trescal.cwms.core.system.Constants;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class SearchInstrumentsCheckOutHisotryController {

	@Autowired
	CheckOutService checkoutService;
	@Autowired
	CheckOutXlsxView xlsxView;

	public static final String VIEW_NAME = "trescal/core/instrument/checkinout/searchCheckout";
	public static final String RESULTS_VIEW_NAME = "trescal/core/instrument/checkinout/searchCheckoutResults";

	@RequestMapping(method = RequestMethod.GET, value = "searchCheckOut.htm")
	public String getPage(Model model, @ModelAttribute("command") CheckOutDto dto) {
		return VIEW_NAME;
	}

	@RequestMapping(method = RequestMethod.POST, value = "searchCheckOut.htm")
	public String showData(Model model, @ModelAttribute("command") CheckOutDto dto) {

		model.addAttribute("command", dto);
		model.addAttribute("checkoutInsts", this.checkoutService.searchCheckOutRecords(dto));
		return RESULTS_VIEW_NAME;
	}

	@RequestMapping(value = "exportcheckout.htm", method = RequestMethod.GET)
	public ModelAndView exportRequest(@RequestParam(value = "barcode", required = true) String barcode,
			@RequestParam(value = "jobno", required = true) String jobno,
			@RequestParam(value = "checkoutdate1", required = true) String checkoutdate1,
			@RequestParam(value = "checkoutdate2", required = true) String checkoutdate2,
			@RequestParam(value = "checkindate1", required = true) String checkindate1,
			@RequestParam(value = "checkindate2", required = true) String checkindate2,
			@RequestParam(value = "checkoutcomplet", required = true) boolean checkoutcomplet) {

		CheckOutDto dto = new CheckOutDto(barcode, jobno, checkoutdate1, checkoutdate2, checkindate1, checkindate2,
				checkoutcomplet);

		List<CheckOut> results = this.checkoutService.searchCheckOutRecords(dto);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("results", results);
		return new ModelAndView(xlsxView, model);
	}

}
