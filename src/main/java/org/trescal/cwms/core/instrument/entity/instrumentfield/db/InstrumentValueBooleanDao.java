package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueBoolean;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentValueBooleanDTO;

public interface InstrumentValueBooleanDao extends BaseDao<InstrumentValueBoolean, Integer> {
	
	List<InstrumentValueBooleanDTO> getAll(InstrumentFieldDefinition fieldDefinition);
	
	List<Instrument> getMatchingInstruments(InstrumentFieldDefinition fieldDefinition, Boolean value);
}