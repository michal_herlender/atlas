package org.trescal.cwms.core.instrument.form;

import javax.validation.constraints.NotNull;

public class QuickDeliveryNoteForm {
	
	private Integer barcode;
	
	@NotNull
	public Integer getBarcode() {
		return barcode;
	}
	
	public void setBarcode(Integer barcode) {
		this.barcode = barcode;
	}
}