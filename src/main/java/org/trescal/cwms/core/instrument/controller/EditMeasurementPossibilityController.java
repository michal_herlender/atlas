package org.trescal.cwms.core.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.measurementpossibility.MeasurementPossibility;
import org.trescal.cwms.core.instrument.entity.measurementpossibility.db.MeasurementPossibilityService;

@Controller @IntranetController
@RequestMapping("editmeasurementpossibility.htm")
public class EditMeasurementPossibilityController {
	@Autowired
	InstrumService instrumentService;
	@Autowired
	MeasurementPossibilityService measurementPossibilityService;	
	@Autowired
	MeasurementPossibilityValidator validator;
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@RequestMapping(method=RequestMethod.GET)
	public String displayForm(){
		return "trescal/core/instrument/editmeasurementpossibility";
	}
	
	
	@ModelAttribute("measurementpossibility")
	public MeasurementPossibility initializeRange(	@RequestParam(value="id", required=false, defaultValue="0" ) int id, 
													@RequestParam(value="plantid", required=false, defaultValue="0") int plantid){
		if(id > 0){
			return measurementPossibilityService.get(id);
		} else {
			MeasurementPossibility mp = new MeasurementPossibility();
			mp.setInstrument(instrumentService.findEagerInstrum(plantid));
			return mp;
		}
	}
	
	@RequestMapping(params = "save", method=RequestMethod.POST)
	public String processSave(@Validated @ModelAttribute("measurementpossibility") MeasurementPossibility mp, BindingResult bindingResult,
			@RequestParam(value="id", required=false, defaultValue="0") int id)
	{
		int plantid = mp.getInstrument().getPlantid();
		validator.validate(mp,bindingResult);
	    if(bindingResult.hasErrors()){
	    	return "trescal/core/instrument/editmeasurementpossibility";
	    }
		measurementPossibilityService.merge(mp);
		return "redirect:viewinstrument.htm?plantid=" + plantid;
	}
	
	@RequestMapping(params = "delete", method=RequestMethod.POST)
	public String ProcessDelete(@ModelAttribute("measurementpossibility") MeasurementPossibility mp, 
			@RequestParam(value="id", required=false, defaultValue="0" ) int id){
		int plantid = mp.getInstrument().getPlantid();
		measurementPossibilityService.delete(mp);
		return "redirect:viewinstrument.htm?plantid=" + plantid;
	}
}
