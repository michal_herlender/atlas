package org.trescal.cwms.core.instrument.entity.instrumentfield;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.tools.FieldType;

@Entity
@Table(name = "instrumentfielddefinition")
public class InstrumentFieldDefinition implements Comparable<InstrumentFieldDefinition> {
	private int instrumentFieldDefinitionid;
	private Company client;
	private String name;
	private Boolean isUpdatable;
	private FieldType fieldType;
	private Set<InstrumentFieldLibraryValue> instrumentFieldLibraryValues;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getInstrumentFieldDefinitionid() {
		return instrumentFieldDefinitionid;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", nullable = false)
	public Company getClient() {
		return client;
	}
	
	@Column(name = "name", nullable = false,columnDefinition = "nvarchar(2000)")
	public String getName() {
		return name;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="fieldtype", columnDefinition="nvarchar(2000)")
	public FieldType getFieldType() {
		return fieldType;
	}

	@Column(name = "isUpdatable", nullable = false)
	public Boolean getIsUpdatable() {
		return isUpdatable;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "instrumentFieldDefinition")
	@OrderBy("name ASC")
	public Set<InstrumentFieldLibraryValue> getInstrumentFieldLibraryValues() {
		return this.instrumentFieldLibraryValues;
	}
	
	public void setInstrumentFieldDefinitionid(int instrumentFieldDefinitionid) {
		this.instrumentFieldDefinitionid = instrumentFieldDefinitionid;
	}
	
	public void setClient(Company client) {
		this.client = client;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setInstrumentFieldLibraryValues(Set<InstrumentFieldLibraryValue> instrumentFieldLibraries)
	{
		this.instrumentFieldLibraryValues = instrumentFieldLibraries;
	}

	
	public void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	public void setIsUpdatable(Boolean isUpdatable) {
		this.isUpdatable = isUpdatable;
	}

	@Override
	public int compareTo(InstrumentFieldDefinition other) {
		return this.name.compareTo(other.name);
	}
}