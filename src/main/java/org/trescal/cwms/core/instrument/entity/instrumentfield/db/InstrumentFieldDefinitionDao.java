package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;

public interface InstrumentFieldDefinitionDao extends BaseDao<InstrumentFieldDefinition, Integer> {
	
	List<InstrumentFieldDefinition> getInstrumentFieldDefinitionsForCompany(Company company);
	InstrumentFieldDefinition getForCompanyAndName(Company company, String name);
}
