package org.trescal.cwms.core.instrument.entity.instrumenthistory.db;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.InstrumentHistory;

@Service("InstrumentHistoryService")
public class InstrumentHistoryServiceImpl implements InstrumentHistoryService
{
	@Autowired
	private InstrumentHistoryDao instrumentHistoryDao;

	public void deleteInstrumentHistory(InstrumentHistory instrumenthistory)
	{
		this.instrumentHistoryDao.remove(instrumenthistory);
	}

	public InstrumentHistory findInstrumentHistory(int id)
	{
		return this.instrumentHistoryDao.find(id);
	}

	public List<InstrumentHistory> getAllInstrumentHistorys()
	{
		return this.instrumentHistoryDao.findAll();
	}

	public void insertInstrumentHistory(InstrumentHistory InstrumentHistory)
	{
		this.instrumentHistoryDao.persist(InstrumentHistory);
	}

	@Override
	public InstrumentHistory prepareNewAddressChangeItem(Contact currentContact, Address oldAddress, Address newAddress, Instrument instrument)
	{
		InstrumentHistory historyItem = new InstrumentHistory();
		historyItem.setInstrument(instrument);
		historyItem.setChangeBy(currentContact);
		historyItem.setChangeDate(new Date());
		historyItem.setAddressUpdated(true);
		historyItem.setOldAddress(oldAddress);
		historyItem.setNewAddress(newAddress);
		return historyItem;
	}

	@Override
	public InstrumentHistory prepareNewContactChangeItem(Contact currentContact, Contact oldContact, Contact newContact, Instrument instrument)
	{
		InstrumentHistory historyItem = new InstrumentHistory();
		historyItem.setInstrument(instrument);
		historyItem.setChangeBy(currentContact);
		historyItem.setChangeDate(new Date());
		historyItem.setContactUpdated(true);
		historyItem.setOldContact(oldContact);
		historyItem.setNewContact(newContact);
		return historyItem;
	}
	
	@Override
	public InstrumentHistory prepareNewCompanyChangeItem(Contact currentContact, Company oldCompany, Company newCompany, Instrument instrument)
	{
		InstrumentHistory historyItem = new InstrumentHistory();
		historyItem.setInstrument(instrument);
		historyItem.setChangeBy(currentContact);
		historyItem.setChangeDate(new Date());
		historyItem.setCompanyUpdated(true);
		historyItem.setOldCompany(oldCompany);
		historyItem.setNewCompany(newCompany);
		return historyItem;
	}

	@Override
	public void saveOrUpdateAllInstrumentHistory(Collection<InstrumentHistory> items)
	{
		if (items != null)
		{
			for (InstrumentHistory item : items)
			{
				this.saveOrUpdateInstrumentHistory(item);
			}
		}
	}

	public void saveOrUpdateInstrumentHistory(InstrumentHistory instrumenthistory)
	{
		this.instrumentHistoryDao.saveOrUpdate(instrumenthistory);
	}

	public void updateInstrumentHistory(InstrumentHistory InstrumentHistory)
	{
		this.instrumentHistoryDao.update(InstrumentHistory);
	}
}