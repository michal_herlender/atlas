package org.trescal.cwms.core.instrument.entity.instrumentfield.dto;

import org.trescal.cwms.core.tools.FieldType;

public abstract class InstrumentValueDTO {

	private Integer plantId;
	private String fieldName;
	
	protected InstrumentValueDTO(Integer plantId, String fieldName) {
		this.plantId = plantId;
		this.fieldName = fieldName;
	}
	
	public Integer getPlantId() {
		return plantId;
	}
	
	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}
	
	public abstract FieldType getFieldType();
	
	public String getFieldName() {
		return fieldName;
	}
	
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
}