package org.trescal.cwms.core.instrument.entity.confidencecheck.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.confidencecheck.ConfidenceCheck;

public interface ConfidenceCheckDao extends BaseDao<ConfidenceCheck, Integer> {

}
