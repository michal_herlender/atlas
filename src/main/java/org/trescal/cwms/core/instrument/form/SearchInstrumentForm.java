package org.trescal.cwms.core.instrument.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Getter @Setter
public class SearchInstrumentForm
{
	private Date addedSince;
	private Integer coid;
	private String desc;
	private Integer descid;
	private String family;
	private Integer familyid;
	private boolean hireOnlyInsts;
	private Collection<InstrumentSearchResultsDTO> instruments;
	private int jobItemCount;
	private String location;
	private Integer locationId;
	private String mfr;
	private Integer mfrid;
	private String model;
	private Integer modelid;
	private Boolean outOfCalibration;
	private int pageNo;
	private Integer personid;
	private String plantid;
	private String plantno;
	private String customerDescription;
	private Boolean customerManaged;
	private Integer addressid;
	private int resultsPerPage;
	// results data
	private PagedResultSet<InstrumentSearchResultsDTO> rs;
	private String searchCriteria;
	private String serialno;
	private Boolean standard;
	private Integer subdivid;
	private List<Integer> flexibleFieldInstrumentIds;
	private LocalDate calDueDate1;
	private LocalDate calDueDate2;
	private LocalDate lastCalDate1;
	private LocalDate lastCalDate2;

	private String sort;
	private String dir;
	private Boolean isExport = false; // set default value to avoid NullPointerException

	@Override
	public String toString() {
		return "SearchInstrumentForm [addedSince=" + addedSince + ", coid=" + coid + ", desc=" + desc + ", descid="
			+ descid + ", family=" + family + ", familyid=" + familyid + ", hireOnlyInsts=" + hireOnlyInsts
			+ ", instruments=" + instruments + ", jobItemCount=" + jobItemCount + ", location=" + location
				+ ", mfr=" + mfr + ", mfrid=" + mfrid + ", model=" + model + ", modelid=" + modelid
				+ ", outOfCalibration=" + outOfCalibration + ", pageNo=" + pageNo + ", personid=" + personid
				+ ", plantid=" + plantid + ", plantno=" + plantno + ", customerDescription=" + customerDescription
				+ ", customerManaged=" + customerManaged + ", addressid=" + addressid + ", resultsPerPage="
				+ resultsPerPage + ", rs=" + rs + ", searchCriteria=" + searchCriteria + ", serialno=" + serialno
				+ ", standard=" + standard + ", subdivid=" + subdivid + ", flexibleFieldInstrumentIds="
				+ flexibleFieldInstrumentIds + ", calDueDate1=" + calDueDate1 + ", calDueDate2=" + calDueDate2
				+ ", lastCalDate1=" + lastCalDate1 + ", lastCalDate2=" + lastCalDate2 + "]";
	}
}