package org.trescal.cwms.core.instrument.controller;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.UpdateCompanyInstrumentsForm;
import org.trescal.cwms.core.instrument.form.UpdateCompanyInstrumentsValidator;
import org.trescal.cwms.core.instrument.service.UpdateCompanyInstrumentsService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class UpdateCompanyInstrumentsController
{
	public final static String MESSAGE_UPDATE = "updatemsg.successfully";
	@Autowired
	private AddressService addServ;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private InstrumService instrumentServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private UpdateCompanyInstrumentsService updateService;
	@Autowired
	private UpdateCompanyInstrumentsValidator validator;
	@Autowired
	private UserService userService;

	@InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
    }
	
	@ModelAttribute("form")
	protected UpdateCompanyInstrumentsForm formBackingObject(HttpSession session,
			@RequestParam(name="formid", required=true) String formid) throws Exception
	{
		// retrieve update instruments form from session using form id passed in url
		UpdateCompanyInstrumentsForm form = (UpdateCompanyInstrumentsForm) session.getAttribute(formid);

		return form;
	}
	
	@RequestMapping(value="/updatecompanyinstruments.htm", method=RequestMethod.GET)
	protected String referenceData(Locale locale, Model model, 
			@ModelAttribute("form") UpdateCompanyInstrumentsForm form) {
		Company company = this.companyService.get(form.getCompanyid());
		Company instrumentCompany = this.companyService.get(form.getInstrumentCompanyid());
		ContactKeyValue noneDto = new ContactKeyValue(0, this.messages.getMessage("company.none", null, "None", locale)); 
		
		model.addAttribute("instrumentCompany", instrumentCompany); 
		model.addAttribute("comp", company);
		model.addAttribute("subdivList", this.subServ.getAllActiveCompanySubdivsKeyValue(form.getCompanyid(), true));
		model.addAttribute("insts", this.instrumentServ.getInstruments(form.getPlantids()));
		int subdivid = form.getSubdivid() != null ? form.getSubdivid() : 0; 
		// get all addresses
		model.addAttribute("addressList", this.addServ.getAllActiveSubdivAddressesKeyValue(subdivid, AddressType.WITHOUT, true));
		// get all contacts
		model.addAttribute("contactList", this.conServ.getContactDtoList(null, subdivid, true, noneDto));
		
		return "trescal/core/instrument/updatecompanyinstruments";
	}
	
	@RequestMapping(value="/updatecompanyinstruments.htm", method=RequestMethod.POST)
	protected String onSubmit(Locale locale, Model model,
		
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("form") @Validated UpdateCompanyInstrumentsForm form, 
			BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData(locale, model, form);
		}
		Contact currentContact = this.userService.get(username).getCon();
		updateService.performUpdate(currentContact, form);
		// After update, the instrument company may have changed
		
		form.setInstrumentCompanyid(form.getCompanyid());
		model.addAttribute("message", messages.getMessage(MESSAGE_UPDATE, null, locale));

		return referenceData(locale, model, form);
	}
}
