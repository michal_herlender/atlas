package org.trescal.cwms.core.instrument.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.documents.images.image.db.ImageService;
import org.trescal.cwms.core.documents.images.jobitemimage.JobItemImage;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.hire.entity.hireaccessorystatus.HireAccessoryStatus;
import org.trescal.cwms.core.instrument.dto.InstrumentValidationIssueDto;
import org.trescal.cwms.core.instrument.entity.checkout.db.CheckOutService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.db.InstrumentValidationStatusService;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtensionType;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.db.CertLinkService;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.db.InstCertLinkService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardType;
import org.trescal.cwms.core.procedure.entity.defaultstandard.db.DefaultStandardService;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRule;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationExtensions;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CertificateClassDefault;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.HTTPSESS_NEW_FILE_LIST,
	Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class ViewInstrumentController {

	protected static final Logger logger = LoggerFactory.getLogger(ViewInstrumentController.class);

	private static final int MAX_RESULTS_CAL_HISTORY = 5;
	private static final int MAX_RESULTS_JOBITEMS = 10;
	private static final int MAX_RESULTS_CERTIFICATES = 20;

	@Value("#{props['cwms.config.instrument.standardsauthentication']}")
	private boolean authenticateBusinessStandards;
	@Autowired
	private CalibrationVerification systemDefaultCalibrationVerification;
	@Autowired
	private CalReqService calReqServ;
	@Autowired
	private CertLinkService certLinkServ;
	@Autowired
	private CertificateService certServ;
	@Value("#{props['recall.default.interval']}")
	private Integer defaultRecallInterval;
	@Autowired
	private DefaultStandardService defStandServ;
	@Value("#{props['cwms.config.jobitem.fast_track_turn']}")
	private int fastTrackTurn;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private ImageService imageServ;
	@Autowired
	private InstrumService instrumServ;
	@Autowired
	private InstCertLinkService instCertLinkService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private RecallRuleService recallRuleServ;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private CalibrationExtensions extensionSystemDefaults;
	@Autowired
	private Deviation deviationSystemDefault;
	@Autowired
	private CertificateClassDefault certificateClassSystemDefault;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private CheckOutService checkOutService;

	@Autowired
	private InstrumentValidationStatusService instrumentValidationStatusService;

	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}

	@ModelAttribute("units")
	public List<KeyValue<String, String>> initializeRecallIntervalUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute("requirements")
	public List<KeyValue<String, String>> initializeRecalRequirementTypes() {
		return RecallRequirementType.getAllTypesForSelect();
	}

	@ModelAttribute("hasCertClass")
	public Boolean canHaveCertClass(@RequestParam(value = "plantid") Integer plantid) {
		Instrument instrument = this.instrumServ.get(plantid);
		return this.certificateClassSystemDefault.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("hasdeviation")
	public Boolean isDeviationPossible(
			@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId) {
		Instrument instrument = this.instrumServ.get(plantId);
		return this.deviationSystemDefault.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("hasverificationstatus")
	public Boolean clientHasCertValidation(
			@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId) {
		Instrument instrument = this.instrumServ.get(plantId);
		return this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY,
				instrument.getComp(), null);
	}

	@ModelAttribute("extensionallowed")
	public Boolean areExtensionsAllowed(
			@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantId) {
		Instrument instrument = this.instrumServ.get(plantId);
		return this.extensionSystemDefaults.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("extensiontypes")
	public List<KeyValue<String, String>> getExtensionTypes() {
		return Arrays.stream(PermitedCalibrationExtensionType.values())
				.map(t -> new KeyValue<>(t.name(), t.getName())).collect(Collectors.toList());
	}

	@ModelAttribute("servicetypes")
	public List<KeyValue<Integer, String>> getServiceTypes() {
		List<ServiceType> serviceTypes = serviceTypeService.getAll();
		return serviceTypes.stream()
				.map(s -> new KeyValue<>(s.getServiceTypeId(), translationService
					.getCorrectTranslation(s.getLongnameTranslation(), LocaleContextHolder.getLocale())))
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/viewinstrument.htm")
	protected String handleRequestInternal(Model model,
			@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer plantid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles) {
		Instrument instrument = this.instrumServ.get(plantid);
		if ((plantid == 0) || (instrument == null)) {
			logger.error("Requested instrument " + plantid + " not found");
			throw new RuntimeException("Requested instrument not found");
		}
		model.addAttribute("instrument", instrument);
		model.addAttribute("flexiblefields", instrumServ.getFlexibleFields(instrument));
		// get a list of the most recent certificate links for this instrument
		// (not all - can be 100s!)
		List<CertLink> clinks = this.certLinkServ.searchByPlantId(plantid, MAX_RESULTS_CERTIFICATES);
		// set directory for all loaded certificates
		// TODO - consolidate directory setting into one method (performance
		// optimization)
		clinks.forEach(cl -> this.certServ.setCertificateDirectory(cl.getCert()));
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.INSTRUMENT,
				plantid, newFiles);
		// add certificate links
		model.addAttribute("certlinks", clinks);
		model.addAttribute("certlinkCount", this.certLinkServ.getCertLinkCount(plantid));
		// get most recent job items (not all for performance!)
		model.addAttribute("jobItems", this.jobItemService.findMostRecentJobItems(plantid, MAX_RESULTS_JOBITEMS));
		model.addAttribute("jobItemCount", this.jobItemService.getCountJobItemsForPlantid(plantid));

		// get the the systemcomponent for file browsing/uploads
		model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.scServ.findComponent(Component.INSTRUMENT));
		model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.addAttribute(Constants.REFDATA_FILES_NAME, fileBrowserServ.getFilesName(fileBrowserWrapper));
		// get the next recall month + year
		model.addAttribute("recalldue", DateTools.getRecallPeriod(instrument.getNextCalDueDate()));
		// get the system default recall interval
		model.addAttribute("defaultrecall", this.defaultRecallInterval);
		// get a list of all recall rules applied to this instrument
		Set<RecallRule> activeRecallRules = this.recallRuleServ.getAllInstrumentRecallRules(instrument.getPlantid(),
				true);
		model.addAttribute("activerecallrules", activeRecallRules);
		// get a list of all old recall rules applied to this instrument
		model.addAttribute("inactiverecallrules",
				this.recallRuleServ.getAllInstrumentRecallRules(instrument.getPlantid(), false));
		// get a list of all images that have been assigned to this instrument
		// (via jobitems)
		List<JobItemImage> images = this.imageServ.getInstrumentImages(plantid);
		model.addAttribute("jobItemImages", images);
		// as a backup load all instrument model images
		model.addAttribute("instModelImage",
				this.imageServ.getMostRecentInstrumentModelImage(instrument.getModel().getModelid()));

		List<Calibration> maxResultsCalHistory = this.instrumServ.getHistory(instrument).stream()
				.limit(MAX_RESULTS_CAL_HISTORY).collect(Collectors.toList());
		model.addAttribute("maxCalHistory", maxResultsCalHistory);
		model.addAttribute("maxResultsCalHistory", MAX_RESULTS_CAL_HISTORY);
		model.addAttribute("historySize", this.instrumServ.getHistory(instrument).size());

		model.addAttribute("privateOnlyNotes", NoteType.INSTRUMENTNOTE.isPrivateOnly());
		model.addAttribute("imageheight", 500);
		model.addAttribute("imagewidth", 750);
		// load any procedures / workinstructions that this instrument is a
		// standard for
		if (instrument.getCalibrationStandard()) {
			model.addAttribute("procstandards",
					this.defStandServ.getAllDefaultStandardsForInstument(plantid, DefaultStandardType.PROCEDURE));
			model.addAttribute("wistandards",
					this.defStandServ.getAllDefaultStandardsForInstument(plantid, DefaultStandardType.WORKINSTRUCTION));
		}
		// check if the user can update this instrument
		model.addAttribute("usercanupdate", !this.authenticateBusinessStandards || this.instrumServ.userCanEditInstrument(instrument).isSuccess());
		// get current recall rule interval (or cal frequency as fallback)
		model.addAttribute("currentCalRecallRuleInterval",
				this.recallRuleServ.getCurrentRecallIntervalDto(instrument, RecallRequirementType.FULL_CALIBRATION));
		model.addAttribute("currentInterimCalRecallRuleInterval",
				this.recallRuleServ.getCurrentRecallIntervalDto(instrument, RecallRequirementType.INTERIM_CALIBRATION));
		model.addAttribute("currentMaintenanceRecallRuleInterval",
				this.recallRuleServ.getCurrentRecallIntervalDto(instrument, RecallRequirementType.MAINTENANCE));
		model.addAttribute(Constants.FAST_TRACK_TURN, this.fastTrackTurn);
		model.addAttribute("calReqs", this.calReqServ.findCalReqsForInstrument(instrument));
		// add the rows per page token
		model.addAttribute("hireAccStatuss", this.statusServ.getAllStatuss(HireAccessoryStatus.class));
		// add business company indicator to page
		model.addAttribute("ownedByBusinessCompany",
				CompanyRole.BUSINESS.equals(instrument.getComp().getCompanyRole()));
		model.addAttribute("calibrationVerification",
			this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY, instrument.getComp(), null));
		model.addAttribute("instCertLinkCount", this.instCertLinkService.getInstCertLinkCount(plantid));
		// Get most recent only, to prevent loading 100s of links
		List<InstCertLink> instCertLinks = this.instCertLinkService.searchByPlantId(plantid, MAX_RESULTS_CERTIFICATES);

		instCertLinks.forEach(icl -> this.certServ.setCertificateDirectory(icl.getCert()));
		model.addAttribute("instCertLinks", instCertLinks);

		model.addAttribute("checkouts", this.checkOutService.getCheckOutRecordsByInstrument(instrument));
		model.addAttribute("validations", instrument.getIssues().stream()
			.map(issue -> InstrumentValidationIssueDto.of(
				issue.getId(),
				issue.getIssue(),
				issue.getRaisedBy().getName(),
				issue.getRaisedOn(),
				issue.getResolvedBy() == null ? "" : issue.getResolvedBy().getName(),
				issue.getResolvedOn(),
				issue.getStatus(),
				issue.getActions()))
			.collect(Collectors.toList()));
		model.addAttribute("validationStatuses", instrumentValidationStatusService.getAllInstrumentValidationStatuss().stream().map(InstrumentValidationIssueDto.ValidationStatus::fromInstrumentValidationStatus).collect(Collectors.toList()));

		return "trescal/core/instrument/viewinstrument";
	}
}
