package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum PermitedCalibrationExtensionType {

    TIME("instrument.timeextensiontype"),
    PERCENTAGE("instrument.percentageextensiontype");

    private String messageCode;

    private MessageSource messageSource;

    PermitedCalibrationExtensionType(String messageCode) {
        this.messageCode = messageCode;
    }

    @Component
    public static class CalExtensionMessageSourceInjector
    {
        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (PermitedCalibrationExtensionType extensionType : EnumSet.allOf(PermitedCalibrationExtensionType.class)) {
                extensionType.setMessageSource(messageSource);
            }
        }
    }

    public void setMessageSource(MessageSource messageSource){
        this.messageSource = messageSource;
    }

    public String getName() {
        return messageSource.getMessage(this.messageCode,null, LocaleContextHolder.getLocale());
    }
}
