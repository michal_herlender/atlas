package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;

@Repository
public class InstrumentValueDaoImpl extends BaseDaoImpl<InstrumentValue, Integer> implements InstrumentValueDao {

	@Override
	protected Class<InstrumentValue> getEntity() {
		return InstrumentValue.class;
	}

	
}
