package org.trescal.cwms.core.instrument.entity.instrumentusagetype.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;

public interface InstrumentUsageTypeService extends BaseService<InstrumentUsageType, Integer>
{
	InstrumentUsageType findDefaultType(Class<?> clazz);

	InstrumentUsageType findInstrumentUsageType(int id);

	List<InstrumentUsageType> getAllInstrumentUsageTypes();

	InstrumentUsageType getByName(String name, Locale locale);
}