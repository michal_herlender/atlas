package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "permitedcalibrationextension")
@PermitedCalibrationExtensionConstraint
public class PermitedCalibrationExtension {


    private Integer extensionId;
    private Instrument instrument;
    private PermitedCalibrationExtensionType type;
    private Integer value;
    private IntervalUnit timeUnit;
    private LocalDate extensionEndDate;

    @Id
    @Column(name = "extensionid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getExtensionId() {
        return this.extensionId;
    }

    public void setExtensionId(Integer extensionId) {
        this.extensionId = extensionId;
    }

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plantid", nullable = false)
    public Instrument getInstrument() {
        return instrument;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    @NotNull
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    public PermitedCalibrationExtensionType getType() {
        return type;
    }

    public void setType(PermitedCalibrationExtensionType type) {
        this.type = type;
    }

    @NotNull
    @Column(name = "value", nullable = false)
    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Column(name = "timeunit")
    @Enumerated(EnumType.STRING)
    public IntervalUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(IntervalUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    @Column(name = "extensionEndDate", nullable = false, columnDefinition = "date")
    public LocalDate getExtensionEndDate() {
        return extensionEndDate;
    }

    public void setExtensionEndDate(LocalDate extensionEndDate) {
        this.extensionEndDate = extensionEndDate;
    }
}
