package org.trescal.cwms.core.instrument.entity.instrument;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;

/*
* This entity maps to a view not a table (view created by SQL script 359)
* At time of writing it's only purpose is to efficiently retrieve some information
* about an instrument's most recent certificate that is required by some customers
* in the instrument excel file download. Restrictions of JPA prevented something
* similar from being achieved directly in the criteria query
* S Chamberlain Dec 2018
**/

//@Entity
//@Immutable
//@Table(name = "mostrecentcertinfo")
public class MostRecentCertificateView implements Serializable {

    private Instrument instrument;
    private BigDecimal deviation;
    private CalibrationVerificationStatus calibrationVerificationStatus;

    @Id
    @OneToOne()
    @JoinColumn(name="plantid")
    public Instrument getInstrument() {
        return instrument;
    }

    @Column(name="deviation")
    public BigDecimal getDeviation() {
        return deviation;
    }

    @Enumerated(EnumType.STRING)
    @Column(name="calibrationverificationstatus")
    public CalibrationVerificationStatus getCalibrationVerificationStatus() {
        return calibrationVerificationStatus;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    public void setDeviation(BigDecimal deviation) {
        this.deviation = deviation;
    }

    public void setCalibrationVerificationStatus(CalibrationVerificationStatus calibrationVerificationStatus) {
        this.calibrationVerificationStatus = calibrationVerificationStatus;
    }
}
