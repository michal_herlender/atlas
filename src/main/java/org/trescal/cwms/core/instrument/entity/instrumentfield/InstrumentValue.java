package org.trescal.cwms.core.instrument.entity.instrumentfield;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Entity
@Table(name="instrumentvalue")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class InstrumentValue implements Comparable<InstrumentValue> {
	
	private int instrumentFieldValueid;	
	private InstrumentFieldDefinition instrumentFieldDefinition;
	private Instrument instrument;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getInstrumentFieldValueid() {
		return instrumentFieldValueid;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, targetEntity=InstrumentFieldDefinition.class)
	@JoinColumn(name = "instrumentFieldDefinitionid", nullable = false)
	public InstrumentFieldDefinition getInstrumentFieldDefinition() {
		return instrumentFieldDefinition;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, targetEntity=Instrument.class)
	@JoinColumn(name = "plantid", nullable = false)
	public Instrument getInstrument() {
		return instrument;
	}
	
	public void setInstrumentFieldValueid(int instrumentFieldValueid) {
		this.instrumentFieldValueid = instrumentFieldValueid;
	}
	
	public void setInstrumentFieldDefinition(InstrumentFieldDefinition instrumentFieldDefinition) {
		this.instrumentFieldDefinition = instrumentFieldDefinition;
	}
	
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
	
	@Override
	public int compareTo(InstrumentValue other) {
		return this.getInstrumentFieldDefinition().getName().compareTo(other.getInstrumentFieldDefinition().getName());
	}
	

}
