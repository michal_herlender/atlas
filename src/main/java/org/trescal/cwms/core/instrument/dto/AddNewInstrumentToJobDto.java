package org.trescal.cwms.core.instrument.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class AddNewInstrumentToJobDto {
	private Integer coid;
	private String plantNo;
	private String serialNo;
	private Integer modelId;
	private Boolean mfrReqForModel;
	private Boolean searchModelMatches;
}
