package org.trescal.cwms.core.instrument.entity.checkout.db;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut_;
import org.trescal.cwms.core.instrument.entity.checkout.dto.CheckOutDto;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository("CheckOutDao")
public class CheckOutDaoImpl extends BaseDaoImpl<CheckOut, Integer> implements CheckOutDao {

	@Override
	protected Class<CheckOut> getEntity() {
		return CheckOut.class;
	}

	@Override
	public CheckOut checkIfInstrumentIsCheckOut(Instrument inst, Job job) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<CheckOut> cq = cb.createQuery(CheckOut.class);
		Root<CheckOut> root = cq.from(CheckOut.class);

		Predicate andClauses = cb.conjunction();
		andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.instrument), inst));
		andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.checkOutComplet), false));
		if (job != null) {
			andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.job), job));
		}

		cq.select(root);
		cq.where(andClauses);

		TypedQuery<CheckOut> query = getEntityManager().createQuery(cq);
		if (query.getResultList().size() > 0)
			return query.getResultList().get(0);
		else
			return null;

	}

	@Override
	public List<CheckOut> getCheckOutInstrumentsByTechnicien(Contact technicien) {
		return getResultList(cb -> {
			CriteriaQuery<CheckOut> cq = cb.createQuery(CheckOut.class);
			Root<CheckOut> root = cq.from(CheckOut.class);

			Predicate andClauses = cb.conjunction();
			andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.technicien), technicien));
			andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.checkOutComplet), false));
			cq.where(andClauses);

			return cq;
		});
	}

	@Override
	public List<CheckOut> getCheckInInstrumentsByTechnicienWithNoConfidenceCheck(Contact technicien) {
		return getResultList(cb -> {
			CriteriaQuery<CheckOut> cq = cb.createQuery(CheckOut.class);
			Root<CheckOut> root = cq.from(CheckOut.class);

			Predicate andClauses = cb.conjunction();
			andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.technicien), technicien));
			andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.checkOutComplet), true));
			andClauses.getExpressions().add(cb.isEmpty(root.get(CheckOut_.confidenceChecks)));
			cq.where(andClauses);

			return cq;
		});
	}

	@Override
	public List<CheckOut> searchCheckOutRecords(CheckOutDto dto, Instrument inst, Job job) {
		return getResultList(cb -> {
			CriteriaQuery<CheckOut> cq = cb.createQuery(CheckOut.class);
			Root<CheckOut> root = cq.from(CheckOut.class);

			Predicate andClauses = cb.conjunction();

			if (inst != null) {
				andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.instrument), inst));
			}

			if (job != null) {
				andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.job), job));
			}

			if (dto.isCheckoutcomplet() == true) {
				andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.checkOutComplet), true));
			} else {
				andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.checkOutComplet), false));
			}

			if (StringUtils.isNotBlank(dto.getCheckoutdate1()) && StringUtils.isNotBlank(dto.getCheckoutdate2())) {

				try {

					Date date1, date2;
					date1 = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getCheckoutdate1());
					date2 = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getCheckoutdate2());

					andClauses.getExpressions().add(cb.between(root.get(CheckOut_.checkOutDate), date1, date2));

				} catch (ParseException e) {
					e.printStackTrace();
				}

			} else if (StringUtils.isNotBlank(dto.getCheckoutdate1())) {

				try {

					Date date1;
					date1 = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getCheckoutdate1());

					andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.checkOutDate), date1));

				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			if (StringUtils.isNotBlank(dto.getCheckindate1()) && StringUtils.isNotBlank(dto.getCheckindate2())) {

				try {

					Date date1, date2;
					date1 = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getCheckindate1());
					date2 = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getCheckindate1());

					andClauses.getExpressions().add(cb.between(root.get(CheckOut_.checkInDate), date1, date2));

				} catch (ParseException e) {
					e.printStackTrace();
				}

			} else if (StringUtils.isNotBlank(dto.getCheckindate1())) {

				try {

					Date date1;
					date1 = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getCheckindate1());

					andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.checkInDate), date1));

				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			cq.where(andClauses);

			return cq;
		});
	}

	@Override
	public List<CheckOut> getCheckOutRecordsByInstrument(Instrument inst) {
		return getResultList(cb -> {
			
			CriteriaQuery<CheckOut> cq = cb.createQuery(CheckOut.class);
			Root<CheckOut> root = cq.from(CheckOut.class);
			Predicate andClauses = cb.conjunction();
			andClauses.getExpressions().add(cb.equal(root.get(CheckOut_.instrument), inst));
			cq.where(andClauses);

			return cq;
		});
	}

}
