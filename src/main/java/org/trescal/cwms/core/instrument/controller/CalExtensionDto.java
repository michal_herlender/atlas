package org.trescal.cwms.core.instrument.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalExtensionDto {

    private Integer extensionId;
    private String type;
    private Integer value;
    private String timeUnit;
    private String timeUnitText;
    private Integer plantid;
    private LocalDate extensionEndDate;
    private Boolean instrumentPreExtension;
    private Boolean instrumentInExtension;
    private Boolean instrumentPostExtension;
    private Boolean success;
    private String message;


    public CalExtensionDto(Boolean success, String message, Integer plantid) {
        this.success = success;
        this.message = message;
        this.plantid = plantid;
    }

}

