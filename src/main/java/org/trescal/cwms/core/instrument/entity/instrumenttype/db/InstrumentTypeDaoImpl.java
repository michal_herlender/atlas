package org.trescal.cwms.core.instrument.entity.instrumenttype.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumenttype.InstrumentType;

@Repository("InsttDao")
public abstract class InstrumentTypeDaoImpl<T extends InstrumentType> extends BaseDaoImpl<T, Integer> implements InstrumentTypeDao<T> {
	
	@SuppressWarnings("unchecked")
	public T findDefaultType() {
		Criteria criteria = getSession().createCriteria(getEntity());
		criteria.add(Restrictions.eq("defaultType", true));
		List<T> list = (List<T>) criteria.list();
		return list.size() > 0 ? list.get(0) : null;
	}
}