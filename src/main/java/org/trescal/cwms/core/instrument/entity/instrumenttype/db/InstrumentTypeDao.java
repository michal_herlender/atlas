package org.trescal.cwms.core.instrument.entity.instrumenttype.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumenttype.InstrumentType;


public interface InstrumentTypeDao<T extends InstrumentType> extends BaseDao<T, Integer> {
	
	T findDefaultType();
}