package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;
import org.trescal.cwms.core.instrument.form.AddEditInstrumentFieldForm;
import org.trescal.cwms.web.instrument.form.FlexibleFieldSearchDTO;

public interface InstrumentValueService extends BaseService<InstrumentValue, Integer> {

	public String getValueAsText(InstrumentValue instrumentValue);


    /**
     * Returns a pre-populated @{@link AddEditInstrumentFieldForm} form backing object for the intranet and portal
     * add/edit instrument field controllers to use.
     * @param instrumentFieldValueId - id of instrument field value used when we are editing an existing value
     * @param fieldDefinitionId - id of field definition id used when we are creating a new value
     * @param plantId - plant id used to connect new value to correct instrument
     * @return
     */
	public AddEditInstrumentFieldForm createFormBackingObject(Integer instrumentFieldValueId, Integer fieldDefinitionId,  Integer plantId);

    /**
     * Creates or updates a @instrumentValue, used by intranet and portal add/edit instrument field controllers
     * @param command - the @{@link AddEditInstrumentFieldForm} form backing object
     */
	public void updateFieldValue(AddEditInstrumentFieldForm command) throws Exception;
	
	/**
	 * Returns a list of instrument ids for instruments with flexible field values that match those passed in 
	 * the search parameters. Use the instrument:flexiblefieldsearch jsp tag to get the list of FlexibleFieldSearchDTOs
	 * the returned list of instrument ids can then be incorporated into an existing search with something like  
	 * instCriteria.add(Restrictions.in("plantid", instrumentidlist)
	 * @param searchParameters
	 */
	public List<Integer> getInstrumentIdsForFlexibleFieldSearch(List<FlexibleFieldSearchDTO> searchParameters);
}

