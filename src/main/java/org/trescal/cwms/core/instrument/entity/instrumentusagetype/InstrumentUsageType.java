package org.trescal.cwms.core.instrument.entity.instrumentusagetype;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumenttype.InstrumentType;
import org.trescal.cwms.core.system.entity.translation.Translation;

/**
 * Defines the different usage types for {@link Instrument} entities, for
 * example indication only, calibrate before use.
 * 
 * @author Richard
 */
@Entity
@Table(name = "instrumentusagetype")
public class InstrumentUsageType extends InstrumentType
{
	private Set<Instrument> instruments;
	private Set<Translation> descriptiontranslation;
	private Set<Translation> nametranslation;
	private InstrumentStatus instrumentStatus;
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="instrumentusagetypedescriptiontranslation", joinColumns=@JoinColumn(name="typeid"))
	public Set<Translation> getDescriptiontranslation() {
		return descriptiontranslation;
	}

	public void setDescriptiontranslation(Set<Translation> descriptiontranslation) {
		this.descriptiontranslation = descriptiontranslation;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="instrumentusagetypenametranslation", joinColumns=@JoinColumn(name="typeid"))
	public Set<Translation> getNametranslation() {
		return nametranslation;
	}
	
	public void setNametranslation(Set<Translation> nametranslation) {
		this.nametranslation = nametranslation;
	}

	@OneToMany(mappedBy = "usageType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Instrument> getInstruments()
	{
		return this.instruments;
	}
	
	@Enumerated(EnumType.ORDINAL)
    @Column(name = "instrumentstatus", nullable = false)
    public InstrumentStatus getInstrumentStatus() {
        return this.instrumentStatus;
    }

	public void setInstruments(Set<Instrument> instruments)
	{
		this.instruments = instruments;
	}

	protected void setInstrumentStatus(InstrumentStatus instrumentStatus) {
		this.instrumentStatus = instrumentStatus;
	}
	
}
