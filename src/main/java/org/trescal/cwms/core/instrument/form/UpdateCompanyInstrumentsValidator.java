package org.trescal.cwms.core.instrument.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class UpdateCompanyInstrumentsValidator extends AbstractBeanValidator implements Validator
{
	@Autowired
	private InstrumService instrumentService;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(UpdateCompanyInstrumentsForm.class);
	}

	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			UpdateCompanyInstrumentsForm form = (UpdateCompanyInstrumentsForm) target;

			// Either a person or an address must be selected, at minimum, if making any updates
			boolean personIdSelected = form.getPersonid() != null && form.getPersonid() != 0;
			boolean addressIdSelected = form.getAddressid() != null && form.getAddressid() != 0;
			
			if (!personIdSelected && !addressIdSelected)
			{
				errors.reject(null, "You have not selected a new contact or new address for these instruments and so no update could take place");
			}
			
			boolean otherSubdivInstruments = false;
			// If any instruments are from another subdiv, both a person and address must be selected 
			for (Integer plantid : form.getPlantids()) {
				Instrument instrument = instrumentService.get(plantid);
				
				if (!instrument.getAdd().getSub().getId().equals(form.getSubdivid())) {
					otherSubdivInstruments  = true;
					break;
				}
				if (!instrument.getCon().getSub().getId().equals(form.getSubdivid())) {
					otherSubdivInstruments  = true;
					break;
				}
			}
			
			if (otherSubdivInstruments && (!personIdSelected || !addressIdSelected)) {
				errors.reject(null, "Both the new contact and new address must be specified when updating one or more instruments belonging to another subdivision.");
			}
		}
	}
}
