package org.trescal.cwms.core.instrument.entity.instrumentnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

/**
 * Entity representing a {@link Note} for an {@link Instrument}.
 */
@Entity
@Table(name = "instrumentnote")
public class InstrumentNote extends Note
{
	private Instrument instrument;
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", unique = false, nullable = false, insertable = true, updatable = true)
	public Instrument getInstrument() {
		return instrument;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.INSTRUMENTNOTE;
	}
	
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
}