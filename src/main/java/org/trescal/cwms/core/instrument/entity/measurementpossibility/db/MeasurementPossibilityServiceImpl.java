package org.trescal.cwms.core.instrument.entity.measurementpossibility.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.entity.measurementpossibility.MeasurementPossibility;

@Service("MeasurementPossibilityService")
public class MeasurementPossibilityServiceImpl extends BaseServiceImpl<MeasurementPossibility, Integer> implements MeasurementPossibilityService
{
	@Autowired
	private MeasurementPossibilityDao mpDao;
	
	@Override
	protected BaseDao<MeasurementPossibility, Integer> getBaseDao() {
		return mpDao;
	}
}