package org.trescal.cwms.core.instrument.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.utils.ExchangeFormatGeneralValidator;
import org.trescal.cwms.core.instrument.form.ImportInstrumentsForm;
import org.trescal.cwms.core.instrument.form.ImportInstrumentsFormValidator;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;

@Controller
@IntranetController
public class ImportInstrumentsController {

	public static final String FORM = "form";
	@Autowired
	private ImportInstrumentsFormValidator validator;
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private ExchangeFormatService efService;
	@Autowired
	private ExchangeFormatGeneralValidator efValidator;

	@InitBinder(FORM)
	protected void initBinder(ServletRequestDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM)
	public ImportInstrumentsForm getForm() {
		return new ImportInstrumentsForm();
	}

	@RequestMapping(value = "/importinstruments.htm", method = RequestMethod.POST)
	public String submitForm(@Valid @ModelAttribute(FORM) ImportInstrumentsForm form, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes, Model model, Locale locale) throws IOException {

		if (bindingResult.hasErrors()) {
			model.addAttribute("servicesType", serviceTypeService.getAll());
			model.addAttribute("locale", locale);
			return "trescal/core/instrument/importinstruments";
		}

		ExchangeFormat ef = efService.get(form.getExchangeFormatId());
		List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil
				.readExcelFile(form.getFile().getInputStream(), ef.getSheetName(), ef.getLinesToSkip());

		if (efValidator.validate(ef, fileContent, bindingResult).hasErrors()) {
			model.addAttribute("servicesType", serviceTypeService.getAll());
			model.addAttribute("locale", locale);
			return "trescal/core/instrument/importinstruments";
		}

		redirectAttributes.addFlashAttribute("fileContent", fileContent);
		redirectAttributes.addFlashAttribute("importInstrumentsForm", form);
		return "redirect:/importedinstrumentssynthesis.htm";
	}

	@RequestMapping(value = "/importinstruments.htm", method = RequestMethod.GET)
	public String doGet(Locale locale, Model model) {
		model.addAttribute("servicesType", serviceTypeService.getAll());
		model.addAttribute("locale", locale);
		return "trescal/core/instrument/importinstruments";
	}

}
