package org.trescal.cwms.core.instrument.controller;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.*;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Objects;

/**
 * Displays a page listing the {@link Instrument} entities associated to a
 * {@link Company}. The page can be submitted back to itself to narrow or widen
 * the list of results. By default no instruments are listed, but a total count
 * is given.
 */
@Controller
@IntranetController
public class ViewCompanyInstrumentController {
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private AddressService addServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private InstrumService instrumentServ;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private ViewCompanyInstrumentValidator validator;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected ViewCompanyInstrumentForm formBackingObject(@RequestParam(value = "coid") Integer coid,
			@RequestParam(value = "subdivid", required = false, defaultValue = "0") Integer subdivid) throws Exception {
		ViewCompanyInstrumentForm form = new ViewCompanyInstrumentForm();
		form.setCompany(this.compServ.get(coid));
		form.setCoid(coid);
		// we basically have to load the referenceData here as this form
		// submits to itself and referenceData is not displayed after a POST.
		// set initial search orientation
		form.setAscOrder(true);
		// always return a list of all instruments beloging to the company
		form.setTotalinstrumentcount(this.instrumentServ.getCompanyInstrumentCount(form.getCompany().getCoid()));
		// set sort types
		form.setSortTypes(EnumSet.allOf(NewQuoteInstSortType.class));
		// get all divisions
		form.setSublist(this.subServ.getAllActiveCompanySubdivs(form.getCompany()));
		// get all company contacts
		form.setCompconlist(this.conServ.getAllCompanyContactsBySubCon(form.getCoid(), true));
		// initialise form search
		InstrumentAndModelSearchForm<Instrument> search = new InstrumentAndModelSearchForm<>();
		search.setCompId(coid);
		search.setPageNo(1);
		search.setResultsPerPage(Constants.RESULTS_PER_PAGE);
		form.setSearch(search);
		if (subdivid != 0)
			form.setSubdivid(subdivid);
		return form;
	}

	@RequestMapping(value = "/viewcompanyinstrument.htm", method = RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/instrument/viewcompanyinstrument";
	}

	@RequestMapping(value = "/viewcompanyinstrument.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(Locale locale, HttpServletRequest request,
			@ModelAttribute("form") @Validated ViewCompanyInstrumentForm form, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("trescal/core/instrument/viewcompanyinstrument", "form", form);
		}
		HttpSession session = request.getSession(true);
		if (form.getSubmitType().equalsIgnoreCase("updatecompinsts")) {
			// create new update company instruments form
			UpdateCompanyInstrumentsForm updateform = new UpdateCompanyInstrumentsForm();
			// add company to form
			updateform.setCompanyid(form.getCompany().getCoid());
			updateform.setInstrumentCompanyid(form.getCompany().getCoid());
			// add instrument list to form
			updateform.setPlantids(form.getBasketIds());
			// create a random string as name of session attribute to be passed
			// in url
			String randomString = RandomStringUtils.randomAlphanumeric(10);
			// add to session
			session.setAttribute(randomString, updateform);
			// redirect user
			return new ModelAndView(new RedirectView("/updatecompanyinstruments.htm?formid=" + randomString, true));
		} else if (form.getSubmitType().equalsIgnoreCase("createquoteinsts")) {
			// create new update company instruments form
			QuoteCompanyInstrumentsForm quoteform = new QuoteCompanyInstrumentsForm();
			// add instrument list to form
			quoteform.setPlantids(form.getBasketIds());
			// create a random string as name of session attribute to be passed
			// in url
			String randomString = RandomStringUtils.randomAlphanumeric(10);
			// add to session
			session.setAttribute(randomString, quoteform);
			// redirect user
			return new ModelAndView(new RedirectView(
					"/quoteform.htm?personid=" + form.getQuoteContact() + "&formid=" + randomString, true));
		} else {
			// load instruments - replaced with paged search
			if (!Objects.equals(form.getSubdivid(), form.getSelectedSubdivid())) {
				form.setSubdivid(form.getSelectedSubdivid());
			}
			int resultsPerPage = form.getSearch().getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE
					: form.getSearch().getResultsPerPage();
			int currentPage = form.getSearch().getPageNo() == 0 ? 1 : form.getSearch().getPageNo();
			PagedResultSet<Instrument> rs = this.instrumentServ.searchInstrumentsPaged(
					new PagedResultSet<>(resultsPerPage, currentPage), form.getSearch(), form.getSubdivid(),
					form.getPersonid(), form.getAddressid(), form.getOutOfCal(), form.getSortType(), form.isAscOrder(), locale);
			form.getSearch().setRs(rs);
			// get all divisions
			form.setSublist(this.subServ.getAllActiveCompanySubdivs(form.getCompany()));
			// get all company contacts
			form.setCompconlist(this.conServ.getAllCompanyContactsBySubCon(form.getCoid(), true));
			// check subdiv id not null
			if (form.getSubdivid() != null && form.getSubdivid() != 0) {
				Subdiv subdiv = subServ.get(form.getSubdivid());
				// get all addresses
				form.setAddlist(this.addServ.getAllActiveSubdivAddresses(subdiv, AddressType.WITHOUT));
				// get all contacts
				form.setConlist(this.conServ.getAllActiveSubdivContacts(form.getSubdivid()));
			}
			return new ModelAndView("trescal/core/instrument/viewcompanyinstrument", "command", form);
		}
	}
}