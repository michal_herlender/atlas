package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueString;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueString_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentValueStringDTO;

@Repository
public class InstrumentValueStringDaoImpl extends BaseDaoImpl<InstrumentValueString, Integer> implements InstrumentValueStringDao {
	
	@Override
	protected Class<InstrumentValueString> getEntity() {
		return InstrumentValueString.class;
	}
	
	@Override
	public List<InstrumentValueStringDTO> getAll(InstrumentFieldDefinition fieldDefinition) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<InstrumentValueStringDTO> cq = cb.createQuery(InstrumentValueStringDTO.class);
		Root<InstrumentValueString> val = cq.from(InstrumentValueString.class);
		Join<InstrumentValueString, Instrument> instr = val.join(InstrumentValueString_.instrument);
		Join<InstrumentValueString, InstrumentFieldDefinition> field = val.join(InstrumentValueString_.instrumentFieldDefinition);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(val.get(InstrumentValueString_.instrumentFieldDefinition), fieldDefinition));
		cq.select(cb.construct(InstrumentValueStringDTO.class,
				instr.get(Instrument_.plantid),
				field.get(InstrumentFieldDefinition_.name),
				val.get(InstrumentValueString_.value)));
		cq.where(clauses);
		TypedQuery<InstrumentValueStringDTO> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Instrument> getMatchingInstruments(InstrumentFieldDefinition fieldDefinition, String value) {
		Criteria criteria = getSession()
				.createCriteria(InstrumentValueString.class).
				add(Restrictions.eq("instrumentFieldDefinition",fieldDefinition)).
				add(Restrictions.eq("value", value)).
				setProjection(Projections.property("instrument"));
		return criteria.list();
	}
}