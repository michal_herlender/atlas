package org.trescal.cwms.core.instrument.entity.hireinstrument.db;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireInstrumentDto;
import org.trescal.cwms.core.hire.dto.HireInstrumentRevenueWrapper;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

@Service("HireInstrumentService")
public class HireInstrumentServiceImpl implements HireInstrumentService
{
	@Autowired
	private HireInstrumentDao hireInstrumentDao;

	public void deleteHireInstrument(HireInstrument hi)
	{
		this.hireInstrumentDao.remove(hi);
	}

	public HireInstrument findHireInstrument(int id)
	{
		return this.hireInstrumentDao.find(id);
	}

	public HireInstrument findHireInstrumentDWR(int id)
	{
		return this.hireInstrumentDao.findHireInstrumentDWR(id);
	}

	public ResultWrapper findReplacementHireInstrumentDWR(int plantid)
	{
		// find hire instrument using barcode
		HireInstrument hi = this.hireInstrumentDao.findHireInstrumentUsingBarcodeDWR(plantid);
		// hire instrument null?
		if (hi != null)
		{
			// return hire instrument
			return new ResultWrapper(true, "", hi, null);
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Hire instrument could not be found", null, null);
		}
	}

	public List<HireInstrument> getAllHireInstruments()
	{
		return this.hireInstrumentDao.findAll();
	}

	public int getHireInstrumentCount()
	{
		return this.hireInstrumentDao.getHireInstrumentCount();
	}

	public List<HireInstrument> getHireInstruments(List<Integer> plantIds)
	{
		return this.hireInstrumentDao.getHireInstruments(plantIds);
	}

	public void insertHireInstrument(HireInstrument hi)
	{
		this.hireInstrumentDao.persist(hi);
	}

	public void saveOrUpdateHireInstrument(HireInstrument hi)
	{
		this.hireInstrumentDao.saveOrUpdate(hi);
	}

	public List<HireInstrumentRevenueWrapper> searchHireInstrumentsRevenueHQL(Integer mfrid, String mfrname, String modelname, Integer descid, String descname, String barcode, String plantno, String serialno)
	{
		return this.hireInstrumentDao.searchHireInstrumentsRevenueHQL(mfrid, mfrname, modelname, descid, descname, barcode, plantno, serialno);
	}


	public void updateHireInstrument(HireInstrument hi)
	{
		this.hireInstrumentDao.update(hi);
	}

	@Override
	public List<HireInstrumentDto> searchHireInstruments(Integer companyId, String plantId, String plantNo, String serialNo, String model, String manufacturer, String subFamily) {
		return hireInstrumentDao.searchHireInstruments(companyId,plantId,plantNo,serialNo,model,manufacturer,subFamily);
	}

    @Override
    public Optional<HireInstrument> getHireInstrumentByPlantId(Integer plantId) {
        return hireInstrumentDao.getHireInstrumentByPlantId(plantId);
    }

}