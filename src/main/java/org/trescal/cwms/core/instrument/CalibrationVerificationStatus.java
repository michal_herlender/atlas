package org.trescal.cwms.core.instrument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;
import java.util.Locale;

/**
 * Enum to hold possible states for calibration verification field in
 * instruments. Mainly required in the avionics industry inittially required in
 * Spain for Iberia where values are Apto (Suitable) and No Apto (not suitable).
 * Not applicable state also needed for instruments where this classification
 * isn't required.
 * List extended see ticket DEV-1390
 */
public enum CalibrationVerificationStatus {
	ACCEPTABLE("calibrationverification.acceptable", "calibrationverification.shortname.acceptable", "Pass", "P"),
	NOT_ACCEPTABLE("calibrationverification.notacceptable","calibrationverification.shortname.notacceptable", "Fail", "F"),
	NOT_APPLICABLE("calibrationverification.notapplicable","calibrationverification.shortname.notapplicable", "Not Applicable", "NA"),
	PROBABLY_PASS("calibrationverification.probablypass","calibrationverification.shortname.probablypass", "Probably pass", "PP"),
	PROBABLY_FAIL("calibrationverification.probablyfail","calibrationverification.shortname.probablyfail", "Probably fail", "PF");

	private final String nameCode;
	private final String shortNameCode;
	private final String defaultName;
	private final String defaultShortName;
	private MessageSource messageSource;

	CalibrationVerificationStatus(String nameCode, String shortNameCode, String defaultName, String defaultShortName) {
		this.nameCode = nameCode;
		this.shortNameCode = shortNameCode;
		this.defaultName = defaultName;
		this.defaultShortName = defaultShortName;
	}

	@Component
	public static class CalibrationVerificationMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (CalibrationVerificationStatus status : EnumSet.allOf(CalibrationVerificationStatus.class)) {
				status.setMessageSource(messageSource);
			}
		}
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getName() {
		return getNameForLocale(LocaleContextHolder.getLocale());
	}

	public String getNameForLocale(Locale locale) {
		return messageSource.getMessage(nameCode, null, defaultName, locale);
	}
	
	public String getShortName() {
		return getNameForLocale(LocaleContextHolder.getLocale());
	}
	
	public String getShortNameForLocale(Locale locale) {
		return messageSource.getMessage(shortNameCode, null, defaultShortName, locale);
	}

}
