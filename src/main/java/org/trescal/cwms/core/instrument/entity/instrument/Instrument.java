package org.trescal.cwms.core.instrument.entity.instrument;

import lombok.Setter;
import lombok.val;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.SortNatural;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.asset.entity.asset.PhysicalAsset;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.instrument.MeasurementType;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentUtils;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;
import org.trescal.cwms.core.instrument.entity.instrumenthistory.InstrumentHistory;
import org.trescal.cwms.core.instrument.entity.instrumentnote.InstrumentNote;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssueComparator;
import org.trescal.cwms.core.instrument.entity.measurementpossibility.MeasurementPossibility;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.range.RangeComparator;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedComparator;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLinkComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemComparator;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItemByDateComparator;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.InstModelTools;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.trescal.cwms.core.tools.DateTools.dateToLocalDate;


@Entity
@Table(name = "instrument")
@Setter
public class Instrument extends Versioned implements NoteAwareEntity, ComponentEntity {
    @Transient
    public static long getDifference(GregorianCalendar a, GregorianCalendar b, TimeUnit units) {
        return units.convert(b.getTimeInMillis() - a.getTimeInMillis(), TimeUnit.MILLISECONDS);
    }

    private Address add; // Default address for storage of the instrument
    private Contact addedBy;
    private LocalDate addedOn;
    private PhysicalAsset asset;
    private Instrument baseUnit;
    private Integer calFrequency = 0;
    private IntervalUnit calFrequencyUnit;
    /**
     * Set of all instances where this {@link Instrument} has been used as a
     * calibration standard.
     */
    private Set<StandardUsed> calibrations;
    /**
     * Defines the number of calibrations this item can be used for before it
     * requires calibration.
     */
    private Integer permittedNumberOfUses = 0;
    /**
     * Defines if this item is calibrated after it has been used to calibrate other
     * items a certain number of times.
     */
    private Boolean calExpiresAfterNumberOfUses = false;
    /**
     * Defines the number of calibrations this item has been used for since its last
     * calibration.
     */
    private Integer usesSinceLastCalibration = 0;
    /**
     * Indicates if this piece of equipment is a calibration standard
     */
    private Boolean calibrationStandard = false;
    private SortedSet<InstrumentCharacteristic> characteristics;
    private Company comp;
    private Contact con; // Contact owner of the instrument
    private ServiceType defaultServiceType;
    @Transient
    private File directory;
    /**
     * Defines the number of days passed as a percentage in the last month leading
     * up to a calibration due date.
     */
    private Integer finalMonthOfCalTimescalePercentage = 0;
    /**
     * Access code / password built directly into the instrument to gain access to
     * it's software interface / firmware.
     */
    private String firmwareAccessCode;
    private Set<HireInstrument> hireInstruments;
    private Set<InstrumentHistory> history;
    /**
     * Defines the number of days passed as a percentage in a calibration cycle
     * using a start date(i.e. last cal, added on), todays date and
     * nextCalDueDate(minus 1 month) see below.
     */
    private Integer inCalTimescalePercentage = 0;
    private Set<InstCertLink> instCertLinks;
    private Set<InstrumentValidationIssue> issues;

    private Set<JobItem> jobItems;
    private Calibration lastCal;
    private Location loc;
    private Mfr mfr;
    private String modelname;
    private InstrumentModel model;
    private Set<Instrument> modules;
    private LocalDate nextCalDueDate;
    private Set<InstrumentNote> notes;
    private int plantid;

    private String plantno;
    private Capability capability;
    private Set<Quotationitem> quoteItems;
    private Set<InstrumentRange> ranges;
    private Boolean recalledNotReceived = false;
    private Set<RecallItem> recalls;
    private Contact replacedBy;
    private LocalDate replacedOn;
    private Instrument replacement;
    private Set<Instrument> replacementTo;
    private String requirement;

    private Boolean scrapped = false;
    private Contact scrappedBy;
    private LocalDate scrappedOn;
    private JobItem scrappedOnJI;
    private String serialno;

    private Set<DefaultStandard> standards;

    private InstrumentStatus status;
    private InstrumentStorageType storageType;

    private Thread thread;
    private InstrumentUsageType usageType;
    private List<WorkInstruction> workInstructions;
    private List<Contract> contracts;

    /*
     * Former barcode is maintained in instrument as it is searchable; 
     * other "former" fields are in instrumentComplementaryField (created when required)  
     */
    private String formerBarCode;

    /*
     * Customer fields
     */
    public String customerDescription;
    public String customerSpecification;
    public String freeTextLocation;
    public String clientBarcode;

    /*
     * Customer Fields to list
     */
    private Set<InstrumentValue> flexibleFields;

    private Boolean customerManaged = false;
    private Set<InstrumentComplementaryField> instrumentComplementaryFields;
    private String simplifiedSerialNo;
    private String simplifiedPlantNo;

    public String getSimplifiedPlantNo() {
        return simplifiedPlantNo;
    }

    public void setSimplifiedPlantNo(String simplifiedPlantNo) {
        this.simplifiedPlantNo = simplifiedPlantNo;
    }


    public String getSimplifiedSerialNo() {
        return simplifiedSerialNo;
    }

    public void setSimplifiedSerialNo(String simplifiedSerialNo) {
        this.simplifiedSerialNo = simplifiedSerialNo;
    }


    /*
     * Measurement possibilities
     */
    private Set<MeasurementPossibility> measurementPossibilities;
    private MeasurementType measurementType;
	
	private String defaultClientRef;

	@NotNull
	@Column(name = "calintervalunit", nullable=false)
	@Enumerated(EnumType.STRING)
	public IntervalUnit getCalFrequencyUnit() {
		return calFrequencyUnit;
	}

    public void setCalFrequencyUnit(IntervalUnit calFrequencyUnit) {
        this.calFrequencyUnit = calFrequencyUnit;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressid")
    public Address getAdd() {
        return this.add;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addedby")
    public Contact getAddedBy() {
        return this.addedBy;
    }

    @Column(name = "addedon", columnDefinition = "date")
    public LocalDate getAddedOn() {
        return this.addedOn;
    }

    /**
     * @return the asset
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assetid")
    public PhysicalAsset getAsset() {
        return this.asset;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "baseunitid")
    public Instrument getBaseUnit() {
        return this.baseUnit;
    }

    @NotNull(message = "Calibration frequency must be a number, e.g. 12")
    @Column(name = "calfrequency", nullable = false)
    public Integer getCalFrequency() {
        return this.calFrequency;
    }

    @OneToMany(mappedBy = "inst", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @SortComparator(StandardUsedComparator.class)
    public Set<StandardUsed> getCalibrations() {
        return this.calibrations;
    }

    @Column(name = "permittednumberofuses")
    public Integer getPermittedNumberOfUses() {
        return this.permittedNumberOfUses;
    }

    @Column(name = "usessincelastcalibration")
    public Integer getUsesSinceLastCalibration() {
        return this.usesSinceLastCalibration;
    }

    @OneToMany(mappedBy = "instrument", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @SortNatural
    public SortedSet<InstrumentCharacteristic> getCharacteristics() {
        return characteristics;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coid", nullable = false)
    public Company getComp() {
        return this.comp;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personid")
    @NotNull(message = "{error.instrument.contact.notnull}")
    public Contact getCon() {
        return this.con;
    }

    @Override
    @Transient
    public Contact getDefaultContact() {
        return this.con;
    }

    /**
     * This method returns the complete description for the instrument
     *
     * @return {@link String} complete model for instrument
     */
    @Transient
    public String getDefinitiveInstrument() {
        // Typology intentionally hid on instrument, as this method is used on the
        // customer portal and internally!
        return InstModelTools.instrumentModelNameViaTranslations(this, true, LocaleContextHolder.getLocale(),
                Locale.UK);
    }

    /**
     * This method returns the correct Mfr name for the instrument, / if mfr is null
     * due to lack of validation of manufacturer of instruments / setting of "/"
     * manufacturer
     *
     * @return Mfr name
     */
    @Transient
    public String getDefinitiveMfrName() {
        Mfr definitiveMfr = getDefinitiveMfr();
        if (definitiveMfr != null) {
            return definitiveMfr.getName();
        } else {
            return "/";
        }
    }

    /**
     * This method returns the correct {@link Mfr} for the instrument Made
     * private due to potential null mfr for incomplete instruments in database.
     *
     * @return {@link Mfr}
     */
    @Transient
    private Mfr getDefinitiveMfr() {
        if (this.model.getModelMfrType().isMfrRequiredForModel()) {
            return this.model.getMfr();
        } else {
            return this.mfr;
        }
    }

    @Override
    @Transient
    public File getDirectory() {
        return this.directory;
    }

    /**
     * created this method for testing purposes so method can be called from page.
     */
    @Transient
    public Integer getFinalMonthOfCalTimescalePercentage() {
        return this.getFinalMonthOfCalTimescalePercentage(null);
    }

    /**
     * added date parameter to this method so a custom date can be passed when
     * testing.
     *
     * @param date custom date passed when testing
     */
    @Transient
    public Integer getFinalMonthOfCalTimescalePercentage(LocalDate date) {
        if (this.nextCalDueDate != null) {
            // get todays date or custom date
            LocalDate todaysDate = date != null ?
                date : LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());

            // get date one month before calibration due date
            LocalDate timescaleWarningStartDate = nextCalDueDate.minusMonths(1);

            // unit is out of calibration
            if (this.isOutOfCalibration(date)) {
                this.finalMonthOfCalTimescalePercentage = 100;
            }
            // todays date is less than date one month before calibration due.
            else if (todaysDate.isBefore(timescaleWarningStartDate)) {
                this.finalMonthOfCalTimescalePercentage = 0;
            } else {

                // get number of days between warning start date and next cal
                // due
                // date
                long startTimescaleToCal = ChronoUnit.DAYS.between(timescaleWarningStartDate, nextCalDueDate);
                // get number of days between warning start date and todays date
                long startTimescaleToToday = ChronoUnit.DAYS.between(timescaleWarningStartDate, todaysDate);

                // work out percentage value to return
                this.finalMonthOfCalTimescalePercentage = ((int) startTimescaleToToday * 100)
                        / (int) startTimescaleToCal;
                // assign default percentage value for display
                if (this.finalMonthOfCalTimescalePercentage < 20) {
                    this.finalMonthOfCalTimescalePercentage = 20;
                }
            }
        } else {
            this.finalMonthOfCalTimescalePercentage = null;
        }

        return this.finalMonthOfCalTimescalePercentage;
    }
    
    /**
     * Intentionally NOT using Lombok's toString() 
     * method as ALL fields are exposed including 
     * complex / buggy implementations like timescale 
     */
    @Override
    public String toString() {
    	return String.valueOf(this.getPlantid());
    }

    @Length(max = 100)
    @Column(name = "firmwareaccesscode", length = 100)
    public String getFirmwareAccessCode() {
        return this.firmwareAccessCode;
    }

    /**
     * Changed from 1:1 to 1:n 2019-02-05 GB; future HireInstrument allocated by company + allow inactivation
     */
    @OneToMany(mappedBy = "inst", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<HireInstrument> getHireInstruments() {
        return this.hireInstruments;
    }

    @OneToMany(mappedBy = "instrument", cascade = CascadeType.ALL)
    @OrderBy("changeDate desc")
    public Set<InstrumentHistory> getHistory() {
        return this.history;
    }

    @Override
    @Transient
    public String getIdentifier() {
        return String.valueOf(this.plantid);
    }

    /**
     * created this method for testing purposes so method can be called from page.
     */
    @Transient
    public Integer getInCalTimescalePercentage() {
        return this.getInCalTimescalePercentage(null);
    }

    /**
     * added date parameter to this method so a custom date can be passed when
     * testing.
     *
     * @param date custom date passed when testing
     */
    @Transient
    public Integer getInCalTimescalePercentage(LocalDate date) {
        if (this.nextCalDueDate != null) {
            // get todays date or custom date
            val todaysDate = date != null ? date : LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());

            // get date one month before calibration due date
            LocalDate timescaleWarningStartDate = this.nextCalDueDate.minusMonths(1);

            // unit is out of calibration or todays date is after date one month
            // before calibration due.
            if (this.isOutOfCalibration(date) || todaysDate.isAfter(timescaleWarningStartDate)) {
                this.inCalTimescalePercentage = 100;
            } else {
                // get date from which calibration timescale is worked out
                val startTimescale = (this.lastCal != null) && (this.lastCal.getCompleteTime() != null) ? dateToLocalDate(this.lastCal.getCompleteTime()) :
                    this.addedOn != null ? this.addedOn : this.nextCalDueDate.minusYears(1);


                // get number of days between last cal and date one month before
                // calibration due
                long startTimescaleToWarn = ChronoUnit.DAYS.between(startTimescale, timescaleWarningStartDate);
                // get number of days between last cal and todays date
                long startTimescaleToToday = ChronoUnit.DAYS.between(startTimescale, todaysDate);
                // work out percentage value to return
                this.inCalTimescalePercentage = 10;
                if (startTimescaleToWarn != 0) {
                    this.inCalTimescalePercentage = ((int) startTimescaleToToday * 100) / (int) startTimescaleToWarn;
                }

                // assign default percentage value for display
                if (this.inCalTimescalePercentage < 10) {
                    this.inCalTimescalePercentage = 10;
                }
            }
        } else {
            this.inCalTimescalePercentage = null;
        }

        return this.inCalTimescalePercentage;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "inst")
    @SortComparator(InstCertLinkComparator.class)
    public Set<InstCertLink> getInstCertLinks() {
        return this.instCertLinks;
    }

    /**
     * this method has been put in to get a count of all instrument history entries.
     * added here as it will be used on the website as well as CWMS2.
     *
     * @return Integer count of entries
     */
    @Transient
    public Integer getInstrumentHistoryTotalCount() {
        int ihcount = 0;

        if (this.getHistory() != null) {
            if (this.getHistory().size() > 0) {
                for (InstrumentHistory ih : this.getHistory()) {
                    if (ih.isAddressUpdated()) {
                        ihcount++;
                    }
                    if (ih.isContactUpdated()) {
                        ihcount++;
                    }
                    if (ih.isPlantNoUpdated()) {
                        ihcount++;
                    }
                    if (ih.isSerialNoUpdated()) {
                        ihcount++;
                    }
                    if (ih.isStatusUpdated()) {
                        ihcount++;
                    }
                }
            }
        }

        return ihcount;
    }

    @OneToMany(mappedBy = "instrument", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @SortComparator(InstrumentValidationIssueComparator.class)
    public Set<InstrumentValidationIssue> getIssues() {
        return this.issues;
    }

    @OneToMany(mappedBy = "inst", fetch = FetchType.LAZY)
    @SortComparator(JobItemComparator.class)
    public Set<JobItem> getJobItems() {
        return this.jobItems;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lastcalid")
    public Calibration getLastCal() {
        return this.lastCal;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "locationid")
    public Location getLoc() {
        return this.loc;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mfrid")
    public Mfr getMfr() {
        return this.mfr;
    }

    @Column(name = "modelname")
    public String getModelname() {
        return modelname;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modelid", nullable = false)
    public InstrumentModel getModel() {
        return this.model;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "baseUnit")
    @SortComparator(InstrumentModuleComparator.class)
    public Set<Instrument> getModules() {
        return this.modules;
    }

    @Column(name = "recalldate", columnDefinition = "date")
    public LocalDate getNextCalDueDate() {
        return this.nextCalDueDate;
    }

    @Override
    @OneToMany(mappedBy = "instrument", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @SortComparator(NoteComparator.class)
    public Set<InstrumentNote> getNotes() {
        return this.notes;
    }

    @Override
    @Transient
    public ComponentEntity getParentEntity() {
        return null;
    }

    /**
     * @return the plantid
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "plantid", nullable = false)
    @Type(type = "int")
    public int getPlantid() {
        return this.plantid;
    }

    @Length(max = 50, message = "length must be between 0 and 50")
    @Column(name = "plantno", length = 50)
    public String getPlantno() {
        return this.plantno;
    }

    @Transient
    public Integer getPrivateActiveNoteCount() {
        return NoteCountCalculator.getPrivateActiveNoteCount(this);
    }

    @Transient
    public Integer getPrivateDeactivatedNoteCount() {
        return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
    }

    @Transient
    public Integer getPublicActiveNoteCount() {
        return NoteCountCalculator.getPublicActiveNoteCount(this);
    }

    @Transient
    public Integer getPublicDeactivatedNoteCount() {
        return NoteCountCalculator.getPublicDeactivedNoteCount(this);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId")
    public Capability getCapability() {
        return this.capability;
    }

    @OneToMany(mappedBy = "inst", fetch = FetchType.LAZY)
    // @SortComparator(JobItemComparator.class)
    public Set<Quotationitem> getQuoteItems() {
        return this.quoteItems;
    }

    @SortComparator(RangeComparator.class)
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "instrument", orphanRemoval = true)
    public Set<InstrumentRange> getRanges() {
        return this.ranges;
    }

    @OneToMany(mappedBy = "instrument", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @SortComparator(RecallItemByDateComparator.class)
    public Set<RecallItem> getRecalls() {
        return this.recalls;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "replacedby")
    public Contact getReplacedBy() {
        return this.replacedBy;
    }

    @Column(name = "replacedon", columnDefinition = "date")
    public LocalDate getReplacedOn() {
        return this.replacedOn;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "replacementid")
    public Instrument getReplacement() {
        return this.replacement;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "replacement")
    public Set<Instrument> getReplacementTo() {
        return this.replacementTo;
    }

    @Length(max = 500)
    @Column(name = "requirement", length = 300)
    public String getRequirement() {
        return this.requirement;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scrappedby")
    public Contact getScrappedBy() {
        return this.scrappedBy;
    }

    public void setReplacedOn(LocalDate replacedOn) {
        this.replacedOn = replacedOn;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scrappedonji")
    public JobItem getScrappedOnJI() {
        return this.scrappedOnJI;
    }

    @Override
    @Transient
    public List<Email> getSentEmails() {
        return null;
    }

    @Length(max = 50, message = "Serial No. must be between 0 and 50 characters")
    @Column(name = "serialno", length = 50)
    public String getSerialno() {
        return this.serialno;
    }

    @OneToMany(mappedBy = "instrument", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<DefaultStandard> getStandards() {
        return this.standards;
    }

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "statusid", nullable = false)
    public InstrumentStatus getStatus() {
        return this.status;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "storageid")
    public InstrumentStorageType getStorageType() {
        return this.storageType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "threadid")
    public Thread getThread() {
        return this.thread;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usageid")
    public InstrumentUsageType getUsageType() {
        return this.usageType;
    }

    @Column(name = "scrappedon", columnDefinition = "date")
    public LocalDate getScrappedOn() {
        return this.scrappedOn;
    }

    @ManyToMany(mappedBy = "instruments", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<WorkInstruction> getWorkInstructions() {
        return this.workInstructions;
    }

    @ManyToMany(mappedBy = "instruments", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<Contract> getContracts() {
        return this.contracts;
    }

    @Override
    @Transient
    public boolean isAccountsEmail() {
        return false;
    }

    @NotNull
    @Column(name = "calexpiresafternumberofuses", nullable = false, columnDefinition = "tinyint")
    public Boolean getCalExpiresAfterNumberOfUses() {
        return this.calExpiresAfterNumberOfUses;
    }

    @Column(name = "calibrationStandard", nullable = false, columnDefinition = "tinyint")
    // @Type(type = "boolean")
    public Boolean getCalibrationStandard() {
        return this.calibrationStandard;
    }

    /**
     * created for testing purposes.
     */
    @Transient
    public boolean isOutOfCalibration() {
        return this.isOutOfCalibration(null);
    }

    /**
     * Calculates if this {@link Instrument} is out of calibration i.e. it's next
     * cal due date is past the current date. Instrument enties
     */
    @Transient
    public boolean isOutOfCalibration(LocalDate date) {
        boolean outOfCalibration = false;
        if (this.nextCalDueDate != null) {
            if (date != null) {
                outOfCalibration = this.nextCalDueDate.isBefore(date);
            } else {
                outOfCalibration = this.nextCalDueDate.isBefore(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
            }
        }
        return outOfCalibration;
    }

    @Column(name = "recallednotreceived", columnDefinition = "tinyint")
    public Boolean getRecalledNotReceived() {
        return this.recalledNotReceived;
    }

    @Column(name = "scrapped", columnDefinition = "tinyint")
    public Boolean getScrapped() {
        return this.scrapped;
    }

    @OneToMany(cascade = {
            CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "instrument", targetEntity = InstrumentValue.class)
    public Set<InstrumentValue> getFlexibleFields() {
        return this.flexibleFields;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "instrument")
    public Set<MeasurementPossibility> getMeasurementPossibilities() {
        return this.measurementPossibilities;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "measurementtype", columnDefinition = "nvarchar(2000)")
    public MeasurementType getMeasurementType() {
        return this.measurementType;
    }

    @Transient
    public InstrumentComplementaryField getInstrumentComplementaryField() {
        //return this.instrumentComplementaryField;
        InstrumentComplementaryField result = null;
        if (this.instrumentComplementaryFields != null && !this.instrumentComplementaryFields.isEmpty()) {
            result = this.instrumentComplementaryFields.iterator().next();
        }
        return result;
    }

    /**
     * use get/setInstrumentComplementaryField for normal use.
     * This OneToMany proxies the association into a Collection, to prevent the lazy loading of
     * InstrumentComplementaryField when Instrument is initialized.
     *
     */
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "instrument", fetch = FetchType.LAZY)
    public Set<InstrumentComplementaryField> getInstrumentComplementaryFields() {
        return this.instrumentComplementaryFields;
    }

    @Transient
    public void setInstrumentComplementaryField(InstrumentComplementaryField instrumentComplementaryField) {
        if (this.instrumentComplementaryFields == null) this.instrumentComplementaryFields = new HashSet<>();
        instrumentComplementaryFields.clear();
        if (instrumentComplementaryField != null) {
            instrumentComplementaryFields.add(instrumentComplementaryField);
        }
    }


    @Column(name = "customermanaged", columnDefinition = "bit")
    public Boolean getCustomerManaged() {
        return customerManaged;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "defaultservicetype")
    public ServiceType getDefaultServiceType() {
        return defaultServiceType;
    }

	@Column(name = "formerbarcode", columnDefinition = "nvarchar(50)")
    public String getFormerBarCode() {
        return this.formerBarCode;
    }

    @Column(name = "customerdescription", columnDefinition = "nvarchar(2000)")
    public String getCustomerDescription() {
        return this.customerDescription;
    }

    @Column(name = "customerspecification", columnDefinition = "nvarchar(100)")
    public String getCustomerSpecification() {
        return this.customerSpecification;
    }

	@Column(name = "freeTextLocation", columnDefinition = "nvarchar(64)")
    public String getFreeTextLocation() {
        return this.freeTextLocation;
    }

	@Column(name = "clientbarcode", columnDefinition = "nvarchar(50)")
    public String getClientBarcode() {
        return this.clientBarcode;
    }

    @Column(name = "defaultClientRef", columnDefinition = "varchar(30)")
	public String getDefaultClientRef() {
		return defaultClientRef;
	}
    
    public void setPlantno(String plantno) {
        if (plantno != null) {
            setSimplifiedPlantNo(InstrumentUtils.simplifyId(plantno));
            this.plantno = plantno.trim().toUpperCase();
        } else {
            this.plantno = null;
        }
    }


    @Override
    public void setSentEmails(List<Email> emails) {

    }

    public void setSerialno(String serialno) {
        setSimplifiedSerialNo(InstrumentUtils.simplifyId(serialno));
        this.serialno = serialno != null ? serialno.trim() : null;
    }


}