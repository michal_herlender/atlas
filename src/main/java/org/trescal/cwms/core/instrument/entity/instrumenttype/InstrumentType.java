package org.trescal.cwms.core.instrument.entity.instrumenttype;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

@MappedSuperclass
public abstract class InstrumentType
{
	private boolean defaultType;
	private String fullDescription;
	private int id;
	private String name;
	private boolean recall;

	@NotNull
	@Length(min = 1, max = 200)
	@Column(name = "fulldescription", length = 200, nullable = false)
	@Deprecated
	public String getFullDescription()
	{
		return this.fullDescription;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "typeid", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "name", length = 50, nullable = false)
	@Deprecated
	public String getName()
	{
		return this.name;
	}

	@NotNull
	@Column(name = "recall", nullable = false, columnDefinition="bit")
	public boolean getRecall()
	{
		return this.recall;
	}

	@NotNull
	@Column(name = "defaulttype", nullable = false, columnDefinition="bit")
	public boolean isDefaultType()
	{
		return this.defaultType;
	}

	public void setDefaultType(boolean defaultType)
	{
		this.defaultType = defaultType;
	}

	public void setFullDescription(String fullDescription)
	{
		this.fullDescription = fullDescription;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setRecall(boolean recall)
	{
		this.recall = recall;
	}
}