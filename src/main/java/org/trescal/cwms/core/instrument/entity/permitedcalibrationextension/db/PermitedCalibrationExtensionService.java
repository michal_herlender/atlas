package org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.controller.CalExtensionDto;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.permitedcalibrationextension.PermitedCalibrationExtension;

public interface PermitedCalibrationExtensionService extends BaseService<PermitedCalibrationExtension,Integer> {
   
	CalExtensionDto insertOrUpdateAjax(CalExtensionDto calExtensionDto);

    PermitedCalibrationExtension getExtensionForInstrument(Instrument instrument);

    CalExtensionDto getAjax(Integer plantId);

	CalExtensionDto deleteAjax(Integer plantId);

    void refreshExtensionEndDate(Instrument instrument);

}
