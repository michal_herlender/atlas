package org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;
import org.trescal.cwms.core.instrument.form.InstrumentIssueValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.web.EmailContent_InstrumentValidation;
import org.trescal.cwms.core.system.entity.status.Status;
import org.trescal.cwms.core.system.entity.status.db.StatusService;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@Service("InstrumentValidationIssueService")
public class InstrumentValidationIssueServiceImpl implements InstrumentValidationIssueService {
	@Autowired
	private EmailContent_InstrumentValidation ecInstrumentValidation;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private InstrumentValidationIssueDao InstrumentValidationIssueDao;
	@Autowired
	private InstrumService instrumServ;
	@Autowired
	private UserService userService;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private InstrumentIssueValidator val;
	@Value("${cwms.admin.email}")
	private String adminEmail;
	
	@Override
	public void deleteInstrumentValidationIssue(InstrumentValidationIssue instrumentvalidationissue)
	{
		this.InstrumentValidationIssueDao.remove(instrumentvalidationissue);
	}

	@Override
	public InstrumentValidationIssue findInstrumentValidationIssue(int id)
	{
		return this.InstrumentValidationIssueDao.find(id);
	}

	@Override
	public List<InstrumentValidationIssue> getAllInstrumentValidationIssues()
	{
		return this.InstrumentValidationIssueDao.findAll();
	}

	@Override
	public void insertInstrumentValidationIssue(InstrumentValidationIssue InstrumentValidationIssue)
	{
		this.InstrumentValidationIssueDao.persist(InstrumentValidationIssue);
	}

	@Override
	public void saveOrUpdateInstrumentValidationIssue(InstrumentValidationIssue instrumentvalidationissue) {
		this.InstrumentValidationIssueDao.persist(instrumentvalidationissue);
	}

	@Override
	public void updateInstrumentValidationIssue(InstrumentValidationIssue InstrumentValidationIssue) {
		this.InstrumentValidationIssueDao.merge(InstrumentValidationIssue);
	}

	@Override
	public ResultWrapper webInsertValidationIssue(int plantid, String issue, HttpSession session)
	{
		Instrument inst = this.instrumServ.get(plantid);

		if (inst != null)
		{
			Status status = this.statusServ.findStatusByName("New", InstrumentValidationStatus.class);

			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact contact = this.userService.getEagerLoad(username).getCon();
			Contact businessContact = contact.getSub().getComp().getDefaultBusinessContact();
			Locale locale = contact.getLocale();

			InstrumentValidationIssue instValIssue = new InstrumentValidationIssue();

			instValIssue.setIssue(issue);
			instValIssue.setRaisedBy(contact);
			instValIssue.setRaisedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			instValIssue.setInstrument(inst);
			instValIssue.setStatus((InstrumentValidationStatus) status);

			BindException errors = new BindException(instValIssue, "iss");
			this.val.validate(instValIssue, errors);

			if (!errors.hasErrors()) {
				this.InstrumentValidationIssueDao.persist(instValIssue);
				EmailContentDto contentDto = this.ecInstrumentValidation.getContent(instValIssue, locale);

				boolean success = this.emailServ.sendAdvancedEmail(contentDto.getSubject(), adminEmail, Collections.singletonList(businessContact.getEmail()), Collections.singletonList(contact.getEmail()),
					Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), contentDto.getBody(), true, false);

				if (success) {
					return new ResultWrapper(true, "", instValIssue, null);
				} else {
					return new ResultWrapper(false, "Error occured sending email validation request", null, null);
				}
			}
			else
			{
				return new ResultWrapper(false, "", instValIssue, errors);
			}
		}
		else
		{
			return new ResultWrapper(false, "The instrument requested for validation could not be found", null, null);
		}
	}
}