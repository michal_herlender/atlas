package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Set;

public interface InstrumentFieldLibraryValueService extends BaseService<InstrumentFieldLibraryValue,Integer> {

    /**
     * Returns all possible library values for a particular field definition as a set of key value pairs
     * @param fieldDefinitionId
     * @param instrumentFieldValueId
     * @return
     */
    public Set<KeyValue<Integer,String>> getLibraryValuesForDefinitionAsKeyValue(Integer fieldDefinitionId,Integer instrumentFieldValueId);

}
