package org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.db;

import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrumentvalidationstatus.InstrumentValidationStatus;

public interface InstrumentValidationStatusService
{
	InstrumentValidationStatus findInstrumentValidationStatus(int id);
	void insertInstrumentValidationStatus(InstrumentValidationStatus instrumentvalidationstatus);
	void updateInstrumentValidationStatus(InstrumentValidationStatus instrumentvalidationstatus);
	List<InstrumentValidationStatus> getAllInstrumentValidationStatuss();
	void deleteInstrumentValidationStatus(InstrumentValidationStatus instrumentvalidationstatus);
	void saveOrUpdateInstrumentValidationStatus(InstrumentValidationStatus instrumentvalidationstatus);
}