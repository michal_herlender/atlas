package org.trescal.cwms.core.instrument.entity.checkout.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.instrument.entity.checkout.dto.CheckOutDto;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

public interface CheckOutDao extends BaseDao<CheckOut, Integer> {

	public CheckOut checkIfInstrumentIsCheckOut(Instrument inst, Job job);

	public List<CheckOut> getCheckOutInstrumentsByTechnicien(Contact technicien);

	public List<CheckOut> getCheckInInstrumentsByTechnicienWithNoConfidenceCheck(Contact technicien);

	public List<CheckOut> searchCheckOutRecords(CheckOutDto dto, Instrument inst, Job job);
	
	public List<CheckOut> getCheckOutRecordsByInstrument(Instrument inst);
}
