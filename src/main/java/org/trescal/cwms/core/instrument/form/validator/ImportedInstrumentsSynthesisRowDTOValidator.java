package org.trescal.cwms.core.instrument.form.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.ImportedInstrumentSynthesisForm.ImportedInstrumentsSynthesisRowDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Component
public class ImportedInstrumentsSynthesisRowDTOValidator extends AbstractBeanValidator implements SmartValidator {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private MfrService mfrService;
    @Autowired
    private InstrumentModelFamilyService instrumentModelFamilyService;
    @Autowired
    private InstrumentModelService instrumentModelService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private InstrumService instrumentService;
    @Autowired
    private NewDescriptionService descriptionService;
    @Autowired
    private ServiceTypeService serviceTypeService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(ImportedInstrumentsSynthesisRowDTO.class);
    }

	@Override
	public void validate(@NotNull Object target, @NotNull Errors errors, Object @NotNull ... validationHints) {

		ImportedInstrumentsSynthesisRowDTO dto = (ImportedInstrumentsSynthesisRowDTO) target;

		Subdiv sub = (Subdiv) validationHints[0];

		Boolean isAllowedToCreatInst = (Boolean) validationHints[1];
		Boolean isAllowedToCreateIntrumentWithoutSerialNo = (Boolean) validationHints[2];

		/*
		 * check if instrument exists based on : if instrument exists with only
		 * the plantno => warning if instrument exists with only the serialno
		 * =>warning if instrument exists with both the plantno and serialno =>
		 * error
		 */
		dto.setWarningMessages(new ArrayList<>());
		Company clientCompany = sub.getComp();
		if (!isAllowedToCreateIntrumentWithoutSerialNo && StringUtils.isBlank(dto.getSerialno()))
			errors.rejectValue("serialno", "error.instrument.serialno");
		if (!isAllowedToCreatInst && StringUtils.isBlank(dto.getPlantno()))
			errors.rejectValue("plantno", "error.instrument.plantno");

		if (StringUtils.isBlank(dto.getPlantno())) {
			if (instrumentService.checkExists(dto.getPlantno(), dto.getSerialno(), clientCompany.getCoid(), null)) {
				// Instrument you are trying to add have potential duplicates the company, please review before adding!
				dto.getWarningMessages().add(messageSource.getMessage("error.instrument.validate.duplicate.company",
					new String[]{""}, LocaleContextHolder.getLocale()));
			} else if (clientCompany.getCompanyGroup() != null && instrumentService.checkExists(dto.getPlantno(), dto.getSerialno(), null, clientCompany.getCompanyGroup().getId())) {
				// Instrument you are trying to add have potential duplicates the group company, please review before adding!
				dto.getWarningMessages().add(messageSource.getMessage("error.instrument.validate.duplicate.companygroup",
					new String[]{clientCompany.getCompanyGroup().getGroupName()}, LocaleContextHolder.getLocale()));
			}
		}
		else if(instrumentService.checkExists(dto.getPlantno(), dto.getSerialno(), clientCompany.getCoid(),null)) {
			if(StringUtils.isNoneBlank(dto.getSerialno())){
				errors.rejectValue("plantno", "error.instrument.validate.serialnoandplantno.exists");
			} else {
				dto.getWarningMessages().add(messageSource.getMessage("error.instrument.validate.duplicate.company",
					new String[] {""}, LocaleContextHolder.getLocale()));
			}
		} 
		else if(clientCompany.getCompanyGroup() != null && instrumentService.checkExists(dto.getPlantno(), dto.getSerialno(), null, clientCompany.getCompanyGroup().getId())) {
			dto.getWarningMessages().add(messageSource.getMessage("error.instrument.validate.duplicate.companygroup",
				new String[] {clientCompany.getCompanyGroup().getGroupName()}, LocaleContextHolder.getLocale()));
		}
		
		// brand validation --
		if (dto.getTmlBrandId() != null) {
			Mfr mfr = mfrService.findTMLMfr(dto.getTmlBrandId());
			if (mfr == null) {
				errors.rejectValue("tmlBrandId", "error.rest.data.notfound",
						new String[] { dto.getTmlBrandId().toString() }, null);
			}
		} else if (StringUtils.isNoneBlank(dto.getBrand())) {
			Mfr mfr = mfrService.findMfrByName(dto.getBrand());
			if (mfr == null) {
				errors.rejectValue("brand", "error.rest.data.notfound", new String[] { "" + dto.getBrand() }, null);
			}
		}

		// instrument model --
		if (dto.getTmlInstrumentModelId() != null) {
			InstrumentModel instrumentModel = instrumentModelService
					.getTMLInstrumentModel(dto.getTmlInstrumentModelId());
			if (instrumentModel == null) {
				errors.rejectValue("tmlInstrumentModelId", "error.rest.data.notfound",
						new String[] { dto.getTmlInstrumentModelId().toString() }, null);
			}
		} else if (dto.getInstrumentModelId() != null) {
			InstrumentModel instrumentModel = instrumentModelService.get(dto.getInstrumentModelId());
			if (instrumentModel == null) {
				errors.rejectValue("instrumentModelId", "error.rest.data.notfound",
						new String[] { dto.getInstrumentModelId().toString() }, null);
			}
		}

		if (dto.getInstrumentModelId() == null) {
			errors.rejectValue("instrumentModelId", "error.rest.data.notfound", new String[] { "" }, null);
		}

		// family --
		if (dto.getTmlFamilyId() != null) {
			InstrumentModelFamily instrumentModelFamily = instrumentModelFamilyService
					.getTMLFamily(dto.getTmlFamilyId());
			if (instrumentModelFamily == null) {
				errors.rejectValue("tmlFamilyId", "error.rest.data.notfound",
						new String[] { dto.getTmlFamilyId().toString() }, null);
			}
		} else if (dto.getFamilyId() != null) {
			InstrumentModelFamily instrumentModelFamily = instrumentModelFamilyService.getFamily(dto.getFamilyId());
			if (instrumentModelFamily == null) {
				errors.rejectValue("familyId", "error.rest.data.notfound",
						new String[] { dto.getFamilyId().toString() }, null);
			}
		}

		// Sub-family --
		if (dto.getTmlSubFamilyId() != null) {
			Description description = descriptionService.getTMLDescription(dto.getTmlSubFamilyId());
			if (description == null) {
				errors.rejectValue("tmlSubFamilyId", "error.rest.data.notfound",
						new String[] { dto.getTmlSubFamilyId().toString() }, null);
			}
		} else if (dto.getSubFamilyId() != null) {
			Description description = descriptionService.findDescription(dto.getSubFamilyId());
			if (description == null) {
				errors.rejectValue("tmlSubFamilyId", "error.rest.data.notfound",
						new String[] { dto.getSubFamilyId().toString() }, null);
			}
		}

		// default procedure
        if (StringUtils.isNoneBlank(dto.getDefaultProcedureReference())) {
            Capability capability = capabilityService
                .getCapabilityByReference(dto.getDefaultProcedureReference(), null, null).stream().findFirst()
                .orElse(null);
            if (capability == null) {
                errors.rejectValue("defaultProcedureReference", "error.rest.data.notfound",
                    new String[]{dto.getDefaultProcedureReference()}, null);
            } else {
                dto.setDefaultProcedureId(capability.getId());
            }
        } else if (dto.getDefaultProcedureId() != null) {
            Capability capability = capabilityService.get(dto.getDefaultProcedureId());
            if (capability == null) {
                errors.rejectValue("defaultProcedureId", "error.rest.data.notfound",
                    new String[]{dto.getDefaultProcedureId().toString()}, null);
            } else {
                dto.setDefaultProcedureReference(capability.getReference());
            }
        }

		// service type
		if (dto.getDefaultServiceTypeId() != null) {
			ServiceType serviceType = serviceTypeService.get(dto.getDefaultServiceTypeId());
			if (serviceType == null) {
				errors.rejectValue("defaultServiceTypeId", "error.rest.data.notfound",
						new String[] { dto.getDefaultServiceTypeId().toString() }, null);
			}
		}

		if (StringUtils.isNoneBlank(dto.getInstrumentUsageType()) && dto.getInstrumentUsageTypeId() == null) {
			errors.rejectValue("instrumentUsageTypeId", "error.rest.data.notfound",
					new String[] { dto.getInstrumentUsageType() }, null);
		}

	}

}
