package org.trescal.cwms.core.instrument.entity.instrumentstoragetype.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;
import org.trescal.cwms.core.instrument.entity.instrumenttype.db.InstrumentTypeDaoImpl;

@Repository("InstrumentStorageTypeDao")
public class InstrumentStorageTypeDaoImpl extends InstrumentTypeDaoImpl<InstrumentStorageType> implements InstrumentStorageTypeDao {

	@Override
	protected Class<InstrumentStorageType> getEntity() {
		return InstrumentStorageType.class;
	}
}