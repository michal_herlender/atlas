package org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;

@Repository("InstrumentValidationIssueDao")
public class InstrumentValidationIssueDaoImpl extends BaseDaoImpl<InstrumentValidationIssue, Integer> implements InstrumentValidationIssueDao {
	
	@Override
	protected Class<InstrumentValidationIssue> getEntity() {
		return InstrumentValidationIssue.class;
	}
}