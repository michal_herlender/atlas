package org.trescal.cwms.core.instrument.entity.instrumentstoragetype.db;

import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType;

public interface InstrumentStorageTypeService
{
	void deleteInstrumentStorageType(InstrumentStorageType instrumentstoragetype);

	InstrumentStorageType findDefaultType(Class<?> clazz);

	InstrumentStorageType findInstrumentStorageType(int id);

	List<InstrumentStorageType> getAllInstrumentStorageTypes();

	void insertInstrumentStorageType(InstrumentStorageType instrumentstoragetype);

	void saveOrUpdateInstrumentStorageType(InstrumentStorageType instrumentstoragetype);

	void updateInstrumentStorageType(InstrumentStorageType instrumentstoragetype);
}