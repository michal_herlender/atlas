package org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;

public interface InstrumentValidationIssueDao extends BaseDao<InstrumentValidationIssue, Integer> {}