package org.trescal.cwms.core.instrument.entity.instrumentrange.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;

public interface InstrumentRangeDao extends BaseDao<InstrumentRange, Integer> {
}