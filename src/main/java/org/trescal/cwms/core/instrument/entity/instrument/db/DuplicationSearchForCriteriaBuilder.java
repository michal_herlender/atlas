package org.trescal.cwms.core.instrument.entity.instrument.db;

import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.audit.entity.db.CriteriaBuilderEffect;
import org.trescal.cwms.core.audit.entity.db.RootSelectRestrictionOrder;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.dto.InstrumentCheckDuplicatesDto;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;

import javax.persistence.criteria.*;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.function.Function;

import static org.trescal.cwms.core.tools.JpaUtils.calculateTypology;

public class DuplicationSearchForCriteriaBuilder {

    static CriteriaBuilderEffect<RootSelectRestrictionOrder<Instrument, InstrumentCheckDuplicatesDto>> buildCriteria(Integer coid) {
        return CriteriaBuilderEffect.lift((cb, cq) -> {
            val instrument = cq.from(Instrument.class);
            val subdiv = instrument.join(Instrument_.con).join(Contact_.sub);
            val company = subdiv.join(Subdiv_.comp);
            val model = instrument.join(Instrument_.model);
            val calibration = instrument.join(Instrument_.lastCal, JoinType.LEFT);
            val clauses = cb.or(
                    cb.equal(company, coid),
                    cb.equal(company.get(Company_.companyGroup), companyGroupSubQuery(cb, cq, coid))
            );
            val sq = cq.subquery(ZonedDateTime.class);
            val sqRoot = sq.from(JobItem.class);
            sq.where(cb.equal(sqRoot.get(JobItem_.inst), instrument));
            sq.select(cb.greatest(sqRoot.get(JobItem_.dateIn)));
            val selection = getSelection(cb, instrument, subdiv, company, model, calibration, sq, checkIfActive(cq, cb, instrument));
            val orders = new ArrayList<javax.persistence.criteria.Order>();
            orders.add(cb.desc(instrument.get(Instrument_.comp)));
            return RootSelectRestrictionOrder.of(instrument, selection, clauses, orders);
        });
    }

    private static Subquery<Integer> companyGroupSubQuery(CriteriaBuilder cb, CriteriaQuery<?> cq, Integer coid) {
        val scq = cq.subquery(Integer.class);
        val company = scq.from(Company.class);
        scq.where(cb.equal(company,coid));
        scq.select(company.get(Company_.companyGroup).get(CompanyGroup_.id));
        return scq;
    }

    public static Subquery<Boolean> checkIfActive(CriteriaQuery<?> cq, CriteriaBuilder cb, Root<Instrument> instrument) {
        val sq = cq.subquery(Boolean.class);
        val root = sq.from(JobItem.class);
        sq.where(cb.equal(root.get(JobItem_.inst), instrument));
        sq.select(cb.max(root.get(JobItem_.state).get(ItemState_.active).as(Integer.class)).as(Boolean.class));
        return sq;
    }

    public static CompoundSelection<InstrumentCheckDuplicatesDto> getSelection(CriteriaBuilder cb, Root<Instrument> instrument, Join<Contact, Subdiv> subdiv, Join<?, Company> company, Join<Instrument, InstrumentModel> model, Join<Instrument, Calibration> calibration, Subquery<ZonedDateTime> lastDateInSubquery, Subquery<Boolean> isActiveSq) {
        return cb.construct(InstrumentCheckDuplicatesDto.class, instrument.get(Instrument_.plantid),
            instrument.get(Instrument_.plantno), instrument.get(Instrument_.simplifiedPlantNo),
            instrument.get(Instrument_.serialno), instrument.get(Instrument_.simplifiedSerialNo),
            instrument.get(Instrument_.formerBarCode),
            subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname), company.get(Company_.groupName),
            translatedModelDescription(model).apply(cb),
            company.get(Company_.coid), company.get(Company_.coname),
            model.get(InstrumentModel_.modelid), generateDescription(instrument, model).apply(cb), lastDateInSubquery.getSelection(),
            calibration.get(Calibration_.calDate), instrument.get(Instrument_.nextCalDueDate), isActiveSq.getSelection(), cb.literal(0.0),
            instrument.get(Instrument_.calFrequency), instrument.get(Instrument_.calFrequencyUnit)
        );
    }


    public static Function<CriteriaBuilder, Expression<String>> generateDescription(From<?, Instrument> instrument, Join<Instrument, InstrumentModel> model) {

        val typology = model.join(InstrumentModel_.description, JoinType.LEFT)
                .get(Description_.typology);

        Effect<CriteriaBuilder, Expression<String>> translatedName = cb -> translatedModelName(model).apply(cb);
        val typologyS = calculateTypology(typology);
        Effect<CriteriaBuilder, Expression<String>> nameWithTypology = translatedName.flatMap(exp ->
                cb -> concatWithSpace(exp, typologyS.apply(cb)).apply(cb)
        );


        Effect<CriteriaBuilder, Expression<String>> result = nameWithTypology.flatMap(name -> cb -> {
                            val mfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
                            mfr.on(cb.isFalse(mfr.get(Mfr_.genericMfr)));
                            return concatWithSpace(name, cb.coalesce(mfr.get(Mfr_.name), "")).apply(cb);
                        }
                )
                .flatMap(r -> cb -> concatWithSpace(r,
                        cb.coalesce(instrument.get(Instrument_.modelname), "")).apply(cb))
                ;
            return nameWithTypology.flatMap(name -> result.flatMap(res ->
                cb -> cb.<String>selectCase().when(cb.equal(model.get(InstrumentModel_.modelMfrType), ModelMfrType.MFR_SPECIFIC), name).otherwise(res)
                    ));
            }


    private static Function<CriteriaBuilder, Expression<String>> translatedModelName(Join<?, InstrumentModel> model) {
        return cb -> {
            val locale = LocaleContextHolder.getLocale();
            val translation = model.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
            translation.on(cb.equal(translation.get(Translation_.locale), locale));
            return translation.get(Translation_.translation);
        };
    }

    private  static  Function<CriteriaBuilder,Expression<String>> translatedModelDescription(Join<?,InstrumentModel> model){
        return  cb -> {
            val locale = LocaleContextHolder.getLocale();
            val t = model.join(InstrumentModel_.description,JoinType.LEFT)
                    .join(Description_.translations,JoinType.LEFT);
            t.on(cb.equal(t.get(Translation_.locale),locale));
            return t.get(Translation_.translation);
        };
    }

    private static Function<CriteriaBuilder, Expression<String>> concatWithSpace(Expression<String> x, Expression<String> y) {
        return cb -> cb.trim(cb.concat(cb.concat(x, " "), y));
    }
}




interface Effect<R,A> extends Function<R,A> {
    default <B> Effect<R,B> map(Function<A,B> mapper){
        return Effect.of(this.andThen(mapper));
    }
    default <B> Effect<R,B> flatMap(Function<A,Effect<R,B>> mapper){
        return r ->  this.andThen(mapper).apply(r).apply(r);
    }

    static <U,T> Effect<U,T> lift(T t){
        return u -> t;
    }

    static <U,T> Effect<U,T> of(Function<U,T> f){
        return (Effect<U,T>)f;
    }
}