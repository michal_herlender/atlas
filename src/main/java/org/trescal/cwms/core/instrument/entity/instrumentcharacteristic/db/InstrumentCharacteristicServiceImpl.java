package org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.db;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;

@Service
public class InstrumentCharacteristicServiceImpl extends BaseServiceImpl<InstrumentCharacteristic, UUID>
		implements InstrumentCharacteristicService {
	
	@Autowired
	private InstrumentCharacteristicDao instrCharDao;
	
	@Override
	protected BaseDao<InstrumentCharacteristic, UUID> getBaseDao() {
		return instrCharDao;
	}
}