package org.trescal.cwms.core.instrument.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.InstrumentSearchResultsDTO;
import org.trescal.cwms.core.instrument.form.SearchInstrumentForm;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.util.Date;

/**
 * Displays form to search for {@link Instrument} entities and displays their
 * results.
 */
@Controller
@IntranetController
public class SearchInstrumentController {
	@Autowired
	private InstrumService instrumentService;

	@ModelAttribute("command")
	protected SearchInstrumentForm formBackingObject(
			@RequestParam(value="contactid", required=false) Integer personId,
			@RequestParam(value="mid", required=false) Integer modelId) throws Exception
	{
		SearchInstrumentForm form = new SearchInstrumentForm();
		form.setPersonid(personId);
		form.setModelid(modelId);
		return form;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@RequestMapping(value="/searchinstrument.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/instrument/searchinstrument";
	}

	@RequestMapping(value="/searchinstrument.htm", method=RequestMethod.GET, params = { "forced=forced" })
	protected String forcedPost(@ModelAttribute("command") SearchInstrumentForm form,
			@RequestParam(value = "contactid", required = false, defaultValue = "") Integer contactid,
			@RequestParam(value = "mid", required = false, defaultValue = "") Integer mid) throws Exception
	{
		return onSubmit(form, contactid, mid);

	}

	@RequestMapping(value="/searchinstrument.htm", method=RequestMethod.GET, params = { "fromquickscan=true" })
	protected String forcedPlantid(@ModelAttribute("command") SearchInstrumentForm form,
			@RequestParam(value = "capturedtext", required = false, defaultValue = "") String capturedtext) throws Exception
	{
		form.setPlantid(capturedtext);
		form.setResultsPerPage(Constants.RESULTS_PER_PAGE);
		form.setPageNo(1);
		return onSubmit(form, null, null);
	}
	
	@RequestMapping(value="/searchinstrument.htm", method=RequestMethod.POST)
	public String onSubmit(@ModelAttribute("command") SearchInstrumentForm form,
			@RequestParam(value = "contactid", required = false, defaultValue = "") Integer contactid,
			@RequestParam(value = "mid", required = false, defaultValue = "") Integer mid) throws Exception
	{
		if(contactid != null && contactid > 0){
			form.setPersonid(contactid);
		}
		form.setModelid(mid);
		PagedResultSet<InstrumentSearchResultsDTO> rs = this.instrumentService.searchInstrumentsPaged(
                new PagedResultSet<>(form.getResultsPerPage(), form.getPageNo()), form);
        form.setRs(rs);
        form.setInstruments(rs.getResults());

		return "trescal/core/instrument/searchinstrumentresults";
	}
}