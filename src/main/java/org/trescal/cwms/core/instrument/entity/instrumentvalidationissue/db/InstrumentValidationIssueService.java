package org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.db;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;

public interface InstrumentValidationIssueService
{
	InstrumentValidationIssue findInstrumentValidationIssue(int id);

	void insertInstrumentValidationIssue(InstrumentValidationIssue instrumentvalidationissue);

	void updateInstrumentValidationIssue(InstrumentValidationIssue instrumentvalidationissue);

	List<InstrumentValidationIssue> getAllInstrumentValidationIssues();

	void deleteInstrumentValidationIssue(InstrumentValidationIssue instrumentvalidationissue);

	void saveOrUpdateInstrumentValidationIssue(InstrumentValidationIssue instrumentvalidationissue);

	ResultWrapper webInsertValidationIssue(int plantid, String issue, HttpSession session);

}