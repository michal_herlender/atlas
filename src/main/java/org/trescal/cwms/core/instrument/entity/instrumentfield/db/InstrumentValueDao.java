package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;

public interface InstrumentValueDao extends BaseDao<InstrumentValue, Integer> {

}
