package org.trescal.cwms.core.instrument.entity.instrument.db;

public class InstrumentUtils {

    static public String simplifyId(String id) {
        return id == null ? "" : id.replaceAll("[\\.\\-\\+_\\(\\)\\]\\{\\}\\[\\]\\s\\/\\\\]", "")
                .replaceAll("(?i)i", "1")
                .replaceAll("(?i)o", "0")
                .replaceAll("(?i)z", "2")
                .replaceAll("(?i)l", "1")
                .replaceAll("(?i)s", "5")
                .replaceAll("^0+", "")
                ;
    }
}
