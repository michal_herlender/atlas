package org.trescal.cwms.core.instrument.entity.instrumentfield.db;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueSelection;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValueSelection_;
import org.trescal.cwms.core.instrument.entity.instrumentfield.dto.InstrumentValueSelectionDTO;

@Repository
public class InstrumentValueSelectionDaoImpl extends BaseDaoImpl<InstrumentValueSelection, Integer> implements InstrumentValueSelectionDao {
	
	@Override
	protected Class<InstrumentValueSelection> getEntity() {
		return InstrumentValueSelection.class;
	}
	
	@Override
	public List<InstrumentValueSelectionDTO> getAll(InstrumentFieldDefinition fieldDefinition) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<InstrumentValueSelectionDTO> cq = cb.createQuery(InstrumentValueSelectionDTO.class);
		Root<InstrumentValueSelection> val = cq.from(InstrumentValueSelection.class);
		Join<InstrumentValueSelection, Instrument> instr = val.join(InstrumentValueSelection_.instrument);
		Join<InstrumentValueSelection, InstrumentFieldDefinition> field = val.join(InstrumentValueSelection_.instrumentFieldDefinition);
		Join<InstrumentValueSelection, InstrumentFieldLibraryValue> libVal = val.join(InstrumentValueSelection_.instrumentFieldLibraryValue);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(val.get(InstrumentValueSelection_.instrumentFieldDefinition), fieldDefinition));
		cq.select(cb.construct(InstrumentValueSelectionDTO.class,
				instr.get(Instrument_.plantid),
				field.get(InstrumentFieldDefinition_.name),
				libVal.get(InstrumentFieldLibraryValue_.name)));
		cq.where(clauses);
		TypedQuery<InstrumentValueSelectionDTO> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Instrument> getMatchingInstruments(InstrumentFieldDefinition fieldDefinition, InstrumentFieldLibraryValue value) {
		Criteria criteria = getSession()
				.createCriteria(InstrumentValueSelection.class).
				add(Restrictions.eq("instrumentFieldDefinition",fieldDefinition)).
				add(Restrictions.eq("instrumentFieldLibraryValue", value)).
				setProjection(Projections.property("instrument"));
		return criteria.list();
	}
}