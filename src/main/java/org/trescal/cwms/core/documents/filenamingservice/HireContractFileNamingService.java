package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.tools.DateTools;

public class HireContractFileNamingService implements FileNamingService
{
	private String fileName = "";

	private String hireno = "";
	protected final Log logger = LogFactory.getLog(this.getClass());
	private String rootDirectory;

	public HireContractFileNamingService(Hire hire)
	{
		this.fileName = "";
		this.rootDirectory = hire.getDirectory().toString();
		this.hireno = hire.getHireno();
	}

	public String getFileName()
	{
		File rootFolder = new File(this.rootDirectory);
		if (!rootFolder.exists())
		{
			this.logger.error("Directory does not exist: '"
					+ this.rootDirectory + "'");
			rootFolder.mkdirs();
		}

		this.hireno = this.hireno.replace('/', '.');

		// create the basic file name 'Hire Contract - hireno - date - rev'
		String basicFileName = "Hire Contract - ".concat(this.hireno).concat(" -"
				+ DateTools.df.format(new Date())).concat(" - rev ");

		// now create the full filename (without any file extension)
		this.fileName = this.rootDirectory.concat(File.separator).concat(this.getNextHireContractFileName(basicFileName, this.rootDirectory));

		return this.fileName;
	}

	/**
	 * From the given basic naming structure and Hire directory this function
	 * gets a list of any existing Hire Contract files for this hire and works
	 * out what the next revision number for the current document should be.
	 * 
	 * @param basicFileName the basic file naming structure for this Hire
	 * @param rootDirectory the path to the Hire version directory for this hire
	 * @return
	 */
	private String getNextHireContractFileName(String basicFileName, String rootDirectory)
	{
		String fileName = "";
		File rootDir = new File(rootDirectory);

		String[] contractFiles = rootDir.list();
		if (contractFiles != null)
		{
			// build an arraylist of files whoose name matches documents
			ArrayList<String> thisContractFiles = new ArrayList<String>();
			for (String aCostingFile : contractFiles)
			{
				if (aCostingFile.contains(basicFileName))
				{
					thisContractFiles.add(aCostingFile);
				}
			}

			if (thisContractFiles.size() > 0)
			{
				int rev = 0;
				// now perform a second pass to decide on the next sequence
				// number
				for (String contractFile : thisContractFiles)
				{
					if (contractFile.lastIndexOf('.') > -1)
					{
						// strip the doc extension
						String document = contractFile.substring(0, contractFile.length()
								- (contractFile.length() - contractFile.lastIndexOf('.')));

						// strip the basic document name from the document to
						// get the document rev number
						String revNo = document.replaceAll(basicFileName, "");

						if (revNo.matches("\\d*") && !revNo.trim().equals(""))
						{
							if (Integer.parseInt(revNo) > rev)
							{
								rev = Integer.parseInt(revNo);
							}
						}
					}
				}
				rev = rev + 1;
				fileName = basicFileName.concat(String.valueOf(rev));
			}
			else
			{
				fileName = basicFileName.concat(String.valueOf(1));
			}
		}
		else
		{
			fileName = basicFileName.concat(String.valueOf(1));
		}
		return fileName;
	}
}
