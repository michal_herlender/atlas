package org.trescal.cwms.core.documents.birt.invoices;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InvoiceItemPODataSet extends IterableDataSet<String>  {
	
	private InvoiceItemService invoiceItemService;
	public static final Logger logger = (Logger) LoggerFactory.getLogger(InvoiceItemPODataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		invoiceItemService = super.getApplicationContext().getBean(InvoiceItemService.class);
		
	}

	@Override
	public Iterator<String> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer invoiceitemid = (Integer) dataSet.getInputParameterValue("invoiceitemid");
		logger.debug(invoiceitemid == null ? "invoiceitemid is null" : "invoiceitemid = " + invoiceitemid.toString());
		InvoiceItem invoiceItem = invoiceItemService.findInvoiceItem(invoiceitemid);


		// Changed 2020-10-07 to return invoice item POs, rather than job item POs.
		List<String> poNumbers = Stream.concat(
				invoiceItem.getInvPOsItem().stream().map(InvoicePOItem::getInvPO).map(InvoicePO::getPoNumber),
				invoiceItem.getInvItemPOs().stream().filter(iip -> iip.getInvPO() == null).map(this::formatPONumber)
		).collect(Collectors.toList());

		return poNumbers.iterator();
	}
	
	private String formatPONumber(InvoiceItemPO iipo) {
		String result = "";
		if (iipo.getBpo() != null)
			result = iipo.getBpo().getPoNumber();
		else if (iipo.getJobPO() != null)
			result = iipo.getJobPO().getPoNumber();
		return result;
	}

	@Override
	public void setColumnValues(String ponum, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("ponum", ponum);
	}
}
