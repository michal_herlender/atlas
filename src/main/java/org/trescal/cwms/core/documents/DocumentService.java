package org.trescal.cwms.core.documents;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.template.Template;

public interface DocumentService
{
	void addFileNameToSession(Document doc, List<String> sessionNewFileList);

	/**
	 * Typical call to use when no additional document generation parameters are needed
	 * @return
	 */
	Document createBirtDocument(Integer entityId, BaseDocumentType documentType, Locale locale, Component component, 
			FileNamingService fileNamingService);

	/**
	 * Used when additional parameters are required (e.g. invoice)
	 *  
	 * @param additionalParameters to pass to BIRT document
	 * @return
	 */
	Document createBirtDocument(Integer entityId, BaseDocumentType documentType, Locale locale, Component component, 
			FileNamingService fileNamingService, Map<String,Object> additionalParameters);

	/**
	 * Changed from createVelocityDocument(...) to createBirtSQLDocument(...) as 
	 * we no longer support any velocity documents 2018-12-05
	 * 
	 * @param model Map containing the model detail to appear in the document
	 * @param component the component requesting the document
	 * @param doctypeName the name of the document type being requested
	 * @param template the template to be used to create the document
	 * @param fnServ a file naming service object to be used to create the name
	 *        of the generated document
	 * @param rootDirectory the directory for this actual component where files
	 *        will be written to (this is only used if the
	 *        DefaultFileNamingService is called.
	 * @param request the {@link HttpServletRequest} object for storing the file
	 *        back into the session.
	 * @param businessCompany the {@link Company} object of the business company that create the document.
	 * @return Document simple POJO representing the created document and a
	 *         boolean to indicate if it was generated successfully
	 */
	Document createBirtSQLDocument(Map<String, Object> model, Component component, String doctypeName, Template template, 
			FileNamingService fnServ, String rootDirectory, List<String> sessionNewFileList, Company businessCompany);
	
	void deleteFile(String fileName);
}
