package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.TPInstruction;

public class ThirdPartyInstructionDataSet extends IterableDataSet<TPInstruction>  {
	private DeliveryItemService deliveryItemService;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		deliveryItemService = super.getApplicationContext().getBean(DeliveryItemService.class);
	}

	@Override
	public Iterator<TPInstruction> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer itemId = (Integer) dataSet.getInputParameterValue("itemid");
		JobDeliveryItem jobdeliveryitem = (JobDeliveryItem) deliveryItemService.findDeliveryItem(itemId,JobDeliveryItem.class);
		Set<TPInstruction> filteredInstructions = jobdeliveryitem.getJobitem().getTpInstructions().stream()
				.filter(tpi -> tpi.isActive() && tpi.isShowOnTPDelNote())
				.collect(Collectors.toSet());
		return filteredInstructions.iterator();
	}

	@Override
	public void setColumnValues(TPInstruction tpInst, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("tpinst",tpInst.getInstruction());
	}

}
