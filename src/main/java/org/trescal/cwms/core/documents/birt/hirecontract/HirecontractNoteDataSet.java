package org.trescal.cwms.core.documents.birt.hirecontract;

import java.util.Collection;

import org.trescal.cwms.core.documents.birt.common.EntityNoteDataSet;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;

public class HirecontractNoteDataSet extends EntityNoteDataSet<HireNote> {

	@Override
	public Collection<HireNote> getNotes(Integer entityId) {
		HireService hireService = getApplicationContext().getBean(HireService.class);
		Hire hire = hireService.get(entityId);
		return hire.getNotes();
	}

}
