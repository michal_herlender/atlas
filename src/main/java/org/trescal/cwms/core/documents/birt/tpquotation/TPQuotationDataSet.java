package org.trescal.cwms.core.documents.birt.tpquotation;

import java.util.Date;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.DateTools;

public class TPQuotationDataSet extends EntityDataSet<TPQuotation>{
	private TPQuotationService tpQuotationService;
	private Locale locale;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		tpQuotationService = super.getApplicationContext().getBean(TPQuotationService.class);
		locale = super.getDocumentLocale();
	}
	
	@Override
	public TPQuotation getEntity(Integer entityId) {
		return tpQuotationService.get(entityId);
	}

	@Override
	public void setColumnValues(TPQuotation tpq, IUpdatableDataSetRow row) throws ScriptException {		
		SupportedCurrency currency = tpq.getCurrency();
		
		row.setColumnValue("contact", tpq.getContact().getName());
		row.setColumnValue("emailcontact", tpq.getContact().getEmail());
		row.setColumnValue("telephonecontact",tpq.getContact().getTelephone());
		row.setColumnValue("mobilecontact",tpq.getContact().getMobile());
		row.setColumnValue("faxcontact",tpq.getContact().getFax());

		row.setColumnValue("daterecieved", DateTools.dateFromLocalDate(tpq.getRegdate()));
		row.setColumnValue("duration", tpq.getDuration());
		row.setColumnValue("qno", tpq.getQno());
		row.setColumnValue("tpqno", tpq.getTpqno());
		row.setColumnValue("primarycontact", tpq.getCreatedBy().getName());
		
		row.setColumnValue("email", tpq.getCreatedBy().getEmail());
		row.setColumnValue("telephone",tpq.getCreatedBy().getTelephone());
		row.setColumnValue("mobile",tpq.getCreatedBy().getMobile());
		row.setColumnValue("fax",tpq.getCreatedBy().getFax());
		row.setColumnValue("currency",tpq.getCurrency().getCurrencyName());
		
		row.setColumnValue("totalCost", CurrencyValueFormatter.format(tpq.getTotalCost(), currency, locale));
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("contact", String.class);
		metaData.addColumn("emailcontact", String.class);
		metaData.addColumn("telephonecontact", String.class);
		metaData.addColumn("mobilecontact", String.class);
		metaData.addColumn("faxcontact", String.class);
		
		metaData.addColumn("daterecieved", Date.class);
		metaData.addColumn("duration", Integer.class);
		metaData.addColumn("qno", String.class);
		metaData.addColumn("tpqno", String.class);
		metaData.addColumn("primarycontact", String.class);
		
		metaData.addColumn("email", String.class);
		metaData.addColumn("telephone", String.class);
		metaData.addColumn("mobile", String.class);
		metaData.addColumn("fax", String.class);
		metaData.addColumn("currency", String.class);

		metaData.addColumn("totalCost", String.class);
		return true;
	}
	
}
