package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.projection.PurchaseOrderItemProjectionDTO;

public class ProjectedJobsSupplierDeliveryItemPODataSet extends IterableDeliveryDataSet<PurchaseOrderItemProjectionDTO>{

	@Override
	public Iterator<PurchaseOrderItemProjectionDTO> getIterator(IDataSetInstance dataSet) throws ScriptException {
		DeliveryItemDTO diDto = super.getDeliveryItemDTO(dataSet);
		List<PurchaseOrderItemProjectionDTO> fullList = Collections.emptyList();
		if (diDto.getJobItem().getSupplierPos() != null)
			fullList = diDto.getJobItem().getSupplierPos();
		
		List<PurchaseOrderItemProjectionDTO> resultList = fullList.stream()
				.filter(poDto -> poDto.getPurchaseOrder() != null)
				.collect(Collectors.toList());
		
		return resultList.iterator();
	}

	@Override
	public void setColumnValues(PurchaseOrderItemProjectionDTO instance, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		String resultpoNumber = "";
		if (instance.getPurchaseOrder().getPurchaseOrderNumber() != null)
			resultpoNumber = instance.getPurchaseOrder().getPurchaseOrderNumber();
		
		row.setColumnValue("posuppliernumber", resultpoNumber);	
	}
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("posuppliernumber", String.class);
		return true;
	}
}
