package org.trescal.cwms.core.documents.birt.jobitemsheet;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.core.tools.DateTools;

@Controller @IntranetController
public class GenerateJobItemSheetController {

    @Autowired
    private BirtEngine birtEngine;
    @Autowired
    private JobService jobService;

    @RequestMapping(value="generatejobitemlist.htm")
    public ResponseEntity<byte[]> produceDocument(
    		@RequestParam(name="jobid", required=true) Integer jobid, 
    		@RequestParam(name="group1", required=true) JobItemSheetGroup group1,
    		@RequestParam(name="group2", required=true) JobItemSheetGroup group2,
    		@RequestParam(name="group3", required=true) JobItemSheetGroup group3,
    		@RequestParam(name="order", required=true) JobItemSheetOrder order,
    		@RequestParam(name="pagegroup", required=false, defaultValue="NONE") JobItemSheetPageGroup pageGroup,
    		@RequestParam(name="dept", required=false, defaultValue="0") Integer dept,
    		@RequestParam(name="subdiv", required=false, defaultValue="0") Integer subdivid
    			) throws IOException, EngineException {

        Locale locale = LocaleContextHolder.getLocale();
        Job job = this.jobService.get(jobid);
        
        String filename = "Job Item Sheet " + job.getJobno() + " " + DateTools.dtf_files.format(new Date()) + ".pdf";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID, jobid);
        parameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_TYPE, BaseDocumentType.JOB_ITEM_SHEET);
        parameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_LOCALE, locale);
        parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_1, group1);
        parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_2, group2);
        parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_3, group3);
        parameters.put(JobItemSheetItemsDataSet.PARAMETER_ORDER, order);
        parameters.put(JobItemSheetItemsDataSet.PARAMETER_DEPT, dept);
        parameters.put(JobItemSheetItemsDataSet.PARAMETER_PAGEGROUP, pageGroup);
        parameters.put(JobItemSheetItemsDataSet.PARAMETER_SUBDIV, subdivid);
        

        //Create binary contents
        byte[] pdf = birtEngine.generatePDFAsByteArray(BaseDocumentType.JOB_ITEM_SHEET.getTemplatePath(), parameters, locale);

        //Create headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        //create response
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
        return response;
    }}
