package org.trescal.cwms.core.documents.pdf;

public interface PDFWritingService 
{
	void createPDF();
}
