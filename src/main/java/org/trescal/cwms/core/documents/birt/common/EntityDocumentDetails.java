package org.trescal.cwms.core.documents.birt.common;

import java.util.Locale;

import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;

/**
 * Interface to provide information used in a resusable "standard" Trescal report design
 * 
 * @author galen
 *
 */
public interface EntityDocumentDetails {
	/**
	 * Returns the "source address" of the document
	 * (normally Trescal business address)
	 */
	Address getSourceAddress(Integer entityId);
	/**
	 * Returns the "destination address" of the document
	 * (could be client, supplier, business) 
	 */
	Address getDestinationAddress(Integer entityId);
	/**
	 * Returns the business company that "owns" the specified document
	 */
	Company getBusinessCompany(Integer entityId);
	/**
	 * Returns a reference to the specified document; normally this is a document number 
	 */
	String getDocumentReference(Integer entityId);
	/**
	 * Returns the title of the document in the specified locale 
	 */
	String getDocumentTitle(Integer entityId, Locale documentLocale);
	/**
	 * Checks whether document exists
	 */
	boolean exists(Integer entityId);
	/**
	 * Checks whether the document can be rendered.
	 * We can assume that exists() has already been called and verified as true
	 * If the document cannot be rendered (e.g. record size, incomplete, etc...) a message is
	 * added to the errors object.
	 */
	boolean canBeRendered(Integer entityId, Errors errors);
}
