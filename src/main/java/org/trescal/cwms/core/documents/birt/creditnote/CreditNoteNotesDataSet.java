package org.trescal.cwms.core.documents.birt.creditnote;

import java.util.Collection;

import org.trescal.cwms.core.documents.birt.common.EntityNoteDataSet;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.CreditNoteNote;

public class CreditNoteNotesDataSet extends EntityNoteDataSet<CreditNoteNote>{

	@Override
	public Collection<CreditNoteNote> getNotes(Integer entityId) {
		CreditNoteService creditnoteService = getApplicationContext().getBean(CreditNoteService.class);
		CreditNote creditnote = creditnoteService.get(entityId); 
		return creditnote.getNotes();
	}

}
