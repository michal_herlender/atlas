package org.trescal.cwms.core.documents.images.imagetag.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.documents.images.imagetag.ImageTag;

@Service("ImageTagService")
public class ImageTagServiceImpl implements ImageTagService
{
	@Autowired
	ImageTagDao imageTagDao;

	public ImageTag findImageTag(int id)
	{
		return imageTagDao.find(id);
	}

	public void insertImageTag(ImageTag ImageTag)
	{
		imageTagDao.persist(ImageTag);
	}

	public void updateImageTag(ImageTag ImageTag)
	{
		imageTagDao.update(ImageTag);
	}

	public List<ImageTag> getAllImageTags()
	{
		return imageTagDao.findAll();
	}

	public void setImageTagDao(ImageTagDao imageTagDao)
	{
		this.imageTagDao = imageTagDao;
	}

	public void deleteImageTag(ImageTag imagetag)
	{
		this.imageTagDao.remove(imagetag);
	}

	public void saveOrUpdateImageTag(ImageTag imagetag)
	{
		this.imageTagDao.saveOrUpdate(imagetag);
	}
}