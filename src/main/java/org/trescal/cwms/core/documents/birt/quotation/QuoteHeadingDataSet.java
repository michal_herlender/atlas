package org.trescal.cwms.core.documents.birt.quotation;

import java.util.Iterator;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

public class QuoteHeadingDataSet extends IterableDataSet<QuoteHeading> {
	private Set<QuoteHeading> quoteheadings;
	
	public static String SUFFIX = " - ";
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		QuotationService quotationService = super.getApplicationContext().getBean(QuotationService.class);
		Integer entityId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		quoteheadings = quotationService.get(entityId).getQuoteheadings();
	}
	
	@Override
	public Iterator<QuoteHeading> getIterator(IDataSetInstance dataSet) {
		return quoteheadings.iterator();
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("headingid", Integer.class);
		metaData.addColumn("headingName", String.class);
		return true;
	}

	@Override
	public void setColumnValues(QuoteHeading qh, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("headingid", qh.getHeadingId());
		
		if ((qh.getHeadingName() != null) && !qh.isSystemDefault()) {
			row.setColumnValue("headingName",qh.getHeadingName()+SUFFIX);
		}
		else {
			row.setColumnValue("headingName", "");
		}
	}
}
