package org.trescal.cwms.core.documents.excel.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;

@Component
@Scope("prototype")
public class ExcelFileReaderUtil {

	private static final int ROW_INDEX_ZERO = 0;
	private static final int SHEET_INDEX_ZERO = 0;

	public boolean canReadFile(InputStream inputStream, String sheetName, Integer headerIndex) {
		try {
			XSSFWorkbook wb = new XSSFWorkbook(inputStream);

			/* get the first sheet */
			XSSFSheet mainSheet;
			if (StringUtils.isNotBlank(sheetName))
				mainSheet = wb.getSheet(sheetName);
			else
				mainSheet = wb.getSheetAt(SHEET_INDEX_ZERO);

			/* get the first row : supposedly the header */
			XSSFRow headerRow;
			if (headerIndex == null)
				headerRow = mainSheet.getRow(ROW_INDEX_ZERO);
			else
				headerRow = mainSheet.getRow(headerIndex);

			readHeader(headerRow);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * @param inputStream
	 * @return a list of rows in form of key(case insensitive) value while the key
	 *         is the corresponding column name, preserving the insert order
	 * @throws IOException normally it should not throw it since it is already
	 *                     tested in validation
	 */
	public ArrayList<LinkedCaseInsensitiveMap<String>> readExcelFile(InputStream inputStream, String sheetName,
			Integer headerIndex) throws IOException {
		return readExcelFile(inputStream, sheetName, headerIndex, null, null).getRight();
	}

	/**
	 * @return : Pair of : excel files rows count, and file content
	 * @throws IOException
	 */
	public Pair<Integer, ArrayList<LinkedCaseInsensitiveMap<String>>> readExcelFile(InputStream inputStream,
			String sheetName, Integer headerIndex, Integer startIndex, Integer endIndex) throws IOException {
		DataFormatter formatter = new DataFormatter();
		ArrayList<LinkedCaseInsensitiveMap<String>> fileContent = new ArrayList<>();

		XSSFSheet mainSheet = getMainSheet(inputStream, sheetName);

		/* get the first row : supposedly the header */
		XSSFRow headerRow;
		if (headerIndex == null)
			headerRow = mainSheet.getRow(ROW_INDEX_ZERO);
		else {
			headerRow = mainSheet.getRow(headerIndex);
		}

		if (headerIndex == null)
			headerIndex = 0;

		if (startIndex == null)
			startIndex = headerIndex + 1;

		if (endIndex == null || endIndex > mainSheet.getLastRowNum())
			endIndex = mainSheet.getLastRowNum();

		/* read the first row content : header content */
		List<String> columnNames = readHeader(headerRow);

		for (int rowIndex = startIndex; rowIndex <= endIndex; rowIndex++) {
			XSSFRow r = mainSheet.getRow(rowIndex);

			// if the whole row is empty -for some reason- just continue
			if (r == null)
				break;

			// read row content
			LinkedCaseInsensitiveMap<String> rowContent = new LinkedCaseInsensitiveMap<>();
			for (String colName : columnNames) {

				int colIndex = columnNames.indexOf(colName);

				XSSFCell c = r.getCell(colIndex, Row.RETURN_NULL_AND_BLANK);
				String val = formatter.formatCellValue(c);

				rowContent.put(colName, val);
			}

			// check if all read cells are not empty
			boolean allCellsEmpty = rowContent.values().stream().allMatch(s -> {
				return StringUtils.isBlank(s);
			});

			if (!allCellsEmpty)
				fileContent.add(rowContent);
		}

		Pair<Integer, ArrayList<LinkedCaseInsensitiveMap<String>>> pair = Pair.of(mainSheet.getLastRowNum(),
				fileContent);

		return pair;
	}

	private XSSFSheet getMainSheet(InputStream inputStream, String sheetName) throws IOException {
		/* read excel workbook */
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);

		/* get the first sheet */
		XSSFSheet mainSheet;
		if (StringUtils.isNotBlank(sheetName))
			mainSheet = wb.getSheet(sheetName);
		else
			mainSheet = wb.getSheetAt(SHEET_INDEX_ZERO);
		return mainSheet;
	}

	public Integer countExcelFileRows(InputStream inputStream, String sheetName) throws IOException {
		XSSFSheet mainSheet = getMainSheet(inputStream, sheetName);
		return mainSheet.getLastRowNum();
	}

	private List<String> readHeader(XSSFRow headerRow) {
		List<String> headerColumn = new ArrayList<>();
		int headerCount = headerRow.getLastCellNum();
		for (int i = 0; i < headerCount; i++) {
			DataFormatter formatter = new DataFormatter();
			String val = formatter.formatCellValue(headerRow.getCell(i));
			headerColumn.add(val.trim());
		}
		return headerColumn;
	}

}
