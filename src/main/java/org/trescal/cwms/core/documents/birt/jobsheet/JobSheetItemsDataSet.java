package org.trescal.cwms.core.documents.birt.jobsheet;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetGroup;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetOrder;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetPageGroup;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.InstModelTools;

import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class JobSheetItemsDataSet extends IterableDataSet<JobItem> {

    private JobService jobService;
    private MessageSource messageSource;
    private TranslationService translationService;
    private Locale locale;
    private Set<JobItem> jobItems;

    private JobItemSheetGroup group1;
    private JobItemSheetGroup group2;
    private JobItemSheetGroup group3;
    private JobItemSheetOrder order;
    private JobItemSheetPageGroup pageGroup;        // Controls page breaks between groups

    private String emptyTitle;
    public final static String MESSAGE_CODE_ALL_JOB_ITEMS = "docs.jobitemsheet.alljobitems";
    public final static String DEFAULT_MESSAGE_ALL_JOB_ITEMS = "All Job Items";

    private String noDepartmentTitle;
	public final static String MESSAGE_CODE_NO_DEPARTMENT = "docs.jobitemsheet.nodepartment";
	public final static String DEFAULT_MESSAGE_NO_DEPARTMENT = "No Department";
	
	private static final Logger logger = LoggerFactory.getLogger(JobSheetItemsDataSet.class);
	
	public final static String PARAMETER_GROUP_1 = "group1";
	public final static String PARAMETER_GROUP_2 = "group2";
	public final static String PARAMETER_GROUP_3 = "group3";
	public final static String PARAMETER_ORDER = "order";
	public final static String PARAMETER_PAGEGROUP = "pagegroup";	// Optional - default no page break 
	public final static String PARAMETER_SUBDIV = "subdiv";			// Optional - default no restriction by subdiv
	public final static String GROUP_SEPARATOR = " - ";

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		
		messageSource = super.getApplicationContext().getBean(MessageSource.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
		jobService = super.getApplicationContext().getBean(JobService.class);
		locale = super.getDocumentLocale();
		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		Job job = jobService.get(id);
		jobItems = job.getItems();

		emptyTitle = messageSource.getMessage(MESSAGE_CODE_ALL_JOB_ITEMS, null, DEFAULT_MESSAGE_ALL_JOB_ITEMS, locale);
		noDepartmentTitle = messageSource.getMessage(MESSAGE_CODE_NO_DEPARTMENT, null, DEFAULT_MESSAGE_NO_DEPARTMENT, locale);
		group1 = (JobItemSheetGroup) reportContext.getParameterValue(PARAMETER_GROUP_1);
		group2 = (JobItemSheetGroup) reportContext.getParameterValue(PARAMETER_GROUP_2);
		group3 = (JobItemSheetGroup) reportContext.getParameterValue(PARAMETER_GROUP_3);
		order = (JobItemSheetOrder) reportContext.getParameterValue(PARAMETER_ORDER);
		pageGroup = (JobItemSheetPageGroup) reportContext.getParameterValue(PARAMETER_PAGEGROUP);
		
		// defaults for optional parameters
		if (pageGroup == null) pageGroup = JobItemSheetPageGroup.NONE;
		
		initDefaults();
	}

	private void initDefaults() {
		if (group1 == null) group1 = JobItemSheetGroup.SUB_FAMILY;
		if (group2 == null) group2 = JobItemSheetGroup.DEPARTMENT;
		if (group3 == null) group3 = JobItemSheetGroup.SERVICE_TYPE;
		if (order == null) order = JobItemSheetOrder.BARCODE;
	}
	
	@Override
	public Iterator<JobItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return jobItems.iterator();
	}

	@Override
	public void setColumnValues(JobItem jobitem, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		logger.debug("jobitemid : "+jobitem.getJobItemId());
		row.setColumnValue("jobitemid", jobitem.getJobItemId());
		row.setColumnValue("jobitemno", jobitem.getItemNo());
		
		
		String serialno = "";
		Integer jobitemno = 0;
		Integer plantid = 0;
		String plantno = "";
		String modelname = InstModelTools.instrumentModelNameViaTranslations(jobitem.getInst(), false, super.getDocumentLocale(), super.getPrimaryLocale());
		String customerdescription = "";
		
		if (jobitem.getInst().getSerialno() != null) {
			serialno = jobitem.getInst().getSerialno();
		}
		
		if (jobitem.getInst().getPlantno() != null) {
			plantno = jobitem.getInst().getPlantno();
		}
		String groupValue1 = getGroupValue(jobitem, group1);
		String groupValue2 = getGroupValue(jobitem, group2);
		String groupValue3 = getGroupValue(jobitem, group3);
		String groupTitle = getGroupTitle(groupValue1, groupValue2, groupValue3);
		String pageGroupTitle = getPageGroupTitle(groupValue1, groupValue2, groupValue3);

		row.setColumnValue("plantid", jobitem.getInst().getPlantid());
		row.setColumnValue("clientref", jobitem.getClientRef());
		row.setColumnValue("modelName", modelname);
		row.setColumnValue("customerdescription", jobitem.getInst().getCustomerDescription());
		row.setColumnValue("interval", jobitem.getInst().getCalFrequency());
		IntervalUnit intervalUnit = jobitem.getInst().getCalFrequencyUnit();
		row.setColumnValue("intervalUnit", intervalUnit.getName(jobitem.getInst().getCalFrequency(), locale));
		row.setColumnValue("plantNumber", plantno);
		row.setColumnValue("serialNumber", serialno);
		
		// Additional code to put the group values and titles into the data set
		row.setColumnValue("group1", groupValue1);
		row.setColumnValue("group2", groupValue2);
		row.setColumnValue("group3", groupValue3);
		row.setColumnValue("groupTitle", groupTitle);
		row.setColumnValue("pageGroupTitle", pageGroupTitle);
	}
	
	private String getGroupValue(JobItem jobitem, JobItemSheetGroup group) {
		String result = "";
		switch (group) {
		case SUB_FAMILY:
			result = getSubFamily(jobitem);
			break;

		case DEPARTMENT:
			result = getDepartment(jobitem);
			break;

		case SERVICE_TYPE:
			result = getServiceType(jobitem);
			break;
			
		case CAPABILITY_CATEGORY:
			result = getCapabilityCategory(jobitem);

		default:
			break;
		}
		return result;
	}
	
	private String getGroupTitle(String groupValue1, String groupValue2, String groupValue3) {
		StringBuffer result = new StringBuffer();
		result.append(groupValue1);
		if (result.length() > 0 && !groupValue2.isEmpty())
			result.append(GROUP_SEPARATOR); 
		result.append(groupValue2);
		if (result.length() > 0 && !groupValue3.isEmpty())
			result.append(GROUP_SEPARATOR); 
		result.append(groupValue3);
		
		if (result.length() == 0) {
			result.append(emptyTitle);
		}
		return result.toString();
	}
	
	private String getPageGroupTitle(String groupValue1, String groupValue2, String groupValue3) {
		String result = emptyTitle;
		switch (pageGroup) {
		case GROUP_3:
			result = getGroupTitle(groupValue1, groupValue2, groupValue3);
			break;
		case GROUP_2:
			result = getGroupTitle(groupValue1, groupValue2, "");
			break;
		case GROUP_1:
			result = getGroupTitle(groupValue1, "", "");
			break;
		default:
			// Use default of emptyTitle = "All Job Items", no page breaks
			break;
		}
		
		return result.toString();
	}
	
	private String getSubFamily(JobItem jobitem) {
		Set<Translation> sfTranslations = jobitem.getInst().getModel().getDescription().getTranslations();
		return translationService.getCorrectTranslation(sfTranslations, locale);
	}
	
	private String getDepartment(JobItem jobitem) {
		String result = noDepartmentTitle;
		if (jobitem.getNextWorkReq() != null) {
			Department department = jobitem.getNextWorkReq().getWorkRequirement().getDepartment();
			if (department != null) 
				result = department.getShortName();
		}
		
		return result;
	}
	
	private String getServiceType(JobItem jobitem) {
		ServiceType serviceType = null;
		if (jobitem.getNextWorkReq() != null &&
				jobitem.getNextWorkReq().getWorkRequirement().getServiceType() != null) {
			serviceType = jobitem.getNextWorkReq().getWorkRequirement().getServiceType();
		}
		else if (jobitem.getCalType().getServiceType() != null) {
			serviceType = jobitem.getCalType().getServiceType();
		}
		String result = "";
	    if (serviceType != null) {
			Set<Translation> stTranslations = serviceType.getShortnameTranslation();
			result = translationService.getCorrectTranslation(stTranslations, locale);
	    }
		return result;
	}
	
	private String getCapabilityCategory(JobItem jobitem) {
		String result = "";
		if (jobitem.getNextWorkReq() != null) {
            Capability capability = jobitem.getNextWorkReq().getWorkRequirement().getCapability();
            Department department = jobitem.getNextWorkReq().getWorkRequirement().getDepartment();
            if (department != null && capability != null) {
                // Need to get capability category (aka sub-department) matching THIS department
                // Normally one, but capability can belong to multiple departments
                CapabilityCategory category = capability.getCategories().stream()
                    .map(catproc -> catproc.getCategory())
                    .filter(cat -> cat.getDepartment().getDeptid() == department.getDeptid())
                    .findFirst()
                    .orElse(null);
                if (category != null) {
                    result = category.getName();
                }
            }
		}
		return result;
	}
	

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("jobitemid", Integer.class);
		metaData.addColumn("jobitemno", Integer.class);
		metaData.addColumn("plantid", Integer.class);
		metaData.addColumn("interval", Integer.class);
		metaData.addColumn("intervalUnit", String.class);
		metaData.addColumn("clientref", String.class);
		metaData.addColumn("plantNumber", String.class);
		metaData.addColumn("serialNumber", String.class);
		metaData.addColumn("modelName", String.class);
		metaData.addColumn("customerdescription", String.class);
		
		// Additional code to describe the group titles
		metaData.addColumn("group1", String.class);
		metaData.addColumn("group2", String.class);
		metaData.addColumn("group3", String.class);
		metaData.addColumn("groupTitle", String.class);
		metaData.addColumn("pageGroupTitle", String.class);

		return true;
	}

}