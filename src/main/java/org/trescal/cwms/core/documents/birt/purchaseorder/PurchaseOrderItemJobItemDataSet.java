package org.trescal.cwms.core.documents.birt.purchaseorder;

import java.util.Collections;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.tools.InstModelTools;

public class PurchaseOrderItemJobItemDataSet extends IterableDataSet<PurchaseOrderJobItem>{
	
	private PurchaseOrderItemService purchaseOrderitemService;
	private static Logger logger = LoggerFactory.getLogger(PurchaseOrderItemJobItemDataSet.class); 
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		purchaseOrderitemService = super.getApplicationContext().getBean(PurchaseOrderItemService.class);
	}
	
	@Override
	public Iterator<PurchaseOrderJobItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer poitemid = super.getInputParameter(dataSet, "poitemid");
		PurchaseOrderItem purchaseOrderItem = purchaseOrderitemService.findPurchaseOrderItem(poitemid);
		if (purchaseOrderItem != null) {
			return purchaseOrderItem.getLinkedJobItems().iterator();
		}
		else {
			logger.error("No PurchaseOrderItem found for poitemid : "+poitemid);
			return Collections.emptyIterator();
		}
	}

	@Override
	public void setColumnValues(PurchaseOrderJobItem poji, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("jobno", poji.getJi().getJob().getJobno());
		row.setColumnValue("itemNo", poji.getJi().getItemNo());
		
		String desc = InstModelTools.instrumentModelNameViaTranslations(poji.getJi().getInst(), super.getDocumentLocale(), super.getPrimaryLocale());
		row.setColumnValue("description", desc);

		row.setColumnValue("serialno", poji.getJi().getInst().getSerialno());
		if (poji.getJi().getInst() != null)
	            row.setColumnValue("trescalId" ,poji.getJi().getInst().getPlantid());
		
		row.setColumnValue("plantno", poji.getJi().getInst().getPlantno());
	}

}
