package org.trescal.cwms.core.documents.birt.creditnote;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.trescal.cwms.core.documents.birt.tax.AbstractTaxEntryDataSet;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;

public class CreditNoteTaxDataSet extends AbstractTaxEntryDataSet<CreditNote> {

	@Override
	public CreditNote getTaxablePricing(IReportContext reportContext, Integer id) {
		CreditNoteService creditNoteService = super.getApplicationContext().getBean(CreditNoteService.class);
		CreditNote creditNote = creditNoteService.get(id);
		return creditNote;
	}

}
