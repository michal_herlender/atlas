package org.trescal.cwms.core.documents.birt.delivery;

import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;

public abstract class DeliveryDocumentDetails implements EntityDocumentDetails {
	@Autowired
	private DeliveryService deliveryService;

	@Override
	public Address getSourceAddress(Integer entityId) {
		Delivery delivery = this.deliveryService.get(entityId);
		Address result = delivery.getOrganisation().getDefaultAddress();
		if (result == null) throw new RuntimeException("No default address defined for business subdiv "+delivery.getOrganisation().getSubdivid());
		return result;
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		Delivery delivery = this.deliveryService.get(entityId);
		return delivery.getAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		Delivery delivery = this.deliveryService.get(entityId);
		return delivery.getOrganisation().getComp();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		Delivery delivery = this.deliveryService.get(entityId);
		return delivery.getDeliveryno();
	}
}
