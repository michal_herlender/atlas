package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;

public class DeliveryItemNoteDataSet extends NoteDataSet<DeliveryItemNote> {
	// Bean initialized in beforeOpen()
	private DeliveryItemService deliveryItemService;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		deliveryItemService = super.getApplicationContext().getBean(DeliveryItemService.class);
	}

	@Override
	public Iterator<DeliveryItemNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer itemId = (Integer) dataSet.getInputParameterValue("itemid");
		DeliveryItem item = deliveryItemService.findDeliveryItem(itemId);
		return super.getActivePublishedNotes(item.getNotes()).iterator();
	}
}
