package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.system.Constants;

public class JobCostingFileNamingService implements FileNamingService
{
	protected final Log logger = LogFactory.getLog(this.getClass());

	private String fileName = "";
	private String rootDirectory;
	private String qno = "";
	private Integer version = 0;

	public JobCostingFileNamingService(JobCosting costing)
	{
		this.fileName = "";
		this.rootDirectory = costing.getDirectory().toString();
		this.qno = costing.getQno();
		this.version = costing.getVer();
	}

	public String getFileName()
	{
		File rootFolder = new File(this.rootDirectory);
		if (!rootFolder.exists())
		{
			this.logger.error("Directory does not exist: '"
					+ this.rootDirectory + "'");
			rootFolder.mkdirs();
		}

		this.qno = this.qno.replace('/', '.');

		// create the basic file name 'Quotation qno vers version rev '
		String basicFileName = "Costing ".concat(this.qno).concat(" ").concat(Constants.FILE_NAMING_VERSION).concat(" ").concat(String.valueOf(this.version).concat(" ").concat(Constants.FILE_NAMING_REVISION).concat(" "));

		// now create the full filename (without any file extension)
		this.fileName = this.rootDirectory.concat(File.separator).concat(this.getNextCostingFileName(basicFileName, this.rootDirectory));

		return this.fileName;
	}

	/**
	 * From the given basic naming structure and Costing directory this function
	 * gets a list of any existing Costing files for this Costing and works out
	 * what the next revision number for the current document should be.
	 * 
	 * @param basicFileName the basic file naming structure for this Costing
	 * @param rootDirectory the path to the Costing version directory for this
	 *            quotation
	 * @return
	 */
	private String getNextCostingFileName(String basicFileName, String rootDirectory)
	{
		String fileName = "";
		File rootDir = new File(rootDirectory);

		String[] costingFiles = rootDir.list();
		if (costingFiles != null)
		{
			// build an arraylist of files whoose name matches documents
			ArrayList<String> thisCostingFiles = new ArrayList<String>();
			for (int i = 0; i < costingFiles.length; i++)
			{
				String aCostingFile = costingFiles[i];
				if (aCostingFile.contains(basicFileName))
				{
					thisCostingFiles.add(aCostingFile);
				}
			}

			if (thisCostingFiles.size() > 0)
			{
				int rev = 0;
				// now perform a second pass to decide on the next sequence
				// number
				for (String costingFile : thisCostingFiles)
				{
					if (costingFile.lastIndexOf('.') > -1)
					{
						// strip the doc extension
						String document = costingFile.substring(0, costingFile.length()
								- (costingFile.length() - costingFile.lastIndexOf('.')));

						// strip the basic document name from the document to
						// get the document rev number
						String revNo = document.replaceAll(basicFileName, "");

						if (revNo.matches("\\d*") && !revNo.trim().equals(""))
						{
							if (Integer.parseInt(revNo) > rev)
							{
								rev = Integer.parseInt(revNo);
							}
						}
					}
				}
				rev = rev + 1;
				fileName = basicFileName.concat(String.valueOf(rev));
			}
			else
			{
				fileName = basicFileName.concat(String.valueOf(1));
			}
		}
		else
		{
			fileName = basicFileName.concat(String.valueOf(1));
		}
		return fileName;
	}

}
