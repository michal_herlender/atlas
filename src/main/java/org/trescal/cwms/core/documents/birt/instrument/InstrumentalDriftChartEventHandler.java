package org.trescal.cwms.core.documents.birt.instrument;

import org.eclipse.birt.chart.model.Chart;
import org.eclipse.birt.chart.model.attribute.AxisType;
import org.eclipse.birt.chart.model.component.Axis;
import org.eclipse.birt.chart.model.component.Label;
import org.eclipse.birt.chart.script.ChartEventHandlerAdapter;
import org.eclipse.birt.chart.script.IChartScriptContext;
import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.springframework.context.ApplicationContext;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.tools.BirtEngine;

public class InstrumentalDriftChartEventHandler extends ChartEventHandlerAdapter {
	
	//initialized in beforeGeneration
	private Integer plantid;
	private InstrumService instrumentService;
	
	@Override
	public void beforeGeneration(Chart cm, IChartScriptContext icsc) {
		IReportContext reportContext = (IReportContext) icsc.getExternalContext().getObject();
		this.plantid = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		ApplicationContext ac = (ApplicationContext) reportContext.getAppContext().get(BirtEngine.APP_CONTEXT_SPRING_KEY);
        this.instrumentService = ac.getBean(InstrumService.class);
	}
	
	@Override
	public void beforeDrawAxisTitle(Axis axis, Label label, IChartScriptContext icsc) {

		//is axis y axis
		if (axis.getType() == AxisType.LINEAR_LITERAL) {

			Instrument instrument = this.instrumentService.get(this.plantid);
			
			//Get deviation unit symbol
			String unitSymbol = instrument.getInstrumentComplementaryField() != null 
					&& instrument.getInstrumentComplementaryField().getDeviationUnits() != null ?
							instrument.getInstrumentComplementaryField().getDeviationUnits().getFormattedSymbol() : "";
		
			//Set axis label to deviation unit symbol
			label.getCaption().setValue(unitSymbol);
			//Set rotation to 0 degrees i.e perpendicular to the axis (default is 90 degrees, along the axis)
			label.getCaption().getFont( ).setRotation( 0 );
		}
	}

}
