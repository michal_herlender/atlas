package org.trescal.cwms.core.documents.birt.jobitemsheet;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;

@Component
public class JobItemSheetDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JobService jobService;

	private static final String CODE_TITLE = "docs.jobitemsheet.title";
	private static final String DEFAULT_MESSAGE_TITLE = "Job Item Sheet";

	@Override
	public Address getSourceAddress(Integer entityId) {
		Job job  = jobService.get(entityId);
		return job.getOrganisation().getDefaultAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		Job job = jobService.get(entityId);
		return job.getReturnTo();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		Job job = jobService.get(entityId);
		return job.getOrganisation().getComp();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		Job job = jobService.get(entityId);
		return job.getJobno();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE,null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}

	@Override
	public boolean exists(Integer entityId) {
		Job job = jobService.get(entityId);
		return (job != null);
	}

	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
