package org.trescal.cwms.core.documents.birt.tax;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.TaxablePricing;
import org.trescal.cwms.core.pricing.entity.tax.PricingTax;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.PercentFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

public abstract class AbstractTaxEntryDataSet<PricingType extends TaxablePricing<?, ?, ?>> extends IterableDataSet<FormattedTaxEntry> {
	
	// Spring beans initialized in init(...)
	private MessageSource messageSource; 
	
	// Data below for specific pricing entity
	private Locale locale;
	private List<FormattedTaxEntry> results;
	
	private static final String CODE_VAT_AMOUNT = "docs.vatamount";
	private static final String MESSAGE_VAT_AMOUNT = "VAT amount";

	/**
	 * Subclasses should implement this to return the TaxablePricing (e.g. Invoice/CreditNote) entry
	 * that the data set is for
	 */
	public abstract PricingType getTaxablePricing(IReportContext reportContext, Integer id);
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		this.locale = super.getDocumentLocale();
		this.messageSource = super.getApplicationContext().getBean(MessageSource.class);
		
		Integer entityId =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		PricingType taxablePricing = getTaxablePricing(reportContext, entityId);
		initResults(taxablePricing);
		
	}
	
	private void initResults(PricingType taxablePricing) {
        this.results = new ArrayList<>();
        SupportedCurrency currency = taxablePricing.getCurrency();
        NumberFormat percentFormat = PercentFormatter.getTaxPercentFormat(locale);

        // Case 1 - If there are no tax entries on the invoice, use the VAT information at invoice level to make one entry
        if (taxablePricing.getTaxes().isEmpty()) {
            FormattedTaxEntry entry = new FormattedTaxEntry();
            entry.setAmount(CurrencyValueFormatter.format(taxablePricing.getTotalCost(), currency, locale));
            entry.setDescription(this.messageSource.getMessage(CODE_VAT_AMOUNT, null, MESSAGE_VAT_AMOUNT, this.locale));
            BigDecimal fractionalVatRate = taxablePricing.getVatRate().divide(new BigDecimal(100));
            entry.setRate(percentFormat.format(fractionalVatRate));
            entry.setTax(CurrencyValueFormatter.format(taxablePricing.getVatValue(), currency, locale));
            this.results.add(entry);
        }
		// Case 2 - Use tax entries on the invoice, convert the tax entries for display
		else {
			for (PricingTax<?> pricingTax : taxablePricing.getTaxes()) {
                FormattedTaxEntry entry = new FormattedTaxEntry();
                entry.setAmount(CurrencyValueFormatter.format(pricingTax.getAmount(), currency, locale));
                entry.setDescription(pricingTax.getDescription());
                entry.setRate(percentFormat.format(pricingTax.getRate()));
                entry.setTax(CurrencyValueFormatter.format(pricingTax.getTax(), currency, locale));
                this.results.add(entry);
            }
		}
	}

	@Override
	public Iterator<FormattedTaxEntry> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return results.iterator();
	}

	@Override
	public void setColumnValues(FormattedTaxEntry instance, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("description", instance.getDescription());
		row.setColumnValue("amount", instance.getAmount());
		row.setColumnValue("rate", instance.getRate());
		row.setColumnValue("tax", instance.getTax());
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
        metaData.addColumn("description", String.class);
        metaData.addColumn("amount", String.class);            // Formatted with currency info
        metaData.addColumn("rate", String.class);
        metaData.addColumn("tax", String.class);            // Formatted with currency info
        return true;
    }

}
