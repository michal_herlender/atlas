package org.trescal.cwms.core.documents.birt.jobcosting;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

public class JobCostingExpenseItemDataSet extends IterableDataSet<JobCostingExpenseItem>{

	private TranslationService translationService;
	private Locale locale;
	private Locale fallbackLocale;
	private List<JobCostingExpenseItem> list;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		JobCostingService jobcostingService = super.getApplicationContext().getBean(JobCostingService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
		locale = super.getDocumentLocale();
		fallbackLocale = super.getPrimaryLocale();

		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		JobCosting jobcosting = jobcostingService.get(id);
		list = jobcosting.getExpenseItems();
	}

	@Override
	public Iterator<JobCostingExpenseItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return list.iterator();
	}

	@Override
	public void setColumnValues(JobCostingExpenseItem expenseItem, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		SupportedCurrency currency = expenseItem.getJobCosting().getCurrency();
		
		row.setColumnValue("expenseitemid", expenseItem.getId());
		row.setColumnValue("itemno", expenseItem.getItemno());	//  + regularItemCount
		String modelname = InstModelTools.modelNameViaTranslations(expenseItem.getJobExpenseItem().getModel(), locale, fallbackLocale);
		row.setColumnValue("modelname", modelname);
		row.setColumnValue("jobnumber", expenseItem.getJobExpenseItem().getJob().getJobno());
		row.setColumnValue("comment", expenseItem.getJobExpenseItem().getComment());
		String servicetype = this.translationService.getCorrectTranslation(expenseItem.getJobExpenseItem().getServiceType().getLongnameTranslation(), locale);
		row.setColumnValue("servicetype", servicetype);
		row.setColumnValue("finalcost", CurrencyValueFormatter.format(expenseItem.getFinalCost(), currency, locale));
		row.setColumnValue("qty", expenseItem.getQuantity());
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("expenseitemid", Integer.class);
		metaData.addColumn("itemno", Integer.class);
		metaData.addColumn("modelname", String.class);
		metaData.addColumn("jobnumber", String.class);
		metaData.addColumn("comment", String.class);
		metaData.addColumn("servicetype", String.class);
		metaData.addColumn("finalcost", String.class);
		metaData.addColumn("qty", String.class);
		return true;
	}
}
