package org.trescal.cwms.core.documents.birt.costcentreporequest;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;

public class CostCentrePORequestDataSet extends EntityDataSet<JobCosting> {

	private JobCostingService jobCostingService;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		jobCostingService = super.getApplicationContext().getBean(JobCostingService.class);
	}

	@Override
	public JobCosting getEntity(Integer entityId) {
		return jobCostingService.get(entityId);
	}

	@Override
	public void setColumnValues(JobCosting ccporeq, IUpdatableDataSetRow row) throws ScriptException {
		row.setColumnValue("jobno", ccporeq.getJob().getJobno());
		row.setColumnValue("amount", ccporeq.getTotalCost());
		row.setColumnValue("currencySymbol", ccporeq.getCurrency().getCurrencySymbol());
		row.setColumnValue("costCentre", ccporeq.getJob().getReturnTo().getCostCentre());
		row.setColumnValue("name", ccporeq.getContact().getName());
		row.setColumnValue("telephone", ccporeq.getContact().getTelephone());
		row.setColumnValue("address", ccporeq.getJob().getReturnTo().getAddr1());
		row.setColumnValue("receivedDate", ccporeq.getJob().getRegDate());
	}
}