package org.trescal.cwms.core.documents.retrieval;

public interface RetrieveDocumentService<Entity> {
	Class<?> getEntityClass();
	
	RetrieveDocumentResult getLatestDocumentForEntity(Integer entityId);
}
