package org.trescal.cwms.core.documents.images.image.db;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.trescal.cwms.core.documents.images.ImagePropertyDTO;
import org.trescal.cwms.core.documents.images.ImageTagDto;
import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.documents.images.image.ImageType;
import org.trescal.cwms.core.documents.images.instrumentmodelimage.InstrumentModelImage;
import org.trescal.cwms.core.documents.images.jobitemimage.JobItemImage;
import org.trescal.cwms.core.documents.images.labelimage.LabelImage;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public interface ImageService
{
	/**
	 * Takes a list of paths pointing to images that are to be associated with
	 * the given {@link JobItem}. Tests that the {@link JobItem} exists. If the
	 * paths point to files that can be found and the images match the supported
	 * image type then the image will be copied into the system's file structure
	 * for maintaining images.
	 * 
	 * @param imagePaths list of paths to images to stream over.
	 * @param descriptions list of descriptions for each image.
	 * @param entityId the id of the {@link JobItem} or {@link InstrumentModel}.
	 * @return {@link JobItem} indicating if operation was a sucess and if so
	 *         results will hold a list of {@link Image}.
	 */
	ResultWrapper addImagesToEntity(int[] imageIds, String[] descriptions, int entityId, ImageTagDto[] tags, ImageType type, HttpServletRequest request);

	/**
	 * Deletes the {@link Image} identified by the given id.
	 * 
	 * @param imageid the id of the image to remove.
	 */
	ResultWrapper ajaxDeleteImage(int imageid);

	void deleteImage(Image image);

	Image findImage(int id);
	
	JobItemImage findJobImage(int id);
	
	InstrumentModelImage findModelImage(int id);
	
	LabelImage findLabelImage(int id);

	List<Image> getAllImages();

	List<InstrumentModelImage> getAllInstrumentModelImages(int modelid);

	/**
	 * Returns all the {@link JobItemImage} that belong to {@link Instrument}s
	 * of the given {@link InstrumentModel} type.
	 * 
	 * @param modelid the {@link InstrumentModel} id.
	 * @return list of matching {@link JobItemImage}.
	 */
	List<JobItemImage> getAllJobitemImagesForModel(int modelid);

	List<LabelImage> getAllLabelImages();

	/**
	 * Returns a {@link JobItemImage} for all images that have been associated
	 * with an {@link Instrument}
	 * 
	 * @param plantid the id of the {@link Image}.
	 * @return list of all {@link ImageHistoryWrapper}'s sorted by newest first.
	 */
	List<JobItemImage> getInstrumentImages(int plantid);

	public LabelImage getMostRecentLabelImage(String name);
	
	/**
	 * this method returns the most recent {@link JobItemImage} image
	 * 
	 * @param plantid id of the {@link Instrument} to find image for
	 * @return {@link JobItemImage}
	 */
	public JobItemImage getMostRecentInstrumentImage(int plantid);

	/**
	 * this method returns the most recent {@link InstrumentModelImage} image
	 * 
	 * @param modelid id of the {@link InstrumentModel} to find image for
	 * @return {@link InstrumentModelImage}
	 */
	public InstrumentModelImage getMostRecentInstrumentModelImage(int modelid);

	/**
	 * this method returns the next most applicable {@link Image} for a job
	 * item. Either from the job item itself, previous job items or instrument
	 * model
	 * 
	 * @param jobItemId id of the {@link JobItem}
	 * @return {@link ImagePropertyDTO}
	 */
	public ImagePropertyDTO getNextApplicableImageForJI(int jobItemId);
	
	/**
	 * Calculates the folder for the {@link image} with the given id. For
	 * example, an image with id 2345 in a 1000 image per folder setup would
	 * return '2000/'.
	 * 
	 * @param imageid the id of the {@link Image}
	 * @return return the folder name for the folder containing the image.
	 */
	String getSystemImageFolder(int imageid);

	/**
	 * Streams the file designated by the currentURL into the new location
	 * specified by the targetURL.
	 * 
	 * @param currentURL the file to stream.
	 * @param targetURL the destination.
	 * @return {@link File} reference to the new File.
	 */
	File importImage(File currentURL, String targetURL);

	Integer insertImage(Image image);

	void saveOrUpdateImage(Image image);

	void updateImage(Image image);

	void uploadImage(Image image, HttpServletRequest request);
}