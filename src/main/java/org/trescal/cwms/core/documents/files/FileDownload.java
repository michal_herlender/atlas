package org.trescal.cwms.core.documents.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.FileController;
import org.trescal.cwms.core.tools.EncryptionTools;

import lombok.extern.slf4j.Slf4j;

/*
 * same controller as in web 
 * package org.trescal.cwms.web.jobs.certificate.controller;
 * TODO remove possibility for unencrypted file access once remaining references removed
 */
@Controller @FileController
@Slf4j
public class FileDownload {
	
	@RequestMapping(value = "/downloadfile.htm", method = RequestMethod.GET)
    public void showImage(@RequestParam("file") String filename, HttpServletResponse response) throws IOException {
		String decryptedFilename = "";
		try {
			decryptedFilename = EncryptionTools.decrypt(filename);
		}
		catch (Exception e) {
			log.error("Error decrypting filename : "+filename);
			// can't download? - maybe it's not encrypted - only possible for internal use 
			// never use filename directly on web interface!
			//decryptedFilename = filename;
		}
		if (decryptedFilename.length() > 0) {
			File file = new File(decryptedFilename);
			if (file.exists() && file.length() < Integer.MAX_VALUE) {
				response.setContentLength((int) file.length());
		        response.setHeader("Content-Disposition","attachment; filename=\"" + file.getName() +"\"");
		        MimetypesFileTypeMap mimeTypes = new MimetypesFileTypeMap();
		        response.setContentType(mimeTypes.getContentType(filename));
		        FileCopyUtils.copy(Files.readAllBytes(file.toPath()), response.getOutputStream());
			}
			else if (!file.exists()) {
				log.error("File did not exist for decrypted filename : "+decryptedFilename);
			}
			else if (file.length() >= Integer.MAX_VALUE) {
				log.error("File length was "+file.length()+" for decrypted filename : "+decryptedFilename);
			}
		}
	}
}