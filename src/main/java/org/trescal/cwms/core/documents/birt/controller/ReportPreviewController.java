package org.trescal.cwms.core.documents.birt.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentResolver;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.birt.invoices.InvoiceDataSet;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetItemsDataSet;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetGroup;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetOrder;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetPageGroup;
import org.trescal.cwms.core.documents.birt.repaircompletionreport.RepairInspectionReportDataSet;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Allows to preview various entity ID based reports
 * intended for use on Admin screen while developing new reports
 * @author galen
 *
 */
@Controller @IntranetController
public class ReportPreviewController {
	
	@Autowired
	private BirtEngine birtEngine;
	@Autowired
	private BaseDocumentResolver resolver;
	@Autowired
	private SupportedLocaleService localeService;
	@Autowired
	private ReportPreviewValidator validator;
	
	@ModelAttribute("form")
	public ReportPreviewForm formBackingObject(Locale locale) {
		ReportPreviewForm form = new ReportPreviewForm();
		form.setDocumentLocale(locale);
		return form;
	}
	
	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) throws Exception {
		binder.setValidator(validator);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/reportpreview.htm")
	public String referenceData(Model model) {
		model.addAttribute("supportedLocales", localeService.getSupportedLocales());
		model.addAttribute("documentTypes", getDocumentTypes());
		return "trescal/core/admin/reportpreview";
	}
	
	/**
	 * Adds default values for additional parameters, when required for specific document types 
	 */
	private void addAdditionalParameters(BaseDocumentType documentType, Map<String,Object> parameters) {
		switch(documentType) {
		case INVOICE_CALIBRATION:
			parameters.put(InvoiceDataSet.PARAMETER_PRINT_DISCOUNT, true);
			parameters.put(InvoiceDataSet.PARAMETER_PRINT_QUANTITY, true);
			parameters.put(InvoiceDataSet.PARAMETER_PRINT_UNITPRICE, true);
			parameters.put(InvoiceDataSet.PARAMETER_SHOW_APPENDIX, true);
			parameters.put(InvoiceDataSet.PARAMETER_SUMMARIZED, false);
			break;
		case JOB_ITEM_SHEET:
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_1, JobItemSheetGroup.DEPARTMENT);			
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_2, JobItemSheetGroup.CAPABILITY_CATEGORY);
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_3, JobItemSheetGroup.SUB_FAMILY);
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_ORDER, JobItemSheetOrder.BARCODE);
			break;
		case JOB_SHEET:
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_1, JobItemSheetGroup.DEPARTMENT);			
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_2, JobItemSheetGroup.CAPABILITY_CATEGORY);
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_3, JobItemSheetGroup.SUB_FAMILY);
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_ORDER, JobItemSheetOrder.BARCODE);
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_PAGEGROUP, JobItemSheetPageGroup.GROUP_3);
			parameters.put(JobItemSheetItemsDataSet.PARAMETER_SUBDIV, 0);
			break;
		case REPAIR_COMPLETION_REPORT:
			parameters.put(RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT, true);
			break;
		case REPAIR_EVALUATION_REPORT:
			parameters.put(RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT, false);
			break;
		default:
			break;
		}
	}
	
	/**
	 * Returns a set of base document types, sorted alphabetically 
	 */
	private Set<KeyValue<BaseDocumentType,String>> getDocumentTypes() {
		Set<KeyValue<BaseDocumentType,String>> result = new TreeSet<>();
		resolver.getSupportedTypes().stream().forEach(
				dt -> result.add(new KeyValue<>(dt, dt.toString())));
		return result;
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/reportpreview.htm")
	public Object generateDocument(Model model,
			@Valid @ModelAttribute("form") ReportPreviewForm form, 
			BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model);
		}
		String filename = form.getDocumentType().toString()+"_"+form.getEntityId()+"_"+form.getDocumentLocale().toString()+".pdf";
	    Map<String, Object> parameters = new HashMap<>();
	    parameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID, form.getEntityId());
	    parameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_TYPE, form.getDocumentType());
	    parameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_LOCALE, form.getDocumentLocale());
		
	    addAdditionalParameters(form.getDocumentType(), parameters);
	    
		//Create binary contents
	    String templatePath = form.getDocumentType().getTemplatePath();
		byte[] pdf = birtEngine.generatePDFAsByteArray(templatePath, parameters, form.getDocumentLocale());
		
		//Create headers
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.parseMediaType("application/pdf"));
	    headers.setContentDispositionFormData(filename, filename);
	    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		
		//create response
		ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdf, headers, HttpStatus.OK);
	    return response;
	}
}
