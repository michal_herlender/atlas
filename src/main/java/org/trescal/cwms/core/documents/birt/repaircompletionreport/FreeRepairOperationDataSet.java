package org.trescal.cwms.core.documents.birt.repaircompletionreport;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperationComparator;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;

public class FreeRepairOperationDataSet extends IterableDataSet<FreeRepairOperation>{

	private RepairInspectionReportService repairInspectionReportService;
	private SortedSet<FreeRepairOperation> freeRepairOperations;

	private static final Logger logger = LoggerFactory.getLogger(FreeRepairOperationDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {

		repairInspectionReportService = super.getApplicationContext().getBean(RepairInspectionReportService.class);

		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		RepairInspectionReport repairInspectionReport = repairInspectionReportService.get(id);
		
		Boolean repairCompletion = (Boolean) reportContext.getParameterValue(RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT);
		if (repairCompletion == null) {
			logger.error("Required parameter not set, defaulting to false : "+RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT);
			repairCompletion = false;
		}
		
		freeRepairOperations = new TreeSet<FreeRepairOperation>(new FreeRepairOperationComparator());
		if(repairCompletion && repairInspectionReport.getRepairCompletionReport() != null)
		{
			logger.debug("Obtaining operations from repair completion report");
			if(repairInspectionReport.getRepairCompletionReport().getFreeRepairOperations() != null)
				repairInspectionReport.getRepairCompletionReport().getFreeRepairOperations().forEach(o -> {
					if(o.getOperationStatus().equals(RepairOperationStateEnum.EXECUTED))
						freeRepairOperations.add(o);
				});
			
			if(repairInspectionReport.getRepairCompletionReport().getValidatedFreeRepairOperations() != null)
				repairInspectionReport.getRepairCompletionReport().getValidatedFreeRepairOperations().forEach(vo -> {
					if(vo.getFreeRepairOperation().getOperationStatus().equals(RepairOperationStateEnum.EXECUTED))
						freeRepairOperations.add(vo.getFreeRepairOperation());
				});
			
		}
		else{
			logger.debug("Obtaining operations from repair inspection report");
			freeRepairOperations.addAll(repairInspectionReport.getFreeRepairOperations());
		}
	}

	@Override
	public Iterator<FreeRepairOperation> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return freeRepairOperations.iterator();
	}

	@Override
	public void setColumnValues(FreeRepairOperation freeRepairOperation, IUpdatableDataSetRow row, int rowCount) throws ScriptException {

		row.setColumnValue("Nameoperation", freeRepairOperation.getName());
		row.setColumnValue("position", freeRepairOperation.getPosition());
		row.setColumnValue("typeoperation", freeRepairOperation.getOperationType().getValue());
		
	}	

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		
		metaData.addColumn("Nameoperation", String.class);
		metaData.addColumn("position", Integer.class);
		metaData.addColumn("typeoperation", String.class);

		return true;
	}

}
