package org.trescal.cwms.core.documents.birt.hirecontract;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;

@Component
public class OffHireDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private HireService hireService;

	private static final String CODE_TITLE = "offhire";
	private static final String DEFAULT_MESSAGE_TITLE = "Off Hire";

	@Override
	public Address getSourceAddress(Integer entityId) {
		Hire hire = hireService.get(entityId);
		return hire.getOrganisation().getDefaultAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		Hire hire = hireService.get(entityId);
		return hire.getAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		Hire hire = hireService.get(entityId);
		return hire.getOrganisation().getComp();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		Hire hire = hireService.get(entityId);
		return hire.getIdentifier();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE,null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}

	@Override
	public boolean exists(Integer entityId) {
		Hire hire = hireService.get(entityId);
		return (hire != null);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
