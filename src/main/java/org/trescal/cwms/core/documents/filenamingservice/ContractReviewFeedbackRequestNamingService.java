package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.Date;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.tools.DateTools;

/**
 * Naming service for creating contract review feedback requests to clients.
 * Implementation names the files with a jobno and date and (1), (2) etc for
 * versions created on the same day.
 * 
 * @author Richard
 */
public class ContractReviewFeedbackRequestNamingService implements FileNamingService
{
	private String root;
	private Job job;

	public ContractReviewFeedbackRequestNamingService(Job job)
	{
		this.root = job.getDirectory().toString();
		this.job = job;
	}

	@Override
	public String getFileName()
	{
		if (this.job.getDirectory() != null)
		{
			String path = this.job.getDirectory().getAbsolutePath().concat(File.separator);
			String filename = "Contract Review Feedback Request - ".concat(this.job.getJobno().replace('/', '.')).concat(" - ").concat(DateTools.df.format(new Date()));
			filename = path.concat(filename);

			File file = new File(filename + ".html");
			int rev = 2;
			while (file.exists())
			{
				// check if this is the first rev (no brackets)
				if (filename.indexOf('(') < 0)
				{
					filename = filename.concat("(" + rev + ")");
					file = new File(filename + ".html");
					rev = rev + 1;
				}
				else
				{
					// there are already brackets, strip them out and add a new
					// bracket + revision
					filename = filename.substring(0, filename.lastIndexOf('(')).concat("("
							+ rev + ")");
					file = new File(filename + ".html");
					rev = rev + 1;
				}
			}

			return filename;
		}
		else
		{
			return new DefaultFileNamingService(Component.JOB, this.root).getFileName();
		}

	}
}
