package org.trescal.cwms.core.documents;

/**
 * Simple holder object that contains a file path to a document that has been
 * created by the application.
 * 
 * @author richard
 */
public class Document
{
	private String fileName;
	private boolean exists;

	public Document()
	{

	}

	public Document(String fileName, boolean exists)
	{
		this.exists = exists;
		this.fileName = fileName;
	}

	public String getFileName()
	{
		return this.fileName;
	}

	public boolean isExists()
	{
		return this.exists;
	}

	public void setExists(boolean exists)
	{
		this.exists = exists;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
}
