package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteModel;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

public class ProjectedJobDeliveryInstructionDataSet extends IterableDeliveryDataSet<InstructionDTO> {
	
	private List<InstructionDTO> result;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		super.init(dataSet, reportContext);
		DeliveryNoteModel model = super.getModel();
		// Only CARRIAGE instructions which are to appear on the delivery note should be included.
		result = model.getInstructionDtos().stream()
				.filter(dto -> dto.getType().equals(InstructionType.CARRIAGE))
				.filter(dto -> (dto.getIncludeOnDelNote() != null))
				.filter(InstructionDTO::getIncludeOnDelNote)
				.collect(Collectors.toList());
	}

	@Override
	public Iterator<InstructionDTO> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return result.iterator();
	}
	
	@Override
	public void setColumnValues(InstructionDTO instance, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("instruction", instance.getInstruction());
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("instruction", String.class);
		return true;
	}




}
