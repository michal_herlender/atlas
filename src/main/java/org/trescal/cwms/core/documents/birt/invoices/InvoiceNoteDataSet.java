package org.trescal.cwms.core.documents.birt.invoices;

import java.util.Collection;

import org.trescal.cwms.core.documents.birt.common.EntityNoteDataSet;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;

public class InvoiceNoteDataSet extends EntityNoteDataSet<InvoiceNote>{

	@Override
	public Collection<InvoiceNote> getNotes(Integer entityId) {
		InvoiceService invoiceService = getApplicationContext().getBean(InvoiceService.class);
		Invoice invoice = invoiceService.findInvoice(entityId); 
		return invoice.getNotes();
	}

}
