package org.trescal.cwms.core.documents.birt.jobitemsheet;

public enum JobItemSheetOrder {
	BARCODE,
	PLANT_NUMBER,
	JOB_ITEM_NUMBER,
	SERIAL_NUMBER;
}
