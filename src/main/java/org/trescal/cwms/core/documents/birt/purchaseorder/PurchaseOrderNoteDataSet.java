package org.trescal.cwms.core.documents.birt.purchaseorder;

import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote.PurchaseOrderNote;

public class PurchaseOrderNoteDataSet extends NoteDataSet<PurchaseOrderNote> {

	private PurchaseOrderService purchaseOrderService;
	private Integer id;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		purchaseOrderService = getApplicationContext().getBean(PurchaseOrderService.class);
		id  = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
	}

	@Override
	public Iterator<PurchaseOrderNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		PurchaseOrder po = purchaseOrderService.find(id); 
		return super.getActivePublishedNotes(po.getNotes()).iterator();
	}

}
