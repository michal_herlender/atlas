package org.trescal.cwms.core.documents.birt.invoices;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.account.entity.bankaccount.db.BankAccountService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;

public class InvoiceBankAccountDataSet extends IterableDataSet<BankAccount> {

	private List<BankAccount> bankAccounts;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		InvoiceService invoiceService = 
				super.getApplicationContext().getBean(InvoiceService.class);
		CompanySettingsForAllocatedCompanyService settingsService = 
				super.getApplicationContext().getBean(CompanySettingsForAllocatedCompanyService.class);
		BankAccountService bankAccountService =
				super.getApplicationContext().getBean(BankAccountService.class);
		
		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		Invoice invoice = invoiceService.findInvoice(id);
		Company invoiceCompany = invoice.getComp();
		Company businessCompany = invoice.getOrganisation();
		
		CompanySettingsForAllocatedCompany companySettings = settingsService.getByCompany(invoiceCompany, businessCompany);
		List<BankAccount> companyBankAccounts = bankAccountService.searchByCompany(businessCompany);
		if (companySettings.getInvoiceFactoring()) {
			bankAccounts = companyBankAccounts.stream().
					filter(ba -> ba.getIncludeOnFactoredInvoices())
					.collect(Collectors.toList());
		}
		else {
			bankAccounts = companyBankAccounts.stream().
					filter(ba -> ba.getIncludeOnNonFactoredInvoices())
					.collect(Collectors.toList());
		}
	}

	@Override
	public Iterator<BankAccount> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return bankAccounts.iterator();
	}

	@Override
	public void setColumnValues(BankAccount bankAccount, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("iban", bankAccount.getIban());
		row.setColumnValue("swift", bankAccount.getSwiftBic());
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("iban", String.class);
		metaData.addColumn("swift", String.class);
		return true;
	}
	
}
