package org.trescal.cwms.core.documents.birt.purchaseorder;

import java.util.Collections;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemnote.PurchaseOrderItemNote;

public class PurchaseOrderItemNoteDataSet extends NoteDataSet<PurchaseOrderItemNote> {

	private PurchaseOrderItemService purchaseOrderItemService;
	private static Logger logger = LoggerFactory.getLogger(PurchaseOrderItemNoteDataSet.class);
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		purchaseOrderItemService = super.getApplicationContext().getBean(PurchaseOrderItemService.class);
	}
	
	@Override
	public Iterator<PurchaseOrderItemNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer poitemid = super.getInputParameter(dataSet, "poitemid");
		PurchaseOrderItem purchaseOrderitem = purchaseOrderItemService.findPurchaseOrderItem(poitemid);
		if (purchaseOrderitem != null) {
			return super.getActivePublishedNotes(purchaseOrderitem.getNotes()).iterator();			
		}
		else {
			logger.error("No PurchaseOrderItem found for poitemid : "+poitemid);
			return Collections.emptyIterator();
		}
	}
}
