package org.trescal.cwms.core.documents.birt.invoices;


import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.db.BusinessDocumentSettingsService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.misc.entity.numberformat.db.NumberFormatService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.InvoiceAmountFormatter;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.PercentFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;


public class InvoiceDataSet extends EntityDataSet<Invoice> {
	// Beans
	private BusinessDocumentSettingsService businessDocumentSettingsService; 
	private InvoiceService invoiceService;
	private InvoiceAmountFormatter invoiceAmountFormatter;
	private NumberFormatService numberFormatService;
	private TranslationService translationService;
	// Entities initialized in init()
	private Invoice invoice;
	private BusinessDocumentSettings documentSettings;
	private static final Logger logger = LoggerFactory.getLogger(InvoiceDataSet.class);

	// Definitions correlating to parameters (all boolean) in invoice .rpttemplate file
	public static final String PARAMETER_SUMMARIZED = "summarized";
	public static final String PARAMETER_PRINT_DISCOUNT = "printDiscount";
	public static final String PARAMETER_PRINT_QUANTITY = "printQuantity";
	public static final String PARAMETER_PRINT_UNITPRICE = "printUnitPrice";
	public static final String PARAMETER_SHOW_APPENDIX = "showAppendix";

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		businessDocumentSettingsService = super.getApplicationContext().getBean(BusinessDocumentSettingsService.class); 
		invoiceService = super.getApplicationContext().getBean(InvoiceService.class);
		invoiceAmountFormatter = super.getApplicationContext().getBean(InvoiceAmountFormatter.class);
		numberFormatService = super.getApplicationContext().getBean(NumberFormatService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
		super.getDocumentLocale();

		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		this.invoice = invoiceService.findInvoice(id);this
		.documentSettings = 
		this.businessDocumentSettingsService.getForCompanyOrDefault(this.invoice.getOrganisation().getCoid());
	}

	@Override
	public Invoice getEntity(Integer entityId) {
		return invoiceService.findInvoice(entityId);
	}

	@Override
	public void setColumnValues(Invoice invoice, IUpdatableDataSetRow row) throws ScriptException {
		SupportedCurrency currency = invoice.getCurrency();
		Locale locale = super.getDocumentLocale();
		
		row.setColumnValue("contact" , invoice.getBusinessContact().getName());
		row.setColumnValue("telephone" , invoice.getBusinessContact().getTelephone());
		row.setColumnValue("email" , invoice.getBusinessContact().getEmail());
		Date issueDate = DateTools.dateFromLocalDate(invoice.getIssuedate());
		row.setColumnValue("issueDate", issueDate);
		row.setColumnValue("fiscalIdentifier" , invoice.getComp().getFiscalIdentifier());
		row.setColumnValue("legalIdentifier" , invoice.getComp().getLegalIdentifier());
		
		boolean displayfiscalIdentifierSub = this.documentSettings.getSubdivisionFiscalIdentifier();
		logger.debug("displayfiscalIdentifierSub : "+displayfiscalIdentifierSub);
		row.setColumnValue("displayfiscalIdentifierSub", displayfiscalIdentifierSub);
		
		if (displayfiscalIdentifierSub) {
			String fiscalIdentifierSub = this.invoice.getAddress().getSub().getSiretNumber();
			row.setColumnValue("fiscalIdentifierSub", fiscalIdentifierSub);
		}
		else {
			row.setColumnValue("fiscalIdentifierSub", "");
		}
		
		row.setColumnValue("currency", invoice.getCurrency().getCurrencyCode());
		Date dueDate = DateTools.dateFromLocalDate(invoice.getDuedate());
		row.setColumnValue("duedate", dueDate);
		// Normally, a validated, active company will have 1 active legal address
		
		row.setColumnValue("invoiceAddressId", invoice.getAddress().getAddrid());
		if (displayfiscalIdentifierSub)	// Managed at subdivision level (e.g. US); legal address irrelevant/hidden 
			row.setColumnValue("legalAddressId", invoice.getAddress().getAddrid());
		else if (invoice.getComp().getLegalAddress() != null) {
			Integer addrid = invoice.getComp().getLegalAddress().getAddrid();
			row.setColumnValue("legalAddressId", addrid);
		}
		else {
			logger.warn("Company id "+invoice.getComp().getCoid()+" has no legal address set");
			row.setColumnValue("legalAddressId", 0);
		}
		boolean displayFinalCostInLetters = this.documentSettings.getInvoiceAmountText();
		logger.debug("displayFinalCostInLetters : "+displayFinalCostInLetters);
		row.setColumnValue("displayFinalCostInLetters", displayFinalCostInLetters);
		if (displayFinalCostInLetters) {
			String finalCostInLetters = this.invoiceAmountFormatter.getTotalAmountInLetters(
					invoice.getFinalCost(), invoice.getCurrency(), locale);
			row.setColumnValue("finalCostInLetters", finalCostInLetters);
		}
		else {
			row.setColumnValue("finalCostInLetters", "");
		}

        Pair<String, String> splitNumber = numberFormatService.splitNumber(invoice.getInvno(), NumerationType.INVOICE,
            invoice.getOrganisation(), null);
        // Convert company number like FR005-- to FR005
        String formattedLeft = splitNumber.getLeft().replace("-", "");
        row.setColumnValue("invNoLeftPart", formattedLeft);
        row.setColumnValue("invNoRightPart", splitNumber.getRight());

        // values for the total invoice
        row.setColumnValue("totalCost", CurrencyValueFormatter.format(invoice.getTotalCost(), currency, locale));
        Double vatRate = invoice.getVatRate() != null ? invoice.getVatRate().doubleValue() : 0d;
        row.setColumnValue("vatRate", PercentFormatter.getTaxPercentFormat(locale).format(vatRate));
        String vatValue = invoice.getVatValue() != null ? CurrencyValueFormatter.format(invoice.getVatValue(), currency, locale) : "";
        row.setColumnValue("vatValue", vatValue);
        row.setColumnValue("finalCost", CurrencyValueFormatter.format(invoice.getFinalCost(), currency, locale));

        String paymentterm = invoice.getPaymentTerm() != null ? invoice.getPaymentTerm().getMessageForLocale(locale) : null;
        row.setColumnValue("paymentterm", paymentterm);

        String paymode = "";
        if (invoice.getPaymentMode() != null) {
            Set<Translation> translations = invoice.getPaymentMode().getTranslations();
			paymode = translationService.getCorrectTranslation(translations, locale);
		}
		row.setColumnValue("paymentmode", paymode);

		// Always use the business company's own bank account (for receiving payments) - all others are "supplier accounts"
//		CompanySettingsForAllocatedCompany companySettings = settingsService.getByCompany(businessCompany, businessCompany);			
//		if(companySettings != null && companySettings.getBankAccount() != null) {
//			row.setColumnValue("iban", companySettings.getBankAccount().getIban());
//			row.setColumnValue("swift", companySettings.getBankAccount().getSwiftBic());	
//		}
//		else {
//			row.setColumnValue("iban", "");
//			row.setColumnValue("swift", "");
//		}
		String description = invoice.getSiteCostingNote() != null ? invoice.getSiteCostingNote() : "";
		row.setColumnValue("description", description);
		row.setColumnValue("zeroDiscounts", getZeroDiscounts(invoice));
	}
	
	/**
	 * Indication of whether there are no discounts on invoice, so that the discount column can be automatically hidden
	 * This tests just on the general discount rate of each item (expected to be consistent with general discounts)
	 * @param invoice
	 * @return whether all discount rates are zero
	 */
	private boolean getZeroDiscounts(Invoice invoice) {
		boolean result = true;
		for (InvoiceItem item : invoice.getItems()) {
			if (item.getGeneralDiscountRate().compareTo(BigDecimal.ZERO) > 0) {
				result = false;
				break;
			}
		}
		return result;
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("contact", String.class);
        metaData.addColumn("telephone", String.class);
        metaData.addColumn("email", String.class);
        metaData.addColumn("issueDate", Date.class);
        metaData.addColumn("fiscalIdentifier", String.class);
        metaData.addColumn("legalIdentifier", String.class);
		metaData.addColumn("displayfiscalIdentifierSub", Boolean.class);
		metaData.addColumn("fiscalIdentifierSub", String.class);
        metaData.addColumn("currency", String.class);
        metaData.addColumn("invoiceAddressId", Integer.class);
        metaData.addColumn("legalAddressId", Integer.class);
        metaData.addColumn("duedate", Date.class);
        metaData.addColumn("totalCost", String.class);
        metaData.addColumn("vatRate", String.class);
        metaData.addColumn("vatValue", String.class);
        metaData.addColumn("finalCost", String.class);
        metaData.addColumn("paymentterm", String.class);
        metaData.addColumn("paymentmode", String.class);
        metaData.addColumn("displayFinalCostInLetters", Boolean.class);
        metaData.addColumn("finalCostInLetters", String.class);
        metaData.addColumn("invNoLeftPart", String.class);
        metaData.addColumn("invNoRightPart", String.class);
		metaData.addColumn("description", String.class);
		metaData.addColumn("zeroDiscounts", Boolean.class);
		return true;
	}

}
