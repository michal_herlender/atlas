package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Iterator;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;

public class GeneralDeliveryItemDataSet extends IterableDataSet<GeneralDeliveryItem> {
	private Set<GeneralDeliveryItem> deliveryItems;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		Integer deliveryId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		DeliveryService deliveryService = super.getApplicationContext().getBean(DeliveryService.class);
		GeneralDelivery delivery = deliveryService.get(deliveryId, GeneralDelivery.class);
		deliveryItems = delivery.getItems();
	}

	@Override
	public void setColumnValues(GeneralDeliveryItem item, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		row.setColumnValue("delitemid", item.getDelitemid());
		row.setColumnValue("count", rowCount);
		row.setColumnValue("description", item.getItemDescription());
	}

	@Override
	public Iterator<GeneralDeliveryItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return deliveryItems.iterator();
	}
}
