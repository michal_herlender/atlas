package org.trescal.cwms.core.documents.birt.tpquotation;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;


public class TPQuotationItemDataSet extends IterableDataSet<TPQuotationItem> {

	private Set<TPQuotationItem> items;
	private TranslationService translationService;
	private Locale locale;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		TPQuotationService tpQuotationService = super.getApplicationContext().getBean(TPQuotationService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
		locale = super.getDocumentLocale();
		TPQuotation tpQuotation = tpQuotationService.get(id);
		items = tpQuotation.getItems();
		
	}
	
	@Override
	public Iterator<TPQuotationItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return items.iterator();
	}

	@Override
	public void setColumnValues(TPQuotationItem itemtpq, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		SupportedCurrency currency = itemtpq.getTpquotation().getCurrency();
		row.setColumnValue("tpqitemid", itemtpq.getId());
		
		Set<Translation> translationsdesc = itemtpq.getModel().getDescription().getTranslations();
		String desc = translationService.getCorrectTranslation(translationsdesc, locale);
		row.setColumnValue("description", desc);
		
		if(itemtpq.getCaltype() != null){
			Set<Translation> translationscaltype = itemtpq.getCaltype().getServiceType().getShortnameTranslation();
			String caltype = translationService.getCorrectTranslation(translationscaltype, locale);
			row.setColumnValue("caltype", caltype);
		}
		else{
			row.setColumnValue("caltype", "");
		}
		row.setColumnValue("qty" , itemtpq.getQuantity());
		row.setColumnValue("cin" , CurrencyValueFormatter.format(itemtpq.getCarriageIn(), currency, locale));
		row.setColumnValue("cout" , CurrencyValueFormatter.format(itemtpq.getCarriageOut(), currency, locale));
		row.setColumnValue("purchase" , CurrencyValueFormatter.format(getTotalCostIfActive(itemtpq.getPurchaseCost()), currency, locale));
		row.setColumnValue("adjustment" ,CurrencyValueFormatter.format(getTotalCostIfActive(itemtpq.getAdjustmentCost()), currency, locale));
		row.setColumnValue("repair", CurrencyValueFormatter.format(getTotalCostIfActive(itemtpq.getRepairCost()), currency, locale));
		row.setColumnValue("calibration", CurrencyValueFormatter.format(getTotalCostIfActive(itemtpq.getCalibrationCost()), currency, locale));
		row.setColumnValue("discount", CurrencyValueFormatter.format(calculateTotalDiscount(itemtpq), currency, locale));
		row.setColumnValue("total", CurrencyValueFormatter.format(itemtpq.getFinalCost(), currency, locale));
		row.setColumnValue("count", rowCount);
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("tpqitemid", Integer.class);
		metaData.addColumn("description", String.class);
		metaData.addColumn("caltype", String.class);
		metaData.addColumn("qty", Integer.class);
		metaData.addColumn("cin", String.class);
		metaData.addColumn("cout", String.class);
		metaData.addColumn("purchase", String.class);
		metaData.addColumn("adjustment", String.class);
		metaData.addColumn("repair", String.class);
		metaData.addColumn("calibration", String.class);
		metaData.addColumn("discount", String.class);
		metaData.addColumn("total", String.class);
		metaData.addColumn("count", Integer.class);
		
		return true;
	}
	
	/**
	 * Calculates and returns the total discount applied to the item
	 * including discounts against individual active linked costs, and the general discount if any 
	 * @param itemtpq
	 * @return
	 */
	private BigDecimal calculateTotalDiscount(TPQuotationItem itemtpq) {
		BigDecimal result = new BigDecimal("0.00");
		for (Cost cost : itemtpq.getCosts()) {
			if (cost.isActive()) {
				result = result.add(cost.getDiscountValue());
			}
		}
		result = result.add(itemtpq.getGeneralDiscountValue());
		return result;
	}

	/**
	 * Returns the amount of the Cost::totalCost field if Cost::active is true
	 * We only include costs if they are active (it's possible to inactivate costs)
	 * @param cost
	 * @return totalCost or 0 if cost inactive
	 */
	private BigDecimal getTotalCostIfActive(Cost cost) {
		return cost.isActive() ? cost.getTotalCost() : new BigDecimal("0.00");
	}
	
}
