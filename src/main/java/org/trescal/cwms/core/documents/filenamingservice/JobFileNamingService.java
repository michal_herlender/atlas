package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.Date;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.tools.DateTools;

/**
 * Custom FileNamingService implementation for jobs. Custom behaviour is to
 * create a filename indicating the job no and the date of creation. No handling
 * for revisions of documents created on the same day is implemented, existing
 * files will be overwritten.
 * 
 * @author jamiev
 */
public class JobFileNamingService implements FileNamingService
{
	private boolean isJobSheet;
	private Job job;

	/**
	 * @param job The job to create obtain the filepath for.
	 */
	public JobFileNamingService(Job job, boolean isJobSheet)
	{
		this.job = job;
		this.isJobSheet = isJobSheet;
	}

	public void deleteExistingJobSheets()
	{
		if (this.isJobSheet)
		{
			// create job sheet filename we are looking for
			String jsFilename = this.job.getDirectory().getAbsolutePath().concat(File.separator).concat("Job Sheet - ".concat(this.job.getJobno().replace('/', '.')).concat(" - "));
			// get all files in job directory
			File[] files = this.job.getDirectory().listFiles();
			// check all files and see if we can match job sheet filename
			for (File f : files)
			{
				// get path of file
				String path = f.getPath();
				// see if it starts with job sheet filename?
				if (path.startsWith(jsFilename))
				{
					// delete the existing file
					f.delete();
				}
			}
		}
	}

	@Override
	public String getFileName()
	{
		String path = this.job.getDirectory().getAbsolutePath().concat(File.separator);
		String filename = "";
		if (this.isJobSheet)
		{
			filename = "Job Sheet - ".concat(this.job.getJobno().replace('/', '.')).concat(" - ").concat(DateTools.df.format(new Date()));
		}
		else
		{
			filename = "Job List - ".concat(this.job.getJobno().replace('/', '.')).concat(" - ").concat(DateTools.df.format(new Date()));
		}

		return path.concat(filename);
	}
}