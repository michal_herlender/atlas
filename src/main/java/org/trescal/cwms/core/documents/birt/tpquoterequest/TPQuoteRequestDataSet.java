package org.trescal.cwms.core.documents.birt.tpquoterequest;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;

public class TPQuoteRequestDataSet extends EntityDataSet<TPQuoteRequest> {
	private TPQuoteRequestService tpQuoteRequestService;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		tpQuoteRequestService = super.getApplicationContext().getBean(TPQuoteRequestService.class);
	}
	
	@Override
	public TPQuoteRequest getEntity(Integer entityId) {
		return tpQuoteRequestService.get(entityId);
	}

	@Override
	public void setColumnValues(TPQuoteRequest tprq, IUpdatableDataSetRow row) throws ScriptException {
		row.setColumnValue("primarycontact", tprq.getContact().getName());
		row.setColumnValue("emailcontact", tprq.getContact().getEmail());
		row.setColumnValue("telephonecontact",tprq.getContact().getTelephone());
		row.setColumnValue("mobilecontact",tprq.getContact().getMobile());
		row.setColumnValue("date", DateTools.dateFromLocalDate(tprq.getDueDate()));
		row.setColumnValue("issuedate", DateTools.dateFromLocalDate(tprq.getIssuedate()));
		row.setColumnValue("reference", tprq.getClientref());
		row.setColumnValue("contact", tprq.getCreatedBy().getName());
		row.setColumnValue("email", tprq.getCreatedBy().getEmail());
		row.setColumnValue("telephone",tprq.getCreatedBy().getTelephone());
		row.setColumnValue("mobile",tprq.getCreatedBy().getMobile());
	}
}
