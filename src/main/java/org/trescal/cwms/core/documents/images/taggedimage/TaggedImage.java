package org.trescal.cwms.core.documents.images.taggedimage;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.documents.images.image.Image;

/**
 * Represents an instance of a tag has been applied to a particular
 * {@link Image}.
 * 
 * @author Richard
 */
@Entity
@Table(name = "taggedimage")
public class TaggedImage extends Auditable
{
	private int id;
	private Image image;
	private String tag;

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "imageid")
	public Image getImage()
	{
		return this.image;
	}

	@NotEmpty
	@Length(max = 100)
	@Column(name = "tag", length = 100, nullable = false)
	public String getTag()
	{
		return this.tag;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setImage(Image image)
	{
		this.image = image;
	}

	public void setTag(String tag)
	{
		this.tag = tag;
	}
}
