package org.trescal.cwms.core.documents.images.systemimage.db;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.documents.images.systemimage.SystemImage;
import org.trescal.cwms.core.documents.images.systemimage.enums.SystemImageType;
import org.trescal.cwms.core.documents.images.systemimage.form.SystemImageForm;

@Service
public class SystemImageServiceImpl extends BaseServiceImpl<SystemImage, Integer> implements SystemImageService {

	@Autowired
	SystemImageDao sysImgDao;
	@Autowired
	CompanyService compService;

	@Override
	protected BaseDao<SystemImage, Integer> getBaseDao() {
		return this.sysImgDao;
	}

	@Override
	public void insertSystemImage(SystemImageForm form) {
		SystemImage img = new SystemImage();

		img.setDescription(form.getDescription());
		img.setFileName(form.getFile().getOriginalFilename());
		img.setBusinessCompany(this.compService.get(form.getBusinessCompId()));
		try {
			img.setFileData(form.getFile().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		img.setType(SystemImageType.DOCUMENT_LOGO);

		this.save(img);
	}

	@Override
	public SystemImage findSystemImageByBusinessCompany(Company comp) {
		return this.sysImgDao.findSystemImageByBusinessCompany(comp);
	}

	@Override
	public void updateSystemImage(SystemImageForm form, SystemImage img) {
		
		img.setDescription(form.getDescription());
		img.setFileName(form.getFile().getOriginalFilename());
		try {
			img.setFileData(form.getFile().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		img.setType(SystemImageType.DOCUMENT_LOGO);

		this.merge(img);
	}

	@Override
	public List<SystemImage> findSystemImagesByBusinessCompany(Company comp) {
		return this.sysImgDao.findSystemImagesByBusinessCompany(comp);
	}

}
