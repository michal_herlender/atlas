package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteModel;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteModelGenerator;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.tools.BirtEngine;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class IterableDeliveryDataSet<T> extends IterableDataSet<T> {

	@Getter
	private DeliveryNoteModel model;
	private Map<Integer, DeliveryItemDTO> mapDeliveryItemsById;
	
	// Used as parameter in some subclasses that return data for a specific delivery item id
	public static final String PARAM_DELIVERY_ITEM_ID = "deliveryItemId";
	
	/**
	 * If overriding init(), subclasses should first call this method; we check whether model has been initialized,
	 * and if not, store it in the report context (Birt defines as generic map, hence warning suppression)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		this.model = (DeliveryNoteModel) reportContext.getAppContext().get(BirtEngine.APP_CONTEXT_MODEL);
		if (this.model == null) {
			Integer deliveryId = (Integer) (reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID));
			Locale locale = super.getDocumentLocale();
			DeliveryNoteModelGenerator modelGenerator = super.getApplicationContext().getBean(DeliveryNoteModelGenerator.class);
			this.model = modelGenerator.getModel(deliveryId, locale);
			reportContext.getAppContext().put(BirtEngine.APP_CONTEXT_MODEL, model);
			if (log.isDebugEnabled()) log.debug("Initialized delivery note model with "+model.getDeliveryItemDtos().size()+" delivery item DTOs");
		}
		this.mapDeliveryItemsById = model.getDeliveryItemDtos().stream()
				.collect(Collectors.toMap(DeliveryItemDTO::getDeliveryItemId, dto -> dto));
	}

	
	/**
	 * Can be used by subclasses where a deliveryItemId is provided as a data set parameter,
	 * to return the related DeliveryItemDTO for the id.
	 * @throws ScriptException 
	 */
	protected DeliveryItemDTO getDeliveryItemDTO(IDataSetInstance dataSet) throws ScriptException {
		if (this.mapDeliveryItemsById == null)
			throw new RuntimeException("mapDeliveryItemsById not yet initialized!");
		Integer deliveryItemId = (Integer) dataSet.getInputParameterValue(IterableDeliveryDataSet.PARAM_DELIVERY_ITEM_ID);
		DeliveryItemDTO itemDto = mapDeliveryItemsById.get(deliveryItemId);
		return itemDto;
	}
}
