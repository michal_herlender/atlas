package org.trescal.cwms.core.documents.birt.jobsheet;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;

public class JobSheetJobItemCertificateDataSet extends IterableDataSet<String>{
	
	private JobItemService jobItemService;	
	private static final Logger logger = LoggerFactory.getLogger(JobSheetJobItemCertificateDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		
		jobItemService = super.getApplicationContext().getBean(JobItemService.class);
	}
	
	@Override
	public Iterator<String> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer itemId = (Integer) dataSet.getInputParameterValue("itemid");
		logger.debug(itemId == null ? "itemId is null" : "itemId = "+itemId.toString());
		JobItem jobItem = (JobItem) jobItemService.findJobItem(itemId);
		List<String> certificateNumbers  = jobItem.getCertLinks().stream()
				.filter(e -> e.getCert().getStatus().equals(CertStatusEnum.RESERVED))
		        .map(cert -> cert.getCert().getCertno()).collect(Collectors.toList());

		return certificateNumbers.iterator();
	}
		
	@Override
	public void setColumnValues(String certificatenos, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("certificatenos", certificatenos);

	}

}