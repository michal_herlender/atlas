package org.trescal.cwms.core.documents.filenamingservice;

/**
 * It is intended that for each Component there be implemented at least one FileNamingService
 * class to define how all documents created for that component type are to be named.
 * @author Richard
 */
public interface FileNamingService 
{
	String getFileName();
}
