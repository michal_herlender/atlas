package org.trescal.cwms.core.documents.birt.creditnote;

import java.util.Date;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.db.BusinessDocumentSettingsService;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.PercentFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.DateTools;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CreditNoteDataSet extends EntityDataSet<CreditNote> {

	private CreditNote creditnote;
	private CreditNoteService creditnoteservice;
	private BusinessDocumentSettingsService businessDocumentSettingsService; 
	private BusinessDocumentSettings documentSettings;
	private Locale locale;
	private static final Logger logger = LoggerFactory.getLogger(CreditNoteDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		creditnoteservice = super.getApplicationContext().getBean(CreditNoteService.class);
		businessDocumentSettingsService = super.getApplicationContext().getBean(BusinessDocumentSettingsService.class);
		
		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		this.creditnote = this.creditnoteservice.get(id);
		this.documentSettings =
		this.businessDocumentSettingsService.getForCompanyOrDefault(this.creditnote.getOrganisation().getCoid());
		locale = super.getDocumentLocale();
	}

	@Override
	public CreditNote getEntity(Integer entityId) {
		return creditnoteservice.get(entityId);
	}

	@Override
	public void setColumnValues(CreditNote creditnote, IUpdatableDataSetRow row) throws ScriptException {
		
		// Ordered alphabetically by column name - please maintain order when changing
		SupportedCurrency currency = creditnote.getCurrency();
	
		row.setColumnValue("businessContactName", creditnote.getCreatedBy().getName());
		row.setColumnValue("businessContactPhone", creditnote.getCreatedBy().getTelephone());
		row.setColumnValue("businessContactEmail", creditnote.getCreatedBy().getEmail());
		row.setColumnValue("creditNoteNumber", creditnote.getCreditNoteNo());
		row.setColumnValue("currencyCode", currency.getCurrencyCode());

		row.setColumnValue("currencyName", currency.getCurrencyName());
		row.setColumnValue("finalCost", CurrencyValueFormatter.format(creditnote.getFinalCost(), currency, locale));
		row.setColumnValue("fiscalIdentifier", creditnote.getInvoice().getComp().getFiscalIdentifier());
//		row.setColumnValue("fiscalIdentifierSub", creditnote.getInvoice().getAddress().getSub().getFiscalIdentifier());
		
		boolean displayfiscalIdentifierSub = this.documentSettings.getSubdivisionFiscalIdentifier();
		logger.debug("displayfiscalIdentifierSub : "+displayfiscalIdentifierSub);
		row.setColumnValue("displayfiscalIdentifierSub", displayfiscalIdentifierSub);
		
		if (displayfiscalIdentifierSub) {
			String fiscalIdentifierSub = this.creditnote.getInvoice().getAddress().getSub().getSiretNumber();
			row.setColumnValue("fiscalIdentifierSub", fiscalIdentifierSub);
		}
		else {
			row.setColumnValue("fiscalIdentifierSub", "");
		}
		
		row.setColumnValue("invoiceAddressId", creditnote.getInvoice().getAddress().getAddrid());
		row.setColumnValue("invoiceNumber", creditnote.getInvoice().getInvno());

		// Currently no due date on the credit note, this is calculated dynamically on issue date
		Date invoiceIssueDate = DateTools.dateFromLocalDate(creditnote.getInvoice().getIssuedate());
		Date issueDate = DateTools.dateFromLocalDate(creditnote.getIssuedate());
		PaymentTerm pt = creditnote.getInvoice().getPaymentTerm();
		Date paymentDueDate = issueDate == null ? null : DateTools.dateFromLocalDate(pt.computePaymentDate(creditnote.getIssuedate()));

		// Technically possible for company to have no legal address ID
		Integer legalAddressId = null;
		if (displayfiscalIdentifierSub)	// Managed at subdivision level (e.g. US); legal address irrelevant/hidden 
			legalAddressId = creditnote.getInvoice().getAddress().getAddrid();
		else if (creditnote.getInvoice().getComp().getLegalAddress() != null)
			legalAddressId = creditnote.getInvoice().getComp().getLegalAddress().getAddrid();
		else 
			log.error("No legal address for company with coid "+creditnote.getInvoice().getComp().getCoid());

        row.setColumnValue("invoiceIssueDate", invoiceIssueDate);
        row.setColumnValue("issueDate", issueDate);
        row.setColumnValue("legalIdentifier", creditnote.getInvoice().getComp().getLegalIdentifier());
        row.setColumnValue("legalAddressId", legalAddressId);
        row.setColumnValue("paymentDueDate", paymentDueDate);

        row.setColumnValue("paymentTerms", creditnote.getInvoice().getPaymentTerm().getMessageForLocale(locale));
        row.setColumnValue("totalCost", CurrencyValueFormatter.format(creditnote.getTotalCost(), currency, locale));
        Double vatRate = creditnote.getVatRate() != null ? creditnote.getVatRate().doubleValue() : 0d;
        row.setColumnValue("vatRate", PercentFormatter.getTaxPercentFormat(locale).format(vatRate));
        String vatValue = creditnote.getVatValue() != null ? CurrencyValueFormatter.format(creditnote.getVatValue(), currency, locale) : "";
        row.setColumnValue("vatValue", vatValue);
    }
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		
		// Ordered alphabetically by column name - please maintain order when changing

		metaData.addColumn("businessContactName", String.class);
		metaData.addColumn("businessContactPhone", String.class);
		metaData.addColumn("businessContactEmail", String.class);
		metaData.addColumn("creditNoteNumber", String.class);
		metaData.addColumn("currencyCode", String.class);
		
		metaData.addColumn("currencyName", String.class);
		metaData.addColumn("finalCost", String.class);
		metaData.addColumn("fiscalIdentifier", String.class);
		metaData.addColumn("fiscalIdentifierSub", String.class);
		metaData.addColumn("displayfiscalIdentifierSub", Boolean.class);
		metaData.addColumn("invoiceAddressId", Integer.class);
        metaData.addColumn("invoiceNumber", String.class);

        metaData.addColumn("invoiceIssueDate", Date.class);
        metaData.addColumn("issueDate", Date.class);
        metaData.addColumn("legalIdentifier", String.class);
        metaData.addColumn("legalAddressId", Integer.class);
        metaData.addColumn("paymentDueDate", Date.class);

        metaData.addColumn("paymentTerms", String.class);
        metaData.addColumn("totalCost", String.class);
        metaData.addColumn("vatRate", String.class);
        metaData.addColumn("vatValue", String.class);

        return true;
    }
}