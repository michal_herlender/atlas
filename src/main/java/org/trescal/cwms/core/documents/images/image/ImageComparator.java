package org.trescal.cwms.core.documents.images.image;

import java.util.Comparator;

public class ImageComparator implements Comparator<Image>
{
	@Override
	public int compare(Image o1, Image o2)
	{
		if (o1.getTags() != null && o1.getTags().stream().anyMatch(tag -> tag.getTag().equalsIgnoreCase("model default"))
				&& (o2.getTags() == null || !o2.getTags().stream().anyMatch(tag -> tag.getTag().equalsIgnoreCase("model default"))))
			return -1;
		else if (o2.getTags() != null && o2.getTags().stream().anyMatch(tag -> tag.getTag().equalsIgnoreCase("model default"))
				&& (o1.getTags() == null || !o1.getTags().stream().anyMatch(tag -> tag.getTag().equalsIgnoreCase("model default"))))
			return 1;
		else if (o1.getAddedOn().equals(o2.getAddedOn())) {
			// compare itemNos
			Integer id1 = o1.getId();
			Integer id2 = o2.getId();
			return id2.compareTo(id1);
		}
		else return o2.getAddedOn().compareTo(o1.getAddedOn());
	}
}