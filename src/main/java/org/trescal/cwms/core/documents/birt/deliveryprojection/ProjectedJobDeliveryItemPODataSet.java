package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;

public class ProjectedJobDeliveryItemPODataSet extends IterableDeliveryDataSet<ClientPurchaseOrderProjectionDTO>{

	@Override
	public Iterator<ClientPurchaseOrderProjectionDTO> getIterator(IDataSetInstance dataSet) throws ScriptException {
		DeliveryItemDTO diDto = super.getDeliveryItemDTO(dataSet);
		List<ClientPurchaseOrderProjectionDTO> fullList = Collections.emptyList();
		if (diDto.getJobItem().getJobItemPos() != null)
			fullList = diDto.getJobItem().getJobItemPos();
		
		// If there are BPOs present we want to return just BPOs; otherwise we return Job POs.
		List<ClientPurchaseOrderProjectionDTO> resultList = fullList.stream()
				.filter(poDto -> poDto.getBpoId() != null)
				.collect(Collectors.toList());
		if (resultList.isEmpty()) {
			resultList = fullList.stream()
					.filter(poDto -> poDto.getPoId() != null)
					.collect(Collectors.toList());
		}
		return resultList.iterator();
	}

	@Override
	public void setColumnValues(ClientPurchaseOrderProjectionDTO instance, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		String result = "";
		if (instance.getBpoNumber() != null)
			result = instance.getBpoNumber();
		else if (instance.getPoNumber() != null)
			result = instance.getPoNumber();
		
		row.setColumnValue("number", result);
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("number", String.class);
		return true;
	}

}
