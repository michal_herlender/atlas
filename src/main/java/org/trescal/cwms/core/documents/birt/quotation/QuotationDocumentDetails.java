package org.trescal.cwms.core.documents.birt.quotation;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;

@Component
public class QuotationDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;
	@Value("${cwms.config.quotation.size.restrictgenerate}")
	private long sizeRestrictGenerate;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private QuotationItemService quotationItemService;

	private static final String CODE_TITLE = "clientquotation";
	private static final String DEFAULT_MESSAGE_TITLE ="Client Quotation";
	private static final String CODE_QUOTATION_SIZE = "error.quotation.itemcount";
	private static final String DEFAULT_MESSAGE_QUOTATION_SIZE = "This quotation has {0} items. This feature is only supported on quotations with fewer than {1} items for performance reasons.";

	@Override
	public Address getSourceAddress(Integer entityId) {
		Quotation qu = quotationService.get(entityId);
		return qu.getSourceAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		Quotation qu = quotationService.get(entityId);
		return qu.getContact().getDefAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		Quotation qu = quotationService.get(entityId);
		return qu.getOrganisation();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		Quotation qu = quotationService.get(entityId);		
		return qu.getQno();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE, null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		boolean result = true;
		long itemCount = quotationItemService.getItemCount(entityId);
		if (itemCount >= sizeRestrictGenerate) {
			errors.reject(CODE_QUOTATION_SIZE, new Object[] {itemCount, sizeRestrictGenerate}, DEFAULT_MESSAGE_QUOTATION_SIZE);
			result = false;
		}
		return result;
	}

	@Override
	public boolean exists(Integer entityId) {
		Quotation qu = quotationService.get(entityId);	
		return (qu != null);
	}

}
