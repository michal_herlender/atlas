package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemAccessoryDTO;

public class ProjectedJobDeliveryItemAccessoryDataSet extends IterableDeliveryDataSet<DeliveryItemAccessoryDTO> {
	
	@Override
	public Iterator<DeliveryItemAccessoryDTO> getIterator(IDataSetInstance dataSet) throws ScriptException {
		DeliveryItemDTO diDto = super.getDeliveryItemDTO(dataSet);
		
		Collection<DeliveryItemAccessoryDTO> accessories = diDto.getAccessories();
		if (accessories == null)
			accessories = Collections.emptyList();
		// Note, some items may not have accessories
		return accessories.iterator();
	}

	@Override
	public void setColumnValues(DeliveryItemAccessoryDTO dto, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		String bestTranslation = dto.getDescriptionTranslation() != null ?
				dto.getDescriptionTranslation() : dto.getDescription();
		
		row.setColumnValue("quantity",dto.getQuantity());
		row.setColumnValue("text",bestTranslation);
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("quantity", Integer.class);
		metaData.addColumn("text", String.class);
		return true;
	}
	
}
