package org.trescal.cwms.core.documents.birt.common;

import java.io.ByteArrayOutputStream;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.eventadapter.ImageEventAdapter;
import org.eclipse.birt.report.engine.api.script.instance.IImageInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.core.tools.barcodeutil.BarcodeUtilService;

/**
 * Generates barcode in preconfigured format
 * Avoids file temporary storage and javascript
 * Note, when adding the image to a report design:
 *  (a) Set the 'source' parameter to 'Expression' in Advanced
 *  (b) Set the 'Value expression' to a default expression (e.g. null)
 *  (c) Set the height of the image (currently 14 mm in barcode config file)
 *  (d) Provide the barcode to render via a named expression 'barcode' with String value
 * See Trescal_A4.rpttemplate for an example
 * @author galen 2018-04-20
 *
 */
public class BarcodeImageEventAdapter extends ImageEventAdapter {
	
	public static Logger logger = LoggerFactory.getLogger(BarcodeImageEventAdapter.class);
	
	@Override
	public void onRender(IImageInstance image, IReportContext reportContext) throws ScriptException {
		ApplicationContext applicationContext = (ApplicationContext) reportContext.getAppContext().get(BirtEngine.APP_CONTEXT_SPRING_KEY);
		if (applicationContext == null) throw new ScriptException("ApplicationContext not injected via key "+BirtEngine.APP_CONTEXT_SPRING_KEY);
		BarcodeUtilService barcodeService = applicationContext.getBean(BarcodeUtilService.class);
		
		Object neValue = image.getNamedExpressionValue("barcode");
		if (neValue == null) throw new ScriptException("No named expresssion 'barcode' present on image");
		String barcodeText = neValue.toString();
		
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			barcodeService.generateBarcode(barcodeText, baos);
			byte[] bytes = baos.toByteArray();
			image.setData(bytes);
			logger.debug("Generated barcode: "+bytes.length+" bytes");
		}
		catch (Exception e) {
			logger.error("Error generating barcode", e);
			throw new ScriptException("Error generating barcode", e);
		}
	}
}
