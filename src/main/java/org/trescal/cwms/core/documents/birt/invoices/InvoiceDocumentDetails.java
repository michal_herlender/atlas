package org.trescal.cwms.core.documents.birt.invoices;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;

@Component
public class InvoiceDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;
	
	private static final String CODE_REGULAR = "docs.invoicetitle";
	private static final String DEFAULT_MESSAGE_REGULAR = "Invoice";
	private static final String CODE_PROFORMA = "docs.proformainv";
	private static final String DEFAULT_MESSAGE_PROFORMA = "Pro-Forma Invoice";

	@Autowired
	private InvoiceService invoiceService;

	@Override
	public Address getSourceAddress(Integer entityId) {
		Invoice invoice = invoiceService.findInvoice(entityId);
		return invoice.getBusinessContact().getDefAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		Invoice invoice = invoiceService.findInvoice(entityId);
		return invoice.getAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		Invoice invoice = invoiceService.findInvoice(entityId);
		return invoice.getOrganisation();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		Invoice invoice = invoiceService.findInvoice(entityId);
		return invoice.getInvno();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		Invoice invoice = invoiceService.findInvoice(entityId);
		if (!invoice.getType().isAddToAccount()) {
			return messageSource.getMessage(CODE_PROFORMA, null, DEFAULT_MESSAGE_PROFORMA, documentLocale);
		}
		else {
			return messageSource.getMessage(CODE_REGULAR, null, DEFAULT_MESSAGE_REGULAR, documentLocale);
		}
	}

	@Override
	public boolean exists(Integer entityId) {
		Invoice invoice = invoiceService.findInvoice(entityId);
		return (invoice != null);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
