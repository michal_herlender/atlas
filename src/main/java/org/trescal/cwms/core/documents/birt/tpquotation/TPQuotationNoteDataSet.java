package org.trescal.cwms.core.documents.birt.tpquotation;

import java.util.Collection;

import org.trescal.cwms.core.documents.birt.common.EntityNoteDataSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;

public class TPQuotationNoteDataSet extends EntityNoteDataSet<TPQuoteNote> {
	@Override
	public Collection<TPQuoteNote> getNotes(Integer entityId) {
		TPQuotationService tpquotationService  = getApplicationContext().getBean(TPQuotationService.class);
		TPQuotation tpq = tpquotationService.get(entityId); 
		return tpq.getNotes();		
	}
}
