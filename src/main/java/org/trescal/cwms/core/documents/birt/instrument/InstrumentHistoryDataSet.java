package org.trescal.cwms.core.documents.birt.instrument;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class InstrumentHistoryDataSet extends IterableDataSet<InstrumentHistoryDataRow> {

    // Initialized in beforeOpen()
    private InstrumService instrumentService;
    private CertificateService certificateService;
    private Integer plantId;

    @Override
    public void init(IDataSetInstance dataSet, IReportContext reportContext) {
        instrumentService = super.getApplicationContext().getBean(InstrumService.class);
        certificateService = super.getApplicationContext().getBean(CertificateService.class);
        plantId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
    }

    @Override
    public Iterator<InstrumentHistoryDataRow> getIterator(IDataSetInstance dataSet) throws ScriptException {
        Instrument instrument = instrumentService.get(plantId);
        List<InstrumentHistoryDataRow> data = populateHistoryData(instrument);
        return data.iterator();
    }


    @Override
    public void setColumnValues(InstrumentHistoryDataRow data, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
        row.setColumnValue("jobno", data.getJobNo());
        row.setColumnValue("jobitem", data.getJobItem());
        row.setColumnValue("received_date", data.getReceivedDate());
        row.setColumnValue("certificate_no", data.getCertNo());
        row.setColumnValue("verification_status", data.getVerificationStatus());
        row.setColumnValue("cal_date", data.getCalDate());
        row.setColumnValue("deviation", data.getDeviation());
    }


    private List<InstrumentHistoryDataRow> populateHistoryData(Instrument instrument) {

        Set<JobItem> jobItems = instrument.getJobItems();
        List<InstrumentHistoryDataRow> data = new ArrayList<>();

        //Add job items
        for (JobItem jobItem : jobItems) {
            Certificate certificate = this.certificateService.getMostRecentCertificateForJobItem(jobItem);
            data.add(new InstrumentHistoryDataRow(certificate, jobItem, instrument));
        }

        //Add non job instrument certs
        for (InstCertLink instCertLink : instrument.getInstCertLinks()) {
            data.add(new InstrumentHistoryDataRow(instCertLink.getCert(), null, instrument));
        }

        //sort entire list by sort_date intertwining job and non job rows in glorious harmony
        data.sort(Comparator.nullsLast(Comparator.comparing(InstrumentHistoryDataRow::getSortDate)));

        return data;

    }


}
