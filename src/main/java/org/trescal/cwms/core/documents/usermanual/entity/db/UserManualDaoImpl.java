package org.trescal.cwms.core.documents.usermanual.entity.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.documents.usermanual.entity.UserManual;
import org.trescal.cwms.core.documents.usermanual.entity.UserManual_;

@Repository("UserManualDao")
public class UserManualDaoImpl extends BaseDaoImpl<UserManual,String> implements UserManualDao {
	@Override
	protected Class<UserManual> getEntity() {
		return UserManual.class;
	}
	
	@Override
	public UserManual getUserManual(Locale locale)
	{
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<UserManual> cq = cb.createQuery(UserManual.class);
		Root<UserManual> root = cq.from(UserManual.class);
		cq.where(cb.equal(root.get(UserManual_.locale), locale));
		List<UserManual> results = getEntityManager().createQuery(cq).getResultList();
		return (results.isEmpty() ? null : results.get(0));
	}
}
