package org.trescal.cwms.core.documents.birt.common;

import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IColumnMetaData;
import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.eventadapter.ScriptedDataSetEventAdapter;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractDataSet extends ScriptedDataSetEventAdapter {
	// Allows initialization to occur on first call to beforeOpen() and not on repeated calls  
	private boolean initialized;
	// Parameters initialized in beforeOpen()
	private Locale documentLocale;
	private ApplicationContext applicationContext;
	// Fallback locale initialized when needed (most data sets don't need)
	private Locale primaryLocale;
	
	/**
	 * Helper method for subclasses to retrieve Spring applicationContext for bean lookup
	 */
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}

	@Override
	public void beforeOpen(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		super.beforeOpen(dataSet, reportContext);
		if (!initialized) {
			applicationContext = (ApplicationContext) reportContext.getAppContext().get(BirtEngine.APP_CONTEXT_SPRING_KEY);
			if (applicationContext == null) throw new ScriptException("ApplicationContext not injected via key "+BirtEngine.APP_CONTEXT_SPRING_KEY);
			
			documentLocale = (Locale) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_DOCUMENT_LOCALE);
			if (documentLocale == null) {
				documentLocale = LocaleContextHolder.getLocale();
				log.warn("No locale provided via key "+BaseDocumentDefinitions.PARAMETER_DOCUMENT_LOCALE+", using thread locale "+documentLocale);
			}
			initialized = true;
		}
	}

	public Locale getDocumentLocale() {
		return documentLocale;
	}

	public Locale getPrimaryLocale() {
		// Only some data sets will use this - lazy initialized
		if (primaryLocale == null) {
			SupportedLocaleService supportedLocaleService = getApplicationContext().getBean(SupportedLocaleService.class);
			primaryLocale = supportedLocaleService.getPrimaryLocale();
		}
		return primaryLocale;
	}
	
	/**
	 * Verification method which is called after row values are set
	 * for any EntityDataSet/IterableDataSet class; if debug enabled (see log4j.xml) the system
	 * will then report field mismatches (bad) and null values (could be OK)
	 * Useful for troubleshooting hidden issues / missing fields in documents  
	 */
	public void verifyRow(IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		if (log.isDebugEnabled()) {
			IColumnMetaData metaData = row.getDataSet().getColumnMetaData();
			if (metaData == null) {
				log.warn("No metadata for data set "+row.getDataSet().getName());
			}
			else {
				for (int index = 1; index < metaData.getColumnCount(); index++) {
					String mdColumnName = metaData.getColumnName(index);
					Object rowValue = row.getColumnValue(mdColumnName);
					if (rowValue == null) {
						log.warn("Row "+rowCount+" of data set "+row.getDataSet().getName()+" has null value for field "+mdColumnName);
					}
					else {
						String mdColumnNativeTypeName = metaData.getColumnNativeTypeName(index);
						if (!rowValue.getClass().getName().equals(mdColumnNativeTypeName))
							log.warn("Row "+rowCount+" of data set "+row.getDataSet().getName()+" has type mismatch for field "+mdColumnName+" expected "+mdColumnNativeTypeName+" was "+rowValue.getClass().getName());
					}
				}
			}
		}
	}
}
