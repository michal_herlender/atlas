package org.trescal.cwms.core.documents.birt.hirecontract;

import java.util.Iterator;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.tools.InstModelTools;

public class HirecontractItemDataSet extends IterableDataSet<HireItem> {

	private Set<HireItem> hireItems;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		HireService hireService = super.getApplicationContext().getBean(HireService.class);
		Hire hire = hireService.get(id);
		hireItems = hire.getItems();
	}
	
	@Override
	public Iterator<HireItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return hireItems.iterator();
	}

	@Override
	public void setColumnValues(HireItem itemhire, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("hireitemid", itemhire.getId());
		row.setColumnValue("count", rowCount);
		row.setColumnValue("qty", itemhire.getQuantity());
		if (itemhire.getHireInst() != null)  {
			row.setColumnValue("plantno", itemhire.getHireInst().getInst().getPlantno());
			
			String desc = InstModelTools.instrumentModelNameViaTranslations(itemhire.getHireInst().getInst(), getDocumentLocale(),getPrimaryLocale());
			row.setColumnValue("description",desc);
		}
		else {
			row.setColumnValue("plantno",itemhire.getHireCrossItem().getPlantno());
			row.setColumnValue("description",itemhire.getHireCrossItem().getDescription());
	}

}
}
