package org.trescal.cwms.core.documents.birt.tpquoterequest;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;

@Component
public class TPQuoteReqDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;

	private static final String CODE_TITLE = "docs.supplierquotationrequest";
	private static final String DEFAULT_MESSAGE_TITLE ="Supplier Quotation Request";

	@Autowired
	private TPQuoteRequestService tpQuoteRequestService;
	@Override
	public Address getSourceAddress(Integer entityId) {
		TPQuoteRequest tpqr	= tpQuoteRequestService.get(entityId);	
		return tpqr.getCreatedBy().getDefAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		TPQuoteRequest tpqr	= tpQuoteRequestService.get(entityId);	
		return tpqr.getContact().getDefAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		TPQuoteRequest tpqr	= tpQuoteRequestService.get(entityId);	
		return tpqr.getOrganisation();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		TPQuoteRequest tpqr	= tpQuoteRequestService.get(entityId);	
		return tpqr.getRequestNo();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE, null,DEFAULT_MESSAGE_TITLE, documentLocale);
	}

	@Override
	public boolean exists(Integer entityId) {
		TPQuoteRequest tpqr	= tpQuoteRequestService.get(entityId);
		return (tpqr != null);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
