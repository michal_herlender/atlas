package org.trescal.cwms.core.documents.images.imagetag;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.documents.images.image.Image;

/**
 * A preset tag that can be applied to an {@link Image} in the form of a
 * taggedimage
 * 
 * @author Richard
 */
@Entity
@Table(name = "imagetag")
public class ImageTag extends Auditable
{
	private String description;
	private int id;
	private String tag;

	@Length(max = 200)
	@Column(name = "description", length = 200)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public int getId()
	{
		return this.id;
	}

	@NotEmpty
	@Length(max = 100)
	@Column(name = "tag", length = 100, nullable = false, unique = true)
	public String getTag()
	{
		return this.tag;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setTag(String tag)
	{
		this.tag = tag;
	}
}
