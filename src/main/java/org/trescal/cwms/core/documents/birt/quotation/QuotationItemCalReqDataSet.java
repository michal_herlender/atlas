package org.trescal.cwms.core.documents.birt.quotation;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.core.tools.SymbolTool;

/**
 * Data set that provides calibration requirements (CalReq) for individual QuotationItems belonging to a single 
 * Quotation.  The CalReq is most commonly set at Instrument level but could be set at other levels also.
 *   
 * For performance reasons, we look up the relevant quotation item cal requirements in the init() call, 
 * and provide just the calReq for each quotation item (if it exists) at the time column value are set.
 * 
 * This reduces the number of queries, as done in the view quotation controller (and formerly with velocity).
 *  
 * @author galen
 */
public class QuotationItemCalReqDataSet extends IterableDataSet<CalReq> {
	private Map<Integer, CalReq> calReqs;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		// Beans are only used in this method, to initialize calReqs
		CalReqService calReqService = super.getApplicationContext().getBean(CalReqService.class);
		QuotationService quotationService = super.getApplicationContext().getBean(QuotationService.class);
		
		Integer quotationId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		Quotation quotation = quotationService.get(quotationId);
		// Only active calReqs are returned by implementation - no need to filter further
		calReqs = calReqService.findCalReqsForQuotationItems(quotation, quotation.getQuotationitems());
	}
	
	@Override
	public Iterator<CalReq> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer qoitemid = super.getInputParameter(dataSet, "qoitemid");
		CalReq calReq = calReqs.get(qoitemid);
		// Quotation item will have only 0 or 1 CalReq associated
		// We only want to publish on quotation document if publish == true
		return calReq != null && calReq.isPublish() ? 
				Collections.singleton(calReq).iterator() : 
				Collections.emptyIterator();
	}

	@Override
	public void setColumnValues(CalReq calReq, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		String points = "";
		String range = "";
		String instruction = "";
		if (calReq.getPointSet() != null) {
			points = StringTools.convertCalibrationPointSetToString(calReq.getPointSet());
			points = SymbolTool.escape(points);
		}
		else if (calReq.getRange() != null) {
			range = StringTools.convertCalibrationRangeToString(calReq.getRange());
			range = SymbolTool.escape(range);
		}
		// Public instructions should be displayed all times, even if range or point set defined 
		if (calReq.getPublicInstructions() != null) {
			instruction = calReq.getPublicInstructions();
			instruction = SymbolTool.escape(instruction);
		}
		
		row.setColumnValue("points", points);
		row.setColumnValue("range", range);
		row.setColumnValue("instruction", instruction);
	}
}
