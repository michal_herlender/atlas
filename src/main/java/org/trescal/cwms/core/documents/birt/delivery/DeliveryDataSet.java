package org.trescal.cwms.core.documents.birt.delivery;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.tools.DateTools;


public class DeliveryDataSet extends EntityDataSet<JobDelivery> {
	// Beans initialized in beforeOpen()
	private DeliveryService deliveryService;
	private MessageSource messageSource;

	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		deliveryService = super.getApplicationContext().getBean(DeliveryService.class);
		messageSource = super.getApplicationContext().getBean(MessageSource.class);
	}

	@Override
	public JobDelivery getEntity(Integer entityId) {
		return deliveryService.get(entityId, JobDelivery.class);
	}

	@Override
	public void setColumnValues(JobDelivery delivery, IUpdatableDataSetRow row) throws ScriptException {
		row.setColumnValue("contact", delivery.getContact().getName());
		row.setColumnValue("telephone", delivery.getContact().getTelephone());
		row.setColumnValue("mobile", delivery.getContact().getMobile());
		row.setColumnValue("email", delivery.getContact().getEmail());
		row.setColumnValue("deliverynbr", delivery.getDeliveryno());
		row.setColumnValue("deliverydate", DateTools.dateFromLocalDate(delivery.getDeliverydate()));
		row.setColumnValue("createdByName", delivery.getCreatedby().getName());
		row.setColumnValue("clientSubdivisionName", delivery.getContact().getSub().getSubname());
		row.setColumnValue("createdByEmail", delivery.getCreatedby().getEmail());
		row.setColumnValue("createdByPhone", delivery.getCreatedby().getTelephone());
		
		// Default values, in case of no courier despatch or return option (support schedule in future?)
		boolean hasCourierDespatch = false;
		String deliveryBy = "";
		String courierDespatchType = "";
		String courierName = "";
		String courierConsignmentNumber = "";
		
		if (delivery.getCrdes() != null) {
			hasCourierDespatch = true;
			deliveryBy = messageSource.getMessage("viewcouriers.courier", null, "Courier", getDocumentLocale());
			courierDespatchType = delivery.getCrdes().getCdtype().getDescription();
			courierName = delivery.getCrdes().getCdtype().getCourier().getName();
			courierConsignmentNumber = delivery.getCrdes().getConsignmentno();
		}
		else if (delivery.getJob().getReturnOption() != null) {
			deliveryBy = delivery.getJob().getReturnOption().toString();
		}
		row.setColumnValue("deliveryBy", deliveryBy);
		row.setColumnValue("hasCourierDespatch", hasCourierDespatch);
		row.setColumnValue("courierDespatchType", courierDespatchType);
		row.setColumnValue("courierName", courierName);
		row.setColumnValue("courierConsignmentNumber", courierConsignmentNumber);
	}
}