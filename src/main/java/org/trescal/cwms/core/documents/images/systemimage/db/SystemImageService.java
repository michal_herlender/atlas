package org.trescal.cwms.core.documents.images.systemimage.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.images.systemimage.SystemImage;
import org.trescal.cwms.core.documents.images.systemimage.form.SystemImageForm;

public interface SystemImageService extends BaseService<SystemImage, Integer> {
	
	public void insertSystemImage(SystemImageForm form);
	
	public SystemImage findSystemImageByBusinessCompany(Company comp);
	
	public void updateSystemImage(SystemImageForm form,SystemImage img);
	
	public List<SystemImage> findSystemImagesByBusinessCompany(Company comp);

}
