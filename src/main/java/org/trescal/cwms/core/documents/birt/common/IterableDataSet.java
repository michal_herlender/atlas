package org.trescal.cwms.core.documents.birt.common;

import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;

import lombok.extern.slf4j.Slf4j;

/**
 * Implements the basic behavior for a ScriptedDataSet where a collection of entities
 * is retrieved, with access to the entities provided via an Iterator.
 * Prevents copy & paste of boilerplate code into subclasses.
 * 
 *  See delivery.GeneralDeliveryItemDataSet and delivery.DeliveryNoteDataSet for variations
 *  of how this can be subclassed. 
 * 
 *  <T> represents the class of the entities retrieved (could be DeliveryItem, DeliveryNote, etc...) 
 *  
 * Subclasses should do three things:
 * (a) implement init(...) to initialize any service beans or global lookups
 * (b) implement getIterator(...) to lookup/return an iterator for the actual entities
 * (c) implement setColumnValues(...) to set the column values
 * 
 * Galen Beck - 2018-03-06
 */
@Slf4j
public abstract class IterableDataSet<T> extends AbstractDataSet {
	// Allows initialization to occur on first call to beforeOpen() and not on repeated calls  
	private boolean initialized;
	// Value determined via open() method via subclass method getIterator();
	private Iterator<T> iterator;
	private int rowCount;
	
	@Override
	public void beforeOpen(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		super.beforeOpen(dataSet, reportContext);
		if (!initialized) {
			init(dataSet, reportContext);
			initialized = true;
		}
	}
	
	/**
	 * Subclass initialization method to perform bean - and potentially entity - lookup
	 * (just called once per instance)
	 * @param IDataSetInstance, IReportContext provided via beforeOpen
	 * @return 
	 */
	public abstract void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException;

	/**
	 * Subclasses must implement this to lookup/return the actual entity
	 * Subclasses should optimize, when possible, to retrieve the underlying collection in beforeOpen() once  
	 * @param dataSet - the dataSet to be ultimately populated
	 * @return the Iterator of type T (or null if none found / no results)
	 */
	public abstract Iterator<T> getIterator(IDataSetInstance dataSet) throws ScriptException;
	
	@Override
	public void open(IDataSetInstance dataSet) throws ScriptException {
		super.open(dataSet);
		iterator = getIterator(dataSet);
		if (iterator == null) {
			log.error("Null iterator returned by subclass : "+this.getClass().getCanonicalName());
			log.error("Input parameter map : "+dataSet.getInputParameters().toString());
		}
		rowCount = 1;
	}

	/**
	 * Subclasses must implement this to populate the actual column values for an entity
	 * @param instance - the entity of type T
	 * @param row - the IUpdatableDataSetRow to have columns populated
	 * @param rowCount - the current row count (1 based)
	 */
	public abstract void setColumnValues(T instance, IUpdatableDataSetRow row, int rowCount) throws ScriptException;

	@Override
	public boolean fetch(IDataSetInstance dataSet, IUpdatableDataSetRow row) throws ScriptException {
		boolean result = false;
		if ((iterator != null) && iterator.hasNext()) {
			T instance = iterator.next();
			setColumnValues(instance, row, rowCount);
			super.verifyRow(row, rowCount);
			rowCount++;
			result = true;
		}
		return result;
	}
	
	/**
	 * Safely gets input parameter (assumed Integer id) and casts as necessary, 
	 * reporting/logging error if not set and returning 0
	 */
	public Integer getInputParameter(IDataSetInstance dataSet, String parameterName) throws ScriptException {
		Integer result = 0;
		Object value = dataSet.getInputParameterValue(parameterName);
		if (value == null) {
			log.error("Input parameter not set : "+parameterName);
		}
		else if (value instanceof Integer) {
			result = (Integer) value;
		}
		else if (value instanceof Double){
			result = ((Double) value).intValue();
			log.warn("Input parameter : "+parameterName+" was Double "+value+" casting to int");
		}
		else {
			log.error("Input parameter : "+parameterName+" was "+value.getClass().getName()+" "+value.toString());
		}
		return result;
	}
}
