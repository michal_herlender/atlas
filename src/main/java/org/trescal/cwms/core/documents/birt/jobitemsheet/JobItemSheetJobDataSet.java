package org.trescal.cwms.core.documents.birt.jobitemsheet;



import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.tools.DateTools;

public class JobItemSheetJobDataSet extends EntityDataSet<Job> {

	private JobService jobService;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		jobService = super.getApplicationContext().getBean(JobService.class);
	}

	@Override
	public Job getEntity(Integer entityId) {
		return jobService.get(entityId);
	}

	@Override
	public void setColumnValues(Job job, IUpdatableDataSetRow row) throws ScriptException {
		row.setColumnValue("clientCompanyName", job.getCon().getSub().getComp().getConame());
		row.setColumnValue("clientSubdivisionName", job.getCon().getSub().getSubname());
		row.setColumnValue("jobNumber", job.getJobno());	
		row.setColumnValue("trescalContactName", job.getCreatedBy().getName());
		row.setColumnValue("jobreceiptdate", DateTools.dateFromZonedDateTime(job.getReceiptDate()));
		
		if (job.getAgreedDelDate() != null) {
			row.setColumnValue("agreeddeldate", DateTools.dateFromLocalDate(job.getAgreedDelDate()));
			}
		
		row.setColumnValue("instrumentcount", job.getItems().size());
		
	}

}