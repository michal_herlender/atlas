package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

import lombok.extern.slf4j.Slf4j;

/**
 * Could be used for any type of JobDelivery, but practice has been to use on client delivery notes 
 * @author galen
 *
 */
@Slf4j
public class DeliveryInstructionDataSet extends IterableDataSet<String> {

	private List<String> instructions;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		DeliveryService deliveryService = getApplicationContext().getBean(DeliveryService.class);
		InstructionService instructionService = getApplicationContext().getBean(InstructionService.class);
		
		Integer id = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		JobDelivery jobDelivery = deliveryService.get(id, JobDelivery.class); 
		
		instructions = getCarriageInstructions(instructionService, jobDelivery);
	}

	@Override
	public Iterator<String> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return instructions.iterator();
	}

	@Override
	public void setColumnValues(String instruction, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("instruction", instruction);
	}

	/**
	 * Updated to query via linked job item IDs; previously would only use instructions from main job 
	 * @param delivery
	 * @return
	 */
	private List<String> getCarriageInstructions(InstructionService instructionService, JobDelivery delivery)
	{
		Integer allocatedSubdivId = delivery.getOrganisation().getSubdivid();
		List<Integer> jobItemIds = delivery.getItems().stream()
				.map(jdi -> jdi.getJobitem().getJobItemId())
				.collect(Collectors.toList());
		List<InstructionDTO> instDtos = Collections.emptyList();
		if (jobItemIds.size() > 0) 
			instDtos = instructionService.findAllByJobItems(jobItemIds, allocatedSubdivId, InstructionType.CARRIAGE);
		if (log.isDebugEnabled()) 
			log.debug("unfiltered instDtos.size() : "+instDtos.size()); 
		
		boolean includeClient = delivery.getType().equals(DeliveryType.CLIENT);
		// We use the "supplier" flag also for internal delivery note, because the notes/requirements 
		// are expected to be similar
		boolean includeSupplier = delivery.getType().equals(DeliveryType.THIRDPARTY) ||
				delivery.getType().equals(DeliveryType.INTERNAL_RETURN) ||
				delivery.getType().equals(DeliveryType.INTERNAL);
		
		List<String> instructions = instDtos.stream()
			.filter(dto -> (includeClient && (dto.getIncludeOnDelNote() != null) && dto.getIncludeOnDelNote()) || 
					(includeSupplier && (dto.getIncludeOnSupplierDelNote() != null) && dto.getIncludeOnSupplierDelNote()))
			.map(dto -> dto.getInstruction())
			.collect(Collectors.toList());

		if (log.isDebugEnabled()) 
			log.debug("filtered instructions.size() : "+instructions.size()); 
		
		return instructions;
	}
	
}
