package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;

import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;

public class FailureReportFileNamingService implements FileNamingService {

	private FaultReport faultReport;
	private String directory;
	private boolean withClientReponse;

	public static String WITH_CLIENT_RESPONSE = "With Client Response";
	public static String WITHOUT_CLIENT_RESPONSE = "Without Client Response";

	/*
	 * Directory passed in, as this isn't a true bean
	 */
	public FailureReportFileNamingService(FaultReport faultReport, String faultReportDirectory,
			boolean withClientReponse) {
		this.faultReport = faultReport;
		this.directory = faultReportDirectory;
		this.withClientReponse = withClientReponse;
	}

	@Override
	public String getFileName() {
		StringBuffer fileName = new StringBuffer();
		fileName.append("Failure Report  - ");
		fileName.append(faultReport.getFaultReportNumber());
		fileName.append(" - ");
//		fileName.append(DateTools.dtf_files.format(new Date()));
//		Logic change 2019-06-11 - formerly, we would include the date+time of generation.
//		Now, we just include whether it's "Without Client Response" or "With Client Response"  
		if (withClientReponse) {
			fileName.append(WITH_CLIENT_RESPONSE);
		} else {
			fileName.append(WITHOUT_CLIENT_RESPONSE);
		}

		StringBuffer result = new StringBuffer(directory);
		result.append(File.separator);
		result.append(fileName);
		return result.toString();
	}

}
