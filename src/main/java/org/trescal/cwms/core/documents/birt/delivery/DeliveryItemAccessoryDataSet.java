package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

public class DeliveryItemAccessoryDataSet extends IterableDataSet<DeliveryItemAccessory> {
	private DeliveryItemService  deliveryItemService; 
	private TranslationService translationService;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		deliveryItemService = super.getApplicationContext().getBean(DeliveryItemService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
	}
	@Override
	public Iterator<DeliveryItemAccessory> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer itemId = (Integer) dataSet.getInputParameterValue("itemid");
		JobDeliveryItem jobdeliveryitem = (JobDeliveryItem) deliveryItemService.findDeliveryItem(itemId,JobDeliveryItem.class);
		return jobdeliveryitem.getAccessories().iterator();
	}
	@Override
	public void setColumnValues(DeliveryItemAccessory itemAccessory, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		row.setColumnValue("qty",itemAccessory.getQty());
		String text = translationService.getCorrectTranslation(itemAccessory.getAccessory().getTranslations(), super.getDocumentLocale());
		row.setColumnValue("text",text);
	}

}
