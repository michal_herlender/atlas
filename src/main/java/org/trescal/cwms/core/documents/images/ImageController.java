package org.trescal.cwms.core.documents.images;

import java.util.function.Function;

import javax.activation.MimetypesFileTypeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.documents.images.image.db.ImageService;
import org.trescal.cwms.core.exception.controller.FileController;

@Controller @FileController
public class ImageController {
	
	@Autowired
	private ImageService imgServ;
	
	private ResponseEntity<byte[]> generateResponseEntity(Integer imgId, Function<Image, byte[]> dataFunction) {
		Image img = imgServ.findImage(imgId);
		MimetypesFileTypeMap mimeTypeMap = new MimetypesFileTypeMap();
		return ResponseEntity.ok()
	    		.contentType(MediaType.parseMediaType(mimeTypeMap.getContentType(img.getFileName())))
	    		.body(dataFunction.apply(img));
	}
	
	@RequestMapping(value = "/displayimage", method = RequestMethod.GET)
    public ResponseEntity<byte[]> showImage(@RequestParam("id") Integer imgId) {
	    return generateResponseEntity(imgId, img -> img.getFileData());
	}
	
	@RequestMapping(value = "/displaythumbnail", method = RequestMethod.GET)
    public ResponseEntity<byte[]> showThumbnail(@RequestParam("id") Integer imgId) {
	    return generateResponseEntity(imgId, img -> img.getThumbData());
	}
}