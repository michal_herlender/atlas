package org.trescal.cwms.core.documents.birt.common;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.system.entity.note.Note;

/**
 * Base class for the retrieval of Note subclasses for BIRT documents
 * On documents we just display notes where active and publish are true 
 * @author galen
 *
 */
public abstract class NoteDataSet<T extends Note> extends IterableDataSet<T> {
	
	public List<T> getActivePublishedNotes(Collection<T> notes) {
		return notes.stream()
			.filter(note -> note.isActive() && note.getPublish())
			.collect(Collectors.toList());
	}

	@Override
	public final void setColumnValues(T note, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("note", note.getNote());
		row.setColumnValue("label", note.getLabel() == null ? "" : note.getLabel());
		row.setColumnValue("count", rowCount);
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("note", String.class);
		metaData.addColumn("label", String.class);
		metaData.addColumn("count", Integer.class);
		return true;
	}
}
