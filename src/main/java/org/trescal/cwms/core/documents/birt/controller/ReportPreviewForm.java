package org.trescal.cwms.core.documents.birt.controller;

import java.util.Locale;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;

public class ReportPreviewForm {
	private BaseDocumentType documentType;
	private Integer entityId;
	private Locale documentLocale;
	
	@NotNull(message="{error.value.notselected}")
	public BaseDocumentType getDocumentType() {
		return documentType;
	}
	@NotNull(message="{error.value.notselected}")
	public Integer getEntityId() {
		return entityId;
	}
	@NotNull(message="{error.value.notselected}")
	public Locale getDocumentLocale() {
		return documentLocale;
	}

	public void setDocumentType(BaseDocumentType documentType) {
		this.documentType = documentType;
	}
	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}
	public void setDocumentLocale(Locale documentLocale) {
		this.documentLocale = documentLocale;
	}
}
