package org.trescal.cwms.core.documents.birt.common;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.system.entity.note.Note;

/**
 * Base abstract class for retrieval of a single list of notes based on single entity ID of report
 * (e.g. InvoiceNote, classes like InvoiceItemNote using parameters should extend NoteDataSet instead) 
 */
public abstract class EntityNoteDataSet<T extends Note> extends NoteDataSet<T> {
	private List<T> activePublishedNotes;
	/**
	 * Subclasses just need to implement this method to return full collection of notes
	 * Filtering (active, publish) is done once in EntityNoteDataSet. 
	 * @return
	 */
	public abstract Collection<T> getNotes(Integer entityId);
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		Integer entityId  = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		Collection<T> notes = getNotes(entityId);
		activePublishedNotes = getActivePublishedNotes(notes);
	}
	
	@Override
	public Iterator<T> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return activePublishedNotes.iterator();
	}
}
