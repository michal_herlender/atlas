package org.trescal.cwms.core.documents.birt.invoices;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.trescal.cwms.core.documents.birt.tax.AbstractTaxEntryDataSet;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;

public class InvoiceTaxDataSet extends AbstractTaxEntryDataSet<Invoice> {

	@Override
	public Invoice getTaxablePricing(
			IReportContext reportContext, Integer id) {
		InvoiceService invoiceService = super.getApplicationContext().getBean(InvoiceService.class);
		Invoice invoice = invoiceService.findInvoice(id);
		return invoice;
	}

}
