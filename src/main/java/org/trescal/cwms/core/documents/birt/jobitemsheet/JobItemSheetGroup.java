package org.trescal.cwms.core.documents.birt.jobitemsheet;

public enum JobItemSheetGroup {
	NONE("None"),
	SUB_FAMILY("Sub-Family"),
	DEPARTMENT("Department"),
	SERVICE_TYPE("Service Type"),
	CAPABILITY_CATEGORY("Capability Category");

	private String groupName;
	
	private JobItemSheetGroup(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupName() {
		return groupName;
	}
	
	public static JobItemSheetGroup parseFullName(String inputName) {
		JobItemSheetGroup result = NONE;
		for (JobItemSheetGroup status : JobItemSheetGroup.values()) {
			if (status.getGroupName().equals(inputName)) {
				result = status;
				break;
			}
		}
		return result;
	}
}
