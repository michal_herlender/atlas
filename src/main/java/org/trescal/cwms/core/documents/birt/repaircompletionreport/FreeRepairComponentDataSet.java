package org.trescal.cwms.core.documents.birt.repaircompletionreport;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponentComparator;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentStateEnum;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperationComparator;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;

public class FreeRepairComponentDataSet extends IterableDataSet<RepairDocumentComponentDTO>{

	private List<RepairDocumentComponentDTO> results;

	private static final Logger logger = LoggerFactory.getLogger(FreeRepairComponentDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {

		RepairInspectionReportService repairInspectionReportService = super.getApplicationContext().getBean(RepairInspectionReportService.class);

		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		RepairInspectionReport repairInspectionReport = repairInspectionReportService.get(id);
		results = new ArrayList<>();
		
		boolean repairCompletion = (boolean) reportContext.getParameterValue(RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT);

		// We don't rely on inherent sort order of the operations, in case the implementation changes to unsorted
		SortedSet<FreeRepairOperation> operations = new TreeSet<>(new FreeRepairOperationComparator());
		operations.addAll(repairInspectionReport.getFreeRepairOperations());

		if(repairCompletion && repairInspectionReport.getRepairCompletionReport() != null) {
			logger.debug("Obtaining additional components from repair completion report");
			// Components may be on either the inspection report, or completion report.
			operations.addAll(repairInspectionReport.getRepairCompletionReport().getFreeRepairOperations());
		}	
			
		for (FreeRepairOperation operation : operations) {
			// We don't rely on inherent sort order of the components, in case the implementation changes to unsorted
			SortedSet<FreeRepairComponent> components = new TreeSet<>(new FreeRepairComponentComparator());
			components.addAll(operation.getFreeRepairComponents());
			components.stream()
				.filter(component -> repairCompletion || !component.getAddedAfterRiRValidation())
				.forEach(component -> results.add(convert(component, repairCompletion)));
		}
	}
	
	private RepairDocumentComponentDTO convert(FreeRepairComponent component, boolean repairCompletion) {
		RepairDocumentComponentDTO dto = new RepairDocumentComponentDTO();
		dto.setName(component.getName());
		dto.setOperation(component.getFreeRepairOperation().getPosition());
		if (repairCompletion && component.getValidatedFreeRepairComponent() != null) {
			// Quantity may be updated / set by user when completing RCR, use final quantity here
			dto.setQuantity(component.getValidatedFreeRepairComponent().getQuantity());
		}
		else {
			dto.setQuantity(component.getQuantity());
		}
		return dto;
	}

	@Override
	public Iterator<RepairDocumentComponentDTO> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return results.iterator();
	}

	@Override
	public void setColumnValues(RepairDocumentComponentDTO dto, IUpdatableDataSetRow row, int rowCount) throws ScriptException {

		row.setColumnValue("Namecomp", dto.getName());
		row.setColumnValue("ComponentOperationNum", dto.getOperation());
		row.setColumnValue("count", rowCount);
		row.setColumnValue("quantity", dto.getQuantity());
		
	}	

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {

		metaData.addColumn("Namecomp", String.class);
		metaData.addColumn("ComponentOperationNum", Integer.class);
		metaData.addColumn("count", Integer.class);
		metaData.addColumn("quantity", Integer.class);
		
		return true;
	}

}
