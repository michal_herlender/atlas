package org.trescal.cwms.core.documents.retrieval;

import lombok.Builder;
import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Builder @NoArgsConstructor @AllArgsConstructor @Getter
public class RetrieveDocumentResult {
	private boolean success;
	private String errorMessage;
	private byte[] data;
	private String filename;
}
