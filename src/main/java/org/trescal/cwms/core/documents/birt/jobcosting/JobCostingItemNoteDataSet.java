package org.trescal.cwms.core.documents.birt.jobcosting;

import java.util.Collections;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark.JobCostingItemNote;

public class JobCostingItemNoteDataSet extends NoteDataSet<JobCostingItemNote> {

	private JobCostingItemService jobcostingItemService;
	private static Logger logger = LoggerFactory.getLogger(JobCostingItemNoteDataSet.class);
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		jobcostingItemService = super.getApplicationContext().getBean(JobCostingItemService.class);
	}
	
	@Override
	public Iterator<JobCostingItemNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer jobcostingitemid = super.getInputParameter(dataSet, "jobcostingitemid");
		JobCostingItem jobcostingItem = jobcostingItemService.get(jobcostingitemid);
		if (jobcostingItem != null) {
			return super.getActivePublishedNotes(jobcostingItem.getNotes()).iterator();			
		}
		else {
			logger.error("No JobCostingItem found for jobcostingitemid : "+jobcostingitemid);
			return Collections.emptyIterator();
		}
	}
}
