package org.trescal.cwms.core.documents.birt.tpquoterequest;

import java.util.Collections;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db.TPQuoteRequestItemService;

public class TPQuoteReqIItemCostTypeDataset extends IterableDataSet<CostType> {
	private TPQuoteRequestItemService tpQuoteRequestItemService;
	private static Logger logger = LoggerFactory.getLogger(TPQuoteReqIItemCostTypeDataset.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		tpQuoteRequestItemService = super.getApplicationContext().getBean(TPQuoteRequestItemService.class);
	}

	@Override
	public Iterator<CostType> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer itemid =  super.getInputParameter(dataSet, "itemid");
		TPQuoteRequestItem  tpQuoteRequestItem = tpQuoteRequestItemService.findTPQuoteRequestItem(itemid);
		if(tpQuoteRequestItem != null){	
			return tpQuoteRequestItem.getCostTypes().iterator();
		}
		else{
			logger.error("No Tp.Quote Request Item found for tpqitemid : "+itemid);
			return Collections.emptyIterator();
		}
	}

	@Override
	public void setColumnValues(CostType tpqcos, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		row.setColumnValue("costtype", tpqcos.getMessageForLocale(getDocumentLocale()));

	}
}
