package org.trescal.cwms.core.documents.birt.jobsheet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetGroup;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetItemsDataSet;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetOrder;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetPageGroup;
import org.trescal.cwms.core.documents.filenamingservice.JobFileNamingService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Controller
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST})
public class JobSheetController {
	@Autowired
	private JobService jobService;
	@Autowired
	private DocumentService documentService;
	
	@RequestMapping(path="/birtjobsheet.htm")
	public String generateJobSheet(Locale locale, Model model,
			@ModelAttribute(name=Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList,
			@RequestParam(name="jobid", required=true) Integer jobId,
			@RequestParam(name="group1", required=true) JobItemSheetGroup group1,
			@RequestParam(name="group2", required=true) JobItemSheetGroup group2,
			@RequestParam(name="group3", required=true) JobItemSheetGroup group3,
			@RequestParam(name="order", required=true) JobItemSheetOrder order,
			@RequestParam(name="pagegroup", required=false, defaultValue="NONE") JobItemSheetPageGroup pageGroup
		) throws IOException, EngineException{ 
		    Job job = this.jobService.get(jobId);
		
		Map<String, Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID, jobId);
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_TYPE, BaseDocumentType.JOB_SHEET);
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_LOCALE, locale);
		additionalParameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_1, group1);
		additionalParameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_2, group2);
		additionalParameters.put(JobItemSheetItemsDataSet.PARAMETER_GROUP_3, group3);
		additionalParameters.put(JobItemSheetItemsDataSet.PARAMETER_ORDER, order);
		additionalParameters.put(JobItemSheetItemsDataSet.PARAMETER_PAGEGROUP, pageGroup);
		
		// create new file naming service
		JobFileNamingService fnService = new JobFileNamingService(job, true);
		// delete existing files
		fnService.deleteExistingJobSheets();
		
		Document doc = documentService.createBirtDocument(jobId, BaseDocumentType.JOB_SHEET,
				locale, Component.JOB, fnService, additionalParameters);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		model.asMap().clear();
		return "redirect:viewjob.htm?jobid=" + jobId + "&loadtab=jobfiles-tab";
		
	}
}
