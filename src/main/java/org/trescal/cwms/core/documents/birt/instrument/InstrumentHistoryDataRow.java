package org.trescal.cwms.core.documents.birt.instrument;

import java.util.Date;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tools.DateTools;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class InstrumentHistoryDataRow {

    private String jobNo;
    private Integer jobItem;
    private Date receivedDate;
    private String certNo;
    private String verificationStatus;
    private Date calDate;
    private Date sortDate;
    private String deviation;

    public InstrumentHistoryDataRow(Certificate certificate, JobItem jobItem, Instrument instrument) {

        String deviationUnits = (instrument.getInstrumentComplementaryField() != null
                && instrument.getInstrumentComplementaryField().getDeviationUnits() != null) ?
                instrument.getInstrumentComplementaryField().getDeviationUnits().getFormattedSymbol() : null;

        if (jobItem != null) {
            this.jobNo = jobItem.getJob().getJobno();
            this.jobItem = jobItem.getItemNo();
            this.receivedDate = DateTools.dateFromZonedDateTime(jobItem.getDateIn());
        }

        if (certificate != null) {
            this.certNo = certificate.getCertno();
            this.verificationStatus = certificate.getCalibrationVerificationStatus() != null ?
                    certificate.getCalibrationVerificationStatus().getName() : "";
            this.calDate = certificate.getCalDate();
            if (certificate.getDeviation() != null) {
                this.deviation = certificate.getDeviation().stripTrailingZeros().toPlainString() + " " + deviationUnits;
            }
        }

        this.sortDate = certificate != null ? certificate.getCalDate() : jobItem != null ? DateTools.dateFromZonedDateTime(jobItem.getDateIn()) : null;
    }

}
