package org.trescal.cwms.core.documents.birt.jobcosting;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;

@Component
public class JobCostingDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JobCostingService jobcostingService;

	private static final String CODE_TITLE = "docs.jobcosting";
	private static final String DEFAULT_MESSAGE_TITLE = "Job Costing";

	@Override
	public Address getSourceAddress(Integer entityId) {
		JobCosting jobcosting  = jobcostingService.get(entityId);
		// Default address of the job's subdivision (not using job costing, at it is at Company level)
		return jobcosting.getJob().getOrganisation().getDefaultAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		JobCosting jobcosting = jobcostingService.get(entityId);
		// Use contact's default address 
		return jobcosting.getContact().getDefAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		JobCosting jobcosting = jobcostingService.get(entityId);
		return jobcosting.getOrganisation();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		JobCosting jobcosting = jobcostingService.get(entityId);
		return jobcosting.getJob().getJobno();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE,null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}

	@Override
	public boolean exists(Integer entityId) {
		JobCosting jobcosting = jobcostingService.get(entityId);
		return (jobcosting != null);
	}

	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
