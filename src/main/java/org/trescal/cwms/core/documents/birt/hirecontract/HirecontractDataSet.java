package org.trescal.cwms.core.documents.birt.hirecontract;

import java.math.BigDecimal;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

public class HirecontractDataSet extends EntityDataSet<Hire> {

	private HireService hireService;
	private Locale locale;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		hireService = super.getApplicationContext().getBean(HireService.class);
		locale = super.getDocumentLocale();
	}

	@Override
	public Hire getEntity(Integer entityId) {
		return hireService.get(entityId);
	}

	@Override
	public void setColumnValues(Hire hire, IUpdatableDataSetRow row) throws ScriptException {
		SupportedCurrency currency = hire.getCurrency();
		row.setColumnValue("hireno", hire.getHireno());
		row.setColumnValue("subdivision", hire.getAddress().getSub().getSubname());
		row.setColumnValue("contact", hire.getContact().getName());

		if (hire.getCrdes() != null) {
			row.setColumnValue("carriageby", hire.getCrdes().getCdtype().getCourier().getName());
			row.setColumnValue("carriagebydesc", hire.getCrdes().getCdtype().getDescription());
		} else {
			row.setColumnValue("carriagebydesc", "");
			row.setColumnValue("carriageby", "");
		}
		row.setColumnValue("telephone", hire.getContact().getTelephone());
		row.setColumnValue("email", hire.getContact().getEmail());
		row.setColumnValue("clientref", hire.getClientref());
		row.setColumnValue("clientpo", hire.getPoNumber());
		row.setColumnValue("hirestartdate", hire.getHireDate());
		row.setColumnValue("totalhirecost", CurrencyValueFormatter.format(hire.getTotalCost(), currency, locale));
		// TODO: remove carriage cost from document
		row.setColumnValue("carriageCost", CurrencyValueFormatter.format(BigDecimal.valueOf(0, 2), currency, locale));
		row.setColumnValue("nettotal", CurrencyValueFormatter.format(hire.getFinalCost(), currency, locale));
	}
}