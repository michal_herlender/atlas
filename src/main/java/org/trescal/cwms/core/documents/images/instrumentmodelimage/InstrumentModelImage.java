package org.trescal.cwms.core.documents.images.instrumentmodelimage;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Entity
@DiscriminatorValue("model")
public class InstrumentModelImage extends Image
{
	private InstrumentModel model;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid")
	public InstrumentModel getModel()
	{
		return this.model;
	}

	public void setModel(InstrumentModel model)
	{
		this.model = model;
	}
}
