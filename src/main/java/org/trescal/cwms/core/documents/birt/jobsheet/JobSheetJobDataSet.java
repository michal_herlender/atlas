package org.trescal.cwms.core.documents.birt.jobsheet;


import java.util.Date;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.tools.DateTools;

public class JobSheetJobDataSet extends EntityDataSet<Job> {

	private JobService jobService;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		jobService = super.getApplicationContext().getBean(JobService.class);
	}

	@Override
	public Job getEntity(Integer entityId) {
		return jobService.get(entityId);
	}

	@Override
	public void setColumnValues(Job job, IUpdatableDataSetRow row) throws ScriptException {
		row.setColumnValue("clientCompanyName", job.getCon().getSub().getComp().getConame());
		row.setColumnValue("clientCompanyNumber", job.getCon().getSub().getComp().getLegalIdentifier());
		row.setColumnValue("clientSubdivisionName", job.getCon().getSub().getSubname());
		row.setColumnValue("clientContactName", job.getCon().getName());
		row.setColumnValue("clientContactPhone", job.getCon().getTelephone());
		row.setColumnValue("clientContactEmail", job.getCon().getEmail());
		row.setColumnValue("returnAddressId", job.getReturnTo().getAddrid());
		
		row.setColumnValue("jobNumber", job.getJobno());
		row.setColumnValue("jobClientRef", job.getClientRef());
		row.setColumnValue("trescalContactName", job.getCreatedBy().getName());
		row.setColumnValue("trescalContactPhone", job.getCreatedBy().getTelephone());
		row.setColumnValue("trescalContactEmail", job.getCreatedBy().getEmail());
		row.setColumnValue("jobcreationdate", DateTools.dateFromLocalDate(job.getRegDate()));
		row.setColumnValue("instrumentcount", job.getItems().size());
		
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("clientCompanyName", String.class);
		metaData.addColumn("clientCompanyNumber", String.class);
		metaData.addColumn("clientSubdivisionName", String.class);
		metaData.addColumn("clientContactName", String.class);
		metaData.addColumn("clientContactPhone", String.class);
		metaData.addColumn("clientContactEmail", String.class);
		metaData.addColumn("returnAddressId", Integer.class);
		
		metaData.addColumn("jobNumber", String.class);
		metaData.addColumn("jobClientRef", String.class);
		metaData.addColumn("trescalContactName", String.class);
		metaData.addColumn("trescalContactPhone", String.class);
		metaData.addColumn("trescalContactEmail", String.class);
		metaData.addColumn("jobcreationdate", Date.class);
		metaData.addColumn("instrumentcount", Integer.class);
		
		return true;
	}
}