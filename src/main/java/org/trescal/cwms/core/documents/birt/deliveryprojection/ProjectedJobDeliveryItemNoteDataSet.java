package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;

public class ProjectedJobDeliveryItemNoteDataSet extends IterableDeliveryNoteDataSet {

	@Override
	public Iterator<NoteProjectionDTO> getIterator(IDataSetInstance dataSet) throws ScriptException {
		DeliveryItemDTO diDto = super.getDeliveryItemDTO(dataSet);
		List<NoteProjectionDTO> notes = diDto.getNotes();
		if (notes == null)
			notes = Collections.emptyList();
		return notes.stream()
				.filter(dto -> dto.getActive() && dto.getPublish())
				.collect(Collectors.toList())
				.iterator();
	}
}
