package org.trescal.cwms.core.documents.birt.repaircompletionreport;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO holder for data contained in OneToMany relationships
 * from repair inspection report / repair completion report
 * 
 * Holds data for components
 * Intended for use in FreeRepairComponent data set 
 *
 */
@Getter @Setter
public class RepairDocumentComponentDTO {
	private String name;
	private Integer operation;	// From the connected operation
	private Integer quantity;
}
