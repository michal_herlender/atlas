package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.tools.DateTools;

/**
 * Custom FileNamingService implementation for deliverys. Custom behaviour is to
 * create a filename indicating the type of delivery note being created (client /
 * third party), the delivery reference and the date of creation. No handling
 * for revisions of documents created on the same day is implemented, existing
 * files will be overwritten.
 * 
 * @author richard
 */
public class DeliveryFileNamingService implements FileNamingService
{
	private Delivery delivery;
	private String delPath;
	protected final Log logger = LogFactory.getLog(this.getClass());

	/**
	 * @param delivery The delivery to create obtain the filepath for.
	 * @param jobPath The path to the job folder for the job the delivery is
	 *        being created for.
	 */
	public DeliveryFileNamingService(Delivery delivery, String delPath)
	{
		this.delivery = delivery;
		this.delPath = delPath;
	}

	@Override
	public String getFileName()
	{
		// build the basic path to the deliveries folder in the job folder
		String filePath = this.delPath.concat(File.separator);

		String fileName = "";
		if (this.delivery.isThirdPartyDelivery())
		{
			fileName = "Third Party Delivery - ";
		}
	    else if (this.delivery.isInternalDelivery())
	    {
			fileName = "Internal Delivery - ";
		}
	    else if (this.delivery.isInternalReturnDelivery())
	    {
			fileName = "Internal Return Delivery - ";
		}
		else
		{
			fileName = "Client Delivery - ";
		}

		// include the reference number of the delivery note
		fileName = fileName.concat(this.delivery.getDeliveryno().replace('/', '.')).concat(" - ").concat(DateTools.df.format(new Date()));

		return filePath.concat(fileName);
	}
}
