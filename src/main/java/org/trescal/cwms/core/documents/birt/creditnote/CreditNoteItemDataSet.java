package org.trescal.cwms.core.documents.birt.creditnote;

import java.util.Iterator;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;

public class CreditNoteItemDataSet extends IterableDataSet<CreditNoteItem>{

	private Set<CreditNoteItem> creditNoteItems;

	private static final Logger logger = LoggerFactory.getLogger(CreditNoteItemDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {

		CreditNoteService creditNoteService = super.getApplicationContext().getBean(CreditNoteService.class);

		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		CreditNote creditnote = creditNoteService.get(id);
		creditNoteItems = creditnote.getItems();
	}

	@Override
	public Iterator<CreditNoteItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return creditNoteItems.iterator();
	}

	@Override
	public void setColumnValues(CreditNoteItem creditNoteItem, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		logger.debug("creditnoteitem id : "+creditNoteItem.getId());

		row.setColumnValue("itemno", creditNoteItem.getItemno());
		row.setColumnValue("description", creditNoteItem.getDescription());
		row.setColumnValue("totalcost", creditNoteItem.getTotalCost().doubleValue());
	}	

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("itemno", Integer.class);
		metaData.addColumn("description", String.class);		
		metaData.addColumn("totalcost", Double.class);
		return true;
	}

}
