package org.trescal.cwms.core.documents.filenamingservice;

import org.apache.commons.lang3.StringUtils;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.tools.DateTools;

import java.io.File;
import java.util.Date;

public class RepairInspectionReportFileNamingService implements FileNamingService {

	private RepairInspectionReport rir;
	private boolean completionReport;
	private String directory;

	/*
	 * Directory passed in, as this isn't a true bean
	 */
	public RepairInspectionReportFileNamingService(RepairInspectionReport rir, boolean completionReport, String reportDirectory) {
		this.rir = rir;
		this.completionReport = completionReport;
		this.directory = reportDirectory;
	}
	
	@Override
	public String getFileName() {
		StringBuffer fileName = new StringBuffer();
		if (this.completionReport) {
			fileName.append(RepairCompletionReport.repairCompletionReportIdentifierPrefix);
			if(StringUtils.isNotBlank(rir.getRepairCompletionReport().getRcrNumber()))
			{
				fileName.append(rir.getRepairCompletionReport().getRcrNumber());
				fileName.append(" - ");
			}
		}
		else {
			
			fileName.append("Repair Inspection Report  - ");
			if(StringUtils.isNotBlank(rir.getRirNumber()))
			{
				fileName.append(rir.getRirNumber());
				fileName.append(" - ");
			}
		}
		fileName.append(DateTools.dtf_files.format(new Date()));
		
		StringBuffer result = new StringBuffer(directory);
		result.append(File.separator);
		result.append(fileName);
		return result.toString();
	}

}
