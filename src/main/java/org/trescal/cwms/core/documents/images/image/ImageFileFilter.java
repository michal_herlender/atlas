package org.trescal.cwms.core.documents.images.image;

import java.io.File;
import java.io.FilenameFilter;

public class ImageFileFilter implements FilenameFilter
{
	@Override
	public boolean accept(File f, String s)
	{
		return s.endsWith(".jpg") || s.endsWith(".JPG") || s.endsWith(".png")
				|| s.endsWith(".PNG") || s.endsWith(".GIF")
				|| s.endsWith(".gif");
	}

}
