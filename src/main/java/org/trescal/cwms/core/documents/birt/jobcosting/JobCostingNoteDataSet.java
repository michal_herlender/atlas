package org.trescal.cwms.core.documents.birt.jobcosting;

import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingnote.JobCostingNote;

public class JobCostingNoteDataSet extends NoteDataSet<JobCostingNote> {

	private JobCostingService jobcostingService;
	private Integer id;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		jobcostingService = getApplicationContext().getBean(JobCostingService.class);
		id  = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
	}

	@Override
	public Iterator<JobCostingNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		JobCosting jobCost = jobcostingService.get(id); 
		return super.getActivePublishedNotes(jobCost.getNotes()).iterator();
	}

}
