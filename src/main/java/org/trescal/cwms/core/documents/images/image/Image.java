package org.trescal.cwms.core.documents.images.image;

import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.images.taggedimage.TaggedImage;
import org.trescal.cwms.core.documents.images.taggedimage.TaggedImageComparator;
import org.trescal.cwms.files.ForeignDBField;
import org.trescal.cwms.files.images.entity.ImageFileData;
import org.trescal.cwms.files.images.entity.ImageThumbData;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "images")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("simple")
public class Image extends Auditable {
	private Contact addedBy;
	private Date addedOn;
	private String description;
	private transient boolean exists;
	private transient File file;
	private ImageFileData imageFile;
	private String fileName;
	private Integer id;
	@Deprecated
	private String relativeFilePath;
	private Set<TaggedImage> tags;
	private ImageThumbData thumbFile;
	
	@Id
	// Was @GeneratedValue(strategy=GenerationType.TABLE) from commit 1094; retrying with IDENTITY 2017-08-22
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId()
	{
		return this.id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getAddedBy()
	{
		return this.addedBy;
	}

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "addedon", nullable = false)
	public Date getAddedOn()
	{
		return this.addedOn;
	}

	@Length(max = 200)
	@Column(name = "description", length = 200)
	public String getDescription()
	{
		return this.description;
	}

	@Transient
	public File getFile()
	{
		return this.file;
	}
	
	@Transient
    public byte[] getFileData() {
		if(imageFile == null) {
			ImageFileData ifd = getImageFile();
			return ifd == null ? new byte[0] : ifd.getFileData();
		}
		else return imageFile.getFileData();
    }
	
    public String getFileName() {
		return fileName;
	}

	@Deprecated
	@Length(max = 300)
	@Column(name = "rfilepath", length = 300, nullable = true)
	public String getRelativeFilePath()
	{
		return this.relativeFilePath;
	}

	@Transient
	public String getRelativeThumbPath()
	{
		String thumbPath = "";
		if ((this.getRelativeFilePath() != null)
			&& (this.getRelativeFilePath().indexOf('.') > -1)) {
			int i = this.relativeFilePath.lastIndexOf('.');
			thumbPath = this.relativeFilePath.substring(0, i).concat("-tb").concat(this.relativeFilePath.substring(i));
		}
		return thumbPath;
	}
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "image")
	@SortComparator(TaggedImageComparator.class)
	public Set<TaggedImage> getTags()
	{
		return this.tags;
	}
	
	@Transient
	public byte[] getThumbData() {
		if(thumbFile == null) {
			ImageThumbData itd = getThumbFile();
			return itd == null ? new byte[0] : itd.getThumbData();
		}
		else return thumbFile.getThumbData();
	}
	
	@Transient
	public boolean isExists()
	{
		return this.exists;
	}

	public void setAddedBy(Contact addedBy)
	{
		this.addedBy = addedBy;
	}

	public void setAddedOn(Date addedOn)
	{
		this.addedOn = addedOn;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setExists(boolean exists)
	{
		this.exists = exists;
	}

	public void setFile(File file)
	{
		this.file = file;
	}
    
    public void setId(Integer id)
	{
		this.id = id;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public void setRelativeFilePath(String relativeFilePath)
	{
		this.relativeFilePath = relativeFilePath;
	}

	public void setTags(Set<TaggedImage> tags)
	{
		this.tags = tags;
	}
	
	@Transient
	@ForeignDBField(fkField="id")
	public ImageFileData getImageFile() {
		return imageFile;
	}
	
	public void setImageFile(ImageFileData imageFile) {
		this.imageFile = imageFile;
	}
	
	@Transient
	@ForeignDBField(fkField="id")
	public ImageThumbData getThumbFile() {
		return thumbFile;
	}
	
	public void setThumbFile(ImageThumbData thumbFile) {
		this.thumbFile = thumbFile;
	}
}