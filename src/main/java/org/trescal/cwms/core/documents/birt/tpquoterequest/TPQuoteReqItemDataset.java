package org.trescal.cwms.core.documents.birt.tpquoterequest;

import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;

public class TPQuoteReqItemDataset extends IterableDataSet<TPQuoteRequestItem> {

	private Set<TPQuoteRequestItem> items;
	private TranslationService translationService;
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		TPQuoteRequestService tpQuoteRequestService = super.getApplicationContext().getBean(TPQuoteRequestService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);

		Integer id = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		TPQuoteRequest tpQuoteRequest = tpQuoteRequestService.get(id);
		items = tpQuoteRequest.getItems();
	}
	
	@Override
	public Iterator<TPQuoteRequestItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return items.iterator();
	}

	@Override
	public void setColumnValues(TPQuoteRequestItem rqitem, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		
		row.setColumnValue("item",rowCount);
		row.setColumnValue("itemid" ,rqitem.getId());
		
		String serialno = "/";
		if(rqitem.getRequestFromJobItem()!= null) {
			JobItem jobItem = rqitem.getRequestFromJobItem(); 
			if (jobItem != null) {
				serialno = jobItem.getInst().getSerialno();
			}
		}
		InstrumentModel model = rqitem.getModel();
		Locale documentLocale = super.getDocumentLocale(); 
		Locale primaryLocale = super.getPrimaryLocale();
		String instmod = "";
		
		if(rqitem.getRequestFromJobItem()!= null){
			Instrument instr = rqitem.getRequestFromJobItem().getInst();
			instmod = InstModelTools.instrumentModelNameViaTranslations(instr, documentLocale, primaryLocale);
			
		}else {
			instmod = InstModelTools.modelNameViaTranslations(model, documentLocale, primaryLocale);
		}
		
		row.setColumnValue("instmod" ,instmod);
		row.setColumnValue("referenceno" ,rqitem.getReferenceNo());
		row.setColumnValue("qty" ,rqitem.getQty());
		row.setColumnValue("serialno", serialno);
		
		String modelNumber = null;
		String brand = null;
		
		if(rqitem.getRequestFromJobItem()!= null) {
			Instrument inst = rqitem.getRequestFromJobItem().getInst();
			row.setColumnValue("barcode", inst.getPlantid());
			if (inst.getModel().getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC)) 
				brand = inst.getModel().getMfr().getName();
			else if (inst.getMfr() != null)
				brand = inst.getMfr().getName();
			if (!inst.getModel().getModel().equals("/"))
				modelNumber = inst.getModel().getModel();
			else
				modelNumber = inst.getModelname();
		}
		else {
			if (model.getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC)) 
				brand = model.getMfr().getName();
			modelNumber = model.getModel();
		}
		
		row.setColumnValue("brand", brand != null ? brand : "/");
		row.setColumnValue("modelNumber", modelNumber != null ? modelNumber : "/");
		
		Set<Translation> translations = rqitem.getCaltype().getServiceType().getLongnameTranslation();
		String caltype = translationService.getCorrectTranslation(translations, super.getDocumentLocale());
		row.setColumnValue("caltype", caltype);
	}

}
