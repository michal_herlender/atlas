package org.trescal.cwms.core.documents.images.systemimage.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.images.systemimage.SystemImage;

public interface SystemImageDao extends BaseDao<SystemImage, Integer> {
	
	public SystemImage findSystemImageByBusinessCompany(Company comp);
	
	public List<SystemImage> findSystemImagesByBusinessCompany(Company comp);

}
