/**
 * 
 */
package org.trescal.cwms.core.documents;

import java.io.File;

import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

/**
 * Simple interface implmented by all entites that have a
 * {@link SystemComponent} directory associated with them. Gurantees access to
 * the file location.
 * 
 * @author Richard
 */
public interface EntityHasDirectory
{
	public File getDirectory();

	public void setDirectory(File file);
}