package org.trescal.cwms.core.documents.birt.repaircompletionreport;

import java.util.Date;
import java.util.Locale;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationTypeEnum;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.enums.AdjustmentPerformedEnum;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;


public class RepairInspectionReportDataSet extends EntityDataSet<RepairInspectionReport> {
	// Beans
	private TranslationService translationService;
	private RepairInspectionReportService repairInspectionReportService;
	// Value of passed report parameter(s)
	private Boolean completionReport; 
	
	private static final Logger logger = LoggerFactory.getLogger(RepairInspectionReportDataSet.class);
    
    /**
     * Mandatory boolean parameter used to indicate whether to render 
     * this report as a "completion report"
     *   true = "Repair Completion Report" 
     *   false = "Repair Inspection Report" 
     */
	public final static String PARAMETER_COMPLETION_REPORT = "completionReport";

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		repairInspectionReportService = super.getApplicationContext().getBean(RepairInspectionReportService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
		completionReport = (Boolean) reportContext.getParameterValue(RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT);
		if (completionReport == null) {
			logger.error("Required parameter not set, defaulting to false : "+RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT);
			completionReport = false;
		}
	}

	@Override
	public RepairInspectionReport getEntity(Integer entityId) {
		logger.info("getting entity");
		return repairInspectionReportService.get(entityId);
	}

	@Override
	public void setColumnValues(RepairInspectionReport rir, IUpdatableDataSetRow row) throws ScriptException {
		logger.info("setting column values");
		Locale locale = super.getDocumentLocale();
		// TODO - determine if there should be a "generating contact" stored in the report

		String trescalName = rir.getTechnician() != null ? rir.getTechnician().getName() : "N/A";
		String trescalPhone = rir.getTechnician() != null ? rir.getTechnician().getTelephone() : "N/A";
		String trescalFax = rir.getTechnician() != null ? rir.getTechnician().getFax() : "N/A";
		String trescalEmail = rir.getTechnician() != null ? rir.getTechnician().getEmail() : "N/A";

		// TODO - consider making RER Allocated
		String trescalSite = rir.getTechnician() != null
				? rir.getTechnician().getSub().getComp().getConame() + " - " + rir.getTechnician().getSub().getSubname()
				: "";

		row.setColumnValue("trescalName", trescalName);
		row.setColumnValue("trescalPhone", trescalPhone);
		row.setColumnValue("trescalFax", trescalFax);
		row.setColumnValue("trescalEmail", trescalEmail);
		row.setColumnValue("trescalSite", trescalSite);
		row.setColumnValue("clientName", rir.getJi().getJob().getCon().getName());
		row.setColumnValue("clientPhone", rir.getJi().getJob().getCon().getTelephone());
		row.setColumnValue("clientFax", rir.getJi().getJob().getCon().getFax());
		row.setColumnValue("clientEmail", rir.getJi().getJob().getCon().getEmail());

		if (rir.getJi().getOnBehalf() != null) {
			row.setColumnValue("clientCompany", rir.getJi().getOnBehalf().getCompany().getConame());
			row.setColumnValue("clientDepartment", rir.getJi().getOnBehalf().getAddress().getSub().getSubname());
		} else {
			row.setColumnValue("clientCompany", rir.getJi().getJob().getCon().getSub().getComp().getConame());
			row.setColumnValue("clientDepartment", rir.getJi().getJob().getCon().getSub().getSubname());
		}
		
		Instrument inst = rir.getJi().getInst();
		if (inst.getCustomerDescription() != null && !inst.getCustomerDescription().isEmpty()) {
			row.setColumnValue("instDescription", inst.getCustomerDescription());
		} else {
			Set<Translation> translations = inst.getModel().getDescription().getTranslations();
			row.setColumnValue("instDescription", translationService.getCorrectTranslation(translations, locale));
		}

		if (inst.getModel().getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC)) {
			row.setColumnValue("instBrand", inst.getModel().getMfr().getName());
		} else if (inst.getMfr() != null) {
			row.setColumnValue("instBrand", inst.getMfr().getName());
		} else {
			row.setColumnValue("instBrand", "/");
		}

		if (inst.getModel().getModel().equals("/") && inst.getModelname() != null) {
			row.setColumnValue("instModel", inst.getModelname());
		} else {
			row.setColumnValue("instModel", inst.getModel().getModel());
		}
		
		row.setColumnValue("instSerial", inst.getSerialno());
		row.setColumnValue("instPlantno", inst.getPlantno());
		row.setColumnValue("instPlantid", inst.getPlantid());
		row.setColumnValue("jobno", rir.getJi().getJob().getJobno());
		row.setColumnValue("jobitemno", rir.getJi().getItemNo());
		row.setColumnValue("clientjobref", rir.getJi().getJob().getClientRef());
		row.setColumnValue("clientjobitemref", rir.getJi().getClientRef());
		row.setColumnValue("collectiondate", DateTools.dateFromZonedDateTime(rir.getJi().getJob().getPickupDate()));
		row.setColumnValue("receiptdate", DateTools.dateFromZonedDateTime(rir.getJi().getDateIn()));
		row.setColumnValue("clientdescription", rir.getClientDescription());
		row.setColumnValue("clientinstructions", rir.getClientInstructions());
		row.setColumnValue("internalDescription", rir.getInternalDescription());
		row.setColumnValue("internalInspection", rir.getInternalInspection());
		row.setColumnValue("leadtime", rir.getLeadTime());
		row.setColumnValue("returndate", rir.getEstimatedReturnDate());
		row.setColumnValue("completedBy", rir.getTechnician() != null ? rir.getTechnician().getName() : "N/A");
		row.setColumnValue("completedDate", rir.getCompletedByTechnicianOn());
		row.setColumnValue("validatedBy", rir.getManager() != null ? rir.getManager().getName() : "N/A");
		row.setColumnValue("validatedOn", rir.getValidatedByManagerOn());
		row.setColumnValue("requiresInternalRepair", calculateRequiresInternalRepair(rir.getFreeRepairOperations()));
		row.setColumnValue("requiresExternalRepair", calculateRequiresExternalRepair(rir.getFreeRepairOperations()));
		row.setColumnValue("requiresNoRepair",(rir.getFreeRepairOperations() == null || rir.getFreeRepairOperations().isEmpty()));
		row.setColumnValue("calibrationServiceType",getCalibrationServiceType(rir.getJi()));
		row.setColumnValue("serviceType",(translationService.getCorrectTranslation(rir.getJi().getServiceType().getLongnameTranslation(),locale)));
		row.setColumnValue("trescalWarranty",rir.getTrescalWarranty());
		row.setColumnValue("misuse",rir.getMisuse());
		row.setColumnValue("misuseComment",rir.getMisuseComment());
		row.setColumnValue("manufacturerWarranty",rir.getManufacturerWarranty());
		row.setColumnValue("manufactureNoWarranty",(!rir.getManufacturerWarranty() && !rir.getTrescalWarranty()));
		
		if(rir.getRepairCompletionReport()!= null)
		{
			RepairCompletionReport rcr = rir.getRepairCompletionReport();
			row.setColumnValue("internalCompletionComments", rcr.getInternalCompletionComments());
			row.setColumnValue("externalCompletionComments", rcr.getExternalCompletionComments());
			row.setColumnValue("validatedByRcr", rcr.getValidatedBy() != null ? rcr.getValidatedBy().getName() : "N/A");
			row.setColumnValue("validatedDateRcr", rcr.getValidatedOn());
			row.setColumnValue("adjustment", AdjustmentPerformedEnum.YES.equals(rcr.getAdjustmentPerformed()));
		}
		else {
			row.setColumnValue("adjustment", false);
		}
		row.setColumnValue("completionReport", this.completionReport);
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("trescalName", String.class);
		metaData.addColumn("trescalPhone", String.class);
		metaData.addColumn("trescalFax", String.class);
		metaData.addColumn("trescalEmail", String.class);
		metaData.addColumn("trescalSite", String.class);
		
		metaData.addColumn("clientName", String.class);
		metaData.addColumn("clientPhone", String.class);
		metaData.addColumn("clientFax", String.class);
		metaData.addColumn("clientEmail", String.class);
		metaData.addColumn("clientCompany", String.class);
		
		metaData.addColumn("clientDepartment", String.class);
		metaData.addColumn("instDescription", String.class);
		metaData.addColumn("instBrand", String.class);
		metaData.addColumn("instModel", String.class);
		metaData.addColumn("instSerial", String.class);
		
		metaData.addColumn("instPlantno", String.class);
		metaData.addColumn("instPlantid", Integer.class);
		metaData.addColumn("jobno", String.class);
		metaData.addColumn("jobitemno", Integer.class);
		metaData.addColumn("clientjobref", String.class);
		
		metaData.addColumn("clientjobitemref", String.class);
		metaData.addColumn("collectiondate", Date.class);
		metaData.addColumn("receiptdate", Date.class);
		metaData.addColumn("clientdescription", String.class);
		metaData.addColumn("clientinstructions", String.class);
		
		metaData.addColumn("internalDescription", String.class);
		metaData.addColumn("internalInspection", String.class);
		metaData.addColumn("leadtime", Integer.class);
		metaData.addColumn("returndate", Date.class);
		metaData.addColumn("completedBy", String.class);
		
		metaData.addColumn("completedDate", Date.class);
		metaData.addColumn("validatedBy", String.class);
		metaData.addColumn("validatedOn", Date.class);
		metaData.addColumn("requiresInternalRepair", Boolean.class);
		metaData.addColumn("requiresExternalRepair", Boolean.class);
		
		metaData.addColumn("requiresNoRepair", Boolean.class);
		metaData.addColumn("calibrationServiceType", String.class);
		metaData.addColumn("serviceType", String.class);
		metaData.addColumn("trescalWarranty", Boolean.class);
		metaData.addColumn("manufacturerWarranty", Boolean.class);
		
		metaData.addColumn("manufactureNoWarranty", Boolean.class);
		metaData.addColumn("misuse", Boolean.class);
		metaData.addColumn("adjustment", Boolean.class);
		metaData.addColumn("misuseComment", String.class);
		metaData.addColumn("internalCompletionComments", String.class);
		
		metaData.addColumn("externalCompletionComments", String.class);
		metaData.addColumn("validatedByRcr", String.class);
		metaData.addColumn("validatedDateRcr", Date.class);
		metaData.addColumn("completionReport", Boolean.class);
		return true;
	}

	private Boolean calculateRequiresInternalRepair(Set<FreeRepairOperation> operations) {
		boolean result = false;
		for (FreeRepairOperation operation : operations) {
			if (operation.getOperationType().equals(RepairOperationTypeEnum.INTERNAL)) {
				result = true;
				break;
			}
			;
		}
		return result;
	}

	private Boolean calculateRequiresExternalRepair(Set<FreeRepairOperation> operations) {
		boolean result = false;
		for (FreeRepairOperation operation : operations) {
			if (operation.getOperationType().equals(RepairOperationTypeEnum.EXTERNAL)) {
				result = true;
				break;
			}
			;
		}
		return result;
	}

	private String getCalibrationServiceType(JobItem jobitem) {
		Locale locale = super.getDocumentLocale();
		ServiceType serviceType = null;
		WorkRequirement wr = null;
		for (JobItemWorkRequirement jiwr : jobitem.getWorkRequirements()) {
			if (!jiwr.getWorkRequirement().getServiceType().getRepair()) {
				wr = jiwr.getWorkRequirement();
					break;
			}
		}
		if(wr != null)
			serviceType = wr.getServiceType();
		
		String result = "";
	    if (serviceType != null) {
			Set<Translation> stTranslations = serviceType.getShortnameTranslation();
			Set<Translation> lgTranslations = serviceType.getLongnameTranslation();
			result = translationService.getCorrectTranslation(stTranslations, locale) +" - "+ translationService.getCorrectTranslation(lgTranslations, locale);
	    }
		return result;
	}
}
