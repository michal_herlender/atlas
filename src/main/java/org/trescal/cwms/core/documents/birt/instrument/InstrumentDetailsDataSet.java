package org.trescal.cwms.core.documents.birt.instrument;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.tools.DateTools;

public class InstrumentDetailsDataSet extends EntityDataSet<Instrument> {

    // Initialized in beforeOpen()
    private InstrumService instrumentService;
    private CalReqService calReqService;


    @Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
        instrumentService = super.getApplicationContext().getBean(InstrumService.class);
        calReqService = super.getApplicationContext().getBean(CalReqService.class);
    }

    @Override
    public Instrument getEntity(Integer entityId) {
        return this.instrumentService.get(entityId);
    }


    @Override
    public void setColumnValues(Instrument instrument, IUpdatableDataSetRow row) throws ScriptException {
        row.setColumnValue("instrumentname", instrumentService.getCorrectInstrumentModelName(instrument));
        row.setColumnValue("serialno", instrument.getSerialno());
        row.setColumnValue("plantno", instrument.getPlantno());
        row.setColumnValue("barcode", instrument.getPlantid());
        row.setColumnValue("lastcaldate", instrumentService.getLastCalDate(instrument));
        row.setColumnValue("calduedate", DateTools.dateFromLocalDate(instrument.getNextCalDueDate()));
        row.setColumnValue("calfrequency", instrument.getCalFrequency() +
                " " + instrument.getCalFrequencyUnit().getName(instrument.getCalFrequency()));
        row.setColumnValue("subdiv", instrument.getCon().getSub().getSubname());
        row.setColumnValue("owner", instrument.getCon().getName());
        row.setColumnValue("calrequirement", calReqService.findCalReqsForInstrument(instrument) != null
                ? calReqService.findCalReqsForInstrument(instrument).getPublicInstructions() : "");
    }



}
