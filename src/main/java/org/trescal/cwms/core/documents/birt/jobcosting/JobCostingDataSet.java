package org.trescal.cwms.core.documents.birt.jobcosting;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.DateTools;

import java.util.Date;
import java.util.Locale;

public class JobCostingDataSet extends EntityDataSet<JobCosting> {

	private JobCostingService jobcostingService;
	private Locale locale;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		jobcostingService = super.getApplicationContext().getBean(JobCostingService.class);
		locale = super.getDocumentLocale();
	}

	@Override
	public JobCosting getEntity(Integer entityId) {
		return jobcostingService.get(entityId);
	}

	@Override
	public void setColumnValues(JobCosting jobCosting, IUpdatableDataSetRow row) throws ScriptException {
		
		SupportedCurrency currency = jobCosting.getCurrency();
		
		row.setColumnValue("jobno", jobCosting.getJob().getJobno());
		row.setColumnValue("costingVersion", jobCosting.getVer());
		row.setColumnValue("creationDate", DateTools.dateFromLocalDate(jobCosting.getRegdate()));
		row.setColumnValue("returnAddressId", jobCosting.getJob().getReturnTo().getAddrid());
		row.setColumnValue("clientReference", jobCosting.getClientref());
		row.setColumnValue("clientContactName", jobCosting.getContact().getName());
		row.setColumnValue("clientContactEmail", jobCosting.getContact().getEmail());
		row.setColumnValue("clientContactPhone", jobCosting.getContact().getTelephone());
		row.setColumnValue("clientContactFax", jobCosting.getContact().getFax());
		row.setColumnValue("businessContactName", jobCosting.getCreatedBy().getName());
		row.setColumnValue("businessContactEmail", jobCosting.getCreatedBy().getEmail());
		row.setColumnValue("businessContactPhone", jobCosting.getCreatedBy().getTelephone());
		row.setColumnValue("businessContactFax", jobCosting.getCreatedBy().getFax());

		row.setColumnValue("showInspectionCosts", jobCosting.getCostingType().isIncludeInspectionCharges());
		row.setColumnValue("itemCountRegular", jobCosting.getItems().size());
		row.setColumnValue("itemCountExpense", jobCosting.getExpenseItems().size());
		
		row.setColumnValue("finalCost", CurrencyValueFormatter.format(jobCosting.getFinalCost(), currency, locale));
		row.setColumnValue("vatRate", jobCosting.getVatRate().doubleValue());
		row.setColumnValue("vatValue", CurrencyValueFormatter.format(jobCosting.getVatValue(), currency, locale));
		row.setColumnValue("totalCost", CurrencyValueFormatter.format(jobCosting.getTotalCost(), currency, locale));
		row.setColumnValue("showVAT", !this.jobcostingService.getAvalaraAware());
	}
	
	/**
	 * TODO discuss PO / BPO requirements for job costing
	 * This reflects the implementation of the OLD SQL BIRT job costing document
	 * PO number first if set, otherwise BPO number if set 
	 */
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("jobno", String.class);
		metaData.addColumn("costingVersion", Integer.class);
		metaData.addColumn("creationDate", Date.class);
		metaData.addColumn("returnAddressId", Integer.class);
		metaData.addColumn("clientReference", String.class);
		metaData.addColumn("clientContactName", String.class);
		metaData.addColumn("clientContactEmail", String.class);
		metaData.addColumn("clientContactPhone", String.class);
		metaData.addColumn("clientContactFax", String.class);
		metaData.addColumn("businessContactName", String.class);
		metaData.addColumn("businessContactEmail", String.class);
		metaData.addColumn("businessContactPhone", String.class);
		metaData.addColumn("businessContactFax", String.class);
		
		metaData.addColumn("showInspectionCosts", Boolean.class);
		metaData.addColumn("itemCountRegular", Integer.class);
		metaData.addColumn("itemCountExpense", Integer.class);

		metaData.addColumn("finalCost", String.class);
		metaData.addColumn("carriage", Double.class);
		metaData.addColumn("vatRate", Double.class);
		metaData.addColumn("vatValue", String.class);
		metaData.addColumn("totalCost", String.class);
		
		metaData.addColumn("showVAT", Boolean.class);
		
		return true;
	}

}