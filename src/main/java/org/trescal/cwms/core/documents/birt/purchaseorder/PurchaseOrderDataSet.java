package org.trescal.cwms.core.documents.birt.purchaseorder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.pricing.purchaseorder.entity.enums.PurchaseOrderTaxableOption;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.DateTools;

public class PurchaseOrderDataSet extends EntityDataSet<PurchaseOrder> {
   private PurchaseOrderService purchaseOrderService;
   private Locale locale;
   private PurchaseOrder po;

   @Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		purchaseOrderService = super.getApplicationContext().getBean(PurchaseOrderService.class); 
		locale = super.getDocumentLocale();
		
		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		this.po = purchaseOrderService.find(id);
		
	}
	
	@Override
	public PurchaseOrder getEntity(Integer entityId) {
		return this.po;
	}

	@Override
	public void setColumnValues(PurchaseOrder po, IUpdatableDataSetRow row) throws ScriptException {
		SupportedCurrency currency = po.getCurrency();
		
		row.setColumnValue("issuedate",getIssueDate(po));
		row.setColumnValue("businessContactName",po.getBusinessContact().getName());
		row.setColumnValue("businessContactEmail",po.getBusinessContact().getEmail());
		row.setColumnValue("accountno",po.getReturnToAddress().getSub().getComp().getCoid());
		row.setColumnValue("reference",po.getClientref());
		row.setColumnValue("businessContactTelephone", po.getBusinessContact().getTelephone());
		row.setColumnValue("businessContactFax", po.getBusinessContact().getFax());			
		row.setColumnValue("deliveryCompany", po.getReturnToAddress().getSub().getComp().getConame());
		row.setColumnValue("deliveryAddress1", po.getReturnToAddress().getAddr1());
		row.setColumnValue("deliveryAddress2", po.getReturnToAddress().getAddr2());
		row.setColumnValue("deliveryAddress3", po.getReturnToAddress().getAddr3());
		row.setColumnValue("deliverytown", po.getReturnToAddress().getTown());
		row.setColumnValue("deliveryRegion", po.getReturnToAddress().getCounty());
		Locale documentLocale = super.getDocumentLocale();
		String countryCode = po.getReturnToAddress().getCountry().getCountryCode();
		String destinationCountryName = (new Locale("", countryCode)).getDisplayCountry(documentLocale);
		row.setColumnValue("deliverycountry", destinationCountryName);
		row.setColumnValue("deliverypostcode", po.getReturnToAddress().getPostcode());
		row.setColumnValue("fax", po.getContact().getFax());
		row.setColumnValue("currencyname", po.getCurrency().getCurrencyName());
		row.setColumnValue("contactName", po.getContact().getName());
		row.setColumnValue("contactEmail", po.getContact().getEmail());
		row.setColumnValue("contactTelephone", po.getContact().getTelephone());
		row.setColumnValue("contactFax", po.getContact().getFax());		
		row.setColumnValue("totals", CurrencyValueFormatter.format(po.getTotalCost(), currency, locale));
		
		boolean useTaxableOption = po.getOrganisation().getBusinessSettings().getUseTaxableOption();
		boolean taxableNotApplicable = po.getTaxable().equals(PurchaseOrderTaxableOption.NOT_APPLICABLE);
		boolean displayTaxable = useTaxableOption && !taxableNotApplicable;  
        row.setColumnValue("displayTaxable", displayTaxable);
        row.setColumnValue("taxable",po.getTaxable().equals(PurchaseOrderTaxableOption.YES));
		// We show item discount information on document only if at least one item is discounted
		row.setColumnValue("discountCount", countDiscounts(po));
	}
	
	private Date getIssueDate(PurchaseOrder po) {
		// Document may be generated before it is actually 'issued' so we use today's date if issued date null
		if (po.getIssuedate() == null)
			return new Date();
		else 
			return DateTools.dateFromLocalDate(po.getIssuedate());		
	}
	
	private int countDiscounts(PurchaseOrder po) {
		int result = 0;
		for (PurchaseOrderItem item : po.getItems()) {
			if (item.getGeneralDiscountRate().compareTo(BigDecimal.ZERO) > 0) {
				result++;
			}
		}
		return result;
	}
}
