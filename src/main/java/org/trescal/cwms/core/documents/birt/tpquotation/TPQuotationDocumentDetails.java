package org.trescal.cwms.core.documents.birt.tpquotation;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;

@Component
public class TPQuotationDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;
	private static final String CODE_TITLE = "docs.supplierquotation";
	private static final String DEFAULT_MESSAGE_TITLE ="Supplier Quotation";

	@Autowired
	private TPQuotationService tpQuotationService;

	@Override
	public Address getSourceAddress(Integer entityId) {
		TPQuotation tpq = tpQuotationService.get(entityId);
		return tpq.getCreatedBy().getDefAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		TPQuotation tpq = tpQuotationService.get(entityId);
		return tpq.getContact().getDefAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		TPQuotation tpq = tpQuotationService.get(entityId);
		return tpq.getOrganisation();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		TPQuotation tpq = tpQuotationService.get(entityId);
		return tpq.getQno();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE, null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}

	@Override
	public boolean exists(Integer entityId) {
		TPQuotation tpq = tpQuotationService.get(entityId);
		return (tpq != null);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
