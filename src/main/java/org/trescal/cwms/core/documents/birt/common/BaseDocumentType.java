package org.trescal.cwms.core.documents.birt.common;

/*
 * Describes the different supported base document types that are available
 * (note, will be gradually implemented)
 * There will sometimes be more than 1 type of document for an entity,
 * so class lookup by itself won't suffice.
 */
public enum BaseDocumentType {
	GENERAL_DELIVERY("/birt/scripted/delivery/general_delivery.rptdesign"),
	CLIENT_DELIVERY("/birt/scripted/delivery/client_delivery.rptdesign"),
	INTERNAL_DELIVERY("/birt/scripted/delivery/client_delivery.rptdesign"),
	THIRD_PARTY_DELIVERY("/birt/scripted/delivery/tp_delivery.rptdesign"),
	PURCHASE_ORDER("/birt/scripted/purchaseorder/purchase_order.rptdesign"),
	TP_QUOTE_REQUEST("/birt/scripted/tp_quote_request/tp_quote_request.rptdesign"),
	TP_QUOTATION("/birt/scripted/tpquotation/tp_quotation.rptdesign"),
	QUOTATION("/birt/scripted/quotation/quotation.rptdesign"),
	FAULT_REPORT("/birt/scripted/faultreport/new_fault.rptdesign"),
	HIRE_CONTRACT("/birt/scripted/hire/hire_contract.rptdesign"),
	OFF_HIRE("/birt/scripted/hire/off_hire.rptdesign"),
	INVOICE_CALIBRATION("/birt/scripted/invoices/invoice_calibration.rptdesign"),
	INVOICE_US_LETTER("/birt/scripted/invoices/invoice_US_Letter.rptdesign"),
	COST_CENTRE_PO_REQUEST("/birt/scripted/costcentreporequest/costcentreporequest.rptdesign"),
	FAILURE_REPORT("/birt/scripted/failurereport/failure_report.rptdesign"),
	JOB_ITEM_SHEET("/birt/scripted/jobitemsheet/jobitemsheet.rptdesign"),
	JOB_SHEET("/birt/scripted/jobsheet/jobsheet.rptdesign"),
	CREDIT_NOTE("/birt/scripted/creditnote/creditnote.rptdesign"),
	REPAIR_EVALUATION_REPORT("/birt/scripted/repairinspectionreport/repairinspection_report.rptdesign"),
	REPAIR_COMPLETION_REPORT("/birt/scripted/repairinspectionreport/repairinspection_report.rptdesign"),
	FAILURE_REPORT_2014("/birt/scripted/failurereport/2014/failure_report_2014.rptdesign"),
	JOB_COSTING("/birt/scripted/jobcosting/jobcosting.rptdesign"),
	DELIVERY_PROJECTION("/birt/scripted/delivery/delivery_projection.rptdesign"),
	SUPPLIER_DELIVERY_PROJECTION("/birt/scripted/delivery/supplier_delivery_projection.rptdesign"),
	RETURN_DELIVERY_PROJECTION("/birt/scripted/delivery/return_delivery_projection.rptdesign");

	private String templatePath;
	private BaseDocumentType(String templatePath) {
		this.templatePath = templatePath;
	}
	public String getTemplatePath() {
		return templatePath;
	}
	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}
}
