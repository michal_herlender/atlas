package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;

@Component
public class GeneralDeliveryDocumentDetails extends DeliveryDocumentDetails {
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private DeliveryService deliveryService; 
	
	public static final String CODE_TITLE = "docs.generaldeliverynote";
	public static final String DEFAULT_MESSAGE_TITLE = "General Delivery Note";
	
	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE, null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}
	
	@Override
	public boolean exists(Integer entityId) {
		GeneralDelivery delivery = this.deliveryService.get(entityId, GeneralDelivery.class);
		return (delivery != null);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
