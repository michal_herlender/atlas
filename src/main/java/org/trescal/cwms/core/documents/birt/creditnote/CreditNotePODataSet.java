package org.trescal.cwms.core.documents.birt.creditnote;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

public class CreditNotePODataSet extends IterableDataSet<String> {

	private Set<String> clientPONumbers;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		CreditNoteService creditNoteService = super.getApplicationContext().getBean(CreditNoteService.class);

		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		CreditNote creditNote = creditNoteService.get(id);
		
		clientPONumbers = new TreeSet<>();
		// For the purpose of display on credit notes, we want to include POs that are attached to at least one invoice item
		// Currently, there is no direct connection from credit note item to PO
		for (InvoiceItem item : creditNote.getInvoice().getItems()) {
			Optional<String> invoicePO = item.getInvPOsItem().stream().map(InvoicePOItem::getInvPO)
					.map(InvoicePO::getPoNumber).findFirst();
			Optional<String> jobPO = item.getInvItemPOs().stream().filter(iip -> iip.getJobPO() != null).map(InvoiceItemPO::getJobPO)
					.map(PO::getPoNumber).findFirst();
			Optional<String> bpo = item.getInvItemPOs().stream().filter(iip -> iip.getBpo() != null).map(InvoiceItemPO::getBpo)
					.map(BPO::getPoNumber).findFirst();

			// Precedence is Invoice PO, then Job PO, then BPO (in case multiples selected)
			String poNumber = invoicePO.orElse(jobPO.orElse(bpo.orElse(null)));
			if (poNumber != null) clientPONumbers.add(poNumber);
		}
	}

	@Override
	public Iterator<String> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return clientPONumbers.iterator();
	}

	@Override
	public void setColumnValues(String instance, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("poNumber", instance);
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("poNumber", String.class);		
		return true;
	}

}
