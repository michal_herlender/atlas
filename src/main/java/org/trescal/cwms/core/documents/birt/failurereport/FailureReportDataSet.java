package org.trescal.cwms.core.documents.birt.failurereport;

import org.apache.commons.lang3.BooleanUtils;
import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportStateEnum;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.StringTools;

import java.util.Date;
import java.util.Locale;
import java.util.Set;

public class FailureReportDataSet extends EntityDataSet<FaultReport> {
	private TranslationService translationService;
	private FaultReportService faultReportService;

	private static final Logger logger = LoggerFactory.getLogger(FailureReportDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		faultReportService = super.getApplicationContext().getBean(FaultReportService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
	}

	@Override
	public FaultReport getEntity(Integer entityId) {
		logger.info("getting entity");
		return faultReportService.findFaultReport(entityId);
	}

	@Override
	public void setColumnValues(FaultReport instance, IUpdatableDataSetRow row) throws ScriptException {
		logger.info("setting column values");

		// Hide additional (English) labels as a second language when an English
		// language report is requested.
		Locale locale = super.getDocumentLocale();
		row.setColumnValue("hideSecondLanguage", locale.getLanguage().equals("en"));
		// TODO - determine if there should be a "generating contact" stored in the
		// report
		String trescalName = instance.getTechnician() != null ? instance.getTechnician().getName() : "";
		String trescalPhone = instance.getTechnician() != null ? instance.getTechnician().getTelephone() : "";
		String trescalFax = instance.getTechnician() != null ? instance.getTechnician().getFax() : "";
		// TODO - consider making Fault Report Allocated
		String trescalSite = instance.getOrganisation().getComp().getConame() + " - "
				+ instance.getOrganisation().getSubname();

		row.setColumnValue("trescalName", trescalName);
		row.setColumnValue("trescalPhone", trescalPhone);
		row.setColumnValue("trescalFax", trescalFax);
		row.setColumnValue("trescalSite", trescalSite);
		row.setColumnValue("clientName", instance.getJobItem().getJob().getCon().getName());
		row.setColumnValue("clientPhone", instance.getJobItem().getJob().getCon().getTelephone());
		row.setColumnValue("clientFax", instance.getJobItem().getJob().getCon().getFax());
		if (instance.getJobItem().getOnBehalf() != null) {
			row.setColumnValue("clientCompany", instance.getJobItem().getOnBehalf().getCompany().getConame());
			row.setColumnValue("clientDepartment",
					instance.getJobItem().getOnBehalf().getAddress().getSub().getSubname());
		} else {
			row.setColumnValue("clientCompany", instance.getJobItem().getJob().getCon().getSub().getComp().getConame());
			row.setColumnValue("clientDepartment", instance.getJobItem().getJob().getCon().getSub().getSubname());
		}

		row.setColumnValue("problemDate", instance.getIssueDate() != null ? instance.getIssueDate() : new Date());
		row.setColumnValue("problemReference", instance.getFaultReportNumber());

		Instrument inst = instance.getJobItem().getInst();
		if (inst.getCustomerDescription() != null && !inst.getCustomerDescription().isEmpty()) {
			row.setColumnValue("instDescription", inst.getCustomerDescription());
		} else {
			Set<Translation> translations = inst.getModel().getDescription().getTranslations();
			row.setColumnValue("instDescription", translationService.getCorrectTranslation(translations, locale));
		}

		if (inst.getModel().getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC)) {
			row.setColumnValue("instBrand", inst.getModel().getMfr().getName());
		} else if (inst.getMfr() != null) {
			row.setColumnValue("instBrand", inst.getMfr().getName());
		} else {
			row.setColumnValue("instBrand", "/");
		}

		if (inst.getModel().getModel().equals("/") && inst.getModelname() != null) {
			row.setColumnValue("instModel", inst.getModelname());
		} else {
			row.setColumnValue("instModel", inst.getModel().getModel());
		}
		row.setColumnValue("instSerial", inst.getSerialno());
		row.setColumnValue("instPlantno", inst.getPlantno());

		row.setColumnValue("jobno", instance.getJobItem().getJob().getJobno());
		row.setColumnValue("jobitemno", instance.getJobItem().getItemNo());

		// TODO when fault report fields are available, populate all these values
		// boolean values for "as found section", listed in same order as on document
		row.setColumnValue("asFoundNotCompliant",
				(instance.getStates() != null) && instance.getStates().stream().anyMatch(s -> !s.isIndeterminate()));
		row.setColumnValue("asFoundOutOfSpecification",
				instance.getStates().contains(FailureReportStateEnum.OUT_OF_SPECIFICATION));
		row.setColumnValue("asFoundOutOfService", instance.getStates().contains(FailureReportStateEnum.OUT_OF_SERVICE));
		row.setColumnValue("asFoundFailureDescription",
				instance.getStates().contains(FailureReportStateEnum.FAILURE_NOT_COMMUNICATED));
		row.setColumnValue("asFoundSafetyDefault",
				instance.getStates().contains(FailureReportStateEnum.SAFETY_DEFAULT));
		row.setColumnValue("asFoundIndeterminate",
				(instance.getStates() != null) && instance.getStates().stream().anyMatch(s -> s.isIndeterminate()));
		row.setColumnValue("asFoundUnknownMPE", instance.getStates().contains(FailureReportStateEnum.UNKNOWN_MPES));
		row.setColumnValue("asFoundEquipmentQuality",
				instance.getStates().contains(FailureReportStateEnum.EQUIPMENT_QUALITY_INADEQUATE));
		row.setColumnValue("asFoundMissingAccessories",
				instance.getStates().contains(FailureReportStateEnum.MISSING_ACCESSORIES));
		row.setColumnValue("asFoundOther", instance.getStates().contains(FailureReportStateEnum.OTHER));
		// Text values for "as found" section
		row.setColumnValue("textAsFoundMissingAccessories", ""); // TODO implement (potentially as set) for new version
																	// of failure report
		row.setColumnValue("textAsFoundOther", formatMultiLineText(instance.getOtherState()));
		row.setColumnValue("textAsFoundComments", formatMultiLineText(instance.getTechnicianComments()));
		// boolean values for "recommendation section", listed in same order as document
		row.setColumnValue("customerAdviceNecessary",
				instance.getClientResponseNeeded() != null ? !instance.getClientResponseNeeded() : false);
		row.setColumnValue("actionCalWithJudgement",
				instance.getRecommendations().contains(FailureReportRecommendationsEnum.CALIBRATION_WITH_JUDGMENT));
		row.setColumnValue("actionCalWithoutJudgement",
				instance.getRecommendations().contains(FailureReportRecommendationsEnum.CALIBRATION_WITHOUT_JUDGMENT));
		row.setColumnValue("actionAdjustment",
				instance.getRecommendations().contains(FailureReportRecommendationsEnum.ADJUSTMENT));
		row.setColumnValue("actionRepair",
				instance.getRecommendations().contains(FailureReportRecommendationsEnum.REPAIR));
		row.setColumnValue("actionScrap",
				instance.getRecommendations().contains(FailureReportRecommendationsEnum.SCRAPPING_PROPOSAL));
		row.setColumnValue("actionRestriction",
				instance.getRecommendations().contains(FailureReportRecommendationsEnum.RESTRICTION));
		row.setColumnValue("actionDispensation", BooleanUtils.isTrue(instance.getDispensation()));
		row.setColumnValue("actionOther",
				instance.getRecommendations().contains(FailureReportRecommendationsEnum.OTHER));

		// boolean values for who and where
		row.setColumnValue("operationTrescal",
				instance.getOperationByTrescal() != null ? instance.getOperationByTrescal() : false);
		row.setColumnValue("operationSupplier",
				instance.getOperationByTrescal() != null ? !instance.getOperationByTrescal() : false);
		row.setColumnValue("operationLaboratory",
				instance.getOperationInLaboratory() != null ? instance.getOperationInLaboratory() : false);
		row.setColumnValue("operationOnsite",
				instance.getOperationInLaboratory() != null ? !instance.getOperationInLaboratory() : false);
		// text/date values for "recommendation section"
		row.setColumnValue("textDispensationComments", formatMultiLineText(instance.getDispensationComments()));
		row.setColumnValue("textActionComments", formatMultiLineText(instance.getManagerComments()));
		row.setColumnValue("textDeliveryTime",
				instance.getEstDeliveryTime() != null ? instance.getEstDeliveryTime() : 0);
		row.setColumnValue("textManagerName",
				instance.getManagerValidationBy() != null ? instance.getManagerValidationBy().getName() : "");
		row.setColumnValue("dateManagerApproval",
				instance.getManagerValidationOn() != null ? instance.getManagerValidationOn() : new Date());

		// boolean values for client approval
		if (instance.getClientApproval() != null) {
			row.setColumnValue("clientAgreement", instance.getClientApproval());
			row.setColumnValue("clientDisagreement",
					instance.getClientApproval() != null ? !instance.getClientApproval() : false);
			// text/date values for client approval
			row.setColumnValue("clientComments", formatMultiLineText(instance.getClientComments()));
			String clientApprovalName = null;
			if (instance.getClientApprovalBy() != null)
				clientApprovalName = instance.getClientApprovalBy().getName();
			else if (instance.getClientAlias() != null)
				clientApprovalName = instance.getClientAlias();
			row.setColumnValue("clientApprovalName", clientApprovalName);
			row.setColumnValue("clientApprovalDate", instance.getClientApprovalOn());
		}

		// date value for footer
		row.setColumnValue("documentGenerationDate", new Date());
	}
	
	/**
	 * Returns empty string if input is null,
	 * otherwise replaces line breaks with <br/> symbol
	 * followed by line break (for situations e.g. debugging 
	 * where output text is well formatted) 
	 * 
	 * For use in "Dynamic Text" element formatted as HTML 
	 * Consider extracting to static method (StringTools / BirtTools?)
	 */
	private String formatMultiLineText(String inputText) {
		String result = "";
		if (inputText != null)
			result = StringTools.replaceLineBreaksWithSpecifiedString(inputText, "<br/>\n");
		return result;
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("hideSecondLanguage", Boolean.class);
		metaData.addColumn("trescalName", String.class);
		metaData.addColumn("trescalPhone", String.class);
		metaData.addColumn("trescalFax", String.class);
		metaData.addColumn("trescalSite", String.class);
		metaData.addColumn("clientName", String.class);
		metaData.addColumn("clientPhone", String.class);
		metaData.addColumn("clientFax", String.class);
		metaData.addColumn("clientCompany", String.class);
		metaData.addColumn("clientDepartment", String.class);
		metaData.addColumn("problemDate", Date.class);
		metaData.addColumn("problemReference", String.class);
		metaData.addColumn("instDescription", String.class);
		metaData.addColumn("instBrand", String.class);
		metaData.addColumn("instModel", String.class);
		metaData.addColumn("instSerial", String.class);
		metaData.addColumn("instPlantno", String.class);
		metaData.addColumn("jobno", String.class);
		metaData.addColumn("jobitemno", Integer.class);

		metaData.addColumn("asFoundNotCompliant", Boolean.class);
		metaData.addColumn("asFoundOutOfSpecification", Boolean.class);
		metaData.addColumn("asFoundOutOfService", Boolean.class);
		metaData.addColumn("asFoundFailureDescription", Boolean.class);
		metaData.addColumn("asFoundSafetyDefault", Boolean.class);
		metaData.addColumn("asFoundIndeterminate", Boolean.class);
		metaData.addColumn("asFoundUnknownMPE", Boolean.class);
		metaData.addColumn("asFoundEquipmentQuality", Boolean.class);
		metaData.addColumn("asFoundMissingAccessories", Boolean.class);
		metaData.addColumn("asFoundOther", Boolean.class);
		// Text values for "as found" section
		metaData.addColumn("textAsFoundMissingAccessories", String.class);
		metaData.addColumn("textAsFoundOther", String.class);
		metaData.addColumn("textAsFoundComments", String.class);
		// boolean values for "re-commendation section", listed in same order as
		// document
		metaData.addColumn("customerAdviceNecessary", Boolean.class);
		metaData.addColumn("actionCalWithJudgement", Boolean.class);
		metaData.addColumn("actionCalWithoutJudgement", Boolean.class);
		metaData.addColumn("actionAdjustment", Boolean.class);
		metaData.addColumn("actionRepair", Boolean.class);
		metaData.addColumn("actionScrap", Boolean.class);
		metaData.addColumn("actionRestriction", Boolean.class);
		metaData.addColumn("actionDispensation", Boolean.class);
		metaData.addColumn("actionOther", Boolean.class);

		// boolean values for who and where
		metaData.addColumn("operationTrescal", Boolean.class);
		metaData.addColumn("operationSupplier", Boolean.class);
		metaData.addColumn("operationLaboratory", Boolean.class);
		metaData.addColumn("operationOnsite", Boolean.class);
		// text/date values for "recommendation section"
		metaData.addColumn("textDispensationComments", String.class);
		metaData.addColumn("textActionComments", String.class);
		metaData.addColumn("textDeliveryTime", String.class);
		metaData.addColumn("textManagerName", String.class);
		metaData.addColumn("dateManagerApproval", Date.class);

		// boolean values for client approval
		metaData.addColumn("clientAgreement", Boolean.class);
		metaData.addColumn("clientDisagreement", Boolean.class);
		// text/date values for client approval
		metaData.addColumn("clientComments", String.class);
		metaData.addColumn("clientApprovalName", String.class);
		metaData.addColumn("clientApprovalDate", Date.class);

		// date value for footer
		metaData.addColumn("documentGenerationDate", Date.class);

		return true;
	}

}
