package org.trescal.cwms.core.documents.birt.quotation;

import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationConditions;

public class QuotationCalibrationConditionDataSet extends IterableDataSet<QuotationCalibrationCondition> {
	private QuotationConditions quotationConditions;
	private final static Logger logger = LoggerFactory.getLogger(QuotationCalibrationConditionDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		QuotationService quotationService = super.getApplicationContext().getBean(QuotationService.class);
		Integer entityId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		Quotation quotation = quotationService.get(entityId);
		quotationConditions = quotationService.findQuotationConditions(quotation);
		logger.debug("Found "+quotationConditions.getCalCon().size()+" quotation cal conditions");
		for (QuotationCalibrationCondition qcc : quotationConditions.getCalCon()) {
			if (qcc == null) logger.debug("null qcc!");
			logger.debug(qcc.getDefCalConId()+" : "+qcc.getConditionText());
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Iterator<QuotationCalibrationCondition> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return (Iterator<QuotationCalibrationCondition>) quotationConditions.getCalCon().iterator();
	}

	@Override
	public void setColumnValues(QuotationCalibrationCondition conditionquote, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		// TODO : The Velocity document requires <br/> for formatting - change DB and remove once VM files deleted.
		String databaseText = conditionquote.getConditionText();
		String displayText = databaseText.replaceAll("<br/>", "");
		row.setColumnValue("conditiontext", displayText);
	}

}
