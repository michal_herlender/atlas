package org.trescal.cwms.core.documents.birt.tpquoterequest;

import java.util.Collection;

import org.trescal.cwms.core.documents.birt.common.EntityNoteDataSet;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;

public class TPQuoteReqINotesDataset extends EntityNoteDataSet<TPQuoteRequestNote>{

	@Override
	public Collection<TPQuoteRequestNote> getNotes(Integer entityId) {
		TPQuoteRequestService tpQuoteRequestService = getApplicationContext().getBean(TPQuoteRequestService.class);
		TPQuoteRequest tpqr = tpQuoteRequestService.get(entityId); 
		return tpqr.getNotes();
	}
}
