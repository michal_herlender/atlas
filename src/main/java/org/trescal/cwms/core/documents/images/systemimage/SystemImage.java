package org.trescal.cwms.core.documents.images.systemimage;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.images.systemimage.enums.SystemImageType;

import lombok.Setter;

@Entity
@Table(name = "systemimage")
@Setter
public class SystemImage {

	private Integer id;
	private SystemImageType type;
	private byte[] fileData;
	private String fileName;
	private String description;
	private Company businessCompany;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	public SystemImageType getType() {
		return type;
	}

	@NotNull
	@Column(name = "filedata", nullable = false, columnDefinition = "varbinary(max)")
	public byte[] getFileData() {
		return fileData;
	}

	@NotNull
	@Column(name = "fileName")
	public String getFileName() {
		return fileName;
	}

	@Nullable
	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", nullable = true)
	public Company getBusinessCompany() {
		return businessCompany;
	}

}
