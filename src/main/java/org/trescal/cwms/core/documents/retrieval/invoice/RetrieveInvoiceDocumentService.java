package org.trescal.cwms.core.documents.retrieval.invoice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.documents.retrieval.FileSystemDocumentRetriever;
import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentResult;
import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Service
public class RetrieveInvoiceDocumentService implements RetrieveDocumentService<CreditNote> {

	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private FileSystemDocumentRetriever fileSystemRetriever;
	
	@Override
	public Class<?> getEntityClass() {
		return Invoice.class;
	}

	@Override
	public RetrieveDocumentResult getLatestDocumentForEntity(Integer entityId) {
		RetrieveDocumentResult result = null;
		Invoice invoice = this.invoiceService.findInvoice(entityId);
		if (invoice != null) {
			result = this.fileSystemRetriever.getLatestDocumentFromFileSystem(Component.INVOICE, invoice.getInvno());
		}
		else {
			result = RetrieveDocumentResult.builder()
					.success(false)
					.errorMessage("Entity not found for specified input")
					.build();
		}
		
		return result;
	}

}
