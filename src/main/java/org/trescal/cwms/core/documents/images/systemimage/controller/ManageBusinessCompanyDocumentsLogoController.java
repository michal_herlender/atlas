package org.trescal.cwms.core.documents.images.systemimage.controller;

import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import javax.activation.MimetypesFileTypeMap;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.documents.images.systemimage.SystemImage;
import org.trescal.cwms.core.documents.images.systemimage.db.SystemImageService;
import org.trescal.cwms.core.documents.images.systemimage.form.SystemImageForm;
import org.trescal.cwms.core.documents.images.systemimage.validator.SystemImageValidator;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY })
public class ManageBusinessCompanyDocumentsLogoController {

	@Autowired
	SystemImageService sysImageService;
	@Autowired
	CompanyService compService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SystemImageValidator validator;

	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);

	}

	@ModelAttribute("command")
	protected SystemImageForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> company) {
		SystemImageForm form = new SystemImageForm();
		form.setBusinessCompId(company.getKey());
		return form;
	}

	@RequestMapping(value = "/managebusinesscomplogo.htm", method = RequestMethod.GET)
	public String referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> company,
			Model model) {
		List<SystemImage> systemImages = this.sysImageService
				.findSystemImagesByBusinessCompany(this.compService.get(company.getKey()));
		if (!systemImages.isEmpty())
			model.addAttribute("systemImages", systemImages);

		model.addAttribute("compId", company.getKey());
		return "trescal/core/admin/managebusinesscomplogo";
	}

	@RequestMapping(value = "/managebusinesscomplogo.htm", method = RequestMethod.POST)
	public String onSubmit(@Valid @ModelAttribute("command") SystemImageForm form, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> company, Model model) {

		if (!bindingResult.hasErrors()) {
			this.sysImageService.insertSystemImage(form);
		}

		return referenceData(company, model);
	}

	@RequestMapping(value = "/deleteSystemImage.json", method = RequestMethod.GET)
	public @ResponseBody ResultWrapper delete(@RequestParam(value = "id", required = true) int id, Locale locale) {

		SystemImage img = this.sysImageService.get(id);
		if (img == null)
			return new ResultWrapper(false, messages.getMessage("error.rest.data.notfound", null, locale));
		else
			this.sysImageService.delete(img);

		return new ResultWrapper(true, "ok");
	}

	@RequestMapping(value = "/displaybusinessCompanyLogo", method = RequestMethod.GET)
	public ResponseEntity<byte[]> showImage(@RequestParam("id") Integer imgId) {
		return generateResponseEntity(imgId, img -> img.getFileData());
	}

	private ResponseEntity<byte[]> generateResponseEntity(Integer imgId, Function<SystemImage, byte[]> dataFunction) {
		SystemImage img = this.sysImageService.get(imgId);
		MimetypesFileTypeMap mimeTypeMap = new MimetypesFileTypeMap();
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(mimeTypeMap.getContentType(img.getFileName())))
				.body(dataFunction.apply(img));
	}

}
