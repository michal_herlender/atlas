package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;

public class DeliveryItemPODataSet extends IterableDataSet<String> {

	private DeliveryItemService deliveryItemService;
	public static final Logger logger = LoggerFactory.getLogger(DeliveryItemPODataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		deliveryItemService = super.getApplicationContext().getBean(DeliveryItemService.class);
	}

	@Override
	public Iterator<String> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer itemId = (Integer) dataSet.getInputParameterValue("itemid");
		logger.debug(itemId == null ? "itemId is null" : "itemId = "+itemId.toString());
		JobDeliveryItem jobDeliveryItem = (JobDeliveryItem) deliveryItemService.findDeliveryItem(itemId, JobDeliveryItem.class);
		List<String> poNumbers = jobDeliveryItem.getJobitem().getItemPOs().stream()
				.map(po -> (po.getBpo() != null) ? po.getBpo().getPoNumber() : po.getPo().getPoNumber())
				.collect(Collectors.toList());


		return poNumbers.iterator();
	}

	@Override
	public void setColumnValues(String ponum, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("ponum", ponum);
	}

}
