package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
@Component
public class ClientDeliveryDocumentDetails extends DeliveryDocumentDetails {

	@Autowired 
	private MessageSource messageSource;
	@Autowired
	private DeliveryService deliveryService; 
	
	public static final String CODE_TITLE = "docs.clientdeliverynote";
	public static final String DEFAULT_MESSAGE_TITLE = "Client Delivery Note";
	
	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE, null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}
	
	@Override
	public boolean exists(Integer entityId) {
		ClientDelivery delivery = this.deliveryService.get(entityId, ClientDelivery.class);
		return (delivery != null);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
