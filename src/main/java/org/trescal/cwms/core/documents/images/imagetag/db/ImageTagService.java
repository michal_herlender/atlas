package org.trescal.cwms.core.documents.images.imagetag.db;

import java.util.List;

import org.trescal.cwms.core.documents.images.imagetag.ImageTag;

public interface ImageTagService
{
	ImageTag findImageTag(int id);
	void insertImageTag(ImageTag imagetag);
	void updateImageTag(ImageTag imagetag);
	List<ImageTag> getAllImageTags();
	void deleteImageTag(ImageTag imagetag);
	void saveOrUpdateImageTag(ImageTag imagetag);
}