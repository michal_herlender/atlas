package org.trescal.cwms.core.documents.birt.jobitemsheet;

/**
 * Defines different page group options for job item sheet
 * (to control page breaks) 
 */
public enum JobItemSheetPageGroup {
	NONE,
	GROUP_1,
	GROUP_2,
	GROUP_3
}
