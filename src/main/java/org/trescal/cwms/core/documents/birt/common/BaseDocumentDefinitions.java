package org.trescal.cwms.core.documents.birt.common;

/*
 * Defines static parameters shared between different document types
 */
public class BaseDocumentDefinitions {
	// Parameters for document generation
	public static final String PARAMETER_ENTITY_ID = "entityid";
	public static final String PARAMETER_DOCUMENT_TYPE = "doctype";
	public static final String PARAMETER_DOCUMENT_LOCALE = "locale";
	public static final String PARAMETER_FACSIMILE = "facsimile";
	// Parameters for document generation
	public static final String COL_DOCUMENT_TITLE = "doctitle";
	public static final String COL_DOCUMENT_REFERENCE = "docref";
	public static final String COL_RECIPIENT_ON_LEFT = "recipientonleft";
	// Rows at left of header, normally indicating Trescal source of document
	public static final String COL_SOURCE_ROW_1 = "source1";
	public static final String COL_SOURCE_ROW_2 = "source2";
	public static final String COL_SOURCE_ROW_3 = "source3";
	public static final String COL_SOURCE_ROW_4 = "source4";
	public static final String COL_SOURCE_ROW_5 = "source5";
	public static final String COL_SOURCE_ROW_6 = "source6";
	public static final String COL_SOURCE_ROW_7 = "source7";
	public static final String COL_SOURCE_ROW_8 = "source8";
	// Rows at right of header, normally indicating recipient of document
	public static final String COL_RECIPIENT_ROW_1 = "recipient1";
	public static final String COL_RECIPIENT_ROW_2 = "recipient2";
	public static final String COL_RECIPIENT_ROW_3 = "recipient3";
	public static final String COL_RECIPIENT_ROW_4 = "recipient4";
	public static final String COL_RECIPIENT_ROW_5 = "recipient5";
	public static final String COL_RECIPIENT_ROW_6 = "recipient6";
	public static final String COL_RECIPIENT_ROW_7 = "recipient7";
	public static final String COL_RECIPIENT_ROW_8 = "recipient8";
	// Footer rows at left (company name, registration)
	public static final String COL_FOOTER_LEFT_1 = "leftfooter1";
	public static final String COL_FOOTER_LEFT_2 = "leftfooter2";
	public static final String COL_FOOTER_LEFT_3 = "leftfooter3";
	public static final String COL_FOOTER_LEFT_4 = "leftfooter4";
	// Footer rows in middle (registered office)
	public static final String COL_FOOTER_MID_1 = "midfooter1";
	public static final String COL_FOOTER_MID_2 = "midfooter2";
	public static final String COL_FOOTER_MID_3 = "midfooter3";
	public static final String COL_FOOTER_MID_4 = "midfooter4";
}
