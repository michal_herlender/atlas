package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.Date;

import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.tools.DateTools;

/**
 * Naming service for creating cost centre purchase order requests.
 * Implementation names the files with a jobno and date and (1), (2) etc for
 * versions created on the same day.
 * 
 * @author Richard
 */
public class CostCentreFileNamingService implements FileNamingService
{
	private JobCosting jc;
	private String root;

	public CostCentreFileNamingService(JobCosting jc)
	{
		this.root = jc.getDirectory().toString();
		this.jc = jc;
	}

	@Override
	public String getFileName()
	{
		if (this.jc.getDirectory() != null)
		{
			String path = this.jc.getDirectory().getAbsolutePath().concat(File.separator);
			String filename = "Cost Centre PO Request - ".concat(this.jc.getQno().replace('/', '.')).concat(" - ").concat(DateTools.df.format(new Date()));
			filename = path.concat(filename);

			File file = new File(filename + ".html");
			int rev = 2;
			while (file.exists())
			{
				// check if this is the first rev (no brackets)
				if (filename.indexOf('(') < 0)
				{
					filename = filename.concat("(" + rev + ")");
					file = new File(filename + ".html");
					rev = rev + 1;
				}
				else
				{
					// there are already brackets, strip them out and add a new
					// bracket + revision
					filename = filename.substring(0, filename.lastIndexOf('(')).concat("("
							+ rev + ")");
					file = new File(filename + ".html");
					rev = rev + 1;
				}
			}

			return filename;
		}
		else
		{
			return new DefaultFileNamingService(Component.JOB_COSTING, this.root).getFileName();
		}

	}
}
