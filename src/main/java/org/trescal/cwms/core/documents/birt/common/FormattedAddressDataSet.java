package org.trescal.cwms.core.documents.birt.common;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormatter;

public class FormattedAddressDataSet extends IterableDataSet<String> {

	private List<String> addressLines;
	public static final String PARAMETER_ADDRESS_ID = "addressid";
	private static final Logger logger = LoggerFactory.getLogger(FormattedAddressDataSet.class); 
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		AddressService addressService = getApplicationContext().getBean(AddressService.class);
		Integer id = super.getInputParameter(dataSet, PARAMETER_ADDRESS_ID); 
		Address address = (id == 0) ? null : addressService.get(id);
		if (address == null) {
			logger.warn("Address not found for id "+(id == null ? "null" : id.toString()));
			addressLines = Collections.emptyList();
		}
		else {
			AddressPrintFormatter formatter = getApplicationContext().getBean(AddressPrintFormatter.class);
			Locale locale = super.getDocumentLocale();
			// Normally include company name on documents, e.g. full address
			boolean printCompanyName = true;
			addressLines = formatter.getAddressText(address, locale, printCompanyName);
		}
	}

	@Override
	public Iterator<String> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return addressLines.iterator();
	}

	@Override
	public void setColumnValues(String instance, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("addressline", instance);
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("addressline", String.class);
		return true;
	}

}
