package org.trescal.cwms.core.documents.birt.hirecontract;

import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

public class OffHireItemDataSet extends IterableDataSet<HireItem> {

	private Set<HireItem> hireItems;
	private Locale locale;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		HireService hireService = super.getApplicationContext().getBean(HireService.class);	
		locale = super.getDocumentLocale();
		Hire hire = hireService.get(id);
		hireItems = hire.getItems(); 
	}

	@Override
	public Iterator<HireItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return hireItems.iterator();
	}

	@Override
	public void setColumnValues(HireItem itemofhir, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		SupportedCurrency currency = itemofhir.getHire().getCurrency();
		row.setColumnValue("hireitemid", itemofhir.getId());
		row.setColumnValue("count", rowCount);
		if (itemofhir.getHireInst() != null)  {
			row.setColumnValue("plantno", itemofhir.getHireInst().getInst().getPlantno());
			
			String desc = InstModelTools.instrumentModelNameViaTranslations(itemofhir.getHireInst().getInst(), getDocumentLocale(),getPrimaryLocale());
			row.setColumnValue("description",desc);
		}
		else {
			row.setColumnValue("plantno",itemofhir.getHireCrossItem().getPlantno());
			row.setColumnValue("description",itemofhir.getHireCrossItem().getDescription());
		}
			row.setColumnValue("dailycost", itemofhir.getHireCost());
			row.setColumnValue("offhired", itemofhir.getOffHireDate());
			row.setColumnValue("hiredays", itemofhir.getOffHireDays());
			row.setColumnValue("hirecost", CurrencyValueFormatter.format(itemofhir.getFinalCost(), currency, locale));
	}	
}


