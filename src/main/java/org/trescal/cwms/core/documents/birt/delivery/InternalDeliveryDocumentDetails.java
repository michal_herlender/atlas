package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.internaldelivery.InternalDelivery;

@Component
public class InternalDeliveryDocumentDetails extends DeliveryDocumentDetails{

	@Autowired 
	private MessageSource messageSource;
	@Autowired
	private DeliveryService deliveryService; 
	
	public static final String CODE_TITLE = "docs.internaldeliverynote";
	public static final String DEFAULT_MESSAGE_TITLE = "Delivery Note";
	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE, null, DEFAULT_MESSAGE_TITLE, documentLocale);
		}

	@Override
	public boolean exists(Integer entityId) {
		InternalDelivery interdelivery = this.deliveryService.get(entityId,InternalDelivery.class);
		return (interdelivery != null);
	}

	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// TODO Auto-generated method stub
		return false;
	}

}
