package org.trescal.cwms.core.documents.birt.jobitemsheet;

import java.util.Comparator;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class JobItemSheetComparator implements Comparator<JobItem> {
	
	private JobItemSheetOrder order;
	
	public JobItemSheetComparator(JobItemSheetOrder order) {
		if (order == null) throw new IllegalArgumentException("order was null!");
		this.order = order;
	}

	@Override
	public int compare(JobItem jobItem0, JobItem jobItem1) {
		int result = 0;
		switch (order) {
		case BARCODE:
			result = jobItem0.getInst().getPlantid() - jobItem1.getInst().getPlantid();
			break;
		case JOB_ITEM_NUMBER:
			result = jobItem0.getItemNo() - jobItem1.getItemNo();
			break;
		case PLANT_NUMBER:
			result = compareString(jobItem0.getInst().getPlantno(), jobItem1.getInst().getPlantno());
			if (result == 0) result = jobItem0.getItemNo() - jobItem1.getItemNo();
			break;
		case SERIAL_NUMBER:
			result = compareString(jobItem0.getInst().getSerialno(), jobItem1.getInst().getSerialno());
			if (result == 0) result = jobItem0.getItemNo() - jobItem1.getItemNo();
			break;
		default:
			throw new UnsupportedOperationException("Undefined sort order!");
		}
		return result;
	}

	public int compareString(String value0, String value1) {
		int result = 0;
		if (value0 != null && value1 != null)
			result = value0.compareTo(value1);
		else if (value0 != null) 
			result = 1;
		else if (value1 != null) 
			result = -1;
		return result;
	}
	
}
