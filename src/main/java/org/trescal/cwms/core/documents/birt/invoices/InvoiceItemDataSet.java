package org.trescal.cwms.core.documents.birt.invoices;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.InstModelTools;

public class InvoiceItemDataSet extends IterableDataSet<InvoiceItem>{
	private Locale locale;
	private Locale fallbackLocale;
	private SimpleDateFormat deliveryDateFormat;
	private InvoiceService invoiceService;
	private MessageSource messageSource; 
	private TranslationService translationService;
	private Set<InvoiceItem> invoiceItems;
	
	private static final String CODE_WITH_DELIVERY_NOTE = "docs.invoicegroup.deliverynote";
	private static final String MESSAGE_WITH_DELIVERY_NOTE = "Delivery note {0} of {1}";
	private static final String CODE_NO_DELIVERY_NOTE = "docs.invoicegroup.nodeliverynote";
	private static final String MESSAGE_NO_DELIVERY_NOTE = "Without delivery note";
	
	private static final String DELIVERY_DATE_FORMAT = "dd MMM yyyy"; 

	private static final Logger logger = LoggerFactory.getLogger(InvoiceItemDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		locale = super.getDocumentLocale();
		fallbackLocale = super.getPrimaryLocale();
		invoiceService = super.getApplicationContext().getBean(InvoiceService.class);
		messageSource = super.getApplicationContext().getBean(MessageSource.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
		deliveryDateFormat = new SimpleDateFormat(DELIVERY_DATE_FORMAT, locale);

		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		Invoice invoice = invoiceService.findInvoice(id);
		invoiceItems = invoice.getItems();
	}

	@Override
	public Iterator<InvoiceItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return invoiceItems.iterator();
	}

	@Override
	public void setColumnValues(InvoiceItem invitem, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		SupportedCurrency currency = invitem.getInvoice().getCurrency();
		logger.debug("invoiceitemid : "+invitem.getId());

		row.setColumnValue("invoiceitemid", invitem.getId());
		row.setColumnValue("itemno", invitem.getItemno());
		row.setColumnValue("description", invitem.getDescription());
		

		// Defaults for optional fields derived from job item, etc...
		String caltype = "";
		String serialno = "";
		String jobno = "";
		String clientref = "";
		Integer jobitemno = 0;
		String plantno = "";
		String modelname = "";
		String expenseitem = "";
		String customerdescription = "";

		if(invitem.getJobItem() != null) {
			JobItem jobItem = invitem.getJobItem(); 
			Set<Translation> translationscaltype = jobItem.getServiceType().getLongnameTranslation();
			caltype = translationService.getCorrectTranslation(translationscaltype, locale);
			modelname = InstModelTools.instrumentModelNameViaTranslations(jobItem.getInst(), locale, fallbackLocale);
			jobno = jobItem.getJob().getJobno();
			jobitemno = jobItem.getItemNo();
			if (jobItem.getInst().getSerialno() != null) {
				serialno = jobItem.getInst().getSerialno();
			}
			if (jobItem.getInst().getPlantno() != null) {
				plantno = jobItem.getInst().getPlantno();
			}
			if (jobItem.getInst().getCustomerDescription() != null) {
				customerdescription = jobItem.getInst().getCustomerDescription(); 
			}
			
			if (invitem.getJobItem().getClientRef() != null) {
				clientref = invitem.getJobItem().getClientRef();
			}
		}
		if (invitem.getExpenseItem() != null){
			expenseitem = InstModelTools.modelNameViaTranslations(invitem.getExpenseItem().getModel(), locale, fallbackLocale);
		    jobno = invitem.getExpenseItem().getJob().getJobno();
		    jobitemno = invitem.getExpenseItem().getItemNo();
		    if (invitem.getExpenseItem().getClientRef() != null) {
		    	clientref = invitem.getExpenseItem().getClientRef();
		    }
		}
		boolean splitCosts = invitem.isBreakUpCosts();
		
		row.setColumnValue("caltype", caltype);
		row.setColumnValue("serialno", serialno);
		row.setColumnValue("jobno", jobno);
		row.setColumnValue("customerdescription", customerdescription);
		row.setColumnValue("jobitemno", jobitemno);
		row.setColumnValue("plantno", plantno);
		row.setColumnValue("modelname", modelname);
		row.setColumnValue("clientref", clientref);
		row.setColumnValue("expenseitem", expenseitem);
		row.setColumnValue("purchasecost", splitCosts ? getActiveSplitTotalCost(invitem.getPurchaseCost()) : getTotalCost(invitem, CostType.PURCHASE));
		row.setColumnValue("adjustmentcost", splitCosts ? getActiveSplitTotalCost(invitem.getAdjustmentCost()) : getTotalCost(invitem, CostType.ADJUSTMENT));
		row.setColumnValue("repaircost", splitCosts ? getActiveSplitTotalCost(invitem.getRepairCost()) : getTotalCost(invitem, CostType.REPAIR));
		row.setColumnValue("calibrationcost", splitCosts ? getActiveSplitTotalCost(invitem.getCalibrationCost()) : getTotalCost(invitem, CostType.CALIBRATION));
		row.setColumnValue("servicecost", splitCosts ? getActiveSplitTotalCost(invitem.getServiceCost()) : getTotalCost(invitem, null));
		row.setColumnValue("discountAmount",getTotalDiscount(invitem));
		row.setColumnValue("discountPercent",invitem.getGeneralDiscountRate());
		row.setColumnValue("totalCost", CurrencyValueFormatter.format(invitem.getTotalCost(), currency, locale));
		row.setColumnValue("finalCost", CurrencyValueFormatter.format(invitem.getFinalCost(), currency, locale));
		
		row.setColumnValue("qty", invitem.getQuantity());
		JobDeliveryItem jobDeliveryItem = getJobDeliveryItem(invitem);
		row.setColumnValue("deliveryheader", formatDeliveryHeader(jobDeliveryItem));
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("invoiceitemid", Integer.class);
		metaData.addColumn("itemno", Integer.class);
		metaData.addColumn("description", String.class);
		metaData.addColumn("caltype", String.class);
		metaData.addColumn("clientref", String.class);
		metaData.addColumn("serialno", String.class);
		metaData.addColumn("expenseitem", String.class);
		metaData.addColumn("jobno", String.class);
		metaData.addColumn("jobitemno", Integer.class);
		metaData.addColumn("plantno", String.class);
		metaData.addColumn("modelname", String.class);
		metaData.addColumn("purchasecost", Double.class);		
		metaData.addColumn("customerdescription", String.class);
		metaData.addColumn("adjustmentcost", Double.class);
		metaData.addColumn("repaircost", Double.class);
		metaData.addColumn("calibrationcost", Double.class);
		metaData.addColumn("servicecost", Double.class);
		metaData.addColumn("discountAmount", Double.class);
		metaData.addColumn("discountPercent", Double.class);

		metaData.addColumn("totalCost", String.class);
		metaData.addColumn("finalCost", String.class);
		metaData.addColumn("qty", String.class);
		metaData.addColumn("deliveryheader", String.class);
		return true;
	}
	
	/*
	 * Returns the most recent delivery item (where the delivery is a CLIENT_DELIVERY) 
	 * associated with the invoice item, or null if no such item exists 
	 * 
	 * TODO - replace with projection of delivery item / delivery data initialized for all invoice items
	 */
	private JobDeliveryItem getJobDeliveryItem(InvoiceItem invoiceItem) {
		JobDeliveryItem result = null;
		if (invoiceItem.getJobItem() != null) {
			List<JobDeliveryItem> clientDeliveryItems = 
					invoiceItem.getJobItem().getDeliveryItems().stream().
						filter(di -> di.getDelivery().getType().equals(DeliveryType.CLIENT)).
						collect(Collectors.toList());
			if (!clientDeliveryItems.isEmpty())
				result = clientDeliveryItems.get(clientDeliveryItems.size() - 1);
		}
		return result;
	}
	
	/*
	 * Formats a delivery header, also accommodating absence of delivery
	 * (more type safe in Java vs BIRT scripting)
	 */
	private String formatDeliveryHeader(JobDeliveryItem jobDeliveryItem) {
		StringBuffer result = new StringBuffer();
		if (jobDeliveryItem != null) {
			String deliveryNumber = jobDeliveryItem.getDelivery().getDeliveryno();
			String formattedDeliveryDate = "";
			if (jobDeliveryItem.getDelivery().getDeliverydate() != null) {
				Date deliveryDate = DateTools.dateFromLocalDate(jobDeliveryItem.getDelivery().getDeliverydate());
				formattedDeliveryDate = deliveryDateFormat.format(deliveryDate); 
			}
				
			String[] args = new String[] {deliveryNumber, formattedDeliveryDate};
			result.append(messageSource.getMessage(CODE_WITH_DELIVERY_NOTE, args, MESSAGE_WITH_DELIVERY_NOTE, locale));
		}
		else {
			result.append(messageSource.getMessage(CODE_NO_DELIVERY_NOTE, null, MESSAGE_NO_DELIVERY_NOTE, locale));
		}
		return result.toString();
	}

	/*
	 * Used for SPLIT costs which are now obsolete on new invoices
	 * Returns total cost value if cost is active. (Supports null Cost values).
	 * (note - total = before discount, final = after discount)
	 */
	private BigDecimal getActiveSplitTotalCost(Cost cost) {
		if (cost != null && cost.isActive()) {
			return cost.getTotalCost();
		}
		else {
			return new BigDecimal("0.00");
		}
	}
	
	/*
	 * Used for SINGLE costs which are only supported on new invoices
	 * Returns cost value based on whether cost type set on nominal matches input; 
	 * if no nominal set or no cost type on nominal, we treat as service cost
	 */
	private BigDecimal getTotalCost(InvoiceItem invitem, CostType costType) {
		BigDecimal result = null;
		CostType itemCostType = null;
		if (invitem.getNominal() != null)
			itemCostType = invitem.getNominal().getCostType();
		if (Objects.equals(itemCostType, costType)) {
			result = invitem.getTotalCost();
		}
		return result;
	}

	private BigDecimal getTotalDiscount(InvoiceItem invoiceitem){
		BigDecimal disc = new BigDecimal("0.00");
		if (invoiceitem.isBreakUpCosts()) {
			disc = disc.add(getActiveDiscount(invoiceitem.getAdjustmentCost()));
			disc = disc.add(getActiveDiscount(invoiceitem.getCalibrationCost()));
			disc = disc.add(getActiveDiscount(invoiceitem.getPurchaseCost()));
			disc = disc.add(getActiveDiscount(invoiceitem.getRepairCost()));
			disc = disc.add(getActiveDiscount(invoiceitem.getServiceCost()));
		}
		else {
			disc = disc.add(invoiceitem.getGeneralDiscountValue());
		}
		return disc;
	}
	private BigDecimal getActiveDiscount(Cost cost){

		if(cost != null && cost.isActive()){
			return cost.getDiscountValue();	
		}
		else{
			return new BigDecimal("0.00");
		}
	}
}
