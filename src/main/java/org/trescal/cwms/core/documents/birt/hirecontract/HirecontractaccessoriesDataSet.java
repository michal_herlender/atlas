package org.trescal.cwms.core.documents.birt.hirecontract;

import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.db.HireItemService;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;

public class HirecontractaccessoriesDataSet extends IterableDataSet<HireItemAccessory> {

	private HireItemService  hireItemService; 

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		hireItemService = super.getApplicationContext().getBean(HireItemService.class);
	}

	@Override
	public Iterator<HireItemAccessory> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer hireitemid = (Integer) getInputParameter(dataSet, "hireitemid");
		HireItem hireItem = hireItemService.findHireItem(hireitemid);
		return hireItem.getAccessories().iterator();
	}

	@Override
	public void setColumnValues(HireItemAccessory accehire, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("accessories", accehire.getHireAccessory().getItem());		
	}

}
