package org.trescal.cwms.core.documents.birt.quotation;



import java.math.BigDecimal;
import java.util.Date;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.QuotationDocText;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.db.QuotationDocCustomTextService;
import org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.db.QuotationDocDefaultTextService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationConditions;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.DateTools;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QuotationDataSet extends EntityDataSet<Quotation> {
	private QuotationService quotationService;
	private QuotationDocCustomTextService customTextService;
	private QuotationDocDefaultTextService defaultTextService;
	private Locale locale;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		quotationService = super.getApplicationContext().getBean(QuotationService.class);
		customTextService = super.getApplicationContext().getBean(QuotationDocCustomTextService.class);
		defaultTextService = super.getApplicationContext().getBean(QuotationDocDefaultTextService.class);
		locale = super.getDocumentLocale();
	}
	@Override
	public Quotation getEntity(Integer entityId) {
		return quotationService.get(entityId);
	}

	@Override
	public void setColumnValues(Quotation qo, IUpdatableDataSetRow row) throws ScriptException {
		
		SupportedCurrency currency = qo.getCurrency();
		
		row.setColumnValue("contact", qo.getContact().getName());
		row.setColumnValue("emailcontact", qo.getContact().getEmail());
		row.setColumnValue("telephonecontact",qo.getContact().getTelephone());
		row.setColumnValue("mobilecontact",qo.getContact().getMobile());
		row.setColumnValue("faxcontact",qo.getContact().getFax());

		row.setColumnValue("contactbusiness", qo.getSourcedBy().getName());
		row.setColumnValue("emailbusiness", qo.getSourcedBy().getEmail());
		row.setColumnValue("telephonebusiness",qo.getSourcedBy().getTelephone());
		row.setColumnValue("mobilebusiness",qo.getSourcedBy().getMobile());
		row.setColumnValue("faxbusiness",qo.getSourcedBy().getFax());

		row.setColumnValue("positionbusiness",qo.getSourcedBy().getPosition());
		row.setColumnValue("companybusiness",qo.getSourcedBy().getSub().getComp().getConame());
		
		row.setColumnValue("reference",qo.getQno());
		row.setColumnValue("jobversion",qo.getVer());
		row.setColumnValue("yourref",qo.getClientref());
		row.setColumnValue("requestdate",DateTools.dateFromLocalDate(qo.getReqdate()));
		row.setColumnValue("issuedate",DateTools.dateFromLocalDate(qo.getIssuedate()));
		row.setColumnValue("quotationvalidity",qo.getDuration());
		row.setColumnValue("expirationdate",DateTools.dateFromLocalDate(qo.getExpiryDate()));
		
		String formattedTotal = CurrencyValueFormatter.format(qo.getTotalCost(), currency, locale);
		if (log.isDebugEnabled())
			log.debug("formattedTotal : "+formattedTotal);
		row.setColumnValue("total", formattedTotal);
		row.setColumnValue("vatRate",qo.getVatRate());
	    row.setColumnValue("vatValue", CurrencyValueFormatter.format(qo.getVatValue(), currency, locale));
		row.setColumnValue("finalCost", CurrencyValueFormatter.format(qo.getFinalCost(), currency, locale));

		QuotationConditions quotationConditions = quotationService.findQuotationConditions(qo);
		if (quotationConditions.getGeneralCon() != null){
			String terms = quotationConditions.getGeneralCon().getConditionText();
			row.setColumnValue("termsandconditions", terms);
		}
		else {
			row.setColumnValue("termsandconditions", "");
		}
		// We show item discount information on quotation only if at least one item is discounted
		row.setColumnValue("discountCount", countDiscounts(qo));
		row.setColumnValue("discountTotal", CurrencyValueFormatter.format(sumDiscounts(qo), currency, locale));
		row.setColumnValue("showDiscounts", qo.isShowDiscounts());
		
		QuotationDocText text = getQuotationDocText(qo);;
		row.setColumnValue("subjectText", text != null ? text.getSubject() : "");
		row.setColumnValue("bodyText", text != null ? text.getBody() : "");
		row.setColumnValue("showVAT", !quotationService.getAvalaraAware());
	}
	
	private int countDiscounts(Quotation qo) {
		int result = 0;
		for (Quotationitem qitem : qo.getQuotationitems()) {
			if (qitem.getUnitDiscountValue().compareTo(BigDecimal.ZERO) > 0) {
				result++;
			}
		}
		return result;
	}
	
	private BigDecimal sumDiscounts(Quotation qo) {
		BigDecimal result = new BigDecimal("0.00");
		for (Quotationitem qitem : qo.getQuotationitems()) {
			BigDecimal itemQuantity = new BigDecimal(qitem.getQuantity());
			BigDecimal itemExtendedDiscount = qitem.getUnitDiscountValue().multiply(itemQuantity);
			result = result.add(itemExtendedDiscount);
		}
		return result;
	}
	
	private QuotationDocText getQuotationDocText(Quotation quotation) {
		QuotationDocText result = null;
		// First see if there is a custom text stored for this quotation
		result = this.customTextService.get(quotation.getId());
		// Second obtain a default text
		if (result == null) {
			Locale documentLocale =  getDocumentLocale();
			result = this.defaultTextService.findDefaultText(documentLocale);
		}
		// If not available (new language?), use the default text for the primary system locale
		if (result == null) {
			Locale primaryLocale = getPrimaryLocale();
			result = this.defaultTextService.findDefaultText(primaryLocale);
		}
		return result;
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("contact", String.class);
		metaData.addColumn("emailcontact", String.class);
		metaData.addColumn("telephonecontact", String.class);
		metaData.addColumn("mobilecontact", String.class);
		metaData.addColumn("faxcontact", String.class);
		
		metaData.addColumn("jobversion", Integer.class);
		metaData.addColumn("yourref", String.class);
		metaData.addColumn("requestdate", Date.class);
		metaData.addColumn("issuedate", Date.class);
		metaData.addColumn("quotationvalidity", Integer.class);
		
		metaData.addColumn("contactbusiness", String.class);
		metaData.addColumn("emailbusiness", String.class);
		metaData.addColumn("telephonebusiness", String.class);
		metaData.addColumn("mobilebusiness", String.class);
		metaData.addColumn("faxbusiness", String.class);

		metaData.addColumn("reference", String.class);
		metaData.addColumn("total", String.class);
		metaData.addColumn("positionbusiness", String.class);
		metaData.addColumn("termsandconditions", String.class);
		metaData.addColumn("companybusiness", String.class);

		metaData.addColumn("discountCount", Integer.class);
		metaData.addColumn("discountTotal", String.class);
		metaData.addColumn("expirationdate", Date.class);
		metaData.addColumn("showDiscounts", Boolean.class);
		metaData.addColumn("vatRate", BigDecimal.class);
		
		metaData.addColumn("vatValue", String.class);
		metaData.addColumn("finalCost", String.class);
		metaData.addColumn("subjectText", String.class);
		metaData.addColumn("bodyText", String.class);
		
		metaData.addColumn("showVAT", Boolean.class);

		return true;
	}
	
}
