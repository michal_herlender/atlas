package org.trescal.cwms.core.documents.images.image;

public enum ImageType
{
	INSTRUMENTMODEL, JOBITEM, GENERAL, LABEL;
}