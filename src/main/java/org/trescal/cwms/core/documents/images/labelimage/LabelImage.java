package org.trescal.cwms.core.documents.images.labelimage;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.trescal.cwms.core.documents.images.image.Image;

@Entity
@DiscriminatorValue("labelimage")
public class LabelImage extends Image {
}
