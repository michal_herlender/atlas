	package org.trescal.cwms.core.documents.birt.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.documents.birt.costcentreporequest.CostCentrePORequestDocumentDetails;
import org.trescal.cwms.core.documents.birt.creditnote.CreditNoteDocumentDetails;
import org.trescal.cwms.core.documents.birt.delivery.ClientDeliveryDocumentDetails;
import org.trescal.cwms.core.documents.birt.delivery.GeneralDeliveryDocumentDetails;
import org.trescal.cwms.core.documents.birt.delivery.InternalDeliveryDocumentDetails;
import org.trescal.cwms.core.documents.birt.delivery.ReturnDeliveryDocumentDetails;
import org.trescal.cwms.core.documents.birt.delivery.SupplierDeliveryDocumentDetails;
import org.trescal.cwms.core.documents.birt.delivery.ThirdPartyDeliveryDocumentDetails;
import org.trescal.cwms.core.documents.birt.faultreport.FaultReportDocumentDetails;
import org.trescal.cwms.core.documents.birt.hirecontract.HirecontractDocumentDetails;
import org.trescal.cwms.core.documents.birt.hirecontract.OffHireDocumentDetails;
import org.trescal.cwms.core.documents.birt.invoices.InvoiceDocumentDetails;
import org.trescal.cwms.core.documents.birt.jobcosting.JobCostingDocumentDetails;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetDocumentDetails;
import org.trescal.cwms.core.documents.birt.jobsheet.JobSheetDocumentDetails;
import org.trescal.cwms.core.documents.birt.purchaseorder.PurchaseOrderDocumentDetails;
import org.trescal.cwms.core.documents.birt.quotation.QuotationDocumentDetails;
import org.trescal.cwms.core.documents.birt.repaircompletionreport.RepairCompletionReportDocumentDetails;
import org.trescal.cwms.core.documents.birt.repaircompletionreport.RepairInspectionReportDocumentDetails;
import org.trescal.cwms.core.documents.birt.tpquotation.TPQuotationDocumentDetails;
import org.trescal.cwms.core.documents.birt.tpquoterequest.TPQuoteReqDocumentDetails;

@Component
public class BaseDocumentResolver implements InitializingBean {
	// Note, these are organized alphabetically - please maintain the order
	@Autowired
	private ClientDeliveryDocumentDetails clientDeliveryDocumentDetails;
	@Autowired
	private SupplierDeliveryDocumentDetails supplierDeliveryDocumentDetails;
	@Autowired
	private ReturnDeliveryDocumentDetails returnDeliveryDocumentDetails;
	@Autowired
	private InternalDeliveryDocumentDetails  internalDeliveryDocumentDetails;
	@Autowired
	private CostCentrePORequestDocumentDetails  costCentrePORequestDocumentDetails;
	@Autowired
	private FaultReportDocumentDetails faultReportDocumentDetails;
	@Autowired
	private RepairCompletionReportDocumentDetails repairCompletionReportDocumentDetails;
	@Autowired
	private RepairInspectionReportDocumentDetails repairInspectionReportDocumentDetails;
	@Autowired
	private GeneralDeliveryDocumentDetails generalDeliveryDocumentDetails;
	@Autowired
	private HirecontractDocumentDetails hirecontractDocumentDetails;
	@Autowired
	private InvoiceDocumentDetails  invoiceDocumentDetails;
	@Autowired
	private OffHireDocumentDetails offHireDocumentDetails;
	@Autowired
	private JobItemSheetDocumentDetails jobitemsheetDocumentDetails;
	@Autowired
	private JobSheetDocumentDetails jobsheetDocumentDetails;
	@Autowired
	private PurchaseOrderDocumentDetails purchaseOrderDocumentDetails;
	@Autowired
	private QuotationDocumentDetails quotationDocumentDetails;
	@Autowired
	private ThirdPartyDeliveryDocumentDetails  tpDeliveryDocumentDetails;
	@Autowired
	private TPQuoteReqDocumentDetails tpQuoteReqDocumentDetails;
	@Autowired
	private JobCostingDocumentDetails  jobCostingDocumentDetails;
	@Autowired
	private CreditNoteDocumentDetails  creditNoteDocumentDetails;
	@Autowired
	private TPQuotationDocumentDetails tpQuotationDocumentDetails;
	
	// Initialized after bean wiring
	private Map<BaseDocumentType, EntityDocumentDetails> detailsMap;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		detailsMap = new HashMap<>();
		detailsMap.put(BaseDocumentType.GENERAL_DELIVERY, generalDeliveryDocumentDetails);
		detailsMap.put(BaseDocumentType.CLIENT_DELIVERY, clientDeliveryDocumentDetails);
		detailsMap.put(BaseDocumentType.INTERNAL_DELIVERY, internalDeliveryDocumentDetails);
		detailsMap.put(BaseDocumentType.FAULT_REPORT, faultReportDocumentDetails);
		detailsMap.put(BaseDocumentType.THIRD_PARTY_DELIVERY, tpDeliveryDocumentDetails);
		detailsMap.put(BaseDocumentType.PURCHASE_ORDER, purchaseOrderDocumentDetails);
		detailsMap.put(BaseDocumentType.TP_QUOTE_REQUEST, tpQuoteReqDocumentDetails);
		detailsMap.put(BaseDocumentType.TP_QUOTATION, tpQuotationDocumentDetails);
		detailsMap.put(BaseDocumentType.QUOTATION, quotationDocumentDetails);
		detailsMap.put(BaseDocumentType.COST_CENTRE_PO_REQUEST, costCentrePORequestDocumentDetails);
		detailsMap.put(BaseDocumentType.HIRE_CONTRACT, hirecontractDocumentDetails);
		detailsMap.put(BaseDocumentType.OFF_HIRE, offHireDocumentDetails);
		detailsMap.put(BaseDocumentType.INVOICE_CALIBRATION, invoiceDocumentDetails);
		detailsMap.put(BaseDocumentType.INVOICE_US_LETTER, invoiceDocumentDetails);
		detailsMap.put(BaseDocumentType.JOB_COSTING, jobCostingDocumentDetails);
		detailsMap.put(BaseDocumentType.CREDIT_NOTE, creditNoteDocumentDetails);
		detailsMap.put(BaseDocumentType.JOB_ITEM_SHEET, jobitemsheetDocumentDetails);
		detailsMap.put(BaseDocumentType.JOB_SHEET, jobsheetDocumentDetails);
		detailsMap.put(BaseDocumentType.REPAIR_EVALUATION_REPORT, repairInspectionReportDocumentDetails);
		detailsMap.put(BaseDocumentType.REPAIR_COMPLETION_REPORT, repairCompletionReportDocumentDetails);
		detailsMap.put(BaseDocumentType.FAILURE_REPORT, faultReportDocumentDetails);	// Only used to check existence
		detailsMap.put(BaseDocumentType.FAILURE_REPORT_2014, faultReportDocumentDetails);	// Only used to check existence
		detailsMap.put(BaseDocumentType.DELIVERY_PROJECTION, clientDeliveryDocumentDetails);	// To start, just client delivery
		detailsMap.put(BaseDocumentType.SUPPLIER_DELIVERY_PROJECTION, supplierDeliveryDocumentDetails);	// To start, just supplier delivery
		detailsMap.put(BaseDocumentType.RETURN_DELIVERY_PROJECTION, returnDeliveryDocumentDetails);	
	}

	public EntityDocumentDetails getComponent(BaseDocumentType documentType) {
		if (documentType == null) throw new IllegalArgumentException("No documentType specified");
		EntityDocumentDetails result = detailsMap.get(documentType);
		if (result == null ) throw new UnsupportedOperationException("No handler registered for type "+documentType);
		return result;
	}

	public Set<BaseDocumentType> getSupportedTypes() {
		return detailsMap.keySet();
	}
}
