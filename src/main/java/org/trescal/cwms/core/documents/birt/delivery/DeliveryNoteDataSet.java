package org.trescal.cwms.core.documents.birt.delivery;

import java.util.Collection;

import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.documents.birt.common.EntityNoteDataSet;

public class DeliveryNoteDataSet extends EntityNoteDataSet<DeliveryNote> {
	@Override
	public Collection<DeliveryNote> getNotes(Integer entityId) {
		DeliveryService deliveryService = getApplicationContext().getBean(DeliveryService.class);
		Delivery delivery = deliveryService.get(entityId); 
		return delivery.getNotes();
	}
}

