package org.trescal.cwms.core.documents.images.taggedimage;

import java.util.Comparator;

public class TaggedImageComparator implements Comparator<TaggedImage>
{
	@Override
	public int compare(TaggedImage o1, TaggedImage o2)
	{
		return o1.getTag().compareTo(o2.getTag());
	}
}
