package org.trescal.cwms.core.documents.birt.certificate;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;

public class CertificateDataSet extends EntityDataSet<Certificate> {
	// Initialized in beforeOpen()
	private CertificateService certificateService;
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		certificateService = super.getApplicationContext().getBean(CertificateService.class);
	}

	@Override
	public Certificate getEntity(Integer entityId) {
		return certificateService.findCertificate(entityId);
	}

	@Override
	public void setColumnValues(Certificate certificate, IUpdatableDataSetRow row) throws ScriptException {
		row.setColumnValue("caldate", certificate.getCalDate());
		row.setColumnValue("certno", certificate.getCertno());
		row.setColumnValue("datevalidated", certificate.getValidation().getDatevalidated());
		row.setColumnValue("responsible", certificate.getValidation().getValidatedBy() != null
				? certificate.getValidation().getValidatedBy().getName()
				: certificate.getValidation().getResponsible());
		row.setColumnValue("status", certificate.getValidation().getStatus().getName());
		row.setColumnValue("note", certificate.getValidation().getNote());
	}

}
