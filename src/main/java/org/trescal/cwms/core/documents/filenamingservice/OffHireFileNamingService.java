package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.tools.FileTools;

public class OffHireFileNamingService implements FileNamingService
{
	private String fileName = "";

	private String hireno = "";
	protected final Log logger = LogFactory.getLog(this.getClass());
	private String rootDirectory;

	public OffHireFileNamingService(Hire hire)
	{
		this.fileName = "";
		this.rootDirectory = hire.getDirectory().toString();
		this.hireno = hire.getHireno();
	}

	public String getFileName()
	{
		File rootFolder = new File(this.rootDirectory);
		if (!rootFolder.exists())
		{
			this.logger.error("Directory does not exist: '"
					+ this.rootDirectory + "'");
			rootFolder.mkdirs();
		}

		this.hireno = this.hireno.replace('/', '.');

		// create the basic file name 'Off Hire - hireno - date - rev'
		String basicFileName = "Off Hire - ".concat(this.hireno).concat(" - Doc ");

		// now create the full filename (without any file extension)
		this.fileName = this.rootDirectory.concat(File.separator).concat(this.getNextOffHireFileName(basicFileName, this.rootDirectory));

		return this.fileName;
	}

	/**
	 * From the given basic naming structure and Hire directory this function
	 * gets a list of any existing Off Hire files for this hire and works out
	 * what the next revision number for the current document should be.
	 * 
	 * @param basicFileName the basic file naming structure for this Hire
	 * @param rootDirectory the path to the Hire version directory for this hire
	 * @return
	 */
	private String getNextOffHireFileName(String basicFileName, String rootDirectory)
	{
		String fileName = "";
		File rootDir = new File(rootDirectory);
		File archiveDir = new File(rootDirectory.concat("//Off Hire Archive"));

		String[] offhireFiles = rootDir.list();
		if (offhireFiles != null)
		{
			// build an arraylist of files whoose name matches documents
			ArrayList<String> thisOffHireFiles = new ArrayList<String>();
			for (String ohFile : offhireFiles)
			{
				if (ohFile.contains(basicFileName))
				{
					thisOffHireFiles.add(ohFile);
				}
			}

			if (thisOffHireFiles.size() > 0)
			{
				// check to see if archive directory exists?
				if (!archiveDir.exists())
				{
					// create directory
					archiveDir.mkdir();
				}
				int rev = 0;
				// now perform a second pass to decide on the next sequence
				// number
				for (String ohFile : thisOffHireFiles)
				{
					if (ohFile.lastIndexOf('.') > -1)
					{
						// strip the doc extension
						String document = ohFile.substring(0, ohFile.length()
								- (ohFile.length() - ohFile.lastIndexOf('.')));

						// strip the basic document name from the document to
						// get the document rev number
						String revNo = document.replaceAll(basicFileName, "");

						if (revNo.matches("\\d*") && !revNo.trim().equals(""))
						{
							if (Integer.parseInt(revNo) > rev)
							{
								rev = Integer.parseInt(revNo);
							}
						}
					}
					// move existing off hire file to archive
					File rootOffHireFile = new File(rootDirectory.concat("//"
							+ ohFile));
					File archiveOffHireFile = new File(archiveDir.getAbsolutePath().concat("//"
							+ ohFile));
					// copy file to archive
					FileTools.copyFile(rootOffHireFile, archiveOffHireFile);
					// delete original root file
					rootOffHireFile.delete();
				}
				rev = rev + 1;
				fileName = basicFileName.concat(String.valueOf(rev));
			}
			else
			{
				fileName = basicFileName.concat(String.valueOf(1));
			}
		}
		else
		{
			fileName = basicFileName.concat(String.valueOf(1));
		}
		return fileName;
	}
}
