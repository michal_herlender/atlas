package org.trescal.cwms.core.documents.birt.quotation;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;

public class QuotationItemDataSet extends IterableDataSet<Quotationitem>{
	private QuoteHeadingService quoteHeadingService;
	private TranslationService translationService;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		quoteHeadingService = super.getApplicationContext().getBean(QuoteHeadingService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
	}
	
	@Override
	public Iterator<Quotationitem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer headingid = super.getInputParameter(dataSet, "headingid");
		QuoteHeading quoteHeading = quoteHeadingService.findQuoteHeading(headingid);
		return quoteHeading.getQuoteitems().iterator();
	}

	@Override
	public void setColumnValues(Quotationitem qitem, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		Locale locale = super.getDocumentLocale();
		Locale primaryLocale = super.getPrimaryLocale();
		
		row.setColumnValue("qoitemid", qitem.getId());	
		row.setColumnValue("count", qitem.getItemno());

		String customerDescription = "";
		String description = "";
		
		if (qitem.getInst() != null){
			description = InstModelTools.instrumentModelNameViaTranslations(qitem.getInst(), locale, primaryLocale);
			customerDescription = qitem.getInst().getCustomerDescription();
		} else if (qitem.getModel() != null) {
			description = InstModelTools.modelNameViaTranslations(qitem.getModel(), locale, primaryLocale);
		}
		row.setColumnValue("description", description);
		row.setColumnValue("customerDescription", customerDescription);

		if (qitem.getServiceType() != null){
			Set<Translation> translationShortName = qitem.getServiceType().getShortnameTranslation();
			String serviceType = translationService.getCorrectTranslation(translationShortName, locale);
			Set<Translation> translationLongName = qitem.getServiceType().getLongnameTranslation();
			String serviceTypeDescription = translationService.getCorrectTranslation(translationLongName, locale);
			
			row.setColumnValue("serviceType", serviceType);
			row.setColumnValue("serviceTypeDescription", serviceTypeDescription);
			row.setColumnValue("serviceTypeId", qitem.getServiceType().getServiceTypeId());
			row.setColumnValue("serviceTypeOrder", qitem.getServiceType().getOrder());
		}
		else{
			row.setColumnValue("serviceType", "");
			row.setColumnValue("serviceTypeDescription", "");
			row.setColumnValue("serviceTypeId", 0);
			row.setColumnValue("serviceTypeOrder", 0);
		}
		row.setColumnValue("referenceno" ,qitem.getReferenceNo());
		if (qitem.getInst() != null)
			row.setColumnValue("trescalId" ,qitem.getInst().getPlantid());
		
		row.setColumnValue("listPrice" ,getListPrice(qitem));
		row.setColumnValue("unitDiscount", qitem.getUnitDiscountValue());
		row.setColumnValue("unitPrice" ,qitem.getUnitCost());
		row.setColumnValue("qty" ,qitem.getQuantity());
		row.setColumnValue("extendedPrice" ,qitem.getFinalCost());
	}
	
	/**
	 * Gets the list price of quotation item, from any attached costs
	 */
	private BigDecimal getListPrice(Quotationitem qitem) {
		BigDecimal result = new BigDecimal("0.00");
		if (qitem.getCalibrationCost() != null)
			result = result.add(qitem.getCalibrationCost().getTotalCost());
		if (qitem.getPurchaseCost() != null) 
			result = result.add(qitem.getPurchaseCost().getTotalCost());
		return result;
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("qoitemid", Integer.class);
		metaData.addColumn("count", Integer.class);
		metaData.addColumn("customerDescription", String.class);
		metaData.addColumn("description", String.class);		
		metaData.addColumn("referenceno", String.class);
		
		metaData.addColumn("listPrice", String.class);
		metaData.addColumn("unitDiscount", BigDecimal.class);
		metaData.addColumn("unitPrice", BigDecimal.class);
		metaData.addColumn("qty", Integer.class);
		metaData.addColumn("extendedPrice", BigDecimal.class);

		metaData.addColumn("serviceType", String.class);
		metaData.addColumn("serviceTypeId", Integer.class);
		metaData.addColumn("serviceTypeDescription", String.class);
		metaData.addColumn("serviceTypeOrder", Integer.class);
		metaData.addColumn("trescalId", Integer.class);
		
		return true;
	}
}

