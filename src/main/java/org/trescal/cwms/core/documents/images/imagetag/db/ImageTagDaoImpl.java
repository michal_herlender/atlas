package org.trescal.cwms.core.documents.images.imagetag.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.documents.images.imagetag.ImageTag;

@Repository("ImageTagDao")
public class ImageTagDaoImpl extends BaseDaoImpl<ImageTag, Integer> implements ImageTagDao {
	
	@Override
	protected Class<ImageTag> getEntity() {
		return ImageTag.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ImageTag> findAll() {
		Criteria criteria = this.getSession().createCriteria(ImageTag.class);
		criteria.addOrder(Order.asc("tag"));
		return criteria.list();
	}
}