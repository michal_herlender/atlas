package org.trescal.cwms.core.documents.birt.faultreport;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;

public class FaultReportDataSet extends EntityDataSet<FaultReport> {

    private FaultReportService faultReportService;
    private InstrumService instrumentService;
    private MessageSource messageSource;

    @Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
        faultReportService = super.getApplicationContext().getBean(FaultReportService.class);
        instrumentService = super.getApplicationContext().getBean(InstrumService.class);
        messageSource = super.getApplicationContext().getBean(MessageSource.class);
    }

    @Override
    public FaultReport getEntity(Integer entityId) {
        return faultReportService.findFaultReport(entityId);
    }

    @Override
    public void setColumnValues(FaultReport faultReport, IUpdatableDataSetRow row) throws ScriptException {

        Locale locale = LocaleContextHolder.getLocale();
        Instrument instrument = faultReport.getJobItem().getInst();
        // get instrument details
        row.setColumnValue("instrumentname", instrumentService.getCorrectInstrumentModelName(instrument));
        row.setColumnValue("serialno", instrument.getSerialno());
        row.setColumnValue("plantno", instrument.getPlantno());
        row.setColumnValue("plantid", instrument.getPlantid());	// Can't use barcode - reserved for image
        row.setColumnValue("lastcaldate", instrumentService.getLastCalDate(instrument));
        row.setColumnValue("calduedate", instrument.getNextCalDueDate());
        row.setColumnValue("calfrequency", instrument.getCalFrequency() +
                " " + instrument.getCalFrequencyUnit().getName(instrument.getCalFrequency()));
        row.setColumnValue("subdiv", instrument.getCon().getSub().getSubname());
        row.setColumnValue("owner", instrument.getCon().getName());

        //get fault details
        row.setColumnValue("reportno", faultReport.getFaultReportNumber());

        //get estimated times/costs
        if (faultReport.getLeadTime() != null) {
            String leadTimeUnit = messageSource.getMessage("days", null, locale);
            row.setColumnValue("leadtime", faultReport.getLeadTime().toString() + " " +  leadTimeUnit);
        }
        else {
        	row.setColumnValue("leadtime", "");
        }
        
        row.setColumnValue("repairtime", niceTime(faultReport.getEstRepairTime(),locale));
        String currencySymbol = faultReport.getValueInBusinessCompanyCurrency() != null && faultReport.getValueInBusinessCompanyCurrency().floatValue() > 0f ? faultReport.getBusinessCompanyCurrency().getCurrencySymbol() : " ";
        row.setColumnValue("cost", faultReport.getCost() + " " + currencySymbol);
        row.setColumnValue("adjustmenttime", niceTime(faultReport.getEstAdjustTime(),locale));
    }

    private String niceTime(Integer minutes, Locale locale) {

        String niceTime = "";
        Long hours = TimeUnit.MINUTES.toHours(minutes);
        Long remainingMinutes = minutes - TimeUnit.HOURS.toMinutes(hours);
        if (hours == 1) {
            niceTime = String.join(" ", niceTime, String.valueOf(hours), messageSource.getMessage("hour", null, locale), " ");
        } else if (hours > 1) {
            niceTime = String.join(" ", niceTime, String.valueOf(hours), messageSource.getMessage("hours", null, locale), " ");
        }
        if (remainingMinutes == 1) {
            niceTime = String.join(" ", niceTime, String.valueOf(remainingMinutes), messageSource.getMessage("minute", null, locale));
        } else if (remainingMinutes > 1) {
            niceTime = String.join(" ", niceTime, String.valueOf(remainingMinutes), messageSource.getMessage("minutes", null, locale));
        }
        return niceTime;
    }
}
