package org.trescal.cwms.core.documents.birt.invoices;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class InvoicePODataSet extends IterableDataSet<String> {

	private Set<String> setPoNumbers;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		InvoiceService invoiceService = super.getApplicationContext().getBean(InvoiceService.class);
		Integer invoiceid = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		Invoice invoice = invoiceService.findInvoice(invoiceid);
		setPoNumbers = new TreeSet<String>(); 
		// display the internal PO rather the client PO for internal invoice case
		if (invoice.getType().getName().equals("Internal")){
			for(PurchaseOrderItem invoiceItemPo : invoice.getInternalPOItems()){
					if(invoiceItemPo.getOrder() != null){
						setPoNumbers.add(invoiceItemPo.getOrder().getPono());
					}
			}
		} else {
			for(InvoiceItem invoiceitem : invoice.getItems()) {
				for (InvoicePOItem invoicePOItem : invoiceitem.getInvPOsItem()) {
					setPoNumbers.add(invoicePOItem.getInvPO().getPoNumber());
				}
				for (InvoiceItemPO invoiceItemPo : invoiceitem.getInvItemPOs()) {
					if (invoiceItemPo.getJobPO() != null)
						setPoNumbers.add(invoiceItemPo.getJobPO().getPoNumber());
					else if (invoiceItemPo.getBpo() != null) {
						setPoNumbers.add(invoiceItemPo.getBpo().getPoNumber());
					}
				}
			}
		}
		
	}

	@Override
	public Iterator<String> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return setPoNumbers.iterator();
	}

	@Override
	public void setColumnValues(String poNumber, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("referenceno",poNumber);		
	}

}
