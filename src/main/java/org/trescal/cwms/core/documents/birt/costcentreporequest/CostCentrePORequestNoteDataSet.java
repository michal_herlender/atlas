package org.trescal.cwms.core.documents.birt.costcentreporequest;

import java.util.Collection;

import org.trescal.cwms.core.documents.birt.common.EntityNoteDataSet;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingnote.JobCostingNote;

public class CostCentrePORequestNoteDataSet extends EntityNoteDataSet<JobCostingNote>{

	@Override
	public Collection<JobCostingNote> getNotes(Integer entityId) {
		JobCostingService jobCostingService = getApplicationContext().getBean(JobCostingService.class);
		JobCosting jobCosting = jobCostingService.get(entityId); 
		return jobCosting.getNotes();
	}
}
