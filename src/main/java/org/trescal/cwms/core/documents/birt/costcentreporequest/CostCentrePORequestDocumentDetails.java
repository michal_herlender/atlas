package org.trescal.cwms.core.documents.birt.costcentreporequest;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;

@Component
public class CostCentrePORequestDocumentDetails  implements EntityDocumentDetails {
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private JobCostingService jobCostingService;
	
	private static final String CODE_TITLE = "jobCosting";
	private static final String DEFAULT_MESSAGE_TITLE = "Cost Centre PO Request";
	
	@Override
	public Address getSourceAddress(Integer entityId) {
		JobCosting 	jobCosting = jobCostingService.get(entityId);
		return jobCosting.getCreatedBy().getDefAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		JobCosting 	jobCosting = jobCostingService.get(entityId);
		return jobCosting.getJob().getReturnTo();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		JobCosting 	jobCosting = jobCostingService.get(entityId);
		return jobCosting.getOrganisation();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		JobCosting 	jobCosting = jobCostingService.get(entityId);
		// We return the job number, so that the version number does not clutter up the barcode
		return jobCosting.getJob().getJobno();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		 return messageSource.getMessage(CODE_TITLE,null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}

	@Override
	public boolean exists(Integer entityId) {
		JobCosting 	jobCosting = jobCostingService.get(entityId);
		return (jobCosting != null);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}
}
