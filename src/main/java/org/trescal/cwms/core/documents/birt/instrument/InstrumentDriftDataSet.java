package org.trescal.cwms.core.documents.birt.instrument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;

public class InstrumentDriftDataSet extends IterableDataSet<Certificate> {

    // Initialized in beforeOpen()
    private Integer plantId;
    private InstrumService instrumentService;
    private CertificateService certificateService;
    //Initialized in getIterator
    private String deviationUnit;

    @Override
    public void init(IDataSetInstance dataSet, IReportContext reportContext) {
        instrumentService = super.getApplicationContext().getBean(InstrumService.class);
        certificateService = super.getApplicationContext().getBean(CertificateService.class);
        plantId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
    }

    @Override
    public Iterator<Certificate> getIterator(IDataSetInstance dataSet) throws ScriptException {
        Instrument instrument = this.instrumentService.get(plantId);

        deviationUnit = (instrument.getInstrumentComplementaryField() != null
                && instrument.getInstrumentComplementaryField().getDeliveryStatus() != null )
                ? instrument.getInstrumentComplementaryField().getDeviationUnits().getFormattedSymbol()
                : "";

        List<Certificate> allCerts = certificateService.getCertificatesForInstrument(instrument);
        if(!allCerts.isEmpty()) {
            return allCerts.iterator();
        } else {
            return new ArrayList<Certificate>().iterator();
        }
    }

    @Override
    public void setColumnValues(Certificate certificate, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
        row.setColumnValue("date", certificate.getCalDate());
        row.setColumnValue("deviation", certificate.getDeviation() != null
                ? certificate.getDeviation().floatValue() : 0f);
        row.setColumnValue("deviationunit",deviationUnit);
    }


}
