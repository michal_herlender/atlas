package org.trescal.cwms.core.documents.birt.delivery;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;

import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

/**
 * Original job item data set, which uses entities.
 * Targeted for deletion once ProjectedJobDeliveryItemDataSet is ready/tested
 */
public class DeliveryJobItemDataSet extends IterableDataSet<JobDeliveryItem> {
	private Set<JobDeliveryItem> deliveryItems;
	private TranslationService translationService;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		DeliveryService deliveryService = super.getApplicationContext().getBean(DeliveryService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);
		Integer entityid = (Integer) (reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID));
		JobDelivery delivery = deliveryService.get(entityid, JobDelivery.class);
		deliveryItems = delivery.getItems();
	}
	@Override
	public Iterator<JobDeliveryItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return deliveryItems.iterator();
	}

	@Override
	public void setColumnValues(JobDeliveryItem item, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		row.setColumnValue("item",rowCount);
		Instrument inst = item.getJobitem().getInst();
		Locale documentLocale = super.getDocumentLocale(); 
		Locale primaryLocale = super.getPrimaryLocale();
		String description = InstModelTools.instrumentModelNameViaTranslations(inst, documentLocale, primaryLocale);
		row.setColumnValue("description", description);
		row.setColumnValue("barcode", item.getJobitem().getInst().getPlantid());
		row.setColumnValue("serialno", item.getJobitem().getInst().getSerialno());
		row.setColumnValue("plantno", item.getJobitem().getInst().getPlantno());
		row.setColumnValue("itemid", item.getDelitemid());
		row.setColumnValue("jobno", item.getJobitem().getJob().getJobno());
		row.setColumnValue("itemNo", item.getJobitem().getItemNo());
		row.setColumnValue("accessoryFreeText", item.getAccessoryFreeText());
		row.setColumnValue("clientref", item.getJobitem().getClientRef());

		// TODO consider using work requirement cal grade once linked
		CalibrationType calType = item.getJobitem().getServiceType().getCalibrationType();
		String calTypeLongName = calType == null ? "" : this.translationService.getCorrectTranslation(calType.getServiceType().getLongnameTranslation(), documentLocale);

		row.setColumnValue("calTypeLongName", calTypeLongName);
		row.setColumnValue("tpAdjustment", item.getTpRequirement() != null && item.getTpRequirement().isAdjustment());
		row.setColumnValue("tpCalibration", item.getTpRequirement() != null && item.getTpRequirement().isCalibration());
		row.setColumnValue("tpRepair", item.getTpRequirement() != null && item.getTpRequirement().isRepair());
		row.setColumnValue("tpInvestigation", item.getTpRequirement() != null && item.getTpRequirement().isInvestigation());
	}


}
