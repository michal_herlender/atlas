package org.trescal.cwms.core.documents.images;

public class ImageTagDto
{
	private String image;
	private int imageId;
	private String tag;

	public String getImage()
	{
		return this.image;
	}

	public int getImageId() {
		return imageId;
	}

	public String getTag()
	{
		return this.tag;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public void setTag(String tag)
	{
		this.tag = tag;
	}

	@Override
	public String toString()
	{
		return this.image + " : " + this.tag;
	}
}
