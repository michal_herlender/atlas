package org.trescal.cwms.core.documents.retrieval.creditnote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.documents.retrieval.FileSystemDocumentRetriever;
import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentResult;
import org.trescal.cwms.core.documents.retrieval.RetrieveDocumentService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Service
public class RetrieveCreditNoteDocumentService implements RetrieveDocumentService<CreditNote> {

	@Autowired
	private CreditNoteService creditNoteService;
	@Autowired
	private FileSystemDocumentRetriever fileSystemRetriever;
	
	@Override
	public Class<?> getEntityClass() {
		return CreditNote.class;
	}

	@Override
	public RetrieveDocumentResult getLatestDocumentForEntity(Integer entityId) {
		RetrieveDocumentResult result = null;
		CreditNote creditNote = this.creditNoteService.get(entityId);
		if (creditNote != null) {
			result = this.fileSystemRetriever.getLatestDocumentFromFileSystem(Component.CREDIT_NOTE, creditNote.getCreditNoteNo());
		}
		else {
			result = RetrieveDocumentResult.builder()
					.success(false)
					.errorMessage("Entity not found for specified input")
					.build();
		}
		
		return result;
	}

}
