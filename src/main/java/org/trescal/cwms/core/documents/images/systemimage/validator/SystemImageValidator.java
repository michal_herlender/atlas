package org.trescal.cwms.core.documents.images.systemimage.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.documents.images.systemimage.form.SystemImageForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class SystemImageValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return SystemImageForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		SystemImageForm form = (SystemImageForm) (target);

		if (form.getFile().isEmpty()) {
			errors.rejectValue("file", "adminarea.managebusinesscompanylogo.empty", null, "Add an image");
		}

	}

}
