package org.trescal.cwms.core.documents.birt.creditnote;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;

@Component
public class CreditNoteDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;
	private static final String CODE_TITLE = "docs.creditnote";
	private static final String DEFAULT_MESSAGE_TITLE = "Credit Note";
	
	@Autowired
	private CreditNoteService creditnoteService;

	@Override
	public Address getSourceAddress(Integer entityId) {
		CreditNote creditnote  = creditnoteService.get(entityId);
		return creditnote.getInvoice().getBusinessContact().getDefAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		CreditNote creditnote  = creditnoteService.get(entityId);
		return creditnote.getInvoice().getAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		CreditNote creditnote = creditnoteService.get(entityId);
		return creditnote.getOrganisation();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		CreditNote creditnote  = creditnoteService.get(entityId);
		return creditnote.getCreditNoteNo();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE,null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}

	@Override
	public boolean exists(Integer entityId) {
		CreditNote creditnote  = creditnoteService.get(entityId);
		return (creditnote != null);
	}

	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}


}
