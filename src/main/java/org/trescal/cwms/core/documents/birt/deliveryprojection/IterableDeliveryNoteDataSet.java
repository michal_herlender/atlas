package org.trescal.cwms.core.documents.birt.deliveryprojection;

import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;

public abstract class IterableDeliveryNoteDataSet extends IterableDeliveryDataSet<NoteProjectionDTO> {

	@Override
	public void setColumnValues(NoteProjectionDTO instance, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
		row.setColumnValue("label", instance.getLabel());
		row.setColumnValue("note", instance.getNote());
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("label", String.class);
		metaData.addColumn("note", String.class);
		return true;
	}
}
