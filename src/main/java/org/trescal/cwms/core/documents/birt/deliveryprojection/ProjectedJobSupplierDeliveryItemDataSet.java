package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Date;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.tools.DateTools;

import io.jsonwebtoken.lang.Collections;

/**
 * New data set that uses projections for delivery item data 
 * Intended as replacement for entity based delivery item data sets
 */
public class ProjectedJobSupplierDeliveryItemDataSet extends IterableDeliveryDataSet<DeliveryItemDTO> {

	@Override
	public Iterator<DeliveryItemDTO> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return super.getModel().getDeliveryItemDtos().iterator();
	}

	@Override
	public void setColumnValues(DeliveryItemDTO deliveryItem, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		boolean hideTypology = true;
		InstrumentProjectionDTO instDto = deliveryItem.getJobItem().getInstrument();
		JobItemProjectionDTO jiDto = deliveryItem.getJobItem(); 
		
		Date requestedDate = null;
		if (!Collections.isEmpty(jiDto.getSupplierPos())) {
			requestedDate = DateTools.dateFromLocalDate(jiDto.getSupplierPos().get(0).getReqDate());
		}
		
		row.setColumnValue("deliveryItemNumber", rowCount);
		row.setColumnValue("description", instDto.getInstrumentModelNameViaFields(hideTypology));
		row.setColumnValue("barcode", instDto.getPlantid());
		row.setColumnValue("serialno", instDto.getSerialno());
		row.setColumnValue("plantno", instDto.getPlantno());
		row.setColumnValue("deliveryItemId", deliveryItem.getDeliveryItemId());
		row.setColumnValue("jobNumber", jiDto.getJob().getJobno());
		row.setColumnValue("jobItemNumber", jiDto.getItemno());
		row.setColumnValue("requestedDate", requestedDate);
		row.setColumnValue("accessoryFreeText", deliveryItem.getAccessoryFreeText());
		row.setColumnValue("jobItemClientRef", jiDto.getClientRef());
		row.setColumnValue("jobClientRef", jiDto.getJob().getClientRef());
		
		String serviceTypeShortName = "";
		if (jiDto.getServiceType() != null)
			serviceTypeShortName = jiDto.getServiceType().getShortName();
					
		String serviceTypeLongName = "";
			if (jiDto.getServiceType() != null)
				serviceTypeLongName =  jiDto.getServiceType().getLongName();
				
		row.setColumnValue("serviceTypeLongName", serviceTypeLongName);
		row.setColumnValue("serviceTypeShortName", serviceTypeShortName);
		
//		 Can be added later, if we choose to use for third party, as well.
		TPRequirementDTO tpDto = deliveryItem.getTpRequirement();
		row.setColumnValue("tpAdjustment", tpDto != null ? tpDto.isAdjustment() : false);  
		row.setColumnValue("tpCalibration", tpDto != null ? tpDto.isCalibration() : false);  
		row.setColumnValue("tpRepair", tpDto != null ? tpDto.isRepair() : false);  
		row.setColumnValue("tpInvestigation", tpDto != null ? tpDto.isInvestigation() : false);
		
		row.setColumnValue("onBehalfOfCompany", jiDto.getOnBehalfCompany() != null ? jiDto.getOnBehalfCompany().getConame() : "");
		row.setColumnValue("customerDescription", instDto.getCustomerDescription());
		
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("deliveryItemNumber", Integer.class);
		metaData.addColumn("description", String.class);
		metaData.addColumn("barcode", Integer.class);
		metaData.addColumn("serialno", String.class);
		metaData.addColumn("plantno", String.class);
		metaData.addColumn("deliveryItemId", Integer.class);
		metaData.addColumn("jobNumber", String.class);
		metaData.addColumn("jobItemNumber", Integer.class);
		metaData.addColumn("requestedDate", Date.class);
		metaData.addColumn("accessoryFreeText", String.class);
		metaData.addColumn("jobItemClientRef", String.class);
		metaData.addColumn("jobClientRef", String.class);
		metaData.addColumn("serviceTypeShortName", String.class);
		metaData.addColumn("serviceTypeLongName", String.class);
		metaData.addColumn("customerDescription", String.class);
		metaData.addColumn("tpAdjustment", Boolean.class);
		metaData.addColumn("tpCalibration", Boolean.class);
		metaData.addColumn("tpRepair", Boolean.class);
		metaData.addColumn("tpInvestigation", Boolean.class);
		metaData.addColumn("onBehalfOfCompany", String.class);
		
		return true;
	}
}