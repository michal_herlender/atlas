package org.trescal.cwms.core.documents.birt.common;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implements the basic behavior for a ScriptedDataSet where a single entity is retrieved.
 * Prevents copy & paste of boilerplate code into subclasses.
 * 
 * <T> represents the generic class of the Entity (could be Customer, Delivery, etc...)
 * 
 * See delivery.GeneralDeliveryDataSet for an example of a subclass.
 *  
 * Subclasses should do three things:
 * (a) override beforeOpen(...) to initialize any service beans, always calling super.beforeOpen();
 * (b) implement getEntity(...) to lookup/return the actual entity
 * (c) implement setColumnValues(...) to set the column values
 * 
 * Galen Beck - 2018-03-06
 */
public abstract class EntityDataSet<T> extends AbstractDataSet {
	// Allows initialization to occur on first call to beforeOpen() and not on repeated calls  
	private boolean initialized;
	
	// Value determined via open() method via subclass method getEntity();
	private T entity;
	private boolean resultAvailable;
	
	private static final Logger logger = LoggerFactory.getLogger(EntityDataSet.class);
	
	/**
	 * Subclasses shouldn't need to override this method, just implement init() to set beans
	 * Entity lookup is done here as it's just needed once per data set
	 */
	@Override
	public void beforeOpen(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		super.beforeOpen(dataSet, reportContext);
		if (!initialized) {
			Integer entityId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
			if (entityId == null) {
				logger.error("Null parameter value for key : "+BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
			}
			init(dataSet, reportContext);
			entity = getEntity(entityId);
			if (entity == null) {
				logger.error("No entity found for id : "+entityId);
			}
			initialized = true;
		}
	}
	
	/**
	 * Subclass initialization method to perform bean lookup (just called once per instance)
	 * @param
	 * @return 
	 */
	public abstract void init(IDataSetInstance dataSet, IReportContext reportContext);
	
	/**
	 * Subclasses must implement this to lookup/return the actual entity 
	 * @param entityId - the id of the entity looked up, of type T
	 * @return the entity of type T (or null if none found)
	 */
	public abstract T getEntity(Integer entityId);
	
	@Override
	public final void open(IDataSetInstance dataSet) throws ScriptException {
		super.open(dataSet);
		resultAvailable = entity != null;
	}
	
	/**
	 * Subclasses must implement this to populate the actual column values for an entity
	 * @param instance - the entity of type T
	 * @param row - the IUpdatableDataSetRow to have columns populated
	 */
	public abstract void setColumnValues(T instance, IUpdatableDataSetRow row) throws ScriptException;
	
	@Override
	public final boolean fetch(IDataSetInstance dataSet, IUpdatableDataSetRow row) throws ScriptException {
		super.fetch(dataSet, row);
		boolean result = resultAvailable;
		if (resultAvailable) {
			setColumnValues(entity, row);
			super.verifyRow(row, 1);
			resultAvailable = false;
		}
		return result;
	}
	
	@Override
	public void close(IDataSetInstance dataSet) throws ScriptException {
		// TODO Auto-generated method stub
		super.close(dataSet);
	}
	
}