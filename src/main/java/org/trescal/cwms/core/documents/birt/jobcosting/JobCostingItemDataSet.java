package org.trescal.cwms.core.documents.birt.jobcosting;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.InstModelTools;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class JobCostingItemDataSet extends IterableDataSet<JobCostingItem>{

	private Locale locale;
	private Locale fallbackLocale;
	private TranslationService translationService;
	private Set<JobCostingItem> jobcostingItem;
	
	private static final Logger logger = LoggerFactory.getLogger(JobCostingItemDataSet.class);

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		locale = super.getDocumentLocale();
		fallbackLocale = super.getPrimaryLocale();
		JobCostingService jobcostingService = super.getApplicationContext().getBean(JobCostingService.class);
		translationService = super.getApplicationContext().getBean(TranslationService.class);

		Integer id = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		JobCosting jobcosting = jobcostingService.get(id);
		jobcostingItem = jobcosting.getItems();
	}

	@Override
	public Iterator<JobCostingItem> getIterator(IDataSetInstance dataSet) {
		return jobcostingItem.iterator();
	}

	@Override
	public void setColumnValues(JobCostingItem jobcostitem, IUpdatableDataSetRow row, int rowCount) throws ScriptException {

		SupportedCurrency currency = jobcostitem.getJobCosting().getCurrency();
		logger.debug("jobcostitemid : " + jobcostitem.getId());

		row.setColumnValue("jobcostitemid", jobcostitem.getId());
		row.setColumnValue("itemno", jobcostitem.getItemno());

		// Defaults for optional fields derived from job item, etc...
		String caltype = "";
		String serialno = "";
		String jobno;
		int jobitemno;
		String plantno = "";
		String modelname;
		String customerDescription = "";

		JobItem jobItem = jobcostitem.getJobItem();
		if (jobItem.getCalType() != null) {
			Set<Translation> translationscaltype = jobItem.getCalType().getServiceType().getLongnameTranslation();
			caltype = translationService.getCorrectTranslation(translationscaltype, locale);
		}
		modelname = InstModelTools.instrumentModelNameViaTranslations(jobItem.getInst(), locale, fallbackLocale);
		jobno = jobItem.getJob().getJobno();
		jobitemno = jobItem.getItemNo();
		if (jobItem.getInst().getSerialno() != null) {
			serialno = jobItem.getInst().getSerialno();
		}
		if (jobItem.getInst().getPlantno() != null) {
			plantno = jobItem.getInst().getPlantno();
		}
		if (jobItem.getInst().getCustomerDescription() != null) {
			customerDescription = jobItem.getInst().getCustomerDescription();
		}
		row.setColumnValue("caltype", caltype);
		row.setColumnValue("serialno", serialno);
		row.setColumnValue("jobno", jobno);
		row.setColumnValue("jobitemno", jobitemno);
		row.setColumnValue("plantno", plantno);
		row.setColumnValue("jobItemDueDate", DateTools.dateFromLocalDate(jobcostitem.getJobItem().getDueDate()));
		row.setColumnValue("barcode", jobcostitem.getJobItem().getInst().getPlantid());
		row.setColumnValue("modelname", modelname);
		row.setColumnValue("customerDescription", customerDescription);
		row.setColumnValue("finalCost", CurrencyValueFormatter.format(jobcostitem.getFinalCost(), currency, locale));
		
		row.setColumnValue("purchasecost", getActiveTotalCost(jobcostitem.getPurchaseCost()).doubleValue());
		row.setColumnValue("adjustmentcost", getActiveTotalCost(jobcostitem.getAdjustmentCost()).doubleValue());
		row.setColumnValue("repaircost", getActiveTotalCost(jobcostitem.getRepairCost()).doubleValue());
		row.setColumnValue("calibrationcost", getActiveTotalCost(jobcostitem.getCalibrationCost()).doubleValue());
		row.setColumnValue("inspectioncost", getFinalCost(jobcostitem.getInspectionCost()).doubleValue());
		
		row.setColumnValue("totalCost", CurrencyValueFormatter.format(jobcostitem.getTotalCost(), currency, locale));
		row.setColumnValue("disc",getTotalDiscount(jobcostitem).doubleValue());
		
		logger.info("inspection cost: "+getFinalCost(jobcostitem.getInspectionCost()).doubleValue());
		if (jobcostitem.getInspectionCost() == null) {
			logger.info("entity null ");
		}
		else {
			logger.info("from entity: active = "+jobcostitem.getInspectionCost().isActive());
			logger.info("from entity: final cost = "+jobcostitem.getInspectionCost().getFinalCost());
		}
	}

	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) {
		metaData.addColumn("jobcostitemid", Integer.class);
		metaData.addColumn("itemno", Integer.class);
		metaData.addColumn("caltype", String.class);
		metaData.addColumn("serialno", String.class);
		metaData.addColumn("totalCost", String.class);
		metaData.addColumn("jobno", String.class);
		metaData.addColumn("jobitemno", Integer.class);
		metaData.addColumn("jobItemDueDate", Date.class);
		metaData.addColumn("plantno", String.class);
		metaData.addColumn("barcode", Integer.class);
		metaData.addColumn("modelname", String.class);
		metaData.addColumn("customerDescription", String.class);
		metaData.addColumn("finalCost", String.class);		
		metaData.addColumn("purchasecost", Double.class);
		metaData.addColumn("adjustmentcost", Double.class);
		metaData.addColumn("repaircost", Double.class);
		metaData.addColumn("calibrationcost", Double.class);
		metaData.addColumn("inspectioncost", Double.class);
		metaData.addColumn("disc", Double.class);
		return true;
	}

	/**
	 * "Total cost" is used for all costs except inspection costs 
	 */
	private BigDecimal getActiveTotalCost(Cost cost) {
		if (cost != null && cost.isActive()) {
			return cost.getTotalCost();
		}
		else {
			return new BigDecimal("0.00");
		}
	}
	
	/**
	 * "Final cost" is used for inspection cost preview
	 * and is normally inactive
	 */
	private BigDecimal getFinalCost(Cost cost) {
		if (cost != null) {
			return cost.getFinalCost();
		}
		else {
			return new BigDecimal("0.00");
		}
	}

	private BigDecimal getTotalDiscount(JobCostingItem jobcostingitem){
		BigDecimal disc = new BigDecimal("0.00");
		disc = disc.add(getActiveDiscount(jobcostingitem.getAdjustmentCost()));
		disc = disc.add(getActiveDiscount(jobcostingitem.getCalibrationCost()));
		disc = disc.add(getActiveDiscount(jobcostingitem.getPurchaseCost()));
		disc = disc.add(getActiveDiscount(jobcostingitem.getRepairCost()));
		disc = disc.add(jobcostingitem.getGeneralDiscountValue());
		return disc;
	}
	private BigDecimal getActiveDiscount(Cost cost){

		if(cost != null && cost.isActive()){
			return cost.getDiscountValue();	
		}
		else{
			return new BigDecimal("0.00");
		}
	}
}
