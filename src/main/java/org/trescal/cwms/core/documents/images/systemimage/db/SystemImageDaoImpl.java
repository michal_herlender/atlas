package org.trescal.cwms.core.documents.images.systemimage.db;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.images.systemimage.SystemImage;
import org.trescal.cwms.core.documents.images.systemimage.SystemImage_;

@Repository("SystemImageDao")
public class SystemImageDaoImpl extends BaseDaoImpl<SystemImage, Integer> implements SystemImageDao {

	@Override
	protected Class<SystemImage> getEntity() {
		return SystemImage.class;
	}

	@Override
	public SystemImage findSystemImageByBusinessCompany(Company BussinesComp) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<SystemImage> cq = cb.createQuery(SystemImage.class);
		Root<SystemImage> systemImageRoot = cq.from(SystemImage.class);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(systemImageRoot.get(SystemImage_.businessCompany), BussinesComp));

		cq.select(systemImageRoot);
		cq.where(clauses);

		TypedQuery<SystemImage> query = getEntityManager().createQuery(cq);
		if (query.getResultList().size() > 0)
			return query.getResultList().get(0);
		else
			return null;
	}

	@Override
	public List<SystemImage> findSystemImagesByBusinessCompany(Company BussinesComp) {
		return getResultList(cb->{
			
			CriteriaQuery<SystemImage> cq = cb.createQuery(SystemImage.class);
			Root<SystemImage> systemImageRoot = cq.from(SystemImage.class);
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(systemImageRoot.get(SystemImage_.businessCompany), BussinesComp));

			cq.select(systemImageRoot);
			cq.where(clauses);
			
			return cq;
			
		});
	}

}
