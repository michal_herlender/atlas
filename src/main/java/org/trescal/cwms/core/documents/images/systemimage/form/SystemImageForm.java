package org.trescal.cwms.core.documents.images.systemimage.form;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SystemImageForm {

	private MultipartFile file;
	private String description;
	private Integer businessCompId;

}
