package org.trescal.cwms.core.documents.birt.repaircompletionreport;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class RepairCompletionReportDocumentDetails extends AbstractRepairReportDocumentDetails {
    @Autowired
    private MessageSource messageSource;

    public static final String CODE_TITLE = "docs.repaircompletionreport";
    public static final String DEFAULT_MESSAGE_TITLE = "Repair/Maintenance Completion Report";
    
    @Override
    public String getDocumentTitle(Integer entityId, Locale documentLocale) {
        return messageSource.getMessage(CODE_TITLE, null, DEFAULT_MESSAGE_TITLE, documentLocale);
    }
}
