package org.trescal.cwms.core.documents.usermanual.entity.db;

import java.util.Locale;

import org.trescal.cwms.core.documents.usermanual.entity.UserManual;

public interface UserManualService {
	UserManual getUserManual(Locale locale);
}
