package org.trescal.cwms.core.documents.images.image.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.documents.images.instrumentmodelimage.InstrumentModelImage;
import org.trescal.cwms.core.documents.images.jobitemimage.JobItemImage;
import org.trescal.cwms.core.documents.images.labelimage.LabelImage;

public interface ImageDao extends BaseDao<Image, Integer> {
	
	JobItemImage findJobImage(int id);

	LabelImage findLabelImage(int id);

	InstrumentModelImage findModelImage(int id);

	List<InstrumentModelImage> getAllInstrumentModelImages(int modelid);
	
	List<JobItemImage> getAllJobitemImagesForModel(int modelid);
	
	List<LabelImage> getAllLabelImages();

	List<JobItemImage> getInstrumentImages(int plantid);
	
	LabelImage getMostRecentLabelImage(String name);

	JobItemImage getMostRecentInstrumentImage(int plantid);
}