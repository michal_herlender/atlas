package org.trescal.cwms.core.documents.images;

public class ImagePropertyDTO
{
	private Integer imgHeight;
	private String imgSrc;
	private String imgType;
	private Integer imgWidth;

	public Integer getImgHeight()
	{
		return this.imgHeight;
	}

	public String getImgSrc()
	{
		return this.imgSrc;
	}

	public String getImgType()
	{
		return this.imgType;
	}

	public Integer getImgWidth()
	{
		return this.imgWidth;
	}

	public void setImgHeight(Integer imgHeight)
	{
		this.imgHeight = imgHeight;
	}

	public void setImgSrc(String imgSrc)
	{
		this.imgSrc = imgSrc;
	}

	public void setImgType(String imgType)
	{
		this.imgType = imgType;
	}

	public void setImgWidth(Integer imgWidth)
	{
		this.imgWidth = imgWidth;
	}

}
