package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Date;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteModel;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteModelGenerator;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.EntityDataSet;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.core.tools.DateTools;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectedJobDeliveryDataSet extends EntityDataSet<DeliveryDTO> {
	private DeliveryNoteModel model;

	@SuppressWarnings("unchecked")
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		model = (DeliveryNoteModel) reportContext.getAppContext().get(BirtEngine.APP_CONTEXT_MODEL);
		if (model == null) {
			Integer deliveryId = (Integer) (reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID));
			Locale locale = super.getDocumentLocale();
			DeliveryNoteModelGenerator modelGenerator = super.getApplicationContext().getBean(DeliveryNoteModelGenerator.class);
			model = modelGenerator.getModel(deliveryId, locale);
			reportContext.getAppContext().put(BirtEngine.APP_CONTEXT_MODEL, model);
		}
	}

	@Override
	public DeliveryDTO getEntity(Integer entityId) {
		return model.getDeliveryDto();
	}

	@Override
	public void setColumnValues(DeliveryDTO dto, IUpdatableDataSetRow row) throws ScriptException {
		row.setColumnValue("deliveryNumber", dto.getDeliveryNumber());
		row.setColumnValue("deliveryDate", DateTools.dateFromLocalDate(dto.getDeliveryDate()));
		// Destination contact, address, and created by are technically nullable (freehand support)
		String clientSubdivision = "";
		String clientContactName = "";
		String clientContactTelephone = "";
		String clientContactEmail = "";
		String createdByContactName = "";
		String createdByContactTelephone = "";
		String createdByContactEmail = "";
		
		if (dto.getDestContact() != null) {
			clientSubdivision = dto.getDestContact().getSubdiv().getSubname();
			clientContactName = dto.getDestContact().getName();
			clientContactTelephone = dto.getDestContact().getTelephone();
			clientContactEmail = dto.getDestContact().getEmail();
		}
		else {
			log.warn("destContact is null for delivery id "+dto.getDeliveryId());
			log.warn("destContactId is "+(dto.getDestContactId() != null ? dto.getDestContactId() : "null"));
		}
		
		if (dto.getCreatedByContact() != null) {
			createdByContactName = dto.getCreatedByContact().getName();
			createdByContactTelephone = dto.getCreatedByContact().getTelephone();
			createdByContactEmail = dto.getCreatedByContact().getEmail();
		}
		else {
			log.warn("createdByContact is null for delivery id "+dto.getDeliveryId());
			log.warn("createdByContactId is "+(dto.getCreatedByContactId() != null ? dto.getCreatedByContactId() : "null"));
		}
		
		row.setColumnValue("clientSubdivisionName", clientSubdivision);
		row.setColumnValue("clientContactName", clientContactName);
		row.setColumnValue("clientContactTelephone", clientContactTelephone);
		row.setColumnValue("clientContactEmail", clientContactEmail);
		row.setColumnValue("createdByContactName", createdByContactName);
		row.setColumnValue("createdByContactTelephone", createdByContactTelephone);
		row.setColumnValue("createdByContactEmail", createdByContactEmail);
		boolean hasCourierDespatch = dto.getCourierDespatch() != null;  
		String courierDespatchName = "";
		String courierDespatchNumber = "";
		String courierDespatchType = "";
		
		if (hasCourierDespatch) {
			courierDespatchName = dto.getCourierDespatch().getCourierName();
			courierDespatchNumber = dto.getCourierDespatch().getConsignmentNumber();
			courierDespatchType = dto.getCourierDespatch().getCourierDespatchTypeDescription();
		}
		row.setColumnValue("courierDespatchName", courierDespatchName);
		row.setColumnValue("courierDespatchNumber", courierDespatchNumber);
		row.setColumnValue("courierDespatchType", courierDespatchType);
		row.setColumnValue("hasCourierDespatch", hasCourierDespatch);
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("deliveryNumber", String.class);
		metaData.addColumn("deliveryDate", Date.class);
		metaData.addColumn("clientSubdivisionName", String.class);
		metaData.addColumn("clientContactName", String.class);
		metaData.addColumn("clientContactTelephone", String.class);
		metaData.addColumn("clientContactEmail", String.class);
		metaData.addColumn("createdByContactName", String.class);
		metaData.addColumn("createdByContactTelephone", String.class);
		metaData.addColumn("createdByContactEmail", String.class);
		metaData.addColumn("hasCourierDespatch", Boolean.class);
		metaData.addColumn("courierDespatchName", String.class);
		metaData.addColumn("courierDespatchNumber", String.class);
		metaData.addColumn("courierDespatchType", String.class);
		
		return true;
	}

}
