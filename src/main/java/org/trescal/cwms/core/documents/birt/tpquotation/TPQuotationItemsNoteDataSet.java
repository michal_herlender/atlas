package org.trescal.cwms.core.documents.birt.tpquotation;

import java.util.Collections;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;

public class TPQuotationItemsNoteDataSet extends NoteDataSet<TPQuoteItemNote> {
	private TPQuotationItemService tpquotationItemService;
	private static Logger logger = LoggerFactory.getLogger(TPQuotationItemsNoteDataSet.class);
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		tpquotationItemService = super.getApplicationContext().getBean(TPQuotationItemService.class);
	}
	@Override
	public Iterator<TPQuoteItemNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer tpqitemid = super.getInputParameter(dataSet, "tpqitemid");
		TPQuotationItem tpquotationItem = tpquotationItemService.findTPQuotationItemWithCosts(tpqitemid);
		if (tpquotationItem != null) {
			return super.getActivePublishedNotes(tpquotationItem.getNotes()).iterator();
		}
		else {
			logger.error("No TP Quotation Item found for tpqitemid : "+tpqitemid);
			return Collections.emptyIterator();
		}
	}
}
