package org.trescal.cwms.core.documents.retrieval;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;

import lombok.extern.slf4j.Slf4j;

/**
 * Spring component which looks up latest document from file system in standard way used for many document types
 */
@Slf4j
@org.springframework.stereotype.Component
public class FileSystemDocumentRetriever {
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private SystemComponentService scServ;
	
	public RetrieveDocumentResult getLatestDocumentFromFileSystem(Component systemComponentType, String identifier) {
		RetrieveDocumentResult result = null;

		SystemComponent systemComponent = this.scServ.findComponent(systemComponentType);
		File directory = this.compDirServ.getDirectory(systemComponent, identifier);
		if (directory.listFiles().length > 0) {
			// TODO evolve / verify how latest version is obtained - may need to use logic similar to FileNamingService 
			String fileName = directory.listFiles()[directory.listFiles().length - 1].getName();
			File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));
			if (file.exists()) {
				try (FileInputStream fis = new FileInputStream(file)) {
					// 10MB max
					byte[] fileData = new byte[(int) file.length()];
					IOUtils.read(fis, fileData);
					fis.close();
					// to base64
					result = RetrieveDocumentResult.builder()
							.success(true)
							.data(fileData)
							.filename(fileName)
							.build();
				} catch (Exception e) {
					log.error("Error reading file", e);
					result = RetrieveDocumentResult.builder()
							.success(false)
							.errorMessage("Error reading file")
							.build();
				}
			} else {
				result = RetrieveDocumentResult.builder()
						.success(false)
						.errorMessage("File not found")
						.build();
			}
		} else {
			result = RetrieveDocumentResult.builder()
					.success(false)
					.errorMessage("No files found for specified input")
					.build();
		}
		
		return result;
	}

}
