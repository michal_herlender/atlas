package org.trescal.cwms.core.documents.birt.purchaseorder;

import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.CurrencyValueFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.DateTools;

public class PurchaseOrderItemDataSet extends IterableDataSet<PurchaseOrderItem> {

	private Set<PurchaseOrderItem> purchaseOrderItems;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		Integer id =  (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		PurchaseOrderService purchaseOrderService = super.getApplicationContext().getBean(PurchaseOrderService.class);
		PurchaseOrder purchaseOrder = purchaseOrderService.find(id);
		purchaseOrderItems = purchaseOrder.getItems();
	}

	@Override
	public Iterator<PurchaseOrderItem> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return purchaseOrderItems.iterator();
	}

	@Override
	public void setColumnValues(PurchaseOrderItem itempo, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		SupportedCurrency currency = itempo.getOrder().getCurrency();
		
		Locale locale = super.getDocumentLocale();
		row.setColumnValue("poitemid", itempo.getId());
		row.setColumnValue("description", itempo.getDescription());
		if (itempo.getCostType() != null) {
			row.setColumnValue("type", itempo.getCostType().getMessageForLocale(locale));
		}
		else {
			row.setColumnValue("type", "");
		}
		row.setColumnValue("reqdate", DateTools.dateFromLocalDate(itempo.getDeliveryDate()));
		row.setColumnValue("qty", itempo.getQuantity());
		row.setColumnValue("cost", CurrencyValueFormatter.format(itempo.getTotalCost(), currency, locale));
		row.setColumnValue("disc", itempo.getGeneralDiscountRate().doubleValue());
		row.setColumnValue("total", itempo.getFinalCost().doubleValue());
		row.setColumnValue("total", CurrencyValueFormatter.format(itempo.getFinalCost(), currency, locale));
		row.setColumnValue("count", rowCount);
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("poitemid", Integer.class);
		metaData.addColumn("description", String.class);
		metaData.addColumn("type", String.class);
		metaData.addColumn("reqdate", Date.class);
		metaData.addColumn("qty", Integer.class);
		metaData.addColumn("cost", String.class);
		metaData.addColumn("disc", Double.class);
		metaData.addColumn("total", String.class);
		metaData.addColumn("count", Integer.class);
		return true;
	}

}
