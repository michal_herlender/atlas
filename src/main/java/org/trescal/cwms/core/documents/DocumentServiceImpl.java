package org.trescal.cwms.core.documents;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.DefaultFileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.componentdoctype.db.ComponentDoctypeService;
import org.trescal.cwms.core.system.entity.componentdoctypetemplate.ComponentDoctypeTemplate;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.template.Template;
import org.trescal.cwms.core.tools.BirtEngine;

@Service("documentService")
public class DocumentServiceImpl implements DocumentService
{
	protected static final Log logger = LogFactory.getLog(DocumentServiceImpl.class);
	
	@Autowired
	private ComponentDoctypeService cdsServ;
	@Autowired
	private BirtEngine birtEngine;
	
	@Override
	public void addFileNameToSession(Document doc, List<String> sessionNewFileList)
	{
		if (doc.isExists() && sessionNewFileList != null) {
			File file = new File(doc.getFileName());
			sessionNewFileList.add(file.getName());
		}
	}
	
	/**
	 * Changed from createVelocityDocument(...) to createBirtSQLDocument(...) as 
	 * we no longer support any velocity documents 2018-12-05
	 * 
	 * @param model Map containing the model detail to appear in the document
	 * @param componentName the name of the component requesting the document
	 * @param doctypeName the name of the document type being requested
	 * @param template the template to be used to create the document
	 * @param fnServ a file naming service object to be used to create the name
	 *        of the generated document
	 * @param rootDirectory the directory for this actual component where files
	 *        will be written to (this is only used if the
	 *        DefaultFileNamingService is called.
	 * @return Document simple POJO representing the created document and a
	 *         boolean to indicate if it was generated successfully 
	 */
	public Document createBirtSQLDocument(Map<String, Object> model, Component component, String doctypeName, Template template, FileNamingService fnServ, String rootDirectory, List<String> sessionNewFileList, Company businessCompany)
	{	
		ComponentDoctype cd = this.cdsServ.findComponentDoctype(component, doctypeName);
		if (cd == null)
		{
			throw new RuntimeException("No component doctype pairing found for System Component:"
					+ component + " Document Type:" + doctypeName);
		}
		else
		{
			if (template == null)
			{
				logger.info("No template provided, looking up default template for "
						+ component + " for " + doctypeName);

				// find the default template for this component and doctype
				// combination
				if ((cd.getTemplates() == null)
						|| (cd.getTemplates().size() < 1))
				{
					throw new RuntimeException("No component templates found for System Component:"
							+ component + " Document Type:" + doctypeName);
				}
				else
				{
					for (ComponentDoctypeTemplate cdt : cd.getTemplates())
					{
						template = cdt.getTemplate();
						if (cdt.isDefaultTemplate())
						{
							logger.info("Using template: "
									+ template.getName() + " ("
									+ template.getTemplatePath() + ")");
							break;
						}
					}
				}

			}

			// basic check that the template exists
			if ((template.getTemplatePath() == null)
					|| template.getTemplatePath().equals(""))
			{
				throw new RuntimeException("Invalid template path found for System Component:"
						+ component
						+ " Document Type:"
						+ doctypeName
						+ " Template: " + template.getName());
			}

			// if there is no FileNamingService then delegate to the default
			if (fnServ == null)
			{
				fnServ = new DefaultFileNamingService(component, rootDirectory);
			}

			String documentFileName = fnServ.getFileName().concat(".pdf");

			// make sure the parent directory exists
			new File(documentFileName.substring(0, documentFileName.lastIndexOf('\\') + 1)).mkdirs();

			try (FileOutputStream fos = new FileOutputStream(documentFileName)) {
				birtEngine.generatePDF(template.getTemplatePath(), model, fos, LocaleContextHolder.getLocale());
			}
			catch (Exception e) {
				logger.error(e);
			}

			// create a simple document object to return to the method caller to
			// indicate if this was a success
			Document doc = new Document();
			doc.setFileName(documentFileName);

			// test that the new document exists
			doc.setExists(new File(documentFileName).exists());
			if (sessionNewFileList != null) {
				// add file name to session for file browser
				this.addFileNameToSession(doc, sessionNewFileList);
			}
			return doc;
		}
	}
	
	@Override
	public void deleteFile(String fileName)
	{
		new File(fileName).delete();
	}
	
	/**
	 * Used when additional parameters are required (e.g. invoice)
	 *  
	 * @param additionalParameters to pass to BIRT document (optional - if null ignored)
	 * @return
	 */
	public Document createBirtDocument(Integer entityId, BaseDocumentType documentType, Locale locale, Component component, 
			FileNamingService fileNamingService, Map<String,Object> additionalParameters) {
	    Map<String, Object> parameters = new HashMap<>();
	    parameters.put(BaseDocumentDefinitions.PARAMETER_ENTITY_ID, entityId);
	    parameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_TYPE, documentType);
	    parameters.put(BaseDocumentDefinitions.PARAMETER_DOCUMENT_LOCALE, locale);
	    if (additionalParameters != null) parameters.putAll(additionalParameters);
	    
	    String documentFileName = fileNamingService.getFileName().concat(".pdf");
		try (FileOutputStream fos = new FileOutputStream(documentFileName)) {
			birtEngine.generatePDF(documentType.getTemplatePath(), parameters, fos, locale);
		}
		catch (Exception e) {
			logger.error(e);
			throw new RuntimeException(e);
		}
		boolean exists = new File(documentFileName).exists();
		return new Document(documentFileName, exists);
	}

	@Override
	public Document createBirtDocument(Integer entityId, BaseDocumentType documentType, Locale locale, Component component, FileNamingService fileNamingService) {
	    return createBirtDocument(entityId, documentType, locale, component, fileNamingService, null);
	}
}
