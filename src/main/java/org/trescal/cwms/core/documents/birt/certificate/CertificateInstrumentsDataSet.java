package org.trescal.cwms.core.documents.birt.certificate;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.IterableDataSet;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.tools.InstModelTools;

public class CertificateInstrumentsDataSet extends IterableDataSet<Instrument> {
	private List<Instrument> instruments; 

    @Override
    public void init(IDataSetInstance dataSet, IReportContext reportContext) {
        CertificateService certificateService = super.getApplicationContext().getBean(CertificateService.class);
        Integer certId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
        Certificate certificate = certificateService.findCertificate(certId);
        instruments = certificateService.getRelatedInstruments(certificate);
    }

    @Override
    public Iterator<Instrument> getIterator(IDataSetInstance dataSet) throws ScriptException {
        return instruments.iterator();
     }

    @Override
    public void setColumnValues(Instrument instrument, IUpdatableDataSetRow row, int rowCount) throws ScriptException {
        Locale documentLocale = super.getDocumentLocale();
        Locale primaryLocale = super.getPrimaryLocale();
        String instrumentName = InstModelTools.modelNameViaTranslations(instrument.getModel(), documentLocale, primaryLocale);
        row.setColumnValue("instrumentname",instrumentName);
        row.setColumnValue("serialno",instrument.getSerialno());
        row.setColumnValue("barcode",instrument.getPlantid());
        row.setColumnValue("plantno",instrument.getPlantno());
    }
}
