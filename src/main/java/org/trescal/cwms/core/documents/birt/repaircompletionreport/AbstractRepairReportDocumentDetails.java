package org.trescal.cwms.core.documents.birt.repaircompletionreport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;

public abstract class AbstractRepairReportDocumentDetails implements EntityDocumentDetails {
    @Autowired
    private RepairInspectionReportService repairInspectionReportService;

    @Override
    public Address getSourceAddress(Integer entityId) {
    	RepairInspectionReport repairInspectionReport = this.repairInspectionReportService.get(entityId);
        Address result = repairInspectionReport.getJi().getJob().getOrganisation().getDefaultAddress();
        if (result == null) throw new RuntimeException("No default address defined for business subdiv "+
        		repairInspectionReport.getJi().getJob().getOrganisation().getSubdivid());
        return result;
    }

    @Override
    public Address getDestinationAddress(Integer entityId) {
    	RepairInspectionReport repairInspectionReport = this.repairInspectionReportService.get(entityId);
        return repairInspectionReport.getJi().getJob().getCon().getDefAddress();
    }

    @Override
    public Company getBusinessCompany(Integer entityId) {
    	RepairInspectionReport repairInspectionReport = this.repairInspectionReportService.get(entityId);
        return repairInspectionReport.getJi().getJob().getOrganisation().getComp();
    }

    @Override
    public String getDocumentReference(Integer entityId) {
    	RepairInspectionReport repairInspectionReport = this.repairInspectionReportService.get(entityId);
        // Note, some older Repair Inspection reports don't have a number; in this case we return the plant id
        
        String docref = repairInspectionReport.getRirNumber();
        if (repairInspectionReport.getRirNumber() == null) {
        	return String.valueOf(repairInspectionReport.getJi().getInst().getPlantid());
        }
        return docref;
    }

    @Override
    public boolean exists(Integer entityId) {
    	RepairInspectionReport repairInspectionReport = this.repairInspectionReportService.get(entityId);
        return (repairInspectionReport != null);
    }
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}
	
}
