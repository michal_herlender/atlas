package org.trescal.cwms.core.documents.birt.quotation;

import java.util.Iterator;
import java.util.List;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;

public class QuotationNoteDataset extends NoteDataSet<QuoteNote> {
	private List<QuoteNote> activePublishedNotes;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		QuotationService quotationService = getApplicationContext().getBean(QuotationService.class);
		Integer id = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
		Quotation quotation = quotationService.get(id); 
		activePublishedNotes = super.getActivePublishedNotes(quotation.getNotes()); 
	}

	@Override
	public Iterator<QuoteNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		return activePublishedNotes.iterator();
	}
}
