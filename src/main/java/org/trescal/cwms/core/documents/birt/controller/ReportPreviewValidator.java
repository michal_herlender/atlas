package org.trescal.cwms.core.documents.birt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentResolver;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ReportPreviewValidator extends AbstractBeanValidator {
	
	@Autowired
	private BaseDocumentResolver resolver;

	@Override
	public boolean supports(Class<?> clazz) {
		return ReportPreviewForm.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		// Test for entity existence only if all fields provided
		if (!errors.hasErrors()) {
			ReportPreviewForm form = (ReportPreviewForm) target;
			EntityDocumentDetails detailsBean = resolver.getComponent(form.getDocumentType());
			if (!detailsBean.exists(form.getEntityId())) {
				errors.reject("error.rest.data.notfound", new Object[]{form.getEntityId()}, "Data not found: {0}");
			}
			else {
				// Entity exists, now check if it can be rendered (max sizes etc...)
				detailsBean.canBeRendered(form.getEntityId(), errors);
			}
		}
	}
}
