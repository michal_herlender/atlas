package org.trescal.cwms.core.documents.birt.purchaseorder;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;

@Component
public class PurchaseOrderDocumentDetails implements EntityDocumentDetails {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private PurchaseOrderService purchaseOrderService;

	private static final String CODE_TITLE = "purchaseorder";
	private static final String DEFAULT_MESSAGE_TITLE = "Purchase Order";

	@Override
	public Address getSourceAddress(Integer entityId) {
		PurchaseOrder po = purchaseOrderService.find(entityId);
		return po.getBusinessContact().getDefAddress();
	}

	@Override
	public Address getDestinationAddress(Integer entityId) {
		PurchaseOrder po = purchaseOrderService.find(entityId);
		return po.getAddress();
	}

	@Override
	public Company getBusinessCompany(Integer entityId) {
		PurchaseOrder po = purchaseOrderService.find(entityId);
		return po.getOrganisation();
	}

	@Override
	public String getDocumentReference(Integer entityId) {
		PurchaseOrder po = purchaseOrderService.find(entityId);
		return po.getPono();
	}

	@Override
	public String getDocumentTitle(Integer entityId, Locale documentLocale) {
		return messageSource.getMessage(CODE_TITLE,null, DEFAULT_MESSAGE_TITLE, documentLocale);
	}

	@Override
	public boolean exists(Integer entityId) {
		PurchaseOrder po = purchaseOrderService.find(entityId);
		return (po != null);
	}
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}

}
