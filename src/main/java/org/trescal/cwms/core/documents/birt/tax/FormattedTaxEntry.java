package org.trescal.cwms.core.documents.birt.tax;

import lombok.Getter;
import lombok.Setter;

/**
 * Holder class for tax information to display on a BIRT document
 * 
 * Instances of this class can have their data populated from:
 *
 * - Invoice (single tax amount)
 * - InvoiceTax (if present)
 * - CreditNote (single tax amount)
 * - CreditNoteTax (if present)  
 */
@Getter
@Setter
public class FormattedTaxEntry {
    private String description;
    private String amount;
    private String rate;
    private String tax;
}
