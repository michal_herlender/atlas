package org.trescal.cwms.core.documents.images.image.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.documents.images.image.ImageComparator;
import org.trescal.cwms.core.documents.images.instrumentmodelimage.InstrumentModelImage;
import org.trescal.cwms.core.documents.images.jobitemimage.JobItemImage;
import org.trescal.cwms.core.documents.images.labelimage.LabelImage;

@Repository("ImageDao")
public class ImageDaoImpl extends BaseDaoImpl<Image, Integer> implements ImageDao {
	
	@Override
	protected Class<Image> getEntity() {
		return Image.class;
	}
	
	@Override
	public JobItemImage findJobImage(int id) {
		Criteria criteria = this.getSession().createCriteria(JobItemImage.class);
		criteria.add(Restrictions.idEq(id));
		return (JobItemImage) criteria.uniqueResult();
	}
	
	@Override
	public InstrumentModelImage findModelImage(int id) {
		Criteria criteria = this.getSession().createCriteria(InstrumentModelImage.class);
		criteria.add(Restrictions.idEq(id));
		return (InstrumentModelImage) criteria.uniqueResult();
	}
	
	@Override
	public LabelImage findLabelImage(int id) {
		Criteria criteria = this.getSession().createCriteria(LabelImage.class);
		criteria.add(Restrictions.idEq(id));
		return (LabelImage) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<InstrumentModelImage> getAllInstrumentModelImages(int modelid) {
		Criteria crit = this.getSession().createCriteria(InstrumentModelImage.class);
		crit.createCriteria("model").add(Restrictions.idEq(modelid));
		crit.addOrder(Order.desc("addedOn"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<JobItemImage> getAllJobitemImagesForModel(int modelid) {
		Criteria crit = this.getSession().createCriteria(JobItemImage.class);
		crit.createCriteria("jobitem").createCriteria("inst").createCriteria("model").add(Restrictions.idEq(modelid));
		crit.addOrder(Order.desc("addedOn"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LabelImage> getAllLabelImages() {
		Criteria crit = this.getSession().createCriteria(LabelImage.class);
		crit.addOrder(Order.asc("description"));
		crit.addOrder(Order.desc("lastModified"));
		return crit.list();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<JobItemImage> getInstrumentImages(int plantid) {
		Criteria crit = this.getSession().createCriteria(JobItemImage.class);
		crit.createCriteria("jobitem").createCriteria("inst").add(Restrictions.idEq(plantid));
		List<JobItemImage> images = (List<JobItemImage>) crit.list();
		images.sort(new ImageComparator());
		return images;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public LabelImage getMostRecentLabelImage(String name) {
		Criteria criteria = this.getSession().createCriteria(LabelImage.class);
		criteria.add(Restrictions.eq("description",name));
		criteria.addOrder(Order.desc("lastModified"));
		criteria.setMaxResults(1);
		List<LabelImage> results = (List<LabelImage>) criteria.list();
		return results.isEmpty() ? null : (LabelImage) results.get(0);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public JobItemImage getMostRecentInstrumentImage(int plantid) {
		Criteria crit = this.getSession().createCriteria(JobItemImage.class);
		crit.createCriteria("jobitem").createCriteria("inst").add(Restrictions.idEq(plantid));
		crit.addOrder(Order.desc("addedOn"));
		crit.setMaxResults(1);
		List<JobItemImage> results = (List<JobItemImage>) crit.list(); 
		return results.isEmpty() ? null : (JobItemImage) results.get(0);
	}
}