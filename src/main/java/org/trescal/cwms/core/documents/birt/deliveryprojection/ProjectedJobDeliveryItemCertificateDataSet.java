package org.trescal.cwms.core.documents.birt.deliveryprojection;

import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IScriptedDataSetMetaData;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;

public class ProjectedJobDeliveryItemCertificateDataSet extends IterableDeliveryDataSet<CertificateProjectionDTO> {
	
	private EnumSet<CertStatusEnum> includeStatus;

	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		super.init(dataSet, reportContext);
		// After calling superclass method the model is available
		if (super.getModel().getIncludeOnlySignedCertificates()) {
			includeStatus = EnumSet.of(CertStatusEnum.SIGNED);
		}
		else {
			includeStatus = EnumSet.of(CertStatusEnum.SIGNED, CertStatusEnum.SIGNING_NOT_SUPPORTED);
		}
	}
	
	@Override
	public Iterator<CertificateProjectionDTO> getIterator(IDataSetInstance dataSet) throws ScriptException {
		DeliveryItemDTO deliveryItemDto = super.getDeliveryItemDTO(dataSet);
		// We filter to just approved certificates
		List<CertificateProjectionDTO> certificates = deliveryItemDto.getJobItem().getCertificates();
		if (certificates == null)
			certificates = Collections.emptyList();
		List<CertificateProjectionDTO> filtered = certificates.stream()
				.filter(certDto -> certDto.getCertStatus() != null)
				.filter(certDto -> includeStatus.contains(certDto.getCertStatus()))
				.collect(Collectors.toList());
		return filtered.iterator();
	}

	@Override
	public void setColumnValues(CertificateProjectionDTO certDto, IUpdatableDataSetRow row, int rowCount)
			throws ScriptException {
		Locale locale = super.getDocumentLocale();
		
		row.setColumnValue("certno", certDto.getCertno());
		row.setColumnValue("thirdCertNo", certDto.getThirdCertNo());
		row.setColumnValue("calDate", certDto.getCalDate());
		CalibrationVerificationStatus cvs = certDto.getCalibrationVerificationStatus();
		row.setColumnValue("cvsShortName", cvs != null ? cvs.getShortNameForLocale(locale) : "");
		row.setColumnValue("cvsLongName", cvs != null ? certDto.getCalibrationVerificationStatus().getNameForLocale(locale) : "");
	}
	
	@Override
	public boolean describe(IDataSetInstance dataSet, IScriptedDataSetMetaData metaData) throws ScriptException {
		metaData.addColumn("certno", String.class);
		metaData.addColumn("thirdCertNo", String.class);
		metaData.addColumn("calDate", Date.class);
		metaData.addColumn("cvsShortName", String.class);
		metaData.addColumn("cvsLongName", String.class);
		return true;
	}

}
