package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;

public class CreditNoteFileNamingService implements FileNamingService
{
	private String cnno = "";
	private boolean issued;
	private String fileName = "";
	protected final Log logger = LogFactory.getLog(this.getClass());
	private String rootDirectory;

	public CreditNoteFileNamingService(CreditNote cn)
	{
		this.fileName = "";
		this.rootDirectory = cn.getDirectory().toString();
		this.cnno = cn.getCreditNoteNo();
		this.issued = cn.isIssued();
	}

	public String getFileName()
	{
		File rootFolder = new File(this.rootDirectory);
		if (!rootFolder.exists())
		{
			this.logger.error("Directory does not exist: '"
					+ this.rootDirectory + "'");
			rootFolder.mkdirs();
		}

		this.cnno = this.cnno.replace('/', '.');

		// create the basic file name 'Credit Note num rev '
		StringBuffer basicFileName = new StringBuffer();
		if (!issued)
			basicFileName.append("Draft ");
		basicFileName.append("Credit Note ");
		basicFileName.append(this.cnno);
		basicFileName.append(" rev ");

		// now create the full filename (without any file extension)
		this.fileName = this.rootDirectory.concat(File.separator).concat(this.getNextInvoiceFileName(basicFileName.toString(), this.rootDirectory));

		return this.fileName;
	}

	/**
	 * From the given basic naming structure and PO directory this function gets
	 * a list of any existing PO files for this Costing and works out what the
	 * next revision number for the current document should be.
	 * 
	 * @param basicFileName the basic file naming structure for this PO
	 * @param rootDirectory the path to the PO directory for this quotation
	 * @return
	 */
	private String getNextInvoiceFileName(String basicFileName, String rootDirectory)
	{
		String fileName = "";
		File rootDir = new File(rootDirectory);

		String[] invFiles = rootDir.list();
		if (invFiles != null)
		{
			// build an arraylist of files whoose name matches documents
			ArrayList<String> thisIFiles = new ArrayList<String>();
			for (String aIFile : invFiles)
			{
				if (aIFile.contains(basicFileName))
				{
					thisIFiles.add(aIFile);
				}
			}

			if (thisIFiles.size() > 0)
			{
				int rev = 0;
				// now perform a second pass to decide on the next sequence
				// number
				for (String invFile : thisIFiles)
				{
					if (invFile.lastIndexOf('.') > -1)
					{
						// strip the doc extension
						String document = invFile.substring(0, invFile.length()
								- (invFile.length() - invFile.lastIndexOf('.')));

						// strip the basic document name from the document to
						// get the document rev number
						String revNo = document.replaceAll(basicFileName, "");

						if (revNo.matches("\\d*") && !revNo.trim().equals(""))
						{
							if (Integer.parseInt(revNo) > rev)
							{
								rev = Integer.parseInt(revNo);
							}
						}
					}
				}
				rev = rev + 1;
				fileName = basicFileName.concat(String.valueOf(rev));
			}
			else
			{
				fileName = basicFileName.concat(String.valueOf(1));
			}
		}
		else
		{
			fileName = basicFileName.concat(String.valueOf(1));
		}
		return fileName;
	}

}
