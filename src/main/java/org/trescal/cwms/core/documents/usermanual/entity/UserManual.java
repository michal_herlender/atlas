package org.trescal.cwms.core.documents.usermanual.entity;

import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "usermanual")
public class UserManual {
	private int id;
	private byte[] fileBinary;
	private Locale locale;
	private String filename;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	@NotNull
	@Column(name = "filebinary", nullable = false, columnDefinition = "varbinary(max)")
	public byte[] getFileBinary() {
		return fileBinary;
	}
	@NotNull
	@Column(name="filename", nullable = false, length=255)
	public String getFilename() {
		return filename;
	}
	@NotNull
	@Column(name = "locale", nullable = false)
	public Locale getLocale() {
		return locale;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setFileBinary(byte[] fileBinary) {
		this.fileBinary = fileBinary;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
