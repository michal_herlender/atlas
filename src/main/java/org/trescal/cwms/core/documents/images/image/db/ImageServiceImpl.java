package org.trescal.cwms.core.documents.images.image.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.images.ImagePropertyDTO;
import org.trescal.cwms.core.documents.images.ImageTagDto;
import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.documents.images.image.ImageComparator;
import org.trescal.cwms.core.documents.images.image.ImageType;
import org.trescal.cwms.core.documents.images.instrumentmodelimage.InstrumentModelImage;
import org.trescal.cwms.core.documents.images.jobitemimage.JobItemImage;
import org.trescal.cwms.core.documents.images.labelimage.LabelImage;
import org.trescal.cwms.core.documents.images.taggedimage.TaggedImage;
import org.trescal.cwms.core.documents.images.taggedimage.TaggedImageComparator;
import org.trescal.cwms.core.dwr.DWRService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.ImageTools;
import org.trescal.cwms.files.images.entity.ImageFileData;
import org.trescal.cwms.files.images.entity.ImageThumbData;

@Service("ImageService")
public class ImageServiceImpl implements ImageService
{
	@Value("#{props['cwms.config.images.systemimages.foldersize']}")
	private Integer imagesFolderSize;
	@Value("#{props['cwms.config.images.systemimages.root']}")
	private String imagesRoot;
	@Value("#{props['cwms.config.images.thumbNailWidth']}")
	private Double thumbNailWidth;
	@Value("#{props['cwms.config.tmp.webcamimages']}")
	private String webCamPath;
	@Autowired
	private DWRService dwrServ;
	@Autowired
	private ImageDao imageDao;
	@Autowired
	private InstrumentModelService modelServ;
	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private UserService userService;
	@PersistenceContext(unitName="entityManagerFactory2")
	private EntityManager entityManager2;
	@Autowired @Qualifier("transactionManager2")
	private PlatformTransactionManager transactionManager2;
	
	@Override
	public void uploadImage(Image image, HttpServletRequest request)
	{
		String username = (String) request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		// Eager load not needed, not in response
		Contact contact = this.userService.get(username).getCon();
		image.setAddedBy(contact);
		image.setAddedOn(new Date());
		this.insertImage(image);
	}
	
	@Override
	public ResultWrapper addImagesToEntity(int[] imageIds, String[] descriptions, int entityId, ImageTagDto[] tags, ImageType type, HttpServletRequest request)
	{
		JobItem ji = null;
		InstrumentModel model = null;
		if ((type != null) && type.equals(ImageType.JOBITEM))
			ji = this.jobItemServ.findJobItem(entityId);
		if ((type != null) && type.equals(ImageType.INSTRUMENTMODEL))
			model = this.modelServ.findInstrumentModel(entityId);
		if ((type == null) || (!type.equals(ImageType.LABEL) && (ji == null) && (model == null)))
			return new ResultWrapper(false, "Could not map image to a valid entity");
		else {
			ArrayList<Image> images = new ArrayList<Image>();
			String username = (String) request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact contact = this.userService.getEagerLoad(username).getCon();
			int count = 0;
			// iterate over the list of images
			for (int imageId : imageIds) {
				//image has an id so it's a db upload
				Image image = null;
				//jobitem or model image??
				if (type.equals(ImageType.JOBITEM)) {
					JobItemImage jimage = this.findJobImage(imageId);
					jimage.setJobitem(ji);
					image = jimage;
				}
				else if (type.equals(ImageType.INSTRUMENTMODEL)) {
					InstrumentModelImage iimage = this.findModelImage(imageId);
					iimage.setModel(model);
					image = iimage;
				}
				else if (type.equals(ImageType.LABEL)) {
					LabelImage limage = this.findLabelImage(imageId);
					image = limage;
				}
				//create a thumbnail
				try {
					image = ImageTools.generateThumbNailImageDb(this.thumbNailWidth, image);
				}
				catch (Exception e) {
					// swallow the exception for now
					e.printStackTrace();
				}
				//update the tags
				if (tags != null)
					// check if any image tags match this image and add
					// them
					for (ImageTagDto d : tags)
						if (d.getImageId() == imageId){
							TaggedImage tag = new TaggedImage();
							tag.setImage(image);
							tag.setTag(d.getTag());
							if (image.getTags() == null)
								image.setTags(new TreeSet<TaggedImage>(new TaggedImageComparator()));
							image.getTags().add(tag);
						}
				//update the other image parameters
				image.setDescription(descriptions[count]);
				image.setAddedBy(contact);
				image.setAddedOn(new Date());
				//update the image
				this.saveOrUpdateImage(image);
				images.add(image);
				count++;
			}
			return new ResultWrapper(true, "", images, null);
		}
	}
	
	@Override
	public ResultWrapper ajaxDeleteImage(int imageid)
	{
		// first find image to be deleted
		Image image = this.findImage(imageid);
		// is image null?
		if (image != null)
		{
			// is job item image?
			if (image instanceof JobItemImage)
			{
				// find job item
				JobItem ji = this.jobItemServ.findJobItem(((JobItemImage) image).getJobitem().getJobItemId());
				// delete the image
				this.deleteImage(image);
				// job item null?
				if (ji != null)
				{
					// create new image properties object
					ImagePropertyDTO ipdto = new ImagePropertyDTO();
					// first try and get most recent instrument image from this
					// or other
					// job items
					Image img = this.imageDao.getMostRecentInstrumentImage(ji.getInst().getPlantid());
					ipdto.setImgType("JOBITEM");
					// is image null?
					if (img == null)
					{
						// source instrument model image
						img = getMostRecentInstrumentModelImage(ji.getInst().getModel().getModelid());
						ipdto.setImgType("INSTMODEL");
					}
					// instrument model image null?
					if (img != null)
					{
						// set source of image
						ipdto.setImgSrc(img.getRelativeThumbPath());
						// return most recent model image
						return new ResultWrapper(true, "", ipdto, null);
					}
					else
					{
						// return unsuccessful result
						return new ResultWrapper(true, "", null, null);
					}
				}
				else
				{
					return new ResultWrapper(false, "Job item could not be found to retrieve image", null, null);
				}
			}
			else if (image instanceof InstrumentModelImage)
			{
				// find instrument model
				InstrumentModel im = this.modelServ.findInstrumentModel(((InstrumentModelImage) image).getModel().getModelid());
				// delete the image
				this.deleteImage(image);
				// job item null?
				if (im != null)
				{
					// create new image properties object
					ImagePropertyDTO ipdto = new ImagePropertyDTO();
					// source instrument model image
					Image img = getMostRecentInstrumentModelImage(im.getModelid());
					ipdto.setImgType("INSTMODEL");
					// instrument model image null?
					if (img != null)
					{
						// set source of image
						ipdto.setImgSrc(img.getRelativeThumbPath());
						// return most recent model image
						return new ResultWrapper(true, "", ipdto, null);
					}
					else
					{
						// return unsuccessful result
						return new ResultWrapper(true, "", null, null);
					}
				}
				else
				{
					return new ResultWrapper(false, "Instrument model could not be found to retrieve image", null, null);
				}
			}
		}
		else
		{
			return new ResultWrapper(false, "Image could not be found", null, null);
		}

		return new ResultWrapper(false, "Appropriate action could not be determined", null, null);
	}

	public void deleteImage(Image image)
	{
		this.imageDao.remove(image);
	}
	
	public Image findImage(int id)
	{
		return this.imageDao.find(id);
	}
	
	@Override
	public JobItemImage findJobImage(int id) {
		return (JobItemImage) this.imageDao.findJobImage(id);
	}

	@Override
	public LabelImage findLabelImage(int id) {
		return (LabelImage) this.imageDao.findLabelImage(id);
	}

	@Override
	public InstrumentModelImage findModelImage(int id) {
		return (InstrumentModelImage) this.imageDao.findModelImage(id);
	}
	
	@Override
	public List<Image> getAllImages()
	{
		return this.imageDao.findAll();
	}

	@Override
	public List<InstrumentModelImage> getAllInstrumentModelImages(int modelid)
	{
		return this.imageDao.getAllInstrumentModelImages(modelid);
	}

	@Override
	public List<JobItemImage> getAllJobitemImagesForModel(int modelid)
	{
		return this.imageDao.getAllJobitemImagesForModel(modelid);
	}

	@Override
	public List<LabelImage> getAllLabelImages() {
		return this.imageDao.getAllLabelImages();
	}

	@Override
	public List<JobItemImage> getInstrumentImages(int plantid)
	{
		return this.imageDao.getInstrumentImages(plantid);
	}

	@Override
	public LabelImage getMostRecentLabelImage(String name) {
		return this.imageDao.getMostRecentLabelImage(name);
	}
	
	@Override
	public JobItemImage getMostRecentInstrumentImage(int plantid)
	{
		return this.imageDao.getMostRecentInstrumentImage(plantid);
	}
	
	@Override
	public InstrumentModelImage getMostRecentInstrumentModelImage(int modelid)
	{
		List<InstrumentModelImage> allModelImages = imageDao.getAllInstrumentModelImages(modelid);
		allModelImages.sort(new ImageComparator());
		return allModelImages.size() == 0 ? null : allModelImages.get(0);
	}
	
	public ImagePropertyDTO getNextApplicableImageForJI(int jobItemId)
	{
		// find job item
		JobItem ji = this.jobItemServ.findJobItem(jobItemId);
		// job item null?
		if (ji != null)
		{
			// create new image properties object
			ImagePropertyDTO ipdto = new ImagePropertyDTO();
			// first try and get most recent instrument image from this or other
			// job items
			Image img = this.imageDao.getMostRecentInstrumentImage(ji.getInst().getPlantid());
			ipdto.setImgType("JOBITEM");
			// is image null?
			if (img == null)
			{
				// source instrument model image
				img = getMostRecentInstrumentModelImage(ji.getInst().getModel().getModelid());
				ipdto.setImgType("INSTMODEL");
			}
			// instrument model image null?
			if (img != null)
			{
				// set source of image
				ipdto.setImgSrc(img.getRelativeThumbPath());
				// return most recent model image
				return ipdto;
			}
			else
			{
				// return no image
				return null;
			}
		}
		else
		{
			// return no image
			return null;
		}
	}
	
	@Override
	public String getSystemImageFolder(int imageid)
	{
		int folderBracket = (int) Math.floor(imageid / this.imagesFolderSize)
				* this.imagesFolderSize;
		return String.valueOf(folderBracket).concat("/");
	}

	@Override
	public File importImage(File currentURL, String targetURL)
	{
		File targetFile = new File(targetURL);
		if ((currentURL != null) && currentURL.exists())
		{
			targetFile.getParentFile().mkdirs();

			try
			{
				FileInputStream input = new FileInputStream(currentURL);

				if (targetFile.exists())
				{
					targetFile.delete();
					targetFile = new File(targetURL);
				}
				FileOutputStream output = new FileOutputStream(targetFile);
				byte[] bytearr = new byte[512];
				int len = 0;
				try
				{
					while ((len = input.read(bytearr)) != -1)
					{
						output.write(bytearr, 0, len);
					}
				}
				catch (IOException exp)
				{
					throw exp;
				}
				finally
				{
					input.close();
					output.close();
				}
			}
			catch (Exception exp)
			{
				exp.printStackTrace();
				return null;
			}
		}
		return targetFile;
	}
	
	@Transactional
	public Integer insertImage(Image image)
	{
		this.imageDao.persist(image);
		TransactionStatus ts = transactionManager2.getTransaction(null);
		ImageFileData ifd = image.getImageFile();
		if(ifd != null) {
			ifd.setImageId(image.getId());
			entityManager2.persist(ifd);
		}
		ImageThumbData itd = image.getThumbFile();
		if(itd != null) {
			itd.setImageId(image.getId());
			entityManager2.persist(itd);
		}
		transactionManager2.commit(ts);
		return image.getId(); 
	}

	@Transactional
	public void saveOrUpdateImage(Image image)
	{
		this.imageDao.saveOrUpdate(image);
		TransactionStatus ts = transactionManager2.getTransaction(null);
		ImageFileData ifd = image.getImageFile();
		if(ifd != null && image.getId() != null) {
			Integer id = image.getId();
			ifd.setImageId(id);
			if(entityManager2.find(ImageFileData.class, id) == null) entityManager2.persist(ifd);
			else entityManager2.merge(ifd);
		}
		ImageThumbData itd = image.getThumbFile();
		if(itd != null && image.getId() != null) {
			Integer id = image.getId();
			itd.setImageId(id);
			if(entityManager2.find(ImageFileData.class, id) == null) entityManager2.persist(itd);
			else entityManager2.merge(itd);
		}
		transactionManager2.commit(ts);
	}
	
	public void setImagesFolderSize(Integer imagesFolderSize) {
		this.imagesFolderSize = imagesFolderSize;
	}
	
	public void updateImage(Image Image)
	{
		this.imageDao.update(Image);
	}

}