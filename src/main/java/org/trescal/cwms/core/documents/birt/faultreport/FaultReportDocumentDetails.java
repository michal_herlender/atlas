package org.trescal.cwms.core.documents.birt.faultreport;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.documents.birt.common.EntityDocumentDetails;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;

@Component
public class FaultReportDocumentDetails implements EntityDocumentDetails {
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FaultReportService faultReportService;

    public static final String CODE_TITLE = "docs.faultreport";
    public static final String DEFAULT_MESSAGE_TITLE = "Fault Report";

    @Override
    public Address getSourceAddress(Integer entityId) {
        FaultReport faultReport = this.faultReportService.findFaultReport(entityId);
        Address result = faultReport.getJobItem().getJob().getOrganisation().getDefaultAddress();
        if (result == null) throw new RuntimeException("No default address defined for business subdiv "+
                faultReport.getJobItem().getJob().getOrganisation().getSubdivid());
        return result;
    }

    @Override
    public Address getDestinationAddress(Integer entityId) {
        FaultReport faultReport = this.faultReportService.findFaultReport(entityId);
        return faultReport.getJobItem().getJob().getCon().getDefAddress();
    }

    @Override
    public Company getBusinessCompany(Integer entityId) {
        FaultReport faultReport = this.faultReportService.findFaultReport(entityId);
        return faultReport.getJobItem().getJob().getOrganisation().getComp();
    }

    @Override
    public String getDocumentReference(Integer entityId) {
        FaultReport faultReport = this.faultReportService.findFaultReport(entityId);
        // Note, some older fault reports don't have a number; in this case we return the plant id
        
        String docref = faultReport.getFaultReportNumber();
        if (faultReport.getFaultReportNumber() == null) {
        	return String.valueOf(faultReport.getJobItem().getInst().getPlantid());
        }
        return docref;
    }

    @Override
    public String getDocumentTitle(Integer entityId, Locale documentLocale) {
        return messageSource.getMessage(CODE_TITLE, null, DEFAULT_MESSAGE_TITLE, documentLocale);
    }

    @Override
    public boolean exists(Integer entityId) {
        FaultReport faultReport = this.faultReportService.findFaultReport(entityId);
        return (faultReport != null);
    }
	
	@Override
	public boolean canBeRendered(Integer entityId, Errors errors) {
		// All documents of this type can be rendered (no size restrictions etc...)
		return true;
	}
}
