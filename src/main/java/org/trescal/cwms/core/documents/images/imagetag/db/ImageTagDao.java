package org.trescal.cwms.core.documents.images.imagetag.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.documents.images.imagetag.ImageTag;

public interface ImageTagDao extends BaseDao<ImageTag, Integer> {}