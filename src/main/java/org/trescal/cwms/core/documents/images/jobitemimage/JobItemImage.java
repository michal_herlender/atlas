package org.trescal.cwms.core.documents.images.jobitemimage;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@DiscriminatorValue("jobitem")
public class JobItemImage extends Image
{	
	private JobItem jobitem;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJobitem()
	{
		return this.jobitem;
	}

	public void setJobitem(JobItem jobitem)
	{
		this.jobitem = jobitem;
	}
}