package org.trescal.cwms.core.documents.usermanual.entity.db;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.documents.usermanual.entity.UserManual;

public class UserManualServiceImpl implements UserManualService {
	
	@Autowired
	private UserManualDao userManualDao;
	
	@Override
	public UserManual getUserManual(Locale locale)
	{
		return this.userManualDao.getUserManual(locale); 
	}
}
