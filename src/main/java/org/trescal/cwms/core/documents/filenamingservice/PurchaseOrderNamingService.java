package org.trescal.cwms.core.documents.filenamingservice;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.system.Constants;

public class PurchaseOrderNamingService implements FileNamingService
{
	protected final Log logger = LogFactory.getLog(this.getClass());

	private String fileName = "";
	private String rootDirectory;
	private String pono = "";

	public PurchaseOrderNamingService(PurchaseOrder order)
	{
		this.fileName = "";
		this.rootDirectory = order.getDirectory().toString();
		this.pono = order.getPono();
	}

	public String getFileName()
	{
		File rootFolder = new File(this.rootDirectory);
		if (!rootFolder.exists())
		{
			this.logger.error("Directory does not exist: '"
					+ this.rootDirectory + "'");
			rootFolder.mkdirs();
		}

		this.pono = this.pono.replace('/', '.');

		// create the basic file name 'PO num rev '
		String basicFileName = "PO ".concat(this.pono).concat(" ").concat(Constants.FILE_NAMING_REVISION).concat(" ");

		// now create the full filename (without any file extension)
		this.fileName = this.rootDirectory.concat(File.separator).concat(this.getNextPurchaseOrderFileName(basicFileName, this.rootDirectory));

		return this.fileName;
	}

	/**
	 * From the given basic naming structure and PO directory this function gets
	 * a list of any existing PO files for this Costing and works out what the
	 * next revision number for the current document should be.
	 * 
	 * @param basicFileName the basic file naming structure for this PO
	 * @param rootDirectory the path to the PO directory for this quotation
	 * @return
	 */
	private String getNextPurchaseOrderFileName(String basicFileName, String rootDirectory)
	{
		String fileName = "";
		File rootDir = new File(rootDirectory);

		String[] poFiles = rootDir.list();
		if (poFiles != null)
		{
			// build an arraylist of files whoose name matches documents
			ArrayList<String> thisPOFiles = new ArrayList<String>();
			for (int i = 0; i < poFiles.length; i++)
			{
				String aPOFile = poFiles[i];
				if (aPOFile.contains(basicFileName))
				{
					thisPOFiles.add(aPOFile);
				}
			}

			if (thisPOFiles.size() > 0)
			{
				int rev = 0;
				// now perform a second pass to decide on the next sequence
				// number
				for (String poFile : thisPOFiles)
				{
					if (poFile.lastIndexOf('.') > -1)
					{
						// strip the doc extension
						String document = poFile.substring(0, poFile.length()
								- (poFile.length() - poFile.lastIndexOf('.')));

						// strip the basic document name from the document to
						// get the document rev number
						String revNo = document.replaceAll(basicFileName, "");

						if (revNo.matches("\\d*") && !revNo.trim().equals(""))
						{
							if (Integer.parseInt(revNo) > rev)
							{
								rev = Integer.parseInt(revNo);
							}
						}
					}
				}
				rev = rev + 1;
				fileName = basicFileName.concat(String.valueOf(rev));
			}
			else
			{
				fileName = basicFileName.concat(String.valueOf(1));
			}
		}
		else
		{
			fileName = basicFileName.concat(String.valueOf(1));
		}
		return fileName;
	}

}
