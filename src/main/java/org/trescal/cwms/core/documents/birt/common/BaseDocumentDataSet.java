package org.trescal.cwms.core.documents.birt.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.IUpdatableDataSetRow;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormatter;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.businesscompanysettings.BusinessCompanySettings;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.db.BusinessDocumentSettingsService;
import org.trescal.cwms.core.company.entity.company.Company;

/**
 * Provides common base information shared among different documents
 * Used in Trescal_A4.rptdesign (base document) and library created from base document
 * @author galen
 *
 */
public class BaseDocumentDataSet extends AbstractDataSet {
	// Allows initialization to occur on first call to beforeOpen() and not on repeated calls  
	private boolean initialized;
	// Parameters initialized in beforeOpen()
	private Integer entityId;
	private BaseDocumentType documentType;
	// Bean lookups
	private MessageSource messageSource;
	// Entities and values looked up in open();
	private Address sourceAddress;
	private Address destinationAddress;
	private BusinessCompanySettings businessCompanySettings;
	private BusinessDocumentSettings businessDocumentSettings;
	private Address legalRegistrationAddress;
	private String documentReference;
	private String documentTitle;
	private List<String> formattedSourceAddress;
	private List<String> formattedDestinationAddress;
	private List<String> formattedLegalAddress;
	// Single row in result for this data set - tracks whether first row available
	private boolean resultAvailable;
	
	private static final String CODE_TEL = "tel";
	private static final String MESSAGE_TEL = "Tel";
	private static final String CODE_FAX = "fax";
	private static final String MESSAGE_FAX = "Fax";
	
	public static final Logger logger = LoggerFactory.getLogger(BaseDocumentResolver.class);
	
	/**
	 * For performance reasons, we do as much as possible inside beforeOpen() for just the first initialization
	 * - beforeOpen() and open() may be called many times for one data set.
	 */
	@Override
	public void beforeOpen(IDataSetInstance dataSet, IReportContext reportContext) throws ScriptException {
		super.beforeOpen(dataSet, reportContext);
		if (!initialized) {
			initialized = true;
			entityId = (Integer) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
			documentType = (BaseDocumentType) reportContext.getParameterValue(BaseDocumentDefinitions.PARAMETER_DOCUMENT_TYPE);
			
			if (entityId == null) throw new ScriptException("Entity id not provided via key "+BaseDocumentDefinitions.PARAMETER_ENTITY_ID);
			if (documentType == null) throw new ScriptException("Document type not provided via key "+BaseDocumentDefinitions.PARAMETER_DOCUMENT_TYPE);
			
			Locale locale = super.getDocumentLocale();
			ApplicationContext ac = super.getApplicationContext();
			BaseDocumentResolver documentResolver = (BaseDocumentResolver) ac.getBean(BaseDocumentResolver.class);
			EntityDocumentDetails entityDocumentDetails = documentResolver.getComponent(documentType);
			AddressService addressService = (AddressService) ac.getBean(AddressService.class);
			AddressPrintFormatter addressFormatter = (AddressPrintFormatter) ac.getBean(AddressPrintFormatter.class);
			BusinessDocumentSettingsService documentSettingsService = (BusinessDocumentSettingsService) ac.getBean(BusinessDocumentSettingsService.class);
			messageSource = (MessageSource) ac.getBean(MessageSource.class);			
			
			// Entity lookups
			sourceAddress = entityDocumentDetails.getSourceAddress(entityId);
			destinationAddress = entityDocumentDetails.getDestinationAddress(entityId);
			Company businessCompany =  entityDocumentDetails.getBusinessCompany(entityId);
			businessCompanySettings = businessCompany.getBusinessSettings();
			businessDocumentSettings = documentSettingsService.getForCompanyOrDefault(businessCompany.getCoid());
			documentReference = entityDocumentDetails.getDocumentReference(entityId);
			documentTitle = entityDocumentDetails.getDocumentTitle(entityId, locale);
			List<Address> addresses = addressService.getAllCompanyAddresses(businessCompanySettings.getCompany().getCoid(), AddressType.LEGAL_REGISTRATION, true);
			legalRegistrationAddress = addresses.get(0);
			checkReferencesValid();
			
			// Address formatting
			formattedSourceAddress = addressFormatter.getAddressText(sourceAddress, locale, true);
			formattedDestinationAddress = addressFormatter.getAddressText(destinationAddress, locale, true);
			formattedLegalAddress = addressFormatter.getAddressText(legalRegistrationAddress, locale, false);
			
			// Add main telephone numbers to source address (if set)
			if (sourceAddress.getTelephone() != null && !sourceAddress.getTelephone().isEmpty()) {
				String telPrefix = this.messageSource.getMessage(CODE_TEL, null, MESSAGE_TEL, locale);
				formattedSourceAddress.add(telPrefix + " " + sourceAddress.getTelephone());
			}
			if (sourceAddress.getFax() != null && !sourceAddress.getFax().isEmpty()) {
				String faxPrefix = this.messageSource.getMessage(CODE_FAX, null, MESSAGE_FAX, locale);
				formattedSourceAddress.add(faxPrefix + " " + sourceAddress.getFax());
			}
		}
	}

	private void checkReferencesValid() {
		if (sourceAddress == null) logger.error("sourceAddress was null!");
		if (destinationAddress == null) logger.error("destinationAddress was null!");
		if (businessCompanySettings == null) logger.error("businessCompanySettings was null!");
		if (documentReference == null) logger.error("documentReference was null!");
		if (documentTitle == null) logger.error("documentTitle was null!");
		if (legalRegistrationAddress == null) logger.error("legalRegistrationAddress was null!");
	}
	
	@Override
	public void open(IDataSetInstance dataSet) throws ScriptException {
		resultAvailable= true;
	}
	
	@Override
	public boolean fetch(IDataSetInstance dataSet, IUpdatableDataSetRow row) throws ScriptException {
		boolean result = resultAvailable;
		if (resultAvailable) {
			// TODO consider moving addresses to lists in separate data sets

			// Source address = Trescal address that the document is coming from
			Iterator<String> sourceIterator = formattedSourceAddress.iterator();
			row.setColumnValue(BaseDocumentDefinitions.COL_SOURCE_ROW_1, sourceIterator.hasNext() ? sourceIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_SOURCE_ROW_2, sourceIterator.hasNext() ? sourceIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_SOURCE_ROW_3, sourceIterator.hasNext() ? sourceIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_SOURCE_ROW_4, sourceIterator.hasNext() ? sourceIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_SOURCE_ROW_5, sourceIterator.hasNext() ? sourceIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_SOURCE_ROW_6, sourceIterator.hasNext() ? sourceIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_SOURCE_ROW_7, sourceIterator.hasNext() ? sourceIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_SOURCE_ROW_8, sourceIterator.hasNext() ? sourceIterator.next() : null);
			
			// Recipient address = Destination (business,client,supplier) address that the document is generated for
			Iterator<String> destIterator = formattedDestinationAddress.iterator();
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ROW_1, destIterator.hasNext() ? destIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ROW_2, destIterator.hasNext() ? destIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ROW_3, destIterator.hasNext() ? destIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ROW_4, destIterator.hasNext() ? destIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ROW_5, destIterator.hasNext() ? destIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ROW_6, destIterator.hasNext() ? destIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ROW_7, destIterator.hasNext() ? destIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ROW_8, destIterator.hasNext() ? destIterator.next() : null);

			// Overall details
			row.setColumnValue(BaseDocumentDefinitions.COL_RECIPIENT_ON_LEFT, businessDocumentSettings.getRecipientAddressOnLeft());
			row.setColumnValue(BaseDocumentDefinitions.COL_DOCUMENT_REFERENCE, documentReference);
			row.setColumnValue(BaseDocumentDefinitions.COL_DOCUMENT_TITLE, documentTitle);
			
			// Business company details in footer
			List<String> leftFooter = new ArrayList<>();
			leftFooter.add(businessCompanySettings.getCompany().getConame());
			if (businessCompanySettings.getLegalRegistration1().trim().length() > 0)
				leftFooter.add(businessCompanySettings.getLegalRegistration1());
			if (businessCompanySettings.getLegalRegistration2().trim().length() > 0)
				leftFooter.add(businessCompanySettings.getLegalRegistration2());
			leftFooter.add(businessCompanySettings.getCompany().getFiscalIdentifier());
			Iterator<String> leftIterator = leftFooter.iterator();
			
			row.setColumnValue(BaseDocumentDefinitions.COL_FOOTER_LEFT_1, leftIterator.hasNext() ? leftIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_FOOTER_LEFT_2, leftIterator.hasNext() ? leftIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_FOOTER_LEFT_3, leftIterator.hasNext() ? leftIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_FOOTER_LEFT_4, leftIterator.hasNext() ? leftIterator.next() : null);
			
			// Note, the footer only supports 4 address lines - documents / configuration should be manually checked
			Iterator<String> legalIterator = formattedLegalAddress.iterator();
			row.setColumnValue(BaseDocumentDefinitions.COL_FOOTER_MID_1, legalIterator.hasNext() ? legalIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_FOOTER_MID_2, legalIterator.hasNext() ? legalIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_FOOTER_MID_3, legalIterator.hasNext() ? legalIterator.next() : null);
			row.setColumnValue(BaseDocumentDefinitions.COL_FOOTER_MID_4, legalIterator.hasNext() ? legalIterator.next() : null);
			
			resultAvailable = false;
		}
		return result;
	}
}
