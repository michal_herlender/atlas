package org.trescal.cwms.core.documents.birt.tpquoterequest;

import java.util.Collections;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db.TPQuoteRequestItemService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote.TPQuoteRequestItemNote;

public class TPQuoteReqIItemNotesDataset extends NoteDataSet<TPQuoteRequestItemNote> {

	private TPQuoteRequestItemService tpquoterequestItemService;
	private static Logger logger = LoggerFactory.getLogger(TPQuoteReqIItemNotesDataset.class);
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		tpquoterequestItemService = super.getApplicationContext().getBean(TPQuoteRequestItemService.class);
	}
	
	@Override
	public Iterator<TPQuoteRequestItemNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer itemid = super.getInputParameter(dataSet, "itemid");
		TPQuoteRequestItem tpquoteRequestItem = tpquoterequestItemService.findTPQuoteRequestItem(itemid);
		if (tpquoteRequestItem != null) {
			return super.getActivePublishedNotes(tpquoteRequestItem.getNotes()).iterator();	
		}
		else {
			logger.error("No TPQuoteRequestItem found for itemid : "+itemid);
			return Collections.emptyIterator();
		}
	}
}