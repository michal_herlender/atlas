package org.trescal.cwms.core.documents.birt.quotation;

import java.util.Collections;
import java.util.Iterator;

import org.eclipse.birt.report.engine.api.script.IReportContext;
import org.eclipse.birt.report.engine.api.script.ScriptException;
import org.eclipse.birt.report.engine.api.script.instance.IDataSetInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.documents.birt.common.NoteDataSet;
import org.trescal.cwms.core.documents.birt.tpquotation.TPQuotationItemsNoteDataSet;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;

public class QuotationItemNoteDataset extends NoteDataSet<QuoteItemNote> {
	private QuotationItemService quotationItemService;
	private static Logger logger = LoggerFactory.getLogger(TPQuotationItemsNoteDataSet.class);
	
	@Override
	public void init(IDataSetInstance dataSet, IReportContext reportContext) {
		quotationItemService = super.getApplicationContext().getBean(QuotationItemService.class);
	}
	@Override
	public Iterator<QuoteItemNote> getIterator(IDataSetInstance dataSet) throws ScriptException {
		Integer qoitemid = super.getInputParameter(dataSet, "qoitemid");
		Quotationitem quotationitem = quotationItemService.findQuotationItem(qoitemid);
		if (quotationitem != null) {
			return super.getActivePublishedNotes(quotationitem.getNotes()).iterator();
		}
		else {
			logger.error("No quotation Item found for qoitemid : "+qoitemid);
			return Collections.emptyIterator();
		}
	}
}
