package org.trescal.cwms.core.external.adveso.events.listeners;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.events.DeleteJobItemEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Component
@Slf4j
public class DeleteJobItemEventListener implements ApplicationListener<DeleteJobItemEvent> {

    private NotificationSystemModelService notificationSystemModelService;

    public DeleteJobItemEventListener(NotificationSystemModelService notificationSystemModelService) {
        this.notificationSystemModelService = notificationSystemModelService;
    }

    @Override
    public void onApplicationEvent(DeleteJobItemEvent event) {
        log.info("Capture delete job item event");
        JobItem jobItem = event.getJobItem();
        notificationSystemModelService.pushToQueue(DeleteJobItemEvent.ENTITY_CLASS, DeleteJobItemEvent.FIELD_NAME,
                DeleteJobItemEvent.OPERATION_TYPE, DeleteJobItemEvent.FIELD_TYPE, jobItem.getJobItemId(),
                jobItem.getInst(), null, jobItem.getJob().getCon());
    }
}