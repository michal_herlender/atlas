package org.trescal.cwms.core.external.adveso.dto;

import org.trescal.cwms.core.external.adveso.enums.AdvesoEventTypeEnum;

import lombok.Data;

@Data
public class AdvesoEventWrapperDTO {

	private Integer orderNo;
	private AdvesoActivityEventDTO activity;
	private AdvesoNotificationEventDTO notification;
	private AdvesoEventTypeEnum type;

	public AdvesoEventWrapperDTO(AdvesoActivityEventDTO activity) {
		this.activity = activity;
		this.type = AdvesoEventTypeEnum.ACTIVITY;
	}

	public AdvesoEventWrapperDTO(AdvesoNotificationEventDTO notification) {
		this.notification = notification;
		this.type = AdvesoEventTypeEnum.NOTIFICATION;
	}

	@Override
	public String toString() {
		return "AdvesoEventWrapperDTO [orderNo=" + orderNo + ", activity=" + activity + ", notification=" + notification
				+ ", type=" + type + "]";
	}

}
