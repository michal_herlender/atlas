package org.trescal.cwms.core.external.adveso.dto;

import java.util.List;

public class NotificationSystemClientModel {

	private NotificationSystemFieldDTO entityId;
	private List<NotificationSystemFieldDTO> parent;
	
	public NotificationSystemFieldDTO getEntityId() {
		return entityId;
	}
	
	public void setEntityId(NotificationSystemFieldDTO entityId) {
		this.entityId = entityId;
	}
	
	public List<NotificationSystemFieldDTO> getParent() {
		return parent;
	}

	public void setParent(List<NotificationSystemFieldDTO> parent) {
		this.parent = parent;
	}

	public NotificationSystemClientModel(NotificationSystemFieldDTO entityId, List<NotificationSystemFieldDTO> parent) {
		super();
		this.entityId = entityId;
		this.parent = parent;
	}
	
	public NotificationSystemClientModel() {
		super();
	}
	
}
