package org.trescal.cwms.core.external.portal.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.external.portal.Portal;
import org.trescal.cwms.core.external.portal.Portals;

public interface PortalDao extends BaseDao<Portal, Integer> {

	Portal getPortalByName(Portals portalName);
	
}
