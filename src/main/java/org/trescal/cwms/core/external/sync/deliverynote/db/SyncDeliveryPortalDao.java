package org.trescal.cwms.core.external.sync.deliverynote.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.external.portal.Portal;
import org.trescal.cwms.core.external.sync.SyncPortalDto;
import org.trescal.cwms.core.external.sync.deliverynote.SyncDeliveryPortal;

public interface SyncDeliveryPortalDao extends BaseDao<SyncDeliveryPortal, Integer> {

	public List<SyncPortalDto> getAllSyncDelivery(List<Integer> deliveries, Portal portal);


	void UpdateSyncPull(List<Integer> syncsId);

}
