package org.trescal.cwms.core.external.adveso.events;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class CertificateStatusChangedEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	public static final NotificationSystemEntityClassEnum ENTITY_CLASS = NotificationSystemEntityClassEnum.Certificate;
	public static final String FIELD_NAME = "status";
	public static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.String;

	private Certificate certificate;
	private JobItem jobItem;
	private NotificationSystemOperationTypeEnum operationType;
	private String newStatus;
	private String oldStatus;

	public CertificateStatusChangedEvent(Object source, JobItem jobItem, Certificate certificate,
			CertStatusEnum oldStatus, CertStatusEnum newStatus) {
		super(source);
		this.jobItem = jobItem;
		this.certificate = certificate;
		this.oldStatus = oldStatus != null ? oldStatus.name() : null;
		this.newStatus = newStatus.name();
		if (oldStatus == null)
			this.operationType = NotificationSystemOperationTypeEnum.CREATE;
		else
			this.operationType = NotificationSystemOperationTypeEnum.UPDATE;
	}

	public Certificate getCertificate() {
		return certificate;
	}

	public NotificationSystemOperationTypeEnum getOperationType() {
		return operationType;
	}

	public JobItem getJobItem() {
		return jobItem;
	}

	public String getOldStatus() {
		return oldStatus;
	}

	public String getNewStatus() {
		return newStatus;
	}

}
