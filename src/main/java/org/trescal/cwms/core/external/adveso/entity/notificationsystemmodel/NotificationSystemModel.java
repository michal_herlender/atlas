package org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import lombok.Setter;

@Entity
@Table(name = "notificationsystemqueue")
@Setter
public class NotificationSystemModel extends Auditable {

	private int id;
	private String entityIdFieldName;
	private NotificationSystemFieldTypeEnum entityIdFieldType;
	private String entityIdFieldValue;
	private NotificationSystemEntityClassEnum entityClass;
	private Instrument instrument;
	private NotificationSystemFieldChangeModel fieldChange;
	private NotificationSystemOperationTypeEnum operationType;
	private Boolean isSent;
	private Date sentOn;
	private Boolean isReceived;
	private Date receivedOn;
	private String lastErrorMessage;
	private Date lastErrorMessageOn;
	private JobItem jobItem;
	private Date createdOn;
	private Contact jobContact;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@Column(name = "issent")
	public Boolean getIsSent() {
		return isSent;
	}

	@Column(name = "senton")
	public Date getSentOn() {
		return sentOn;
	}

	@Column(name = "isreceived")
	public Boolean getIsReceived() {
		return isReceived;
	}

	@Column(name = "receivedon")
	public Date getReceivedOn() {
		return receivedOn;
	}

	@Column(name = "lasterrormessage")
	public String getLastErrorMessage() {
		return lastErrorMessage;
	}

	@Column(name = "lasterrormessageon")
	public Date getLastErrorMessageOn() {
		return lastErrorMessageOn;
	}

	@Column(name = "fieldname")
	public String getEntityIdFieldName() {
		return entityIdFieldName;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "fieldtype")
	public NotificationSystemFieldTypeEnum getEntityIdFieldType() {
		return entityIdFieldType;
	}

	@Column(name = "fieldvalue")
	public String getEntityIdFieldValue() {
		return entityIdFieldValue;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "entityclass")
	public NotificationSystemEntityClassEnum getEntityClass() {
		return entityClass;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "operationtype")
	public NotificationSystemOperationTypeEnum getOperationType() {
		return operationType;
	}

	// Never cascade changes to instrument, and always use lazy fetching on
	// @ManyToOne
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable = true)
	public Instrument getInstrument() {
		return instrument;
	}
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "notification")
	public NotificationSystemFieldChangeModel getFieldChange() {
		return fieldChange;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = true)
	public JobItem getJobItem() {
		return jobItem;
	}

	@Column(name = "createdon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedOn() {
		return createdOn;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcontactid", nullable = true)
	public Contact getJobContact() {
		return jobContact;
	}

}
