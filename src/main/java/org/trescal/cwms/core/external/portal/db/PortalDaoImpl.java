package org.trescal.cwms.core.external.portal.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.external.portal.Portal;
import org.trescal.cwms.core.external.portal.Portals;

@Repository
public class PortalDaoImpl extends BaseDaoImpl<Portal, Integer> implements PortalDao {

	@Override
	protected Class<Portal> getEntity() {
		return Portal.class;
	}

	@Override
	public Portal getPortalByName(Portals portalName) {
		return getSingleResult(cb -> {
			CriteriaQuery<Portal> cq = cb.createQuery(Portal.class);
			Root<Portal> portalRoot = cq.from(Portal.class);	
			cq.where(cb.equal(portalRoot.get("portalName"),portalName));
			return cq;
		});
	}
}
