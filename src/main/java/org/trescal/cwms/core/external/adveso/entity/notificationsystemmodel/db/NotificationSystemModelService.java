package org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.external.adveso.dto.AdvesoNotificationEventDTO;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.external.adveso.form.SearchNotificationForm;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public interface NotificationSystemModelService extends BaseService<NotificationSystemModel, Integer> {

	public List<NotificationSystemModel> getNeverSent();

	public boolean itemExist(NotificationSystemModel item);

	public void getNotificationsToResent(SearchNotificationForm form);

	public JobItem getNotificationJobItem(Integer notificationId);

	NotificationSystemModel pushToQueue(NotificationSystemEntityClassEnum entityClass, String fieldName,
			NotificationSystemOperationTypeEnum operationType, NotificationSystemFieldTypeEnum fieldType,
			Integer entityId, Instrument instrument, JobItem jobItem, Contact jobContact);

	/**
	 * 
	 * @return count of deleted notifications
	 */
	public Integer deleteSentAdvesoNotificationsSystem();

	public List<NotificationSystemModel> getNotificationSystemModelbyId(List<Integer> idsnotif);

	public void updateAfterSending(boolean success, Date date, String errorMessage,
			List<Integer> advesoNotificationIds);

	public List<NotificationSystemModel> getByIds(List<Integer> notificationIds);

	public List<AdvesoNotificationEventDTO> getNeverSentDTOs();

}
