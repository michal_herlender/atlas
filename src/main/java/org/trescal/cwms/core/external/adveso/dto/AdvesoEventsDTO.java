package org.trescal.cwms.core.external.adveso.dto;

import java.util.List;

import lombok.Data;

@Data
public class AdvesoEventsDTO {

	// Token used by Shared Services to identify service providers
	private final static String AK = "216FA9C8-9267-476C-A93D-E5BBA9FA75C0";

	private String accessKey;
	private List<AdvesoEventWrapperDTO> events;

	public AdvesoEventsDTO(List<AdvesoEventWrapperDTO> events) {
		this.events = events;
		this.accessKey = AK;
	}

}
