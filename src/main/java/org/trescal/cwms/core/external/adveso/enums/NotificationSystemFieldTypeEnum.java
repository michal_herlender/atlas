/**
 * 
 */
package org.trescal.cwms.core.external.adveso.enums;

import java.io.Serializable;

/**
 * @author m.amnay
 */
public enum NotificationSystemFieldTypeEnum implements Serializable {
	String, Integer, Long, Boolean, Date;
}