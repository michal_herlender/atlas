package org.trescal.cwms.core.external.sync.deliverynote;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.external.portal.Portal;

@Entity
@Table(name = "syncdeliveryportal")
public class SyncDeliveryPortal {
	
	private int syncDeliveryId;
	
	private JobDelivery delivery;
	
	private Date lastPull;
	
	private Portal portal;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "syncDeliveryId", nullable = false, unique = true)
	public int getSyncDeliveryId() {
		return syncDeliveryId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="deliveryId", nullable=false, foreignKey=@ForeignKey(name="FK_DeliverySync"))
	public JobDelivery getDelivery() {
		return delivery;
	}
	
	@Column(name = "lastPull", nullable=false, columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastPull() {
		return lastPull;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="portalId", nullable=false, foreignKey=@ForeignKey(name="FK_DeliveryPortalSync"))
	public Portal getPortal() {
		return portal;
	}
	
	public void setDelivery(JobDelivery delivery) {
		this.delivery = delivery;
	}

	public void setPortal(Portal portal) {
		this.portal = portal;
	}

	public void setLastPull(Date lastPull) {
		this.lastPull = lastPull;
	}

	public void setSyncDeliveryId(int syncDeliveryId) {
		this.syncDeliveryId = syncDeliveryId;
	}
}
