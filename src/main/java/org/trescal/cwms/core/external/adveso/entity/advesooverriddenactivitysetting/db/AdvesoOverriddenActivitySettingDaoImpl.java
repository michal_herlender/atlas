package org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting_;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity_;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity_;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

@Repository
public class AdvesoOverriddenActivitySettingDaoImpl extends BaseDaoImpl<AdvesoOverriddenActivitySetting, Integer>
		implements AdvesoOverriddenActivitySettingDao {

	@Override
	protected Class<AdvesoOverriddenActivitySetting> getEntity() {
		return AdvesoOverriddenActivitySetting.class;
	}

	@Override
	public List<AdvesoOverriddenActivitySetting> getAdvesoOverriddenActivitiesSettingBySubdiv(Integer subdivid) {
		
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AdvesoOverriddenActivitySetting> query = builder.createQuery(AdvesoOverriddenActivitySetting.class);
        Root<AdvesoOverriddenActivitySetting> aoasRoot =  query.from(AdvesoOverriddenActivitySetting.class);
        Join<AdvesoOverriddenActivitySetting, Subdiv> subdivJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.subdiv);
       	
		Predicate conjunction = builder.conjunction();
        conjunction.getExpressions().add(builder.equal(subdivJoin.get(Subdiv_.subdivid),subdivid));
		
        query.where(conjunction);

        query.select(aoasRoot);

        return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public List<AdvesoActivityDTO> getNotTransferableSubdivLevelActivitiesBySubdiv(Integer subdivid) {
		
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AdvesoActivityDTO> query = builder.createQuery(AdvesoActivityDTO.class);
        
        Root<AdvesoOverriddenActivitySetting> aoasRoot =  query.from(AdvesoOverriddenActivitySetting.class);
        Join<AdvesoOverriddenActivitySetting, Subdiv> subdivJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.subdiv);
        Join<AdvesoOverriddenActivitySetting, AdvesoTransferableActivity> ataJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.advesoTransferableActivity);
        Join<AdvesoTransferableActivity, ItemActivity> itemActivityJoin = ataJoin.join(AdvesoTransferableActivity_.itemActivity);
       	
		Predicate conjunction = builder.conjunction();
        conjunction.getExpressions().add(builder.equal(subdivJoin.get(Subdiv_.subdivid),subdivid));
        conjunction.getExpressions().add(builder.equal(itemActivityJoin.get(ItemActivity_.active),true));
        conjunction.getExpressions().add(builder.equal(aoasRoot.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso),false));
		
        query.where(conjunction);

        query.select(builder.construct(AdvesoActivityDTO.class, 
        		itemActivityJoin.get(ItemActivity_.stateid.getName()),
        		itemActivityJoin.get(ItemActivity_.description.getName()),
        		aoasRoot.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso.getName()),
        		aoasRoot.get(AdvesoOverriddenActivitySetting_.isDisplayedOnJobOrderDetails.getName()),
        		subdivJoin.get(Subdiv_.subdivid.getName())
 				));

        return getEntityManager().createQuery(query).getResultList();
	}
	
	@Override
	public List<AdvesoActivityDTO> getTransferableSubdivLevelActivitiesBySubdiv(Integer subdivid) {
		
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AdvesoActivityDTO> query = builder.createQuery(AdvesoActivityDTO.class);
        
        Root<AdvesoOverriddenActivitySetting> aoasRoot =  query.from(AdvesoOverriddenActivitySetting.class);
        Join<AdvesoOverriddenActivitySetting, Subdiv> subdivJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.subdiv);
        Join<AdvesoOverriddenActivitySetting, AdvesoTransferableActivity> ataJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.advesoTransferableActivity);
        Join<AdvesoTransferableActivity, ItemActivity> itemActivityJoin = ataJoin.join(AdvesoTransferableActivity_.itemActivity);
       	
		Predicate conjunction = builder.conjunction();
        conjunction.getExpressions().add(builder.equal(subdivJoin.get(Subdiv_.subdivid),subdivid));
        conjunction.getExpressions().add(builder.equal(itemActivityJoin.get(ItemActivity_.active),true));
        conjunction.getExpressions().add(builder.equal(aoasRoot.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso),true));
		
        query.where(conjunction);

        query.select(builder.construct(AdvesoActivityDTO.class, 
        		itemActivityJoin.get(ItemActivity_.stateid.getName()),
        		itemActivityJoin.get(ItemActivity_.description.getName()),
        		aoasRoot.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso.getName()),
        		aoasRoot.get(AdvesoOverriddenActivitySetting_.isDisplayedOnJobOrderDetails.getName()),
        		subdivJoin.get(Subdiv_.subdivid.getName())
 				));

        return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public List<AdvesoActivityDTO> getNotDisplayedSubdivLevelActivitiesBySubdiv(Integer subdivid) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AdvesoActivityDTO> query = builder.createQuery(AdvesoActivityDTO.class);
        
        Root<AdvesoOverriddenActivitySetting> aoasRoot =  query.from(AdvesoOverriddenActivitySetting.class);
        Join<AdvesoOverriddenActivitySetting, Subdiv> subdivJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.subdiv);
        Join<AdvesoOverriddenActivitySetting, AdvesoTransferableActivity> ataJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.advesoTransferableActivity);
        Join<AdvesoTransferableActivity, ItemActivity> itemActivityJoin = ataJoin.join(AdvesoTransferableActivity_.itemActivity);
       	
		Predicate conjunction = builder.conjunction();
        conjunction.getExpressions().add(builder.equal(subdivJoin.get(Subdiv_.subdivid),subdivid));
        conjunction.getExpressions().add(builder.equal(itemActivityJoin.get(ItemActivity_.active),true));
        conjunction.getExpressions().add(builder.equal(aoasRoot.get(AdvesoOverriddenActivitySetting_.isDisplayedOnJobOrderDetails),false));
		
        query.where(conjunction);

        query.select(builder.construct(AdvesoActivityDTO.class, 
        		itemActivityJoin.get(ItemActivity_.stateid.getName()),
        		itemActivityJoin.get(ItemActivity_.description.getName()),
        		aoasRoot.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso.getName()),
        		aoasRoot.get(AdvesoOverriddenActivitySetting_.isDisplayedOnJobOrderDetails.getName()),
        		subdivJoin.get(Subdiv_.subdivid.getName())
 				));

        return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public List<AdvesoActivityDTO> getDisplayedSubdivLevelActivitiesBySubdiv(Integer subdivid) {
		
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AdvesoActivityDTO> query = builder.createQuery(AdvesoActivityDTO.class);
        
        Root<AdvesoOverriddenActivitySetting> aoasRoot =  query.from(AdvesoOverriddenActivitySetting.class);
        Join<AdvesoOverriddenActivitySetting, Subdiv> subdivJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.subdiv);
        Join<AdvesoOverriddenActivitySetting, AdvesoTransferableActivity> ataJoin = aoasRoot.join(AdvesoOverriddenActivitySetting_.advesoTransferableActivity);
        Join<AdvesoTransferableActivity, ItemActivity> itemActivityJoin = ataJoin.join(AdvesoTransferableActivity_.itemActivity);
       	
		Predicate conjunction = builder.conjunction();
        conjunction.getExpressions().add(builder.equal(subdivJoin.get(Subdiv_.subdivid),subdivid));
        conjunction.getExpressions().add(builder.equal(itemActivityJoin.get(ItemActivity_.active),true));
        conjunction.getExpressions().add(builder.equal(aoasRoot.get(AdvesoOverriddenActivitySetting_.isDisplayedOnJobOrderDetails),true));
		
        query.where(conjunction);

        query.select(builder.construct(AdvesoActivityDTO.class, 
        		itemActivityJoin.get(ItemActivity_.stateid.getName()),
        		itemActivityJoin.get(ItemActivity_.description.getName()),
        		aoasRoot.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso.getName()),
        		aoasRoot.get(AdvesoOverriddenActivitySetting_.isDisplayedOnJobOrderDetails.getName()),
        		subdivJoin.get(Subdiv_.subdivid.getName())
 				));

        return getEntityManager().createQuery(query).getResultList();
	}

}
