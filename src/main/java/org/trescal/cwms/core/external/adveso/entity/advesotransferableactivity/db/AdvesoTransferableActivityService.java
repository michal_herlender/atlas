package org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db;


import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

public interface AdvesoTransferableActivityService extends BaseService<AdvesoTransferableActivity, Integer> {

	public List<AdvesoActivityDTO> getDefaultActivities();
	public List<AdvesoTransferableActivity> getAllFromCache();
	
}
