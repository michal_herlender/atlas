package org.trescal.cwms.core.external.adveso.events;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class UploadCertificateEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;
	
	public static final NotificationSystemEntityClassEnum ENTITY_CLASS = NotificationSystemEntityClassEnum.Certificate;
	public static final String FIELD_NAME = "certid";
	public static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.Integer; 
	
	private Certificate certificate;
	private JobItem jobItem;
	private NotificationSystemOperationTypeEnum operationType;

	public UploadCertificateEvent(Object source, Certificate certificate, boolean isReplaced, JobItem jobItem) {
		super(source);
		this.certificate = certificate;
		if(isReplaced)
			this.operationType = NotificationSystemOperationTypeEnum.CERTIFICATE_REPLACE;
		else
			this.operationType = NotificationSystemOperationTypeEnum.CERTIFICATE_UPLOAD;
		this.jobItem = jobItem;
	}

	public Certificate getCertificate() {
		return certificate;
	}

	public NotificationSystemOperationTypeEnum getOperationType() {
		return operationType;
	}

	public JobItem getJobItem() {
		return jobItem;
	}

}
