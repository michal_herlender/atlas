package org.trescal.cwms.core.external.adveso.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.text.StringEscapeUtils;

import java.util.Date;

@Getter @Setter
public class AdvesoJobItemActivityDTO {

	private int id;
	private Integer itemNo;
	private String jobno;
	private String coname;
	private Integer plantid;
	private String plantno;
	private String activityType;
	private Date activityDate;
	private Date sentOn;
	private Date errorDate;
	private String lastErrorMessage;

	public AdvesoJobItemActivityDTO(int id, Integer itemNo, String jobno, String coname, Integer plantid,
			String plantno, String activityType, Date activityDate, Date sentOn, Date errorDate, String lastErrorMessage) {
		super();
		this.id = id;
		this.itemNo = itemNo;
		this.jobno = jobno;
		this.coname = coname;
		this.plantid = plantid;
		this.plantno = plantno;
		this.activityType = activityType;
		this.activityDate = activityDate;
		this.sentOn = sentOn;
		this.errorDate = errorDate;
		this.lastErrorMessage = StringEscapeUtils.escapeHtml4(lastErrorMessage);
	}

}
