package org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;

@Entity
@Table(name = "advesotransferableactivity")
public class AdvesoTransferableActivity extends Versioned {

	private int id;
	private Boolean isTransferredToAdveso;
	private Boolean isDisplayedOnJobOrderDetails;
	private ItemActivity itemActivity;
	private List<AdvesoOverriddenActivitySetting> advesoOverriddenActivitiesSetting;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "itemactivityid", foreignKey=@ForeignKey(name="FK_advesotransferable_itemactivityid"))
	public ItemActivity getItemActivity() {
		return itemActivity;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "advesoTransferableActivity", fetch = FetchType.LAZY, orphanRemoval = true)
	public List<AdvesoOverriddenActivitySetting> getAdvesoOverriddenActivitiesSetting() {
		return advesoOverriddenActivitiesSetting;
	}

	public Boolean getIsTransferredToAdveso() {
		return isTransferredToAdveso;
	}

	public void setIsTransferredToAdveso(Boolean isTransferredToAdveso) {
		this.isTransferredToAdveso = isTransferredToAdveso;
	}

	public Boolean getIsDisplayedOnJobOrderDetails() {
		return isDisplayedOnJobOrderDetails;
	}

	public void setIsDisplayedOnJobOrderDetails(Boolean isDisplayedOnJobOrderDetails) {
		this.isDisplayedOnJobOrderDetails = isDisplayedOnJobOrderDetails;
	}

	public void setItemActivity(ItemActivity itemActivity) {
		this.itemActivity = itemActivity;
	}

	public void setAdvesoOverriddenActivitiesSetting(
			List<AdvesoOverriddenActivitySetting> advesoOverriddenActivitiesSetting) {
		this.advesoOverriddenActivitiesSetting = advesoOverriddenActivitiesSetting;
	}

	@Override
	public boolean equals(Object arg0) {
		return (arg0 instanceof AdvesoTransferableActivity && ((AdvesoTransferableActivity) arg0).id == this.id);
	}

}
