package org.trescal.cwms.core.external.adveso.events;

import java.util.Date;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;

public class CompleteCalibrationEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	public static final NotificationSystemEntityClassEnum ENTITY_CLASS = NotificationSystemEntityClassEnum.Calibration;
	public static final String FIELD_NAME = "id";
	public static final NotificationSystemOperationTypeEnum OPERATION_TYPE = NotificationSystemOperationTypeEnum.COMPLETE_CALIBRATION;
	public static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.Integer;

	private Calibration calibration;
	private NotificationSystemFieldChangeModel nsfcm;

	public CompleteCalibrationEvent(Object source, Calibration calibration, CalibrationStatus oldStatus) {
		super(source);
		this.calibration = calibration;
		this.nsfcm = new NotificationSystemFieldChangeModel(Calibration_.status.getName(),
				NotificationSystemFieldTypeEnum.String, oldStatus.getName(), calibration.getStatus().getName(),
				new Date());
	}

	public Calibration getCalibration() {
		return calibration;
	}

	public NotificationSystemFieldChangeModel getNsfcm() {
		return nsfcm;
	}
}
