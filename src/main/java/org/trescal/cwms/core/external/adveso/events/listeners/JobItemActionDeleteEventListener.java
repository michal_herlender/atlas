package org.trescal.cwms.core.external.adveso.events.listeners;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.external.adveso.events.DeleteJobItemActionEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;

@Component
@Slf4j
public class JobItemActionDeleteEventListener implements ApplicationListener<DeleteJobItemActionEvent> {

	private AdvesoJobItemActivityService advesoJobItemActivityService;

	public JobItemActionDeleteEventListener(AdvesoJobItemActivityService advesoJobItemActivityService) {
		this.advesoJobItemActivityService = advesoJobItemActivityService;
	}

	@Override
	public void onApplicationEvent(DeleteJobItemActionEvent event) {
		log.info("Capture delete job item action event");
		JobItemAction jobItemAction = event.getJobItemAction();
		advesoJobItemActivityService.deleteFromQueueIfJobItemActionIsTransferable(jobItemAction);
	}
}