package org.trescal.cwms.core.external.adveso.dto;

import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;

public class NotificationSystemFieldChangeDTO {

	private String fieldName;
	
	private NotificationSystemFieldTypeEnum fieldType;
	
	private String oldFieldValue;

	private String newFieldValue;
	
	private String comment;
	

	public String getOldFieldValue() {
		return oldFieldValue;
	}

	public void setOldFieldValue(String oldFieldValue) {
		this.oldFieldValue = oldFieldValue;
	}
	
	public String getNewFieldValue() {
		return newFieldValue;
	}

	public void setNewFieldValue(String newFieldValue) {
		this.newFieldValue = newFieldValue;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public NotificationSystemFieldTypeEnum getFieldType() {
		return fieldType;
	}

	public void setFieldType(NotificationSystemFieldTypeEnum fieldType) {
		this.fieldType = fieldType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public NotificationSystemFieldChangeDTO(NotificationSystemFieldChangeModel model) {
		super();
		this.fieldName = model.getFieldName();
		this.fieldType = model.getFieldType();
		this.oldFieldValue = model.getOldFieldValue();
		this.newFieldValue = model.getNewFieldValue();
	}

	public NotificationSystemFieldChangeDTO() {
		super();
	}

}
