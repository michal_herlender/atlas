package org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.external.adveso.dto.AdvesoNotificationDTO;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel_;
import org.trescal.cwms.core.external.adveso.enums.EventStatusSearchForm;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.external.adveso.form.SearchNotificationForm;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.tools.CriteriaApiTools;

@Repository
public class NotificationSystemModelDaoImpl extends BaseDaoImpl<NotificationSystemModel, Integer>
		implements NotificationSystemModelDao {

	@Override
	protected Class<NotificationSystemModel> getEntity() {
		return NotificationSystemModel.class;
	}

	@Override
	public List<NotificationSystemModel> getNeverSent(int maxNotifications) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<NotificationSystemModel> query = cb.createQuery(NotificationSystemModel.class);

		Root<NotificationSystemModel> nsmRoot = query.from(NotificationSystemModel.class);

		Predicate conjunction = cb.conjunction();
		conjunction.getExpressions().add(cb.or(cb.isNull(nsmRoot.get(NotificationSystemModel_.isSent)),
				cb.equal(nsmRoot.get(NotificationSystemModel_.isSent), false)));

		query.select(nsmRoot);
		query.where(conjunction);
		query.orderBy(cb.asc(nsmRoot.get(NotificationSystemModel_.id)));

		return getEntityManager().createQuery(query).setMaxResults(maxNotifications).getResultList();
	}

	@Override
	public boolean itemExist(NotificationSystemModel item) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<NotificationSystemModel> query = builder.createQuery(NotificationSystemModel.class);

		Root<NotificationSystemModel> nsmRoot = query.from(NotificationSystemModel.class);

		Predicate conjunction = builder.conjunction();

		conjunction.getExpressions()
				.add(builder.equal(nsmRoot.get(NotificationSystemModel_.entityClass.getName()), item.getEntityClass()));
		conjunction.getExpressions().add(builder
				.equal(nsmRoot.get(NotificationSystemModel_.entityIdFieldName.getName()), item.getEntityIdFieldName()));
		conjunction.getExpressions().add(builder.equal(
				nsmRoot.get(NotificationSystemModel_.entityIdFieldValue.getName()), item.getEntityIdFieldValue()));
		conjunction.getExpressions().add(
				builder.equal(nsmRoot.get(NotificationSystemModel_.operationType.getName()), item.getOperationType()));

		query.where(conjunction);

		query.select(nsmRoot);

		return getEntityManager().createQuery(query).getResultList().size() > 0;
	}

	@Override
	public void getNotificationsToResent(SearchNotificationForm form) {

		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AdvesoNotificationDTO> query = builder.createQuery(AdvesoNotificationDTO.class);

		Root<NotificationSystemModel> nsRoot = query.from(NotificationSystemModel.class);
		nsRoot.alias("nsRoot");

		Join<NotificationSystemModel, Instrument> instJoin = nsRoot.join(NotificationSystemModel_.instrument,
				JoinType.LEFT);
		instJoin.alias("instJoin");

		Join<NotificationSystemModel, JobItem> jiJoin = nsRoot.join(NotificationSystemModel_.jobItem, JoinType.LEFT);
		jiJoin.alias("jiJoin");

		Join<JobItem, Job> jJoin = jiJoin.join(JobItem_.job, JoinType.LEFT);
		jJoin.alias("jJoin");

		Predicate conjunction = builder.conjunction();

		if (form.getStatus() != null && form.getStatus().equals(EventStatusSearchForm.PENDING.name())) {
			conjunction.getExpressions()
					.add(builder.equal(nsRoot.get(NotificationSystemModel_.isSent.getName()), false));
			conjunction.getExpressions()
					.add(builder.isNull(nsRoot.get(NotificationSystemModel_.lastErrorMessage.getName())));
		} else if (form.getStatus() != null && form.getStatus().equals(EventStatusSearchForm.SUCCESS.name())) {
			conjunction.getExpressions()
					.add(builder.equal(nsRoot.get(NotificationSystemModel_.isSent.getName()), true));
		} else if (form.getStatus() != null && form.getStatus().equals(EventStatusSearchForm.ERROR.name())) {
			conjunction.getExpressions()
					.add(builder.isNotNull(nsRoot.get(NotificationSystemModel_.lastErrorMessage.getName())));
			conjunction.getExpressions()
					.add(builder.equal(nsRoot.get(NotificationSystemModel_.isSent.getName()), false));
		}

		if (form.getEntityClass() != null && !form.getEntityClass().isEmpty()) {
			conjunction.getExpressions().add(builder.equal(nsRoot.get(NotificationSystemModel_.entityClass.getName()),
					NotificationSystemEntityClassEnum.valueOf(form.getEntityClass())));
		}

		if (form.getOperationType() != null && !form.getOperationType().isEmpty()) {
			conjunction.getExpressions().add(builder.equal(nsRoot.get(NotificationSystemModel_.operationType.getName()),
					NotificationSystemOperationTypeEnum.valueOf(form.getOperationType())));
		}

		query.where(conjunction);

		// count results
		Long count = CriteriaApiTools.count(getEntityManager(), query, nsRoot);
		form.getRs().setResultsCount(count.intValue());// count.intValue());

		// get results
		query.select(builder.construct(AdvesoNotificationDTO.class, nsRoot.get(NotificationSystemModel_.id.getName()),
				nsRoot.get(NotificationSystemModel_.entityIdFieldName),
				nsRoot.get(NotificationSystemModel_.entityIdFieldValue),
				nsRoot.get(NotificationSystemModel_.entityClass),
				nsRoot.get(NotificationSystemModel_.operationType),
				nsRoot.get(NotificationSystemModel_.isSent),
				nsRoot.get(NotificationSystemModel_.lastErrorMessage),
				nsRoot.get(NotificationSystemModel_.createdOn),
				instJoin.get(Instrument_.plantid),
				jiJoin.get(JobItem_.jobItemId),
				jiJoin.get(JobItem_.itemNo),
				jJoin.get(Job_.jobno),
				nsRoot.get(NotificationSystemModel_.sentOn)));

		query.orderBy(builder.desc(nsRoot.get(NotificationSystemModel_.id)));

		TypedQuery<AdvesoNotificationDTO> typedQuery = getEntityManager().createQuery(query);
		typedQuery.setFirstResult(form.getRs().getStartResultsFrom());
		typedQuery.setMaxResults(form.getRs().getResultsPerPage());

		form.getRs().setResults(typedQuery.getResultList());
	}

	@Override
	public Integer deleteSentAdvesoNotificationsSystem() {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -1);
		Date oneYearBeforeCurrentDate = cal.getTime();

		CriteriaDelete<NotificationSystemModel> delete = builder.createCriteriaDelete(NotificationSystemModel.class);
		Root<NotificationSystemModel> root = delete.from(NotificationSystemModel.class);
		delete.where(builder.and(builder.equal(root.get(NotificationSystemModel_.isSent), true),
				builder.lessThan(root.get(NotificationSystemModel_.sentOn), oneYearBeforeCurrentDate)));

		Query query = getEntityManager().createQuery(delete);
		return query.executeUpdate();
	}

	@Override
	public List<NotificationSystemModel> getNotificationSystemModelbyId(List<Integer> idsnotif) {
		List<NotificationSystemModel> result = null;
		if (idsnotif == null || idsnotif.isEmpty()) {
			// Query won't work (throws SQL exception) if empty ids list passed; result is
			// implicitly empty
			result = Collections.emptyList();
		} else {
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<NotificationSystemModel> query = builder.createQuery(NotificationSystemModel.class);

			Root<NotificationSystemModel> nsmRoot = query.from(NotificationSystemModel.class);

			query.select(nsmRoot).where(nsmRoot.get(NotificationSystemModel_.id).in(idsnotif));

			result = getEntityManager().createQuery(query).getResultList();
		}
		return result;
	}

	@Override
	public void updateAfterSending(boolean success, Date date, String errorMessage,
			List<Integer> advesoNotificationIds) {

		CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
		CriteriaUpdate<NotificationSystemModel> cu = cb.createCriteriaUpdate(NotificationSystemModel.class);
		cu.from(NotificationSystemModel.class);
		cu.set(NotificationSystemModel_.isSent, success);
		cu.set(NotificationSystemModel_.lastErrorMessage, errorMessage);
		if (success)
			cu.set(NotificationSystemModel_.sentOn, date);
		else
			cu.set(NotificationSystemModel_.lastErrorMessageOn, date);
		cu.where(cu.getRoot().get(NotificationSystemModel_.id).in(advesoNotificationIds));

		this.getEntityManager().createQuery(cu).executeUpdate();

	}

	@Override
	public List<NotificationSystemModel> getByIds(List<Integer> notificationIds) {
		return getResultList(cb -> {
			CriteriaQuery<NotificationSystemModel> cq = cb.createQuery(NotificationSystemModel.class);
			Root<NotificationSystemModel> root = cq.from(NotificationSystemModel.class);
			cq.where(root.get(NotificationSystemModel_.id).in(notificationIds));
			return cq;
		});
	}

}
