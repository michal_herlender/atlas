/**
 * 
 */
package org.trescal.cwms.core.external.adveso.enums;

import java.io.Serializable;

/**
 * @author m.amnay
 */
public enum NotificationSystemOperationTypeEnum implements Serializable {
	CREATE, UPDATE, DELETE, CALL_OFF, COMPLETE, CERTIFICATE_UPLOAD, CERTIFICATE_REPLACE, COMPLETE_CALIBRATION;
}