package org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.external.adveso.dto.AdvesoJobItemActivityDTO;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.form.SearchEventForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface AdvesoJobItemActivityDao extends BaseDao<AdvesoJobItemActivity, Integer>{
	
	public List<AdvesoJobItemActivity> getAdvesoJobItemActivitiesByActivityId(JobItemAction jia);

	public void findActivities(SearchEventForm form, PagedResultSet<AdvesoJobItemActivityDTO> prs);
	
	public Integer deleteSentJobItemActivities();

	List<AdvesoJobItemActivity> getNeverSentActivities(int maxActivities);
	
	AdvesoJobItemActivity getCertificateAdvesoJobItemActivityByJIAction(JobItemAction jia);

	public void updateAfterSending(boolean success, Date date, String errorMessage, List<Integer> advesoActivityIds);

	public List<AdvesoJobItemActivity> getByIds(List<Integer> activitiyIds);
}
