package org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.external.adveso.dto.AdvesoActivityEventDTO;
import org.trescal.cwms.core.external.adveso.dto.AdvesoJobItemActivityDTO;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.form.SearchEventForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

public interface AdvesoJobItemActivityService extends BaseService<AdvesoJobItemActivity, Integer> {

	public List<AdvesoJobItemActivity> getAdvesoJobItemActivitiesByActivityId(JobItemAction jia);

	public void findActivities(SearchEventForm form, PagedResultSet<AdvesoJobItemActivityDTO> prs) throws ParseException;

	public List<AdvesoJobItemActivity> getNeverSentActivities();

	public Integer deleteSentAdvesoJobItemActivities();

	boolean isTransferableToAdveso(JobItemAction jobItemAction);

	void pushToQueue(JobItemAction jobItemAction);

	void pushToQueueIfJobItemActionIsTransferable(JobItemAction jobItemAction);

	void deleteFromQueueIfJobItemActionIsTransferable(JobItemAction jobItemAction);
	
    List<AdvesoActivityDTO> getSubdivActivities(Integer subdivid);
    
    AdvesoJobItemActivity getCertificateAdvesoJobItemActivityByJIAction(JobItemAction jia);

	public void updateAfterSending(boolean success, Date date, String errorMessage, List<Integer> advesoActivityIds);

	public List<AdvesoJobItemActivity> getByIds(List<Integer> activitiyIds);

	public List<AdvesoActivityEventDTO> getNeverSentActivitiesDTOs();
	
	void generateAdvesoActivities();
}
