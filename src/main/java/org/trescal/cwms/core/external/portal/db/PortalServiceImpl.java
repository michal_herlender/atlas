package org.trescal.cwms.core.external.portal.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.external.portal.Portal;
import org.trescal.cwms.core.external.portal.Portals;

@Service
public class PortalServiceImpl extends BaseServiceImpl<Portal, Integer> implements PortalService {

	@Autowired
	PortalDao portalDao;
	
	@Override
	protected BaseDao<Portal, Integer> getBaseDao() {
		return portalDao;
	}

	@Override
	public Portal get(Portals portal) {
		return portalDao.getPortalByName(portal);
	}

}
