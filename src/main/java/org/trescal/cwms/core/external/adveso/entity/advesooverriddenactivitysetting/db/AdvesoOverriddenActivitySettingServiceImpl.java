package org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.db;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

@Service
public class AdvesoOverriddenActivitySettingServiceImpl extends BaseServiceImpl<AdvesoOverriddenActivitySetting, Integer>
		implements AdvesoOverriddenActivitySettingService {

	@Autowired
	private AdvesoOverriddenActivitySettingDao dao;

	@Override
	protected BaseDao<AdvesoOverriddenActivitySetting, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public List<AdvesoOverriddenActivitySetting> getAdvesoOverriddenActivitiesSettingBySubdiv(Integer subdivid) {
		return dao.getAdvesoOverriddenActivitiesSettingBySubdiv(subdivid);
	}

	@Override
	public List<AdvesoActivityDTO> getNotTransferableSubdivLevelActivitiesBySubdiv(Integer subdivid) {
		return dao.getNotTransferableSubdivLevelActivitiesBySubdiv(subdivid);
	}

	@Override
	public List<AdvesoActivityDTO> getTransferableSubdivLevelActivitiesBySubdiv(Integer subdivid) {
		return dao.getTransferableSubdivLevelActivitiesBySubdiv(subdivid);
	}

	@Override
	public List<AdvesoActivityDTO> getNotDisplayedSubdivLevelActivitiesBySubdiv(Integer subdivid) {
		return dao.getNotDisplayedSubdivLevelActivitiesBySubdiv(subdivid);
	}

	@Override
	public List<AdvesoActivityDTO> getDisplayedSubdivLevelActivitiesBySubdiv(Integer subdivid) {
		return dao.getDisplayedSubdivLevelActivitiesBySubdiv(subdivid);
	}
	
	@Override
	public List<AdvesoOverriddenActivitySetting> mergeSubdivOverriddenActivities(List<AdvesoTransferableActivity> generalTransferableActivities, Integer subdivid)
	{
		List<AdvesoOverriddenActivitySetting> overriddenActivities = dao.getAdvesoOverriddenActivitiesSettingBySubdiv(subdivid);
		List<AdvesoOverriddenActivitySetting> list = new ArrayList<>();
		for (AdvesoTransferableActivity advesoTransferableActivity : generalTransferableActivities) {
			AdvesoOverriddenActivitySetting aoas = overriddenActivities.stream().filter(t -> t.getAdvesoTransferableActivity().equals(advesoTransferableActivity)).findFirst().orElse(null);
			if(aoas == null)
			{
				aoas = new AdvesoOverriddenActivitySetting();
				aoas.setAdvesoTransferableActivity(advesoTransferableActivity);
				aoas.setId(0);
				aoas.setIsTransferredToAdveso(advesoTransferableActivity.getIsTransferredToAdveso());
				aoas.setIsDisplayedOnJobOrderDetails(advesoTransferableActivity.getIsDisplayedOnJobOrderDetails());
			}
			list.add(aoas);
		}
		return list;
	}
}
