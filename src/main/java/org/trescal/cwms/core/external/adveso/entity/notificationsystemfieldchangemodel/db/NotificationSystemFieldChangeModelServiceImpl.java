package org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.events.JobItemDataUpdateEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Service
public class NotificationSystemFieldChangeModelServiceImpl
		extends BaseServiceImpl<NotificationSystemFieldChangeModel, Integer>
		implements NotificationSystemFieldChangeModelService {

	@Autowired
	private NotificationSystemFieldChangeModelDao dao;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@Override
	protected BaseDao<NotificationSystemFieldChangeModel, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public void saveAndPublishNotificationFieldChange(JobItem ji, String fieldName, NotificationSystemFieldTypeEnum fieldType, String newValue, String oldValue) {
		Date changeDate = new Date();
		NotificationSystemFieldChangeModel nsfc = new NotificationSystemFieldChangeModel(fieldName, fieldType, oldValue, newValue, changeDate);
		JobItemDataUpdateEvent updateEvent = new JobItemDataUpdateEvent(this, ji, nsfc);
		applicationEventPublisher.publishEvent(updateEvent);
	}
}
