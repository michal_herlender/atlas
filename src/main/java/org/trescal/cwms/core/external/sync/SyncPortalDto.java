package org.trescal.cwms.core.external.sync;

import java.util.Date;

public class SyncPortalDto {

	private int syncId;
	
	private int elementId;
	
	private Date lastPull;

	public SyncPortalDto() {
		
	}
	
	public SyncPortalDto(int syncId, int elementId, Date lastPull) {
		super();
		this.syncId = syncId;
		this.elementId = elementId;
		this.lastPull = lastPull;
	}

	public int getSyncId() {
		return syncId;
	}

	public void setSyncId(int syncId) {
		this.syncId = syncId;
	}

	public int getElementId() {
		return elementId;
	}

	public void setElementId(int elementId) {
		this.elementId = elementId;
	}

	public Date getLastPull() {
		return lastPull;
	}

	public void setLastPull(Date lastPull) {
		this.lastPull = lastPull;
	}
	
	
}
