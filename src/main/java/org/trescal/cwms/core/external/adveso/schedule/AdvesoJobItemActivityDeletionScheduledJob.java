package org.trescal.cwms.core.external.adveso.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

/**
 * Delete already sent activities that are older than one year
 */
@Component("AdvesoJobItemActivityDeletionScheduledJob")
public class AdvesoJobItemActivityDeletionScheduledJob {

	@Autowired
	private AdvesoJobItemActivityService advesoJobItemActivityService;
	@Autowired
	private ScheduledTaskService scheduledTaskService;
	
	private static Logger logger = LoggerFactory.getLogger(AdvesoJobItemActivityDeletionScheduledJob.class); 

	public void deleteAlreadySentAdvesoJobItemActivities() {
		if (this.scheduledTaskService.scheduledTaskIsActive(this.getClass().getName(),
				Thread.currentThread().getStackTrace())) {
			long startTime = System.currentTimeMillis();
			// Using logging level of INFO as not planned to run very often
			logger.info("Running deleteAlreadySentAdvesoJobItemActivities()");
			
			int deleteCount = advesoJobItemActivityService.deleteSentAdvesoJobItemActivities();
			long finishTime = System.currentTimeMillis();

			StringBuffer result = new StringBuffer();
			result.append("Deleted "+deleteCount+" activities in ");
			result.append((finishTime-startTime)+" ms");
			logger.info(result.toString());
			this.scheduledTaskService.reportOnScheduledTaskRun(this.getClass().getName(), 
					Thread.currentThread().getStackTrace(), true, result.toString());
			
		}
		else {
			logger.info("Not running task, task is inactive or tasks globally inactive");
		}
}
}
