package org.trescal.cwms.core.external.adveso.form;


import org.trescal.cwms.core.external.adveso.dto.AdvesoNotificationDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

public class SearchNotificationForm
{
	private String status;
	private String entityClass;
	private String operationType;
	private Integer coid;
	private String coname;
	private String jobno;
	private String jobitemno;
	private PagedResultSet<AdvesoNotificationDTO> rs;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEntityClass() {
		return entityClass;
	}
	public void setEntityClass(String entityClass) {
		this.entityClass = entityClass;
	}
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public Integer getCoid() {
		return coid;
	}
	public void setCoid(Integer coid) {
		this.coid = coid;
	}
	public String getConame() {
		return coname;
	}
	public void setConame(String coname) {
		this.coname = coname;
	}
	public String getJobno() {
		return jobno;
	}
	public void setJobno(String jobno) {
		this.jobno = jobno;
	}
	public String getJobitemno() {
		return jobitemno;
	}
	public void setJobitemno(String jobitemno) {
		this.jobitemno = jobitemno;
	}
	public PagedResultSet<AdvesoNotificationDTO> getRs() {
		return rs;
	}
	public void setRs(PagedResultSet<AdvesoNotificationDTO> rs) {
		this.rs = rs;
	}
}