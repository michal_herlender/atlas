package org.trescal.cwms.core.external.adveso.enums;

public enum EventStatusSearchForm {
	ALL,
	ERROR,
	SUCCESS,
	PENDING
}
