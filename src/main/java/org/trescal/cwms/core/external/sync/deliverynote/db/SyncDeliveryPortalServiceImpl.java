package org.trescal.cwms.core.external.sync.deliverynote.db;


import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryDao;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.external.portal.Portal;
import org.trescal.cwms.core.external.sync.SyncPortalDto;
import org.trescal.cwms.core.external.sync.deliverynote.SyncDeliveryPortal;

@Service
public class SyncDeliveryPortalServiceImpl extends BaseServiceImpl<SyncDeliveryPortal, Integer> implements SyncDeliveryPortalService {

	@Autowired
	private SyncDeliveryPortalDao syncDeliveryPortaldao;
	@Autowired
	private DeliveryDao deliveryDao;
	
	@Override
	protected BaseDao<SyncDeliveryPortal, Integer> getBaseDao() {
		return syncDeliveryPortaldao;
	}

	@Override
	public List<SyncPortalDto> getAllSyncDelivery(List<Integer> deliveries, Portal portal) {
		return syncDeliveryPortaldao.getAllSyncDelivery(deliveries,portal);
	}
	
	@Transactional
	@Override
	public void saveAll(List<Integer> deliveries, Portal portal) {
		Date currentDate = Calendar.getInstance().getTime();
		deliveries = deliveries.stream().distinct().collect(Collectors.toList());
		List<JobDelivery> lstDel = deliveryDao
				.findDeliveries(deliveries);
		for (int id : deliveries) {
			SyncDeliveryPortal syncAdd = new SyncDeliveryPortal();
			syncAdd.setDelivery(lstDel.stream().filter(x -> x.getDeliveryid() == id).findFirst().orElse(null));
			syncAdd.setPortal(portal);
			syncAdd.setLastPull(currentDate);
			this.getBaseDao().persist(syncAdd);
		}
	}
	@Transactional
	@Override
	public void updatePullAll(List<Integer> syncs) {
		this.syncDeliveryPortaldao.UpdateSyncPull(syncs);
	}
}
