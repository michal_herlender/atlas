package org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.db;


import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

public interface AdvesoOverriddenActivitySettingService extends BaseService<AdvesoOverriddenActivitySetting, Integer> {

	public List<AdvesoOverriddenActivitySetting> getAdvesoOverriddenActivitiesSettingBySubdiv(Integer subdivid);
	public List<AdvesoActivityDTO> getNotTransferableSubdivLevelActivitiesBySubdiv(Integer subdivid);
	public List<AdvesoActivityDTO> getTransferableSubdivLevelActivitiesBySubdiv(Integer subdivid);
	public List<AdvesoActivityDTO> getNotDisplayedSubdivLevelActivitiesBySubdiv(Integer subdivid);
	public List<AdvesoActivityDTO> getDisplayedSubdivLevelActivitiesBySubdiv(Integer subdivid);
	public List<AdvesoOverriddenActivitySetting> mergeSubdivOverriddenActivities(List<AdvesoTransferableActivity> generalTransferableActivities, Integer subdivid);
}
