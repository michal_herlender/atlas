package org.trescal.cwms.core.external.sync.deliverynote.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.external.portal.Portal;
import org.trescal.cwms.core.external.sync.SyncPortalDto;
import org.trescal.cwms.core.external.sync.deliverynote.SyncDeliveryPortal;

public interface SyncDeliveryPortalService extends BaseService<SyncDeliveryPortal,Integer>{

	public List<SyncPortalDto> getAllSyncDelivery(List<Integer> deliveries,Portal portal);
	
	void updatePullAll(List<Integer> syncs);

	void saveAll(List<Integer> deliveries, Portal portal);
	
}
