package org.trescal.cwms.core.external.adveso.dto;

import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;

import lombok.Data;

@Data
public class NotificationSystemFieldDTO {

	private String fieldName;

	private NotificationSystemFieldTypeEnum fieldType;

	private String fieldValue;

	private NotificationSystemEntityClassEnum entityClass;

	public NotificationSystemFieldDTO(String fieldName, NotificationSystemFieldTypeEnum fieldType, String fieldValue,
			NotificationSystemEntityClassEnum entityClass) {
		super();
		this.fieldName = fieldName;
		this.fieldType = fieldType;
		this.fieldValue = fieldValue;
		this.entityClass = entityClass;
	}

}
