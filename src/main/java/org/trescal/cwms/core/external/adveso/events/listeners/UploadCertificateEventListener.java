package org.trescal.cwms.core.external.adveso.events.listeners;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.external.adveso.events.UploadCertificateEvent;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;

@Component
public class UploadCertificateEventListener implements ApplicationListener<UploadCertificateEvent> {

	@Autowired
	private NotificationSystemModelService notificationSystemModelService;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;

	@Override
	public void onApplicationEvent(UploadCertificateEvent event) {

		Certificate certificate = event.getCertificate();
		JobItem jobItem = event.getJobItem();
		NotificationSystemOperationTypeEnum operationType = event.getOperationType();

		boolean islinkToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
				jobItem.getJob().getCon().getSub().getSubdivid(),
				jobItem.getJob().getOrganisation().getComp().getCoid(),  new Date());

		if (islinkToAdveso)
			notificationSystemModelService.pushToQueue(UploadCertificateEvent.ENTITY_CLASS,
					UploadCertificateEvent.FIELD_NAME, operationType, UploadCertificateEvent.FIELD_TYPE,
					certificate.getCertid(), null, jobItem, null);

	}

}
