package org.trescal.cwms.core.external.adveso.events.listeners;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.events.JobItemActionCreateOrUpdateEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;

@Component
public class JobItemActionCreateOrUpdateEventListener implements ApplicationListener<JobItemActionCreateOrUpdateEvent> {

	@Autowired
	private AdvesoJobItemActivityService advesoJobItemActivityService;
	@Autowired
	private NotificationSystemModelService notificationSystemModelService;

	@Override
	public void onApplicationEvent(JobItemActionCreateOrUpdateEvent event) {
		JobItemAction jobItemAction = event.getJobItemAction();
		Boolean jobItemComplete = event.getJobItemComplete();
		if (jobItemAction != null) {
			// push activity to queue if only it is transferable
			advesoJobItemActivityService.pushToQueueIfJobItemActionIsTransferable(jobItemAction);
			// if the jobitem is complete send a notification to Adveso
			if (BooleanUtils.isTrue(jobItemComplete)) {
				notificationSystemModelService.pushToQueue(JobItemActionCreateOrUpdateEvent.ENTITY_CLASS,
						JobItemActionCreateOrUpdateEvent.FIELD_NAME, JobItemActionCreateOrUpdateEvent.OPERATION_TYPE,
						JobItemActionCreateOrUpdateEvent.FIELD_TYPE,
						Integer.valueOf(jobItemAction.getJobItem().getJobItemId()),
						jobItemAction.getJobItem().getInst(),null, jobItemAction.getJobItem().getJob().getCon());
			}
		}

	}

}
