package org.trescal.cwms.core.external.adveso.events.listeners;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db.NotificationSystemFieldChangeModelService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.events.CompleteCalibrationEvent;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;

@Component
public class CompleteCalibrationEventListener implements ApplicationListener<CompleteCalibrationEvent> {

	@Autowired
	private NotificationSystemModelService notificationSystemModelService;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private NotificationSystemFieldChangeModelService notificationSystemFieldChangeModelService;

	@Override
	public void onApplicationEvent(CompleteCalibrationEvent event) {

		Calibration calibration = event.getCalibration();
		// get first jobitem from callink
		JobItem jobItem = calibration.getLinks().iterator().next().getJi();

		boolean islinkToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
				jobItem.getJob().getCon().getSub().getSubdivid(),
				jobItem.getJob().getOrganisation().getComp().getCoid(),  new Date());

		if (islinkToAdveso)
			for (CalLink link : calibration.getLinks()) {

				NotificationSystemModel notificationSystemModel = notificationSystemModelService.pushToQueue(
						CompleteCalibrationEvent.ENTITY_CLASS, CompleteCalibrationEvent.FIELD_NAME,
						CompleteCalibrationEvent.OPERATION_TYPE, CompleteCalibrationEvent.FIELD_TYPE,
						calibration.getId(), null, link.getJi(), null);

				NotificationSystemFieldChangeModel nsfcm = event.getNsfcm();
				nsfcm.setNotification(notificationSystemModel);
				notificationSystemFieldChangeModelService.save(nsfcm);

			}
	}

}
