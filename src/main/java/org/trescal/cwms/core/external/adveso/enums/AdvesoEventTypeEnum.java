package org.trescal.cwms.core.external.adveso.enums;

public enum AdvesoEventTypeEnum {

	ACTIVITY, NOTIFICATION;

}
