package org.trescal.cwms.core.external.adveso.events;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class DeleteJobItemEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;
	
	public static final NotificationSystemEntityClassEnum ENTITY_CLASS = NotificationSystemEntityClassEnum.JobItem;
	public static final String FIELD_NAME = "jobItemId";
	public static final NotificationSystemOperationTypeEnum OPERATION_TYPE = NotificationSystemOperationTypeEnum.DELETE;
	public static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.Integer; 
	
	private JobItem jobItem;

	public DeleteJobItemEvent(Object source, JobItem jobItem) {
		super(source);
		this.jobItem = jobItem;
	}

	public JobItem getJobItem() {
		return jobItem;
	}

}
