package org.trescal.cwms.core.external.adveso.events;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class JobItemDataUpdateEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	public static final NotificationSystemEntityClassEnum ENTITY_CLASS = NotificationSystemEntityClassEnum.JobItem;
	public static final String FIELD_NAME = "jobItemId";
	public static final NotificationSystemOperationTypeEnum OPERATION_TYPE = NotificationSystemOperationTypeEnum.UPDATE;
	public static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.Integer;

	private JobItem jobItem;
	private NotificationSystemFieldChangeModel nsfcm;

	public JobItemDataUpdateEvent(Object source, JobItem jobItem, NotificationSystemFieldChangeModel nsfcm) {
		super(source);
		this.jobItem = jobItem;
		this.nsfcm = nsfcm;
	}

	public JobItem getJobItem() {
		return jobItem;
	}

	public NotificationSystemFieldChangeModel getNsfcm() {
		return nsfcm;
	}

}
