package org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.form.SearchNotificationForm;

public interface NotificationSystemModelDao extends BaseDao<NotificationSystemModel, Integer> {

	public List<NotificationSystemModel> getNeverSent(int maxNotifications);

	public boolean itemExist(NotificationSystemModel item);

	void getNotificationsToResent(SearchNotificationForm form);

	public Integer deleteSentAdvesoNotificationsSystem();

	List<NotificationSystemModel> getNotificationSystemModelbyId(List<Integer> idsnotif);

	public void updateAfterSending(boolean success, Date date, String errorMessage,
			List<Integer> advesoNotificationIds);

	public List<NotificationSystemModel> getByIds(List<Integer> notificationIds);
}
