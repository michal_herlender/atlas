package org.trescal.cwms.core.external.adveso.events.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db.NotificationSystemFieldChangeModelService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.external.adveso.events.RCRCompletionDateChangedEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.tools.DateTools;

@Component
public class RCRCompletionDateChangedEventListener implements ApplicationListener<RCRCompletionDateChangedEvent> {

	private static final String FIELD_NAME = "id";
	private static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.Integer;
	@Autowired
	private NotificationSystemModelService notificationSystemModelService;
	@Autowired
	private NotificationSystemFieldChangeModelService notificationSystemFieldChangeModelService;

	@Override
	public void onApplicationEvent(RCRCompletionDateChangedEvent event) {

		RepairCompletionReport rcr = event.getRcr();
		JobItem jobItem = event.getJobItem();
		NotificationSystemOperationTypeEnum operationType = event.getOperationType();

		NotificationSystemModel notification = notificationSystemModelService.pushToQueue(
				RCRCompletionDateChangedEvent.ENTITY_CLASS, FIELD_NAME, operationType, FIELD_TYPE, rcr.getRcrId(),
				null, jobItem, null);

		NotificationSystemFieldChangeModel nsfc = new NotificationSystemFieldChangeModel(
				RCRCompletionDateChangedEvent.FIELD_NAME, RCRCompletionDateChangedEvent.FIELD_TYPE,
				event.getOldValue() != null ? DateTools.dtf_activities.format(event.getOldValue()) : null,
				DateTools.dtf_activities.format(event.getNewValue()), rcr.getLastModified());
		nsfc.setNotification(notification);
		notificationSystemFieldChangeModelService.save(nsfc);

	}
}
