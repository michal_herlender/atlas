package org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.external.adveso.dto.AdvesoActivityEventDTO;
import org.trescal.cwms.core.external.adveso.dto.AdvesoJobItemActivityDTO;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.db.AdvesoOverriddenActivitySettingService;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db.AdvesoTransferableActivityService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.events.DeleteJobItemActionEvent;
import org.trescal.cwms.core.external.adveso.form.SearchEventForm;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;
import org.trescal.cwms.rest.adveso.dto.transformer.AdvesoActivityEventTransformer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class AdvesoJobItemActivityServiceImpl extends BaseServiceImpl<AdvesoJobItemActivity, Integer>
	implements AdvesoJobItemActivityService {

	@Autowired
	private AdvesoJobItemActivityDao dao;
	@Autowired
	private AdvesoTransferableActivityService advesoTransferableActivityService;
	@Autowired
	private AdvesoOverriddenActivitySettingService advesoOverriddenActivitySettingService;
	@Autowired
	private NotificationSystemModelService notificationSystemModelService;
	@Autowired
	private JobCostingService jobCostingService;
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private JobItemService jobItemService;
	@Value("#{props['cwms.advesointerface.maxactivitiestosend']}")
	private int maxActivities;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private AdvesoActivityEventTransformer activityEventTransformer;
	@Autowired
	private JobItemActionService jobItemActionService;

	@Override
	protected BaseDao<AdvesoJobItemActivity, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public List<AdvesoJobItemActivity> getNeverSentActivities() {
		return dao.getNeverSentActivities(maxActivities);
	}

	@Override
	public List<AdvesoJobItemActivity> getAdvesoJobItemActivitiesByActivityId(JobItemAction jia) {
		return dao.getAdvesoJobItemActivitiesByActivityId(jia);
	}

	@Override
	public void findActivities(SearchEventForm form, PagedResultSet<AdvesoJobItemActivityDTO> prs) {
		this.dao.findActivities(form, prs);
	}

	@Override
	public Integer deleteSentAdvesoJobItemActivities() {
		return dao.deleteSentJobItemActivities();
	}

	@Override
	public boolean isTransferableToAdveso(JobItemAction jobItemAction) {
		List<AdvesoTransferableActivity> generalTransferedActivities = advesoTransferableActivityService
				.getAllFromCache();
		if (generalTransferedActivities != null && !generalTransferedActivities.isEmpty()) {
			List<AdvesoOverriddenActivitySetting> overriddenActivities = advesoOverriddenActivitySettingService
					.mergeSubdivOverriddenActivities(generalTransferedActivities,
							jobItemAction.getJobItem().getJob().getCon().getSub().getSubdivid());
			if (overriddenActivities != null && !overriddenActivities.isEmpty())
				return overriddenActivities.stream().anyMatch(a -> a.getAdvesoTransferableActivity().getItemActivity()
						.getStateid().equals(jobItemAction.getActivity().getStateid()) && a.getIsTransferredToAdveso());
		}
		return false;
	}

	@Override
	public void pushToQueue(JobItemAction jobItemAction) {
		AdvesoJobItemActivity advesoJobItemActivity = new AdvesoJobItemActivity();
		advesoJobItemActivity.setCreatedOn(jobItemAction.getCreatedOn() != null ? jobItemAction.getCreatedOn() : new Date());
		advesoJobItemActivity.setJobItemAction(jobItemAction);
		advesoJobItemActivity.setIsSent(false);
		boolean isSingleJobItem = false;
		boolean isJobCosting = false;
		if (jobItemAction.getActivity() != null && jobItemAction.getActivity().getGroupLinks() != null) {
			// set job costing link
			if (jobItemService.stateHasGroupOfKeyName(jobItemAction.getActivity(), StateGroup.COSTING_ISSUED)) {
				String activityRemark = jobItemAction.getRemark();
				Integer jobCostingVersion = null;
				Locale messageLocale = jobItemAction.getJobItem().getJob().getOrganisation().getComp().getDocumentLanguage();
				if (activityRemark != null) {
					String costingFileMessage = this.messageSource.getMessage("costing.file", null, "Costing File",
						messageLocale) + " : ";
					if (activityRemark.contains(costingFileMessage)) {
						int endIndex = activityRemark.indexOf("&nbsp;");
						String fileName;
						int beginIndex = activityRemark.indexOf(costingFileMessage) + costingFileMessage.length();
						if (endIndex == -1)
							fileName = activityRemark.substring(
								beginIndex);
						else
							fileName = activityRemark.substring(
								beginIndex, endIndex);
						advesoJobItemActivity.setJobCostingFileName(fileName);
						String[] fileNameSplit = fileName.split(" ");
						if (fileNameSplit.length >= 4) {
							jobCostingVersion = Integer.valueOf(fileNameSplit[3]);
						}
					} else {
						Pattern p = Pattern.compile(Constants.VERSION_TEXT + "([0-9]*)");
						Matcher m = p.matcher(activityRemark);
						while (m.find()){
						    jobCostingVersion = Integer.valueOf(m.group(1));
						}
					}
				}
				if (jobCostingVersion != null) {
					JobCosting jobCosting = jobCostingService.getJobCosting(jobItemAction.getJobItem().getJobItemId(),
							jobCostingVersion);
					if (jobCosting != null) {
						advesoJobItemActivity.setLinkedEntityType("JobCosting");
						advesoJobItemActivity.setLinkedEntityId(jobCosting.getId());
						isJobCosting = true;
						isSingleJobItem = PricingType.SINGLE_JOB_ITEM.equals(jobCosting.getType());
					}
				}
			}

			// set calibration link
			if (jobItemService.stateHasGroupOfKeyName(jobItemAction.getActivity(), StateGroup.CALIBRATION)) {
				Calibration lastCalibration = this.calibrationService
						.findLastCalibrationForJobitem(jobItemAction.getJobItem().getJobItemId());
				if (lastCalibration != null) {
					advesoJobItemActivity.setLinkedEntityType("Calibration");
					advesoJobItemActivity.setLinkedEntityId(lastCalibration.getId());
				}
			}
		}
		
		if(!isJobCosting || (isJobCosting && isSingleJobItem))
			this.save(advesoJobItemActivity);
	}

	@Async
	@Override
	public void pushToQueueIfJobItemActionIsTransferable(JobItemAction jobItemAction) {
		boolean istransferedActivity = this.isTransferableToAdveso(jobItemAction);
		if (istransferedActivity) {
			this.pushToQueue(jobItemAction);
		}
	}

	@Async
	@Override
	public void deleteFromQueueIfJobItemActionIsTransferable(JobItemAction jobItemAction) {
		/*
		 * either the AdvesoJobItemActivity is already sent to Adveso or it is still
		 * waiting in the queue
		 */
		List<AdvesoJobItemActivity> ajisa = this.getAdvesoJobItemActivitiesByActivityId(jobItemAction);
		// get the first AdvesoJobItemActivity refering to the creation
		// of the JobItemAction
		AdvesoJobItemActivity creationEvent;
		if (!ajisa.isEmpty()) {
			creationEvent = ajisa.get(0);

			// if already sent
			if (creationEvent.getIsSent()) {
				// notify adveso
				notificationSystemModelService.pushToQueue(DeleteJobItemActionEvent.ENTITY_CLASS,
					DeleteJobItemActionEvent.FIELD_NAME, DeleteJobItemActionEvent.OPERATION_TYPE,
					DeleteJobItemActionEvent.FIELD_TYPE, creationEvent.getJobItemAction().getId(),
					creationEvent.getJobItemAction().getJobItem().getInst(), null, creationEvent.getJobItemAction().getJobItem().getJob().getCon());
			}
			// delete all AdvesoJobItemActivity
			// refering to
			// jobitemaction in the queue
			ajisa.forEach(a -> dao.remove(a));

		}
	}

	@Override
	public List<AdvesoActivityDTO> getSubdivActivities(Integer subdivid) {
		List<AdvesoActivityDTO> defaultActs = advesoTransferableActivityService.getDefaultActivities();
		List<AdvesoActivityDTO> subdivLevelNotTransActs = advesoOverriddenActivitySettingService
			.getNotTransferableSubdivLevelActivitiesBySubdiv(subdivid);
		List<AdvesoActivityDTO> subdivLevelTransActs = advesoOverriddenActivitySettingService
			.getTransferableSubdivLevelActivitiesBySubdiv(subdivid);

		List<AdvesoActivityDTO> result = new ArrayList<>(defaultActs);
		result.removeAll(subdivLevelNotTransActs);
		result.removeAll(subdivLevelTransActs);
		result.addAll(subdivLevelTransActs);
		result = result.stream().filter(AdvesoActivityDTO::isTransferable).collect(Collectors.toList());
		result.forEach(r -> r.setSubdivID(subdivid));
		return result;
	}

	@Override
	public AdvesoJobItemActivity getCertificateAdvesoJobItemActivityByJIAction(JobItemAction jia) {
		return dao.getCertificateAdvesoJobItemActivityByJIAction(jia);
	}

	@Override
	public void updateAfterSending(boolean success, Date date, String errorMessage, List<Integer> advesoActivityIds) {
		dao.updateAfterSending(success, date, errorMessage, advesoActivityIds);
	}

	@Override
	public List<AdvesoJobItemActivity> getByIds(List<Integer> activitiyIds) {
		return dao.getByIds(activitiyIds);
	}

	@Override
	public List<AdvesoActivityEventDTO> getNeverSentActivitiesDTOs() {
		List<AdvesoJobItemActivity> neverSentActivities = this.getNeverSentActivities();
		return activityEventTransformer.convertList(neverSentActivities);
	}

	@Override
	public void generateAdvesoActivities() {
		List<Integer> notProcessedActionsId = this.jobItemActionService.findNotProcessedActionsIdForAdveso();
		for (Integer jobItemActionId : notProcessedActionsId) {
			JobItemAction action = this.jobItemActionService.get(jobItemActionId);
			if(action != null)
				this.pushToQueue(action);
		}
	}
}
