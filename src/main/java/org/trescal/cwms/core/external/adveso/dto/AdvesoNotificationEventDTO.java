package org.trescal.cwms.core.external.adveso.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;

import lombok.Data;

@Data
public class AdvesoNotificationEventDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private NotificationSystemFieldDTO entityId;
	private NotificationSystemOperationTypeEnum operationType;
	private NotificationSystemFieldChangeDTO entityChange;
	private List<NotificationSystemFieldDTO> parent;
	private NotificationSystemClientModel client;
	private String lastModified;
	private String createdOn;
	private Date createdOnNonFormatted;

	@Override
	public String toString() {
		return "AdvesoNotificationEventDTO [id=" + id + ", entityId=" + entityId + ", operationType=" + operationType
				+ ", entityChange=" + entityChange + ", createdOn=" + createdOn + "]";
	}

}
