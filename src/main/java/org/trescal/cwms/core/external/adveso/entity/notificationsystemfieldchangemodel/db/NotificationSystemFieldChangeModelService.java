package org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public interface NotificationSystemFieldChangeModelService
		extends BaseService<NotificationSystemFieldChangeModel, Integer> {

	void saveAndPublishNotificationFieldChange(JobItem ji, String fieldName, NotificationSystemFieldTypeEnum fieldType, String newValue, String oldValue);

}
