package org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;

import lombok.Setter;

@Entity
@Table(name = "notificationsystemfieldchange")
@Setter
public class NotificationSystemFieldChangeModel {

	private Integer id;
	private String fieldName;
	private NotificationSystemFieldTypeEnum fieldType;
	private String oldFieldValue;
	private String newFieldValue;
	private Date changeDate;
	private NotificationSystemModel notification;

	public NotificationSystemFieldChangeModel() {
	}

	public NotificationSystemFieldChangeModel(String fieldName, NotificationSystemFieldTypeEnum fieldType,
			String oldFieldValue, String newFieldValue, Date changeDate) {
		super();
		this.fieldName = fieldName;
		this.fieldType = fieldType;
		this.oldFieldValue = oldFieldValue;
		this.newFieldValue = newFieldValue;
		this.changeDate = changeDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@Column(name = "oldvalue")
	public String getOldFieldValue() {
		return oldFieldValue;
	}

	@Column(name = "newvalue")
	public String getNewFieldValue() {
		return newFieldValue;
	}

	@Column(name = "fieldname")
	public String getFieldName() {
		return fieldName;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "fieldtype")
	public NotificationSystemFieldTypeEnum getFieldType() {
		return fieldType;
	}

	@NotNull
	@Column(name = "changedate", nullable = false, columnDefinition = "datetime2")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getChangeDate() {
		return this.changeDate;
	}

	@OneToOne
	@JoinColumn(name = "notificationid")
	public NotificationSystemModel getNotification() {
		return notification;
	}
}
