package org.trescal.cwms.core.external.adveso.events;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;

public class DeleteJobItemActionEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;
	
	public static final NotificationSystemEntityClassEnum ENTITY_CLASS = NotificationSystemEntityClassEnum.JobItemAction;
	public static final String FIELD_NAME = "jobItemActionId";
	public static final NotificationSystemOperationTypeEnum OPERATION_TYPE = NotificationSystemOperationTypeEnum.DELETE;
	public static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.Integer;
	
	private JobItemAction jobItemAction;
	

	public DeleteJobItemActionEvent(Object source, JobItemAction jobItemAction) {
		super(source);
		this.jobItemAction = jobItemAction;
	}

	public JobItemAction getJobItemAction() {
		return jobItemAction;
	}

}
