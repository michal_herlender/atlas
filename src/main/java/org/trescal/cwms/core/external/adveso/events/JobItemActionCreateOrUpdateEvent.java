package org.trescal.cwms.core.external.adveso.events;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;

public class JobItemActionCreateOrUpdateEvent extends ApplicationEvent {
	
	public static final NotificationSystemEntityClassEnum ENTITY_CLASS = NotificationSystemEntityClassEnum.JobItem;
	public static final String FIELD_NAME = "jobItemId";
	public static final NotificationSystemOperationTypeEnum OPERATION_TYPE = NotificationSystemOperationTypeEnum.COMPLETE;
	public static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.Integer;

	private static final long serialVersionUID = 1L;

	private JobItemAction jobItemAction;
	private Boolean jobItemComplete;

	public JobItemActionCreateOrUpdateEvent(Object source, JobItemAction jobItemAction, Boolean jobItemComplete) {
		super(source);
		this.jobItemAction = jobItemAction;
		this.jobItemComplete = jobItemComplete;
	}

	public JobItemAction getJobItemAction() {
		return jobItemAction;
	}

	public Boolean getJobItemComplete() {
		return jobItemComplete;
	}

}
