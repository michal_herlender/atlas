package org.trescal.cwms.core.external.sync.deliverynote.db;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery_;
import org.trescal.cwms.core.external.portal.Portal;
import org.trescal.cwms.core.external.sync.SyncPortalDto;
import org.trescal.cwms.core.external.sync.deliverynote.SyncDeliveryPortal;

@Repository
public class SyncDeliveryPortalDaoImpl extends BaseDaoImpl<SyncDeliveryPortal, Integer> implements SyncDeliveryPortalDao{

	@Override
	protected Class<SyncDeliveryPortal> getEntity() {
		return SyncDeliveryPortal.class;
	}

	@Override
	public List<SyncPortalDto> getAllSyncDelivery(List<Integer> deliveries,Portal portal) {
		return getResultList(cb -> {
			CriteriaQuery<SyncPortalDto> cq = cb.createQuery(SyncPortalDto.class);
			Root<SyncDeliveryPortal> syncRoot = cq.from(SyncDeliveryPortal.class);	
			Join<SyncDeliveryPortal,JobDelivery> del = syncRoot.join("delivery");
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(del.in(deliveries));
			clauses.getExpressions().add(cb.equal(syncRoot.get("portal"), portal));
			cq.where(clauses);
			return cq.select(cb.construct(SyncPortalDto.class, syncRoot.get("syncDeliveryId"),del.get(JobDelivery_.deliveryid),syncRoot.get("lastPull")));
		});
	}

	@Override
	public void UpdateSyncPull(List<Integer> syncsId) {
		Date currentDate = Calendar.getInstance().getTime();
		CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
		CriteriaUpdate<SyncDeliveryPortal> cu = cb.createCriteriaUpdate(SyncDeliveryPortal.class);
		cu.from(SyncDeliveryPortal.class);
		cu.set("lastPull", currentDate);
		cu.where(cu.getRoot().in(syncsId));
		this.getEntityManager().createQuery(cu).executeUpdate();
	}
}
