package org.trescal.cwms.core.external.adveso.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobCostingItemDTO;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
/**
 * Note, the Adveso interface expects a standard date format of "dd.MM.yyyy - HH.mm.ss" even for fields
 * which are "date only" e.g. LocalDate, therefore we can't use just "dd.MM.yyyy" in LocalDate output
 * (e.g. in any future updates / conversions) - otherwise it breaks the Adveso interface
 */
public class AdvesoActivityEventDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int jobItemActionId;
	private String jobItemNumber;
	private String plantNo;
	private int plantId;
	private int subdivID;
	private int activityID;
	private String activityName;
	private Integer queueId;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy - 00.00.00")
	private LocalDate plannedDeliveryDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy - 00.00.00")
	private LocalDate approvedDeliveryDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy - HH.mm.ss")
	private LocalDateTime realDeliveryDate;
	private Boolean isOnSite;
	private Boolean isActionDeleted;
	private String itemNo;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy - HH.mm.ss")
	private LocalDateTime receiptDate;
	private int servicetypeId;
	private String serviceType;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy - HH.mm.ss")
	private LocalDateTime startDate;
	private String remarks;

	private String formerID;
	private boolean IsDeleted;

	private String calVerificationStatus;
	private List<AdvesoJobCostingItemDTO> jobCostingItems;

	private Integer actionOutComeID;
	private String actionOutComeName;

	private String technicienName;
	private String jobCostingFileId;
	private String jobCostingType;
	private String oldJobItemReference;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy - HH.mm.ss")
	private LocalDateTime createdOn;
	private Date createdOnNonFormatted;
	private Integer endStatusId;
	private String endStatusDescription;

	@Override
	public String toString() {
		return "AdvesoActivityEventDTO [jobItemActionId=" + jobItemActionId + ", jobItemNumber=" + jobItemNumber
				+ ", plantId=" + plantId + ", queueId=" + queueId + ", createdOn=" + createdOn + "]";
	}

}
