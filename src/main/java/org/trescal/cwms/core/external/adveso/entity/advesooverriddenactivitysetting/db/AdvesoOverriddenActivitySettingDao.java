package org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

public interface AdvesoOverriddenActivitySettingDao extends BaseDao<AdvesoOverriddenActivitySetting, Integer>{
	
	public List<AdvesoOverriddenActivitySetting> getAdvesoOverriddenActivitiesSettingBySubdiv(Integer subdivid);
	public List<AdvesoActivityDTO> getNotTransferableSubdivLevelActivitiesBySubdiv(Integer subdivid);
	public List<AdvesoActivityDTO> getTransferableSubdivLevelActivitiesBySubdiv(Integer subdivid);
	public List<AdvesoActivityDTO> getNotDisplayedSubdivLevelActivitiesBySubdiv(Integer subdivid);
	public List<AdvesoActivityDTO> getDisplayedSubdivLevelActivitiesBySubdiv(Integer subdivid);
	
}
