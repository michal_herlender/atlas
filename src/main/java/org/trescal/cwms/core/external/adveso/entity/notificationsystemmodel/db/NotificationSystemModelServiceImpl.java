package org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.external.adveso.dto.AdvesoNotificationEventDTO;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.external.adveso.form.SearchNotificationForm;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.rest.adveso.dto.transformer.AdvesoNotificationSystemTransformer;

import java.util.Date;
import java.util.List;

@Service
public class NotificationSystemModelServiceImpl extends BaseServiceImpl<NotificationSystemModel, Integer>
	implements NotificationSystemModelService {

	@Autowired
	private NotificationSystemModelDao dao;
	@Autowired
	private JobItemActionService jobItemActionService;
	@Autowired
	private JobItemService jobItemService;
	@Value("#{props['cwms.advesointerface.maxnotificationstosend']}")
	private int maxNotifications;
	@Autowired
	private AdvesoNotificationSystemTransformer notificationEventransformer;

	@Override
	protected BaseDao<NotificationSystemModel, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public List<NotificationSystemModel> getNeverSent() {
		return dao.getNeverSent(maxNotifications);
	}

	@Override
	public boolean itemExist(NotificationSystemModel item) {
		return dao.itemExist(item);
	}

	@Override
	public void getNotificationsToResent(SearchNotificationForm form) {
		dao.getNotificationsToResent(form);
	}

	@Override
	public JobItem getNotificationJobItem(Integer notificationId) {
		NotificationSystemModel notification = dao.find(notificationId);
		JobItem jobItem = null;
		if (notification != null) {
			if (notification.getEntityClass().equals(NotificationSystemEntityClassEnum.JobItem)
					&& notification.getEntityIdFieldName().equals("jobItemId")) {
				jobItem = jobItemService.findJobItem(Integer.parseInt(notification.getEntityIdFieldValue()));
			} else if (notification.getEntityClass().equals(NotificationSystemEntityClassEnum.JobItemAction)
					&& notification.getEntityIdFieldName().equals("jobItemActionId")) {
				JobItemAction jia = jobItemActionService.get(Integer.valueOf(notification.getEntityIdFieldValue()));
				jobItem = jia != null ? jia.getJobItem() : null;
			}
		}
		return jobItem;
	}

	@Override
	public NotificationSystemModel pushToQueue(NotificationSystemEntityClassEnum entityClass, String fieldName,
			NotificationSystemOperationTypeEnum operationType, NotificationSystemFieldTypeEnum fieldType,
			Integer entityId, Instrument instrument, JobItem jobItem, Contact jobContact) {
		NotificationSystemModel notificationSystemModel = new NotificationSystemModel();
		notificationSystemModel.setCreatedOn(new Date());
		notificationSystemModel.setEntityClass(entityClass);
		notificationSystemModel.setEntityIdFieldName(fieldName);
		notificationSystemModel.setEntityIdFieldType(fieldType);
		notificationSystemModel.setEntityIdFieldValue(entityId.toString());
		notificationSystemModel.setInstrument(instrument);
		notificationSystemModel.setOperationType(operationType);
		notificationSystemModel.setJobItem(jobItem);
		notificationSystemModel.setIsSent(false);
		notificationSystemModel.setJobContact(jobContact);
		this.save(notificationSystemModel);
		return notificationSystemModel;
	}

	@Override
	public Integer deleteSentAdvesoNotificationsSystem() {
		return dao.deleteSentAdvesoNotificationsSystem();
	}

	@Override
	public List<NotificationSystemModel> getNotificationSystemModelbyId(List<Integer> idsnotif) {
		return dao.getNotificationSystemModelbyId(idsnotif);
	}

	@Override
	public void updateAfterSending(boolean success, Date date, String errorMessage,
			List<Integer> advesoNotificationIds) {
		dao.updateAfterSending(success, date, errorMessage, advesoNotificationIds);
	}

	@Override
	public List<NotificationSystemModel> getByIds(List<Integer> notificationIds) {
		return dao.getByIds(notificationIds);
	}

	@Override
	public List<AdvesoNotificationEventDTO> getNeverSentDTOs() {
		List<NotificationSystemModel> neverSentNotifications = this.getNeverSent();
		return notificationEventransformer.convertList(neverSentNotifications);
	}
}
