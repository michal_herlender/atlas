package org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;

@Entity
@Table(name = "advesojobItemactivityqueue")
public class AdvesoJobItemActivity extends Auditable {

	private int id;
	private JobItemAction jobItemAction;
	private Boolean isSent;
	private Date sentOn;
	private Boolean isReceived;
	private Date receivedOn;
	private Boolean isTreated;
	private Date treatedOn;
	private String lastErrorMessage;
	private Date lastErrorMessageOn;
	private Date modifiedOn;
	private Integer linkedEntityId;
	private String linkedEntityType;
	private String jobCostingFileName;
	private Date createdOn;

	public AdvesoJobItemActivity() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Note - relationship will be fetched eagerly due to NotFoundAction.IGNORE,
	 * regardless of fetch type specified in ManyToOne 
	 * (removed FetchType.LAZY to eliminate warning of fetch type being ignored) 
	 */
	@ManyToOne
	@JoinColumn(name = "jobitemactionid", foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
	@NotFound(action = NotFoundAction.IGNORE)
	public JobItemAction getJobItemAction() {
		return jobItemAction;
	}

	public void setJobItemAction(JobItemAction jobItemActionId) {
		this.jobItemAction = jobItemActionId;
	}

	public Boolean getIsSent() {
		return isSent;
	}

	public void setIsSent(Boolean isSent) {
		this.isSent = isSent;
	}

	public Date getSentOn() {
		return sentOn;
	}

	public void setSentOn(Date sentOn) {
		this.sentOn = sentOn;
	}

	public Boolean getIsReceived() {
		return isReceived;
	}

	public void setIsReceived(Boolean isReceived) {
		this.isReceived = isReceived;
	}

	public Date getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(Date receivedOn) {
		this.receivedOn = receivedOn;
	}

	public Boolean getIsTreated() {
		return isTreated;
	}

	public void setIsTreated(Boolean isTreated) {
		this.isTreated = isTreated;
	}

	public Date getTreatedOn() {
		return treatedOn;
	}

	public void setTreatedOn(Date treatedOn) {
		this.treatedOn = treatedOn;
	}

	public String getLastErrorMessage() {
		return lastErrorMessage;
	}

	public void setLastErrorMessage(String lastErrorMessage) {
		this.lastErrorMessage = lastErrorMessage;
	}

	public Date getLastErrorMessageOn() {
		return lastErrorMessageOn;
	}

	public void setLastErrorMessageOn(Date lastErrorMessageOn) {
		this.lastErrorMessageOn = lastErrorMessageOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Column(name = "jobcostingfilename")
	public String getJobCostingFileName() {
		return jobCostingFileName;
	}

	public void setJobCostingFileName(String jobCostingFileName) {
		this.jobCostingFileName = jobCostingFileName;
	}

	@Column(name = "linkedentityid")
	public Integer getLinkedEntityId() {
		return linkedEntityId;
	}

	public void setLinkedEntityId(Integer linkedEntityId) {
		this.linkedEntityId = linkedEntityId;
	}

	@Column(name = "linkedentitytype")
	public String getLinkedEntityType() {
		return linkedEntityType;
	}

	public void setLinkedEntityType(String linkedEntityType) {
		this.linkedEntityType = linkedEntityType;
	}

	@Column(name = "createdon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
