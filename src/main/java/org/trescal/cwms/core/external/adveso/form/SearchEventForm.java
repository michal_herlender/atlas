package org.trescal.cwms.core.external.adveso.form;


import java.time.LocalDateTime;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.external.adveso.enums.EventStatusSearchForm;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchEventForm {
    @NotNull
    private EventStatusSearchForm status;
    @NotNull
    private String action;
    private LocalDateTime activityDateFrom;
    private LocalDateTime activityDateTo;
    private Integer coid;
    private String comptext;
    private String jobno;
    private String jobitemno;
    private Integer resultsPerPage;
    private Integer pageNo;
    private Map<Integer, Boolean> resendIds;

    public SearchEventForm() {
        // Actual defaults initialized in PagedResultSet
        resultsPerPage = 0;
        pageNo = 0;
    }
}