package org.trescal.cwms.core.external.adveso.service;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.db.AdvesoOverriddenActivitySettingService;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db.AdvesoTransferableActivityService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeDTO;

@Service
public class AdminActivitiesManagementServiceImpl implements AdminActivitiesManagementService {
	@Autowired
	private AdvesoTransferableActivityService advesoTransferableActivityService;
	@Autowired
	private AdvesoOverriddenActivitySettingService advesoOverriddenActivitySettingService;
	@Autowired
	private MessageSource messageSource;
	
    @Override
	public void initSubdivActivities(Model model, List<SystemDefaultTypeDTO> sdt, Integer subdivid, Locale locale)
	{
		List<AdvesoTransferableActivity> generalTransferableActivities = advesoTransferableActivityService.getAll();
		List<AdvesoOverriddenActivitySetting> activities = advesoOverriddenActivitySettingService.mergeSubdivOverriddenActivities(generalTransferableActivities, subdivid);
		model.addAttribute("activities", activities);
		model.addAttribute("islinktoadveso", isLinkToAdveso(sdt, locale));
	}
	
	private boolean isLinkToAdveso(List<SystemDefaultTypeDTO> list, Locale locale)
	{
		SystemDefaultTypeDTO item = list.stream().filter(s->s.getSystemDefault().getId().equals(SystemDefaultNames.LINK_TO_ADVESO)).findFirst().orElse(null);
		String yes = messageSource.getMessage("yes", null, locale);
		if(item != null)
			return item.getValue().equalsIgnoreCase(yes);
		else
			return false;
	}
}
