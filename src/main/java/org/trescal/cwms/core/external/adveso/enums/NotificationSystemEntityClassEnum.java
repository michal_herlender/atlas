/**
 * 
 */
package org.trescal.cwms.core.external.adveso.enums;

import java.io.Serializable;

/**
 * @author m.amnay
 */
public enum NotificationSystemEntityClassEnum implements Serializable {
	JobItemAction, 
	JobItem, 
	Job, 
	Delivery, 
	Instrument, 
	Company, 
	Subdiv, 
	Certificate, 
	Calibration,
	RepairCompletionReport;
}