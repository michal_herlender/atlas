package org.trescal.cwms.core.external.adveso.schedule;

import org.apache.commons.lang3.mutable.MutableInt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.dto.AdvesoActivityEventDTO;
import org.trescal.cwms.core.external.adveso.dto.AdvesoEventWrapperDTO;
import org.trescal.cwms.core.external.adveso.dto.AdvesoNotificationEventDTO;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;
import org.trescal.cwms.rest.adveso.service.RestClientAdvesoQueueService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Sends activities and notifications from the queues to the sharedServices
 * (destined to Adveso)
 */
@Component("AdvesoSynchronizerScheduledJob")
public class AdvesoSynchronizerScheduledJob {

	private final static Logger logger = LoggerFactory.getLogger(AdvesoSynchronizerScheduledJob.class);

	@Autowired
	private RestClientAdvesoQueueService restClientAdvesoQueueService;
	@Autowired
	private AdvesoJobItemActivityService advesoJobItemActivityService;
	@Autowired
	private NotificationSystemModelService notificationSystemModelService;
	@Autowired
	private ScheduledTaskService scheduledTaskService;
	

	/* this task is already configured as non concurrent */
	public void sync() {
		if (this.scheduledTaskService.scheduledTaskIsActive(this.getClass().getName(),
				Thread.currentThread().getStackTrace())) {
			// start adveso activities generation
			this.advesoJobItemActivityService.generateAdvesoActivities();
			
			// start adveso activities synchronization
			long startTime = System.currentTimeMillis();

			// get never sent activities
			List<AdvesoActivityEventDTO> neverSentActivities = advesoJobItemActivityService
					.getNeverSentActivitiesDTOs();
			List<AdvesoEventWrapperDTO> events = neverSentActivities.stream().map(AdvesoEventWrapperDTO::new).collect(Collectors.toList());

			// get never sent notifications
			List<AdvesoNotificationEventDTO> neverSentNotifications = notificationSystemModelService.getNeverSentDTOs();
			events.addAll(neverSentNotifications.stream().map(AdvesoEventWrapperDTO::new)
					.collect(Collectors.toList()));

			// order events
			Comparator<AdvesoEventWrapperDTO> comparator = Comparator.nullsFirst(Comparator.comparing(e -> restClientAdvesoQueueService.getCreatedOnDate(e), Comparator.nullsFirst(Comparator.naturalOrder())));

			// sort

			events.sort(comparator);

			// set ordre no
			MutableInt cnt = new MutableInt(0);
			events.forEach(e -> {
				cnt.increment();
				e.setOrderNo(cnt.intValue());
			});

			// send events
			restClientAdvesoQueueService.sendEvents(events);

			long finishTime = System.currentTimeMillis();
			StringBuilder result = new StringBuilder();
			result.append("Sent events= ").append(events.size());
			result.append(", time=");
			result.append((finishTime - startTime));
			result.append(" ms");
			logger.debug(result.toString());
			this.scheduledTaskService.reportOnScheduledTaskRun(this.getClass().getName(),
					Thread.currentThread().getStackTrace(), true, result.toString());
		}
	}

}
