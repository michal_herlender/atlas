package org.trescal.cwms.core.external.adveso.events;

import java.util.Date;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;

import lombok.Getter;

@Getter
public class RCRCompletionDateChangedEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;
	public static final NotificationSystemEntityClassEnum ENTITY_CLASS = NotificationSystemEntityClassEnum.RepairCompletionReport;
	public static final String FIELD_NAME = "repairCompletionDate";
	public static final NotificationSystemFieldTypeEnum FIELD_TYPE = NotificationSystemFieldTypeEnum.Date;

	private RepairCompletionReport rcr;
	private JobItem jobItem;
	private NotificationSystemOperationTypeEnum operationType;
	private Date newValue;
	private Date oldValue;

	public RCRCompletionDateChangedEvent(Object source, RepairCompletionReport rcr, JobItem jobItem, Date oldValue,
			Date newValue) {
		super(source);
		this.rcr = rcr;
		this.jobItem = jobItem;
		this.oldValue = oldValue;
		this.newValue = newValue;
		if (oldValue == null)
			this.operationType = NotificationSystemOperationTypeEnum.CREATE;
		else
			this.operationType = NotificationSystemOperationTypeEnum.UPDATE;
	}

}
