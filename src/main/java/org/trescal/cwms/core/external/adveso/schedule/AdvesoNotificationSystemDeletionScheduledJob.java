package org.trescal.cwms.core.external.adveso.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

/**
 * Delete already sent notifications that are older than one year
 */
@Component("AdvesoNotificationSystemDeletionScheduledJob")
public class AdvesoNotificationSystemDeletionScheduledJob {

	@Autowired
	private NotificationSystemModelService notificationSystemModelService;
	@Autowired
	private ScheduledTaskService scheduledTaskService;
	
	private static Logger logger = LoggerFactory.getLogger(AdvesoNotificationSystemDeletionScheduledJob.class); 

	public void deleteAlreadySentAdvesoNotificationsSystem() {
		if (this.scheduledTaskService.scheduledTaskIsActive(this.getClass().getName(),
				Thread.currentThread().getStackTrace())) {
			long startTime = System.currentTimeMillis();
			// Using logging level of INFO as not planned to run very often
			logger.info("Running deleteAlreadySentAdvesoNotificationsSystem()");
			
			int deleteCount = notificationSystemModelService.deleteSentAdvesoNotificationsSystem();
			
			long finishTime = System.currentTimeMillis();

			StringBuffer result = new StringBuffer();
			result.append("Deleted "+deleteCount+" notifications in ");
			result.append((finishTime-startTime)+" ms");
			logger.info(result.toString());
			this.scheduledTaskService.reportOnScheduledTaskRun(this.getClass().getName(), 
					Thread.currentThread().getStackTrace(), true, result.toString());
		}
		else {
			logger.info("Not running task, task is inactive or tasks globally inactive");
		}
	}
}
