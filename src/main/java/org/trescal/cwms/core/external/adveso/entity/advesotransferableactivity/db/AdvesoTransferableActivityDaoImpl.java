package org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db;


import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity_;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity_;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

@Repository
public class AdvesoTransferableActivityDaoImpl extends BaseDaoImpl<AdvesoTransferableActivity, Integer>
		implements AdvesoTransferableActivityDao {

	@Override
	protected Class<AdvesoTransferableActivity> getEntity() {
		return AdvesoTransferableActivity.class;
	}

	@Override
	public List<AdvesoActivityDTO> getDefaultActivities() {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AdvesoActivityDTO> query = builder.createQuery(AdvesoActivityDTO.class);
        
        Root<AdvesoTransferableActivity> ataRoot =  query.from(AdvesoTransferableActivity.class);
        Join<AdvesoTransferableActivity, ItemActivity> itemActivityJoin = ataRoot.join(AdvesoTransferableActivity_.itemActivity);
       	
		Predicate conjunction = builder.conjunction();
        conjunction.getExpressions().add(builder.equal(itemActivityJoin.get(ItemActivity_.active),true));
        //conjunction.getExpressions().add(builder.equal(ataRoot.get(AdvesoTransferableActivity_.isTransferredToAdveso),true));
		
        query.where(conjunction);

        query.select(builder.construct(AdvesoActivityDTO.class, 
        		itemActivityJoin.get(ItemActivity_.stateid.getName()),
        		itemActivityJoin.get(ItemActivity_.description.getName()),
        		ataRoot.get(AdvesoTransferableActivity_.isTransferredToAdveso.getName()),
        		ataRoot.get(AdvesoTransferableActivity_.isDisplayedOnJobOrderDetails.getName())
 				));

        return getEntityManager().createQuery(query).getResultList();
	}
	
	
}
