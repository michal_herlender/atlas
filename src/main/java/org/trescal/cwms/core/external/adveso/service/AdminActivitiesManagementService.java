package org.trescal.cwms.core.external.adveso.service;

import java.util.List;
import java.util.Locale;

import org.springframework.ui.Model;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeDTO;

public interface AdminActivitiesManagementService {
	public void initSubdivActivities(Model model, List<SystemDefaultTypeDTO> sdt, Integer subdivid, Locale locale);
}
