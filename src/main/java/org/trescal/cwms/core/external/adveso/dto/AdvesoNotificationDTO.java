package org.trescal.cwms.core.external.adveso.dto;


import lombok.Getter;
import lombok.Setter;
import org.apache.commons.text.StringEscapeUtils;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;

import java.util.Date;

@Setter @Getter
public class AdvesoNotificationDTO {
	
	private int id;
	private String fieldname;
	private String fieldvalue;
	private String entityclass;
	private String operationtype;
	private Boolean sent;
	private Integer jobItemId;
	private Integer jobItemNo;
	private String jobNo;
	private String lastErrorMessage;
	private Integer plantid;
	private Date createdOn;
	private Date sentOn;
	
	public AdvesoNotificationDTO(NotificationSystemModel notification) {
		this.id = notification.getId();
		this.fieldname = notification.getEntityIdFieldName();
		this.fieldvalue = notification.getEntityIdFieldValue();
		this.entityclass = notification.getEntityClass().name();
		this.operationtype = notification.getOperationType().name();
		this.sent = notification.getIsSent();
		this.lastErrorMessage = StringEscapeUtils.escapeHtml4(notification.getLastErrorMessage());
		this.createdOn = notification.getCreatedOn();
		this.sentOn = notification.getSentOn();
	}

	public AdvesoNotificationDTO(int id, String fieldname, String fieldvalue,
			NotificationSystemEntityClassEnum entityclass, NotificationSystemOperationTypeEnum operationtype,
			boolean isSent, String lastErrorMessage, Date createdOn, Integer plantid,Integer jobItemId,Integer jobItemNo,String jobNo,
			Date sentOn) {
		super();
		this.id = id;
		this.fieldname = fieldname;
		this.fieldvalue = fieldvalue;
		this.entityclass = entityclass.name();
		this.operationtype = operationtype.name();
		this.sent = isSent;
		this.lastErrorMessage = lastErrorMessage;
		this.createdOn = createdOn;
		this.plantid = plantid;
		this.jobItemId = jobItemId;
		this.jobItemNo = jobItemNo;
		this.jobNo = jobNo;
		this.sentOn = sentOn;
	}
}
