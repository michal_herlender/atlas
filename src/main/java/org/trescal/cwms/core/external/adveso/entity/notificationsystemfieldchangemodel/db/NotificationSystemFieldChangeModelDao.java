package org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;

public interface NotificationSystemFieldChangeModelDao extends BaseDao<NotificationSystemFieldChangeModel, Integer>{
	
}
