package org.trescal.cwms.core.external.portal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "portal")
public class Portal {
	
	private int portalId;
	private Portals portalName;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "portalId", nullable = false, unique = true)
	public int getPortalId() {
		return portalId;
	}
	
	@Column(name = "portalName")
	@Enumerated(EnumType.STRING)
	public Portals getPortalName() {
		return portalName;
	}

	public void setPortalId(int portalId) {
		this.portalId = portalId;
	}
	public void setPortalName(Portals portalName) {
		this.portalName = portalName;
	}
}