package org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

@Service
public class AdvesoTransferableActivityServiceImpl extends BaseServiceImpl<AdvesoTransferableActivity, Integer>
		implements AdvesoTransferableActivityService {

	@Autowired
	private AdvesoTransferableActivityDao dao;

	@Override
	protected BaseDao<AdvesoTransferableActivity, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public List<AdvesoActivityDTO> getDefaultActivities() {
		return dao.getDefaultActivities();
	}

	@Override
	@Cacheable(cacheNames = "cachedAllGeneralTransferableJobItemActivities")
	public List<AdvesoTransferableActivity> getAllFromCache() {
		return super.getAll();
	}

	@Override
	@CacheEvict(cacheNames = "cachedAllGeneralTransferableJobItemActivities", beforeInvocation = true, allEntries = true)
	public void save(AdvesoTransferableActivity object) {
		super.save(object);
	}

	@Override
	@CacheEvict(cacheNames = "cachedAllGeneralTransferableJobItemActivities", beforeInvocation = true, allEntries = true)
	public void update(AdvesoTransferableActivity object) {
		super.update(object);
	}

	@Override
	@CacheEvict(cacheNames = "cachedAllGeneralTransferableJobItemActivities", beforeInvocation = true, allEntries = true)
	public void delete(AdvesoTransferableActivity object) {
		super.delete(object);
	}

}
