package org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.external.adveso.dto.AdvesoJobItemActivityDTO;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity_;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting_;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity_;
import org.trescal.cwms.core.external.adveso.form.SearchEventForm;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction_;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault_;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication_;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity_;

@Repository
public class AdvesoJobItemActivityDaoImpl extends BaseDaoImpl<AdvesoJobItemActivity, Integer>
		implements AdvesoJobItemActivityDao {

	@Override
	protected Class<AdvesoJobItemActivity> getEntity() {
		return AdvesoJobItemActivity.class;
	}

	// alias are used to fix a problem in the query : duplicate names
	@Override
	public List<AdvesoJobItemActivity> getNeverSentActivities(int maxActivities) {

		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AdvesoJobItemActivity> query = builder.createQuery(AdvesoJobItemActivity.class);

		Root<AdvesoJobItemActivity> ajiaRoot = query.from(AdvesoJobItemActivity.class);
		ajiaRoot.alias("ajiaRoot");

		Join<AdvesoJobItemActivity, JobItemAction> jobItemActionJoin = ajiaRoot
				.join(AdvesoJobItemActivity_.jobItemAction.getName());
		ajiaRoot.alias("jobItemActionJoin");

		Join<JobItemAction, JobItem> jobItemJoin = jobItemActionJoin.join(JobItemAction_.jobItem.getName());
		ajiaRoot.alias("jobItemJoin");

		Join<JobItem, Job> jobJoin = jobItemJoin.join(JobItem_.job.getName());
		ajiaRoot.alias("jobJoin");

		Join<Job, Contact> contactJoin = jobJoin.join(Job_.con.getName());
		ajiaRoot.alias("contactJoin");

		Join<Contact, Subdiv> subdivJoin = contactJoin.join(Contact_.sub.getName());
		ajiaRoot.alias("subdivJoin");

		Join<Subdiv, SystemDefaultApplication> systemDefaultApplicationJoin = subdivJoin
				.join(Subdiv_.systemDefaults.getName());
		ajiaRoot.alias("systemDefaultApplicationJoin");

		Join<SystemDefaultApplication, SystemDefault> systemDefaultJoin = systemDefaultApplicationJoin
				.join(SystemDefaultApplication_.systemDefault.getName());
		ajiaRoot.alias("systemDefaultJoin");

		Join<JobItemAction, ItemActivity> itemActivityJoin = jobItemActionJoin.join(JobItemAction_.activity.getName());
		ajiaRoot.alias("itemActivityJoin");

		Join<ItemActivity, AdvesoTransferableActivity> advesoTransferableActivityJoin = itemActivityJoin
				.join(ItemActivity_.advesoTransferableActivities.getName());
		ajiaRoot.alias("advesoTransferableActivityJoin");

		Join<AdvesoTransferableActivity, AdvesoOverriddenActivitySetting> advesoOverriddenActivitySettingJoin = advesoTransferableActivityJoin
				.join(AdvesoTransferableActivity_.advesoOverriddenActivitiesSetting.getName(), JoinType.LEFT);
		ajiaRoot.alias("advesoOverriddenActivitySettingJoin");
		advesoOverriddenActivitySettingJoin
				.on(builder.equal(subdivJoin.get(Subdiv_.subdivid), advesoOverriddenActivitySettingJoin
						.get(AdvesoOverriddenActivitySetting_.subdiv.getName()).get(Subdiv_.subdivid.getName())));

		Predicate conjunction = builder.conjunction();

		// is not sent yet
		conjunction.getExpressions().add(builder.equal(ajiaRoot.get(AdvesoJobItemActivity_.isSent.getName()), 0));

		// make sure that is still linked to Adveso
		conjunction.getExpressions().add(
				builder.equal(systemDefaultJoin.get(SystemDefault_.id.getName()), SystemDefaultNames.LINK_TO_ADVESO));
		conjunction.getExpressions().add(
				builder.equal(systemDefaultApplicationJoin.get(SystemDefaultApplication_.value.getName()), "true"));

		// is activity transferable by setting overiden (subdiv level)
		Predicate hasTheSettingTransferableAtSubdivLevel = builder.isNotNull(advesoOverriddenActivitySettingJoin
				.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso.getName()));
		Predicate hasTheSettingTransferableAtSubdivLevelActive = builder.equal(advesoOverriddenActivitySettingJoin
				.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso.getName()), 1);
		Predicate isTransferableAtSubdivLevel = builder.and(hasTheSettingTransferableAtSubdivLevel,
				hasTheSettingTransferableAtSubdivLevelActive);

		// is activity transferable by setting overiden (subdiv level)
		Predicate hasTheSettingTransferable = builder.isNull(advesoOverriddenActivitySettingJoin
				.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso.getName()));
		Predicate hasTheSettingTransferableActive = builder.equal(
				advesoTransferableActivityJoin.get(AdvesoTransferableActivity_.isTransferredToAdveso.getName()), 1);
		Predicate isTransferable = builder.and(hasTheSettingTransferable, hasTheSettingTransferableActive);

		Predicate isTransferableAtGlobalOrSubdivLevels = builder.or(isTransferableAtSubdivLevel, isTransferable);
		conjunction.getExpressions().add(isTransferableAtGlobalOrSubdivLevels);

		query.select(ajiaRoot);
		query.where(conjunction);
		query.orderBy(builder.asc(ajiaRoot.get(AdvesoJobItemActivity_.id)));

		return getEntityManager().createQuery(query).setMaxResults(maxActivities).getResultList();
	}

	@Override
	public void findActivities(SearchEventForm form, PagedResultSet<AdvesoJobItemActivityDTO> prs) {
		this.completePagedResultSet(prs, AdvesoJobItemActivityDTO.class, cb -> cq -> {
			Root<AdvesoJobItemActivity> root = cq.from(AdvesoJobItemActivity.class);
			// Can be null, if job item action is deleted (due to job item deletion!) hence
			// left join
			Join<AdvesoJobItemActivity, JobItemAction> jia = root.join(AdvesoJobItemActivity_.jobItemAction,
					JoinType.LEFT);
			Join<JobItemAction, JobItem> ji = jia.join(JobItemAction_.jobItem, JoinType.LEFT);
			Join<JobItem, Job> job = ji.join(JobItem_.job, JoinType.LEFT);
			Join<Job, Contact> contact = job.join(Job_.con, JoinType.LEFT);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub, JoinType.LEFT);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.LEFT);
			Join<JobItem, Instrument> inst = ji.join(JobItem_.inst, JoinType.LEFT);
			Join<JobItemAction, ItemActivity> itemActivity = jia.join(JobItemAction_.activity, JoinType.LEFT);

			Predicate clauses = cb.conjunction();
			List<Expression<Boolean>> expressions = clauses.getExpressions();
			if (form.getActivityDateFrom() != null) {
				Date fromDate = DateTools.dateFromLocalDateTime(form.getActivityDateFrom());
				expressions.add(cb.greaterThanOrEqualTo(jia.get(JobItemAction_.startStamp), fromDate));
			}
			if (form.getActivityDateTo() != null) {
				Date toDate = DateTools.dateFromLocalDateTime(form.getActivityDateTo());
				expressions.add(cb.lessThanOrEqualTo(jia.get(JobItemAction_.startStamp), toDate));
			}
			if (form.getCoid() != null) {
				expressions.add(cb.equal(company.get(Company_.coid), form.getCoid()));
			}
			if (form.getJobno() != null && !form.getJobno().isEmpty()) {
				expressions.add(cb.equal(job.get(Job_.jobno), form.getJobno()));

			}
			if (form.getJobitemno() != null && !form.getJobitemno().isEmpty()) {
				expressions.add(cb.equal(ji.get(JobItem_.itemNo), form.getJobitemno()));
			}

			if (form.getStatus() != null) {
				switch ((form.getStatus())) {
				case PENDING:
					expressions.add(cb.equal(root.get(AdvesoJobItemActivity_.isSent), false));
					expressions.add(cb.isNull(root.get(AdvesoJobItemActivity_.lastErrorMessageOn)));
					break;
				case SUCCESS:
					expressions.add(cb.equal(root.get(AdvesoJobItemActivity_.isSent), true));
					break;
				case ERROR:
					expressions.add(cb.equal(root.get(AdvesoJobItemActivity_.isSent), false));
					expressions.add(cb.isNotNull(root.get(AdvesoJobItemActivity_.lastErrorMessageOn)));
					break;
				case ALL:
					break;
				}
			}

			cq.where(clauses);

			CompoundSelection<AdvesoJobItemActivityDTO> selection = cb.construct(AdvesoJobItemActivityDTO.class, 
					root.get(AdvesoJobItemActivity_.id),
					ji.get(JobItem_.itemNo),
					job.get(Job_.jobno),
					company.get(Company_.coname),
					inst.get(Instrument_.plantid),
					inst.get(Instrument_.plantno),
					itemActivity.get(ItemActivity_.description),		// English only
					jia.get(JobItemAction_.startStamp),
					root.get(AdvesoJobItemActivity_.sentOn),
					root.get(AdvesoJobItemActivity_.lastErrorMessageOn),
					root.get(AdvesoJobItemActivity_.lastErrorMessage));

			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(AdvesoJobItemActivity_.id)));

			return Triple.of(root, selection, order);
		});
	}

	@Override
	public List<AdvesoJobItemActivity> getAdvesoJobItemActivitiesByActivityId(JobItemAction jia) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AdvesoJobItemActivity> query = builder.createQuery(AdvesoJobItemActivity.class);

		Root<AdvesoJobItemActivity> ajiaRoot = query.from(AdvesoJobItemActivity.class);
		ajiaRoot.alias("ajiaRoot");

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions()
				.add(builder.equal(ajiaRoot.get(AdvesoJobItemActivity_.jobItemAction.getName()), jia));

		query.where(conjunction);
		query.select(ajiaRoot);

		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public Integer deleteSentJobItemActivities() {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -1);
		Date oneYearBeforeCurrentDate = cal.getTime();

		CriteriaDelete<AdvesoJobItemActivity> delete = builder.createCriteriaDelete(AdvesoJobItemActivity.class);
		Root<AdvesoJobItemActivity> root = delete.from(AdvesoJobItemActivity.class);
		delete.where(builder.and(builder.equal(root.get(AdvesoJobItemActivity_.isSent), true),
				builder.lessThan(root.get(AdvesoJobItemActivity_.sentOn), oneYearBeforeCurrentDate)));

		Query query = getEntityManager().createQuery(delete);
		return query.executeUpdate();
	}

	@Override
	public AdvesoJobItemActivity getCertificateAdvesoJobItemActivityByJIAction(JobItemAction jia) {
		List<AdvesoJobItemActivity> result = getResultList(cb -> {
			CriteriaQuery<AdvesoJobItemActivity> query = cb.createQuery(AdvesoJobItemActivity.class);
			Root<AdvesoJobItemActivity> ajiaRoot = query.from(AdvesoJobItemActivity.class);
			ajiaRoot.alias("ajiaRoot");

			Predicate conjunction = cb.conjunction();
			conjunction.getExpressions()
					.add(cb.and(cb.isNotNull(ajiaRoot.get(AdvesoJobItemActivity_.linkedEntityId.getName())), cb.and(
							cb.equal(ajiaRoot.get(AdvesoJobItemActivity_.jobItemAction.getName()), jia),
							cb.equal(ajiaRoot.get(AdvesoJobItemActivity_.linkedEntityType.getName()), "Certificate"))));
			query.where(conjunction);
			query.select(ajiaRoot);

			return query;
		});
		return result.size() > 0 ? result.get(0) : null;
	}

	@Override
	public void updateAfterSending(boolean success, Date date, String errorMessage, List<Integer> advesoActivityIds) {

		CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
		CriteriaUpdate<AdvesoJobItemActivity> cu = cb.createCriteriaUpdate(AdvesoJobItemActivity.class);
		cu.from(AdvesoJobItemActivity.class);
		cu.set(AdvesoJobItemActivity_.isSent, success);
		cu.set(AdvesoJobItemActivity_.lastErrorMessage, errorMessage);
		if (success)
			cu.set(AdvesoJobItemActivity_.sentOn, date);
		else
			cu.set(AdvesoJobItemActivity_.lastErrorMessageOn, date);
		cu.where(cu.getRoot().get(AdvesoJobItemActivity_.id).in(advesoActivityIds));

		this.getEntityManager().createQuery(cu).executeUpdate();
	}

	@Override
	public List<AdvesoJobItemActivity> getByIds(List<Integer> activitiyIds) {
		return getResultList(cb -> {
			CriteriaQuery<AdvesoJobItemActivity> cq = cb.createQuery(AdvesoJobItemActivity.class);
			Root<AdvesoJobItemActivity> root = cq.from(AdvesoJobItemActivity.class);
			cq.where(root.get(AdvesoJobItemActivity_.id).in(activitiyIds));
			return cq;
		});
	}
}
