package org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;

@Entity
@Table(name = "advesooverriddenactivitysetting")
public class AdvesoOverriddenActivitySetting extends Versioned {

	private int id;
	private Boolean isTransferredToAdveso;
	private Boolean isDisplayedOnJobOrderDetails;
	private AdvesoTransferableActivity advesoTransferableActivity;
	private Subdiv subdiv;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "advesotransferableactivityid", foreignKey=@ForeignKey(name="FK_advesosetting_advesotransferableactivityid"))
	public AdvesoTransferableActivity getAdvesoTransferableActivity() {
		return advesoTransferableActivity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid", foreignKey=@ForeignKey(name="FK_advesosetting_subdivid"))
	public Subdiv getSubdiv() {
		return subdiv;
	}

	public Boolean getIsTransferredToAdveso() {
		return isTransferredToAdveso;
	}

	public void setIsTransferredToAdveso(Boolean isTransferredToAdveso) {
		this.isTransferredToAdveso = isTransferredToAdveso;
	}

	public Boolean getIsDisplayedOnJobOrderDetails() {
		return isDisplayedOnJobOrderDetails;
	}

	public void setIsDisplayedOnJobOrderDetails(Boolean isDisplayedOnJobOrderDetails) {
		this.isDisplayedOnJobOrderDetails = isDisplayedOnJobOrderDetails;
	}

	public void setAdvesoTransferableActivity(AdvesoTransferableActivity advesoTransferableActivity) {
		this.advesoTransferableActivity = advesoTransferableActivity;
	}

	public void setSubdiv(Subdiv subdiv) {
		this.subdiv = subdiv;
	}
	
}
