package org.trescal.cwms.core.external.adveso.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class SearchEventFormValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(SearchEventForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		SearchEventForm form = (SearchEventForm) obj;
		super.validate(form, errors);

		/**
		 * JOBITEMNO VALIDATION
		 */
		if (form.getJobitemno() != null)
		{
			if (!form.getJobitemno().trim().isEmpty()
					&& !NumberTools.isAnInteger(form.getJobitemno().trim()))
			{
				// check that it converts into a number
				errors.rejectValue("jobitemno", "jobitemno", null, "The jobitemno given must be a number.");
			}
		}
	}
}