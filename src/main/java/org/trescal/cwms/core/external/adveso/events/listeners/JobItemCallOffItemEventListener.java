package org.trescal.cwms.core.external.adveso.events.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.events.JobItemCallOffItemEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Component
public class JobItemCallOffItemEventListener implements ApplicationListener<JobItemCallOffItemEvent> {

	@Autowired
	private NotificationSystemModelService notificationSystemModelService;

	@Override
	public void onApplicationEvent(JobItemCallOffItemEvent event) {

		JobItem jobItem = event.getJobItem();

		notificationSystemModelService.pushToQueue(JobItemCallOffItemEvent.ENTITY_CLASS, JobItemCallOffItemEvent.FIELD_NAME,
				JobItemCallOffItemEvent.OPERATION_TYPE, JobItemCallOffItemEvent.FIELD_TYPE, jobItem.getJobItemId(),
				jobItem.getInst(),null, jobItem.getJob().getCon());

	}

}
