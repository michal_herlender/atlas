package org.trescal.cwms.core.external.portal.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.external.portal.Portal;
import org.trescal.cwms.core.external.portal.Portals;

public interface PortalService extends BaseService<Portal,Integer> {
	
	public Portal get(Portals portal);
	
}
