package org.trescal.cwms.core.external.adveso.events.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db.NotificationSystemFieldChangeModelService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.events.JobItemDataUpdateEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Component
public class JobItemDataUpdateEventListener implements ApplicationListener<JobItemDataUpdateEvent> {

	@Autowired
	private NotificationSystemModelService notificationSystemModelService;
	@Autowired
	private NotificationSystemFieldChangeModelService notificationSystemFieldChangeModelService;

	@Override
	public void onApplicationEvent(JobItemDataUpdateEvent event) {

		JobItem ji = event.getJobItem();

		NotificationSystemModel notificationSystemModel = notificationSystemModelService.pushToQueue(
				JobItemDataUpdateEvent.ENTITY_CLASS, JobItemDataUpdateEvent.FIELD_NAME,
				JobItemDataUpdateEvent.OPERATION_TYPE, JobItemDataUpdateEvent.FIELD_TYPE,
				Integer.valueOf(ji.getJobItemId()), ji.getInst(), null, ji.getJob().getCon());

		NotificationSystemFieldChangeModel nsfcm = event.getNsfcm();
		nsfcm.setNotification(notificationSystemModel);
		notificationSystemFieldChangeModelService.save(nsfcm);

	}

}
