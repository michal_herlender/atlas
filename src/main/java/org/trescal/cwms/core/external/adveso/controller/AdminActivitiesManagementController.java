package org.trescal.cwms.core.external.adveso.controller;

import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.db.AdvesoOverriddenActivitySettingService;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db.AdvesoTransferableActivityService;
import org.trescal.cwms.core.external.adveso.service.AdminActivitiesManagementService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeDTO;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY,
		Constants.SESSION_ATTRIBUTE_USERNAME})
public class AdminActivitiesManagementController {
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private AdvesoTransferableActivityService advesoTransferableActivityService;
	@Autowired
	private AdvesoOverriddenActivitySettingService advesoOverriddenActivitySettingService;
	@Autowired
	private AdminActivitiesManagementService adminActivitiesManagementService;


	@RequestMapping(value="/subdivmanageactivities.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request,@RequestParam(value="subdivid") Integer subdivid,
			@RequestParam(value="idTranferrable") ArrayList<Integer> ids,
			@ModelAttribute("systemdefaulttypes") ArrayList<SystemDefaultTypeDTO> sdt,
			Locale locale,
			Model model) throws Exception
	{
		for (Integer id : ids) {
			AdvesoTransferableActivity ata = advesoTransferableActivityService.get(id);
			boolean istransReq = ServletRequestUtils.getBooleanParameter(request, "istrans_"+id, false);
			boolean isdispReq = ServletRequestUtils.getBooleanParameter(request, "isdisp_"+id, false);
			Integer idOverridden = ServletRequestUtils.getIntParameter(request, "idOverridden_"+id, 0);
			AdvesoOverriddenActivitySetting aoas = null;
			if(idOverridden != 0)
				aoas = advesoOverriddenActivitySettingService.get(idOverridden);
			
			if(aoas == null && (ata.getIsTransferredToAdveso() != istransReq || ata.getIsDisplayedOnJobOrderDetails() != isdispReq))
			{
				aoas = new AdvesoOverriddenActivitySetting();
				Subdiv subdiv = subdivService.get(subdivid);
				subdiv.getBusinessSettings();
				aoas.setAdvesoTransferableActivity(ata);
				aoas.setSubdiv(subdiv);
				aoas.setIsDisplayedOnJobOrderDetails(isdispReq);
				aoas.setIsTransferredToAdveso(istransReq);
				advesoOverriddenActivitySettingService.save(aoas);
			}
			else if(aoas != null && ata.getIsTransferredToAdveso() == istransReq && ata.getIsDisplayedOnJobOrderDetails() == isdispReq)
				advesoOverriddenActivitySettingService.delete(aoas);
			else if(aoas != null && (ata.getIsTransferredToAdveso() != istransReq || ata.getIsDisplayedOnJobOrderDetails() != isdispReq))
			{
				aoas.setIsDisplayedOnJobOrderDetails(isdispReq);
				aoas.setIsTransferredToAdveso(istransReq);
				advesoOverriddenActivitySettingService.update(aoas);
			}
		}
		adminActivitiesManagementService.initSubdivActivities(model, sdt, subdivid, locale);
		return new ModelAndView(new RedirectView("viewsub.htm?subdivid="+subdivid+"&ajax=subdiv&width=400"));
	}
	
	
	@RequestMapping(value = "/deleteOActivity.json", method = RequestMethod.GET)
	public @ResponseBody ResultWrapper delete(@RequestParam(value = "id", required = true) int id, @RequestParam(value="subdivid", required = false) Integer subdivid, Locale locale) {

		if(id != 0)
		{
		AdvesoOverriddenActivitySetting aoas = advesoOverriddenActivitySettingService.get(id);
		advesoOverriddenActivitySettingService.delete(aoas);

		return new ResultWrapper(true, "ok");
		}
		else
			return new ResultWrapper(false, "This activity doesn't existe in Subdivision Overridden activities");
	}
}