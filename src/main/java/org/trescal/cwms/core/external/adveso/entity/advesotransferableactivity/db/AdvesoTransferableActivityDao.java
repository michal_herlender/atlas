package org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db;


import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.rest.adveso.dto.AdvesoActivityDTO;

public interface AdvesoTransferableActivityDao extends BaseDao<AdvesoTransferableActivity, Integer>{
	
	public List<AdvesoActivityDTO> getDefaultActivities();
	
}
