package org.trescal.cwms.core.external.adveso.events.listeners;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db.NotificationSystemFieldChangeModelService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.NotificationSystemModel;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.external.adveso.events.CertificateStatusChangedEvent;
import org.trescal.cwms.core.external.adveso.events.UploadCertificateEvent;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;

@Component
public class CertificateStatusChangedEventListener implements ApplicationListener<CertificateStatusChangedEvent> {

	@Autowired
	private NotificationSystemModelService notificationSystemModelService;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private NotificationSystemFieldChangeModelService notificationSystemFieldChangeModelService;

	@Override
	public void onApplicationEvent(CertificateStatusChangedEvent event) {

		Certificate certificate = event.getCertificate();
		JobItem jobItem = event.getJobItem();
		NotificationSystemOperationTypeEnum operationType = event.getOperationType();

		boolean islinkToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
				jobItem.getJob().getCon().getSub().getSubdivid(),
				jobItem.getJob().getOrganisation().getComp().getCoid(),  new Date());

		if (islinkToAdveso) {
			NotificationSystemModel notification = notificationSystemModelService.pushToQueue(
					CertificateStatusChangedEvent.ENTITY_CLASS, CertificateStatusChangedEvent.FIELD_NAME, operationType,
					UploadCertificateEvent.FIELD_TYPE, certificate.getCertid(), null, jobItem, null);

			NotificationSystemFieldChangeModel nsfc = new NotificationSystemFieldChangeModel(
					CertificateStatusChangedEvent.FIELD_NAME, NotificationSystemFieldTypeEnum.String,
					event.getOldStatus(), event.getNewStatus(), certificate.getLastModified());
			nsfc.setNotification(notification);
			notificationSystemFieldChangeModelService.save(nsfc);

		}

	}

}
