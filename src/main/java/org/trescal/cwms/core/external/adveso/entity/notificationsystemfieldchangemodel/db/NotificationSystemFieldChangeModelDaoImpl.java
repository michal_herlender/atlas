package org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.NotificationSystemFieldChangeModel;

@Repository
public class NotificationSystemFieldChangeModelDaoImpl extends BaseDaoImpl<NotificationSystemFieldChangeModel, Integer>
		implements NotificationSystemFieldChangeModelDao {

	@Override
	protected Class<NotificationSystemFieldChangeModel> getEntity() {
		return NotificationSystemFieldChangeModel.class;
	}
	
	
}
