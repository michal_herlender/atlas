package org.trescal.cwms.core.workallocation.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;
import org.trescal.cwms.core.workallocation.engineerallocation.dto.EngineerAllocationDto;
import org.trescal.cwms.core.workallocation.engineerallocation.dto.ItemAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocateItemsToEngineerForm;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.EngineerAllocationMessageComparator;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.db.EngineerAllocationMessageService;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.dto.EngineerAllocationMessageDto;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;
import org.trescal.cwms.core.workflow.dto.SelectedJobDTO;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;
import org.trescal.cwms.spring.model.KeyValue;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
@Slf4j
public class AllocateItemsToEngineerController {
	@Value("#{props['cwms.config.jobitem.fast_track_turn']}")
	private int fastTrackTurn;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private DepartmentService deptServ;
	@Autowired
	private EngineerAllocationService eaServ;
	@Autowired
	private EngineerAllocationMessageService eamServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;


	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	/**
	 * Note - removed department membership tests for now (revisit later for
	 * managers?) and made subdiv aware 2016-06-23 GB
	 */
	@ModelAttribute("form")
	protected AllocateItemsToEngineerForm formBackingObject() {
		AllocateItemsToEngineerForm form = new AllocateItemsToEngineerForm();
		form.setItemAllocations(new AutoPopulatingList<>(ItemAllocation.class));
		return form;
	}

	@RequestMapping(value = "/allocateitemstoengineer.htm", method = RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("form") AllocateItemsToEngineerForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		Contact currentCon = this.userService.get(username).getCon();
		for (ItemAllocation ia : form.getItemAllocations()) {
			if (ia.isDeallocation()) {
				EngineerAllocation ea = this.eaServ.findActiveEngineerAllocationForItem(ia.getJobItemId());
				if (ea != null) {
					this.eaServ.deleteEngineerAllocation(ea);
				}
			} else {
				EngineerAllocation ea = new EngineerAllocation();
				ea.setAllocatedBy(currentCon);
				ea.setAllocatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
				ea.setAllocatedTo(this.contactService.get(ia.getPersonId()));
				ea.setItemAllocated(this.jiServ.findJobItem(ia.getJobItemId()));
				ea.setAllocatedFor(ia.getDate());
				ea.setTimeOfDay(ia.getTime());
				ea.setAllocatedUntil((ia.getDateUntil() != null) ? ia.getDateUntil() : ia.getDate());
				ea.setTimeOfDayUntil((ia.getTimeUntil() != null) ? ia.getTimeUntil() : ia.getTime());
				ea.setDept(this.deptServ.get(ia.getDeptId()));
				if ((ia.getMessage() != null) && !ia.getMessage().trim().isEmpty()) {
					EngineerAllocationMessage message = new EngineerAllocationMessage();
					message.setAllocation(ea);
					message.setMessage(ia.getMessage().trim());
					message.setSetBy(currentCon);
					message.setSetOn(new Date());
					ea.setMessages(new TreeSet<>(new EngineerAllocationMessageComparator()));
					ea.getMessages().add(message);
				}
				this.eaServ.insertEngineerAllocation(ea);
			}
		}
		return new RedirectView("allocateitemstoengineer.htm?loadtab=allocated-tab");
	}

	private Map<LocalDate, Map<TimeOfDay, List<EngineerAllocationDto>>> getEngineerAllocationDtos(Integer businessSubdivId,
																								  Locale locale) {
		List<EngineerAllocationDto> allocationDtos = this.eaServ.getAllActiveAllocationDtos(businessSubdivId, locale);
		List<EngineerAllocationMessageDto> messageDtos = this.eamServ.getAllActiveMessageDtos(businessSubdivId, locale);

		Map<Integer, EngineerAllocationDto> allocationMap = allocationDtos.stream()
			.collect(Collectors.toMap(EngineerAllocationDto::getEngineerAllocationId, dto -> dto));
		log.info("allocatedMap.size() : " + allocationMap.size());

		allocationDtos.stream().filter(ea -> ea.getAllocatedFor() == null)
			.forEach(ea -> log.error("Null allocatedFor"));
		allocationDtos.stream().filter(ea -> ea.getTimeOfDay() == null).forEach(ea -> log.error("Null timeOfDay"));

		Map<LocalDate, Map<TimeOfDay, List<EngineerAllocationDto>>> result = allocationDtos.stream()
			.collect(Collectors.groupingBy(EngineerAllocationDto::getAllocatedFor, TreeMap::new,
				Collectors.groupingBy(EngineerAllocationDto::getTimeOfDay)));

		// Populate messages
		for (EngineerAllocationMessageDto messageDto : messageDtos) {
			EngineerAllocationDto allocationDto = allocationMap.get(messageDto.getAllocationId());
			if (allocationDto == null)
				log.error("Allocation id not found in map : " + messageDto.getAllocationId());
			else {
				if (allocationDto.getMessages() == null)
					allocationDto.setMessages(new HashSet<>());
				allocationDto.getMessages().add(messageDto);
			}
		}

		return result;
	}

	/*
	 * Returns a Map indicating whether the contact can add/edit/delete allocations
	 * for the specified department id
	 */
	private Map<Integer, Boolean> getAuthorisations(Subdiv subdiv, Contact contact) {

		Map<Integer, Boolean> result = new HashMap<>();
		boolean authorisedAllDepts = this.authenticationService.hasRight(contact,
				Permission.ALLOCATE_ITEM_TO_SUBDIVISION, subdiv.getId());
		for (Department dept : subdiv.getDepartments()) {
			if (dept.getType().isProcedures()) {
				boolean authorisation = authorisedAllDepts;
				if (!authorisedAllDepts) {
					DepartmentMember member = contact.getMember().stream()
						.filter(dm -> dm.getDepartment().getDeptid().equals(dept.getId())).findFirst().orElse(null);
					authorisation = ((member != null) && member.getDepartmentRole().isManage());
				}
				result.put(dept.getDeptid(), authorisation);
			}
		}
		return result;
	}

	private Map<Department, SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>>> getItems(Subdiv subdiv) {
		Map<Department, SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>>> items = new LinkedHashMap<>();
		for (Department dept : subdiv.getDepartments()) {
			if (dept.getType().isProcedures()) {
				items.put(dept, this.jiServ.getActiveJobItemsForDeptReadyForWork(dept.getDeptid()));
			}
		}
		return items;
	}

	@RequestMapping(value = "/allocateitemstoengineer.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Subdiv subdiv = this.subdivService.get(subdivDto.getKey());
		model.addAttribute("allocatedDtos", getEngineerAllocationDtos(subdivDto.getKey(), locale));
		model.addAttribute("timeOfDayAM", TimeOfDay.AM);
		model.addAttribute("timeOfDayPM", TimeOfDay.PM);
		model.addAttribute("authorisations", getAuthorisations(subdiv, contact));
		model.addAttribute("items", getItems(subdiv));
		model.addAttribute(Constants.FAST_TRACK_TURN, this.fastTrackTurn);
		return "trescal/core/workflow/allocateitemstoengineer";
	}
}