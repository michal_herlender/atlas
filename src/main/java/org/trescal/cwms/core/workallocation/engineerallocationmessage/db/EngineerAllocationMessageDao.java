package org.trescal.cwms.core.workallocation.engineerallocationmessage.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.dto.EngineerAllocationMessageDto;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;

public interface EngineerAllocationMessageDao extends BaseDao<EngineerAllocationMessage, Integer> {
	
	List<EngineerAllocationMessageDto> getAllActiveMessageDtos(Integer businessSubdivId, Locale locale);
}