package org.trescal.cwms.core.workallocation.engineerallocation.form;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NewAllocateItemsToEngineerForm {

    private AllocateWorkToEngineerSearchForm searchForm;
    private BasketAllocationItemsForm  basketForm;
    private ItemAllocationForm dto;

    private AllocatedItemsSearchForm allocatedItemsSearchForm;
    private AllocatedItems allocatedItems;


}
