package org.trescal.cwms.core.workallocation.engineerallocationmessage;

import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;

import java.util.Comparator;

public class EngineerAllocationMessageComparator implements Comparator<EngineerAllocationMessage>
{
	@Override
	public int compare(EngineerAllocationMessage eam1, EngineerAllocationMessage eam2)
	{
		if ((eam1.getSetOn() != null) && (eam2.getSetOn() != null)
				&& !eam1.getSetOn().equals(eam2.getSetOn()))
		{
			return eam1.getSetOn().compareTo(eam2.getSetOn());
		}
		else
		{
			return Integer.compare(eam1.getId(), eam2.getId());
		}
	}
}
