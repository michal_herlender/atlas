package org.trescal.cwms.core.workallocation.engineerallocation.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;

import java.util.List;

@Getter
@Setter
public class ItemAllocationForm {

    private List<SelectedJobItemDTO> row ;
}
