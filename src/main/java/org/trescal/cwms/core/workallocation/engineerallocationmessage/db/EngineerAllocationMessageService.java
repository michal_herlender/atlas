package org.trescal.cwms.core.workallocation.engineerallocationmessage.db;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.dto.EngineerAllocationMessageDto;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;

public interface EngineerAllocationMessageService
{
	ResultWrapper ajaxAddEngineerAllocationMessage(int engineerAllocationId, String message, HttpSession httpSession);

	ResultWrapper ajaxDeleteEngineerAllocationMessage(int messageId, HttpSession httpSession);

	ResultWrapper ajaxEditEngineerAllocationMessage(int messageId, String message, HttpSession httpSession);

	void deleteEngineerAllocationMessage(EngineerAllocationMessage engineerallocationmessage);

	EngineerAllocationMessage findEngineerAllocationMessage(int id);

	List<EngineerAllocationMessage> getAllEngineerAllocationMessages();

	void insertEngineerAllocationMessage(EngineerAllocationMessage engineerallocationmessage);

	void updateEngineerAllocationMessage(EngineerAllocationMessage engineerallocationmessage);
	
	List<EngineerAllocationMessageDto> getAllActiveMessageDtos(Integer businessSubdivId, Locale locale);
	
}