package org.trescal.cwms.core.workallocation.engineerallocation.db;

import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction_;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.workallocation.engineerallocation.dto.EngineerAllocationDto;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation_;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocatedItemsSearchForm;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage_;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.StateGroupType;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Repository("EngineerAllocationDao")
public class EngineerAllocationDaoImpl extends BaseDaoImpl<EngineerAllocation, Integer>
	implements EngineerAllocationDao {

	@Override
	protected Class<EngineerAllocation> getEntity() {
		return EngineerAllocation.class;
	}

	@Override
	public EngineerAllocation findActiveEngineerAllocationForItem(int jobItemId) {
		return getFirstResult(cb -> {
			CriteriaQuery<EngineerAllocation> cq = cb.createQuery(EngineerAllocation.class);
			Root<EngineerAllocation> allocation = cq.from(EngineerAllocation.class);
			Join<EngineerAllocation, JobItem> jobItem = allocation.join(EngineerAllocation_.itemAllocated);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(allocation.get(EngineerAllocation_.active)));
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public List<EngineerAllocation> getActiveAllocationsForEngineer(int personId, LocalDate maxAllocatedFor) {
		
		return getResultList(cb -> {
			CriteriaQuery<EngineerAllocation> cq = cb.createQuery(EngineerAllocation.class);
			Root<EngineerAllocation> root = cq.from(EngineerAllocation.class);
			Join<EngineerAllocation, Contact> allocatedTo = root.join(EngineerAllocation_.allocatedTo, JoinType.INNER);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(EngineerAllocation_.active)));
			clauses.getExpressions().add(cb.equal(allocatedTo.get(Contact_.personid), personId));
			if (maxAllocatedFor != null) {
				clauses.getExpressions().add(cb.lessThanOrEqualTo(root.get(EngineerAllocation_.allocatedFor), maxAllocatedFor));
			}
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(EngineerAllocation_.allocatedFor)),
					cb.asc(root.get(EngineerAllocation_.timeOfDay)),
					cb.asc(root.get(EngineerAllocation_.id)));
			return cq;
		});
	}
	
	/**
	 * Returns a projection list of all allocated work for departments in the specified subdiv
	 * Model name in the specified locale
	 * 28 days out
	 */
	public List<EngineerAllocationDto> getAllActiveAllocationDtos(Integer businessSubdivId, Locale locale) {
		LocalDate dateUntil = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).plusDays(Constants.FUTURE_ALLOCATION_DAYS);

		return getResultList(cb -> {
			CriteriaQuery<EngineerAllocationDto> cq = cb.createQuery(EngineerAllocationDto.class);
            Root<EngineerAllocation> root = cq.from(EngineerAllocation.class);
            Join<EngineerAllocation, Department> dept = root.join(EngineerAllocation_.dept);
            Join<Department, Subdiv> subdiv = dept.join(Department_.subdiv);
            // Additional joins used for result fields
            Join<EngineerAllocation, Contact> allocatedBy = root.join(EngineerAllocation_.allocatedBy);
            Join<EngineerAllocation, Contact> allocatedTo = root.join(EngineerAllocation_.allocatedTo);
            Join<EngineerAllocation, JobItem> jobItem = root.join(EngineerAllocation_.itemAllocated);
            Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
            Join<JobItem, Job> job = jobItem.join(JobItem_.job);
            // Capability optional on job item (in future, use work requirement?)
            Join<JobItem, Capability> capability = jobItem.join(JobItem_.capability, JoinType.LEFT);
            Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
            Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model);
            // Translation may not be present
            Join<InstrumentModel, Translation> instrumentModeltranslation = instrumentModel.join(InstrumentModel_.nameTranslations, JoinType.LEFT);

            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), businessSubdivId));
            clauses.getExpressions().add(cb.isTrue(root.get(EngineerAllocation_.active)));
            clauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));
            clauses.getExpressions().add(cb.lessThanOrEqualTo(root.get(EngineerAllocation_.allocatedFor), dateUntil));
            clauses.getExpressions().add(cb.equal(instrumentModeltranslation.get(Translation_.locale), locale));

            cq.where(clauses);
            cq.select(cb.construct(EngineerAllocationDto.class,
                root.get(EngineerAllocation_.id),
                root.get(EngineerAllocation_.allocatedFor),
                root.get(EngineerAllocation_.allocatedOn),
                root.get(EngineerAllocation_.allocatedUntil),
                root.get(EngineerAllocation_.timeOfDay),
                root.get(EngineerAllocation_.timeOfDayUntil),
                allocatedBy.get(Contact_.personid),
                allocatedBy.get(Contact_.firstName),
                allocatedBy.get(Contact_.lastName),
                allocatedTo.get(Contact_.personid),
                allocatedTo.get(Contact_.firstName),
                allocatedTo.get(Contact_.lastName),
                dept.get(Department_.deptid),
                capability.get(Capability_.reference),
                job.get(Job_.jobid),
                job.get(Job_.jobno),
                jobItem.get(JobItem_.jobItemId),
                jobItem.get(JobItem_.itemNo),
                instrumentModeltranslation.get(Translation_.translation)
            ));
			
			return cq;
		});
	}

	@Override
	public Long countEngineerAllocationsBetweenDates(LocalDate startDate, LocalDate endDate, Integer allocatedSubdivId) {

		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<EngineerAllocation> root = cq.from(EngineerAllocation.class);
			Join<EngineerAllocation, Department> dept = root.join(EngineerAllocation_.dept);
			Join<Department, Subdiv> subdiv = dept.join(Department_.subdiv);

			// Matches long running allocation that may begin before the startDate AND end after the endDate
			Predicate innerClause = cb.conjunction();
			innerClause.getExpressions().add(cb.lessThan(root.get(EngineerAllocation_.allocatedFor), startDate));
			innerClause.getExpressions().add(cb.greaterThan(root.get(EngineerAllocation_.allocatedUntil), endDate));

			Predicate dateClause = cb.disjunction();
			dateClause.getExpressions().add(cb.between(root.get(EngineerAllocation_.allocatedFor), startDate, endDate));
			dateClause.getExpressions().add(cb.between(root.get(EngineerAllocation_.allocatedUntil), startDate, endDate));
			dateClause.getExpressions().add(innerClause);

			Predicate mainClause = cb.conjunction();
			mainClause.getExpressions().add(cb.isTrue(root.get(EngineerAllocation_.active)));
			mainClause.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), allocatedSubdivId));
			mainClause.getExpressions().add(dateClause);

			cq.where(mainClause);

			return cq.select(cb.count(root));

		});
	}

	public List<EngineerAllocation> getEngineerAllocationsBetweenDates(LocalDate startDate, LocalDate endDate, Subdiv subdiv) {

		return getResultList(
			cb -> {
				val cq = cb.createQuery(EngineerAllocation.class);
				val engineerAllocation = cq.from(EngineerAllocation.class);
				cq.where(
					cb.equal(engineerAllocation.join(EngineerAllocation_.dept).get(Department_.subdiv), subdiv),
					cb.isTrue(engineerAllocation.get(EngineerAllocation_.active)),
					cb.or(cb.between(engineerAllocation.get(EngineerAllocation_.allocatedFor), startDate, endDate), cb.between(engineerAllocation.get(EngineerAllocation_.allocatedUntil), startDate, endDate)),
					cb.and(cb.lessThanOrEqualTo(engineerAllocation.get(EngineerAllocation_.allocatedFor), startDate), cb.greaterThanOrEqualTo(engineerAllocation.get(EngineerAllocation_.allocatedUntil), startDate))
				);
				return cq;
			}

		);
	}

	@Override
	public EngineerAllocation findEnginerAllocationByJobItemAndDepartement(JobItem jobitem, Department dep) {
		
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<EngineerAllocation> cq = cb.createQuery(EngineerAllocation.class);
		Root<EngineerAllocation> eaRoot = cq.from(EngineerAllocation.class);
		
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(eaRoot.get(EngineerAllocation_.itemAllocated),jobitem));
		clauses.getExpressions().add(cb.equal(eaRoot.get(EngineerAllocation_.DEPT),dep));

		cq.select(eaRoot);
		cq.where(clauses);

		TypedQuery<EngineerAllocation> query = getEntityManager().createQuery(cq);
		if (query.getResultList().size() > 0)
			return query.getResultList().get(0);
		else
			return null;
	}

	@Override
	public List<SelectedJobItemDTO> getAllocatedJobItems(List<Integer> procIds, List<Integer> stateIds, Boolean isAllocatedToEngineer, Subdiv subdiv,
																	   StateGroupLinkType sglType, Integer addressId, AllocatedItemsSearchForm form) {
		Locale locale = LocaleContextHolder.getLocale();
		return getResultList(cb -> {
			CriteriaQuery<SelectedJobItemDTO> cq = cb.createQuery(SelectedJobItemDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Job, JobStatus> jobStatus = job.join(Job_.js, javax.persistence.criteria.JoinType.LEFT);
			Join<JobStatus, Translation> jobStatusName = jobStatus.join(JobStatus_.nametranslations,
					javax.persistence.criteria.JoinType.LEFT);
			jobStatusName.on(cb.equal(jobStatusName.get(Translation_.locale), locale));
			Join<Job, Contact> clientContact = job.join(Job_.con, javax.persistence.criteria.JoinType.LEFT);
			Join<Contact, Subdiv> clientSubdiv = clientContact.join(Contact_.sub);
			Join<Subdiv, Company> clientCompany = clientSubdiv.join(Subdiv_.comp);
			Join<Job, TransportOption> transportOutOption = job.join(Job_.returnOption,
					javax.persistence.criteria.JoinType.LEFT);
			Join<TransportOption, TransportMethod> transportOutMethod = transportOutOption.join(TransportOption_.method,
					javax.persistence.criteria.JoinType.LEFT);
			Join<TransportMethod, Translation> transportOutName = transportOutMethod
					.join(TransportMethod_.methodTranslation, javax.persistence.criteria.JoinType.LEFT);
			transportOutName.on(cb.equal(transportOutName.get(Translation_.locale), locale));
			Join<TransportOption, Subdiv> transportOutSubdiv = transportOutOption.join(TransportOption_.sub,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, Capability> procedure = jobItem.join(JobItem_.capability, javax.persistence.criteria.JoinType.LEFT);
			Join<Capability, CapabilityFilter> capabilityFilter = procedure.join(Capability_.capabilityFilters,javax.persistence.criteria.JoinType.LEFT);

			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst,
					javax.persistence.criteria.JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<InstrumentModel, Description> desc = model.join(InstrumentModel_.description);
			Join<Description,Translation> descTrasnlation = desc.join(Description_.translations,javax.persistence.criteria.JoinType.LEFT);
			descTrasnlation.on(cb.equal(descTrasnlation.get(Translation_.locale),locale));
			Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType,
					javax.persistence.criteria.JoinType.LEFT);
			Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType,
					javax.persistence.criteria.JoinType.LEFT);
			Join<ServiceType, Translation> serviceTypeShortName = serviceType.join(ServiceType_.shortnameTranslation,
					javax.persistence.criteria.JoinType.LEFT);
			serviceTypeShortName.on(cb.equal(serviceTypeShortName.get(Translation_.locale), locale));
			Join<JobItem, Contact> contractReviewBy = jobItem.join(JobItem_.lastContractReviewBy,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state, javax.persistence.criteria.JoinType.LEFT);
			Expression<String> itemStateName = joinTranslation(cb, itemState, ItemState_.translations, locale);
			Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, JobItemAction> action = jobItem.join(JobItem_.actions,
					javax.persistence.criteria.JoinType.LEFT);

			Subquery<Integer> actionSubQuery = cq.subquery(Integer.class);
			Root<JobItemAction> sqActions = actionSubQuery.from(JobItemAction.class);
			actionSubQuery.where(cb.equal(sqActions.get(JobItemAction_.jobItem), jobItem));
			actionSubQuery.select(cb.max(sqActions.get(JobItemAction_.id)));
			action.on(cb.equal(action.get(JobItemAction_.id), actionSubQuery));

			Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, Address> calAddress = jobItem.join(JobItem_.calAddr,
					javax.persistence.criteria.JoinType.LEFT);
			Subquery<Long> jiCountSq = cq.subquery(Long.class);
			Root<JobItem> jiCount = jiCountSq.from(JobItem.class);
			jiCountSq.where(cb.equal(jiCount.get(JobItem_.job), job));
			jiCountSq.select(cb.count(jiCount));



			Join<EngineerAllocation,JobItem> ea = jobItem.join(JobItem_.ENGINEER_ALLOCATIONS);
			Join<EngineerAllocation,EngineerAllocationMessage> eam = ea.join(EngineerAllocation_.MESSAGES);
			Join<EngineerAllocation,Contact> allocatedTo = ea.join(EngineerAllocation_.ALLOCATED_TO);




			cq.select(cb.construct(SelectedJobItemDTO.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), jiCountSq.getSelection(), jobItem.get(JobItem_.dueDate),
					jobItem.get(JobItem_.turn), job.get(Job_.jobid), job.get(Job_.jobno),
					jobStatusName.get(Translation_.translation), job.get(Job_.regDate), job.get(Job_.type),
					clientContact.get(Contact_.personid), clientContact.get(Contact_.firstName),
					clientContact.get(Contact_.lastName), clientContact.get(Contact_.active),
					clientSubdiv.get(Subdiv_.subdivid), clientSubdiv.get(Subdiv_.subname),
					clientSubdiv.get(Subdiv_.active), clientCompany.get(Company_.coid),
					clientCompany.get(Company_.coname), transportOutOption.get(TransportOption_.id),
					transportOutName.get(Translation_.translation), transportOutSubdiv.get(Subdiv_.subdivCode),
					procedure.get(Capability_.id), procedure.get(Capability_.reference), procedure.get(Capability_.name),
					instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
					instrument.get(Instrument_.serialno), model.get(InstrumentModel_.modelid),
					modelName.get(Translation_.translation), serviceTypeShortName.get(Translation_.translation),
					serviceType.get(ServiceType_.displayColour), serviceType.get(ServiceType_.displayColourFastTrack),
					contractReviewBy.get(Contact_.personid), contractReviewBy.get(Contact_.firstName),
					contractReviewBy.get(Contact_.lastName), itemState.get(ItemState_.stateid), itemStateName,
					action.get(JobItemAction_.endStamp), action.get(JobItemAction_.remark),
					action.get(JobItemAction_.id), allocatedSubdiv.get(Subdiv_.subdivid),
					allocatedSubdiv.get(Subdiv_.subdivCode),capabilityFilter.get(CapabilityFilter_.standardTime),
					ea.get(EngineerAllocation_.ALLOCATED_FOR),eam.get(EngineerAllocationMessage_.message),
					allocatedTo.get(Contact_.personid),allocatedTo.get(Contact_.firstName),allocatedTo.get(Contact_.lastName),
					ea.get(EngineerAllocation_.ID),instrument.get(Instrument_.plantid),eam.get(EngineerAllocationMessage_.id)
					));

			cq.distinct(true);
			Predicate clauses = cb.conjunction();
			Predicate allocatedSubdivPredicate = cb.equal(allocatedSubdiv, subdiv);
			Predicate currentSubdivPredicate = cb.equal(currentAddress.get(Address_.sub), subdiv);
			Predicate allocatedNotCurrentSubdivPredicate = cb.and(cb.equal(allocatedSubdiv, subdiv),
					cb.notEqual(currentAddress.get(Address_.sub), subdiv));
			Predicate calibrationSubdivPredicate = cb.disjunction();
			if (stateIds != null) {
				List<Integer> depGroups = StateGroup.getAllStateGroupsOfType(StateGroupType.DEPARTMENT).stream()
						.map(StateGroup::getId).collect(Collectors.toList());
				clauses.getExpressions().add(stateGroupLink.get(StateGroupLink_.groupId).in(depGroups));
				allocatedSubdivPredicate = cb.and(allocatedSubdivPredicate,
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.ALLOCATED_SUBDIV));
				currentSubdivPredicate = cb.and(currentSubdivPredicate,
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.CURRENT_SUBDIV));
				allocatedNotCurrentSubdivPredicate = cb.and(allocatedNotCurrentSubdivPredicate, cb.equal(
						stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV));
				calibrationSubdivPredicate = cb.and(cb.equal(calAddress.get(Address_.sub), subdiv),
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.CALIBRATION_SUBDIV));
			}
			if (sglType == null) {
				Predicate sglDisjunction = cb.disjunction();
				sglDisjunction.getExpressions().add(allocatedSubdivPredicate);
				sglDisjunction.getExpressions().add(currentSubdivPredicate);
				sglDisjunction.getExpressions().add(allocatedNotCurrentSubdivPredicate);
				sglDisjunction.getExpressions().add(calibrationSubdivPredicate);
				clauses.getExpressions().add(sglDisjunction);
			}
			if (procIds != null)
				if (procIds.isEmpty())
					clauses.getExpressions().add(cb.isNull(procedure.get(Capability_.id)));
				else
					clauses.getExpressions().add(procedure.get(Capability_.id).in(procIds));
			if (stateIds != null) {
				if (stateIds.isEmpty())
					clauses.getExpressions().add(cb.isNull(itemState.get(ItemState_.stateid)));
				else
					clauses.getExpressions().add(itemState.get(ItemState_.stateid).in(stateIds));
			} else
				clauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));


			if (isAllocatedToEngineer != null) {
				Subquery<Long> engineerAllocationSq = cq.subquery(Long.class);
				Root<EngineerAllocation> engineerAllocation = engineerAllocationSq.from(EngineerAllocation.class);
				Join<EngineerAllocation, JobItem> allocatedItem = engineerAllocation
						.join(EngineerAllocation_.itemAllocated);
				engineerAllocationSq.where(cb.equal(allocatedItem, jobItem));
				engineerAllocationSq.select(cb.count(engineerAllocation));
				if (isAllocatedToEngineer)
					clauses.getExpressions().add(cb.notEqual(engineerAllocationSq, 0L));
				else
					clauses.getExpressions().add(cb.equal(engineerAllocationSq, 0L));
			}

			if(form.getClientCompanyId()!=null){
				clauses.getExpressions().add(cb.equal(clientCompany.get(Company_.coid),form.getClientCompanyId()));
			}
			if(form.getClientSubdivId()!=null){
				clauses.getExpressions().add(cb.equal(clientSubdiv.get(Subdiv_.subdivid),form.getClientSubdivId()));
			}
			if(form.getServiceTypeId()!=null){
				clauses.getExpressions().add(cb.equal(serviceType.get(ServiceType_.serviceTypeId),form.getServiceTypeId()));
			}
			if(form.getJobtype()!=null){
				clauses.getExpressions().add(cb.equal(job.get(Job_.type),form.getJobtype()));
			}

			if(form.getDescId()!=null){
				clauses.getExpressions().add(cb.equal(desc.get(Description_.id),form.getDescId()));
			}
			if(form.getRangeDueDate()==true){
				clauses.getExpressions().add(cb.between(jobItem.get(JobItem_.dueDate),form.getAweDueDate1(),form.getAwedueDate2()));
			}else{
				if(form.getAweDueDate1()!=null)
					clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.dueDate),form.getAweDueDate1()));
			}

			if(form.getRangeProdDate()==true){
				clauses.getExpressions().add(cb.between(ea.get(EngineerAllocation_.ALLOCATED_FOR),form.getAweProdDate1(),form.getAweProdDate2()));
			}else{
				if(form.getAweProdDate1()!=null){
					clauses.getExpressions().add(cb.equal(ea.get(EngineerAllocation_.ALLOCATED_FOR),form.getAweProdDate1()));
				}
			}

			if(form.getSubdivEngineerId()!=null){
				clauses.getExpressions().add(cb.equal(ea.get(EngineerAllocation_.ALLOCATED_TO),form.getSubdivEngineerId()));
			}

			if (form.getBarcode() != null) {
				clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.plantid),form.getBarcode()));
			}

			cq.where(clauses);
			cq.orderBy(cb.asc(job.get(Job_.jobno)), cb.asc(jobItem.get(JobItem_.itemNo)));
			return cq;
		});
	}

	@Override
	public List<EngineerAllocation> getActiveAllocationsForEngineerByDate(int personId, LocalDate date) {
		return getResultList(cb -> {
			CriteriaQuery<EngineerAllocation> cq = cb.createQuery(EngineerAllocation.class);
			Root<EngineerAllocation> root = cq.from(EngineerAllocation.class);
			Join<EngineerAllocation, Contact> allocatedTo = root.join(EngineerAllocation_.allocatedTo, JoinType.INNER);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(EngineerAllocation_.active)));
			clauses.getExpressions().add(cb.equal(allocatedTo.get(Contact_.personid), personId));
			if (date != null) {
				clauses.getExpressions().add(cb.equal(root.get(EngineerAllocation_.allocatedFor), date));
			}
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(EngineerAllocation_.allocatedFor)));

			return cq;
		});
	}

}