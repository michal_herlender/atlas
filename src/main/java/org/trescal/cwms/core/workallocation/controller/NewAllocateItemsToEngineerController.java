package org.trescal.cwms.core.workallocation.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.procedure.dto.CapabilityCategoryDTO;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.workallocation.engineerallocation.AutoSheduling;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.form.*;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.db.EngineerAllocationMessageService;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;
import org.trescal.cwms.core.workflow.dto.SelectedJobDTO;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME ,Constants.SESSION_ATTRIBUTE_COMPANY})
public class NewAllocateItemsToEngineerController {

    @Autowired
    private DepartmentService deptServ;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private CapabilityCategoryService capabilityCategoryService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private EngineerAllocationService engineerAllocationService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private UserService userService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private EngineerAllocationMessageService engineerAllocationMessageService;


    public static final String FORM_NAME = "form";
    public static final String VIEW_NAME ="trescal/core/workallocation/newallocateitemtoengineer";
    public static final String OVERLAY_NAME ="trescal/core/workallocation/reallocatitem_overlay";


    @InitBinder(FORM_NAME)
    public void initBinder(WebDataBinder binder) {
        binder.setAutoGrowCollectionLimit(10000);
        binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
    }

    @ModelAttribute(FORM_NAME)
    protected NewAllocateItemsToEngineerForm formBackingObject() {
        NewAllocateItemsToEngineerForm form = new NewAllocateItemsToEngineerForm();
        return form;
    }

    @ModelAttribute("jobtypes")
    Set <JobType> getJobTypes(){
        return JobType.getActiveJobTypes();
    }

    @ModelAttribute("AutoSheduling")
    AutoSheduling[] getAutoSheduling(){
        return AutoSheduling.values();
    }

    @ModelAttribute("labs")
    List<Department> getDepartements(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto){
        return this.deptServ.getAllSubdivDepartments(subdivDto.getKey()).stream().filter(dep -> dep.getType().isProcedures()).collect(Collectors.toList());
    }

    @ModelAttribute("serviceTypes")
    List<ServiceType> getServiceTypes(){
        return this.serviceTypeService.getAllCalServiceTypes();
    }

    @ModelAttribute("clientCompanies")
    List<CompanyProjectionDTO> getClientCompaniesDTosFromSubdiv(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto){
        return this.companyService.getClientCompaniesDTosFromSubdiv(this.subdivService.get(subdivDto.getKey()));
    }

    @ModelAttribute("subEngineers")
    List<Contact> getSubdivContacts(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto){
        return this.contactService.getAllSubdivContacts(subdivDto.getKey());
    }


    @RequestMapping(value = "/newallocateitemstoengineer.htm", method = RequestMethod.GET)
    protected String referenceData() throws Exception {
        return VIEW_NAME;
    }


    @RequestMapping(value = "/newallocateitemstoengineer.htm",params = {"submit", "!addtobasket","!reset","!allocate","!allocated","!save"}, method = RequestMethod.POST)
    protected String search(Model model, @ModelAttribute(FORM_NAME) NewAllocateItemsToEngineerForm form, BindingResult bindingResult,
                            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto ) throws Exception {
        model.addAttribute("items", this.jobItemService.getActiveJobItemsReadyForAllocation(this.subdivService.get(subdivDto.getKey()),form.getSearchForm()));

        if(form.getSearchForm().getClientCompanyId()!=null){
            model.addAttribute("subdivs",this.subdivService.getSubdivProjectionDTOsByCompany(form.getSearchForm().getClientCompanyId()));
        }
        if(form.getSearchForm().getLaboratoryId()!=null){
            model.addAttribute("sublabs",this.capabilityCategoryService.getAll(form.getSearchForm().getLaboratoryId()));
        }


        return VIEW_NAME;
    }

    @RequestMapping(value = "/newallocateitemstoengineer.htm",params = {"!submit", "addtobasket","!reset","!allocate","!allocated","!save"}, method = RequestMethod.POST)
    protected String addItemsToBasket(Model model,
                                            @ModelAttribute(FORM_NAME) NewAllocateItemsToEngineerForm form , BindingResult bindingResult,
                                            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                        @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companydto) throws Exception {

            List<SelectedJobItemDTO> dtos = form.getDto().getRow().stream().filter(dt -> dt.getCheck() == true).collect(Collectors.toList());
            Set<Integer> procsIds   =  dtos.stream().map(SelectedJobItemDTO::getProcedureId).collect(Collectors.toSet());
            List<Integer> tmcs = dtos.stream().filter(dt -> dt.getTmc()!=null).map(SelectedJobItemDTO::getTmc).collect(Collectors.toList());

            model.addAttribute("subdivEngineer",this.capabilityService.getAutorizedContactFromCapabilities(procsIds,subdivDto.getKey()));
            model.addAttribute("businessCompany",this.capabilityService.getCapabilitieInSubdivsOfBc(procsIds,companydto.getKey()));
            model.addAttribute("basketItems", this.engineerAllocationService.groupeSelectedItems(dtos));
            model.addAttribute("basketsize",dtos.size());
            model.addAttribute("sumOfTmcs",tmcs.stream().filter(t->t!=null).reduce(0,Integer::sum));
            model.addAttribute("items", this.engineerAllocationService.groupeSelectedItems(form.getDto().getRow()));
        return VIEW_NAME;
    }



    @RequestMapping(value = "/newallocateitemstoengineer.htm",params = {"!submit", "!addtobasket","!reset","allocate","!allocated","!save"}, method = RequestMethod.POST)
    protected String allocateItemsToEngineer(Model model,
                                             @Valid @ModelAttribute(FORM_NAME) NewAllocateItemsToEngineerForm form, BindingResult bindingResult,
                                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto ,
                                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto ) throws Exception {

                this.engineerAllocationService.allocateItemsToEngineer(form.getBasketForm(),
                        this.userService.get(username).getCon(),this.subdivService.get(subdivDto.getKey()),companyDto.getKey());

        return VIEW_NAME;
    }

    @RequestMapping(value = "/newallocateitemstoengineer.htm",params = {"!submit", "!addtobasket","!reset","!allocate","allocated","!save"}, method = RequestMethod.POST)
    protected String getAllocatedJobItems(Model model,
                                             @ModelAttribute(FORM_NAME) NewAllocateItemsToEngineerForm form, BindingResult bindingResult,
                                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto ,
                                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {



        Map<LocalDate,SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>>> groupedMap =this.engineerAllocationService.getGroupedAllocatedItems(
                this.subdivService.get(subdivDto.getKey()),form.getAllocatedItemsSearchForm());


        Map<LocalDate , Integer> SumOfTmcsByDate = new TreeMap<>();
        Map<LocalDate, Integer> tottalItemsPerdate = new TreeMap<>();

        for(LocalDate date : groupedMap.keySet()){
            Integer sumOfTmc = 0;
            Integer totalItems = 0;
            for(SelectedJobDTO job : groupedMap.get(date).keySet()){
                totalItems += groupedMap.get(date).get(job).size();
                for(SelectedJobItemDTO dto : groupedMap.get(date).get(job)){
                    sumOfTmc += dto.getTmc();
                }
            }
            SumOfTmcsByDate.put(date,sumOfTmc);
            tottalItemsPerdate.put(date,totalItems);
        }

        model.addAttribute("allocatedItems",groupedMap);
        model.addAttribute("SumOfTmcsByDate",SumOfTmcsByDate);
        model.addAttribute("tottalItemsPerdate",tottalItemsPerdate);
        return VIEW_NAME;
    }

    @RequestMapping(value = "/newallocateitemstoengineer.htm",params = {"!submit", "!addtobasket","!reset","!allocate","!allocated","save"}, method = RequestMethod.POST)
    protected String saveAllocateItemsToEngineer(Model model,
                                             @ModelAttribute(FORM_NAME) NewAllocateItemsToEngineerForm form, BindingResult bindingResult,
                                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto ,
                                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {


        for(SelectedJobItemDTO dto : form.getAllocatedItems().getRow().stream().filter(dt -> dt.getCheck()==true).collect(Collectors.toList())){
            EngineerAllocation ea = this.engineerAllocationService.findEngineerAllocation(dto.getAllocationId());
            ea.setAllocatedFor(dto.getProduction_date());

            EngineerAllocationMessage eam =this.engineerAllocationMessageService.findEngineerAllocationMessage(dto.getAllocationMessageId());
            eam.setMessage(dto.getItem_comment());
            this.engineerAllocationMessageService.updateEngineerAllocationMessage(eam);
            this.engineerAllocationService.updateEngineerAllocation(ea);
        }
        return getAllocatedJobItems(model,form,bindingResult,subdivDto,username);
    }


    @RequestMapping(value = "/reallocteoverlay.htm", method = RequestMethod.GET)
    protected String deallocateOrReallocateItem(Model model, @RequestParam(name = "allocationId", required = true) Integer allocationId,
                                                @RequestParam(name = "EngineerId", required = true) Integer EngineerId,
                                                @RequestParam(name = "capabiltyId", required = true) Integer capabiltyId,
                                                @RequestParam(name = "allocationMessageId", required = true) Integer allocationMessageId,
                                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto ) throws Exception {

        Set<Integer> procsIds = new TreeSet<>();
        procsIds.add(capabiltyId);
        model.addAttribute("subdivEngineer",this.capabilityService.getAutorizedContactFromCapabilities(procsIds,subdivDto.getKey()));
        model.addAttribute("allocationId",allocationId);
        model.addAttribute("allocationMessageId",allocationMessageId);
        return OVERLAY_NAME;
    }


    @RequestMapping(value = "/reallocation.json", method = RequestMethod.GET)
    public @ResponseBody ResultWrapper reallocation(@RequestParam(name = "allocationId", required = true) Integer allocationId,
                                                    @RequestParam(name = "engineerId", required = true) Integer engineerId,
                                                    @RequestParam(name = "comment", required = true) String comment,
                                                    @RequestParam(name = "allocationMessageId", required = true) Integer allocationMessageId,
                                                    @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {

        EngineerAllocation ea = this.engineerAllocationService.findEngineerAllocation(allocationId);
        ea.setAllocatedTo(this.contactService.get(engineerId));

        EngineerAllocationMessage eam =this.engineerAllocationMessageService.findEngineerAllocationMessage(allocationMessageId);
        eam.setMessage(comment);
        this.engineerAllocationMessageService.updateEngineerAllocationMessage(eam);
        this.engineerAllocationService.updateEngineerAllocation(ea);

        return  new ResultWrapper(true, "ok");
    }

    @RequestMapping(value = "/deleteallocation.json", method = RequestMethod.GET)
    public @ResponseBody ResultWrapper deleteAllocation(@RequestParam(name = "allocationId", required = false) Integer allocationId) {
        this.engineerAllocationService.deleteEngineerAllocation(this.engineerAllocationService.findEngineerAllocation(allocationId));
        return  new ResultWrapper(true, "ok");
    }




    @RequestMapping(value = "/getSubLaboratoriesByLabId.json", method = RequestMethod.GET)
    public @ResponseBody
    List<CapabilityCategoryDTO> getSubLaboratoriesByLabId(@RequestParam(value = "LabId", required = true) int LabId) {
        return this.capabilityCategoryService.getAll(LabId);
    }

    @RequestMapping(value = "/getSubdivsByCoid.json", method = RequestMethod.GET)
    public @ResponseBody
    List<SubdivProjectionDTO> getSubdivs(@RequestParam(value = "coid", required = true) int coid) {
        return this.subdivService.getSubdivProjectionDTOsByCompany(coid);
    }




}
