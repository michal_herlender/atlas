package org.trescal.cwms.core.workallocation.engineerallocation.entity;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.EngineerAllocationMessageComparator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "engineerallocation")
public class EngineerAllocation {
    private boolean active;
    private Contact allocatedBy;
    private LocalDate allocatedFor;
    private LocalDate allocatedOn;
    private Contact allocatedTo;
    private LocalDate allocatedUntil;
    private Department dept;
    private int id;
    private JobItem itemAllocated;
    private Set<EngineerAllocationMessage> messages;
    private TimeOfDay timeOfDay;
    private TimeOfDay timeOfDayUntil;
    private Integer estimatedAllocationTime;

    public EngineerAllocation() {
        this.active = true;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "allocatedbyid", nullable = false)
    public Contact getAllocatedBy() {
        return this.allocatedBy;
    }

    public void setAllocatedBy(Contact allocatedBy) {
        this.allocatedBy = allocatedBy;
    }

    @NotNull
    @Column(name = "allocatedfor", nullable = false, columnDefinition = "date")
    public LocalDate getAllocatedFor() {
        return this.allocatedFor;
    }

    public void setAllocatedFor(LocalDate allocatedFor) {
        this.allocatedFor = allocatedFor;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "allocatedtoid", nullable = false)
    public Contact getAllocatedTo() {
        return this.allocatedTo;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deptid", nullable = false)
    public Department getDept() {
        return this.dept;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    public void setAllocatedTo(Contact allocatedTo) {
        this.allocatedTo = allocatedTo;
    }

    @NotNull
    @Column(name = "allocatedon", nullable = false, columnDefinition = "date")
    public LocalDate getAllocatedOn() {
        return this.allocatedOn;
    }

    public void setAllocatedOn(LocalDate allocatedOn) {
        this.allocatedOn = allocatedOn;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jobitemid", nullable = false)
    public JobItem getItemAllocated() {
        return this.itemAllocated;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "allocation")
    @SortComparator(EngineerAllocationMessageComparator.class)
    public Set<EngineerAllocationMessage> getMessages() {
        return this.messages;
    }

    @Column(name = "allocateduntil", columnDefinition = "date" ,nullable = true)
    public LocalDate getAllocatedUntil() {
        return this.allocatedUntil;
    }

    public void setAllocatedUntil(LocalDate allocatedUntil) {
        this.allocatedUntil = allocatedUntil;
    }

    @Column(name = "active", nullable = false, columnDefinition = "bit")
    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "timeofday", nullable = true)
    public TimeOfDay getTimeOfDay() {
        return this.timeOfDay;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "timeofdayuntil", nullable = true )
    public TimeOfDay getTimeOfDayUntil() {
        return this.timeOfDayUntil;
    }

    public void setDept(Department dept) {
        this.dept = dept;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setItemAllocated(JobItem itemAllocated) {
        this.itemAllocated = itemAllocated;
    }

	public void setMessages(Set<EngineerAllocationMessage> messages)
	{
		this.messages = messages;
	}

	public void setTimeOfDay(TimeOfDay timeOfDay)
	{
		this.timeOfDay = timeOfDay;
	}

	public void setTimeOfDayUntil(TimeOfDay timeOfDayUntil)
	{
		this.timeOfDayUntil = timeOfDayUntil;
	}


    @Column(name = "estimatedallocationtime")
    public Integer getEstimatedAllocationTime() {
        return estimatedAllocationTime;
    }

    public void setEstimatedAllocationTime(Integer estimatedAllocationTime) {
        this.estimatedAllocationTime = estimatedAllocationTime;
    }
}
