package org.trescal.cwms.core.workallocation.engineerallocation.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workallocation.engineerallocation.dto.EngineerAllocationDto;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocateWorkToEngineerSearchForm;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocatedItemsSearchForm;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

public interface EngineerAllocationDao extends BaseDao<EngineerAllocation, Integer> {
    EngineerAllocation findActiveEngineerAllocationForItem(int jobItemId);

    /**
     * Converted method to JPA and added maxAllocatedFor as a query parameter
     * (keeping logic out of DAO) - this should be refactored to EngineerAllocationDto for performance
     * 
     * @param personId (mandatory) person that the allocations are for
     * @param maxAllocatedFor LocalDate (optional) date up to which to return allocations 
     * @return List
     */
    List<EngineerAllocation> getActiveAllocationsForEngineer(int personId, LocalDate maxAllocatedFor);

    List<EngineerAllocationDto> getAllActiveAllocationDtos(Integer businessSubdivId, Locale locale);

    Long countEngineerAllocationsBetweenDates(LocalDate startDate, LocalDate endDate, Integer allocatedSubdivId);

    List<EngineerAllocation> getEngineerAllocationsBetweenDates(LocalDate startDate, LocalDate endDate, Subdiv subdiv);

    EngineerAllocation findEnginerAllocationByJobItemAndDepartement(JobItem jobitem, Department dep);

    public List<SelectedJobItemDTO> getAllocatedJobItems(List<Integer> procIds, List<Integer> stateIds, Boolean isAllocatedToEngineer,
                                                                       Subdiv subdiv, StateGroupLinkType sglType, Integer addressId, AllocatedItemsSearchForm form);

    public List<EngineerAllocation> getActiveAllocationsForEngineerByDate(int personId, LocalDate date);

}