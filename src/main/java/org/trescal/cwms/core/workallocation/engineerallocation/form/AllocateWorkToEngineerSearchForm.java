package org.trescal.cwms.core.workallocation.engineerallocation.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.workflow.dto.SelectedJobDTO;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;

@Getter
@Setter
public class AllocateWorkToEngineerSearchForm {

   private JobType jobtype;
   private Integer laboratoryId;
   private Integer subLabId;
   private Integer descId;
   private String desc;
   private Boolean rangeDueDate;
   private LocalDate aweDueDate1;
   private LocalDate awedueDate2;
   private Integer clientCompanyId;
   private Integer clientSubdivId;
   private Integer serviceTypeId;

}
