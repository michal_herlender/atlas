package org.trescal.cwms.core.workallocation.engineerallocation.dto;

import java.time.LocalDate;
import java.util.Set;

import org.trescal.cwms.core.company.entity.contact.ContactFormatTools;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.dto.EngineerAllocationMessageDto;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EngineerAllocationDto {
	// Fields from EngineerAllocation
	private Integer engineerAllocationId;
	private LocalDate allocatedFor;
	private LocalDate allocatedOn;
	private LocalDate allocatedUntil;
	private TimeOfDay timeOfDay;
	private TimeOfDay timeOfDayUntil;
	// Fields from Person joins
	private Integer allocatedById;
	private String allocatedByFirstName;
	private String allocatedByLastName;
	private Integer allocatedToId;
	private String allocatedToFirstName;
	private String allocatedToLastName;
	// Fields from other joins
	private Integer deptId;
	private String capabilityReference;
	private Integer jobId;
	private String jobNumber;
	private Integer jobItemId;
	private Integer jobItemNumber;
	private String modelName;
	// Set after initial query	
	private Set<EngineerAllocationMessageDto> messages;
	
	// Constructor for JPA projection
	public EngineerAllocationDto(Integer engineerAllocationId, LocalDate allocatedFor, LocalDate allocatedOn, LocalDate allocatedUntil, TimeOfDay timeOfDay, 
			TimeOfDay timeOfDayUntil, Integer allocatedById, String allocatedByFirstName, String allocatedByLastName, Integer allocatedToId, 
			String allocatedToFirstName, String allocatedToLastName, Integer deptId, String capabilityReference, Integer jobId, 
			String jobNumber, Integer jobItemId, Integer jobItemNumber, String modelName) {
		// Fields from EngineerAllocation
		this.engineerAllocationId = engineerAllocationId;
		this.allocatedFor = allocatedFor;
		this.allocatedOn = allocatedOn;
		this.allocatedUntil = allocatedUntil;
		this.timeOfDay = timeOfDay;
		this.timeOfDayUntil = timeOfDayUntil;
		// Fields from Person joins
		this.allocatedById = allocatedById;
		this.allocatedByFirstName = allocatedByFirstName;
		this.allocatedByLastName = allocatedByLastName;
		this.allocatedToId = allocatedToId;
		this.allocatedToFirstName = allocatedToFirstName;
		this.allocatedToLastName = allocatedToLastName;
		// Fields from other joins
		this.deptId = deptId;
		this.capabilityReference = capabilityReference;
		this.jobId = jobId;
		this.jobNumber = jobNumber;
		this.jobItemId = jobItemId;
		this.jobItemNumber = jobItemNumber;
		this.modelName = modelName;
	}
	
	// Calculated field
	public String getAllocatedByInitials() {
		return ContactFormatTools.getInitials(allocatedByFirstName, allocatedByLastName);
	}
	
	// Calculated field
	public String getAllocatedToShortenedName() {
		return ContactFormatTools.getShortenedName(allocatedToFirstName, allocatedToLastName);
	}

	// Calculated field
	public String getAllocatedToName() {
		return allocatedToFirstName+" "+allocatedToLastName;
	}
	

}
