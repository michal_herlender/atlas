package org.trescal.cwms.core.workallocation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocatedItemsSearchForm;
import org.trescal.cwms.core.workallocation.engineerallocation.form.NewAllocateItemsToEngineerForm;
import org.trescal.cwms.core.workflow.dto.SelectedJobDTO;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME ,Constants.SESSION_ATTRIBUTE_COMPANY})
public class NewViewWorkForUserController {

    @Autowired
    private EngineerAllocationService engineerAllocationService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private DepartmentService deptServ;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private UserService userService;

    public static final String FORM_NAME = "form";
    public static final String VIEW_NAME ="trescal/core/workallocation/newviewworkforuser";

    @ModelAttribute("jobtypes")
    Set<JobType> getJobTypes(){
        return JobType.getActiveJobTypes();
    }

    @ModelAttribute("labs")
    List<Department> getDepartements(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto){
        return this.deptServ.getAllSubdivDepartments(subdivDto.getKey()).stream().filter(dep -> dep.getType().isProcedures()).collect(Collectors.toList());
    }

    @ModelAttribute("serviceTypes")
    List<ServiceType> getServiceTypes(){
        return this.serviceTypeService.getAllCalServiceTypes();
    }

    @ModelAttribute("clientCompanies")
    List<CompanyProjectionDTO> getClientCompaniesDTosFromSubdiv(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto){
        return this.companyService.getClientCompaniesDTosFromSubdiv(this.subdivService.get(subdivDto.getKey()));
    }

    @ModelAttribute(FORM_NAME)
    protected AllocatedItemsSearchForm formBackingObject() {
        AllocatedItemsSearchForm form = new AllocatedItemsSearchForm();
        return form;
    }

    @RequestMapping(value = "/newviewworkforuser.htm", method = RequestMethod.GET)
    protected String referenceData() throws Exception {
        return VIEW_NAME;

    }


    @RequestMapping(value = "/newviewworkforuser.htm", method = RequestMethod.POST)
    protected String getAllocatedJobItems(Model model,
                                          @ModelAttribute(FORM_NAME) AllocatedItemsSearchForm form, BindingResult bindingResult,
                                          @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto ,
                                          @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {

        form.setSubdivEngineerId(this.userService.get(username).getCon().getPersonid());
        Map<LocalDate, SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>>> groupedMap =this.engineerAllocationService.getGroupedAllocatedItems(
                this.subdivService.get(subdivDto.getKey()),form);

        Map<LocalDate, Integer> tottalItemsPerdate = new TreeMap<>();

        for(LocalDate date : groupedMap.keySet()){
            Integer totalItems = 0;
            for(SelectedJobDTO job : groupedMap.get(date).keySet()){
                totalItems += groupedMap.get(date).get(job).size();
            }
            tottalItemsPerdate.put(date,totalItems);
        }

        model.addAttribute("allocatedItems",groupedMap);
        model.addAttribute("tottalItemsPerdate",tottalItemsPerdate);
        return VIEW_NAME;
    }
}
