package org.trescal.cwms.core.workallocation.engineerallocation.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import java.time.LocalDate;

@Getter
@Setter
public class AllocatedItemsSearchForm {

    private JobType jobtype;
    private Integer laboratoryId;
    private Integer subLabId;
    private Integer descId;
    private String desc;
    private Boolean rangeDueDate;
    private LocalDate aweDueDate1;
    private LocalDate awedueDate2;
    private Integer clientCompanyId;
    private Integer clientSubdivId;
    private Integer serviceTypeId;
    private Integer barcode;
    private Boolean rangeProdDate;
    private LocalDate aweProdDate1;
    private LocalDate aweProdDate2;
    private Integer SubdivEngineerId;

}
