package org.trescal.cwms.core.workallocation.engineerallocationmessage.db;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation_;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.dto.EngineerAllocationMessageDto;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage_;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

@Repository("EngineerAllocationMessageDao")
public class EngineerAllocationMessageDaoImpl extends BaseDaoImpl<EngineerAllocationMessage, Integer> implements EngineerAllocationMessageDao {
	@Override
	protected Class<EngineerAllocationMessage> getEntity() {
		return EngineerAllocationMessage.class;
	}

	@Override
	public List<EngineerAllocationMessageDto> getAllActiveMessageDtos(Integer businessSubdivId, Locale locale) {
		LocalDate dateUntil = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).plusDays(Constants.FUTURE_ALLOCATION_DAYS);

		return getResultList(cb -> {
			CriteriaQuery<EngineerAllocationMessageDto> cq = cb.createQuery(EngineerAllocationMessageDto.class);
			Root<EngineerAllocationMessage> root = cq.from(EngineerAllocationMessage.class);
			Join<EngineerAllocationMessage, EngineerAllocation> allocation = root.join(EngineerAllocationMessage_.allocation);
			Join<EngineerAllocation, Department> dept = allocation.join(EngineerAllocation_.dept);
			Join<EngineerAllocation, JobItem> jobItem = allocation.join(EngineerAllocation_.itemAllocated);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
			Join<Department, Subdiv> subdiv = dept.join(Department_.subdiv);

			Join<EngineerAllocationMessage, Contact> contact = root.join(EngineerAllocationMessage_.setBy);
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), businessSubdivId));
			clauses.getExpressions().add(cb.isTrue(allocation.get(EngineerAllocation_.active)));
			clauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));
			clauses.getExpressions().add(cb.lessThanOrEqualTo(allocation.get(EngineerAllocation_.allocatedFor), dateUntil));
			cq.where(clauses);
			
			cq.select(cb.construct(EngineerAllocationMessageDto.class, 
					root.get(EngineerAllocationMessage_.id),
					root.get(EngineerAllocationMessage_.message),
					root.get(EngineerAllocationMessage_.setOn),
					allocation.get(EngineerAllocation_.id),
					contact.get(Contact_.personid),
					contact.get(Contact_.firstName),
					contact.get(Contact_.lastName))
				);
			
			return cq;
		});  
	}
}