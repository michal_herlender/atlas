package org.trescal.cwms.core.workallocation.engineerallocation.db;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workallocation.engineerallocation.dto.EngineerAllocationDto;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocateWorkToEngineerSearchForm;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocatedItemsSearchForm;
import org.trescal.cwms.core.workallocation.engineerallocation.form.BasketAllocationItemsForm;
import org.trescal.cwms.core.workflow.dto.SelectedJobDTO;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.*;

public interface EngineerAllocationService {
	ResultWrapper ajaxGetFinishDate(String startDate, TimeOfDay startTime, int halfDays, HttpServletRequest request);

	ResultWrapper completeAllocatedWork(int allocatedWorkId);

	void deleteEngineerAllocation(EngineerAllocation engineerallocation);

	/**
	 * Active here refers to the allocation having active=true (at any time)
	 * @param jobItemId
	 * @return
	 */
	EngineerAllocation findActiveEngineerAllocationForItem(int jobItemId);

	EngineerAllocation findEngineerAllocation(int id);

	/**
	 * "Active" here refers to the allocation both having active=true and being any time
	 * up to 28 days from now (as defined in a constant.)
	 *
	 * @param personId
	 * @return
	 */
	List<EngineerAllocationDto> getAllActiveAllocationDtos(Integer businessSubdivId, Locale locale);

	List<EngineerAllocation> getActiveAllocationsForEngineer(int personId, LocalDate maxAllocatedFor);

	Long countEngineerAllocationsBetweenDates(LocalDate startDate, LocalDate endDate, Integer allocatedSubdivId);

	/**
	 * Returns all active {@link EngineerAllocation} entries with a start date
	 * or due date between the given {@link Date}s
	 *
	 * @param startDate the start {@link Date}
	 * @param endDate   the end {@link Date}
	 * @return the {@link List} of {@link EngineerAllocation} entries
	 */
	List<EngineerAllocation> getEngineerAllocationsBetweenDates(LocalDate startDate, LocalDate endDate, Subdiv subdiv);

	void insertEngineerAllocation(EngineerAllocation engineerallocation);

	void updateEngineerAllocation(EngineerAllocation engineerallocation);
	
	void completeEngineerAllocation(JobItem jobitem);
	
	EngineerAllocation findEnginerAllocationByJobItemAndDepartement(JobItem jobitem,Department dep);

	SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> groupeSelectedItems(List<SelectedJobItemDTO> dtos);

	void allocateItemsToEngineer(BasketAllocationItemsForm basketForm, Contact currentContact, Subdiv subdiv,Integer allocatedCompanyId);

	public List<SelectedJobItemDTO> getAllocatedJobItems(List<Integer> procIds, List<Integer> stateIds, Boolean isAllocatedToEngineer,
																	   Subdiv subdiv, StateGroupLinkType sglType, Integer addressId, AllocatedItemsSearchForm form);

	Map<LocalDate,SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>>> getGroupedAllocatedItems(Subdiv subdiv, AllocatedItemsSearchForm form);
}