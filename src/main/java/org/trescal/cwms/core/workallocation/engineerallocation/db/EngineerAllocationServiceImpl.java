package org.trescal.cwms.core.workallocation.engineerallocation.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.WorkingHoursPerDay;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.workallocation.engineerallocation.AutoSheduling;
import org.trescal.cwms.core.workallocation.engineerallocation.dto.EngineerAllocationDto;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocateWorkToEngineerSearchForm;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocatedItemsSearchForm;
import org.trescal.cwms.core.workallocation.engineerallocation.form.BasketAllocationItemsForm;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.EngineerAllocationMessageComparator;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;
import org.trescal.cwms.core.workflow.dto.DateAndTime;
import org.trescal.cwms.core.workflow.dto.SelectedJobDTO;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.entity.workassignment.db.WorkAssignmentService;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service("EngineerAllocationService")
public class EngineerAllocationServiceImpl implements EngineerAllocationService {
    @Autowired
    private EngineerAllocationDao engineerAllocationDao;
    @Autowired
    private LocaleResolver localeResolver;
    @Autowired
    private CapabilityCategoryService pcService;
	@Value("${cwms.config.jobitem.fast_track_turn}")
	private int fastTrackTurn;
	@Autowired
	private ContactService contactService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private CapabilityCategoryService capabilityCategoryService;
	@Autowired
	private CapabilityService capabilityService;
	@Autowired
	private ItemStateService itemStateServ;
	@Autowired
	private WorkAssignmentService workAssignmentServ;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private WorkingHoursPerDay workingHoursPerDay;

    @Override
    public ResultWrapper ajaxGetFinishDate(String startDate, TimeOfDay startTime, int halfDays,
                                           HttpServletRequest request) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("EEE dd MMM yyyy", localeResolver.resolveLocale(request));
            Date start = sdf.parse(startDate);
            int compensation = (startTime.equals(TimeOfDay.AM)) ? 1 : 0;
            int days = (halfDays - compensation) / 2;
            int remainder = halfDays % 2;
			Calendar cal = new GregorianCalendar();
			cal.setTime(start);

			for (int i = 0; i < days;) {
				cal.add(Calendar.DATE, 1);

				if ((cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
						&& (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY)) {
					i++;
				}
			}

			TimeOfDay endTime = (remainder > 0) ? startTime
					: (startTime.equals(TimeOfDay.AM)) ? TimeOfDay.PM : TimeOfDay.AM;
			return new ResultWrapper(true, null, new DateAndTime(cal.getTime(), endTime, sdf), null);
		} catch (ParseException e) {
			return new ResultWrapper(false, "Could not calculate finish time");
		}
	}

	@Override
	public ResultWrapper completeAllocatedWork(int allocatedWorkId) {
		EngineerAllocation ea = this.findEngineerAllocation(allocatedWorkId);
		ea.setActive(false);
		this.updateEngineerAllocation(ea);

		return new ResultWrapper(true, null);
	}

	@Override
	public void deleteEngineerAllocation(EngineerAllocation engineerallocation) {
		this.engineerAllocationDao.remove(engineerallocation);
	}

	@Override
	public EngineerAllocation findActiveEngineerAllocationForItem(int jobItemId) {
		return this.engineerAllocationDao.findActiveEngineerAllocationForItem(jobItemId);
	}

	@Override
	public EngineerAllocation findEngineerAllocation(int id) {
		return this.engineerAllocationDao.find(id);
	}

	@Override
	public List<EngineerAllocation> getActiveAllocationsForEngineer(int personId, LocalDate maxAllocatedFor) {
		return this.engineerAllocationDao.getActiveAllocationsForEngineer(personId, maxAllocatedFor);
	}

	@Override
	public List<EngineerAllocationDto> getAllActiveAllocationDtos(Integer businessSubdivId, Locale locale) {
		return this.engineerAllocationDao.getAllActiveAllocationDtos(businessSubdivId, locale);
	}

	@Override
	public Long countEngineerAllocationsBetweenDates(LocalDate startDate, LocalDate endDate, Integer allocatedSubdivId) {
		return this.engineerAllocationDao.countEngineerAllocationsBetweenDates(startDate, endDate, allocatedSubdivId);
	}

	@Override
	public List<EngineerAllocation> getEngineerAllocationsBetweenDates(LocalDate startDate, LocalDate endDate, Subdiv subdiv) {
		return this.engineerAllocationDao.getEngineerAllocationsBetweenDates(startDate, endDate, subdiv);
	}

	@Override
	public void insertEngineerAllocation(EngineerAllocation EngineerAllocation) {
		this.engineerAllocationDao.persist(EngineerAllocation);
	}

	@Override
	public void updateEngineerAllocation(EngineerAllocation engineerAllocation) {
		this.engineerAllocationDao.merge(engineerAllocation);
	}

	@Override
	public EngineerAllocation findEnginerAllocationByJobItemAndDepartement(JobItem jobitem, Department dep) {
		return this.engineerAllocationDao.findEnginerAllocationByJobItemAndDepartement(jobitem, dep);
	}

	@Override
	public void completeEngineerAllocation(JobItem jobitem) {

		Map<Department, List<WorkRequirement>> groupedByDep = this.groupWorkRequirementByDepartement(jobitem);

		for (Map.Entry<Department, List<WorkRequirement>> g : groupedByDep.entrySet()) {

			List<WorkRequirement> wr = g.getValue();

			List<WorkRequirement> completedWreq = wr.stream()
					.filter(wreq -> wreq.getStatus().equals(WorkrequirementStatus.COMPLETE))
					.collect(Collectors.toList());

			if (completedWreq.size() == wr.size()) {
				EngineerAllocation ea = this.findEnginerAllocationByJobItemAndDepartement(jobitem, g.getKey());
				if (ea != null) {
					ea.setActive(false);
					this.engineerAllocationDao.merge(ea);
				}
			}

		}

	}

	private Map<Department, List<WorkRequirement>> groupWorkRequirementByDepartement(JobItem jobitem) {

		List<WorkRequirement> wrs = jobitem.getWorkRequirements().stream()
			.map(JobItemWorkRequirement::getWorkRequirement).collect(Collectors.toList());

		Map<Department, List<WorkRequirement>> groupedByDep;

		// case 1 workRequirement with not null departement

		groupedByDep = wrs.stream().filter(wr -> wr.getDepartment() != null)
			.collect(Collectors.groupingBy(WorkRequirement::getDepartment));

		// case 2 : workRequirement have null departement

		for (WorkRequirement wreq : wrs) {

			if (wreq.getDepartment() == null) {

                CapabilityCategory procsCat = this.pcService.getBySubdivAndCapability(jobitem.getJob().getOrganisation(),
                    wreq.getCapability());

                if (procsCat != null) {
                    if (!groupedByDep.containsKey(procsCat.getDepartment())) {

                        List<WorkRequirement> wrtemp = new ArrayList<>();
                        wrtemp.add(wreq);

                        groupedByDep.put(procsCat.getDepartment(), wrtemp);

                    } else if (groupedByDep.containsKey(procsCat.getDepartment())) {

                        groupedByDep.get(procsCat.getDepartment()).add(wreq);
                    }
				}

			}

		}

		return groupedByDep;
	}


	@Override
	public SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> groupeSelectedItems(List<SelectedJobItemDTO> dtos) {
		Map<Integer, List<SelectedJobItemDTO>> groupedJobItems = dtos.stream()
				.collect(Collectors.groupingBy(SelectedJobItemDTO::getJobId));
		SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> resultMap = new TreeMap<>();
		groupedJobItems
				.forEach(
						(jobId, jobItems) -> resultMap.put(
								new SelectedJobDTO(jobId, jobItems.get(0).getJobNo(), jobItems.get(0).getJobStatus(),
										jobItems.get(0).getJobRegDate(),
										jobItems.get(0).getDueDate(),
										jobItems.stream().map(SelectedJobItemDTO::getTurn).min(Integer::compareTo)
												.orElse(-1) <= fastTrackTurn,
										jobItems.get(0).getJobType(), jobItems.get(0).getClientContactId(),
										jobItems.get(0).getClientContactFirstName() + " "
												+ jobItems.get(0).getClientContactLastName(),
										jobItems.get(0).getClientContactActive()
												&& jobItems.get(0).getClientSubdivActive()
												&& jobItems.get(0).getClientCompanyActive(),
										jobItems.get(0).getClientCompanyId(), jobItems.get(0).getClientCompanyName(),
										jobItems.get(0).getClientSubdivId(), jobItems.get(0).getClientSubdivName(),
										jobItems.get(0).getClientCompanyActive(),
										jobItems.get(0).getClientCompanyOnStop(), jobItems.get(0).getTransportOutId(),
										jobItems.get(0).getTransportOutName() == null ? ""
												: jobItems.get(0).getTransportOutName()
												+ (jobItems.get(0).getTransportOutSubdiv() == null ? ""
												: " - " + jobItems.get(0).getTransportOutSubdiv()),
										jobItems.get(0).getAllocatedSubdivId(),
										jobItems.get(0).getAllocatedSubdivCode()),
								jobItems));
		return resultMap;
	}

	@Override
	public void allocateItemsToEngineer(BasketAllocationItemsForm basketForm, Contact currentContact, Subdiv subdiv,Integer allocatedCompanyId) {

		for(SelectedJobItemDTO dto : basketForm.getBasketItems()){
			EngineerAllocation ea = new EngineerAllocation();
			ea.setAllocatedBy(currentContact);
			ea.setAllocatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));

			if(basketForm.getBusnissCompEngineerId()!=null)
				ea.setAllocatedTo(this.contactService.get(basketForm.getBusnissCompEngineerId()));
			else
				ea.setAllocatedTo(this.contactService.get(basketForm.getSubdivEngineerI()));

			ea.setItemAllocated(this.jobItemService.findJobItem(dto.getJobItemId()));

			if(basketForm.getAutoSheduling().equals(AutoSheduling.NO)){
				ea.setAllocatedFor(dto.getProduction_date());
			}else {
				/* Automatic scheduling*/
				List<EngineerAllocation> eas =this.engineerAllocationDao.getActiveAllocationsForEngineerByDate(ea.getAllocatedTo().getPersonid(),null);
				if(eas!=null){
					Double workingHours = workingHoursPerDay.getValueByScope(Scope.CONTACT,ea.getAllocatedTo().getPersonid(),allocatedCompanyId);
					Integer convertedHoursToMin =  workingHours.intValue();
					eas.get(0).getAllocatedFor();
					if(eas.get(0).getAllocatedFor().isAfter(LocalDate.now())){
						List<EngineerAllocation> temp =	this.engineerAllocationDao.getActiveAllocationsForEngineer(ea.getAllocatedTo().getPersonid(),eas.get(0).getAllocatedFor());
						Integer estimatedAllocationTime = temp.stream().map(EngineerAllocation::getEstimatedAllocationTime).reduce(0,Integer::sum);
						if(convertedHoursToMin > estimatedAllocationTime){
							ea.setAllocatedFor(eas.get(0).getAllocatedFor());
						}else{
							ea.setAllocatedFor(eas.get(0).getAllocatedFor().plusDays(1));
						}
					}else{
						ea.setAllocatedFor(dto.getProduction_date());
					}
				}else{
					ea.setAllocatedFor(dto.getProduction_date());
				}

			}

			if(dto.getTmc()!=null)
				ea.setEstimatedAllocationTime(dto.getTmc());

			ea.setDept(this.capabilityCategoryService.getBySubdivAndCapability(subdiv,this.capabilityService.get(dto.getProcedureId())).getDepartment());

			if ((dto.getItem_comment() != null) && !dto.getItem_comment().trim().isEmpty()) {
				EngineerAllocationMessage message = new EngineerAllocationMessage();
				message.setAllocation(ea);
				message.setMessage(dto.getItem_comment().trim());
				message.setSetBy(currentContact);
				message.setSetOn(new Date());
				ea.setMessages(new TreeSet<>(new EngineerAllocationMessageComparator()));
				ea.getMessages().add(message);
			}
			this.insertEngineerAllocation(ea);
		}

	}


	@Override
	public List<SelectedJobItemDTO> getAllocatedJobItems(List<Integer> procIds, List<Integer> stateIds, Boolean isAllocatedToEngineer
			, Subdiv subdiv, StateGroupLinkType sglType, Integer addressId, AllocatedItemsSearchForm form) {
		return this.engineerAllocationDao.getAllocatedJobItems(procIds,stateIds,isAllocatedToEngineer,subdiv,sglType,addressId,form);
	}

	@Override
	public Map<LocalDate,SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>>> getGroupedAllocatedItems(Subdiv subdiv, AllocatedItemsSearchForm form) {
		EnumSet<StateGroup> stateGroups = EnumSet.of(StateGroup.CALIBRATION, StateGroup.AWAITINGCALIBRATION,
				StateGroup.RESUMECALIBRATION, StateGroup.AWAITINGGENERALSERVICEOPERATION,
				StateGroup.GENERALSERVICEOPERATION, StateGroup.RESUMEGENERALSERVICEOPERATION,
				StateGroup.AWAITING_REPAIR_WORK_BY_TECHNICIAN);
		List<Integer> stateIds = itemStateServ.getItemStateIdsForStateGroups(stateGroups);

		List<Integer> procIds = new ArrayList<Integer>();


		if(form.getLaboratoryId()!=null){
			Department dept = departmentService.get(form.getLaboratoryId());
			procIds = workAssignmentServ.getCapabilitiesIdsAssignedToDepartment(dept);
		}
		else {
			for (Department dept : subdiv.getDepartments()) {
				if (dept.getType().isProcedures()) {
					procIds.addAll(workAssignmentServ.getCapabilitiesIdsAssignedToDepartment(dept));
				}
			}
		}

		List<SelectedJobItemDTO> selectedJobItems = this.getAllocatedJobItems(procIds, stateIds, true,
				subdiv, null, null,form);
		Set<Integer> clientCompanyIds = selectedJobItems.stream().map(SelectedJobItemDTO::getClientCompanyId)
				.collect(Collectors.toSet());
		List<CompanySettingsForAllocatedCompany> companySettings = companySettingsService.getAll(clientCompanyIds,
				subdiv.getComp());
		SortedMap<Integer, CompanySettingsForAllocatedCompany> companySettingsMap = new TreeMap<>();
		companySettings.forEach(cs -> companySettingsMap.put(cs.getCompany().getCoid(), cs));
		selectedJobItems.forEach(ji -> {
			if (companySettingsMap.containsKey(ji.getClientCompanyId())) {
				CompanySettingsForAllocatedCompany settings = companySettingsMap.get(ji.getClientCompanyId());
				ji.setClientCompanyActive(settings.isActive());
				ji.setClientCompanyOnStop(settings.isOnStop());
			} else {
				ji.setClientCompanyActive(false);
				ji.setClientCompanyOnStop(true);
			}
		});

		Map<LocalDate,List<SelectedJobItemDTO>> groupedItemsByProdDate =selectedJobItems.stream()
				.collect(Collectors.groupingBy(SelectedJobItemDTO::getProduction_date));
		Map<LocalDate,Map<Integer, List<SelectedJobItemDTO>>> groupGroupedItems = new HashMap<>();
		for(LocalDate date : groupedItemsByProdDate.keySet()){
			groupGroupedItems.put(date,groupedItemsByProdDate.get(date).stream().collect(Collectors.groupingBy(SelectedJobItemDTO::getJobId)));
		}
		Map<LocalDate,SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>>> finalMap = new TreeMap<>();
		for(LocalDate date : groupGroupedItems.keySet()){
			Map<Integer, List<SelectedJobItemDTO>> insideMap= groupGroupedItems.get(date);
			SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> resultMap = new TreeMap<>();
			insideMap
					.forEach(
							(jobId, jobItems) -> resultMap.put(
									new SelectedJobDTO(jobId, jobItems.get(0).getJobNo(), jobItems.get(0).getJobStatus(),
											jobItems.get(0).getJobRegDate(),
											jobItems.stream()
													.map(SelectedJobItemDTO::getDueDate).min(LocalDate::compareTo)
													.orElse(null),
											jobItems.stream().map(SelectedJobItemDTO::getTurn).min(Integer::compareTo)
													.orElse(-1) <= fastTrackTurn,
											jobItems.get(0).getJobType(), jobItems.get(0).getClientContactId(),
											jobItems.get(0).getClientContactFirstName() + " "
													+ jobItems.get(0).getClientContactLastName(),
											jobItems.get(0).getClientContactActive()
													&& jobItems.get(0).getClientSubdivActive()
													&& jobItems.get(0).getClientCompanyActive(),
											jobItems.get(0).getClientCompanyId(), jobItems.get(0).getClientCompanyName(),
											jobItems.get(0).getClientSubdivId(), jobItems.get(0).getClientSubdivName(),
											jobItems.get(0).getClientCompanyActive(),
											jobItems.get(0).getClientCompanyOnStop(), jobItems.get(0).getTransportOutId(),
											jobItems.get(0).getTransportOutName() == null ? ""
													: jobItems.get(0).getTransportOutName()
													+ (jobItems.get(0).getTransportOutSubdiv() == null ? ""
													: " - " + jobItems.get(0).getTransportOutSubdiv()),
											jobItems.get(0).getAllocatedSubdivId(),
											jobItems.get(0).getAllocatedSubdivCode()),
									jobItems));

			finalMap.put(date,resultMap);
		}

		return finalMap;
	}

}
