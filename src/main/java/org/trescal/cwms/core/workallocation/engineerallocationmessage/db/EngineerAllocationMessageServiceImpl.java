package org.trescal.cwms.core.workallocation.engineerallocationmessage.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.dto.EngineerAllocationMessageDto;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;
import org.trescal.cwms.core.workallocation.engineerallocationmessage.entity.EngineerAllocationMessage;

@Service("EngineerAllocationMessageService")
public class EngineerAllocationMessageServiceImpl implements EngineerAllocationMessageService
{
	@Autowired
	private UserService userService;
	@Autowired
	private EngineerAllocationService eaServ;
	@Autowired
	private EngineerAllocationMessageDao engineerAllocationMessageDao;

	@Override
	public ResultWrapper ajaxAddEngineerAllocationMessage(int engineerAllocationId, String message, HttpSession httpSession)
	{
		Contact currentContact = this.userService.get((String) httpSession.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME)).getCon();		
		EngineerAllocationMessage m = new EngineerAllocationMessage();

		// Hydrate the proxy for the contact (used by DWR after)
		currentContact.getName();
		m.setAllocation(this.eaServ.findEngineerAllocation(engineerAllocationId));
		m.setMessage(message);
		m.setSetBy(currentContact);
		m.setSetOn(new Date());

		this.insertEngineerAllocationMessage(m);

		return new ResultWrapper(true, null, m, null);
	}

	@Override
	public ResultWrapper ajaxDeleteEngineerAllocationMessage(int messageId, HttpSession httpSession)
	{
		// find engineer message to delete
		EngineerAllocationMessage eam = this.findEngineerAllocationMessage(messageId);
		// message null?
		if (eam != null)
		{
			// get the current contact requesting deletion
			Contact currentContact = this.userService.get((String) httpSession.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME)).getCon();		
			// check that current contact is the author of message
			if (currentContact.getPersonid() == eam.getSetBy().getPersonid())
			{
				// delete this message
				this.deleteEngineerAllocationMessage(eam);
				// return successful wrapper
				return new ResultWrapper(true, "", null, null);
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "You were not the author of this message and so cannot delete it", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "The engineer allocation message could not be found", null, null);
		}
	}

	@Override
	public ResultWrapper ajaxEditEngineerAllocationMessage(int messageId, String message, HttpSession httpSession)
	{
		// find engineer message to delete
		EngineerAllocationMessage eam = this.findEngineerAllocationMessage(messageId);
		// message null?
		if (eam != null)
		{
			// get the current contact requesting deletion
			Contact currentContact = this.userService.get((String) httpSession.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME)).getCon();		
			// check that current contact is the author of message
			if (currentContact.getPersonid() == eam.getSetBy().getPersonid())
			{
				// Hydrate the proxy for the contact (used by DWR after)
				eam.getSetBy().getName();
				// update the message text
				eam.setMessage(message);
				// update allocation message
				this.updateEngineerAllocationMessage(eam);
				// return successful wrapper
				return new ResultWrapper(true, null, eam, null);
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "You were not the author of this message and so cannot delete it", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "The engineer allocation message could not be found", null, null);
		}
	}

	@Override
	public void deleteEngineerAllocationMessage(EngineerAllocationMessage engineerallocationmessage)
	{
		this.engineerAllocationMessageDao.remove(engineerallocationmessage);
	}

	@Override
	public EngineerAllocationMessage findEngineerAllocationMessage(int id)
	{
		return this.engineerAllocationMessageDao.find(id);
	}

	@Override
	public List<EngineerAllocationMessage> getAllEngineerAllocationMessages()
	{
		return this.engineerAllocationMessageDao.findAll();
	}

	@Override
	public void insertEngineerAllocationMessage(EngineerAllocationMessage EngineerAllocationMessage)
	{
		this.engineerAllocationMessageDao.persist(EngineerAllocationMessage);
	}

	@Override
	public void updateEngineerAllocationMessage(EngineerAllocationMessage EngineerAllocationMessage)
	{
		this.engineerAllocationMessageDao.update(EngineerAllocationMessage);
	}
	
	@Override
	public List<EngineerAllocationMessageDto> getAllActiveMessageDtos(Integer businessSubdivId, Locale locale) {
		return this.engineerAllocationMessageDao.getAllActiveMessageDtos(businessSubdivId, locale);
	}

}