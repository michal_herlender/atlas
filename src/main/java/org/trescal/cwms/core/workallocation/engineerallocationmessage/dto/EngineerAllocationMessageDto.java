package org.trescal.cwms.core.workallocation.engineerallocationmessage.dto;

import java.util.Date;

import org.trescal.cwms.core.company.entity.contact.ContactFormatTools;

public class EngineerAllocationMessageDto {
	private Integer messageId;
	private String message;
	private Date setOn;
	// Fields from join
	private Integer allocationId;
	private Integer setById;
	private String setByFirstName;
	private String setByLastName;
	
	public EngineerAllocationMessageDto(Integer messageId, String message, Date setOn, Integer allocationId, Integer setById, 
			String setByFirstName, String setByLastName) {
		this.messageId = messageId;
		this.message = message;
		this.setOn = setOn;
		// Fields from join
		this.allocationId = allocationId;
		this.setById = setById;
		this.setByFirstName = setByFirstName;
		this.setByLastName = setByLastName;
	}
	
	// Calculated field
	public String getSetByInitials() {
		return ContactFormatTools.getInitials(setByFirstName, setByLastName);
	}
	
	public Integer getMessageId() {
		return messageId;
	}
	public String getMessage() {
		return message;
	}
	public Date getSetOn() {
		return setOn;
	}
	public Integer getAllocationId() {
		return allocationId;
	}
	public Integer getSetById() {
		return setById;
	}
	public String getSetByFirstName() {
		return setByFirstName;
	}
	public String getSetByLastName() {
		return setByLastName;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setSetOn(Date setOn) {
		this.setOn = setOn;
	}
	public void setAllocationId(Integer allocationId) {
		this.allocationId = allocationId;
	}
	public void setSetById(Integer setById) {
		this.setById = setById;
	}
	public void setSetByFirstName(String setByFirstName) {
		this.setByFirstName = setByFirstName;
	}
	public void setSetByLastName(String setByLastName) {
		this.setByLastName = setByLastName;
	}
}
