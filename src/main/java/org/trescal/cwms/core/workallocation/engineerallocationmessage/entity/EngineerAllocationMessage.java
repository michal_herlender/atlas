package org.trescal.cwms.core.workallocation.engineerallocationmessage.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;

@Entity
@Table(name = "engineerallocationmessage")
public class EngineerAllocationMessage
{
	private EngineerAllocation allocation;
	private int id;
	private String message;
	private Contact setBy;
	private Date setOn;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "allocationid")
	public EngineerAllocation getAllocation()
	{
		return this.allocation;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Length(min = 1, max = 500)
	@Column(name = "message", nullable = true, unique = false, length = 500)
	public String getMessage()
	{
		return this.message;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setbyid", nullable=false)
	public Contact getSetBy()
	{
		return this.setBy;
	}

	@Column(name = "seton")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSetOn()
	{
		return this.setOn;
	}

	public void setAllocation(EngineerAllocation allocation)
	{
		this.allocation = allocation;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setSetBy(Contact setBy)
	{
		this.setBy = setBy;
	}

	public void setSetOn(Date setOn)
	{
		this.setOn = setOn;
	}
}