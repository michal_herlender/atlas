package org.trescal.cwms.core.workallocation.engineerallocation.form;

import org.springframework.util.AutoPopulatingList;
import org.trescal.cwms.core.workallocation.engineerallocation.dto.ItemAllocation;

public class AllocateItemsToEngineerForm
{
	private AutoPopulatingList<ItemAllocation> itemAllocations;

	public AutoPopulatingList<ItemAllocation> getItemAllocations()
	{
		return this.itemAllocations;
	}

	public void setItemAllocations(AutoPopulatingList<ItemAllocation> itemAllocations)
	{
		this.itemAllocations = itemAllocations;
	}
}