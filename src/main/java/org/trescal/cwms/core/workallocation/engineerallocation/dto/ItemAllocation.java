package org.trescal.cwms.core.workallocation.engineerallocation.dto;

import lombok.Data;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;

import java.time.LocalDate;

@Data
public class ItemAllocation {
	private LocalDate date;
	private LocalDate dateUntil;
	private boolean deallocation;
	private int deptId;
	private int jobItemId;
	private String message;
	private int personId;
	private TimeOfDay time;
	private TimeOfDay timeUntil;

}
