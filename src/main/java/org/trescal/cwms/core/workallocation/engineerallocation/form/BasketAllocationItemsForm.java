package org.trescal.cwms.core.workallocation.engineerallocation.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.workallocation.engineerallocation.AutoSheduling;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;

import java.util.List;

@Getter
@Setter
public class BasketAllocationItemsForm {

    private Integer SubdivEngineerI;
    private Integer busnissCompEngineerId;
    private AutoSheduling autoSheduling;
    private List<SelectedJobItemDTO> basketItems ;

}
