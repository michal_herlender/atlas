package org.trescal.cwms.core.avalara.avalaraconfig;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.trescal.cwms.core.company.entity.company.Company;

import lombok.Setter;
import net.avalara.avatax.rest.client.enums.AvaTaxEnvironment;

@Entity
@Table(name = "avalaraconfig", uniqueConstraints = @UniqueConstraint(columnNames = "accountid", name = "UK_avalaraconfig_accountid"))
@Setter
public class AvalaraConfiguration {

	private Integer id;
	private Company businessCompany;
	private Integer accountId;
	private String licenseKey;
	private AvaTaxEnvironment environment;
	private Integer avalaraId;
	private String avalaraCode;

	@Id
	public Integer getId() {
		return id;
	}

	@MapsId
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", foreignKey = @ForeignKey(name = "FK_avalaraconfig_company"))
	public Company getBusinessCompany() {
		return businessCompany;
	}

	@Column(name = "accountid")
	public Integer getAccountId() {
		return accountId;
	}

	@Column(name = "licensekey")
	public String getLicenseKey() {
		return licenseKey;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "environment")
	public AvaTaxEnvironment getEnvironment() {
		return environment;
	}

	@Column(name = "avalaraid")
	public Integer getAvalaraId() {
		return avalaraId;
	}

	@Column(name = "avalaracode")
	public String getAvalaraCode() {
		return avalaraCode;
	}
}