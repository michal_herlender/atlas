package org.trescal.cwms.core.avalara;

import lombok.extern.slf4j.Slf4j;
import net.avalara.avatax.rest.client.AvaTaxClient;
import net.avalara.avatax.rest.client.TransactionBuilder;
import net.avalara.avatax.rest.client.enums.DocumentType;
import net.avalara.avatax.rest.client.enums.VoidReasonCode;
import net.avalara.avatax.rest.client.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.avalara.avalaraconfig.AvalaraConfiguration;
import org.trescal.cwms.core.avalara.avalaraconfig.db.AvalaraConfigurationDao;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

import java.util.ArrayList;
import java.util.List;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

@Component
@Slf4j
public class AvalaraConnector {

	@Autowired
	private AvalaraConfigurationDao avalaraConfigDao;

	private AvalaraConfiguration getConfiguration(Integer companyId) {
		return avalaraConfigDao.find(companyId);
	}

	private AvaTaxClient getClient(AvalaraConfiguration avalaraConfig) {
		AvaTaxClient client = new AvaTaxClient("atlas", "1.0", "localhost", avalaraConfig.getEnvironment());
		client.withSecurity(avalaraConfig.getAccountId().toString(), avalaraConfig.getLicenseKey());
		return client;
	}

	public TransactionBuilder getTransactionBuilder(DocumentType documentType, Integer businessCompanyId,
			String customerCode) {
		AvalaraConfiguration avalaraConfig = getConfiguration(businessCompanyId);
		if (avalaraConfig == null)
			return null;
		else {
			AvaTaxClient client = getClient(avalaraConfig);
			return new TransactionBuilder(client, avalaraConfig.getAvalaraCode(), documentType, customerCode);
		}
	}

	public boolean commitTransaction(Invoice invoice) {
		Integer businessCompanyId = invoice.getOrganisation().getCoid();
		AvalaraConfiguration avalaraConfig = getConfiguration(businessCompanyId);
		if (avalaraConfig == null)
			return true;
		else {
			AvaTaxClient client = getClient(avalaraConfig);
			VerifyTransactionModel model = new VerifyTransactionModel();
			model.setVerifyTotalAmount(invoice.getTotalCost());
			model.setVerifyTotalTax(invoice.getVatValue());
			model.setVerifyTransactionDate(dateFromLocalDate(invoice.getInvoiceDate()));
			try {
				TransactionModel tm = client.verifyTransaction(avalaraConfig.getAvalaraCode(), invoice.getInvno(),
					DocumentType.SalesInvoice, "", model);
				log.info(tm.toString());
				return true;
			} catch (Exception e) {
				log.error(e.getMessage());
				return false;
			}
		}
	}

	public boolean commitTransaction(CreditNote creditNote) {
		Integer businessCompanyId = creditNote.getOrganisation().getCoid();
		AvalaraConfiguration avalaraConfig = getConfiguration(businessCompanyId);
		if (avalaraConfig == null)
			return true;
		else {
			AvaTaxClient client = getClient(avalaraConfig);
			VerifyTransactionModel model = new VerifyTransactionModel();
			model.setVerifyTotalAmount(creditNote.getTotalCost());
			model.setVerifyTotalTax(creditNote.getVatValue());
			model.setVerifyTransactionDate(dateFromLocalDate(creditNote.getInvoice().getInvoiceDate()));
			try {
				TransactionModel tm = client.verifyTransaction(avalaraConfig.getAvalaraCode(),
					creditNote.getCreditNoteNo(), DocumentType.ReturnInvoice, "", model);
				log.info(tm.toString());
				return true;
			} catch (Exception e) {
				log.error(e.getMessage());
				return false;
			}
		}
	}

	public boolean voidTransaction(Invoice invoice) {
		Integer businessCompanyId = invoice.getOrganisation().getCoid();
		AvalaraConfiguration avalaraConfig = getConfiguration(businessCompanyId);
		if (avalaraConfig == null)
			return true;
		else {
			AvaTaxClient client = getClient(avalaraConfig);
			VoidTransactionModel model = new VoidTransactionModel();
			model.setCode(VoidReasonCode.DocDeleted);
			try {
				client.voidTransaction(avalaraConfig.getAvalaraCode(), invoice.getInvno(), DocumentType.SalesInvoice,
						"", model);
				return true;
			} catch (Exception e) {
				log.error(e.getMessage());
				return false;
			}
		}
	}

	public CustomerModel commitCustomer(Company customer, Integer companyId) {
		try {
			AvalaraConfiguration config = getConfiguration(companyId);
			if (config != null) {
				AvaTaxClient client = getClient(config);
				CustomerModel customerModel;
				// Test, whether customer already exists in Avalara
				boolean newModel = false;
				try {
					customerModel = client.getCustomer(config.getAvalaraId(), customer.getFiscalIdentifier(), "");
				} catch (Exception e) {
					// Customer doesn't exist
					newModel = true;
					customerModel = new CustomerModel();
				}
				Address legalAddress = customer.getLegalAddress();
				customerModel = new CustomerModel();
				customerModel.setName(customer.getConame());
				customerModel.setCustomerCode(customer.getFiscalIdentifier());
				customerModel.setCity(legalAddress.getTown());
				customerModel.setPostalCode(legalAddress.getPostcode());
				customerModel.setRegion(legalAddress.getCounty());
				customerModel.setCountry(legalAddress.getCountry().getCountryCode());
				customerModel.setLine1(legalAddress.getAddr1());
				customerModel.setLine2(legalAddress.getAddr2());
				if (newModel) {
					ArrayList<CustomerModel> customerModels = new ArrayList<>();
					customerModels.add(customerModel);
					customerModels = client.createCustomers(config.getAvalaraId(), customerModels);
					return customerModels.get(0);
				} else {
					return client.updateCustomer(config.getAvalaraId(), customer.getFiscalIdentifier(), customerModel);
				}
			} else
				return null;
		} catch (Exception e) {
			log.info(e.getMessage());
			return null;
		}
	}

	public CustomerModel requestCustomer(Company customer, Integer companyId) throws Exception {
		try {
			AvalaraConfiguration config = getConfiguration(companyId);
			AvaTaxClient client = getClient(config);
			return client.getCustomer(config.getAvalaraId(), customer.getFiscalIdentifier(), "");
		} catch (Exception e) {
			log.info(e.getMessage());
			return new CustomerModel();
		}
	}

	public ValidatedAddressInfo validateAddress(AddressValidationInfo address, Integer companyId) {
		try {
			AvalaraConfiguration config = getConfiguration(companyId);
			AvaTaxClient client = getClient(config);
			AddressResolutionModel addressResolutionModel = client.resolveAddressPost(address);
			List<ValidatedAddressInfo> validatedAddresses = addressResolutionModel.getValidatedAddresses();
			return validatedAddresses.isEmpty() ? null : validatedAddresses.get(0);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
}