package org.trescal.cwms.core.avalara.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.avalara.AvalaraConnector;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class AvalaraAddressValidationController {

	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Autowired
	private AvalaraConnector avalaraConnector;
	
	public Address validateAddress() {
		return null;
	}
}