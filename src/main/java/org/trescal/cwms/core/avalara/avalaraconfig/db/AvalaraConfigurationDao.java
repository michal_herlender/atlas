package org.trescal.cwms.core.avalara.avalaraconfig.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.avalara.avalaraconfig.AvalaraConfiguration;

public interface AvalaraConfigurationDao extends BaseDao<AvalaraConfiguration, Integer> {
}