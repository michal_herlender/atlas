package org.trescal.cwms.core.avalara.avalaraconfig.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.avalara.avalaraconfig.AvalaraConfiguration;

@Service
public class AvalaraConfigurationServiceImpl extends BaseServiceImpl<AvalaraConfiguration, Integer>
		implements AvalaraConfigurationService {

	@Autowired
	private AvalaraConfigurationDao avalaraConfigurationDao;

	@Override
	protected BaseDao<AvalaraConfiguration, Integer> getBaseDao() {
		return avalaraConfigurationDao;
	}
}