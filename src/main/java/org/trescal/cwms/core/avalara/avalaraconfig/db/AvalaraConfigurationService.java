package org.trescal.cwms.core.avalara.avalaraconfig.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.avalara.avalaraconfig.AvalaraConfiguration;

public interface AvalaraConfigurationService extends BaseService<AvalaraConfiguration, Integer> {
}