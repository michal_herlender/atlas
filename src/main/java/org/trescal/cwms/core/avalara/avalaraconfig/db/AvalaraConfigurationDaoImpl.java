package org.trescal.cwms.core.avalara.avalaraconfig.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.avalara.avalaraconfig.AvalaraConfiguration;

@Repository
public class AvalaraConfigurationDaoImpl extends BaseDaoImpl<AvalaraConfiguration, Integer>
		implements AvalaraConfigurationDao {

	@Override
	protected Class<AvalaraConfiguration> getEntity() {
		return AvalaraConfiguration.class;
	}
}