package org.trescal.cwms.core.recall.entity.recalldetail;

import java.util.Comparator;

import org.trescal.cwms.core.recall.entity.recall.RecallType;

public class RecallDetailComparator implements Comparator<RecallDetail>
{
	@Override
	public int compare(RecallDetail rd1, RecallDetail rd2)
	{
		// check recall type of recall detail - both contact types?
		if ((rd1.getRecallType().equals(RecallType.CONTACT))
				&& (rd2.getRecallType().equals(RecallType.CONTACT)))
		{
			// check contact company name
			if (rd1.getContact().getSub().getComp().getConame().equals(rd2.getContact().getSub().getComp().getConame()))
			{
				// check contact lastname
				if (rd1.getContact().getLastName().equals(rd2.getContact().getLastName()))
				{
					// check contact firstname
					if (rd1.getContact().getFirstName().equals(rd2.getContact().getFirstName()))
					{
						Integer personid1 = rd1.getContact().getPersonid();
						Integer personid2 = rd2.getContact().getPersonid();

						return personid1.compareTo(personid2);
					}
					else
					{
						return rd1.getContact().getFirstName().compareTo(rd2.getContact().getFirstName());
					}
				}
				else
				{
					return rd1.getContact().getLastName().compareTo(rd2.getContact().getLastName());
				}
			}
			else
			{
				return rd1.getContact().getSub().getComp().getConame().compareTo(rd2.getContact().getSub().getComp().getConame());
			}
		}
		// check recall type of recall detail - both address types?
		else if ((rd1.getRecallType().equals(RecallType.ADDRESS))
				&& (rd2.getRecallType().equals(RecallType.ADDRESS)))
		{
			// check contact company name
			if (rd1.getAddress().getSub().getComp().getConame().equals(rd2.getAddress().getSub().getComp().getConame()))
			{
				// check subdiv name
				if (rd1.getAddress().getSub().getSubname().equals(rd2.getAddress().getSub().getSubname()))
				{
					// check address county
					if (rd1.getAddress().getCounty().equals(rd2.getAddress().getCounty()))
					{
						// check address town
						if (rd1.getAddress().getTown().equals(rd2.getAddress().getTown()))
						{
							// check address county
							if (rd1.getAddress().getAddr1().equals(rd2.getAddress().getAddr1()))
							{
								Integer addrid1 = rd1.getAddress().getAddrid();
								Integer addrid2 = rd2.getAddress().getAddrid();

								return addrid1.compareTo(addrid2);
							}
							else
							{
								return rd1.getAddress().getAddr1().compareTo(rd2.getAddress().getAddr1());
							}
						}
						else
						{
							return rd1.getAddress().getTown().compareTo(rd2.getAddress().getTown());
						}
					}
					else
					{
						return rd1.getAddress().getCounty().compareTo(rd2.getAddress().getCounty());
					}
				}
				else
				{
					return rd1.getAddress().getSub().getSubname().compareTo(rd2.getAddress().getSub().getSubname());
				}
			}
			else
			{
				return rd1.getAddress().getSub().getComp().getConame().compareTo(rd2.getAddress().getSub().getComp().getConame());
			}
		}
		// check recall type of recall detail - one contact and one address
		// type?
		else if ((rd1.getRecallType().equals(RecallType.CONTACT))
				&& (rd2.getRecallType().equals(RecallType.ADDRESS)))
		{
			return -1;
		}
		// check recall type of recall detail - one address and one contact
		// type?
		else
		{
			return 1;
		}
	}
}
