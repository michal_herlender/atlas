package org.trescal.cwms.core.recall.generator;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db.RecallCompanyConfigurationService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

@Component("RecallGeneratorTask")
public class RecallGeneratorTaskImpl implements RecallGeneratorTask {
	@Autowired
	private ContactService contactService;
	@Autowired
	private RecallCompanyConfigurationService rccService;
	@Autowired
	private RecallGeneratorService recallGeneratorService;
	@Autowired
	private ScheduledTaskService scheduledTaskService;

	private Logger logger = LoggerFactory.getLogger(RecallGeneratorTaskImpl.class);
	
	@Override
	public void executeTask() {
		if (this.scheduledTaskService.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace())) {
			logger.info("Starting task");
			String message = this.generateRecalls(); 
			this.scheduledTaskService.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
			logger.info("Completed task");
		}
		else {
			logger.info("Not running task, is inactive or not located");
		}
	}
	
	/**
	 * Generates recall reports for all companies for which recall is enabled
	 * The recall generation is split between own transaction (service call), 
	 * as this is likely to be a long running task when many companies enabled
	 * (or many contacts for one recall)
	 */
	@Override
	public String generateRecalls() {
		int countRecalls = 0;
		int countRecallDetails = 0;
		int countRecallItems = 0;
		
		long startTime = System.currentTimeMillis();
		List<Integer> listConfigs = rccService.getActiveCoids();
		logger.info("Generating recall for "+listConfigs.size()+" enabled companies ");
		for (Integer coid : listConfigs) {
			// 1) Generate recall and recall items 
			Recall recall = recallGeneratorService.generateRecall(coid);
			Integer recallId = recall.getId();
			countRecalls++;
			countRecallItems += recall.getItems().size();
			
			// 2) Find active contacts for company
			List<Integer> contactIds = contactService.getContactDtoList(coid, null, true)
					.stream()
					.map(dto -> dto.getKey())
					.collect(Collectors.toList());
			
			// 3) Generate recall details for each active contact (if enabled at contact level)
			logger.info("Generating recalldetails for "+contactIds.size()+" contacts");
			for(Integer contactId : contactIds) {
				boolean createdRecallDetail = recallGeneratorService.generateRecallDetail(recallId, contactId);
				if (createdRecallDetail)
					countRecallDetails++;
			}
			
			
		}
		long finishTime = System.currentTimeMillis();
		
		StringBuffer message = new StringBuffer();
		message.append(countRecalls);
		message.append(" recalls, ");
		message.append(countRecallDetails);
		message.append(" recall details, ");
		message.append(countRecallItems);
		message.append(" recall items, ");
		message.append((finishTime-startTime)+" ms total time");
		
		logger.info(message.toString());
		
		return message.toString();
	}
	
}
