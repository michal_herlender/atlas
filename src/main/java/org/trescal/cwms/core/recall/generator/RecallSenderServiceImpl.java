package org.trescal.cwms.core.recall.generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember;
import org.trescal.cwms.core.company.entity.mailgroupmember.db.MailGroupMemberService;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;
import org.trescal.cwms.core.recall.entity.recall.RecallError;
import org.trescal.cwms.core.recall.entity.recall.RecallSendStatus;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db.RecallCompanyConfigurationService;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.db.RecallDetailService;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallitem.db.RecallItemService;
import org.trescal.cwms.core.recall.xssf.generic.RecallXssfCreatorGeneric;
import org.trescal.cwms.core.recall.xssf.iberia.RecallXssfCreatorIberia;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.recall.EmailContent_Recall;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.system.entity.emailrecipient.db.EmailRecipientService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.FileNameCleaner;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;

/**
 * Author : Galen Beck - 2017-05-12
 * Second stage utility that sends recalls based on the RecallDetail entities created by RecallGenerator
 * Initially created specific for Iberia requirements, expect to refactor Iberia-specific functionality into configuration over time 
 */
@Service
public class RecallSenderServiceImpl implements RecallSenderService {
	@Autowired
	private BusinessDetailsService businessDetailsService;
	@Autowired
	private ComponentDirectoryService componentDirectoryService;
	@Autowired
	private EmailContent_Recall emailContentRecall;
	@Autowired
	private EmailService emailService;
	@Autowired
	private EmailRecipientService emailRecipientService;
	@Autowired
	private MailGroupMemberService mailGroupMemberService;
	@Autowired
	private RecallCompanyConfigurationService rccService;
	@Autowired
	private RecallDetailService recallDetailService; 
	@Autowired
	private RecallItemService recallItemService; 
	@Autowired
	private RecallXssfCreatorGeneric recallWorkbookGeneric;
	@Autowired
	private RecallXssfCreatorIberia recallWorkbookIberia;
	@Autowired
	private SystemComponentService systemComponentService;

	/* Parameters */	
	@Value("${cwms.recall.sender.sendemail}")
	private Boolean sendEmail;
	@Value("${cwms.recall.sender.testingrecipient}")
	private Boolean testingRecipient;
	@Value("${cwms.recall.sender.testingrecipientemail}")
	private String testingRecipientEmail;
	
	public static final Logger logger = LoggerFactory.getLogger(RecallSenderServiceImpl.class);
	
	public static final int MAX_FILENAME_LENGTH = 127;
	
	@Override
	public void sendRecall(Integer recallDetailId, RecallSenderStatistics statistics) {
		RecallDetail detail = this.recallDetailService.get(recallDetailId);
		// Wrapping in try/catch, so that if an error occurs on one recall, it doesn't crash the job
		try {
			SystemComponent scRecall = this.systemComponentService.findEagerComponent(Component.RECALL);
	
			Company clientCompany = detail.getRecall().getCompany();
			Contact businessContact = clientCompany.getDefaultBusinessContact();
			BusinessDetails bdetails = this.businessDetailsService.getAllBusinessDetails(businessContact, businessContact.getDefAddress(), businessContact.getSub().getComp());
			RecallCompanyConfiguration config = this.rccService.get(clientCompany.getId());
			if (config != null) {
				List<RecallItem> recallItems = null;
				switch(detail.getRecallType()) {
					case COMPANY:
						recallItems = this.recallItemService.getRecallItemsForCompany(detail.getRecall().getId(), detail.getContact().getSub().getComp().getCoid());
					break;	
					case SUBDIV:
						recallItems = this.recallItemService.getRecallItemsForSubdiv(detail.getRecall().getId(), detail.getContact().getSub().getSubdivid());
					break;	
					case ADDRESS:
						recallItems = this.recallItemService.getRecallItemsForAddress(detail.getRecall().getId(), detail.getAddress().getAddrid());
					break;	
					case CONTACT:
						recallItems = this.recallItemService.getRecallItemsForContact(detail.getRecall().getId(), detail.getContact().getPersonid());
					break;	
					default:
						recordError(detail, RecallError.UNSUPPORTED_RECALL_TYPE, statistics);
				}
				if (recallItems != null) {
					if (!recallItems.isEmpty()) {
						XSSFWorkbook wb = null;
						switch(config.getAttachmentType()) {
							case DEFAULT:
								wb = this.recallWorkbookGeneric.createWorkbook(detail, recallItems);
								break;
							case IBERIA:
								wb = this.recallWorkbookIberia.createWorkbook(detail, recallItems);
								break;
							default:
								logger.error("Unsupported attachment type");
						}
						
						String fileName = getFileName(detail);
						File directory = this.componentDirectoryService.getDirectory(scRecall, detail.getRecall().getIdentifier());
						File fileXlsx = new File(directory, fileName);
						boolean fileOutputSuccess = writeFile(detail, wb, fileXlsx, statistics);
						if (fileOutputSuccess) {
							detail.setPathToFile(fileXlsx.getAbsolutePath());
							Locale locale = detail.getContact().getLocale();
							
							EmailContentDto contentDto = emailContentRecall.getContent(detail, locale);
							String emailBody = contentDto.getBody();
							String subject = contentDto.getSubject();
							String recipientEmail = detail.getContact().getEmail();
							// Determine any additional contacts subscribed
							// TODO - expand concept of mailgroups to be able to subscribe at a certain level (not just subdiv)
							List<MailGroupMember> memberList = this.mailGroupMemberService.getMembersOfGroupForSubdiv(MailGroupType.RECALLSUMMARY, detail.getContact().getSub().getSubdivid(), true);
							if (EmailValidator.getInstance().isValid(recipientEmail)) {
								boolean ccEmailsValid = validateEmailAddresses(memberList);  
								if (ccEmailsValid) {
									if (testingRecipient) {
										logger.info("testing mode, overwriting recipient from "+recipientEmail+" to "+testingRecipientEmail);
										recipientEmail = testingRecipientEmail;
										logger.info("testing mode, clearing "+memberList.size()+" cc email addresses:");
										for (MailGroupMember member : memberList) {
											logger.info(member.getContact().getEmail()+" from contact id "+member.getContact().getId());
										}
										memberList = Collections.emptyList();
									}
									boolean mailed = true;
									if (sendEmail) {
										List<String> ccEmailAddressList = memberList.stream().map(member -> member.getContact().getEmail()).collect(Collectors.toList());
										mailed = this.emailService.sendAdvancedEmail(subject, bdetails.getRecallEmail(), Arrays.asList(recipientEmail), ccEmailAddressList, Collections.emptyList(), Arrays.asList(bdetails.getSalesEmail()), Arrays.asList(fileXlsx), emailBody, true, false);
									}
									if (mailed) {
										Email email = new Email();
										email.setBody(emailBody);
										email.setComponent(Component.RECALL);
										email.setEntityId(detail.getRecall().getId());
										email.setFrom(bdetails.getRecallEmail());
										email.setPublish(true);
										email.setSentBy(businessContact);
										email.setSentOn(new Date());
										email.setSubject(subject);
										this.emailService.insertEmail(email);
										EmailRecipient recipientTo = new EmailRecipient();
										recipientTo.setEmail(email);
										recipientTo.setEmailAddress(recipientEmail);
										recipientTo.setRecipientCon(detail.getContact());
										recipientTo.setType(RecipientType.EMAIL_TO);
										this.emailRecipientService.insertEmailRecipient(recipientTo);
										for (MailGroupMember member : memberList) {
											EmailRecipient recipientCC = new EmailRecipient();
											recipientCC.setEmail(email);
											recipientCC.setEmailAddress(member.getContact().getEmail());
											recipientCC.setRecipientCon(member.getContact());
											recipientCC.setType(RecipientType.EMAIL_CC);
											this.emailRecipientService.insertEmailRecipient(recipientCC);
										}
										
										recordSuccess(detail, statistics);
									}
									else {
										recordError(detail, RecallError.EMAIL_SEND_FAILURE, statistics);
									}
								}
								else {
									recordError(detail, RecallError.BAD_EMAIL_CC_ADDRESS, statistics);
								}
							}
							else {
								recordError(detail, RecallError.BAD_EMAIL_ADDRESS, statistics);
							}
						}
						else {
							recordError(detail, RecallError.RECALL_LIST_EMPTY, statistics);
						}
					}
					else {
						recordError(detail, RecallError.RECALL_LIST_NULL, statistics);
					}
				}
			}
			else {
				recordError(detail, RecallError.NO_CONFIG, statistics);
			}
		}
		catch (Exception e) {
			recordError(detail, RecallError.IO_EXCEPTION, statistics);
		}
	}
	
	private void recordError(RecallDetail detail, RecallError error, RecallSenderStatistics statistics) {
		logger.error(error.name()+" error on recall detail "+detail.getId());
		detail.setError(true);
		detail.setRecallError(error);
		detail.setRecallSendStatus(RecallSendStatus.ERROR);
		this.recallDetailService.merge(detail);
		statistics.incrementCountError();
	}

	private void recordSuccess(RecallDetail detail, RecallSenderStatistics statistics) {
		logger.info("Success sending recall detail "+detail.getId());
		detail.setError(false);
		detail.setRecallError(RecallError.NONE);
		detail.setRecallSendStatus(RecallSendStatus.SENT);
		this.recallDetailService.merge(detail);
		statistics.incrementCountSent();
	}
	
	/*
	 * Performs validation on any contact emails addresses contained in list; 
	 * returns false if there are any invalid addresses.
	 */
	private boolean validateEmailAddresses(List<MailGroupMember> memberList) {
		boolean result = true;
		for (MailGroupMember member : memberList) {
			if (member.getContact().getEmail() == null) {
				result = false;
				logger.error("Null email address for mail group member contact id "+member.getContact().getId());
			}
			else if (!EmailValidator.getInstance().isValid(member.getContact().getEmail())) {
				result = false;
				logger.error("Invalid email address "+member.getContact().getEmail()+" for mail group member contact id "+member.getContact().getId());
			}
		}
		return result;
	}
	
	/*
	 * Returns a unique file name for recall, currently based on recall type and id
	 * Support maximum of 127 characters for filename
	 */
	private String getFileName(RecallDetail detail) {
		StringBuffer fileName = new StringBuffer();
		fileName.append(this.emailContentRecall.getRecallTypeDescription(detail));
		fileName.append(".xlsx");
		String cleanFileName = FileNameCleaner.cleanFileName(fileName.toString());
		return cleanFileName.length() <= MAX_FILENAME_LENGTH ? cleanFileName : cleanFileName.substring(0, MAX_FILENAME_LENGTH);
	}
		
	private boolean writeFile(RecallDetail detail, XSSFWorkbook wb, File file, RecallSenderStatistics statistics) {
		boolean success = true;
		try (FileOutputStream os = new FileOutputStream(file)) {
			wb.write(os);
		}
		catch (IOException ex) {
			success = false;
			logger.error(file.toString(), ex);
			recordError(detail, RecallError.IO_EXCEPTION, statistics);
		}
		return success;
	}
}
