package org.trescal.cwms.core.recall.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.db.RecallService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;

@Controller @IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class ViewRecallController
{
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private RecallService recServ;
	@Autowired
	private SystemComponentService scServ;
	
	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<String>();
	}
	
	@RequestMapping(value="/viewrecall.htm", method=RequestMethod.GET)
	protected ModelAndView handleRequestInternal(
			@RequestParam(value="id", required=false, defaultValue="0") Integer id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles) throws Exception
	{
		Recall recall = this.recServ.findRecall(id);
		if ((id == 0) || (recall == null)) {
			throw new Exception("Recall could not be found");
		}
		else {
			Map<String, Object> recallMap = new HashMap<String, Object>();
			String rootPath = this.compDirServ.getDirectory(this.scServ.findComponent(Component.RECALL), recall.getRecallNo()).getAbsolutePath();
			// get files in the root directory
			FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.RECALL, recall.getId(), newFiles);
			// root directory
			recallMap.put("root", rootPath);
			recallMap.put("recall", recall);
			recallMap.put(Constants.REFDATA_SYSTEM_COMPONENT, this.scServ.findComponent(Component.RECALL));
			recallMap.put(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
			recallMap.put(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
			return new ModelAndView("trescal/core/recall/viewrecall", "command", recallMap);
		}
	}
}