package org.trescal.cwms.core.recall.entity.recalldetail.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.RecallSendStatus;
import org.trescal.cwms.core.recall.entity.recall.Recall_;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail_;

@Repository("RecallDetailDao")
public class RecallDetailDaoImpl extends BaseDaoImpl<RecallDetail, Integer> implements RecallDetailDao
{
	@Override
	protected Class<RecallDetail> getEntity() {
		return RecallDetail.class;
	}
	
	@Override
	public List<RecallDetail> getRecallDetailsForContact(int personid)
	{
		return getResultList(cb ->{
			CriteriaQuery<RecallDetail> cq = cb.createQuery(RecallDetail.class);
			Root<RecallDetail> root = cq.from(RecallDetail.class);
			Join<RecallDetail, Contact> contact = root.join(RecallDetail_.contact);
			Join<RecallDetail, Recall> recall = root.join(RecallDetail_.recall);
			cq.where(cb.equal(contact.get(Contact_.personid), personid));
			cq.orderBy(cb.desc(recall.get(Recall_.date)));
			return cq;
		});
		
	}
	
	@Override
	public List<RecallDetail> getRecallDetailsForRecall(int recallid)
	{
		return getResultList(cb ->{
			CriteriaQuery<RecallDetail> cq = cb.createQuery(RecallDetail.class);
			Root<RecallDetail> root = cq.from(RecallDetail.class);
			Join<RecallDetail, Recall> recall = root.join(RecallDetail_.recall);
			cq.where(cb.equal(recall.get(Recall_.id), recallid));
			return cq;
		});
	}
	
	@Override
	public List<Integer> getRecallDetailIdsForSendStatus(RecallSendStatus recallSendStatus) {
		CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
		Root<RecallDetail> root = cq.from(RecallDetail.class);
		cq.where(cb.equal(root.get(RecallDetail_.recallSendStatus), recallSendStatus));
		cq.select(root.get(RecallDetail_.id));
		return this.getEntityManager().createQuery(cq).getResultList();
	}

}