package org.trescal.cwms.core.recall.entity.recallrule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;



@RestController
public class RecallRuleAPIController {

    @Autowired
    RecallRuleService recallRuleService;

    @RequestMapping(value="/addrecallrule.json",method=RequestMethod.POST,
    		consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public RecallRuleResponseDto insertNewRule(@RequestBody RecallRuleRequestDto recallRuleRequestDto){
        return recallRuleService.insertAjax(recallRuleRequestDto);
    }

    @RequestMapping(value="/deactivaterecallrule.json",method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public RecallRuleResponseDto deactivateRule(@RequestBody RecallRuleRequestDto recallRuleRequestDto){
        return recallRuleService.deactivateAjax(recallRuleRequestDto);
    }

    @RequestMapping(value="/reactivaterecallrule.json",method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    public RecallRuleResponseDto reactivateRule(@RequestBody RecallRuleRequestDto recallRuleRequestDto){
        return recallRuleService.reactivateAjax(recallRuleRequestDto);
    }



}