package org.trescal.cwms.core.recall.xssf.iberia;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.xssf.AbstractRecallXssfCreator;
import org.trescal.cwms.core.recall.xssf.RecallXssfContext;
import org.trescal.cwms.core.tools.DateTools;

import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Iberia specific class to create recall workbook in the locale of the user
 * (expected to be normally Spanish)
 * 
 * @author Galen
 */
@Component
public class RecallXssfCreatorIberiaImpl extends AbstractRecallXssfCreator implements RecallXssfCreatorIberia {

	@Autowired
	private MessageSource messages;
	
	public static final Logger logger = LoggerFactory.getLogger(RecallXssfCreatorIberiaImpl.class);

	private static final int ROW_MERGED_HEADER = 0;
	private static final int ROW_COLUMN_HEADER = 1;
	// Definitions for multi-header format at top
	private static final int MERGED_EQUIPMENT_BEGIN;
	private static final int MERGED_EQUIPMENT_END;
	private static final int MERGED_CALIBRATION_BEGIN;
	private static final int MERGED_CALIBRATION_END;
	private static final int MERGED_INSTALLATION_BEGIN;
	private static final int MERGED_INSTALLATION_END;
	// Definitions for individual columnns
	private static final int COL_PLANT_NO;
	private static final int COL_PLANT_ID;
	private static final int COL_DESCRIPTION;
	private static final int COL_PART_NO;
	private static final int COL_SERIAL_NO;
	private static final int COL_LAST_CAL_DATE;
	private static final int COL_NEXT_CAL_DATE;
	private static final int COL_PLACE;
	private static final int COL_TBD1;	// Centro ext
	private static final int COL_TBD2;	// METR
	private static final int COL_INSTALLATION_DESC;
	private static final int COL_INSTALLATION_PART_NO;
	private static final int COL_INSTALLATION_SERIAL_NO;
	
	static {
		int colIndex = 0;
		// Definitions for individual columns
		COL_PLANT_NO = colIndex++;
		COL_PLANT_ID = colIndex++;
		COL_DESCRIPTION = colIndex++;
		COL_PART_NO = colIndex++;
		COL_SERIAL_NO = colIndex++;
		COL_LAST_CAL_DATE = colIndex++;
		COL_NEXT_CAL_DATE = colIndex++;
		COL_PLACE = colIndex++;
		COL_TBD1 = colIndex++;
		COL_TBD2 = colIndex++;
		COL_INSTALLATION_DESC = colIndex++;
		COL_INSTALLATION_PART_NO = colIndex++;
		COL_INSTALLATION_SERIAL_NO = colIndex;
		// Definitions for multiple headers
		MERGED_EQUIPMENT_BEGIN = COL_PLANT_NO;
		MERGED_EQUIPMENT_END = COL_SERIAL_NO;
		MERGED_CALIBRATION_BEGIN = COL_LAST_CAL_DATE;
		MERGED_CALIBRATION_END = COL_TBD2;
		MERGED_INSTALLATION_BEGIN = COL_INSTALLATION_DESC;
		MERGED_INSTALLATION_END = COL_INSTALLATION_SERIAL_NO;
	}
	
	// Note, if the custom field names change they must be updated here
	private static final String CUSTOM_FIELD_PART_NO = "Part No";
	private static final String CUSTOM_FIELD_PLACE = "Lugar de trabajo";
	private static final String CUSTOM_FIELD_TBD1 = "Laboratorio Externo";
	private static final String CUSTOM_FIELD_TBD2 = "";
	private static final String CUSTOM_FIELD_INSTALLATION_DESC = "Eq Superior - Description";
	private static final String CUSTOM_FIELD_INSTALLATION_PART_NO = "Eq Superior - Part No";
	private static final String CUSTOM_FIELD_INSTALLATION_SERIAL_NO = "Eq Superior - Serial No";

	private static final String KEY_SHEET_CURRENT_MONTH = "recall.iberia.sheet.currentmonth";	// e.g. Current Month
	private static final String KEY_SHEET_PAST_DUE = "recall.iberia.sheet.pastdue";				// e.g. Past Due
	
	private static final String KEY_MERGED_EQUIPMENT = "recall.iberia.merged.equipment";
	private static final String KEY_MERGED_CALIBRATION = "recall.iberia.merged.calibration";
	private static final String KEY_MERGED_INSTALLATION = "recall.iberia.merged.installation";
	private static final String KEY_COL_PLANT_NO = "recall.iberia.column.plantno";
	private static final String KEY_COL_PLANT_ID = "recall.iberia.column.plantid";
	private static final String KEY_COL_DESCRIPTION = "recall.iberia.column.description";
	private static final String KEY_COL_PART_NO = "recall.iberia.column.partno";
	private static final String KEY_COL_SERIAL_NO = "recall.iberia.column.serialno";
	private static final String KEY_COL_LAST_CAL_DATE = "recall.iberia.column.lastcaldate";
	private static final String KEY_COL_NEXT_CAL_DATE = "recall.iberia.column.nextcaldate";
	private static final String KEY_COL_PLACE = "recall.iberia.column.place";
	private static final String KEY_COL_TBD1 = "recall.iberia.column.tbd1";
	private static final String KEY_COL_TBD2 = "recall.iberia.column.tbd2";
	private static final String KEY_COL_INSTALLATION_DESC = "recall.iberia.column.install.desc";
	private static final String KEY_COL_INSTALLATION_PART_NO = "recall.iberia.column.install.partno";
	private static final String KEY_COL_INSTALLATION_SERIAL_NO = "recall.iberia.column.install.serialno";
	
	@Override
	public XSSFWorkbook createWorkbook(RecallDetail detail, List<RecallItem> items) {
		Locale locale = detail.getContact().getLocale();
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheetPastDue = wb.createSheet(messages.getMessage(KEY_SHEET_PAST_DUE, null, locale));
		XSSFSheet sheetCurrentMonth = wb.createSheet(messages.getMessage(KEY_SHEET_CURRENT_MONTH, null, locale));
		RecallXssfContext context = new RecallXssfContext();
		context.setWb(wb);
		context.setSheetPastDue(sheetPastDue);
		context.setSheetCurrent(sheetCurrentMonth);
		context.setLocale(locale);
		createStyles(context);
		createHeaders(context, sheetPastDue);
		createHeaders(context, sheetCurrentMonth);
		addItems(context, items);
		autosize(sheetPastDue, COL_INSTALLATION_SERIAL_NO);
		autosize(sheetCurrentMonth, COL_INSTALLATION_SERIAL_NO);
		return wb;
	}
	private void createHeaders(RecallXssfContext context, XSSFSheet sheet) {
		Locale locale = context.getLocale();
		XSSFRow rowMergedHeader = sheet.createRow(ROW_MERGED_HEADER);
		createCell(rowMergedHeader, MERGED_EQUIPMENT_BEGIN, messages.getMessage(KEY_MERGED_EQUIPMENT, null, locale), context.getCellStyleGrey15());
		createCell(rowMergedHeader, MERGED_CALIBRATION_BEGIN, messages.getMessage(KEY_MERGED_CALIBRATION, null, locale), context.getCellStyleGrey5());
		createCell(rowMergedHeader, MERGED_INSTALLATION_BEGIN, messages.getMessage(KEY_MERGED_INSTALLATION, null, locale), context.getCellStyleGrey15());

		sheet.addMergedRegion(new CellRangeAddress(ROW_MERGED_HEADER, ROW_MERGED_HEADER, MERGED_EQUIPMENT_BEGIN, MERGED_EQUIPMENT_END));
		sheet.addMergedRegion(new CellRangeAddress(ROW_MERGED_HEADER, ROW_MERGED_HEADER, MERGED_CALIBRATION_BEGIN, MERGED_CALIBRATION_END));
		sheet.addMergedRegion(new CellRangeAddress(ROW_MERGED_HEADER, ROW_MERGED_HEADER, MERGED_INSTALLATION_BEGIN, MERGED_INSTALLATION_END));

		XSSFRow rowColumnHeader = sheet.createRow(ROW_COLUMN_HEADER);
		createCell(rowColumnHeader, COL_PLANT_NO, messages.getMessage(KEY_COL_PLANT_NO, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_PLANT_ID, messages.getMessage(KEY_COL_PLANT_ID, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_DESCRIPTION, messages.getMessage(KEY_COL_DESCRIPTION, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_PART_NO, messages.getMessage(KEY_COL_PART_NO, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_SERIAL_NO, messages.getMessage(KEY_COL_SERIAL_NO, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_LAST_CAL_DATE, messages.getMessage(KEY_COL_LAST_CAL_DATE, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_NEXT_CAL_DATE, messages.getMessage(KEY_COL_NEXT_CAL_DATE, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_PLACE, messages.getMessage(KEY_COL_PLACE, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_TBD1, messages.getMessage(KEY_COL_TBD1, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_TBD2, messages.getMessage(KEY_COL_TBD2, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_INSTALLATION_DESC, messages.getMessage(KEY_COL_INSTALLATION_DESC, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_INSTALLATION_PART_NO, messages.getMessage(KEY_COL_INSTALLATION_PART_NO, null, locale), context.getCellStyleLightBlue());
		createCell(rowColumnHeader, COL_INSTALLATION_SERIAL_NO, messages.getMessage(KEY_COL_INSTALLATION_SERIAL_NO, null, locale), context.getCellStyleLightBlue());
	}
	/**
	 * Adds each item to either past due sheet or current month, based on its due date
	 */
	private void addItems(RecallXssfContext context, List<RecallItem> items) {
		for (RecallItem ritem : items) {
			if (isCurrent(ritem)) {
				addItem(context, context.getSheetCurrent(), ritem);
			}
			else if (isPastDue(ritem)) {
				addItem(context, context.getSheetPastDue(), ritem);
			}

		}
	}
	
	private void addItem(RecallXssfContext context, XSSFSheet sheet, RecallItem ritem) {
		Locale locale = context.getLocale();
		Date lastCalDate = getLastCalDate(ritem.getInstrument());
		Date nextCalDueDate = DateTools.dateFromLocalDate(ritem.getInstrument().getNextCalDueDate());

		XSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);

		createCell(context, row, COL_PLANT_NO, ritem.getInstrument().getPlantno());
		row.createCell(COL_PLANT_ID).setCellValue(ritem.getInstrument().getPlantid());
		createCell(context, row, COL_DESCRIPTION, getDescription(ritem.getInstrument(), locale));
		createCell(context, row, COL_PART_NO, getCustomField(ritem.getInstrument(), CUSTOM_FIELD_PART_NO));
		createCell(context, row, COL_SERIAL_NO, ritem.getInstrument().getSerialno());
		createCell(context, row, COL_LAST_CAL_DATE, lastCalDate);
		createCell(context, row, COL_NEXT_CAL_DATE, nextCalDueDate);
		createCell(context, row, COL_PLACE, getCustomField(ritem.getInstrument(), CUSTOM_FIELD_PLACE));
		createCell(context, row, COL_TBD1, getCustomField(ritem.getInstrument(), CUSTOM_FIELD_TBD1));
		createCell(context, row, COL_TBD2, getCustomField(ritem.getInstrument(), CUSTOM_FIELD_TBD2));
		createCell(context, row, COL_INSTALLATION_DESC, getCustomField(ritem.getInstrument(), CUSTOM_FIELD_INSTALLATION_DESC));
		createCell(context, row, COL_INSTALLATION_PART_NO, getCustomField(ritem.getInstrument(), CUSTOM_FIELD_INSTALLATION_PART_NO));
		createCell(context, row, COL_INSTALLATION_SERIAL_NO, getCustomField(ritem.getInstrument(), CUSTOM_FIELD_INSTALLATION_SERIAL_NO));
	}
}