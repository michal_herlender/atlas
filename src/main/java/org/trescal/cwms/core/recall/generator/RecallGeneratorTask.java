package org.trescal.cwms.core.recall.generator;

public interface RecallGeneratorTask {
	void executeTask();
	String generateRecalls();
}
