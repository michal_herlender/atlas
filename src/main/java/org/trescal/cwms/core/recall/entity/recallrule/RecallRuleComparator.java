package org.trescal.cwms.core.recall.entity.recallrule;

import java.util.Comparator;

/**
 * Custom {@link Comparator} implementation used to sort {@link RecallRule}. The
 * sorting hierarchy runs as follows: active, instrument, contact, subdiv and
 * finally company. Where two active rules exist for the same type
 * (instrument/contact etc) the date of the rule is used (although this *should*
 * never be a real scenario).
 */
public class RecallRuleComparator implements Comparator<RecallRule>
{
	@Override
	public int compare(RecallRule o1, RecallRule o2)
	{
		if (!o1.isActive() && o2.isActive()) return 1;
		else if (o1.isActive() && !o2.isActive()) return -1;
		else if (o1.getRecallRuleType().equals(o2.getRecallRuleType()))
			return ((Integer) o1.getId()).compareTo(o2.getId());
		else return ((Integer) o1.getRecallRuleType().getOrder()).compareTo(o2.getRecallRuleType().getOrder());
	}
}