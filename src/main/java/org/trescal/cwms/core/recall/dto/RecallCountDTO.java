package org.trescal.cwms.core.recall.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RecallCountDTO {
    private Integer recallid;
    private String recallNo;
    private String coname;
    private LocalDate date;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Long rdetailsSize;
    private long count;

    public RecallCountDTO(Integer recallid, String recallNo, String coname, LocalDate date, LocalDate dateFrom, LocalDate dateTo,
                          Long rdetailsSize, long count) {
        super();
        this.recallid = recallid;
        this.recallNo = recallNo;
        this.coname = coname;
        this.date = date;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.rdetailsSize = rdetailsSize;
        this.count = count;
    }

}
