package org.trescal.cwms.core.recall.entity.recallresponsestatus;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNote;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "recallresponsestatus")
public class RecallResponseStatus
{
	private String description;
	private Set<RecallResponseNote> recallResponseNotes;
	private int statusid;
	private Set<Translation> descriptiontranslation;

	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "statusdescription", nullable = true, unique = false, length = 1000)
	public String getDescription()
	{
		return this.description;
	}

	@OneToMany(mappedBy = "recallResponseStatus", fetch = FetchType.LAZY)
	public Set<RecallResponseNote> getRecallResponseNotes()
	{
		return this.recallResponseNotes;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "statusid", nullable = false, unique = true)
	@Type(type = "int")
	public int getStatusid()
	{
		return this.statusid;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setRecallResponseNotes(Set<RecallResponseNote> recallResponseNotes)
	{
		this.recallResponseNotes = recallResponseNotes;
	}

	public void setStatusid(int statusid)
	{
		this.statusid = statusid;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="recallresponsestatustranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getDescriptiontranslation() {
		return descriptiontranslation;
	}

	public void setDescriptiontranslation(Set<Translation> descriptiontranslation) {
		this.descriptiontranslation = descriptiontranslation;
	}

}
