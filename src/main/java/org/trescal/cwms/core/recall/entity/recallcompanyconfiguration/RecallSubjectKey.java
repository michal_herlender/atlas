package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration;

public enum RecallSubjectKey {
	DEFAULT("recall.generic.email.subject"),
	IBERIA("recall.iberia.email.subject");
	
	private String messageCode;
	
	private RecallSubjectKey(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessageCode() {
		return messageCode;
	}
}
