package org.trescal.cwms.core.recall.entity.recallrule.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRule;

@Repository("RecallRuleDao")
public class RecallRuleDaoImpl extends BaseDaoImpl<RecallRule, Integer> implements RecallRuleDao
{
	@Override
	protected Class<RecallRule> getEntity() {
		return RecallRule.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<RecallRule> getAllInstrumentRecallRules(Instrument instrument, Boolean active){
        Criteria crit = getSession().createCriteria(RecallRule.class);
        crit.add(Restrictions.eq("instrument", instrument));
        //if(active != null) crit.add(Restrictions.eq("active", active));
        if(active != null) crit.add(Restrictions.sqlRestriction("active = " + active));
        return (List<RecallRule>) crit.list();
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<RecallRule> getAllInstrumentRecallRules(Integer coid, Integer subdivid, Integer personid, Integer plantid, Boolean active)
	{
		Criteria crit = getSession().createCriteria(RecallRule.class);
		StringBuffer buf = new StringBuffer();
		int critAdded = 0;
		if (active != null) crit.add(Restrictions.eq("active", active));
		// this basically sucks, but can't find anyway of conditional OR-ring
		// like this using normal criteria
		if (coid != null) {
			buf.append("{alias}.coid = " + coid);
			critAdded++;
		}
		if (subdivid != null) {
			if (critAdded > 0) buf.append(" or ");
			buf.append("{alias}.subdivid = " + subdivid);
			critAdded++;
		}
		if (personid != null) {
			if (critAdded > 0) buf.append(" or ");
			buf.append("{alias}.personid = " + personid);
			critAdded++;
		}
		if (plantid != null) {
			if (critAdded > 0) buf.append(" or ");
			buf.append(" {alias}.plantid = " + plantid);
			critAdded++;
		}
		if (!buf.toString().equals(""))
			crit.add(Restrictions.sqlRestriction("(" + buf.toString() + ")"));
		return (List<RecallRule>) crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RecallRule> getAllInstrumentSpecificRecallRules(int plantid, Boolean active, Integer excludeRuleId)
	{
		Criteria criteria = getSession().createCriteria(RecallRule.class);
		criteria.createCriteria("instrument").add(Restrictions.idEq(plantid));
		if (active != null) criteria.add(Restrictions.eq("active", active.booleanValue()));
		if (excludeRuleId != null) criteria.add(Restrictions.ne("id", excludeRuleId));
		return (List<RecallRule>) criteria.list();
	}
	
	@Override
	public void saveOrUpdateAll(List<RecallRule> rules)
	{
		rules.stream().forEach(rule -> saveOrUpdate(rule));
	}
}