package org.trescal.cwms.core.recall.entity.recallresponsestatus.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.recall.entity.recallresponsestatus.RecallResponseStatus;

@Repository("RecallResponseStatusDao")
public class RecallResponseStatusDaoImpl extends BaseDaoImpl<RecallResponseStatus, Integer> implements RecallResponseStatusDao
{
	@Override
	protected Class<RecallResponseStatus> getEntity() {
		return RecallResponseStatus.class;
	}
}