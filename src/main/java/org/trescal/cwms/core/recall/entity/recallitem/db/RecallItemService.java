package org.trescal.cwms.core.recall.entity.recallitem.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;

public interface RecallItemService extends BaseService<RecallItem, Integer>
{
	Long countRecallItemsForAddress(int recallid, int addressid);

	Long countRecallItemsForContact(int recallid, int contactid);
	
	Long countRecallItemsForCompany(int recallid, int companyid);
	
	Long countRecallItemsForSubdiv(int recallid, int subdivid);
	
	Long countRecallItemsExcludedForAddress(int recallid, int addressid);
	
	Long countRecallItemsExcludedForContact(int recallid, int contactid);
	
	Long countRecallItemsExcludedForCompany(int recallid, int companyid);
	
	Long countRecallItemsExcludedForSubdiv(int recallid, int subdivid);
	
	List<RecallItem> getRecallItems(List<Integer> recallItemIds);
	
	List<RecallItem> getRecallItemsForAddress(int recallid, int addrid);
	
	List<RecallItem> getRecallItemsForContact(int recallid, int personid);
	
	List<RecallItem> getRecallItemsForCompany(int recallid, int coid);
	
	List<RecallItem> getRecallItemsForSubdiv(int recallid, int subdivid);
	
	ResultWrapper getRecallItemsForRecallType(String recalltype, int recallid, Integer personid, Integer addrid);
}