package org.trescal.cwms.core.recall.generator;

public interface RecallSenderService {
	void sendRecall(Integer recallDetailId, RecallSenderStatistics statistics);
}
