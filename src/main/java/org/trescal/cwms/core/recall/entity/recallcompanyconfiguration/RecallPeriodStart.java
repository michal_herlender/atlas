package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration;

public enum RecallPeriodStart {
	FIRST_OF_MONTH,
	CURRENT_DATE;
}
