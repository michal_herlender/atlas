package org.trescal.cwms.core.recall.xssf;

import java.util.Comparator;

/*
 * Compares two keys by Subdiv, Address, then Contact
 * (Currently, expected that key will always be used within same Company)
 */
public class RecallSectionComparator implements Comparator<RecallSectionKey> {

	@Override
	public int compare(RecallSectionKey key0, RecallSectionKey key1) {
		int result = key0.getContact().getSub().getSubname().compareTo(key1.getContact().getSub().getSubname());
		if (result == 0) {
			result = key0.getContact().getSub().getSubdivid() - key1.getContact().getSub().getSubdivid();
			if (result == 0) {
				result = key0.getAddress().getAddr1().compareTo(key1.getAddress().getAddr1());
				if (result == 0) {
					result = key0.getAddress().getAddrid() - key1.getAddress().getAddrid();
					if (result == 0) {
						result = key0.getContact().getName().compareTo(key1.getContact().getName());
						if (result == 0) {
							result = key0.getContact().getId() - key1.getContact().getId();
						}
					}
				}
			}
		}
		return result;
	}

}
