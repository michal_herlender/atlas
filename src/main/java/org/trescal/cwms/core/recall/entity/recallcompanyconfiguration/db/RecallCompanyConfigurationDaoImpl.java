package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration_;

@Repository
public class RecallCompanyConfigurationDaoImpl extends BaseDaoImpl<RecallCompanyConfiguration, Integer> implements RecallCompanyConfigurationDao {

	@Override
	protected Class<RecallCompanyConfiguration> getEntity() {
		return RecallCompanyConfiguration.class;
	}

	@Override
	public List<Integer> getActiveCoids() {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		
		CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
		Root<RecallCompanyConfiguration> root = cq.from(RecallCompanyConfiguration.class);
		cq.where(cb.isTrue(root.get(RecallCompanyConfiguration_.active)));
		cq.select(root.get(RecallCompanyConfiguration_.id));
		
		return getEntityManager().createQuery(cq).getResultList();
	}

}
