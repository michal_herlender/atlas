package org.trescal.cwms.core.recall.entity.recallnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "recallnote")
public class RecallNote extends Note
{
	private Recall recall;
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return null;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "recallid", unique = false, nullable = false)
	public Recall getRecall() {
		return recall;
	}
	
	public void setRecall(Recall recall) {
		this.recall = recall;
	}
}