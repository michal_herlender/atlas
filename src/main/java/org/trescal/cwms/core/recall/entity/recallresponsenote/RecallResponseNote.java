package org.trescal.cwms.core.recall.entity.recallresponsenote;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallresponsestatus.RecallResponseStatus;

@Entity
@Table(name = "recallresponsenote")
public class RecallResponseNote extends Auditable
{
	private String comment;
	private int noteid;
	private RecallItem recallitem;
	private RecallResponseStatus recallResponseStatus;
	private Contact setBy;
	private Date setOn;

	@Length(min = 1, max = 1000)
	@Column(name = "comment", nullable = true, unique = false, length = 1000)
	public String getComment()
	{
		return this.comment;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "noteid", nullable = false, unique = true)
	@Type(type = "int")
	public int getNoteid()
	{
		return this.noteid;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "recallitemid")
	public RecallItem getRecallitem()
	{
		return this.recallitem;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid")
	public RecallResponseStatus getRecallResponseStatus()
	{
		return this.recallResponseStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setbyid")
	public Contact getSetBy()
	{
		return this.setBy;
	}

	@NotNull
	@Column(name = "seton")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSetOn()
	{
		return this.setOn;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public void setNoteid(int noteid)
	{
		this.noteid = noteid;
	}

	public void setRecallitem(RecallItem recallitem)
	{
		this.recallitem = recallitem;
	}

	public void setRecallResponseStatus(RecallResponseStatus recallResponseStatus)
	{
		this.recallResponseStatus = recallResponseStatus;
	}

	public void setSetBy(Contact setBy)
	{
		this.setBy = setBy;
	}

	public void setSetOn(Date setOn)
	{
		this.setOn = setOn;
	}
}
