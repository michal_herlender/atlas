package org.trescal.cwms.core.recall.xssf.generic;

import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.xssf.RecallSectionKey;
import org.trescal.cwms.core.recall.xssf.RecallXssfContext;

public class RecallXssfContextGeneric extends RecallXssfContext {
	private XSSFSheet sheetSummary;
	private Map<RecallSectionKey, List<RecallItem>> mapPastDue;
	private Map<RecallSectionKey, List<RecallItem>> mapCurrent;
	private Boolean usesCustomFields;
	private Integer colCustomFieldEnd;
	private Integer colLastField;
	public XSSFSheet getSheetSummary() {
		return sheetSummary;
	}
	public void setSheetSummary(XSSFSheet sheetSummary) {
		this.sheetSummary = sheetSummary;
	}
	public Map<RecallSectionKey, List<RecallItem>> getMapPastDue() {
		return mapPastDue;
	}
	public Map<RecallSectionKey, List<RecallItem>> getMapCurrent() {
		return mapCurrent;
	}
	public void setMapPastDue(Map<RecallSectionKey, List<RecallItem>> mapPastDue) {
		this.mapPastDue = mapPastDue;
	}
	public void setMapCurrent(Map<RecallSectionKey, List<RecallItem>> mapCurrent) {
		this.mapCurrent = mapCurrent;
	}
	public Integer getColCustomFieldEnd() {
		return colCustomFieldEnd;
	}
	public void setColCustomFieldEnd(Integer colCustomFieldEnd) {
		this.colCustomFieldEnd = colCustomFieldEnd;
	}
	public Boolean getUsesCustomFields() {
		return usesCustomFields;
	}
	public void setUsesCustomFields(Boolean usesCustomFields) {
		this.usesCustomFields = usesCustomFields;
	}
	public Integer getColLastField() {
		return colLastField;
	}
	public void setColLastField(Integer colLastField) {
		this.colLastField = colLastField;
	}

}
