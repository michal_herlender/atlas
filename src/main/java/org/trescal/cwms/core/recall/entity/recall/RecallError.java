package org.trescal.cwms.core.recall.entity.recall;

public enum RecallError {
	NONE,
	UNSUPPORTED_RECALL_TYPE,
	IO_EXCEPTION,
	EMAIL_SEND_FAILURE,
	BAD_EMAIL_ADDRESS,
	BAD_EMAIL_CC_ADDRESS,
	RECALL_LIST_NULL,
	RECALL_LIST_EMPTY,
	NO_CONFIG;
}
