package org.trescal.cwms.core.recall.entity.recallresponsenote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNote;

@Repository("RecallResponseNoteDao")
public class RecallResponseNoteDaoImpl extends BaseDaoImpl<RecallResponseNote, Integer> implements RecallResponseNoteDao
{
	@Override
	protected Class<RecallResponseNote> getEntity() {
		return RecallResponseNote.class;
	}
}