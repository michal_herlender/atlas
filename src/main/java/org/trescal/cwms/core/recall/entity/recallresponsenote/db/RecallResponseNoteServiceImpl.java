package org.trescal.cwms.core.recall.entity.recallresponsenote.db;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallitem.db.RecallItemService;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNote;
import org.trescal.cwms.core.recall.entity.recallresponsestatus.RecallResponseStatus;
import org.trescal.cwms.core.recall.entity.recallresponsestatus.db.RecallResponseStatusService;
import org.trescal.cwms.core.system.Constants;

@Service("RecallResponseNoteService")
public class RecallResponseNoteServiceImpl extends BaseServiceImpl<RecallResponseNote, Integer> implements RecallResponseNoteService
{
	@Autowired
	private RecallItemService recallItemServ;
	@Autowired
	private RecallResponseNoteDao rrnDao;
	@Autowired
	private RecallResponseStatusService rrsServ;
	@Autowired
	private UserService userService;
	
	@Override
	protected BaseDao<RecallResponseNote, Integer> getBaseDao() {
		return rrnDao;
	}
	
	@Override
	public ResultWrapper deleteRecallResponseNoteDWR(int noteid) {
		// get recall response note
		RecallResponseNote recallResNote = this.get(noteid);
		// note not null?
		if (recallResNote != null) {
			// delete note
			this.delete(recallResNote);
			// return success
			return new ResultWrapper(true, "", null, null);
		}
		else return new ResultWrapper(false, "The recall response note could not be found", null, null);
	}
	
	@Override
	public ResultWrapper insertRecallResponseNoteDWR(int recallitemid, int statusid, String comment, HttpSession session)
	{
		// find recall item to link to
		RecallItem ritem = this.recallItemServ.get(recallitemid);
		// recall item null?
		if (ritem == null) return new ResultWrapper(false, "The recall could not be found", null, null);
		// find recall response status
		RecallResponseStatus rrstatus = this.rrsServ.get(statusid);
		// status null?
		if (rrstatus == null) return new ResultWrapper(false, "The recall response status could not be found", null, null);
		// get the current contact
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.get(username).getCon();
		// contact null?
		if (contact == null) return new ResultWrapper(false, "The current contact could not be found", null, null);
		// create new recall response note
		RecallResponseNote rrNote = new RecallResponseNote();
		// populate
		rrNote.setRecallResponseStatus(rrstatus);
		rrNote.setComment(comment);
		rrNote.setRecallitem(ritem);
		rrNote.setSetBy(contact);
		rrNote.setSetOn(new Date());
		// insert the new recall response note
		this.save(rrNote);
		// By default, contact is not initialized by get() but we need its name.
		contact.getName();
		// return successful wrapper
		return new ResultWrapper(true, "", rrNote, null);
	}
}