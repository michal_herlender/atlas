package org.trescal.cwms.core.recall.entity.recall.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.dto.RecallCountDTO;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.RecallForm;
import org.trescal.cwms.core.recall.entity.recall.RecallType;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

public interface RecallDao extends AllocatedDao<Company, Recall, Integer> {

    List<Instrument> findIntrumentsOnActiveJobs(Collection<Instrument> insts);

    Recall findRecall(String recallNo);

    List<Instrument> getFutureRecallList(RecallType recallType, int id, LocalDate dateFrom, LocalDate dateTo);

    RecallItem getMostRecentRecallItemForInstrument(int plantid);

    List<Instrument> getProhibitionList(RecallType recallType, int id, LocalDate beforeDate);

    PagedResultSet<RecallCountDTO> getRecalls(PagedResultSet<RecallCountDTO> rs, RecallForm recallForm);

    List<CompanyKeyValue> getRecallCompanies(RecallForm recallForm);
}