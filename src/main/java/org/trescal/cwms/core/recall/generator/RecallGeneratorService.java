package org.trescal.cwms.core.recall.generator;

import org.trescal.cwms.core.recall.entity.recall.Recall;

public interface RecallGeneratorService {
	Recall generateRecall(Integer companyId);
	boolean generateRecallDetail(Integer recallId, Integer contactId);
}
