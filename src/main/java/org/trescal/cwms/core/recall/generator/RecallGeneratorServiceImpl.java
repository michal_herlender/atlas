package org.trescal.cwms.core.recall.generator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.recall.entity.recall.*;
import org.trescal.cwms.core.recall.entity.recall.db.RecallService;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallPeriodStart;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db.RecallCompanyConfigurationService;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetailComparator;
import org.trescal.cwms.core.recall.entity.recalldetail.db.RecallDetailService;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallitem.db.RecallItemService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.RecallLevel;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.RecallToContact;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Author : Galen Beck - 2017-05-12
 * First stage utility that generates Recall, RecallDetail, RecallItem entities which are sent in a second stage
 */
@Service
public class RecallGeneratorServiceImpl implements RecallGeneratorService {
	@Autowired
	private ContactService contactService; 
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private RecallCompanyConfigurationService rccService; 
	@Autowired
	private RecallDetailService recallDetailService; 
	@Autowired
	private RecallItemService recallItemService;
	@Autowired
	private RecallService recallService;
	/* System Defaults */
	@Autowired
	private RecallToContact systemDefaultRecallToContact;
	@Autowired
	private RecallLevel systemDefaultRecallLevel;
	
	public static final Logger logger = LoggerFactory.getLogger(RecallGeneratorServiceImpl.class);

	private Recall createRecall(RecallCompanyConfiguration config) {
		Company company = config.getCompany();

		GregorianCalendar calendar = new GregorianCalendar();
		LocalDate dateToday = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		String recallNo = dateToday.format(DateTimeFormatter.ISO_DATE);
		DateTools.setTimeToZeros(calendar);
		// Otherwise, we use current date to start recall
		if (config.getPeriodStart().equals(RecallPeriodStart.FIRST_OF_MONTH)) {
			calendar.set(GregorianCalendar.DAY_OF_MONTH, 1);
		}
		LocalDate dateFrom = config.getPeriodStart().equals(RecallPeriodStart.FIRST_OF_MONTH) ?
			dateToday.withDayOfMonth(1) : dateToday;
		calendar.add(GregorianCalendar.MONTH, config.getFutureMonths());
		calendar.set(GregorianCalendar.DAY_OF_MONTH, calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH));
		LocalDate plusMoths = dateToday.plusMonths(config.getFutureMonths());
		LocalDate dateTo = plusMoths.withDayOfMonth(plusMoths.lengthOfMonth());

		Recall recall = new Recall();
		recall.setCompany(company);
		recall.setDate(dateToday);
		recall.setDateFrom(dateFrom);
		recall.setDateTo(dateTo);
		recall.setItems(new HashSet<>());
		recall.setOrganisation(company.getDefaultBusinessContact().getSub().getComp());
		recall.setRecallNo(recallNo);
		recall.setRdetails(new TreeSet<>(new RecallDetailComparator()));
		
		this.recallService.insertRecall(recall);
		return recall;
	}
	
	/*
	 * Generates recall items for whole company at once
	 */
	private void createRecallItems(Recall recall, Company company) {
		List<Instrument> instruments = this.instrumentService.queryForRecall(company, recall.getDateTo());
		logger.info("instruments.size() : " + instruments.size());
		List<Integer> idsExcluded = this.instrumentService.queryForRecallWithActiveJobItems(company, recall.getDateTo());
		logger.info("idsExcluded.size() : " + idsExcluded.size());
		createRecallItems(recall, instruments, idsExcluded);
		logger.info("recall.items.size() : " + recall.getItems().size());
	}

	private void createRecallItems(Recall recall, List<Instrument> instruments, List<Integer> idsExcluded) {
		Set<Integer> idsExcludedSet = new HashSet<>(idsExcluded);
		for(Instrument instrument : instruments) {
			RecallItem recallItem = new RecallItem();
			//recallItem.setContact(contact);
			recallItem.setExcludeFromNotification(idsExcludedSet.contains(instrument.getPlantid()));
			recallItem.setInstrument(instrument);
			recallItem.setRecall(recall);
			this.recallItemService.save(recallItem);
			recall.getItems().add(recallItem);
		}
	}
	
	@Override
	public Recall generateRecall(Integer companyId) {
		long startTime = System.currentTimeMillis();
		RecallCompanyConfiguration config = this.rccService.get(companyId);
		Company company = config.getCompany();

		logger.info("Generating recall for company " + company.getConame() + " id " + company.getCoid());

		Recall recall = createRecall(config);
		createRecallItems(recall, company);

		long finishTime = System.currentTimeMillis();
		String message = recall.getItems().size() +
			" recall items";
		logger.info(message);
		logger.info("Execution time " + (finishTime - startTime) + " ms");
		return recall;
	}

	private boolean getIgnoredOnSetting(Contact contact) {
		// If recall is turned off for user, there should be no recall sent
		String sdTextValue = this.systemDefaultRecallToContact.getValueHierarchical(Scope.CONTACT, contact, null).getValue();
		Boolean sdBooleanValue = this.systemDefaultRecallToContact.parseValue(sdTextValue);
		return !sdBooleanValue;
	}

	private RecallType getRecallType(Contact contact) {
		String sdTextValue = this.systemDefaultRecallLevel.getValueHierarchical(Scope.CONTACT, contact, null).getValue();
		return this.systemDefaultRecallLevel.parseValue(sdTextValue);
	}
	
	private long countRecallItems(int recallid, Contact contact, RecallType recallType) {
		long result = 0;
		switch (recallType) {
		case COMPANY:
			result = this.recallItemService.countRecallItemsForCompany(recallid, contact.getSub().getComp().getCoid());
			break;
		case SUBDIV:
			result = this.recallItemService.countRecallItemsForSubdiv(recallid, contact.getSub().getSubdivid());
			break;
		case ADDRESS:
			result = this.recallItemService.countRecallItemsForAddress(recallid, contact.getDefAddress().getAddrid());
			break;
		case CONTACT:
			result = this.recallItemService.countRecallItemsForContact(recallid, contact.getPersonid());
			break;
		default:
			logger.error("Undefined recall type "+recallType);
			break;
		}
		return result;
	}
	
	private long countRecallItemsExcluded(int recallid, Contact contact, RecallType recallType) {
		long result = 0;
		switch (recallType) {
		case COMPANY:
			result = this.recallItemService.countRecallItemsExcludedForCompany(recallid, contact.getSub().getComp().getCoid());
			break;
		case SUBDIV:
			result = this.recallItemService.countRecallItemsExcludedForSubdiv(recallid, contact.getSub().getSubdivid());
			break;
		case ADDRESS:
			result = this.recallItemService.countRecallItemsExcludedForAddress(recallid, contact.getDefAddress().getAddrid());
			break;
		case CONTACT:
			result = this.recallItemService.countRecallItemsExcludedForContact(recallid, contact.getPersonid());
			break;
		default:
			logger.error("Undefined recall type "+recallType);
			break;
		}
		return result;
	}

	@Override
	public boolean generateRecallDetail(Integer recallId, Integer contactId) {
		Recall recall = this.recallService.findRecall(recallId);
		Contact contact = this.contactService.get(contactId);

		// If recall is ignored on setting (recall not enabled), no need to run queries or store RecallDetails
		boolean created = false;
		if (getIgnoredOnSetting(contact)) {
			logger.info("Skipping recall for "+contact.getName()+" contact id "+contact.getId()+" as not enabled for contact");
		}
		else {
			RecallType recallType = getRecallType(contact);
			logger.info("Generating recall for "+contact.getName()+" contact id "+contact.getId()+" type "+recallType);
			long countInstruments = countRecallItems(recall.getId(), contact, recallType);
			long countInstrumentsExcluded = countRecallItemsExcluded(recall.getId(), contact, recallType);
			// If user has NO instruments or all were ignored (e.g. all on job) there should be no recall sent
			boolean ignoredAllOnJob = (countInstruments > 0) && (countInstruments == countInstrumentsExcluded);
			RecallSendType recallSendType = (countInstruments == 0) || ignoredAllOnJob ? RecallSendType.NOTSENT : RecallSendType.EMAILED; 
			RecallSendStatus recallSendStatus = (countInstruments == 0) || ignoredAllOnJob ? RecallSendStatus.NOT_REQUIRED : RecallSendStatus.TO_SEND;
			
			RecallDetail recallDetail = new RecallDetail();
			recallDetail.setAddress(contact.getDefAddress());	// Note, some contacts don't have a default address
			recallDetail.setContact(contact);
			recallDetail.setError(false);
			recallDetail.setIgnoredAllOnJob(ignoredAllOnJob);
			recallDetail.setIgnoredOnSetting(false);
			recallDetail.setNumberOfItems((int) countInstruments);
			recallDetail.setPathToFile("");
			recallDetail.setRecall(recall);
			recallDetail.setRecallError(RecallError.NONE);
			recallDetail.setRecallSendStatus(recallSendStatus);
			recallDetail.setRecallSendType(recallSendType);
			recallDetail.setRecallType(recallType);
			this.recallDetailService.save(recallDetail);
			created = true;
		}
		return created;
	}
}
