package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration;

public interface RecallCompanyConfigurationDao extends BaseDao<RecallCompanyConfiguration, Integer> {
	List<Integer> getActiveCoids();
}
