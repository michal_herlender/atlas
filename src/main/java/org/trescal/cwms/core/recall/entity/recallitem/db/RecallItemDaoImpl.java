package org.trescal.cwms.core.recall.entity.recallitem.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;

@Repository("RecallItemDao")
public class RecallItemDaoImpl extends BaseDaoImpl<RecallItem, Integer> implements RecallItemDao
{
	@Override
	protected Class<RecallItem> getEntity() {
		return RecallItem.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RecallItem> getRecallItems(List<Integer> recallItemIds)
	{
		Criteria crit = getSession().createCriteria(RecallItem.class);
		crit.add(Restrictions.in("id", recallItemIds));
		return (List<RecallItem>) crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RecallItem> getRecallItemsForAddress(int recallid, int addrid)
	{
		Criteria crit = getSession().createCriteria(RecallItem.class);
		crit.createCriteria("recall").add(Restrictions.idEq(recallid));
		Criteria instcrit = crit.createCriteria("instrument");
		instcrit.createCriteria("add").add(Restrictions.idEq(addrid));
		instcrit.addOrder(Order.asc("nextCalDueDate"));
		return (List<RecallItem>) crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RecallItem> getRecallItemsForContact(int recallid, int personid)
	{
		Criteria crit = getSession().createCriteria(RecallItem.class);
		crit.setFetchMode("instrument", FetchMode.JOIN);
		Criteria recallCrit = crit.createCriteria("recall");
		recallCrit.add(Restrictions.idEq(recallid));
		Criteria instCrit = crit.createCriteria("instrument");
		Criteria contCrit = instCrit.createCriteria("con");
		contCrit.add(Restrictions.idEq(personid));
		instCrit.addOrder(Order.asc("nextCalDueDate"));
		return (List<RecallItem>) crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RecallItem> getRecallItemsForCompany(int recallid, int coid)
	{
		Criteria crit = getSession().createCriteria(RecallItem.class);
		crit.setFetchMode("instrument", FetchMode.JOIN);
		Criteria recallCrit = crit.createCriteria("recall");
		recallCrit.add(Restrictions.idEq(recallid));
		Criteria instCrit = crit.createCriteria("instrument");
		Criteria compCrit = instCrit.createCriteria("comp");		
		compCrit.add(Restrictions.idEq(coid));
		instCrit.addOrder(Order.asc("nextCalDueDate"));
		return (List<RecallItem>) crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<RecallItem> getRecallItemsForSubdiv(int recallid, int subdivid)
	{
		Criteria crit = getSession().createCriteria(RecallItem.class);
		crit.setFetchMode("instrument", FetchMode.JOIN);
		crit.setFetchMode("instrument.complementaryField", FetchMode.JOIN);
		Criteria recallCrit = crit.createCriteria("recall");
		recallCrit.add(Restrictions.idEq(recallid));
		Criteria instCrit = crit.createCriteria("instrument");
		Criteria contCrit = instCrit.createCriteria("con");
		Criteria subCrit = contCrit.createCriteria("sub");
		subCrit.add(Restrictions.idEq(subdivid));
		instCrit.addOrder(Order.asc("nextCalDueDate"));
		// Join creates copies!
		instCrit.setProjection(null);
		instCrit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<RecallItem>) crit.list();
	}
	
	private Criteria getBaseCriteriaForAddress(int recallid, int addressid) {
		Criteria crit = getSession().createCriteria(RecallItem.class);
		Criteria recallCrit = crit.createCriteria("recall");
		recallCrit.add(Restrictions.idEq(recallid));
		Criteria instCrit = crit.createCriteria("instrument");
		Criteria addrCrit = instCrit.createCriteria("add");
		addrCrit.add(Restrictions.idEq(addressid));
		return crit;
	}
	
	private Criteria getBaseCriteriaForContact(int recallid, int contactid) {
		Criteria crit = getSession().createCriteria(RecallItem.class);
		Criteria recallCrit = crit.createCriteria("recall");
		recallCrit.add(Restrictions.idEq(recallid));
		Criteria instCrit = crit.createCriteria("instrument");
		Criteria conCrit = instCrit.createCriteria("con");
		conCrit.add(Restrictions.idEq(contactid));
		return crit;
	}
	
	private Criteria getBaseCriteriaForCompany(int recallid, int companyid) {
		Criteria crit = getSession().createCriteria(RecallItem.class);
		Criteria recallCrit = crit.createCriteria("recall");
		recallCrit.add(Restrictions.idEq(recallid));
		Criteria instCrit = crit.createCriteria("instrument");
		Criteria conCrit = instCrit.createCriteria("con");
		Criteria subCrit = conCrit.createCriteria("sub");
		Criteria compCrit = subCrit.createCriteria("comp");
		compCrit.add(Restrictions.idEq(companyid));
		return crit;
	}
	
	private Criteria getBaseCriteriaForSubdiv(int recallid, int subdivid) {
		Criteria crit = getSession().createCriteria(RecallItem.class);
		Criteria recallCrit = crit.createCriteria("recall");
		recallCrit.add(Restrictions.idEq(recallid));
		Criteria instCrit = crit.createCriteria("instrument");
		Criteria conCrit = instCrit.createCriteria("con");
		Criteria subCrit = conCrit.createCriteria("sub");
		subCrit.add(Restrictions.idEq(subdivid));
		return crit;
	}

	@Override
	public Long countRecallItemsForAddress(int recallid, int addressid) {
		Criteria crit = getBaseCriteriaForAddress(recallid, addressid);
		return (Long) crit.setProjection(Projections.rowCount()).uniqueResult();
	}

	@Override
	public Long countRecallItemsForContact(int recallid, int contactid) {
		Criteria crit = getBaseCriteriaForContact(recallid, contactid);
		return (Long) crit.setProjection(Projections.rowCount()).uniqueResult();
	}
	
	@Override
	public Long countRecallItemsForCompany(int recallid, int companyid) {
		Criteria crit = getBaseCriteriaForCompany(recallid, companyid);
		return (Long) crit.setProjection(Projections.rowCount()).uniqueResult();
	}
	
	@Override
	public Long countRecallItemsForSubdiv(int recallid, int subdivid)
	{
		Criteria crit = getBaseCriteriaForSubdiv(recallid, subdivid);
		return (Long) crit.setProjection(Projections.rowCount()).uniqueResult();
	}

	@Override
	public Long countRecallItemsExcludedForAddress(int recallid, int addressid) {
		Criteria crit = getBaseCriteriaForAddress(recallid, addressid);
		crit.add(Restrictions.eq("excludeFromNotification", true));
		return (Long) crit.setProjection(Projections.rowCount()).uniqueResult();
	}
	
	@Override
	public Long countRecallItemsExcludedForContact(int recallid, int contactid) {
		Criteria crit = getBaseCriteriaForContact(recallid, contactid);
		crit.add(Restrictions.eq("excludeFromNotification", true));
		return (Long) crit.setProjection(Projections.rowCount()).uniqueResult();
	}
	
	@Override
	public Long countRecallItemsExcludedForCompany(int recallid, int companyid) {
		Criteria crit = getBaseCriteriaForCompany(recallid, companyid);
		crit.add(Restrictions.eq("excludeFromNotification", true));
		return (Long) crit.setProjection(Projections.rowCount()).uniqueResult();
	}
	
	@Override
	public Long countRecallItemsExcludedForSubdiv(int recallid, int subdivid)
	{
		Criteria crit = getBaseCriteriaForSubdiv(recallid, subdivid);
		crit.add(Restrictions.eq("excludeFromNotification", true));
		return (Long) crit.setProjection(Projections.rowCount()).uniqueResult();
	}
}