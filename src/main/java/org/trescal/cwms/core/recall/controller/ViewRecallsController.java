package org.trescal.cwms.core.recall.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.recall.dto.RecallCountDTO;
import org.trescal.cwms.core.recall.entity.recall.RecallForm;
import org.trescal.cwms.core.recall.entity.recall.db.RecallService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.servlet.ServletException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class ViewRecallsController {
	@Autowired
	private RecallService recServ;

	@InitBinder("formBackingObject")
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	public final static int RECALL_RESULTS_PER_PAGE = 25;

	@ModelAttribute("recallform")
	protected RecallForm formBackingObject(@RequestParam(name = "months", required = false, defaultValue = "3") Integer months) throws ServletException {

		RecallForm recallForm = new RecallForm();

		LocalDate recallDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())
			.minusMonths(1);
		recallForm.setDate(recallDate);
		recallForm.setMonths(months);

		return recallForm;

	}

	@RequestMapping(value = "/viewrecalls.htm", method = RequestMethod.GET)
	protected ModelAndView handleRequestInternal(Model model,
												 @ModelAttribute("recallform") RecallForm recallForm,
												 @RequestParam(value = "coid", required = false, defaultValue = "0") Integer coid) throws Exception {
		if (coid != null && coid != 0) {
			recallForm.setCoid(coid);
		}
		PagedResultSet<RecallCountDTO> rs = new PagedResultSet<>(recallForm.getResultsPerPage() == 0 ? RECALL_RESULTS_PER_PAGE : recallForm.getResultsPerPage(), recallForm.getPageNo() == 0 ? 1 : recallForm.getPageNo());
		this.recServ.getRecalls(rs, recallForm);
		recallForm.setResultsTotal(rs.getResultsCount());
		recallForm.setRs(rs);
		recallForm.setResultsPerPage(rs.getResultsPerPage());
		recallForm.setPageNo(rs.getCurrentPage());

		List<CompanyKeyValue> clients = this.recServ.getRecallCompanies(recallForm);
		model.addAttribute("clients", clients);

		return new ModelAndView("trescal/core/recall/viewrecalls", "recallform", recallForm);
	}
	
	@RequestMapping(value="/viewrecalls.htm", method=RequestMethod.POST)
	public ModelAndView onExport(Model model, @ModelAttribute("recallform") RecallForm recallForm) throws Exception {
		PagedResultSet<RecallCountDTO> rs = new PagedResultSet<>(recallForm.getResultsPerPage() == 0 ? RECALL_RESULTS_PER_PAGE : recallForm.getResultsPerPage(), recallForm.getPageNo() == 0 ? 1 : recallForm.getPageNo());
		this.recServ.getRecalls(rs, recallForm);
		recallForm.setResultsTotal(rs.getResultsCount());
		recallForm.setRs(rs);
		recallForm.setResultsPerPage(rs.getResultsPerPage());
		recallForm.setPageNo(rs.getCurrentPage());
		List<CompanyKeyValue> clients = this.recServ.getRecallCompanies(recallForm);
		model.addAttribute("clients", clients);

		return new ModelAndView("trescal/core/recall/viewrecalls", "recallform", recallForm);
	}
}

