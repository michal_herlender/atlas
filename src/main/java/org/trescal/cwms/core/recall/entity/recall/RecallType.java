package org.trescal.cwms.core.recall.entity.recall;

public enum RecallType
{
	CONTACT, ADDRESS, SUBDIV, COMPANY;
}
