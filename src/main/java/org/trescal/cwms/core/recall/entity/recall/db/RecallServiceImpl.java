package org.trescal.cwms.core.recall.entity.recall.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.dto.RecallCountDTO;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.RecallForm;
import org.trescal.cwms.core.recall.entity.recall.RecallType;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service("recallService")
public class RecallServiceImpl implements RecallService {
	@Autowired
	private AddressService addrServ;
	@Autowired
	private ContactService contServ;
	@Autowired
	private RecallDao recallDao;

	protected final Log logger = LogFactory.getLog(this.getClass());

	private List<Instrument> findIntrumentsOnActiveJobs(Collection<Instrument> insts)
	{
		return this.recallDao.findIntrumentsOnActiveJobs(insts);
	}

	@Override
	public Recall findRecall(Integer recallId)
	{
		return this.recallDao.find(recallId);
	}

	@Override
	public Recall findRecall(String recallNo)
	{
		return this.recallDao.findRecall(recallNo);
	}

	@Override
	public List<Instrument> getFutureRecallList(RecallType recallType, int id, LocalDate dateFrom, int months) {
		LocalDate dateTo = dateFrom.plusMonths(months);

		List<Instrument> insts = this.recallDao.getFutureRecallList(recallType, id, dateFrom, dateTo);

		if (insts.size() > 0) {
			List<Instrument> activeOnes = this.findIntrumentsOnActiveJobs(insts);
			insts.removeAll(activeOnes);
		}

		return insts;
	}

	public ResultWrapper getFutureRecallListDWR(int recallid, String recallType, int id, int months) {
		// create empty instrument list
		List<Instrument> insts;
		// find recall
		Recall recall = this.recallDao.find(recallid);
		// can we find recall?
		if (recall != null) {
			// check recall type
			if (recallType.equals(RecallType.CONTACT.toString())) {
				// check contact exists?
				if (this.contServ.get(id) != null) {
					// get future recall items for contact
					insts = this.getFutureRecallList(RecallType.CONTACT, id, recall.getDateTo(), months);
				} else {
					// return un-successful result wrapper
					return new ResultWrapper(false, "The contact to retrieve future recall items for could not be found", null, null);
				}
			} else
			{
				// check address exists
				if (this.addrServ.get(id) != null)
				{
					// get future recall items for contact
					insts = this.getFutureRecallList(RecallType.ADDRESS, id, recall.getDateTo(), months);
				}
				else
				{
					// return un-successful result wrapper
					return new ResultWrapper(false, "The address to retrieve future recall items for could not be found", null, null);
				}
			}

			// return successful result wrapper
			return new ResultWrapper(true, "", insts, null);
		}
		else
		{
			// return un-successful result wrapper
			return new ResultWrapper(false, "The recall could not be found", null, null);
		}
	}

	public RecallItem getMostRecentRecallItemForInstrument(int plantid)
	{
		return this.recallDao.getMostRecentRecallItemForInstrument(plantid);
	}

	/*
	 * GB 2016-04-28 This had an implementation that fails when there are over 2100 instruments past due (Singapore)  
	 * Perhaps rewrite recall query completely in future - but to get working I batched into chunks of 1000.
	 * Was:
		if (insts.size() > 0)
		{
			List<Instrument> activeOnes = this.findIntrumentsOnActiveJobs(insts);
			insts.removeAll(activeOnes);
		}
	 */
	@Override
	public List<Instrument> getProhibitionList(RecallType recallType, int id, LocalDate beforeDate) {
		List<Instrument> insts = this.recallDao.getProhibitionList(recallType, id, beforeDate);
		List<Instrument> result = new ArrayList<>();

		int fromIndex = 0;
		while (fromIndex < insts.size()) {
			int toIndex = Math.min(fromIndex + 1000, insts.size());
			List<Instrument> subList = insts.subList(fromIndex, toIndex);
			List<Instrument> activeOnes = this.findIntrumentsOnActiveJobs(subList);
			result.addAll(subList.stream().filter(i -> !activeOnes.contains(i)).collect(Collectors.toList()));
			fromIndex = toIndex;
		}

		return result;
	}

	public ResultWrapper getProhibitionListDWR(int recallid, String recallType, int id) {
		// create empty instrument list
		List<Instrument> insts;
		// find recall
		Recall recall = this.recallDao.find(recallid);
		// can we find recall?
		if (recall != null) {
			// check recall type
			if (recallType.equals(RecallType.CONTACT.toString())) {
				// check contact exists?
				if (this.contServ.get(id) != null) {
					// get prohibition items for contact
					insts = this.getProhibitionList(RecallType.CONTACT, id, recall.getDate());
				} else {
					// return un-successful result wrapper
					return new ResultWrapper(false, "The contact to retrieve prohibition items for could not be found", null, null);
				}
			} else
			{
				// check address exists
				if (this.addrServ.get(id) != null)
				{
					// get prohibition items for contact
					insts = this.getProhibitionList(RecallType.ADDRESS, id, recall.getDate());
				}
				else
				{
					// return un-successful result wrapper
					return new ResultWrapper(false, "The address to retrieve prohibition items for could not be found", null, null);
				}
			}

			// return successful result wrapper
			return new ResultWrapper(true, "", insts, null);
		} else {
			// return un-successful result wrapper
			return new ResultWrapper(false, "The recall could not be found", null, null);
		}
	}

	public RecallDao getRecallDao() {
		return this.recallDao;
	}

	public void insertRecall(Recall r) {
		this.recallDao.persist(r);
	}

	public void updateRecall(Recall r) {
		this.recallDao.merge(r);
	}

	@Override
	public PagedResultSet<RecallCountDTO> getRecalls(PagedResultSet<RecallCountDTO> rs, RecallForm recallForm) {
		return this.recallDao.getRecalls(rs, recallForm);
	}

	@Override
	public List<CompanyKeyValue> getRecallCompanies(RecallForm recallForm) {
		return this.recallDao.getRecallCompanies(recallForm);
	}
}
