package org.trescal.cwms.core.recall.entity.recallresponsenote.db;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNote;

public interface RecallResponseNoteService extends BaseService<RecallResponseNote, Integer>
{
	ResultWrapper deleteRecallResponseNoteDWR(int noteid);
	ResultWrapper insertRecallResponseNoteDWR(int recallitemid, int statusid, String comment, HttpSession session);
}