package org.trescal.cwms.core.recall.entity.recall;

public enum RecallSendStatus {
	TO_SEND,
	SENT,
	ERROR,
	CANCELLED,
	NOT_REQUIRED;
}
