package org.trescal.cwms.core.recall.entity.recalldetail.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.recall.entity.recall.RecallSendStatus;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;

@Service("RecallDetailService")
public class RecallDetailServiceImpl extends BaseServiceImpl<RecallDetail, Integer> implements RecallDetailService
{
	@Autowired
	private RecallDetailDao rdDao;
	
	@Override
	protected BaseDao<RecallDetail, Integer> getBaseDao() {
		return rdDao;
	}
	
	@Override
	public List<RecallDetail> getRecallDetailsForContact(int personid) {
		return this.rdDao.getRecallDetailsForContact(personid);
	}
	
	@Override
	public List<RecallDetail> getRecallDetailsForRecall(int recallid) {
		return this.rdDao.getRecallDetailsForRecall(recallid);
	}
	
	@Override
	public List<Integer> getRecallDetailIdsForSendStatus(RecallSendStatus recallSendStatus) {
		return this.rdDao.getRecallDetailIdsForSendStatus(recallSendStatus);
	}
}