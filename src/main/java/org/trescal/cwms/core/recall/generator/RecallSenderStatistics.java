package org.trescal.cwms.core.recall.generator;

public class RecallSenderStatistics {
	private int countFound;
	private int countSent;
	private int countError;
	
	public int getCountFound() {
		return countFound;
	}
	public int getCountSent() {
		return countSent;
	}
	public int getCountError() {
		return countError;
	}
	public void setCountFound(int countFound) {
		this.countFound = countFound;
	}
	public void incrementCountSent() {
		this.countSent++;
	}
	public void incrementCountError() {
		this.countError++;
	}
}
