package org.trescal.cwms.core.recall.entity.recall.db;

import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.dto.RecallCountDTO;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.RecallForm;
import org.trescal.cwms.core.recall.entity.recall.RecallType;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.List;

public interface RecallService {
    Recall findRecall(Integer recallId);

    Recall findRecall(String recallNo);

    /**
     * Creates a list of all items that will require calibration from the given
     * date and over the given period of months. Contact or address are chosen
     * by {@link RecallType}. List only shows active instruments that *should*
     * be recalled.
     *
     * @param recallType recallType the {@link RecallType}.
     * @param id         the id of the {@link Contact} or {@link Address} that owns the
     *                   equipment.
     * @param dateFrom   the date to search from.
     * @param months     the number of months to search over.
     * @return list of {@link Instrument}s.
     */
    List<Instrument> getFutureRecallList(RecallType recallType, int id, LocalDate dateFrom, int months);

    ResultWrapper getFutureRecallListDWR(int recallid, String recallType, int id, int months);

    RecallItem getMostRecentRecallItemForInstrument(int plantid);

    /**
     * Creates a list of all items that are currently out of calibration for
     * either the {@link Contact} or {@link Address} specified by the given id.
     * Contact or address are chosen by {@link RecallType}. List only shows
     * active instruments that *should* be recalled.
     *
     * @param this       allows a date to be provided before which we want to find
     *                   prohibition items when run in recall todays date is sufficient but
     *                   when loading after the event recall date needs to be passed.
     * @param recallType the {@link RecallType}.
     * @param id         the id of the {@link Contact} or {@link Address} that owns the
     *                   equipment.
     * @param beforeDate
     * @return
     */
    List<Instrument> getProhibitionList(RecallType recallType, int id, LocalDate beforeDate);

    ResultWrapper getProhibitionListDWR(int recallid, String recallType, int id);

    void insertRecall(Recall r);

    void updateRecall(Recall r);

    PagedResultSet<RecallCountDTO> getRecalls(PagedResultSet<RecallCountDTO> rs, RecallForm recallForm);

    List<CompanyKeyValue> getRecallCompanies(RecallForm recallForm);
}
