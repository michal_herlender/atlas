package org.trescal.cwms.core.recall.generator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.recall.entity.recall.RecallSendStatus;
import org.trescal.cwms.core.recall.entity.recalldetail.db.RecallDetailService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

@Component("RecallSenderTask")
public class RecallSenderTaskImpl implements RecallSenderTask {
	@Autowired
	private RecallSenderService recallSenderService; 
	@Autowired
	private ScheduledTaskService scheduledTaskService;
	@Autowired
	private RecallDetailService recallDetailService;
	
	private Logger logger = LoggerFactory.getLogger(RecallSenderTaskImpl.class);
	
	/*
	 * Intentionally not in a @Service component so that this runs outside of a transaction
	 * This way individual recalls receive their own transaction
	 */
	@Override
	public void executeTask() {
		if (this.scheduledTaskService.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace())) {
			logger.info("Starting task");
			String message = sendRecalls(); 
			this.scheduledTaskService.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
			logger.info("Completed task");
		}
		else {
			logger.info("Not running task, is inactive or not located");
		}
	}

	@Override
	public String sendRecalls() {
		// We want to create documents and send email for any recall item that has not yet been sent (on any recall for now)
		long startTime = System.currentTimeMillis(); 
		RecallSenderStatistics statistics = sendRecallsWithStatistics();
		long finishTime = System.currentTimeMillis();
		logger.info("Execution time "+(finishTime-startTime)+" ms");
		StringBuffer message = new StringBuffer();
		message.append(statistics.getCountFound());
		message.append(" recall details processed, ");
		message.append(statistics.getCountSent());
		message.append(" sent, ");
		message.append(statistics.getCountError());
		message.append(" error, ");
		message.append((finishTime-startTime)+" ms total time");
		
		logger.info(message.toString());
		return message.toString();
	}
	
	/**
	 * Sends recalls in individual transactions (needed as this is a potentially very long running task) 
	 */
	private RecallSenderStatistics sendRecallsWithStatistics() {
		List<Integer> listToSend = this.recallDetailService.getRecallDetailIdsForSendStatus(RecallSendStatus.TO_SEND);
		logger.info("Found "+listToSend.size()+" recall details to send");
		RecallSenderStatistics statistics = new RecallSenderStatistics();
		statistics.setCountFound(listToSend.size());
		for (Integer recallDetailId : listToSend) {
			this.recallSenderService.sendRecall(recallDetailId, statistics);
		}
		return statistics;
	}
}
