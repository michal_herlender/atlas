package org.trescal.cwms.core.recall.entity.recallitem;

import java.time.LocalDate;
import java.util.Comparator;

public class RecallItemByDateComparator implements Comparator<RecallItem> {
    public int compare(RecallItem ri1, RecallItem ri2) {
        if ((ri1.getId() == 0) || (ri2.getId() == 0)) {
            return Integer.compare(ri1.getInstrument().getPlantid(), ri2.getInstrument().getPlantid());
        } else {
            LocalDate d1 = ri1.getRecall().getDate();
            LocalDate d2 = ri2.getRecall().getDate();

            if (!d1.isEqual(d2)) {
                return d2.compareTo(d1);
            }

            return Integer.compare(ri2.getId(), ri1.getId());
        }
    }
}
