package org.trescal.cwms.core.recall.entity.recallrule.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.entity.recallrule.*;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface RecallRuleService extends BaseService<RecallRule, Integer> {
    /**
     * Deactivate's the {@link RecallRule} identified by the given id and if
     * updateRecallDate is true will also update the {@link Instrument}'s recall
     * date to be that calculated by the next applicable rule or the system default
     * interval.
     *
     * @param id               the {@link RecallRule} id.
     * @param plantid          the id of the {@link RecallRule}'s
     *                         {@link Instrument}.
     * @param updateRecallDate indicates if the recall date should recalculated.
     * @return {@link ResultWrapper} containing the {@link Instrument} if the update
     * was a success.
     */
    ResultWrapper deactivateRecallRule(int id, int plantid, boolean updateRecallDate);

    Set<RecallRule> getAllInstrumentRecallRules(Instrument inst, Boolean active);

    /**
     * Returns a Set of all active {@link RecallRule} entities that affect the
     * {@link Instrument} identified by the given plantid.
     *
     * @param plantid the id of the {@link Instrument}.
     * @param active  indicates if the {@link RecallRule}s returned should be active
	 *                or not.
	 * @return list of {@link RecallRule}.
	 */
	Set<RecallRule> getAllInstrumentRecallRules(int plantid, Boolean active);

	/**
	 * Returns a {@link List} of {@link RecallRule} entities that are specific to
	 * the {@link Instrument} identified by the given plantid only.
	 * 
	 * @param plantid       id of the {@link Instrument}.
	 * @param active        indicates if the {@link RecallRule} should be active,
	 *                      null for all.
	 * @param excludeRuleId id of a {@link RecallRule} to ignore in this search.
	 * @return {@link List} of {@link RecallRule}.
	 */
	List<RecallRule> getAllInstrumentSpecificRecallRules(int plantid, Boolean active, Integer excludeRuleId);

    /**
     * Gets the highest priority (i.e. currently used) recall interval for this
     * {@link Instrument}, or the calibration frequency if no {@link RecallRule} s
     * are active on the {@link Instrument}
     *
     * @param inst the {@link Instrument}
     * @return the recall interval
     */
    RecallRuleIntervalDto getCurrentRecallIntervalDto(Instrument inst, RecallRequirementType requirementType);

    /**
     * Inserts a new {@link RecallRule} for the given {@link Instrument}. If
     * applyToRecallDate is true then the {@link Instrument}'s current recall date
     * will be updated with this information.
     *
     * @param plantid        id of the {@link Instrument}.
     * @param interval       period in months
     * @param calibrateOn    a new calibration date for instrument if recallFromCert
     *                       is false.
     * @param recallFromCert update the recall date for the {@link Instrument} using
     *                       the previous certificate.
     * @return {@link ResultWrapper} containing the new {@link RecallRule} if the
     * operation was a success.
     */
    ResultWrapper insert(Integer plantid, Integer interval, LocalDate calibrateOn, Boolean recallFromCert, IntervalUnit unit,
                         RecallRequirementType requirement, Boolean updateInstrument, ServiceType serviceType);

    RecallRuleInterval getCurrentRecallInterval(Instrument inst, RecallRequirementType requirementType,
                                                ServiceType serviceType);

    /**
     * Inserts a new {@link RecallRule} for the given {@link Instrument}. If
     * applyToRecallDate is true then the {@link Instrument}'s current recall date
     * will be updated with this information
     *
     * @param dto {@RecallRuleDto} containing data needed by insert method
	 * @return {@link RecallRuleResponseDto} containing the new {@link RecallRule}
	 *         if the operation was a success.
	 */
	RecallRuleResponseDto insertAjax(RecallRuleRequestDto dto);

	/**
	 * Reactivate's the {@link RecallRule} identified by the given id and if
	 * updateRecallDate is true will also update the {@link Instrument}'s recall
	 * date to be that calculated by the next applicable rule or the system default
	 * interval. Any current active {@link Instrument} level {@link RecallRule} that
	 * exists for this {@link Instrument} will be deactivated.
	 * 
	 * @param id               the {@link RecallRule} id.
	 * @param plantid          the id of the {@link RecallRule}'s
	 *                         {@link Instrument}.
	 * @param updateRecallDate indicates if the recall date should recalculated.
	 * @return {@link ResultWrapper} containing the {@link Instrument} if the update
	 *         was a success.
	 */
	ResultWrapper reactivateRecallRule(int id, int plantid, boolean updateRecallDate);

	RecallRuleResponseDto reactivateAjax(RecallRuleRequestDto requestDTO);

	void saveOrUpdateAll(List<RecallRule> rules);

	RecallRuleResponseDto deactivateAjax(RecallRuleRequestDto recallRuleRequestDto);

}