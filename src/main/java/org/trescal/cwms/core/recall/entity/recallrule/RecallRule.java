package org.trescal.cwms.core.recall.entity.recallrule;

import org.hibernate.annotations.Check;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.recall.enums.RecallRuleType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "recallrule")
@Check(constraints = "([period]>=(1))")
public class RecallRule extends Auditable {
    private boolean active;
    private Company comp;
    private Contact con;
    private Contact deactivatedBy;
    private LocalDate deactivatedOn;
    private int id;
    private Instrument instrument;
    private int interval;
    private InstrumentModel model;
    private RecallRuleType recallRuleType;
    private Contact setBy;
    private LocalDate setOn;
    private Subdiv sub;
    private RecallRequirementType recallRequirementType;
    private IntervalUnit intervalUnit;
    private LocalDate calibrateOn;
    private Boolean updateInstrument;
    private Boolean calculateFromCert;
    private ServiceType serviceType;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coid")
    public Company getComp() {
        return this.comp;
    }

    public void setComp(Company comp) {
        this.comp = comp;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personid")
    public Contact getCon() {
        return this.con;
    }

    public void setCon(Contact con) {
        this.con = con;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deactivatedby")
    public Contact getDeactivatedBy() {
        return this.deactivatedBy;
    }

    public void setDeactivatedBy(Contact deactivatedBy) {
        this.deactivatedBy = deactivatedBy;
    }

    @Column(name = "deactivatedon", columnDefinition = "date")
    public LocalDate getDeactivatedOn() {
        return this.deactivatedOn;
    }

    public void setDeactivatedOn(LocalDate deactivatedOn) {
        this.deactivatedOn = deactivatedOn;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Min(0)
    @Column(name = "period", nullable = false)
    public int getInterval() {
        return this.interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plantid")
    public Instrument getInstrument() {
        return this.instrument;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    @Column(name = "recallruletype", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    public RecallRuleType getRecallRuleType() {
        return this.recallRuleType;
    }

    public void setRecallRuleType(RecallRuleType recallRuleType) {
        this.recallRuleType = recallRuleType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modelid")
    public InstrumentModel getModel() {
        return this.model;
    }

    public void setModel(InstrumentModel model) {
        this.model = model;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "setby")
    public Contact getSetBy() {
        return this.setBy;
    }

    public void setSetBy(Contact setBy) {
        this.setBy = setBy;
    }

    @NotNull
    @Column(name = "seton", columnDefinition = "date")
    public LocalDate getSetOn() {
        return this.setOn;
    }

    public void setSetOn(LocalDate setOn) {
        this.setOn = setOn;
    }

    @Column(name = "active", nullable = false, columnDefinition = "tinyint")
    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subdivid")
    public Subdiv getSub() {
        return this.sub;
    }

    public void setSub(Subdiv sub) {
        this.sub = sub;
    }

    @Column(name = "recallrequirementtype", length = 20)
    @Enumerated(EnumType.STRING)
    public RecallRequirementType getRecallRequirementType() {
        return recallRequirementType;
    }

    @Column(name = "intervalunit", length = 20)
    @Enumerated(EnumType.STRING)
    public IntervalUnit getIntervalUnit() {
        return intervalUnit;
    }

    @Column(name = "calibrateOn", columnDefinition = "date")
    public LocalDate getCalibrateOn() {
        return calibrateOn;
    }

    public void setCalibrateOn(LocalDate calibrateOn) {
        this.calibrateOn = calibrateOn;
    }

    @Column(name = "updateInstrument", columnDefinition = "bit")
    public Boolean getUpdateInstrument() {
        return updateInstrument;
    }

    public void setUpdateInstrument(Boolean updateInstrument) {
        this.updateInstrument = updateInstrument;
    }

    @Column(name = "calculateFromCert", columnDefinition = "bit")
    public Boolean getCalculateFromCert() {
        return calculateFromCert;
    }

    public void setCalculateFromCert(Boolean calculateFromCert) {
        this.calculateFromCert = calculateFromCert;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "servicetypeid")
    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setRecallRequirementType(RecallRequirementType recallRequirementType) {
        this.recallRequirementType = recallRequirementType;
    }

    public void setIntervalUnit(IntervalUnit intervalUnit) {
        this.intervalUnit = intervalUnit;
    }

    public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}
}