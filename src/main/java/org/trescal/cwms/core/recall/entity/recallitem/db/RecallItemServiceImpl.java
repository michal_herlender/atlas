package org.trescal.cwms.core.recall.entity.recallitem.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.RecallType;
import org.trescal.cwms.core.recall.entity.recall.db.RecallService;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;

@Service("RecallItemService")
public class RecallItemServiceImpl extends BaseServiceImpl<RecallItem, Integer> implements RecallItemService
{
	@Autowired
	private ContactService contServ;
	@Autowired
	private RecallItemDao recallItemDao;
	@Autowired
	private RecallService recallServ;
	
	@Override
	protected BaseDao<RecallItem, Integer> getBaseDao() {
		return recallItemDao;
	}

	@Override
	public Long countRecallItemsForAddress(int recallid, int addressid) {
		return this.recallItemDao.countRecallItemsForAddress(recallid, addressid);
	}

	@Override
	public Long countRecallItemsForContact(int recallid, int contactid) {
		return this.recallItemDao.countRecallItemsForContact(recallid, contactid);
	}
	
	@Override
	public Long countRecallItemsForCompany(int recallid, int companyid) {
		return this.recallItemDao.countRecallItemsForCompany(recallid, companyid);
	}
		
	@Override
	public Long countRecallItemsForSubdiv(int recallid, int subdivid) {
		return this.recallItemDao.countRecallItemsForSubdiv(recallid, subdivid);
	}
	
	@Override
	public Long countRecallItemsExcludedForAddress(int recallid, int addressid) {
		return this.recallItemDao.countRecallItemsExcludedForAddress(recallid, addressid);
	}
	
	@Override
	public Long countRecallItemsExcludedForContact(int recallid, int contactid) {
		return this.recallItemDao.countRecallItemsExcludedForContact(recallid, contactid);
	}
	
	@Override
	public Long countRecallItemsExcludedForCompany(int recallid, int companyid) {
		return this.recallItemDao.countRecallItemsExcludedForCompany(recallid, companyid);
	}
	
	@Override
	public Long countRecallItemsExcludedForSubdiv(int recallid, int subdivid) {
		return this.recallItemDao.countRecallItemsExcludedForSubdiv(recallid, subdivid);
	}
	
	@Override
	public List<RecallItem> getRecallItems(List<Integer> recallItemIds)
	{
		return this.recallItemDao.getRecallItems(recallItemIds);
	}
	
	public List<RecallItem> getRecallItemsForAddress(int recallid, int addrid)
	{
		return this.recallItemDao.getRecallItemsForAddress(recallid, addrid);
	}
	
	public List<RecallItem> getRecallItemsForContact(int recallid, int personid)
	{
		return this.recallItemDao.getRecallItemsForContact(recallid, personid);
	}
	
	public List<RecallItem> getRecallItemsForCompany(int recallid, int coid)
	{
		return this.recallItemDao.getRecallItemsForCompany(recallid, coid);
	}
	
	public List<RecallItem> getRecallItemsForSubdiv(int recallid, int subdivid)
	{
		return this.recallItemDao.getRecallItemsForSubdiv(recallid, subdivid);
	}
	
	public ResultWrapper getRecallItemsForRecallType(String recalltype, int recallid, Integer personid, Integer addrid)
	{
		// find recall
		Recall recall = this.recallServ.findRecall(recallid);
		// is recall null?
		if (recall != null) {
			List<RecallItem> riList = null;
			if (recalltype.equalsIgnoreCase(RecallType.CONTACT.name())) {
				riList = this.recallItemDao.getRecallItemsForContact(recallid, personid);
			}
			else if (recalltype.equalsIgnoreCase(RecallType.ADDRESS.name())) { 
				riList = this.recallItemDao.getRecallItemsForAddress(recallid, addrid);
			}
			else if (recalltype.equalsIgnoreCase(RecallType.SUBDIV.name())) { 
				Contact con = this.contServ.get(personid);
				riList = this.recallItemDao.getRecallItemsForSubdiv(recallid, con.getSub().getSubdivid());
			}
			else if (recalltype.equalsIgnoreCase(RecallType.COMPANY.name())) { 
				Contact con = this.contServ.get(personid);
				riList = this.recallItemDao.getRecallItemsForCompany(recallid, con.getSub().getComp().getCoid());
			}
			// find contact
			// is contact null?
			if (riList != null) {
				// return result wrapper
				return new ResultWrapper(true, "", riList, null);
			}
			else
			{
				// return error in wrapper
				return new ResultWrapper(false, "Error retrieving recall list", null, null);
			}
		}
		else
		// return error in wrapper
		return new ResultWrapper(false, "The recall could not be found", null, null);
	}
}