package org.trescal.cwms.core.recall.entity.recallrule.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRule;

public interface RecallRuleDao extends BaseDao<RecallRule, Integer>
{
	List<RecallRule> getAllInstrumentRecallRules(Instrument inst, Boolean active);
	
	List<RecallRule> getAllInstrumentRecallRules(Integer coid, Integer subdivid, Integer personid, Integer plantid, Boolean active);
	
	List<RecallRule> getAllInstrumentSpecificRecallRules(int plantid, Boolean active, Integer excludeRuleId);
	
	void saveOrUpdateAll(List<RecallRule> rules);
}