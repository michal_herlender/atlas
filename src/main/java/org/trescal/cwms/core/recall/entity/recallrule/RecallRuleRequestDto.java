package org.trescal.cwms.core.recall.entity.recallrule;

import java.time.LocalDate;

public class RecallRuleRequestDto {

    private Integer plantId;
    private String interval;
    private LocalDate calibrateOn;
    private Boolean recallFromCert;
    private String unit;
    private String requirement;
    private Integer recallRuleId;
    private Boolean updateRecallDate;
    private Boolean updateInstrument;
    private Integer serviceTypeId;

    
    public Boolean getRecallFromCert() { return recallFromCert; }

    public void setRecallFromCert(Boolean recallFromCert) { this.recallFromCert = recallFromCert;}

    public Integer getRecallRuleId() { return recallRuleId; }

    public void setRecallRuleId(Integer recallRuleId) { this.recallRuleId = recallRuleId; }

    public Boolean getUpdateRecallDate() { return updateRecallDate; }

    public void setUpdateRecallDate(Boolean updateRecallDate) { this.updateRecallDate = updateRecallDate; }

    public Integer getPlantId() {
        return plantId;
    }

    public void setPlantId(Integer plantId) {
        this.plantId = plantId;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public LocalDate getCalibrateOn() {
        return calibrateOn;
    }

    public void setCalibrateOn(LocalDate calibrateOn) {
        this.calibrateOn = calibrateOn;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public Boolean getUpdateInstrument() { return updateInstrument; }

    public void setUpdateInstrument(Boolean updateInstrument) { this.updateInstrument = updateInstrument; }

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(Integer calTypeId) {
		this.serviceTypeId = calTypeId;
	}
}
