package org.trescal.cwms.core.recall.entity.recallresponsenote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNote;

public interface RecallResponseNoteDao extends BaseDao<RecallResponseNote, Integer> {}