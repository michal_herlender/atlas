package org.trescal.cwms.core.recall.enums;

public enum RecallRuleType
{
	COMPANY(5), SUBDIV(4), CONTACT(3), COMPANY_MODEL(2), INSTRUMENT(1);

	private int order;

	private RecallRuleType(int order)
	{
		this.order = order;
	}

	public int getOrder()
	{
		return this.order;
	}

	public void setOrder(int order)
	{
		this.order = order;
	}

}
