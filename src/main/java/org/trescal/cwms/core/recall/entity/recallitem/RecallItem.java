package org.trescal.cwms.core.recall.entity.recallitem;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNote;
import org.trescal.cwms.core.recall.entity.recallresponsenote.RecallResponseNoteComparator;

@Entity
@Table(name = "recallitem", uniqueConstraints = { @UniqueConstraint(columnNames = { "plantid", "recallid" }) })
public class RecallItem extends Auditable
{
	@Deprecated
	private Contact contact;
	// flags if this item should have been recalled now, but has been exlcuded
	// from any notifications sent to the client. Typically this is because the
	// item is already on an active job. We keep in the recall item (instead of
	// deleting it) for historical and tracing purposes.
	private boolean excludeFromNotification = false;
	private int id;
	private Instrument instrument;
	private Recall recall;
	private Set<RecallResponseNote> recallResponseNotes;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	@Deprecated
	public Contact getContact()
	{
		return this.contact;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid")
	public Instrument getInstrument()
	{
		return this.instrument;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "recallid")
	public Recall getRecall()
	{
		return this.recall;
	}

	@SortComparator(RecallResponseNoteComparator.class)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "recallitem")
	public Set<RecallResponseNote> getRecallResponseNotes()
	{
		return this.recallResponseNotes;
	}

	@Column(name = "excludefromnotification", columnDefinition="tinyint")
	public boolean isExcludeFromNotification()
	{
		return this.excludeFromNotification;
	}

	@Deprecated
	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setExcludeFromNotification(boolean excludeFromNotification)
	{
		this.excludeFromNotification = excludeFromNotification;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInstrument(Instrument instrument)
	{
		this.instrument = instrument;
	}

	public void setRecall(Recall recall)
	{
		this.recall = recall;
	}

	public void setRecallResponseNotes(Set<RecallResponseNote> recallResponseNotes)
	{
		this.recallResponseNotes = recallResponseNotes;
	}
}
