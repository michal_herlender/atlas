package org.trescal.cwms.core.recall.entity.recallresponsestatus.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.recall.entity.recallresponsestatus.RecallResponseStatus;

public interface RecallResponseStatusDao extends BaseDao<RecallResponseStatus, Integer> {}