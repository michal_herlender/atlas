package org.trescal.cwms.core.recall.xssf.generic;

import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;

public interface RecallXssfCreatorGeneric {
	XSSFWorkbook createWorkbook(RecallDetail detail, List<RecallItem> items);
}
