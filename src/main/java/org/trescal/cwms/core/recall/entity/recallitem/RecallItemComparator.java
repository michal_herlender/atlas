package org.trescal.cwms.core.recall.entity.recallitem;

import java.util.Comparator;

public class RecallItemComparator implements Comparator<RecallItem>
{
	@Override
	public int compare(RecallItem o1, RecallItem o2)
	{
		if ((o1.getId() == 0) || (o2.getId() == 0))
		{
			return ((Integer) o1.getInstrument().getPlantid()).compareTo(o2.getInstrument().getPlantid());
		}
		else
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}

	}
}
