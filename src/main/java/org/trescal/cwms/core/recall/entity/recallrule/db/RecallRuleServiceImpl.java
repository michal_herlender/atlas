package org.trescal.cwms.core.recall.entity.recallrule.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.recall.dto.DeactivateRecallRuleWrapper;
import org.trescal.cwms.core.recall.entity.recallrule.*;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.recall.enums.RecallRuleType;
import org.trescal.cwms.core.recall.form.genericentityvalidator.RecallRuleValidator;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

@Service("RecallRuleService")
public class RecallRuleServiceImpl extends BaseServiceImpl<RecallRule, Integer> implements RecallRuleService {
	@Value("#{props['recall.default.interval']}")
	Integer defaultRecallInterval;
	@Autowired
	InstrumService instrumentServ;
	@Autowired
	RecallRuleDao recallRuleDao;
	@Autowired
	SessionUtilsService sessionServ;
	@Autowired
	RecallRuleValidator validator;
	@Autowired
	ServiceTypeService serviceTypeService;
	@Autowired
	TranslationService translationService;

	// If RecallRequirementType = SERVICE_TYPE then recall rule is only same type if
	// service type also matches
	private static Predicate<RecallRule> isSameType(RecallRequirementType requirementType, ServiceType serviceType) {
		return r -> (r.getRecallRequirementType().equals(requirementType)
				&& !r.getRecallRequirementType().equals(RecallRequirementType.SERVICE_TYPE))
				|| (r.getRecallRequirementType().equals(RecallRequirementType.SERVICE_TYPE)
						&& r.getServiceType().equals(serviceType));
	}

	@Override
	protected BaseDao<RecallRule, Integer> getBaseDao() {
		return recallRuleDao;
	}

	@Override
	public RecallRuleResponseDto deactivateAjax(RecallRuleRequestDto recallRuleRequestDto) {

		ResultWrapper result = this.deactivateRecallRule(recallRuleRequestDto.getRecallRuleId(),
				recallRuleRequestDto.getPlantId(), recallRuleRequestDto.getUpdateRecallDate());

		RecallRuleResponseDto responseDto = new RecallRuleResponseDto(result.isSuccess(), result.getMessage());

		Instrument instrument = this.instrumentServ.get(recallRuleRequestDto.getPlantId());

		if (result.getResults() != null) {
			DeactivateRecallRuleWrapper wrapper = (DeactivateRecallRuleWrapper) result.getResults();
			responseDto.setRecallRuleId(wrapper.getInactiveRecallRule().getId());
			responseDto.setDeactivatedBy(wrapper.getInactiveRecallRule().getDeactivatedBy().getName());
			responseDto.setDeactivatedOn(wrapper.getInactiveRecallRule().getDeactivatedOn());
			responseDto.setRequirement(wrapper.getInactiveRecallRule().getRecallRequirementType().getName());
			responseDto.setRequirementCode(wrapper.getInactiveRecallRule().getRecallRequirementType().name());
			if (recallRuleRequestDto.getUpdateRecallDate()) {
				if (wrapper.getActiveRecallRule() != null && wrapper.getActiveRecallRule().size() > 0) {
					TreeSet<RecallRule> activeRules = new TreeSet<>(new RecallRuleComparator());
					activeRules.addAll(wrapper.getActiveRecallRule());
					RecallRule activeRule = activeRules.first();
					responseDto.setInterval(activeRule.getInterval());
					responseDto.setUnits(activeRule.getIntervalUnit().getName(activeRule.getInterval()));

				} else {
					responseDto.setInterval(this.defaultRecallInterval);
					responseDto.setUnits(IntervalUnit.MONTH.getName(this.defaultRecallInterval));
				}
				responseDto.setInstOutOfCal(instrument.isOutOfCalibration());
				responseDto.setInstNextCalDate(instrument.getNextCalDueDate());
				responseDto.setInstNextCalDateInms(instrument.getNextCalDueDate().atStartOfDay(LocaleContextHolder.getTimeZone().toZoneId()).toInstant().toEpochMilli());
				responseDto.setInCalTimescalePercentage(instrument.getInCalTimescalePercentage());
				responseDto.setFinalMonthOfCalTimescalePercentage(instrument.getFinalMonthOfCalTimescalePercentage());
			}
		}

		return responseDto;
	}

	@Override
	public ResultWrapper deactivateRecallRule(int id, int plantid, boolean updateRecallDate) {
		// get the rule and update the status to deactivated.
		RecallRule rule = this.get(id);
		Instrument i = this.instrumentServ.get(plantid);
		if (rule == null)
			return new ResultWrapper(false, "An invalid recall rule was specified.", null, null);
		else {
			ResultWrapper wrapper;
			rule.setActive(false);
			rule.setDeactivatedBy(this.sessionServ.getCurrentContact());
			rule.getDeactivatedBy().getName(); // get contact for javascript binding
			rule.setDeactivatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			rule.setInstrument(i);
			this.merge(rule);
			if (updateRecallDate && rule.getRecallRequirementType().equals(RecallRequirementType.FULL_CALIBRATION)) {
				Set<RecallRule> allRules = this.getAllInstrumentRecallRules(plantid, true);
				Set<RecallRule> applicableRules = allRules.stream()
					.filter(r -> r.getRecallRequirementType().equals(RecallRequirementType.FULL_CALIBRATION))
					.collect(Collectors.toSet());
				int interval = this.defaultRecallInterval;
				IntervalUnit unit = IntervalUnit.MONTH;
				if (applicableRules.size() > 0) {
					for (RecallRule r : applicableRules) {
						interval = r.getInterval();
						unit = r.getIntervalUnit();
						break;
					}
				} else {
					interval = i.getCalFrequency();
					unit = i.getCalFrequencyUnit();
				}
				Instrument inst = this.instrumentServ.calculateAndUpdateRecallDate(plantid, interval, unit);
				DeactivateRecallRuleWrapper rrwrap = new DeactivateRecallRuleWrapper(rule, applicableRules, inst);
				wrapper = new ResultWrapper(true, "", rrwrap, null);
			} else {
				DeactivateRecallRuleWrapper rrwrap = new DeactivateRecallRuleWrapper(rule, null, null);
				wrapper = new ResultWrapper(true, "", rrwrap, null);
			}
			return wrapper;
		}
	}

	@Override
	public Set<RecallRule> getAllInstrumentRecallRules(Instrument inst, Boolean active) {
		Integer coid = null;
		Integer subdivid = null;
		Integer personid = null;
		if (inst != null) {
			coid = inst.getComp().getCoid();
			subdivid = inst.getAdd() == null ? null : inst.getAdd().getSub().getSubdivid();
			personid = inst.getCon().getPersonid();
		}
		List<RecallRule> rules = this.recallRuleDao.getAllInstrumentRecallRules(coid, subdivid, personid,
			inst != null ? inst.getPlantid() : null, active);
		TreeSet<RecallRule> ruleSet = new TreeSet<>(new RecallRuleComparator());
		ruleSet.addAll(rules);
		return ruleSet;
	}

	@Override
	public Set<RecallRule> getAllInstrumentRecallRules(int plantid, Boolean active) {
		Instrument inst = this.instrumentServ.get(plantid);
		return this.getAllInstrumentRecallRules(inst, active);
	}

	@Override
	public List<RecallRule> getAllInstrumentSpecificRecallRules(int plantid, Boolean active, Integer excludeRuleId) {
		return this.recallRuleDao.getAllInstrumentSpecificRecallRules(plantid, active, excludeRuleId);
	}

	@Override
	public RecallRuleIntervalDto getCurrentRecallIntervalDto(Instrument inst, RecallRequirementType requirementType) {
		RecallRuleIntervalDto dto = new RecallRuleIntervalDto();
		RecallRuleInterval recallRuleInterval = this.getCurrentRecallInterval(inst, requirementType, null);

		dto.setInterval(recallRuleInterval.getInterval());
		dto.setTranslatedUnit(recallRuleInterval.getUnit().getName(recallRuleInterval.getInterval()));
		dto.setUnitId(recallRuleInterval.getUnit().name());

		// Set Overdue flag
		switch (requirementType) {
			case FULL_CALIBRATION:
				dto.setOverdue(inst.getNextCalDueDate() != null && inst.getNextCalDueDate().isBefore(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())));
				break;
			case INTERIM_CALIBRATION:
				dto.setOverdue(inst.getInstrumentComplementaryField() != null
					&& inst.getInstrumentComplementaryField().getNextInterimCalibrationDate() != null
					&& inst.getInstrumentComplementaryField().getNextInterimCalibrationDate().before(new Date()));
				break;
			case MAINTENANCE:
				dto.setOverdue(inst.getInstrumentComplementaryField() != null
					&& inst.getInstrumentComplementaryField().getNextMaintenanceDate() != null
					&& inst.getInstrumentComplementaryField().getNextMaintenanceDate().before(new Date()));
				break;
			default:
				break;
		}

		return dto;
	}

	@Override
	public RecallRuleInterval getCurrentRecallInterval(Instrument inst, RecallRequirementType requirementType,
			ServiceType serviceType) {

		RecallRuleInterval recallRuleInterval = new RecallRuleInterval();
		Set<RecallRule> rRules = this.getAllInstrumentRecallRules(inst, true);
		if (serviceType != null) {
			rRules = rRules.stream()
				.filter(r -> r.getServiceType() != null
					&& r.getServiceType().getServiceTypeId() == serviceType.getServiceTypeId()).collect(Collectors.toSet());
		}
		if (!rRules.isEmpty()) {
			for (RecallRule rr : rRules) {
				// get only the first active one in the set for this requirement type (as these
				// are
				// ordered by priority)
				if (rr.isActive() && rr.getRecallRequirementType().equals(requirementType)) {
					recallRuleInterval.setInterval(rr.getInterval());
					recallRuleInterval.setUnit(rr.getIntervalUnit());
					break;
				}
			}
		}
		// use instrument cal frequency if looking for a FULL_CALIBRATION interval and
		// none was found from a recall rule
		if (recallRuleInterval.getInterval() == null) {
			if (requirementType.equals(RecallRequirementType.FULL_CALIBRATION)) {
				recallRuleInterval.setInterval(inst.getCalFrequency());
				recallRuleInterval.setUnit(inst.getCalFrequencyUnit());
			} else {
				recallRuleInterval.setInterval(0);
				recallRuleInterval.setUnit(IntervalUnit.MONTH);
			}
		}
		return recallRuleInterval;

	}

	@Override
	public RecallRuleResponseDto insertAjax(RecallRuleRequestDto requestDto) {
		IntervalUnit unit;
		RecallRequirementType requirementType;
		ServiceType serviceType = serviceTypeService.get(requestDto.getServiceTypeId());
		try {
			unit = IntervalUnit.valueOf(requestDto.getUnit());
		} catch (Exception e) {
			return new RecallRuleResponseDto(false, "Invalid unit");
		}
		try {
			requirementType = RecallRequirementType.valueOf(requestDto.getRequirement());
		} catch (Exception e) {
			return new RecallRuleResponseDto(false, "Invalid requirement type");
		}

		LocalDate calibrateOn = requestDto.getCalibrateOn();

		int interval = 0;
		if (!requestDto.getInterval().isEmpty()) {
			interval = Integer.parseInt(requestDto.getInterval());
		}

		ResultWrapper response;
		response = this.insert(requestDto.getPlantId(), interval, calibrateOn, requestDto.getRecallFromCert(), unit,
			requirementType, requestDto.getUpdateInstrument(), serviceType);

		return createResponseDto(response);
	}

	@Override
	public ResultWrapper insert(Integer plantid, Integer interval, LocalDate calibrateOn, Boolean recallFromCert,
								IntervalUnit unit, RecallRequirementType requirementType, Boolean updateInstrument,
								ServiceType serviceType) {
		Instrument i = this.instrumentServ.get(plantid);
		RecallRule rule;
		if (i == null)
			return new ResultWrapper(false, "An instrument with the barcode " + plantid + " could not be found.", null,
				null);
		else {
			rule = new RecallRule();
			rule.setActive(true);
			rule.setSetOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			rule.setSetBy(this.sessionServ.getCurrentContact());
			rule.getSetBy().getName(); // get contact for javascript binding
			rule.setInterval(interval);
			rule.setInstrument(i);
			rule.setRecallRuleType(RecallRuleType.INSTRUMENT);
			rule.setIntervalUnit(unit);
			rule.setCalculateFromCert(recallFromCert);
			rule.setUpdateInstrument(updateInstrument);
			if (requirementType.equals(RecallRequirementType.SERVICE_TYPE)) {
				rule.setServiceType(serviceType);
			} else {
				rule.setServiceType(null);
			}
			rule.setRecallRequirementType(requirementType);
			BindException errors = new BindException(rule, "rule");
			this.validator.validate(rule, errors);
			if (!errors.hasErrors()) {
				this.save(rule);
				// update any other instrument recall rules
				final List<RecallRule> otherRules = this.recallRuleDao.getAllInstrumentSpecificRecallRules(plantid,
						true, rule.getId());
				final List<RecallRule> otherRulesOfSameType = otherRules.stream()
						.filter(isSameType(requirementType, serviceType)).collect(Collectors.toList());
				if (otherRulesOfSameType.size() > 0) {
					for (RecallRule r : otherRulesOfSameType) {
						r.setActive(false);
						r.setDeactivatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
						r.setDeactivatedBy(this.sessionServ.getCurrentContact());
					}
					this.recallRuleDao.saveOrUpdateAll(otherRulesOfSameType);
				}
				// update the instrument's recall date if requested
				if (recallFromCert.equals(Boolean.TRUE)
						&& requirementType.equals(RecallRequirementType.FULL_CALIBRATION)) {
					this.instrumentServ.calculateAndUpdateRecallDate(i.getPlantid(), interval, unit);
					if (updateInstrument) {
						i.setCalFrequency(interval);
						i.setCalFrequencyUnit(unit);
					}
				} else {
					try {
						if (calibrateOn != null) {
							rule.setCalibrateOn(calibrateOn);
							switch (requirementType) {
							case FULL_CALIBRATION:
								if (rule.getInterval() > 0) {
									i.setNextCalDueDate(calibrateOn);
								} else {
									i.setNextCalDueDate(null);
								}
								break;
							case INTERIM_CALIBRATION:
								if (rule.getInterval() > 0) {
									i.getInstrumentComplementaryField().setNextInterimCalibrationDate(dateFromLocalDate(calibrateOn));
								} else {
									i.getInstrumentComplementaryField().setNextInterimCalibrationDate(null);
								}
								break;
							case MAINTENANCE:
								if (rule.getInterval() > 0) {
									i.getInstrumentComplementaryField().setNextMaintenanceDate(dateFromLocalDate(calibrateOn));
								} else {
									i.getInstrumentComplementaryField().setNextMaintenanceDate(null);
								}
								break;
								default:
									break;
							}
						}
						if (updateInstrument
							&& requirementType.equals(RecallRequirementType.FULL_CALIBRATION)) {
							i.setCalFrequency(interval);
							i.setCalFrequencyUnit(unit);
						}
					} catch (Exception e) {
						// if validation has passed this should never be thrown
						e.printStackTrace();
					}
				}
			}
			this.instrumentServ.merge(i);
			return new ResultWrapper(errors, rule);
		}
	}

	/**
	 * Used by both insertAjax() and reactivateAjax()
	 * 
	 * @return dto
	 */
	private RecallRuleResponseDto createResponseDto(ResultWrapper response) {
		RecallRuleResponseDto responseDto = new RecallRuleResponseDto(response.isSuccess(), response.getMessage());

		if (response.getResults() != null) {
			RecallRule rule = (RecallRule) response.getResults();
			responseDto.setRecallRuleId(rule.getId());
			responseDto.setSetBy(rule.getSetBy().getName());
			responseDto.setSetOn(rule.getSetOn());
			responseDto.setInterval(rule.getInterval());
			responseDto.setUnits(rule.getIntervalUnit().getName(rule.getInterval()));
			responseDto.setRuleNextDate(rule.getCalibrateOn());
			responseDto.setRequirement(!rule.getRecallRequirementType().equals(RecallRequirementType.SERVICE_TYPE)
				? rule.getRecallRequirementType().getName()
				: translationService.getCorrectTranslation(rule.getServiceType().getLongnameTranslation(),
				LocaleContextHolder.getLocale()));
			responseDto.setRequirementCode(rule.getRecallRequirementType().name());
			responseDto.setInstOutOfCal(rule.getInstrument().isOutOfCalibration());
			if (rule.getInstrument().getNextCalDueDate() != null) {
				responseDto.setInstNextCalDate(rule.getInstrument().getNextCalDueDate());
				responseDto.setInstNextCalDateInms(rule.getInstrument().getNextCalDueDate().atStartOfDay(LocaleContextHolder.getTimeZone().toZoneId()).toInstant().toEpochMilli());
				responseDto.setInCalTimescalePercentage(rule.getInstrument().getInCalTimescalePercentage());
				responseDto.setFinalMonthOfCalTimescalePercentage(
					rule.getInstrument().getFinalMonthOfCalTimescalePercentage());
			}
			InstrumentComplementaryField complementaryField = rule.getInstrument().getInstrumentComplementaryField();
			if (complementaryField != null) {
				if (complementaryField.getNextInterimCalibrationDate() != null) {
					responseDto.setInstNextInterimCalDate(
						complementaryField.getNextInterimCalibrationDate());
				}
				if (complementaryField.getNextMaintenanceDate() != null) {
					responseDto
						.setInstNextMaintenanceDate(complementaryField.getNextMaintenanceDate());
				}
			}
			if ((complementaryField != null) && (complementaryField.getNextMaintenanceDate() != null)) {
				responseDto.setMaintenanceOverdue(complementaryField.getNextMaintenanceDate().before(new Date()));
			} else {
				responseDto.setMaintenanceOverdue(false);
			}
			if ((complementaryField != null) && (complementaryField.getNextInterimCalibrationDate() != null)) {
				responseDto.setInterimCalOverdue(complementaryField.getNextInterimCalibrationDate().before(new Date()));
			} else {
				responseDto.setInterimCalOverdue(false);
			}
		}

		return responseDto;
	}

	@Override
	public RecallRuleResponseDto reactivateAjax(RecallRuleRequestDto requestDTO) {
		ResultWrapper result = this.reactivateRecallRule(requestDTO.getRecallRuleId(), requestDTO.getPlantId(),
				requestDTO.getUpdateRecallDate());

		return createResponseDto(result);
	}

	@Override
	public ResultWrapper reactivateRecallRule(int id, int plantid, boolean updateRecallDate) {
		RecallRule rule = this.get(id);
		Instrument i = this.instrumentServ.get(plantid);
		if (rule == null) {
			return new ResultWrapper(false, "An invalid recall rule was specified.", null, null);
		} else {
			// first deactivate any active recall rules of the same type
			List<RecallRule> currentRules = this.getAllInstrumentSpecificRecallRules(plantid, true, id);
			final List<RecallRule> otherRulesOfSameType = currentRules.stream()
				.filter(r -> r.getRecallRequirementType().equals(rule.getRecallRequirementType()))
				.collect(Collectors.toList());
			for (RecallRule r : otherRulesOfSameType) {
				r.setActive(false);
				r.setDeactivatedBy(this.sessionServ.getCurrentContact());
				r.setDeactivatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
				this.merge(r);
			}

			ResultWrapper wrapper;

			// now reactivate the requested rule
			rule.setActive(true);
			rule.setDeactivatedBy(null);
			rule.setDeactivatedOn(null);
			rule.setSetBy(this.sessionServ.getCurrentContact());
			rule.getSetBy().getName(); // get contact for javascript binding
			rule.setSetOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			rule.setInstrument(i);
			this.merge(rule);

			if (updateRecallDate) {
				this.instrumentServ.calculateAndUpdateRecallDate(plantid, rule.getInterval(), rule.getIntervalUnit());
			}
			wrapper = new ResultWrapper(true, "", rule, null);
			return wrapper;
		}
	}

	@Override
	public void saveOrUpdateAll(List<RecallRule> rules) {
		this.recallRuleDao.saveOrUpdateAll(rules);
	}

}