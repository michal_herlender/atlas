package org.trescal.cwms.core.recall.entity.recallresponsestatus.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.recall.entity.recallresponsestatus.RecallResponseStatus;

public interface RecallResponseStatusService extends BaseService<RecallResponseStatus, Integer>
{
	/**
	 * this method returns a {@link ResultWrapper} containing list of all
	 * {@link RecallResponseStatus}'s.
	 * 
	 * @return a {@link ResultWrapper}
	 */
	ResultWrapper getAllRecallResponseStatusDWR();
}