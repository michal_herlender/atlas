package org.trescal.cwms.core.recall.entity.recallrule;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
public class RecallRuleResponseDto {

    private Integer recallRuleId;
    private String setBy;
    private LocalDate setOn;
    private String deactivatedBy;
    private LocalDate deactivatedOn;
    private Integer interval;
    private String units;
    private String errorMessage;
    private String requirement;
    private String requirementCode;
    private Boolean instOutOfCal;
    private LocalDate instNextCalDate;
    private Long instNextCalDateInms;
    private Integer inCalTimescalePercentage;
    private Integer finalMonthOfCalTimescalePercentage;
    private Date instNextInterimCalDate;
    private Boolean interimCalOverdue;
    private Date instNextMaintenanceDate;
    private Boolean maintenanceOverdue;
    private LocalDate ruleNextDate;

    private Boolean success;


    public RecallRuleResponseDto(Boolean success, String errorMessage) {
        super();
        this.success = success;
        this.errorMessage = errorMessage;
    }


}
