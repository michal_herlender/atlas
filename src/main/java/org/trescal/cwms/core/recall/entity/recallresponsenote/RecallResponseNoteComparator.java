package org.trescal.cwms.core.recall.entity.recallresponsenote;

import java.util.Comparator;

public class RecallResponseNoteComparator implements Comparator<RecallResponseNote>
{
	@Override
	public int compare(RecallResponseNote o1, RecallResponseNote o2)
	{
		if ((o1.getNoteid() == 0) && (o2.getNoteid() == 0))
		{
			return 1;
		}
		else
		{
			return ((Integer) o2.getNoteid()).compareTo(o1.getNoteid());
		}
	}
}
