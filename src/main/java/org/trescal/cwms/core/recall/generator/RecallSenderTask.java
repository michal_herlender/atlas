package org.trescal.cwms.core.recall.generator;

public interface RecallSenderTask {
	void executeTask();
	String sendRecalls();
}
