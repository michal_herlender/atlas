package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;

/**
 * This entity stores configurations to activate recall reports and settings at company level
 * These settings are "company wide" so are not tracked via system defaults
 * @author Galen
 *
 */
@Entity
@Table(name="recallcompanyconfiguration")
public class RecallCompanyConfiguration {
	private int id;
	private boolean active;
	private RecallAttachmentType attachmentType;
	private Company company;
	private int futureMonths;
	private boolean pastDue;
	private RecallPeriodStart periodStart;
	private RecallSubjectKey subjectKey;
	private RecallTemplateType templateType;
	private String customText;
	
	/** 
	 * Populated with Company ID using MapsId, not generated ID 
	 */ 
	@Id
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return id;
	}

	@NotNull
	@Column(name = "active", nullable=false, columnDefinition="bit default 0")
	public boolean getActive() {
		return active;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "attachmenttype", nullable = false)
	public RecallAttachmentType getAttachmentType() {
		return attachmentType;
	}

	/**
	 * Intentionally not bidirectional 
	 */
	@MapsId
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="company_coid")
	public Company getCompany() {
		return company;
	}

	@NotNull
	@Column(name = "futuremonths", nullable=false)
	public int getFutureMonths() {
		return futureMonths;
	}

	@NotNull
	@Column(name = "pastdue", nullable=false, columnDefinition="bit default 1")
	public boolean getPastDue() {
		return pastDue;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "periodstart", nullable = false)
	public RecallPeriodStart getPeriodStart() {
		return periodStart;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "subjectkey", nullable = false)
	public RecallSubjectKey getSubjectKey() {
		return subjectKey;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "templatetype", nullable = false)
	public RecallTemplateType getTemplateType() {
		return templateType;
	}

	@NotNull
	@Length(max=2000)
	@Column(name = "customtext", nullable=false, columnDefinition="nvarchar(2000)")
	public String getCustomText() {
		return customText;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setAttachmentType(RecallAttachmentType attachmentType) {
		this.attachmentType = attachmentType;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setFutureMonths(int futureMonths) {
		this.futureMonths = futureMonths;
	}

	public void setPastDue(boolean pastDue) {
		this.pastDue = pastDue;
	}

	public void setPeriodStart(RecallPeriodStart periodStart) {
		this.periodStart = periodStart;
	}

	public void setSubjectKey(RecallSubjectKey subjectKey) {
		this.subjectKey = subjectKey;
	}

	public void setTemplateType(RecallTemplateType templateType) {
		this.templateType = templateType;
	}

	public void setCustomText(String customText) {
		this.customText = customText;
	}

}
