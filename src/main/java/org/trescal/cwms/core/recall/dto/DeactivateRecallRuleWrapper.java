package org.trescal.cwms.core.recall.dto;

import java.util.Set;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRule;

public class DeactivateRecallRuleWrapper
{
	private Set<RecallRule> activeRecallRule;
	private RecallRule inactiveRecallRule;
	private Instrument instrument;

	public DeactivateRecallRuleWrapper(RecallRule inactiveRecallRule, Set<RecallRule> activeRecallRule, Instrument instrument)
	{
		super();
		this.inactiveRecallRule = inactiveRecallRule;
		this.activeRecallRule = activeRecallRule;
		this.instrument = instrument;
	}

	public Set<RecallRule> getActiveRecallRule()
	{
		return this.activeRecallRule;
	}

	public RecallRule getInactiveRecallRule()
	{
		return this.inactiveRecallRule;
	}

	public Instrument getInstrument()
	{
		return this.instrument;
	}

	public void setActiveRecallRule(Set<RecallRule> activeRecallRule)
	{
		this.activeRecallRule = activeRecallRule;
	}

	public void setInactiveRecallRule(RecallRule inactiveRecallRule)
	{
		this.inactiveRecallRule = inactiveRecallRule;
	}

	public void setInstrument(Instrument instrument)
	{
		this.instrument = instrument;
	}

}
