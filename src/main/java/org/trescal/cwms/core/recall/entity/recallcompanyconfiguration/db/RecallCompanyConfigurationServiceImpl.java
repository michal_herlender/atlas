package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration;

@Service
public class RecallCompanyConfigurationServiceImpl 
	extends BaseServiceImpl<RecallCompanyConfiguration, Integer>
	implements RecallCompanyConfigurationService {

	@Autowired
	private RecallCompanyConfigurationDao baseDao;
	
	@Override
	protected BaseDao<RecallCompanyConfiguration, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public List<Integer> getActiveCoids() {
		return baseDao.getActiveCoids();
	}
	
	
}
