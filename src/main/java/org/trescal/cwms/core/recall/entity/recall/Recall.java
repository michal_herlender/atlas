package org.trescal.cwms.core.recall.entity.recall;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetailComparator;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallnote.RecallNote;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "recall")
public class Recall extends Allocated<Company> implements ComponentEntity, NoteAwareEntity {
    private Company company;
    private LocalDate date;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private File directory;
    private int id;
    private Set<RecallItem> items;
    private Set<RecallNote> notes;
    private Set<RecallDetail> rdetails;
    private String recallNo;

    // TODO add @NotNull constraints once field created / updated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coid")
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @NotNull
    @Column(name = "date", nullable = false)
    public LocalDate getDate() {
        return this.date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @NotNull
    @Column(name = "datefrom", nullable = false, columnDefinition = "date")
    public LocalDate getDateFrom() {
        return this.dateFrom;
    }

    @Override
    @Transient
    public File getDirectory() {
        return this.directory;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Override
    @Transient
    public Contact getDefaultContact() {
        return null;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    @Transient
    public ComponentEntity getParentEntity() {
        return null;
    }

    @Transient
    public Integer getPrivateActiveNoteCount() {
        return NoteCountCalculator.getPrivateActiveNoteCount(this);
    }

    /**
     * 2017-07-10 : Changed from recallNo to id to support multi-company recall
     * Directories will be renamed accordingly on file system upon deployment.
     */
    @Override
    @Transient
    public String getIdentifier() {
        return String.valueOf(id);
    }

    @Override
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "recall")
    @SortComparator(NoteComparator.class)
    public Set<RecallNote> getNotes() {
        return this.notes;
    }

    public void setNotes(Set<RecallNote> notes) {
        this.notes = notes;
    }

    @Transient
    public Integer getPrivateDeactivatedNoteCount() {
        return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
    }

    @Transient
    public Integer getPublicActiveNoteCount() {
        return NoteCountCalculator.getPublicActiveNoteCount(this);
    }

    @Transient
    public Integer getPublicDeactivatedNoteCount() {
        return NoteCountCalculator.getPublicDeactivedNoteCount(this);
    }

    @NotNull
    @Column(name = "dateto", nullable = false, columnDefinition = "date")
    public LocalDate getDateTo() {
        return this.dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    @Transient
    public List<Email> getSentEmails() {
        return null;
    }

    @NotEmpty
    @Column(name = "recallno", nullable = false, length = 10)
    public String getRecallNo() {
        return this.recallNo;
    }

    @Override
    @Transient
    public boolean isAccountsEmail() {
        return false;
    }

    @OneToMany(mappedBy = "recall")
    public Set<RecallItem> getItems() {
        return this.items;
    }

    public void setItems(Set<RecallItem> items) {
        this.items = items;
    }

    @OneToMany(mappedBy = "recall")
    @SortComparator(RecallDetailComparator.class)
    public Set<RecallDetail> getRdetails() {
        return this.rdetails;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    public void setRdetails(Set<RecallDetail> rdetails) {
        this.rdetails = rdetails;
    }

    public void setRecallNo(String recallNo) {
        this.recallNo = recallNo;
    }

	@Override
	public void setSentEmails(List<Email> emails)
	{

	}
}