package org.trescal.cwms.core.recall.entity.recallrule;


import org.trescal.cwms.core.system.enums.IntervalUnit;

public class RecallRuleInterval {

    Integer interval;
    IntervalUnit unit;
    
    public RecallRuleInterval() {
		super();
	}

	public RecallRuleInterval(Integer interval, IntervalUnit unit) {
		super();
		this.interval = interval;
		this.unit = unit;
	}

	public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public IntervalUnit getUnit() {
        return unit;
    }

    public void setUnit(IntervalUnit unit) {
        this.unit = unit;
    }
}
