package org.trescal.cwms.core.recall.entity.recallresponsestatus.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.recall.entity.recallresponsestatus.RecallResponseStatus;

@Service("RecallResponseStatusService")
public class RecallResponseStatusServiceImpl extends BaseServiceImpl<RecallResponseStatus, Integer> implements RecallResponseStatusService
{
	@Autowired
	private RecallResponseStatusDao rrsDao;
	
	@Override
	protected BaseDao<RecallResponseStatus, Integer> getBaseDao() {
		return rrsDao;
	}
	
	@Override
	public ResultWrapper getAllRecallResponseStatusDWR() {
		// get all recall response status's
		List<RecallResponseStatus> rrsList = this.getAll();
		// found status's?
		return rrsList.size() > 0 ? new ResultWrapper(true, "", rrsList, null) :
			new ResultWrapper(false, "No recall response status could be found", null, null);
	}
}