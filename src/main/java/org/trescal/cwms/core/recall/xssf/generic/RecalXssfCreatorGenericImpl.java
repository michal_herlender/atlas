package org.trescal.cwms.core.recall.xssf.generic;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.xssf.AbstractRecallXssfCreator;
import org.trescal.cwms.core.recall.xssf.RecallSectionComparator;
import org.trescal.cwms.core.recall.xssf.RecallSectionKey;

import java.util.*;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

@Component
public class RecalXssfCreatorGenericImpl extends AbstractRecallXssfCreator implements RecallXssfCreatorGeneric {

    // Definitions for headers at top that define
    private static final int COL_HEADER_LABEL;
    private static final int COL_HEADER_VALUE_START;
    @Autowired
    private MessageSource messages;
    private static final int COL_HEADER_VALUE_END;
    // Definitions for multi-header format at top
    private static final int MERGED_EQUIPMENT_BEGIN;
    private static final int MERGED_EQUIPMENT_END;
    private static final int MERGED_CALIBRATION_BEGIN;
    private static final int MERGED_CALIBRATION_END;
    private static final int MERGED_CUSTOM_FIELD_BEGIN;    // Region varies by customer
    // Definitions for individual columnns in main export
    private static final int COL_PLANT_NO;
    private static final int COL_PLANT_ID;
	private static final int COL_BRAND;
	private static final int COL_MODEL;
	private static final int COL_DESCRIPTION;
	private static final int COL_SERIAL_NO;
	private static final int COL_LOCATION;
	private static final int COL_LAST_CAL_DATE;
	private static final int COL_NEXT_CAL_DATE;
	private static final int COL_CAL_INTERVAL;
	private static final int COL_CAL_INTERVAL_UNITS;
	private static final int COL_CUSTOM_FIELD_BEGIN;	// Region varies by customer
	// Definitions for rows / columns in summary sheet table
	private static final int SUMMARY_COL_SUBDIVISION;
	private static final int SUMMARY_COL_ADDRESS;
	private static final int SUMMARY_COL_CONTACT;
	private static final int SUMMARY_COL_PASTDUE;
	private static final int SUMMARY_COL_DUE;
	// Definitions for rows / columns in summary sheet header
	private static final int SUMMARY_COL_LABEL;
	private static final int SUMMARY_COL_VALUE;

	private static final String KEY_COL_PLANT_NO = "recall.generic.column.plantno";
	private static final String KEY_COL_PLANT_ID = "recall.generic.column.plantid";
	private static final String KEY_COL_BRAND = "recall.generic.column.brand";
	private static final String KEY_COL_MODEL = "recall.generic.column.model";
	private static final String KEY_COL_DESCRIPTION = "recall.generic.column.description";
	private static final String KEY_COL_SERIAL_NO = "recall.generic.column.serialno";
	private static final String KEY_COL_LOCATION = "recall.generic.column.location";
	private static final String KEY_COL_LAST_CAL_DATE = "recall.generic.column.lastcaldate";
	private static final String KEY_COL_NEXT_CAL_DATE = "recall.generic.column.nextcaldate";
	private static final String KEY_COL_CAL_INTERVAL = "recall.generic.column.calinterval";
	private static final String KEY_COL_CAL_INTERVAL_UNITS = "recall.generic.column.calintervalunits";

	private static final String KEY_HEADER_SUBDIV = "recall.generic.header.subdiv";
	private static final String KEY_HEADER_ADDRESS = "recall.generic.header.address";
	private static final String KEY_HEADER_CONTACT = "recall.generic.header.contact";

	private static final String KEY_MERGED_EQUIPMENT = "recall.generic.merged.equipment";
	private static final String KEY_MERGED_CALIBRATION = "recall.generic.merged.calibration";
	private static final String KEY_MERGED_CUSTOM_FIELD = "recall.generic.merged.customfield";
	
	private static final String KEY_SUMMARY_REFERENCE = "email.ourref";
	private static final String KEY_SUMMARY_REPORT_TYPE = "email.recall.reporttype";
	private static final String KEY_SUMMARY_START_DATE = "tools.datefrom";
	private static final String KEY_SUMMARY_FINISH_DATE = "tools.dateto";

	private static final String KEY_SHEET_SUMMARY = "recall.generic.sheet.summary";
	private static final String KEY_SHEET_PAST_DUE = "recall.generic.sheet.pastdue";
	private static final String KEY_SHEET_DUE = "recall.generic.sheet.due";
	
	static {
		int colIndex = 0;
		// Definitions for individual columns
		COL_PLANT_NO = colIndex++;
		COL_PLANT_ID = colIndex++;
		COL_BRAND = colIndex++;
		COL_MODEL = colIndex++;
		COL_DESCRIPTION = colIndex++;
		COL_SERIAL_NO = colIndex++;
		COL_LOCATION = colIndex++;
		COL_LAST_CAL_DATE = colIndex++;
		COL_NEXT_CAL_DATE = colIndex++;
		COL_CAL_INTERVAL = colIndex++;
		COL_CAL_INTERVAL_UNITS = colIndex++;
		COL_CUSTOM_FIELD_BEGIN = colIndex;
		// Definitions for header areas
		COL_HEADER_LABEL = COL_PLANT_NO;
		COL_HEADER_VALUE_START = COL_PLANT_ID;
		COL_HEADER_VALUE_END = COL_DESCRIPTION;
		MERGED_EQUIPMENT_BEGIN = COL_PLANT_NO;
		MERGED_EQUIPMENT_END = COL_LOCATION;
		MERGED_CALIBRATION_BEGIN = COL_LAST_CAL_DATE;
		MERGED_CALIBRATION_END = COL_CAL_INTERVAL_UNITS;
		MERGED_CUSTOM_FIELD_BEGIN = COL_CUSTOM_FIELD_BEGIN;
		// Defnitions for summary
		colIndex = 0;
		SUMMARY_COL_SUBDIVISION = colIndex++;
		SUMMARY_COL_ADDRESS = colIndex++;
		SUMMARY_COL_CONTACT = colIndex++;
		SUMMARY_COL_PASTDUE = colIndex++;
		SUMMARY_COL_DUE = colIndex;
		colIndex = 0;
		SUMMARY_COL_LABEL = colIndex++;
		SUMMARY_COL_VALUE = colIndex;
	}
	
	@Override
	public XSSFWorkbook createWorkbook(RecallDetail detail, List<RecallItem> items) {
		Locale locale = detail.getContact().getLocale();
		// Required for enum based message access
		LocaleContextHolder.setLocale(locale);
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheetSummary = wb.createSheet(messages.getMessage(KEY_SHEET_SUMMARY, null, locale));
		XSSFSheet sheetPastDue = wb.createSheet(messages.getMessage(KEY_SHEET_PAST_DUE, null, locale));
		XSSFSheet sheetCurrent = wb.createSheet(messages.getMessage(KEY_SHEET_DUE, null, locale));
		RecallXssfContextGeneric context = new RecallXssfContextGeneric();
		context.setWb(wb);
		context.setSheetSummary(sheetSummary);
		context.setSheetPastDue(sheetPastDue);
		context.setSheetCurrent(sheetCurrent);
		context.setLocale(locale);
		initContext(context, detail);
		super.createStyles(context);
		generateMaps(items, context);
		populateSummary(context, detail);
		populateInstruments(context, context.getSheetPastDue(), context.getMapPastDue());
		populateInstruments(context, context.getSheetCurrent(), context.getMapCurrent());
		
		autosize(sheetCurrent, context.getColLastField());
		autosize(sheetPastDue, context.getColLastField());
		autosize(sheetSummary, SUMMARY_COL_DUE);
		return wb;
	}
	
	/**
	 * Generates sorted maps that are used for aggregating display of report by subdivision / address / contact
	 * Keys are held in both maps (even if list empty) to aid in summary data
	 */	
	private void generateMaps(List<RecallItem> items, RecallXssfContextGeneric context) {
		Map<RecallSectionKey, List<RecallItem>> mapCurrent = new TreeMap<>(new RecallSectionComparator());
		Map<RecallSectionKey, List<RecallItem>> mapPastDue = new TreeMap<>(new RecallSectionComparator());
		for (RecallItem item : items) {
			if (!item.isExcludeFromNotification()) {
				RecallSectionKey key = new RecallSectionKey(item.getInstrument().getAdd(), item.getInstrument().getCon());
				if (!mapCurrent.containsKey(key)) mapCurrent.put(key, new ArrayList<>());
				if (!mapPastDue.containsKey(key)) mapPastDue.put(key, new ArrayList<>());
				if (isPastDue(item)) {
					mapPastDue.get(key).add(item);
				}
				else if (isCurrent(item)) {
					mapCurrent.get(key).add(item);
				}
				else {
					// Do nothing, instrument recall date may have changed between generation and sending
					logger.error("Plant id "+item.getInstrument().getPlantid()+" was neither current nor past due, skipping");
				}
			}
		}
		context.setMapCurrent(mapCurrent);
		context.setMapPastDue(mapPastDue);
	}
	
	/**
	 * Calculates the position of the last field for the specified customer (taking into account flexible fields)
	 */
	private void initContext(RecallXssfContextGeneric context, RecallDetail detail) {
		int customFieldCount = detail.getRecall().getCompany().getInstrumentFieldDefinition().size();
		if (customFieldCount > 0) {
			context.setColCustomFieldEnd(COL_CAL_INTERVAL_UNITS+customFieldCount);
			context.setColLastField(COL_CAL_INTERVAL_UNITS+customFieldCount);
			context.setUsesCustomFields(true);
		}
		else {
			context.setUsesCustomFields(false);
			context.setColLastField(COL_CAL_INTERVAL_UNITS);
		}
	}

	private void populateSummary(RecallXssfContextGeneric context, RecallDetail detail) {
        // Populate labels regarding report
        addSummaryLabelRow(context, KEY_SUMMARY_REFERENCE, detail.getRecall().getRecallNo());
        addSummaryLabelRow(context, KEY_SUMMARY_START_DATE, dateFromLocalDate(detail.getRecall().getDateFrom()));
        addSummaryLabelRow(context, KEY_SUMMARY_FINISH_DATE, dateFromLocalDate(detail.getRecall().getDateTo()));
        addSummaryLabelRow(context, KEY_SUMMARY_REPORT_TYPE, detail.getRecallType().toString());

        // Populate summary section
        addSummaryHeader(context);
        for (RecallSectionKey key : context.getMapCurrent().keySet()) {
            addSummaryRow(context, key);
        }
    }
	private void addSummaryLabelRow(RecallXssfContextGeneric context, String labelKey, String text) {
		XSSFRow row = context.getSheetSummary().createRow(context.getSheetSummary().getLastRowNum() + 1);
		String labelText = messages.getMessage(labelKey, null, context.getLocale());
		super.createCell(row, SUMMARY_COL_LABEL, labelText, context.getCellStyleLightBlue());
		super.createCell(row, SUMMARY_COL_VALUE, text, context.getCellStyleGrey5());
	}
	private void addSummaryLabelRow(RecallXssfContextGeneric context, String labelKey, Date date) {
		XSSFRow row = context.getSheetSummary().createRow(context.getSheetSummary().getLastRowNum() + 1);
		String labelText = messages.getMessage(labelKey, null, context.getLocale());
		super.createCell(row, SUMMARY_COL_LABEL, labelText, context.getCellStyleLightBlue());
		super.createCell(row, SUMMARY_COL_VALUE, date, context.getCellStyleGrey5DateLeft());
	}
	private void addSummaryHeader(RecallXssfContextGeneric context) {
		Locale locale = context.getLocale();
		XSSFRow row = context.getSheetSummary().createRow(context.getSheetSummary().getLastRowNum() + 2);
		super.createCell(row, SUMMARY_COL_SUBDIVISION, messages.getMessage(KEY_HEADER_SUBDIV, null, locale), context.getCellStyleLightBlue());
		super.createCell(row, SUMMARY_COL_ADDRESS, messages.getMessage(KEY_HEADER_ADDRESS, null, locale), context.getCellStyleLightBlue());
		super.createCell(row, SUMMARY_COL_CONTACT, messages.getMessage(KEY_HEADER_CONTACT, null, locale), context.getCellStyleLightBlue());
		super.createCell(row, SUMMARY_COL_PASTDUE, messages.getMessage(KEY_SHEET_PAST_DUE, null, locale), context.getCellStyleLightBlue());
		super.createCell(row, SUMMARY_COL_DUE, messages.getMessage(KEY_SHEET_DUE, null, locale), context.getCellStyleLightBlue());
	}
	private void addSummaryRow(RecallXssfContextGeneric context, RecallSectionKey key) {
		XSSFRow row = context.getSheetSummary().createRow(context.getSheetSummary().getLastRowNum()+1);
		super.createCell(context, row, SUMMARY_COL_SUBDIVISION, key.getContact().getSub().getSubname());
		super.createCell(context, row, SUMMARY_COL_ADDRESS, key.getAddress().getAddr1() + " - "+key.getAddress().getTown());
		super.createCell(context, row, SUMMARY_COL_CONTACT, key.getContact().getName());
		int countPastDue = context.getMapPastDue().get(key).size();
		int countDue = context.getMapCurrent().get(key).size();
		row.createCell(SUMMARY_COL_PASTDUE).setCellValue(countPastDue);
		row.createCell(SUMMARY_COL_DUE).setCellValue(countDue);
	}
	
	/**
	 * Populates all sections of a sheet (past due or current)
	 */
	private void populateInstruments(RecallXssfContextGeneric context, XSSFSheet sheet, Map<RecallSectionKey, List<RecallItem>> map) {
		for (RecallSectionKey key : map.keySet()) {
			List<RecallItem> items = map.get(key);
			// We only display a section if it contains items
			if (!items.isEmpty()) {
				addSectionHeader(context, sheet, key);
				for (RecallItem item : items) {
					addRecallItem(context, sheet, item);
				}
			}
		}
	}
	/**
	 * Adds a section header (several rows) describing the subdiv / address / contact specified by the key
	 * followed by a header describing all instrument fields
	 */
	private void addSectionHeader(RecallXssfContextGeneric context, XSSFSheet sheet, RecallSectionKey key) {
		Locale locale = context.getLocale();
		addMergedHeaderRow(context, sheet, messages.getMessage(KEY_HEADER_SUBDIV, null, locale), key.getContact().getSub().getSubname(), true);
		addMergedHeaderRow(context, sheet, messages.getMessage(KEY_HEADER_ADDRESS, null, locale), key.getAddress().getAddr1()+" - "+key.getAddress().getTown(), false);
		addMergedHeaderRow(context, sheet, messages.getMessage(KEY_HEADER_CONTACT, null, locale), key.getContact().getName(), false);
		addMergedEquipmentHeader(context, sheet);
		addEquipmentHeader(context, sheet, key.getContact().getSub().getComp());
	}
	/**
	 * Adds a merged row (normally used to describe subdiv, address, contact) to sheet
	 */
	private void addMergedHeaderRow(RecallXssfContextGeneric context, XSSFSheet sheet, String label, String value, boolean firstRow) {
		int rowNum = sheet.getLastRowNum() + 1;
		if (firstRow) {
			rowNum++;
		}
		XSSFRow row = sheet.createRow(rowNum);
		createCell(row, COL_HEADER_LABEL, label, context.getCellStyleLightBlue());
		createCell(row, COL_HEADER_VALUE_START, value, context.getCellStyleGrey5());
		sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), COL_HEADER_VALUE_START, COL_HEADER_VALUE_END));
	}
	/**
	 * Adds merged header with three sections above equipment header 
	 */
	private void addMergedEquipmentHeader(RecallXssfContextGeneric context, XSSFSheet sheet) {
		Locale locale = context.getLocale();
		XSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);
		createCell(row, MERGED_EQUIPMENT_BEGIN, messages.getMessage(KEY_MERGED_EQUIPMENT, null, locale), context.getCellStyleGrey15());
		sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), MERGED_EQUIPMENT_BEGIN, MERGED_EQUIPMENT_END));
		createCell(row, MERGED_CALIBRATION_BEGIN, messages.getMessage(KEY_MERGED_CALIBRATION, null, locale), context.getCellStyleGrey5());
		sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), MERGED_CALIBRATION_BEGIN, MERGED_CALIBRATION_END));
		if (context.getUsesCustomFields()) {
			createCell(row, MERGED_CUSTOM_FIELD_BEGIN, messages.getMessage(KEY_MERGED_CUSTOM_FIELD, null, locale), context.getCellStyleGrey15());
			sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), MERGED_CUSTOM_FIELD_BEGIN, context.getColCustomFieldEnd()));
		}
	}
	/**
	 * Adds merged header with three sections above equipment header 
	 */
	private void addEquipmentHeader(RecallXssfContextGeneric context, XSSFSheet sheet, Company company) {
		Locale locale = context.getLocale();
		XSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);
		createCell(row, COL_PLANT_NO, messages.getMessage(KEY_COL_PLANT_NO, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_PLANT_ID, messages.getMessage(KEY_COL_PLANT_ID, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_BRAND, messages.getMessage(KEY_COL_BRAND, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_MODEL, messages.getMessage(KEY_COL_MODEL, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_DESCRIPTION, messages.getMessage(KEY_COL_DESCRIPTION, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_SERIAL_NO, messages.getMessage(KEY_COL_SERIAL_NO, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_LOCATION, messages.getMessage(KEY_COL_LOCATION, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_LAST_CAL_DATE, messages.getMessage(KEY_COL_LAST_CAL_DATE, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_NEXT_CAL_DATE, messages.getMessage(KEY_COL_NEXT_CAL_DATE, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_CAL_INTERVAL, messages.getMessage(KEY_COL_CAL_INTERVAL, null, locale), context.getCellStyleLightBlue());
		createCell(row, COL_CAL_INTERVAL_UNITS, messages.getMessage(KEY_COL_CAL_INTERVAL_UNITS, null, locale), context.getCellStyleLightBlue());
		// Flexible fields (varying length - possibly none)
		int col = COL_CUSTOM_FIELD_BEGIN;
		for (InstrumentFieldDefinition definition : company.getInstrumentFieldDefinition()) {
			createCell(row, col, definition.getName(), context.getCellStyleLightBlue());
			col++;
		}
	}

	/**
	 * Adds a row describing the specified instrument
	 */
	private void addRecallItem(RecallXssfContextGeneric context, XSSFSheet sheet, RecallItem item) {
        Locale locale = context.getLocale();
        Instrument inst = item.getInstrument();

        XSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);
        createCell(row, COL_PLANT_NO, inst.getPlantno(), context.getCellStyleText());
        row.createCell(COL_PLANT_ID).setCellValue(inst.getPlantid());
        createCell(row, COL_BRAND, getBrand(inst), context.getCellStyleText());
        createCell(row, COL_MODEL, getModel(inst), context.getCellStyleText());
        createCell(row, COL_DESCRIPTION, super.getDescription(inst, locale), context.getCellStyleText());
        createCell(row, COL_SERIAL_NO, inst.getSerialno(), context.getCellStyleText());
        createCell(row, COL_LOCATION, getLocation(inst), context.getCellStyleText());
        createCell(context, row, COL_LAST_CAL_DATE, super.getLastCalDate(inst));
        createCell(context, row, COL_NEXT_CAL_DATE, dateFromLocalDate(inst.getNextCalDueDate()));
        row.createCell(COL_CAL_INTERVAL).setCellValue(inst.getCalFrequency());
        createCell(context, row, COL_CAL_INTERVAL_UNITS, inst.getCalFrequencyUnit().getName(inst.getCalFrequency()));
        // Flexible fields (varying length - possibly none)
        int col = COL_CUSTOM_FIELD_BEGIN;
        for (InstrumentFieldDefinition definition : inst.getComp().getInstrumentFieldDefinition()) {
            createCell(context, row, col, getCustomField(inst, definition.getName()));
            col++;
        }
    }
	
	private String getBrand(Instrument instrument) {
		String result = null;
		if (instrument.getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC)) {
			if (instrument.getMfr() != null) result = instrument.getMfr().getName(); 
		}
		else {
			result = instrument.getModel().getMfr().getName();
		}
		return result;
	}
	
	/**
	 * Return the model number of the instrument, 
	 * givng preference to the instrument model name first and then using the model name from the model
	 */
	private String getModel(Instrument instrument) {
		String result;
		if ((instrument.getModelname() != null) && instrument.getModelname().length() > 0)
			result = instrument.getModelname();
		else result = instrument.getModel().getModel();
		return result;
	}
	
	private String getLocation(Instrument inst) {
		return inst.getLoc() == null ? null : inst.getLoc().getLocation(); 
	}
}
