package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration;

public enum RecallTemplateType {
	DEFAULT("/email/recall/generic.html"),
	IBERIA("/email/recall/es/iberia.html");
	
	private String thymeleafPath;
	
	private RecallTemplateType(String thymeleafPath) {
		this.thymeleafPath = thymeleafPath;
	}

	public String getThymeleafPath() {
		return thymeleafPath;
	}
}
