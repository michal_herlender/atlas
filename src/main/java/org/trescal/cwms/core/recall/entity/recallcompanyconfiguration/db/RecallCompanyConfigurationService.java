package org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration;

public interface RecallCompanyConfigurationService extends BaseService<RecallCompanyConfiguration, Integer> {
	List<Integer> getActiveCoids();
}
