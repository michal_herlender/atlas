package org.trescal.cwms.core.recall.xssf;

import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentValueService;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

/**
 * Base class with shared functionality between different formats of recall workbooks
 * @author Galen
 *
 */
public abstract class AbstractRecallXssfCreator {

	@Autowired
	private TranslationService translationService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private InstrumentValueService instrumentValueService; 
	
	public static final Logger logger = LoggerFactory.getLogger(AbstractRecallXssfCreator.class);
	
	// From https://poi.apache.org/apidocs/org/apache/poi/ss/usermodel/BuiltinFormats.html
	public static final String FORMAT_TEXT = "text";
	public static final String FORMAT_DATE = "d/m/yy";

	protected void autosize(XSSFSheet sheet, int lastColumnIndex) {
		for (int i = 0; i <= lastColumnIndex; i++) {
			sheet.autoSizeColumn(i);
		}
	}
	
	protected void createStyles(RecallXssfContext context) {
		XSSFWorkbook wb = context.getWb();
		XSSFDataFormat dataFormat = wb.createDataFormat();
		{
			XSSFFont font = wb.createFont();
			// Note, there is a bug with 255/255/255 in POI
			font.setColor(IndexedColors.WHITE.getIndex());
			
			XSSFCellStyle cellStyleLightBlue = wb.createCellStyle();
			cellStyleLightBlue.setDataFormat(dataFormat.getFormat(FORMAT_TEXT));
			cellStyleLightBlue.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyleLightBlue.setFillForegroundColor(new XSSFColor(new java.awt.Color(0,176,240)));
			cellStyleLightBlue.setFont(font);			
			context.setCellStyleLightBlue(cellStyleLightBlue);
		}
		{
			XSSFCellStyle cellStyleGrey5 = wb.createCellStyle();
			cellStyleGrey5.setDataFormat(dataFormat.getFormat(FORMAT_TEXT));
			cellStyleGrey5.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyleGrey5.setFillForegroundColor(new XSSFColor(new java.awt.Color(240,240,240)));
			context.setCellStyleGrey5(cellStyleGrey5);
		}
		{
			XSSFCellStyle cellStyleGrey5DateLeft = wb.createCellStyle();
			cellStyleGrey5DateLeft.setDataFormat(dataFormat.getFormat(FORMAT_TEXT));
			cellStyleGrey5DateLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyleGrey5DateLeft.setFillForegroundColor(new XSSFColor(new java.awt.Color(240,240,240)));
			cellStyleGrey5DateLeft.setDataFormat(dataFormat.getFormat(FORMAT_DATE));
			cellStyleGrey5DateLeft.setAlignment(HorizontalAlignment.LEFT);
			context.setCellStyleGrey5DateLeft(cellStyleGrey5DateLeft);
		}
		{
			XSSFCellStyle cellStyleGrey15 = wb.createCellStyle();
			cellStyleGrey15.setDataFormat(dataFormat.getFormat(FORMAT_TEXT));
			cellStyleGrey15.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyleGrey15.setFillForegroundColor(new XSSFColor(new java.awt.Color(217,217,217)));
			context.setCellStyleGrey15(cellStyleGrey15);
		}
		{
			XSSFCellStyle cellStyleText = wb.createCellStyle();
			cellStyleText.setDataFormat(dataFormat.getFormat(FORMAT_TEXT));
			context.setCellStyleText(cellStyleText);
		}
		{
			XSSFCellStyle cellStyleDate = wb.createCellStyle();
			cellStyleDate.setDataFormat(dataFormat.getFormat(FORMAT_DATE));
			context.setCellStyleDate(cellStyleDate);
		}
	}

	protected void createCell(RecallXssfContext context, XSSFRow row, int columnIndex, Date value) {
		createCell(row, columnIndex, value, context.getCellStyleDate());
	}

	protected void createCell(XSSFRow row, int columnIndex, Date value, XSSFCellStyle cellStyle) {
		if (value != null) {
			XSSFCell cell = row.createCell(columnIndex);
			cell.setCellValue(value);
			cell.setCellStyle(cellStyle);
		}
	}

	protected void createCell(RecallXssfContext context, XSSFRow row, int columnIndex, String value) {
		createCell(row, columnIndex, value, context.getCellStyleText());
	}

	protected void createCell(XSSFRow row, int columnIndex, String value, XSSFCellStyle cellStyle) {
		if (value != null) {
			XSSFCell cell = row.createCell(columnIndex);
			cell.setCellValue(value);
			cell.setCellStyle(cellStyle);
		}
	}

	protected String getCustomField(Instrument instrument, String customFieldName) {
		String result = null;
		InstrumentValue value = instrument.getFlexibleFields().stream().filter(field -> field.getInstrumentFieldDefinition().getName().equals(customFieldName)).findFirst().orElse(null);
		if (value != null) {
			result = this.instrumentValueService.getValueAsText(value);
		}
		return result;
	}
	protected String getDescription(Instrument instrument, Locale locale) {
		String result = "";
		if ((instrument.getCustomerDescription() != null) && !instrument.getCustomerDescription().isEmpty()) {
			result = instrument.getCustomerDescription();
		}
		else {
			String modelTranslation = this.translationService.getCorrectTranslation(instrument.getModel().getNameTranslations(), locale);
			if (modelTranslation != null) result = modelTranslation;
		}
		return result;
	}
	protected Date getLastCalDate(Instrument instrument) {
		return instrumentService.getLastCalDate(instrument);
	}
	
	// Note, all compared fields are TemporalType.DATE so time fields should be zero. 
	protected boolean isCurrent(RecallItem ritem) {
		boolean result = false;
		if (!ritem.isExcludeFromNotification()) {
			LocalDate calDueDate = ritem.getInstrument().getNextCalDueDate();
			if (calDueDate != null) {
				LocalDate startOfRecall = ritem.getRecall().getDateFrom();
				LocalDate endOfRecall = ritem.getRecall().getDateTo();
				if (!calDueDate.isBefore(startOfRecall) && !calDueDate.isAfter(endOfRecall)) {
					result = true;
				}
			}
		}
		return result;
	}
	
	// Note, all compared fields are TemporalType.DATE so time fields should be zero. 
	protected boolean isPastDue(RecallItem ritem) {
		boolean result = false;
		if (!ritem.isExcludeFromNotification()) {
			LocalDate calDueDate = ritem.getInstrument().getNextCalDueDate();
			if (calDueDate != null) {
				LocalDate startOfRecall = ritem.getRecall().getDateFrom();
				if (calDueDate.isBefore(startOfRecall)) result = true;
			}
		}
		return result;
	}

}
