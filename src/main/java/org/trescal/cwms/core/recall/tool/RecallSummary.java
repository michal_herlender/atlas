package org.trescal.cwms.core.recall.tool;

import java.util.ArrayList;
import java.util.List;

public class RecallSummary
{
	private int documents = 0;
	private int emailed = 0;
	private int faxed = 0;
	private List<String> errors = new ArrayList<String>();
	private List<String> ignoredDocs = new ArrayList<String>();
	private List<String> printedDocs = new ArrayList<String>();

	public void addDocument()
	{
		this.documents++;
	}

	public void addEmailed()
	{
		this.emailed++;
	}

	public void addError(String error)
	{
		this.errors.add(error);
	}

	public void addFaxed()
	{
		this.faxed++;
	}

	public void addIgnored(String ignored)
	{
		this.ignoredDocs.add(ignored);
	}

	public void addPrinted(String printed)
	{
		this.printedDocs.add(printed);
	}

	public int getDocuments()
	{
		return this.documents;
	}

	public int getEmailed()
	{
		return this.emailed;
	}

	public List<String> getErrors()
	{
		return this.errors;
	}

	public int getFaxed()
	{
		return this.faxed;
	}

	public List<String> getIgnoredDocs()
	{
		return this.ignoredDocs;
	}

	public List<String> getPrintedDocs()
	{
		return this.printedDocs;
	}

	public void setDocuments(int documents)
	{
		this.documents = documents;
	}

	public void setEmailed(int emailed)
	{
		this.emailed = emailed;
	}

	public void setErrors(List<String> errors)
	{
		this.errors = errors;
	}

	public void setFaxed(int faxed)
	{
		this.faxed = faxed;
	}

	public void setIgnoredDocs(List<String> ignoredDocs)
	{
		this.ignoredDocs = ignoredDocs;
	}

	public void setPrintedDocs(List<String> printedDocs)
	{
		this.printedDocs = printedDocs;
	}
}
