package org.trescal.cwms.core.recall.entity.recalldetail;

import java.net.URLEncoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.RecallError;
import org.trescal.cwms.core.recall.entity.recall.RecallSendStatus;
import org.trescal.cwms.core.recall.entity.recall.RecallSendType;
import org.trescal.cwms.core.recall.entity.recall.RecallType;
import org.trescal.cwms.core.tools.EncryptionTools;

@Entity
@Table(name = "recalldetail")
public class RecallDetail extends Auditable
{
	private Address address;
	private Contact contact;
	private Boolean error;
	private int id;
	private Boolean ignoredAllOnJob;
	private Boolean ignoredOnSetting;
	private int numberOfItems;
	private String pathToFile;
	private Recall recall;
	private RecallSendType recallSendType;
	private RecallType recallType;
	private RecallSendStatus recallSendStatus;
	private RecallError recallError;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "addrid", nullable=true)
	public Address getAddress()
	{
		return this.address;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable=true)
	public Contact getContact()
	{
		return this.contact;
	}

	@Column(name = "error", nullable=true, columnDefinition="tinyint")
	public Boolean getError()
	{
		return this.error;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "ignoredallonjob", nullable=true, columnDefinition="tinyint")
	public Boolean getIgnoredAllOnJob()
	{
		return this.ignoredAllOnJob;
	}

	@Column(name = "ignoredonsetting", nullable=true, columnDefinition="tinyint")
	public Boolean getIgnoredOnSetting()
	{
		return this.ignoredOnSetting;
	}

	@Type(type = "int")
	@Column(name = "numberofitems", nullable=true)
	public int getNumberOfItems()
	{
		return this.numberOfItems;
	}

	/**
	 * @return the clientRef
	 */
	@Length(min = 0, max = 200)
	@Column(name = "pathtofile", length = 200, nullable = true)
	public String getPathToFile()
	{
		return this.pathToFile;
	}
	
	@Transient
	public String getFilePathEncrypted() {
		try
		{
			return URLEncoder.encode(EncryptionTools.encrypt(this.pathToFile), "UTF-8");
		}
		catch (Exception ex) {
			return "";
		}
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "recallid", nullable=false)
	public Recall getRecall()
	{
		return this.recall;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "recallsendtype", nullable = false)
	public RecallSendType getRecallSendType()
	{
		return this.recallSendType;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "recalltype", nullable = false)
	public RecallType getRecallType()
	{
		return this.recallType;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "recallsendstatus", nullable = false)
	public RecallSendStatus getRecallSendStatus() {
		return recallSendStatus;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "recallerror", nullable = false)
	public RecallError getRecallError() {
		return recallError;
	}

	public void setAddress(Address address)
	{
		this.address = address;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setError(Boolean error)
	{
		this.error = error;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setIgnoredAllOnJob(Boolean ignoredAllOnJob)
	{
		this.ignoredAllOnJob = ignoredAllOnJob;
	}

	public void setIgnoredOnSetting(Boolean ignoredOnSetting)
	{
		this.ignoredOnSetting = ignoredOnSetting;
	}

	public void setNumberOfItems(int numberOfItems)
	{
		this.numberOfItems = numberOfItems;
	}

	public void setPathToFile(String pathToFile)
	{
		this.pathToFile = pathToFile;
	}

	public void setRecall(Recall recall)
	{
		this.recall = recall;
	}

	public void setRecallSendType(RecallSendType recallSendType)
	{
		this.recallSendType = recallSendType;
	}

	public void setRecallType(RecallType recallType)
	{
		this.recallType = recallType;
	}

	public void setRecallSendStatus(RecallSendStatus recallSendStatus) {
		this.recallSendStatus = recallSendStatus;
	}

	public void setRecallError(RecallError recallError) {
		this.recallError = recallError;
	}

}
