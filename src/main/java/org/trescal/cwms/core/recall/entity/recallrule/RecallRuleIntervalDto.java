package org.trescal.cwms.core.recall.entity.recallrule;



public class RecallRuleIntervalDto {

    private Integer interval;
    private String translatedUnit;
    private Boolean overdue;
    private String unitId;

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public String getTranslatedUnit() {
        return translatedUnit;
    }

    public void setTranslatedUnit(String translatedUnit) {
        this.translatedUnit = translatedUnit;
    }

    public Boolean getOverdue() {
        return overdue;
    }

    public void setOverdue(Boolean overdue) {
        this.overdue = overdue;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }
}
