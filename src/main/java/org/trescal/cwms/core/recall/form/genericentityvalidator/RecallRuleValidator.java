package org.trescal.cwms.core.recall.form.genericentityvalidator;

import org.springframework.validation.Errors;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRule;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

public class RecallRuleValidator extends AbstractBeanValidator {

	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(RecallRule.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		
		RecallRule rule = (RecallRule) target;
		super.validate(rule, errors);
	 
	}
}
