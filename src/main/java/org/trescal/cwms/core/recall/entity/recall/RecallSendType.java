package org.trescal.cwms.core.recall.entity.recall;

public enum RecallSendType
{
	EMAILED("Emailed"), FAXED("Faxed"), NOTSENT("Not Sent"), PRINTED("Printed");

	String desc;

	RecallSendType(String desc)
	{
		this.desc = desc;
	}

	public String getDesc()
	{
		return this.desc;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}

}
