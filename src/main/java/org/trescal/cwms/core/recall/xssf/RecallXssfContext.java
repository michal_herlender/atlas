package org.trescal.cwms.core.recall.xssf;

import java.util.Locale;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Placeholder class for the generation of a single XssfWorkbook.
 * Holds all the styles, etc... so that just this context can be passed between methods 
 */
public class RecallXssfContext {
	private Locale locale;
	private XSSFWorkbook wb;
	private XSSFSheet sheetPastDue;
	private XSSFSheet sheetCurrent;
	private XSSFCellStyle cellStyleLightBlue;
	private XSSFCellStyle cellStyleGrey5;
	private XSSFCellStyle cellStyleGrey5DateLeft;
	private XSSFCellStyle cellStyleGrey15;
	private XSSFCellStyle cellStyleText;
	private XSSFCellStyle cellStyleDate;
	
	public XSSFWorkbook getWb() {
		return wb;
	}
	public XSSFSheet getSheetPastDue() {
		return sheetPastDue;
	}
	public XSSFSheet getSheetCurrent() {
		return sheetCurrent;
	}
	public XSSFCellStyle getCellStyleLightBlue() {
		return cellStyleLightBlue;
	}
	public XSSFCellStyle getCellStyleText() {
		return cellStyleText;
	}
	public XSSFCellStyle getCellStyleDate() {
		return cellStyleDate;
	}
	public void setWb(XSSFWorkbook wb) {
		this.wb = wb;
	}
	public void setSheetPastDue(XSSFSheet sheetPastDue) {
		this.sheetPastDue = sheetPastDue;
	}
	public void setSheetCurrent(XSSFSheet sheetCurrentMonth) {
		this.sheetCurrent = sheetCurrentMonth;
	}
	public void setCellStyleLightBlue(XSSFCellStyle cellStyleHeader) {
		this.cellStyleLightBlue = cellStyleHeader;
	}
	public void setCellStyleText(XSSFCellStyle cellStyleText) {
		this.cellStyleText = cellStyleText;
	}
	public void setCellStyleDate(XSSFCellStyle cellStyleDate) {
		this.cellStyleDate = cellStyleDate;
	}
	public XSSFCellStyle getCellStyleGrey5() {
		return cellStyleGrey5;
	}
	public XSSFCellStyle getCellStyleGrey15() {
		return cellStyleGrey15;
	}
	public void setCellStyleGrey5(XSSFCellStyle cellStyleGrey5) {
		this.cellStyleGrey5 = cellStyleGrey5;
	}
	public void setCellStyleGrey15(XSSFCellStyle cellStyleGrey15) {
		this.cellStyleGrey15 = cellStyleGrey15;
	}
	public Locale getLocale() {
		return locale;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public XSSFCellStyle getCellStyleGrey5DateLeft() {
		return cellStyleGrey5DateLeft;
	}
	public void setCellStyleGrey5DateLeft(XSSFCellStyle cellStyleGrey5DateLeft) {
		this.cellStyleGrey5DateLeft = cellStyleGrey5DateLeft;
	}
}
