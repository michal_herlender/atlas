package org.trescal.cwms.core.recall.entity.recall;

import lombok.Data;
import org.trescal.cwms.core.recall.dto.RecallCountDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;

@Data
public class RecallForm {
    private Integer months;
    private LocalDate date;
    private Integer coid;
    private int pageNo;
    private int resultsPerPage;
    private int resultsTotal;
    private PagedResultSet<RecallCountDTO> rs;
}
