package org.trescal.cwms.core.recall.entity.recalldetail.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.recall.entity.recall.RecallSendStatus;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;

public interface RecallDetailService extends BaseService<RecallDetail, Integer>
{
	/**
	 * returns a list of {@link RecallDetail}'s for a specific company contact
	 * 
	 * @param personid id of contact to retrieve recall details for
	 */
	List<RecallDetail> getRecallDetailsForContact(int personid);
	
	/**
	 * this method returns all {@link RecallDetail}'s for a recall id
	 * 
	 * @param recallid id of the recall to return {@link RecallDetail}'s for
	 * @return {@link List} of {@link RecallDetail}'s
	 */
	List<RecallDetail> getRecallDetailsForRecall(int recallid);
	
	List<Integer> getRecallDetailIdsForSendStatus(RecallSendStatus recallSendStatus);
}