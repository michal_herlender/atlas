package org.trescal.cwms.core.recall.xssf;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;

/**
 * Sort key used for aggregating data in recall workbook
 */
public class RecallSectionKey {
	private Address address;
	private Contact contact;
	
	public RecallSectionKey(Address address, Contact contact) {
		if (address == null) throw new IllegalArgumentException("Address must not be null");
		if (contact == null) throw new IllegalArgumentException("Contact must not be null");
		
		this.address = address;
		this.contact = contact;
	}

	public Address getAddress() {
		return address;
	}

	public Contact getContact() {
		return contact;
	}
}
