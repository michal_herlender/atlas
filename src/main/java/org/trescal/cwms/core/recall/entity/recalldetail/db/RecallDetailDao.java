package org.trescal.cwms.core.recall.entity.recalldetail.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.recall.entity.recall.RecallSendStatus;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;

public interface RecallDetailDao extends BaseDao<RecallDetail, Integer>
{
	List<RecallDetail> getRecallDetailsForContact(int personid);
	
	List<RecallDetail> getRecallDetailsForRecall(int recallid);
	
	List<Integer> getRecallDetailIdsForSendStatus(RecallSendStatus recallSendStatus);
}