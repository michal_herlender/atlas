package org.trescal.cwms.core.recall.enums;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.spring.model.KeyValue;

public enum RecallRequirementType {

	FULL_CALIBRATION("recall.calibration", true, true), MAINTENANCE("recall.maintenance", false, false),
	INTERIM_CALIBRATION("recall.interim", false, false), SERVICE_TYPE("recall.servicetype", false, true);

	private final String nameCode;
	private MessageSource messageSource;
	private Boolean defaultType;
	private Boolean active;

	private RecallRequirementType(String nameCode, Boolean defaultType, Boolean active) {
		this.nameCode = nameCode;
		this.defaultType = defaultType;
		this.active = active;
	}

	@Component
	public static class RecallRequirementTypeMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (RecallRequirementType dateUnit : EnumSet.allOf(RecallRequirementType.class)) {
				dateUnit.setMessageSource(messageSource);
			}
		}
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getName() {
		return messageSource.getMessage(nameCode, null, LocaleContextHolder.getLocale());
	}

	public static List<KeyValue<String, String>> getAllTypesForSelect() {
		return Arrays.stream(RecallRequirementType.values()).filter(t -> t.active)
				.map(u -> new KeyValue<String, String>(u.name(), u.getName())).collect(Collectors.toList());
	}

	public Boolean isDefaultType() {
		return this.defaultType;
	}

	public Boolean getActive() {
		return active;
	}

}
