package org.trescal.cwms.core.recall.entity.recall.db;

import lombok.val;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrument.entity.instrumentstoragetype.InstrumentStorageType_;
import org.trescal.cwms.core.instrument.entity.instrumentusagetype.InstrumentUsageType_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.recall.dto.RecallCountDTO;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recall.RecallForm;
import org.trescal.cwms.core.recall.entity.recall.RecallType;
import org.trescal.cwms.core.recall.entity.recall.Recall_;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail_;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository("RecallDao")
public class RecallDaoImpl extends AllocatedToCompanyDaoImpl<Recall, Integer> implements RecallDao {

	@Override
	protected Class<Recall> getEntity() {
		return Recall.class;
	}


	@Override
	public List<Instrument> findIntrumentsOnActiveJobs(Collection<Instrument> insts) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			val jobItem = instrument.join(Instrument_.jobItems);
			cq.where(cb.isTrue(jobItem.join(JobItem_.state).get(ItemState_.active)),
				instrument.in(insts));
			return cq;
		});
	}

	@Override
	public Recall findRecall(String recallNo) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Recall.class);
			cq.where(cb.equal(cq.from(Recall.class).get(Recall_.recallNo), recallNo));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<Instrument> getFutureRecallList(RecallType recallType, int id, LocalDate dateFrom, LocalDate dateTo) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			cq.where(cb.or(cb.notEqual(instrument.get(Instrument_.status), InstrumentStatus.BER),
				cb.notEqual(instrument.get(Instrument_.status), InstrumentStatus.DO_NOT_RECALL)),
				cb.isTrue(instrument.join(Instrument_.usageType).get(InstrumentUsageType_.recall)),
				cb.isTrue(instrument.join(Instrument_.storageType).get(InstrumentStorageType_.recall)),
				cb.greaterThan(instrument.get(Instrument_.nextCalDueDate), dateFrom),
				cb.lessThanOrEqualTo(instrument.get(Instrument_.nextCalDueDate), dateTo),
				recallType == RecallType.ADDRESS ? cb.equal(instrument.get(Instrument_.add), id) :
					cb.equal(instrument.get(Instrument_.con), id)
			);
			cq.orderBy(cb.asc(instrument.get(Instrument_.nextCalDueDate)));
			cq.orderBy(cb.asc(instrument.get(Instrument_.serialno)));
			return cq;
		});
	}

	@Override
	public RecallItem getMostRecentRecallItemForInstrument(int plantid) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(RecallItem.class);
			val recallItem = cq.from(RecallItem.class);
			cq.where(cb.equal(recallItem.get(RecallItem_.instrument), plantid));
			cq.orderBy(cb.desc(recallItem.get(RecallItem_.recall).get(Recall_.date)));
			cq.orderBy(cb.desc(recallItem.get(RecallItem_.id)));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<Instrument> getProhibitionList(RecallType recallType, int id, LocalDate beforeDate) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Instrument.class);
			val instrument = cq.from(Instrument.class);
			cq.where(cb.or(cb.notEqual(instrument.get(Instrument_.status), InstrumentStatus.BER),
				cb.notEqual(instrument.get(Instrument_.status), InstrumentStatus.DO_NOT_RECALL)),
				cb.isTrue(instrument.join(Instrument_.usageType).get(InstrumentUsageType_.recall)),
				cb.isTrue(instrument.join(Instrument_.storageType).get(InstrumentStorageType_.recall)),
				beforeDate != null ?
					cb.lessThanOrEqualTo(instrument.get(Instrument_.nextCalDueDate), beforeDate) : cb.lessThanOrEqualTo(instrument.get(Instrument_.nextCalDueDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())),
				recallType == RecallType.ADDRESS ? cb.equal(instrument.get(Instrument_.add), id) :
					cb.equal(instrument.get(Instrument_.con), id)
			);
			cq.orderBy(cb.asc(instrument.get(Instrument_.nextCalDueDate)));
			cq.orderBy(cb.asc(instrument.get(Instrument_.serialno)));
			return cq;
		});
	}

	@Override
	public PagedResultSet<RecallCountDTO> getRecalls(PagedResultSet<RecallCountDTO> rs, RecallForm recallForm) {
		this.completePagedResultSet(rs, RecallCountDTO.class, cb -> cq -> {
			Root<Recall> recall = cq.from(Recall.class);
			Join<Recall, Company> company = recall.join(Recall_.company);
			
			Subquery<Long> itemsCountSubQuery = cq.subquery(Long.class);
			Root<RecallItem> recallItems = itemsCountSubQuery.from(RecallItem.class);
			itemsCountSubQuery.where(cb.equal(recallItems.get(RecallItem_.recall), recall));
			itemsCountSubQuery.select(cb.count(recallItems));
			
			Subquery<Long> detailsCountSubQuery = cq.subquery(Long.class);
			Root<RecallDetail> recallDetails = detailsCountSubQuery.from(RecallDetail.class);
			detailsCountSubQuery.where(cb.equal(recallDetails.get(RecallDetail_.recall), recall));
			detailsCountSubQuery.select(cb.count(recallDetails));
			
			if (recallForm.getDate() != null)
				cq.where(cb.greaterThan(recall.get(Recall_.date), recallForm.getDate()));
			
			if(recallForm.getCoid() != null){
				cq.where(cb.equal(company.get(Company_.coid), recallForm.getCoid()));
			}
			
			cq.distinct(true);
			CompoundSelection<RecallCountDTO> selection = cb.construct(RecallCountDTO.class, 
					recall.get(Recall_.id),
					recall.get(Recall_.recallNo),
					company.get(Company_.coname),
					recall.get(Recall_.date),
					recall.get(Recall_.dateFrom),
					recall.get(Recall_.dateTo),
					detailsCountSubQuery.getSelection(),
					itemsCountSubQuery.getSelection());
			
			List<javax.persistence.criteria.Order> orders = new ArrayList<>();
			orders.add(cb.desc(recall.get(Recall_.id)));
			return Triple.of(recall.get(Recall_.id), selection, orders);
		});
		return rs;
	}

	@Override
	public List<CompanyKeyValue> getRecallCompanies(RecallForm recallForm){
		return getResultList(cb ->{
			CriteriaQuery<CompanyKeyValue> cq = cb.createQuery(CompanyKeyValue.class);
			Root<Recall> recall = cq.from(Recall.class);
			Join<Recall, Company> company = recall.join(Recall_.company);
			if (recallForm.getDate() != null)
				cq.where(cb.greaterThan(recall.get(Recall_.date), recallForm.getDate()));
			cq.select(cb.construct(CompanyKeyValue.class, company.get(Company_.coid), company.get(Company_.coname)));
			cq.distinct(true);
			cq.orderBy(cb.asc(company.get(Company_.coname)));
			return cq;
		});
	}

}