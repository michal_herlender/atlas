package org.trescal.cwms.core.exchangeformat.dto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;

public class ExchangeFormatDTO {

	private Integer id;
	private String name;
	private String description;
	private String dateFormat;
	private Integer clientCompanyId;
	private Integer businessCompanyId;
	private Integer businessSubdivId;
	private String exchangeFormatLevel;
	private List<Integer> exchangeFormatFieldNameDetailsId;

	private ExchangeFormatTypeEnum exchangeFormatTypeEnum;
	private String exchangeFormatType;

	private String businessCompanyName;
	private String clientCompanyName;
	private String businessSudivName;

	private String lastModifiedBy;
	private Date lastModified;
	
	private boolean deleted;

	public ExchangeFormatDTO(ExchangeFormat ef) {
		super();
		this.id = ef.getId();
		this.name = ef.getName();
		this.description = ef.getDescription();
		this.dateFormat = ef.getDateFormat().getValue();
		if (ef.getClientCompany() != null)
			this.clientCompanyId = ef.getClientCompany().getId();
		if (ef.getBusinessCompany() != null)
			this.businessCompanyId = ef.getBusinessCompany().getId();
		if (ef.getBusinessSubdiv() != null)
			this.businessSubdivId = ef.getBusinessSubdiv().getId();
		this.exchangeFormatLevel = ef.getExchangeFormatLevel().name();
		this.exchangeFormatFieldNameDetailsId = ef.getExchangeFormatFieldNameDetails().stream()
				.map(ExchangeFormatFieldNameDetails::getId).collect(Collectors.toList());
		this.deleted = ef.isDeleted();
	}

	public ExchangeFormatDTO(Integer id, String name, String description, ExchangeFormatTypeEnum exchangeFormatType,
			ExchangeFormatLevelEnum exchangeFormatLevel, Integer businessCompanyId, String businessCompanyName,
			Integer clientCompanyId, String clientCompanyName, Integer businessSubdivId, String businessSudivName,
			Contact lastModifiedBy, Date lastModified, Boolean deleted) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.exchangeFormatType = exchangeFormatType.getValue();
		this.exchangeFormatLevel = exchangeFormatLevel.toString();
		this.businessCompanyId = businessCompanyId;
		this.businessCompanyName = businessCompanyName;
		this.clientCompanyId = clientCompanyId;
		this.clientCompanyName = clientCompanyName;
		this.businessSubdivId = businessSubdivId;
		this.businessSudivName = businessSudivName;
		this.lastModifiedBy = lastModifiedBy.getName();
		this.lastModified=lastModified;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public Integer getClientCompanyId() {
		return clientCompanyId;
	}

	public void setClientCompanyId(Integer clientCompanyId) {
		this.clientCompanyId = clientCompanyId;
	}

	public Integer getBusinessCompanyId() {
		return businessCompanyId;
	}

	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}

	public Integer getBusinessSubdivId() {
		return businessSubdivId;
	}

	public void setBusinessSubdivId(Integer businessSubdivId) {
		this.businessSubdivId = businessSubdivId;
	}

	public String getExchangeFormatLevel() {
		return exchangeFormatLevel;
	}

	public void setExchangeFormatLevel(String exchangeFormatLevel) {
		this.exchangeFormatLevel = exchangeFormatLevel;
	}

	public List<Integer> getExchangeFormatFieldNameDetailsId() {
		return exchangeFormatFieldNameDetailsId;
	}

	public void setExchangeFormatFieldNameDetailsId(List<Integer> exchangeFormatFieldNameDetailsId) {
		this.exchangeFormatFieldNameDetailsId = exchangeFormatFieldNameDetailsId;
	}

	public String getExchangeFormatType() {
		return exchangeFormatType;
	}

	public void setExchangeFormatType(String exchangeFormatType) {
		this.exchangeFormatType = exchangeFormatType;
	}

	public String getBusinessCompanyName() {
		return businessCompanyName;
	}

	public void setBusinessCompanyName(String businessCompanyName) {
		this.businessCompanyName = businessCompanyName;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public ExchangeFormatTypeEnum getExchangeFormatTypeEnum() {
		return exchangeFormatTypeEnum;
	}

	public void setExchangeFormatTypeEnum(ExchangeFormatTypeEnum exchangeFormatTypeEnum) {
		this.exchangeFormatTypeEnum = exchangeFormatTypeEnum;
	}

	public String getBusinessSudivName() {
		return businessSudivName;
	}

	public void setBusinessSudivName(String businessSudivName) {
		this.businessSudivName = businessSudivName;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	
	

}
