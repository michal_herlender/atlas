package org.trescal.cwms.core.exchangeformat.dto.form.validator;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatServiceImpl;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExchangeFormatFileValidator extends AbstractBeanValidator {

	@Autowired
	private ServletContext servletContext;
	@Autowired
	private ExchangeFormatService exchangeFormatServ;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private AsnService asnService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(PrebookingJobForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		PrebookingJobForm pbjf = (PrebookingJobForm) target;
		ExchangeFormat selectedEf;
		if (pbjf.getExchangeFormat() != null) {
			selectedEf = exchangeFormatServ.get(pbjf.getExchangeFormat());
		} else if (pbjf.getAsnId() != null) {
			selectedEf = asnService.get(pbjf.getAsnId()).getExchangeFormat();
		} else {
			errors.reject("exchangeformat.import.validation.exchangeformat.notfound", new String[] {},
					"Exchange format not found");
			return;
		}

		File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
		Asn asn = asnService.get(pbjf.getAddrid());
		File file = null;
		try {
			file = efFileService.getFile(asn.getExchangeFormatFile(), tempDir);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		/* open file */
		XSSFWorkbook wb;
		try {
			wb = new XSSFWorkbook(new FileInputStream(file));
			/* get sheet */
			XSSFSheet mainSheet;
			if (StringUtils.isNotEmpty(selectedEf.getSheetName())) {
				mainSheet = wb.getSheet(selectedEf.getSheetName());
				if (mainSheet == null) {
					errors.reject("exchangeformat.import.validation.sheet.notfound",
							new String[] { selectedEf.getSheetName() }, "the following sheet does not exist : ");
					return;
				}
			} else
				mainSheet = wb.getSheetAt(ExchangeFormatServiceImpl.MAIN_SHEET);

			/* read column names from header row */
			List<String> columnsHeaderList = new ArrayList<>();
			XSSFRow headerRow = mainSheet.getRow(selectedEf.getLinesToSkip());
			int headerCount = headerRow.getLastCellNum();
			for (int i = 0; i < headerCount; i++) {
				DataFormatter formatter = new DataFormatter();
				String val = formatter.formatCellValue(headerRow.getCell(i));
				columnsHeaderList.add(val);
			}

			/* check mandatory columns if they all exist in file */
			List<String> missingMandatoryFields = new ArrayList<>();
			boolean allMandatoryFieldsExistInFile = selectedEf.getExchangeFormatFieldNameDetails().stream()
					.filter(efd -> {
						return efd.isMandatory();
					}).allMatch(efd -> {
						boolean mandatoryFieldExistInFile = columnsHeaderList.stream().anyMatch(t -> {
							return t.equalsIgnoreCase(
									StringUtils.isNotEmpty(efd.getTemplateName()) ? efd.getTemplateName()
											: efd.getFieldName().getValue());
						});
						if (!mandatoryFieldExistInFile) {
							missingMandatoryFields
									.add(StringUtils.isNotEmpty(efd.getTemplateName()) ? efd.getTemplateName()
											: efd.getFieldName().getValue());
						}
						return mandatoryFieldExistInFile;
					});

			if (!allMandatoryFieldsExistInFile) {
				errors.reject("exchangeformat.import.validation.mandatorycolumns.missing",
						new String[] { missingMandatoryFields.toString() },
						"The following mandatory fields are not found : ");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
