package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasServiceType;

public interface AliasServiceTypeService extends BaseService<AliasServiceType, Integer> {

}
