package org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.audit.entity.db.CriteriaQueryGenerator;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.exchangeformat.dto.ExchangeFormatDTO;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat_;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;

@Repository
public class ExchangeFormatDaoImpl extends BaseDaoImpl<ExchangeFormat, Integer> implements ExchangeFormatDao {

	@Override
	protected Class<ExchangeFormat> getEntity() {
		return ExchangeFormat.class;
	}

	@Override
	public List<ExchangeFormat> getByTypeAndClientCompany(ExchangeFormatTypeEnum type, Company company,
			Subdiv businessSubdiv) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ExchangeFormat> cq = cb.createQuery(ExchangeFormat.class);
		Root<ExchangeFormat> root = cq.from(ExchangeFormat.class);

		Predicate typePredicate = cb.conjunction();
		if (type != null)
			typePredicate = cb.equal(root.get(ExchangeFormat_.type.getName()), type);

		Predicate clientCompanyPredicate = cb.conjunction();
		if (company != null)
			clientCompanyPredicate = cb.equal(root.get(ExchangeFormat_.clientCompany), company);

		Predicate businessCompanyPredicate = cb.and(
				cb.equal(root.get(ExchangeFormat_.businessCompany), businessSubdiv.getComp()),
				cb.isNull(root.get(ExchangeFormat_.clientCompany)));
		Predicate businessSubdivPredicate = cb.and(cb.equal(root.get(ExchangeFormat_.businessSubdiv), businessSubdiv),
				cb.isNull(root.get(ExchangeFormat_.clientCompany)));
		
		Predicate activeEf = cb.and(cb.equal(root.get(ExchangeFormat_.deleted), false));

		cq.distinct(true);
		cq.where(cb.and(typePredicate,activeEf,
				cb.or(clientCompanyPredicate, businessCompanyPredicate, businessSubdivPredicate)));

		TypedQuery<ExchangeFormat> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<ExchangeFormat> getByTypeAndClientCompany(ExchangeFormatTypeEnum type, Company company) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ExchangeFormat> cq = cb.createQuery(ExchangeFormat.class);
		Root<ExchangeFormat> root = cq.from(ExchangeFormat.class);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.and(cb.equal(root.get(ExchangeFormat_.clientCompany), company)));
		if (type != null)
			clauses.getExpressions().add(cb.equal(root.get(ExchangeFormat_.type.getName()), type));
		
		clauses.getExpressions().add(cb.equal(root.get(ExchangeFormat_.deleted), false));

		cq.where(clauses);

		TypedQuery<ExchangeFormat> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<ExchangeFormat> getByTypeAndBusinessCompany(ExchangeFormatTypeEnum type, Company businessCompany) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ExchangeFormat> cq = cb.createQuery(ExchangeFormat.class);
		Root<ExchangeFormat> root = cq.from(ExchangeFormat.class);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions()
				.add(cb.and(cb.equal(root.get(ExchangeFormat_.businessCompany.getName()), businessCompany)));
		if (type != null)
			clauses.getExpressions().add(cb.equal(root.get(ExchangeFormat_.type), type));

		cq.where(clauses);

		TypedQuery<ExchangeFormat> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<ExchangeFormat> getByTypeAndBusinessSubdivs(ExchangeFormatTypeEnum type, List<Subdiv> subdivList) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ExchangeFormat> cq = cb.createQuery(ExchangeFormat.class);
		Root<ExchangeFormat> root = cq.from(ExchangeFormat.class);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.and(root.get(ExchangeFormat_.businessSubdiv.getName()).in(subdivList)));
		if (type != null)
			clauses.getExpressions().add(cb.equal(root.get(ExchangeFormat_.type), type));

		cq.where(clauses);

		TypedQuery<ExchangeFormat> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<ExchangeFormat> getByTypeAndBusinessSubdiv(ExchangeFormatTypeEnum type, Subdiv subdiv) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ExchangeFormat> cq = cb.createQuery(ExchangeFormat.class);
		Root<ExchangeFormat> root = cq.from(ExchangeFormat.class);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.and(cb.equal(root.get(ExchangeFormat_.businessSubdiv.getName()), subdiv)));
		if (type != null)
			clauses.getExpressions().add(cb.equal(root.get(ExchangeFormat_.type), type));

		cq.where(clauses);

		TypedQuery<ExchangeFormat> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public ExchangeFormat getExchangeFormatByLevelAndType(ExchangeFormatTypeEnum type, ExchangeFormatLevelEnum level) {
		return getSingleResult(cb -> {
			CriteriaQuery<ExchangeFormat> cq = cb.createQuery(ExchangeFormat.class);
			Root<ExchangeFormat> root = cq.from(ExchangeFormat.class);
			Predicate clauses = cb.conjunction();
			if (type != null)
				clauses.getExpressions().add(cb.equal(root.get(ExchangeFormat_.type), type));
			if (level != null)
				clauses.getExpressions().add(cb.equal(root.get(ExchangeFormat_.exchangeFormatLevel), level));

			cq.where(clauses);
			return cq;
		});

	}

	/**
	 * @param type
	 *            : type of ef, if null return all types
	 * @param businessCompany
	 *            : with this we should return ef of the business company and
	 *            all it subdivs
	 * @param businessSubdiv
	 *            : return ef of subdiv
	 * @param clientCompany
	 *            : return ef of client company
	 */
	public List<ExchangeFormatDTO> getExchangeFormats(ExchangeFormatTypeEnum type, Company businessCompany,
			Subdiv businessSubdiv, Company clientCompany) {

		return getResultList(cb -> {

			CriteriaQuery<ExchangeFormatDTO> cq = cb.createQuery(ExchangeFormatDTO.class);
			Root<ExchangeFormat> ef = cq.from(ExchangeFormat.class);

			Join<ExchangeFormat, Company> businessCompanyJoin = ef.join(ExchangeFormat_.businessCompany, JoinType.LEFT);
			Join<ExchangeFormat, Company> clientCompanyJoin = ef.join(ExchangeFormat_.clientCompany, JoinType.LEFT);
			Join<ExchangeFormat, Subdiv> businessSubdivJoin = ef.join(ExchangeFormat_.businessSubdiv, JoinType.LEFT);
			
			Predicate andClauses = cb.conjunction();
			Predicate orClauses = cb.disjunction();

			if (type != null) {
				andClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.type), type));
			}
			
			if( clientCompany != null && businessCompany != null && businessSubdiv != null ) {
				
				Predicate andClauses1 = cb.conjunction();
				Predicate andClauses2 = cb.conjunction();
				Predicate andClauses3 = cb.conjunction();
					
				andClauses1.getExpressions().add(cb.isNotNull(ef.get(ExchangeFormat_.clientCompany)));
				andClauses1.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.clientCompany), clientCompany));
				orClauses.getExpressions().add(andClauses1);
				
				andClauses2.getExpressions().add(cb.isNull(ef.get(ExchangeFormat_.clientCompany)));
				andClauses2.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.businessCompany), businessCompany));
				orClauses.getExpressions().add(andClauses2);
				
				andClauses3.getExpressions().add(cb.isNull(ef.get(ExchangeFormat_.clientCompany)));
				andClauses3.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.businessSubdiv), businessSubdiv));
				orClauses.getExpressions().add(andClauses3);
				
			}
			
			else if (clientCompany != null) {
				orClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.clientCompany), clientCompany));
			} else if (businessCompany != null) {
				Predicate orPredicate = cb.disjunction();
				orPredicate.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.businessCompany).get(Company_.coid),
						businessCompany.getCoid()));

				Subquery<Subdiv> subdivsSubquery = cq.subquery(Subdiv.class);
				Root<Subdiv> subRoot = subdivsSubquery.from(Subdiv.class);
				subdivsSubquery.select(subRoot).where(cb.equal(subRoot.get(Subdiv_.comp), businessCompany));

				orPredicate.getExpressions().add(ef.get(ExchangeFormat_.businessSubdiv).in(subdivsSubquery));

				orClauses.getExpressions().add(orPredicate);
			} else if (businessSubdiv != null) {
				orClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.businessSubdiv), businessSubdiv));
			}
			
			
			orClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.exchangeFormatLevel), ExchangeFormatLevelEnum.GLOBAL));
			
			andClauses.getExpressions().add(orClauses);
			
			andClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.deleted), false));

			cq.where(andClauses);

			cq.select(cb.construct(ExchangeFormatDTO.class, ef.get(ExchangeFormat_.id), ef.get(ExchangeFormat_.name),
					ef.get(ExchangeFormat_.description), ef.get(ExchangeFormat_.type),
					ef.get(ExchangeFormat_.exchangeFormatLevel), businessCompanyJoin.get(Company_.coid),
					businessCompanyJoin.get(Company_.coname), clientCompanyJoin.get(Company_.coid),
					clientCompanyJoin.get(Company_.coname), businessSubdivJoin.get(Subdiv_.subdivid),
					businessSubdivJoin.get(Subdiv_.subname), ef.get(ExchangeFormat_.lastModifiedBy),
					ef.get(ExchangeFormat_.lastModified), ef.get(ExchangeFormat_.deleted)));
			return cq;

		});
	}

	@Override
	public Integer countExchangeFormats(ExchangeFormatTypeEnum type, Company businessCompany, Subdiv businessSubdiv,
			Company clientCompany) {
		return getCount(buildCountExchangeFormats(type, businessCompany, businessSubdiv, clientCompany));
	}

	protected CriteriaQueryGenerator<ExchangeFormat> buildCountExchangeFormats(ExchangeFormatTypeEnum type,
			Company businessCompany, Subdiv businessSubdiv, Company clientCompany) {
		return cb -> cq -> {

			Root<ExchangeFormat> ef = cq.from(ExchangeFormat.class);

			Predicate andClauses = cb.conjunction();
			Predicate orClauses = cb.disjunction();

			if (type != null) {
				andClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.type), type));
			}
			if (clientCompany != null) {
				orClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.clientCompany), clientCompany));
			} else if (businessCompany != null) {
				Predicate orPredicate = cb.disjunction();
				orPredicate.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.businessCompany).get(Company_.coid),
						businessCompany.getCoid()));

				Subquery<Subdiv> subdivsSubquery = cq.subquery(Subdiv.class);
				Root<Subdiv> subRoot = subdivsSubquery.from(Subdiv.class);
				subdivsSubquery.select(subRoot).where(cb.equal(subRoot.get(Subdiv_.comp), businessCompany));

				orPredicate.getExpressions().add(ef.get(ExchangeFormat_.businessSubdiv).in(subdivsSubquery));

				orClauses.getExpressions().add(orPredicate);
			} else if (businessSubdiv != null) {
				orClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.businessSubdiv), businessSubdiv));
			}
			
			
			orClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.exchangeFormatLevel), ExchangeFormatLevelEnum.GLOBAL));
			
			andClauses.getExpressions().add(orClauses);
			
			andClauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.deleted), false));

			cq.where(andClauses);
			return Triple.of(ef, null, null);
		};
	}

}
