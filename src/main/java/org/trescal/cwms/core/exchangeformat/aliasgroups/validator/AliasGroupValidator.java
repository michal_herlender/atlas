package org.trescal.cwms.core.exchangeformat.aliasgroups.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.AliasDetailDTO;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.AliasGroupDTO;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AliasGroupValidator extends AbstractBeanValidator {

	@Autowired
	AliasGroupService aliasGroupService;
	@Autowired
	CompanyService companyService;

	@Override
	public boolean supports(Class<?> clazz) {
		return AliasGroupDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		AliasGroupDTO dto = (AliasGroupDTO) (target);

		if (dto.getAliasGroupName() == null || dto.getAliasGroupName().equals("")) {
			errors.rejectValue("aliasGroupName", "aliasgroup.alaisgroupnamenotnull", null,
					"Alias Group name shouldn't be null");
		}

		if (dto.getAliasesForm() == null) {
			errors.rejectValue("aliasesForm", "aliasgroup.alaisesnotnull", null, "Must have at least one alias");
		}

		// validating if default group is already set

		if (dto.isDefaultForCurrentBusinessComp() == true) {
			AliasGroup aliasGroup = this.aliasGroupService
					.checkIfDefaultAliasGroupIsAlreadyDefined(this.companyService.get(dto.getAllocatedCompanyid()));
			if (aliasGroup != null && !aliasGroup.getId().equals(dto.getAlaisGroupId())) {
				errors.rejectValue("defaultForCurrentBusinessComp", "aliasgroup.canthavetwodefaultperbuiscomp",
						new Object[] { aliasGroup.getId(), aliasGroup.getName() },
						"Can't have more than one default alias group");
			}
		}

		// checking for duplicated values
		if (dto.getAliasesForm() != null) {

			for (AliasDetailDTO aliasesdto : dto.getAliasesForm()) {

				Long duplicatedElements = dto.getAliasesForm().stream()
						.filter(aldto -> aliasesdto.getValue().equals(aldto.getValue())
								&& aliasesdto.getAlias().equalsIgnoreCase(aldto.getAlias()))
						.count();

				if (duplicatedElements > 1)
					errors.reject("aliasgroup.noduplicatedvalues", null, "Can't have duplicate Values");
			}

		}
	}

}
