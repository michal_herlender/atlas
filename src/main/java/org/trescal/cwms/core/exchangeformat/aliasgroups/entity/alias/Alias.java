package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias;


import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;


@MappedSuperclass
public abstract class Alias<E> {

	private Integer id;
	private String alias;
	private AliasGroup aliasGroup;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "aliasgroupid")
	public AliasGroup getAliasGroup() {
		return aliasGroup;
	}

	public void setAliasGroup(AliasGroup aliasGroup) {
		this.aliasGroup = aliasGroup;
	}
	
	@Transient
	abstract E getValue();
	



}
