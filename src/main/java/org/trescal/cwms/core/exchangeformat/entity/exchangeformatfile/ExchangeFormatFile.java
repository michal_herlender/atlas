package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile;

import lombok.Setter;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.files.ForeignDBField;
import org.trescal.cwms.files.exchangeformat.ExchangeFormatFileData;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Setter
@Table(name = "exchangeformatfile")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public class ExchangeFormatFile extends Auditable {

	private Integer id;
	private String fileName;
	private Contact addedBy;
	private Date addedOn;
	private ExchangeFormatFileData exchangeFormatFileData;
	private String type;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable = false, foreignKey = @ForeignKey(name = "FK_exchangeformatfile_personid"))
	public Contact getAddedBy() {
		return this.addedBy;
	}

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "addedon", nullable = false)
	public Date getAddedOn() {
		return this.addedOn;
	}

	@Transient
	@ForeignDBField(fkField = "id")
	public ExchangeFormatFileData getExchangeFormatFileData() {
		return exchangeFormatFileData;
	}

	@Column(name = "fileName", nullable = false)
	public String getFileName() {
		return fileName;
	}

	@Column(name = "type", updatable = false, insertable = false)
	public String getType() {
		return type;
	}

}