package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile_;

@Repository
public class ExchangeFormatFileDaoImpl extends BaseDaoImpl<ExchangeFormatFile, Integer>
		implements ExchangeFormatFileDao {

	@Override
	protected Class<ExchangeFormatFile> getEntity() {
		return ExchangeFormatFile.class;
	}

	@Override
	public List<ExchangeFormatFile> getByFileName(String filename) {
		return getResultList(cb -> {
			CriteriaQuery<ExchangeFormatFile> cq = cb.createQuery(ExchangeFormatFile.class);
			Root<ExchangeFormatFile> root = cq.from(ExchangeFormatFile.class);
			cq.where(cb.equal(root.get(ExchangeFormatFile_.fileName), filename));
			return cq;
		});
	}
}
