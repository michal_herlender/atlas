package org.trescal.cwms.core.exchangeformat.aliasgroups.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasBoolean;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasIntervalUnit;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasServiceType;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;

@Entity
@Table(name = "aliasgroup")
public class AliasGroup extends Allocated<Company> {

	private Integer id;
	private String name;
	private String description;
	private Contact createdBy;
	private LocalDateTime createdOn;
	private List<AliasBoolean> booleanAliases;
	private List<AliasIntervalUnit> intervalUnitAliases;
	private List<AliasServiceType> serviceTypeAliases;
	private List<ExchangeFormat> exchangeFormats;
	private boolean defaultBusinessCompany;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdby")
	public Contact getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Contact createdBy) {
		this.createdBy = createdBy;
	}

	@DateTimeFormat(iso = ISO.DATE_TIME)
	@Column(name = "createdOn")
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	@OneToMany(mappedBy = "aliasGroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	public List<AliasBoolean> getBooleanAliases() {
		return booleanAliases;
	}

	public void setBooleanAliases(List<AliasBoolean> booleanAliases) {
		this.booleanAliases = booleanAliases;
	}

	@OneToMany(mappedBy = "aliasGroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	public List<AliasIntervalUnit> getIntervalUnitAliases() {
		return intervalUnitAliases;
	}

	public void setIntervalUnitAliases(List<AliasIntervalUnit> intervalUnitAliases) {
		this.intervalUnitAliases = intervalUnitAliases;
	}

	@OneToMany(mappedBy = "aliasGroup", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	public List<AliasServiceType> getServiceTypeAliases() {
		return serviceTypeAliases;
	}

	public void setServiceTypeAliases(List<AliasServiceType> serviceTypeAliases) {
		this.serviceTypeAliases = serviceTypeAliases;
	}

	@OneToMany(mappedBy = "aliasGroup", cascade = { CascadeType.PERSIST }, fetch = FetchType.LAZY)
	public List<ExchangeFormat> getExchangeFormats() {
		return exchangeFormats;
	}

	public void setExchangeFormats(List<ExchangeFormat> exchangeFormats) {
		this.exchangeFormats = exchangeFormats;
	}

	public boolean isDefaultBusinessCompany() {
		return defaultBusinessCompany;
	}

	public void setDefaultBusinessCompany(boolean defaultBuisnessCompany) {
		this.defaultBusinessCompany = defaultBuisnessCompany;
	}

}
