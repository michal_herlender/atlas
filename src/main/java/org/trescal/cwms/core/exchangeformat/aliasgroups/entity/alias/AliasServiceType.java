package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Entity
@Table(name = "aliasservicetype")
public class AliasServiceType extends Alias<ServiceType> {

	private ServiceType value;

	public void setValue(ServiceType value) {
		this.value = value;
	}
	
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicetypeid")
	public ServiceType getValue() {
		return value;
	}




}
