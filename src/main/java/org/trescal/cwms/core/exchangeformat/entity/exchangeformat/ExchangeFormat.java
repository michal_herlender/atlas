package org.trescal.cwms.core.exchangeformat.entity.exchangeformat;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatDateFormatEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "exchangeformat")
public class ExchangeFormat extends Versioned {

	private Integer id;
	private String name;
	private String description;
	private Company clientCompany;
	private Company businessCompany;
	private Subdiv businessSubdiv;
	private ExchangeFormatDateFormatEnum dateFormat;
	private ExchangeFormatLevelEnum exchangeFormatLevel;
	private List<ExchangeFormatFieldNameDetails> exchangeFormatFieldNameDetails;
	private int linesToSkip;
	private String sheetName;
	private ExchangeFormatTypeEnum type;
	private AliasGroup aliasGroup;
	private boolean deleted;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	@Column(name = "description", length = 200)
	public String getDescription() {
		return description;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "exchangeFormat", fetch = FetchType.LAZY, orphanRemoval = true)
	@OrderBy("position")
	public List<ExchangeFormatFieldNameDetails> getExchangeFormatFieldNameDetails() {
		return exchangeFormatFieldNameDetails;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clientCompanyid", foreignKey = @ForeignKey(name = "FK_ClientCompany_coid"))
	public Company getClientCompany() {
		return clientCompany;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "businessCompanyid", foreignKey = @ForeignKey(name = "FK_BusinessCompany_coid"))
	public Company getBusinessCompany() {
		return businessCompany;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "businessSubdivid", foreignKey = @ForeignKey(name = "FK_BusinessSubdiv_subdivid"))
	public Subdiv getBusinessSubdiv() {
		return businessSubdiv;
	}

	@Enumerated(EnumType.STRING)
	public ExchangeFormatDateFormatEnum getDateFormat() {
		return dateFormat;
	}

	@Enumerated(EnumType.STRING)
	public ExchangeFormatLevelEnum getExchangeFormatLevel() {
		return exchangeFormatLevel;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="eftype")
	public ExchangeFormatTypeEnum getType() {
		return type;
	}

	@Column(name = "linestoskip")
	public int getLinesToSkip() {
		return linesToSkip;
	}

	@Column(name = "sheetname")
	public String getSheetName() {
		return sheetName;
	}

	public void setBusinessSubdiv(Subdiv businessSubdiv) {
		this.businessSubdiv = businessSubdiv;
	}

	public void setDateFormat(ExchangeFormatDateFormatEnum dateFormat) {
		this.dateFormat = dateFormat;
	}

	public void setExchangeFormatLevel(ExchangeFormatLevelEnum exchangeFormatLevelEnum) {
		this.exchangeFormatLevel = exchangeFormatLevelEnum;
	}

	public void setBusinessCompany(Company businessCompany) {
		this.businessCompany = businessCompany;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setClientCompany(Company clientCompany) {
		this.clientCompany = clientCompany;
	}

	public void setExchangeFormatFieldNameDetails(List<ExchangeFormatFieldNameDetails> exchangeFormatFieldNameDetails) {
		this.exchangeFormatFieldNameDetails = exchangeFormatFieldNameDetails;
	}

	public void setLinesToSkip(int linesToSkip) {
		this.linesToSkip = linesToSkip;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public void setType(ExchangeFormatTypeEnum type) {
		this.type = type;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "aliasgroupid")
	public AliasGroup getAliasGroup() {
		return aliasGroup;
	}

	public void setAliasGroup(AliasGroup aliasGroup) {
		this.aliasGroup = aliasGroup;
	}
	
	@Column(name = "deleted", nullable = false, columnDefinition = "bit default 0")
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}
