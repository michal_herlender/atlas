package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasServiceType;

@Repository("AliasServiceTypeDao")
public class AliasServiceTypeDaoImp extends BaseDaoImpl<AliasServiceType, Integer> implements AliasServiceTypeDao {

	@Override
	protected Class<AliasServiceType> getEntity() {
		return AliasServiceType.class;
	}

}
