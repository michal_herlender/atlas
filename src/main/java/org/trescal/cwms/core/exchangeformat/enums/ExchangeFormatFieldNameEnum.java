package org.trescal.cwms.core.exchangeformat.enums;

import java.util.*;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ExchangeFormatFieldNameEnum {
	
	UNDEFINED("exchangeformat.fieldname.undefined", "Undefined", 
			Arrays.asList(ExchangeFormatTypeEnum.values())),
	ID_TRESCAL("exchangeformat.fieldname.idtrescal", "ID Trescal",
			Arrays.asList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING, 
			ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION), Integer.class),
	PLANT_NO("exchangeformat.fieldname.plantno", "Plant No",
			Arrays.asList(ExchangeFormatTypeEnum.values())),
	CUSTOMER_DESCRIPTION("exchangeformat.fieldname.customerdescription", "Customer description", 
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING, 
			ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP)),
	BRAND("exchangeformat.fieldname.brand", "Brand", 
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING, 
			ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP)),
	MODEL("exchangeformat.fieldname.model", "Model", 
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING, 
			ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP)),
	SERIAL_NUMBER("exchangeformat.fieldname.serialnumber", "Serial number", 
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING, 
			ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP)),
	EXPECTED_SERVICE("exchangeformat.fieldname.expectedservice", "Expected service", 
			Arrays.asList(ExchangeFormatTypeEnum.values()), ServiceType.class,true),
	TECHNICAL_REQUIREMENTS("exchangeformat.fieldname.technicalrequirements", "Technical requirements", 
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING, 
			ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP)),
	CUSTOMER_COMMENTS("exchangeformat.fieldname.customercomments", "Customer comments", 
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING, 
			ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP)),
	DELIVERY_DATE("exchangeformat.fieldname.deliverydate", "Delivery date",
			Collections.singletonList(ExchangeFormatTypeEnum.PREBOOKING)),
	URGENCY("exchangeformat.fieldname.urgency", "Urgency",
			Collections.singletonList(ExchangeFormatTypeEnum.PREBOOKING)),
	CLIENT_REF("exchangeformat.fieldname.clientref", "Client Ref", 
			Arrays.asList(ExchangeFormatTypeEnum.PREBOOKING,ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION)),
	UNDER_GUARANTEE("exchangeformat.fieldname.underguarantee", "under guarantee", Arrays.asList(
			ExchangeFormatTypeEnum.PREBOOKING, ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION)),
	PO_NUMBER("exchangeformat.fieldname.ponumber", "PO Number",
			Collections.singletonList(ExchangeFormatTypeEnum.PREBOOKING)),
	LOCATION("exchangeformat.fieldname.location", "Location", Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, 
			ExchangeFormatTypeEnum.PREBOOKING, ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	TML_BRAND_ID("exchangeformat.fieldname.tmlbrandid", "TML Brand ID",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION), Integer.class),
	TML_INSTRUMENT_MODEL_ID("exchangeformat.fieldname.tmlinstrumentmodelid",
			"TML Instrument Model ID", Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, 
					ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION), Integer.class),
	INSTRUMENT_MODEL_ID("exchangeformat.fieldname.instrumentmodelid",
			"Instrument Model ID", Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, 
					ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION), Integer.class),
	TML_FAMILY_ID("exchangeformat.fieldname.tmlfamilyid",
			"TML Family ID", Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION), Integer.class),
	FAMILY_ID("exchangeformat.fieldname.familyid", "Family ID",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION), Integer.class),
	TML_SUBFAMILY_ID("exchangeformat.fieldname.tmlsubfamilyid",
			"TML SubFamily ID", Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION), Integer.class),
	SUBFAMILY_ID("exchangeformat.fieldname.subfamilyid", "SubFamily ID",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION), Integer.class),
	NEXT_CAL_DUEDATE("exchangeformat.fieldname.nextcalduedate", "Next Calibration Due Date",
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Date.class),
	CAL_FREQUENCY("exchangeformat.fieldname.calfrequency", "Calibration Frequency",
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING), Integer.class),
	INTERVAL_UNIT("exchangeformat.fieldname.intervalunit", "Interval Unit",
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING),
			IntervalUnit.class,true),
	DEFAULT_PROCEDURE_REFERENCE("exchangeformat.fieldname.defaultprocedure", "Default Procedure Reference",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION)),
	DEFAULT_WORKINSTRUCTION("exchangeformat.fieldname.defaultworkinstruction", "Default WorkInstruction",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION)),
	CONTACT("exchangeformat.fieldname.contact", "Contact",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION)),
	ADDRESS("exchangeformat.fieldname.address", "Address",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION)),
	CONTACT_ID("exchangeformat.fieldname.contactid", "Contact Id",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION), Integer.class),
	ADDRESS_ID("exchangeformat.fieldname.addressid", "Address Id",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION), Integer.class),
	OPERATION_BY_HRID("exchangeformat.fieldname.operationbyhrid", "Operation by (HR-ID)",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), String.class),
	OPERATION_TYPE("exchangeformat.fieldname.operationtype", "Operation type",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), ExchangeFormatOperationTypeEnum.class),
	OPERATION_DATE("exchangeformat.fieldname.operationdate", "Operation Date",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Date.class),
	OPERATION_START_DATE("exchangeformat.fieldname.operationstartdate", "Operation Start Date",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Date.class),
	OPERATION_DURATION("exchangeformat.fieldname.operationduration", "Operation duration",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Integer.class),
	DOCUMENT_NO("exchangeformat.fieldname.documentno", "Document No",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), String.class),
	OPERATION_VALIDATED_ON("exchangeformat.fieldname.operationvalidatedon", "Operation Validated on",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Date.class),
	OPERATION_VALIDATED_BY_HRID("exchangeformat.fieldname.operationvalidatedbyhrid", "Operation Validated by",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	SERVICETYPE_ID("exchangeformat.fieldname.servicetypeid", "ServiceType Id",
			Arrays.asList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION, ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION, ExchangeFormatTypeEnum.PREBOOKING), Integer.class),
	JOBITEM_EQUIVALENT_ID("exchangeformat.fieldname.jobitemequivalentid","JobItem equivalent id",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	CALIBRATION_OUTCOME("exchangeformat.fieldname.calibrationoutcome","Calibration outcome",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), ActionOutcomeValue_Calibration.class),
	ADJUSTMENT("exchangeformat.fieldname.adjustment", "Adjustment",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	OPTIMIZATION("exchangeformat.fieldname.optimization", "Optimization",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	RESTRICTION("exchangeformat.fieldname.restriction", "Restriction",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	REPAIR("exchangeformat.fieldname.repair", "Repair",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	CALIBRATION_CLASS("exchangeformat.fieldname.calibrationclass", "Calibration class",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), CalibrationClass.class),
	CALIBRATION_VERIFICATION_STATUS("exchangeformat.fieldname.calibrationverificationstatus","Calibration Verification Status",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), CalibrationVerificationStatus.class),
	FR_STATES("exchangeformat.fieldname.frstates","Failure report states",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	FR_STATE_OTHER("exchangeformat.fieldname.frstateother","Failure report - state other",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	FR_TECHNICIAN_COMMENTS("exchangeformat.fieldname.frtechniciancomments","Failure report - technician comments",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	FR_RECOMMENDATIONS("exchangeformat.fieldname.frrecommendations","Failure report - recommendations",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	FR_DISPENSATION_COMMENTS("exchangeformat.fieldname.frdispensationcomments","Failure report - dispensation comments",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	FR_VALIDATION_COMMENTS("exchangeformat.fieldname.frvalidationcomments","Failure report - validation comments",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	FR_SEND_TO_CLIENT("exchangeformat.fieldname.frsenttoclient","Send Failure report to client",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	FR_CLIENT_RESPONSE_NEEDED("exchangeformat.fieldname.frclientresponseneeded","Client response needed on Failure report",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	FR_FINAL_OUTCOME("exchangeformat.fieldname.frfinaloutcome","Failure report - final outcome",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), FailureReportRecommendationsEnum.class),
	FR_CLIENT_APPROVAL_ON("exchangeformat.fieldname.frclientapprovalon","Failure report - client approval on",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Date.class),
	FR_CLIENT_DECISION("exchangeformat.fieldname.frclientdecision","Failure report - client decision",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	FR_CLIENT_COMMENTS("exchangeformat.fieldname.frclientcomments","Failure report client comments",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	FR_OPERATION_BY_TRESCAL("exchangeformat.fieldname.froperationbytrescal","Failure Report - operation Carried out by Trescal",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	FR_SUBDIV_ID("exchangeformat.fieldname.subdivid","Failure Report - Subdiv Id",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	FR_ESTIMATED_DELIVERY_TIME("exchangeformat.fieldname.estimated.delivery.time","Failure Report - Estimated delivery time",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION), Boolean.class),
	FR_CLIENT_ALIAS("exchangeformat.fieldname.clientalias","Failure Report - Client Alias",
			Collections.singletonList(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)),
	CATALOG_PRICE("exchangeformat.fieldname.catalogprice","Catalog price",
			Collections.singletonList(ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION)),
	DISCOUNT_RATE("exchangeformat.fieldname.discountrate","Discount rate",
			Collections.singletonList(ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION)),
	FINAL_PRICE("exchangeformat.fieldname.finalprice","Final price",
			Collections.singletonList(ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION)),
	PUBLIC_NOTE("exchangeformat.fieldname.publicnote","Public note",
			Collections.singletonList(ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION)),
    PRIVATE_NOTE("exchangeformat.fieldname.privatenote","Private note",
			Collections.singletonList(ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION)),
	INSTRUMENT_USAGE_TYPE("exchangeformat.fieldname.instrumentusagetype","Usage type",
			Collections.singletonList(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION));

	private ReloadableResourceBundleMessageSource messages;
	private final String fullName;
	private final String messageCode;
	private final Class<?> dataType;
	private final List<ExchangeFormatTypeEnum> types;
	private boolean needAlias;
	
	ExchangeFormatFieldNameEnum(String messageCode, String fullName, List<ExchangeFormatTypeEnum> types) {
		this.fullName = fullName;
		this.messageCode = messageCode;
		this.types = types;
		this.dataType = String.class;
	}
	
	ExchangeFormatFieldNameEnum(String messageCode, String fullName, List<ExchangeFormatTypeEnum> types, Class<?> dataType) {
		this.fullName = fullName;
		this.messageCode = messageCode;
		this.types = types;
		this.dataType = dataType;
	}
	
	ExchangeFormatFieldNameEnum(String messageCode, String fullName, List<ExchangeFormatTypeEnum> types, Class<?> dataType,
								boolean needAlias) {
		this.fullName = fullName;
		this.messageCode = messageCode;
		this.dataType = dataType;
		this.types = types;
		this.needAlias = needAlias;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public Class<?> getDataType() {
		return dataType;
	}
	
	public static ExchangeFormatFieldNameEnum parseFullName(String fullName) {
		ExchangeFormatFieldNameEnum result = UNDEFINED;
		for (ExchangeFormatFieldNameEnum status : ExchangeFormatFieldNameEnum.values()) {
			if (status.getFullName().equals(fullName)) {
				result = status;
				break;
			}
		}
		return result;
	}
	
	public String getValue() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.fullName, locale);
		}
		return this.fullName;
	}

	public String getName() {
		return this.name();
	}
	
	public List<ExchangeFormatTypeEnum> getTypes() {
		return types;
	}
	
	@Component
	public static class ExchangeFormatFieldNameMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (ExchangeFormatFieldNameEnum effn : EnumSet.allOf(ExchangeFormatFieldNameEnum.class))
            	effn.setMessageSource(messages);
        }
	}

	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
	
	public boolean isNeedAlias() {
		return needAlias;
	}

	
	


	
	
	
	

}