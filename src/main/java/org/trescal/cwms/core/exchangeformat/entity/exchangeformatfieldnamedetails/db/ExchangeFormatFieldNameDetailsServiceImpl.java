package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;

@Service
public class ExchangeFormatFieldNameDetailsServiceImpl extends BaseServiceImpl<ExchangeFormatFieldNameDetails, Integer>
		implements ExchangeFormatFieldNameDetailsService {

	@Autowired
	private ExchangeFormatFieldNameDetailsDao dao;

	@Override
	protected BaseDao<ExchangeFormatFieldNameDetails, Integer> getBaseDao() {
		return dao;
	}
	
	@Override
	public List<ExchangeFormatFieldNameDetails> getByExchangeFormat(Integer efId){
		return dao.getByExchangeFormat(efId);
	}
	

}
