package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.exceptions.MultipleExcelFilesFoundInZip;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.exceptions.NoExcelFileFoundInZip;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatzipfile.OperationsImportZipFile;
import org.trescal.cwms.files.exchangeformat.ExchangeFormatFileData;

@Service
public class ExchangeFormatFileServiceImpl extends BaseServiceImpl<ExchangeFormatFile, Integer>
		implements ExchangeFormatFileService {

	@Autowired
	private ExchangeFormatFileDao dao;
	@PersistenceContext(unitName = "entityManagerFactory2")
	private EntityManager entityManager2;
	@Autowired
	@Qualifier("transactionManager2")
	private PlatformTransactionManager transactionManager2;
	@Autowired
	private ServletContext servletContext;

	@Override
	protected BaseDao<ExchangeFormatFile, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public ExchangeFormatFile createEfFile(Contact addedBy, MultipartFile multipartFile, ExchangeFormatFile efFile)
			throws IOException {

		efFile.setAddedBy(addedBy);
		efFile.setAddedOn(new Date());
		efFile.setExchangeFormatFileData(new ExchangeFormatFileData());
		efFile.getExchangeFormatFileData().setFileData(multipartFile.getBytes());
		efFile.setFileName(multipartFile.getOriginalFilename());

		// to get the id
		efFile = this.insertExchangeFormatFile(efFile);

		// get uploaded file and save it in temporary server directory with the filename
		// being the id of the efFile + the original extension
		String filename = efFile.getId().toString() + '.'
				+ FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
		File file = new File(tempDir, filename);

		// transfer to file in tmp folder
		multipartFile.transferTo(file);

		// if zip file, extract it
		if (efFile instanceof OperationsImportZipFile)
			extractZipFile(file);

		return efFile;
	}

	@Override
	public File getFile(ExchangeFormatFile efFile, File tempDir) throws IOException {

		File file = new File(tempDir,
				efFile.getId().toString() + '.' + FilenameUtils.getExtension(efFile.getFileName()));
		// check if it already exists in server tmp folder, else get it from files db
		if (!file.exists()) {
			FileUtils.writeByteArrayToFile(file, efFile.getExchangeFormatFileData().getFileData());
			// extract if zip file
			if (efFile.getType().equals(OperationsImportZipFile.class.getSimpleName()))
				extractZipFile(file);
		}
		return file;
	}

	/**
	 * If the original file is a ZIP file, this method will extract it and then get
	 * the excel file inside.
	 * Throws : 
	 * NoExcelFileFoundInZip if no excel file inside.
	 * MultipleExcelFilesFoundInZip if multiple files are inside.
	 */
	@Override
	public File getExcelFile(ExchangeFormatFile efFile, File tempDir) throws IOException {
		File file = this.getFile(efFile, tempDir);
		if (efFile.getType().equals(OperationsImportZipFile.class.getSimpleName())) {
			// get extracted folder
			String extractedFolderPath = StringUtils.removeEnd(file.getPath(),
					'.' + FilenameUtils.getExtension(file.getPath()));
			File extractedFolder = new File(extractedFolderPath);

			// look for the excel file (we should have just one)
			List<File> files = Arrays.asList(extractedFolder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String filename) {
					return filename.toLowerCase().endsWith(".xls") || filename.toLowerCase().endsWith(".xlsx");
				}
			}));
			if (files.isEmpty())
				throw new NoExcelFileFoundInZip();
			else if (files.size() > 1)
				throw new MultipleExcelFilesFoundInZip();
			else
				file = files.get(0);

		}
		return file;
	}

	private void extractZipFile(File file) throws IOException, FileNotFoundException, ZipException {
		// destinationFolder = name of the source file without zip extension
		String destinationFolderPath = StringUtils.removeEnd(file.getPath(),
				'.' + FilenameUtils.getExtension(file.getPath()));
		File destinationFolder = new File(destinationFolderPath);
		destinationFolder.mkdir();
		try (ZipFile zipFile = new ZipFile(file)) {
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				File entryDestination = new File(destinationFolder.getAbsolutePath(), entry.getName());
				if (entry.isDirectory()) {
					entryDestination.mkdirs();
				} else {
					entryDestination.getParentFile().mkdirs();
					try (InputStream in = zipFile.getInputStream(entry);
							OutputStream out = new FileOutputStream(entryDestination, true)) {
						IOUtils.copy(in, out);
					}
				}
			}
		}
	}

	@Transactional
	public ExchangeFormatFile insertExchangeFormatFile(ExchangeFormatFile exchangeFormatFile) {
		this.dao.persist(exchangeFormatFile);
		TransactionStatus ts = transactionManager2.getTransaction(null);
		ExchangeFormatFileData pfd = exchangeFormatFile.getExchangeFormatFileData();
		if (pfd != null) {
			pfd.setEntityId(exchangeFormatFile.getId());
			entityManager2.persist(pfd);
		}
		transactionManager2.commit(ts);
		return exchangeFormatFile;
	}

	@Transactional
	public void saveOrUpdateExchangeFormatFile(ExchangeFormatFile exchangeFormatFile) {
		exchangeFormatFile = this.dao.merge(exchangeFormatFile);
		TransactionStatus ts = transactionManager2.getTransaction(null);
		ExchangeFormatFileData pfd = exchangeFormatFile.getExchangeFormatFileData();
		if (pfd != null && exchangeFormatFile.getId() != null) {
			Integer id = exchangeFormatFile.getId();
			pfd.setEntityId(id);
			if (entityManager2.find(ExchangeFormatFileData.class, id) == null)
				entityManager2.persist(pfd);
			else
				entityManager2.merge(pfd);
		}
		transactionManager2.commit(ts);
	}

	@Override
	public boolean fileNameExists(String fileName) {
		return dao.getByFileName(fileName).size() > 0;
	}
}
