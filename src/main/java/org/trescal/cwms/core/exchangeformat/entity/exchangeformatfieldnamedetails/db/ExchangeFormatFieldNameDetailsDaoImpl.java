package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat_;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails_;

@Repository
public class ExchangeFormatFieldNameDetailsDaoImpl extends BaseDaoImpl<ExchangeFormatFieldNameDetails, Integer>
		implements ExchangeFormatFieldNameDetailsDao {

	@Override
	protected Class<ExchangeFormatFieldNameDetails> getEntity() {
		return ExchangeFormatFieldNameDetails.class;
	}

	@Override
	public List<ExchangeFormatFieldNameDetails> getByExchangeFormat(Integer efId) {
		return getResultList(cb -> {
			CriteriaQuery<ExchangeFormatFieldNameDetails> cq = cb.createQuery(ExchangeFormatFieldNameDetails.class);
			Root<ExchangeFormatFieldNameDetails> root = cq.from(ExchangeFormatFieldNameDetails.class);
			Join<ExchangeFormatFieldNameDetails, ExchangeFormat> ef = root
					.join(ExchangeFormatFieldNameDetails_.exchangeFormat);

			cq.where(cb.equal(ef.get(ExchangeFormat_.id), efId));

			return cq;
		});
	}

}
