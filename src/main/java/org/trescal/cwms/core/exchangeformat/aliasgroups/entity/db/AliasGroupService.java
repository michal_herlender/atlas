package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.AliasGroupDTO;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

public interface AliasGroupService extends BaseService<AliasGroup, Integer> {
	
	public IntervalUnit determineIntervalUnitValue(AliasGroup aliasGroup, String defaultValue);
	
	public AliasGroup checkIfDefaultAliasGroupIsAlreadyDefined(Company businessCompany);
	
	public void saveOrEdit(AliasGroupDTO dto);
	
	public List<AliasGroup> getAliasGroupsForAllocatedCompany(Company AllocatedCompany);
	
	ServiceType determineExpectedServiceTypeFromAliasOrValue(AliasGroup aliasGroup, String defaultValue);
	
	ServiceType determineServiceTypeFromExpectedServiceType(String ExpectedServiceType);
	
}
