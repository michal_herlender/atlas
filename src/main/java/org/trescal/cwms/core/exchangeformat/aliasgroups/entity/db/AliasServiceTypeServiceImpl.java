package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasServiceType;

@Service
public class AliasServiceTypeServiceImpl extends BaseServiceImpl<AliasServiceType, Integer> implements AliasServiceTypeService{
	
	
	@Autowired
	AliasServiceTypeDao dao;
	
	@Override
	protected BaseDao<AliasServiceType, Integer> getBaseDao() {
		return this.dao;
	}

}
