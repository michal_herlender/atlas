package org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db;


import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exchangeformat.dto.ExchangeFormatDTO;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;

public interface ExchangeFormatService extends BaseService<ExchangeFormat, Integer> {

	List<ExchangeFormat> getByTypeAndClientCompany(ExchangeFormatTypeEnum type, Company company, Subdiv businessSubdiv);
	
	List<ExchangeFormat> getByTypeAndClientCompany(ExchangeFormatTypeEnum type, Company company);

	List<ExchangeFormat> getByTypeAndBusinessCompany(ExchangeFormatTypeEnum type, Company businessCompany);

	List<ExchangeFormat> getByTypeAndBusinessSubdivs(ExchangeFormatTypeEnum type, List<Subdiv> subdivList);

	List<ExchangeFormat> getByTypeAndBusinessSubdiv(ExchangeFormatTypeEnum type, Subdiv subdiv);

	void createOrUpdate(ExchangeFormat ef);

	byte[] getExchangeFormatExcelTemplateFile(ExchangeFormat ef, Locale locale);

	Map<String, ExchangeFormatFieldNameEnum> getColumnNamesMapping(ExchangeFormat ef);
	
	ExchangeFormat getExchangeFormatByLevelAndType(ExchangeFormatTypeEnum type, ExchangeFormatLevelEnum level);
	
	public List<ExchangeFormatDTO> getExchangeFormats(ExchangeFormatTypeEnum type, Company businessCompany,
			Subdiv businessSubdiv, Company clientCompany);
	
	public Integer countExchangeFormats(ExchangeFormatTypeEnum type, Company businessCompany,
			Subdiv businessSubdiv, Company clientCompany);

	List<ExchangeFormatFieldNameDetails> getEfFields(Integer id);
}
