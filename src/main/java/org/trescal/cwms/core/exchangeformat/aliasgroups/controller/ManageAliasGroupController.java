package org.trescal.cwms.core.exchangeformat.aliasgroups.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.AliasGroupDTO;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.transformer.AliasGroupTransformer;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.aliasgroups.validator.AliasGroupValidator;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY })
public class ManageAliasGroupController {

	@Autowired
	private UserService userService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private AliasGroupService aliasGroupService;
	@Autowired
	private AliasGroupTransformer transformer;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private AsnService asnService;
	@Autowired
	private AliasGroupValidator validator;

	public static final String FORM_NAME = "aliasgroupform";
	public static final String ACTION_ADD = "add";
	public static final String ACTION_EDIT = "edit";
	public static final String VIEW_NAME = "trescal/core/logistics/aliasgroupoverlay";
	public static final String MANAGE_ALIAS_VIEW_NAME = "/trescal/core/admin/managealiasgroups";

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);

	}

	@ModelAttribute("exchangeformatfieldname")
	public List<ExchangeFormatFieldNameEnum> exchangeFormatFieldName() {
		List<ExchangeFormatFieldNameEnum> exchangeFormatFields = new ArrayList<ExchangeFormatFieldNameEnum>();
		for (ExchangeFormatFieldNameEnum exchangeFormatFlied : ExchangeFormatFieldNameEnum.values()) {
			exchangeFormatFields.add(exchangeFormatFlied);
		}
		return exchangeFormatFields.stream().filter(eff -> eff.isNeedAlias() == true).collect(Collectors.toList());
	}

	@ModelAttribute("intervalunits")
	public IntervalUnit[] intervalUnits() {
		return IntervalUnit.values();
	}

	@ModelAttribute("servicetypes")
	public List<ServiceType> serviceTypes() {
		return this.serviceTypeService.getAllCalServiceTypes();
	}

	@RequestMapping(method = RequestMethod.GET, value = "managealiasgroups.htm")
	public String manageAliasGroups(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> company, Locale locale) {

		List<AliasGroupDTO> dtos = new ArrayList<AliasGroupDTO>();
		for (AliasGroup aliasGroup : this.aliasGroupService
				.getAliasGroupsForAllocatedCompany(this.companyService.get(company.getKey()))) {
			dtos.add(this.transformer.transform(aliasGroup));
		}
		model.addAttribute("aliasGroups", dtos);
		model.addAttribute("locale", locale);
		return MANAGE_ALIAS_VIEW_NAME;
	}

	@RequestMapping(method = RequestMethod.GET, value = "editaliasgroupoverlay.htm")
	public String showOverlay(Model model, @RequestParam(name = "aliasgroupid", required = false) Integer aliasgroupid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> company, Locale locale,
			@ModelAttribute(FORM_NAME) AliasGroupDTO dto) {

		if (aliasgroupid != null) {
			dto = this.transformer.transform(this.aliasGroupService.get(aliasgroupid));
			dto.setAction(ACTION_EDIT);
		} else if (dto == null || dto.getAlaisGroupId() == null) {
			if (dto == null)
				dto = new AliasGroupDTO();
			dto.setAction(ACTION_ADD);
		}

		dto.setAllocatedCompanyid(company.getKey());
		model.addAttribute(FORM_NAME, dto);
		model.addAttribute("locale", locale);
		return VIEW_NAME;
	}

	@RequestMapping(method = RequestMethod.POST, value = "editaliasgroupoverlay.htm")
	public String addEditAliasGoup(@RequestParam(name = "aliasgroupid", required = false) Integer aliasgroupid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> company, Locale locale,
			@Valid @ModelAttribute(FORM_NAME) AliasGroupDTO dto, BindingResult bindingResult, Model model)
			throws Exception {

		if (!bindingResult.hasErrors()) {
			if (dto.getAction().equals(ACTION_ADD)) {
				dto.setCreatedById(this.userService.get(username).getCon().getId());
				dto.setCreatedOn(LocalDateTime.now());
			}

			this.aliasGroupService.saveOrEdit(dto);
		}

		return VIEW_NAME;
	}

	@RequestMapping(value = "/deleteAliasGroup.json", method = RequestMethod.GET)
	public @ResponseBody ResultWrapper delete(@RequestParam(value = "id", required = true) int id, Locale locale) {

		AliasGroup aliasGroup = this.aliasGroupService.get(id);
		if (aliasGroup == null) {
			return new ResultWrapper(false, messages.getMessage("error.rest.data.notfound", null, locale));
		} else if (aliasGroup.isDefaultBusinessCompany() == true) {
			return new ResultWrapper(false, messages.getMessage("error.rest.data.defaultaliasgroup", null, locale));
		} else if (aliasGroup.isDefaultBusinessCompany() == false) {
			// look if the alias Group is used by any exchange format
			List<Asn> list = this.asnService.getAsnByAliasGroup(aliasGroup);
			if (!list.isEmpty()) {
				// verify the ef if it's used in a prebooking
				return new ResultWrapper(false,
						messages.getMessage("error.rest.data.defaultaliasgrouplinkedtoefinuse", null, locale));
			} else {
				// detache the alias group for the efs
				for (ExchangeFormat ef : aliasGroup.getExchangeFormats()) {
					ef.setAliasGroup(null);
				}
				// delete if the aliasGroup isn't used in any ef
				this.aliasGroupService.delete(aliasGroup);
			}
		}

		return new ResultWrapper(true, "ok");
	}

}
