package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.AliasGroupDTO;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.transformer.AliasGroupTransformer;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasIntervalUnit;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.enums.IntervalUnit;

@Service
public class AliasGroupServiceImpl extends BaseServiceImpl<AliasGroup, Integer> implements AliasGroupService {

	@Autowired
	AliasGroupDao aliasGroupDao;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	AliasGroupTransformer transformer;
	@Autowired
	ServiceTypeService serviceTypeService;
	@Autowired
	AliasServiceTypeService aliasServiceTypeservice;

	@Override
	protected BaseDao<AliasGroup, Integer> getBaseDao() {
		return this.aliasGroupDao;
	}

	@Override
	public IntervalUnit determineIntervalUnitValue(AliasGroup aliasGroup, String defaultValue) {
		IntervalUnit iu = null;
		if (aliasGroup != null && aliasGroup.getIntervalUnitAliases() != null) {
			AliasIntervalUnit aliasIU = aliasGroup.getIntervalUnitAliases().stream()
					.filter(aiu -> aiu.getAlias().equalsIgnoreCase(defaultValue)).findAny().orElse(null);
			if (aliasIU != null)
				iu = aliasIU.getValue();
		} else {

			iu = Arrays.stream(IntervalUnit.values()).filter(i -> {
				String[] locales = { "en", "es", "fr", "de" };
				List<String> possibleValues = new ArrayList<>();
				Arrays.asList(locales).forEach(loc -> {
					possibleValues.add(
							messageSource.getMessage(i.getSingularNameCode(), null, new Locale(loc)).toLowerCase());
					possibleValues
							.add(messageSource.getMessage(i.getPluralNameCode(), null, new Locale(loc)).toLowerCase());
					possibleValues.add(
							messageSource.getMessage(i.getSingleCharacterCode(), null, new Locale(loc)).toLowerCase());
				});
				if (possibleValues.contains(defaultValue.toLowerCase()))
					return true;
				else
					return false;
			}).findAny().orElse(null);
		}
		return iu;
	}

	@Override
	public AliasGroup checkIfDefaultAliasGroupIsAlreadyDefined(Company businessCompany) {
		return this.aliasGroupDao.checkIfDefaultAliasGroupIsAlreadyDefined(businessCompany);
	}

	@Override
	public void saveOrEdit(AliasGroupDTO dto) {
		AliasGroup aliasGroup = this.transformer.transform(dto);

		if (aliasGroup.getId() != null) {
			List<AliasServiceType> newAliasServiceTyps = aliasGroup.getServiceTypeAliases().stream()
					.filter(ast -> ast.getId() == null).collect(Collectors.toList());
			if (!newAliasServiceTyps.isEmpty()) {
				aliasGroup.getServiceTypeAliases().removeAll(newAliasServiceTyps);
				this.merge(aliasGroup);
				for (AliasServiceType ast : newAliasServiceTyps) {
					ast.setAliasGroup(aliasGroup);
					this.aliasServiceTypeservice.merge(ast);
				}
			}
		} else {
			this.merge(aliasGroup);
		}
	}

	@Override
	public List<AliasGroup> getAliasGroupsForAllocatedCompany(Company AllocatedCompany) {
		return this.aliasGroupDao.getAliasGroupsForAllocatedCompany(AllocatedCompany);
	}

	@Override
	public ServiceType determineExpectedServiceTypeFromAliasOrValue(AliasGroup aliasGroup, String defaultValue) {
		if (StringUtils.isNoneBlank(defaultValue)) {
			if (aliasGroup != null && aliasGroup.getServiceTypeAliases() != null) {
				AliasServiceType aliasServiceType = aliasGroup.getServiceTypeAliases().stream()
						.filter(ast -> ast.getAlias().equalsIgnoreCase(defaultValue)).findAny().orElse(null);
				if (aliasServiceType != null)
					return aliasServiceType.getValue();
			}
		}

		return determineServiceTypeFromExpectedServiceType(defaultValue);
	}

	@Override
	public ServiceType determineServiceTypeFromExpectedServiceType(String ExpectedServiceType) {
		for (ServiceType st : this.serviceTypeService.getAll()) {
			boolean shortNamematches = st.getShortnameTranslation().stream().anyMatch(s -> {
				return s.getTranslation().toLowerCase().trim().equals(ExpectedServiceType.toLowerCase());
			});
			if (shortNamematches)
				return st;
			else {
				boolean longNamematches = st.getLongnameTranslation().stream().anyMatch(s -> {
					return s.getTranslation().toLowerCase().trim().equals(ExpectedServiceType.toLowerCase());
				});
				if (longNamematches)
					return st;
			}
		}
		return null;
	}

}
