package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;

public interface ExchangeFormatFileService extends BaseService<ExchangeFormatFile, Integer> {

	ExchangeFormatFile insertExchangeFormatFile(ExchangeFormatFile exchangeFormatFile);

	void saveOrUpdateExchangeFormatFile(ExchangeFormatFile exchangeFormatFile);

	boolean fileNameExists(String fileName);

	ExchangeFormatFile createEfFile(Contact addedBy, MultipartFile file, ExchangeFormatFile efFile) throws IOException;

	File getFile(ExchangeFormatFile efFile, File tempDir) throws IOException;

	File getExcelFile(ExchangeFormatFile efFile, File tempDir) throws IOException;
}
