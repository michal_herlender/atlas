package org.trescal.cwms.core.exchangeformat.enums;

public enum ExchangeFormatLevelEnum {
	
	BUSINESSCOMPANY, BUSINESSSUBDIV, GLOBAL;

}
