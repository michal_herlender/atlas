package org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTAutoFilter;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumn;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exchangeformat.dto.ExchangeFormatDTO;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

@Service
public class ExchangeFormatServiceImpl extends BaseServiceImpl<ExchangeFormat, Integer>
		implements ExchangeFormatService {

	private static final String IMPORT_TEMPLATE_ES = "exchangeFormatLayout_Es.xlsx";
	private static final String IMPORT_TEMPLATE_FR = "exchangeFormatLayout_Fr.xlsx";
	private static final String IMPORT_TEMPLATE_EN = "exchangeFormatLayout_En.xlsx";
	public static final int MAIN_SHEET = 0;
	public static final String MAIN_TABLE_NAME = "mainTable";
	public static final int MAIN_TABLE_DEFAULT_ROW_COUNT = 100;
	public static final String MAIN_TABLE_STYLE_NAME = "TableStyleMedium13";
	public static final String SHEET_MODIFICATION_PASSWORD = "DO_NOT_MODIFY!";

	@Autowired
	private ExchangeFormatDao dao;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private ServiceTypeService serviceTypeService;

	@Override
	protected BaseDao<ExchangeFormat, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public List<ExchangeFormat> getByTypeAndClientCompany(ExchangeFormatTypeEnum type, Company company,
			Subdiv businessSubdiv) {
		return dao.getByTypeAndClientCompany(type, company, businessSubdiv);
	}

	@Override
	public List<ExchangeFormat> getByTypeAndBusinessCompany(ExchangeFormatTypeEnum type, Company businessCompany) {
		return dao.getByTypeAndBusinessCompany(type, businessCompany);
	}

	@Override
	public List<ExchangeFormat> getByTypeAndBusinessSubdivs(ExchangeFormatTypeEnum type, List<Subdiv> subdivList) {
		return dao.getByTypeAndBusinessSubdivs(type, subdivList);
	}

	@Override
	public List<ExchangeFormat> getByTypeAndBusinessSubdiv(ExchangeFormatTypeEnum type, Subdiv subdiv) {
		return dao.getByTypeAndBusinessSubdiv(type, subdiv);
	}

	@Override
	public void createOrUpdate(ExchangeFormat ef) {
		for (ExchangeFormatFieldNameDetails efd : ef.getExchangeFormatFieldNameDetails()) {
			efd.setExchangeFormat(ef);
		}
		dao.merge(ef);
	}

	@Override
	public byte[] getExchangeFormatExcelTemplateFile(ExchangeFormat ef, Locale locale) {
		try {
			// get template file based on locale
			File file;
			if (locale.getLanguage().equals(new Locale("es").getLanguage())) {
				file = new File(getClass().getResource("/excel_templates/" + IMPORT_TEMPLATE_ES).toURI());
			} else if (locale.getLanguage().equals(new Locale("fr").getLanguage())) {
				file = new File(getClass().getResource("/excel_templates/" + IMPORT_TEMPLATE_FR).toURI());
			} else {
				file = new File(getClass().getResource("/excel_templates/" + IMPORT_TEMPLATE_EN).toURI());
			}

			// read file
			XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));

			/* header */
			XSSFSheet mainSheet = makeHeader(ef, wb);

			/* main table */
			makeTable(ef, mainSheet);

			// create table cells
			makeTableContent(ef, mainSheet);

			/* validation */
			// for service type
			boolean serviceTypeExists = ef.getExchangeFormatFieldNameDetails().stream().anyMatch(efd -> {
				return efd.getFieldName().equals(ExchangeFormatFieldNameEnum.EXPECTED_SERVICE);
			});
			if (serviceTypeExists) {
				serviceTypeInFileValidation(ef, locale, mainSheet);
			}

			/* render */
			// write to byte array
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try {
				wb.write(bos);
			} finally {
				bos.close();
			}
			return bos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void serviceTypeInFileValidation(ExchangeFormat ef, Locale locale, XSSFSheet mainSheet) {
		// get service type column position
		int serviceTypePosition = ef.getExchangeFormatFieldNameDetails().stream().filter(efd -> {
			return efd.getFieldName().equals(ExchangeFormatFieldNameEnum.EXPECTED_SERVICE);
		}).findFirst().get().getPosition();

		// get translated service type names
		List<ServiceType> serviceTypes = serviceTypeService.getAll();
		String[] serviceTypeNames = (String[]) serviceTypes.stream().map(st -> {
			return this.translationService.getCorrectTranslation(st.getShortnameTranslation(), locale);
		}).collect(Collectors.toList()).toArray(new String[serviceTypes.size()]);

		// create validator
		DataValidationHelper dvHelper = new XSSFDataValidationHelper(mainSheet);
		DataValidationConstraint constraint = (XSSFDataValidationConstraint) dvHelper
				.createExplicitListConstraint(serviceTypeNames);

		CellRangeAddressList addressList = new CellRangeAddressList(ef.getLinesToSkip() + 1,
				ef.getLinesToSkip() + MAIN_TABLE_DEFAULT_ROW_COUNT, serviceTypePosition - 1, serviceTypePosition - 1);
		XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(constraint, addressList);
		validation.setShowErrorBox(true);
		validation.createErrorBox("Trescal Echange Format", "Please choose an element from the list");
		mainSheet.addValidationData(validation);
	}

	private void makeTableContent(ExchangeFormat ef, XSSFSheet mainSheet) {
		DataFormat df = mainSheet.getWorkbook().createDataFormat();
		for (int i = ef.getLinesToSkip(); i <= ef.getLinesToSkip() + MAIN_TABLE_DEFAULT_ROW_COUNT; i++) {
			// Create a Row
			XSSFRow row = mainSheet.createRow(i);
			for (int j = 0; j < ef.getExchangeFormatFieldNameDetails().size(); j++) {
				XSSFCell localXSSFCell = row.createCell(j, Cell.CELL_TYPE_STRING);
				// fix problem with leading zeros
				localXSSFCell.getCellStyle().setDataFormat(df.getFormat("@"));
				if (i == ef.getLinesToSkip()) {
					ExchangeFormatFieldNameDetails efd = ef.getExchangeFormatFieldNameDetails().get(j);
					if (StringUtils.isBlank(efd.getTemplateName()))
						localXSSFCell.setCellValue(efd.getFieldName().getValue());
					else
						localXSSFCell.setCellValue(efd.getTemplateName());

					// autosize columns
					mainSheet.autoSizeColumn(j);
				}
			}
		}
	}

	private void makeTable(ExchangeFormat ef, XSSFSheet mainSheet) {
		// create main table
		XSSFTable mainTable = mainSheet.createTable();
		mainTable.setName(MAIN_TABLE_NAME);
		mainTable.setDisplayName(MAIN_TABLE_NAME);

		// get CTTable object
		CTTable cttable = mainTable.getCTTable();
		cttable.setName(MAIN_TABLE_NAME);
		cttable.setDisplayName(MAIN_TABLE_NAME);
		cttable.setId(ef.getId());

		// set auto filter in header
		CTAutoFilter autoFilter = cttable.addNewAutoFilter();
		cttable.setAutoFilter(autoFilter);

		// Style the table
		// cttable.addNewTableStyleInfo();
		// CTTableStyleInfo style = cttable.getTableStyleInfo();
		// style.setName(MAIN_TABLE_STYLE_NAME);
		// style.setShowRowStripes(true);
		// style.setShowColumnStripes(true);
		// style.setShowFirstColumn(true);

		// Define the data range including headers
		AreaReference my_data_range = new AreaReference(new CellReference(ef.getLinesToSkip(), 0), new CellReference(
				ef.getLinesToSkip() + MAIN_TABLE_DEFAULT_ROW_COUNT, ef.getExchangeFormatFieldNameDetails().size() - 1));
		cttable.setRef(my_data_range.formatAsString());

		// create columns
		CTTableColumns columns = cttable.addNewTableColumns();
		// define number of columns
		columns.setCount(ef.getExchangeFormatFieldNameDetails().size());
		// Define Header Information for the Table
		for (ExchangeFormatFieldNameDetails efd : ef.getExchangeFormatFieldNameDetails()) {
			CTTableColumn column = columns.addNewTableColumn();
			if (StringUtils.isEmpty(efd.getTemplateName()))
				column.setName(efd.getFieldName().getFullName());
			else
				column.setName(efd.getTemplateName());
			column.setId(efd.getId());
		}
	}

	private XSSFSheet makeHeader(ExchangeFormat ef, XSSFWorkbook wb) {

		// locked Cell from editing
		// CellStyle lockedCellStyle = wb.createCellStyle();
		// lockedCellStyle.setLocked(true);

		// write id in file
		XSSFSheet mainSheet = wb.getSheetAt(MAIN_SHEET);
		// set sheet name if defined in ef
		if (StringUtils.isNotEmpty(ef.getSheetName())) {
			wb.setSheetName(wb.getSheetIndex(mainSheet), ef.getSheetName());
		}
		return mainSheet;
	}

	@Override
	public List<ExchangeFormat> getByTypeAndClientCompany(ExchangeFormatTypeEnum type, Company company) {
		return dao.getByTypeAndClientCompany(type, company);
	}

	@Override
	public Map<String, ExchangeFormatFieldNameEnum> getColumnNamesMapping(ExchangeFormat ef) {
		Map<String, ExchangeFormatFieldNameEnum> columnsNameMapping = ef.getExchangeFormatFieldNameDetails().stream()
				.collect(Collectors.toMap(efn -> {
					if (StringUtils.isBlank(efn.getTemplateName()))
						return efn.getFieldName().getFullName();
					else
						return efn.getTemplateName();
				}, ExchangeFormatFieldNameDetails::getFieldName, (u, v) -> {
					throw new IllegalStateException(String.format("Duplicate key %s", u));
				}, LinkedHashMap::new));

		return columnsNameMapping;
	}

	@Override
	public ExchangeFormat getExchangeFormatByLevelAndType(ExchangeFormatTypeEnum type, ExchangeFormatLevelEnum level) {
		return this.dao.getExchangeFormatByLevelAndType(type, level);
	}

	@Override
	public List<ExchangeFormatDTO> getExchangeFormats(ExchangeFormatTypeEnum type, Company businessCompany,
			Subdiv businessSubdiv, Company clientCompany) {
		return this.dao.getExchangeFormats(type, businessCompany, businessSubdiv, clientCompany);
	}

	@Override
	public Integer countExchangeFormats(ExchangeFormatTypeEnum type, Company businessCompany, Subdiv businessSubdiv,
			Company clientCompany) {
		return this.dao.countExchangeFormats(type, businessCompany, businessSubdiv, clientCompany);
	}

	@Override
	public List<ExchangeFormatFieldNameDetails> getEfFields(Integer id) {
		ExchangeFormat ef = this.get(id);
		return ef.getExchangeFormatFieldNameDetails();
	}

}
