package org.trescal.cwms.core.exchangeformat.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.AliasGroupDTO;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.transformer.AliasGroupTransformer;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.dto.form.ExchangeFormatForm;
import org.trescal.cwms.core.exchangeformat.dto.form.validator.ExchangeFormatFormValidator;
import org.trescal.cwms.core.exchangeformat.dto.transformer.ExchangeFormatFormTransformer;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatDateFormatEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
	Constants.SESSION_ATTRIBUTE_COMPANY})
public class ExchangeFormatController {

	private final String VIEW_NAME = "trescal/core/logistics/exchangeformat";
	private final String FORM_NAME = "exchangeformatform";
	private final String REQUESTMAPPING_VALUE = "/exchangeformat.htm";

	@Autowired
	private ExchangeFormatFormTransformer transformer;
	@Autowired
	private ExchangeFormatService exchangeFormatServ;
	@Autowired
	private CompanyService companyServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private ExchangeFormatService exchangeFormatService;
	@Autowired
	private ExchangeFormatFormValidator validator;
	@Autowired
	private MessageSource messages;
	@Autowired
	private AliasGroupService aliasGroupService;
	@Autowired
	private AliasGroupTransformer aliasGroupTransformer;
	@Autowired
	private AsnService asnService;

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {

		binder.registerCustomEditor(ExchangeFormatFieldNameEnum.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String value) throws IllegalArgumentException {
				if (StringUtils.isBlank(value))
					return;

				setValue(ExchangeFormatFieldNameEnum.valueOf(value));
			}

			@Override
			public String getAsText() {
				if (getValue() == null)
					return "";

				return ((ExchangeFormatFieldNameEnum) getValue()).name();
			}
		});
	}

	@ModelAttribute("exchangeformatfieldname")
	public ExchangeFormatFieldNameEnum[] exchangeFormatFieldName() {
		return ExchangeFormatFieldNameEnum.values();
	}

	@ModelAttribute("exchangedateformatforselect")
	public ExchangeFormatDateFormatEnum[] exchangeDateFormatForSelect() {
		return ExchangeFormatDateFormatEnum.values();
	}

	@ModelAttribute("exchangeformattypes")
	public List<ExchangeFormatTypeEnum> exchangeFormatTypes() {
		List<ExchangeFormatTypeEnum> exchangeFormatTypes = new ArrayList<ExchangeFormatTypeEnum>();
		for (ExchangeFormatTypeEnum exchangeFormatType : ExchangeFormatTypeEnum.values()) {
			// remove the possibility to create an PREBOOKING_FROM_MOBILEAPP
			// exchange format
			if (!exchangeFormatType.equals(ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP))
				exchangeFormatTypes.add(exchangeFormatType);
		}
		return exchangeFormatTypes;
	}

	@ModelAttribute("aliasgroupdtos")
	public List<AliasGroupDTO> aliasGroups(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		List<AliasGroupDTO> aliasGroupDTOs = new ArrayList<AliasGroupDTO>();
		for (AliasGroup aliasgroup : this.aliasGroupService
				.getAliasGroupsForAllocatedCompany(this.companyServ.get(companyDto.getKey()))) {
			aliasGroupDTOs.add(this.aliasGroupTransformer.transform(aliasgroup));
		}
		return aliasGroupDTOs;
	}

	@RequestMapping(value = REQUESTMAPPING_VALUE, method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "id", required = false) Integer efid,
			@RequestParam(value = "businessCompany", required = false) Integer buscompid,
			@RequestParam(value = "businessSubdiv", required = false) Integer bussubdivid,
			@RequestParam(value = "clientComp", required = false) Integer clientid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@RequestParam(value = "action", required = true) String action, HttpServletRequest request, Model model)
			throws Exception {

		ExchangeFormatForm eff;
		if (efid != null) {
			eff = transformer.transform(exchangeFormatServ.get(efid));
		} else {
			eff = new ExchangeFormatForm();
			eff.setLinesToSkip(0);
			eff.setExchangeFormatFieldNameDetailsForm(new ArrayList<>());
			if (eff.getCompanySelected() == null)
				eff.setCompanySelected(true);
		}
		if (StringUtils.isBlank(eff.getRefererUrl()))
			eff.setRefererUrl(request.getHeader("referer"));

		if (action.equals("create")) { // create mode
			if (buscompid != null) {
				Company company = companyServ.get(buscompid);
				model.addAttribute("businessCompany", company);
				eff.setFromBusinessCompany(true);
			} else if (bussubdivid != null) {
				Subdiv subdiv = subdivServ.get(bussubdivid);
				model.addAttribute("businessSubdiv", subdiv);
				eff.setFromBusinessSubdiv(true);
			} else if (clientid != null) {
				Company company = companyServ.get(clientid);
				model.addAttribute("clientCompany", company);
				Company allocatedCompany = companyServ.get(companyDto.getKey());
				model.addAttribute("businessCompany", allocatedCompany);
				Subdiv allocatedSubdiv = subdivServ.get(subdivDto.getKey());
				model.addAttribute("businessSubdiv", allocatedSubdiv);

				eff.setFromClient(true);
			}
		} else if (action.equals("edit")) { // edit mode
			Company company;
			if (buscompid != null) {
				if (eff.getBusinessCompanyId() != null) {
					company = companyServ.get(eff.getBusinessCompanyId());
				} else
					company = companyServ.get(buscompid);
				model.addAttribute("businessCompany", company);
				eff.setFromBusinessCompany(true);
				if (eff.getClientCompanyId() != null) {
					Company clientCompany = companyServ.get(eff.getClientCompanyId());
					model.addAttribute("clientCompany", clientCompany);
				}
			} else if (bussubdivid != null) {
				Subdiv subdiv;
				if (eff.getBusinessSubdivId() != null) {
					subdiv = subdivServ.get(eff.getBusinessSubdivId());
				} else
					subdiv = subdivServ.get(bussubdivid);

				model.addAttribute("businessSubdiv", subdiv);
				eff.setFromBusinessSubdiv(true);
				if (eff.getClientCompanyId() != null) {
					Company clientCompany = companyServ.get(eff.getClientCompanyId());
					model.addAttribute("clientCompany", clientCompany);
				}
			} else if (clientid != null) {
				Company companyClient;
				if (eff.getClientCompanyId() != null) {
					companyClient = companyServ.get(eff.getClientCompanyId());
				} else
					companyClient = companyServ.get(clientid);

				model.addAttribute("clientCompany", companyClient);
				eff.setFromClient(true);
				if (eff.getBusinessCompanyId() != null) {
					Company buscompany = companyServ.get(eff.getBusinessCompanyId());
					model.addAttribute("businessCompany", buscompany);
					eff.setCompanySelected(true);
				}
				if (eff.getBusinessSubdivId() != null) {
					Subdiv bussubdiv = subdivServ.get(eff.getBusinessSubdivId());
					model.addAttribute("businessSubdiv", bussubdiv);
					eff.setCompanySelected(false);
				}
			}
		}

		model.addAttribute("action", action);
		model.addAttribute(FORM_NAME, eff);

		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = REQUESTMAPPING_VALUE, method = RequestMethod.POST)
	protected ModelAndView createUpdate(@Valid @ModelAttribute(FORM_NAME) ExchangeFormatForm eff, BindingResult result,
										@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
										@RequestParam(value = "clientComp", required = false) Integer clientid,
										@RequestParam(value = "action") String action,
										@RequestParam(value = "businessCompany", required = false) Integer buscompid,
										@RequestParam(value = "businessSubdiv", required = false) Integer bussubdivid, HttpServletRequest request,
										Model model) throws Exception {

		if (!result.hasErrors()) {
			// remove deleted items
			if (eff.getExchangeFormatFieldNameDetailsForm() != null)
				eff.getExchangeFormatFieldNameDetailsForm().removeIf(e -> e.getPosition() == null);
			// get business subdiv id from search plugin if exists
			if (eff.getSubdivid() != null)
				eff.setBusinessSubdivId(eff.getSubdivid());

			if (eff.isFromClient() && eff.getType() != ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP) {
				if (eff.getCompanySelected())
					eff.setBusinessSubdivId(null);
				else
					eff.setBusinessCompanyId(null);
			}

			// init ef if a default alias group found 
			if (action.equals("create")) {
				if (eff.getAliasGroupid() == 0) {
					AliasGroup aliasGroup = this.aliasGroupService
							.checkIfDefaultAliasGroupIsAlreadyDefined(this.companyServ.get(companyDto.getKey()));
					if (aliasGroup != null) {
						eff.setAliasGroupid(aliasGroup.getId());
					}
				}
			}

			exchangeFormatServ.createOrUpdate(transformer.transform(eff));
			// we no longer use 'eff.getRefererUrl()' because in some
			// browsers(Google Chrome for example) doesn't get the correct url
			String tab = "&loadtab=exchangeformat-tab";
			String url;
			if (StringUtils.isNotBlank(eff.getRefererUrl())) {

				if (clientid != null) {
					url = "/viewcomp.htm?coid=" + clientid;
				} else if (buscompid != null)
					url = "/viewcomp.htm?coid=" + buscompid;
				else
					url = "/viewsub.htm?subdivid=" + bussubdivid;

				RedirectView rv = new RedirectView(url + tab, true);
				rv.setExposeModelAttributes(false);
				return new ModelAndView(rv);
			} else
				return new ModelAndView(VIEW_NAME);
		}

		if (buscompid != null) {
			Company company = companyServ.get(buscompid);
			model.addAttribute("businessCompany", company);
			eff.setFromBusinessCompany(true);
		} else if (bussubdivid != null) {
			Subdiv subdiv = subdivServ.get(bussubdivid);
			model.addAttribute("businessSubdiv", subdiv);
			eff.setFromBusinessSubdiv(true);
		} else if (clientid != null) {
			Company company = companyServ.get(clientid);
			model.addAttribute("clientCompany", company);
			eff.setFromClient(true);

			if (!ObjectUtils.isEmpty(eff.getCompanySelected())) {
				Company allocatedCompany = companyServ.get(companyDto.getKey());
				model.addAttribute("businessCompany", allocatedCompany);
			} else {
				Subdiv allocatedSubdiv = subdivServ.get(subdivDto.getKey());
				model.addAttribute("businessSubdiv", allocatedSubdiv);
			}

		}
		model.addAttribute("action", action);
		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = "/deleteExchangeFormat.json", method = RequestMethod.GET)
	public @ResponseBody ResultWrapper delete(@RequestParam(value = "id", required = true) int id, Locale locale) {

		ExchangeFormat ef = exchangeFormatService.get(id);
		if (ef == null)
			return new ResultWrapper(false, messages.getMessage("error.rest.data.notfound", null, locale));

		List<Asn> listAsn = asnService.getAllAsnActiveByExchangeFormatId(id);

		// if the exchange format is used in a prebooking
		if (!listAsn.isEmpty()) {
			return new ResultWrapper(false,
					messages.getMessage("error.rest.exchangeformat.in.use", new Object[] { listAsn.size() }, locale));
		} else {
			ef.setDeleted(true);
			this.exchangeFormatServ.merge(ef);
		}
		return new ResultWrapper(true, "ok");
	}

	@RequestMapping(value = "/downloadExchangeFormatTemplate", method = RequestMethod.GET)
	public void downloadTemplate(@RequestParam(value = "id", required = true) int id, HttpServletResponse response,
			Locale locale) throws IOException {

		ExchangeFormat ef = exchangeFormatService.get(id);
		if (ef == null)
			return;

		// name
		String filename = "EF" + id + "_" + ef.getName() + ".xlsx";

		// make the template
		byte[] template = exchangeFormatServ.getExchangeFormatExcelTemplateFile(ef, locale);

		if (template != null) {
			// set response headers
			response.setContentType(URLConnection.guessContentTypeFromName(filename));
			response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
			response.setContentLength(template.length);

			// copy file to response
			FileCopyUtils.copy(template, response.getOutputStream());
		}

	}

}