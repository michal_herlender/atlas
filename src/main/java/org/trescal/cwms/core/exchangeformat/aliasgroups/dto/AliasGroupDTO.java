package org.trescal.cwms.core.exchangeformat.aliasgroups.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class AliasGroupDTO implements Serializable {

	private static final long serialVersionUID = -7200047124692372272L;

	private Integer alaisGroupId;
	private String action;
	private String aliasGroupName;
	private String aliasGroupDesc;
	private Integer createdById;
	private LocalDateTime createdOn;
	private boolean defaultForCurrentBusinessComp;
	private List<AliasDetailDTO> aliasesForm;

	private Integer allocatedCompanyid;
	private String allocatedCompanyName;

	public Integer getAlaisGroupId() {
		return alaisGroupId;
	}

	public void setAlaisGroupId(Integer alaisGroupId) {
		this.alaisGroupId = alaisGroupId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAliasGroupName() {
		return aliasGroupName;
	}

	public void setAliasGroupName(String aliasGroupName) {
		this.aliasGroupName = aliasGroupName;
	}

	public String getAliasGroupDesc() {
		return aliasGroupDesc;
	}

	public void setAliasGroupDesc(String aliasGroupDesc) {
		this.aliasGroupDesc = aliasGroupDesc;
	}
	
	public Integer getCreatedById() {
		return createdById;
	}

	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public List<AliasDetailDTO> getAliasesForm() {
		return aliasesForm;
	}

	public void setAliasesForm(List<AliasDetailDTO> aliasesForm) {
		this.aliasesForm = aliasesForm;
	}

	public boolean isDefaultForCurrentBusinessComp() {
		return defaultForCurrentBusinessComp;
	}

	public void setDefaultForCurrentBusinessComp(boolean defaultForCurrentBusinessComp) {
		this.defaultForCurrentBusinessComp = defaultForCurrentBusinessComp;
	}

	public Integer getAllocatedCompanyid() {
		return allocatedCompanyid;
	}

	public void setAllocatedCompanyid(Integer allocatedCompanyid) {
		this.allocatedCompanyid = allocatedCompanyid;
	}

	public String getAllocatedCompanyName() {
		return allocatedCompanyName;
	}

	public void setAllocatedCompanyName(String allocatedCompanyName) {
		this.allocatedCompanyName = allocatedCompanyName;
	}

}
