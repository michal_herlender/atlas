package org.trescal.cwms.core.exchangeformat.dto.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.exchangeformat.dto.form.ExchangeFormatFieldNameDetailsForm;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.db.ExchangeFormatFieldNameDetailsService;
import org.trescal.cwms.core.tools.EntityDtoTransformer;

@Service
public class ExchangeFormatFieldNameDetailsFormTransformer
		implements EntityDtoTransformer<ExchangeFormatFieldNameDetails, ExchangeFormatFieldNameDetailsForm> {

	@Autowired
	private ExchangeFormatFieldNameDetailsService exchangeFormatFieldNameDetailsServ;

	@Autowired
	private ExchangeFormatService exchangeFormatServ;
	
	@Override
	public ExchangeFormatFieldNameDetails transform(ExchangeFormatFieldNameDetailsForm dto) {
		ExchangeFormatFieldNameDetails entity;
		if (dto.getId() != null)
			entity = exchangeFormatFieldNameDetailsServ.get(dto.getId());
		else
			entity = new ExchangeFormatFieldNameDetails();

		entity.setFieldName(dto.getFieldName());
		entity.setMandatory(dto.isMandatory());
		entity.setPosition(dto.getPosition());
		entity.setTemplateName(dto.getTemplateName());
		if (dto.getExchangeFormatid() != null)
			entity.setExchangeFormat(exchangeFormatServ.get(dto.getExchangeFormatid()));

		return entity;
	}

	@Override
	public ExchangeFormatFieldNameDetailsForm transform(ExchangeFormatFieldNameDetails entity) {

		ExchangeFormatFieldNameDetailsForm dto = new ExchangeFormatFieldNameDetailsForm();
		// BeanUtils.copyProperties(entity, dto);
		dto.setFieldName(entity.getFieldName());

		if (entity.getExchangeFormat() != null)
			dto.setExchangeFormatid(entity.getExchangeFormat().getId());

		dto.setMandatory(entity.isMandatory());
		dto.setId(entity.getId());
		dto.setPosition(entity.getPosition());
		dto.setTemplateName(entity.getTemplateName());

		return dto;
	}

}
