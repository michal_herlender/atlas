package org.trescal.cwms.core.exchangeformat.dto.form;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatDateFormatEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;

public class ExchangeFormatForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String description;
	private ExchangeFormatDateFormatEnum dateFormat;
	private int linesToSkip;
	private String sheetName;

	private Integer businessCompanyId;
	private Integer businessSubdivId;
	private Integer clientCompanyId;

	private boolean fromBusinessCompany;
	private boolean fromBusinessSubdiv;
	private boolean fromClient;

	private String refererUrl;
	private List<ExchangeFormatFieldNameDetailsForm> exchangeFormatFieldNameDetailsForm;
	private ExchangeFormatTypeEnum type;
	private ExchangeFormatLevelEnum level;

	private Integer aliasGroupid;
	private boolean deleted;

	/* radion button */
	private Boolean companySelected;

	/* used by the search subdiv plugin */
	private Integer subdivid;

	public interface FromBusinessCompany {
	}

	public interface FromBusinessSubdiv {
	}

	public interface FromClientCompanyOrSubdiv {
	}

	@NotNull
	@Size(min = 2)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@NotNull
	public ExchangeFormatDateFormatEnum getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(ExchangeFormatDateFormatEnum dateFormat) {
		this.dateFormat = dateFormat;
	}

	public List<ExchangeFormatFieldNameDetailsForm> getExchangeFormatFieldNameDetailsForm() {
		return exchangeFormatFieldNameDetailsForm;
	}

	public void setExchangeFormatFieldNameDetailsForm(
			List<ExchangeFormatFieldNameDetailsForm> exchangeFormatFieldNameDetailsForm) {
		this.exchangeFormatFieldNameDetailsForm = exchangeFormatFieldNameDetailsForm;
	}

	public Integer getSubdivid() {
		return subdivid;
	}

	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	public Integer getBusinessCompanyId() {
		return businessCompanyId;
	}

	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}

	public Integer getBusinessSubdivId() {
		return businessSubdivId;
	}

	public void setBusinessSubdivId(Integer businessSubdivId) {
		this.businessSubdivId = businessSubdivId;
	}

	public Integer getClientCompanyId() {
		return clientCompanyId;
	}

	public void setClientCompanyId(Integer clientCompanyId) {
		this.clientCompanyId = clientCompanyId;
	}

	public boolean isFromBusinessCompany() {
		return fromBusinessCompany;
	}

	public void setFromBusinessCompany(Boolean fromBusinessCompany) {
		this.fromBusinessCompany = fromBusinessCompany;
	}

	public boolean isFromBusinessSubdiv() {
		return fromBusinessSubdiv;
	}

	public void setFromBusinessSubdiv(boolean fromBusinessSubdiv) {
		this.fromBusinessSubdiv = fromBusinessSubdiv;
	}

	public boolean isFromClient() {
		return fromClient;
	}

	public void setFromClient(boolean fromClient) {
		this.fromClient = fromClient;
	}

	public String getRefererUrl() {
		return refererUrl;
	}

	public void setRefererUrl(String refererUrl) {
		this.refererUrl = refererUrl;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getCompanySelected() {
		return companySelected;
	}

	public void setCompanySelected(Boolean companySelected) {
		this.companySelected = companySelected;
	}

	public void setFromBusinessCompany(boolean fromBusinessCompany) {
		this.fromBusinessCompany = fromBusinessCompany;
	}

	public int getLinesToSkip() {
		return linesToSkip;
	}

	public void setLinesToSkip(int linesToSkip) {
		this.linesToSkip = linesToSkip;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public ExchangeFormatTypeEnum getType() {
		return type;
	}

	public void setType(ExchangeFormatTypeEnum type) {
		this.type = type;
	}

	public ExchangeFormatLevelEnum getLevel() {
		return level;
	}

	public void setLevel(ExchangeFormatLevelEnum level) {
		this.level = level;
	}
	
	public Integer getAliasGroupid() {
		return aliasGroupid;
	}

	public void setAliasGroupid(Integer aliasGroupid) {
		this.aliasGroupid = aliasGroupid;
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

}