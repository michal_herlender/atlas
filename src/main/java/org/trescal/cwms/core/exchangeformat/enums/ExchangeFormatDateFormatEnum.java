package org.trescal.cwms.core.exchangeformat.enums;

public enum ExchangeFormatDateFormatEnum {

	DD_MM_YYYY("dd/MM/yyyy"),
	MM_DD_YYYY("MM/dd/yyyy"), 
	DD_MM_YYYY_HH_MM("dd/MM/yyyy - HH:mm"), 
	DD_MM_YYYY_HH_MM_SS("dd/MM/yyyy - HH:mm:ss");

	private String value;

	private ExchangeFormatDateFormatEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return this.name();
	}
}