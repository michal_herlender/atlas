package org.trescal.cwms.core.exchangeformat.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasIntervalUnit;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.spring.model.KeyValue;

@Component
public class ExchangeFormatGeneralValidator {

	private static final int MAX_ROWS_IN_EF_FILE_250 = 250;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private AliasGroupService aliasGroupService;

	private List<KeyValue<Integer, String>> serviceTypes;

	public BindingResult validate(ExchangeFormat ef, List<LinkedCaseInsensitiveMap<String>> fileContent,
			BindingResult br) {

		serviceTypes = serviceTypeService.getDTOListWithShortNames(null, null, LocaleContextHolder.getLocale(), false);

		// check if file is not empty
		if (fileContent.isEmpty()) {
			br.reject("exchangeformat.emptyfile");
			return br;
		}

		if (fileContent.size() > MAX_ROWS_IN_EF_FILE_250) {
			br.reject("exchangeformat.morethan250");
			return br;
		}

		// check if all mandatory columns exist
		Set<String> fileColumns = fileContent.get(0).keySet();
		List<String> missingFileColumns = new ArrayList<String>();
		for (ExchangeFormatFieldNameDetails efColumn : ef.getExchangeFormatFieldNameDetails()) {
			if (efColumn.isMandatory()) {
				boolean fieldExists = fileColumns.stream().anyMatch(c -> {
					if (efColumn.getTemplateName() != null && !StringUtils.isBlank(efColumn.getTemplateName()))
						return c.trim().toLowerCase().equals(efColumn.getTemplateName().trim().toLowerCase());
					else
						return c.trim().toLowerCase().equals(efColumn.getFieldName().getValue().trim().toLowerCase());
				});
				if (!fieldExists) {
					if (efColumn.getTemplateName() != null && !StringUtils.isBlank(efColumn.getTemplateName()))
						missingFileColumns.add(efColumn.getTemplateName());
					else
						missingFileColumns.add(efColumn.getFieldName().getValue());
				}
			}
		}
		if (!missingFileColumns.isEmpty()) {
			br.reject("exchangeformat.import.validation.mandatorycolumns.missing",
					new String[] { String.join(",", missingFileColumns) }, "");
			return br;
		}

		// check if data exists in all fields in mandatory columns
		List<String> mandatoryColumnsWithMissingFields = new ArrayList<String>();
		List<String> mandatoryColumnsWithInvaliddata = new ArrayList<String>();

		List<ExchangeFormatFieldNameDetails> columns = ef.getExchangeFormatFieldNameDetails().stream()
				.collect(Collectors.toList());

		int index = 1;
		for (LinkedCaseInsensitiveMap<String> row : fileContent) {
			Iterator<ExchangeFormatFieldNameDetails> columnIterator = columns.iterator();
			while (columnIterator.hasNext()) {
				ExchangeFormatFieldNameDetails column = columnIterator.next();
				if (row.get(getColumnName(column)) != null) {
					String currentValue = row.get(getColumnName(column)).trim();
					if (column.isMandatory() && StringUtils.isBlank(currentValue)) {
						// add column to list of errors
						mandatoryColumnsWithMissingFields.add(getColumnName(column));
						// remove from list, so it will not be scanned it future
						// iterations
						columnIterator.remove();
					} else if (StringUtils.isNotBlank(currentValue) && !dataIsValide(currentValue, column)) {
						// add column to list of errors
						mandatoryColumnsWithInvaliddata
								.add(getColumnName(column) + " (L:" + (index + ef.getLinesToSkip() + 1) + ")");
						columnIterator.remove();
					}
				}
			}
			index++;
		}
		// missing mandatory fields
		if (!mandatoryColumnsWithMissingFields.isEmpty()) {
			br.reject("exchangeformat.import.validation.mandatorycolumns.missingdata",
					new String[] { String.join(",", mandatoryColumnsWithMissingFields) }, "");
			return br;
		}
		// columns with invalid data
		if (!mandatoryColumnsWithInvaliddata.isEmpty()) {
			br.reject("exchangeformat.import.validation.columnswithinvaliddata",
					new String[] { String.join(",", mandatoryColumnsWithInvaliddata) }, "");
			return br;
		}

		return br;
	}

	private boolean dataIsValide(String val, ExchangeFormatFieldNameDetails column) {

		if (!dataTypeValide(val, column) || !columnIdIsValide(val, column))
			return false;
		return true;
	}

	private boolean columnIdIsValide(String val, ExchangeFormatFieldNameDetails column) {
		switch (column.getFieldName()) {
		// validation of service type id
		case SERVICETYPE_ID:
			boolean doesExist = this.serviceTypes.stream().anyMatch(st -> {
				return st.getKey().equals(Integer.valueOf(val));
			});
			if (!doesExist)
				return false;
			break;

		default:
			break;
		}
		return true;
	}

	@SuppressWarnings("rawtypes")
	private boolean dataTypeValide(String currentValue, ExchangeFormatFieldNameDetails column) {

		Class<?> expecteddataType = column.getFieldName().getDataType();

		if (expecteddataType.equals(Integer.class)) {
			try {
				if (column.getExchangeFormat().getAliasGroup() == null && column.getFieldName().isNeedAlias())
					Integer.valueOf(currentValue);
			} catch (Exception e) {
				return false;
			}
		} else if (expecteddataType.equals(Date.class)) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(column.getExchangeFormat().getDateFormat().getValue());
				sdf.setLenient(false);
				sdf.parse(currentValue);
			} catch (Exception e) {
				return false;
			}

		} else if (expecteddataType.equals(ServiceType.class)) {
			return this.aliasGroupService.determineExpectedServiceTypeFromAliasOrValue(
					column.getExchangeFormat().getAliasGroup(), currentValue) != null;
		} else if (expecteddataType.equals(IntervalUnit.class)) {
			// test for all languages, and singular/plural forms:
			// month/months...
			if (column.getExchangeFormat().getAliasGroup() != null
					&& column.getExchangeFormat().getAliasGroup().getIntervalUnitAliases() != null) {
				AliasIntervalUnit aliasIU = column.getExchangeFormat().getAliasGroup().getIntervalUnitAliases().stream()
						.filter(aiu -> aiu.getAlias().equalsIgnoreCase(currentValue)).findAny().orElse(null);
				if (aliasIU == null)
					return false;
			} else {
				List<String> acceptedValues = Arrays.stream(IntervalUnit.values()).flatMap(iu -> {
					List<String> possibleValues = new ArrayList<>();
					String[] locales = { "en", "es", "fr", "de" };
					Arrays.asList(locales).forEach(loc -> {
						possibleValues.add(messageSource.getMessage(iu.getSingularNameCode(), null, new Locale(loc))
								.toLowerCase());
						possibleValues.add(
								messageSource.getMessage(iu.getPluralNameCode(), null, new Locale(loc)).toLowerCase());
						possibleValues.add(messageSource.getMessage(iu.getSingleCharacterCode(), null, new Locale(loc))
								.toLowerCase());
					});
					return possibleValues.stream();
				}).collect(Collectors.toList());
				if (!acceptedValues.contains(currentValue.toLowerCase()))
					return false;
			}

		} else if (expecteddataType.isEnum()) {
			@SuppressWarnings({ "unchecked" })
			List<Enum> enumValues = (List<Enum>) Arrays.asList(expecteddataType.getEnumConstants());
			for (Enum en : enumValues) {
				if (en.toString().equalsIgnoreCase(currentValue))
					return true;
			}
			return false;
		}

		return true;
	}

	private String getColumnName(ExchangeFormatFieldNameDetails effnd) {
		if (effnd.getTemplateName() != null && !StringUtils.isBlank(effnd.getTemplateName()))
			return effnd.getTemplateName();
		else
			return effnd.getFieldName().getValue();
	}

}
