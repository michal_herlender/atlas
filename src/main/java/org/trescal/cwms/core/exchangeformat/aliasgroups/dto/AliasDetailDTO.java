package org.trescal.cwms.core.exchangeformat.aliasgroups.dto;

import java.io.Serializable;

import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

public class AliasDetailDTO implements Serializable {

	private static final long serialVersionUID = -1137318392084260091L;

	private Integer aliasId;
	private ExchangeFormatFieldNameEnum effn;
	private String value;
	private String alias;

	private ServiceType serviceType;
	private IntervalUnit intervalUnit;

	public Integer getAliasId() {
		return aliasId;
	}

	public void setAliasId(Integer aliasId) {
		this.aliasId = aliasId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public ExchangeFormatFieldNameEnum getEffn() {
		return effn;
	}

	public void setEffn(ExchangeFormatFieldNameEnum effn) {
		this.effn = effn;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	public IntervalUnit getIntervalUnit() {
		return intervalUnit;
	}

	public void setIntervalUnit(IntervalUnit intervalUnit) {
		this.intervalUnit = intervalUnit;
	}

}

