package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;

public interface ExchangeFormatFileDao extends BaseDao<ExchangeFormatFile, Integer> {

	List<ExchangeFormatFile> getByFileName(String filename);

}
