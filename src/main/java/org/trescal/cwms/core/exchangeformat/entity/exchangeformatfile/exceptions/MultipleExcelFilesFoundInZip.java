package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.exceptions;

public class MultipleExcelFilesFoundInZip extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MultipleExcelFilesFoundInZip() {
		super("Multiple Excel files found inside zip, only 1 Excel file is allowed !");
	}

}
