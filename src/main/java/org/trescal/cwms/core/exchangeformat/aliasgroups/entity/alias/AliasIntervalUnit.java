package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.trescal.cwms.core.system.enums.IntervalUnit;

@Entity
@Table(name = "aliasintervalunit")
public class AliasIntervalUnit extends Alias<IntervalUnit> {

	private IntervalUnit value;

	public void setValue(IntervalUnit value) {
		this.value = value;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "value")
	public IntervalUnit getValue() {
		return value;
	}
	



}