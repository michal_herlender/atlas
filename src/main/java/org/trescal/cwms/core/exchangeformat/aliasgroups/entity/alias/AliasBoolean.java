package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "aliasboolean")
public class AliasBoolean extends Alias<Boolean> {

	private Boolean value;

	public void setValue(Boolean value) {
		this.value = value;
	}

	
	@Column(name = "value")
	public Boolean getValue() {
		return value;
	}



}