package org.trescal.cwms.core.exchangeformat.dto.form;

import java.io.Serializable;

import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;

public class ExchangeFormatFieldNameDetailsForm implements Serializable {
	private static final long serialVersionUID = -1250413236050842813L;

	private Integer id;
	private Integer position;
	private boolean mandatory;
	private String templateName;
	private Integer exchangeFormatid;
	private ExchangeFormatFieldNameEnum fieldName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public Integer getExchangeFormatid() {
		return exchangeFormatid;
	}

	public void setExchangeFormatid(Integer exchangeFormatid) {
		this.exchangeFormatid = exchangeFormatid;
	}

	public ExchangeFormatFieldNameEnum getFieldName() {
		return fieldName;
	}

	public void setFieldName(ExchangeFormatFieldNameEnum fieldName) {
		this.fieldName = fieldName;
	}

}
