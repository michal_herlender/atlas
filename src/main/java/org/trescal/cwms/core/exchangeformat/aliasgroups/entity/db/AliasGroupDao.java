package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;

public interface AliasGroupDao extends BaseDao<AliasGroup, Integer> {

	public AliasGroup checkIfDefaultAliasGroupIsAlreadyDefined(Company businessCompany);

	public List<AliasGroup> getAliasGroupsForAllocatedCompany(Company AllocatedCompany);
}
