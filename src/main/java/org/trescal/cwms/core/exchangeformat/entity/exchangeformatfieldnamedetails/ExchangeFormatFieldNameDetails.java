package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;

@Entity
@Table(name = "exchangeformatfieldnamedetails")
public class ExchangeFormatFieldNameDetails {

	private Integer id;
	private int position;
	private boolean mandatory;
	private String templateName;
	private ExchangeFormat exchangeFormat;
	private ExchangeFormatFieldNameEnum fieldName;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@Column(name = "position")
	public int getPosition() {
		return position;
	}

	@Column(name = "mandatory")
	public boolean isMandatory() {
		return mandatory;
	}

	@Column(name = "templateName")
	public String getTemplateName() {
		return templateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "exchangeFormatid", foreignKey = @ForeignKey(name = "FK_exchangeformat_id"), nullable = true)
	public ExchangeFormat getExchangeFormat() {
		return exchangeFormat;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "fieldName")
	public ExchangeFormatFieldNameEnum getFieldName() {
		return fieldName;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public void setExchangeFormat(ExchangeFormat exchangeFormat) {
		this.exchangeFormat = exchangeFormat;
	}

	public void setFieldName(ExchangeFormatFieldNameEnum fieldName) {
		this.fieldName = fieldName;
	}

}
