package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;

public interface ExchangeFormatFieldNameDetailsDao extends BaseDao<ExchangeFormatFieldNameDetails, Integer> {

	List<ExchangeFormatFieldNameDetails> getByExchangeFormat(Integer efId);

}
