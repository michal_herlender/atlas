package org.trescal.cwms.core.exchangeformat.dto.transformer;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.dto.form.ExchangeFormatForm;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatLevelEnum;
import org.trescal.cwms.core.tools.EntityDtoTransformer;

@Service
public class ExchangeFormatFormTransformer implements EntityDtoTransformer<ExchangeFormat, ExchangeFormatForm> {

	@Autowired
	private ExchangeFormatService exchangeFormatServ;
	@Autowired
	private CompanyService companyServ;
	@Autowired
	private SubdivService subDivServ;
	@Autowired
	private ExchangeFormatFieldNameDetailsFormTransformer exchangeFormatFieldNameDetailsFormTransformer;
	@Autowired
	private AliasGroupService AliasGroupService;

	@Override
	public ExchangeFormat transform(ExchangeFormatForm dto) {
		ExchangeFormat entity;
		if (dto.getId() != null)
			entity = exchangeFormatServ.get(dto.getId());
		else {
			entity = new ExchangeFormat();
			entity.setExchangeFormatFieldNameDetails(new ArrayList<>());
		}
		if (dto.getClientCompanyId() != null) // field is optional
			entity.setClientCompany(companyServ.get(dto.getClientCompanyId()));
		entity.setDateFormat(dto.getDateFormat());
		entity.setDescription(dto.getDescription());
		entity.setName(dto.getName());
		entity.setLinesToSkip(dto.getLinesToSkip());
		entity.setSheetName(dto.getSheetName());
		entity.setType(dto.getType());
		entity.setExchangeFormatLevel(dto.getLevel());
		entity.getExchangeFormatFieldNameDetails().clear();
		entity.setDeleted(dto.isDeleted());
		if (!CollectionUtils.isEmpty(dto.getExchangeFormatFieldNameDetailsForm())) {
			entity.getExchangeFormatFieldNameDetails()
				.addAll(dto.getExchangeFormatFieldNameDetailsForm().stream().map(efddto -> exchangeFormatFieldNameDetailsFormTransformer.transform(efddto)).collect(Collectors.toList()));
		}
		if (dto.getBusinessCompanyId() != null) {
			entity.setBusinessCompany(companyServ.get(dto.getBusinessCompanyId()));
			entity.setExchangeFormatLevel(ExchangeFormatLevelEnum.BUSINESSCOMPANY);
		} else {
			entity.setBusinessCompany(null);
		}
		if (dto.getBusinessSubdivId() != null) {
			entity.setBusinessSubdiv(subDivServ.get(dto.getBusinessSubdivId()));
			entity.setExchangeFormatLevel(ExchangeFormatLevelEnum.BUSINESSSUBDIV);
		} else {
			entity.setBusinessSubdiv(null);
		}
		entity.setAliasGroup(this.AliasGroupService.get(dto.getAliasGroupid()));
		return entity;
	}

	@Override
	public ExchangeFormatForm transform(ExchangeFormat entity) {

		ExchangeFormatForm dto = new ExchangeFormatForm();
		// BeanUtils.copyProperties(entity, dto);
		dto.setId(entity.getId());
		if (entity.getClientCompany() != null)
			dto.setClientCompanyId(entity.getClientCompany().getId());
		dto.setDateFormat(entity.getDateFormat());
		dto.setDescription(entity.getDescription());
		dto.setName(entity.getName());
		dto.setType(entity.getType());
		dto.setLevel(entity.getExchangeFormatLevel());
		dto.setLinesToSkip(entity.getLinesToSkip());
		dto.setSheetName(entity.getSheetName());
		dto.setDeleted(entity.isDeleted());
		if (!CollectionUtils.isEmpty(entity.getExchangeFormatFieldNameDetails())) {
			dto.setExchangeFormatFieldNameDetailsForm(
				entity.getExchangeFormatFieldNameDetails().stream().map(efdEntity -> exchangeFormatFieldNameDetailsFormTransformer.transform(efdEntity)).collect(Collectors.toList()));
		}
		if (entity.getBusinessCompany() != null)
			dto.setBusinessCompanyId(entity.getBusinessCompany().getId());

		if (entity.getBusinessSubdiv() != null) {
			dto.setBusinessSubdivId(entity.getBusinessSubdiv().getId());
			dto.setBusinessCompanyId(entity.getBusinessSubdiv().getComp().getId());
		}

		if (entity.getAliasGroup() != null) {
			dto.setAliasGroupid(entity.getAliasGroup().getId());
		}

		return dto;
	}

}
