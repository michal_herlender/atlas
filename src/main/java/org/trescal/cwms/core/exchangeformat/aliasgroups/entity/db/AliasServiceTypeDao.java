package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasServiceType;

public interface AliasServiceTypeDao extends BaseDao<AliasServiceType, Integer>  {

}
