package org.trescal.cwms.core.exchangeformat.dto.form.validator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.exchangeformat.dto.form.ExchangeFormatForm;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Service
public class ExchangeFormatFormValidator extends AbstractBeanValidator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ExchangeFormatForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ExchangeFormatForm eff = (ExchangeFormatForm) target;
		super.validate(target, errors);

		// remove deleted items
		if (eff.getExchangeFormatFieldNameDetailsForm() != null)
			eff.getExchangeFormatFieldNameDetailsForm().removeIf(e -> e.getPosition() == null);
		/* General validation */
		if (!CollectionUtils.isNotEmpty(eff.getExchangeFormatFieldNameDetailsForm())) {
			errors.reject("exchangeformat.emptyfieldlist", "Please add fields !");
			return;
		} else {
			// plantId or SerialNo or PlantNo should be in the list
			boolean plantIdOrSerialNoOrPlantNo = eff.getExchangeFormatFieldNameDetailsForm().stream().anyMatch(efdf -> {
				return efdf.getFieldName().equals(ExchangeFormatFieldNameEnum.ID_TRESCAL)
						|| efdf.getFieldName().equals(ExchangeFormatFieldNameEnum.SERIAL_NUMBER)
						|| efdf.getFieldName().equals(ExchangeFormatFieldNameEnum.PLANT_NO);
			});
			if (!plantIdOrSerialNoOrPlantNo) {
				errors.reject("exchangeformat.instrumentidfield",
						"Please select at least Trescal ID or Serial No or Plant No");
			}
		}

		/* depending on creation types */
		if (eff.isFromBusinessCompany())
			super.validate(target, errors, ExchangeFormatForm.FromBusinessCompany.class);
		else if (eff.isFromBusinessSubdiv())
			super.validate(target, errors, ExchangeFormatForm.FromBusinessSubdiv.class);
		else if (eff.isFromClient()) {
			super.validate(target, errors, ExchangeFormatForm.FromClientCompanyOrSubdiv.class);

			if (eff.getCompanySelected() == null && eff.getType() != ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP) {
				errors.reject("exchangeformat.businessCompanyAndSubdivNull",
						"Please choose either a business subdiv or a company");
			}
			if (eff.isFromClient() && eff.getClientCompanyId() == null
					&& eff.getType() != ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP) {
				errors.reject("exchangeformat.clientcompanyNull", "clientCompanyId : may not be null");
			}

			if (eff.isFromBusinessCompany() && eff.getBusinessCompanyId() == null
					&& eff.getType() != ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP) {
				errors.reject("exchangeformat.buscompanyNull", "businessCompanyId : may not be null");
			}
			if (eff.isFromBusinessSubdiv() && eff.getBusinessSubdivId() == null
					&& eff.getType() != ExchangeFormatTypeEnum.PREBOOKING_FROM_MOBILEAPP) {
				errors.reject("exchangeformat.busSubdivNull", "businessSubdivId : may not be null");
			}

		}

		// validate mandatory fields for exchange format types
		if (eff.getType().equals(ExchangeFormatTypeEnum.OPERATIONS_IMPORTATION)) {

			List<ExchangeFormatFieldNameEnum> mandatoryFields = Arrays.asList(ExchangeFormatFieldNameEnum.PLANT_NO,
					ExchangeFormatFieldNameEnum.OPERATION_DATE, ExchangeFormatFieldNameEnum.OPERATION_START_DATE,
					ExchangeFormatFieldNameEnum.OPERATION_TYPE, ExchangeFormatFieldNameEnum.OPERATION_DURATION,
					ExchangeFormatFieldNameEnum.OPERATION_BY_HRID, ExchangeFormatFieldNameEnum.SERVICETYPE_ID,
					ExchangeFormatFieldNameEnum.JOBITEM_EQUIVALENT_ID, ExchangeFormatFieldNameEnum.CAL_FREQUENCY,
					ExchangeFormatFieldNameEnum.INTERVAL_UNIT, ExchangeFormatFieldNameEnum.NEXT_CAL_DUEDATE,
					ExchangeFormatFieldNameEnum.CALIBRATION_OUTCOME, ExchangeFormatFieldNameEnum.CALIBRATION_CLASS,
					ExchangeFormatFieldNameEnum.CALIBRATION_VERIFICATION_STATUS, ExchangeFormatFieldNameEnum.ADJUSTMENT,
					ExchangeFormatFieldNameEnum.REPAIR, ExchangeFormatFieldNameEnum.OPTIMIZATION,
					ExchangeFormatFieldNameEnum.RESTRICTION);

			// TODO: validate mandatory fields also

			List<ExchangeFormatFieldNameEnum> currentFields = eff.getExchangeFormatFieldNameDetailsForm().stream()
					.map(effndf -> {
						return effndf.getFieldName();
					}).collect(Collectors.toList());

			List<String> missingFields = mandatoryFields.stream().filter(mf -> {
				return !currentFields.contains(mf);
			}).map(mf -> {
				return mf.getValue();
			}).collect(Collectors.toList());

			if (!missingFields.isEmpty()) {
				errors.reject("exchangeformat.errors.missingmandatoryfields",
						new String[] { String.join(",", missingFields) }, "The following fields are missing");
			}

		} else if (eff.getType().equals(ExchangeFormatTypeEnum.INSTRUMENTS_IMPORTATION)) {

			List<ExchangeFormatFieldNameEnum> mandatoryFields = Arrays.asList(ExchangeFormatFieldNameEnum.CAL_FREQUENCY,
					ExchangeFormatFieldNameEnum.INTERVAL_UNIT);
			List<ExchangeFormatFieldNameEnum> currentFields = eff.getExchangeFormatFieldNameDetailsForm().stream()
					.map(effndf -> {
						return effndf.getFieldName();
					}).collect(Collectors.toList());

			List<String> missingFields = mandatoryFields.stream().filter(mf -> {
				return !currentFields.contains(mf);
			}).map(mf -> {
				return mf.getValue();
			}).collect(Collectors.toList());

			if (!missingFields.isEmpty()) {
				errors.reject("exchangeformat.errors.missingmandatoryfields",
						new String[] { String.join(",", missingFields) }, "The following fields are missing");
			}

			// test if there is one of these two fields :
			// TML_INSTRUMENT_MODEL_ID or INSTRUMENT_MODEL_ID
			boolean hasTMLModelIdOrModelId = eff.getExchangeFormatFieldNameDetailsForm().stream().anyMatch(effndf -> {
				return effndf.getFieldName().equals(ExchangeFormatFieldNameEnum.TML_INSTRUMENT_MODEL_ID)
						|| effndf.getFieldName().equals(ExchangeFormatFieldNameEnum.INSTRUMENT_MODEL_ID);
			});

			if (!hasTMLModelIdOrModelId) {
				errors.reject("exchangeformat.tmlmodelidormodelidismissing",
						"Please Add either 'TML Instrument Model id' or 'Instrument Model id'");
			}

		} else if (eff.getType().equals(ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION)) {

			List<ExchangeFormatFieldNameEnum> mandatoryFields = Arrays.asList(ExchangeFormatFieldNameEnum.PLANT_NO,
					ExchangeFormatFieldNameEnum.EXPECTED_SERVICE, ExchangeFormatFieldNameEnum.FINAL_PRICE);

			List<ExchangeFormatFieldNameEnum> currentFields = eff.getExchangeFormatFieldNameDetailsForm().stream()
					.map(effndf -> {
						return effndf.getFieldName();
					}).collect(Collectors.toList());

			List<String> missingFields = mandatoryFields.stream().filter(mf -> {
				return !currentFields.contains(mf);
			}).map(mf -> {
				return mf.getValue();
			}).collect(Collectors.toList());

			if (!missingFields.isEmpty()) {
				errors.reject("exchangeformat.errors.missingmandatoryfields",
						new String[] { String.join(",", missingFields) }, "The following fields are missing");
			}
		}

	}

}
