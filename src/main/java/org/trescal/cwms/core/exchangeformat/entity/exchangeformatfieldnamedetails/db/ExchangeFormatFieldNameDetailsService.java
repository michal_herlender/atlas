package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;

public interface ExchangeFormatFieldNameDetailsService  extends BaseService<ExchangeFormatFieldNameDetails, Integer> {

	List<ExchangeFormatFieldNameDetails> getByExchangeFormat(Integer efId);

}
