package org.trescal.cwms.core.exchangeformat.enums;

import java.util.Comparator;

import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;

public class ExchangeFormatFieldNameDetailsComparator implements Comparator<ExchangeFormatFieldNameDetails> {

	@Override
	public int compare(ExchangeFormatFieldNameDetails arg0, ExchangeFormatFieldNameDetails arg1) {

		return Integer.compare(arg0.getPosition(), arg1.getPosition());
	}

}
