package org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup_;

@Repository("AliasGroupDao")
public class AliasGroupDaoImpl extends BaseDaoImpl<AliasGroup, Integer> implements AliasGroupDao {

	@Override
	protected Class<AliasGroup> getEntity() {
		return AliasGroup.class;
	}

	@Override
	public AliasGroup checkIfDefaultAliasGroupIsAlreadyDefined(Company businessCompany) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AliasGroup> cq = cb.createQuery(AliasGroup.class);
		Root<AliasGroup> aliasGroupRoot = cq.from(AliasGroup.class);

		Predicate clauses = cb.conjunction();
		Predicate defBuisComp = cb.equal(aliasGroupRoot.get(AliasGroup_.organisation), businessCompany);
		Predicate isDefaultForAllocatedCompany = cb.equal(aliasGroupRoot.get(AliasGroup_.defaultBusinessCompany), true);
		
		clauses.getExpressions().add(defBuisComp);
		clauses.getExpressions().add(isDefaultForAllocatedCompany);
		
		cq.select(aliasGroupRoot);
		cq.where(clauses);

		TypedQuery<AliasGroup> query = getEntityManager().createQuery(cq);
		if (query.getResultList().size() > 0)
			return query.getResultList().get(0);
		else
			return null;

	}

	@Override
	public List<AliasGroup> getAliasGroupsForAllocatedCompany(Company AllocatedCompany) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AliasGroup> cq = cb.createQuery(AliasGroup.class);
		Root<AliasGroup> aliasGroupRoot = cq.from(AliasGroup.class);

		Predicate clauses = cb.conjunction();
		Predicate defBuisComp = cb.equal(aliasGroupRoot.get(AliasGroup_.organisation), AllocatedCompany);
		clauses.getExpressions().add(defBuisComp);

		cq.select(aliasGroupRoot);
		cq.where(clauses);
		TypedQuery<AliasGroup> query = getEntityManager().createQuery(cq);

		return query.getResultList();

	}

}
