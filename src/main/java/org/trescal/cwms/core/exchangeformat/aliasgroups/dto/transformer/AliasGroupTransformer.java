package org.trescal.cwms.core.exchangeformat.aliasgroups.dto.transformer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.AliasDetailDTO;
import org.trescal.cwms.core.exchangeformat.aliasgroups.dto.AliasGroupDTO;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasIntervalUnit;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.alias.AliasServiceType;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.EntityDtoTransformer;

@Service
public class AliasGroupTransformer implements EntityDtoTransformer<AliasGroup, AliasGroupDTO> {

	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private AliasGroupService aliasGroupService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService contactService;

	@Override
	public AliasGroup transform(AliasGroupDTO dto) {
		AliasGroup entity;
		if (dto.getAlaisGroupId() == null) {
			entity = new AliasGroup();
		} else {
			entity = this.aliasGroupService.get(dto.getAlaisGroupId());
		}
		if (entity.getServiceTypeAliases() == null) {
			entity.setServiceTypeAliases(new ArrayList<AliasServiceType>());
		}
		if (entity.getIntervalUnitAliases() == null) {
			entity.setIntervalUnitAliases(new ArrayList<AliasIntervalUnit>());
		}

		entity.setName(dto.getAliasGroupName());
		entity.setDescription(dto.getAliasGroupDesc());

		entity.setCreatedOn(dto.getCreatedOn());
		if (dto.getCreatedById() != null) {
			entity.setCreatedBy(this.contactService.get(dto.getCreatedById()));
		}

		entity.setOrganisation(this.companyService.get(dto.getAllocatedCompanyid()));

		if (dto.isDefaultForCurrentBusinessComp() == true) {
			entity.setDefaultBusinessCompany(true);
		} else {
			entity.setDefaultBusinessCompany(false);
		}

		for (AliasDetailDTO aliasDto : dto.getAliasesForm()) {
			if (aliasDto.getEffn().equals(ExchangeFormatFieldNameEnum.EXPECTED_SERVICE)) {
				AliasServiceType aliasServiceType;
				if (aliasDto.getAliasId() == null) {
					aliasServiceType = new AliasServiceType();
				} else {
					aliasServiceType = entity.getServiceTypeAliases().stream()
							.filter(ast -> ast.getId().equals(aliasDto.getAliasId())).findFirst().orElse(null);
				}
				aliasServiceType.setAlias(aliasDto.getAlias());
				aliasServiceType.setValue(this.serviceTypeService.get(Integer.parseInt(aliasDto.getValue())));
				aliasServiceType.setAliasGroup(entity);

				if (aliasDto.getAliasId() == null) {
					entity.getServiceTypeAliases().add(aliasServiceType);
				}
			} else if (aliasDto.getEffn().equals(ExchangeFormatFieldNameEnum.INTERVAL_UNIT)) {
				AliasIntervalUnit aliasIntervalUnit;
				if (aliasDto.getAliasId() == null) {
					aliasIntervalUnit = new AliasIntervalUnit();
				} else {
					aliasIntervalUnit = entity.getIntervalUnitAliases().stream()
							.filter(aiu -> aiu.getId().equals(aliasDto.getAliasId())).findFirst().orElse(null);
				}
				aliasIntervalUnit.setAlias(aliasDto.getAlias());
				aliasIntervalUnit.setValue(IntervalUnit.valueOf(aliasDto.getValue()));
				aliasIntervalUnit.setAliasGroup(entity);
				if (aliasDto.getAliasId() == null) {
					entity.getIntervalUnitAliases().add(aliasIntervalUnit);
				}
			}

		}
		// delete deleted aliases
		List<Integer> intervalUnitIds = dto.getAliasesForm().stream().filter(
				aiu -> aiu.getEffn().equals(ExchangeFormatFieldNameEnum.INTERVAL_UNIT) && aiu.getAliasId() != null)
				.map(aiu -> aiu.getAliasId()).collect(Collectors.toList());

		List<Integer> serviceTypeIds = dto.getAliasesForm().stream().filter(
				ast -> ast.getEffn().equals(ExchangeFormatFieldNameEnum.EXPECTED_SERVICE) && ast.getAliasId() != null)
				.map(ast -> ast.getAliasId()).collect(Collectors.toList());

		Iterator<AliasIntervalUnit> iUIterator = entity.getIntervalUnitAliases().iterator();
		while (iUIterator.hasNext()) {
			AliasIntervalUnit aiu = iUIterator.next();
			if (aiu.getId() != null && !intervalUnitIds.contains(aiu.getId()))
				iUIterator.remove();
		}

		Iterator<AliasServiceType> sTypeIterator = entity.getServiceTypeAliases().iterator();
		while (sTypeIterator.hasNext()) {
			AliasServiceType ast = sTypeIterator.next();
			if (ast.getId() != null && !serviceTypeIds.contains(ast.getId()))
				sTypeIterator.remove();
		}

		return entity;
	}

	@Override
	public AliasGroupDTO transform(AliasGroup entity) {
		AliasGroupDTO dto = new AliasGroupDTO();
		List<AliasDetailDTO> aliases = new ArrayList<AliasDetailDTO>();

		dto.setAlaisGroupId(entity.getId());
		dto.setAliasGroupName(entity.getName());
		dto.setAliasGroupDesc(entity.getDescription());
		dto.setCreatedOn(entity.getCreatedOn());
		if (entity.getCreatedBy() != null) {
			dto.setCreatedById(entity.getCreatedBy().getId());
		}
		dto.setAllocatedCompanyid(entity.getOrganisation().getId());
		dto.setAllocatedCompanyName(entity.getOrganisation().getConame());

		if (entity.isDefaultBusinessCompany() == true) {
			dto.setDefaultForCurrentBusinessComp(true);
		} else {
			dto.setDefaultForCurrentBusinessComp(false);
		}

		for (AliasServiceType aliasServiceType : entity.getServiceTypeAliases()) {
			AliasDetailDTO aliasDetailsDTO = new AliasDetailDTO();
			aliasDetailsDTO.setAliasId(aliasServiceType.getId());
			aliasDetailsDTO.setAlias(aliasServiceType.getAlias());
			aliasDetailsDTO.setEffn(ExchangeFormatFieldNameEnum.EXPECTED_SERVICE);
			aliasDetailsDTO.setServiceType(aliasServiceType.getValue());
			aliasDetailsDTO.setValue(Integer.toString(aliasServiceType.getValue().getServiceTypeId()));

			aliases.add(aliasDetailsDTO);
		}

		for (AliasIntervalUnit aliasIntervalUnit : entity.getIntervalUnitAliases()) {
			AliasDetailDTO aliasDetailsDTO = new AliasDetailDTO();
			aliasDetailsDTO.setAliasId(aliasIntervalUnit.getId());
			aliasDetailsDTO.setAlias(aliasIntervalUnit.getAlias());
			aliasDetailsDTO.setEffn(ExchangeFormatFieldNameEnum.INTERVAL_UNIT);
			aliasDetailsDTO.setValue(aliasIntervalUnit.getValue().toString());
			aliasDetailsDTO.setIntervalUnit(aliasIntervalUnit.getValue());

			aliases.add(aliasDetailsDTO);
		}

		dto.setAliasesForm(aliases);

		return dto;
	}

}
