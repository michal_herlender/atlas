package org.trescal.cwms.core.exchangeformat.enums;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum ExchangeFormatTypeEnum {
	PREBOOKING("exchangeformat.type.prebooking"),
	OPERATIONS_IMPORTATION("exchangeformat.type.operationsimportation"),
	INSTRUMENTS_IMPORTATION("exchangeformat.type.instrumentsimportation"),
	PREBOOKING_FROM_MOBILEAPP("exchangeformat.type.prebookingmobileapp"),
	QUOTATION_ITEMS_IMPORTATION("exchangeformat.type.quotationitemsimportation");
	
	private ReloadableResourceBundleMessageSource messages;
	private String messageCode;
	
	private ExchangeFormatTypeEnum(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getValue() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name(), locale);
		}
		return this.toString();
	}

	public String getName() {
		return this.name();
	}
	
	@Component
	public static class ExchangeFormatFieldNameMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (ExchangeFormatTypeEnum effn : EnumSet.allOf(ExchangeFormatTypeEnum.class))
            	effn.setMessageSource(messages);
        }
	}

	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
}
