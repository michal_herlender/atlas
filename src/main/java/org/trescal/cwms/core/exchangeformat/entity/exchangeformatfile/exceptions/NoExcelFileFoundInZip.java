package org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.exceptions;

public class NoExcelFileFoundInZip extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoExcelFileFoundInZip() {
		super("No Excel file found inside zip, One Excel file is expected !");
	}

}
