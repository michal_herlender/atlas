package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemAtFirstLocation implements LookupResultMessage
{
	YES("lookupresultmessage.itematfirstlocation.yes"),
	NO("lookupresultmessage.itematfirstlocation.no");
	
	private String messageCode;
	
	private Lookup_ItemAtFirstLocation(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}