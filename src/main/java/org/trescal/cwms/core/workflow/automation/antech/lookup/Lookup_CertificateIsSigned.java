package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_CertificateIsSigned implements LookupResultMessage
{
	SIGNED("lookupresultmessage.certificateissigned.signed"),
	NOT_SIGNED("lookupresultmessage.certificateissigned.notsigned");
	
	private String messageCode;
	
	private Lookup_CertificateIsSigned(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}