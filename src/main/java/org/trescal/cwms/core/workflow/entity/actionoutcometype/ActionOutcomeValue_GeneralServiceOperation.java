package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_GeneralServiceOperation implements ActionOutcomeValue {
	COMPLETED_SUCCESSFULLY(
			"Completed - Successfully",
			"actionoutcome.generalserviceoperation.completedsuccessfully"),
	COMPLETED_UNSUCCESSFULLY(
			"Completed - Unsuccessfully",
			"actionoutcome.generalserviceoperation.completedunsuccessfully"),
	FAILED_REDO(
			"Failed Redo",
			"actionoutcome.generalserviceoperation.failedredo"),
	ON_HOLD(
			"On-Hold",
			"actionoutcome.generalserviceoperation.onhold");

	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_GeneralServiceOperation(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.GENERAL_SERVICE_OPERATION;
	}

}
