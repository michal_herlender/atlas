package org.trescal.cwms.core.workflow.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.misc.entity.hook.db.HookService;
import org.trescal.cwms.core.misc.entity.hookactivity.db.HookActivityService;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupinstance.db.LookupInstanceService;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;

@Controller @IntranetController
public class WorkflowEditorController
{
	@Autowired
	private HookService hookService;
	@Autowired
	private HookActivityService hookActivityService;
	@Autowired
	private ItemStateService stateServ;
	@Autowired
	private LookupInstanceService lookupInstanceService;
	
	@ModelAttribute("lastActivity")
	protected ItemActivity populateLastActivity(@RequestParam(value="lastActivityId", required=false) Integer activityId) {
		if (activityId == null) return null;
		return this.stateServ.findItemActivity(activityId);
	}

	@ModelAttribute("lastLookup")
	protected LookupInstance populateLastLookup(@RequestParam(value="lastLookupId", required=false) Integer lookupId) {
		if (lookupId == null) return null;
		return this.lookupInstanceService.get(lookupId);
	}
	
	@ModelAttribute("lastStatus")
	protected ItemStatus populateLastStatus(@RequestParam(value="lastStatusId", required=false) Integer statusId) {
		if (statusId == null) return null;
		return this.stateServ.findItemStatus(statusId);
	}

	@ModelAttribute("lastHook")
	protected Hook populateLastHook(@RequestParam(value="lastHookId", required=false) Integer hookId) {
		if (hookId == null) return null;
		return this.hookService.findHook(hookId);
	}
	
	@RequestMapping(value="/workfloweditor_activity.htm")
	protected ModelAndView getActivity(@RequestParam(value="activityId") int activityId, Model model) {
		ItemActivity activity = this.stateServ.findItemActivity(activityId);
		model.addAttribute("startActivity", activity);
		return new ModelAndView("trescal/core/workflow/workfloweditor_activity", "command", model);
	}
	
	@RequestMapping(value="/workfloweditor_status.htm")
	protected ModelAndView getStatus(@RequestParam(value="statusId") int statusId, Model model) {
		ItemStatus status = this.stateServ.findItemStatus(statusId);
		model.addAttribute("startStatus", status);
		return new ModelAndView("trescal/core/workflow/workfloweditor_status", "command", model);
	}
	
	@RequestMapping(value="/workfloweditor_lookup.htm")
	protected ModelAndView getLookupInstance(@RequestParam(value="lookupId") int lookupId, Model model) {
		LookupInstance lookupInstance = this.lookupInstanceService.get(lookupId);
		model.addAttribute("startLookupInstance", lookupInstance);
		model.addAttribute("lookupResultMessages", lookupInstance.getLookupFunction().getLookupResultMessages());
		model.addAttribute("mapLookupResults", createLookupResultMap(lookupInstance));
		
		return new ModelAndView("trescal/core/workflow/workfloweditor_lookup", "command", model);
	}
	
	@RequestMapping(value="/workfloweditor_hook.htm")
	protected ModelAndView getHookInstance(@RequestParam(value="hookId") int hookId, Model model) {
		Hook hook = this.hookService.findHook(hookId);
		model.addAttribute("startHook", hook);
		model.addAttribute("hookActivities", hookActivityService.findForHook(hookId));
		
		return new ModelAndView("trescal/core/workflow/workfloweditor_hook", "command", model);
	}
	
	/*
	 * Creates a map of defined LookupResult messages
	 * 0 - LookupResult
	 * 1 - LookupResult
	 * Note that if workflow is not configured correctly, a result message may exist in LookupFunction but no value 
	 * could be mapped here, it is intended to show as an error in the user interface.
	 */
	private Map<LookupResultMessage, LookupResult> createLookupResultMap(LookupInstance lookupInstance) {
		Map<LookupResultMessage, LookupResult> map = new TreeMap<>();
		// Step 1 - add all defined LookupResult instances to map
		for (LookupResult lookupResult : lookupInstance.getLookupResults()) {
			map.put(lookupResult.getResultMessage(), lookupResult);
		}
		return map;
	}
	
	@RequestMapping(value="/workfloweditor.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception
	{
		HashMap<String, Object> refData = new HashMap<String, Object>();
		refData.put("lookups", this.lookupInstanceService.getAll());
		refData.put("holdStatuses", this.stateServ.getAllItemStatuses(HoldStatus.class, false));
		refData.put("workStatuses", this.stateServ.getAllItemStatuses(WorkStatus.class, false));
		refData.put("activities", this.stateServ.getAllItemActivities());
		refData.put("hooks", this.hookService.getAllHooks());
		return new ModelAndView("trescal/core/workflow/workfloweditor", "command", refData);
	}
}