package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemAtTP implements LookupResultMessage
{
	YES("lookupresultmessage.itemattp.yes"),
	NO("lookupresultmessage.itemattp.no");
	
	private String messageCode;
	
	private Lookup_ItemAtTP(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}