package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtInspectionLocation;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemAtInspectionLocation")
public class Lookup_ItemAtInspectionLocationService extends Lookup {

	@Value("#{props['cwms.config.address.base_address_id']}")
	private int baseAddrId;

	@Override
	public String getDescription() {
		return "Determines whether the item is at the base address required for inspections.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM IS AT INSPECTION LOCATION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = ji.getCurrentAddr() == null || ji.getCurrentAddr().getAddrid() == this.baseAddrId
				? Lookup_ItemAtInspectionLocation.YES
				: Lookup_ItemAtInspectionLocation.NO;
		return result;
	}

	/*
	 * TODO works not with multiple subdivs - problem with enum LookupFunction too
	 */
	public void setBaseAddrId(int baseAddrId) {
		this.baseAddrId = baseAddrId;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemAtInspectionLocation.values();
	}
}