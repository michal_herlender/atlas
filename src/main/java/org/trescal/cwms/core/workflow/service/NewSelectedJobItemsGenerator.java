package org.trescal.cwms.core.workflow.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.company.projection.comparator.SubdivProjectionDTOComparator;
import org.trescal.cwms.core.jobs.job.projection.comparator.JobProjectionDTODueDateComparator;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand.*;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionResult;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.*;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionResult;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionService;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTOComparator;
import org.trescal.cwms.core.procedure.dto.LaboratoryCategorySplit;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.util.*;
import java.util.stream.Collectors;

@Service @Slf4j
public class NewSelectedJobItemsGenerator {
	@Autowired
	private JobProjectionService jobProjectionService;
	@Autowired
	private JobItemProjectionService jobItemProjectionService;
	@Autowired
	private NewSelectedJobItemsDao baseDao;

	public NewSelectedJobItemsModel getGroupedJobItems(List<Integer> procIds,
													   List<Integer> stateIds, Integer days, Boolean includeBusiness, StateGroupLinkType sglType,
													   Integer addressId, Integer allocatedSubdivId, Integer allocatedCompanyId, LaboratoryCategorySplit laboratoryCategorySplit
			, Locale locale) {
		NewSelectedJobItemsModel model = new NewSelectedJobItemsModel();

		log.debug("allocatedSubdivId = " + allocatedSubdivId);
		log.debug("allocatedCompanyId = " + allocatedCompanyId);

		// Retrieves both job items and instruments in Dao method
		List<JobItemProjectionDTO> jobItemDtos = this.baseDao.getActiveJobItemsByDaysAllocationsAndBusiness(procIds, stateIds, days,
				includeBusiness, allocatedSubdivId, sglType, addressId, laboratoryCategorySplit, locale);
		log.debug("jobItemDtos.size() = " + jobItemDtos.size());

		// Further (slight?) optimization possible in that contract / on-behalf-of just needed for export view 
		JobItemProjectionCommand jiCommand = new JobItemProjectionCommand(locale, allocatedCompanyId, JI_LOAD_SERVICE_TYPES.class,
				JI_LOAD_ITEM_STATES.class, JI_LOAD_CAPABILITIES.class, JI_LOAD_CONTRACT_REVIEWERS.class, JI_LOAD_CONTRACT.class, JI_LOAD_ON_BEHALF.class);

		JobItemProjectionResult jiResult = this.jobItemProjectionService.loadJobItemProjections(jobItemDtos, jiCommand);

		// TODO is loading projections for over 2000 jobs required/anticipated?
		JobProjectionCommand jobCommand = new JobProjectionCommand(locale, allocatedCompanyId, JOB_LOAD_CONTACT.class, 
				JOB_LOAD_STATUS.class, JOB_LOAD_TRANSPORT_OUT.class, JOB_LOAD_ORGANISATION.class, JOB_COUNT_ITEMS.class,JOB_EXPENSE_ITEMS.class);
		JobProjectionResult jobResult = this.jobProjectionService.getDTOsByJobItems(jobItemDtos, jobCommand);
		
		Collections.sort(jobResult.getJobDtos(), new JobProjectionDTODueDateComparator());

		model.setJobDtos(jobResult.getJobDtos());
		model.setJobItemCount(jobItemDtos.size());
		
		// TODO aggregate filtering projections into model
		model.setBusinessSubdivs(getBusinessSubdivs(jobResult));
		model.setClientCompanies(getClientCompanies(jobResult));
		model.setClientSubdivs(getClientSubdivs(jobResult));
		model.setContractReviewers(getContractReviewers(jiResult));
		model.setItemStates(getItemStates(jiResult));
		model.setCapabilities(getCapabilities(jiResult));
		model.setTransportOptions(getTransportOptions(jobResult));
		model.setServiceTypes(getServiceTypes(jiResult));
		
		return model;
	}	
	
	private Collection<KeyValueIntegerString> getBusinessSubdivs(JobProjectionResult jobResult) {
		Set<KeyValueIntegerString> businessSubdivs = new TreeSet<>();
		for (SubdivProjectionDTO subdivDto : jobResult.getBusinessSubdivDtos()) {
			// TODO - well formatted identification like FR-005-AIX (right now just subdiv name - was code)
			businessSubdivs.add(new KeyValueIntegerString(subdivDto.getSubdivid(), subdivDto.getSubname()));
		}
		return businessSubdivs;
	}
	
	private Collection<KeyValueIntegerString> getClientCompanies(JobProjectionResult jobResult) {
		Set<KeyValueIntegerString> clientCompanies = 
			jobResult.getContactDtos().stream()
				.map(contactDto -> contactDto.getSubdiv())
				.distinct()
				.map(subdivDto -> subdivDto.getCompany())
				.distinct()
				.map(companyDto -> new KeyValueIntegerString(companyDto.getCoid(), companyDto.getConame()))
				.collect(Collectors.toCollection(TreeSet::new));
		
		return clientCompanies;
	}
	
	private MultiValuedMap<Integer, SubdivProjectionDTO> getClientSubdivs(JobProjectionResult jobResult) {
		MultiValuedMap<Integer, SubdivProjectionDTO> mapSubdivsByCompanyId = new ArrayListValuedHashMap<>();
		
		SortedSet<SubdivProjectionDTO> clientSubdivs = new TreeSet<>(new SubdivProjectionDTOComparator());
		jobResult.getContactDtos().stream()
				.map(contactDto -> contactDto.getSubdiv())
				.forEach(subdivDto -> clientSubdivs.add(subdivDto));
		
		for (SubdivProjectionDTO subdivDto : clientSubdivs) {
			mapSubdivsByCompanyId.put(subdivDto.getCompany().getCoid(), subdivDto);
		}
		
		return mapSubdivsByCompanyId;
	}
	
	private Collection<KeyValueIntegerString> getContractReviewers(JobItemProjectionResult jobItemResult) {
		Set<KeyValueIntegerString> contractReviewers = 
			jobItemResult.getContractReviewers().stream()
				.map(contactDto -> new KeyValueIntegerString(contactDto.getPersonid(), contactDto.getName()))
				.collect(Collectors.toCollection(TreeSet::new));
		return contractReviewers; 
	}
	
	private Collection<KeyValueIntegerString> getItemStates(JobItemProjectionResult jobItemResult) {
		// Unsorted in jobItemResult
		Set<KeyValueIntegerString> itemStates = new TreeSet<>();
		itemStates.addAll(jobItemResult.getItemStates());
		return itemStates;
	}
	
	private Collection<CapabilityProjectionDTO> getCapabilities(JobItemProjectionResult jobItemResult) {
		// Unsorted in jobItemResult
		Set<CapabilityProjectionDTO> capabilities = new TreeSet<>(new CapabilityProjectionDTOComparator());
		capabilities.addAll(jobItemResult.getCapabilities());
		return capabilities;
	}
	
	private Collection<KeyValueIntegerString> getTransportOptions(JobProjectionResult jobResult) {
		// Already sorted in underlying service
		return jobResult.getTransportOutDtos();
	}
	
	private Collection<KeyValueIntegerString> getServiceTypes(JobItemProjectionResult jobItemResult) {
		Set<KeyValueIntegerString> serviceTypes = new TreeSet<>();
		serviceTypes.addAll(jobItemResult.getServiceTypes());
		return serviceTypes;
	}
}