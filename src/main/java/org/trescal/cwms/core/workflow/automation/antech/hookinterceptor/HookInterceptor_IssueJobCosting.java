package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

/*
 * Updated 2016-10-13 (GB) to initialize all HookInterceptors before doing updates.
 * OpenActivityException may be thrown (will now be propagated to caller as @Transational
 * annotation removed, and also removed AOP advice around HookInterceptors), caller would
 * previously get an UnexpectedRollbackException instead.
 */

@Service
public class HookInterceptor_IssueJobCosting extends HookInterceptor<HookInterceptor_IssueJobCosting.Input> {
	public static final String HOOK_NAME = "issue-job-costing";

	@Autowired
	private MessageSource messageSource;

	public static class Input {

		public JobCosting jobCosting;
		public String fileName;

		public Input(JobCosting jobCosting, String fileName) {
			this.jobCosting = jobCosting;
			this.fileName = fileName;
		}
	}

	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: ISSUE JOB COSTING");
			Set<JobCostingItem> items = input.jobCosting.getItems();
			Contact con = this.sessionServ.getCurrentContact();

			Map<JobItem, HookInterceptorContext> contexts = new HashMap<>();

			for (JobCostingItem jci : items) {
				JobItem ji = jci.getJobItem();
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);
				contexts.put(ji, context);
			}
			for (JobCostingItem jci : items) {
				JobItem ji = jci.getJobItem();
				HookInterceptorContext context = contexts.get(ji);

				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				jia.setStartStamp(new Date());
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());
				Locale messageLocale = ji.getJob().getOrganisation().getComp().getDocumentLanguage() != null ? 
						ji.getJob().getOrganisation().getComp().getDocumentLanguage():con.getLocale();
				String remark = this.messageSource.getMessage("docs.jobcosting", null, "Job Costing : ",
						messageLocale) + " : "
						+ input.jobCosting.getIdentifier();
						
				
				if (input.fileName != null && !input.fileName.isEmpty())
					remark = remark.concat("<br/>"
							+ this.messageSource.getMessage("costing.file", null, "Costing File",
									messageLocale)
							+ " : " + input.fileName);
				//set link to jobcosting
				remark = remark.concat("&nbsp;<a class='mainlink' href='viewjobcosting.htm?id=" + input.jobCosting.getId() + "'>" +
								"<img src='img/icons/link.png' width='16' height='16' title='' />" +
							"</a>");
				jia.setRemark(remark);

				if (context.isCompleteActivity()) {
					jia.setEndStamp(new Date());
					jia.setTimeSpent(5);
					jia.setCompletedBy(con);
				}

				ItemStatus endStatus = super.getNextStatus(context);

				ji.setState(endStatus);
				jobItemService.updateJobItem(ji);
				if (context.isCompleteActivity()) {
					jia.setEndStatus(endStatus);
				}

				this.persist(jia, endStatus, context);
			}
		}
	}
}