package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Repair;

@Entity
@DiscriminatorValue("repair")
public class ActionOutcome_Repair extends ActionOutcome {
	private ActionOutcomeValue_Repair value;
	
	public ActionOutcome_Repair() {
		this.type = ActionOutcomeType.REPAIR;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_Repair getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_Repair value) {
		this.value = value;
	}

}
