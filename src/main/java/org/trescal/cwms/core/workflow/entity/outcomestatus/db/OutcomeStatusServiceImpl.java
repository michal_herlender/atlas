package org.trescal.cwms.core.workflow.entity.outcomestatus.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

@Service("OutcomeStatusService")
public class OutcomeStatusServiceImpl implements OutcomeStatusService {
	@Autowired
	private OutcomeStatusDao outcomeStatusDao;

	public void deleteOutcomeStatus(OutcomeStatus outcomestatus) {
		this.outcomeStatusDao.remove(outcomestatus);
	}

	public OutcomeStatus findOutcomeStatus(int id) {
		return this.outcomeStatusDao.find(id);
	}

	public OutcomeStatus findOutcomeStatusWithActivityAndStatus(int activityId, int statusId) {
		return this.outcomeStatusDao.findOutcomeStatusWithActivityAndStatus(activityId, statusId);
	}

	public List<OutcomeStatus> getAllOutcomeStatuss() {
		return this.outcomeStatusDao.findAll();
	}

	public List<OutcomeStatus> getOutcomeStatusesForActivity(int activityId, boolean includeLookups) {
		return this.outcomeStatusDao.getOutcomeStatusesForActivity(activityId, includeLookups);
	}

	public void insertOutcomeStatus(OutcomeStatus OutcomeStatus) {
		this.outcomeStatusDao.persist(OutcomeStatus);
	}

	public void saveOrUpdateOutcomeStatus(OutcomeStatus outcomestatus) {
		this.outcomeStatusDao.saveOrUpdate(outcomestatus);
	}

	public void updateOutcomeStatus(OutcomeStatus OutcomeStatus) {
		this.outcomeStatusDao.update(OutcomeStatus);
	}
}