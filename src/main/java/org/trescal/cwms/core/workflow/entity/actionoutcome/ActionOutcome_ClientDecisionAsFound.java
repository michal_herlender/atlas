package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionAsFound;

@Entity
@DiscriminatorValue("client_decision_as_found")
public class ActionOutcome_ClientDecisionAsFound extends ActionOutcome {
	private ActionOutcomeValue_ClientDecisionAsFound value;
	
	public ActionOutcome_ClientDecisionAsFound() {
		this.type = ActionOutcomeType.CLIENT_DECISION_AS_FOUND;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_ClientDecisionAsFound getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_ClientDecisionAsFound value) {
		this.value = value;
	}

}
