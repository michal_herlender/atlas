package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemHasBeenContractReviewed;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemHasBeenContractReviewed")
public class Lookup_ItemHasBeenContractReviewedService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item has been contract reviewed or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM HAS BEEN CONTRACT REVIEWED");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = Lookup_ItemHasBeenContractReviewed.NO;
		for (JobItemAction action : ji.getActions()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;

				if (activity.getActivityDesc().equals("Contract review") && !activity.isDeleted()) {
					result = Lookup_ItemHasBeenContractReviewed.YES;
					break;
				}
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemHasBeenContractReviewed.values();
	}
}