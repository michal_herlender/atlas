package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_CompanyIsOnStop implements LookupResultMessage
{
	ONSTOP("lookupresultmessage.companyisonstop.onstop"),
	TRUSTED("lookupresultmessage.companyisonstop.trusted");
	
	private String messageCode;
	
	private Lookup_CompanyIsOnStop(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}