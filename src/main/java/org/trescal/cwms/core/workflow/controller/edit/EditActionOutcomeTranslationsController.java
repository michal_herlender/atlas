package org.trescal.cwms.core.workflow.controller.edit;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.form.edit.EditActionOutcomeTranslationsForm;
import org.trescal.cwms.core.workflow.form.genericentityvalidator.EditActionOutcomeValidator;
import org.trescal.cwms.core.workflow.service.EditWorkflowService;

@Controller @IntranetController
public class EditActionOutcomeTranslationsController {
	
	@Autowired
	private EditActionOutcomeValidator validator;
	@Autowired
	private EditWorkflowService editWorkflowService; 
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@InitBinder("form")
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute("form")
	public EditActionOutcomeTranslationsForm formBackingObject(@RequestParam(name="id", required=true) int activityId) {
		EditActionOutcomeTranslationsForm form = new EditActionOutcomeTranslationsForm();
		Map<Integer,Map<Locale,String>> map = new TreeMap<>();
		ItemActivity activity = this.itemStateService.findItemActivity(activityId);
		for (ActionOutcome ao : activity.getActionOutcomes()) {
			Map<Locale,String> translationMap = new HashMap<>();
			for (Translation t : ao.getTranslations()) {
				translationMap.put(t.getLocale(), t.getTranslation());
			}
			map.put(ao.getId(), translationMap);
		}
		form.setAoIdsToTranslations(map);
		return form;
	}
	
	@RequestMapping(value="editactionoutcomes.htm", method=RequestMethod.GET)
	public String showView(Model model, @RequestParam(name="id", required=true) int activityId) {
		model.addAttribute("supportedLocales", this.supportedLocaleService.getSupportedLocales());
		model.addAttribute("lastActivity", this.itemStateService.findItemActivity(activityId));
		return "trescal/core/workflow/edit/editactionoutcomes";
	}
	
	@RequestMapping(value="editactionoutcomes.htm", method=RequestMethod.POST)
	public String onSubmit(Model model, 
			@RequestParam(name="id", required=true) int activityId,
			@ModelAttribute("form") EditActionOutcomeTranslationsForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return showView(model, activityId);
		}
		editWorkflowService.updateActionOutcomeTranslations(form);
		return "redirect:workfloweditor_activity.htm?activityId="+activityId;
	}
	
}
