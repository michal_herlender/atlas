package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NeedCostingToClientForRepair;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_NeedCostingToClientForRepair")
public class Lookup_NeedCostingToClientForRepairService extends Lookup {

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Need Costing to Client For Repair");
		
		JobItem ji = jobItemService.findJobItem(jobItemId);
		WorkRequirement wr = ji.getNextWorkReq().getWorkRequirement();
		if(wr.getType().equals(WorkRequirementType.THIRD_PARTY))
			return Lookup_NeedCostingToClientForRepair.NO;
		return Lookup_NeedCostingToClientForRepair.YES;
	}

	@Override
	public String getDescription() {
		return "Decides whether to go to costing for repair job";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_NeedCostingToClientForRepair.values();
	}

}
