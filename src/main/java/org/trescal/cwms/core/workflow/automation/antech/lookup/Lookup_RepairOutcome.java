package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_RepairOutcome implements LookupResultMessage
{
	NO_FURTHER_ACTION("lookupresultmessage.repairoutcome.nofurtheraction"),
	CALIBRATION("lookupresultmessage.repairoutcome.calibration"),
	THIRD_PARTY_REPAIR("lookupresultmessage.repairoutcome.thirdpartyrepair"),
	UNIT_BER("lookupresultmessage.repairoutcome.unitber"),
	SPARE_PARTS("lookupresultmessage.repairoutcome.spareparts"),
	REPLACE_COMPONENTS("lookupresultmessage.repairoutcome.replacecomponents"),
	NOT_REPAIRED_NO_FURTHER_ACTION("lookupresultmessage.repairoutcome.notrepairednofurtheraction"),
	NOT_REPAIRED_FURTHER_CALIBRATION("lookupresultmessage.repairoutcome.notrepairedfurthercalibration");
	
	private String messageCode;
	
	private Lookup_RepairOutcome(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}