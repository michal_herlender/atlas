package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ApproveDeliveryNoteCreationByCSR;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_DNCreationApprovalRequired;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_DNCreationApprovalRequired")
public class Lookup_DNCreationApprovalRequiredService extends Lookup {

	@Autowired
	private ApproveDeliveryNoteCreationByCSR approveDeliveryNoteCreationByCSR;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {

		JobItem ji = jobItemService.findJobItem(jobItemId);
		Subdiv businessSubdiv = ji.getJob().getOrganisation();

		boolean requiresApproval = approveDeliveryNoteCreationByCSR.parseValueHierarchical(Scope.SUBDIV, businessSubdiv,
				businessSubdiv.getComp());

		if (requiresApproval)
			return Lookup_DNCreationApprovalRequired.YES;
		else
			return Lookup_DNCreationApprovalRequired.NO;
	}

	@Override
	public String getDescription() {
		return "Decides whether to add a step for approving the creation of the delivery note or not";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_DNCreationApprovalRequired.values();
	}

}
