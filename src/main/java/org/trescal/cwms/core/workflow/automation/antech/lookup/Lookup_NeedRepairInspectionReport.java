package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_NeedRepairInspectionReport implements LookupResultMessage {
	YES_TO_TECHNICIAN_PART("lookupresultmessage.needrepairinspectionreport.yestotechnicianpart"),
	NO("lookupresultmessage.needrepairinspectionreport.no"),
	YES_TO_MANAGER_PART("lookupresultmessage.needrepairinspectionreport.yestomanagerpart");

	private String messageCode;

	private Lookup_NeedRepairInspectionReport(String messageCode){
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}

}
