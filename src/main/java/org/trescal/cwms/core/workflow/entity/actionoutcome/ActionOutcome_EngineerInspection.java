package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_EngineerInspection;

@Entity
@DiscriminatorValue("engineer_inspection")
public class ActionOutcome_EngineerInspection extends ActionOutcome {
	private ActionOutcomeValue_EngineerInspection value;
	
	public ActionOutcome_EngineerInspection() {
		this.type = ActionOutcomeType.ENGINEER_INSPECTION;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_EngineerInspection getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_EngineerInspection value) {
		this.value = value;
	}

}
