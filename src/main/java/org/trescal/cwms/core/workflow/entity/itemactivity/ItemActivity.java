package org.trescal.cwms.core.workflow.entity.itemactivity;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import org.hibernate.annotations.SortComparator;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatusComparator;

@Entity
public abstract class ItemActivity extends ItemState
{
	private Set<ActionOutcome> actionOutcomes;
	private ActionOutcomeType actionOutcomeType;		// Optional, indicates the type/group of ActionOutcomeValues if used
	private LookupInstance lookup;
	private SortedSet<OutcomeStatus> outcomeStatuses;	// Was previously SortedSet<OutcomeStatus> 2016-08-22 GB
	protected boolean override;
	protected boolean progress;
	protected ItemActivityType type;
	private List<AdvesoTransferableActivity> advesoTransferableActivities;
	
	@OneToMany(mappedBy = "itemActivity", cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
	public List<AdvesoTransferableActivity> getAdvesoTransferableActivities()
	{
		return this.advesoTransferableActivities;
	}
	
	@OneToMany(mappedBy = "activity", cascade=CascadeType.ALL, orphanRemoval=true)
	@OrderBy("id")
	public Set<ActionOutcome> getActionOutcomes()
	{
		return this.actionOutcomes;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="actionoutcometype", nullable=true)
	public ActionOutcomeType getActionOutcomeType() {
		return actionOutcomeType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lookupid", nullable = true)
	public LookupInstance getLookup()
	{
		return this.lookup;
	}

	/*
	 * Removed @OrderBy annotation 2016-08-22 due to conflict with @SortComparator in Hibernate 4.3.x
	 * See https://hibernate.atlassian.net/browse/HHH-9688
	 */
	@OneToMany(mappedBy = "activity", orphanRemoval=true, cascade=CascadeType.ALL)
	@SortComparator(OutcomeStatusComparator.class)
	public SortedSet<OutcomeStatus> getOutcomeStatuses()
	{
		return this.outcomeStatuses;
	}

	public void setAdvesoTransferableActivities(List<AdvesoTransferableActivity> advesoTransferableActivities) {
		this.advesoTransferableActivities = advesoTransferableActivities;
	}

	/*
	 * Defined here in ItemActivity to prevent "proxy narrowing" exceptions in DWR, variable initialized in subclass
	 * GB 2016-04-09
	 */
	@Transient
	public boolean isOverride() {
		return this.override;
	}

	/*
	 * Defined here in ItemActivity to prevent "proxy narrowing" exceptions in DWR, variable initialized in subclass
	 * GB 2016-04-09
	 */
	@Transient
	public boolean isProgress() {
		return this.progress;
	}

	@Transient
	public ItemActivityType getType() {
		return this.type;
	}
	
	
	@Override
	@Transient
	public boolean isStatus()
	{
		return false;
	}

	public void setActionOutcomes(Set<ActionOutcome> actionOutcomes)
	{
		this.actionOutcomes = actionOutcomes;
	}

	public void setLookup(LookupInstance lookup)
	{
		this.lookup = lookup;
	}

	public void setOutcomeStatuses(SortedSet<OutcomeStatus> outcomeStatuses)
	{
		this.outcomeStatuses = outcomeStatuses;
	}

	public void setActionOutcomeType(ActionOutcomeType actionOutcomeType) {
		this.actionOutcomeType = actionOutcomeType;
	}

}