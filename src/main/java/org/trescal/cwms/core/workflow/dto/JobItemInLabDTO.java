package org.trescal.cwms.core.workflow.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JobItemInLabDTO {

	private Integer jobItemId;
	private Date dueDate;
	private Integer procedureCategoryId;
}