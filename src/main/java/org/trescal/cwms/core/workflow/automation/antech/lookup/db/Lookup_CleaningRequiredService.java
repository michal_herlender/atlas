package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CleaningRequired;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Component("Lookup_CleaningRequired")
public class Lookup_CleaningRequiredService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item requires cleaning or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: CLEANING REQUIRED");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result;
		if (!ji.getReqCleaning()) {
			result = Lookup_CleaningRequired.NO;
		} else {
			result = Lookup_CleaningRequired.YES;
			TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
			actions.addAll(ji.getActions());
			// loop backwards through actions
			activityLoop: for (JobItemAction action : actions.descendingSet()) {
				ItemActivity activity = this.itemStateService.findItemActivity(action.getActivity().getStateid());
				// find if item has been cleaned already
				for (StateGroupLink gl : activity.getGroupLinks()) {
					// if it has been cleaned already it doesn't require
					// cleaning
					if (gl.getGroup().equals(StateGroup.CLEAN) && !action.isDeleted()) {
						result = Lookup_CleaningRequired.NO;
						break activityLoop;
					}
				}
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_CleaningRequired.values();
	}
}