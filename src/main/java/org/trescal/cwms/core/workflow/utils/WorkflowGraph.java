package org.trescal.cwms.core.workflow.utils;

import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "graph")
@XmlAccessorType(XmlAccessType.FIELD)
public class WorkflowGraph {

	@XmlAttribute
	private String name;
	@XmlElement(name = "start")
	private WorkflowGraphNode start;
	@XmlElement(name = "activity")
	private Set<WorkflowGraphNode> activitySet;
	@XmlElement(name = "status")
	private Set<WorkflowGraphNode> statusSet;
	@XmlElement(name = "lookup")
	private Set<WorkflowGraphNode> lookupSet;
	@XmlElement(name = "lookupResult")
	private Set<WorkflowGraphNode> lookupResultSet;
	@XmlElement(name = "hidden")
	private Set<WorkflowGraphNode> outcomeStatusSet;
	@XmlElement(name = "actionOutcome")
	private Set<WorkflowGraphNode> actionOutcomeSet;
	@XmlElement(name = "flow")
	private Set<WorkflowGraphFlow> flowSet;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WorkflowGraphNode getStart() {
		return start;
	}

	public void setStart(WorkflowGraphNode start) {
		this.start = start;
	}

	public Set<WorkflowGraphNode> getActivitySet() {
		return activitySet;
	}

	public void setActivitySet(Set<WorkflowGraphNode> activitySet) {
		this.activitySet = activitySet;
	}

	public Set<WorkflowGraphNode> getStatusSet() {
		return statusSet;
	}

	public void setStatusSet(Set<WorkflowGraphNode> statusSet) {
		this.statusSet = statusSet;
	}

	public Set<WorkflowGraphNode> getLookupSet() {
		return lookupSet;
	}

	public void setLookupSet(Set<WorkflowGraphNode> lookupSet) {
		this.lookupSet = lookupSet;
	}

	public Set<WorkflowGraphNode> getLookupResultSet() {
		return lookupResultSet;
	}

	public void setLookupResultSet(Set<WorkflowGraphNode> lookupResultSet) {
		this.lookupResultSet = lookupResultSet;
	}

	public Set<WorkflowGraphNode> getActionOutcomeSet() {
		return actionOutcomeSet;
	}

	public void setActionOutcomeSet(Set<WorkflowGraphNode> actionOutcomeSet) {
		this.actionOutcomeSet = actionOutcomeSet;
	}

	public Set<WorkflowGraphFlow> getFlowSet() {
		return flowSet;
	}

	public void setFlowSet(Set<WorkflowGraphFlow> flowSet) {
		this.flowSet = flowSet;
	}

	public Set<WorkflowGraphNode> getOutcomeStatusSet() {
		return outcomeStatusSet;
	}

	public void setOutcomeStatusSet(Set<WorkflowGraphNode> outcomeStatusSet) {
		this.outcomeStatusSet = outcomeStatusSet;
	}

}
