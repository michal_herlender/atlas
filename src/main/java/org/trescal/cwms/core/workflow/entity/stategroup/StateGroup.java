package org.trescal.cwms.core.workflow.entity.stategroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.workflow.entity.StateGroupType;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;

public enum StateGroup {
	CLIENTDELIVERY(1, "clientdelivery", "stategroup.displayname.clientdelivery", "Ready for client delivery note"),
	ONSTOP(2, "onstop", "stategroup.displayname.onstop", "On stop"),
	DECISION(3, "decision", "stategroup.displayname.decision", "Client decision"),
	DESPATCH(4, "despatch", "stategroup.displayname.despatch", "Ready to be despatched"),
	RECEIVE(5, "receive", "stategroup.displayname.receive", "In transit, ready to be received"),
	CALIBRATION(6, "calibration", "stategroup.displayname.calibration", "Calibration"),
	REPAIR(7, "repair", "stategroup.displayname.repair", "Repair"),
	ADJUSTMENT(8, "adjustment", "stategroup.displayname.adjustment", "Adjustment"),
	TPDELIVERY(9, "tpdelivery", "stategroup.displayname.tpdelivery", "Ready for third party delivery note"),
	CLEAN(10, "clean", "stategroup.displayname.clean", "Cleaning"),
	BUSINESSTRANSIT(11, "businesstransit", "stategroup.displayname.businesstransit", "Business transit"),
	CLIENTTRANSIT(12, "clienttransit", "stategroup.displayname.clienttransit", "Client transit"),
	TPTRANSIT(13, "tptransit", "stategroup.displayname.tptransit", "Third party transit"),
	LABOURTRANSIT(14, "labourtransit", "stategroup.displayname.labourtransit", "Reason for transit is to perform calibration/repair"),
	RETURNTRANSIT(15, "returntransit", "stategroup.displayname.returntransit", "Reason for transit is to return to client"),
	EXCHANGE(16, "exchange", "stategroup.displayname.exchange", "Item exchanged by third party"),
	TPREQUIREMENT(17, "tprequirement", "stategroup.displayname.tprequirement", "Ready for third party requirements"),
	AWAITINGCALIBRATION(18, "awaitingcalibration", "stategroup.displayname.awaitingcalibration", "Ready for calibration"),
	ONHOLD(19, "onhold", "stategroup.displayname.onhold", "On hold"),
	AWAITINGDECISION(20, "awaitingdecision", "stategroup.displayname.awaitingdecision", "Waiting for client decision"),
	CLIENTDESPATCH(21, "clientdespatch", "stategroup.displayname.clientdespatch", "Ready for client despatch"),
	AWAITINGDESPATCHAPPROVAL(22, "awaitingdespatchapproval", "stategroup.displayname.awaitingdespatchapproval", "Waiting for approval for client despatch"),
	TPDESPATCH(23, "tpdespatch", "stategroup.displayname.tpdespatch", "Ready for third party despatch"),
	INSPECTION(24, "inspection", "stategroup.displayname.inspection", "Inspection"),
	DESPATCHAPPROVAL(25, "despatchapproval", "stategroup.displayname.despatchapproval", "Despatch approval"),
	AWAITINGTRUSTED(26, "awaitingtrusted", "stategroup.displayname.awaitingtrusted", "On stop, ready to be trusted"),
	RESUMECALIBRATION(27, "resumecalibration", "stategroup.displayname.resumecalibration", "Ready to resume on-hold calibration"),
	AWAITINGSUPPLIER(28, "awaitingsupplier", "stategroup.displayname.awaitingsupplier", "Waiting for third party"),
	CSCONTRACTREVIEW(29, "cscontractreview", "stategroup.displayname.cscontractreview", "1 - Contract Review", StateGroupLinkType.ALLOCATED_SUBDIV),
	CSJOBCOSTING(30, "csjobcosting", "stategroup.displayname.csjobcosting", "2 - Job Costing", StateGroupLinkType.ALLOCATED_SUBDIV),
	CSCLIENTACTION(31, "csclientaction", "stategroup.displayname.csclientaction", "3 - Awaiting Client Approval / Decision", StateGroupLinkType.ALLOCATED_SUBDIV),
	CSOTHER(32, "csother", "stategroup.displayname.csother", "4 - Other", StateGroupLinkType.ALLOCATED_SUBDIV),
	IODELIVERY(33, "iodelivery", "stategroup.displayname.iodelivery", "1 - Awaiting Delivery Note", StateGroupLinkType.CURRENT_SUBDIV),
	IODESPATCH(34, "iodespatch", "stategroup.displayname.iodespatch", "2 - Awaiting Despatch", StateGroupLinkType.CURRENT_SUBDIV),
	IOOTHER(35, "ioother", "stategroup.displayname.ioother", "3 - Other", StateGroupLinkType.CURRENT_SUBDIV),
	LAB(36, "lab", "stategroup.displayname.lab", "Laboratory", StateGroupLinkType.CURRENT_SUBDIV),
	TPWORK(37, "tpwork", "stategroup.displayname.tpwork", "Third Party Work", StateGroupLinkType.ALLOCATED_SUBDIV),
	ITMISC(38, "itmisc", "stategroup.displayname.itmisc", "1 - Misc", StateGroupLinkType.ALLOCATED_SUBDIV),
	INSPECTIONTRANSIT(39, "inspectiontransit", "stategroup.displayname.inspectiontransit", "Reason for transit is to return to base for inspection"),
	BEFOREONSITE(40, "beforeonsite", "stategroup.displayname.beforeonsite", "1 - Before Onsite", StateGroupLinkType.ALLOCATED_SUBDIV),
	DURINGONSITE(41, "duringonsite", "stategroup.displayname.duringonsite", "2 - During Onsite", StateGroupLinkType.ALLOCATED_SUBDIV),
	AFTERONSITE(42, "afteronsite", "stategroup.displayname.afteronsite", "3 - After Onsite", StateGroupLinkType.ALLOCATED_SUBDIV),
	INTERNALDELIVERY(43, "internaldelivery", "stategroup.displayname.internaldelivery", "Ready for internal delivery note"),
	AWAITINGINTERNALPO(44, "createinternalpo", "stategroup.displayname.createinternalpo", "Create internal purchase order"),
	INANOTHERBUSINESSCOMPANY(45, "inanotherbusinesscompany", "stategroup.displayname.inanothertrescalsubdivision", "In another Trescal subdivision", StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV),
	DIRECTWORKACTIVITY(46,"directworkactivity","stategroup.displayname.directworkactivity","Direct work activity"),
	CALLEDOFF(47,"calledoff","stategroup.displayname.calledoff","Called Off"),
	TIME_RELATED(48, "timerelated", "stategroup.displayname.timerelated", "Time Related"),
	IN_TRANSIT(49, "intransit", "stategroup.displayname.intransit", "4 - In Transit", StateGroupLinkType.ALLOCATED_SUBDIV),
	FURTHER_CALIBRATION_POSSIBLE(50,"furthercalibrationpossible","stategroup.displayname.furthercalibrationpossible","Further calibration possible"),
	CREATE_NEW_JOBITEM_FOR_REPAIR(51,"createnewjobitemforrepair","stategroup.displayname.createnewjobitemforrepair","Create new JobItem for Repair"),
	SELECT_NEXT_WORK_REQUIREMENT(52,"selectnextworkrequirement","stategroup.displayname.selectnextworkrequirement","Select next workrequirement"),
	TLM_UPLOAD_CERTIFICATE(53,"tlmeventuploadcertificate","stategroup.displayname.tlmeventuploadcertificate","TLM - Upload Certificate"),
	AWAITING_SELECTION_OF_FR_OUTCOME(54,"awaitingselectionoffroutcome","stategroup.displayname.awaitingselectionoffroutcome","Awaiting Selection of failure report ouctome"),
	COSTING_ISSUED(55, "costingissued","stategroup.displayname.costingissued","Costing issued"),
	AWAITINGRCRUPDATE(56, "awaitingrcrupdate","stategroup.displayname.awaitingrcrupdate","Awaiting RCR update"),
	FAILURE_REPORT_AVAILABLE_TO_ADVESO(57, "failurereportavailabletoadveso","stategroup.displayname.failurereportavailabletoadveso","Failure Report Available to Adveso"),
	AWAITINGRCRVALIDATION(58, "awaitingrcrvalidation","stategroup.displayname.awaitingrcrvalidation","Awaiting RCR validation"),
	FR_REQUIREDELIVERY(59, "failurereportrequiredelivery","stategroup.displayname.failurereportrequiredelivery","Informational Failure Report Require Delivery"),
	RIR_TECHNICIAN_VALIDATION(60, "rirtechnicianvalidation","stategroup.displayname.rirtechnicianvalidation","Repair inspection report validated by technician"),
	RIR_CSR_REVIEW(61, "rircsrvalidation","stategroup.displayname.rirtechnicianvalidation","Repair inspection report validated by csr"),
	AWAITING_TP_CERTIFICATE(62, "awaitingtpcertificate","stategroup.displayname.awaitingtpcertificate","Awaiting third party certificate"),
	AWAITING_TP_PO(63, "awaitingtppo","stategroup.displayname.awaitingtppo","Awaiting PO for third party work"),
	AWAITING_CONTRACT_REVIEW(64, "awaitingcontractreview","","Awaiting contract review"),
	AWAITINGGENERALSERVICEOPERATION(65, "awaitinggeneralserviceoperation","stategroup.displayname.awaitinggeneralserviceoperation","Ready for General Service Operation"),
	GENERALSERVICEOPERATION(66, "generalserviceoperation","stategroup.displayname.generalserviceoperation","General Service Operation"),
	RESUMEGENERALSERVICEOPERATION(67, "resumegeneralserviceoperation", "stategroup.displayname.resumegeneralserviceoperation", "Ready to resume on-hold general service operation"),
	AWAITING_GSO_DOCUMENT_UPLOAD(68, "awaitinggsodocumentupload","stategroup.displayname.awaitinggsodocumentupload","Ready for General Service Operation Document Upload"),
	CSGSO(69, "csgso", "stategroup.displayname.csgso", "5 - General Service Operation", StateGroupLinkType.ALLOCATED_SUBDIV),
	RCR_VALIDATION(70, "rcrvalidation","stategroup.displayname.rcrvalidation","Repair completion report validated"),
	GSO_DOCUMENT(71, "gsodocument","stategroup.displayname.gsodocument","General service operation document"),
	INTERNAL_RETURN_DELIVERY(72, "internalreturndelivery", "stategroup.displayname.internalreturndelivery", "Ready for internal return delivery note"),
	AWAITING_REPAIR_WORK_BY_TECHNICIAN(73,"awaitingrepairworkbytechnician","stategroup.displayname.awaitingrepairworkbytechnician","Awaiting repair work by technician"),
	RIR_MANAGER_VALIDATION(74, "rirmanagervalidation","stategroup.displayname.rirmanagervalidation","Repair Inspection Report validated by Manager"),
	RECORD_REPAIR_TIME_BY_TECHNICIAN(75, "recordrepairtimebytechnician","stategroup.displayname.recordrepairtimebytechnician","Record repair time by technician"),
	AWAITING_CLIENT_RECEIPT_CONFIRMATION(76, "clientreceiptconfirmation","stategroup.displayname.clientreceiptconfirmation","Awaiting client receipt confirmation"),
	START_LAB_CERTIFICATE_REPLACEMENT(77, "startlabcertificatereplacement","stategroup.displayname.startlabcertificatereplacement","Ready to start laboratory certificate replacement"),
	START_THIRDPARTY_CERTIFICATE_REPLACEMENT(78, "startthirdpartycertificatereplacement","stategroup.displayname.startthirdpartycertificatereplacement","Ready to start third party certificate replacement"),
	START_ONSITE_CERTIFICATE_REPLACEMENT(79, "startonsitecertificatereplacement","stategroup.displayname.startonsitecertificatereplacement","Ready to start on-site certificate replacement");
	
	private String displayName;
	private int id;
	private String keyName;
	private StateGroupType type;
	private StateGroupLinkType linkType;		// Relevant only for StateGroupType.DEPARTMENT
	private ReloadableResourceBundleMessageSource messages;
	private String messageCodeDisplayName;

	@Component
	public static class StateGroupMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (StateGroup rt : EnumSet.allOf(StateGroup.class))
               rt.setMessageSource(messages);
        }
	}

	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	/**
	 * Constructor for StateGroupType.WORKFLOW
	 */
	private StateGroup (int id, String keyName, String messageCodeDisplayName, String displayName) {
		this.id = id;
		this.keyName = keyName;
		this.messageCodeDisplayName = messageCodeDisplayName;
		this.displayName = displayName;
		this.type = StateGroupType.WORKFLOW;
	}

	private StateGroup (int id, String keyName, String messageCodeDisplayName, String displayName, StateGroupLinkType linkType) {
		this.id = id;
		this.keyName = keyName;
		this.messageCodeDisplayName = messageCodeDisplayName;
		this.displayName = displayName;
		this.type = StateGroupType.DEPARTMENT;
		this.linkType = linkType;
	}

	public String getDisplayName() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCodeDisplayName, null, this.displayName, locale);
		}
		return this.displayName;
	}

	public int getId() {
		return this.id;
	}

	/**
	 * Key names are intentionally not translated but are used internally only.  GB 2016-08-31
	 */
	public String getKeyName() {
		return this.keyName;
	}

	public StateGroupType getType() {
		return this.type;
	}

	public StateGroupLinkType getLinkType() {
		return this.linkType;
	}

	public static StateGroup getStateGroupById(Integer id) {
		List<StateGroup> stateGroups = Arrays.asList(StateGroup.values());
		return stateGroups.stream().filter(sg -> sg.getId() == id).findAny().orElse(null);
	}

	public static List<StateGroup> getAllStateGroupsOfType(StateGroupType type) {
		List<StateGroup> list = new ArrayList<StateGroup>();
		for (StateGroup sg: StateGroup.values()) {
			if (sg.getType().equals(type))
				list.add(sg);
		}
		return list;
	}

	public String getMessageCodeDisplayName() {
		return messageCodeDisplayName;
	}
}