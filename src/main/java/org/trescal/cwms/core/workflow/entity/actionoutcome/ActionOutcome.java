package org.trescal.cwms.core.workflow.entity.actionoutcome;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "actionoutcome")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class ActionOutcome {
	private boolean active;
	private ItemActivity activity;
	private boolean defaultOutcome;
	private String description;					// deprecated, replaced with ActionOutcomeValue enum
	private int id;
	private boolean positiveOutcome;
	private Set<Translation> translations;		// deprecated, replaced with ActionOutcomeValue enum
	protected ActionOutcomeType type;			// Initialized by concrete subclass

	@Transient
	public abstract ActionOutcomeValue getGenericValue();
	
	@Transient
	public ActionOutcomeType getType() {
		return this.type;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "activityid")
	public ItemActivity getActivity()
	{
		return this.activity;
	}

	@Deprecated
	@Length(max = 200)
	@Column(name = "description", nullable = false, length = 200)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "active", nullable = false, columnDefinition="bit")
	public boolean isActive()
	{
		return this.active;
	}

	@Column(name = "defaultoutcome", nullable = false, columnDefinition="bit")
	public boolean isDefaultOutcome()
	{
		return this.defaultOutcome;
	}

	@Column(name = "positiveoutcome", nullable = false, columnDefinition="bit")
	public boolean isPositiveOutcome()
	{
		return this.positiveOutcome;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setActivity(ItemActivity activity)
	{
		this.activity = activity;
	}

	public void setDefaultOutcome(boolean defaultOutcome)
	{
		this.defaultOutcome = defaultOutcome;
	}

	/**
	 * @deprecated
	 * Use enums getGenericValue() or getValue() on concrete class 
	 */
	@Deprecated
	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setPositiveOutcome(boolean positiveOutcome)
	{
		this.positiveOutcome = positiveOutcome;
	}

	/**
	 * @deprecated
	 * Use enums getGenericValue() or getValue() on concrete class 
	 */
	@Deprecated
	@ElementCollection(fetch=FetchType.LAZY)
	@Fetch(FetchMode.SELECT)
	@CollectionTable(name="actionoutcometranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	
	@Deprecated
	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
}