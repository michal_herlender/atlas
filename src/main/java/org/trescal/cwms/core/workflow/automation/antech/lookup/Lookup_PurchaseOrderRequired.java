package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_PurchaseOrderRequired implements LookupResultMessage {
	
	YES("lookupresultmessage.purchaseorderrequired.yes"),
	NO("lookupresultmessage.purchaseorderrequired.no");
	
	private String messageCode;
	
	private Lookup_PurchaseOrderRequired(String messageCode) {
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return messageCode;
	}
}