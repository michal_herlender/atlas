package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_FaultReportIsApprovedByClient implements LookupResultMessage {

	APPROVED("actionoutcome.faultreport.client.approved"), 
	NOT_APPROVED("actionoutcome.faultreport.client.not.approved"), 
	CLIENT_CHOOSED_ANOTHER_OUTCOME("actionoutcome.faultreport.client.choosed.another.outcome");

	private String messageCode;

	private Lookup_FaultReportIsApprovedByClient(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}

}
