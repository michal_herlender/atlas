package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;

@Entity
@DiscriminatorValue("calibration")
public class ActionOutcome_Calibration extends ActionOutcome {
	private ActionOutcomeValue_Calibration value;
	
	public ActionOutcome_Calibration() {
		this.type = ActionOutcomeType.CALIBRATION;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_Calibration getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_Calibration value) {
		this.value = value;
	}
}