package org.trescal.cwms.core.workflow.form.edit;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivityType;

public class EditItemActivityForm extends AbstractItemStateForm {
	private List<EditActionOutcomeDto> actionOutcomeDtos;
	private EditActionOutcomeDto templateActionOutcome;
	private Boolean actionOutcomesEnabled;
	private Boolean lookupOrStatus;
	private ActionOutcomeType actionOutcomeType;
	private Integer lookupId;
	private Integer statusId;
	private ItemActivityType type;
	
	@Valid
	public List<EditActionOutcomeDto> getActionOutcomeDtos() {
		return actionOutcomeDtos;
	}
	@NotNull
	public Boolean getLookupOrStatus() {
		return lookupOrStatus;
	}
	@NotNull	// But selection only relevant if action outcomes enabled 
	public ActionOutcomeType getActionOutcomeType() {
		return actionOutcomeType;
	}
	public Integer getLookupId() {
		return lookupId;
	}
	@NotNull
	public ItemActivityType getType() {
		return type;
	}
	public Integer getStatusId() {
		return statusId;
	}
	// Does not require validation - template only
	public EditActionOutcomeDto getTemplateActionOutcome() {
		return templateActionOutcome;
	}
	@NotNull
	public Boolean getActionOutcomesEnabled() {
		return actionOutcomesEnabled;
	}
	public void setActionOutcomeDtos(List<EditActionOutcomeDto> actionOutcomeDtos) {
		this.actionOutcomeDtos = actionOutcomeDtos;
	}
	public void setLookupId(Integer lookupId) {
		this.lookupId = lookupId;
	}
	public void setLookupOrStatus(Boolean lookupOrStatus) {
		this.lookupOrStatus = lookupOrStatus;
	}
	public void setActionOutcomeType(ActionOutcomeType actionOutcomeType) {
		this.actionOutcomeType = actionOutcomeType;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public void setType(ItemActivityType type) {
		this.type = type;
	}
	public void setTemplateActionOutcome(EditActionOutcomeDto templateActionOutcome) {
		this.templateActionOutcome = templateActionOutcome;
	}
	public void setActionOutcomesEnabled(Boolean actionOutcomesEnabled) {
		this.actionOutcomesEnabled = actionOutcomesEnabled;
	}
}
