package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemIsOrificePlate implements LookupResultMessage
{
	YES("lookupresultmessage.itemisorificeplate.yes"),
	NO("lookupresultmessage.itemisorificeplate.no");
	
	private String messageCode;
	
	private Lookup_ItemIsOrificePlate(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}