package org.trescal.cwms.core.workflow.entity.workassignment.dto;

import lombok.Value;

import java.util.Map;

@Value(staticConstructor = "of")
public class WorkAssigmentLaboratoryCounter implements WorkAssigmentCounters {

    Map<Integer, WorkByDepartment> workByDepartmentMap;
    Map<WorkCountDay, Integer> uncategorised;
    Map<WorkCountDay, Integer> summary;
}
