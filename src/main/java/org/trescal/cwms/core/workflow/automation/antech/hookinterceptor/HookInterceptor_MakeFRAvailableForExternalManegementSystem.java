package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.TreeSet;

@Service
public class HookInterceptor_MakeFRAvailableForExternalManegementSystem
	extends HookInterceptor<HookInterceptor_MakeFRAvailableForExternalManegementSystem.Input> {

	public static final String HOOK_NAME = "make-fr-available-to-external-managment-system";

	public static class Input {

		public FaultReport fr;
		public Contact con;
		public String remark;
		public Date startDate;
		public Integer time;

		public Input(FaultReport fr, Contact con, Date startDate, Integer time, String remark) {
			this.fr = fr;
			this.con = con;
			this.startDate = startDate;
			this.time = time;
			this.remark = remark;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: " + HOOK_NAME);
			Contact con = input.con;
			JobItem ji = input.fr.getJobItem();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) ji.getState());
			if (input.startDate == null)
				jia.setStartStamp(input.fr.getManagerValidationOn());
			else
				jia.setStartStamp(input.startDate);
			jia.setStartedBy(con);
			if (StringUtils.isNotBlank(input.remark))
				jia.setRemark(input.remark);
			jia.setCreatedOn(new Date());
			jia.setJobItem(ji);
			jia.setComplete(context.isCompleteActivity());

			if (context.isCompleteActivity()) {
				if (input.time == null)
					jia.setTimeSpent(0);
				else
					jia.setTimeSpent(input.time);
				jia.setEndStamp(DateUtils.addMinutes(jia.getStartStamp(), jia.getTimeSpent()));
				jia.setCompletedBy(con);
			}

			// adds the action into the job item in the session
			if (ji.getActions() == null) {
				ji.setActions(new TreeSet<JobItemAction>(new JobItemActionComparator()));
			}
			ji.getActions().add(jia);

			ItemStatus endStatus = super.getNextStatus(context);
			ji.setState(endStatus);

			jobItemService.updateJobItem(ji);

			if (context.isCompleteActivity()) {
				jia.setEndStatus(endStatus);
			}

			this.persist(jia, endStatus, context);
		}
	}
}
