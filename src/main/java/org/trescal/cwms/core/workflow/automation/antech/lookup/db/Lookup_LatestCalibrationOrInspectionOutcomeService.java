package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_LatestCalibrationOrInspectionOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

/* TODO changed the behaviour - not used? */
@Component("Lookup_LatestCalibrationOrInspectionOutcome")
public class Lookup_LatestCalibrationOrInspectionOutcomeService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public String getDescription() {
		return "Determines the outcome of the item's latest calibration or inspection (depending which is most recent).";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: LATEST CALIBRATION OR INSPECTION OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions to get the MOST RECENT calibration or
		// inspection
		// in case there is more than one
		activityLoop: for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity jia = (JobItemActivity) action;
				ItemActivity activity = this.itemStateService.findItemActivity(jia.getActivity().getStateid());
				for (StateGroupLink sgl : activity.getGroupLinks()) {
					if ((sgl.getGroup().equals(StateGroup.CALIBRATION) || sgl.getGroup().equals(StateGroup.INSPECTION))
							&& !jia.isDeleted()) {
						ao = jia.getOutcome();
						break activityLoop;
					}
				}
			}
		}
		if (ao == null) {
			result = Lookup_LatestCalibrationOrInspectionOutcome.THIRD_PARTY_CALIBRATION;
		} else {
			FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);
			if (fr != null && !fr.isAwaitingFinalisation()) {

				// TODO : important
				// if
				// (fr.getRecommendation().equals(FailureReportOutcomeEnum.REPAIR))
				// {
				// if (!fr.getOperationByTrescal())
				// result =
				// Lookup_LatestCalibrationOrInspectionOutcome.THIRD_PARTY_REPAIR;
				// else
				// result =
				// Lookup_LatestCalibrationOrInspectionOutcome.INHOUSE_REPAIR;
				// } else if
				// (fr.getRecommendation().equals(FailureReportOutcomeEnum.CALIBRATION_WITH_JUDGMENT))
				// {
				// if (!fr.getOperationByTrescal())
				// result =
				// Lookup_LatestCalibrationOrInspectionOutcome.THIRD_PARTY_CALIBRATION;
				// } else if
				// (fr.getRecommendation().equals(FailureReportOutcomeEnum.ADJUSTMENT))
				// {
				// result =
				// Lookup_LatestCalibrationOrInspectionOutcome.ADJUSTMENT;
				// }
			}

			if (ao.getDescription().contains("third party repair")) {
				result = Lookup_LatestCalibrationOrInspectionOutcome.THIRD_PARTY_REPAIR;
			} else if (ao.getDescription().contains("in-house repair")) {
				result = Lookup_LatestCalibrationOrInspectionOutcome.INHOUSE_REPAIR;
			} else if (ao.getDescription().contains("adjustment")) {
				result = Lookup_LatestCalibrationOrInspectionOutcome.ADJUSTMENT;
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_LatestCalibrationOrInspectionOutcome.values();
	}
}