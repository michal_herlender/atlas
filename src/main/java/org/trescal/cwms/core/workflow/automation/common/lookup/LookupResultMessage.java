package org.trescal.cwms.core.workflow.automation.common.lookup;

public interface LookupResultMessage
{
	public String getMessageCode();
	public int ordinal();
	public String name();
}