package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtFirstLocation;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemAtFirstLocation")
public class Lookup_ItemAtFirstLocationService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item is at the first location where it is due, either a base location or a calibration location.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM AT FIRST LOCATION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		if ((ji.getCurrentAddr() != null) && (ji.getCalAddr() != null)) {
			// if item has to go to base first
			if (ji.getBaseAddr() != null) {
				// if item is at base currently
				if (ji.getCurrentAddr().getAddrid() == ji.getBaseAddr().getAddrid()) {
					result = Lookup_ItemAtFirstLocation.YES;
				} else {
					result = Lookup_ItemAtFirstLocation.NO;
				}
			}
			// else item is going straight to cal addr
			else {
				// if item is at cal addr currently
				if (ji.getCurrentAddr().getAddrid() == ji.getCalAddr().getAddrid()) {
					result = Lookup_ItemAtFirstLocation.YES;
				} else {
					result = Lookup_ItemAtFirstLocation.NO;
				}
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemAtFirstLocation.values();
	}
}