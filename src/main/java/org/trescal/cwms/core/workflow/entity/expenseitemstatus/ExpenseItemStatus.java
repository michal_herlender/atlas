package org.trescal.cwms.core.workflow.entity.expenseitemstatus;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

@Entity
@DiscriminatorValue("expenseitemstate")
public class ExpenseItemStatus extends ItemState {

	/**
	 * @return false - otherwise it would be a selectable status for job items
	 */
	@Override
	@Transient
	public boolean isStatus() {
		return false;
	}
}