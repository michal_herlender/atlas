package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_GeneralServiceOperation;

@Entity
@DiscriminatorValue("general_service_operation")
public class ActionOutcome_GeneralServiceOperation extends ActionOutcome {
	private ActionOutcomeValue_GeneralServiceOperation value;
	
	public ActionOutcome_GeneralServiceOperation() {
		this.type = ActionOutcomeType.GENERAL_SERVICE_OPERATION;
	}
	
	@Transient
	@Override
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_GeneralServiceOperation getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_GeneralServiceOperation value) {
		this.value = value;
	}

}
