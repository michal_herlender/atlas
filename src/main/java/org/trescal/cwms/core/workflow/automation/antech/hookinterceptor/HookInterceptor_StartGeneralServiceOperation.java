package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_StartGeneralServiceOperation extends HookInterceptor<HookInterceptor_StartGeneralServiceOperation.Input> {

	public static final String HOOK_NAME = "start-general-service-operation";

	@Autowired
	private JobItemService jiServ;

	public static class Input {
		public GeneralServiceOperation gso;
		public boolean resuming;
		public Contact contact;
		public Date startStamp;
		public Date endStamp;
		
		public Input(GeneralServiceOperation gso, boolean resuming, Contact contact, Date startStamp, Date endStamp) {
			super();
			this.gso = gso;
			this.resuming = resuming;
			this.contact = contact;
			this.startStamp = startStamp;
			this.endStamp = endStamp;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: START GENERAL SERVICE OPERATION");
			GeneralServiceOperation gso = input.gso;
			Contact con = input.contact != null ? gso.getStartedBy() : this.sessionServ.getCurrentContact();
			Date startStamp = input.startStamp;
			Date endStamp = input.endStamp;

			for (GsoLink link : gso.getLinks()) {
				
				JobItem ji = link.getJi();
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

				if (this.jiServ.isReadyForGeneralServiceOperation(ji) || input.resuming) {
					JobItemActivity jia = new JobItemActivity();
					jia.setActivity(context.getActivity());
					jia.setActivityDesc(context.getActivity().getDescription());
					jia.setStartStatus((ItemStatus) ji.getState());
					jia.setStartStamp(startStamp != null ? startStamp : new Date());
					jia.setStartedBy(con);
					jia.setCreatedOn(new Date());
					jia.setJobItem(ji);
					jia.setComplete(context.isCompleteActivity());

					if (context.isCompleteActivity()) {
						if (endStamp != null)
							jia.setEndStamp(endStamp);
						else
							jia.setEndStamp(new Date());
						jia.setTimeSpent(5);
						jia.setCompletedBy(con);
					}

					// adds the action into the job item in the session
					ji.getActions().add(jia);

					ItemStatus endStatus = null;
					if (context.isCompleteActivity()) {
						endStatus = super.getNextStatus(context);
						ji.setState(endStatus);
						jia.setEndStatus(endStatus);
					} else
						ji.setState(context.getActivity());
					jobItemService.updateJobItem(ji);
					super.persist(jia, endStatus, context);
				}
			}
		}
	}

	public void setJiServ(JobItemService jiServ) {
		this.jiServ = jiServ;
	}
}