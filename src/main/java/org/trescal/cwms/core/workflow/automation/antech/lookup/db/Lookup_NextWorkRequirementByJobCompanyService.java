package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NextWorkRequirementByJobCompany;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

import java.util.List;

@Component("Lookup_NextWorkRequirementByJobCompany")
public class Lookup_NextWorkRequirementByJobCompanyService extends Lookup {

    @Autowired
    private JobItemWorkRequirementService jobItemWorkRequirementService;

    @Override
    public LookupResultMessage lookupStatus(int jobItemId) {
        logger.debug("RUNNING LOOKUP: NEXT WORK REQUIREMENT BY JOB COMPANY");
        JobItem jobItem = jobItemService.findJobItem(jobItemId);
        int jobCompanyId = jobItem.getJob().getOrganisation().getComp().getCoid();

        List<JobItemWorkRequirement> jiWrs = jobItemWorkRequirementService.getForJobItemView(jobItem);

        // exit point : if all work requirement are completed or cancelled
        boolean allWorkRequiementAreCompleted = jiWrs.stream().map(jiwr -> jiwr.getWorkRequirement())
            .allMatch(wr -> wr.getStatus().equals(WorkrequirementStatus.COMPLETE) || wr.getStatus().equals(WorkrequirementStatus.CANCELLED));
        if (allWorkRequiementAreCompleted)
            return Lookup_NextWorkRequirementByJobCompany.NO_FURTHER_WORK;

		LookupResultMessage result = Lookup_NextWorkRequirementByJobCompany.UNKNOWN;
		if (jobItem.getNextWorkReq() != null) {
			WorkRequirement wr = jobItem.getNextWorkReq().getWorkRequirement();
			if (!wr.getStatus().equals(WorkrequirementStatus.COMPLETE) && !wr.getStatus().equals(WorkrequirementStatus.CANCELLED)) {
                // Lookup result only determined if the work requirement is
                // still to be executed
                if (wr.getCapability() != null) {
                    if (wr.getType().equals(WorkRequirementType.THIRD_PARTY))
                        result = Lookup_NextWorkRequirementByJobCompany.SUPPLIER;
                    else {
                        if (wr.getCapability().getOrganisation().getComp().getCoid() == jobCompanyId) {
                            result = Lookup_NextWorkRequirementByJobCompany.SAME_BUSINESS_COMP;
                        } else {
                            if (wr.getCapability().getOrganisation().getComp().getCompanyRole()
                                .equals(CompanyRole.BUSINESS))
                                result = Lookup_NextWorkRequirementByJobCompany.OTHER_BUSINESS_COMP;
                            else if (wr.getCapability().getOrganisation().getComp().getCompanyRole()
                                .equals(CompanyRole.SUPPLIER))
                                result = Lookup_NextWorkRequirementByJobCompany.SUPPLIER;
                        }
                    }
                }
            }
		}

		return result;
	}

	@Override
	public String getDescription() {
		return "Checks whether the next work requirement is to be performed by the same business company as the job";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_NextWorkRequirementByJobCompany.values();
	}
}