package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionForGeneralServiceOperation;

@Entity
@DiscriminatorValue("client_decision_gso")
public class ActionOutcome_ClientDecisionForGeneralServiceOperation extends ActionOutcome {
	private ActionOutcomeValue_ClientDecisionForGeneralServiceOperation value;
	
	public ActionOutcome_ClientDecisionForGeneralServiceOperation() {
		this.type = ActionOutcomeType.CLIENT_DECISION_GENERAL_SERVICE_OPERATION;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_ClientDecisionForGeneralServiceOperation getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_ClientDecisionForGeneralServiceOperation value) {
		this.value = value;
	}

}
