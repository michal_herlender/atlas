package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementComparator;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TPRequirements;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_TPRequirements;

@Component("Lookup_TPRequirements")
public class Lookup_TPRequirementsService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the outcome of the item's most recent third party requirement set.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: TP REQUIREMENTS");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		if (ji.getTpRequirements().size() > 0) {
			TreeSet<TPRequirement> tprs = new TreeSet<>(new TPRequirementComparator());
			tprs.addAll(ji.getTpRequirements());
			TPRequirement tpr = tprs.first();
			ActionOutcome ao = tpr.getOutcome();
			if (ao != null) {

				ActionOutcomeValue_TPRequirements outcome = (ActionOutcomeValue_TPRequirements) ao.getGenericValue();

				switch (outcome) {
				case ANOTHER_BUSINESS_SUBDIVISION_NO_PO_REQUIRED:
					result = Lookup_TPRequirements.WORK_AT_PARTNER;
					break;
				case NOT_QUOTED_TP_QUOTES_WITHOUT_ITEM:
					result = Lookup_TPRequirements.TP_CAN_QUOTE;
					break;
				case NOT_QUOTED_SEND_TO_TP_FOR_QUOTE:
					result = Lookup_TPRequirements.SEND_TO_TP_FOR_QUOTE;
					break;
				case NOT_QUOTED_SEND_TO_TP_FOR_QUOTE_INSPECTION_COSTS:
					result = Lookup_TPRequirements.SEND_TO_TP_FOR_QUOTE_WITH_INSPECTION;
					break;
				case PRE_QUOTED_COSTED_SEND_TO_TP_FOR_WORK:
					result = Lookup_TPRequirements.PRE_QUOTED_AND_COSTED;
					break;
				case SUPPLIER_CONTRACT_NO_PO_REQUIRED_NO_RETURN:
					result = Lookup_TPRequirements.CONTRACT_SEND_TO_TP_NO_RETURN;
					break;
				case SUPPLIER_CONTRACT_NO_PO_REQUIRED_WITH_RETURN:
					result = Lookup_TPRequirements.CONTRACT_SEND_TO_TP_WITH_RETURN;
					break;
				default:
					break;
				}

			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_TPRequirements.values();
	}
}