package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NoOperationPerformedOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_NoOperationPerformed;

@Component("Lookup_NoOperationPerformedOutcome")
public class Lookup_NoOperationPerformedOutcomeService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item to deliver need job costing or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {

		logger.debug("RUNNING LOOKUP: NO OPERATION PERFORMED OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions to get the MOST RECENT no operation performed
		for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;
				if ((activity.getActivityDesc().equals("No operation performed")
						|| activity.getActivityDesc().equals("No onsite operation performed"))
						&& !activity.isDeleted()) {
					ao = activity.getOutcome();
					break;
				}
			}
		}

		if (ao != null && ao.getGenericValue().equals(ActionOutcomeValue_NoOperationPerformed.FAILURE_REPORT_REQUIRED))
			return Lookup_NoOperationPerformedOutcome.FAILURE_REPORT_REQUIRED;
		return Lookup_NoOperationPerformedOutcome.FAILURE_REPORT_NOT_REQUIRED;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_NoOperationPerformedOutcome.values();
	}
}