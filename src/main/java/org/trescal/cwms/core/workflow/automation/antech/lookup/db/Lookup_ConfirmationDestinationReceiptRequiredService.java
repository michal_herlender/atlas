package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ClientDeliveryDestinationReceiptDate;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ConfirmationDestinationReceiptRequired;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ConfirmationDestinationReceiptRequired")
public class Lookup_ConfirmationDestinationReceiptRequiredService extends Lookup {

	@Autowired
	private ClientDeliveryDestinationReceiptDate clientDeliveryDestinationReceiptDate;
	
	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Confirmation of Destination Receipt");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		Subdiv clientSubdiv = ji.getJob().getCon().getSub();
		boolean clientDeliveryDestinationReceiptDateRequired = clientDeliveryDestinationReceiptDate
				.parseValueHierarchical(Scope.SUBDIV, clientSubdiv,
						clientSubdiv.getComp());
		
		if(clientDeliveryDestinationReceiptDateRequired) {
			return Lookup_ConfirmationDestinationReceiptRequired.YES;
		} else
			return Lookup_ConfirmationDestinationReceiptRequired.NO;
	}

	@Override
	public String getDescription() {
		return "Determines whether the confirmation of destination receipt is required or not.";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ConfirmationDestinationReceiptRequired.values();
	}
}
