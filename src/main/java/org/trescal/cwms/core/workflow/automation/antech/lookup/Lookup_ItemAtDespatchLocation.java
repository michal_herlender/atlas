package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemAtDespatchLocation implements LookupResultMessage
{
	YES("lookupresultmessage.itematdespatchlocation.yes"),
	NO("lookupresultmessage.itematdespatchlocation.no");
	
	private String messageCode;
	
	private Lookup_ItemAtDespatchLocation(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}