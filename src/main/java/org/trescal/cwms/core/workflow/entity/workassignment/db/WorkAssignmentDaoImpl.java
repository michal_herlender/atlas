package org.trescal.cwms.core.workflow.entity.workassignment.db;

import io.vavr.Tuple2;
import lombok.val;
import org.hibernate.dialect.Dialect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ActiveClientJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ActiveClientJobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter_;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.workflow.dto.JobItemInLabDTO;
import org.trescal.cwms.core.workflow.dto.LabCounts;
import org.trescal.cwms.core.workflow.dto.LabCountsRaw;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;
import org.trescal.cwms.core.workflow.entity.workassignment.WorkAssignment;
import org.trescal.cwms.core.workflow.entity.workassignment.WorkAssignment_;

import javax.persistence.criteria.*;
import java.time.Clock;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository("WorkAssignmentDao")
public class WorkAssignmentDaoImpl extends BaseDaoImpl<WorkAssignment, Integer> implements WorkAssignmentDao {

    private final Clock clock;

    @Value("#{props['hibernate.dialect']}")
    private Class<? extends Dialect> dialect;

    public WorkAssignmentDaoImpl(Clock clock) {
        super();
        this.clock = clock;
    }

    @Override
    protected Class<WorkAssignment> getEntity() {
        return WorkAssignment.class;
    }

    @Override
    public List<WorkAssignment> getForDepartmentType(DepartmentType deptType) {
        return getResultList(cb -> {
            CriteriaQuery<WorkAssignment> cq = cb.createQuery(WorkAssignment.class);
            Root<WorkAssignment> workAssignment = cq.from(WorkAssignment.class);
            cq.where(cb.equal(workAssignment.get(WorkAssignment_.deptType), deptType));
            return cq;
        });
	}

	public List<JobItemInLabDTO> getAllInSubdivLabs(Integer subdivId) {
		return getResultList(cb -> {
            CriteriaQuery<JobItemInLabDTO> cq = cb.createQuery(JobItemInLabDTO.class);
            Root<JobItem> jobItem = cq.from(JobItem.class);
            Join<JobItem, Capability> procedure = jobItem.join(JobItem_.capability, JoinType.LEFT);
            Join<Capability, CategorisedCapability> catProc = procedure.join(Capability_.categories, JoinType.LEFT);
            Join<CategorisedCapability, CapabilityCategory> category = catProc.join(CategorisedCapability_.category,
                JoinType.LEFT);
            Join<CapabilityCategory, Department> department = category.join(CapabilityCategory_.department,
                JoinType.LEFT);
            Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(department.get(Department_.subdiv), subdivId));
            clauses.getExpressions()
                    .add(cb.equal(jobItem.get(JobItem_.currentAddr), department.get(Department_.address)));
            clauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));
            cq.where(clauses);
            cq.select(cb.construct(JobItemInLabDTO.class, jobItem.get(JobItem_.jobItemId),
                    jobItem.get(JobItem_.dueDate), category.get(CapabilityCategory_.id)));
            return cq;
        });
    }


    private Function<CriteriaBuilder, Function<CriteriaQuery<?>, Join<JobItem, ActiveClientJobItem>>>
    getActiveJobItemForDayWithComparator(LocalDate day, From<?, JobItem> jobItem, BiFunction<Expression<LocalDate>, LocalDate, Predicate> comparator) {
        return cb -> cq -> {
            val aji = jobItem.join(JobItem_.activeClientJobItem, JoinType.LEFT);
            aji.on(comparator.apply(aji.get(ActiveClientJobItem_.dueDate), day));
            return aji;
        };
    }

    private Function<CriteriaBuilder, Function<CriteriaQuery<?>, Join<JobItem, ActiveClientJobItem>>>
    getActiveJobItemForDay(LocalDate day, From<?, JobItem> jobItem) {
        return cb -> getActiveJobItemForDayWithComparator(day, jobItem, cb::equal).apply(cb);
    }

    public Map<Tuple2<Integer, Boolean>, LabCounts> getLabWorkCounts() {
        return getResultList(cb -> {
            val cq = cb.createQuery(LabCountsRaw.class);
            val jobItem = cq.from(JobItem.class);
            val departments = jobItem.join(JobItem_.currentAddr).join(Address_.departments);
            val itemState = jobItem.join(JobItem_.state);
            val stateGroupLink = itemState.join(ItemState_.groupLinks);
            val capability = jobItem.join(JobItem_.capability);
            val categorisedCapability = capability.join(Capability_.categories);
            val capabilityCategory = categorisedCapability.join(CategorisedCapability_.category);
            val filter = capability.join(Capability_.capabilityFilters, JoinType.LEFT);
            val calibrationType = filter.join(CapabilityFilter_.servicetype, JoinType.LEFT)
                    .join(ServiceType_.calibrationType, JoinType.LEFT);

            val currentDate = LocalDate.now(clock.withZone(LocaleContextHolder.getTimeZone().toZoneId()));
            val overdue = getActiveJobItemForDayWithComparator(currentDate.plusDays(1), jobItem, cb::lessThan)
                    .apply(cb).apply(cq);
            val dayOne = getActiveJobItemForDay(currentDate.plusDays(1), jobItem)
                    .apply(cb).apply(cq);
            val dayTwo = getActiveJobItemForDay(currentDate.plusDays(2), jobItem)
                    .apply(cb).apply(cq);
            val dayThree = getActiveJobItemForDay(currentDate.plusDays(3), jobItem)
                    .apply(cb).apply(cq);
            val dayFour = getActiveJobItemForDay(currentDate.plusDays(4), jobItem)
                    .apply(cb).apply(cq);
            val dayFive = getActiveJobItemForDayWithComparator(currentDate.plusDays(4), jobItem, cb::greaterThan)
                    .apply(cb).apply(cq);

            val bus = jobItem.join(JobItem_.activeBusinessJobItem, JoinType.LEFT);
            cq.where(
                    cb.isTrue(itemState.get(ItemState_.active)),
                    cb.equal(capabilityCategory.get(CapabilityCategory_.department), departments),
                    cb.equal(stateGroupLink.get(StateGroupLink_.groupId), 36)
            );
            cq.select(cb.construct(LabCountsRaw.class,
                    capabilityCategory.get(CapabilityCategory_.id),
                    calibrationType.get(CalibrationType_.accreditationSpecific),
                    cb.countDistinct(jobItem.get(JobItem_.jobItemId)),
                    cb.countDistinct(overdue),
                    cb.countDistinct(dayOne),
                    cb.countDistinct(dayTwo),
                    cb.countDistinct(dayThree),
                    cb.countDistinct(dayFour),
                    cb.countDistinct(dayFive),
                    cb.countDistinct(bus)
            ));
            cq.groupBy(capabilityCategory.get(CapabilityCategory_.id), calibrationType.get(CalibrationType_.accreditationSpecific));

            return cq;
        }).stream()
                .collect(Collectors.toMap(r -> io.vavr.Tuple.of(r.getId(), r.getAcreditationSpecific() != null && r.getAcreditationSpecific()),
                        LabCountsRaw::toLabCounts, LabCounts::mergeLabCounts));
    }

    @Override
    public List<Integer> getCapabilitiesIdsAssignedToCategory(CapabilityCategory pc) {
        return getResultList(cb -> {
            val cq = cb.createQuery(Integer.class);
            val capability = cq.from(Capability.class);
            cq.where(cb.equal(capability.join(Capability_.categories).get(CategorisedCapability_.category), pc));
            cq.select(capability.get(Capability_.id));
            return cq;
        });
    }

    @Override
    public List<Integer> getCapabilitiesIdsAssignedToDeptartment(Department dept) {
        return getResultList(cb -> {
            val cq = cb.createQuery(Integer.class);
            val capability = cq.from(Capability.class);
            cq.where(cb.equal(capability.join(Capability_.categories).join(CategorisedCapability_.category).get(CapabilityCategory_.department), dept));
            cq.select(capability.get(Capability_.id));
            return cq;
        });
    }

    @Override
    public List<StateGroup> getStateGroupsAssignedToDeptType(DepartmentType deptType) {
        return getResultStream(cb -> {
            val cq = cb.createQuery(Integer.class);
            val workAssigment = cq.from(WorkAssignment.class);
            cq.where(cb.equal(workAssigment.get(WorkAssignment_.deptType), deptType));
            cq.select(workAssigment.get(WorkAssignment_.stateGroupId));
            return cq;
        }).map(StateGroup::getStateGroupById).collect(Collectors.toList());
    }

	public List<StateGroup> getStateGroupsForDepartmentTypes(Collection<DepartmentType> types) {
        return getResultStream(cb -> {
            val cq = cb.createQuery(Integer.class);
            val workAssigment = cq.from(WorkAssignment.class);
            cq.where(workAssigment.get(WorkAssignment_.deptType).in(types));
            cq.select(workAssigment.get(WorkAssignment_.stateGroupId));
            return cq;
        }).map(StateGroup::getStateGroupById).collect(Collectors.toList());
    }
}