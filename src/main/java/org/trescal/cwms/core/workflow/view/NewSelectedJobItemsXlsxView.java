package org.trescal.cwms.core.workflow.view;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.trescal.cwms.core.jobs.job.views.AbstractJobItemXlsxStreamingView;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;
import org.trescal.cwms.core.workflow.service.NewSelectedJobItemsModel;

@Component
public class NewSelectedJobItemsXlsxView extends AbstractJobItemXlsxStreamingView {

	@Autowired
	private LocaleResolver localeResolver;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SXSSFWorkbook streamingWorkbook = (SXSSFWorkbook) workbook;
		Sheet sheet = workbook.createSheet("Data");
		XlsxViewDecorator decorator = new XlsxViewDecorator(sheet, streamingWorkbook, true, null);
		Locale locale = this.localeResolver.resolveLocale(request);
		NewSelectedJobItemsModel modelObject = (NewSelectedJobItemsModel) model.get("model");
		Map<Integer, Integer> columnWidthMap = writeLabels(decorator, locale, true);
		writeResults(decorator, modelObject.getJobDtos(), true);
		decorator.autoSizeColumns();
		resizeSelectedColumns(sheet, columnWidthMap);
		
		response.setHeader("Content-Disposition", "attachment; filename=\"jobitems_export.xlsx\"");
	}
}
