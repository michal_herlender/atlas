package org.trescal.cwms.core.workflow.entity.outcomestatus.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

@Repository("OutcomeStatusDao")
public class OutcomeStatusDaoImpl extends BaseDaoImpl<OutcomeStatus, Integer> implements OutcomeStatusDao
{
	@Override
	protected Class<OutcomeStatus> getEntity() {
		return OutcomeStatus.class;
	}
	
	public OutcomeStatus findOutcomeStatusWithActivityAndStatus(int activityId, int statusId) {
		Criteria criteria = getSession().createCriteria(OutcomeStatus.class);
		criteria.createCriteria("activity").add(Restrictions.idEq(activityId));
		criteria.createCriteria("status").add(Restrictions.idEq(statusId));
		return (OutcomeStatus) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<OutcomeStatus> getOutcomeStatusesForActivity(int activityId, boolean includeLookups) {
		Criteria crit = getSession().createCriteria(OutcomeStatus.class);
		crit.createCriteria("activity").add(Restrictions.idEq(activityId));
		if (!includeLookups) {
			crit.add(Restrictions.isNotNull("status"));
			crit.createCriteria("status").addOrder(Order.asc("description"));
		}
		else  crit.addOrder(Order.asc("id"));
		return crit.list();
	}
}