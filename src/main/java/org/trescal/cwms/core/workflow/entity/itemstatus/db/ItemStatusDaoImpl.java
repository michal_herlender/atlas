package org.trescal.cwms.core.workflow.entity.itemstatus.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Repository("ItemStatusDao")
public class ItemStatusDaoImpl extends BaseDaoImpl<ItemStatus, Integer> implements ItemStatusDao {
	
	@Override
	protected Class<ItemStatus> getEntity() {
		return ItemStatus.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItemActivity> findActivities() {
		Criteria actCriteria = getSession().createCriteria(ItemStatus.class);
		actCriteria.add(Restrictions.sqlRestriction("lower({alias}.activity) = 'y'"));
		actCriteria.addOrder(Order.asc("description"));
		List<ItemActivity> list = actCriteria.list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItemStatus> findStatuses() {
		Criteria actCriteria = getSession().createCriteria(ItemStatus.class);
		actCriteria.add(Restrictions.sqlRestriction("lower({alias}.status) = 'y'"));
		actCriteria.addOrder(Order.asc("description"));
		List<ItemStatus> list = actCriteria.list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItemStatus> searchItemStatus(String description) {
		Criteria itemStatusCriteria = getSession().createCriteria(ItemStatus.class);
		itemStatusCriteria.add(Restrictions.sqlRestriction("lower({alias}.description) like lower('" + description + "%')"));
		itemStatusCriteria.addOrder(Order.asc("description"));
		return itemStatusCriteria.list();
	}
}