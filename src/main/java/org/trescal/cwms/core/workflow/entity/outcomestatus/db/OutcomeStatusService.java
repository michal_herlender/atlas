package org.trescal.cwms.core.workflow.entity.outcomestatus.db;

import java.util.List;

import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

public interface OutcomeStatusService
{
	/**
	 * Deletes the given {@link OutcomeStatus} from the database.
	 * 
	 * @param outcomestatus the {@link OutcomeStatus} to delete.
	 */
	void deleteOutcomeStatus(OutcomeStatus outcomestatus);

	/**
	 * Returns the {@link OutcomeStatus} with the given ID.
	 * 
	 * @param id the {@link OutcomeStatus} ID.
	 * @return the {@link OutcomeStatus}
	 */
	OutcomeStatus findOutcomeStatus(int id);

	/**
	 * Returns the {@link OutcomeStatus} with an activity with the given ID and
	 * status with the given ID, or null if this {@link OutcomeStatus} doesn't
	 * yet exist.
	 * 
	 * @param activityId the {@link ItemActivity} ID.
	 * @param statusId the {@link ItemStatus} ID.
	 * @return the {@link OutcomeStatus} or null.
	 */
	OutcomeStatus findOutcomeStatusWithActivityAndStatus(int activityId, int statusId);

	/**
	 * Returns a {@link List} of all {@link OutcomeStatus} entities in the
	 * database.
	 * 
	 * @return the {@link List} of {@link OutcomeStatus} entities.
	 */
	List<OutcomeStatus> getAllOutcomeStatuss();

	/**
	 * Returns all {@link OutcomeStatus}es for the {@link ItemActivity} with
	 * the given ID. Note: This method only returns {@link OutcomeStatus}es
	 * that point to actual {@link ItemStatus}es and NOT the ones which point
	 * to Lookups.
	 * 
	 * @param activityId the {@link ItemActivity} ID.
	 * @return the {@link List} of {@link OutcomeStatus}es.
	 */
	List<OutcomeStatus> getOutcomeStatusesForActivity(int activityId, boolean includeLookups);

	/**
	 * Inserts the given {@link OutcomeStatus} into the database.
	 * 
	 * @param outcomestatus the {@link OutcomeStatus} to insert.
	 */
	void insertOutcomeStatus(OutcomeStatus outcomestatus);

	/**
	 * Updates the given {@link OutcomeStatus} if it exists in the database,
	 * otherwise inserts it.
	 * 
	 * @param outcomestatus the {@link OutcomeStatus} to update or insert.
	 */
	void saveOrUpdateOutcomeStatus(OutcomeStatus outcomestatus);

	/**
	 * Updates the given {@link OutcomeStatus} in the database.
	 * 
	 * @param outcomestatus the {@link OutcomeStatus} to update.
	 */
	void updateOutcomeStatus(OutcomeStatus outcomestatus);
}