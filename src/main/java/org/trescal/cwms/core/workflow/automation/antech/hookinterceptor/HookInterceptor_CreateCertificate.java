package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;

@Service
public class HookInterceptor_CreateCertificate extends HookInterceptor<HookInterceptor_CreateCertificate.Input> {

	public static final String HOOK_NAME = "create-certificate";
	public static final Integer CERTIFICATE_DEFAULT_TIME_SPENT = 5;

	@Autowired
	private JobItemService jobItemService;

	public static class Input {
		public CertLink cl;
		public Integer timeSpent;
		public Contact contact;
		public Date startStamp;
		public Date endStamp;

		public Input(CertLink cl, Integer timeSpent) {
			super();
			this.cl = cl;
			this.timeSpent = timeSpent;
		}

		public Input(CertLink cl, Contact contact, Integer timeSpent, Date startStamp, Date endDate) {
			super();
			this.cl = cl;
			this.timeSpent = timeSpent;
			this.contact = contact;
			this.startStamp = startStamp;
			this.endStamp = endDate;
		}

	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: CREATE CERTIFICATE");
			JobItem ji = input.cl.getJobItem();

			// Removed/commented out the reload below - GB 2016-02-19 - should
			// be unnecessary now.
			// reload job item (to prevent item status being out of date)
			// MUST NOT MERGE/UPDATE THE ITEM FIRST - THIS BRINGS STATUS ISSUES
			// ji = (JobItem) this.hookServ.findObject(ji.getJobItemId(),
			// JobItem.class);

			// In order to be able to create certificates during calibration,
			// updated so
			// that updates only occur if item is NOT in calibration, otherwise
			// OpenActivityException occurs.
			// GB 2016-02-19
			if (!this.jobItemService.isInCalibration(ji)) {
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

				// cannot rely solely on getCurrentContact here because creating
				// a
				// cert may be a background task, and in those cases will not
				// have a
				// current contact in the session
				// unless it is provided explicitly
				Contact con;
				if (input.contact != null)
					con = input.contact;
				else {
					Contact sessionCon = this.sessionServ.getCurrentContact();
					con = (sessionCon != null) ? sessionCon : input.cl.getCert().getRegisteredBy();
				}

				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				if (input.startStamp != null)
					jia.setStartStamp(input.startStamp);
				else
					jia.setStartStamp(new Date());
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());

				if (context.isCompleteActivity()) {
					// set end date
					if (input.endStamp != null)
						jia.setEndStamp(input.endStamp);
					else if (input.timeSpent != null)
						jia.setEndStamp(DateUtils.addMinutes(jia.getStartStamp(), input.timeSpent));
					else
						jia.setEndStamp(new Date());
					// set timespent
					if (input.timeSpent == null)
						jia.setTimeSpent(CERTIFICATE_DEFAULT_TIME_SPENT);
					else
						jia.setTimeSpent(input.timeSpent);
					jia.setCompletedBy(con);
				}

				ItemStatus endStatus = super.getNextStatus(context);
				ji.setState(endStatus);

				jobItemService.updateJobItem(ji);

				if (context.isCompleteActivity()) {
					jia.setEndStatus(endStatus);
				}

				this.persist(jia, endStatus, context);
			}
		}
	}
}