package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_NeedRepairCompletionReportValidation implements LookupResultMessage {

	YES("yes"),
	NO("no");

	private String messageCode;

	private Lookup_NeedRepairCompletionReportValidation(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}

}
