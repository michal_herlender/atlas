package org.trescal.cwms.core.workflow.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.proxy.HibernateProxyHelper;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.holdactivity.HoldActivity;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;
import org.trescal.cwms.core.workflow.entity.overrideactivity.OverrideActivity;
import org.trescal.cwms.core.workflow.entity.progressactivity.ProgressActivity;
import org.trescal.cwms.core.workflow.entity.transitactivity.TransitActivity;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;

/*
 * Standalone class to create a List of nodes in format needed for jstree component for a specific ItemState root
 * Galen Beck - 2015-10-20
 */
public class WorkflowDescriptionLinearBuilder {
	public static final String KEY_ID = "id";
	public static final String KEY_TEXT = "text";
	public static final String KEY_CHILDREN = "children";
	
	private List<WorkflowNode> rootList;
	private Set<ItemState> visitedItemStates;
	private Set<LookupInstance> visitedLookupInstances;
	private int nextId;
	public WorkflowDescriptionLinearBuilder(ItemActivity rootItemActivity) {
		this.visitedItemStates = new HashSet<>();
		this.visitedLookupInstances = new HashSet<>();
		this.rootList = new ArrayList<WorkflowNode>();
		addToListFromItemActivity(rootList, rootItemActivity);
		this.nextId = 1;
	}
	
	public List<WorkflowNode> getRootList() {
		return rootList;
	}

	/*
	 * Either parent or child node, is child node until children added
	 */
	private WorkflowNode makeNode(String text) {
		return new WorkflowNode(nextId++, text);
	}
	
	private void addToListFromItemActivity(List<WorkflowNode> list, ItemActivity itemActivity) {
		if (visitedItemStates.contains(itemActivity)) {
			list.add(makeNode("See "+getText(itemActivity)));
			return;
		}
		visitedItemStates.add(itemActivity);
		WorkflowNode node = makeNode(getText(itemActivity));
		list.add(node);
		
		// ItemActivity can have either lookupinstance or outcomestatus(es) or actionoutcome(s) 
		if (itemActivity.getLookup() != null) {
			list.add(recursiveNodeFromLookup(itemActivity.getLookup()));
		}
		else if (itemActivity.getOutcomeStatuses().size() == 1) {
			OutcomeStatus outcomeStatus = itemActivity.getOutcomeStatuses().first(); 
			addToListFromOutcomeStatus(list, outcomeStatus);
		}
		else if (itemActivity.getOutcomeStatuses().size() > 1) {
			for (OutcomeStatus outcomeStatus : itemActivity.getOutcomeStatuses()) {
				addToListFromOutcomeStatus(node.getChildren(), outcomeStatus);
			}
		}
		else if (itemActivity.getActionOutcomes().size() == 1) {
			ActionOutcome actionOutcome = itemActivity.getActionOutcomes().iterator().next();
			addToListFromActionOutcome(list, actionOutcome);
		}
		else if (itemActivity.getActionOutcomes().size() > 1) {
			for (ActionOutcome actionOutcome : itemActivity.getActionOutcomes()) {
				addToListFromActionOutcome(node.getChildren(), actionOutcome);
			}
		}
	}
	private void addToListFromItemStatus(List<WorkflowNode> list, ItemStatus itemStatus) {
		if (visitedItemStates.contains(itemStatus)) {
			list.add(makeNode("See "+getText(itemStatus)));
			return;
		}
		visitedItemStates.add(itemStatus);
		WorkflowNode node = makeNode(getText(itemStatus));
		list.add(node);
		// ItemStatus should have 1 or more nextActivities
		if (itemStatus.getNextActivities().size() == 1) {
			NextActivity nextActivity = itemStatus.getNextActivities().iterator().next(); 
			addToListFromNextActivity(list, nextActivity);
		}
		else if (itemStatus.getNextActivities().size() > 1) {
			for (NextActivity nextActivity : itemStatus.getNextActivities()) {
				addToListFromNextActivity(node.getChildren(), nextActivity);
			}
		}
	}
	private WorkflowNode recursiveNodeFromLookup(LookupInstance lookupInstance) {
		if (visitedLookupInstances.contains(lookupInstance)) {
			return makeNode("See LookupInstance "+lookupInstance.getId());
		}
		visitedLookupInstances.add(lookupInstance);
		WorkflowNode node = makeNode(getText(lookupInstance));
		// It's expected that a lookup will / should always have multiple results
		for (LookupResult lookupResult : lookupInstance.getLookupResults()) {
			node.addChild(recursiveNodeFromLookupResult(lookupResult));
		}
		return node;
	}
	private WorkflowNode recursiveNodeFromLookupResult(LookupResult lookupResult) {
		WorkflowNode node = makeNode(getText(lookupResult));
		if (lookupResult.getOutcomeStatus() != null) {
			addToListFromOutcomeStatus(node.getChildren(), lookupResult.getOutcomeStatus());
		}
		return node;
	}
	private void addToListFromOutcomeStatus(List<WorkflowNode> list, OutcomeStatus outcomeStatus) {
		// Outcome status is really a join to status or lookup so not explicitly displayed
		if (outcomeStatus.getStatus() != null) {
			addToListFromItemStatus(list, outcomeStatus.getStatus());
		}
		else if (outcomeStatus.getLookup() != null) {
			list.add(recursiveNodeFromLookup(outcomeStatus.getLookup()));
		}
	}
	private void addToListFromActionOutcome(List<WorkflowNode> list, ActionOutcome actionOutcome) {
		WorkflowNode node = makeNode(getText(actionOutcome));
		list.add(node);
		addToListFromItemActivity(list, actionOutcome.getActivity());
	}
	private void addToListFromNextActivity(List<WorkflowNode> list, NextActivity nextActivity) {
		// NextActivity is really a join to activity so not explicitly displayed
		addToListFromItemActivity(list, nextActivity.getActivity());
	}
	private String getText(ActionOutcome actionOutcome) {
		StringBuffer result = new StringBuffer();
		result.append("Action Outcome");
		result.append(" ");
		result.append(actionOutcome.getId());
		return result.toString();
	}
	private String getText(ItemState itemState) {
		/*
		 *  Note, due to Javassist the simple name returns things like ItemStatus_$$_javassist_63 hence the use
		 *  of HibernateProxyHelper
		 *  Using if rather than switch for type safety
		 */
		StringBuffer result = new StringBuffer();
		Class<?> clazz = HibernateProxyHelper.getClassWithoutInitializingProxy(itemState);
		if (HoldActivity.class.equals(clazz)) result.append("Hold Activity");
		else if (OverrideActivity.class.equals(clazz)) result.append("Override Activity");
		else if (ProgressActivity.class.equals(clazz)) result.append("Progress Activity");
		else if (TransitActivity.class.equals(clazz)) result.append("Transit Activity");
		else if (WorkActivity.class.equals(clazz)) result.append("Work Activity");
		else if (HoldStatus.class.equals(clazz)) result.append("Hold Status");
		else if (WorkStatus.class.equals(clazz)) result.append("Work Status");
		else if (ItemStatus.class.equals(clazz)) result.append("Item Status");
		else if (ItemActivity.class.equals(clazz)) result.append("Item Activity");
		else result.append(itemState.getClass().getSimpleName());
		
		result.append(" ");
		result.append(itemState.getStateid());
		result.append(" ");
		// TODO add translations for locale 
		result.append(itemState.getDescription());
		return result.toString();
	}
	private String getText(LookupInstance lookupInstance) {
		StringBuffer result = new StringBuffer();
		result.append("LookupInstance");
		result.append(" ");
		result.append(lookupInstance.getId());
		result.append(" ");
		// TODO add translations for locale 
		result.append(lookupInstance.getLookupFunction().getBeanName());
		return result.toString();
	}
	private String getText(LookupResult lookupResult) {
		StringBuffer result = new StringBuffer();
		result.append("LookupResult");
		result.append(" ");
		result.append(lookupResult.getId());
		result.append(" ");
		result.append(lookupResult.getResultMessageId());
		result.append(" ");
		result.append(lookupResult.getResultMessage());
		return result.toString();
	}
}