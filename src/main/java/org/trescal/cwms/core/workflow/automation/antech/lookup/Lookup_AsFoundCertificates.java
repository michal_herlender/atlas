package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_AsFoundCertificates implements LookupResultMessage
{
	RECORDED_PREFERENCE("lookupresultmessage.asfoundcertificates.recorded"),
	NO_RECORDED_PREFERENCE("lookupresultmessage.asfoundcertificates.norecorded");
	
	private String messageCode;
	
	private Lookup_AsFoundCertificates(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getMessageCode() {
		return this.messageCode;
	}
}
