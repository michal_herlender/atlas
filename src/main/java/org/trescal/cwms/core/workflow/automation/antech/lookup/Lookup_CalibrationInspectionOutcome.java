package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_CalibrationInspectionOutcome implements LookupResultMessage
{
	INHOUSE_CALIBRATION("lookupresultmessage.calibrationinspectionoutcome.inhousecalibration"),
	THIRDPARTY_CALIBRATION("lookupresultmessage.calibrationinspectionoutcome.thirdpartycalibration"),
	THIRDPARTY_REPAIR("lookupresultmessage.calibrationinspectionoutcome.thirdpartyrepair"),
	INHOUSE_REPAIR("lookupresultmessage.calibrationinspectionoutcome.inhouserepair"),
	ADJUSTMENT("lookupresultmessage.calibrationinspectionoutcome.adjustment"),
	UNIT_BER("lookupresultmessage.calibrationinspectionoutcome.unitber");
	
	private String messageCode;
	
	private Lookup_CalibrationInspectionOutcome(String messageCode) {
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return messageCode;
	}
}
