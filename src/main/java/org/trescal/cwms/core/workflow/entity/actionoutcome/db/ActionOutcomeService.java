package org.trescal.cwms.core.workflow.entity.actionoutcome.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.spring.model.KeyValue;

public interface ActionOutcomeService extends BaseService<ActionOutcome, Integer> {

	ActionOutcome createActionOutcome(ActionOutcomeType type, String value);

	ActionOutcome findDefaultActionOutcomeForActivity(int activityId);

	List<ActionOutcome> getActionOutcomesForActivityId(int activityId);

	List<ActionOutcome> getActionOutcomesForActivity(ItemActivity activity);

	ActionOutcome getByTypeAndValue(ActionOutcomeType type, ActionOutcomeValue value);

	List<KeyValue<Integer, String>> getActionOutcomesTranslatedForActivityId(int activityId, Locale locale);

	List<KeyValue<Integer, String>> getActionOutcomesByType(ActionOutcomeType actionOutcomeType,
			Locale locale);
}