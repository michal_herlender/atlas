package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.JobItemToCalLinkComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CertIssuedFromCal;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_CertIssuedFromCal")
public class Lookup_CertIssuedFromCalService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether an item had a certificate issued for it's last calibration.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: CERT ISSUED FROM CAL");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result;
		if ((ji.getCalLinks() != null) && (ji.getCalLinks().size() > 0)) {
			logger.trace("Analyzing " + ji.getCalLinks().size() + " cal links");
			TreeSet<CalLink> links = new TreeSet<>(new JobItemToCalLinkComparator());
			links.addAll(ji.getCalLinks());
			CalLink lastCal = links.last();
			logger.trace("Analyzing cal link " + lastCal.getId() + " for cal " + lastCal.getCal().getId());
			logger.trace(Boolean.toString(lastCal.isCertIssued()));
			result = (lastCal.isCertIssued() ? Lookup_CertIssuedFromCal.WAS_ISSUED
					: Lookup_CertIssuedFromCal.NOT_ISSUED);
		} else {
			logger.trace("No cal links, cert not issued");
			result = Lookup_CertIssuedFromCal.NOT_ISSUED;
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_CertIssuedFromCal.values();
	}
}