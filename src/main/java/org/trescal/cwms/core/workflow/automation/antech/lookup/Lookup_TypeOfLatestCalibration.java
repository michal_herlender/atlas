package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_TypeOfLatestCalibration implements LookupResultMessage
{
	PRE_CALIBRATION("lookupresultmessage.typeoflatestcalibration.precalibration"),
	POST_CALIBRATION("lookupresultmessage.typeoflatestcalibration.postcalibration");
	
	private String messageCode;
	
	private Lookup_TypeOfLatestCalibration(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}