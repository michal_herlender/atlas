package org.trescal.cwms.core.workflow.entity.lookupresult.db;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

@Service("LookupResultService")
public class LookupResultServiceImpl implements LookupResultService
{
	@Autowired
	private LookupResultDao lookupResultDao;

	public Logger logger = LoggerFactory.getLogger(LookupResultServiceImpl.class);

	@Override
	public void deleteLookupResult(LookupResult lookupresult)
	{
		this.lookupResultDao.remove(lookupresult);
	}

	@Override
	public LookupResult findLookupResult(int id)
	{
		return this.lookupResultDao.find(id);
	}

	@Override
	public LookupResult findLookupResultForInstanceAndMessage(Integer lookupInstanceId, LookupResultMessage result) {
		return this.lookupResultDao.findLookupResultForInstanceAndMessage(lookupInstanceId, result);
	}

	@Override
	public List<LookupResult> getAllLookupResults()
	{
		return this.lookupResultDao.findAll();
	}

	@Override
	public List<LookupResult> getAllLookupResultsForActivity(int activityId)
	{
		return this.lookupResultDao.getAllLookupResultsForActivity(activityId);
	}

	@Override
	public OutcomeStatus getResult(Integer lookupInstanceId, LookupResultMessage result)
	{
		logger.debug("lookupInstanceId: "+lookupInstanceId+" result: "+result.ordinal());
		LookupResult lr = this.findLookupResultForInstanceAndMessage(lookupInstanceId, result);

		OutcomeStatus os = (lr == null) ? null : lr.getOutcomeStatus();
		return os;
	}

	@Override
	public void insertLookupResult(LookupResult LookupResult)
	{
		this.lookupResultDao.persist(LookupResult);
	}
}