package org.trescal.cwms.core.workflow.entity.lookupinstance.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;

public interface LookupInstanceDao extends BaseDao<LookupInstance, Integer>
{

	List<LookupInstance> findLookupInstanceByName(String name);
}