package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ContractReviewOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Component("Lookup_ContractReviewOutcome")
public class Lookup_ContractReviewOutcomeService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the outcome of the item's contract review.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: CONTRACT REVIEW OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions to get the MOST RECENT contract review
		// in case there is more than one
		for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;
				if ((activity.getActivityDesc().equals("Contract review")
						|| activity.getActivityDesc().equals("Onsite contract review")) && !activity.isDeleted()) {
					// get the outcome of the contract review
					ao = activity.getOutcome();
					break;
				}
			}
		}
		if (ao != null) {
			String desc = ao.getDescription();
			if (desc.contains("in-house calibration")) {
				result = Lookup_ContractReviewOutcome.INHOUSE_CALIBRATION;
			} else if (desc.contains("third party calibration")) {
				result = Lookup_ContractReviewOutcome.THIRD_PARTY_CALIBRATION;
			} else if (desc.contains("in-house repair")) {
				result = Lookup_ContractReviewOutcome.INHOUSE_REPAIR;
			} else if (desc.contains("third party repair")) {
				result = Lookup_ContractReviewOutcome.THIRD_PARTY_REPAIR;
			} else if (desc.contains("adjustment")) {
				result = Lookup_ContractReviewOutcome.ADJUSTMENT;
			} else if (desc.contains("inspection")) {
				result = Lookup_ContractReviewOutcome.INSPECTION;
			} else if (desc.contains("client purchase order")) {
				result = Lookup_ContractReviewOutcome.AWAITING_CPO;
			} else if (desc.contains("client feedback")) {
				result = Lookup_ContractReviewOutcome.AWAITING_FEEDBACK;
			} else if (desc.contains("quotation to client")) {
				result = Lookup_ContractReviewOutcome.AWAITING_QUOTATION;
			} else if (desc.contains("No action")) {
				result = Lookup_ContractReviewOutcome.NO_ACTION;
			} else if (desc.contains("on-site calibration")) {
				result = Lookup_ContractReviewOutcome.ONSITE_CALIBRATION;
			} else if (desc.contains("Failure Report")) {
				result = Lookup_ContractReviewOutcome.REQUIRES_FAILURE_REPORT;
			}
			// Logging
			if (result == null) {
				logger.error("No lookup result for description: " + desc);
			} else {
				logger.debug("Lookup result: " + result);
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ContractReviewOutcome.values();
	}
}