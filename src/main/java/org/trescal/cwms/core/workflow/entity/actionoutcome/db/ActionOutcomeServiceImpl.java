package org.trescal.cwms.core.workflow.entity.actionoutcome.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_Adjustment;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_Calibration;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ClientDecisionAsFound;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ClientDecisionCosting;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ClientDecisionExchange;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ClientDecisionFurtherWork;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ClientDecisionTPFurtherWork;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ClientDecisionTPInspectionCosts;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ClientDecisionTPJobCosting;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ComponentsFitted;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_ContractReview;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_EngineerInspection;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_PostTPFurtherWork;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_Repair;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_TPRequirements;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Adjustment;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionAsFound;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionCosting;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionFurtherWork;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionTPFurtherWork;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ComponentsFitted;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ContractReview;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_EngineerInspection;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_PostTPFurtherWork;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Repair;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_TPRequirements;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.spring.model.KeyValue;

@Service("ActionOutcomeService")
public class ActionOutcomeServiceImpl extends BaseServiceImpl<ActionOutcome, Integer> implements ActionOutcomeService {
	@Autowired
	private ActionOutcomeDao actionOutcomeDao;

	@Autowired
	private ItemStateService itemStateService;

	@Override
	protected BaseDao<ActionOutcome, Integer> getBaseDao() {
		return actionOutcomeDao;
	}

	/**
	 * Factory Method - Used to create specific concrete class of action outcome and
	 * and set value, description, translations (not expected to change) TODO - once
	 * all refactoring is done, delete description and translations Does not
	 * persist, caller should add to activity and use cascade to save
	 */
	@Override
	public ActionOutcome createActionOutcome(ActionOutcomeType type, String value) {
		ActionOutcome result;
		if (type == null)
			throw new IllegalArgumentException("type must not be null");
		if (value == null)
			throw new IllegalArgumentException("value must not be null");
		switch (type) {
		case ADJUSTMENT:
			result = new ActionOutcome_Adjustment();
			((ActionOutcome_Adjustment) result).setValue(ActionOutcomeValue_Adjustment.valueOf(value));
			break;
		case CALIBRATION:
			result = new ActionOutcome_Calibration();
			((ActionOutcome_Calibration) result).setValue(ActionOutcomeValue_Calibration.valueOf(value));
			break;
		case CLIENT_DECISION_AS_FOUND:
			result = new ActionOutcome_ClientDecisionAsFound();
			((ActionOutcome_ClientDecisionAsFound) result)
					.setValue(ActionOutcomeValue_ClientDecisionAsFound.valueOf(value));
			break;
		case CLIENT_DECISION_COSTING:
			result = new ActionOutcome_ClientDecisionCosting();
			((ActionOutcome_ClientDecisionCosting) result)
					.setValue(ActionOutcomeValue_ClientDecisionCosting.valueOf(value));
			break;
		case CLIENT_DECISION_EXCHANGE:
			result = new ActionOutcome_ClientDecisionExchange();
			((ActionOutcome_ClientDecisionExchange) result)
					.setValue(ActionOutcomeValue_ClientDecisionCosting.valueOf(value));
			break;
		case CLIENT_DECISION_FURTHER_WORK:
			result = new ActionOutcome_ClientDecisionFurtherWork();
			((ActionOutcome_ClientDecisionFurtherWork) result)
					.setValue(ActionOutcomeValue_ClientDecisionFurtherWork.valueOf(value));
			break;
		case CLIENT_DECISION_TP_FURTHER_WORK:
			result = new ActionOutcome_ClientDecisionTPFurtherWork();
			((ActionOutcome_ClientDecisionTPFurtherWork) result)
					.setValue(ActionOutcomeValue_ClientDecisionTPFurtherWork.valueOf(value));
			break;
		case CLIENT_DECISION_TP_INSPECTION_COSTING:
			result = new ActionOutcome_ClientDecisionTPInspectionCosts();
			((ActionOutcome_ClientDecisionTPInspectionCosts) result)
					.setValue(ActionOutcomeValue_ClientDecisionCosting.valueOf(value));
			break;
		case CLIENT_DECISION_TP_JOB_COSTING:
			result = new ActionOutcome_ClientDecisionTPJobCosting();
			((ActionOutcome_ClientDecisionTPJobCosting) result)
					.setValue(ActionOutcomeValue_ClientDecisionCosting.valueOf(value));
			break;
		case COMPONENTS_FITTED:
			result = new ActionOutcome_ComponentsFitted();
			((ActionOutcome_ComponentsFitted) result).setValue(ActionOutcomeValue_ComponentsFitted.valueOf(value));
			break;
		case CONTRACT_REVIEW:
			result = new ActionOutcome_ContractReview();
			((ActionOutcome_ContractReview) result).setValue(ActionOutcomeValue_ContractReview.valueOf(value));
			break;
		case ENGINEER_INSPECTION:
			result = new ActionOutcome_EngineerInspection();
			((ActionOutcome_EngineerInspection) result).setValue(ActionOutcomeValue_EngineerInspection.valueOf(value));
			break;
		case POST_TP_FURTHER_WORK:
			result = new ActionOutcome_PostTPFurtherWork();
			((ActionOutcome_PostTPFurtherWork) result).setValue(ActionOutcomeValue_PostTPFurtherWork.valueOf(value));
			break;
		case REPAIR:
			result = new ActionOutcome_Repair();
			((ActionOutcome_Repair) result).setValue(ActionOutcomeValue_Repair.valueOf(value));
			break;
		case TP_REQUIREMENTS:
			result = new ActionOutcome_TPRequirements();
			((ActionOutcome_TPRequirements) result).setValue(ActionOutcomeValue_TPRequirements.valueOf(value));
			break;

		default:
			throw new UnsupportedOperationException("Unsupported type : " + type);
		}
		return result;
	}

	@Override
	public ActionOutcome findDefaultActionOutcomeForActivity(int activityId) {
		return this.actionOutcomeDao.findDefaultActionOutcomeForActivity(activityId);
	}

	/**
	 * Called by DWR
	 */
	@Override
	public List<ActionOutcome> getActionOutcomesForActivityId(int activityId) {
		ItemActivity itemActivity = this.itemStateService.findItemActivity(activityId);
		return getActionOutcomesForActivity(itemActivity);
	}

	@Override
	public List<KeyValue<Integer, String>> getActionOutcomesTranslatedForActivityId(int activityId, Locale locale) {
		ItemActivity itemActivity = this.itemStateService.findItemActivity(activityId);
		List<ActionOutcome> aos = getActionOutcomesForActivity(itemActivity);
		List<KeyValue<Integer, String>> res = new ArrayList<>();
		for (ActionOutcome ao : aos) {
			res.add(new KeyValue<>(ao.getId(), ao.getGenericValue().getTranslationForLocale(locale)));
		}
		return res;
	}

	@Override
	public List<KeyValue<Integer, String>> getActionOutcomesByType(ActionOutcomeType actionOutcomeType,
			Locale locale) {
		Class<? extends ActionOutcome> clazz = actionOutcomeType.getOutcomeClass();
		List<ActionOutcome> aos = (List<ActionOutcome>) this.actionOutcomeDao.getByTypeAndValue(clazz, null);
		List<KeyValue<Integer, String>> res = new ArrayList<>();
		for (ActionOutcome ao : aos) {
			res.add(new KeyValue<Integer, String>(ao.getId(), ao.getGenericValue().getTranslationForLocale(locale)));
		}
		return res;
	}

	@Override
	public List<ActionOutcome> getActionOutcomesForActivity(ItemActivity activity) {
		if (activity == null)
			return null;
		else
			return activity.getActionOutcomes().stream().filter(ao -> ao.isActive()).collect(Collectors.toList());
	}

	@Override
	public ActionOutcome getByTypeAndValue(ActionOutcomeType actionOutcomeType, ActionOutcomeValue value) {
		Class<? extends ActionOutcome> clazz = actionOutcomeType.getOutcomeClass();
		return this.actionOutcomeDao.getByTypeAndValue(clazz, value).stream().findFirst().orElse(null);
	}
}