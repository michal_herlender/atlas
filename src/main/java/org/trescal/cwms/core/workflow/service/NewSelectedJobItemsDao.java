package org.trescal.cwms.core.workflow.service;

import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AbstractDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.Contract_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction_;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem_;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.procedure.dto.LaboratoryCategorySplit;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation_;
import org.trescal.cwms.core.workflow.entity.StateGroupType;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Repository
public class NewSelectedJobItemsDao extends AbstractDaoImpl {

	public List<JobItemProjectionDTO> getActiveJobItems(List<Integer> procIds, List<Integer> stateIds,
														Integer allocatedSubdivId, StateGroupLinkType sglType, Integer addressId, Locale locale) {
		return getActiveJobItemsInner(procIds, stateIds, null, null, false, allocatedSubdivId, sglType, addressId, LaboratoryCategorySplit.NONE, locale);
	}

	public List<JobItemProjectionDTO> getActiveJobItemsByDaysAllocationsAndBusiness(List<Integer> procIds, List<Integer> stateIds, Integer days,
																					Boolean includeBusiness, Integer allocatedSubdivId, StateGroupLinkType sglType, Integer addressId, LaboratoryCategorySplit laboratoryCategorySplit, Locale locale) {
		return getActiveJobItemsInner(procIds, stateIds, days, includeBusiness, null, allocatedSubdivId, sglType, addressId, laboratoryCategorySplit, locale);
	}

	private List<JobItemProjectionDTO> getActiveJobItemsInner(List<Integer> procIds, List<Integer> stateIds, Integer days,
															  Boolean includeBusiness, Boolean isAllocatedToEngineer, Integer allocatedSubdivId, StateGroupLinkType sglType,
															  Integer addressId, LaboratoryCategorySplit laboratoryCategorySplit, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemProjectionDTO> cq = cb.createQuery(JobItemProjectionDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, Subdiv> allocatedSubdiv = job.join(Job_.organisation.getName());
			Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr,
					JoinType.LEFT);
			Join<Address, Subdiv> currentSubdiv = currentAddress.join(Address_.sub,
					JoinType.LEFT);
			Join<JobItem, Address> calAddress = jobItem.join(JobItem_.calAddr,
					JoinType.LEFT);
			Join<Address, Subdiv> calSubdiv = calAddress.join(Address_.sub,
					JoinType.LEFT);
			Join<JobItem, Capability> capability = jobItem.join(JobItem_.capability,
					JoinType.LEFT);
			Join<Capability, CapabilityFilter> capabilityFilter = capability.join(Capability_.capabilityFilters, JoinType.LEFT);
			Join<ServiceType, CalibrationType> filterCalibrationType = capabilityFilter.join(CapabilityFilter_.servicetype, JoinType.LEFT)
					.join(ServiceType_.calibrationType, JoinType.LEFT);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst,
					JoinType.INNER);
			Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model,
					JoinType.INNER);
			Join<InstrumentModel, Translation> instrumentModelTranslation = instrumentModel.join(InstrumentModel_.nameTranslations,
					JoinType.LEFT);
			instrumentModelTranslation.on(cb.equal(instrumentModelTranslation.get(Translation_.locale), locale));
			Join<Instrument, Mfr> instrumentMfr = instrument.join(Instrument_.mfr,
					JoinType.LEFT);
			Join<InstrumentModel, Description> instrumentModelDescription = instrumentModel.join(InstrumentModel_.description,
                JoinType.INNER);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state,
					JoinType.LEFT);
			Join<JobItem, ServiceType> serviceType = jobItem.join(JobItem_.serviceType,
					JoinType.LEFT);
			Join<ServiceType, CalibrationType> calType = serviceType.join(ServiceType_.calibrationType,
					JoinType.LEFT);
			Join<JobItem, Contact> contractReviewBy = jobItem.join(JobItem_.lastContractReviewBy,
					JoinType.LEFT);
			Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks,
					JoinType.LEFT);
			Join<JobItem, JobItemAction> action = jobItem.join(JobItem_.actions,
					JoinType.LEFT);
			Join<JobItem, OnBehalfItem> onBehalf = jobItem.join(JobItem_.onBehalf, JoinType.LEFT);
			Join<OnBehalfItem, Company> onBehalfCompany = onBehalf.join(OnBehalfItem_.company, JoinType.LEFT);
			Join<JobItem, Contract> contract = jobItem.join(JobItem_.contract, JoinType.LEFT);

			Subquery<Integer> actionSubQuery = cq.subquery(Integer.class);
			Root<JobItemAction> sqActions = actionSubQuery.from(JobItemAction.class);
			actionSubQuery.where(cb.equal(sqActions.get(JobItemAction_.jobItem), jobItem));
			actionSubQuery.select(cb.max(sqActions.get(JobItemAction_.id)));
			action.on(cb.equal(action.get(JobItemAction_.id), actionSubQuery));
			
			// Some fields (dateComplete, hasContractReview) are not used hence nullLiteral

            cq.select(cb.construct(JobItemProjectionDTO.class, jobItem.get(JobItem_.jobItemId),
                job.get(Job_.jobid), jobItem.get(JobItem_.itemNo),

					instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
					instrument.get(Instrument_.serialno), instrument.get(Instrument_.customerDescription),
					instrument.get(Instrument_.calibrationStandard), instrument.get(Instrument_.scrapped),
					instrument.get(Instrument_.modelname), instrumentModel.get(InstrumentModel_.modelid),
					instrumentModelTranslation.get(Translation_.translation),
					instrumentModel.get(InstrumentModel_.modelMfrType), instrumentMfr.get(Mfr_.genericMfr),
					instrumentMfr.get(Mfr_.name), instrumentModelDescription.get(Description_.typology),

					serviceType.get(ServiceType_.serviceTypeId), cb.nullLiteral(String.class),
					capability.get(Capability_.id),
					itemState.get(ItemState_.stateid), onBehalfCompany.get(Company_.coid),
					contractReviewBy.get(Contact_.personid), jobItem.get(JobItem_.turn),
					cb.nullLiteral(Date.class), jobItem.get(JobItem_.dateIn),
					jobItem.get(JobItem_.dueDate), jobItem.get(JobItem_.clientRef),
					contract.get(Contract_.id), cb.nullLiteral(Boolean.class),
					action.get(JobItemAction_.id), action.get(JobItemAction_.startStamp),
					action.get(JobItemAction_.remark)
			));

			cq.distinct(true);
			Predicate clauses = cb.conjunction();
			Predicate allocatedSubdivPredicate = cb.equal(allocatedSubdiv.get(Subdiv_.subdivid), allocatedSubdivId);
			Predicate currentSubdivPredicate = cb.equal(currentSubdiv.get(Subdiv_.subdivid), allocatedSubdivId);
			Predicate allocatedNotCurrentSubdivPredicate = cb.and(cb.equal(allocatedSubdiv, allocatedSubdivId),
					cb.notEqual(currentSubdiv.get(Subdiv_.subdivid), allocatedSubdivId));
			Predicate calibrationSubdivPredicate = cb.disjunction();
			if (stateIds != null) {
				List<Integer> depGroups = StateGroup.getAllStateGroupsOfType(StateGroupType.DEPARTMENT).stream()
					.map(StateGroup::getId).collect(Collectors.toList());
				clauses.getExpressions().add(stateGroupLink.get(StateGroupLink_.groupId).in(depGroups));
				allocatedSubdivPredicate = cb.and(allocatedSubdivPredicate,
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.ALLOCATED_SUBDIV));
				currentSubdivPredicate = cb.and(currentSubdivPredicate,
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.CURRENT_SUBDIV));
				allocatedNotCurrentSubdivPredicate = cb.and(allocatedNotCurrentSubdivPredicate, 
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV));
				calibrationSubdivPredicate = cb.and(cb.equal(calSubdiv.get(Subdiv_.subdivid), allocatedSubdivId),
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.CALIBRATION_SUBDIV));
			}
			if (sglType == null) {
				Predicate sglDisjunction = cb.disjunction();
				sglDisjunction.getExpressions().add(allocatedSubdivPredicate);
				sglDisjunction.getExpressions().add(currentSubdivPredicate);
				sglDisjunction.getExpressions().add(allocatedNotCurrentSubdivPredicate);
				sglDisjunction.getExpressions().add(calibrationSubdivPredicate);
				clauses.getExpressions().add(sglDisjunction);
			} else if (sglType == StateGroupLinkType.ALLOCATED_SUBDIV) {
				clauses.getExpressions().add(allocatedSubdivPredicate);
			} else if (sglType == StateGroupLinkType.CURRENT_SUBDIV) {
				clauses.getExpressions().add(currentSubdivPredicate);
			} else if (sglType == StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV) {
				clauses.getExpressions().add(allocatedNotCurrentSubdivPredicate);
			} else if (sglType == StateGroupLinkType.CALIBRATION_SUBDIV) {
				clauses.getExpressions().add(calibrationSubdivPredicate);
			}
			if (procIds != null)
				if (procIds.isEmpty())
					clauses.getExpressions().add(cb.isNull(capability.get(Capability_.id)));
				else
					clauses.getExpressions().add(capability.get(Capability_.id).in(procIds));
			if (stateIds != null) {
				if (stateIds.isEmpty())
					clauses.getExpressions().add(cb.isNull(itemState.get(ItemState_.stateid)));
				else
					clauses.getExpressions().add(itemState.get(ItemState_.stateid).in(stateIds));
			} else
				clauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));
			if (days != null) {
				val localDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
				if (days <= 0) {
					clauses.getExpressions().add(cb.lessThanOrEqualTo(jobItem.get(JobItem_.dueDate), localDate));
				} else if (days >= 5) {
					clauses.getExpressions().add(cb.greaterThanOrEqualTo(jobItem.get(JobItem_.dueDate), localDate.plusDays(5)));
				} else {
					clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.dueDate), localDate.plusDays(days)));
				}
			}
			if (laboratoryCategorySplit != LaboratoryCategorySplit.NONE) {

				clauses.getExpressions().add(cb.equal(filterCalibrationType.get(CalibrationType_.accreditationSpecific),
						laboratoryCategorySplit == LaboratoryCategorySplit.ACCREDITED));
			}
			if (includeBusiness != null) {
				Join<Job, Contact> clientContact = job.join(Job_.con, javax.persistence.criteria.JoinType.LEFT);
				Join<Contact, Subdiv> clientSubdiv = clientContact.join(Contact_.sub);
				Join<Subdiv, Company> clientCompany = clientSubdiv.join(Subdiv_.comp);
				if (includeBusiness)
					clauses.getExpressions()
							.add(cb.equal(clientCompany.get(Company_.companyRole), CompanyRole.BUSINESS));
				else
					clauses.getExpressions()
							.add(cb.notEqual(clientCompany.get(Company_.companyRole), CompanyRole.BUSINESS));
			}
			if (isAllocatedToEngineer != null) {
				Subquery<Long> engineerAllocationSq = cq.subquery(Long.class);
				Root<EngineerAllocation> engineerAllocation = engineerAllocationSq.from(EngineerAllocation.class);
				Join<EngineerAllocation, JobItem> allocatedItem = engineerAllocation
						.join(EngineerAllocation_.itemAllocated);
				engineerAllocationSq.where(cb.equal(allocatedItem, jobItem));
				engineerAllocationSq.select(cb.count(engineerAllocation));
				if (isAllocatedToEngineer)
					clauses.getExpressions().add(cb.notEqual(engineerAllocationSq, 0L));
				else
					clauses.getExpressions().add(cb.equal(engineerAllocationSq, 0L));
			}
			if (addressId != null)
				clauses.getExpressions().add(cb.equal(currentAddress.get(Address_.addrid), addressId));
			cq.where(clauses);
			
			return cq;
		});
	}
	
	
}
