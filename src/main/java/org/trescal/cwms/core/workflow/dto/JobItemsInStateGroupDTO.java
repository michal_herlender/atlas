package org.trescal.cwms.core.workflow.dto;

import java.util.List;

import lombok.Value;

@Value(staticConstructor = "of")
public class JobItemsInStateGroupDTO {

	private Integer stateGroupId;
	private String stateGroupName;
	private List<JobItemsInStateDTO> jobItemsInState;

	public Long getJobItemsInStateGroup() {
		return jobItemsInState.stream().mapToLong(JobItemsInStateDTO::getJobItemsInState).sum();
	}
}