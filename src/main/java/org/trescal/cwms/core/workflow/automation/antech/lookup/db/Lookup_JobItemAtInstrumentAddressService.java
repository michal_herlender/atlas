package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_JobItemAtInstrumentAddress;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_JobItemAtInstrumentAddress")
public class Lookup_JobItemAtInstrumentAddressService extends Lookup {

	@Autowired
	private JobItemService jobItemService;

	@Override
	public String getDescription() {
		return "Determines whether the item at the instrument address or not";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM AT INSTRUMENT ADDRESS");
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		Address currentAddress = jobItem.getCurrentAddr();
		Address instrumentAddress = jobItem.getInst().getAdd();

		if (currentAddress != null && instrumentAddress != null
				&& instrumentAddress.getAddrid() == currentAddress.getAddrid()) {
			return Lookup_JobItemAtInstrumentAddress.YES;
		} else
			return Lookup_JobItemAtInstrumentAddress.NO;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_JobItemAtInstrumentAddress.values();
	}
}