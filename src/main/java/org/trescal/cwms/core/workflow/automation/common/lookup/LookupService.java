package org.trescal.cwms.core.workflow.automation.common.lookup;

import java.util.List;

public interface LookupService {

	List<LookupWrapper> getAllLookups();
}