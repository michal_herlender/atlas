package org.trescal.cwms.core.workflow.entity.actionoutcome.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;

public interface ActionOutcomeDao extends BaseDao<ActionOutcome, Integer> {

	ActionOutcome findDefaultActionOutcomeForActivity(int activityId);

	<T extends ActionOutcome> List<T> getByTypeAndValue(Class<T> clazz, ActionOutcomeValue value);

}