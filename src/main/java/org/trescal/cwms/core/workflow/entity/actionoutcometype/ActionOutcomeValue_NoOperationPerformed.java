package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_NoOperationPerformed implements ActionOutcomeValue {

	FAILURE_REPORT_NOT_REQUIRED("Failure Report not required", "actionoutcome.noactionperformed.failurereporetnotrequired"),
	FAILURE_REPORT_REQUIRED("Failure Report required", "actionoutcome.contractreview.requiredfailurereport");

	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_NoOperationPerformed(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.CLIENT_DECISION_FURTHER_WORK;
	}

}
