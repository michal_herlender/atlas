package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FaultReportsRequireDelivery;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_FaultReportsRequireDelivery")
public class Lookup_FaultReportsRequireDeliveryService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;
	
	@Autowired
	private JobItemService jiService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {

		logger.debug("RUNNING LOOKUP: Fault Reports Require Delivery ");
		FaultReport fr = faultReportService.getFirstFailureReportByJobitemIdForDelivery(jobItemId);

		if (fr != null)
			return Lookup_FaultReportsRequireDelivery.YES;
		else {
			JobItem ji = jiService.findJobItem(jobItemId);
			if(ji != null && JobType.SITE.equals(ji.getJob().getType()))
				return Lookup_FaultReportsRequireDelivery.NO_ONSITE;
			else
				return Lookup_FaultReportsRequireDelivery.NO_INHOUSE;	
		}
	}

	@Override
	public String getDescription() {
		return "based on failure report parameter, decide whether deliver, or not during the final inspection";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_FaultReportsRequireDelivery.values();
	}
}