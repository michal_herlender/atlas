package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FaultReportAwaitingUpdate;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_FaultReportAwaitingUpdate")
public class Lookup_FaultReportAwaitingUpdateService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public String getDescription() {
		return "Determines whether an item has a fault report which requires updates before the workflow should continue.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM FAULT REPORT AWAITING FINALISATION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		FaultReport faultReport = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), null);
		Lookup_FaultReportAwaitingUpdate result = null;
		result = (faultReport != null && faultReport.isAwaitingFinalisation()) ? Lookup_FaultReportAwaitingUpdate.YES
				: Lookup_FaultReportAwaitingUpdate.NO;
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_FaultReportAwaitingUpdate.values();
	}
}