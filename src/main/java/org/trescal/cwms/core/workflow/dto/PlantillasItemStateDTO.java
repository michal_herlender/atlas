package org.trescal.cwms.core.workflow.dto;

public class PlantillasItemStateDTO {
	private Integer stateid;
	private Boolean active;
	private Boolean retired;
	private String description;
	private String descriptionFallback;
	private Integer sglAwaitingCalibration;
	private Integer sglResumeCalibration;
	private Integer sglCalibration;
	
	/*
	 * Default constructor for existing Hibernate Criteria API
	 */
	public PlantillasItemStateDTO() {
	}

	/*
	 * Full constructor for JPA API
	 */
	public PlantillasItemStateDTO(Integer stateid, Boolean active, Boolean retired, String description,
			String descriptionFallback, Integer sglAwaitingCalibration, Integer sglResumeCalibration,
			Integer sglCalibration) {
		super();
		this.stateid = stateid;
		this.active = active;
		this.retired = retired;
		this.description = description;
		this.descriptionFallback = descriptionFallback;
		this.sglAwaitingCalibration = sglAwaitingCalibration;
		this.sglResumeCalibration = sglResumeCalibration;
		this.sglCalibration = sglCalibration;
	}

	public Integer getStateid() {
		return stateid;
	}

	public Boolean getActive() {
		return active;
	}

	public Boolean getRetired() {
		return retired;
	}

	public String getDescription() {
		return description;
	}

	public String getDescriptionFallback() {
		return descriptionFallback;
	}

	public Integer getSglAwaitingCalibration() {
		return sglAwaitingCalibration;
	}

	public Integer getSglResumeCalibration() {
		return sglResumeCalibration;
	}

	public Integer getSglCalibration() {
		return sglCalibration;
	}

	public void setStateid(Integer stateid) {
		this.stateid = stateid;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setRetired(Boolean retired) {
		this.retired = retired;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescriptionFallback(String descriptionFallback) {
		this.descriptionFallback = descriptionFallback;
	}

	public void setSglAwaitingCalibration(Integer sglAwaitingCalibration) {
		this.sglAwaitingCalibration = sglAwaitingCalibration;
	}

	public void setSglResumeCalibration(Integer sglResumeCalibration) {
		this.sglResumeCalibration = sglResumeCalibration;
	}

	public void setSglCalibration(Integer sglCalibration) {
		this.sglCalibration = sglCalibration;
	}
}
