package org.trescal.cwms.core.workflow.entity.stategrouplink.db;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.dto.JobItemsInStateDTO;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

public interface StateGroupLinkService extends BaseService<StateGroupLink, Integer> {

	List<StateGroupLink> getAllByStateGroup(StateGroup group);

	boolean containsStateGroup(Set<StateGroupLink> links, StateGroup group);

	List<JobItemsInStateDTO> getJobItemsInStateGroup(StateGroup stateGroup, Integer companyId, Integer subdivId,
			Locale locale);
	
	List<ItemStateDTO> getLinkedItemStates(StateGroup stateGroup, Locale locale);
}