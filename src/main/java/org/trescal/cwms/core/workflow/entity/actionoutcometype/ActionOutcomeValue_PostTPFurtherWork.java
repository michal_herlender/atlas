package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_PostTPFurtherWork implements ActionOutcomeValue {
	NO_FURTHER_WORK_NECESSARY(
			"No further work necessary",
			"actionoutcome.post_tp_furtherwork.done"),
	REQUIRES_INHOUSE_CALIBRATION(
			"Unit requires in-house calibration",
			"actionoutcome.post_tp_furtherwork.inhousecal"),
	REQUIRES_INHOUSE_REPAIR(
			"Unit requires in-house repair",
			"actionoutcome.post_tp_furtherwork.inhouserepair"),
	REQUIRES_ADJUSTMENT(
			"Unit requires adjustment",
			"actionoutcome.post_tp_furtherwork.adjustment"),
	REQUIRES_FURTHER_TP_WORK(
			"Unit requires further third party work",
			"actionoutcome.post_tp_furtherwork.furthertp"),
	REQUIRES_FAILURE_REPORT(
			"Unit requires failure report",
			"actionoutcome.post_tp_furtherwork.failurereport"),
	REQUIRES_COSTING_BEFORE_CLIENT_DESPATCH(
			"Unit requires costing before despatch to client",
			"actionoutcome.post_tp_furtherwork.costing"),
	WORK_NOT_COMPLETED_AT_TP(
			"Work has not been completed at third party",
			"actionoutcome.post_tp_furtherwork.notcompleted"),
	TP_REPAIR_UNSUCCESSFUL_RETURN_TO_TP(
			"Third party repair un-successful - return to third party",
			"actionoutcome.post_tp_furtherwork.repairunsuccessful"),
	NO_FURTHER_WORK_NECESSARY_NO_DELIVERY_REQUIRED("No further work necessary, no delivery required",
			"actionoutcome.post_tp_furtherwork.donenodeliveryrequired"),
	NO_FURTHER_WORK_FOR_CURRENT_WR("No further work necessary for current WorkRequirement", "lookupresultmessage.posttpinspectionoutcome.nofurtherworkforcurrentwr"),
	FURTHER_WORK_NECESSARY_BY_SELECTION_OF_NEXT_WR("Further work necessary for newt WorkRequirement", "lookupresultmessage.posttpinspectionoutcome.furtherworknextwr"),
	NOT_COMPLETED_RETURN_TO_SUPPLIER("Work has not been completed at third party", "lookupresultmessage.posttpinspectionoutcome.notcompleted"),
	UNSUCCESSFUL("Third party repair un-successful - return to third party", "lookupresultmessage.posttpinspectionoutcome.unsuccessful");
	

	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_PostTPFurtherWork(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.POST_TP_FURTHER_WORK;
	}

}
