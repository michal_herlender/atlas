package org.trescal.cwms.core.workflow.entity.itemstate;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Superclass for ItemStatus and ItemActivity entities.
 *
 * @author jamiev
 */
@Entity
@Table(name = "itemstate")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class ItemState
{
	/**
	 * this 'active' field tells whether or not the state represents items that
	 * are active
	 */
	private Boolean active = false;
	@Deprecated
	private String description;
	private Set<StateGroupLink> groupLinks;
	private Set<JobItem> items;

	/**
	 * this 'retired' field tells whether or not the state is an old (CWMS 1)
	 * state that has now been superceded
     */
    private Boolean retired = false;
    private int stateid;
    private Set<Translation> translations;

    public ItemState() {

    }

    /**
     * @return the description
     */
    @Column(name = "description", length = 150)
    public String getDescription() {
        return this.description;
    }

    @OneToMany(mappedBy = "state", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public Set<StateGroupLink> getGroupLinks() {
        return this.groupLinks;
    }

    /**
     * @return the items
     */
	@OneToMany(mappedBy = "state", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<JobItem> getItems()
	{
		return this.items;
	}

	/**
	 * @return the stateid
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "stateid", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getStateid()
	{
		return this.stateid;
	}

	@NotNull
	@Column(name = "active", nullable=false, columnDefinition="bit")
	public Boolean getActive()
	{
		return this.active;
	}

	@NotNull
	@Column(name = "retired", nullable=false, columnDefinition="bit")
	public Boolean getRetired()
	{
		return this.retired;
	}

	@Transient
	public abstract boolean isStatus();

	public void setActive(Boolean active)
	{
		this.active = active;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setGroupLinks(Set<StateGroupLink> groupLinks)
	{
		this.groupLinks = groupLinks;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Set<JobItem> items)
	{
		this.items = items;
	}

	public void setRetired(Boolean retired)
	{
		this.retired = retired;
	}

	/**
	 * @param stateid the stateid to set
	 */
	public void setStateid(Integer stateid)
	{
		this.stateid = stateid;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="itemstatetranslation", joinColumns=@JoinColumn(name="stateid"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	
	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
}