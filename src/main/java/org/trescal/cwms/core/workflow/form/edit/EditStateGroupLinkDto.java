package org.trescal.cwms.core.workflow.form.edit;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;

public class EditStateGroupLinkDto {
	private Integer id;						// ID of existing StateGroupLink being edited
	private StateGroup stateGroup;
	private StateGroupLinkType linkType;
	private Boolean delete;					// Whether existing link should be deleted
	
	public EditStateGroupLinkDto() {
		// Default constructor for Spring binding
	}
	
	public EditStateGroupLinkDto(StateGroupLink sgl) {
		this.id = sgl.getId();
		this.stateGroup = sgl.getGroup();
		this.linkType = sgl.getType();
		this.delete = false;
	}
	
	@NotNull
	public Integer getId() {
		return id;
	}
	@NotNull
	public StateGroup getStateGroup() {
		return stateGroup;
	}
	@NotNull
	public StateGroupLinkType getLinkType() {
		return linkType;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setStateGroup(StateGroup stateGroup) {
		this.stateGroup = stateGroup;
	}
	public void setLinkType(StateGroupLinkType linkType) {
		this.linkType = linkType;
	}

	public Boolean getDelete() {
		return delete;
	}

	public void setDelete(Boolean delete) {
		this.delete = delete;
	}
}
