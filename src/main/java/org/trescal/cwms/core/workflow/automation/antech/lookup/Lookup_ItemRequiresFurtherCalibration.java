package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemRequiresFurtherCalibration implements LookupResultMessage
{
	YES("lookupresultmessage.itemrequiresfurthercalibration.yes"),
	NO("lookupresultmessage.itemrequiresfurthercalibration.no");
	
	private String messageCode;
	
	private Lookup_ItemRequiresFurtherCalibration(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}