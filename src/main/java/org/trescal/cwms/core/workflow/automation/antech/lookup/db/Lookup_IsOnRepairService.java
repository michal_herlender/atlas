package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_IsOnRepair;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_IsOnRepair")
public class Lookup_IsOnRepairService extends Lookup {

	@Autowired
	private RepairInspectionReportService rirService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Is On Repair");
		
		List<RepairInspectionReport> rir = rirService.getAllRirByJobItemId(jobItemId);
		if (rir != null && !rir.isEmpty())
			return Lookup_IsOnRepair.YES;
		else {
			return Lookup_IsOnRepair.NO;
		}
	}

	@Override
	public String getDescription() {
		return "Item is on Repair";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_IsOnRepair.values();
	}
}