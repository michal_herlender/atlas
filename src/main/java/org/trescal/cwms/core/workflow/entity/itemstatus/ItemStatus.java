package org.trescal.cwms.core.workflow.entity.itemstatus;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;

@Entity
public abstract class ItemStatus extends ItemState
{
	private Set<NextActivity> nextActivities;
	protected boolean hold;
	
	@OneToMany(mappedBy = "status", cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.LAZY)
	@OrderBy("id")
	public Set<NextActivity> getNextActivities()
	{
		return this.nextActivities;
	}

	@Transient
	public boolean isHold() {
		return hold;
	}

	@Override
	@Transient
	public boolean isStatus()
	{
		return true;
	}

	public void setNextActivities(Set<NextActivity> nextActivities)
	{
		this.nextActivities = nextActivities;
	}
}
