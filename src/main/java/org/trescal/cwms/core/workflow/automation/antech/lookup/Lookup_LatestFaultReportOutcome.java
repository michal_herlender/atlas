package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_LatestFaultReportOutcome implements LookupResultMessage {

	ERP_CALIBRATION("lookupresultmessage.faultreport.erpcalibration"),
	ERP_REPAIR("lookupresultmessage.faultreport.erprepair"),
	ERP_ADJUSTEMENT("lookupresultmessage.faultreport.erpadjustment"),
	THIRD_PARTY("lookupresultmessage.faultreport.thirdparty"),
	SCRAPPING_PROPOSAL("lookupresultmessage.faultreport.scrappingproposal"),
	RETURN_TO_CLIENT("lookupresultmessage.faultreport.returntoclient"),
	COULD_NOT_DETEMINE("lookupresultmessage.faultreport.couldnotdetemine");
	
	private String messageCode;

	private Lookup_LatestFaultReportOutcome(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
}
