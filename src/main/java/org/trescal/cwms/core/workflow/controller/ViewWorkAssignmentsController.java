package org.trescal.cwms.core.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.departmenttype.db.DepartmentTypeService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.workflow.entity.StateGroupType;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.db.StateGroupLinkService;
import org.trescal.cwms.core.workflow.entity.workassignment.WorkAssignment;
import org.trescal.cwms.core.workflow.entity.workassignment.db.WorkAssignmentService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
public class ViewWorkAssignmentsController {
    @Autowired
    private DepartmentTypeService deptTypeServ;
    @Autowired
    private ItemStateService itemStateService;
    @Autowired
    private StateGroupLinkService sglService;
    @Autowired
    private WorkAssignmentService workAssignServ;

    @RequestMapping(value = "/viewworkassignments.htm", method = RequestMethod.GET)
    public ModelAndView referenceData() throws Exception {
        Map<String, Object> refData = new HashMap<>();
        refData.put("keyValues", getKeyValues());
        refData.put("stateGroupMap", getStateGroupMap());
        refData.put("itemStateMap", getUnlinkedItemStates());
		refData.put("deptTypes", deptTypeServ.getActiveDepartmentTypes());
		refData.put("deptTypeMap", getDepartmentTypeMap());
		return new ModelAndView("trescal/core/admin/viewworkassignments", refData);
	}
	
	private Map<DepartmentType,List<WorkAssignment>> getDepartmentTypeMap() {
		Map<DepartmentType,List<WorkAssignment>> result = new TreeMap<>();
		for (DepartmentType type: deptTypeServ.getActiveDepartmentTypes()) {
			List<WorkAssignment> list = this.workAssignServ.getForDepartmentType(type);
			result.put(type, list);
		}
		return result;
	}
	
	/*
	 * Returns a set of key values with Department Types, but also with Third Party State Group
	 */
	private Set<KeyValue<String,String>> getKeyValues() {
		Set<KeyValue<String,String>> result = new TreeSet<>();
		for (DepartmentType type : deptTypeServ.getActiveDepartmentTypes()) {
            result.add(new KeyValue<>(type.name(), type.getMessage()));
		}
		return result;
	}
	
	
	
	/*
	 * Prepare a map of StateGroup to StateGroupLinks (thereby indicating item state)
	 */
	private Map<StateGroup, List<StateGroupLink>> getStateGroupMap() {
        List<StateGroupLink> stateGroupLinks = sglService.getAll();
        Set<StateGroup> stateGroups = stateGroupLinks.stream().map(StateGroupLink::getGroup).collect(Collectors.toSet());
        Map<StateGroup, List<StateGroupLink>> stateGroupMap = new HashMap<>();
        stateGroups.forEach(sg -> stateGroupMap.put(sg, new ArrayList<>()));
        stateGroupLinks.forEach(sgl -> stateGroupMap.get(sgl.getGroup()).add(sgl));
        return stateGroupMap;
    }
	
	/*
	 * Prepare a map of state ids to active ItemState values that have no StateGroupLink of type DEPARTMENT
	 * (therefore hidden from workflow)
	 */
	private Map<Integer, ItemStatus> getUnlinkedItemStates() {
		// First, build map of all active ItemState values by id 
		List<ItemStatus> activeItemStates = this.itemStateService.getAllItemStatuses(ItemStatus.class, false);
		Map<Integer, ItemStatus> result = new HashMap<>();
        activeItemStates.forEach(itemState -> result.put(itemState.getStateid(), itemState));
		// Now remove items that have a state group link, just for DEPARTMENT or THIRD_PARTY
		List<StateGroupLink> stateGroupLinks = sglService.getAll();
        stateGroupLinks.stream().filter(s -> s.getState() != null).filter(s -> s.getGroup().getType().equals(StateGroupType.DEPARTMENT)).map(s -> s.getState().getStateid()).forEach(result::remove);
		return result;
	}
}