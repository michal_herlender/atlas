package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationTypeEnum;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NextRepairOperation;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

/*CURRENTLY NOT USED*/
@Component("Lookup_NextRepairOperation")
public class Lookup_NextRepairOperationService extends Lookup {

	@Autowired
	private RepairInspectionReportService rirService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: NEXT REPAIR OPERATION");
		Subdiv subdiv = jobItemService.findJobItem(jobItemId).getCurrentAddr().getSub();
		RepairInspectionReport rir = rirService.getRirByJobItemIdAndSubdivId(jobItemId, subdiv.getSubdivid());

		if (rir != null) {
			FreeRepairOperation fro = rir.getFreeRepairOperations().stream()
					.filter(o -> o.getOperationStatus().equals(RepairOperationStateEnum.AWAITING_EXECUTION)).findFirst()
					.orElse(null);
			if (fro == null)
				return Lookup_NextRepairOperation.AWAITING_RCR_COMPLETION;
			else if (fro.getOperationType().equals(RepairOperationTypeEnum.INTERNAL)) {
				if (fro.getAddedAfterRiRValidation())
					return Lookup_NextRepairOperation.AWAITING_COSTING_INTERNAL_REPAIR;
				else
					return Lookup_NextRepairOperation.AWAITING_INTERNAL_REPAIR;
			} else {
				if (fro.getAddedAfterRiRValidation())
					return Lookup_NextRepairOperation.AWAITING_PO_EXTERNAL_REPAIR;
				else
					return Lookup_NextRepairOperation.AWAITING_EXTERNAL_REPAIR;
			}
		} else
			return null;
	}

	@Override
	public String getDescription() {
		return "Checks whether the item has other operation to execute";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_NextRepairOperation.values();
	}
}