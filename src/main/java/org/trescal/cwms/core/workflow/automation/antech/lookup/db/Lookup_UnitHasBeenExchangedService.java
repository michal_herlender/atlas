package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_UnitHasBeenExchanged;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Component("Lookup_UnitHasBeenExchanged")
public class Lookup_UnitHasBeenExchangedService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether this item has been exchanged for a new unit by the third party.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: UNIT HAS BEEN EXCHANGED");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = Lookup_UnitHasBeenExchanged.NO;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions
		activityLoop: for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity jia = (JobItemActivity) action;
				ItemActivity activity = this.itemStateService.findItemActivity(jia.getActivity().getStateid());
				// check if this is an 'exchange' activity
				for (StateGroupLink sgl : activity.getGroupLinks()) {
					if (sgl.getGroup().equals(StateGroup.EXCHANGE) && !jia.isDeleted()) {
						result = Lookup_UnitHasBeenExchanged.YES;
						break activityLoop;
					}
				}
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_UnitHasBeenExchanged.values();
	}
}