package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_CompleteGeneralServiceOperation extends HookInterceptor<HookInterceptor_CompleteGeneralServiceOperation.Input> {

	public static final String HOOK_NAME = "complete-general-service-operation";

	public static class Input {
		public GeneralServiceOperation gso;
		private String remark;

		public Input(GeneralServiceOperation gso, String remark) {
			this.gso = gso;
			this.remark = remark;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: COMPLETE GENERAL SERVICE OPERATION");
			Contact con = input.gso.getCompletedBy() != null ? input.gso.getCompletedBy() : this.sessionServ.getCurrentContact();
			GeneralServiceOperation gso = input.gso;
			Date startStamp = gso.getStartedOn();
			Date endStamp = gso.getCompletedOn();
			String remark = input.remark;
			for (GsoLink link : gso.getLinks()) {
				
			if(link.getOutcome() != null) {
				JobItem ji = link.getJi();

				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);
				JobItemActivity jia = null;

				for (JobItemAction action : ji.getActions()) {
					if (action instanceof JobItemActivity) {
						JobItemActivity activity = (JobItemActivity) action;
						if (!activity.isComplete() && activity.getActivity().getDescription()
								.equals(context.getActivity().getDescription())) {
							jia = activity;
							break;
						}
					}
				}

				ItemStatus endStatus = null;
				if (context.isCompleteActivity() && (jia != null)) {

					if (startStamp != null)
						jia.setStartStamp(startStamp);
					if (endStamp != null)
						jia.setEndStamp(endStamp);
					else
						jia.setEndStamp(new Date());

					int mins = (link.getTimeSpent() != null) ? link.getTimeSpent()
							: DateTools.getMinutesDifference(jia.getStartStamp(), jia.getEndStamp());

					jia.setTimeSpent(mins);
					jia.setCompletedBy(con);
					jia.setComplete(true);
					jia.setOutcome(link.getOutcome());

					if ((remark != null) && !remark.trim().isEmpty()) {
						jia.setRemark(remark);
					}

					endStatus = super.getNextStatus(context);

					ji.setState(endStatus);
					jobItemService.updateJobItem(ji);

					jia.setEndStatus(endStatus);

					this.persist(jia, endStatus, context);
				}
			}
			}
		}
	}
}