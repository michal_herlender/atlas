package org.trescal.cwms.core.workflow.form.edit;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.util.AutoPopulatingList;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupFunction;
import org.trescal.cwms.spring.helpers.IntegerElementFactory;

public class EditLookupInstanceForm {
	private String description;
	private LookupFunction lookupFunction;
	private AutoPopulatingList<EditOutcomeStatusDto> outcomeStatusDtos;
	private AutoPopulatingList<Integer> selectedLookups;
	private EditOutcomeStatusDto templateOutcomeStatus;

	/**
	 * AutoPopulatingList needed for selectedLookups, as no lookups may be chosen.
	 * As auto-grow is off in Controller, outcomeStatusDtos also uses AutoPopulatingList.
	 */
	public EditLookupInstanceForm() {
		this.selectedLookups = new AutoPopulatingList<>(new IntegerElementFactory(-1));
		this.outcomeStatusDtos = new AutoPopulatingList<>(EditOutcomeStatusDto.class);
	}
	
	@Valid
	public AutoPopulatingList<EditOutcomeStatusDto> getOutcomeStatusDtos() {
		return outcomeStatusDtos;
	}
	// Optional - if no outcome statuses, will be empty
	public AutoPopulatingList<Integer> getSelectedLookups() {
		return selectedLookups;
	}
	// Not validated - template only 
	public EditOutcomeStatusDto getTemplateOutcomeStatus() {
		return templateOutcomeStatus;
	}
	@NotNull
	public LookupFunction getLookupFunction() {
		return lookupFunction;
	}
	@NotNull
	public String getDescription() {
		return description;
	}
	
	public void setOutcomeStatusDtos(AutoPopulatingList<EditOutcomeStatusDto> outcomeStatusDtos) {
		this.outcomeStatusDtos = outcomeStatusDtos;
	}
	public void setSelectedLookups(AutoPopulatingList<Integer> selectedLookups) {
		this.selectedLookups = selectedLookups;
	}
	public void setTemplateOutcomeStatus(EditOutcomeStatusDto templateOutcomeStatus) {
		this.templateOutcomeStatus = templateOutcomeStatus;
	}
	public void setLookupFunction(LookupFunction lookupFunction) {
		this.lookupFunction = lookupFunction;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
