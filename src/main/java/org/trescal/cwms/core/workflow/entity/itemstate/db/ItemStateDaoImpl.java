package org.trescal.cwms.core.workflow.entity.itemstate.db;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.system.dto.TranslationDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.dto.PlantillasItemStateDTO;
import org.trescal.cwms.core.workflow.entity.holdactivity.HoldActivity;
import org.trescal.cwms.core.workflow.entity.holdactivity.HoldActivity_;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus_;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus_;
import org.trescal.cwms.core.workflow.entity.overrideactivity.OverrideActivity;
import org.trescal.cwms.core.workflow.entity.overrideactivity.OverrideActivity_;
import org.trescal.cwms.core.workflow.entity.progressactivity.ProgressActivity;
import org.trescal.cwms.core.workflow.entity.progressactivity.ProgressActivity_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;
import org.trescal.cwms.core.workflow.entity.transitactivity.TransitActivity;
import org.trescal.cwms.core.workflow.entity.transitactivity.TransitActivity_;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity_;
import org.trescal.cwms.core.workflow.entity.workassignment.WorkAssignment;
import org.trescal.cwms.core.workflow.entity.workassignment.WorkAssignment_;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.val;

@Repository("ItemStateDao")
public class ItemStateDaoImpl extends BaseDaoImpl<ItemState, Integer> implements ItemStateDao {
	@Override
	protected Class<ItemState> getEntity() {
		return ItemState.class;
	}

	@Override
	public ItemActivity findItemActivity(Integer id) {
		return getEntityManager().find(ItemActivity.class, id);
	}

	@Override
	public ItemActivity findItemActivityByName(String name) {
		Criteria crit = getSession().createCriteria(ItemActivity.class);
		crit.add(Restrictions.eq("description", name));
		crit.add(Restrictions.eq("retired", false));
		crit.addOrder(Order.asc("stateid"));
		crit.setFetchMode("actionOutcomes", FetchMode.JOIN);
		crit.setFetchMode("actionOutcomes", FetchMode.JOIN);
		return (ItemActivity) crit.uniqueResult();
	}

	/**
	 * retired or not
	 */
	@Override
	public ItemActivity findAllItemActivityByName(String name) {
		Criteria crit = getSession().createCriteria(ItemActivity.class);
		crit.add(Restrictions.eq("description", name));
		crit.addOrder(Order.asc("stateid"));
		crit.setFetchMode("actionOutcomes", FetchMode.JOIN);
		return (ItemActivity) crit.uniqueResult();
	}

	public ItemStatus findItemStatus(int id) {
		return getEntityManager().find(ItemStatus.class, id);
	}

	public ItemStatus findItemStatusByName(String name) {
		return getFirstResult(cb ->{
			CriteriaQuery<ItemStatus> cq = cb.createQuery(ItemStatus.class);
			Root<ItemStatus> itemStatus = cq.from(ItemStatus.class);
			cq.where(cb.equal(itemStatus.get(ItemStatus_.description), name));
			cq.orderBy(cb.asc(itemStatus.get(ItemStatus_.stateid)));
			return cq;
		}).orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemState> getAll(StateGroup stateGroup) {
		Criteria criteria = getSession().createCriteria(ItemState.class);
		criteria.createCriteria("groupLinks", "groups", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("groups.groupId", stateGroup.getId()));
		return criteria.list();
	}

	@Override
	public List<ItemState> getAllForStateGroups(List<Integer> stateGroupIds) {
		
		return getResultList(cb -> {
			CriteriaQuery<ItemState> cq = cb.createQuery(ItemState.class);
			Root<ItemState> root = cq.from(ItemState.class);
			Join<ItemState, StateGroupLink> groupLinks = root.join(ItemState_.groupLinks);

			cq.where(groupLinks.get(StateGroupLink_.groupId).in(stateGroupIds));
			cq.distinct(true);

			return cq;
		});
	}

	/**
	 * StateGroupLinkType
	 * 
	 * @param type
	 * @return a unique list of ItemState ids linked to the specified department
	 *         type
	 */
	@Override
	public List<Integer> getItemStateIdsForDepartmentTypes(EnumSet<DepartmentType> deptTypes) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
		Root<ItemState> itemState = cq.from(ItemState.class);
		Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks);

		Subquery<Integer> sq = cq.subquery(Integer.class);
		Root<WorkAssignment> workAssignment = sq.from(WorkAssignment.class);
		sq.where(workAssignment.get(WorkAssignment_.deptType).in(deptTypes));
		sq.select(workAssignment.get(WorkAssignment_.stateGroupId));
		sq.distinct(true);

		cq.where(stateGroupLink.get(StateGroupLink_.groupId).in(sq));
		cq.select(itemState.get(ItemState_.stateid));
		cq.distinct(true);

		return getEntityManager().createQuery(cq).getResultList();
	}
	
	@Override 
	public List<Integer> getItemStateIdsForStateGroups(EnumSet<StateGroup> stateGroups) {
		if (stateGroups == null || stateGroups.isEmpty())
			throw new IllegalArgumentException("At least one stateGroup must be specified!");
		return getResultList(cb -> {
			Set<Integer> stateGroupIds = stateGroups.stream().map(StateGroup::getId).collect(Collectors.toSet());
			
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<ItemState> itemState = cq.from(ItemState.class);
			Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks);
			
			cq.where(stateGroupLink.get(StateGroupLink_.groupId).in(stateGroupIds));
			cq.select(itemState.get(ItemState_.stateid));
			cq.distinct(true);

			return cq;
		});
	}

	public List<ItemActivity> getAllItemActivities() {
		return getResultList(cb ->{
			CriteriaQuery<ItemActivity> cq = cb.createQuery(ItemActivity.class);
			cq.from(ItemActivity.class);
			return cq;
		});
	}
	
	@Override
	public List<KeyValueIntegerString> getDTOList(Collection<Integer> stateIds, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<ItemState> root = cq.from(ItemState.class);
			Join<ItemState, Translation> translation = root.join(ItemState_.translations, javax.persistence.criteria.JoinType.LEFT);
			translation.on(cb.equal(translation.get(Translation_.locale), locale));
			
			CompoundSelection<KeyValueIntegerString> selection = cb.construct(KeyValueIntegerString.class, 
					root.get(ItemState_.stateid),
					translation.get(Translation_.translation));
			
			cq.where(root.get(ItemState_.stateid).in(stateIds));
			cq.select(selection);
			
			return cq;
		});
	}
	
	public List<ItemStatus> getAllItemStatuses(Class<? extends ItemStatus> clazz, boolean includeRetired) {
		return  getResultList( cb->{
			val cq = cb.createQuery(ItemStatus.class);
			val root = cq.from(clazz);
			if(!includeRetired){
				cq.where(cb.isFalse(root.get(ItemStatus_.retired)));
			}
			cq.select(root);
			cq.distinct(true);
			return cq;
		});
	}
	
	@Override
	public List<KeyValueIntegerString> getAllTranslatedItemStatuses(Class<? extends ItemStatus> clazz, boolean includeRetired, Locale locale){
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			val itemStatus = cq.from(clazz);
			val translation = itemStatus.join(ItemStatus_.translations);
			translation.on(cb.equal(translation.get(Translation_.locale), locale));
			
			CompoundSelection<KeyValueIntegerString> selection = cb.construct(KeyValueIntegerString.class, 
					itemStatus.get(ItemStatus_.stateid),
					translation.get(Translation_.translation));
			
			if(!includeRetired){
				cq.where(cb.isFalse(itemStatus.get(ItemStatus_.retired)));
			}
			
			cq.select(selection);
			cq.distinct(true);
			cq.orderBy(cb.asc(translation.get(Translation_.translation)));
			return cq;
		});
	}

	public List<ProgressActivity> getAllProgressActivities(boolean includeRetired) {
		return getResultList(cb->{
			CriteriaQuery<ProgressActivity> cq = cb.createQuery(ProgressActivity.class);
			Root<ProgressActivity> progressActivity = cq.from(ProgressActivity.class);
			if(!includeRetired){
				cq.where(cb.isFalse(progressActivity.get(ProgressActivity_.retired)));
			}
			return cq;
		});
	}

	public List<WorkActivity> getAllWorkActivities() {
		return getResultList(cb->{
			CriteriaQuery<WorkActivity> cq = cb.createQuery(WorkActivity.class);
			cq.from(WorkActivity.class);
			return cq;
		});
	}

	public PlantillasItemStateDTO getPlantillasItemState(Locale locale, int stateId) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<PlantillasItemStateDTO> cq = cb.createQuery(PlantillasItemStateDTO.class);

		Root<ItemState> rootIS = cq.from(ItemState.class);
		Join<ItemState, Translation> joinTranslation = rootIS.join(ItemState_.translations,
				javax.persistence.criteria.JoinType.LEFT);

		Join<ItemState, StateGroupLink> joinSGL_AwaitingCalibration = rootIS.join(ItemState_.groupLinks,
				javax.persistence.criteria.JoinType.LEFT);
		joinSGL_AwaitingCalibration.on(cb.equal(joinSGL_AwaitingCalibration.get(StateGroupLink_.groupId),
				StateGroup.AWAITINGCALIBRATION.getId()));

		Join<ItemState, StateGroupLink> joinSGL_ResumeCalibration = rootIS.join(ItemState_.groupLinks,
				javax.persistence.criteria.JoinType.LEFT);
		joinSGL_ResumeCalibration.on(
				cb.equal(joinSGL_ResumeCalibration.get(StateGroupLink_.groupId), StateGroup.RESUMECALIBRATION.getId()));

		Join<ItemState, StateGroupLink> joinSGL_Calibration = rootIS.join(ItemState_.groupLinks,
				javax.persistence.criteria.JoinType.LEFT);
		joinSGL_Calibration
				.on(cb.equal(joinSGL_Calibration.get(StateGroupLink_.groupId), StateGroup.CALIBRATION.getId()));

		cq.distinct(true);
		cq.select(cb.construct(PlantillasItemStateDTO.class, rootIS.get(ItemState_.stateid),
				rootIS.get(ItemState_.active), rootIS.get(ItemState_.retired), rootIS.get(ItemState_.description),
				joinTranslation.get(Translation_.translation), joinSGL_AwaitingCalibration.get(StateGroupLink_.groupId),
				joinSGL_ResumeCalibration.get(StateGroupLink_.groupId),
				joinSGL_Calibration.get(StateGroupLink_.groupId)));

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(rootIS.get(ItemState_.stateid), stateId));
		clauses.getExpressions().add(cb.equal(joinTranslation.get(Translation_.locale), locale));

		cq.where(clauses);

		TypedQuery<PlantillasItemStateDTO> query = getEntityManager().createQuery(cq);
		List<PlantillasItemStateDTO> list = query.getResultList();
		if (list.size() > 1)
			throw new NonUniqueResultException(list.size() + " results");
		return list.isEmpty() ? null : list.get(0);
	}

	public List<PlantillasItemStateDTO> getPlantillasItemStates(Locale locale) {
		Criteria criteria = getSession().createCriteria(ItemState.class);
		criteria.createCriteria("translations", "stateName", JoinType.LEFT_OUTER_JOIN,
				Restrictions.eq("locale", locale));
		ProjectionList projection = Projections.projectionList();
		projection.add(Projections.property("stateid"), "stateid");
		projection.add(Projections.property("description"), "descriptionFallback");
		projection.add(Projections.property("stateName.translation"), "description");
		projection.add(Projections.property("active"), "active");
		projection.add(Projections.property("retired"), "retired");

		criteria.setProjection(projection);
		criteria.setResultTransformer(Transformers.aliasToBean(PlantillasItemStateDTO.class));

		@SuppressWarnings("unchecked")
		List<PlantillasItemStateDTO> results = criteria.list();
		return results;
	}

	public List<TranslationDTO> getTranslatedItemStates(Locale locale, boolean includeRetired) {
		Criteria criteria = getSession().createCriteria(ItemState.class);
		// Note, below was failing in Hibernate 5.2 with locale not included in
		// SQL query.
		// criteria.createCriteria("translations", "stateName",
		// JoinType.LEFT_OUTER_JOIN, Restrictions.eq("locale", locale));
		Criteria criteriaTranslation = criteria.createCriteria("translations", "stateName", JoinType.LEFT_OUTER_JOIN);
		criteriaTranslation.add(Restrictions.eq("locale", locale));
		if (!includeRetired)
			criteria.add(Restrictions.eq("retired", false));
		ProjectionList projection = Projections.projectionList();
		projection.add(Projections.property("stateid"), "id");
		projection.add(Projections.property("description"), "fallback");
		projection.add(Projections.property("stateName.translation"), "translation");

		criteria.setProjection(projection);
		criteria.setResultTransformer(Transformers.aliasToBean(TranslationDTO.class));

		@SuppressWarnings("unchecked")
		List<TranslationDTO> results = criteria.list();
		return results;
	}

	@Override
	public List<ItemStateDTO> getHoldActivities(List<Integer> exist_list) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ItemStateDTO> cq = cb.createQuery(ItemStateDTO.class);
		Root<HoldActivity> root = cq.from(HoldActivity.class);

		Predicate clauses = cb.conjunction();

		if (exist_list.size() > 0)
			clauses.getExpressions().add(cb.not(root.get(HoldActivity_.stateid.getName()).in(exist_list)));

		clauses.getExpressions().add(cb.equal(root.get(HoldActivity_.active), true));
		clauses.getExpressions().add(cb.equal(root.get(HoldActivity_.retired), false));

		cq.select(cb.construct(ItemStateDTO.class, root.get(HoldActivity_.stateid.getName()),
				root.get(HoldActivity_.description.getName())));
		cq.distinct(true);

		cq.where(clauses);

		return getEntityManager().createQuery(cq).getResultList();
	}

	@Override
	public List<ItemStateDTO> getOverrideActivities(List<Integer> exist_list) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ItemStateDTO> cq = cb.createQuery(ItemStateDTO.class);
		Root<OverrideActivity> root = cq.from(OverrideActivity.class);

		Predicate clauses = cb.conjunction();

		if (exist_list.size() > 0)
			clauses.getExpressions().add(cb.not(root.get(OverrideActivity_.stateid.getName()).in(exist_list)));

		clauses.getExpressions().add(cb.equal(root.get(OverrideActivity_.active), true));
		clauses.getExpressions().add(cb.equal(root.get(OverrideActivity_.retired), false));

		cq.select(cb.construct(ItemStateDTO.class, root.get(HoldActivity_.stateid.getName()),
				root.get(HoldActivity_.description.getName())));
		cq.distinct(true);

		cq.where(clauses);

		return getEntityManager().createQuery(cq).getResultList();
	}

	@Override
	public List<ItemStateDTO> getProgressActivities(List<Integer> exist_list) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ItemStateDTO> cq = cb.createQuery(ItemStateDTO.class);
		Root<ProgressActivity> root = cq.from(ProgressActivity.class);

		Predicate clauses = cb.conjunction();

		if (exist_list.size() > 0)
			clauses.getExpressions().add(cb.not(root.get(ProgressActivity_.stateid.getName()).in(exist_list)));

		clauses.getExpressions().add(cb.equal(root.get(ProgressActivity_.active), true));
		clauses.getExpressions().add(cb.equal(root.get(ProgressActivity_.retired), false));

		cq.select(cb.construct(ItemStateDTO.class, root.get(HoldActivity_.stateid.getName()),
				root.get(HoldActivity_.description.getName())));
		cq.distinct(true);

		cq.where(clauses);

		return getEntityManager().createQuery(cq).getResultList();
	}

	@Override
	public List<ItemStateDTO> getTransitActivities(List<Integer> exist_list) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ItemStateDTO> cq = cb.createQuery(ItemStateDTO.class);
		Root<TransitActivity> root = cq.from(TransitActivity.class);

		Predicate clauses = cb.conjunction();

		if (exist_list.size() > 0)
			clauses.getExpressions().add(cb.not(root.get(TransitActivity_.stateid.getName()).in(exist_list)));

		clauses.getExpressions().add(cb.equal(root.get(TransitActivity_.active), true));
		clauses.getExpressions().add(cb.equal(root.get(TransitActivity_.retired), false));

		cq.select(cb.construct(ItemStateDTO.class, root.get(HoldActivity_.stateid.getName()),
				root.get(HoldActivity_.description.getName())));
		cq.distinct(true);

		cq.where(clauses);

		return getEntityManager().createQuery(cq).getResultList();
	}

	@Override
	public List<ItemStateDTO> getWorkActivities(List<Integer> exist_list) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ItemStateDTO> cq = cb.createQuery(ItemStateDTO.class);
		Root<WorkActivity> root = cq.from(WorkActivity.class);

		Predicate clauses = cb.conjunction();

		if (exist_list.size() > 0)
			clauses.getExpressions().add(cb.not(root.get(WorkActivity_.stateid.getName()).in(exist_list)));

		clauses.getExpressions().add(cb.equal(root.get(WorkActivity_.active), true));
		clauses.getExpressions().add(cb.equal(root.get(WorkActivity_.retired), false));

		cq.select(cb.construct(ItemStateDTO.class, root.get(HoldActivity_.stateid.getName()),
				root.get(HoldActivity_.description.getName())));
		cq.distinct(true);

		cq.where(clauses);

		return getEntityManager().createQuery(cq).getResultList();
	}

	@Override
	public List<HoldStatus> getHoldStatus() {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<HoldStatus> cq = cb.createQuery(HoldStatus.class);
		Root<HoldStatus> root = cq.from(HoldStatus.class);

		Predicate clauses = cb.conjunction();

		clauses.getExpressions().add(cb.equal(root.get(HoldStatus_.active), true));
		clauses.getExpressions().add(cb.equal(root.get(HoldStatus_.retired), false));

		cq.distinct(true);

		cq.where(clauses);

		return getEntityManager().createQuery(cq).getResultList();
	}

}