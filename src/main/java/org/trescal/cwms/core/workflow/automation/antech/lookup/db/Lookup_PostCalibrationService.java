package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_PostCalibration;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Component("Lookup_PostCalibration")
public class Lookup_PostCalibrationService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public String getDescription() {
		return "Determines the success of a calibration just performed on an item.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: POST CALIBRATION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		FaultReport fr = faultReportService.getLastFailureReportByJobitemId(jobItemId, true);
		LookupResultMessage result = null;
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions to get the MOST RECENT calibration
		// in case there is more than one
		activityLoop: for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity jia = (JobItemActivity) action;
				ItemActivity activity = this.itemStateService.findItemActivity(jia.getActivity().getStateid());
				for (StateGroupLink sgl : activity.getGroupLinks()) {
					if (sgl.getGroup().equals(StateGroup.CALIBRATION) && !jia.isDeleted()) {
						ao = jia.getOutcome();
						break activityLoop;
					}
				}
			}
		}
		if (ao == null) {
			result = Lookup_PostCalibration.SUCCESSFULLY;
		} else {
			if (ao.isPositiveOutcome()) {
				result = Lookup_PostCalibration.SUCCESSFULLY;
			} else {

				if (ao.getGenericValue().equals(ActionOutcomeValue_Calibration.ON_HOLD)) {
					result = Lookup_PostCalibration.CALIBRATION_ON_HOLD;
				} else if (fr != null && !fr.isAwaitingFinalisation()) {

					if (fr.getRecommendations().contains(FailureReportRecommendationsEnum.ADJUSTMENT)) {
						result = Lookup_PostCalibration.ADJUSTMENT_APPROVED;
					} else if (fr.getRecommendations().contains(FailureReportRecommendationsEnum.REPAIR)) {
						if (fr.getOperationByTrescal()) {
							result = Lookup_PostCalibration.INHOUSE_REPAIR;
						} else {
							result = Lookup_PostCalibration.THIRD_PARTY_REPAIR;
						}
					} else if (fr.getRecommendations().contains(FailureReportRecommendationsEnum.SCRAPPING_PROPOSAL)) {
						result = Lookup_PostCalibration.FAILED_UNIT_BER;
					} else {
						result = Lookup_PostCalibration.NOT_SUCCESSFUL;
					}
				}
				if (ao.getGenericValue().equals(ActionOutcomeValue_Calibration.NOT_SUCCESSFUL)) {
					result = Lookup_PostCalibration.NOT_SUCCESSFUL;
				}
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_PostCalibration.values();
	}
}