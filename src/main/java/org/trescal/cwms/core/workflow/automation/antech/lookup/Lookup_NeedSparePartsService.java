package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Repair;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Component("Lookup_NeedSpareParts")
public class Lookup_NeedSparePartsService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines for most recent repair if spare parts are needed";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: REPAIR OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		JobItemWorkRequirement jiwr = ji.getNextWorkReq();
		LookupResultMessage result = null;
		ActionOutcome ao = null;
		for (JobItemAction action : ji.getActions()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;

				for (StateGroupLink sgl : activity.getActivity().getGroupLinks()) {
					if (sgl.getGroup().equals(StateGroup.REPAIR) && !activity.isDeleted()) {
						ao = activity.getOutcome();
					}
				}
			}
		}
		if (ao != null) {
			ActionOutcomeValue_Repair aor = (ActionOutcomeValue_Repair) ao.getGenericValue();

			switch (aor) {
			case REPLACEMENT_COMPONENTS_REQUIRED:
				result = Lookup_NeedSpareParts.REPLACE_COMPONENTS;
				break;
			case SPARES_REQUIRED:
				result = Lookup_NeedSpareParts.SPARE_PARTS;
				break;
			default:
				result = Lookup_NeedSpareParts.NO;
				break;
			}

		}

		jiwr.getWorkRequirement().setStatus(WorkrequirementStatus.COMPLETE);

		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_NeedSpareParts.values();
	}

}
