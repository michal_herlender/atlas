package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NeedRepairInspectionReport;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_NeedRepairInspectionReport")
public class Lookup_NeedRepairInspectionReportService extends Lookup {

	@Autowired
	private RepairInspectionReportService rirService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Need Repair Inspection Report");

		JobItem ji = jobItemService.findJobItem(jobItemId);
		Subdiv subdiv = ji.getCurrentAddr().getSub();
		RepairInspectionReport rir = rirService.getRirByJobItemIdAndSubdivId(jobItemId, subdiv.getSubdivid());

		/*
		 * If next wr has service type repair in current jobitem subdiv, and we don't
		 * have already a validated RIR, go make one
		 * 
		 */
		if (ji.getNextWorkReq().getWorkRequirement().getServiceType().getRepair()) {
			if (WorkRequirementType.THIRD_PARTY.equals(ji.getNextWorkReq().getWorkRequirement().getType())
					&& (rir == null || BooleanUtils.isNotTrue(rir.getValidatedByManager())))
				return Lookup_NeedRepairInspectionReport.YES_TO_MANAGER_PART;
			else if (WorkRequirementType.REPAIR.equals(ji.getNextWorkReq().getWorkRequirement().getType())
					&& (rir == null || BooleanUtils.isNotTrue(rir.getCompletedByTechnician())))
				return Lookup_NeedRepairInspectionReport.YES_TO_TECHNICIAN_PART;
		}
		return Lookup_NeedRepairInspectionReport.NO;
	}

	@Override
	public String getDescription() {
		return "Decides whether an RIR is needed";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_NeedRepairInspectionReport.values();
	}

}
