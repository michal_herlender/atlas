package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_IsOnRepair implements LookupResultMessage {
	
	YES("lookupresultmessage.isonrepair.yes"),
	NO("lookupresultmessage.isonrepair.no");
	
	private String messageCode;
	
	private Lookup_IsOnRepair(String messageCode) {
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return messageCode;
	}
}