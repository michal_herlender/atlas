package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_ClientDecisionTPFurtherWork implements ActionOutcomeValue {
	APPROVED_FURTHER_WORK(
			"Client has approved further work",
			"actionoutcome.cd_tp_furtherwork.approved"),
	INSPECTION_FEES_ONLY(
			"Client requested no further action, inspection fees only",
			"actionoutcome.cd_tp_furtherwork.inspectiononly"),
	AS_FOUND_RESULTS_ONLY(
			"Client has approved as-found results only",
			"actionoutcome.cd_tp_furtherwork.asfoundonly");

	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_ClientDecisionTPFurtherWork(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.CLIENT_DECISION_TP_FURTHER_WORK;
	}

}
