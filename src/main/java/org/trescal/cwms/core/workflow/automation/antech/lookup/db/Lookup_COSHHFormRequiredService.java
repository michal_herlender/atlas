package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_COSHHFormRequired;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

/**
 * No longer relevant, previously used "coshh required" model flag at Antech
 * Always returns NO
 * TODO : Remove from workflow via design / script / query to delete LookupInstance and delete class once confirmed unused
 */
@Deprecated
@Component("Lookup_COSHHFormRequired")
public class Lookup_COSHHFormRequiredService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item requires a COSHH form or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: COSHH FORM REQUIRED");
		return Lookup_COSHHFormRequired.NO;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_COSHHFormRequired.values();
	}
}