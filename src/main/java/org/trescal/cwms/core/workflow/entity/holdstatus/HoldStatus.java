package org.trescal.cwms.core.workflow.entity.holdstatus;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Entity
@DiscriminatorValue("holdstatus")
public class HoldStatus extends ItemStatus
{
	/*
	 * Hold status established in constructor rather than implementing method, 
	 * as hibernate proxy narrowing was causing DWR to fail.
	 * GB 2016-03-07
	 */
	public HoldStatus() {
		super.hold = true;
	}
}