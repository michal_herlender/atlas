package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_StartOnSiteCertificateReplacement
		extends HookInterceptor<HookInterceptor_StartOnSiteCertificateReplacement.Input> {

	public static final String HOOK_NAME = "start-onsite-certificate-replacement";

	public static class Input {
		public Certificate certificate;
		public JobItem ji;
		public Contact replacedBy;
		public Date replacementDate;

		public Input(Certificate certificate, JobItem ji, Contact replacedBy, Date replacementDate) {
			super();
			this.certificate = certificate;
			this.ji = ji;
			this.replacedBy = replacedBy;
			this.replacementDate = replacementDate;
		}
	}

	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: START ONSITE CETIFICATE REPLACEMENT");
			Certificate certificate = input.certificate;
			Contact con = input.replacedBy != null ? input.replacedBy : this.sessionServ.getCurrentContact();

			JobItem ji = input.ji;
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			if (context.getActivity() != null) {
				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				jia.setStartStamp(input.replacementDate != null ? input.replacementDate : new Date());
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());
				jia.setRemark(certificate.getCertno());

				if (context.isCompleteActivity()) {
					jia.setEndStamp(new Date());
					jia.setTimeSpent(5);
					jia.setCompletedBy(con);
				}

				ItemStatus endStatus = null;
				if (context.isCompleteActivity()) {
					endStatus = super.getNextStatus(context);
					ji.setState(endStatus);
					jia.setEndStatus(endStatus);
				} else
					ji.setState(context.getActivity());

				super.persist(jia, endStatus, context);

				// adds the action into the job item in the session
				ji.getActions().add(jia);
				jobItemService.updateJobItem(ji);
			}
		}
	}
}