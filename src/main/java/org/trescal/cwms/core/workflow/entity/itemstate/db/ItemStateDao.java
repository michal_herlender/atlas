package org.trescal.cwms.core.workflow.entity.itemstate.db;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.system.dto.TranslationDTO;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.dto.PlantillasItemStateDTO;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.progressactivity.ProgressActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface ItemStateDao extends BaseDao<ItemState, Integer>
{	
	
	ItemActivity findItemActivity(Integer id);
	
	ItemActivity findItemActivityByName(String name);
	
	ItemStatus findItemStatus(int id);
	
	ItemStatus findItemStatusByName(String name);
	
	List<ItemState> getAll(StateGroup stateGroup);
	
	List<ItemState> getAllForStateGroups(List<Integer> stateGroups);
	
	List<ItemActivity> getAllItemActivities();
	
	List<KeyValueIntegerString> getDTOList(Collection<Integer> stateIds, Locale locale);

	List<ItemStateDTO> getHoldActivities(List<Integer> exist_list);
	
	List<ItemStateDTO> getOverrideActivities(List<Integer> exist_list);
	
	List<ItemStateDTO> getProgressActivities(List<Integer> exist_list);
	
	List<ItemStateDTO> getTransitActivities(List<Integer> exist_list);
	
	List<ItemStateDTO> getWorkActivities(List<Integer> exist_list);
	
	List<ItemStatus> getAllItemStatuses(Class<? extends ItemStatus> clazz, boolean includeRetired);
	
	List<KeyValueIntegerString> getAllTranslatedItemStatuses(Class<? extends ItemStatus> clazz, boolean includeRetired, Locale locale);
	
	List<ProgressActivity> getAllProgressActivities(boolean includeRetired);
	
	List<WorkActivity> getAllWorkActivities();
	
	List<Integer> getItemStateIdsForDepartmentTypes(EnumSet<DepartmentType> deptTypes);
	
	List<Integer> getItemStateIdsForStateGroups(EnumSet<StateGroup> stateGroups);

	PlantillasItemStateDTO getPlantillasItemState(Locale locale, int stateId);
	
	List<PlantillasItemStateDTO> getPlantillasItemStates(Locale locale);
	
	List<TranslationDTO> getTranslatedItemStates(Locale locale, boolean includeRetired);

	ItemActivity findAllItemActivityByName(String name);
	
	List<HoldStatus> getHoldStatus();
}