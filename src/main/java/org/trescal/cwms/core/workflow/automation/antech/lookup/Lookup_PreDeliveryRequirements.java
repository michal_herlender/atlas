package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_PreDeliveryRequirements implements LookupResultMessage {
	
	INTERNAL("lookupresultmessage.predeliveryrequirements.internal"),
	INSIDE_COMPANY("lookupresultmessage.predeliveryrequirements.inside"),
	EXTERNAL("lookupresultmessage.predeliveryrequirements.external"),
	ALREADY_AT_CUSTOMER_ADDRESS("lookupresultmessage.predeliveryrequirements.atcustomeraddress");
	
	private String messageCode;
	
	private Lookup_PreDeliveryRequirements(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return messageCode;
	}
}