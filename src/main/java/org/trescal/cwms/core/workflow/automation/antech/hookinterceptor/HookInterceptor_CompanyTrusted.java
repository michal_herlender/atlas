package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

@Service
public class HookInterceptor_CompanyTrusted extends HookInterceptor<List<JobItem>>
{
	public static final String HOOK_NAME = "company-trusted";

	@Transactional
	@Override
	public void recordAction(List<JobItem> items) throws OpenActivityException
	{
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive())
		{
			this.logger.debug("RUNNING HOOK: COMPANY TRUSTED");
			Contact con = this.sessionServ.getCurrentContact();

			for (JobItem ji : items)
			{
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				jia.setStartStamp(new Date());
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());

				if (context.isCompleteActivity())
				{
					jia.setEndStamp(new Date());
					jia.setTimeSpent(5);
					jia.setCompletedBy(con);
				}

				ItemStatus endStatus = null;
				if (context.isCompleteActivity())
				{
					// see if there is a next status
					endStatus = super.getNextStatus(context);
					// if not
					if (endStatus == null) {
						// get the status before the item was put on stop
						endStatus = this.statusCalcServ.getPreviousStatus(ji, StateGroup.ONSTOP);
					}
					ji.setState(endStatus);
					jia.setEndStatus(endStatus);
				}
				else ji.setState(context.getActivity());
				jobItemService.updateJobItem(ji);
				this.persist(jia, endStatus, context);
			}
		}
	}
}