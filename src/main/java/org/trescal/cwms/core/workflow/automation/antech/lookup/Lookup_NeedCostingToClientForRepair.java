package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_NeedCostingToClientForRepair implements LookupResultMessage {

	YES("lookupresultmessage.needcostingtoclientforrepair.yes"),
	NO("lookupresultmessage.needcostingtoclientforrepair.no");

	private String messageCode;

	private Lookup_NeedCostingToClientForRepair(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}

}
