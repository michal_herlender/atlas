package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemIsOrificePlate;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

/**
 * No longer relevant, previously used "always base first" model flag at Antech
 * Always returns NO
 * TODO : Remove from workflow via design / script / query to delete LookupInstance and delete class once confirmed unused
 */
@Component("Lookup_ItemIsOrificePlate")
@Deprecated
public class Lookup_ItemIsOrificePlateService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item is an orifice plate or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM IS ORIFICE PLATE");
		return Lookup_ItemIsOrificePlate.NO;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemIsOrificePlate.values();
	}
}