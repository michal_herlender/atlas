package org.trescal.cwms.core.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Controller @IntranetController
public class ViewCleaningListController
{
	@Autowired
	private ItemStateService stateServ;

	@RequestMapping(value="/cleaninglist.htm")
	protected ModelAndView handleRequestInternal() throws Exception
	{
		// This may eventually need to be a state group instead of a single
		// state, but this will be fine for now
		ItemStatus status = this.stateServ.findItemStatusByName("Awaiting cleaning");
		// if there is a cleaning status
		if (status != null) {
			int stateid = status.getStateid();
			// show page with all items at that status
			return new ModelAndView(new RedirectView("viewselectedjobitems.htm?stateid=" + stateid));
		}
		// if there is no cleaning statuses
		else {
			// show same page but with no items
			return new ModelAndView(new RedirectView("viewselectedjobitems.htm?fallback=true"));
		}
	}
}