package org.trescal.cwms.core.workflow.entity.stategrouplink.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.dto.JobItemsInStateDTO;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

public interface StateGroupLinkDao extends BaseDao<StateGroupLink, Integer> {

	List<StateGroupLink> getAllByStateGroup(StateGroup group);

	List<JobItemsInStateDTO> getJobItemsInStateGroup(StateGroup stateGroup, Integer companyId, Integer subdivId,
			Locale locale);

	List<ItemStateDTO> getLinkedItemStates(StateGroup stateGroup, Locale locale);
}