package org.trescal.cwms.core.workflow.form.edit;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class EditHookForm {
	private String name;
	private Boolean active;
	private Boolean alwaysPossible;
	private Boolean beginActivity;
	private Boolean completeActivity;
	private Integer defaultActivityId;
	private List<EditHookActivtyDto> hookActivityDtos;
	private EditHookActivtyDto templateDto;
	
	@NotNull
	@Length(min=1 , max=100)
	public String getName() {
		return name;
	}
	@NotNull
	public Boolean getActive() {
		return active;
	}
	@NotNull
	public Boolean getAlwaysPossible() {
		return alwaysPossible;
	}
	@NotNull
	public Boolean getBeginActivity() {
		return beginActivity;
	}
	@NotNull
	public Boolean getCompleteActivity() {
		return completeActivity;
	}
	@NotNull
	public Integer getDefaultActivityId() {
		return defaultActivityId;
	}
	@Valid
	public List<EditHookActivtyDto> getHookActivityDtos() {
		return hookActivityDtos;
	}
	// Template DTO, does not require validation
	public EditHookActivtyDto getTemplateDto() {
		return templateDto;
	}

	public void setName(String name) {
		this.name = name;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public void setAlwaysPossible(Boolean alwaysPossible) {
		this.alwaysPossible = alwaysPossible;
	}
	public void setBeginActivity(Boolean beginActivity) {
		this.beginActivity = beginActivity;
	}
	public void setCompleteActivity(Boolean completeActivity) {
		this.completeActivity = completeActivity;
	}
	public void setDefaultActivityId(Integer defaultActivityId) {
		this.defaultActivityId = defaultActivityId;
	}
	public void setHookActivityDtos(List<EditHookActivtyDto> hookActivities) {
		this.hookActivityDtos = hookActivities;
	}
	public void setTemplateDto(EditHookActivtyDto templateHookActivity) {
		this.templateDto = templateHookActivity;
	}
}
