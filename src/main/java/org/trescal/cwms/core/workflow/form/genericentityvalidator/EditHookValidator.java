package org.trescal.cwms.core.workflow.form.genericentityvalidator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.form.edit.EditHookForm;

@Component
public class EditHookValidator extends AbstractBeanValidator {
	
	@Override
	public boolean supports(Class<?> arg0) {
		return EditHookForm.class.isAssignableFrom(arg0);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			EditHookForm form = (EditHookForm) target;
			if (form.getActive()) {
				if (!form.getAlwaysPossible() && form.getHookActivityDtos().isEmpty()) {
					errors.reject("When an active hook is not always possible, at least one hook activity must be configured.");
				}
			}
		}
		
	}
}
