package org.trescal.cwms.core.workflow.automation.common.lookup;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

/**
 * @author jamiev
 */
public interface StatusCalculationService
{
	/**
	 * Calculates and returns the end {@link ItemStatus} for the activity (and
	 * therefore the new current {@link ItemStatus} for the {@link JobItem})
	 * using any necessary {@link Lookup} classes.
	 * 
	 * @param activity the {@link ItemActivity} being completed
	 * @param ji the {@link JobItem} the {@link ItemActivity} is taking place on
	 * @return the {@link ItemStatus}
	 */
	ItemStatus getNextStatus(ItemActivity activity, JobItem ji);

	/**
	 * Retrieves the starting {@link ItemStatus} at the last point a
	 * {@link JobItem} had an overriding {@link ItemActivity} in the
	 * {@link StateGroup} with the given group key. Used for reverting a
	 * {@link JobItem} to an {@link ItemStatus} it was at before it was on-stop,
	 * on hold, etc.
	 * 
	 * @param ji the {@link JobItem}
	 * @param group the {@link StateGroup} to look for
	 * @return the {@link JobItem}'s previous {@link ItemStatus}
	 */
	ItemStatus getPreviousStatus(JobItem ji, StateGroup group);
}
