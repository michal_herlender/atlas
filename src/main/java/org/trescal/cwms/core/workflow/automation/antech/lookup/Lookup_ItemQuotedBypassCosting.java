package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemQuotedBypassCosting implements LookupResultMessage {
	
	YES("lookupresultmessage.itemhasbeenquoted.yes"),
	NO("lookupresultmessage.itemhasbeenquoted.no");

	private String messageCode;
	
	private Lookup_ItemQuotedBypassCosting(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
	
	

}
