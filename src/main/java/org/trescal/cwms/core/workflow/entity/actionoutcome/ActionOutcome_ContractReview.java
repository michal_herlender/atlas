package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ContractReview;

@Entity
@DiscriminatorValue("contract_review")
public class ActionOutcome_ContractReview extends ActionOutcome {
	private ActionOutcomeValue_ContractReview value;
	
	public ActionOutcome_ContractReview() {
		this.type = ActionOutcomeType.CONTRACT_REVIEW;
	}

	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}
	
	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_ContractReview getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_ContractReview value) {
		this.value = value;
	}
}
