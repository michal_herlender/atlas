package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemRepairedAtTP implements LookupResultMessage
{
	YES("lookupresultmessage.itemrepairedattp.yes"),
	NO("lookupresultmessage.itemrepairedattp.no");
	
	private String messageCode;
	
	private Lookup_ItemRepairedAtTP(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}