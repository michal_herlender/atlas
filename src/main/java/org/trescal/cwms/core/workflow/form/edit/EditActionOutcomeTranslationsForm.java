package org.trescal.cwms.core.workflow.form.edit;

import java.util.Locale;
import java.util.Map;

import javax.validation.constraints.NotNull;

public class EditActionOutcomeTranslationsForm {
	private Map<Integer, Map<Locale, String>> aoIdsToTranslations;

	@NotNull
	public Map<Integer, Map<Locale, String>> getAoIdsToTranslations() {
		return aoIdsToTranslations;
	}

	public void setAoIdsToTranslations(Map<Integer, Map<Locale, String>> aoIdsToTranslations) {
		this.aoIdsToTranslations = aoIdsToTranslations;
	}
}
