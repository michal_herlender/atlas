package org.trescal.cwms.core.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.departmenttype.db.DepartmentTypeService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.TranslationDTO;
import org.trescal.cwms.core.workflow.entity.StateGroupType;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.workassignment.db.WorkAssignmentService;
import org.trescal.cwms.core.workflow.entity.workassignment.dto.WorkCountDay;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Controller used to display different lists of work that can be applied to
 * {@link DepartmentType}s.
 */
@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class ViewWorkForDepartmentTypeController {
    @Autowired
    private DepartmentService deptServ;
    @Autowired
    private DepartmentTypeService deptTypeServ;
    @Autowired
    private CapabilityCategoryService procCatServ;
    @Autowired
    private ItemStateService stateServ;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private WorkAssignmentService workAssignServ;

    @RequestMapping(value = "/viewworkfordepttype.htm")
    protected ModelAndView handleRequestInternal(Locale locale,
                                                 Model model,
                                                 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
        Subdiv subdiv = subdivService.get(subdivDto.getKey());
        model.addAttribute("works", workAssignServ.getWorksForDepartmentTypes(subdiv));
        model.addAttribute("statuses", this.prepareStatuses(locale));
        model.addAttribute("stategroups", this.prepareStateGroups());
        model.addAttribute("depttypes", this.prepareDeptTypes());
        model.addAttribute("depts", this.prepareDepts());
        model.addAttribute("proccats", this.prepareProcCats());
        model.addAttribute("workDays", Arrays.asList(WorkCountDay.values()));
        return new ModelAndView("/trescal/core/workflow/viewworkfordepttype", "model", model);
    }

    /*
     * Global map of department IDs to Departments (ok that they are for other subdivs)
     */
    public Map<Integer, Department> prepareDepts() {
        return deptServ.getAll().stream()
                .collect(Collectors.toMap(Department::getDeptid, Function.identity()));
    }
	
	/*
	 * Global map of DepartmentType IDs to DepartmentType for all active departments
     */
    public Map<Integer, DepartmentType> prepareDeptTypes() {
        return deptTypeServ.getActiveDepartmentTypes().stream()
                .collect(Collectors.toMap(Enum::ordinal, Function.identity()));
    }

    /*
     * Global map of ProcedureCategory IDs for all departments/subdivs/companies to ProcedureCategory
     */
    public Map<Integer, CapabilityCategory> prepareProcCats() {
        return procCatServ.getAll().stream()
                .collect(Collectors.toMap(CapabilityCategory::getId, Function.identity()));
    }

    /*
     * Global map of StateGroup ids to String display names (localized)
     */
    public Map<Integer, String> prepareStateGroups() {
        return StateGroup.getAllStateGroupsOfType(StateGroupType.DEPARTMENT)
                .stream()
                .collect(Collectors.toMap(StateGroup::getId, StateGroup::getDisplayName));
    }
	
	/*
	 * Prepares global map of statuses to localized descriptions
	 */
	public Map<Integer, String> prepareStatuses(Locale locale)
	{
		List<TranslationDTO> translations = this.stateServ.getTranslatedItemStates(locale, false);
        return translations.stream().collect(Collectors.toMap(TranslationDTO::getId, TranslationDTO::getBestTranslation));
	}
}