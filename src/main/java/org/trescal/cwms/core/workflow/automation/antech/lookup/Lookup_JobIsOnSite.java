package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_JobIsOnSite implements LookupResultMessage {
	YES("yes"), NO("no");

	private String messageCode;

	private Lookup_JobIsOnSite(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}

}
