package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_IsThirdPartyWork implements LookupResultMessage {

	NO("no"),
	YES("yes");

	private String messageCode;

	private Lookup_IsThirdPartyWork(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}

}
