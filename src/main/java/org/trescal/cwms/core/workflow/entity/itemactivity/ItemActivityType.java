package org.trescal.cwms.core.workflow.entity.itemactivity;

/*
 * 2017-10-29 GB: Created for user interface selection in workflow editor
 * TODO - perhaps we could eliminate the different classes and use this type in ItemActivity
 */
public enum ItemActivityType {
	UNDEFINED,
	HOLD_ACTIVITY,
	OVERRIDE_ACTIVITY,
	PROGRESS_ACTIVITY,
	TRANSIT_ACTIVITY,
	WORK_ACTIVITY
}
