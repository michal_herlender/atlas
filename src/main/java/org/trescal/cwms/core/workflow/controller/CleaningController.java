package org.trescal.cwms.core.workflow.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.workflow.form.CleaningForm;
import org.trescal.cwms.core.workflow.form.CleaningJobItem;
import org.trescal.cwms.core.workflow.form.CleaningValidator;

@Controller @IntranetController
public class CleaningController
{
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private CleaningValidator validator;
	
	@ModelAttribute("form")
	protected CleaningForm formBackingObject(@RequestParam(value="plantid", required=false, defaultValue="0") Integer barcode) throws Exception
	{
		return new CleaningForm();
	}
	
	@ModelAttribute("cleaningJI")
	protected CleaningJobItem jobItemsToClean(@ModelAttribute("form") CleaningForm form) throws Exception
	{
		if(form.getPlantids() == null)
		{
			form.setPlantids(new ArrayList<>());
			if(form.getPlantid() != null && !form.getPlantid().isEmpty())
				form.getPlantids().add(form.getPlantid());
		}
				
		CleaningJobItem cleaningJI = new CleaningJobItem();
				
		for (String plantid : form.getPlantids()) {
			if (NumberTools.isAnInteger(plantid)) {
				if(cleaningJI.getJis() == null) cleaningJI.setJis(new ArrayList<>());
				if(cleaningJI.getCleaneds() == null) cleaningJI.setCleaneds(new ArrayList<>());
				if(cleaningJI.getPlantids() == null) cleaningJI.setPlantids(new ArrayList<>());
					cleaningJI.getJis().add(this.jiServ.findMostRecentJobItem(Integer.parseInt(plantid)));
					cleaningJI.getCleaneds().add(false);
					cleaningJI.getPlantids().add(plantid);
					}
			}
		return cleaningJI;
	}
	
	@InitBinder("cleaningJI")
	protected void initBinder(WebDataBinder binder) throws Exception
	{
		binder.setValidator(validator);
	}
	
	@RequestMapping(value="/cleaning.htm", method=RequestMethod.POST)
	protected String onSubmit(@Valid @ModelAttribute("cleaningJI") CleaningJobItem cleaningJI, BindingResult result, @ModelAttribute("form") CleaningForm form, Model model) throws Exception
	{
		if(cleaningJI.getJis() != null)
			for (int i=0; i<cleaningJI.getJis().size();i++) {
				if(cleaningJI.getCleaneds().get(i).equals(true))
				{this.jiServ.cleanItem(cleaningJI.getJis().get(i), form.getTimeSpent(), form.getRemark());
				form.setCleaned(true);
				}
			}
		return formView();
	}
	
	@RequestMapping(value="/cleaning.htm", method=RequestMethod.GET)
	protected String formView() {
		return "trescal/core/workflow/cleaning";
	}
}