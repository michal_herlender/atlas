package org.trescal.cwms.core.workflow.form.genericentityvalidator;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.form.edit.EditItemStatusForm;

@Component
public class EditItemStatusValidator extends AbstractBeanValidator {
	
	@Autowired
	private SupportedLocaleService supportedLocaleService; 

	@Override
	public boolean supports(Class<?> arg0) {
		return EditItemStatusForm.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		// Perform further validation only if form has no JPA constraint errors (no nulls)
		if (!errors.hasErrors()) {
			EditItemStatusForm form = (EditItemStatusForm) target;
			// Not mandatory for an item status to have one or more next activities (manual override/hook may occur)
			Locale locale = this.supportedLocaleService.getPrimaryLocale();
			String defaultTranslation = form.getTranslations().get(locale);
			if ((defaultTranslation == null) || defaultTranslation.isEmpty()) {
				errors.reject("edititemstatus.translation","Item status must have translation defined for primary locale "+locale.toString());
			}
			// TODO - consider check for duplicate status names?
		}
		
	}
	
}
