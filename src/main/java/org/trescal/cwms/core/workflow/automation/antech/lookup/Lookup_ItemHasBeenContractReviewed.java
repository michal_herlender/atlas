package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemHasBeenContractReviewed implements LookupResultMessage
{
	YES("lookupresultmessage.itemhasbeencontractreviewed.yes"),
	NO("lookupresultmessage.itemhasbeencontractreviewed.no");
	
	private String messageCode;
	
	private Lookup_ItemHasBeenContractReviewed(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}