package org.trescal.cwms.core.workflow.automation.common.lookup;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service("LookupService")
public class LookupServiceImpl implements LookupService, ApplicationContextAware {

	@Override
	public List<LookupWrapper> getAllLookups() {
		LookupFunction[] lookupFunctions = LookupFunction.values();
		List<LookupWrapper> lookups = new ArrayList<LookupWrapper>();
		for (LookupFunction lf : lookupFunctions) {
			LookupWrapper lw = new LookupWrapper();
			lw.setBeanName(lf.name());
			lw.setDescription(lf.name());
			List<LookupResultMessage> lrm = new ArrayList<LookupResultMessage>();
			for (LookupResultMessage message : lf.getLookupResultMessages())
				lrm.add(message);
			lw.setResultMessages(lrm);
			lookups.add(lw);
		}
		return lookups;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	}
}