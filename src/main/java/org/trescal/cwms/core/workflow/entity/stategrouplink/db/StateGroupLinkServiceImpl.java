package org.trescal.cwms.core.workflow.entity.stategrouplink.db;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.dto.JobItemsInStateDTO;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Repository("StateGroupLinkService")
public class StateGroupLinkServiceImpl extends BaseServiceImpl<StateGroupLink, Integer>
		implements StateGroupLinkService {

	@Autowired
	private StateGroupLinkDao stateGroupLinkDao;

	@Override
	protected BaseDao<StateGroupLink, Integer> getBaseDao() {
		return stateGroupLinkDao;
	}

	@Override
	public List<StateGroupLink> getAllByStateGroup(StateGroup group) {
		return this.stateGroupLinkDao.getAllByStateGroup(group);
	}

	@Override
	public boolean containsStateGroup(Set<StateGroupLink> links, StateGroup group) {
		return links.stream().anyMatch(link -> link.getGroup().equals(group));
	}

	@Override
	public List<JobItemsInStateDTO> getJobItemsInStateGroup(StateGroup stateGroup, Integer companyId, Integer subdivId,
			Locale locale) {
		return stateGroupLinkDao.getJobItemsInStateGroup(stateGroup, companyId, subdivId, locale);
	}

	@Override
	public List<ItemStateDTO> getLinkedItemStates(StateGroup stateGroup, Locale locale) {
		return stateGroupLinkDao.getLinkedItemStates(stateGroup, locale);
	}
}