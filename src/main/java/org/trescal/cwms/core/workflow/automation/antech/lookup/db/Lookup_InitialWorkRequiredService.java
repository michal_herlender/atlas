package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_InitialWorkRequired;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Component("Lookup_InitialWorkRequired")
public class Lookup_InitialWorkRequiredService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether an item requires inspection, repair, adjustment or calibration and where this can be performed.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: INITIAL WORK REQUIRED");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions to find an inspection activity
		for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;

				if (activity.getActivityDesc().equals("Unit inspected by engineer") && !activity.isDeleted()) {
					// get the outcome of the contract review
					ao = activity.getOutcome();
					break;
				}
			}
		}
		// if there's no inspection, look for a contract review
		if (ao == null) {
			for (JobItemAction action : actions.descendingSet()) {
				if (action instanceof JobItemActivity) {
					JobItemActivity activity = (JobItemActivity) action;

					if (activity.getActivityDesc().equals("Contract review") && !activity.isDeleted()) {
						// get the outcome of the contract review
						ao = activity.getOutcome();
						break;
					}
				}
			}
		}
		if (ao != null) {
			String desc = ao.getDescription();
			if (desc.contains("in-house calibration")) {
				result = Lookup_InitialWorkRequired.INHOUSE_CALIBRATION;
			} else if (desc.contains("third party calibration")) {
				result = Lookup_InitialWorkRequired.THIRD_PARTY_CALIBRATION;
			} else if (desc.contains("in-house repair")) {
				result = Lookup_InitialWorkRequired.INHOUSE_REPAIR;
			} else if (desc.contains("third party repair")) {
				result = Lookup_InitialWorkRequired.THIRD_PARTY_REPAIR;
			} else if (desc.contains("adjustment")) {
				result = Lookup_InitialWorkRequired.ADJUSTMENT;
			} else if (desc.contains("inspection")) {
				result = Lookup_InitialWorkRequired.PRELIMINARY_INSPECTION;
			} else if (desc.contains("on-site calibration")) {
				result = Lookup_InitialWorkRequired.ONSITE_CALIBRATION;
			} else if (desc.contains("No action")) {
				result = Lookup_InitialWorkRequired.NO_ACTION;
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_InitialWorkRequired.values();
	}
}