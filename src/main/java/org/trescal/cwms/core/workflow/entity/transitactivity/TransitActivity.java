package org.trescal.cwms.core.workflow.entity.transitactivity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivityType;

@Entity
@DiscriminatorValue("transitactivity")
public class TransitActivity extends ItemActivity
{
	/*
	 * Override and progress status established in constructor rather than implementing method, 
	 * as hibernate proxy narrowing was causing DWR exceptions in previous implementation.
	 * GB 2016-04-09
	 */
	public TransitActivity() {
		this.override = false;
		this.progress = false;
		this.type = ItemActivityType.TRANSIT_ACTIVITY;		
	}
}