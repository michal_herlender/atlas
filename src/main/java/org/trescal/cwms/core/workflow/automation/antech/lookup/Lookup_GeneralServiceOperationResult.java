package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_GeneralServiceOperationResult implements LookupResultMessage {
	
	COMPLETED("lookupresultmessage.generalserviceoperationresult.completed"),
	CANCELLED("lookupresultmessage.generalserviceoperationresult.cancelled"),
	ON_HOLD("lookupresultmessage.generalserviceoperationresult.onhold"),
	NEEDS_FAILURE_REPORT("lookupresultmessage.generalserviceoperationresult.needfailurereport");
	
	private String messageCode;

	private Lookup_GeneralServiceOperationResult(String messageCode){
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}

}
