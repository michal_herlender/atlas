package org.trescal.cwms.core.workflow.entity.nextactivity.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;

public interface NextActivityDao extends BaseDao<NextActivity, Integer> {
	
	NextActivity findNextActivityWithActivityAndStatus(int activityId, int statusId);
	
	List<NextActivity> getNextActivitiesForStatus(int statusId, Boolean manualEntryAllowed);
}