package org.trescal.cwms.core.workflow.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.workflow.entity.StateGroupType;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.entity.stategrouplink.db.StateGroupLinkService;

@Controller @IntranetController
public class DashboardConfigurationController {

	@Autowired
	private StateGroupLinkService stateGroupLinkService;
	
	@ModelAttribute("stateGroupLinksMap")
	public Map<StateGroup, List<StateGroupLink>> stateGroupLinksMap() {
		return stateGroupLinkService.getAll().stream().filter(link -> link.getGroup().getType().equals(StateGroupType.DEPARTMENT)).collect(Collectors.groupingBy(StateGroupLink::getGroup));
	}
	
	@ModelAttribute("linktypes")
	public StateGroupLinkType[] linkTypes() {
		return StateGroupLinkType.values();
	}
	
	@RequestMapping(value="/dashboardconfig.htm", method=RequestMethod.GET)
	public ModelAndView onRequest() {
		return new ModelAndView("/trescal/core/workflow/dashboardconfiguration");
	}
}