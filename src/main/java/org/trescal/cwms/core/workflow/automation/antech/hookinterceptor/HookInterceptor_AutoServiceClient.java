package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_AutoServiceClient extends HookInterceptor<Map<Integer, List<JobItem>>> {
	
	@Value("${cwms.deployed.url}")
	private String deployedURL;
	@Autowired
	private UserService userServ;
	public static final String HOOK_NAME = "auto-service-client";

	@Transactional
	@Override
	public void recordAction(Map<Integer, List<JobItem>> itemsBeingChased) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: AUTO SERVICE CLIENT");

			for (Integer chaseNum : itemsBeingChased.keySet()) {
				List<JobItem> jis = itemsBeingChased.get(chaseNum);

				for (JobItem ji : jis) {
					Contact con = this.userServ.findSystemContact();
					HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

					JobItemActivity jia = new JobItemActivity();
					jia.setActivity(context.getActivity());
					jia.setActivityDesc(context.getActivity().getDescription());
					jia.setStartStatus((ItemStatus) ji.getState());
					jia.setStartStamp(new Date());
					jia.setStartedBy(con);
					jia.setCreatedOn(new Date());
					jia.setJobItem(ji);
					jia.setComplete(context.isCompleteActivity());

					String link = this.deployedURL.concat("viewjob.htm?jobid=")
							.concat(String.valueOf(ji.getJob().getJobid())).concat("&loadtab=email-tab");
					jia.setRemark("CWMS auto-progressing - " + NumberTools.appendAptLettersToNumber(chaseNum)
							+ " reminder (<a href=\"" + link + "\">View</a>)");

					if (context.isCompleteActivity()) {
						jia.setEndStamp(new Date());
						jia.setTimeSpent(5);
						jia.setCompletedBy(con);
					}

					// add the action to the collection
					ji.getActions().add(jia);

					ji = jobItemService.mergeJobItem(ji);

					if (context.isCompleteActivity()) {
						jia.setEndStatus((ItemStatus) ji.getState());
					}

					this.persist(jia, ji.getState(), context);
				}
			}
		}
	}
}