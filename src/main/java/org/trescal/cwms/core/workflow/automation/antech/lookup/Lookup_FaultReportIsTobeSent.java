package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_FaultReportIsTobeSent implements LookupResultMessage {

	NO("no"),
	CSR_DESICION("lookupresultmessage.csrdecision");

	private String messageCode;

	private Lookup_FaultReportIsTobeSent(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}

}
