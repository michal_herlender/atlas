package org.trescal.cwms.core.workflow.dto;

import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;

public class ItemStateDTO
{
	private Integer id;
	private String description;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public ItemStateDTO(Integer id, String description) {
		super();
		this.id = id;
		this.description = description;
	}
	
	public ItemStateDTO(ItemActivity itemActivity) {
		this.id = itemActivity.getStateid();
		this.description = itemActivity.getDescription();
	}
}