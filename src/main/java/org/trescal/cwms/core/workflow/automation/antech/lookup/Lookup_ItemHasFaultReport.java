package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemHasFaultReport implements LookupResultMessage
{
	YES("lookupresultmessage.itemhasfaultreport.yes"),
	NO("lookupresultmessage.itemhasfaultreport.no");
	
	private String messageCode;
	
	private Lookup_ItemHasFaultReport(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}