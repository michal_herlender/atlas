package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_PostTPInspectionOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_PostTPFurtherWork;

@Component("Lookup_PostTPInspectionOutcome")
public class Lookup_PostTPInspectionOutcomeService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the outcome of the inspection after receiving the item back from a third party.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: POST TP INSPECTION OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions to get the MOST RECENT inspection
		// in case there is more than one
		for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity jia = (JobItemActivity) action;
				// in some legacy cases, activity may be null!
				if (jia.getActivity() != null) {
					if (jia.getActivity().getDescription().contains("inspected by engineer (post third party")
							&& !jia.isDeleted()) {
						ao = jia.getOutcome();
						break;
					}
				}
			}
		}
		if (ao != null) {
			if (ActionOutcomeValue_PostTPFurtherWork.NO_FURTHER_WORK_FOR_CURRENT_WR.name()
					.equals(ao.getGenericValue().name())) {
				ji.getNextWorkReq().getWorkRequirement().setStatus(WorkrequirementStatus.COMPLETE);
				jobItemService.mergeJobItem(ji);
				result = Lookup_PostTPInspectionOutcome.NO_FURTHER_WORK_SWITCH_TO_NEXT_WORK;
			} else if (ActionOutcomeValue_PostTPFurtherWork.FURTHER_WORK_NECESSARY_BY_SELECTION_OF_NEXT_WR.name()
					.equals(ao.getGenericValue().name())) {
				ji.getNextWorkReq().getWorkRequirement().setStatus(WorkrequirementStatus.COMPLETE);
				jobItemService.mergeJobItem(ji);
				result = Lookup_PostTPInspectionOutcome.FURTHER_WORK_NECESSARY_AWAITING_SELECTION_OF_NEXT_WR;
			} else if (ActionOutcomeValue_PostTPFurtherWork.NOT_COMPLETED_RETURN_TO_SUPPLIER.name()
					.equals(ao.getGenericValue().name())) {
				result = Lookup_PostTPInspectionOutcome.WORK_NOT_COMPLETED_RETURN_TO_SUPPLIER;
			} else if (ActionOutcomeValue_PostTPFurtherWork.UNSUCCESSFUL.name().equals(ao.getGenericValue().name())) {
				ji.getNextWorkReq().getWorkRequirement().setStatus(WorkrequirementStatus.COMPLETE);
				jobItemService.mergeJobItem(ji);
				result = Lookup_PostTPInspectionOutcome.WORK_UNSUCCESSFUL_AWAITING_FR_UPDATE;
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_PostTPInspectionOutcome.values();
	}
}