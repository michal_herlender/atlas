package org.trescal.cwms.core.workflow.entity.workassignment;

import java.util.Comparator;

import org.trescal.cwms.core.system.Constants;

/**
 * @author jamiev
 */
public class DaysComparator implements Comparator<String>
{
	static final int ALL = 1;
	static final int BUSINESS = 8;
	static final int DAY_1 = 6;
	static final int DAY_2 = 5;
	static final int DAY_3 = 4;
	static final int DAY_4 = 3;
	static final int DAY_5 = 2;
	static final int OVERDUE = 7;

	public int compare(String day1, String day2)
	{
		Integer int1 = 0;
		Integer int2 = 0;

		if (day1.equals(Constants.DAYS_MAP_ALL))
		{
			int1 = ALL;
		}
		if (day1.equals(Constants.DAYS_MAP_5_DAYS))
		{
			int1 = DAY_5;
		}
		if (day1.equals(Constants.DAYS_MAP_4_DAYS))
		{
			int1 = DAY_4;
		}
		if (day1.equals(Constants.DAYS_MAP_3_DAYS))
		{
			int1 = DAY_3;
		}
		if (day1.equals(Constants.DAYS_MAP_2_DAYS))
		{
			int1 = DAY_2;
		}
		if (day1.equals(Constants.DAYS_MAP_1_DAYS))
		{
			int1 = DAY_1;
		}
		if (day1.equals(Constants.DAYS_MAP_OVERDUE))
		{
			int1 = OVERDUE;
		}
		if (day1.equals(Constants.DAYS_MAP_BUSINESS))
		{
			int1 = BUSINESS;
		}

		if (day2.equals(Constants.DAYS_MAP_ALL))
		{
			int2 = ALL;
		}
		if (day2.equals(Constants.DAYS_MAP_5_DAYS))
		{
			int2 = DAY_5;
		}
		if (day2.equals(Constants.DAYS_MAP_4_DAYS))
		{
			int2 = DAY_4;
		}
		if (day2.equals(Constants.DAYS_MAP_3_DAYS))
		{
			int2 = DAY_3;
		}
		if (day2.equals(Constants.DAYS_MAP_2_DAYS))
		{
			int2 = DAY_2;
		}
		if (day2.equals(Constants.DAYS_MAP_1_DAYS))
		{
			int2 = DAY_1;
		}
		if (day2.equals(Constants.DAYS_MAP_OVERDUE))
		{
			int2 = OVERDUE;
		}
		if (day2.equals(Constants.DAYS_MAP_BUSINESS))
		{
			int2 = BUSINESS;
		}

		return int1.compareTo(int2);
	}
}