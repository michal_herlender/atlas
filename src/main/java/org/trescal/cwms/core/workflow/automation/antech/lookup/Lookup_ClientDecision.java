package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ClientDecision implements LookupResultMessage
{
	POSITIVE("lookupresultmessage.clientdecision.positive"),
	NEGATIVE("lookupresultmessage.clientdecision.negative"),
	NEGATIVE_BACK_TOAWAITINGCOSTING("lookupresultmessage.clientdecision.negativebacktoawaitingcosting");
	
	private String messageCode;
	
	private Lookup_ClientDecision(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}