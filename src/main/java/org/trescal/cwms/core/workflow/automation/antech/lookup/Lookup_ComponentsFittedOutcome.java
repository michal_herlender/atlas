package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ComponentsFittedOutcome implements LookupResultMessage
{
	FURTHER_REPAIR("lookupresultmessage.componentsfittedoutcome.furtherrepair"),
	CALIBRATION("lookupresultmessage.componentsfittedoutcome.calibration"),
	NO_FURTHER_WORK("lookupresultmessage.componentsfittedoutcome.nofurtherwork");
	
	private String messageCode;
	
	private Lookup_ComponentsFittedOutcome(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}