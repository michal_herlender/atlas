package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_PerformContractReview
		extends HookInterceptor<HookInterceptor_PerformContractReview.Input> {

	private static final int DEFAULT_TIME = 5;
	public static final String HOOK_NAME = "perform-contract-review";

	public static class Input {
		public ContractReview conRev;
		public Integer timeSpent;
		public Date startDate;
		public Date endDate;
		public Contact contact;

		public Input(ContractReview conRev, Integer time, Date startDate, Date endDate, Contact contact) {
			this.conRev = conRev;
			this.timeSpent = time;
			this.startDate = startDate;
			this.endDate = endDate;
			this.contact = contact;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: PERFORM CONTRACT REVIEW");
			Set<ContractReviewItem> items = input.conRev.getItems();
			Contact con = input.contact != null ? input.contact : this.sessionServ.getCurrentContact();

			for (ContractReviewItem cri : items) {
				JobItem ji = cri.getJobitem();
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				jia.setRemark(input.conRev.getComments());
				if (input.startDate == null)
					jia.setStartStamp(new Date());
				else
					jia.setStartStamp(input.startDate);
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());

				if (context.isCompleteActivity()) {
					if (input.timeSpent == null)
						jia.setTimeSpent(DEFAULT_TIME);
					else
						jia.setTimeSpent(input.timeSpent);

					if (input.endDate == null)
						jia.setEndStamp(new Date());
					else
						jia.setEndStamp(input.endDate);

					jia.setCompletedBy(con);
					jia.setOutcome(ji.getConRevOutcome());
				}

				// adds the action into the job item in the session
				if (ji.getActions() == null) {
					ji.setActions(new TreeSet<JobItemAction>(new JobItemActionComparator()));
				}
				ji.getActions().add(jia);

				ItemStatus endStatus = super.getNextStatus(context);

				ji.setState(endStatus);
				if (context.isCompleteActivity()) {
					jia.setEndStatus(endStatus);
				}

				jobItemService.updateJobItem(ji);
				this.persist(jia, endStatus, context);
			}
		}
	}
}