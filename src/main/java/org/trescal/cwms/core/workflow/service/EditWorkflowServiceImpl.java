package org.trescal.cwms.core.workflow.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.misc.entity.hook.db.HookService;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;
import org.trescal.cwms.core.misc.entity.hookactivity.db.HookActivityService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.holdactivity.HoldActivity;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupinstance.db.LookupInstanceService;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatusComparator;
import org.trescal.cwms.core.workflow.entity.outcomestatus.db.OutcomeStatusService;
import org.trescal.cwms.core.workflow.entity.overrideactivity.OverrideActivity;
import org.trescal.cwms.core.workflow.entity.progressactivity.ProgressActivity;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.transitactivity.TransitActivity;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;
import org.trescal.cwms.core.workflow.form.edit.AbstractItemStateForm;
import org.trescal.cwms.core.workflow.form.edit.EditActionOutcomeDto;
import org.trescal.cwms.core.workflow.form.edit.EditActionOutcomeTranslationsForm;
import org.trescal.cwms.core.workflow.form.edit.EditHookActivtyDto;
import org.trescal.cwms.core.workflow.form.edit.EditHookForm;
import org.trescal.cwms.core.workflow.form.edit.EditItemActivityForm;
import org.trescal.cwms.core.workflow.form.edit.EditItemStatusForm;
import org.trescal.cwms.core.workflow.form.edit.EditLookupInstanceForm;
import org.trescal.cwms.core.workflow.form.edit.EditNextActivityDto;
import org.trescal.cwms.core.workflow.form.edit.EditOutcomeStatusDto;
import org.trescal.cwms.core.workflow.form.edit.EditStateGroupLinkDto;

@Service
public class EditWorkflowServiceImpl implements EditWorkflowService {
	
	@Autowired
	private ActionOutcomeService actionOutcomeService;
	@Autowired
	private HookService hookService; 
	@Autowired
	private HookActivityService hookActivityService; 
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private LookupInstanceService lookupInstanceService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private OutcomeStatusService outcomeStatusService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	private final static Logger logger = LoggerFactory.getLogger(EditWorkflowServiceImpl.class);
	
	@Override
	public void updateActionOutcomeTranslations(EditActionOutcomeTranslationsForm form) {
		for (Integer id : form.getAoIdsToTranslations().keySet()) {
			ActionOutcome ao = this.actionOutcomeService.get(id);
			Map<Locale, Translation> existingTranslations = new HashMap<>();
			ao.getTranslations().stream().forEach(tr -> existingTranslations.put(tr.getLocale(), tr));
			for(Locale locale : form.getAoIdsToTranslations().get(id).keySet()) {
				String text = form.getAoIdsToTranslations().get(id).get(locale);
				Translation translation = existingTranslations.get(locale);
				if (text.isEmpty() && translation != null) {
					ao.getTranslations().remove(translation);
				}
				else if (!text.isEmpty() && translation == null) {
					translation = new Translation(locale, text);
					ao.getTranslations().add(translation);
				}
				else if (!text.isEmpty() && translation != null) {
					translation.setTranslation(text);
				}
			}
		}
	}
	/*
	 * Updates basic fields on new or existing entity
	 */
	private void updateHookFields(Hook hook, EditHookForm form) {
		hook.setActive(form.getActive());
		hook.setActivity(form.getDefaultActivityId() != 0 ? this.itemStateService.findItemActivity(form.getDefaultActivityId()) : null);
		hook.setAlwaysPossible(form.getAlwaysPossible());
		hook.setBeginActivity(form.getBeginActivity());
		hook.setCompleteActivity(form.getCompleteActivity());
		hook.setName(form.getName());	
	}
	
	@Override
	public int createHook(EditHookForm form) {
		Hook hook = new Hook();
		updateHookFields(hook, form);
		hook.setHookActivities(new HashSet<>());
		updateHookActivities(form, hook);
		this.hookService.insertHook(hook);
		return hook.getId();
	}

	@Override
	public void updateHook(EditHookForm form, int hookId) {
		Hook hook = this.hookService.findHook(hookId);
		updateHookFields(hook, form);
		updateHookActivities(form, hook);
	}
	
	@Override
	public int createItemActivity(EditItemActivityForm form) {
		ItemActivity itemActivity = null;
		switch (form.getType()) {
		case HOLD_ACTIVITY:
			itemActivity = new HoldActivity();
			break;
		case OVERRIDE_ACTIVITY:
			itemActivity = new OverrideActivity();
			break;
		case PROGRESS_ACTIVITY:
			itemActivity = new ProgressActivity();
			break;
		case TRANSIT_ACTIVITY:
			itemActivity = new TransitActivity();
			break;
		case UNDEFINED:
			throw new IllegalArgumentException("UNDEFINED is an invalid actvity type");
		case WORK_ACTIVITY:
			itemActivity = new WorkActivity();
			break;
		default:
			throw new IllegalArgumentException("No activity type specified");
		}
		if (form.getLookupOrStatus()) {
			// Wires to lookup directly - no outcomestatus required
			// In validator - only able to have one active action outcome type at a time
			LookupInstance lookupInstance = lookupInstanceService.get(form.getLookupId());
			itemActivity.setActionOutcomes(new HashSet<>());
			updateActionOutcomes(form, itemActivity);
			itemActivity.setLookup(lookupInstance);
		}
		else {
			// Should wire to status - create a single outcomestatus for the selected activity
			// actionOutcomeType should always be null, for a status connection
			updateSingleOutcomeStatus(form, itemActivity);
		}
		
		updateItemActivityFields(form, itemActivity);
		itemActivity.setGroupLinks(new HashSet<>());
		updateStateGroupLinks(form, itemActivity);
		this.itemStateService.insertItemState(itemActivity);
		return itemActivity.getStateid();
	}
	
	private void updateItemActivityFields(EditItemActivityForm form, ItemActivity itemActivity) {
		itemActivity.setActive(form.getActive());
		itemActivity.setDescription(form.getDescription());
		itemActivity.setRetired(form.getRetired());
		itemActivity.setTranslations(createTranslations(form));
		if (form.getLookupOrStatus() && form.getActionOutcomesEnabled()) {
			itemActivity.setActionOutcomeType(form.getActionOutcomeType());
		}
		else {
			itemActivity.setActionOutcomeType(null);
		}
	}
	
	@Override
	public void updateItemActivity(EditItemActivityForm form, int activityId) {
		ItemActivity itemActivity = this.itemStateService.findItemActivity(activityId);
		updateItemActivityFields(form, itemActivity);
		updateStateGroupLinks(form, itemActivity);
		if (form.getLookupOrStatus()) {
			// Should wire to lookup
			if ((itemActivity.getLookup() == null) || (itemActivity.getLookup().getId() != form.getLookupId())) {
				itemActivity.setLookup(this.lookupInstanceService.get(form.getLookupId()));
			}
			// If changing status to lookup, there should be one existing action outcome - can be edited normally
			updateActionOutcomes(form, itemActivity);
		}
		else {
			// Should wire to status
			itemActivity.setLookup(null);
			itemActivity.getActionOutcomes().clear();
			updateSingleOutcomeStatus(form, itemActivity);
		}
	}

	@Override
	public int createItemStatus(EditItemStatusForm form) {
		ItemStatus itemStatus = null;
		if (form.getWorkStatus()) {
			itemStatus = new WorkStatus();
		}
		else {
			itemStatus = new HoldStatus();
		}
		itemStatus.setActive(form.getActive());
		itemStatus.setDescription(form.getDescription());
		itemStatus.setGroupLinks(new HashSet<>());
		updateStateGroupLinks(form, itemStatus);
		itemStatus.setNextActivities(new HashSet<>());
		updateNextActivities(form, itemStatus);
		itemStatus.setRetired(form.getRetired());
		itemStatus.setTranslations(createTranslations(form));
		
		itemStateService.insertItemState(itemStatus);
		return itemStatus.getStateid();
	}

	@Override
	public int createLookupInstance(EditLookupInstanceForm form) {
		LookupInstance sourceLookup = new LookupInstance();
		sourceLookup.setDescription(form.getDescription());
		sourceLookup.setLookupFunction(form.getLookupFunction());
		sourceLookup.setLookupResults(new HashSet<>());
		updateOutcomeStatuses(form, sourceLookup);
		lookupInstanceService.save(sourceLookup);
		return sourceLookup.getId();
	}

	@Override
	public void updateLookupInstance(EditLookupInstanceForm form, int lookupId) {
		LookupInstance sourceLookup = this.lookupInstanceService.get(lookupId);
		sourceLookup.setDescription(form.getDescription());
		sourceLookup.setLookupFunction(form.getLookupFunction());
		updateOutcomeStatuses(form, sourceLookup);
	}

	@Override
	public void updateItemStatus(EditItemStatusForm form, int stateId) {
		ItemStatus itemStatus = this.itemStateService.findItemStatus(stateId);
		itemStatus.setActive(form.getActive());
		itemStatus.setDescription(form.getDescription());
		updateStateGroupLinks(form, itemStatus);
		updateNextActivities(form, itemStatus);
		itemStatus.setRetired(form.getRetired());
		itemStatus.setTranslations(createTranslations(form));		
	}

	/**
	 * For new or existing HookActivity entry
	 */
	private void updateHookActivity(EditHookActivtyDto dto, HookActivity hookActivity) {
		hookActivity.setActivity(this.itemStateService.findItemActivity(dto.getFinishActivityId()));
		hookActivity.setBeginActivity(dto.getBeginActivity());
		hookActivity.setCompleteActivity(dto.getCompleteActivity());
		if (dto.getStatusOrActivity()) {				
			hookActivity.setState(this.itemStateService.findItemState(dto.getStartStatusId()));
		}
		else {
			hookActivity.setState(this.itemStateService.findItemState(dto.getStartActivityId()));
		}
	}
	
	/**
	 * For existing or new Hook entry, creates, updates, deletes (all via cascade/orphan removal)
	 */
	private void updateHookActivities(EditHookForm form, Hook hook) {
		for (EditHookActivtyDto dto : form.getHookActivityDtos()) {
			if (dto.getId() == 0) {
				// New activity
				HookActivity hookActivity = new HookActivity();
				hookActivity.setHook(hook);
				updateHookActivity(dto, hookActivity);
				logger.debug("Created hookActivity to activity id "+hookActivity.getActivity().getStateid());
				hook.getHookActivities().add(hookActivity);
			}
			else if (dto.getDelete()) {
				// Delete activity
				HookActivity hookActivity = this.hookActivityService.findHookActivity(dto.getId());
				logger.debug("Deleted ha id "+hookActivity.getId());
				hook.getHookActivities().remove(hookActivity);
			}
			else {
				// Updated activity
				HookActivity hookActivity = this.hookActivityService.findHookActivity(dto.getId());
				logger.debug("Updated ha id "+hookActivity.getId());
				updateHookActivity(dto, hookActivity);
			}
		}
	}
	

	
	/**
	 * For new or existing ItemActivity entry (creates, updates, deletes)
	 * Updated through cascade/orphan removal via ItemActivity::getActionOutcomes()
	 */
	private void updateActionOutcomes(EditItemActivityForm form, ItemActivity activity) {
		for (EditActionOutcomeDto dto : form.getActionOutcomeDtos()) {
			if (dto.getId() == 0) {
				// Creates specific concrete class of actionoutcome based on type, and sets value
				ActionOutcome ao = this.actionOutcomeService.createActionOutcome(dto.getType(), dto.getValue());
				ao.setActivity(activity);
				updateActionOutcomeFields(dto, ao);
				initializeDeprecatedActionOutcomeFields(ao);
				activity.getActionOutcomes().add(ao);
			}
			else if (dto.getDelete()) {
				// Delete action outcome (note, this may not work and throw referential constraints, if used)
				ActionOutcome ao = this.actionOutcomeService.get(dto.getId());
				activity.getActionOutcomes().remove(ao);
			}
			else {
				// Update action outcome (type/value not allowed to change, once created/used)
				ActionOutcome ao = this.actionOutcomeService.get(dto.getId());
				updateActionOutcomeFields(dto, ao);
			}
		}
	}
	
	/**
	 * @deprecated
	 * For use on new actionoutcomes only
	 * TODO once all lookup refactoring is done, remove this method
	 * together with description / translation fields 
	 */
	@Deprecated
	private void initializeDeprecatedActionOutcomeFields(ActionOutcome ao) {
		ao.setDescription(ao.getGenericValue().getDescription());
		ao.setTranslations(new HashSet<>());
		for (Locale locale : this.supportedLocaleService.getSupportedLocales()) {
			try {
				String message = messageSource.getMessage(ao.getGenericValue().getMessageCode(), null, locale);
				Translation t = new Translation(locale, message);
				ao.getTranslations().add(t);
			}
			catch (NoSuchMessageException e) {
				// No action required, this just indicates that a translation isn't defined for this code/locale
			}
		}
	}
	
	private void updateActionOutcomeFields(EditActionOutcomeDto dto, ActionOutcome ao) {
		ao.setActive(dto.getActive());
		ao.setDefaultOutcome(dto.getDefaultOutcome());
		ao.setPositiveOutcome(dto.getPositiveOutcome());
	}

	/**
	 * For new or existing LookupInstance; creates one or more outcome statuses (set must be initialised)
	 */
	private void updateOutcomeStatuses(EditLookupInstanceForm form, LookupInstance sourceLookup) {
		int index = 0; 
		for (EditOutcomeStatusDto dto : form.getOutcomeStatusDtos()) {
			if (dto.getId() == 0) {
				// Create outcome status (and its results)
				OutcomeStatus os = new OutcomeStatus();		  // Leaving Activity null
				updateOutcomeStatusConnection(dto, os);
				Set<LookupResult> results = createLookupResults(form, os, sourceLookup, index); 
				os.setLookupResults(results);
				sourceLookup.getLookupResults().addAll(results);
			}
			else if (dto.getDelete()) {
				// Delete outcome status (and any associated lookup results that are currently connected)
				OutcomeStatus os = this.outcomeStatusService.findOutcomeStatus(dto.getId());
				sourceLookup.getLookupResults().removeAll(os.getLookupResults());
				os.getLookupResults().clear();
				this.outcomeStatusService.deleteOutcomeStatus(os);
			}
			else {
				// Update outcome status (delete any associated lookup results currently connected, then recreate - easier.)
				OutcomeStatus os = this.outcomeStatusService.findOutcomeStatus(dto.getId());
				updateOutcomeStatusConnection(dto, os);
				sourceLookup.getLookupResults().removeAll(os.getLookupResults());	// Orphan removal
				os.getLookupResults().clear();
				Set<LookupResult> newResults = createLookupResults(form, os, sourceLookup, index); 
				os.getLookupResults().addAll(newResults);
				sourceLookup.getLookupResults().addAll(newResults);		// Cascade save
			}
			index++;
		}
	}
	
	private void updateOutcomeStatusConnection(EditOutcomeStatusDto dto, OutcomeStatus os) {
		if (dto.getLookupOrStatus()) {
			// Connecting to lookup
			LookupInstance lookupInstance = this.lookupInstanceService.get(dto.getLookupId());
			os.setLookup(lookupInstance);
			os.setStatus(null);
		}
		else {
			// Connecting to status
			ItemStatus status = this.itemStateService.findItemStatus(dto.getStatusId());
			os.setStatus(status);
			os.setLookup(null);
		}		
	}
	
	private Set<LookupResult> createLookupResults(EditLookupInstanceForm form, OutcomeStatus outcomeStatus, LookupInstance sourceLookup, int outcomeStatusIndex) {
		Set<LookupResult> results = new HashSet<>();
		for (int messageIndex = 0; messageIndex < sourceLookup.getLookupFunction().getLookupResultMessages().length; messageIndex++) {
			if (outcomeStatusIndex == form.getSelectedLookups().get(messageIndex)) {
				// This result message should be wired to the specified outcome status
				LookupResult lr = new LookupResult();
				lr.setOutcomeStatus(outcomeStatus);
				lr.setLookup(sourceLookup);
				lr.setResultMessageId(messageIndex);
				results.add(lr);
			}
		}
		return results;
	}

	/**
	 * For new or existing ItemActivity entry wired to ItemStatus; 
	 * ensures there is one single OutcomeStatus using selected statusId
	 * Remaining fields are null / false (no lookupresult needed)
	 */
	private void updateSingleOutcomeStatus(EditItemActivityForm form, ItemActivity activity) {
		if (activity.getOutcomeStatuses() == null) {
			activity.setOutcomeStatuses(new TreeSet<>(new OutcomeStatusComparator()));
		}
		OutcomeStatus outcomeStatus = null;
		if (activity.getOutcomeStatuses().isEmpty()) {
			outcomeStatus = new OutcomeStatus();
			outcomeStatus.setActivity(activity);
			activity.getOutcomeStatuses().add(outcomeStatus);	// Saved via cascade
		}
		else {
			// Keep the first outcome status, remove the rest
			outcomeStatus = activity.getOutcomeStatuses().first();
			Set<OutcomeStatus> removals = new HashSet<>();
			for (OutcomeStatus candidate : activity.getOutcomeStatuses()) {
				if (candidate.getId() != outcomeStatus.getId()) removals.add(candidate); 
			}
			activity.getOutcomeStatuses().removeAll(removals);
		}
		
		outcomeStatus.setLookup(null);
		if ((outcomeStatus.getStatus() == null) || (outcomeStatus.getStatus().getStateid() != form.getStatusId())) {
			outcomeStatus.setStatus(itemStateService.findItemStatus(form.getStatusId()));
		}
	}
	
	/**
	 * For new or existing ItemStatus or ItemActivity entity
	 */
	private void updateStateGroupLinks(AbstractItemStateForm form, ItemState itemState) {
		// Creates, Deletes, Updates 
		Map<Integer, StateGroupLink> map = new HashMap<>();
		itemState.getGroupLinks().stream().forEach(sgl -> map.put(sgl.getId(), sgl));
		for (EditStateGroupLinkDto dto : form.getStateGroupLinks()) {
			if (dto.getId() == 0) {
				StateGroupLink sgl = new StateGroupLink();
				sgl.setGroup(dto.getStateGroup());
				sgl.setState(itemState);
				sgl.setType(dto.getLinkType());
				itemState.getGroupLinks().add(sgl);
				logger.debug("Created sgl with group "+dto.getStateGroup()+" and type "+dto.getLinkType());
			}
			else if (dto.getDelete()) {
				StateGroupLink sgl = map.get(dto.getId());
				itemState.getGroupLinks().remove(sgl);		// Orphan removal
				logger.debug("Deleted sgl with id "+dto.getId());
			}
			else {
				StateGroupLink sgl = map.get(dto.getId());
				if (sgl.getGroup() != dto.getStateGroup()) {
					logger.debug("Updated sgl with id "+dto.getId()+" from group "+sgl.getGroup()+" to "+dto.getStateGroup());
					sgl.setGroup(dto.getStateGroup());
				}
				if (sgl.getType() != dto.getLinkType()) {
					logger.debug("Updated sgl with id "+dto.getId()+" from type "+sgl.getType()+" to "+dto.getLinkType());
					sgl.setType(dto.getLinkType());
				}
			}
		}
	}
	
	/**
	 * For new or existing ItemStatus entity
	 */
	private void updateNextActivities(EditItemStatusForm form, ItemStatus itemStatus) {
		// Creates, Deletes, Updates
		Map<Integer, NextActivity> map = new HashMap<>();
		itemStatus.getNextActivities().stream().forEach(na -> map.put(na.getId(), na));
		for (EditNextActivityDto dto : form.getNextActivities()) {
			if (dto.getId() == 0) {
				NextActivity nextActivity = new NextActivity();
				nextActivity.setActivity(this.itemStateService.findItemActivity(dto.getActivityId()));
				nextActivity.setManualEntryAllowed(dto.getManualEntryAllowed());
				nextActivity.setStatus(itemStatus);
				itemStatus.getNextActivities().add(nextActivity);
				logger.debug("Created nextActivity with activity id "+dto.getActivityId());
			}
			else if (dto.getDelete()) {
				NextActivity na = map.get(dto.getId());
				itemStatus.getNextActivities().remove(na);	// Orphan removal
				logger.debug("Deleted nextActivity with id "+dto.getId());
			}
			else {
				NextActivity nextActivity = map.get(dto.getId());
				if (nextActivity.getActivity().getStateid() != dto.getActivityId()) {
					logger.debug("Updated nextActivity with id "+dto.getId()+" from activity "+nextActivity.getActivity().getStateid()+" to "+dto.getActivityId());
					nextActivity.setActivity(this.itemStateService.findItemActivity(dto.getActivityId()));
				}
				if (nextActivity.isManualEntryAllowed() != dto.getManualEntryAllowed()) {
					logger.debug("Updated nextActivity with id "+dto.getId()+" from manualEntryAllowed "+nextActivity.isManualEntryAllowed()+" to "+dto.getManualEntryAllowed());
					nextActivity.setManualEntryAllowed(dto.getManualEntryAllowed());
				}
				
			}
		}
		
	}
	
	/**
	 * Used by both ItemStatus and ItemActivity
	 */
	private Set<Translation> createTranslations(AbstractItemStateForm form) {
		Set<Translation> result = new HashSet<>();
		for (Locale locale : form.getTranslations().keySet()) {
			result.add(new Translation(locale, form.getTranslations().get(locale)));
		}
		return result;
	}
}