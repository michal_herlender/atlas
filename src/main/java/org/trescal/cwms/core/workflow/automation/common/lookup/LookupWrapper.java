package org.trescal.cwms.core.workflow.automation.common.lookup;

import java.util.List;
import java.util.Map;

public class LookupWrapper
{
	private String beanName;
	private String description;
	private Map<String, String> outcomes;
	private List<LookupResultMessage> resultMessages;
	
	public String getBeanName()
	{
		return this.beanName;
	}

	public String getDescription()
	{
		return this.description;
	}
	
	public Map<String, String> getOutcomes()
	{
		return this.outcomes;
	}
	
	public List<LookupResultMessage> getResultMessages() {
		return resultMessages;
	}
	
	public void setBeanName(String beanName)
	{
		this.beanName = beanName;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setOutcomes(Map<String, String> outcomes)
	{
		this.outcomes = outcomes;
	}
	
	public void setResultMessages(List<LookupResultMessage> resultMessages) {
		this.resultMessages = resultMessages;
	}
}