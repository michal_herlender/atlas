package org.trescal.cwms.core.workflow.automation.common.hookinterceptor;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

public class HookInterceptorContext {
	private Hook hook;
	private JobItem jobItem;
	private boolean beginActivity;
	private boolean completeActivity;
	private ItemState currentState;
	private ItemActivity activity;
	private ItemActivity previousActivity;
	private HookActivity hookActivity;
	public HookInterceptorContext(Hook hook, JobItem jobItem) {
		this.hook = hook;
		this.jobItem = jobItem;
	}
	public boolean isBeginActivity() {
		return beginActivity;
	}
	public void setBeginActivity(boolean beginActivity) {
		this.beginActivity = beginActivity;
	}
	public boolean isCompleteActivity() {
		return completeActivity;
	}
	public void setCompleteActivity(boolean completeActivity) {
		this.completeActivity = completeActivity;
	}
	public ItemState getCurrentState() {
		return currentState;
	}
	public void setCurrentState(ItemState currentState) {
		this.currentState = currentState;
	}
	public ItemActivity getActivity() {
		return activity;
	}
	public void setActivity(ItemActivity activity) {
		this.activity = activity;
	}
	public ItemActivity getPreviousActivity() {
		return previousActivity;
	}
	public void setPreviousActivity(ItemActivity previousActivity) {
		this.previousActivity = previousActivity;
	}
	public HookActivity getHookActivity() {
		return hookActivity;
	}
	public void setHookActivity(HookActivity hookActivity) {
		this.hookActivity = hookActivity;
	}
	public JobItem getJobItem() {
		return jobItem;
	}
	public Hook getHook() {
		return hook;
	}
}
