package org.trescal.cwms.core.workflow.controller.edit;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.misc.entity.hook.db.HookService;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;
import org.trescal.cwms.core.misc.entity.hookactivity.db.HookActivityService;
import org.trescal.cwms.core.workflow.form.edit.EditHookActivtyDto;
import org.trescal.cwms.core.workflow.form.edit.EditHookForm;
import org.trescal.cwms.core.workflow.form.genericentityvalidator.EditHookValidator;
import org.trescal.cwms.core.workflow.service.EditWorkflowService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
public class EditHookController extends AbstractEditWorkflowController {
	@Autowired
	private HookActivityService hookActivityService;
	@Autowired
	private HookService hookService;
	@Autowired
	private EditWorkflowService editWorkflowService; 
	@Autowired
	private EditHookValidator validator;
	
	@InitBinder("form")
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute("form")
	public EditHookForm formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") int id) {
		EditHookForm form = new EditHookForm();
		if (id != 0) {
			// Editing existing hook
			Hook hook = this.hookService.findHook(id);
			form.setActive(hook.isActive());
			form.setAlwaysPossible(hook.getAlwaysPossible());
			form.setBeginActivity(hook.getBeginActivity());
			form.setCompleteActivity(hook.getCompleteActivity());
			form.setDefaultActivityId(hook.getActivity() != null ? hook.getActivity().getStateid() : 0);
			form.setName(hook.getName());		// TODO could be an enum
			form.setHookActivityDtos(getHookActivityDtos(hook));
		}
		else {
			// registering new hook (normally just done at hook creation... perhaps do by script once confirmed)
			form.setActive(true);
			form.setAlwaysPossible(true);
			form.setBeginActivity(true);
			form.setCompleteActivity(true);
			form.setDefaultActivityId(0);
			form.setHookActivityDtos(new ArrayList<>());
		}
		EditHookActivtyDto templateDto = new EditHookActivtyDto();
		templateDto.setId(0);
		templateDto.setBeginActivity(true);
		templateDto.setCompleteActivity(true);
		templateDto.setFinishActivityId(0);
		templateDto.setStartActivityId(0);
		templateDto.setStartStatusId(0);
		templateDto.setStatusOrActivity(true);
		
		form.setTemplateDto(templateDto);
		return form;
	}
	
	public ArrayList<EditHookActivtyDto> getHookActivityDtos(Hook hook) {
		ArrayList<EditHookActivtyDto> result = new ArrayList<EditHookActivtyDto>();
		for (HookActivity hookActivity : this.hookActivityService.findForHook(hook.getId())) {
			EditHookActivtyDto dto = new EditHookActivtyDto();
			dto.setId(hookActivity.getId());
			dto.setBeginActivity(hookActivity.isBeginActivity());
			dto.setCompleteActivity(hookActivity.isCompleteActivity());
			dto.setStatusOrActivity(hookActivity.getState().isStatus());
			if (hookActivity.getState().isStatus()) {
				dto.setStartStatusId(hookActivity.getState().getStateid());
				dto.setStartActivityId(0);
			}
			else {
				dto.setStartStatusId(0);
				dto.setStartActivityId(hookActivity.getState().getStateid());
			}
			dto.setFinishActivityId(hookActivity.getActivity().getStateid());
			result.add(dto);
		}
		return result;
	}
	
	private List<KeyValue<Boolean, String>> getSelectStatusActivity() {
		List<KeyValue<Boolean, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(Boolean.FALSE, "Activity"));
		result.add(new KeyValue<>(Boolean.TRUE, "Status"));
		return result;
	}
	
	@RequestMapping(value="edithook.htm", method=RequestMethod.GET)
	public String showView(Model model,
			@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		model.addAttribute("itemActivities", getItemActivities());
		model.addAttribute("itemStatuses", getItemStatuses());
		model.addAttribute("selectBoolean", getSelectBoolean());
		model.addAttribute("selectStatusActivity", getSelectStatusActivity());
		if (id != 0) {
			model.addAttribute("lastHook", this.hookService.findHook(id));
		}
		return "trescal/core/workflow/edit/edithook";
	}

	@RequestMapping(value="edithook.htm", method=RequestMethod.POST)
	public String onSubmit(Model model,
			@Validated @ModelAttribute("form") EditHookForm form,
			BindingResult bindingResult,
			@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		if (bindingResult.hasErrors()) {
			return showView(model, id);
		}
		Integer hookId = null;
		if (id == 0) {
			// Creating new record
			hookId = this.editWorkflowService.createHook(form);
		}
		else {
			// Editing existing record
			hookId = id;
			this.editWorkflowService.updateHook(form, hookId);
		}
		return "redirect:workfloweditor_hook.htm?hookId="+hookId;
	}
	
}
