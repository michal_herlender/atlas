package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.events.AutomaticFaultReportGenerationEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HookInterceptor_FailureReportClientResponse
	extends HookInterceptor<HookInterceptor_FailureReportClientResponse.Input> {

	public static final String HOOK_NAME = "failurereport-clientresponse";

	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	public static class Input {
		public FaultReport fr;
		public Date startDate;
		public Integer timeSpent;
		public Contact contact;

		public Input(FaultReport fr, Date startDate, Integer timeSpent, Contact contact) {
			this.fr = fr;
			this.startDate = startDate;
			this.timeSpent = timeSpent;
			this.contact = contact;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		JobItem ji = input.fr.getJobItem();

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: FAILURE REPORT CLIENT RESPONSE");

			Contact con;
			if (input.contact == null)
				con = this.sessionServ.getCurrentContact();
			else
				con = input.contact;

			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			List<Integer> possibleStartStatesIds = hook.getHookActivities().stream()
					.map(ha -> ha.getState().getStateid()).collect(Collectors.toList());

			if (possibleStartStatesIds.contains(ji.getState().getStateid())) {
				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				if (input.fr.getClientComments() == null)
					jia.setActivityDesc(context.getActivity().getDescription());
				else
					jia.setActivityDesc(input.fr.getClientComments());
				jia.setStartStatus((ItemStatus) ji.getState());
				if (input.startDate == null)
					jia.setStartStamp(new Date());
				else
					jia.setStartStamp(input.startDate);
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());

				if (context.isCompleteActivity()) {
					jia.setTimeSpent(input.timeSpent);
					jia.setEndStamp(DateUtils.addMilliseconds(jia.getStartStamp(), jia.getTimeSpent()));
					jia.setCompletedBy(con);
				}

				// generate document with client response
				boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
						ji.getJob().getCon().getSub().getSubdivid(), ji.getJob().getOrganisation().getComp().getCoid(),
						 new Date());
				if (isLinkedToAdveso) {
					AutomaticFaultReportGenerationEvent automaticGenerationEvent = new AutomaticFaultReportGenerationEvent(
							this, input.fr, true);
					applicationEventPublisher.publishEvent(automaticGenerationEvent);
				}

				ItemStatus endStatus = super.getNextStatus(context);
				ji.setState(endStatus);
				jobItemService.updateJobItem(ji);
				if (context.isCompleteActivity()) {
					jia.setEndStatus(endStatus);
				}

				super.persist(jia, endStatus, context);
			}
		}
	}

}