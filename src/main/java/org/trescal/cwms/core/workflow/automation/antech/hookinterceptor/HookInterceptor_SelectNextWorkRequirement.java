package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectNextWorkRequirement.Input;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_SelectNextWorkRequirement extends HookInterceptor<Input> {

	public static final String HOOK_NAME = "select-next-work-requirement";

	public static class Input {
		public JobItem ji;
		public String remarks;

		public Input(JobItem ji, String remarks) {
			super();
			this.ji = ji;
			this.remarks = remarks;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: select-next-work-requirement");
			Contact con = this.sessionServ.getCurrentContact();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, input.ji);
			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) input.ji.getState());
			jia.setStartStamp(new Date());
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(input.ji);
			jia.setComplete(context.isCompleteActivity());
			jia.setRemark(input.remarks);
			if (context.isCompleteActivity()) {
				jia.setEndStamp(new Date());
				jia.setTimeSpent(5);
				jia.setCompletedBy(con);
			}
			ItemStatus endStatus = super.getNextStatus(context);
			input.ji.setState(endStatus);
			jobItemService.updateJobItem(input.ji);
			if (context.isCompleteActivity())
				jia.setEndStatus(endStatus);
			this.persist(jia, endStatus, context);
		}
	}

}
