package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLinkComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TPCalibrationOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Component("Lookup_TPCalibrationOutcome")
public class Lookup_TPCalibrationOutcomeService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the outcome of the third party calibration (based on the most recent third party certificate).";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: TP CALIBRATION OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		logger.trace("Certificates: " + ji.getCertLinks().size());
		LookupResultMessage result = null;
		ActionOutcome ao = null;
		TreeSet<CertLink> certs = new TreeSet<>(new CertLinkComparator());
		certs.addAll(ji.getCertLinks());
		logger.trace("Certificates: " + certs.size());
		for (CertLink cl : certs.descendingSet()) {
			if (cl.getCert().getType().equals(CertificateType.CERT_THIRDPARTY)) {
				ao = cl.getCert().getThirdOutcome();
				break;
			}
		}
		if (ao != null) {
			String desc = ao.getDescription();
			desc = desc.toLowerCase();
			if(desc.contains("no further work necessary, no delivery required")) {
				result = Lookup_TPCalibrationOutcome.NO_FURTHER_WORK_NO_DELIVERY_REQUIRED;
			} else if (desc.contains("no further work")) {
				result = Lookup_TPCalibrationOutcome.NO_FURTHER_WORK;
			} else if (desc.contains("in-house calibration")) {
				result = Lookup_TPCalibrationOutcome.INHOUSE_CALIBRATION;
			} else if (desc.contains("in-house repair")) {
				result = Lookup_TPCalibrationOutcome.INHOUSE_REPAIR;
			} else if (desc.contains("adjustment")) {
				result = Lookup_TPCalibrationOutcome.ADJUSTMENT;
			} else if (desc.contains("third party work")) {
				result = Lookup_TPCalibrationOutcome.THIRD_PARTY_WORK;
			} else if (desc.contains("costing")) {
				result = Lookup_TPCalibrationOutcome.COSTING;
			} else if (desc.contains("failure report")){
				result = Lookup_TPCalibrationOutcome.FAILURE_REPORT;
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_TPCalibrationOutcome.values();
	}
}