package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TPItemReturned;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Component("Lookup_TPItemReturned")
public class Lookup_TPItemReturnedService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether an item has been rejected and sent back to third party for another repair missing out third party requirements and quotes etc";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: TP Item Returned");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		ActionOutcome ao = null;
		boolean lastActionInspection = false;
		// loop backwards through actions to get the MOST RECENT inspection
		// or third party requirements
		for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity jia = (JobItemActivity) action;

				// in some legacy cases, activity may be null!
				if (jia.getActivity() != null) {
					if (jia.getActivity().getDescription().contains("inspected by engineer (post third party")
							&& !jia.isDeleted()) {
						lastActionInspection = true;
						ao = jia.getOutcome();
						break;
					} else if (jia.getActivity().getDescription().contains("Third party requirements recorded")
							&& !jia.isDeleted()) {
						break;
					}
				}
			}
		}
		if (lastActionInspection) {
			if (ao != null) {
				String desc = ao.getDescription();
				if (desc.contains("repair un-successful")) {
					result = Lookup_TPItemReturned.RETURNED;
				} else {
					result = Lookup_TPItemReturned.NORMAL_ROUTE;
				}
			}
		} else {
			result = Lookup_TPItemReturned.NORMAL_ROUTE;
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_TPItemReturned.values();
	}
}