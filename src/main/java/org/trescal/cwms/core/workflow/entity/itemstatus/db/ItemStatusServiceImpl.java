package org.trescal.cwms.core.workflow.entity.itemstatus.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

public class ItemStatusServiceImpl extends BaseServiceImpl<ItemStatus, Integer> implements ItemStatusService {
	
	@Autowired
	private ItemStatusDao isDao;

	public List<ItemActivity> findActivities() {
		return this.isDao.findActivities();
	}

	public ItemStatus findItemStatus(Integer id) {
		return this.isDao.find(id);
	}

	public List<ItemStatus> findStatuses() {
		return this.isDao.findStatuses();
	}

	public List<ItemStatus> getAllItemStatuses() {
		return this.isDao.findAll();
	}

	public void insertItemStatus(ItemStatus i) {
		this.isDao.persist(i);
	}

	public List<ItemStatus> searchItemStatus(String description) {
		return this.isDao.searchItemStatus(description);
	}

	/**
	 * @param isDao
	 *            the isDao to set
	 */
	public void setIsDao(ItemStatusDao isDao) {
		this.isDao = isDao;
	}

	public void updateItemStatus(ItemStatus i) {
		this.isDao.update(i);
	}


	@Override
	protected BaseDao<ItemStatus, Integer> getBaseDao() {
		return this.isDao;
	}
}
