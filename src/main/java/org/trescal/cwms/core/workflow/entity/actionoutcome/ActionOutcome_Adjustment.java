package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Adjustment;

@Entity
@DiscriminatorValue("adjustment")
public class ActionOutcome_Adjustment extends ActionOutcome {
	private ActionOutcomeValue_Adjustment value;
	
	public ActionOutcome_Adjustment() {
		this.type = ActionOutcomeType.ADJUSTMENT;
	}
	
	@Transient
	@Override
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_Adjustment getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_Adjustment value) {
		this.value = value;
	}

}
