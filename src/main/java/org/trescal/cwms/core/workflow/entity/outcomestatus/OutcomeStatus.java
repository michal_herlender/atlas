package org.trescal.cwms.core.workflow.entity.outcomestatus;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;

@Entity
@Table(name = "outcomestatus")
public class OutcomeStatus
{
	private ItemActivity activity;
	@Deprecated
	private boolean defaultOutcome;				// See notes on getter
	private int id;
	private LookupInstance lookup;
	private Set<LookupResult> lookupResults;
	private ItemStatus status;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "activityid", nullable = true)
	public ItemActivity getActivity()
	{
		return this.activity;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lookupid", nullable = true)
	public LookupInstance getLookup()
	{
		return this.lookup;
	}

	// Intentionally non cascade or orphan removal - this is secondary, lookup results "owned" by LookupInstance.
	@OneToMany(mappedBy = "outcomeStatus", fetch = FetchType.LAZY)
	@OrderBy("id, resultmessage")
	public Set<LookupResult> getLookupResults()
	{
		return this.lookupResults;
	}

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = true)
	public ItemStatus getStatus()
	{
		return this.status;
	}

	/**
	 * It seems the defaultoutcome for outcomestatus is not used in database (old Antech entries all 0)
	 * Could be removed?  Not supporting in workflow editor for now.
	 * GB 2017-11-03
	 */
	@Deprecated
	@NotNull
	@Column(name = "defaultoutcome", nullable = false, columnDefinition="tinyint")
	public boolean isDefaultOutcome()
	{
		return this.defaultOutcome;
	}

	public void setActivity(ItemActivity activity)
	{
		this.activity = activity;
	}

	@Deprecated
	public void setDefaultOutcome(boolean defaultOutcome)
	{
		this.defaultOutcome = defaultOutcome;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLookup(LookupInstance lookup)
	{
		this.lookup = lookup;
	}

	public void setLookupResults(Set<LookupResult> lookupResults)
	{
		this.lookupResults = lookupResults;
	}

	public void setStatus(ItemStatus status)
	{
		this.status = status;
	}
}