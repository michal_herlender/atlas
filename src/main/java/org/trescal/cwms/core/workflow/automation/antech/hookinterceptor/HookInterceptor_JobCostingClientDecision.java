package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;


@Service
public class HookInterceptor_JobCostingClientDecision extends HookInterceptor<HookInterceptor_JobCostingClientDecision.Input> {
	public static final String HOOK_NAME = "job-costing-client-decision";

	public static class Input {

		public JobCosting jobCosting;
		public ActionOutcome ao;

		public Input(JobCosting jobCosting, ActionOutcome ao) {	
			this.jobCosting = jobCosting;
			this.ao = ao;
		}
	}

	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: JOB COSTING CLIENT RESPONSE");
			Set<JobCostingItem> items = input.jobCosting.getItems();
			Contact con = this.sessionServ.getCurrentContact();

			Map<JobItem, HookInterceptorContext> contexts = new HashMap<>();

			for (JobCostingItem jci : items) {
				JobItem ji = jci.getJobItem();
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);
				contexts.put(ji, context);
			}
			for (JobCostingItem jci : items) {
				JobItem ji = jci.getJobItem();
				HookInterceptorContext context = contexts.get(ji);

				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				jia.setStartStamp(new Date());
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());
				
				if (context.isCompleteActivity()) {
					jia.setEndStamp(new Date());
					jia.setTimeSpent(5);
					jia.setCompletedBy(con);
					jia.setOutcome(input.ao);
				}
				ji.getActions().add(jia);

				ItemStatus endStatus = super.getNextStatus(context);

				ji.setState(endStatus);
				jobItemService.updateJobItem(ji);
				if (context.isCompleteActivity()) {
					jia.setEndStatus(endStatus);
				}

				this.persist(jia, endStatus, context);
			}
		}
	}
}