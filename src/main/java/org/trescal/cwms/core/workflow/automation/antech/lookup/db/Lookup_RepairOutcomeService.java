package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_RepairOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Repair;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Component("Lookup_RepairOutcome")
public class Lookup_RepairOutcomeService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the outcome of the item's most recent repair.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: REPAIR OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		JobItemWorkRequirement jiwr = ji.getNextWorkReq();
		LookupResultMessage result = null;
		ActionOutcome ao = null;
		for (JobItemAction action : ji.getActions()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;

				for (StateGroupLink sgl : activity.getActivity().getGroupLinks()) {
					if (sgl.getGroup().equals(StateGroup.REPAIR) && !activity.isDeleted()) {
						ao = activity.getOutcome();
					}
				}
			}
		}
		
		if (ao != null) {

			ActionOutcomeValue_Repair aor = (ActionOutcomeValue_Repair) ao.getGenericValue();

			switch (aor) {
			case REPAIRED_NO_FURTHER_ACTION_REQUIRED:
				result = Lookup_RepairOutcome.NO_FURTHER_ACTION;
				break;
			case REPAIRED_FURTHER_CALIBRATION_NECESSARY:
				result = Lookup_RepairOutcome.CALIBRATION;
				break;
			case UNABLE_TO_REPAIR_REQUIRES_THIRD_PARTY_REPAIR:
				result = Lookup_RepairOutcome.THIRD_PARTY_REPAIR;
				break;
			case UNABLE_TO_REPAIR_BER:
				result = Lookup_RepairOutcome.UNIT_BER;
				break;
			case NOT_REPAIRED_NO_FURTHER_ACTION_REQUIRED:
				result = Lookup_RepairOutcome.NOT_REPAIRED_NO_FURTHER_ACTION;
				break;
			case NOT_REPAIRED_FURTHER_CALIBRATION_NECESSARY:
				result = Lookup_RepairOutcome.NOT_REPAIRED_FURTHER_CALIBRATION;
				break;
			default:
				// the other cases for spare parts are managed in
				// 'needspaireparts' lookup
				break;
			}
		}

		jiwr.getWorkRequirement().setStatus(WorkrequirementStatus.COMPLETE);

		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_RepairOutcome.values();
	}
}