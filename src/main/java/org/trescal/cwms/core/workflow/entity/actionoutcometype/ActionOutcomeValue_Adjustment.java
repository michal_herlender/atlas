package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_Adjustment implements ActionOutcomeValue {
	UNIT_ADJUSTED(
			"Unit adjusted",
			"actionoutcome.adjustment.adjusted"),
	UNABLE_TO_ADJUST_BER(
			"Unable to adjust - Unit B.E.R.",
			"actionoutcome.adjustment.uta_ber"),
	UNABLE_TO_ADJUST_REQUIRES_INHOUSE_REPAIR(
			"Unable to adjust - requires in-house repair",
			"actionoutcome.adjustment.uta_inhouserepair"),
	UNABLE_TO_ADJUST_REQUIRES_THIRD_PARTY_REPAIR(
			"Unable to adjust - requires third party repair",
			"actionoutcome.adjustment.uta_tprepair"),
	NOT_SUCCESSFUL(
			"Not successful",
			"actionoutcome.adjustment.notsuccessful");

	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_Adjustment(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.ADJUSTMENT;
	}
}
