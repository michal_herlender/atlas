package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_LastDeliveryType implements LookupResultMessage
{
	CLIENT("client"),
	THIRDPARTY("thirdparty");
	
	private String messageCode;
	
	private Lookup_LastDeliveryType(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}