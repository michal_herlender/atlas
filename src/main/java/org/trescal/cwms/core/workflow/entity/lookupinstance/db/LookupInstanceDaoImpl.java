package org.trescal.cwms.core.workflow.entity.lookupinstance.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance_;

@Repository("LookupInstanceDao")
public class LookupInstanceDaoImpl extends BaseDaoImpl<LookupInstance, Integer> implements LookupInstanceDao {
	
	@Override
	protected Class<LookupInstance> getEntity() {
		return LookupInstance.class;
	}

	@Override
	public List<LookupInstance> findLookupInstanceByName(String name) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<LookupInstance> cq = cb.createQuery(LookupInstance.class);
		Root<LookupInstance> root = cq.from(LookupInstance.class);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(root.get(LookupInstance_.description), name));

		cq.select(root);
		cq.where(clauses);
		cq.orderBy(cb.asc(root.get(LookupInstance_.id)));

		return getEntityManager().createQuery(cq).getResultList();
	}
}