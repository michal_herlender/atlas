package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_InitialWorkRequired implements LookupResultMessage
{
	INHOUSE_CALIBRATION("lookupresultmessage.contractreviewoutcome.inhousecalibration"),
	INHOUSE_REPAIR("lookupresultmessage.contractreviewoutcome.inhouserepair"),
	PRELIMINARY_INSPECTION("lookupresultmessage.contractreviewoutcome.inspection"),
	THIRD_PARTY_REPAIR("lookupresultmessage.contractreviewoutcome.thirdpartyrepair"),
	THIRD_PARTY_CALIBRATION("lookupresultmessage.contractreviewoutcome.thirdpartycalibration"),
	ADJUSTMENT("lookupresultmessage.contractreviewoutcome.adjustment"),
	ONSITE_CALIBRATION("lookupresultmessage.contractreviewoutcome.onsitecalibration"),
	NO_ACTION("lookupresultmessage.contractreviewoutcome.noaction"),;
	
	private String messageCode;
	
	private Lookup_InitialWorkRequired(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}