package org.trescal.cwms.core.workflow.entity.stategrouplink;

/**
 * Indicates, in which subdivisions the StateGroupLink applies
 * ALLOCATED_SUBDIV: in the subdivision, where the job was created
 * ALLOCATED_COMPANY: in all subdivisions of the business company, where the job was created
 * CURRENT_SUBDIV: in the subdivision, where the job item currently is
 * CURRENT_COMPANY: in all subdivisions of the business company, where the job
 * CALIBRATION_SUBDIV: in the subdivision, where the next calibration should be done
 * item currently is
 */
public enum StateGroupLinkType {
	
	ALLOCATED_SUBDIV,
	ALLOCATED_COMPANY,
	CURRENT_SUBDIV,
	CURRENT_COMPANY,
	ALLOCATED_NOTCURRENT_SUBDIV,
	CALIBRATION_SUBDIV;
}