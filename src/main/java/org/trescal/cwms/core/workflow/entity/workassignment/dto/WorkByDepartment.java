package org.trescal.cwms.core.workflow.entity.workassignment.dto;

import io.vavr.Tuple2;
import lombok.Value;
import org.trescal.cwms.core.procedure.dto.LaboratoryCategorySplit;

import java.util.Map;

@Value(staticConstructor = "of")
public class WorkByDepartment {

    Map<Tuple2<Integer, LaboratoryCategorySplit>, Map<WorkCountDay, Integer>> workByCategory;
    Map<WorkCountDay, Integer> total;
}
