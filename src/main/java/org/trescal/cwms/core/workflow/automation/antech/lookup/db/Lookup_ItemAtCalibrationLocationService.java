package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtCalibrationLocation;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemAtCalibrationLocation")
public class Lookup_ItemAtCalibrationLocationService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item is at the location where it is due to be calibrated.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM AT CALIBRATION LOCATION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = ji.getCurrentAddr() == null || ji.getCalAddr() == null
				|| ji.getCurrentAddr().getAddrid() == ji.getCalAddr().getAddrid() ? Lookup_ItemAtCalibrationLocation.YES
						: Lookup_ItemAtCalibrationLocation.NO;
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemAtCalibrationLocation.values();
	}
}