package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ContractReviewOutcome implements LookupResultMessage
{
	INHOUSE_REPAIR("lookupresultmessage.contractreviewoutcome.inhouserepair"),
	THIRD_PARTY_REPAIR("lookupresultmessage.contractreviewoutcome.thirdpartyrepair"),
	ADJUSTMENT("lookupresultmessage.contractreviewoutcome.adjustment"),
	INHOUSE_CALIBRATION("lookupresultmessage.contractreviewoutcome.inhousecalibration"),
	THIRD_PARTY_CALIBRATION("lookupresultmessage.contractreviewoutcome.thirdpartycalibration"),
	INSPECTION("lookupresultmessage.contractreviewoutcome.inspection"),
	AWAITING_CPO("lookupresultmessage.contractreviewoutcome.awaitingcpo"),
	AWAITING_FEEDBACK("lookupresultmessage.contractreviewoutcome.awaitingfeedback"),
	NO_ACTION("lookupresultmessage.contractreviewoutcome.noaction"),
	AWAITING_QUOTATION("lookupresultmessage.contractreviewoutcome.awaitingquotation"),
	ONSITE_CALIBRATION("lookupresultmessage.contractreviewoutcome.onsitecalibration"),
	REQUIRES_FAILURE_REPORT("lookupresultmessage.contractreviewoutcome.requriesfailurereport");
	
	private String messageCode;
	
	private Lookup_ContractReviewOutcome(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}