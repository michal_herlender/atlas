package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting")
public class Lookup_ItemHasBeenQuotedOrApprovedToBypassCostingService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether an item has a been quoted for or approved by customer services to bypass job costing.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM HAS BEEN QUOTED OR APPROVED TO BYPASS COSTING");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.NO;
		// check to see if this item has been quoted for
		if (ji.getCalibrationCost() != null) {
			if (ji.getCalibrationCost().getCostSrc() != null) {
				if (ji.getCalibrationCost().getCostSrc().equals(CostSource.QUOTATION)) {
					result = Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.YES;
				}
			}
		}
		// if this item isn't quoted, check to see if the item is in a group
		// with any other items that have been quoted...
		if (result == Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.NO && (ji.getGroup() != null)) {
			groupItemLoop: for (JobItem groupItem : ji.getGroup().getItems()) {
				if (groupItem.getJobItemId() != ji.getJobItemId()) {
					if (groupItem.getCalibrationCost() != null) {
						if (groupItem.getCalibrationCost().getCostSrc() != null) {
							if (groupItem.getCalibrationCost().getCostSrc().equals(CostSource.QUOTATION)) {
								result = Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.YES;
								break groupItemLoop;
							}
						}
					}
				}
			}
		}
		// if still no quote found, check to see if the item has been marked by
		// customer services as good to go without need for job costing
		if (ji.getApprovedToBypassCosting()) {
			result = Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.YES;
		}
		// if this item isn't approved to bypass costing, check to see if the
		// item is in a group with any other items that have been approved...
		if (result == Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.NO && (ji.getGroup() != null)) {
			groupItemLoop: for (JobItem groupItem : ji.getGroup().getItems()) {
				if (groupItem.getJobItemId() != ji.getJobItemId()) {
					if (groupItem.getApprovedToBypassCosting()) {
						result = Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.YES;
						break groupItemLoop;
					}
				}
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.values();
	}
}