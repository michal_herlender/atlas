package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemAtCalibrationLocation implements LookupResultMessage
{
	YES("lookupresultmessage.itematcalibrationlocation.yes"),
	NO("lookupresultmessage.itematcalibrationlocation.no");
	
	private String messageCode;
	
	private Lookup_ItemAtCalibrationLocation(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}