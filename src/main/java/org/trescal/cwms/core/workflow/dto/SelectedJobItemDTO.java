package org.trescal.cwms.core.workflow.dto;

import java.time.LocalDate;
import java.util.Date;

import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class SelectedJobItemDTO implements Comparable<SelectedJobItemDTO> {

	private Boolean check;
	private Integer jobItemId;
	private Integer jobItemNo;
	private Integer jobItemCount;
	private LocalDate dueDate;
	private Integer turn;
	private Integer jobId;
	private String jobNo;
	private String jobStatus;
	private LocalDate jobRegDate;
	private JobType jobType;
	private Integer clientContactId;
	private String clientContactFirstName;
	private String clientContactLastName;
	private Boolean clientContactActive;
	private Integer clientSubdivId;
	private String clientSubdivName;
	private Boolean clientSubdivActive;
	private Integer clientCompanyId;
	private String clientCompanyName;
	private Boolean clientCompanyActive;
	private Boolean clientCompanyOnStop;
	private Integer transportOutId;
	private String transportOutName;
	private String transportOutSubdiv;
	private Integer plantId;
	private String plantNo;
	private String serialNo;
	private Integer modelId;
	private String modelName;
	private Integer procedureId;
	private String procedureReference;
	private String procedureName;
	private String serviceTypeName;
	private String displayColour;
	private String displayColourFastTrack;
	private Integer contractReviewerId;
	private String contractReviewerFirstName;
	private String contractReviewerLastName;
	private Integer stateId;
	private String stateName;
	private String lastActionRemark;
	private Date lastActionEndStamp;
	private Integer actionId;
	private Integer allocatedSubdivId;
	private String allocatedSubdivCode;
	private InstrumentProjectionDTO instruDto;

	private LocalDate production_date;
	private String item_comment;
	private Integer tmc;
	private Integer barcode;
	private Integer allocatedForId;
	private String allocatedForName;
	private Integer allocationId;
	private Integer allocationMessageId;


	public SelectedJobItemDTO() {
	}

	public SelectedJobItemDTO(Integer jobitemId, Integer jobItemNo, Long jiCount, LocalDate dueDate, Integer turn,
							  Integer jobId, String jobNo, String jobStatus, LocalDate jobRegDate, JobType jobType, Integer clientContactId,
							  String clientContactFirstName, String clientContactLastName, Boolean clientContactActive,
							  Integer clientSubdivId, String clientSubdivName, Boolean clientSubdivActive, Integer clientCompanyId,
							  String clientCompanyName, Integer transportOutId, String transportOutName, String transportOutSubdiv,
							  Integer procedureId, String procedureReference, String procedureName, Integer plantId, String plantNo,
							  String serialNo, Integer modelId, String modelName, String serviceTypeName, String displayColour,
							  String displayColourFastTrack, Integer contractReviewerId, String contractReviewerFirstName,
							  String contractReviewerLastName, Integer stateId, String stateName, Date lastActionEndStamp,
							  String lastActionRemark, Integer actionId, Integer allocatedSubdivId, String allocatedSubdivCode) {
		this.jobItemId = jobitemId;
		this.jobItemNo = jobItemNo;
		this.jobItemCount = jiCount.intValue();
		this.dueDate = dueDate;
		this.turn = turn;
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.jobStatus = jobStatus;
		this.jobRegDate = jobRegDate;
		this.jobType = jobType;
		this.clientContactId = clientContactId;
		this.clientContactFirstName = clientContactFirstName;
		this.clientContactLastName = clientContactLastName;
		this.clientContactActive = clientContactActive;
		this.clientSubdivId = clientSubdivId;
		this.clientSubdivName = clientSubdivName;
		this.clientSubdivActive = clientSubdivActive;
		this.clientCompanyId = clientCompanyId;
		this.clientCompanyName = clientCompanyName;
		this.transportOutId = transportOutId;
		this.transportOutName = transportOutName;
		this.transportOutSubdiv = transportOutSubdiv;
		this.procedureId = procedureId;
		this.procedureReference = procedureReference;
		this.procedureName = procedureName;
		this.plantId = plantId;
		this.plantNo = plantNo;
		this.serialNo = serialNo;
		this.modelId = modelId;
		this.modelName = modelName;
		this.serviceTypeName = serviceTypeName;
		this.displayColour = displayColour;
		this.displayColourFastTrack = displayColourFastTrack;
		this.contractReviewerId = contractReviewerId;
		this.contractReviewerFirstName = contractReviewerFirstName;
		this.contractReviewerLastName = contractReviewerLastName;
		this.stateId = stateId;
		this.stateName = stateName;
		this.lastActionEndStamp = lastActionEndStamp;
		this.lastActionRemark = lastActionRemark;
		this.actionId = actionId;
		this.allocatedSubdivId = allocatedSubdivId;
		this.allocatedSubdivCode = allocatedSubdivCode;
	}

	public SelectedJobItemDTO(Integer jobitemId, Integer jobItemNo, Long jiCount, LocalDate dueDate, Integer turn,
			Integer jobId, String jobNo, String jobStatus, LocalDate jobRegDate, JobType jobType, Integer clientContactId,
			String clientContactFirstName, String clientContactLastName, Boolean clientContactActive,
			Integer clientSubdivId, String clientSubdivName, Boolean clientSubdivActive, Integer clientCompanyId,
			String clientCompanyName, Integer transportOutId, String transportOutName, String transportOutSubdiv,
			Integer procedureId, String procedureReference, String procedureName, Integer plantId, String plantNo,
			String serialNo, Integer modelId, String modelName, String serviceTypeName, String displayColour,
			String displayColourFastTrack, Integer contractReviewerId, String contractReviewerFirstName,
			String contractReviewerLastName, Integer stateId, String stateName, Date lastActionEndStamp,
			String lastActionRemark, Integer actionId, Integer allocatedSubdivId, String allocatedSubdivCode,Integer tmc) {
		this.jobItemId = jobitemId;
		this.jobItemNo = jobItemNo;
		this.jobItemCount = jiCount.intValue();
		this.dueDate = dueDate;
		this.turn = turn;
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.jobStatus = jobStatus;
		this.jobRegDate = jobRegDate;
		this.jobType = jobType;
		this.clientContactId = clientContactId;
		this.clientContactFirstName = clientContactFirstName;
		this.clientContactLastName = clientContactLastName;
		this.clientContactActive = clientContactActive;
		this.clientSubdivId = clientSubdivId;
		this.clientSubdivName = clientSubdivName;
		this.clientSubdivActive = clientSubdivActive;
		this.clientCompanyId = clientCompanyId;
		this.clientCompanyName = clientCompanyName;
		this.transportOutId = transportOutId;
		this.transportOutName = transportOutName;
		this.transportOutSubdiv = transportOutSubdiv;
		this.procedureId = procedureId;
		this.procedureReference = procedureReference;
		this.procedureName = procedureName;
		this.plantId = plantId;
		this.plantNo = plantNo;
		this.serialNo = serialNo;
		this.modelId = modelId;
		this.modelName = modelName;
		this.serviceTypeName = serviceTypeName;
		this.displayColour = displayColour;
		this.displayColourFastTrack = displayColourFastTrack;
		this.contractReviewerId = contractReviewerId;
		this.contractReviewerFirstName = contractReviewerFirstName;
		this.contractReviewerLastName = contractReviewerLastName;
		this.stateId = stateId;
		this.stateName = stateName;
		this.lastActionEndStamp = lastActionEndStamp;
		this.lastActionRemark = lastActionRemark;
		this.actionId = actionId;
		this.allocatedSubdivId = allocatedSubdivId;
		this.allocatedSubdivCode = allocatedSubdivCode;
		this.tmc = tmc;
	}

	public SelectedJobItemDTO(Integer jobitemId, Integer jobItemNo, Long jiCount, LocalDate dueDate, Integer turn,
							  Integer jobId, String jobNo, String jobStatus, LocalDate jobRegDate, JobType jobType, Integer clientContactId,
							  String clientContactFirstName, String clientContactLastName, Boolean clientContactActive,
							  Integer clientSubdivId, String clientSubdivName, Boolean clientSubdivActive, Integer clientCompanyId,
							  String clientCompanyName, Integer transportOutId, String transportOutName, String transportOutSubdiv,
							  Integer procedureId, String procedureReference, String procedureName, Integer plantId, String plantNo,
							  String serialNo, Integer modelId, String modelName, String serviceTypeName, String displayColour,
							  String displayColourFastTrack, Integer contractReviewerId, String contractReviewerFirstName,
							  String contractReviewerLastName, Integer stateId, String stateName, Date lastActionEndStamp,
							  String lastActionRemark, Integer actionId, Integer allocatedSubdivId, String allocatedSubdivCode,Integer tmc,
							  LocalDate production_date,String item_comment,Integer allocatedForId,String allocatedFistName,String allocatedForLastName,
							  Integer allocationId, Integer barcode,Integer allocationMessageId) {
		this.jobItemId = jobitemId;
		this.jobItemNo = jobItemNo;
		this.jobItemCount = jiCount.intValue();
		this.dueDate = dueDate;
		this.turn = turn;
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.jobStatus = jobStatus;
		this.jobRegDate = jobRegDate;
		this.jobType = jobType;
		this.clientContactId = clientContactId;
		this.clientContactFirstName = clientContactFirstName;
		this.clientContactLastName = clientContactLastName;
		this.clientContactActive = clientContactActive;
		this.clientSubdivId = clientSubdivId;
		this.clientSubdivName = clientSubdivName;
		this.clientSubdivActive = clientSubdivActive;
		this.clientCompanyId = clientCompanyId;
		this.clientCompanyName = clientCompanyName;
		this.transportOutId = transportOutId;
		this.transportOutName = transportOutName;
		this.transportOutSubdiv = transportOutSubdiv;
		this.procedureId = procedureId;
		this.procedureReference = procedureReference;
		this.procedureName = procedureName;
		this.plantId = plantId;
		this.plantNo = plantNo;
		this.serialNo = serialNo;
		this.modelId = modelId;
		this.modelName = modelName;
		this.serviceTypeName = serviceTypeName;
		this.displayColour = displayColour;
		this.displayColourFastTrack = displayColourFastTrack;
		this.contractReviewerId = contractReviewerId;
		this.contractReviewerFirstName = contractReviewerFirstName;
		this.contractReviewerLastName = contractReviewerLastName;
		this.stateId = stateId;
		this.stateName = stateName;
		this.lastActionEndStamp = lastActionEndStamp;
		this.lastActionRemark = lastActionRemark;
		this.actionId = actionId;
		this.allocatedSubdivId = allocatedSubdivId;
		this.allocatedSubdivCode = allocatedSubdivCode;
		this.tmc = tmc;
		this.production_date = production_date;
		this.item_comment = item_comment;
		this.allocatedForId = allocatedForId;
		this.allocatedForName = allocatedFistName +" "+allocatedForLastName;
		this.allocationId = allocationId;
		this.barcode = barcode;
		this.allocationMessageId = allocationMessageId;
	}

	@Override
	public int compareTo(SelectedJobItemDTO other) {
		Integer jobNoDiff = jobNo.compareTo(other.jobNo);
		return jobNoDiff == 0 ? jobItemNo - other.jobItemNo : jobNoDiff;
	}
	
}