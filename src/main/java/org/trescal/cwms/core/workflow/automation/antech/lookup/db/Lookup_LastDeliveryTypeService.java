package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.thirdpartydelivery.ThirdPartyDelivery;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_LastDeliveryType;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_LastDeliveryType")
public class Lookup_LastDeliveryTypeService extends Lookup {

	@Autowired
	private DeliveryService deliveryService;
	
	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Last jobitem delivery type");
		List<ClientDelivery> clientDeliveries = (List<ClientDelivery>) this.deliveryService.findByJobItem(jobItemId, DeliveryType.CLIENT);
		List<ThirdPartyDelivery> tpDeliveries = (List<ThirdPartyDelivery>) this.deliveryService.findByJobItem(jobItemId, DeliveryType.THIRDPARTY);
		ClientDelivery clientDelivery = clientDeliveries != null && !clientDeliveries.isEmpty() ? clientDeliveries.get(0):null;
		ThirdPartyDelivery tpDelivery = tpDeliveries != null && !tpDeliveries.isEmpty() ? tpDeliveries.get(0):null;
		if(clientDelivery != null && (tpDelivery == null || (tpDelivery != null && clientDelivery.getDeliverydate().compareTo(tpDelivery.getDeliverydate()) > 0)))
			return Lookup_LastDeliveryType.CLIENT;
		else 
			return Lookup_LastDeliveryType.THIRDPARTY;
	}

	@Override
	public String getDescription() {
		return "Determines the last jobitem delivery type.";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_LastDeliveryType.values();
	}
}
