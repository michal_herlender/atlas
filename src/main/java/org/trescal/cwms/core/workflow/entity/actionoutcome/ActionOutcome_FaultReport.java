package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_FaultReport;

@Entity
@DiscriminatorValue("fault_report")
@Deprecated
public class ActionOutcome_FaultReport extends ActionOutcome {
	private ActionOutcomeValue_FaultReport value;
	
	public ActionOutcome_FaultReport() {
		this.type = ActionOutcomeType.FAULT_REPORT;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_FaultReport getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_FaultReport value) {
		this.value = value;
	}

}
