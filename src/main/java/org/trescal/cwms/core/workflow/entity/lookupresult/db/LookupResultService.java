package org.trescal.cwms.core.workflow.entity.lookupresult.db;

import java.util.List;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

public interface LookupResultService
{
	void deleteLookupResult(LookupResult lookupresult);

	LookupResult findLookupResult(int id);

	LookupResult findLookupResultForInstanceAndMessage(Integer lookupInstanceId, LookupResultMessage result);
	//	LookupResult findLookupResultFromActivityAndResult(Integer activityId, Integer lookupInstanceId, LookupResultMessage result);

	List<LookupResult> getAllLookupResults();

	List<LookupResult> getAllLookupResultsForActivity(int activityId);

	/**
	 * Returns the {@link OutcomeStatus} for the {@link ItemActivity} with the
	 * given ID, and with a lookup result matching the given String.
	 * 
	 * @param activityId the {@link ItemActivity} ID.
	 * @param result the lookup result to match.
	 * @return the {@link OutcomeStatus}
	 */
	OutcomeStatus getResult(Integer lookupInstanceId, LookupResultMessage result);

	void insertLookupResult(LookupResult lookupresult);
}