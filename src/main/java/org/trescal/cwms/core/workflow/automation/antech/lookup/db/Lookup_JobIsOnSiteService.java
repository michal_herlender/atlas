package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_JobIsOnSite;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_JobIsOnSite")
public class Lookup_JobIsOnSiteService extends Lookup {

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Job Is On Site");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		if (ji.getJob().getType().equals(JobType.SITE)) {
			result = Lookup_JobIsOnSite.YES;
		} else {
			result = Lookup_JobIsOnSite.NO;
		}
		return result;
	}

	@Override
	public String getDescription() {
		return "Determines whether the job is an onsite job or not";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_JobIsOnSite.values();
	}
}