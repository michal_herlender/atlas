package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_DelayCertificate extends HookInterceptor<Certificate> {
	
	public static final String HOOK_NAME = "delay-certificate";
	
	@Autowired
	private JobItemService jobItemService;
	
	@Transactional
	@Override
	public void recordAction(Certificate cert) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: DELAY CERTIFICATE");
			Contact con = this.sessionServ.getCurrentContact();
			for (CertLink cl : cert.getLinks()) {
				JobItem ji = cl.getJobItem();
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);
				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				jia.setStartStamp(new Date());
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());
				if (context.isCompleteActivity()) {
					jia.setEndStamp(new Date());
					jia.setTimeSpent(5);
					jia.setCompletedBy(con);
				}
				ItemStatus endStatus = super.getNextStatus(context);
				ji.setState(endStatus);
				jobItemService.updateJobItem(ji);
				if (context.isCompleteActivity()) jia.setEndStatus(endStatus);
				this.persist(jia, endStatus, context);
			}
		}
	}
}