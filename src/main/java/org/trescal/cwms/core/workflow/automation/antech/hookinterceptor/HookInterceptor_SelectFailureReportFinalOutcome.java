package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;

@Service
public class HookInterceptor_SelectFailureReportFinalOutcome
	extends HookInterceptor<HookInterceptor_SelectFailureReportFinalOutcome.Input> {

	public static final String HOOK_NAME = "select-failure-report-outcome";

	public static class Input {

		public JobItem ji;
		public String remark;
		public Date startDate;
		public Integer time;
		public Contact contact;

		public Input(JobItem jobItem, String remark, Date startDate, Integer time, Contact contact) {
			this.ji = jobItem;
			this.remark = remark;
			this.startDate = startDate;
			this.time = time;
			this.contact = contact;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: select-fr-final-outcome");

			Contact con;
			if (input.contact == null)
				con = this.sessionServ.getCurrentContact();
			else
				con = input.contact;
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, input.ji);
			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) input.ji.getState());
			if (input.startDate == null)
				jia.setStartStamp(new Date());
			else
				jia.setStartStamp(input.startDate);
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(input.ji);
			jia.setComplete(context.isCompleteActivity());
			jia.setRemark(input.remark);

			if (context.isCompleteActivity()) {
				if (input.time == null)
					jia.setTimeSpent(0);
				else
					jia.setTimeSpent(input.time);
				jia.setEndStamp(DateUtils.addMinutes(jia.getStartStamp(), jia.getTimeSpent()));
				jia.setCompletedBy(con);
			}

			input.ji.getActions().add(jia);

			ItemStatus endStatus = super.getNextStatus(context);
			input.ji.setState(endStatus);
			jobItemService.updateJobItem(input.ji);
			if (context.isCompleteActivity())
				jia.setEndStatus(endStatus);
			this.persist(jia, endStatus, context);
		}
	}
}