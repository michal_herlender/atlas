package org.trescal.cwms.core.workflow.form.edit;

import javax.validation.constraints.NotNull;

/*
 * This is really a combined editing DTO for OutcomeStatus / LookupResult
 * intended for editing OutcomeStatuses attached to an activity.
 */
public class EditOutcomeStatusDto {
	private Integer id;					// OutcomeStatus id (if it already exists, may be new)
	private Boolean lookupOrStatus;		// Whether outcomestatus is connected to a lookup or status
	private Integer statusId; 			// Chosen status connected to outcomestatus
	private Integer lookupId;			// Chosen lookup connected to outcomestatus
	private Boolean delete;
	
	@NotNull
	public Integer getId() {
		return id;
	}
	@NotNull
	public Boolean getLookupOrStatus() {
		return lookupOrStatus;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public Integer getLookupId() {
		return lookupId;
	}
	@NotNull
	public Boolean getDelete() {
		return delete;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public void setLookupId(Integer lookupId) {
		this.lookupId = lookupId;
	}
	public void setLookupOrStatus(Boolean lookupOrStatus) {
		this.lookupOrStatus = lookupOrStatus;
	}
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}
}
