package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_EngineerInspection implements ActionOutcomeValue {
	REQUIRES_IN_HOUSE_CALIBRATION("Unit requires in-house calibration","actionoutcome.engineerinspection.inhousecal"),
	REQUIRES_THIRD_PARTY_CALIBRATION("Unit requires third party calibration","actionoutcome.engineerinspection.tpcal"),
	REQUIRES_THIRD_PARTY_REPAIR("Unit requires third party repair","actionoutcome.engineerinspection.tprepair"),
	REQUIRES_INHOUSE_REPAIR("Unit requires in-house repair","actionoutcome.engineerinspection.inhouserepair"),
	REQUIRES_ADJUSTMENT("Unit requires adjustment","actionoutcome.engineerinspection.adjustment"),
	READY_FOR_CALIBRATION("Inspected with no problems, ready for calibration","actionoutcome.engineerinspection.readyforcal"),
	IS_BER("Unit is B.E.R.","actionoutcome.engineerinspection.ber"),
	NO_ACTION_REQUIRED("No action required","actionoutcome.contractreview.noaction");
	
	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_EngineerInspection(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.ENGINEER_INSPECTION;
	}

}
