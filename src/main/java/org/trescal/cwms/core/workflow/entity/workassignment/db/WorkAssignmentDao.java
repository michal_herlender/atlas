package org.trescal.cwms.core.workflow.entity.workassignment.db;

import io.vavr.Tuple2;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.workflow.dto.LabCounts;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.workassignment.WorkAssignment;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface WorkAssignmentDao extends BaseDao<WorkAssignment, Integer> {
    List<WorkAssignment> getForDepartmentType(DepartmentType departmentType);

    Map<Tuple2<Integer, Boolean>, LabCounts> getLabWorkCounts();

    List<Integer> getCapabilitiesIdsAssignedToCategory(CapabilityCategory pc);

    List<Integer> getCapabilitiesIdsAssignedToDeptartment(Department dept);

    List<StateGroup> getStateGroupsAssignedToDeptType(DepartmentType deptType);

    List<StateGroup> getStateGroupsForDepartmentTypes(Collection<DepartmentType> types);

}