package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_PostTPFurtherWork;

@Entity
@DiscriminatorValue("post_tp_further_work")
public class ActionOutcome_PostTPFurtherWork extends ActionOutcome {
	private ActionOutcomeValue_PostTPFurtherWork value;
	
	public ActionOutcome_PostTPFurtherWork() {
		this.type = ActionOutcomeType.POST_TP_FURTHER_WORK;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_PostTPFurtherWork getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_PostTPFurtherWork value) {
		this.value = value;
	}

}
