package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_Repair implements ActionOutcomeValue {
	REPAIRED_NO_FURTHER_ACTION_REQUIRED(
			"Unit repaired - no further action required",
			"actionoutcome.repair.repaired_done"),
	REPAIRED_FURTHER_CALIBRATION_NECESSARY(
			"Unit repaired - further calibration necessary",
			"actionoutcome.repair.repaired_furthercal"),
	UNABLE_TO_REPAIR_REQUIRES_THIRD_PARTY_REPAIR(
			"Unable to repair - requires third party repair",
			"actionoutcome.repair.unable_tprepair"),
	UNABLE_TO_REPAIR_BER(
			"Unable to repair - Unit B.E.R.",
			"actionoutcome.repair.unable_ber"),
	SPARES_REQUIRED(
			"Spares required",
			"actionoutcome.repair.spares_required"),
	REPLACEMENT_COMPONENTS_REQUIRED(
			"Replacement components required",
			"actionoutcome.repair.components_required"),
	NOT_REPAIRED_NO_FURTHER_ACTION_REQUIRED(
			"Unit not repaired - no further action required",
			"actionoutcome.repair.notrepaired_done"),
	NOT_REPAIRED_FURTHER_CALIBRATION_NECESSARY(
			"Unit not repaired - further calibration necessary",
			"actionoutcome.repair.notrepaired_furthercal");
	
	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_Repair(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.REPAIR;
	}
}
