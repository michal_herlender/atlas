package org.trescal.cwms.core.workflow.entity.workassignment.db;

import io.vavr.Tuple2;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.workflow.dto.LabCounts;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.workassignment.WorkAssignment;
import org.trescal.cwms.core.workflow.entity.workassignment.dto.WorkAssigmentCounters;

import java.util.List;
import java.util.Map;

public interface WorkAssignmentService {
    /**
     * Deletes the given {@link WorkAssignment} from the database.
     *
     * @param workassignment the {@link WorkAssignment} to delete.
     */
    void deleteWorkAssignment(WorkAssignment workassignment);

    /**
     * Returns the {@link WorkAssignment} with the given ID
     *
     * @param id the {@link WorkAssignment} ID.
     * @return the {@link WorkAssignment}
     */
    WorkAssignment findWorkAssignment(int id);

    /**
	 * Returns a {@link List} of all {@link WorkAssignment}s in the database.
	 * 
	 * @return the {@link List} of {@link WorkAssignment}s.
	 */
	List<WorkAssignment> getAllWorkAssignments();
	
	List<WorkAssignment> getForDepartmentType(DepartmentType departmentType);

    Map<Tuple2<Integer, Boolean>, LabCounts> getLabWorkCounts();

    List<Integer> getCapabilitiesIdsAssignedToCategory(CapabilityCategory pc);

    List<Integer> getCapabilitiesIdsAssignedToDepartment(Department dept);

    /**
     * Returns a {@link List} of {@link StateGroup}s assigned to the given
     * {@link DepartmentType}
     *
     * @param deptType the {@link DepartmentType}
     * @return the {@link List} of {@link StateGroup}s
     */
    List<StateGroup> getStateGroupsAssignedToDeptType(DepartmentType deptType);

    Map<Integer, WorkAssigmentCounters> getWorksForDepartmentTypes(Subdiv subdiv);

    /**
	 * Inserts the given {@link WorkAssignment} into the database.
	 * 
	 * @param workassignment the {@link WorkAssignment} to insert.
	 */
	void insertWorkAssignment(WorkAssignment workassignment);

    List<StateGroup> getLabStateGroups();
}