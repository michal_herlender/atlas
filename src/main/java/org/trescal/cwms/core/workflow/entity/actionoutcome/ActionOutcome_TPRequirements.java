package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_TPRequirements;

@Entity
@DiscriminatorValue("tp_requirements")
public class ActionOutcome_TPRequirements extends ActionOutcome {
	private ActionOutcomeValue_TPRequirements value;
	
	public ActionOutcome_TPRequirements() {
		this.type = ActionOutcomeType.TP_REQUIREMENTS;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_TPRequirements getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_TPRequirements value) {
		this.value = value;
	}

}
