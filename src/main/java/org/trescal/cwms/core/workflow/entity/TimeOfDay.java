package org.trescal.cwms.core.workflow.entity;

/**
 * @author JamieV
 */
public enum TimeOfDay
{
	AM, PM;
}