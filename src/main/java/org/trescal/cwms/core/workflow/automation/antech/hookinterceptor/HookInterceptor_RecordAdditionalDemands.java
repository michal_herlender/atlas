package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_RecordAdditionalDemands extends HookInterceptor<JobItem> {

	public static final String HOOK_NAME = "record-additional-demands";

	@Transactional
	@Override
	public void recordAction(JobItem jobItem) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: RECORD ADDITIONAL DEMANDS");
			Contact contact = this.sessionServ.getCurrentContact();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, jobItem);
			JobItemActivity activity = new JobItemActivity();
			activity.setActivity(context.getActivity());
			activity.setActivityDesc(context.getActivity().getDescription());
			activity.setStartStatus((ItemStatus) jobItem.getState());
			activity.setStartStamp(new Date());
			activity.setStartedBy(contact);
			activity.setCreatedOn(new Date());
			activity.setJobItem(jobItem);
			activity.setComplete(context.isCompleteActivity());
			activity.setRemark("");
			if (context.isCompleteActivity()) {
				activity.setEndStamp(new Date());
				activity.setTimeSpent(5);
				activity.setCompletedBy(contact);
			}
			ItemStatus endStatus = super.getNextStatus(context);
			jobItem.setState(endStatus);
			jobItemService.mergeJobItem(jobItem);
			if (context.isCompleteActivity())
				activity.setEndStatus(endStatus);
			super.persist(activity, endStatus, context);
		}
	}
}