package org.trescal.cwms.core.workflow.automation.common.lookup;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.db.SystemDefaultApplicationService;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;

public abstract class Lookup {

	@Autowired
	protected ItemStateService itemStateService;
	@Autowired
	protected JobItemService jobItemService;
	@Autowired
	protected SystemDefaultApplicationService sysDefAppServ;

	protected static final Logger logger = LoggerFactory.getLogger(Lookup.class);

	public abstract LookupResultMessage lookupStatus(int jobItemId);

	// Returns a one line description of what the lookup does, moved from XML meta
	// data
	public abstract String getDescription();

	protected abstract LookupResultMessage[] getLookupResultMessagesValues();

	public List<LookupResultMessage> getLookupResultMessages() {
		List<LookupResultMessage> result = new ArrayList<LookupResultMessage>();
		for (LookupResultMessage lrm : this.getLookupResultMessagesValues())
			result.add(lrm);
		return result;
	}
}