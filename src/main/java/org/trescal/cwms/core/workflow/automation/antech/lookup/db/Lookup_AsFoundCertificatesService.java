package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AsFoundCertificates;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_AsFoundCertificates;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_AsFoundCertificates")
public class Lookup_AsFoundCertificatesService extends Lookup {

	@Autowired
	private AsFoundCertificates asFoundCertificates;

	@Override
	public String getDescription() {
		return "Determines whether the client has a preference to issue 'as found' certificates.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: AS FOUND CERTIFICATES");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = asFoundCertificates.parseValue(asFoundCertificates
				.getValueHierarchical(Scope.CONTACT, ji.getJob().getCon(), ji.getJob().getOrganisation().getComp())
				.getValue()) ? Lookup_AsFoundCertificates.RECORDED_PREFERENCE
						: Lookup_AsFoundCertificates.NO_RECORDED_PREFERENCE;
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_AsFoundCertificates.values();
	}
}