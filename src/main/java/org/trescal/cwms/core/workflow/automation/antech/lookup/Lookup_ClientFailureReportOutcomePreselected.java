package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ClientFailureReportOutcomePreselected implements LookupResultMessage {

	YES("yes"), NO("no");

	private String messageCode;

	private Lookup_ClientFailureReportOutcomePreselected(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
}
