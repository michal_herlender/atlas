package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_RepairInspectionReportCompleted implements LookupResultMessage {

	NO("no"),
	YES("yes");

	private String messageCode;

	private Lookup_RepairInspectionReportCompleted(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}

}
