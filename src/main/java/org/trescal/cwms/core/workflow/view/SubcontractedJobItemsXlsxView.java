package org.trescal.cwms.core.workflow.view;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAtTP;
import org.trescal.cwms.core.tools.reports.dto.ItemsAtTPWrapper;

@Component
public class SubcontractedJobItemsXlsxView extends AbstractXlsxStreamingView{

	@Autowired
	private LocaleResolver localeResolver;
	@Autowired
	private MessageSource messageSource;
	
	public static final Logger logger = LoggerFactory.getLogger(SubcontractedJobItemsXlsxView.class);
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		long startTime = System.currentTimeMillis();
		Locale userLocale = localeResolver.resolveLocale(request);
		List<ItemsAtTPWrapper> itemsAtTpWrapper = (List<ItemsAtTPWrapper>) model.get("itemsAtTPWrapper");
		List<ItemAtTP> itemsAtTp = (List<ItemAtTP>) model.get("itemAtTP");
		
		// create a new Excel sheet
		Sheet sheet1 = workbook.createSheet(messageSource.getMessage("", null, "Summary", userLocale));
		sheet1.setDefaultColumnWidth(27);
		
		
		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);

		// create style for header cells
		CellStyle styleHeader = workbook.createCellStyle();
		Font fontheader = workbook.createFont();
		fontheader.setFontName("Arial");
		styleHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
		fontheader.setBoldweight(Font.BOLDWEIGHT_BOLD);
		fontheader.setColor(HSSFColor.WHITE.index);
		styleHeader.setFont(fontheader);

		workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);

		// create style for date cells
		DataFormat dateFormat = workbook.createDataFormat();
		CellStyle dateStyle = workbook.createCellStyle();
		dateStyle.setDataFormat(dateFormat.getFormat("d/m/yyyy"));
		
		DataFormat dateFormat2 = workbook.createDataFormat();
		CellStyle dateStyle2 = workbook.createCellStyle();
		dateStyle2.setDataFormat(dateFormat2.getFormat("d/m/yyyy hh:mm:ss"));

		int cellHeader = 0;
		int rowIndex = 0;
		// create header row
		
		Row row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue("Subcontracted Job Items  :");
		row.getCell(cellHeader).setCellStyle(styleHeader);
		Row header1 = sheet1.createRow(rowIndex++);
		
		header1.createCell(0).setCellValue(messageSource.getMessage("Item No", null, "Items", userLocale));
		header1.getCell(0).setCellStyle(style);
		
		header1.createCell(1).setCellValue(messageSource.getMessage("", null, "Company Role", userLocale));
		header1.getCell(1).setCellStyle(style);
		
		header1.createCell(2).setCellValue(messageSource.getMessage("", null, "Country", userLocale));
		header1.getCell(2).setCellStyle(style);
		
		header1.createCell(3).setCellValue(messageSource.getMessage("", null, "Company", userLocale));
		header1.getCell(3).setCellStyle(style);
		
		header1.createCell(4).setCellValue(messageSource.getMessage("", null, "Subdivision", userLocale));
		header1.getCell(4).setCellStyle(style);
		
		for (ItemsAtTPWrapper item : itemsAtTpWrapper ) {
			Row row1 = sheet1.createRow(rowIndex++);
			row1.createCell(0).setCellValue(item.getItemCount());
			row1.createCell(1).setCellValue(item.getCompanyRole());
			row1.createCell(2).setCellValue(item.getCountry());
			row1.createCell(3).setCellValue(item.getCompName());
			row1.createCell(4).setCellValue(item.getSubdivName());
		}

		Sheet sheet2 = workbook.createSheet(messageSource.getMessage("", null, "Details", userLocale));
		sheet2.setDefaultColumnWidth(27);
		
		Row header2 = sheet2.createRow(0);
		
		header2.createCell(0).setCellValue(messageSource.getMessage("", null, "Company Role", userLocale));
		header2.getCell(0).setCellStyle(style);
		
		header2.createCell(1).setCellValue(messageSource.getMessage("", null, "Country", userLocale));
		header2.getCell(1).setCellStyle(style);
		
		header2.createCell(2).setCellValue(messageSource.getMessage("", null, "Company", userLocale));
		header2.getCell(2).setCellStyle(style);
		
		header2.createCell(3).setCellValue(messageSource.getMessage("", null, "Subdivision", userLocale));
		header2.getCell(3).setCellStyle(style);
		
		header2.createCell(4).setCellValue(messageSource.getMessage("", null, "Job Number", userLocale));
		header2.getCell(4).setCellStyle(style);
		
		header2.createCell(5).setCellValue(messageSource.getMessage("", null, "Item Number", userLocale));
		header2.getCell(5).setCellStyle(style);

		header2.createCell(6).setCellValue(messageSource.getMessage("", null, "Date In", userLocale));
		header2.getCell(6).setCellStyle(style);

		header2.createCell(7).setCellValue(messageSource.getMessage("", null, "Third Party Contact", userLocale));
		header2.getCell(7).setCellStyle(style);
		
		header2.createCell(8).setCellValue(messageSource.getMessage("", null, "Email", userLocale));
		header2.getCell(8).setCellStyle(style);
		
		header2.createCell(9).setCellValue(messageSource.getMessage("", null, "Telephone", userLocale));
		header2.getCell(9).setCellStyle(style);

		header2.createCell(10).setCellValue(messageSource.getMessage("", null, "Client", userLocale));
		header2.getCell(10).setCellStyle(style);

		header2.createCell(11).setCellValue(messageSource.getMessage("", null, "Instrument", userLocale));
		header2.getCell(11).setCellStyle(style);

		header2.createCell(12).setCellValue(messageSource.getMessage("", null, "Serial No", userLocale));
		header2.getCell(12).setCellStyle(style);
		
		header2.createCell(13).setCellValue(messageSource.getMessage("", null, "State", userLocale));
		header2.getCell(13).setCellStyle(style);
		
		header2.createCell(14).setCellValue(messageSource.getMessage("", null, "Sent TP", userLocale));
		header2.getCell(14).setCellStyle(style);
		
		header2.createCell(15).setCellValue(messageSource.getMessage("", null, "PO Del Date", userLocale));
		header2.getCell(15).setCellStyle(style);
		
		header2.createCell(16).setCellValue(messageSource.getMessage("", null, "PO Number", userLocale));
		header2.getCell(16).setCellStyle(style);
		
		header2.createCell(17).setCellValue(messageSource.getMessage("", null, "Days Away", userLocale));
		header2.getCell(17).setCellStyle(style);
		
		int rowCount2 = 1;
		for(ItemAtTP itemTp : itemsAtTp) {
			Row row1 = sheet2.createRow(rowCount2++);
			row1.createCell(0).setCellValue(itemTp.getCompanyrole());
			row1.createCell(1).setCellValue(itemTp.getCountry());
			row1.createCell(2).setCellValue(itemTp.getConame());
			row1.createCell(3).setCellValue(itemTp.getSubname());
			row1.createCell(4).setCellValue(itemTp.getJobNo());
			row1.createCell(5).setCellValue(itemTp.getItemNo());
			row1.createCell(7).setCellValue(itemTp.getName());
			row1.createCell(8).setCellValue(itemTp.getEmail());
			row1.createCell(9).setCellValue(itemTp.getTelephone());
			row1.createCell(10).setCellValue(itemTp.getClientCompany());
			row1.createCell(11).setCellValue(itemTp.getItemDesc());
			row1.createCell(12).setCellValue(itemTp.getSerialNo());
			row1.createCell(13).setCellValue(itemTp.getStatusTranslation());
			
			if(itemTp.getSentOn() != null){
				row1.createCell(14).setCellValue(itemTp.getSentOn());
				row1.getCell(14).setCellStyle(dateStyle);
			}
			if(itemTp.getDateIn() != null){
				row1.createCell(6).setCellValue(itemTp.getDateIn());
				row1.getCell(6).setCellStyle(dateStyle);
			} 

			if(itemTp.getPoDelDate() != null){
				row1.createCell(15).setCellValue(itemTp.getPoDelDate());
				row1.getCell(15).setCellStyle(dateStyle);
			}
			row1.createCell(16).setCellValue(itemTp.getPono());
			row1.createCell(17).setCellValue(itemTp.getDaysAway());
			
		}
		
		long finishTime = System.currentTimeMillis();
		logger.debug("JobItemViewXlsView processed "+ (rowIndex-1) +" jobs/items in "+(finishTime-startTime)+"ms ");
		
	}
}
