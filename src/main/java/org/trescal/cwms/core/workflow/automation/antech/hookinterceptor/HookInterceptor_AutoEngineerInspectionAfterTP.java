package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_AutoEngineerInspectionAfterTP extends HookInterceptor<HookInterceptor_AutoEngineerInspectionAfterTP.Input>
{
	public static final String HOOK_NAME = "auto-engineer-inspection";
	@Autowired
	private ActionOutcomeService actionOutcomeServ;

	public static class Input {
		
		private JobItem ji;
		
		public Input(JobItem ji) {
			this.ji = ji;
		}
	}
	
	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: AUTO ENGINEER INSPECTION");

				
					Contact con = this.sessionServ.getCurrentContact();;
					JobItem ji = input.ji;
					HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

					JobItemActivity jia = new JobItemActivity();
					jia.setActivity(context.getActivity());
					jia.setActivityDesc(context.getActivity().getDescription());
					jia.setStartStatus((ItemStatus) ji.getState());
					jia.setStartStamp(new Date());
					jia.setStartedBy(con);
					jia.setCreatedOn(new Date());
					jia.setJobItem(ji);
					jia.setComplete(context.isCompleteActivity());
					
					if (context.isCompleteActivity()) {
						jia.setEndStamp(new Date());
						jia.setTimeSpent(5);
						jia.setCompletedBy(con);
						ActionOutcome defaultOutcome = this.actionOutcomeServ
								.findDefaultActionOutcomeForActivity(jia.getActivity().getStateid());
						jia.setOutcome(defaultOutcome);
					}

					ji.getActions().add(jia);
					
					ItemStatus endStatus = super.getNextStatus(context);
					ji.setState(endStatus);
					jobItemService.mergeJobItem(ji);
					if (context.isCompleteActivity()) {
						jia.setEndStatus(endStatus);
					}

					this.persist(jia, endStatus, context);
				}
	}
}
