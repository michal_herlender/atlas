package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_NoOperationPerformedOutcome implements LookupResultMessage {

	FAILURE_REPORT_NOT_REQUIRED("actionoutcome.noactionperformed.failurereporetnotrequired"),
	FAILURE_REPORT_REQUIRED("actionoutcome.contractreview.requiredfailurereport");

	private String messageCode;

	private Lookup_NoOperationPerformedOutcome(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}

}
