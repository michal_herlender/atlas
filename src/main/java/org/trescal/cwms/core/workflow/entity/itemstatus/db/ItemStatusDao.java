package org.trescal.cwms.core.workflow.entity.itemstatus.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

public interface ItemStatusDao extends BaseDao<ItemStatus, Integer> {
	
	List<ItemActivity> findActivities();
	
	List<ItemStatus> findStatuses();
	
	List<ItemStatus> searchItemStatus(String description);
}