package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemRequiresFurtherCalibration;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemRequiresFurtherCalibration")
public class Lookup_ItemRequiresFurtherCalibrationService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item requires another calibration or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM REQUIRES FURTHER CALIBRATION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = Lookup_ItemRequiresFurtherCalibration.NO;
		for (JobItemWorkRequirement jiwr : ji.getWorkRequirements()) {
			if ((jiwr.getWorkRequirement().getType().equals(WorkRequirementType.CALIBRATION)
                || jiwr.getWorkRequirement().getType().equals(WorkRequirementType.ONSITE)) &&
                !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE) &&
                !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED) &&
                (jiwr.getWorkRequirement().getCapability() != null)) {
                result = Lookup_ItemRequiresFurtherCalibration.YES;
                break;
            }
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemRequiresFurtherCalibration.values();
	}
}