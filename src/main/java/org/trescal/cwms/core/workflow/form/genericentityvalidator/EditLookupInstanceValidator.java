package org.trescal.cwms.core.workflow.form.genericentityvalidator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.form.edit.EditLookupInstanceForm;
import org.trescal.cwms.core.workflow.form.edit.EditOutcomeStatusDto;

@Component
public class EditLookupInstanceValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> arg0) {
		return EditLookupInstanceForm.class.isAssignableFrom(arg0);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			EditLookupInstanceForm form = (EditLookupInstanceForm) target;
			long countOutcomeStatus = form.getOutcomeStatusDtos().stream().
					filter(dto -> !dto.getDelete()).count();
			if (countOutcomeStatus == 0) {
				errors.reject("editlookupinstance.nooutcomestatus", "The Lookup Instance must have at least one Outcome Status defined.");
			}
			for (EditOutcomeStatusDto dto : form.getOutcomeStatusDtos()) {
				if (!dto.getDelete()) {
					if (dto.getLookupOrStatus() && (dto.getLookupId() == 0)) {
						errors.reject("editlookupinstance.chooselookuporstatus", "A Lookup or Status must be chosen for each Outcome Status");
					}
					else if (!dto.getLookupOrStatus() && (dto.getStatusId() == 0)) {
						errors.reject("editlookupinstance.chooselookuporstatus", "A Lookup or Status must be chosen for each Outcome Status");
					}
				}
			}
			// Check that a valid (not to delete) outcome status is defined for each lookup result message
			for (int i = 0; i < form.getLookupFunction().getLookupResultMessages().length; i++) {
				EditOutcomeStatusDto dto = null;
				// Possible that no outome statuses have been created for wiring, hence no lookups
				if (form.getSelectedLookups().size() > i) {
					Integer selectedOutcome = form.getSelectedLookups().get(i);
					if (selectedOutcome != null) {
						dto = form.getOutcomeStatusDtos().get(selectedOutcome);
					}
				}
				if (dto == null) {
					errors.reject("editlookupinstance.selectoutcomestatus", "Select an Outcome Status for Lookup Result "+i);
				}
				else if (dto.getDelete()) {
					errors.reject("editlookupinstance.deletedoutcomestatus", "Select a non-deleted Outcome Status for Lookup Result "+i);
				}
			}
		}
	}

}
