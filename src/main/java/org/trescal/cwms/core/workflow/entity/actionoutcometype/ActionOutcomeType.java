package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.workflow.entity.actionoutcome.*;

import javax.annotation.PostConstruct;

public enum ActionOutcomeType {
	ADJUSTMENT(ActionOutcome_Adjustment.class, ActionOutcomeValue_Adjustment.class),
	CALIBRATION(ActionOutcome_Calibration.class, ActionOutcomeValue_Calibration.class),
	CLIENT_DECISION_AS_FOUND(ActionOutcome_ClientDecisionAsFound.class, ActionOutcomeValue_ClientDecisionAsFound.class),
	CLIENT_DECISION_COSTING(ActionOutcome_ClientDecisionCosting.class, ActionOutcomeValue_ClientDecisionCosting.class),
	CLIENT_DECISION_EXCHANGE(ActionOutcome_ClientDecisionExchange.class,
			ActionOutcomeValue_ClientDecisionCosting.class),
	CLIENT_DECISION_FURTHER_WORK(ActionOutcome_ClientDecisionFurtherWork.class,
			ActionOutcomeValue_ClientDecisionFurtherWork.class),
	CLIENT_DECISION_TP_FURTHER_WORK(ActionOutcome_ClientDecisionTPFurtherWork.class,
			ActionOutcomeValue_ClientDecisionTPFurtherWork.class),
	CLIENT_DECISION_TP_INSPECTION_COSTING(ActionOutcome_ClientDecisionTPInspectionCosts.class,
			ActionOutcomeValue_ClientDecisionCosting.class),
	CLIENT_DECISION_TP_JOB_COSTING(ActionOutcome_ClientDecisionTPJobCosting.class,
			ActionOutcomeValue_ClientDecisionCosting.class),
	COMPONENTS_FITTED(ActionOutcome_ComponentsFitted.class, ActionOutcomeValue_ComponentsFitted.class),
	CONTRACT_REVIEW(ActionOutcome_ContractReview.class, ActionOutcomeValue_ContractReview.class),
	ENGINEER_INSPECTION(ActionOutcome_EngineerInspection.class, ActionOutcomeValue_EngineerInspection.class),
	FAULT_REPORT(ActionOutcome_FaultReport.class, ActionOutcomeValue_FaultReport.class), /* Deprecated */
	POST_TP_FURTHER_WORK(ActionOutcome_PostTPFurtherWork.class, ActionOutcomeValue_PostTPFurtherWork.class),
	REPAIR(ActionOutcome_Repair.class, ActionOutcomeValue_Repair.class),
	TP_REQUIREMENTS(ActionOutcome_TPRequirements.class, ActionOutcomeValue_TPRequirements.class),
	NO_OPERATION_PERFORMED(ActionOutcome_NoOperationPerformed.class, ActionOutcomeValue_NoOperationPerformed.class),
	GENERAL_SERVICE_OPERATION(ActionOutcome_GeneralServiceOperation.class, ActionOutcomeValue_GeneralServiceOperation.class),
	CLIENT_DECISION_GENERAL_SERVICE_OPERATION(ActionOutcome_ClientDecisionForGeneralServiceOperation.class, ActionOutcomeValue_ClientDecisionForGeneralServiceOperation.class);

	private final Class<? extends ActionOutcome> outcomeClass;
	private final Class<? extends Enum<?>> valueClass;

	ActionOutcomeType(Class<? extends ActionOutcome> outcomeClass, Class<? extends Enum<?>> valueClass) {
		this.outcomeClass = outcomeClass;
		this.valueClass = valueClass;
	}

	/**
	 * Injects all values with message source at once
	 */
	@Component
	public static class ActionOutcomeValue_MessageSourceInjector {

		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (ActionOutcomeType type : ActionOutcomeType.values())
				for (Enum<?> value : type.getValueClass().getEnumConstants())
					((ActionOutcomeValue) value).setMessageSource(messageSource);
		}
	}

	public Class<? extends Enum<?>> getValueClass() {
		return valueClass;
	}

	/**
	 * @return the outcomeClass
	 */
	public Class<? extends ActionOutcome> getOutcomeClass() {
		return outcomeClass;
	}
}