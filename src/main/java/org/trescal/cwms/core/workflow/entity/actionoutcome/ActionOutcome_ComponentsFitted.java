package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ComponentsFitted;

@Entity
@DiscriminatorValue("components_fitted")
public class ActionOutcome_ComponentsFitted extends ActionOutcome {
	private ActionOutcomeValue_ComponentsFitted value;
	
	public ActionOutcome_ComponentsFitted() {
		this.type = ActionOutcomeType.COMPONENTS_FITTED;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_ComponentsFitted getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_ComponentsFitted value) {
		this.value = value;
	}

}
