package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_AdditionalDemandsCompleted implements LookupResultMessage {
	NOT_COMPLETED("lookupresultmessage.additionaldemandscompleted.notcompleted"),
	COMPLETED("lookupresultmessage.additionaldemandscompleted.completed");

	private String messageCode;

	private Lookup_AdditionalDemandsCompleted(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
}