package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_PlaceOnHold extends HookInterceptor<HookInterceptor_PlaceOnHold.Input>
{
	public static final String HOOK_NAME = "place-on-hold";
	
	public static class Input {
		private JobItem ji;
		private ItemStatus newStatus;
		private String remark;
		public Input(JobItem ji, ItemStatus newStatus, String remark) {
			this.ji = ji;
			this.newStatus = newStatus;
			this.remark = remark;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException
	{
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive())
		{
			this.logger.debug("RUNNING HOOK: PLACE ON HOLD");
			Contact con = this.sessionServ.getCurrentContact();
			ItemStatus newStatus = (ItemStatus) input.newStatus;
			JobItem ji = (JobItem) input.ji;
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) ji.getState());
			jia.setStartStamp(new Date());
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(ji);
			jia.setComplete(context.isCompleteActivity());

			if (context.isCompleteActivity())
			{
				jia.setEndStamp(new Date());
				jia.setTimeSpent(5);
				jia.setCompletedBy(con);
			}
			ji.setState(newStatus);
			jobItemService.updateJobItem(ji);
			if (context.isCompleteActivity())
			{
				jia.setRemark(input.remark);
				jia.setEndStatus(newStatus);
			}

			this.persist(jia, newStatus, context);
		}
	}
}