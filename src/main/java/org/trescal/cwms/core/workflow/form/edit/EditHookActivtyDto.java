package org.trescal.cwms.core.workflow.form.edit;

import javax.validation.constraints.NotNull;

public class EditHookActivtyDto {
	private Integer id;
	private Boolean beginActivity;
	private Boolean completeActivity;
	private Boolean statusOrActivity;	// Hook could be activated in either a status or activity - stateid in HookActivity
	private Integer startStatusId;		// If statusOrActivity = true
	private Integer startActivityId;	// If statusOrActivity = true
	private Integer finishActivityId;	// Destination activty 	
	private Boolean delete;
	
	@NotNull
	public Integer getId() {
		return id;
	}
	@NotNull
	public Boolean getBeginActivity() {
		return beginActivity;
	}
	@NotNull
	public Boolean getCompleteActivity() {
		return completeActivity;
	}
	@NotNull
	public Boolean getStatusOrActivity() {
		return statusOrActivity;
	}
	public Integer getStartStatusId() {
		return startStatusId;
	}
	public Integer getStartActivityId() {
		return startActivityId;
	}
	@NotNull
	public Integer getFinishActivityId() {
		return finishActivityId;
	}
	@NotNull
	public Boolean getDelete() {
		return delete;
	}

	public void setBeginActivity(Boolean beginActivity) {
		this.beginActivity = beginActivity;
	}
	public void setCompleteActivity(Boolean completeActivity) {
		this.completeActivity = completeActivity;
	}
	public void setStatusOrActivity(Boolean statusOrActivity) {
		this.statusOrActivity = statusOrActivity;
	}
	public void setStartStatusId(Integer startStatusId) {
		this.startStatusId = startStatusId;
	}
	public void setStartActivityId(Integer startActivityId) {
		this.startActivityId = startActivityId;
	}
	public void setFinishActivityId(Integer finishActivityId) {
		this.finishActivityId = finishActivityId;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}	
}
