package org.trescal.cwms.core.workflow.form.edit;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;

/**
 * Intended for editing ActionOutcome attached to an ItemActivity.
 */
public class EditActionOutcomeDto {
	private Integer id;
	private Boolean active;
	private Boolean defaultOutcome;
	private Boolean positiveOutcome;
	private ActionOutcomeType type;		// Set for each dto, as the type *might* not always match the form
	private String value;				// The value together with type (set on form) gets the ActionOutcomeValue  
	private Boolean delete;
	
	@NotNull
	public Integer getId() {
		return id;
	}
	@NotNull
	public Boolean getActive() {
		return active;
	}
	@NotNull
	public Boolean getDefaultOutcome() {
		return defaultOutcome;
	}
	@NotNull
	public Boolean getPositiveOutcome() {
		return positiveOutcome;
	}
	@NotNull
	public ActionOutcomeType getType() {
		return type;
	}
	@NotNull
	public String getValue() {
		return value;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public void setDefaultOutcome(Boolean defaultOutcome) {
		this.defaultOutcome = defaultOutcome;
	}
	public void setPositiveOutcome(Boolean positiveOutcome) {
		this.positiveOutcome = positiveOutcome;
	}
	public Boolean getDelete() {
		return delete;
	}
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setType(ActionOutcomeType type) {
		this.type = type;
	}
}
