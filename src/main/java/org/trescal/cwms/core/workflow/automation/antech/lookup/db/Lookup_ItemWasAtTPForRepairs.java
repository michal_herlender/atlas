package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemWasAtTPForRepairs implements LookupResultMessage {

	YES("yes"), NO("no");

	private String messageCode;

	private Lookup_ItemWasAtTPForRepairs(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}

}
