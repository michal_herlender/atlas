package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtTP;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemAtTP")
public class Lookup_ItemAtTPService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether or not the item is at a third party.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM AT TP");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		if (ji.getCurrentAddr() != null) {
			Subdiv sub = ji.getCurrentAddr().getSub();
			if (sub.getComp().getCompanyRole().equals(CompanyRole.SUPPLIER)) {
				// Job item is evidently at a third party
				result = Lookup_ItemAtTP.YES;
			} 
			else if (sub.getComp().getCompanyRole().equals(CompanyRole.BUSINESS)
					&& (sub.getSubdivid() != ji.getJob().getOrganisation().getSubdivid())) {
				// At a business subdivision other than the job's subdivision
				// If the next work requirement is set, we can use it to help 
				// determine whether the job item is at the 'correct' TP address
				Integer nextWorkRequirementAddressId = null;
				Integer nextWorkReqTPHomeSubdivId = null;
				if (ji.getNextWorkReq() != null) {
                    WorkRequirement wr = ji.getNextWorkReq().getWorkRequirement();
                    if (wr.getAddress() != null) {
                        nextWorkRequirementAddressId = wr.getAddress().getAddrid();
                    }
                    if (wr.getCapability() != null && wr.getType().equals(WorkRequirementType.THIRD_PARTY)) {
                        // This is the "owning subdivision" of the third party capability;
                        // the third party will always be external / different to this subdiv id.
                        nextWorkReqTPHomeSubdivId = wr.getCapability().getOrganisation().getSubdivid();
                    }
                }
				
				if (nextWorkRequirementAddressId != null) {
					if (nextWorkRequirementAddressId.equals(ji.getCurrentAddr().getAddrid())) {
						result = Lookup_ItemAtTP.YES;
					}
					else {
						result = Lookup_ItemAtTP.NO;
					}
				} 
				else if (nextWorkReqTPHomeSubdivId != null) {
					if (nextWorkReqTPHomeSubdivId.equals(ji.getCurrentAddr().getSub().getSubdivid())) {
						// Job item is still at the "home" business subdivision from where it should be externalized
						result = Lookup_ItemAtTP.NO;
					}
					else {
						result = Lookup_ItemAtTP.YES;
					}
				}
				else {
					// No other information, however if the job item is currently at a different business subdivision
					// than the home subdivision of the job, consider it as being at a third party
					result = Lookup_ItemAtTP.YES;
				}
			}
			else {
				result = Lookup_ItemAtTP.NO;
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemAtTP.values();
	}
}