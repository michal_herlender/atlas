package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemAtInspectionLocation implements LookupResultMessage
{
	YES("lookupresultmessage.itematinspectionlocation.yes"),
	NO("lookupresultmessage.itematinspectionlocation.no");
	
	private String messageCode;
	
	private Lookup_ItemAtInspectionLocation(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}