package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

/*
 * Note - this class was used by MD configuration only - not Antech. 
 */
@Service
public class HookInterceptor_AddItemPO extends HookInterceptor<JobItemPO>
{
	public static final String HOOK_NAME = "add-item-po"; 
	@Override
	public void recordAction(JobItemPO jipo) throws OpenActivityException
	{
		Hook hook = super.lookupHook(HOOK_NAME);

		if ((hook != null) && hook.isActive())
		{
			this.logger.debug("RUNNING HOOK: ADD ITEM PO");
			Contact con = this.sessionServ.getCurrentContact();
			JobItem ji = jipo.getItem();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			// ONLY ADD THE ACTIVITY AND CHANGE THE STATUS IF THIS IS THE FIRST
			// PO ADDED TO THE JOB ITEM!!
			if (ji.getItemPOs().size() == 1)
			{
				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				jia.setStartStamp(new Date());
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());

				if (context.isCompleteActivity())
				{
					jia.setEndStamp(new Date());
					jia.setTimeSpent(5);
					jia.setCompletedBy(con);
				}

				ItemStatus endStatus = super.getNextStatus(context);
				ji.setState(endStatus);

				jobItemService.mergeJobItem(ji);

				if (context.isCompleteActivity())
				{
					jia.setEndStatus(endStatus);
				}

				this.persist(jia, endStatus, context);
			}
		}
	}
}