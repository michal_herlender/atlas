package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_TPCalibrationOutcome implements LookupResultMessage
{
	NO_FURTHER_WORK("lookupresultmessage.tpcalibrationoutcome.nofurtherwork"),
	INHOUSE_CALIBRATION("lookupresultmessage.tpcalibrationoutcome.inhousecalibration"),
	INHOUSE_REPAIR("lookupresultmessage.tpcalibrationoutcome.inhouserepair"),
	ADJUSTMENT("lookupresultmessage.tpcalibrationoutcome.adjustment"),
	THIRD_PARTY_WORK("lookupresultmessage.tpcalibrationoutcome.thirdpartywork"),
	COSTING("lookupresultmessage.tpcalibrationoutcome.costing"),
	FAILURE_REPORT("lookupresultmessage.tpcalibrationoutcome.failurereport"),
	NO_FURTHER_WORK_NO_DELIVERY_REQUIRED("lookupresultmessage.tpcalibrationoutcome.nofurtherworknodeliveryrequired");
	
	private String messageCode;
	
	private Lookup_TPCalibrationOutcome(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}