package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.TreeSet;

@Service
public class HookInterceptor_RIRReviewByCSR extends HookInterceptor<HookInterceptor_RIRReviewByCSR.Input> {

	public static final String HOOK_NAME = "rir-review-by-csr";

	public static class Input {
		RepairInspectionReport rir;

		public Input(RepairInspectionReport rir) {
			super();
			this.rir = rir;
		}

	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: " + HOOK_NAME);
			Contact con = this.sessionServ.getCurrentContact();
			JobItem ji = input.rir.getJi();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) ji.getState());
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(ji);
			jia.setComplete(context.isCompleteActivity());
			jia.setCompletedBy(con);

			jia.setTimeSpent(5); // default to 5min
			Date currentDate = new Date();
			Date startDate = DateUtils.addMinutes(currentDate, -5);
			jia.setStartStamp(startDate);
			jia.setEndStamp(currentDate);

			// adds the action into the job item in the session
			if (ji.getActions() == null) {
				ji.setActions(new TreeSet<JobItemAction>(new JobItemActionComparator()));
			}
			ji.getActions().add(jia);

			ItemStatus endStatus = super.getNextStatus(context);
			ji.setState(endStatus);

			jobItemService.updateJobItem(ji);

			if (context.isCompleteActivity()) {
				jia.setEndStatus(endStatus);
			}

			this.persist(jia, endStatus, context);
		}
	}

}
