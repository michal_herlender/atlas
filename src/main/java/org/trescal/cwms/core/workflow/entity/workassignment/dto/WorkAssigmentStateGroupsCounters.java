package org.trescal.cwms.core.workflow.entity.workassignment.dto;

import lombok.Value;

import java.util.Map;

@Value(staticConstructor = "of")
public class WorkAssigmentStateGroupsCounters implements WorkAssigmentCounters {
    Map<Integer, WorkAssigmentStateGroupCounter> stateGroups;
}
