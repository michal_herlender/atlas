package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_PostCalibration implements LookupResultMessage
{
	SUCCESSFULLY("lookupresultmessage.postcalibration.successfully"),
	ADJUSTMENT_APPROVED("lookupresultmessage.postcalibration.adjustmentapproved"),
	ADJUSTMENT_NOT_APPROVED("lookupresultmessage.postcalibration.adjustmentnotapproved"),
	THIRD_PARTY_REPAIR("lookupresultmessage.postcalibration.thirdpartyrepair"),
	INHOUSE_REPAIR("lookupresultmessage.postcalibration.inhouserepair"),
	FAILED_THIRD_PARTY_CALIBRATION("lookupresultmessage.postcalibration.failedthirdpartycalibration"),
	FAILED_THIRD_PARTY_REPAIR("lookupresultmessage.postcalibration.failedthirdpartyrepair"),
	FAILED_INHOUSE_REPAIR("lookupresultmessage.postcalibration.failedinhouserepair"),
	FAILED_UNIT_BER("lookupresultmessage.postcalibration.failedunitber"),
	CALIBRATION_ON_HOLD("lookupresultmessage.postcalibration.calibrationonhold"),
	NOT_SUCCESSFUL("lookupresultmessage.postcalibration.notsuccessful");
	
	private String messageCode;
	
	private Lookup_PostCalibration(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}