package org.trescal.cwms.core.workflow.entity.lookupinstance.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;

public interface LookupInstanceService extends BaseService<LookupInstance, Integer>
{

	List<LookupInstance> findLookupInstanceByName(String name);
}