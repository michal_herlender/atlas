package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_FaultReportAwaitingUpdate implements LookupResultMessage
{
	NO("lookupresultmessage.faultreportawaitingupdate.no"),
	YES("lookupresultmessage.faultreportawaitingupdate.yes");
	
	private String messageCode;
	
	private Lookup_FaultReportAwaitingUpdate(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}