package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ComponentsFittedOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Component("Lookup_ComponentsFittedOutcome")
public class Lookup_ComponentsFittedOutcomeService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the work required after an item's new components have been fitted.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: COMPONENTS FITTED OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		ActionOutcome ao = null;
		for (JobItemAction action : ji.getActions()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;

				if (activity.getActivityDesc().trim().equalsIgnoreCase("Replacement components fitted")
						&& !activity.isDeleted()) {
					ao = activity.getOutcome();
				}
			}
		}
		String desc = ao == null ? "" : ao.getDescription();
		return desc.contains("further repair") ? Lookup_ComponentsFittedOutcome.FURTHER_REPAIR
				: desc.contains("no further calibration") ? Lookup_ComponentsFittedOutcome.NO_FURTHER_WORK
						: desc.contains("further calibration") ? Lookup_ComponentsFittedOutcome.CALIBRATION : null;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ComponentsFittedOutcome.values();
	}
}