package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

@Service
public class HookInterceptor_TakeOffHold extends HookInterceptor<HookInterceptor_TakeOffHold.Input>
{
	public static final String HOOK_NAME = "take-off-hold";
	
	public static class Input {
		private JobItem jobItem;
		private String remark;
		public Input(JobItem jobItem, String remark) {
			this.jobItem = jobItem;
			this.remark = remark;
		}
	}
	
	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException
	{
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive())
		{
			this.logger.debug("RUNNING HOOK: TAKE OFF HOLD");
			JobItem ji = input.jobItem;
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);
			Contact con = this.sessionServ.getCurrentContact();

			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) ji.getState());
			jia.setStartStamp(new Date());
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(ji);
			jia.setComplete(context.isCompleteActivity());

			ItemStatus endStatus = null;
			if (context.isCompleteActivity())
			{
				jia.setEndStamp(new Date());
				jia.setTimeSpent(5);
				jia.setCompletedBy(con);
				jia.setRemark(input.remark);
				endStatus = this.statusCalcServ.getPreviousStatus(ji, StateGroup.ONHOLD);
				ji.setState(endStatus);
				jia.setEndStatus(endStatus);
			}
			else ji.setState(context.getActivity());
			jobItemService.updateJobItem(ji);
			this.persist(jia, endStatus, context);
		}
	}
}