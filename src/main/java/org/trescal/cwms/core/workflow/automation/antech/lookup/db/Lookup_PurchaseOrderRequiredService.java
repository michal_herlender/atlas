package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_PurchaseOrderRequired;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_PurchaseOrderRequired")
public class Lookup_PurchaseOrderRequiredService extends Lookup {

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Purchase Order Required");
		
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		Company allocatedCompany = jobItem.getJob().getOrganisation().getComp();
		Company currentCompany = jobItem.getCurrentAddr().getSub().getComp();
		Company nextWorkingCompany = jobItem.getWorkRequirements().stream()
				.filter(wr -> wr.getWorkRequirement().getType().equals(WorkRequirementType.CALIBRATION))
				.allMatch(wr -> wr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE) ||
						        wr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED)) ? null : jobItem.getCalAddr().getSub().getComp();
		return allocatedCompany.equals(nextWorkingCompany) || currentCompany.equals(nextWorkingCompany)
				|| nextWorkingCompany == null ? Lookup_PurchaseOrderRequired.NO : Lookup_PurchaseOrderRequired.YES;
	}

	@Override
	public String getDescription() {
		return "Purchase order required";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_PurchaseOrderRequired.values();
	}
}