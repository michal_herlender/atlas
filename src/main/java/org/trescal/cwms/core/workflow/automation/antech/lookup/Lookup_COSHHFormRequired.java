package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_COSHHFormRequired implements LookupResultMessage
{
	YES("lookupresultmessage.coshhformrequired.yes"),
	NO("lookupresultmessage.coshhformrequired.no");
	
	private String messageCode;
	
	private Lookup_COSHHFormRequired(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}