package org.trescal.cwms.core.workflow.form;

import java.util.List;

public class CleaningForm
{
	private boolean cleaned;
	private List<String> plantids;
	private String plantid;
	private String remark;
	private int timeSpent;


	public boolean isCleaned()
	{
		return this.cleaned;
	}

	public List<String> getPlantids()
	{
		return this.plantids;
	}

	public String getRemark()
	{
		return this.remark;
	}

	public int getTimeSpent()
	{
		return this.timeSpent;
	}

	public void setCleaned(boolean cleaned)
	{
		this.cleaned = cleaned;
	}

	public void setPlantids(List<String> plantids)
	{
		this.plantids = plantids;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public void setTimeSpent(int timeSpent)
	{
		this.timeSpent = timeSpent;
	}

	public String getPlantid() {
		return plantid;
	}

	public void setPlantid(String plantid) {
		this.plantid = plantid;
	}

}