package org.trescal.cwms.core.workflow.utils;

import java.util.HashSet;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

@Component
public class WorkflowGraphGenerator {

	@Autowired
	private TranslationService translationService;
	
	private WorkflowGraph graph;
	private Locale locale;

	public WorkflowGraph build(String graphName, ItemActivity root,Locale locale) {

		// TODO: change ids to something like type+id
		// TODO: refractor creating nodes and flows
		
		this.graph = new WorkflowGraph();
		this.locale = locale;
		this.graph.setName(graphName);
		this.graph.setActivitySet(new HashSet<WorkflowGraphNode>());
		this.graph.setStatusSet(new HashSet<WorkflowGraphNode>());
		this.graph.setLookupSet(new HashSet<WorkflowGraphNode>());
		this.graph.setLookupResultSet(new HashSet<WorkflowGraphNode>());
		this.graph.setActionOutcomeSet(new HashSet<WorkflowGraphNode>());
		this.graph.setOutcomeStatusSet(new HashSet<WorkflowGraphNode>());
		this.graph.setFlowSet(new HashSet<WorkflowGraphFlow>());

		// root
		graph.setStart(new WorkflowGraphNode(getTypeAndId(root), getText(root)));

		navigateFromActivity(root);
		return graph;
	}

	private void navigateFromActivity(ItemActivity itemActivity) {

		// ItemActivity can have either lookupinstance or outcomestatus(es) or
		// actionoutcome(s)
		if (itemActivity.getLookup() != null) {
			graph.getFlowSet()
					.add(new WorkflowGraphFlow(getTypeAndId(itemActivity), getTypeAndId(itemActivity.getLookup())));
			WorkflowGraphNode node = new WorkflowGraphNode(getTypeAndId(itemActivity.getLookup()),
					getText(itemActivity.getLookup()));
			if (!graph.getLookupSet().contains(node)) {
				graph.getLookupSet().add(node);
				navigateFromLookup(itemActivity.getLookup());
			}
		} else if (itemActivity.getOutcomeStatuses().size() >= 1) {
			for (OutcomeStatus outcomeStatus : itemActivity.getOutcomeStatuses()) {
				graph.getOutcomeStatusSet().add(
						new WorkflowGraphNode(getTypeAndId(outcomeStatus), "--"));
				graph.getFlowSet().add(new WorkflowGraphFlow(getTypeAndId(itemActivity), getTypeAndId(outcomeStatus)));
				navigateFromOutComeStatus(outcomeStatus);
			}
		} else if (itemActivity.getActionOutcomes().size() >= 1) {
			for (ActionOutcome actionOutcome : itemActivity.getActionOutcomes()) {
				graph.getActionOutcomeSet()
						.add(new WorkflowGraphNode(getTypeAndId(actionOutcome), getText(actionOutcome)));
				graph.getFlowSet().add(new WorkflowGraphFlow(getTypeAndId(itemActivity), getTypeAndId(actionOutcome)));
				navigateFromActionOutcome(actionOutcome);
			}
		}
	}

	private void navigateFromActionOutcome(ActionOutcome actionOutcome) {
		ItemActivity itemActivity = actionOutcome.getActivity();
		graph.getFlowSet().add(new WorkflowGraphFlow(getTypeAndId(actionOutcome), getTypeAndId(itemActivity)));
		if (!graph.getActivitySet().contains(itemActivity)) {
			graph.getActivitySet().add(new WorkflowGraphNode(getTypeAndId(itemActivity), getText(itemActivity)));
			navigateFromActivity(itemActivity);
		}
	}

	private void navigateFromOutComeStatus(OutcomeStatus outcomeStatus) {
		// Outcome status is a join to status or lookup
		if (outcomeStatus.getStatus() != null) {
			ItemStatus itemStatus = outcomeStatus.getStatus();
			graph.getStatusSet().add(new WorkflowGraphNode(getTypeAndId(itemStatus), getText(itemStatus)));
			graph.getFlowSet().add(new WorkflowGraphFlow(getTypeAndId(outcomeStatus), getTypeAndId(itemStatus)));
			navigateFromItemStatus(itemStatus);
		} else if (outcomeStatus.getLookup() != null) {
			LookupInstance lookup = outcomeStatus.getLookup();
			graph.getFlowSet().add(new WorkflowGraphFlow(getTypeAndId(outcomeStatus), getTypeAndId(lookup)));
			WorkflowGraphNode node = new WorkflowGraphNode(getTypeAndId(lookup), getText(lookup));
			if (!graph.getLookupSet().contains(node)) {
				graph.getLookupSet().add(node);
				navigateFromLookup(lookup);
			}
		}
	}

	private void navigateFromItemStatus(ItemStatus itemStatus) {
		// ItemStatus should have 1 or more nextActivities
		for (NextActivity nextActivity : itemStatus.getNextActivities()) {
			ItemActivity itemActivity = nextActivity.getActivity();
			graph.getFlowSet().add(new WorkflowGraphFlow(getTypeAndId(itemStatus), getTypeAndId(itemActivity)));
			WorkflowGraphNode node = new WorkflowGraphNode(getTypeAndId(itemActivity), getText(itemActivity));
			if (!graph.getActivitySet().contains(node)) {
				graph.getActivitySet().add(node);
				navigateFromActivity(itemActivity);
			}
		}
	}

	private void navigateFromLookup(LookupInstance lookup) {
		// It's expected that a lookup will / should always have multiple
		// results
		for (LookupResult lookupResult : lookup.getLookupResults()) {
			graph.getLookupResultSet().add(new WorkflowGraphNode(getTypeAndId(lookupResult), getText(lookupResult)));
			graph.getFlowSet().add(new WorkflowGraphFlow(getTypeAndId(lookup), getTypeAndId(lookupResult)));
			navigateFromLookupResult(lookupResult);
		}
	}

	private void navigateFromLookupResult(LookupResult lookupResult) {
		if (lookupResult.getOutcomeStatus() != null) {
			graph.getOutcomeStatusSet().add(new WorkflowGraphNode(getTypeAndId(lookupResult.getOutcomeStatus()),
					"--"));
			graph.getFlowSet().add(
					new WorkflowGraphFlow(getTypeAndId(lookupResult), getTypeAndId(lookupResult.getOutcomeStatus())));
			navigateFromOutComeStatus(lookupResult.getOutcomeStatus());
		}
	}

	private String getTypeAndId(Object node) {
		if (node instanceof ItemActivity)
			return ItemActivity.class.getSimpleName() + "-" + ((ItemActivity) node).getStateid();
		else if (node instanceof ItemStatus)
			return ItemStatus.class.getSimpleName() + "-" + ((ItemStatus) node).getStateid();
		else if (node instanceof ActionOutcome)
			return ActionOutcome.class.getSimpleName() + "-" + ((ActionOutcome) node).getId();
		else if (node instanceof OutcomeStatus)
			return OutcomeStatus.class.getSimpleName() + "-" + ((OutcomeStatus) node).getId();
		else if (node instanceof LookupInstance)
			return LookupInstance.class.getSimpleName() + "-" + ((LookupInstance) node).getId();
		else if (node instanceof LookupResult)
			return LookupResult.class.getSimpleName() + "-" + ((LookupResult) node).getId();
		return null;
	}

	private String getText(ActionOutcome actionOutcome) {
		StringBuffer result = new StringBuffer();
		result.append("Action Outcome");
		result.append(" ");
		result.append(actionOutcome.getId());
		return result.toString();
	}

	private String getText(ItemState itemState) {

		StringBuffer result = new StringBuffer();
//		Class<?> clazz = HibernateProxyHelper.getClassWithoutInitializingProxy(itemState);
//		if (HoldActivity.class.equals(clazz))
//			result.append("Hold Activity");
//		else if (OverrideActivity.class.equals(clazz))
//			result.append("Override Activity");
//		else if (ProgressActivity.class.equals(clazz))
//			result.append("Progress Activity");
//		else if (TransitActivity.class.equals(clazz))
//			result.append("Transit Activity");
//		else if (WorkActivity.class.equals(clazz))
//			result.append("Work Activity");
//		else if (HoldStatus.class.equals(clazz))
//			result.append("Hold Status");
//		else if (WorkStatus.class.equals(clazz))
//			result.append("Work Status");
//		else if (ItemStatus.class.equals(clazz))
//			result.append("Item Status");
//		else if (ItemActivity.class.equals(clazz))
//			result.append("Item Activity");
//		else
//			result.append(itemState.getClass().getSimpleName());

//		result.append(" ");
		result.append(itemState.getStateid());
		result.append(" ");
		result.append(translationService.getCorrectTranslation(itemState.getTranslations(), locale));
		return result.toString();
	}

	private String getText(LookupInstance lookupInstance) {
		StringBuffer result = new StringBuffer();
//		result.append("LookupInstance");
//		result.append(" ");
//		result.append(lookupInstance.getId());
//		result.append(" ");
		// TODO add translations for locale
//		result.append(lookupInstance.getLookupFunction().getBeanName());
		result.append(lookupInstance.getDescription());
		return result.toString();
	}

	private String getText(LookupResult lookupResult) {
		StringBuffer result = new StringBuffer();
//		result.append("LookupResult");
//		result.append(" ");
//		result.append(lookupResult.getId());
//		result.append(" ");
		result.append(lookupResult.getResultMessageId());
		result.append(" ");
		result.append(lookupResult.getResultMessage());
		return result.toString();
	}

}
