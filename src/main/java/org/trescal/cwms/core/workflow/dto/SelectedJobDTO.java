package org.trescal.cwms.core.workflow.dto;

import java.time.LocalDate;

import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SelectedJobDTO implements Comparable<SelectedJobDTO> {

	private int jobId;
	private String jobNo;
	private String jobStatus;
	private LocalDate regDate;
	private LocalDate dueDate;
	private Boolean fastTrack;
	private JobType type;
	private Integer clientCompanyId;
	private String clientCompanyName;
	private Integer clientSubdivId;
	private String clientSubdivName;
	private Integer clientContactId;
	private String clientContactName;
	private Boolean clientCompanyActive;
	private Boolean clientCompanyOnStop;
	private Boolean clientContactActive;
	private Integer transportOutId;
	private String transportOutName;
	private Integer allocatedSubdivId;
	private String allocatedSubdivCode;

	public SelectedJobDTO(int jobId, String jobNo, String jobStatus, LocalDate regDate, LocalDate dueDate, Boolean fastTrack,
			JobType type, Integer clientContactId, String clientContactName, Boolean clientContactActive,
			Integer clientCompanyId, String clientCompanyName, Integer clientSubdivId, String clientSubdivName,
			Boolean clientCompanyActive, Boolean clientCompanyOnStop, Integer transportOutId, String transportOutName,
			Integer allocatedSubdivId, String allocatedSubdivCode) {
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.jobStatus = jobStatus;
		this.setRegDate(regDate);
		this.dueDate = dueDate;
		this.fastTrack = fastTrack;
		this.type = type;
		this.clientContactId = clientContactId;
		this.clientContactName = clientContactName;
		this.clientContactActive = clientContactActive;
		this.clientCompanyId = clientCompanyId;
		this.clientCompanyName = clientCompanyName;
		this.setClientSubdivId(clientSubdivId);
		this.clientSubdivName = clientSubdivName;
		this.clientCompanyActive = clientCompanyActive;
		this.clientCompanyOnStop = clientCompanyOnStop;
		this.transportOutId = transportOutId;
		this.transportOutName = transportOutName;
		this.allocatedSubdivId = allocatedSubdivId;
		this.allocatedSubdivCode = allocatedSubdivCode;
	}

	@Override
	public int compareTo(SelectedJobDTO other) {
		return fastTrack && !other.fastTrack ? -1
				: !fastTrack && other.fastTrack ? 1
						: dueDate.compareTo(other.dueDate) != 0 ? dueDate.compareTo(other.getDueDate())
								: jobId - other.getJobId();
	}
}