package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;

public interface ActionOutcomeValue {
	ActionOutcomeType getType();
	String getDescription();		// This should match ActionOutcome::description until all lookups refactored to use enums
	String name();					// This returns the EXACT name from the enum
	String getMessageCode();
	String getTranslation();		// Returns best translation for current locale
	String getTranslationForLocale(Locale locale);
	void setMessageSource(MessageSource messageSource);
}
