package org.trescal.cwms.core.workflow.form;

import java.util.List;

import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ViewQueuedCalibrationsForm
{
	private List<QueuedCalibration> accreditedQueuedCals;
	private List<QueuedCalibration> queuedCals;
	private List<Integer> selectedCals;

}
