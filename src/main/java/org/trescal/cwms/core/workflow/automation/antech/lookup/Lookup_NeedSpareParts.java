package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_NeedSpareParts implements LookupResultMessage {

	NO("no"), 
	REPLACE_COMPONENTS("lookupresultmessage.repairoutcome.replacecomponents"),
	SPARE_PARTS("lookupresultmessage.repairoutcome.spareparts");

	private String messageCode;

	private Lookup_NeedSpareParts(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}

}
