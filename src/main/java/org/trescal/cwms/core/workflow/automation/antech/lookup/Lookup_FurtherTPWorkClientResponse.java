package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_FurtherTPWorkClientResponse implements LookupResultMessage
{
	APPROVED_FURTHER_WORK("lookupresultmessage.furthertpworkclientresponse.approvedfurtherwork"),
	REQUESTED_NO_FURTHER_ACTION("lookupresultmessage.furthertpworkclientresponse.requestednofurtheraction"),
	APPROVED_AS_FOUND_RESULTS("lookupresultmessage.furthertpworkclientresponse.approvedasfoundresults");
	
	private String messageCode;
	
	private Lookup_FurtherTPWorkClientResponse(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}