package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_LatestFaultReportOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_LatestFaultReportOutcome")
public class Lookup_LatestFaultReportOutcomeService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Latest Failure Report Outcome");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		// load fault report
		FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);

		if (FailureReportFinalOutcome.INTERNAL_CALIBRATION_WITH_JUDGMENT.equals(fr.getFinalOutcome())
				|| FailureReportFinalOutcome.INTERNAL_CALIBRATION_WITHOUT_JUDGMENT.equals(fr.getFinalOutcome())) {
			return Lookup_LatestFaultReportOutcome.ERP_CALIBRATION;
		} else if (FailureReportFinalOutcome.INTERNAL_REPAIR.equals(fr.getFinalOutcome())
				|| FailureReportFinalOutcome.THIRD_PARTY_REPAIR.equals(fr.getFinalOutcome())) {
			return Lookup_LatestFaultReportOutcome.ERP_REPAIR;
		} else if (FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITH_JUDGMENT.equals(fr.getFinalOutcome())
				|| FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT.equals(fr.getFinalOutcome())) {
			return Lookup_LatestFaultReportOutcome.THIRD_PARTY;
		} else if (FailureReportFinalOutcome.SCRAP.equals(fr.getFinalOutcome()))
			return Lookup_LatestFaultReportOutcome.SCRAPPING_PROPOSAL;
		else if (FailureReportFinalOutcome.RETURN_TO_CLIENT.equals(fr.getFinalOutcome()))
			return Lookup_LatestFaultReportOutcome.RETURN_TO_CLIENT;
		else if (FailureReportFinalOutcome.SPARE_PARTS_PROCUREMENT.equals(fr.getFinalOutcome()))
			return Lookup_LatestFaultReportOutcome.THIRD_PARTY;

		return Lookup_LatestFaultReportOutcome.COULD_NOT_DETEMINE;
	}

	@Override
	public String getDescription() {
		return "Determines the outcome of fault report";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_LatestFaultReportOutcome.values();
	}
}