package org.trescal.cwms.core.workflow.utils;

import java.util.ArrayList;
import java.util.List;

public class WorkflowNode {
	private int id;
	private String text;
	private List<WorkflowNode> children;
	
	public WorkflowNode(int id, String text) {
		this.id = id;
		this.text = text;
		this.children = new ArrayList<WorkflowNode>();
	}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public List<WorkflowNode> getChildren() {
		return children;
	}
	
	public void addChild(WorkflowNode node) {
		this.children.add(node);
	}
	public void addChildren(List<WorkflowNode> children) {
		this.children.addAll(children);
	}
}
