package org.trescal.cwms.core.workflow.controller.edit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.form.edit.EditItemStatusForm;
import org.trescal.cwms.core.workflow.form.edit.EditNextActivityDto;
import org.trescal.cwms.core.workflow.form.genericentityvalidator.EditItemStatusValidator;
import org.trescal.cwms.core.workflow.service.EditWorkflowService;

@Controller @IntranetController
public class EditItemStatusController extends AbstractEditWorkflowController {
	@Autowired
	private EditWorkflowService editWorkflowService;
	@Autowired
	private EditItemStatusValidator validator;
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@InitBinder("form")
    private void initValidator(WebDataBinder binder) {
    	binder.setValidator(validator);
    }
	
	@ModelAttribute("form")
	public EditItemStatusForm formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		EditItemStatusForm form = new EditItemStatusForm();
		form.setDefaultActivityId(0);
		form.setDefaultManualEntryAllowed(false);
		form.setDefaultLinkType(StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV);
		form.setDefaultStateGroup(StateGroup.INANOTHERBUSINESSCOMPANY);
		if (id == 0) {
			// Creating new record
			form.setActive(true);
			form.setRetired(false);
			form.setNextActivities(new ArrayList<>());
			form.setStateGroupLinks(new ArrayList<>());
			form.setWorkStatus(true);
		}
		else {
			// Editing existing record
			ItemStatus itemStatus = this.itemStateService.findItemStatus(id);
			form.setActive(itemStatus.getActive());
			form.setDescription(itemStatus.getDescription());
			form.setNextActivities(getNextActivitiesList(itemStatus));
			form.setRetired(itemStatus.getRetired());
			form.setStateGroupLinks(getStateGroupList(itemStatus));
			form.setTranslations(getTranslationMap(itemStatus.getTranslations()));
			form.setWorkStatus(itemStatus.isStatus());
		}
		return form;
	}
	
	private List<EditNextActivityDto> getNextActivitiesList(ItemStatus itemStatus) {
		return itemStatus.getNextActivities().stream().
				map(nextActivity -> new EditNextActivityDto(nextActivity)).
				collect(Collectors.toList());
	}
	
	@RequestMapping(value="edititemstatus.htm", method=RequestMethod.GET)
	public String showView(Model model,
			@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		model.addAttribute("supportedLocales", this.supportedLocaleService.getSupportedLocales());
		model.addAttribute("stateGroupLinkTypes", StateGroupLinkType.values());
		model.addAttribute("stateGroups", StateGroup.values());
		model.addAttribute("itemActivities", getItemActivities());
		model.addAttribute("selectBoolean", getSelectBoolean());
		
		if (id != 0) model.addAttribute("lastStatus", this.itemStateService.findItemStatus(id));
		return "trescal/core/workflow/edit/edititemstatus";
	}
	
	@RequestMapping(value="edititemstatus.htm", method=RequestMethod.POST)
	public String onSubmit(Model model,
			@Validated @ModelAttribute("form") EditItemStatusForm form,
			BindingResult bindingResult,
			@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		if (bindingResult.hasErrors()) {
			return showView(model, id);
		}
		Integer statusId = null;
		if (id == 0) {
			// Creating new record
			statusId = this.editWorkflowService.createItemStatus(form);
		}
		else {
			// Editing existing record
			statusId = id;
			this.editWorkflowService.updateItemStatus(form, statusId);
		}
		return "redirect:workfloweditor_status.htm?statusId="+statusId;
	}
}
