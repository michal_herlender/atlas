package org.trescal.cwms.core.workflow.form.edit;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;

public class EditNextActivityDto {
	private Integer id;						// ID of the corresponding NextActivity, if it exists
	private Boolean manualEntryAllowed;		// Whether user can manually choose
	private Integer activityId;				// ID of the ItemActivity to link to
	private Boolean delete;					// Whether NextActivity is to be deleted
	
	public EditNextActivityDto() {
		// Default constructor for Spring binding
	}
	
	public EditNextActivityDto(NextActivity nextActivity) {
		id = nextActivity.getId();
		manualEntryAllowed = nextActivity.isManualEntryAllowed(); 
		activityId = nextActivity.getActivity().getStateid();
		this.delete = false;
	}
	
	@NotNull
	public Integer getId() {
		return id;
	}
	@NotNull
	public Boolean getManualEntryAllowed() {
		return manualEntryAllowed;
	}
	@NotNull
	public Integer getActivityId() {
		return activityId;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public void setManualEntryAllowed(Boolean manualEntryAllowed) {
		this.manualEntryAllowed = manualEntryAllowed;
	}
	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public Boolean getDelete() {
		return delete;
	}

	public void setDelete(Boolean delete) {
		this.delete = delete;
	}
}
