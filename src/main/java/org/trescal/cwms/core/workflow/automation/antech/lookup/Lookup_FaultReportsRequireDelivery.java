package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_FaultReportsRequireDelivery implements LookupResultMessage {

	YES("yes"), NO_INHOUSE("jifaultreport.no.inhouse"), NO_ONSITE("jifaultreport.no.onsite");

	private String messageCode;

	private Lookup_FaultReportsRequireDelivery(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return this.messageCode;
	}

}
