package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_CostsWithinApprovedLimit implements LookupResultMessage
{
	WITHIN_LIMIT("lookupresultmessage.costswithinapprovedlimit.withinlimit"),
	EXCEED_LIMIT("lookupresultmessage.costswithinapprovedlimit.exceedlimit"),
	NO_LIMIT("lookupresultmessage.costswithinapprovedlimit.nolimit"),
	NO_COSTING_REQUIRED("lookupresultmessage.costswithinapprovedlimit.nocostingrequired");
	
	private String messageCode;
	
	private Lookup_CostsWithinApprovedLimit(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}