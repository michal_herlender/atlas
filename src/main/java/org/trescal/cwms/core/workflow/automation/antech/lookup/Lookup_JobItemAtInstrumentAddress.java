package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_JobItemAtInstrumentAddress implements LookupResultMessage
{
	YES("lookupresultmessage.jobitematinstrumentaddress.yes"),
	NO("lookupresultmessage.jobitematinstrumentaddress.no");
	
	private String messageCode;
	
	private Lookup_JobItemAtInstrumentAddress(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}