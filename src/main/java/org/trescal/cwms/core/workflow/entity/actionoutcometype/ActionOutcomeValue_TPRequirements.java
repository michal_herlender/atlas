package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_TPRequirements implements ActionOutcomeValue {
	NOT_QUOTED_TP_QUOTES_WITHOUT_ITEM(
			"Item is not quoted, third party can quote without seeing item",
			"actionoutcome.tp_reqs.notquoted_tpquotes"),
	NOT_QUOTED_SEND_TO_TP_FOR_QUOTE(
			"Item is not quoted, send to third party for quote",
			"actionoutcome.tp_reqs.notquoted_sendtpforquote"),
	NOT_QUOTED_SEND_TO_TP_FOR_QUOTE_INSPECTION_COSTS(
			"Item is not quoted, send to third party for quote (with inspection costs to client)",
			"actionoutcome.tp_reqs.notquoted_sendtpforinspection"),
	PRE_QUOTED_SEND_CLIENT_COSTS(
			"Item is pre-quoted, send client job costing",
			"actionoutcome.tp_reqs.prequoted_sendclientcosts"),
	PRE_QUOTED_NOT_COSTED_SEND_TO_TP_FOR_WORK(
			"Item is pre-quoted but not costed, send item to third party for work",
			"actionoutcome.tp_reqs.prequoted_notcosted_sendtp"),
	PRE_QUOTED_COSTED_SEND_TO_TP_FOR_WORK(
			"Item is pre-quoted and costed, send item to third party for work",
			"actionoutcome.tp_reqs.prequoted_costed_sendtp"),
	ANOTHER_BUSINESS_SUBDIVISION_NO_PO_REQUIRED(
			"Item requires work at another subdivision of our business company, no PO required",
			"actionoutcome.tp_reqs.businesssubdivision"),
	SUPPLIER_CONTRACT_NO_PO_REQUIRED_NO_RETURN(
			"Item is covered by a specific contract with the supplier, no supplier PO required, and no return back to Trescal",
			"actionoutcome.tp_reqs.suppliercontractnoporequirednoreturn"),
	SUPPLIER_CONTRACT_NO_PO_REQUIRED_WITH_RETURN(
			"Item is covered by a specific contract with the supplier, no supplier PO required, but will return back to Trescal",
			"suppliercontractnoporequiredwithreturn");

	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_TPRequirements(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.TP_REQUIREMENTS;
	}


}
