package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_AdjustmentOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_Adjustment;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;

@Component("Lookup_AdjustmentOutcome")
public class Lookup_AdjustmentOutcomeService extends Lookup {

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ADJUSTMENT OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage lr = null;
		ActionOutcome ao = null;
		for (JobItemAction action : ji.getActions()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;
				if (ActionOutcomeType.ADJUSTMENT.equals(activity.getActivity().getActionOutcomeType())) {
					ao = activity.getOutcome();
				}
			}
		}
		// By convention, all should be ActionOutcome_Adjustment, however not explicitly
		// enforced in DB
		if ((ao != null) && ActionOutcome_Adjustment.class.isAssignableFrom(ao.getClass())) {
			ActionOutcome_Adjustment ao_adjustment = (ActionOutcome_Adjustment) ao;
			switch (ao_adjustment.getValue()) {
			case UNIT_ADJUSTED:
				lr = Lookup_AdjustmentOutcome.ADJUSTED;
				break;
			case UNABLE_TO_ADJUST_BER:
				lr = Lookup_AdjustmentOutcome.UNIT_BER;
				break;
			case UNABLE_TO_ADJUST_REQUIRES_INHOUSE_REPAIR:
				lr = Lookup_AdjustmentOutcome.INHOUSE_REPAIR;
				break;
			case UNABLE_TO_ADJUST_REQUIRES_THIRD_PARTY_REPAIR:
				lr = Lookup_AdjustmentOutcome.THIRDPARTY_REPAIR;
				break;
			case NOT_SUCCESSFUL:
				lr = Lookup_AdjustmentOutcome.NOT_SUCCESSFUL;
				break;
			default:
			}
		}
		return lr;
	}

	@Override
	public String getDescription() {
		return "Determines the outcome of the item's most recent adjustment.";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_AdjustmentOutcome.values();
	}
}