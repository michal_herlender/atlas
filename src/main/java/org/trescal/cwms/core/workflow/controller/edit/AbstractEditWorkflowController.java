package org.trescal.cwms.core.workflow.controller.edit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupinstance.db.LookupInstanceService;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;
import org.trescal.cwms.core.workflow.form.edit.EditStateGroupLinkDto;
import org.trescal.cwms.spring.model.KeyValue;

public abstract class AbstractEditWorkflowController {

	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private LookupInstanceService lookupInstanceService;

	/**
	 * Creates a bindable map for the specified translations
	 */
	protected Map<Locale,String> getTranslationMap(Set<Translation> translations) {
		Map<Locale,String> result = new HashMap<>();
		for (Translation translation : translations) {
			result.put(translation.getLocale(), translation.getTranslation());
		}
		return result;
	}
	
	/**
	 * Created to prevent renaming of "hidden" values for input checkboxes with template rows
	 */
	protected List<KeyValue<Boolean, String>> getSelectBoolean() {
		List<KeyValue<Boolean, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(Boolean.FALSE, "No"));
		result.add(new KeyValue<>(Boolean.TRUE, "Yes"));
		return result;
	}
	
	/**
	 * Returns a list of the linked state groups as dtos for binding / editing
	 */
	protected List<EditStateGroupLinkDto> getStateGroupList(ItemState itemState) {
		return itemState.getGroupLinks().stream().
				map(sgl -> new EditStateGroupLinkDto(sgl)).
				collect(Collectors.toList());
	}
	
	/**
	 * Returns a key/value list of all item activities in system for user selection
	 * (maybe we should put those that are retired or inactive in their own option group?)
	 * Maybe different types should also be in their own selection group?
	 */
	protected List<KeyValue<Integer, String>> getItemActivities() {
		List<KeyValue<Integer, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(0, "-- No Item Activity Selected --"));
		addItemStates(itemStateService.getAllItemActivities(), result);
		return result;
	}
	
	/**
	 * Converts item state (activity or status) to DTO for UI selection and adds to list
	 */
	private void addItemStates(List<? extends ItemState> itemStates, List<KeyValue<Integer, String>> result) {
		for (ItemState is : itemStates) {
			String value = is.getStateid().toString()+" - "+is.getDescription();
			result.add(new KeyValue<Integer, String>(is.getStateid(), value));
		}
	}
	
	/**
	 * Returns a key/value list of all item activities in system for user selection
	 * (maybe we should put those that are retired or inactive in their own option group?)
	 */
	protected List<KeyValue<Integer, String>> getItemStatuses() {
		List<KeyValue<Integer, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(0, "-- No Item Status Selected --"));
		addItemStates(itemStateService.getAllItemStatuses(WorkStatus.class, true), result);
		addItemStates(itemStateService.getAllItemStatuses(HoldStatus.class, true), result);
		return result;
	}
	
	protected List<KeyValue<Integer, String>> getLookups() {
		List<KeyValue<Integer, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(0, "-- No Lookup Selected --"));
		for (LookupInstance lookup : this.lookupInstanceService.getAll()) {
			result.add(new KeyValue<>(lookup.getId(), lookup.getId()+" - "+lookup.getLookupFunction().name()));
		}
		return result;
	}
}