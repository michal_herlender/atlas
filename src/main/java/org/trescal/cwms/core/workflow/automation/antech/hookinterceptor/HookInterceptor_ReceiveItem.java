package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.TreeSet;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Service
public class HookInterceptor_ReceiveItem extends HookInterceptor<JobItem>
{
	public static final String HOOK_NAME = "receive-item";
	
	public JobItemTransit getPreviousTransit(JobItem ji)
	{
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());

		// loop backwards through actions
		for (JobItemAction action : actions.descendingSet())
		{
			// find where action is a transit
			if (action instanceof JobItemTransit)
			{
				// return the transit
				return (JobItemTransit) action;
			}
		}

		// cannot find the last transit
		return null;
	}

	@Transactional
	@Override
	public void recordAction(JobItem ji) throws OpenActivityException
	{
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive())
		{
			logger.debug("RUNNING HOOK: RECEIVE ITEM");
			Contact con = this.sessionServ.getCurrentContact();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			JobItemTransit jit = new JobItemTransit();
			jit.setActivity(context.getActivity());
			jit.setActivityDesc(context.getActivity().getDescription());
			jit.setStartStatus((ItemStatus) ji.getState());
			jit.setStartStamp(new Date());
			jit.setStartedBy(con);
			jit.setCreatedOn(new Date());
			jit.setJobItem(ji);

			// transit-specific fields
			JobItemTransit despatch = this.getPreviousTransit(ji);
			if (despatch != null) {
				for (StateGroupLink sgl : ji.getState().getGroupLinks())
				{
					if (sgl.getGroup().equals(StateGroup.BUSINESSTRANSIT))
					{
						jit.setFromAddr(despatch.getFromAddr());
						jit.setFromLoc(despatch.getFromLoc());
					}

					if (sgl.getGroup().equals(StateGroup.TPTRANSIT))
					{
						jit.setFromAddr(despatch.getToAddr());
						jit.setFromLoc(despatch.getToLoc());
					}
				}
			}
			else {
				logger.warn("No previous transit found for job item id "+ji.getJobItemId());
			}

			jit.setToAddr(ji.getCurrentAddr());
			jit.setToLoc(ji.getCurrentLoc());
			jit.setTransitDate(new Date());

			if (context.isCompleteActivity())
			{
				jit.setEndStamp(new Date());
				jit.setTimeSpent(5);
			}

			// adds the action into the job item in the session
			if (ji.getActions() == null)
			{
				ji.setActions(new TreeSet<JobItemAction>(new JobItemActionComparator()));
			}
			ji.getActions().add(jit);

			ItemStatus endStatus = super.getNextStatus(context);
			ji.setState(endStatus);
			jobItemService.updateJobItem(ji);
			if (context.isCompleteActivity())
			{
				jit.setEndStatus(endStatus);
			}

			this.persist(jit, endStatus, context);
		}
	}
}