package org.trescal.cwms.core.workflow.controller;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;

@Controller
@JsonController
public class EngineerAllocationController {

    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private TransportOptionService transportOptService;
    @Autowired
    private CapabilityService procService;

    @RequestMapping(value = "allocateitemtoengineer.htm", method = RequestMethod.GET)
    public ModelAndView allocateItemToEngineer(@RequestParam(name = "jobItemId", required = true) Integer jobItemId,
                                               @RequestParam(name = "deptId", required = true) Integer deptId, Locale locale) {

        Map<String, Object> referenceData = new HashMap<String, Object>();

        Integer transportOptionId = this.transportOptService.getTransportOptionByJobItemId(jobItemId);

        if (transportOptionId != null) {
			LocalDate date = this.transportOptService.getNextAvailable(transportOptionId, null);
			referenceData.put("nextAvialableTransportDate", date);
		}

		JobItemProjectionDTO dto = this.jobItemService.getJobItemProjectionDTO(jobItemId, locale);

		referenceData.put("availableDays", getAvailableDays());
		referenceData.put("timesOfDay", TimeOfDay.values());
		referenceData.put("deptId", deptId);
		referenceData.put("dto", dto);
		referenceData.put("proc", this.procService.get(dto.getCapabilityId()));

		return new ModelAndView("trescal/core/workflow/allocateitemtoengineer_overlay", referenceData);
	}

	private List<LocalDate> getAvailableDays() {
		List<LocalDate> availableDays = new ArrayList<>();
		LocalDate date = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		for (int i = 0; i < Constants.FUTURE_ALLOCATION_DAYS; i++) {
			if (!date.getDayOfWeek().equals(DayOfWeek.SATURDAY) &&
				!date.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
				availableDays.add(date);
			}
			date = date.plusDays(1);
		}
		return availableDays;
	}
}