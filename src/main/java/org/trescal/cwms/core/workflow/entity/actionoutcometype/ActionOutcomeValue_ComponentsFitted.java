package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_ComponentsFitted implements ActionOutcomeValue {
	REQUIRES_FURTHER_REPAIR("Requires further repair","actionoutcome.componentsfitted.furtherrepair"),
	REPAIRED_REQUIRES_FURTHER_CALIBRATION("Repaired, requires further calibration","actionoutcome.componentsfitted.furthercal"),
	REPAIRED_REQUIRES_NO_FURTHER_CALIBRATION("Repaired, requires no further calibration","actionoutcome.componentsfitted.nofurthercal");
	
	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_ComponentsFitted(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.COMPONENTS_FITTED;
	}

}
