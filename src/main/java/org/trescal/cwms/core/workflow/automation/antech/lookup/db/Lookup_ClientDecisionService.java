package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientDecision;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionCosting;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Component("Lookup_ClientDecision")
public class Lookup_ClientDecisionService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the outcome of the most recent client decision on adjustment costing.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: CLIENT DECISION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions
		activityLoop: for (JobItemAction action : actions.descendingSet()) {
			// in some legacy cases, activity may be null!
			if (action.getActivity() != null) {
				// find latest client decision
				for (StateGroupLink gl : action.getActivity().getGroupLinks()) {
					if (gl.getGroup().equals(StateGroup.DECISION) && !action.isDeleted()) {
						if (action instanceof JobItemActivity) {
							// get the outcome
							JobItemActivity temp = (JobItemActivity) action;
							ao = temp.getOutcome();
							break activityLoop;
						}
					}
				}
			}
		}
		
		LookupResultMessage result;
		if(ActionOutcomeValue_ClientDecisionCosting.APPROVED.equals(ao.getGenericValue()))
			result = Lookup_ClientDecision.POSITIVE;
		else if(ActionOutcomeValue_ClientDecisionCosting.REJECTED.equals(ao.getGenericValue()))
			result = Lookup_ClientDecision.NEGATIVE;
		else
			result = Lookup_ClientDecision.NEGATIVE_BACK_TOAWAITINGCOSTING;
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ClientDecision.values();
	}
}