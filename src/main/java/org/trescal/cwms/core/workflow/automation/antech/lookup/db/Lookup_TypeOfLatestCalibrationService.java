package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TypeOfLatestCalibration;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Component("Lookup_TypeOfLatestCalibration")
public class Lookup_TypeOfLatestCalibrationService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the latest calibration for an item was pre-calibration or post-calibration.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: TYPE OF LATEST CALIBRATION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		JobItemActivity latestCal = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions to get the MOST RECENT calibration
		// in case there is more than one
		activityLoop: for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity jia = (JobItemActivity) action;

				ItemActivity activity = this.itemStateService.findItemActivity(jia.getActivity().getStateid());

				for (StateGroupLink sgl : activity.getGroupLinks()) {
					if (sgl.getGroup().equals(StateGroup.CALIBRATION) && !jia.isDeleted()) {
						latestCal = jia;
						break activityLoop;
					}
				}
			}
		}
		if (latestCal == null) {
			result = Lookup_TypeOfLatestCalibration.PRE_CALIBRATION;
		} else {
			if (latestCal.getActivityDesc().toLowerCase().contains("post")) {
				result = Lookup_TypeOfLatestCalibration.POST_CALIBRATION;
			} else {
				result = Lookup_TypeOfLatestCalibration.PRE_CALIBRATION;
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_TypeOfLatestCalibration.values();
	}
}