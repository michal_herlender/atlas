package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemHasFaultReport;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemHasFaultReport")
public class Lookup_ItemHasFaultReportService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public String getDescription() {
		return "Determines whether an item has a fault report.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM HAS FAULT REPORT");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), false);
		LookupResultMessage result = fr == null ? Lookup_ItemHasFaultReport.NO : Lookup_ItemHasFaultReport.YES;
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemHasFaultReport.values();
	}
}