package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Calendar;
import java.util.Date;

@Service
public class HookInterceptor_RecordProductionRepairTime extends HookInterceptor<HookInterceptor_RecordProductionRepairTime.Input> {

	public static final String HOOK_NAME = "record-production-repair-time";

	public static class Input {
		public JobItem ji;
		public String rcrIdentifier;
		public Integer timespent;
		public Contact contact;
		public Date startDate;
		public String comment;
		private String operationName;

		public Input(JobItem ji, String rcrIdentifier, Integer timespent, Contact contact, Date startDate, String comment, String operationName) {
			this.ji = ji;
			this.rcrIdentifier = rcrIdentifier;
			this.timespent = timespent;
			this.contact = contact;
			this.startDate = startDate;
			this.comment = comment;
			this.operationName = operationName;
		}
	}
	
	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: RECORD PRODUCTION REPAIR TIME");

					Contact con = input.contact;
					JobItem ji = input.ji;
					HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

					JobItemActivity jia = new JobItemActivity();
					jia.setActivity(context.getActivity());
					jia.setActivityDesc(context.getActivity().getDescription());
					jia.setStartStatus((ItemStatus) ji.getState());
					jia.setStartStamp(input.startDate);
					jia.setStartedBy(con);
					jia.setCreatedOn(new Date());
					jia.setJobItem(ji);
					jia.setComplete(context.isCompleteActivity());
					jia.setRemark(input.rcrIdentifier.concat("<br/>").concat(input.operationName));
					if(StringUtils.isNotBlank(input.comment))
						jia.setRemark(jia.getRemark().concat("<br/>").concat(input.comment));

					if (context.isCompleteActivity()) {
						Calendar endDate = Calendar.getInstance();
						endDate.setTime(input.startDate);
						endDate.add(Calendar.MINUTE, input.timespent);
						jia.setEndStamp(endDate.getTime());
						jia.setTimeSpent(input.timespent);
						jia.setCompletedBy(con);
					}

					if (context.isCompleteActivity()) {
						jia.setEndStatus((ItemStatus) ji.getState());
					}

					this.persist(jia, ji.getState(), context);
		}
	}
}