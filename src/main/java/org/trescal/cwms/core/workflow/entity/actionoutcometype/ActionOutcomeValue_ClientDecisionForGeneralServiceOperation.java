package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_ClientDecisionForGeneralServiceOperation implements ActionOutcomeValue {
	ACCEPTED(
			"General service operation accepted",
			"actionoutcome.cd_generalserviceoperation.accepted"),
	REJECTED(
			"General service operation rejected",
			"actionoutcome.cd_generalserviceoperation.rejected"),
	REJECTED_REDO(
			"General service operation rejected redo",
			"actionoutcome.cd_generalserviceoperation.rejectedredo");
	
	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_ClientDecisionForGeneralServiceOperation(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.CLIENT_DECISION_GENERAL_SERVICE_OPERATION;
	}

}
