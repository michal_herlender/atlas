package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_AdditionalDemandsCompleted;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_AdditionalDemandsCompleted")
public class Lookup_AdditionalDemandsCompletedService extends Lookup {

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ADDITIONAL DEMANDS COMPLETED");
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		if(jobItem.getAdditionalDemands() != null){
			return jobItem.getAdditionalDemands().stream().allMatch(jid -> jid.getAdditionalDemandStatus().isCompleted())
					? Lookup_AdditionalDemandsCompleted.COMPLETED
					: Lookup_AdditionalDemandsCompleted.NOT_COMPLETED;
		}
		return Lookup_AdditionalDemandsCompleted.COMPLETED;
	}

	@Override
	public String getDescription() {
		return "Determines, whether all additional demands are completed";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_AdditionalDemandsCompleted.values();
	}
}