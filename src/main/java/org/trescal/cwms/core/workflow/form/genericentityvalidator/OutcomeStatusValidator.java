package org.trescal.cwms.core.workflow.form.genericentityvalidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractEntityValidator;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;
import org.trescal.cwms.core.workflow.entity.outcomestatus.db.OutcomeStatusService;

@Component("OutcomeStatusValidator")
public class OutcomeStatusValidator extends AbstractEntityValidator
{
	@Autowired
	BeanValidator beanValidator;
	@Autowired
	private OutcomeStatusService outcomeServ;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(OutcomeStatus.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		OutcomeStatus os = (OutcomeStatus) target;
		if (errors == null) errors = new BindException(os, "os");
		beanValidator.validate(os, errors);
		// check this status is not already an outcome status for this activity
		if (this.outcomeServ.findOutcomeStatusWithActivityAndStatus(os.getActivity().getStateid(), os.getStatus().getStateid()) != null)
		{
			errors.rejectValue("status", null, "This status is already a possible outcome from this activity.");
		}
		// make the errors available outside of this function
		this.errors = (BindException) errors;
	}
}