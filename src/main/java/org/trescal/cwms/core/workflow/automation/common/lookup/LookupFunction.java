package org.trescal.cwms.core.workflow.automation.common.lookup;

import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_AdditionalDemandsCompleted;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_AdjustmentOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_AsFoundCertificates;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_COSHHFormRequired;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CalibrationInspectionOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CertIssuedFromCal;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CertificateIsSigned;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CleaningRequired;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientDecision;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientFailureReportOutcomePreselected;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientResponseRequiredOnFaultReport;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CompanyIsOnStop;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ComponentsFittedOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ConfirmationDestinationReceiptRequired;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ConfirmationCostRequired;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ContractReviewOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CostsWithinApprovedLimit;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_DNCreationApprovalRequired;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_JobItemAtInstrumentAddress;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FaultReportAwaitingUpdate;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FaultReportIsApprovedByClient;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FaultReportIsTobeSent;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FaultReportsRequireDelivery;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FurtherTPWorkClientResponse;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_GeneralServiceOperationResult;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_InitialWorkRequired;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_IsOnRepair;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtCalibrationLocation;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtDespatchLocation;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtFirstLocation;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtInspectionLocation;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtTP;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemCalibratedAtTP;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemHasBeenCalibrated;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemHasBeenContractReviewed;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemHasFaultReport;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemIsOrificePlate;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemQuotedBypassCosting;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemRequiresFurtherCalibration;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_JobIsOnSite;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_LatestCalibrationOrInspectionOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_LatestFaultReportOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NeedCostingToClientForRepair;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NeedRepairCompletionReportValidation;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NeedRepairInspectionReport;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NeedSpareParts;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NextWorkRequirementByJobCompany;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_PostCalibration;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_PostTPInspectionOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_PreDeliveryRequirements;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_RepairOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TPCalibrationOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TPItemReturned;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TPRequirements;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TypeOfLatestCalibration;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TypeOfNextWorkRequirement;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_UnitHasBeenExchanged;
import org.trescal.cwms.core.workflow.automation.antech.lookup.db.Lookup_ItemWasAtTPForRepairs;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemRepairedAtTP;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NoOperationPerformedOutcome;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_HasDocumentForMostRecentGeneralOperation;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientDecisionRequiredForGeneralServiceOperation;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_LastDeliveryType;


public enum LookupFunction {
	// 0 to 4
	ADJUSTMENT_OUTCOME(Lookup_AdjustmentOutcome.values()),
	AS_FOUND_CERTIFICATES(Lookup_AsFoundCertificates.values()),
	CALIBRATION_INSPECTION_OUTCOME(Lookup_CalibrationInspectionOutcome.values()),
	CERTIFICATE_IS_SIGNED(Lookup_CertificateIsSigned.values()),
	CERTIFICATE_ISSUED_FROM_CAL(Lookup_CertIssuedFromCal.values()),
	// 5 to 9
	CLEANING_REQUIRED(Lookup_CleaningRequired.values()),
	CLIENT_DECISION(Lookup_ClientDecision.values()),
	COMPANY_IS_ON_STOP(Lookup_CompanyIsOnStop.values()),
	COMPONENTS_FITTED_OUTCOME(Lookup_ComponentsFittedOutcome.values()),
	CONFIRMATION_COST_REQUIRED(Lookup_ConfirmationCostRequired.values()),
	// 10 to 14
	CONTRACT_REVIEW_OUTCOME(Lookup_ContractReviewOutcome.values()),
	COSHH_FORM_REQUIRED(Lookup_COSHHFormRequired.values()),
	COSTS_WITHIN_APPROVED_LIMIT(Lookup_CostsWithinApprovedLimit.values()),
	DESPATCH_APPROVAL_REQUIRED(Lookup_JobItemAtInstrumentAddress.values()),
	FAULT_REPORT_AWAITING_UPDATE(Lookup_FaultReportAwaitingUpdate.values()),
	// 15 to 19
	FURTHER_TP_WORK_CLIENT_RESPONSE(Lookup_FurtherTPWorkClientResponse.values()),
	INITIAL_WORK_REQUIRED(Lookup_InitialWorkRequired.values()),
	ITEM_AT_CALIBRATION_LOCATION(Lookup_ItemAtCalibrationLocation.values()),
	ITEM_AT_DESPATCH_LOCATION(Lookup_ItemAtDespatchLocation.values()),
	ITEM_AT_FIRST_LOCATION(Lookup_ItemAtFirstLocation.values()),
	// 20 to 24
	ITEM_AT_INSPECTION_LOCATION(Lookup_ItemAtInspectionLocation.values()),
	ITEM_AT_THIRD_PARTY(Lookup_ItemAtTP.values()),
	ITEM_CALIBRATED_AT_THIRD_PARTY(Lookup_ItemCalibratedAtTP.values()),
	ITEM_HAS_BEEN_CALIBRATED(Lookup_ItemHasBeenCalibrated.values()),
	ITEM_HAS_BEEN_CONTRACT_REVIEWED(Lookup_ItemHasBeenContractReviewed.values()),
	// 25 to 29
	ITEM_HAS_BEEN_QUOTED(Lookup_ItemHasBeenQuotedOrApprovedToBypassCosting.values()),
	ITEM_HAS_FAULT_REPORT(Lookup_ItemHasFaultReport.values()),
	ITEM_IS_ORIFICE_PLATE(Lookup_ItemIsOrificePlate.values()),
	ITEM_REQUIRES_FURTHER_CALIBRATION(Lookup_ItemRequiresFurtherCalibration.values()),
	LATEST_CALIBRATION_OR_INSPECTION_OUTCOME(Lookup_LatestCalibrationOrInspectionOutcome.values()),
	// 30 to 34
	POST_CALIBRATION(Lookup_PostCalibration.values()),
	POST_TP_INSPECTION_OUTCOME(Lookup_PostTPInspectionOutcome.values()),
	REPAIR_OUTCOME(Lookup_RepairOutcome.values()),
	TP_CALIBRATION_OUTCOME(Lookup_TPCalibrationOutcome.values()),
	TP_ITEM_RETURNED(Lookup_TPItemReturned.values()),
	// 35 to 39
	TP_REQUIREMENTS(Lookup_TPRequirements.values()),
	TYPE_OF_LATEST_CALIBRATION(Lookup_TypeOfLatestCalibration.values()),
	UNIT_HAS_BEEN_EXCHANGED(Lookup_UnitHasBeenExchanged.values()),
	JOB_IS_ONSITE(Lookup_JobIsOnSite.values()),
	PRE_DELIVERY_REQUIREMENTS(Lookup_PreDeliveryRequirements.values()),
	// 40 to 45
	NEXT_WORK_REQUIREMENT_AT_JOB_COMPANY(Lookup_NextWorkRequirementByJobCompany.values()),
	FAULTREPORT_IS_TO_BE_SENT(Lookup_FaultReportIsTobeSent.values()),
	CLIENT_ADVICE_NEEDED_FOR_FAULT_REPORT(Lookup_ClientResponseRequiredOnFaultReport.values()),
	LATEST_FAULT_REPORT_OUTCOME(Lookup_LatestFaultReportOutcome.values()),
	FAULT_REPORT_IS_APPROVED_BY_CLIENT(Lookup_FaultReportIsApprovedByClient.values()),
	FAULT_REPORT_OUTCOME_SELECTED(Lookup_ClientFailureReportOutcomePreselected.values()),
	// 46 to 50
	ADDITIONAL_DEMANDS_COMPLETED(Lookup_AdditionalDemandsCompleted.values()),
	JOBITEM_IS_ON_REPAIR(Lookup_IsOnRepair.values()),
	TYPE_OF_NEXT_WORK_REQUIREMENT(Lookup_TypeOfNextWorkRequirement.values()),
	NEED_REPAIR_INSPECTION_REPORT(Lookup_NeedRepairInspectionReport.values()),
	NEED_COSTING_TOCLIENT_FORREPAIR(Lookup_NeedCostingToClientForRepair.values()),
	// 51 to 55
	NEED_REPAIR_COMPLETION_REPORT(Lookup_NeedRepairCompletionReportValidation.values()),
	NEED_SPARE_PARTS(Lookup_NeedSpareParts.values()),
	ITEM_WAS_AT_TP_FOR_REPAIRS(Lookup_ItemWasAtTPForRepairs.values()),
	GENERAL_OPERATION_RESULT(Lookup_GeneralServiceOperationResult.values()),
	FAILURE_REPORTS_REQUIRE_DELIVERY(Lookup_FaultReportsRequireDelivery.values()),
	// 56 to 60
	ITEM_REPAIRED_AT_THIRD_PARTY(Lookup_ItemRepairedAtTP.values()),
	NO_OPERATION_PERFORMED_OUTCOME(Lookup_NoOperationPerformedOutcome.values()),
	ITEM_QUOTED_BYPASS_COSTING(Lookup_ItemQuotedBypassCosting.values()),
	ITEM_HAS_GSO_DOCUMENT(Lookup_HasDocumentForMostRecentGeneralOperation.values()),
	CLIENT_DECISION_REQUIRED_FOR_GSO(Lookup_ClientDecisionRequiredForGeneralServiceOperation.values()),
	// 61
	DN_CREATION_REQUIRES_APPROVAL(Lookup_DNCreationApprovalRequired.values()),
	// 62
	DN_DESTINATION_RECEIPT_DATE(Lookup_ConfirmationDestinationReceiptRequired.values()),
	// 63
	LAST_JOBITEM_DELIVERY_TYPE(Lookup_LastDeliveryType.values());

	private LookupResultMessage[] resultMessages;

	private LookupFunction(LookupResultMessage[] resultMessages) {
		this.resultMessages = resultMessages;
	}

	public LookupResultMessage[] getLookupResultMessages() {
		return resultMessages;
	}

	public String getBeanName() {
		return getLookupResultMessages()[0].getClass().getSimpleName();
	}
}