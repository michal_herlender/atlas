package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_PostTPInspectionOutcome implements LookupResultMessage
{
	NO_FURTHER_WORK_SWITCH_TO_NEXT_WORK("lookupresultmessage.posttpinspectionoutcome.nofurtherworkswitchtonextwork"),
	FURTHER_WORK_NECESSARY_AWAITING_SELECTION_OF_NEXT_WR("lookupresultmessage.posttpinspectionoutcome.furtherworknextwr"),
	WORK_NOT_COMPLETED_RETURN_TO_SUPPLIER("lookupresultmessage.posttpinspectionoutcome.notcompleted"),
	WORK_UNSUCCESSFUL_AWAITING_FR_UPDATE("lookupresultmessage.posttpinspectionoutcome.unsuccessful");
	
	private String messageCode;
	
	private Lookup_PostTPInspectionOutcome(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}