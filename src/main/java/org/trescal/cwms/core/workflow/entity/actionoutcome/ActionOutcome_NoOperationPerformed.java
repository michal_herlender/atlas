package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_NoOperationPerformed;

@Entity
@DiscriminatorValue("no_operation_performed")
public class ActionOutcome_NoOperationPerformed extends ActionOutcome {
	private ActionOutcomeValue_NoOperationPerformed value;
	
	public ActionOutcome_NoOperationPerformed() {
		this.type = ActionOutcomeType.NO_OPERATION_PERFORMED;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_NoOperationPerformed getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_NoOperationPerformed value) {
		this.value = value;
	}

}
