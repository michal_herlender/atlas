package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemHasBeenCalibrated;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Component("Lookup_ItemHasBeenCalibrated")
public class Lookup_ItemHasBeenCalibratedService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item has been calibrated or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM HAS BEEN CALIBRATED");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = Lookup_ItemHasBeenCalibrated.NO;
		activityLoop: for (JobItemAction action : ji.getActions()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;

				if (activity.isComplete()) {
					for (StateGroupLink sgl : activity.getActivity().getGroupLinks()) {
						if (sgl.getGroup().equals(StateGroup.CALIBRATION) && !activity.isDeleted()) {
							result = Lookup_ItemHasBeenCalibrated.YES;
							break activityLoop;
						}
					}
				}
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemHasBeenCalibrated.values();
	}
}