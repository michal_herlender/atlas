package org.trescal.cwms.core.workflow.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.dto.JobItemsInStateDTO;
import org.trescal.cwms.core.workflow.dto.JobItemsInStateGroupDTO;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.db.StateGroupLinkService;
import org.trescal.cwms.core.workflow.entity.workassignment.db.WorkAssignmentService;

@Controller
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JobItemDashboardController {

	@Autowired
	private StateGroupLinkService stateGroupLinkService;
	@Autowired
	private WorkAssignmentService workAssignmentService;

	@RequestMapping(value = "/jobitemdashboard.htm", method = RequestMethod.GET)
	public ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto, Locale locale) {
		Map<String, Object> model = new HashMap<String, Object>();
		Map<DepartmentType, List<JobItemsInStateGroupDTO>> workByDepartment = new HashMap<DepartmentType, List<JobItemsInStateGroupDTO>>();
		for (DepartmentType departmentType : DepartmentType.values()) {
			if (departmentType.isActive() && !departmentType.isProcedures()) {
				List<JobItemsInStateGroupDTO> departmentStateGroups = new ArrayList<JobItemsInStateGroupDTO>();
				List<StateGroup> stateGroups = workAssignmentService.getStateGroupsAssignedToDeptType(departmentType);
				for (StateGroup stateGroup : stateGroups) {
					List<JobItemsInStateDTO> jobItemsInStates = stateGroupLinkService.getJobItemsInStateGroup(
							stateGroup, companyDto.getKey(), subdivDto.getKey(), LocaleContextHolder.getLocale());
					departmentStateGroups.add(JobItemsInStateGroupDTO.of(stateGroup.getId(),
							stateGroup.getDisplayName(), jobItemsInStates));
				}
				workByDepartment.put(departmentType, departmentStateGroups);
			}
		}
		model.put("workByDepartment", workByDepartment);
		return new ModelAndView("/trescal/core/workflow/jobitemdashboard", model);
	}
}