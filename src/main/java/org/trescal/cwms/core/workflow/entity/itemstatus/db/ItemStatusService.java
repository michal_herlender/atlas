package org.trescal.cwms.core.workflow.entity.itemstatus.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

public interface ItemStatusService extends BaseService<ItemStatus, Integer> {
	List<ItemActivity> findActivities();

	ItemStatus findItemStatus(Integer id);

	List<ItemStatus> findStatuses();

	List<ItemStatus> getAllItemStatuses();

	void insertItemStatus(ItemStatus i);

	List<ItemStatus> searchItemStatus(String description);

	void updateItemStatus(ItemStatus i);
}
