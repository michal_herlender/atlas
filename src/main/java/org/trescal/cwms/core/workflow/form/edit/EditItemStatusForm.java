package org.trescal.cwms.core.workflow.form.edit;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class EditItemStatusForm extends AbstractItemStateForm {
	private Boolean workStatus;							// If true = WorkStatus, false = HoldStatus
	private List<EditNextActivityDto> nextActivities;
	private Integer defaultActivityId;					// Default id for new activity entry (used in binding)
	private Boolean defaultManualEntryAllowed;			// Default selection for new activity entry (used in binding)
	
	@Valid
	public List<EditNextActivityDto> getNextActivities() {
		return nextActivities;
	}
	@NotNull
	public Boolean getWorkStatus() {
		return workStatus;
	}
	@NotNull
	public Integer getDefaultActivityId() {
		return defaultActivityId;
	}
	@NotNull
	public Boolean getDefaultManualEntryAllowed() {
		return defaultManualEntryAllowed;
	}
	
	public void setNextActivities(List<EditNextActivityDto> nextActivities) {
		this.nextActivities = nextActivities;
	}
	public void setWorkStatus(Boolean workStatus) {
		this.workStatus = workStatus;
	}
	public void setDefaultActivityId(Integer defaultActivityId) {
		this.defaultActivityId = defaultActivityId;
	}
	public void setDefaultManualEntryAllowed(Boolean defaultManualEntryAllowed) {
		this.defaultManualEntryAllowed = defaultManualEntryAllowed;
	}
}
