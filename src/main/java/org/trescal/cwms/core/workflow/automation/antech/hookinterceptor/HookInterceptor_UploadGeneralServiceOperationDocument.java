package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_UploadGeneralServiceOperationDocument.Input;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.Locale;
import java.util.TreeSet;

@Service
public class HookInterceptor_UploadGeneralServiceOperationDocument extends HookInterceptor<Input> {

	public static final String HOOK_NAME = "upload-general-service-operation-document";

	@Autowired
	private MessageSource messageSource;

	public static class Input {
		public GsoDocument gsoDoc;
		public String fileName;

		public Input(GsoDocument gsoDoc, String fileName) {
			super();
			this.gsoDoc = gsoDoc;
			this.fileName = fileName;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: UPLOAD GENERAL SERVICE OPERATION DOCUMENT");
			Contact con = this.sessionServ.getCurrentContact();
			for (GsoDocumentLink link : input.gsoDoc.getLinks()) {
					JobItem ji = link.getJi();
					HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

					JobItemActivity jia = new JobItemActivity();
					jia.setActivity(context.getActivity());
					jia.setActivityDesc(context.getActivity().getDescription());
					jia.setStartStatus((ItemStatus) ji.getState());
					jia.setStartStamp(new Date());
					jia.setStartedBy(con);
					jia.setCreatedOn(new Date());
					jia.setJobItem(ji);
					jia.setComplete(context.isCompleteActivity());
					Locale messageLocale = ji.getJob().getOrganisation().getComp().getDocumentLanguage() != null
							? ji.getJob().getOrganisation().getComp().getDocumentLanguage()
							: con.getLocale();
					jia.setRemark(this.messageSource.getMessage("file", null, "File", messageLocale) + " : "
							+ input.fileName);

					if (context.isCompleteActivity()) {
						jia.setTimeSpent(5);
						jia.setEndStamp(DateUtils.addMinutes(jia.getStartStamp(), jia.getTimeSpent()));
						jia.setCompletedBy(con);
					}

					// adds the action into the job item in the session
					if (ji.getActions() == null) {
						ji.setActions(new TreeSet<JobItemAction>(new JobItemActionComparator()));
					}
					ji.getActions().add(jia);

					ItemStatus endStatus = super.getNextStatus(context);
					ji.setState(endStatus);

					jobItemService.updateJobItem(ji);

					if (context.isCompleteActivity()) {
						jia.setEndStatus(endStatus);
					}

					this.persist(jia, endStatus, context);
			}
		}
	}

}
