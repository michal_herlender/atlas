package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.Locale;

@Service
public class HookInterceptor_CreateFaultReport extends HookInterceptor<HookInterceptor_CreateFaultReport.Input> {
	public static final String HOOK_NAME = "create-fault-report";

	@Autowired
	private MessageSource messageSource;

	public static class Input {
		public FaultReport fr;
		public String remark;
		public Integer timeSpent;
		public Contact contact;
		public Date startStamp;
		public Date endStamp;

		public Input(FaultReport fr) {
			this.fr = fr;
		}

		public Input(FaultReport fr, Integer timeSpent, String remark, Contact contact, Date startStamp,
				Date endStamp) {
			this.fr = fr;
			this.timeSpent = timeSpent;
			this.remark = remark;
			this.contact = contact;
			this.startStamp = startStamp;
			this.endStamp = endStamp;
		}

	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		FaultReport fr = input.fr;

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: CREATE FAULT REPORT");
			Contact con = input.contact != null ? input.contact : this.sessionServ.getCurrentContact();
			JobItem ji = fr.getJobItem();

			// Only log fault report action if item state is not an activity
			if (ji.getState().isStatus()) {
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				if (input.remark == null)
					jia.setActivityDesc(context.getActivity().getDescription());
				else
					jia.setActivityDesc(input.remark);
				jia.setStartStatus((ItemStatus) ji.getState());
				if (input.startStamp == null)
					jia.setStartStamp(new Date());
				else
					jia.setStartStamp(input.startStamp);
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());
				Locale messageLocale = ji.getJob().getOrganisation().getComp().getDocumentLanguage() != null
						? ji.getJob().getOrganisation().getComp().getDocumentLanguage()
						: con.getLocale();
				jia.setRemark(this.messageSource.getMessage("jifaultreport.faultreport", null, "Failure Report : ",
						messageLocale) + " : " + fr.getFaultReportNumber());
				if (context.isCompleteActivity()) {
					if (input.timeSpent == null)
						jia.setTimeSpent(5);
					else
						jia.setTimeSpent(input.timeSpent);
					if (input.endStamp != null)
						jia.setEndStamp(input.endStamp);
					else if (input.timeSpent != null)
						jia.setEndStamp(DateUtils.addMinutes(jia.getStartStamp(), input.timeSpent));
					else
						jia.setEndStamp(new Date());

					jia.setCompletedBy(con);
				}
				ItemStatus endStatus = super.getNextStatus(context);
				ji.setState(endStatus);
				jobItemService.mergeJobItem(ji);
				if (context.isCompleteActivity()) {
					jia.setEndStatus(endStatus);
				}

				this.persist(jia, endStatus, context);
			}
		}
	}
}