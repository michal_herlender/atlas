package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_Calibration;

public enum ActionOutcomeValue_Calibration implements ActionOutcomeValue {
	SUCCESSFUL("Calibrated successfully","actionoutcome.calibration.success"),
	IS_BER("Unit B.E.R.","actionoutcome.calibration.failure_ber"),
	REQUIRES_THIRD_PARTY_CALIBRATION("Unit requires third party calibration",
			"actionoutcome.calibration.failure_tpcal"),
	REQUIRES_THIRD_PARTY_REPAIR("Unit requires third party repair",
			"actionoutcome.calibration.failure_tprepair"),
	REQUIRES_INHOUSE_REPAIR("Unit requires in-house repair",
			"actionoutcome.calibration.failure_inhouserepair"),
	CALIBRATED_REQUIRES_ADJUSTMENT_PRE_APPROVED("Calibrated - requires adjustment (pre-approved)",
			"actionoutcome.calibration.success_adjustment_approved"),
	CALIBRATED_REQUIRES_ADJUSTMENT_NOT_YET_APPROVED("Calibrated - requires adjustment (not yet approved)",
			"actionoutcome.calibration.success_adjustment_notapproved"),
	CALIBRATED_REQUIRES_THIRD_PARTY_REPAIR("Calibrated - requires third party repair",
			"actionoutcome.calibration.success_tprepair"),
	CALIBRATED_REQUIRES_INHOUSE_REPAIR("Calibrated - requires in-house repair",
			"actionoutcome.calibration.success_inhouserepair"),
	ON_HOLD("Calibration placed on hold",
			"actionoutcome.calibration.onhold"),
	NOT_SUCCESSFUL("Not Successful"
			,"actionoutcome.calibration.notsuccessful");
	
	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_Calibration(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.CALIBRATION;
	}

}
