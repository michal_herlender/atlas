package org.trescal.cwms.core.workflow.entity.actionoutcome.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome_;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;

@Repository("ActionOutcomeDao")
public class ActionOutcomeDaoImpl extends BaseDaoImpl<ActionOutcome, Integer> implements ActionOutcomeDao {

	@Override
	protected Class<ActionOutcome> getEntity() {
		return ActionOutcome.class;
	}

	@Override
	public ActionOutcome findDefaultActionOutcomeForActivity(int activityId) {
		return getSingleResult(cb -> {
			CriteriaQuery<ActionOutcome> cq = cb.createQuery(ActionOutcome.class);
			Root<ActionOutcome> actionOutcome = cq.from(ActionOutcome.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(actionOutcome.get(ActionOutcome_.defaultOutcome)));
			clauses.getExpressions().add(cb.isTrue(actionOutcome.get(ActionOutcome_.active)));
			clauses.getExpressions().add(cb.equal(actionOutcome.get(ActionOutcome_.activity), activityId));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public <T extends ActionOutcome> List<T> getByTypeAndValue(Class<T> clazz, ActionOutcomeValue value) {
		return getResultList(cb -> {
			CriteriaQuery<T> cq = cb.createQuery(clazz);
			Root<T> root = cq.from(clazz);
			if (value != null)
				cq.where(cb.equal(root.get("value"), value));
			cq.select(root);
			return cq;
		});
	}
}