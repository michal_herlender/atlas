package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_CertIssuedFromCal implements LookupResultMessage
{
	WAS_ISSUED("lookupresultmessage.certissuedfromcal.wasissued"),
	NOT_ISSUED("lookupresultmessage.certissuedfromcal.notissued");
	
	private String messageCode;
	
	private Lookup_CertIssuedFromCal(String messageCode) {
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}