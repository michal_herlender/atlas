package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_TypeOfNextWorkRequirement implements LookupResultMessage {

	ADJUSTMENT("lookupresultmessage.typeofnextworkrequirement.adjustment"),
	CALIBRATION("lookupresultmessage.typeofnextworkrequirement.calibration"),
	REPAIR("lookupresultmessage.typeofnextworkrequirement.repair"),
	THIRD_PARTY("lookupresultmessage.typeofnextworkrequirement.thirdparty"),
	ONSITE_CALIBRATION("lookupresultmessage.typeofnextworkrequirement.onsitecalibration"),
	GENERAL_SERVICE_OPERATION("lookupresultmessage.typeofnextworkrequirement.generalserviceoperation");

	private String messageCode;

	private Lookup_TypeOfNextWorkRequirement(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}

}
