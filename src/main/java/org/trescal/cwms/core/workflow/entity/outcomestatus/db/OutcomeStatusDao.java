package org.trescal.cwms.core.workflow.entity.outcomestatus.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

public interface OutcomeStatusDao extends BaseDao<OutcomeStatus, Integer> {
	
	OutcomeStatus findOutcomeStatusWithActivityAndStatus(int activityId, int statusId);
	
	List<OutcomeStatus> getOutcomeStatusesForActivity(int activityId, boolean includeLookups);
}