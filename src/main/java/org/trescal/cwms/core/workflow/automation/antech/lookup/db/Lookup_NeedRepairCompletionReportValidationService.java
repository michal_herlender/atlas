package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.db.RepairCompletionReportService;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_NeedRepairCompletionReportValidation;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_NeedRepairCompletionReportValidation")
public class Lookup_NeedRepairCompletionReportValidationService extends Lookup {

	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private RepairCompletionReportService rcrService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Need Repair Completion Report");

		JobItem ji = jobItemService.findJobItem(jobItemId);
		RepairCompletionReport rcr = rcrService.getByJobitem(jobItemId);

		// added to deal the certificate cancel and replace case
		// when the item is at client address (CurrentAddr == null)
		if(ji.getCurrentAddr() == null)
			return Lookup_NeedRepairCompletionReportValidation.NO;
		// is the jobitem currently at original subdiv : to be able to only
		// validate the RCR in the original subdiv
		Subdiv allocatedSubdiv = ji.getJob().getOrganisation();
		Subdiv currentSubdiv = ji.getCurrentAddr().getSub();
		boolean sameSubdiv = currentSubdiv.getSubdivid() == allocatedSubdiv.getSubdivid();

		if (rcr != null && (rcr.getValidated() == null || !rcr.getValidated()) && sameSubdiv)
			return Lookup_NeedRepairCompletionReportValidation.YES;
		return Lookup_NeedRepairCompletionReportValidation.NO;
	}

	@Override
	public String getDescription() {
		return "Decides whether an RIR is needed";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_NeedRepairCompletionReportValidation.values();
	}

}
