package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_NextWorkRequirementByJobCompany implements LookupResultMessage {
	SAME_BUSINESS_COMP("lookupresultmessage.nextworkrequirement.samebusinesscompany"),
	OTHER_BUSINESS_COMP("lookupresultmessage.nextworkrequirement.otherbusinesscompany"),
	UNKNOWN("unknown"),
	NO_FURTHER_WORK("lookupresultmessage.nextworkrequirement.nofurtherwork"),
	SUPPLIER("companyrole.supplier");

	private String messageCode;

	private Lookup_NextWorkRequirementByJobCompany(String messageCode) {
		this.messageCode = messageCode;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}

}
