package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemCalibratedAtTP implements LookupResultMessage
{
	YES("lookupresultmessage.itemcalibratedattp.yes"),
	NO("lookupresultmessage.itemcalibratedattp.no");
	
	private String messageCode;
	
	private Lookup_ItemCalibratedAtTP(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}