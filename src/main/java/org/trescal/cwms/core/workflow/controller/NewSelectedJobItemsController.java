package org.trescal.cwms.core.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.procedure.dto.LaboratoryCategorySplit;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.entity.stategrouplink.db.StateGroupLinkService;
import org.trescal.cwms.core.workflow.entity.workassignment.db.WorkAssignmentService;
import org.trescal.cwms.core.workflow.service.NewSelectedJobItemsGenerator;
import org.trescal.cwms.core.workflow.service.NewSelectedJobItemsModel;
import org.trescal.cwms.core.workflow.view.NewSelectedJobItemsXlsxView;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Performance optimized replacement for the ViewSelectedJobItemsController
 */
@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class NewSelectedJobItemsController {

    @Value("${cwms.config.jobitem.fast_track_turn}")
    private int fastTrackTurn;
    @Autowired
    private DepartmentService deptServ;
    @Autowired
    private ItemStateService itemStateService;
    @Autowired
    private CapabilityCategoryService capabilityCategoryService;
    @Autowired
    private WorkAssignmentService workAssignServ;
    @Autowired
    private StateGroupLinkService stateGroupLinkService;
    @Autowired
    private NewSelectedJobItemsXlsxView xlsxView;
    @Autowired
    private NewSelectedJobItemsGenerator modelGenerator;

    public String VIEW_NAME = "trescal/core/workflow/newselectedjobitems";

    @RequestMapping(value = "/viewselectedjobitems.htm", method = RequestMethod.GET)
	public String referenceData(Model model, @ModelAttribute(name="model") NewSelectedJobItemsModel modelObject) {

		model.addAttribute("model", modelObject);
		model.addAttribute("jobTypes", JobType.getActiveJobTypes());
		model.addAttribute("fastTrackTurn", fastTrackTurn);
		
		return VIEW_NAME;
	}
	
	@ModelAttribute(name="model")
	private NewSelectedJobItemsModel getModelObject(Locale locale,
													@RequestParam(value = "deptid", required = false) Integer deptId,
													@RequestParam(value = "proccatid", required = false) Integer procCatId,
													@RequestParam(value = "stateid", required = false) Integer stateId,
													@RequestParam(value = "groupid", required = false) Integer groupId,
													@RequestParam(value = "days", required = false) Integer days,
													@RequestParam(value = "business", required = false) Boolean includeBusiness,
													@RequestParam(value = "accredited", defaultValue = "NONE") LaboratoryCategorySplit laboratoryCategorySplit,
													@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
													@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Integer allocatedSubdivId = subdivDto.getKey();
		Integer allocatedCompanyId = companyDto.getKey();
		
		List<Integer> procIds = null;
		List<Integer> stateIds = null;
		Integer addressId = null;
		if (deptId != null) {
			Department dept = this.deptServ.get(deptId);
			if (dept != null) {
                procIds = this.workAssignServ.getCapabilitiesIdsAssignedToDepartment(dept);
                addressId = dept.getAddress().getAddrid();
                if (dept.getType().isProcedures())
                    stateIds = getProcedureStateIds();
            } else {
				// empty list for job items without procedure (uncategorised)
				procIds = new ArrayList<Integer>();
				stateIds = getProcedureStateIds();
			}
		} else if (procCatId != null) {
            CapabilityCategory capabilityCategory = this.capabilityCategoryService.get(procCatId);
            if (capabilityCategory != null) {
                procIds = this.workAssignServ.getCapabilitiesIdsAssignedToCategory(capabilityCategory);
                addressId = capabilityCategory.getDepartment().getAddress().getAddrid();
                if (capabilityCategory.getDepartment().getType().isProcedures())
                    stateIds = getProcedureStateIds();
            }
        } else if (stateId != null) {
			stateIds = new ArrayList<>();
			stateIds.add(stateId);
        } else if (groupId != null) {
            stateIds = stateGroupLinkService.getAllByStateGroup(StateGroup.getStateGroupById(groupId)).stream()
                .map(link -> link.getState().getStateid()).distinct().collect(Collectors.toList());
        }

		StateGroupLinkType sglType = null;
		if (groupId != null) {
			sglType = StateGroup.getStateGroupById(groupId).getLinkType();
		}

		NewSelectedJobItemsModel modelObject = modelGenerator.getGroupedJobItems(procIds, stateIds, days,
				includeBusiness, sglType, addressId, allocatedSubdivId, allocatedCompanyId, laboratoryCategorySplit, locale);
		return modelObject;
	}
	
	private List<Integer> getProcedureStateIds() {
		return this.itemStateService.getItemStateIdsForDepartmentTypes(EnumSet.of(DepartmentType.LABORATORY));
	}
	
	@RequestMapping(value = "/exportselectedjobitems.htm", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView exportData(@ModelAttribute(name="model") NewSelectedJobItemsModel modelObject){
		return new ModelAndView(xlsxView, "model", modelObject); 
	}
	
}
