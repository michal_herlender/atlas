package org.trescal.cwms.core.workflow.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.trescal.cwms.core.workflow.entity.TimeOfDay;

public class DateAndTime
{
	private Date date;
	private TimeOfDay time;
	private SimpleDateFormat sdf;

	public DateAndTime(Date date, TimeOfDay time, SimpleDateFormat sdf)
	{
		this.date = date;
		this.time = time;
		this.sdf = sdf;
	}

	public Date getDate()
	{
		return this.date;
	}

	public String getFormattedDate()
	{
		return sdf.format(this.date);
	}

	public String getFormattedDateAndTime()
	{
		return this.toString();
	}

	public TimeOfDay getTime()
	{
		return this.time;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setTime(TimeOfDay time)
	{
		this.time = time;
	}

	@Override
	public String toString()
	{
		return this.getFormattedDate().concat(" ").concat(this.time.toString());
	}
	
	public SimpleDateFormat getSdf() {
		return sdf;
	}
	
	public void setSdf(SimpleDateFormat sdf) {
		this.sdf = sdf;
	}
}