package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ItemHasBeenCalibrated implements LookupResultMessage
{
	YES("lookupresultmessage.itemhasbeencalibrated.yes"),
	NO("lookupresultmessage.itemhasbeencalibrated.no");
	
	private String messageCode;
	
	private Lookup_ItemHasBeenCalibrated(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}