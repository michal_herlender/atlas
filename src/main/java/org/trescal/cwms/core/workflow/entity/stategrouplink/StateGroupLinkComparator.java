package org.trescal.cwms.core.workflow.entity.stategrouplink;

import java.util.Comparator;

public class StateGroupLinkComparator implements Comparator<StateGroupLink>
{
	public int compare(StateGroupLink sgl1, StateGroupLink sgl2)
	{
		int id1 = sgl1.getGroup().getId();
		int id2 = sgl2.getGroup().getId();

		if (id1 == id2) {
            String desc1 = sgl1.getState() != null ? sgl1.getState().getDescription() : "";
            String desc2 = sgl2.getState() != null ? sgl2.getState().getDescription() : "";

            if (desc1.compareTo(desc2) != 0) {
                return desc1.compareTo(desc2);
            } else {
                return Integer.compare(sgl1.getId(), sgl2.getId());
            }
        } else {
            return Integer.compare(id1, id2);
        }
	}
}
