package org.trescal.cwms.core.workflow.entity.lookupinstance.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;

@Service("LookupInstanceService")
public class LookupInstanceServiceImpl extends BaseServiceImpl<LookupInstance, Integer>
		implements LookupInstanceService {
	@Autowired
	private LookupInstanceDao lookupInstanceDao;

	@Override
	protected BaseDao<LookupInstance, Integer> getBaseDao() {
		return lookupInstanceDao;
	}

	@Override
	public List<LookupInstance> findLookupInstanceByName(String name) {
		return lookupInstanceDao.findLookupInstanceByName(name);
	}
}