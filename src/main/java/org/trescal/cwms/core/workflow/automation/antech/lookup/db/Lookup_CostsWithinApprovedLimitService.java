package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ApprovedLimit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CostsWithinApprovedLimit;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_CostsWithinApprovedLimit")
public class Lookup_CostsWithinApprovedLimitService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private ApprovedLimit approvedLimit;

	@Override
	public String getDescription() {
		return "Determines whether the costs of the item's fault report are within the client's pre-approved limit or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: COSTS WITHIN APPROVED LIMIT");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);

		LookupResultMessage result = null;
		Double limit = approvedLimit.parseValue(approvedLimit
				.getValueHierarchical(Scope.CONTACT, ji.getJob().getCon(), ji.getJob().getOrganisation().getComp())
				.getValue());
		if (fr != null && fr.getNoCostingRequired() != null && fr.getNoCostingRequired()) {
			result = Lookup_CostsWithinApprovedLimit.NO_COSTING_REQUIRED;
		} else if (limit == 0) {
			result = Lookup_CostsWithinApprovedLimit.NO_LIMIT;
		} else {
			BigDecimal approvedLimit = new BigDecimal(limit);
			BigDecimal cost = fr.getCost();
			if (cost != null) {
				if (approvedLimit.compareTo(cost) == -1) {
					// limit is less than cost
					result = Lookup_CostsWithinApprovedLimit.EXCEED_LIMIT;
				} else {
					// limit is greater than or equal to cost
					result = Lookup_CostsWithinApprovedLimit.WITHIN_LIMIT;
				}
			} else {
				// cost on fault report is null which probably means there is no
				// cost and therefore this doesn't require costing?
				result = Lookup_CostsWithinApprovedLimit.WITHIN_LIMIT;
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_CostsWithinApprovedLimit.values();
	}
}