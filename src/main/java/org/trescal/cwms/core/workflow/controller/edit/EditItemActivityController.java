package org.trescal.cwms.core.workflow.controller.edit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupFunction;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivityType;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.form.edit.EditActionOutcomeDto;
import org.trescal.cwms.core.workflow.form.edit.EditItemActivityForm;
import org.trescal.cwms.core.workflow.form.genericentityvalidator.EditItemActivityValidator;
import org.trescal.cwms.core.workflow.service.EditWorkflowService;

@Controller @IntranetController
public class EditItemActivityController extends AbstractEditWorkflowController {
	
	@Autowired
	private EditWorkflowService editWorkflowService; 
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private EditItemActivityValidator validator;

	@InitBinder("form")
    private void initValidator(WebDataBinder binder) {
    	binder.setValidator(validator);
    }

	@ModelAttribute("form")
	public EditItemActivityForm formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		EditItemActivityForm form = new EditItemActivityForm();
		// TODO - maybe these should be blank and user specified?
		form.setDefaultLinkType(StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV);
		form.setDefaultStateGroup(StateGroup.INANOTHERBUSINESSCOMPANY);
		
		if (id == 0) {
			// Creating new activity record
			form.setActionOutcomeDtos(new ArrayList<>());
			form.setActive(true);
			form.setActionOutcomesEnabled(false);
			form.setActionOutcomeType(ActionOutcomeType.ADJUSTMENT);
			form.setLookupId(0);
			form.setRetired(false);
			form.setStateGroupLinks(new ArrayList<>());
		}
		else {
			// Editing existing record
			ItemActivity activity = this.itemStateService.findItemActivity(id);
			form.setActionOutcomeDtos(getActionOutcomeDtos(activity));
			form.setActionOutcomeType(activity.getActionOutcomeType() == null ? ActionOutcomeType.ADJUSTMENT : activity.getActionOutcomeType());
			form.setActionOutcomesEnabled(activity.getActionOutcomeType() != null);
			form.setActive(activity.getActive());
			form.setDescription(activity.getDescription());
			/*
			 * ItemActivity can either connect to a state, via a single outcome status,
			 * or to a lookup, in which case there are multiple outcome statuses,
			 * one per lookup result. 
			 */
			form.setLookupOrStatus(activity.getLookup() != null);
			form.setLookupId(activity.getLookup() != null ? activity.getLookup().getId() : 0);
			/*
			 * Get single "default" status ID from the default status outcome (should be a single status outcome, if no lookup)
			 */
			form.setStatusId(getDefaultStatusId(activity));
			form.setRetired(activity.getRetired());
			form.setStateGroupLinks(super.getStateGroupList(activity));
			form.setTranslations(super.getTranslationMap(activity.getTranslations()));
			form.setType(activity.getType()); 
		}

		// Contains defaults for template row in adding 'new' action outcome
		EditActionOutcomeDto templateActionOutcome = new EditActionOutcomeDto();
		templateActionOutcome.setId(0);
		templateActionOutcome.setActive(true);
		templateActionOutcome.setType(form.getActionOutcomeType());
		templateActionOutcome.setDefaultOutcome(false);
		templateActionOutcome.setPositiveOutcome(true);
		form.setTemplateActionOutcome(templateActionOutcome);
		
		return form;
	}
	
	private int getDefaultStatusId(ItemActivity activity) {
		int result = 0;
		if (activity.getOutcomeStatuses().size() == 1) {
			OutcomeStatus os = activity.getOutcomeStatuses().first();
			if (os.getStatus() != null) result = os.getStatus().getStateid();
		}
		return result;
	}
	
	/*
	 * Returns a map used for selecting action outcome types
	 */
	private Map<ActionOutcomeType, List<String>> getActionOutcomeValues() {
		Map<ActionOutcomeType, List<String>> result = new HashMap<>();
		for (ActionOutcomeType type : ActionOutcomeType.values()) {
			List<String> list = new ArrayList<>();
			result.put(type, list);
			for (Enum<?> value : type.getValueClass().getEnumConstants()) {
				list.add(value.name());
			}
		}
		return result;
	}
	
	private List<EditActionOutcomeDto> getActionOutcomeDtos(ItemActivity activity) {
		// These are only relevant (should exist) if a Lookup is connected
		List<EditActionOutcomeDto> result = new ArrayList<>();
		for (ActionOutcome ao : activity.getActionOutcomes()) {
			EditActionOutcomeDto dto = new EditActionOutcomeDto();
			dto.setActive(ao.isActive());
			dto.setDefaultOutcome(ao.isDefaultOutcome());
			dto.setId(ao.getId());
			dto.setPositiveOutcome(ao.isPositiveOutcome());
			dto.setType(ao.getType());
			dto.setValue(ao.getGenericValue().name());
			result.add(dto);
		}
		return result;
	}
	
	@RequestMapping(value="edititemactivity.htm", method=RequestMethod.GET)
	public String showView(Model model,
			@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		model.addAttribute("actionOutcomeTypes", ActionOutcomeType.values());
		model.addAttribute("actionOutcomeValues", getActionOutcomeValues());
		model.addAttribute("itemActivities", getItemActivities());
		model.addAttribute("itemStatuses", getItemStatuses());
		model.addAttribute("lookups", getLookups());
		model.addAttribute("lookupFunctions", LookupFunction.values());
		model.addAttribute("selectBoolean", getSelectBoolean());
		model.addAttribute("stateGroupLinkTypes", StateGroupLinkType.values());
		model.addAttribute("stateGroups", StateGroup.values());
		model.addAttribute("supportedLocales", this.supportedLocaleService.getSupportedLocales());
		model.addAttribute("types", ItemActivityType.values());
		
		if (id != 0) {
			ItemActivity itemActivity = this.itemStateService.findItemActivity(id);
			model.addAttribute("lastActivity", itemActivity);
		}
		else {
			model.addAttribute("currentMessageCount", 0);
		}
		return "trescal/core/workflow/edit/edititemactivity";
	}

	@RequestMapping(value="edititemactivity.htm", method=RequestMethod.POST)
	public String onSubmit(Model model,
			@Validated @ModelAttribute("form") EditItemActivityForm form,
			BindingResult bindingResult,
			@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		if (bindingResult.hasErrors()) {
			return showView(model, id);
		}
		Integer activityId = null;
		if (id == 0) {
			// Creating new record
			activityId = this.editWorkflowService.createItemActivity(form);
		}
		else {
			// Editing existing record
			activityId = id;
			this.editWorkflowService.updateItemActivity(form, activityId);
		}
		return "redirect:workfloweditor_activity.htm?activityId="+activityId;
	}
	
}