package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_BookInItem.Input;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.TreeSet;

@Service
public class HookInterceptor_BookInItem extends HookInterceptor<Input> {
	private static final int DEFAULT_TIME = 0;
	public static final String HOOK_NAME = "book-in-item";

	public static class Input {

		public JobItem ji;
		public String comment;
		public Date startDate;
		public Integer timeSpent;
		public Contact contact;

		public Input(JobItem ji, String comment, Date startDate, Integer timeSpent, Contact contact) {
			super();
			this.ji = ji;
			this.comment = comment;
			this.startDate = startDate;
			this.timeSpent = timeSpent;
			this.contact = contact;
		}
	}

	/**
	 * Used for initializing JobItem with default state for initial save
	 * @return
	 */
	public ItemActivity getDefaultActivity() {
		Hook hook = super.lookupHook(HOOK_NAME);
		if (hook.getActivity() == null) throw new RuntimeException("No activity (default activity) configured for hook "+HOOK_NAME);
		return hook.getActivity();
	}
	
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: BOOK IN ITEM");
			Contact con;
			if (input.contact != null)
				con = input.contact;
			else
				con = this.sessionServ.getCurrentContact();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, input.ji);

			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			// No explicit start state for booking in item; intentionally null 
			jia.setStartStatus(null);
			if (input.startDate != null)
				jia.setStartStamp(input.startDate);
			else
				jia.setStartStamp(new Date());
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(input.ji);
			jia.setComplete(context.isCompleteActivity());
			jia.setRemark(input.comment);

			if (context.isCompleteActivity()) {
				if (input.timeSpent != null)
					jia.setTimeSpent(input.timeSpent);
				else
					jia.setTimeSpent(DEFAULT_TIME);
				jia.setEndStamp(DateUtils.addMinutes(jia.getStartStamp(), jia.getTimeSpent()));
				jia.setCompletedBy(con);
			}

			// adds the action into the job item in the session
			if (input.ji.getActions() == null) {
				input.ji.setActions(new TreeSet<>(new JobItemActionComparator()));
			}
			input.ji.getActions().add(jia);

			ItemStatus endStatus = super.getNextStatus(context);
			input.ji.setState(endStatus);

			if (context.isCompleteActivity()) {
				jia.setEndStatus(endStatus);
			}

			// Job item activity is automatically persisted via JPA cascade
		}
	}
}
