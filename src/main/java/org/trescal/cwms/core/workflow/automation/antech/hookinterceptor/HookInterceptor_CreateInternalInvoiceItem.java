package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.invoice.entity.costs.calcost.InvoiceCalibrationCost;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_CreateInternalInvoiceItem extends HookInterceptor<JobItem> {
	
	public static final Logger logger = LoggerFactory.getLogger(HookInterceptor_CreateInternalInvoiceItem.class);
	public static final String HOOK_NAME = "create-internal-invoice-item";
	
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private InvoiceItemService invoiceItemService;
	@Value("#{props['cwms.config.costs.invoices.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	
	@Override
	public void recordAction(JobItem jobItem) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		if (hook.isActive()) {
			logger.debug("RUNNING HOOK: Create Internal Invoice Item");
			Contact con = this.sessionServ.getCurrentContact();
			PurchaseOrderItem poItem = jobItem.getPurchaseOrderItems().stream().map(PurchaseOrderJobItem::getPoitem).findAny().orElse(null);
			if(poItem != null) {
				HookInterceptorContext context = initialiseFieldsForItem(hook, jobItem);
				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) jobItem.getState());
				jia.setStartStamp(new Date());
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(jobItem);
				jia.setComplete(context.isCompleteActivity());
				if (context.isCompleteActivity())
				{
					jia.setEndStamp(new Date());
					jia.setTimeSpent(0);
					jia.setCompletedBy(con);
				}
				ItemStatus endStatus = super.getNextStatus(context);
	
				jobItem.setState(endStatus);
				jobItemService.updateJobItem(jobItem);
				
				Invoice invoice = invoiceService.getInternalInvoice(jobItem.getJob().getOrganisation(), poItem.getOrder());
				InvoiceItem invoiceItem = new InvoiceItem();
				invoiceItem.setFinalCost(new BigDecimal("0.00"));
				invoiceItem.setTotalCost(new BigDecimal("0.00"));
				invoiceItem.setGeneralDiscountRate(new BigDecimal("0.00"));
				invoiceItem.setGeneralDiscountValue(new BigDecimal("0.00"));
				invoiceItem.setInvoice(invoice);
				invoiceItem.setJobItem(jobItem);
				invoiceItem.setQuantity(1);
				invoiceItem.setInvoice(invoice);
				invoiceItem.setItemno(invoice.getItems().stream().mapToInt(InvoiceItem::getItemno).max().orElse(0) + 1);
				invoice.getItems().add(invoiceItem);
				switch(poItem.getCostType()) {
				case ADJUSTMENT:
					break;
				case CALIBRATION:
					InvoiceCalibrationCost icc = new InvoiceCalibrationCost();
					icc.setAutoSet(true);
					CostCalculator.populateCostType(CostType.CALIBRATION, poItem.getTotalCost(), poItem.getGeneralDiscountRate(), true, icc);
					invoiceItem.setCalibrationCost(icc);
					break;
				case INSPECTION:
					break;
				case OTHER:
					break;
				case PURCHASE:
					break;
				case REPAIR:
					break;
				case SERVICE:
					break;
				default:
					break;
				}
				CostCalculator.updateSingleCost(invoiceItem.getAdjustmentCost(), this.roundUpFinalCosts);
				CostCalculator.updateSingleCost(invoiceItem.getCalibrationCost(), this.roundUpFinalCosts);
				CostCalculator.updateSingleCost(invoiceItem.getRepairCost(), this.roundUpFinalCosts);
				CostCalculator.updateSingleCost(invoiceItem.getPurchaseCost(), this.roundUpFinalCosts);
				CostCalculator.updateItemCostWithRunningTotal(invoiceItem);
				CostCalculator.updatePricingFinalCost(invoice, invoice.getItems());
				invoiceItemService.merge(invoiceItem);
			}
		}
	}
}