package org.trescal.cwms.core.workflow.entity.nextactivity.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;

public interface NextActivityService
{
	/**
	 * Creates a new {@link NextActivity} object using the values given and
	 * persists it in the database.
	 * 
	 * @param activityId the {@link ItemActivity} ID.
	 * @param statusId the {@link ItemStatus} ID.
	 * @param manualEntryAllowed true if the {@link ItemActivity} will be able
	 *        to be entered manually, false otherwise.
	 * @return a {@link ResultWrapper} indicating the success of the operation.
	 */
	ResultWrapper createNextActivity(int activityId, int statusId, boolean manualEntryAllowed);

	/**
	 * Deletes the {@link NextActivity} from the database.
	 * 
	 * @param nextactivity the {@link NextActivity} to delete.
	 */
	void deleteNextActivity(NextActivity nextactivity);

	/**
	 * Returns the {@link NextActivity} with the given ID.
	 * 
	 * @param id the {@link NextActivity} ID.
	 * @return the {@link NextActivity}
	 */
	NextActivity findNextActivity(int id);

	/**
	 * Returns the {@link NextActivity} with an activity with the given ID and
	 * status with the given ID, or null if this {@link NextActivity} doesn't
	 * yet exist.
	 * 
	 * @param activityId the {@link ItemActivity} ID.
	 * @param statusId the {@link ItemStatus} ID.
	 * @return the {@link NextActivity} or null.
	 */
	NextActivity findNextActivityWithActivityAndStatus(int activityId, int statusId);

	/**
	 * Returns a {@link List} of all {@link NextActivity} entities in the
	 * database.
	 * 
	 * @return the {@link List} of {@link NextActivity} entities.
	 */
	List<NextActivity> getAllNextActivitys();

	/**
	 * Returns a {@link List} of all {@link NextActivity} entities for the
	 * {@link ItemStatus} with the given ID.
	 * 
	 * @param statusId the {@link ItemStatus} ID.
	 * @param manualEntryAllowed true to return only the {@link NextActivity}s
	 *        that have manual entry allowed, false to return the
	 *        {@link NextActivity}s that CANNOT be entered manually, or null to
	 *        return both.
	 * @return the {@link List} of {@link NextActivity} entities.
	 */
	List<NextActivity> getNextActivitiesForStatus(int statusId, Boolean manualEntryAllowed);

	/**
	 * Inserts the given {@link NextActivity} into the database.
	 * 
	 * @param nextactivity the {@link NextActivity} to insert.
	 */
	void insertNextActivity(NextActivity nextactivity);

	ResultWrapper removeNextActivity(int id);

	/**
	 * Updates the given {@link NextActivity} in the database if it already
	 * exists, otherwise creates it.
	 * 
	 * @param nextactivity the {@link NextActivity} to create or update.
	 */
	void saveOrUpdateNextActivity(NextActivity nextactivity);

	void setManualEntryAllowed(int nextActivityId, boolean manualEntryAllowed);

	/**
	 * Updates the given {@link NextActivity} in the database.
	 * 
	 * @param nextactivity the {@link NextActivity} to update.
	 */
	void updateNextActivity(NextActivity nextactivity);
}