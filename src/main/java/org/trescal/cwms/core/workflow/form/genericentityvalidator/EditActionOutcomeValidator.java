package org.trescal.cwms.core.workflow.form.genericentityvalidator;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.form.edit.EditActionOutcomeTranslationsForm;

@Component
public class EditActionOutcomeValidator extends AbstractBeanValidator {
	
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@Override
	public boolean supports(Class<?> arg0) {
		return EditActionOutcomeTranslationsForm.class.isAssignableFrom(arg0);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		Locale primaryLocale = this.supportedLocaleService.getPrimaryLocale();
		EditActionOutcomeTranslationsForm form = (EditActionOutcomeTranslationsForm) target;
		boolean primaryTranslationPresent = true;
		for (Integer id : form.getAoIdsToTranslations().keySet()) {
			String text = form.getAoIdsToTranslations().get(id).get(primaryLocale);
			if ((text == null) || text.isEmpty()) {
				primaryTranslationPresent = false;
			}
		}
		if (!primaryTranslationPresent) errors.reject("A translation in the primary locale must be defined for all action outcomes.");
	}
	
}
