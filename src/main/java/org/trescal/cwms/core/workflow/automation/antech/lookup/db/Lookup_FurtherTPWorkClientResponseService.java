package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FurtherTPWorkClientResponse;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Component("Lookup_FurtherTPWorkClientResponse")
public class Lookup_FurtherTPWorkClientResponseService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the client's latest response to further third party work.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: FURTHER TP WORK CLIENT RESPONSE");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions
		for (JobItemAction action : actions.descendingSet()) {
			// find the decision on further work
			if (action.getActivity().getDescription().contains("decision received for further work at third party")
					&& !action.isDeleted()) {
				if (action instanceof JobItemActivity) {
					JobItemActivity activity = (JobItemActivity) action;
					ao = activity.getOutcome();
					break;
				}
			}
		}
		LookupResultMessage result = null;
		if (ao != null) {
			String desc = ao.getDescription();
			if (ao.isPositiveOutcome()) {
				result = Lookup_FurtherTPWorkClientResponse.APPROVED_FURTHER_WORK;
			} else {
				if (desc.contains("no further action")) {
					result = Lookup_FurtherTPWorkClientResponse.REQUESTED_NO_FURTHER_ACTION;
				} else if (desc.contains("as-found")) {
					result = Lookup_FurtherTPWorkClientResponse.APPROVED_AS_FOUND_RESULTS;
				}
			}
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_FurtherTPWorkClientResponse.values();
	}
}