package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_TPItemReturned implements LookupResultMessage
{
	RETURNED("lookupresultmessage.tpitemreturned.returned"),
	NORMAL_ROUTE("lookupresultmessage.tpitemreturned.normalroute");
	
	private String messageCode;
	
	private Lookup_TPItemReturned(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}