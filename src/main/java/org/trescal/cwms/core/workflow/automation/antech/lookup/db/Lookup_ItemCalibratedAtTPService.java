package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementComparator;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemCalibratedAtTP;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemCalibratedAtTP")
public class Lookup_ItemCalibratedAtTPService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item was calibrated on its last third party visit.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM CALIBRATED AT TP");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		if (ji.getTpRequirements().size() > 0) {
			TreeSet<TPRequirement> tprs = new TreeSet<TPRequirement>(new TPRequirementComparator());
			tprs.addAll(ji.getTpRequirements());
			TPRequirement tpr = (TPRequirement) Hibernate.unproxy(tprs.first());
			if (tpr.isCalibration()) {
				result = Lookup_ItemCalibratedAtTP.YES;
			} else {
				result = Lookup_ItemCalibratedAtTP.NO;
			}
		} else {
			// if there is no TP requirements, return NO to make sure this goes
			// to awaiting inspection
			result = Lookup_ItemCalibratedAtTP.NO;
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemCalibratedAtTP.values();
	}
}