package org.trescal.cwms.core.workflow.entity.workassignment;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

import javax.persistence.*;

@Entity
@Table(name = "workassignment", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"depttypeid", "stategroupid"})})
public class WorkAssignment {

    private DepartmentType deptType;
    private int id;
    private StateGroup stateGroup;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "depttypeid")
    public DepartmentType getDeptType() {
        return this.deptType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@Column(name = "stategroupid")
	public Integer getStateGroupId() {
		if (this.stateGroup != null)
			return this.stateGroup.getId();
		return 0;
	}

	@Transient
	public StateGroup getStateGroup() {
		return this.stateGroup;
	}

	public void setDeptType(DepartmentType deptType) {
		this.deptType = deptType;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setStateGroupId(Integer stateGroup) {
		if (stateGroup != null)
			setStateGroup(StateGroup.getStateGroupById(stateGroup));
	}

	public void setStateGroup(StateGroup stateGroup) {
		this.stateGroup = stateGroup;
	}
}