package org.trescal.cwms.core.workflow.entity.itemstate.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.dto.TranslationDTO;
import org.trescal.cwms.core.tools.TranslationUtils;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.dto.PlantillasItemStateDTO;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.progressactivity.ProgressActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.util.*;
import java.util.stream.Collectors;

@Service("ItemStateService")
public class ItemStateServiceImpl extends BaseServiceImpl<ItemState, Integer> implements ItemStateService {
    @Autowired
    private ItemStateDao itemStateDao;

    public void deleteItemState(ItemState i) {
        this.itemStateDao.remove(i);
    }

	public void deleteItemStateById(int id) {
		this.deleteItemState(this.itemStateDao.find(id));
	}

	public ItemActivity findItemActivity(int id) {
		return this.itemStateDao.findItemActivity(id);
	}

	public ItemActivity findItemActivityByName(String name) {
		return this.itemStateDao.findItemActivityByName(name);
	}

	@Override
	public ItemActivity findAllItemActivityByName(String name) {
		return this.itemStateDao.findAllItemActivityByName(name);
	}

	public ItemState findItemState(int id) {
		return this.itemStateDao.find(id);
	}

	public ItemStatus findItemStatus(int id) {
		return this.itemStateDao.findItemStatus(id);
	}

	public ItemStatus findItemStatusByName(String name) {
		return this.itemStateDao.findItemStatusByName(name);
	}
	
	@Override
	public List<KeyValueIntegerString> getDTOList(Collection<Integer> stateIds, Locale locale) {
		List<KeyValueIntegerString> result = Collections.emptyList();
		if (!stateIds.isEmpty()) {
			result = this.itemStateDao.getDTOList(stateIds, locale);
		}
		return result;
	}

	@Override
	public List<ItemState> getAll(StateGroup stateGroup) {
		return itemStateDao.getAll(stateGroup);
	}

	@Override
	public List<ItemState> getAllForStateGroups(List<Integer> stateGroups) {
		return itemStateDao.getAllForStateGroups(stateGroups);
	}

	public List<ItemActivity> getAllItemActivities() {
		return this.itemStateDao.getAllItemActivities();
	}

	public List<ItemStatus> getAllItemStatuses(Class<? extends ItemStatus> clazz, boolean includeRetired) {
		return this.itemStateDao.getAllItemStatuses(clazz, includeRetired);
	}
	
	public List<KeyValueIntegerString> getAllTranslatedItemStatuses(Class<? extends ItemStatus> clazz, boolean includeRetired, Locale locale){
		return this.itemStateDao.getAllTranslatedItemStatuses(clazz, includeRetired, locale);
	}

	public List<ProgressActivity> getAllProgressActivities(boolean includeRetired) {
		return this.itemStateDao.getAllProgressActivities(includeRetired);
	}

	public List<WorkActivity> getAllWorkActivities() {
		return this.itemStateDao.getAllWorkActivities();
	}

	public List<Integer> getItemStateIdsForDepartmentTypes(EnumSet<DepartmentType> deptTypes) {
		return this.itemStateDao.getItemStateIdsForDepartmentTypes(deptTypes);
	}

	public List<Integer> getItemStateIdsForStateGroups(EnumSet<StateGroup> stateGroups) {
		List<Integer> result = Collections.emptyList();
		if (stateGroups != null && !stateGroups.isEmpty()) {
			result = this.itemStateDao.getItemStateIdsForStateGroups(stateGroups);
		}
		return result;
	}

	public void insertItemState(ItemState i) {
		this.itemStateDao.persist(i);
	}

	public void setItemStateDao(ItemStateDao itemStateDao) {
		this.itemStateDao = itemStateDao;
	}

	public void updateItemState(ItemState i) {
		this.itemStateDao.update(i);
	}

	public PlantillasItemStateDTO getPlantillasItemState(Locale locale, int stateId) {
		return this.itemStateDao.getPlantillasItemState(locale, stateId);
	}

	public List<PlantillasItemStateDTO> getPlantillasItemStates(Locale locale) {
		return this.itemStateDao.getPlantillasItemStates(locale);
	}

	public List<TranslationDTO> getTranslatedItemStates(Locale locale, boolean includeRetired) {
		return this.itemStateDao.getTranslatedItemStates(locale, includeRetired);
	}

	@Override
	public List<ItemStateDTO> getHoldActivities(List<Integer> exist_list) {
		return this.itemStateDao.getHoldActivities(exist_list);
	}

	@Override
	public List<ItemStateDTO> getOverrideActivities(List<Integer> exist_list) {
		return this.itemStateDao.getOverrideActivities(exist_list);
	}

	@Override
	public List<ItemStateDTO> getProgressActivities(List<Integer> exist_list) {
		return this.itemStateDao.getProgressActivities(exist_list);
	}

	@Override
	public List<ItemStateDTO> getTransitActivities(List<Integer> exist_list) {
		return this.itemStateDao.getTransitActivities(exist_list);
	}

	@Override
	public List<ItemStateDTO> getWorkActivities(List<Integer> exist_list) {
		return this.itemStateDao.getWorkActivities(exist_list);
	}

	@Override
	public List<ItemStateDTO> getItemStatusForPrebooking(JobType jobType, Locale locale) {

        List<ItemStateDTO> itemsList = new ArrayList<>();

        List<ItemState> acrItems;
        if (jobType.equals(JobType.SITE))
            acrItems = this.getAll(StateGroup.BEFOREONSITE);
        else
            acrItems = this.getAll(StateGroup.CSCONTRACTREVIEW);
        acrItems.forEach(i -> itemsList.add(new ItemStateDTO(i.getStateid(), TranslationUtils.getBestTranslation(i.getTranslations()).orElse(""))));

        List<HoldStatus> onHoldItems = this.itemStateDao.getHoldStatus();
        onHoldItems.forEach(hi -> itemsList.add(new ItemStateDTO(hi.getStateid(),
            TranslationUtils.getBestTranslation(hi.getTranslations()).orElse("")
        )));
        return itemsList.stream().sorted(Comparator.comparing(ItemStateDTO::getDescription)).collect(Collectors.toList());
    }

	@Override
	public void persist(WorkStatus ws) {
		this.itemStateDao.persist(ws);
	}

	@Override
	protected BaseDao<ItemState, Integer> getBaseDao() {
		return itemStateDao;
	}
}