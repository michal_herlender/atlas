package org.trescal.cwms.core.workflow.entity.nextactivity.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;
import org.trescal.cwms.core.workflow.form.genericentityvalidator.NextActivityValidator;

@Service("NextActivityService")
public class NextActivityServiceImpl implements NextActivityService
{
	@Autowired
	private ItemStateService itemStateServ;
	@Autowired
	private NextActivityDao nextActivityDao;
	@Autowired
	private NextActivityValidator validator;
	
	public ResultWrapper createNextActivity(int activityId, int statusId, boolean manualEntryAllowed)
	{
		NextActivity na = new NextActivity();
		na.setStatus(this.itemStateServ.findItemStatus(statusId));
		na.setActivity(this.itemStateServ.findItemActivity(activityId));
		na.setManualEntryAllowed(manualEntryAllowed);

		this.validator.validate(na, null);

		if (!this.validator.getErrors().hasErrors())
		{
			this.insertNextActivity(na);
		}

		return new ResultWrapper(this.validator.getErrors(), na);
	}

	public void deleteNextActivity(NextActivity nextactivity)
	{
		this.nextActivityDao.remove(nextactivity);
	}

	public NextActivity findNextActivity(int id)
	{
		return this.nextActivityDao.find(id);
	}

	public NextActivity findNextActivityWithActivityAndStatus(int activityId, int statusId)
	{
		return this.nextActivityDao.findNextActivityWithActivityAndStatus(activityId, statusId);
	}

	public List<NextActivity> getAllNextActivitys()
	{
		return this.nextActivityDao.findAll();
	}

	public List<NextActivity> getNextActivitiesForStatus(int statusId, Boolean manualEntryAllowed)
	{
		return this.nextActivityDao.getNextActivitiesForStatus(statusId, manualEntryAllowed);
	}

	public void insertNextActivity(NextActivity NextActivity)
	{
		this.nextActivityDao.persist(NextActivity);
	}

	public ResultWrapper removeNextActivity(int id)
	{
		NextActivity na = this.findNextActivity(id);

		this.deleteNextActivity(na);

		return new ResultWrapper(true, null, null, null);
	}

	public void saveOrUpdateNextActivity(NextActivity nextactivity)
	{
		this.nextActivityDao.saveOrUpdate(nextactivity);
	}
	
	public void setManualEntryAllowed(int nextActivityId, boolean manualEntryAllowed)
	{
		NextActivity na = this.findNextActivity(nextActivityId);
		na.setManualEntryAllowed(manualEntryAllowed);
		this.updateNextActivity(na);
	}
	
	public void updateNextActivity(NextActivity NextActivity)
	{
		this.nextActivityDao.update(NextActivity);
	}
}