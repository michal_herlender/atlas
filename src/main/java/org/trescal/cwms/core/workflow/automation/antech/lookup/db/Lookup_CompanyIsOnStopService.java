package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CompanyIsOnStop;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_CompanyIsOnStop")
public class Lookup_CompanyIsOnStopService extends Lookup {

	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;

	@Override
	public String getDescription() {
		return "Determines whether the company owning the job that the item is on is on-stop or not.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: COMPANY IS ON STOP");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		CompanySettingsForAllocatedCompany companySettings = companySettingsService
				.getByCompany(ji.getJob().getCon().getSub().getComp(), ji.getJob().getOrganisation().getComp());
		return (companySettings.isOnStop()) ? Lookup_CompanyIsOnStop.ONSTOP : Lookup_CompanyIsOnStop.TRUSTED;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_CompanyIsOnStop.values();
	}
}