package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionFurtherWork;

@Entity
@DiscriminatorValue("client_decision_further_work")
public class ActionOutcome_ClientDecisionFurtherWork extends ActionOutcome {
	private ActionOutcomeValue_ClientDecisionFurtherWork value;
	
	public ActionOutcome_ClientDecisionFurtherWork() {
		this.type = ActionOutcomeType.CLIENT_DECISION_FURTHER_WORK;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_ClientDecisionFurtherWork getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_ClientDecisionFurtherWork value) {
		this.value = value;
	}

}
