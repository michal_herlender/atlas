package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemQuotedBypassCosting;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemQuotedBypassCosting")
public class Lookup_ItemQuotedBypassCostingService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether an item has a been quoted for or approved by customer services to bypass job costing.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM HAS BEEN QUOTED OR APPROVED TO BYPASS COSTING");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = Lookup_ItemQuotedBypassCosting.NO;
		
		if (ji.getCalibrationCost() != null) {
			if (ji.getCalibrationCost().getCostSrc() != null) {
				if (ji.getCalibrationCost().getCostSrc().equals(CostSource.QUOTATION)) {
					result = Lookup_ItemQuotedBypassCosting.YES;
				}
			}
		}
		
		if (result == Lookup_ItemQuotedBypassCosting.NO && (ji.getGroup() != null)) {
			groupItemLoop: for (JobItem groupItem : ji.getGroup().getItems()) {
				if (groupItem.getJobItemId() != ji.getJobItemId()) {
					if (groupItem.getCalibrationCost() != null) {
						if (groupItem.getCalibrationCost().getCostSrc() != null) {
							if (groupItem.getCalibrationCost().getCostSrc().equals(CostSource.QUOTATION)) {
								result = Lookup_ItemQuotedBypassCosting.YES;
								break groupItemLoop;
							}
						}
					}
				}
			}
		}
		
		if (ji.getApprovedToBypassCosting()) {
			result = Lookup_ItemQuotedBypassCosting.YES;
		}
		
		if (result == Lookup_ItemQuotedBypassCosting.NO && (ji.getGroup() != null)) {
			groupItemLoop: for (JobItem groupItem : ji.getGroup().getItems()) {
				if (groupItem.getJobItemId() != ji.getJobItemId()) {
					if (groupItem.getApprovedToBypassCosting()) {
						result = Lookup_ItemQuotedBypassCosting.YES;
						break groupItemLoop;
					}
				}
			}
		}
			
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemQuotedBypassCosting.values();
	}

}
