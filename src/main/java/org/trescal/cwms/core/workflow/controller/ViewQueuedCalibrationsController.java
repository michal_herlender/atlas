package org.trescal.cwms.core.workflow.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.db.QueuedCalibrationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.form.ViewQueuedCalibrationsForm;

@Controller @IntranetController
public class ViewQueuedCalibrationsController
{
	@Autowired
	private QueuedCalibrationService qcServ;
	
	@ModelAttribute("form")
	protected ViewQueuedCalibrationsForm formBackingObject(HttpServletRequest request) throws Exception
	{
		Contact con = (Contact) request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_CONTACT);
		ViewQueuedCalibrationsForm form = new ViewQueuedCalibrationsForm();
		form.setQueuedCals(this.qcServ.getActiveQueuedCalibrationsOrderedByJob());
		form.setAccreditedQueuedCals(this.qcServ.getAccreditedActiveQueuedCalibrations(con.getPersonid()));
		return form;
	}
	
	@RequestMapping(value="/viewqueuedcalibrations.htm", method=RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("form") ViewQueuedCalibrationsForm form, HttpSession session) throws Exception
	{
		List<QueuedCalibration> selectedCals = this.qcServ.getQueuedCalibrationsWithIds(form.getSelectedCals());
		this.qcServ.printSelectedQueuedCalibrations(selectedCals, session);
		return new RedirectView("jobsearch.htm");
	}
	
	@RequestMapping(value="/viewqueuedcalibrations.htm", method=RequestMethod.GET)
	protected String formView() {
		return "trescal/core/workflow/viewqueuedcalibrations";
	}
}