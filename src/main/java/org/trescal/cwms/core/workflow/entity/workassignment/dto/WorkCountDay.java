package org.trescal.cwms.core.workflow.entity.workassignment.dto;

import lombok.Getter;
import org.trescal.cwms.core.system.Constants;

@Getter
public enum WorkCountDay {
    ALL(Constants.DAYS_MAP_ALL, null),
    DAY_5(Constants.DAYS_MAP_5_DAYS, 5),
    DAY_4(Constants.DAYS_MAP_4_DAYS, 4),
    DAY_3(Constants.DAYS_MAP_3_DAYS, 3),
    DAY_2(Constants.DAYS_MAP_2_DAYS, 2),
    DAY_1(Constants.DAYS_MAP_1_DAYS, 1),
    DAY_OVERDUE(Constants.DAYS_MAP_OVERDUE, 0),
    DAY_BUSINESS(Constants.DAYS_MAP_BUSINESS, null);

    public final String code;
    public final Integer number;

    WorkCountDay(String code, Integer number) {

        this.number = number;
        this.code = code;
    }
}
