package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_NextRepairOperation implements LookupResultMessage
{
	AWAITING_RCR_COMPLETION("lookupresultmessage.nextrepairoperation.awaitingrcrcompletion"),
	AWAITING_INTERNAL_REPAIR("lookupresultmessage.nextrepairoperation.awaitinginternalrepair"),
	AWAITING_COSTING_INTERNAL_REPAIR("lookupresultmessage.nextrepairoperation.awaitingcostingforinternalrepair"),
	AWAITING_EXTERNAL_REPAIR("lookupresultmessage.nextrepairoperation.awaitingexternalrepair"),
	AWAITING_PO_EXTERNAL_REPAIR("lookupresultmessage.nextrepairoperation.awaitingpoforexternalrepair");
	
	private String messageCode;
	
	private Lookup_NextRepairOperation(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getMessageCode() {
		return this.messageCode;
	}
}
