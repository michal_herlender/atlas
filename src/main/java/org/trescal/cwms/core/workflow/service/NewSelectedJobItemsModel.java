package org.trescal.cwms.core.workflow.service;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.MultiValuedMap;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.Getter;
import lombok.Setter;

/**
 * Type safe placeholder class for model data associated with job items view
 */
@Getter @Setter
public class NewSelectedJobItemsModel {
	// Main results provided via jobDto hierarchy
	private List<JobProjectionDTO> jobDtos;
	// Used for filters - type varies by implementation
	private Collection<KeyValueIntegerString> transportOptions;
	private Collection<KeyValueIntegerString> clientCompanies;
	private MultiValuedMap<Integer, SubdivProjectionDTO> clientSubdivs;
	private Collection<KeyValueIntegerString> itemStates;
	private Collection<CapabilityProjectionDTO> capabilities;
	private Collection<KeyValueIntegerString> businessSubdivs;
	private Collection<KeyValueIntegerString> contractReviewers;
	private Collection<KeyValueIntegerString> serviceTypes;
	// Other information in model
	private Integer jobItemCount;
}
