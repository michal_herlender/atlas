package org.trescal.cwms.core.workflow.form.genericentityvalidator;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivityType;
import org.trescal.cwms.core.workflow.form.edit.EditItemActivityForm;

@Component
public class EditItemActivityValidator extends AbstractBeanValidator {
	
	@Autowired
	private SupportedLocaleService supportedLocaleService; 

	@Override
	public boolean supports(Class<?> arg0) {
		return EditItemActivityForm.class.isAssignableFrom(arg0);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		// Perform further validation only if form has no JPA constraint errors (no nulls)
		if (!errors.hasErrors()) {
			EditItemActivityForm form = (EditItemActivityForm) target;
			if (form.getType().equals(ItemActivityType.UNDEFINED)) {
				errors.rejectValue("type", "error.value.notselected", "A value must be selected.");
			}
			if (form.getActive() && !form.getRetired()) {
				if (form.getLookupOrStatus()) {
					// ItemActivity will connect to Lookup
					if (form.getLookupId() == 0) {
						errors.reject("edititemactivity.lookuporstatus", "Active, non-retired item activities must have a Lookup or Status connection.");
					}
					if (form.getActionOutcomeType() != null) {
						// All active action outcomes need to be consistent with the selected type  
						long mismatchedActionOutcomeCount = form.getActionOutcomeDtos().stream()
								.filter(dto -> dto.getActive() && !dto.getDelete() && !dto.getType().equals(form.getActionOutcomeType()))
								.count();
						if (mismatchedActionOutcomeCount > 0) {
							errors.reject("edititemactivity.mismatched","The type of all active action outcomes must match the activity's action outcome type, but "+mismatchedActionOutcomeCount+" are mismatched.");
						}
					}
					else {
						// Should have no active action outcomes if no type selected
						long actionOutcomeCount = form.getActionOutcomeDtos().stream()
								.filter(dto -> dto.getActive() && !dto.getDelete())
								.count();
						if (actionOutcomeCount > 0) {
							errors.reject("edititemactivity.notype","There cannot be any active action outcomes when the type is none, but there are "+actionOutcomeCount+" present.");
						}
					}
					long defaultActionOutcomeCount = form.getActionOutcomeDtos().stream()
							.filter(dto -> !dto.getDelete() && dto.getDefaultOutcome())
							.count();
					if (defaultActionOutcomeCount > 1) {
						errors.reject("edititemactivity.multipledefaults","There must be at most one default action outcome, but there are "+defaultActionOutcomeCount+" selected.");
					}
				}
				else {
					// ItemActivity will connect to ItemStatus
					if (form.getStatusId() == 0) {
						errors.reject("edititemactivity.lookuporstatus", "Active, non-retired item activities must have a Lookup or Status connection.");
					}
				}
			}
			Locale locale = this.supportedLocaleService.getPrimaryLocale();
			String defaultTranslation = form.getTranslations().get(locale);
			if ((defaultTranslation == null) || defaultTranslation.isEmpty()) {
				errors.reject("edititemactivity.translation","Item activity must have translation defined for primary locale "+locale.toString());
			}
		}
	}

}
