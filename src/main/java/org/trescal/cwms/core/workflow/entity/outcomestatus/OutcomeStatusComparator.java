package org.trescal.cwms.core.workflow.entity.outcomestatus;

import java.util.Comparator;

/*
 * Simple comparator required to be able to return OutcomeStatus as a sortedSet which is used currently in lookup methods
 * TODO: Evaluate Lookup methods further and consider using the defaultStatus rather than just the one that is returned first.
 * We might then not need to have a SortType on ItemActivity.getOutcomeStatuses()
 * GB 2015-09-24
 */
public class OutcomeStatusComparator implements Comparator<OutcomeStatus> {
	@Override
	public int compare(OutcomeStatus arg0, OutcomeStatus arg1) {
		return arg0.getId() - arg1.getId();
	}

}
