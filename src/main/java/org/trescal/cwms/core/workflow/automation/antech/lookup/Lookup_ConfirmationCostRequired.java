package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_ConfirmationCostRequired implements LookupResultMessage
{
	YES("lookupresultmessage.confirmationcostrequired.yes"),
	NO("lookupresultmessage.confirmationcostrequired.no");
	
	private String messageCode;
	
	private Lookup_ConfirmationCostRequired(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}