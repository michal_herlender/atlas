package org.trescal.cwms.core.workflow.automation.common.lookup;

import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.lookupresult.db.LookupResultService;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

@Service("StatusCalculationServiceImpl")
public class StatusCalculationServiceImpl implements StatusCalculationService {
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private LookupResultService lookupResultServ;
	@Autowired
	private ItemStateService itemStateService;

	public final static Logger logger = LoggerFactory.getLogger(StatusCalculationServiceImpl.class);

	@Override
	public ItemStatus getNextStatus(ItemActivity activity, JobItem ji) {
		ItemStatus status = null;
		if (activity.getLookup() != null) {
			// get lookup with the name given and run its method
			Lookup lookup = (Lookup) this.applicationContext
					.getBean(activity.getLookup().getLookupFunction().getBeanName());
			logger.debug("Lookup: " + lookup.getDescription() + " JobItem: " + ji.getJobItemId() + " Activity lookup: "
					+ activity.getLookup());
			// gives us the result: 'a', 'b', 'c', etc.
			LookupResultMessage result = lookup.lookupStatus(ji.getJobItemId());
			// gets the outcome status associated with that result
			if (activity.getLookup() != null) {
				status = this.getOutcomeStatusWithCode(ji.getJobItemId(), activity.getLookup().getId(), result);
			} else {
				status = this.getOutcomeStatusWithCode(ji.getJobItemId(), null, result);
			}
		} else {
			if (activity.getOutcomeStatuses().size() == 1) {
				OutcomeStatus os = activity.getOutcomeStatuses().iterator().next();
				if (os.getStatus() != null) {
					status = os.getStatus();
				} else {
					// i've confused myself... what if this is a lookup?
					// can i recursively call this method as below?
				}
			} else {
				for (OutcomeStatus os : activity.getOutcomeStatuses()) {
					if (os.isDefaultOutcome()) {
						if (os.getStatus() != null) {
							status = os.getStatus();
						} else {
							// i've confused myself... what if this is a lookup?
							// can i recursively call this method as below?
						}
					}
				}
			}
		}
		logger.debug("Outcome status: " + status.getDescription());
		return status;
	}

	protected ItemStatus getOutcomeStatusWithCode(int jobItemId, Integer lookupInstanceId,
			LookupResultMessage lookupResult) {
		// if no lookup result is returned, something has gone wrong in a lookup
		if (lookupResult == null) {
			logger.error("Workflow wiring error, no lookup result provided");
			// return error status
			return this.itemStateService.findItemStatusByName("Unable to determine correct status");
		}
		OutcomeStatus outcome = this.lookupResultServ.getResult(lookupInstanceId, lookupResult);
		if (outcome == null) {
			logger.error("Workflow wiring error, no outcome status for lookup instance " + lookupInstanceId
					+ ", lookup result " + lookupResult);
		}
		logger.debug("Determine outcome status for lookup instance " + lookupInstanceId + ", lookup result "
				+ lookupResult + " results in " + outcome.getId());
		if (outcome.getStatus() != null) {
			logger.debug("Returning status " + outcome.getStatus().getStateid());
			return outcome.getStatus();
		} else {
			Lookup lookup = (Lookup) this.applicationContext.getBean(
					outcome.getLookup().getLookupFunction().getLookupResultMessages()[0].getClass().getSimpleName());
			logger.debug("Delegating to lookup " + outcome.getLookup().getId());
			LookupResultMessage result = lookup.lookupStatus(jobItemId);
			return this.getOutcomeStatusWithCode(jobItemId, outcome.getLookup().getId(), result);
		}
	}

	@Override
	public ItemStatus getPreviousStatus(JobItem ji, StateGroup group) {
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());

		// loop backwards through actions
		for (JobItemAction action : actions.descendingSet()) {
			// find where status was overridden
			for (StateGroupLink gl : action.getActivity().getGroupLinks()) {
				if (gl.getGroup().equals(group)) {
					// return the status before it was overridden
					return action.getStartStatus();
				}
			}
		}

		// cannot find overriding activity
		return null;
	}
}