package org.trescal.cwms.core.workflow.automation.common.hookinterceptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.misc.entity.hook.db.HookService;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;
import org.trescal.cwms.core.misc.entity.hookactivity.db.HookActivityService;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workflow.automation.common.lookup.StatusCalculationService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.TreeSet;

/*
 * July 16, 2015 GB:
 * 
 * Refactored the HookInterceptor class (eventually rename to "JobItemWorkflowBean"?) and its 
 * inheriting instance classes to be regular beans that are called from services as needed,
 * and not wired up using an XML AOP scheme.
 * 
 * Also externalized many bean fields which were non-threadsafe into a new "HookInterceptorContext" 
 * (eventually rename to "JobItemWorkflowContext?") that contains relevant entities that are relevant 
 * to each session.
 */
public abstract class HookInterceptor<T> {
	@Autowired
	protected AuthenticationService authServ;
	@Autowired
	protected HookService hookServ;
	@Autowired
	protected HookActivityService hookActServ;
	@Autowired
	protected JobItemService jobItemService;
	@Autowired
	protected JobItemActionService jobItemActionService;
	@Autowired
	protected SessionUtilsService sessionServ;
	@Autowired
	protected StatusCalculationService statusCalcServ;
	@Value("#{props['cwms.config.workflow.log_duplicate_actions']}")
	protected boolean logDuplicates;

	protected final Log logger = LogFactory.getLog(this.getClass());

	protected Hook lookupHook(String hookName) {
		return this.hookServ.findHookByName(hookName);
	}

	protected ItemStatus getNextStatus(HookInterceptorContext context) {
		// also check if the hook is 'always possible', which are special cases
		// such as overriding the status of an item, or setting a company as
		// on-stop which will affect items no matter where they are in the
		// workflow
		if ((context.getHookActivity() == null) && !context.getHook().getAlwaysPossible()) {
			// if no hook activity, return current state because this activity
			// has happened outside of the workflow
			return (ItemStatus) context.getJobItem().getState();
		} else {
			// else use the activity to calculate the next status based on where
			// the item is in the workflow process
			logger.debug("ContextActivity: " + context.getActivity().getDescription());
			logger.debug("Job Item Id: " + context.getJobItem().getJobItemId());
			return this.statusCalcServ.getNextStatus(context.getActivity(), context.getJobItem());
		}
	}

	protected HookInterceptorContext initialiseFieldsForItem(Hook hook, JobItem ji) throws OpenActivityException {
		HookInterceptorContext context = new HookInterceptorContext(hook, ji);
		// if the item has no state but the action can always happen, set the
		// hook activity as null so as to use the hook's default details
		if ((ji.getState() == null) && hook.getAlwaysPossible())
			context.setHookActivity(null);
		else {
			logger.debug("Set Hook Activity");
			logger.debug("Hook: " + hook.getId() + ", JobItemState: " + ji.getState().getStateid());
			context.setHookActivity(
				this.hookActServ.findHookActivityForHookAndState(hook.getId(), ji.getState().getStateid()).orElse(null));
			logger.debug(context.getActivity());
		}
		// if the item has a current status store it for later use
		if (ji.getState() != null)
			context.setCurrentState(ji.getState());
		// find last non-deleted activity if available
		if ((ji.getActions() != null) && (ji.getActions().size() > 0)) {
			TreeSet<JobItemAction> actions = new TreeSet<>(new JobItemActionComparator());
			actions.addAll(ji.getActions());
			// loop backwards to get last non-deleted activity
			for (JobItemAction action : actions.descendingSet()) {
				if (!action.isDeleted()) {
					context.setPreviousActivity(action.getActivity());
					break;
				}
			}
		}
		// if there is a hookActivity (i.e. the activity is part of the
		// workflow), use the details from that
		HookActivity hookActivity = context.getHookActivity();
		if (hookActivity != null) {
			context.setActivity(hookActivity.getActivity());
			context.setBeginActivity(hookActivity.isBeginActivity());
			context.setCompleteActivity(hookActivity.isCompleteActivity());
		}
		// otherwise, get the fallback details from the hook itself
		else {
			context.setActivity(hook.getActivity());
			context.setBeginActivity(hook.getBeginActivity());
			context.setCompleteActivity(hook.getCompleteActivity());
		}
		// if item is currently in the middle of an activity, throw custom
		// exception
		if ((ji.getState() instanceof ItemActivity) && context.isBeginActivity()) {
			Mfr mfr;
			if (ji.getInst().getModel().getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC))
				mfr = ji.getInst().getModel().getMfr();
			else
				mfr = ji.getInst().getMfr();
			String itemDetails = ji.getItemNo() + " : " + mfr.getName() + " " + ji.getInst().getModel().getModel() + " "
				+ " (Job " + ji.getJob().getJobno() + ")";
			String currentActivity = ji.getState().getDescription();
			String attemptedActivity = context.getActivity().getDescription();
			throw new OpenActivityException(itemDetails, currentActivity, attemptedActivity);
		}
		return context;
	}

	protected boolean isWorthPersisting(ItemState endState, HookInterceptorContext context, JobItemAction jia) {
		return
		// if system setting is to log duplicate activities, always return true
		this.logDuplicates
				// if the item has no state, or item has no history, or there is
				// no
				// end status, persist the activity
				|| context.getCurrentState() == null || context.getPreviousActivity() == null || endState == null
				// if the item's state hasn't changed and this activity is the
				// same as the previous one, do not persist the duplicate action
				|| context.getCurrentState().getStateid().intValue() != endState.getStateid().intValue() || context
						.getPreviousActivity().getStateid().intValue() != context.getActivity().getStateid().intValue()
				|| this.authServ.hasRight(jia.getStartedBy(), Permission.AUTO_CWMS);
	}

	protected void persist(JobItemAction jia, ItemState endState, HookInterceptorContext context) {
		if (this.isWorthPersisting(endState, context, jia)) {
			jobItemActionService.save(jia);
		}
	}

	/**
	 * The method that all hooks must override. This method should record the
	 * appropriate action for the job item.
	 * 
	 *            the context used to provide JobItem(s)
	 * @throws OpenActivityException
	 *             if trying to record an activity for an item already with an
	 *             incomplete activity
	 */
	public abstract void recordAction(T instance) throws OpenActivityException;
}