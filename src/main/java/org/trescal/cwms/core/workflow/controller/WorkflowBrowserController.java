package org.trescal.cwms.core.workflow.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.utils.WorkflowDescriptionLinearBuilder;
import org.trescal.cwms.core.workflow.utils.WorkflowGraph;
import org.trescal.cwms.core.workflow.utils.WorkflowGraphGenerator;
import org.trescal.cwms.core.workflow.utils.WorkflowNode;

@Controller
@IntranetController
public class WorkflowBrowserController {
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private WorkflowGraphGenerator workflowGraphGenerator;

	@RequestMapping("/workflowbrowser.htm")
	public String viewScreen() {
		return "trescal/core/workflow/workflowbrowser";
	}

	/*
	 * Verified demo example: Set children = new ArraySet();
	 * children.add(makeChild(2, "Child node 1")); children.add(makeChild(3,
	 * "Child node 2")); return makeParent(1, "Root node", children);
	 */

	@RequestMapping("/workflowbrowser.json")
	public @ResponseBody List<WorkflowNode> referenceData(Locale locale) {
		ItemActivity rootItemActivity = itemStateService.findItemActivity(1);
		// WorkflowDescriptionBuilder builder = new
		// WorkflowDescriptionBuilder(rootItemState, locale);
		// return builder.getMap();
		WorkflowDescriptionLinearBuilder builder = new WorkflowDescriptionLinearBuilder(rootItemActivity);
		return builder.getRootList();
	}

	@RequestMapping(value = "/workflowgraph/{activityId}", produces = { MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody WorkflowGraph generateGraphData(
			@PathVariable(name = "activityId", required = true) Integer activityId,
			@RequestParam(name = "graphName", required = false, defaultValue = "MySuperGraph") String graphName,
			Locale locale) {

		ItemActivity rootItemActivity = itemStateService.findItemActivity(activityId);
		if (rootItemActivity != null) {
			return workflowGraphGenerator.build(graphName, rootItemActivity, locale);
		} else
			return null;
	}
}
