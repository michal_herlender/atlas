package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_HasDocumentForMostRecentGeneralOperation;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_HasDocumentForMostRecentGeneralOperation")
public class Lookup_HasDocumentForMostRecentGeneralOperationService extends Lookup {

	@Autowired
	private GeneralServiceOperationService gsoService;

	@Override
	public String getDescription() {
		return "Determines whether an item has a document for most recent general service operation.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM HAS GENERAL SERVICE OPERATION DOCUMENT");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		GeneralServiceOperation gso = gsoService.getLastGSOByJobitemId(ji.getJobItemId());
		boolean hasDoc = false;
		if(gso != null && CollectionUtils.isNotEmpty(gso.getGsoDocuments()))
		{
			for (GsoDocument gsoDoc : gso.getGsoDocuments()) {
				if(gsoDoc.getLinks().stream().anyMatch(i -> i.getJi().equals(ji)))
				{
					hasDoc = true;
					break;
				}
			}	
		}
		LookupResultMessage result = hasDoc ? Lookup_HasDocumentForMostRecentGeneralOperation.YES : Lookup_HasDocumentForMostRecentGeneralOperation.NO;
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_HasDocumentForMostRecentGeneralOperation.values();
	}
}