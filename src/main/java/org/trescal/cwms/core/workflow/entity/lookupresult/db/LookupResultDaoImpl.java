package org.trescal.cwms.core.workflow.entity.lookupresult.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;

@Repository("LookupResultDao")
public class LookupResultDaoImpl extends BaseDaoImpl<LookupResult, Integer> implements LookupResultDao {
	
	private static final Logger logger = LoggerFactory.getLogger(LookupResultDaoImpl.class);
	
	@Override
	protected Class<LookupResult> getEntity() {
		return LookupResult.class;
	}

	public LookupResult findLookupResultForInstanceAndMessage(Integer lookupInstanceId,
			LookupResultMessage result) {
		if (lookupInstanceId == null) throw new IllegalArgumentException("lookupInstanceId was null!");
		if (result == null) throw new IllegalArgumentException("LookupResultMessage was null!");
		Criteria criteria = getSession().createCriteria(LookupResult.class);
		criteria.add(Restrictions.eq("resultMessageId", result.ordinal()));
		criteria.createCriteria("lookup").add(Restrictions.idEq(lookupInstanceId));
		// If workflow configured correctly, there should be 1 unique result
		// log error if multiple / no results found, but return first result found
		List<LookupResult> results = (List<LookupResult>) criteria.list();
		if (results.size() != 1) {
			logger.error("Workflow configuration error, expected 1 LookupResult for lookup "+lookupInstanceId+" and message "+result.ordinal()+" but found "+results.size());
		}
		return !results.isEmpty() ? results.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<LookupResult> getAllLookupResultsForActivity(int activityId) {
		Criteria crit = getSession().createCriteria(LookupResult.class);
		crit.createCriteria("activity").add(Restrictions.idEq(activityId));
		return crit.list();
	}
}