package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientDecisionRequiredForGeneralServiceOperation;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientResponseRequiredOnFaultReport;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_GeneralServiceOperation;

import java.util.TreeSet;

@Component("Lookup_ClientDecisionRequiredForGeneralServiceOperation")
public class Lookup_ClientDecisionRequiredForGeneralServiceOperationService extends Lookup {

	@Autowired
	private GeneralServiceOperationService gsoService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {

		logger.debug("RUNNING LOOKUP: Client Decision Required For General Service Operation");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		GeneralServiceOperation gso = gsoService.getLastGSOByJobitemId(ji.getJobItemId());

		if (BooleanUtils.isTrue(gso.getClientDecisionNeeded())) {
			TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
			actions.addAll(ji.getActions());
			ActionOutcome gsoAo = null;
			for (JobItemAction action : actions.descendingSet()) {
				if (action instanceof JobItemActivity) {
					JobItemActivity activity = (JobItemActivity) action;
					if (activity.getOutcome() != null) {
						gsoAo = activity.getOutcome();
						break;
					}
				}
			}

			if (ActionOutcomeValue_GeneralServiceOperation.COMPLETED_SUCCESSFULLY.equals(gsoAo.getGenericValue()))
				return Lookup_ClientResponseRequiredOnFaultReport.YES;
			else
				return Lookup_ClientResponseRequiredOnFaultReport.NO;
		} else
			return Lookup_ClientResponseRequiredOnFaultReport.NO;
	}

	@Override
	public String getDescription() {
		return "Determines where to go, depending if the client decision is needed for general service operation";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ClientDecisionRequiredForGeneralServiceOperation.values();
	}
}