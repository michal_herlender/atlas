package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_TPRequirements implements LookupResultMessage
{
	TP_CAN_QUOTE("lookupresultmessage.tprequirements.tpcanquote"),
	SEND_TO_TP_FOR_QUOTE("lookupresultmessage.tprequirements.sendtotpforquote"),
	SEND_TO_TP_FOR_QUOTE_WITH_INSPECTION("lookupresultmessage.tprequirements.sendtotpforquotewithinspection"),
	ISSUE_JOB_COSTING("lookupresultmessage.tprequirements.issuejobcosting"),
	PRE_QUOTED_NOT_COSTED("lookupresultmessage.tprequirements.prequotednotcosted"),
	PRE_QUOTED_AND_COSTED("lookupresultmessage.tprequirements.prequotedandcosted"),
	WORK_AT_PARTNER("lookupresultmessage.tprequirements.workatpartner"),
	CONTRACT_SEND_TO_TP_NO_RETURN("lookupresultmessage.tprequirements.contractsendtotpnoreturn"),
	CONTRACT_SEND_TO_TP_WITH_RETURN("lookupresultmessage.tprequirements.contractsendtotpwithreturn");
	
	private String messageCode;
	
	private Lookup_TPRequirements(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}