package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_AdjustmentOutcome implements LookupResultMessage
{
	ADJUSTED("lookupresultmessage.adjustmentoutcome.adjusted"),
	UNIT_BER("lookupresultmessage.adjustmentoutcome.unitber"),
	INHOUSE_REPAIR("lookupresultmessage.adjustmentoutcome.inhouserepair"),
	THIRDPARTY_REPAIR("lookupresultmessage.adjustmentoutcome.thirdpartyrepair"),
	NOT_SUCCESSFUL("lookupresultmessage.adjustmentoutcome.notsuccessful");
	
	private String messageCode;
	
	private Lookup_AdjustmentOutcome(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getMessageCode() {
		return this.messageCode;
	}
}