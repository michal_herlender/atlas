package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_ContractReview implements ActionOutcomeValue {
	REQUIRES_INHOUSE_CALIBRATION("Requires in-house calibration","actionoutcome.contractreview.inhousecalibration"),
	REQUIRES_THIRD_PARTY_CALIBRATION("Requires third party calibration","actionoutcome.contractreview.tpcalibration"),
	REQUIRES_INHOUSE_REPAIR("Requires in-house repair","actionoutcome.contractreview.inhouserepair"),
	REQUIRES_THIRD_PARTY_REPAIR("Requires third party repair","actionoutcome.contractreview.tprepair"),
	REQUIRES_ADJUSTMENT("Requires adjustment","actionoutcome.contractreview.adjustment"),
	REQUIRES_INSPECTION_TO_DETERMINE_WORK("Requires inspection to determine work","actionoutcome.contractreview.inspection"),
	AWAITING_CLIENT_PURCHASE_ORDER("Awaiting client purchase order","actionoutcome.contractreview.clientpo"),
	AWAITING_CLIENT_FEEDBACK("Awaiting client feedback","actionoutcome.contractreview.clientfeedback"),
	NO_ACTION_REQUIRED("No action required","actionoutcome.contractreview.noaction"),
	AWAITING_QUOTATION_TO_CLIENT("Awaiting quotation to client","actionoutcome.contractreview.quotation"),
	REQUIRES_ONSITE_CALIBRATION("Requires on-site calibration","actionoutcome.contractreview.onsitecalibration");
	
	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_ContractReview(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}

	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.CONTRACT_REVIEW;
	}
}