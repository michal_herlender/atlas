package org.trescal.cwms.core.workflow.entity.itemstate.db;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.dto.TranslationDTO;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.dto.PlantillasItemStateDTO;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.progressactivity.ProgressActivity;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.workactivity.WorkActivity;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

/**
 * Interface for accessing and manipulating {@link ItemState} entities.
 * 
 * @author jamiev
 */
public interface ItemStateService extends BaseService<ItemState, Integer>
{
	/**
	 * Deletes the given {@link ItemState} from the database.
	 * 
	 * @param i the {@link ItemState} to delete.
	 */
	void deleteItemState(ItemState i);

	/**
	 * Deletes the {@link ItemState} with the given ID from the database.
	 * 
	 * @param id the ID of the {@link ItemState} to delete.
	 */
	void deleteItemStateById(int id);
	/**
	 * Returns the {@link ItemActivity} entity with the given ID.
	 * 
	 * @param id the {@link ItemActivity} ID.
	 * @return the {@link ItemActivity}
	 */
	ItemActivity findItemActivity(int id);

	ItemActivity findItemActivityByName(String name);

	/**
	 * Returns the {@link ItemState} entity with the given ID.
	 * 
	 * @param id the {@link ItemState} ID.
	 * @return the {@link ItemState}
	 */
	ItemState findItemState(int id);

	/**
	 * Returns the {@link ItemStatus} entity with the given ID.
	 * 
	 * @param id the {@link ItemStatus} ID.
	 * @return the {@link ItemStatus}
	 */
	ItemStatus findItemStatus(int id);

	ItemStatus findItemStatusByName(String name);
	
	List<ItemState> getAll(StateGroup stateGroup);
	
	List<ItemState> getAllForStateGroups(List<Integer> stateGroups);
	
	/**
	 * Returns all {@link ItemActivity} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link ItemActivity} entities.
	 */
	List<ItemActivity> getAllItemActivities();
	
	List<KeyValueIntegerString> getDTOList(Collection<Integer> stateIds, Locale locale);
	
	List<ItemStateDTO> getHoldActivities(List<Integer> exist_list);
	
	List<ItemStateDTO> getOverrideActivities(List<Integer> exist_list);
	
	List<ItemStateDTO> getProgressActivities(List<Integer> exist_list);
	
	List<ItemStateDTO> getTransitActivities(List<Integer> exist_list);
	
	List<ItemStateDTO> getWorkActivities(List<Integer> exist_list);

	/**
	 * Returns all {@link ItemStatus} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link ItemStatus} entities.
	 */
	List<ItemStatus> getAllItemStatuses(Class<? extends ItemStatus> clazz, boolean includeRetired);
	
	/**
	 * Returns all translated {@link ItemStatus} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link ItemStatus} entities.
	 */
	List<KeyValueIntegerString> getAllTranslatedItemStatuses(Class<? extends ItemStatus> clazz, boolean includeRetired, Locale locale);

	/**
	 * Returns all {@link ProgressActivity} entities stored in the database.
	 * 
	 * @param includeRetired whether to include retired {@link ItemState}s or
	 *        not
	 * @return the {@link List} of {@link ProgressActivity} entities.
	 */
	List<ProgressActivity> getAllProgressActivities(boolean includeRetired);

	/**
	 * Returns all {@link WorkActivity} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link WorkActivity} entities.
	 */
	List<WorkActivity> getAllWorkActivities();

	/**
	 * @param type DepartmentType
	 * @return a unique list of ItemState ids linked to the specified department type
	 */
	List<Integer> getItemStateIdsForDepartmentTypes(EnumSet<DepartmentType> deptTypes);

	/**
	 * @param type StateGroup
	 * @return a unique list of ItemState ids linked to the specified StateGroups
	 */
	List<Integer> getItemStateIdsForStateGroups(EnumSet<StateGroup> stateGroups);
	
	/**
	 * Inserts the given {@link ItemState} into the database.
	 * 
	 * @param i the {@link ItemState} to insert.
	 */
	void insertItemState(ItemState i);

	/**
	 * Updates the given {@link ItemState} in the database.
	 * 
	 * @param i the {@link ItemState} to update.
	 */
	void updateItemState(ItemState i);
	
	PlantillasItemStateDTO getPlantillasItemState(Locale locale, int stateId);
	/**
	 * Gets ItemState information with fields for transmission to Plantillas 
	 * @param locale
	 * @return
	 */
	List<PlantillasItemStateDTO> getPlantillasItemStates(Locale locale);
	
	List<TranslationDTO> getTranslatedItemStates(Locale locale, boolean includeRetired);
	
	List<ItemStateDTO> getItemStatusForPrebooking(JobType jobType, Locale locale);

	ItemActivity findAllItemActivityByName(String name);

	void persist(WorkStatus ws);
	
}