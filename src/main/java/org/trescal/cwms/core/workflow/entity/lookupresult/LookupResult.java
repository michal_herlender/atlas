package org.trescal.cwms.core.workflow.entity.lookupresult;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;

@Entity
@Table(name = "lookupresult")
public class LookupResult {
//	@Deprecated
//	private ItemActivity activity; // See notes on getter
	private int id;
	private LookupInstance lookup;
	private OutcomeStatus outcomeStatus;
	private int resultMessageId;
	private LookupResultMessage resultMessage;

	/**
	 * LookupResult is really associated with the lookup; there is no need to
	 * associate an activity Unused and to be removed
	 */
//	@Deprecated
//	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
//	@JoinColumn(name = "activityid")
//	public ItemActivity getActivity() {
//		return this.activity;
//	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lookupid")
	public LookupInstance getLookup() {
		return this.lookup;
	}

	// Persist only for cascade; we delete LookupResult independendly of
	// OutcomeStatus
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "outcomestatusid")
	public OutcomeStatus getOutcomeStatus() {
		return this.outcomeStatus;
	}

	@NotNull
	@Column(name = "resultmessage", nullable = false)
	public int getResultMessageId() {
		return this.resultMessageId;
	}

	@Transient
	public LookupResultMessage getResultMessage() {
		return this.resultMessage;
	}

//	@Deprecated
//	public void setActivity(ItemActivity activity) {
//		this.activity = activity;
//	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLookup(LookupInstance lookup) {
		this.lookup = lookup;
	}

	public void setOutcomeStatus(OutcomeStatus outcomeStatus) {
		this.outcomeStatus = outcomeStatus;
	}

	public void setResultMessage(LookupResultMessage resultMessage) {
		this.resultMessage = resultMessage;
		this.resultMessageId = resultMessage.ordinal();
	}

	public void setResultMessageId(int resultMessageId) {
		this.resultMessageId = resultMessageId;
		this.resultMessage = this.getLookup().getLookupFunction().getLookupResultMessages()[resultMessageId];
	}
}