package org.trescal.cwms.core.workflow.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.validation.AbstractBeanValidator;


@Component
public class CleaningValidator extends AbstractBeanValidator {
	
public static final String ERROR_CODE = "error.cleaning.barcodenotrecognised";
public static final String DEFAULT_MESSAGE = "Barcode {0} not recognized as an active item on a jobb";
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CleaningJobItem.class);
	}
	

	@Override
	public void validate(Object obj, Errors errors)
	{
		super.validate(obj, errors);
		CleaningJobItem cleaningJI = (CleaningJobItem) obj;
		
		/**
		 * BUSINESS VALIDATION
		 */
		if (cleaningJI.getJis() == null || cleaningJI.getJis().size() == 0)
		{
			errors.rejectValue("jis", "jis", null, "Barcode "
					+ " Empty List!");
		}
		else
		{
			for (int i=0;i<cleaningJI.getJis().size();i++) {
				JobItem ji = cleaningJI.getJis().get(i);
				if(ji == null || !ji.getState().getActive())
				errors.rejectValue("plantids["+ i +"]", ERROR_CODE, new String [] {cleaningJI.getPlantids().get(i)}, DEFAULT_MESSAGE);
				else
					cleaningJI.getCleaneds().set(i, true);
			}
		}
		
	}
}