package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_WaitForNewCalibration
		extends HookInterceptor<HookInterceptor_WaitForNewCalibration.Input> {

	public static final String HOOK_NAME = "wait-for-new-calibration";

	public static class Input {

		private JobItem ji;
		private Date date;

		public Input(JobItem ji, Date startDate) {
			super();
			this.ji = ji;
			this.date = startDate;
		}

	}

	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: WAIT FOR NEW CALIBRATION");
			Contact con = this.sessionServ.getCurrentContact();
			JobItem ji = input.ji;
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) ji.getState());
			jia.setStartStamp(input.date);
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(ji);
			jia.setComplete(context.isCompleteActivity());

			if (context.isCompleteActivity()) {
				jia.setEndStamp(input.date);
				jia.setTimeSpent(0);
				jia.setCompletedBy(con);
			}

			ItemStatus endStatus = super.getNextStatus(context);

			ji.setState(endStatus);
			jobItemService.updateJobItem(ji);

			if (context.isCompleteActivity()) {
				jia.setEndStatus(endStatus);
			}

			super.persist(jia, endStatus, context);
		}
	}

}
