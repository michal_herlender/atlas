package org.trescal.cwms.core.workflow.entity.stategrouplink;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

import javax.persistence.*;

@Entity
@Table(name = "stategrouplink", uniqueConstraints = {@UniqueConstraint(columnNames = {"groupid", "stateid"})})
public class StateGroupLink {

    private StateGroup group;
    private int id;
    private ItemState state;
    private StateGroupLinkType type;

	@Column(name = "groupid")
	public Integer getGroupId()
	{
		if (this.group != null)
			return this.group.getId();
		return 0;
	}
	
	@Transient
	public StateGroup getGroup()
	{
		return this.group;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stateid")
	public ItemState getState()
	{
		return this.state;
	}

	/**
	 * @deprecated 2020-04-07 Use type from linked state group
	 * future development: split state group and store workflow related state groups in database
	 */
	@Enumerated(EnumType.STRING)
	public StateGroupLinkType getType() {
		return type;
	}

	public void setGroupId(Integer group)
	{
		if (group != null)
			setGroup(StateGroup.getStateGroupById(group));
	}
	
	public void setGroup(StateGroup group)
	{
		this.group = group;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setState(ItemState state)
	{
		this.state = state;
	}
	
	public void setType(StateGroupLinkType type) {
		this.type = type;
	}
}