package org.trescal.cwms.core.workflow.entity.nextactivity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Entity
@Table(name = "nextactivity", uniqueConstraints = { @UniqueConstraint(columnNames = { "activityid", "statusid" }) })
public class NextActivity {
	private ItemActivity activity;
	private int id;
	private boolean manualEntryAllowed;
	private ItemStatus status;

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@JoinColumn(name = "activityid", nullable = true)
	public ItemActivity getActivity() {
		return this.activity;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = true)
	public ItemStatus getStatus() {
		return this.status;
	}

	@NotNull
	@Column(name = "manualentryallowed", nullable = false, columnDefinition = "tinyint")
	public boolean isManualEntryAllowed() {
		return this.manualEntryAllowed;
	}

	public void setActivity(ItemActivity activity) {
		this.activity = activity;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setManualEntryAllowed(boolean manualEntryAllowed) {
		this.manualEntryAllowed = manualEntryAllowed;
	}

	public void setStatus(ItemStatus status) {
		this.status = status;
	}
}
