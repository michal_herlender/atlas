package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_TypeOfNextWorkRequirement;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_TypeOfNextWorkRequirement")
public class Lookup_TypeOfNextWorkRequirementService extends Lookup {

	@Autowired
	private JobItemService jobItemService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Type of Next Work Requirement");

		JobItem ji = jobItemService.findJobItem(jobItemId);
		WorkRequirement wr = ji.getNextWorkReq().getWorkRequirement();

		switch (wr.getType()) {
		case ADJUSTMENT:
			return Lookup_TypeOfNextWorkRequirement.ADJUSTMENT;
		case CALIBRATION:
			return Lookup_TypeOfNextWorkRequirement.CALIBRATION;
		case REPAIR:
			return Lookup_TypeOfNextWorkRequirement.REPAIR;
		case THIRD_PARTY:
			return Lookup_TypeOfNextWorkRequirement.THIRD_PARTY;
		case ONSITE:
			return Lookup_TypeOfNextWorkRequirement.ONSITE_CALIBRATION;
		case GENERAL_SERVICE_OPERATION:
			return Lookup_TypeOfNextWorkRequirement.GENERAL_SERVICE_OPERATION;
		default:
			break;
		}
		return null;
	}

	@Override
	public String getDescription() {
		return "Decides where to go from the next work reqquirement";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_TypeOfNextWorkRequirement.values();
	}

}
