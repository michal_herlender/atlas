package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemAtDespatchLocation;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemAtDespatchLocation")
public class Lookup_ItemAtDespatchLocationService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item is at the location where it is due to be despatched to the client from.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM AT DESPATCH LOCATION");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		Subdiv currentlyAt = null;
		Subdiv leavingFrom = null;
		if ((ji.getCurrentAddr() != null) && (ji.getReturnOption() != null)) {
			currentlyAt = ji.getCurrentAddr().getSub();
			leavingFrom = ji.getReturnOption().getSub();
		}
		LookupResultMessage result = null;
		if (leavingFrom != null) {
			if (currentlyAt != null) {
				if (currentlyAt.getSubdivid() == leavingFrom.getSubdivid()) {
					result = Lookup_ItemAtDespatchLocation.YES;
				} else {
					result = Lookup_ItemAtDespatchLocation.NO;
				}
			}
		} else {
			result = Lookup_ItemAtDespatchLocation.YES;
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemAtDespatchLocation.values();
	}
}