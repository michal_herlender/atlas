package org.trescal.cwms.core.workflow.entity.workassignment.db;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.departmenttype.db.DepartmentTypeService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.dto.LaboratoryCategorySplit;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.dto.LabCounts;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkComparator;
import org.trescal.cwms.core.workflow.entity.stategrouplink.db.StateGroupLinkService;
import org.trescal.cwms.core.workflow.entity.workassignment.WorkAssignment;
import org.trescal.cwms.core.workflow.entity.workassignment.dto.*;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.trescal.cwms.core.procedure.dto.LaboratoryCategorySplit.*;

@Service("WorkAssignmentService")
@Slf4j
public class WorkAssignmentServiceImpl implements WorkAssignmentService {
    private final DepartmentTypeService deptTypeServ;
    private final DepartmentService deptService;
    private final JobItemService jiServ;
    private final StateGroupLinkService stateGroupLinkServ;
    private final WorkAssignmentDao workAssignmentDao;

    public WorkAssignmentServiceImpl(DepartmentTypeService deptTypeServ, DepartmentService deptService, JobItemService jiServ, StateGroupLinkService stateGroupLinkServ, WorkAssignmentDao workAssignmentDao) {
        this.deptTypeServ = deptTypeServ;
        this.deptService = deptService;
        this.jiServ = jiServ;
        this.stateGroupLinkServ = stateGroupLinkServ;
        this.workAssignmentDao = workAssignmentDao;
    }

    public Boolean businessItems(Map<String, Integer> daysMap, String day) {
        Boolean business = false;
        if (daysMap.get(day) == null) {
            if (day.equals(Constants.DAYS_MAP_BUSINESS)) {
                business = true;
            } else {
                business = null;
            }
        }

        return business;
    }
	
	public List<WorkAssignment> getForDepartmentType(DepartmentType departmentType) {
		return this.workAssignmentDao.getForDepartmentType(departmentType);
	}

	public void deleteWorkAssignment(WorkAssignment workassignment)
	{
		this.workAssignmentDao.remove(workassignment);
	}

	public WorkAssignment findWorkAssignment(int id)
	{
		return this.workAssignmentDao.find(id);
	}

	public List<WorkAssignment> getAllWorkAssignments()
	{
		return this.workAssignmentDao.findAll();
	}

    public Map<Tuple2<Integer, Boolean>, LabCounts> getLabWorkCounts() {
        return this.workAssignmentDao.getLabWorkCounts();
    }

    public List<Integer> getCapabilitiesIdsAssignedToCategory(CapabilityCategory pc) {
        return this.workAssignmentDao.getCapabilitiesIdsAssignedToCategory(pc);
    }

    public List<Integer> getCapabilitiesIdsAssignedToDepartment(Department dept) {
        return this.workAssignmentDao.getCapabilitiesIdsAssignedToDeptartment(dept);
    }

    public List<StateGroup> getStateGroupsAssignedToDeptType(DepartmentType deptType) {
        return this.workAssignmentDao.getStateGroupsAssignedToDeptType(deptType);
    }

    private WorkAssigmentStateGroupsCounters countWorkForDepartmentTypeByStatus(DepartmentType deptType, Subdiv subdiv) {
        List<StateGroup> stateGroups = this.getStateGroupsAssignedToDeptType(deptType);
        val counters = stateGroups.stream()
                .map(sg -> Tuple.of(sg.getId(), getCountsByStateGroupAndSubdiv(sg, subdiv)))
                .map(t -> t.append(t._2.values().stream().reduce(0, Integer::sum)))
                .map(t -> Tuple.of(t._1, WorkAssigmentStateGroupCounter.of(t._2, t._3)))
                .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2, (u1, u2) -> u1, TreeMap::new));
        return WorkAssigmentStateGroupsCounters.of(counters);
    }

    private Map<Integer, Integer> getCountsByStateGroupAndSubdiv(StateGroup sg, Subdiv subdiv) {
        return stateGroupLinkServ.getAllByStateGroup(sg).stream().sorted(new StateGroupLinkComparator())
                .filter(sgl -> Objects.nonNull(sgl.getState()))
                .map(sgl -> Tuple.of(sgl.getState().getStateid(),
                        jiServ.getCountAllJobItemsForState(sgl.getState(), subdiv.getId(), sgl.getGroup().getLinkType()).intValue()
                ))
                .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2));
    }

    @Override
    public Map<Integer, WorkAssigmentCounters> getWorksForDepartmentTypes(Subdiv subdiv) {
        return deptTypeServ.getActiveDepartmentTypes().stream()
                .flatMap(dt -> dt.isProcedures() ? mapLaboratoryType(dt, subdiv) : mapOtherDepartments(dt, subdiv))
                .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2));
    }

    private Stream<Tuple2<Integer, WorkAssigmentLaboratoryCounter>> mapLaboratoryType(DepartmentType departmentType, Subdiv subdiv) {
        val departments = deptService.getByTypeAndSubdiv(departmentType, subdiv.getSubdivid());
        val labWorks = this.getLabWorkCounts();
        val workByDepartment = departments.stream().map(d -> Tuple.of(d.getDeptid(),
                        calculateWorkForDepartment(d.getCategories(), labWorks)))
                .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2));
        val summary = workByDepartment.values().stream().map(WorkByDepartment::getTotal)
                .reduce(emptyCountByDay(), this::sumWorkCountDays);
        val uncategorised = countUncategorised(subdiv);
        return Stream.of(Tuple.of(departmentType.ordinal(),
                WorkAssigmentLaboratoryCounter.of(workByDepartment, uncategorised, summary)));
    }

    private Map<WorkCountDay, Integer> countUncategorised(Subdiv subdiv) {
        return Stream.of(WorkCountDay.values()).map(d -> Tuple.of(d,
                        jiServ.getCountActiveJobItemsWithProcs(null, d.number,
                                d == WorkCountDay.ALL ? null : d == WorkCountDay.DAY_BUSINESS, true, subdiv)))
                .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2));
    }

    private WorkByDepartment calculateWorkForDepartment(Set<CapabilityCategory> categories, Map<Tuple2<Integer, Boolean>, LabCounts> labWorks) {
        val categoriesMap = categories.stream()
                .flatMap(c -> countByCategory(c.getId(), c.getSplitTraceable(), labWorks.get(Tuple.of(c.getId(), true)), labWorks.get(Tuple.of(c.getId(), false))))
                .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2, (u1, u2) -> u1, TreeMap::new));
        val total = categoriesMap.values().stream().reduce(emptyCountByDay(), this::sumWorkCountDays);
        return WorkByDepartment.of(categoriesMap, total);
    }

    private Map<WorkCountDay, Integer> sumWorkCountDays(Map<WorkCountDay, Integer> value, Map<WorkCountDay, Integer> accumulator) {
        return Stream.concat(value.entrySet().stream(), accumulator.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, Integer::sum, TreeMap::new));
    }

    private Map<WorkCountDay, Integer> emptyCountByDay() {
        return Stream.of(WorkCountDay.values()).map(d -> Tuple.of(d, 0)).collect(Collectors.toMap(Tuple2::_1, Tuple2::_2));
    }

    private Stream<Tuple2<Tuple2<Integer, LaboratoryCategorySplit>, Map<WorkCountDay, Integer>>> countByCategory(Integer catId, Boolean split, LabCounts labCountsAccredited, LabCounts labCountsTraceable) {
        val accredited = labCountsAccredited == null ? emptyCountByDay() :
                Stream.of(WorkCountDay.values()).map(d -> Tuple.of(d, countForDay(labCountsAccredited, d)))
                        .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2, (u1, u2) -> u1, TreeMap::new));
        val traceable = labCountsTraceable == null ? emptyCountByDay() :
                Stream.of(WorkCountDay.values()).map(d -> Tuple.of(d, countForDay(labCountsTraceable, d)))
                        .collect(Collectors.toMap(Tuple2::_1, Tuple2::_2, (u1, u2) -> u1, TreeMap::new));
        return split ?
                Stream.of(
                        Tuple.of(Tuple.of(catId, ACCREDITED), accredited),
                        Tuple.of(Tuple.of(catId, TRACEABLE), traceable)
                ) :
                Stream.of(
                        Tuple.of(Tuple.of(catId, NONE), sumWorkCountDays(accredited, traceable))
                );
    }

    private Integer countForDay(@NotNull LabCounts labCounts, WorkCountDay d) {
        switch (d) {
            case ALL:
                return labCounts.getTotal();
            case DAY_1:
                return labCounts.getOne();
            case DAY_2:
                return labCounts.getTwo();
            case DAY_3:
                return labCounts.getThree();
            case DAY_4:
                return labCounts.getFour();
            case DAY_5:
                return labCounts.getFive();
            case DAY_OVERDUE:
                return labCounts.getOverdue();
            case DAY_BUSINESS:
                return labCounts.getBusiness();
        }
        return 0; // JAVA 8 cannot check for totality
    }

    private Stream<Tuple2<Integer, WorkAssigmentStateGroupsCounters>> mapOtherDepartments(DepartmentType departmentType, Subdiv subdiv) {
        return Stream.of(Tuple.of(departmentType.ordinal(), countWorkForDepartmentTypeByStatus(departmentType, subdiv)));
    }

    public void insertWorkAssignment(WorkAssignment WorkAssignment) {
        this.workAssignmentDao.persist(WorkAssignment);
    }

    public List<StateGroup> getLabStateGroups() {
        Collection<DepartmentType> types = this.deptTypeServ.getProcEnabledDepartmentTypes();
        return this.workAssignmentDao.getStateGroupsForDepartmentTypes(types);
    }
}