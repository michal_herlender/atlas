package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_PreDeliveryRequirements;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

import java.util.Objects;

@Component("Lookup_PreDeliveryRequirements")
public class Lookup_PreDeliveryRequirementsService extends Lookup {

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: PRE DELIVERY REQUIREMENTS ");
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		Subdiv allocatedSubdiv = jobItem.getJob().getOrganisation();
		
		if(jobItem.getCurrentAddr() == null)
			return Lookup_PreDeliveryRequirements.ALREADY_AT_CUSTOMER_ADDRESS;
		Subdiv currentSubdiv = jobItem.getCurrentAddr().getSub();
		// next subdiv is either the next calibration location or, if all work
		// is done (complete/cancelled),
		// the subdiv of the pre-defined return option
		Subdiv nextSubdiv = jobItem.getWorkRequirements().stream().allMatch(wr -> wr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE) || 
				                                                                  wr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED))
				? Objects.requireNonNull(jobItem.getReturnOption(),String.format("The return option for the job item (%d) is not provided",jobItem.getInst().getPlantid()))
				.getSub() : jobItem.getCalAddr().getSub();
		if (currentSubdiv.getSubdivid() == nextSubdiv.getSubdivid())
			return Lookup_PreDeliveryRequirements.INTERNAL;
		else if (currentSubdiv.getComp().getCoid() == nextSubdiv.getComp().getCoid()
				|| allocatedSubdiv.getSubdivid() == nextSubdiv.getSubdivid())
			return Lookup_PreDeliveryRequirements.INSIDE_COMPANY;
		else
			return Lookup_PreDeliveryRequirements.EXTERNAL;
	}

	@Override
	public String getDescription() {
		return "Pre delivery requirements";
	}	

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_PreDeliveryRequirements.values();
	}
}