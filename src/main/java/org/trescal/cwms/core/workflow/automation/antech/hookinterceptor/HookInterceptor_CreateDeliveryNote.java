package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_CreateDeliveryNote extends HookInterceptor<Delivery> {
	public static final String HOOK_NAME = "create-delivery-note";

	@Autowired
	private MessageSource messageSource;

	@Transactional
	@Override
	public void recordAction(Delivery delivery) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: CREATE DELIVERY NOTE");
			Contact con = this.sessionServ.getCurrentContact();

			if (delivery instanceof JobDelivery) {
				JobDelivery del = (JobDelivery) delivery;

				for (DeliveryItem di : del.getItems()) {
					JobDeliveryItem jdItem = (JobDeliveryItem) di;
					JobItem ji = jdItem.getJobitem();
					HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

					JobItemActivity jia = new JobItemActivity();
					jia.setActivity(context.getActivity());
					jia.setActivityDesc(context.getActivity().getDescription());
					jia.setStartStatus((ItemStatus) ji.getState());
					jia.setStartStamp(new Date());
					jia.setStartedBy(con);
					jia.setCreatedOn(new Date());
					jia.setJobItem(ji);
					jia.setComplete(context.isCompleteActivity());
					Locale messageLocale = ji.getJob().getOrganisation().getComp().getDocumentLanguage() != null ? 
							ji.getJob().getOrganisation().getComp().getDocumentLanguage():con.getLocale();
					jia.setRemark(this.messageSource.getMessage("docs.deliverynote", null, "Delivery Note : ",
							messageLocale) + " : "
							+ del.getIdentifier() + ".");

					if (context.isCompleteActivity()) {
						jia.setEndStamp(new Date());
						jia.setTimeSpent(5);
						jia.setCompletedBy(con);
					}

					ItemStatus endStatus = null;
					if (context.isCompleteActivity()) {
						endStatus = super.getNextStatus(context);
						ji.setState(endStatus);
						jia.setEndStatus(endStatus);
					} else
						ji.setState(context.getActivity());
					jobItemService.updateJobItem(ji);
					this.persist(jia, endStatus, context);
				}
			}
		}
	}
}