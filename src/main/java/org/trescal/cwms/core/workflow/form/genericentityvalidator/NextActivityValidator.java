package org.trescal.cwms.core.workflow.form.genericentityvalidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractEntityValidator;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;
import org.trescal.cwms.core.workflow.entity.nextactivity.db.NextActivityService;

@Component("NextActivityValidator")
public class NextActivityValidator extends AbstractEntityValidator
{
	@Autowired
	BeanValidator beanValidator;
	
	private NextActivityService nextActServ;

	public void setNextActServ(NextActivityService nextActServ)
	{
		this.nextActServ = nextActServ;
	}

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(NextActivity.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		NextActivity na = (NextActivity) target;
		if (errors == null) errors = new BindException(na, "na");
		beanValidator.validate(na, errors);
		// check this activity is not already a next activity for this status
		if (this.nextActServ.findNextActivityWithActivityAndStatus(na.getActivity().getStateid(), na.getStatus().getStateid()) != null)
		{
			errors.rejectValue("activity", null, "This activity can already be started from this status.");
		}
		// make the errors available outside of this function
		this.errors = (BindException) errors;
	}
}