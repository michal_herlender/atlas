package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ConfirmationCosting;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ConfirmationCostRequired;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ConfirmationCostRequired")
public class Lookup_ConfirmationCostRequiredService extends Lookup {

	@Autowired
	private ConfirmationCosting confirmationCosting;

	@Override
	public String getDescription() {
		return "Determines whether the client requires a confirmation costing to be issued.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: CONFIRMATION COST REQUIRED");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = confirmationCosting.parseValue(confirmationCosting
				.getValueHierarchical(Scope.CONTACT, ji.getJob().getCon(), ji.getJob().getOrganisation().getComp())
				.getValue()) ? Lookup_ConfirmationCostRequired.YES : Lookup_ConfirmationCostRequired.NO;
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ConfirmationCostRequired.values();
	}
}