package org.trescal.cwms.core.workflow.controller.edit;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupFunction;

/**
 * Controller returns fragments of full pages, etc... related to workflow
 * This is separated from other workflow controllers to prevent calls to formBackingObject in those controllers
 * 
 * TODO: maybe we should have a different error handler than @IntranetController for these responses to prevent whole page errors 
 */
@Controller @IntranetController
public class WorkflowFragmentController {
	
	/*
	 * Intended for AJAX use
	 * Displays a lookup results table for the lookups associated with the selected LookupInstance ID
	 */
	@RequestMapping(value="lookupresultstable.htm", method=RequestMethod.GET)
	public String showLookupResultsTable(Model model, 
			@RequestParam(name="function", required=true) LookupFunction function) {
		model.addAttribute("lookupFunction", function);
		return "trescal/core/workflow/edit/tablelookupresults";
	}
}
