package org.trescal.cwms.core.workflow.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LabCountsRaw {
    Integer id;
    Boolean acreditationSpecific;
    Long total, overdue, one, two, three, four, five, business;

    public LabCounts toLabCounts() {
        return new LabCounts(total.intValue(), overdue.intValue(), one.intValue(), two.intValue(), three.intValue(),
                four.intValue(), five.intValue(), business.intValue());
    }


}
