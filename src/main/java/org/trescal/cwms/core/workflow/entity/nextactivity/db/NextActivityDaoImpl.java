package org.trescal.cwms.core.workflow.entity.nextactivity.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;

@Repository("NextActivityDao")
public class NextActivityDaoImpl extends BaseDaoImpl<NextActivity, Integer> implements NextActivityDao
{
	@Override
	protected Class<NextActivity> getEntity() {
		return NextActivity.class;
	}
	
	public NextActivity findNextActivityWithActivityAndStatus(int activityId, int statusId) {
		Criteria criteria = getSession().createCriteria(NextActivity.class);
		criteria.createCriteria("activity").add(Restrictions.idEq(activityId));
		criteria.createCriteria("status").add(Restrictions.idEq(statusId));
		return (NextActivity) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<NextActivity> getNextActivitiesForStatus(int statusId, Boolean manualEntryAllowed) {
		Criteria crit = getSession().createCriteria(NextActivity.class);
		// fetch the activity lookups for ajax purposes
		crit.setFetchMode("activity.lookup", FetchMode.JOIN);
		crit.createCriteria("status").add(Restrictions.idEq(statusId));
		if (manualEntryAllowed != null) crit.add(Restrictions.eq("manualEntryAllowed", manualEntryAllowed));
		Criteria activityCrit = crit.createCriteria("activity");
		activityCrit.addOrder(Order.asc("description"));
		// fetch the activity lookups for ajax purposes
		activityCrit.setFetchMode("lookup", FetchMode.JOIN);
		return crit.list();
	}
}