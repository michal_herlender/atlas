package org.trescal.cwms.core.workflow.automation.antech.jobinterceptor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.events.JobItemActionCreateOrUpdateEvent;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.db.JobStatusService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.rest.adveso.service.AdvesoEventService;

import java.time.LocalDate;
import java.util.Date;

import static org.springframework.context.i18n.LocaleContextHolder.getTimeZone;

@Aspect
@Component
@Configurable
public class JobStatusInterceptor {
	@Autowired
	private JobService jobServ;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobStatusService jobStatusServ;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private AdvesoEventService advesoEventService;
	@Autowired
	private NotificationSystemModelService notificationSystemModelService;

	private final static Logger logger = LoggerFactory.getLogger(JobStatusInterceptor.class);

	// TODO optimize this check via query
	public boolean allItemsOnJobAreComplete(Job job) {
		boolean isComplete = true;
		if (job.getItems() != null && job.getItems().size() > 0) {
			for (JobItem item : job.getItems()) {
				// if item has no state yet, assume it is active because it is
				// possibly just being added to the job
				if ((item.getState() == null) || item.getState().getActive()) {
					isComplete = false;
					break;
				}
			}
		} else {
			isComplete = false;
		}

		return isComplete;
	}

	/**
	 * This advice runs before the job item deletion, because the job item is no
	 * longer attached to the session after deletion.
	 * 
	 */
	@Before("execution(* *.*(..)) && @annotation(jobItemDeletion)")
	public void checkJobStatus_JobItemDeleted(JoinPoint jp, JobItemDeletion jobItemDeletion) {
		logger.info("RUNNING JOB STATUS INTERCEPTOR - JOB ITEM DELETION");

		Integer jobItemId = (Integer) jp.getArgs()[0];
		JobItem ji = jobItemService.get(jobItemId);
		Job job = ji.getJob();

		updateJobStatusAndDateIfRequired(job);

		Company businessCompany = job.getOrganisation().getComp();
		boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
				ji.getJob().getCon().getSub().getSubdivid(), businessCompany.getCoid(),  new Date());
		if (isLinkedToAdveso) {
			advesoEventService.publishJobItemDeletionEvent(ji);
		}
	}

	@After("execution(* *.*(..)) && @annotation(jobItemActionChange)")
	public void checkJobStatus_ItemAction(JoinPoint jp, JobItemActionChange jobItemActionChange) {
		logger.info("RUNNING JOB STATUS INTERCEPTOR - JOB ITEM ACTION CHANGE");

		JobItemAction action = (JobItemAction) jp.getArgs()[0];
		JobItem ji = action.getJobItem();
		Job job = ji.getJob();

		// if the state is an end state and the completion date of the item has
		// not already been set
		boolean jobItemComplete = (ji.getState() != null) && !ji.getState().getActive();

		if (jobItemComplete && (ji.getDateComplete() == null)) {
			logger.debug("Setting dateComplete on job item");
			ji.setDateComplete(new Date());
		}
		else if (!jobItemComplete && (ji.getDateComplete() != null)) {
			logger.debug("Clearing dateComplete on job item");
			ji.setDateComplete(null);
		}


		// Only check rest of job for completion if this item is completed
		if (jobItemComplete) {
			updateJobStatusAndDateIfRequired(job);
			// push JobItem complete notification to the queue
			Company businessCompany = job.getOrganisation().getComp();
			boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
					job.getCon().getSub().getSubdivid(), businessCompany.getCoid(),  new Date());
			if(isLinkedToAdveso)
				notificationSystemModelService.pushToQueue(JobItemActionCreateOrUpdateEvent.ENTITY_CLASS,
                    JobItemActionCreateOrUpdateEvent.FIELD_NAME, JobItemActionCreateOrUpdateEvent.OPERATION_TYPE,
                    JobItemActionCreateOrUpdateEvent.FIELD_TYPE,
                    ji.getJobItemId(),
                    ji.getInst(), null, job.getCon());
		}
	}

	private void updateJobStatusAndDateIfRequired(Job job) {
		// if all items are complete
		if (this.allItemsOnJobAreComplete(job)) {
			// only update the date/status if the job wasn't previously still
			// 'On-going', actions after invoice/completion (such as cert
			// re-signing) should not move the complete date on!
			if (!job.getJs().isComplete()) {
				logger.debug("Updating job status to complete");
				// set to complete
				JobStatus complete = this.jobStatusServ.findCompleteStatus();
				job.setJs(complete);
				job.setDateComplete(LocalDate.now(getTimeZone().toZoneId()));
				this.jobServ.merge(job);
			}
		}
	}
}