package org.trescal.cwms.core.workflow.controller.edit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupFunction;
import org.trescal.cwms.core.workflow.entity.lookupinstance.LookupInstance;
import org.trescal.cwms.core.workflow.entity.lookupinstance.db.LookupInstanceService;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;
import org.trescal.cwms.core.workflow.entity.outcomestatus.OutcomeStatus;
import org.trescal.cwms.core.workflow.form.edit.EditLookupInstanceForm;
import org.trescal.cwms.core.workflow.form.edit.EditOutcomeStatusDto;
import org.trescal.cwms.core.workflow.form.genericentityvalidator.EditLookupInstanceValidator;
import org.trescal.cwms.core.workflow.service.EditWorkflowService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
public class EditLookupInstanceController extends AbstractEditWorkflowController {

	@Autowired
	private EditWorkflowService editWorkflowService; 
	@Autowired
	private LookupInstanceService lookupInstanceService;
	@Autowired
	private EditLookupInstanceValidator validator;

	@InitBinder("form")
    private void initValidator(WebDataBinder binder) {
		// Enables AutoPopulatingList to grow its own list (e.g. selectedLookups) 
		binder.setAutoGrowNestedPaths(false);
    	binder.setValidator(validator);
    }
	
	/**
	 * Note the list of outcome status DTOs needs to be built first
	 * Sets the values on the selectedLookups array as needed
	 */
	private void setSelectedLookups(LookupInstance lookupInstance, EditLookupInstanceForm form) {
		int maxMessageCount = getMaxMessageCount();
		for (int index = 0; index < maxMessageCount; index++) {
			if (form.getSelectedLookups().size() == index) {
				form.getSelectedLookups().add(-1);
			}
		}
		// Make a map of outcome status ids to their index in the editing list
		Map<Integer,Integer> idsToPositions = new HashMap<>();
		int dtoIndex = 0;
		for (EditOutcomeStatusDto dto : form.getOutcomeStatusDtos()) {
			idsToPositions.put(dto.getId(), dtoIndex);
			dtoIndex++;
		}
		
		// For all the existing outcome status / lookups, set a list indicating which outcome status is selected
		// for each result message ID.
		for (LookupResult lr : lookupInstance.getLookupResults()) {
			dtoIndex = idsToPositions.get(lr.getOutcomeStatus().getId());
			form.getSelectedLookups().set(lr.getResultMessageId(), dtoIndex);
		}
	}

	/*
	 * Adds the OutcomeStatusDtos to the form 
	 * These should only exist editing a current lookup
	 * Note, unwired outcome status will not appear in the list
	 * TODO we should consider having some sort of sourceLookup in outcomestatus, to get directly.
	 * currently, this is done by activity, where it seems a lookup always follows a specific activity
	 * (but shouldn't have to - this way, a lookup could have an outcomestatus directly, and be wired anywhere in the workflow.)
	 * We could also consider - making lookup always belong to one activity, force in database, fix nulls.
	 * Multiple activities could connect to the same lookup currently.
	 * Note, some of the lookups result from other lookups.  
	 */
	private void setOutcomeStatusDtos(LookupInstance lookupInstance, EditLookupInstanceForm form) {
		Set<Integer> uniqueOsIds = new HashSet<>();
		for (LookupResult lr : lookupInstance.getLookupResults()) {
			OutcomeStatus os = lr.getOutcomeStatus();
			if (!uniqueOsIds.contains(os.getId())) {
				EditOutcomeStatusDto osDto = new EditOutcomeStatusDto();
				osDto.setId(os.getId());
				// Outcome status can be connected to a lookup, or to a status - but not both at once
				osDto.setLookupOrStatus(os.getLookup() != null);
				osDto.setLookupId(os.getLookup() != null ? os.getLookup().getId() : 0);
				osDto.setStatusId(os.getStatus() != null ? os.getStatus().getStateid() : 0);
				form.getOutcomeStatusDtos().add(osDto);
				uniqueOsIds.add(os.getId());
			}
		}
	}

	@ModelAttribute("form")
	public EditLookupInstanceForm formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		EditLookupInstanceForm form = new EditLookupInstanceForm();
		
		// Contains defaults for template row in adding 'new' outcome status
		EditOutcomeStatusDto templateOutcomeStatus = new EditOutcomeStatusDto();
		templateOutcomeStatus.setId(0);
		templateOutcomeStatus.setLookupId(0);
		templateOutcomeStatus.setLookupOrStatus(true);
		templateOutcomeStatus.setStatusId(0);
		form.setTemplateOutcomeStatus(templateOutcomeStatus);
		if (id == 0) {
			// Creating new record
			form.setLookupFunction(LookupFunction.values()[0]);
		}
		else {
			// Editing existing record, default to first defined lookup function
			LookupInstance lookupInstance = this.lookupInstanceService.get(id);
			setOutcomeStatusDtos(lookupInstance, form);
			setSelectedLookups(lookupInstance, form);
			form.setLookupFunction(lookupInstance.getLookupFunction());
			form.setDescription(lookupInstance.getDescription());
		}
		
		return form;
	}

	/*
	 * Return the maximum number of messages currently in use by all LookupFunctions in existence.
	 * This is used to determine how many columns to create for wiring OutcomeStatus,
	 * though not all of them may be selectable depending on the selected Lookup
	 */
	private int getMaxMessageCount() {
		int result = 0;
		
		for (LookupFunction function : LookupFunction.values()) {
			if (function.getLookupResultMessages().length > result) {
				result = function.getLookupResultMessages().length;
			}
		}
		return result;
	}
	
	private List<KeyValue<Boolean, String>> getSelectLookupOrStatus() {
		List<KeyValue<Boolean, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(Boolean.TRUE, "Lookup"));
		result.add(new KeyValue<>(Boolean.FALSE, "Status"));
		return result;
	}

	@RequestMapping(value="editlookupinstance.htm", method=RequestMethod.GET)
	public String showView(Model model,
			@ModelAttribute("form") EditLookupInstanceForm form,
			@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		model.addAttribute("maxMessageCount", getMaxMessageCount());
		model.addAttribute("selectLookupOrStatus", getSelectLookupOrStatus());
		model.addAttribute("itemStatuses", getItemStatuses());
		model.addAttribute("lookups", getLookups());
		model.addAttribute("lookupFunctions", LookupFunction.values());
		model.addAttribute("selectBoolean", getSelectBoolean());
		model.addAttribute("currentMessageCount", form.getLookupFunction().getLookupResultMessages().length);

		if (id != 0) {
			// Editing existing lookup instance
			LookupInstance lookupInstance = this.lookupInstanceService.get(id);
			model.addAttribute("lastLookup", lookupInstance);
		}
		return "trescal/core/workflow/edit/editlookupinstance";
	}
	

	@RequestMapping(value="editlookupinstance.htm", method=RequestMethod.POST)
	public String onSubmit(Model model,
			@Validated @ModelAttribute("form") EditLookupInstanceForm form,
			BindingResult bindingResult,
			@RequestParam(name="id", required=false, defaultValue="0") Integer id) {
		if (bindingResult.hasErrors()) {
			return showView(model, form, id);
		}
		Integer lookupId = null;
		if (id == 0) {
			// Creating new record
			lookupId = this.editWorkflowService.createLookupInstance(form);
		}
		else {
			// Editing existing record
			lookupId = id;
			this.editWorkflowService.updateLookupInstance(form, lookupId);
		}
		return "redirect:workfloweditor_lookup.htm?lookupId="+lookupId;
	}
	
}
