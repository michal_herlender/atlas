package org.trescal.cwms.core.workflow.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@JsonController
public class ActionOutcomesJsonController {

	@Autowired
	private ItemStateService isService;
	@Autowired
	private ActionOutcomeService aoService;

	@RequestMapping(value = "/actionOutcomes.json", method = RequestMethod.GET)
	@ResponseBody
	public List<KeyValue<Integer, String>> doGet(@RequestParam(name = "id", required = true) Integer id,
			Locale locale) {
		if (id == null)
			return null;
		ItemActivity it = isService.findItemActivity(id);
		return aoService.getActionOutcomesByType(it.getActionOutcomeType(), locale);
	}

}
