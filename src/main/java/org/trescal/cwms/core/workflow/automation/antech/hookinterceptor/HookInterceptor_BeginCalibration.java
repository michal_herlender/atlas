package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_BeginCalibration extends HookInterceptor<HookInterceptor_BeginCalibration.Input> {

	public static final String HOOK_NAME = "begin-calibration";

	@Autowired
	private JobItemService jiServ;

	public static class Input {
		public Calibration cal;
		public boolean resuming;
		public Contact contact;
		public Date startStamp;
		public Date endStamp;
		public String remark;

		public Input(Calibration cal, boolean resuming, Contact contact, Date startStamp, Date endStamp,
				String remark) {
			super();
			this.cal = cal;
			this.resuming = resuming;
			this.contact = contact;
			this.startStamp = startStamp;
			this.endStamp = endStamp;
			this.remark = remark;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: BEGIN CALIBRATION");
			Calibration cal = input.cal;
			boolean resuming = input.resuming;
			Contact con = input.contact != null ? input.contact : this.sessionServ.getCurrentContact();

			for (CalLink cl : cal.getLinks()) {
				JobItem ji = cl.getJi();
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

				// only record activity if item is 'Awaiting calibration' or
				// similar. in theory base units could be going off down the
				// repair route in which case we cannot record the activity
				// because this will take it off course! However, make an
				// exception for resuming on-hold calibrations because these
				// items will not technically be "ready for cal"...
				if (this.jiServ.isReadyForCalibration(ji) || resuming) {
					JobItemActivity jia = new JobItemActivity();
					jia.setActivity(context.getActivity());
					jia.setActivityDesc(context.getActivity().getDescription());
					jia.setStartStatus((ItemStatus) ji.getState());
					jia.setStartStamp(input.startStamp != null ? input.startStamp : new Date());
					jia.setStartedBy(con);
					jia.setCreatedOn(new Date());
					jia.setJobItem(ji);
					jia.setComplete(context.isCompleteActivity());
					jia.setRemark(input.remark);

					if (context.isCompleteActivity()) {
						if (input.endStamp != null)
							jia.setEndStamp(input.endStamp);
						else
							jia.setEndStamp(new Date());
						jia.setTimeSpent(5);
						jia.setCompletedBy(con);
						jia.setOutcome(cl.getOutcome());
					}

					

					ItemStatus endStatus = null;
					if (context.isCompleteActivity()) {
						endStatus = super.getNextStatus(context);
						ji.setState(endStatus);
						jia.setEndStatus(endStatus);
					} else
						ji.setState(context.getActivity());
					
					super.persist(jia, endStatus, context);
					
					// adds the action into the job item in the session
					ji.getActions().add(jia);
					jobItemService.updateJobItem(ji);
				}
			}
		}
	}

	public void setJiServ(JobItemService jiServ) {
		this.jiServ = jiServ;
	}
}