package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_NoOperationPerformed.Input;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.TreeSet;

@Service
public class HookInterceptor_NoOperationPerformed extends HookInterceptor<Input> {
	public static final String HOOK_NAME = "no-operation-performed";

	public static class Input {

		public JobItem ji;
		public String comment;
		public ActionOutcome actionOutcome;
		public Integer timeSpent;
		public Date startDate;

		public Input(JobItem ji, String comment, ActionOutcome actionOutcome, Date startDate, Integer timeSpent) {
			this.ji = ji;
			this.comment = comment;
			this.actionOutcome = actionOutcome;
			this.startDate = startDate;
			this.timeSpent = timeSpent;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: NO ACTION REQUIRED FOR CALIBRATION");
			Contact con = this.sessionServ.getCurrentContact();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, input.ji);

			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) input.ji.getState());
			if (input.startDate == null)
				jia.setStartStamp(new Date());
			else
				jia.setStartStamp(input.startDate);
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(input.ji);
			jia.setComplete(context.isCompleteActivity());
			jia.setRemark(input.comment);
			jia.setOutcome(input.actionOutcome);

			if (context.isCompleteActivity()) {
				if (input.timeSpent == null)
					jia.setTimeSpent(5);
				else
					jia.setTimeSpent(input.timeSpent);
				jia.setEndStamp(DateUtils.addMinutes(jia.getStartStamp(), jia.getTimeSpent()));

				jia.setCompletedBy(con);
			}

			// adds the action into the job item in the session
			if (input.ji.getActions() == null) {
				input.ji.setActions(new TreeSet<>(new JobItemActionComparator()));
			}
			input.ji.getActions().add(jia);
			this.jobItemActionService.save(jia);

			ItemStatus endStatus = super.getNextStatus(context);
			input.ji.setState(endStatus);

			jobItemService.updateJobItem(input.ji);

			if (context.isCompleteActivity()) {
				jia.setEndStatus(endStatus);
			}

			this.persist(jia, endStatus, context);
		}
	}
}
