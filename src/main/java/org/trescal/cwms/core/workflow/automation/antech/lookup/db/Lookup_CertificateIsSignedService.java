package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CertificateIsSigned;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_CertificateIsSigned")
public class Lookup_CertificateIsSignedService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item's most recent certificate has been signed or not.";
	}

	@Override
	public List<LookupResultMessage> getLookupResultMessages() {
		List<LookupResultMessage> result = new ArrayList<LookupResultMessage>();
		result.add(Lookup_CertificateIsSigned.SIGNED);
		result.add(Lookup_CertificateIsSigned.NOT_SIGNED);
		return result;
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: CERTIFICATE IS SIGNED");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		// TODO: Further troubleshoot - for on-site calibration, cert links appear empty
		// when cert first created?
		//
		Certificate cert = null;
		if ((ji.getCertLinks() != null) && !ji.getCertLinks().isEmpty()) {
			cert = ji.getCertLinks().last().getCert();
		}
		if (cert == null) {
			boolean certLinksNull = (ji.getCertLinks() == null);
			logger.error("No cert found for job item, cert links null : " + certLinksNull);
			if (!certLinksNull)
				logger.error("Job item cert links size " + ji.getCertLinks().size());
			return Lookup_CertificateIsSigned.NOT_SIGNED;
		} else if (cert.getStatus().equals(CertStatusEnum.DELETED)) {
			return Lookup_CertificateIsSigned.SIGNED;
		} else {
			return cert.getStatus().equals(CertStatusEnum.SIGNED) ? Lookup_CertificateIsSigned.SIGNED
					: Lookup_CertificateIsSigned.NOT_SIGNED;
		}
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_CertificateIsSigned.values();
	}
}