package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientFailureReportOutcomePreselected;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ClientFailureReportOutcomePreselected")
public class Lookup_ClientFailureReportOutcomePreselectedService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {

		logger.debug("RUNNING LOOKUP: Client Failure Report Outcome Preselected");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);

		if (fr.getOutcomePreselected() != null && fr.getOutcomePreselected()) {
			return Lookup_ClientFailureReportOutcomePreselected.YES;
		} else {
			return Lookup_ClientFailureReportOutcomePreselected.NO;
		}
	}

	@Override
	public String getDescription() {
		return "Decides whether the outcome of the failure report is preselected (we will continue with the failure report recommendation)";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ClientFailureReportOutcomePreselected.values();
	}
}