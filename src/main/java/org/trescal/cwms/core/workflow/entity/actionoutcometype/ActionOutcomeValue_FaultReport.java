package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

@Deprecated
public enum ActionOutcomeValue_FaultReport implements ActionOutcomeValue {
	
	ERP_CALIBRATION("Calibration","actionoutcome.faultreport.erpcalibration"),
	ERP_REPAIR("Repair","actionoutcome.faultreport.repair"),
	ERP_ADJUSTMENT("Adjustment","actionoutcome.faultreport.inhouseadjustment"),
	THIRD_PARTY("Third party","actionoutcome.faultreport.thirdparty"),
	SCRAP("Scrap","actionoutcome.faultreport.scrap"),
	RESTRICTION("Restriction","actionoutcome.faultreport.restriction"),
	RETURN_TO_CLIENT("Return to client","actionoutcome.faultreport.returntoclient");
	
	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_FaultReport(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.FAULT_REPORT;
	}


}
