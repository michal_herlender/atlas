package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_GeneralServiceOperationResult;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionForGeneralServiceOperation;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_GeneralServiceOperation;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

@Component("Lookup_GeneralServiceOperationResult")
public class Lookup_GeneralServiceOperationResultService extends Lookup {

	@Autowired
	private WorkRequirementService wrService;
	@Autowired
	private JobItemService jiService;

	@Override
	public String getDescription() {
		return "Determines the outcome of general service operation.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: GENERAL SERVICE OPERATION RESULT");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		boolean hasGsoDocument = false;
		ActionOutcome gsoActionOutcome = null;
		ActionOutcome gsoClientDecisionActionOutcome = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions
		activityLoop: for (JobItemAction action : actions.descendingSet()) {
			// in some legacy cases, activity may be null!
			if (action.getActivity() != null) {
				if (!action.isDeleted() && action instanceof JobItemActivity) {
					// get the outcome
					JobItemActivity temp = (JobItemActivity) action;
					if (temp.getOutcome() != null) {
						if (temp.getOutcome().getType()
								.equals(ActionOutcomeType.CLIENT_DECISION_GENERAL_SERVICE_OPERATION))
							gsoClientDecisionActionOutcome = temp.getOutcome();
						else if (temp.getOutcome().getType().equals(ActionOutcomeType.GENERAL_SERVICE_OPERATION)) {
							gsoActionOutcome = temp.getOutcome();
							break activityLoop;
						}
					}
					else if(this.jiService.stateHasGroupOfKeyName(temp.getActivity(), StateGroup.GSO_DOCUMENT))
						hasGsoDocument = true;
				}
			}
		}

		LookupResultMessage result;
		if (ActionOutcomeValue_GeneralServiceOperation.COMPLETED_SUCCESSFULLY
				.equals(gsoActionOutcome.getGenericValue())) {
			// check current workrequirement if client decision is required and accepted or
			// rejected, else if client decision is not required 
			if ((gsoClientDecisionActionOutcome != null && (gsoClientDecisionActionOutcome.getGenericValue()
					.equals(ActionOutcomeValue_ClientDecisionForGeneralServiceOperation.ACCEPTED)
					|| gsoClientDecisionActionOutcome.getGenericValue()
							.equals(ActionOutcomeValue_ClientDecisionForGeneralServiceOperation.REJECTED)))
					|| hasGsoDocument) {
				this.wrService.checkCurrentWorkRequirement(ji);
			}
			result = Lookup_GeneralServiceOperationResult.COMPLETED;
		} else if (ActionOutcomeValue_GeneralServiceOperation.COMPLETED_UNSUCCESSFULLY
				.equals(gsoActionOutcome.getGenericValue()))
			result = Lookup_GeneralServiceOperationResult.NEEDS_FAILURE_REPORT;
		else if (ActionOutcomeValue_GeneralServiceOperation.FAILED_REDO.equals(gsoActionOutcome.getGenericValue()))
			result = Lookup_GeneralServiceOperationResult.CANCELLED;
		else
			result = Lookup_GeneralServiceOperationResult.ON_HOLD;
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_GeneralServiceOperationResult.values();
	}
}