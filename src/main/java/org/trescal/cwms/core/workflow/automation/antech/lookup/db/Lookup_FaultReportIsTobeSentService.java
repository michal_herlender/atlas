package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.failurereport.enums.FailureReportApprovalTypeEnum;
import org.trescal.cwms.core.failurereport.enums.SentFaultReportSettingEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.events.AutomaticFaultReportGenerationEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SendFaultReportToClient;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FaultReportIsTobeSent;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

import java.util.Date;

@Component("Lookup_FaultReportIsTobeSent")
public class Lookup_FaultReportIsTobeSentService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private SendFaultReportToClient systemDefaultSendFRToClient;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {

		logger.debug("RUNNING LOOKUP: Fault Report Is To Be Sent");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);

		if (fr.getSendToClient() != null) {
			// generate PDF document of FR if linked to adveso
			boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
					ji.getJob().getCon().getSub().getSubdivid(), ji.getJob().getOrganisation().getComp().getCoid(),
					 new Date());
			if (fr.getSendToClient() == true && isLinkedToAdveso) {
				AutomaticFaultReportGenerationEvent automaticGenerationEvent = new AutomaticFaultReportGenerationEvent(
						this, fr, false);
				applicationEventPublisher.publishEvent(automaticGenerationEvent);
			}

			boolean informationalFR = fr.getSendToClient() == true && !isLinkedToAdveso
					&& BooleanUtils.isTrue(fr.getOutcomePreselected());
			if (fr.getSendToClient() == false || informationalFR) {
				if (informationalFR)
					this.faultReportService.setInformationalFRToBeSent(fr);
				return Lookup_FaultReportIsTobeSent.NO;
			} else {
				return Lookup_FaultReportIsTobeSent.CSR_DESICION;
			}
		}

		String sdTextValue = systemDefaultSendFRToClient.getValueHierarchical(Scope.COMPANY,
				ji.getJob().getCon().getSub().getComp(), ji.getJob().getOrganisation().getComp()).getValue();
		SentFaultReportSettingEnum set = systemDefaultSendFRToClient.parseValue(sdTextValue);
		if (set.equals(SentFaultReportSettingEnum.NO)) {
			fr.setApprovalType(FailureReportApprovalTypeEnum.NOT_NECESSARY);
			faultReportService.updateFaultReport(fr);
			return Lookup_FaultReportIsTobeSent.NO;
		} else
			return Lookup_FaultReportIsTobeSent.CSR_DESICION;
	}

	@Override
	public String getDescription() {
		return "based on company setting, decide whether not to send the fault report, or let the CSR decides";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_FaultReportIsTobeSent.values();
	}
}
