package org.trescal.cwms.core.workflow.entity.lookupresult.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;

public interface LookupResultDao extends BaseDao<LookupResult, Integer> {
	
	public LookupResult findLookupResultForInstanceAndMessage(Integer lookupInstanceId, LookupResultMessage result);
	
	List<LookupResult> getAllLookupResultsForActivity(int activityId);
}