package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.external.adveso.events.CompleteCalibrationEvent;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationActivityLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.db.CalibrationActivityLinkService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_CompleteCalibration extends HookInterceptor<HookInterceptor_CompleteCalibration.Input> {

	public static final String HOOK_NAME = "complete-calibration";

	@Autowired
	private CalibrationActivityLinkService calibrationActivityLinkService;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	public static class Input {
		public Calibration cal;
		public CalibrationStatus oldStatus;
		public String remark;
		public Integer timeIncrement;
		public Contact contact;
		public Date startStamp;
		public Date endStamp;

		public Input(Calibration cal, CalibrationStatus oldStatus, String remark) {
			this.cal = cal;
			this.oldStatus = oldStatus;
			this.remark = remark;
		}

		public Input(Calibration cal, CalibrationStatus oldStatus, String remark, Integer timeIncrement,
				Contact contact, Date startStamp, Date endDate) {
			this.cal = cal;
			this.oldStatus = oldStatus;
			this.remark = remark;
			this.timeIncrement = timeIncrement;
			this.contact = contact;
			this.startStamp = startStamp;
			this.startStamp = startStamp;
			this.endStamp = endDate;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: COMPLETE CALIBRATION");
			Contact con = input.contact != null ? input.contact : this.sessionServ.getCurrentContact();
			Calibration cal = input.cal;
			CalibrationStatus oldStatus = input.oldStatus;
			String remark = input.remark;

			for (CalLink cl : cal.getLinks()) {
				JobItem ji = cl.getJi();

				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);
				JobItemActivity jia = null;

				for (JobItemAction action : ji.getActions()) {
					if (action instanceof JobItemActivity) {
						JobItemActivity activity = (JobItemActivity) action;
						if (!activity.isComplete() && activity.getActivity().getDescription()
								.equals(context.getActivity().getDescription())) {
							jia = activity;
							break;
						}
					}
				}

				ItemStatus endStatus = null;
				if (context.isCompleteActivity() && (jia != null)) {

					if (input.startStamp != null)
						jia.setStartStamp(input.startStamp);
					if (input.endStamp != null)
						jia.setEndStamp(input.endStamp);
					else
						jia.setEndStamp(new Date());

					int mins = (cl.getTimeSpent() != null) ? cl.getTimeSpent()
							: DateTools.getMinutesDifference(jia.getStartStamp(), jia.getEndStamp());

					// if the time increment exists it means that we're
					// registering a time increment from TLM
					jia.setTimeSpent(input.timeIncrement != null ? input.timeIncrement : mins);
					jia.setCompletedBy(con);
					jia.setComplete(true);
					jia.setOutcome(cl.getOutcome());

					if ((remark != null) && !remark.trim().isEmpty()) {
						jia.setRemark(remark);
					}

					endStatus = super.getNextStatus(context);

					ji.setState(endStatus);
					jobItemService.updateJobItem(ji);

					jia.setEndStatus(endStatus);

					this.persist(jia, endStatus, context);

					// link the jia with the calibration
					CalibrationActivityLink calibrationActivityLink = new CalibrationActivityLink(cal, jia);
					calibrationActivityLinkService.persist(calibrationActivityLink);

					// publish a new Complete Calibration Event only if the outcome is SUCCESSFUL or
					// NOT_SUCCESSFUL
					if (cl.getOutcome() != null
							&& (ActionOutcomeValue_Calibration.SUCCESSFUL.equals(cl.getOutcome().getGenericValue())
									|| ActionOutcomeValue_Calibration.NOT_SUCCESSFUL
											.equals(cl.getOutcome().getGenericValue()))) {
						CompleteCalibrationEvent completeCalEvent = new CompleteCalibrationEvent(this, cal, oldStatus);
						applicationEventPublisher.publishEvent(completeCalEvent);
					}
				}
			}
		}
	}
}