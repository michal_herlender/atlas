package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HookInterceptor_SignCertificate extends HookInterceptor<HookInterceptor_SignCertificate.Input> {
	public static final String HOOK_NAME = "sign-certificate";

	public static class Input {
		public Certificate certificate;
		public Contact contact;
		public Integer timeSpent;
		public Date startStamp;

		public Input(Certificate certificate, Contact contact) {
			super();
			this.certificate = certificate;
			this.contact = contact;
		}

		public Input(Certificate certificate, Contact contact, Integer timeSpent, Date startStamp) {
			super();
			this.certificate = certificate;
			this.contact = contact;
			this.timeSpent = timeSpent;
			this.startStamp = startStamp;
		}

	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: SIGN CERTIFICATE");

			Contact con;
			if (input.contact != null)
				con = input.contact;
			else
				con = this.sessionServ.getCurrentContact();

			List<JobItem> jobItems = input.certificate.getLinks().stream().map(CertLink::getJobItem)
				.collect(Collectors.toList());
			for (JobItem ji : jobItems) {
				HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setStartStatus((ItemStatus) ji.getState());
				if (input.startStamp == null)
					jia.setStartStamp(new Date());
				else
					jia.setStartStamp(input.startStamp);
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());

				if (context.isCompleteActivity()) {
					//end date
					if (input.startStamp != null && input.timeSpent != null)
						jia.setEndStamp(DateUtils.addMinutes(input.startStamp, input.timeSpent));
					else
						jia.setEndStamp(new Date());
					//time spent
					if (input.timeSpent == null)
						jia.setTimeSpent(5);
					else
						jia.setTimeSpent(input.timeSpent);
					jia.setCompletedBy(con);
				}

				ItemStatus endStatus = super.getNextStatus(context);
				ji.setState(endStatus);
				jobItemService.mergeJobItem(ji);
				if (context.isCompleteActivity()) {
					jia.setEndStatus(endStatus);
				}

				this.persist(jia, endStatus, context);
			}
		}
	}
}