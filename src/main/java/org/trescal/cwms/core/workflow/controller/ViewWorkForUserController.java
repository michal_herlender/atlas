package org.trescal.cwms.core.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.upcomingwork.db.UpcomingWorkService;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller used to display different lists of work that can be applied to the
 * given {@link Contact}. This will currently load all different lists as part
 * of the same request, but may in the future need to be updated to display this
 * dynamically using Ajax.
 * <p>
 * TODO add work assigned to a contact by a lab manager.
 */
@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ViewWorkForUserController
{
	@Value("#{props['cwms.config.jobitem.fast_track_turn']}")
	private int fastTrackTurn;
	@Autowired
	private CalibrationService calServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private DepartmentService deptServ;
	@Autowired
	private EngineerAllocationService eaServ;
	@Autowired
	private JobItemActivityService jobItemActServ;
	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UpcomingWorkService uwServ;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/viewworkforuser.htm")
	protected ModelAndView handleRequestInternal(
			@RequestParam(value="personid", required=false, defaultValue="0") Integer personId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception
	{
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Contact contact;
		if (personId == 0) {
			contact = userService.get(username).getCon();
			if (contact == null) throw new Exception("Cannot find currently logged in contact");
			personId = contact.getPersonid();
		}
		else {
			contact = this.conServ.get(personId);
			if (contact == null) throw new Exception("Cannot find contact with personid = " + personId);
		}
        Map<String, Object> model = new HashMap<>();
		model.put("contact", contact);
		List<Department> depts = this.deptServ.getByTypeAndSubdiv(DepartmentType.LABORATORY, allocatedSubdiv.getId());
		LocalDate maxAllocatedFor = calculateMaxAllocatedFor();
		model.put("depts", depts);
		// get all items that lab manager has allocated to engineer
		model.put("allocatedwork", this.eaServ.getActiveAllocationsForEngineer(personId, maxAllocatedFor));
		// get all ongoing activities started by the user
		model.put("ongoingactivities", this.jobItemActServ.getAllIncompleteActivityForContact(contact));
		// get all ongoing calibrations started by the user
		model.put("ongoingcals", this.calServ.getAllOngoingCalibrationsByContact(personId));
		// get all work available to this user
		model.put("available", this.jobItemServ.getAvailableTaskList(contact));
		// get all future upcoming work
		model.put("upcomingwork", this.uwServ.getUpcomingWorkForUser(personId, allocatedSubdiv));
		// get all business contacts
		model.put("businesscontacts", this.conServ.getAllActiveSubdivContacts(allocatedSubdiv.getId()));
		// get fast track turnaround from properties
		model.put("fastTrackTurn", this.fastTrackTurn);
		return new ModelAndView("/trescal/core/workflow/viewworkforuser", "command", model);
	}
	
	private LocalDate calculateMaxAllocatedFor() {
		LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		return today.plusDays(Constants.FUTURE_ALLOCATION_DAYS);
	}
}