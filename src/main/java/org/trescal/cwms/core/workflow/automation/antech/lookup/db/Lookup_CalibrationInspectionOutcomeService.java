package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_CalibrationInspectionOutcome;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Component("Lookup_CalibrationInspectionOutcome")
public class Lookup_CalibrationInspectionOutcomeService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines the outcome of the item's inspection prior to calibration.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: CALIBRATION INSPECTION OUTCOME");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		ActionOutcome ao = null;
		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		// loop backwards through actions to get the MOST RECENT calibration
		// inspection
		// in case there is more than one
		for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;
				if (activity.getActivityDesc().equals("Unit inspected by engineer prior to calibration")
						&& !activity.isDeleted()) {
					// get the outcome of the contract review
					ao = activity.getOutcome();
					break;
				}
			}
		}
		String desc = ao.getDescription();
		return desc.contains("ready for calibration") ? Lookup_CalibrationInspectionOutcome.INHOUSE_CALIBRATION
				: desc.contains("third party calibration") ? Lookup_CalibrationInspectionOutcome.THIRDPARTY_CALIBRATION
						: desc.contains("third party repair") ? Lookup_CalibrationInspectionOutcome.THIRDPARTY_REPAIR
								: desc.contains("in-house repair") ? Lookup_CalibrationInspectionOutcome.INHOUSE_REPAIR
										: desc.contains("adjustment") ? Lookup_CalibrationInspectionOutcome.ADJUSTMENT
												: desc.contains("B.E.R.") ? Lookup_CalibrationInspectionOutcome.UNIT_BER
														: null;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_CalibrationInspectionOutcome.values();
	}
}