package org.trescal.cwms.core.workflow.form;

import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class CleaningJobItem
{
	private List<Boolean> cleaneds;
	private List<JobItem> jis;
	private List<String> plantids;
	
	public List<Boolean> getCleaneds() {
		return cleaneds;
	}
	public void setCleaneds(List<Boolean> cleaneds) {
		this.cleaneds = cleaneds;
	}
	public List<JobItem> getJis() {
		return jis;
	}
	public void setJis(List<JobItem> jis) {
		this.jis = jis;
	}
	public List<String> getPlantids() {
		return plantids;
	}
	public void setPlantids(List<String> plantids) {
		this.plantids = plantids;
	}
	
}