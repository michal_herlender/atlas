package org.trescal.cwms.core.workflow.form.edit;

import javax.validation.constraints.NotNull;

/*
 * No need to update the activityId or lookupId - these are all the same for one editing context.
 */
public class EditLookupResultDto {
	private Integer id;
	private Integer outcomeId;
	private Integer resultMessageId;
	
	@NotNull
	public Integer getId() {
		return id;
	}
	@NotNull
	public Integer getOutcomeId() {
		return outcomeId;
	}
	@NotNull
	public Integer getResultMessageId() {
		return resultMessageId;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setOutcomeId(Integer outcomeId) {
		this.outcomeId = outcomeId;
	}
	public void setResultMessageId(Integer resultMessageId) {
		this.resultMessageId = resultMessageId;
	}
}
