package org.trescal.cwms.core.workflow.form.edit;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;

public class AbstractItemStateForm {

	private Boolean active;
	private String description;
	private Boolean retired;
	private List<EditStateGroupLinkDto> stateGroupLinks;
	private StateGroup defaultStateGroup;
	private StateGroupLinkType defaultLinkType;
	private Map<Locale,String> translations;

	@NotNull
	public Boolean getActive() {
		return active;
	}

	@NotNull
	public Boolean getRetired() {
		return retired;
	}

	@NotNull
	@Length(min=1 , max=150)
	public String getDescription() {
		return description;
	}

	public Map<Locale, String> getTranslations() {
		return translations;
	}

	@Valid
	public List<EditStateGroupLinkDto> getStateGroupLinks() {
		return stateGroupLinks;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setRetired(Boolean retired) {
		this.retired = retired;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTranslations(Map<Locale, String> translations) {
		this.translations = translations;
	}

	public void setStateGroupLinks(List<EditStateGroupLinkDto> stateGroupLinks) {
		this.stateGroupLinks = stateGroupLinks;
	}

	public StateGroup getDefaultStateGroup() {
		return defaultStateGroup;
	}

	public StateGroupLinkType getDefaultLinkType() {
		return defaultLinkType;
	}

	public void setDefaultStateGroup(StateGroup defaultStateGroup) {
		this.defaultStateGroup = defaultStateGroup;
	}

	public void setDefaultLinkType(StateGroupLinkType defaultLinkType) {
		this.defaultLinkType = defaultLinkType;
	}

}
