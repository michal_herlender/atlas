package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_FaultReportIsApprovedByClient;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_FaultReportIsApprovedByClient")
public class Lookup_FaultReportIsApprovedByClientService extends Lookup {

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Fault Report In Approved By Client");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		return "Determines where to go, depending on the client approval of the fault report";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_FaultReportIsApprovedByClient.values();
	}

}