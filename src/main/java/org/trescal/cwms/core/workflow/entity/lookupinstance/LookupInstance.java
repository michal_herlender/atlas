package org.trescal.cwms.core.workflow.entity.lookupinstance;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupFunction;
import org.trescal.cwms.core.workflow.entity.lookupresult.LookupResult;

@Entity
@Table(name = "lookupinstance")
public class LookupInstance
{
	private int id;
	private LookupFunction lookupName;
	private Set<LookupResult> lookupResults;
	private String description;

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}
	
	@NotNull
	@Column(name = "lookupfunction", nullable = false)
	@Enumerated(EnumType.ORDINAL)
	public LookupFunction getLookupFunction()
	{
		return this.lookupName;
	}
	
	@OneToMany(mappedBy = "lookup", cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.LAZY)
	@OrderBy("id, resultmessage")
	public Set<LookupResult> getLookupResults()
	{
		return this.lookupResults;
	}

	/**
	 * Formerly used for lookup function, can now use as description (any text desired) 
	 * to differentiate different lookups with the same function 
	 */
	@NotNull
	@Column(name="lookup", length=100, nullable=false)
	public String getDescription() {
		return description;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setLookupFunction(LookupFunction lookupName)
	{
		this.lookupName = lookupName;
	}

	public void setLookupResults(Set<LookupResult> lookupResults) {
		this.lookupResults = lookupResults;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}