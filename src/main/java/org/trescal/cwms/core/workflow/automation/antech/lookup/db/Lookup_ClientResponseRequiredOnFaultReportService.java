package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.failurereport.enums.FailureReportApprovalTypeEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ClientResponseRequiredOnFaultReport;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

import java.util.TreeSet;

@Component("Lookup_ClientResponseRequiredOnFaultReport")
public class Lookup_ClientResponseRequiredOnFaultReportService extends Lookup {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {

		logger.debug("RUNNING LOOKUP: Client Response Required On Fault Report");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);

		TreeSet<JobItemAction> actions = new TreeSet<JobItemAction>(new JobItemActionComparator());
		actions.addAll(ji.getActions());
		for (JobItemAction action : actions.descendingSet()) {
			if (action instanceof JobItemActivity) {
				JobItemActivity activity = (JobItemActivity) action;
				if (StringUtils.containsIgnoreCase(activity.getActivityDesc(), "Report sent to client")
						&& !activity.isDeleted()) {
					// default to email
					fr.setApprovalType(FailureReportApprovalTypeEnum.EMAIL);
					faultReportService.updateFaultReport(fr);
					break;
				} else if (StringUtils.containsIgnoreCase(activity.getActivityDesc(),
						"Report is available to External Management System") && !activity.isDeleted()) {
					fr.setApprovalType(FailureReportApprovalTypeEnum.ADVESO);
					faultReportService.updateFaultReport(fr);
					break;
				}
			}
		}
		LookupResultMessage lrm;
		if (fr != null && (!fr.getClientResponseNeeded() || fr.getOutcomePreselected())) {
			lrm = Lookup_ClientResponseRequiredOnFaultReport.NO;
		} else {
			lrm = Lookup_ClientResponseRequiredOnFaultReport.YES;
		}

		return lrm;
	}

	@Override
	public String getDescription() {
		return "Determines where to go, depending if the client advice is needed for approving the fault report";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ClientResponseRequiredOnFaultReport.values();
	}
}