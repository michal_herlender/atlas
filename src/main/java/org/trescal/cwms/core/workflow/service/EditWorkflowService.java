package org.trescal.cwms.core.workflow.service;

import org.trescal.cwms.core.workflow.form.edit.EditActionOutcomeTranslationsForm;
import org.trescal.cwms.core.workflow.form.edit.EditHookForm;
import org.trescal.cwms.core.workflow.form.edit.EditItemActivityForm;
import org.trescal.cwms.core.workflow.form.edit.EditItemStatusForm;
import org.trescal.cwms.core.workflow.form.edit.EditLookupInstanceForm;

public interface EditWorkflowService {
	int createHook(EditHookForm form);
	int createItemActivity(EditItemActivityForm form);
	int createItemStatus(EditItemStatusForm form);
	int createLookupInstance(EditLookupInstanceForm form);
	void updateActionOutcomeTranslations(EditActionOutcomeTranslationsForm form);
	void updateHook(EditHookForm form, int hookId);
	void updateItemActivity(EditItemActivityForm form, int activityId);
	void updateItemStatus(EditItemStatusForm form, int stateId);
	void updateLookupInstance(EditLookupInstanceForm form, int lookupId);
}
