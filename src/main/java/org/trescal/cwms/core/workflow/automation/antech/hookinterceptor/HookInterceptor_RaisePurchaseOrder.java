package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_RaisePurchaseOrder extends HookInterceptor<PurchaseOrderJobItem> {
	public static final String HOOK_NAME = "raise-purchase-order";

	@Autowired
	private MessageSource messageSource;

	@Transactional
	@Override
	public void recordAction(PurchaseOrderJobItem poji) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: RAISE PURCHASE ORDER");
			Contact con = this.sessionServ.getCurrentContact();
			if (con == null) {
				this.logger.info("contact null");
			} else {
				this.logger.info(con.getId());
			}

			JobItem ji = poji.getJi();
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			JobItemActivity jia = new JobItemActivity();
			jia.setActivity(context.getActivity());
			jia.setActivityDesc(context.getActivity().getDescription());
			jia.setStartStatus((ItemStatus) ji.getState());
			jia.setStartStamp(new Date());
			jia.setStartedBy(con);
			jia.setCreatedOn(new Date());
			jia.setJobItem(ji);
			jia.setComplete(context.isCompleteActivity());
			Locale messageLocale = ji.getJob().getOrganisation().getComp().getDocumentLanguage() != null
					? ji.getJob().getOrganisation().getComp().getDocumentLanguage()
					: con.getLocale();
			jia.setRemark(this.messageSource.getMessage("purchaseorder.ponum", null, "Purchase Order :", messageLocale)
					+ " : " + poji.getPoitem().getOrder().getIdentifier() + ".");

			if (context.isCompleteActivity()) {
				jia.setEndStamp(new Date());
				jia.setTimeSpent(5);
				jia.setCompletedBy(con);
			}

			ItemStatus endStatus = null;
			if (context.isCompleteActivity()) {
				endStatus = super.getNextStatus(context);
				ji.setState(endStatus);
				jia.setEndStatus(endStatus);
			} else
				ji.setState(context.getActivity());
			jobItemService.updateJobItem(ji);
			this.persist(jia, endStatus, context);
		}
	}
}