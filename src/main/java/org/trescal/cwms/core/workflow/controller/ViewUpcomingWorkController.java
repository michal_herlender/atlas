package org.trescal.cwms.core.workflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactComparator;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.DepartmentComparator;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLinkAjaxDto;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.db.UpcomingWorkJobLinkService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.EngineerAllocationWrapper;
import org.trescal.cwms.core.system.dto.UpcomingWorkWrapper;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;
import org.trescal.cwms.core.system.entity.upcomingwork.db.UpcomingWorkService;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class ViewUpcomingWorkController {
	private final static int numOfWeeksDisplayed = 4;
	@Autowired
	private ContactService contactService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private EngineerAllocationService engineerAllocationService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UpcomingWorkService upcomingWorkService;
	@Autowired
	private UpcomingWorkJobLinkService upcomingWorkJobLinkService;

	@RequestMapping(value = "/viewupcomingwork.htm")
	protected ModelAndView handleRequestInternal(@RequestParam(value = "startdate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
												 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Map<String, Object> model = new HashMap<>();
		TreeMap<Integer, List<LocalDate>> dateList = new TreeMap<>();
		TreeMap<Integer, List<UpcomingWorkWrapper>> workList = new TreeMap<>();
		TreeMap<Integer, List<EngineerAllocationWrapper>> allocationList = new TreeMap<>();
		// If no start date provided, we use today's date (for user's time zone)
		if (startDate == null) {
			startDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		}
		LocalDate prev = startDate.with(DayOfWeek.MONDAY).minusWeeks(numOfWeeksDisplayed);

		for (int j = 0; j < numOfWeeksDisplayed; j++) {

			LocalDate monday = startDate.with(DayOfWeek.MONDAY).plusWeeks(j);
			List<LocalDate> dates = Stream.iterate(monday, d -> d.plusDays(1)).limit(5).collect(Collectors.toList());
			LocalDate friday = monday.with(DayOfWeek.FRIDAY);
			List<UpcomingWork> upcomingWorkBetweenDates = this.upcomingWorkService.getUpcomingWorkBetweenDates(monday, friday, allocatedSubdiv);
			List<EngineerAllocation> engineerAllocationsBetweenDates = this.engineerAllocationService.getEngineerAllocationsBetweenDates(monday, friday, allocatedSubdiv);
			List<UpcomingWorkWrapper> upcomingWorkWrappers = new ArrayList<>();
			for (UpcomingWork upcomingWork : upcomingWorkBetweenDates) {
				int start = 0;
				int end = 0;
				for (int i = 0; i < dates.size(); i++) {
					LocalDate localDate = dates.get(i);
					if ((upcomingWork.getStartDate() != null) && upcomingWork.getStartDate().isEqual(localDate)) {
						start = i + 1;
					}
					if (upcomingWork.getDueDate().isEqual(localDate)) {
						end = i + 1;
					}
				}

				if ((upcomingWork.getStartDate() != null) && upcomingWork.getStartDate().isBefore(monday)) {
					start = 1;
				}
				if (upcomingWork.getDueDate().isAfter(friday)) {
					end = dates.size();
				}
				List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos = upcomingWorkJobLinkService.getJobByUpcomingWork(upcomingWork);
				upcomingWorkWrappers.add(new UpcomingWorkWrapper(upcomingWork, (start == 0) ? end : start, end, upcomingWorkJobLinkAjaxDtos));
			}

			List<EngineerAllocationWrapper> engAllocWraps = new ArrayList<>();
			for (EngineerAllocation engineerAllocation : engineerAllocationsBetweenDates) {
				int start = 0;
				int end = 0;
				for (int i = 0; i < dates.size(); i++) {
					LocalDate localDate = dates.get(i);
					if (engineerAllocation.getAllocatedFor().isEqual(localDate)) {
						start = i + 1;
					}
					if (engineerAllocation.getAllocatedUntil().isEqual(localDate)) {
						end = i + 1;
					}
				}
				if (engineerAllocation.getAllocatedFor().isBefore(monday)) {
					start = 1;
				}
				if (engineerAllocation.getAllocatedUntil().isAfter(friday)) {
					end = dates.size();
				}
				engAllocWraps.add(new EngineerAllocationWrapper(engineerAllocation, (start == 0) ? end : start, end));
			}
			dateList.put(j, dates);
			workList.put(j, upcomingWorkWrappers);
			allocationList.put(j, engAllocWraps);
		}
		// get next monday after displayed weeks
		LocalDate next = startDate.with(DayOfWeek.MONDAY).plusWeeks(numOfWeeksDisplayed);
		List<Department> depts = this.departmentService.getByTypeAndSubdiv(DepartmentType.LABORATORY, allocatedSubdiv.getId());
		depts.sort(new DepartmentComparator());
		model.put("depts", depts);
		model.put("datelist", dateList);
		model.put("worklist", workList);
		model.put("allocationlist", allocationList);
		model.put("next", next);
		model.put("previous", prev);
		model.put("numWeeks", numOfWeeksDisplayed);
		List<Contact> contacts = this.contactService.getAllActiveSubdivContacts(allocatedSubdiv.getId());
		contacts.sort(new ContactComparator());
		model.put("businessUsers", contacts);
		return new ModelAndView("/trescal/core/workflow/viewupcomingwork", "model", model);
	}
}