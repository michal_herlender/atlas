package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import java.util.Date;
import java.util.TreeSet;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

@Service
public class HookInterceptor_DespatchItem extends HookInterceptor<HookInterceptor_DespatchItem.Input>
{
	public static final String HOOK_NAME = "despatch-item";

	public static class Input {
		private JobItem ji;
		private Address fromAddr;
		private Location fromLoc;
		private Address toAddr;
		private Location toLoc;
		public Input(JobItem ji, Address fromAddr, Location fromLoc, Address toAddr, Location toLoc) {
			this.ji = ji;
			this.fromAddr = fromAddr;
			this.fromLoc = fromLoc;
			this.toAddr = toAddr;
			this.toLoc = toLoc;
		}
	}
	
	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException
	{
		Hook hook = super.lookupHook(HOOK_NAME);

		if (hook.isActive())
		{
			this.logger.debug("RUNNING HOOK: DESPATCH ITEM");
			Contact con = this.sessionServ.getCurrentContact();
			JobItem ji = input.ji;
			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			JobItemTransit jit = new JobItemTransit();
			jit.setActivity(context.getActivity());
			jit.setActivityDesc(context.getActivity().getDescription());
			jit.setStartStatus((ItemStatus) ji.getState());
			jit.setStartStamp(new Date());
			jit.setStartedBy(con);
			jit.setCreatedOn(new Date());
			jit.setJobItem(ji);


			jit.setFromAddr(input.fromAddr);
			jit.setFromLoc(input.fromLoc);
			jit.setToAddr(input.toAddr);
			jit.setToLoc(input.toLoc);
			jit.setTransitDate(new Date());

			if (context.isCompleteActivity())
			{
				jit.setEndStamp(new Date());
				jit.setTimeSpent(5);
			}

			// adds the action into the job item in the session
			if (ji.getActions() == null)
			{
				ji.setActions(new TreeSet<JobItemAction>(new JobItemActionComparator()));
			}
			ji.getActions().add(jit);

			ItemStatus endStatus = super.getNextStatus(context);
			ji.setState(endStatus);
			jobItemService.updateJobItem(ji);
			if (context.isCompleteActivity())
			{
				jit.setEndStatus(endStatus);
			}

			this.persist(jit, endStatus, context);
		}
	}
}