package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_CleaningRequired implements LookupResultMessage
{
	YES("lookupresultmessage.cleaningrequired.yes"),
	NO("lookupresultmessage.cleaningrequired.no");
	
	private String messageCode;
	
	private Lookup_CleaningRequired(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}
