package org.trescal.cwms.core.workflow.dto;

public class LabCounts
{
	private int business;
	private int five;
	private int four;
	private int one;
	private int overdue;
	private int three;
	private int total;
	private int two;

	public LabCounts(int total, int overdue, int one, int two, int three, int four, int five, int business)
	{
		this.five = five;
		this.four = four;
		this.one = one;
		this.overdue = overdue;
		this.three = three;
		this.total = total;
		this.two = two;
		this.business = business;
	}

	public int getBusiness()
	{
		return this.business;
	}

	public int getFive()
	{
		return this.five;
	}

	public int getFour()
	{
		return this.four;
	}

	public int getOne()
	{
		return this.one;
	}

	public int getOverdue()
	{
		return this.overdue;
	}

	public int getThree()
	{
		return this.three;
	}

	public int getTotal()
	{
		return this.total;
	}

	public int getTwo()
	{
		return this.two;
	}

	public void setBusiness(int business)
	{
		this.business = business;
	}

	public void setFive(int five)
	{
		this.five = five;
	}

	public void setFour(int four)
	{
		this.four = four;
	}

	public void setOne(int one)
	{
		this.one = one;
	}

	public void setOverdue(int overdue)
	{
		this.overdue = overdue;
	}

	public void setThree(int three)
	{
		this.three = three;
	}

    public void setTotal(int total) {
        this.total = total;
    }

    public void setTwo(int two) {
        this.two = two;
    }

    public static LabCounts mergeLabCounts(LabCounts l1, LabCounts l2) {
        return new LabCounts(l1.total + l2.total,
                l1.overdue + l2.overdue,
                l1.one + l2.one,
                l1.two + l2.two,
                l1.three + l2.three,
                l1.four + l2.four,
                l1.five + l2.five,
                l1.business + l2.business
        );
    }
}