package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

public enum Lookup_LatestCalibrationOrInspectionOutcome implements LookupResultMessage
{
	THIRD_PARTY_REPAIR("lookupresultmessage.latestcalibrationorinspectionoutcome.thirdpartyrepair"),
	INHOUSE_REPAIR("lookupresultmessage.latestcalibrationorinspectionoutcome.inhouserepair"),
	ADJUSTMENT("lookupresultmessage.latestcalibrationorinspectionoutcome.adjustment"),
	THIRD_PARTY_CALIBRATION("lookupresultmessage.latestcalibrationorinspectionoutcome.thirdpartycalibration");
	
	private String messageCode;
	
	private Lookup_LatestCalibrationOrInspectionOutcome(String messageCode){
		this.messageCode = messageCode;
	}
	
	@Override
	public String getMessageCode() {
		return this.messageCode;
	}
}