package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionCosting;

@Entity
@DiscriminatorValue("client_decision_tp_job_costing")
public class ActionOutcome_ClientDecisionTPJobCosting extends ActionOutcome {
	private ActionOutcomeValue_ClientDecisionCosting value;
	
	public ActionOutcome_ClientDecisionTPJobCosting() {
		this.type = ActionOutcomeType.CLIENT_DECISION_TP_JOB_COSTING;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_ClientDecisionCosting getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_ClientDecisionCosting value) {
		this.value = value;
	}

}
