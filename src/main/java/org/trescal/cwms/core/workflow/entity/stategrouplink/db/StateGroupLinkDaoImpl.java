package org.trescal.cwms.core.workflow.entity.stategrouplink.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.dto.JobItemsInStateDTO;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;

@Repository
public class StateGroupLinkDaoImpl extends BaseDaoImpl<StateGroupLink, Integer> implements StateGroupLinkDao {

	@Override
	protected Class<StateGroupLink> getEntity() {
		return StateGroupLink.class;
	}

	@Override
	public List<StateGroupLink> getAllByStateGroup(StateGroup group) {
		return getResultList(cb -> {
			CriteriaQuery<StateGroupLink> cq = cb.createQuery(StateGroupLink.class);
			Root<StateGroupLink> stateGroupLink = cq.from(StateGroupLink.class);
			cq.where(cb.equal(stateGroupLink.get(StateGroupLink_.groupId), group.getId()));
			return cq;
		});
	}

	@Override
	public List<JobItemsInStateDTO> getJobItemsInStateGroup(StateGroup stateGroup, Integer companyId, Integer subdivId,
			Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemsInStateDTO> cq = cb.createQuery(JobItemsInStateDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, ItemState> state = jobItem.join(JobItem_.state);
			Expression<String> stateName = joinTranslation(cb, state, ItemState_.translations, locale);
			Join<ItemState, StateGroupLink> groupLink = state.join(ItemState_.groupLinks);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(groupLink.get(StateGroupLink_.groupId), stateGroup.getId()));
			StateGroupLinkType groupType = stateGroup.getLinkType();
			if (groupType == StateGroupLinkType.ALLOCATED_COMPANY || groupType == StateGroupLinkType.ALLOCATED_SUBDIV
					|| groupType == StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV) {
				Join<JobItem, Job> job = jobItem.join(JobItem_.job);
				if (groupType == StateGroupLinkType.ALLOCATED_COMPANY) {
					Join<Job, Subdiv> allocatedSubdiv = job.join(Job_.organisation.getName());
					clauses.getExpressions().add(cb.equal(allocatedSubdiv.get(Subdiv_.comp), companyId));
				} else
					clauses.getExpressions().add(cb.equal(job.get(Job_.organisation), subdivId));
			}
			if (groupType == StateGroupLinkType.CURRENT_COMPANY || groupType == StateGroupLinkType.CURRENT_SUBDIV
					|| groupType == StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV) {
				Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr,
						javax.persistence.criteria.JoinType.LEFT);
				if (groupType == StateGroupLinkType.CURRENT_COMPANY) {
					Join<Address, Subdiv> currentSubdiv = currentAddress.join(Address_.sub);
					clauses.getExpressions().add(cb.equal(currentSubdiv.get(Subdiv_.comp), companyId));
				} else if (groupType == StateGroupLinkType.CURRENT_SUBDIV)
					clauses.getExpressions().add(cb.equal(currentAddress.get(Address_.sub), subdivId));
				else
					clauses.getExpressions().add(cb.notEqual(currentAddress.get(Address_.sub), subdivId));
			}
			if (groupType == StateGroupLinkType.CALIBRATION_SUBDIV) {
				Join<JobItem, Address> calibrationAddress = jobItem.join(JobItem_.calAddr,
						javax.persistence.criteria.JoinType.LEFT);
				clauses.getExpressions().add(cb.equal(calibrationAddress.get(Address_.sub), subdivId));
			}
			cq.where(clauses);
			cq.groupBy(state.get(ItemState_.stateid), stateName);
			cq.select(cb.construct(JobItemsInStateDTO.class, state.get(ItemState_.stateid), stateName,
					cb.count(jobItem.get(JobItem_.jobItemId))));
			return cq;
		});
	}

	@Override
	public List<ItemStateDTO> getLinkedItemStates(StateGroup stateGroup, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ItemStateDTO> cq = cb.createQuery(ItemStateDTO.class);
			Root<StateGroupLink> stateGroupLink = cq.from(StateGroupLink.class);
			Join<StateGroupLink, ItemState> itemState = stateGroupLink.join(StateGroupLink_.state);
			Expression<String> stateName = joinTranslation(cb, itemState, ItemState_.translations, locale);
			cq.where(cb.equal(stateGroupLink.get(StateGroupLink_.groupId), stateGroup.getId()));
			cq.select(cb.construct(ItemStateDTO.class, itemState.get(ItemState_.stateid), stateName));
			return cq;
		});
	}
}