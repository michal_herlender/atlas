package org.trescal.cwms.core.workflow.entity.actionoutcome;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_ClientDecisionTPFurtherWork;

@Entity
@DiscriminatorValue("client_decision_tp_further_work")
public class ActionOutcome_ClientDecisionTPFurtherWork extends ActionOutcome {
	private ActionOutcomeValue_ClientDecisionTPFurtherWork value;
	
	public ActionOutcome_ClientDecisionTPFurtherWork() {
		this.type = ActionOutcomeType.CLIENT_DECISION_TP_FURTHER_WORK;
	}
	
	@Override
	@Transient
	public ActionOutcomeValue getGenericValue() {
		return getValue();
	}

	@NotNull
	@Column(name = "value", nullable=false)
	@Enumerated(EnumType.STRING)
	public ActionOutcomeValue_ClientDecisionTPFurtherWork getValue() {
		return value;
	}

	public void setValue(ActionOutcomeValue_ClientDecisionTPFurtherWork value) {
		this.value = value;
	}

}
