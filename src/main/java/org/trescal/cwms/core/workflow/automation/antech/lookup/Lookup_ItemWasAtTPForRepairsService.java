package org.trescal.cwms.core.workflow.automation.antech.lookup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.workflow.automation.antech.lookup.db.Lookup_ItemWasAtTPForRepairs;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemWasAtTPForRepairs")
public class Lookup_ItemWasAtTPForRepairsService extends Lookup {

	@Autowired
	private JobItemService jobItemService;

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: Lookup_ItemWatAtTPForRepairs");

		JobItem ji = jobItemService.findJobItem(jobItemId);
		if (ji.getNextWorkReq().getWorkRequirement().getServiceType().getRepair())
			return Lookup_ItemWasAtTPForRepairs.YES;
		return Lookup_ItemWasAtTPForRepairs.NO;
	}

	@Override
	public String getDescription() {
		return "Test whether the item was in repairs in a third party work";
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemWasAtTPForRepairs.values();
	}

}
