package org.trescal.cwms.core.workflow.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JobItemsInStateDTO {

	private Integer stateId;
	private String stateName;
	private Long jobItemsInState;
}