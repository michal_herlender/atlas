package org.trescal.cwms.core.workflow.automation.antech.hookinterceptor;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptor;
import org.trescal.cwms.core.workflow.automation.common.hookinterceptor.HookInterceptorContext;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HookInterceptor_SendFailureReportToClient
	extends HookInterceptor<HookInterceptor_SendFailureReportToClient.Input> {

	public static final String HOOK_NAME = "send-failure-report-to-client";

	public static class Input {
		public FaultReport fr;
		public Date startDate;
		public Integer timeSpent;
		public Contact contact;

		public Input(FaultReport fr, Date startDate, Integer timeSpent, Contact contact) {
			this.fr = fr;
			this.startDate = startDate;
			this.timeSpent = timeSpent;
			this.contact = contact;
		}
	}

	@Transactional
	@Override
	public void recordAction(Input input) throws OpenActivityException {
		Hook hook = super.lookupHook(HOOK_NAME);
		JobItem ji = input.fr.getJobItem();

		if (hook.isActive()) {
			this.logger.debug("RUNNING HOOK: SEND FAILURE REPORT TO CLIENT");

			Contact con;
			if (input.contact == null)
				con = this.sessionServ.getCurrentContact();
			else
				con = input.contact;

			HookInterceptorContext context = super.initialiseFieldsForItem(hook, ji);

			List<Integer> possibleStartStatesIds = hook.getHookActivities().stream()
					.map(ha -> ha.getState().getStateid()).collect(Collectors.toList());

			if (possibleStartStatesIds.contains(ji.getState().getStateid())) {
				JobItemActivity jia = new JobItemActivity();
				jia.setActivity(context.getActivity());
				jia.setActivityDesc(context.getActivity().getDescription());
				jia.setRemark(input.fr.getClientComments());
				jia.setStartStatus((ItemStatus) ji.getState());
				if (input.startDate == null)
					jia.setStartStamp(new Date());
				else
					jia.setStartStamp(input.startDate);
				jia.setStartedBy(con);
				jia.setCreatedOn(new Date());
				jia.setJobItem(ji);
				jia.setComplete(context.isCompleteActivity());

				if (context.isCompleteActivity()) {
					jia.setTimeSpent(input.timeSpent);
					jia.setEndStamp(DateUtils.addMilliseconds(jia.getStartStamp(), jia.getTimeSpent()));
					jia.setCompletedBy(con);
				}

				ItemStatus endStatus = super.getNextStatus(context);
				ji.setState(endStatus);
				jobItemService.updateJobItem(ji);
				if (context.isCompleteActivity()) {
					jia.setEndStatus(endStatus);
				}

				super.persist(jia, endStatus, context);
			}
		}
	}

}