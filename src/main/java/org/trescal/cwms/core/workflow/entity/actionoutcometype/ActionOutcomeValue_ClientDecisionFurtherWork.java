package org.trescal.cwms.core.workflow.entity.actionoutcometype;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public enum ActionOutcomeValue_ClientDecisionFurtherWork implements ActionOutcomeValue {
	TP_REPAIR_APPROVED(
			"Client approves third party repair",
			"actionoutcome.cd_furtherwork.approved"),
	FURTHER_WORK_REJECTED(
			"Client rejects further work",
			"actionoutcome.cd_furtherwork.rejected");

	private String description;
	private String messageCode;
	private MessageSource messageSource;
	
	private ActionOutcomeValue_ClientDecisionFurtherWork(String description, String messageCode) {
		this.description = description;
		this.messageCode = messageCode;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMessageCode() {
		return messageCode;
	}
	
	@Override
	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public ActionOutcomeType getType() {
		return ActionOutcomeType.CLIENT_DECISION_FURTHER_WORK;
	}

}
