package org.trescal.cwms.core.workflow.automation.antech.lookup.db;

import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementComparator;
import org.trescal.cwms.core.workflow.automation.antech.lookup.Lookup_ItemRepairedAtTP;
import org.trescal.cwms.core.workflow.automation.common.lookup.Lookup;
import org.trescal.cwms.core.workflow.automation.common.lookup.LookupResultMessage;

@Component("Lookup_ItemRepairedAtTP")
public class Lookup_ItemRepairedAtTPService extends Lookup {

	@Override
	public String getDescription() {
		return "Determines whether the item was repaired on its last third party visit.";
	}

	@Override
	public LookupResultMessage lookupStatus(int jobItemId) {
		logger.debug("RUNNING LOOKUP: ITEM REPAIRED AT TP");
		JobItem ji = jobItemService.findJobItem(jobItemId);
		LookupResultMessage result = null;
		if (ji.getTpRequirements().size() > 0) {
			TreeSet<TPRequirement> tprs = new TreeSet<TPRequirement>(new TPRequirementComparator());
			tprs.addAll(ji.getTpRequirements());
			TPRequirement tpr = tprs.last();
			if (tpr.isRepair()) {
				result = Lookup_ItemRepairedAtTP.YES;
			} else {
				result = Lookup_ItemRepairedAtTP.NO;
			}
		} else {
			// if there is no TP requirements, return NO to make sure this goes
			// to lookup 'Third Party Calibration Outcome'
			result = Lookup_ItemRepairedAtTP.NO;
		}
		return result;
	}

	@Override
	protected LookupResultMessage[] getLookupResultMessagesValues() {
		return Lookup_ItemRepairedAtTP.values();
	}
}