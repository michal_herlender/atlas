package org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteDTO;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.creditnote.form.CreditNoteHomeForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface CreditNoteDao extends BaseDao<CreditNote, Integer>
{
	
	List<KeyValueIntegerString> getNumbersPerInvoices(Collection<Integer> invoiceIds);
	
	List<CreditNote> findAllCreditNotesByAccountStatus(Company businessCompany, AccountStatus accStatus);
	
	CreditNote findByNumber(String cnNo, Integer orgid);
	
	List<CreditNote> getAllAtStatus(Company businessCompany, CreditNoteStatus status);
	
	PagedResultSet<CreditNoteDTO> searchCreditNotes(CreditNoteHomeForm form, PagedResultSet<CreditNoteDTO> rs);
}