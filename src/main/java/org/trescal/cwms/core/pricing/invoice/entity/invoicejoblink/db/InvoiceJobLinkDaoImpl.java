package org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceJobLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink_;

@Repository("InvoiceJobLinkDao")
public class InvoiceJobLinkDaoImpl extends BaseDaoImpl<InvoiceJobLink, Integer> implements InvoiceJobLinkDao {

	@Override
	protected Class<InvoiceJobLink> getEntity() {
		return InvoiceJobLink.class;
	}

	@Override
	public InvoiceJobLink findByJobNo(String jobno) {
		return getFirstResult(cb -> {
			CriteriaQuery<InvoiceJobLink> cq = cb.createQuery(InvoiceJobLink.class);
			Root<InvoiceJobLink> invoiceJobLink = cq.from(InvoiceJobLink.class);
			cq.where(cb.equal(invoiceJobLink.get(InvoiceJobLink_.jobno), jobno));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<InvoiceJobLink> findForInvoice(int invoiceid) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceJobLink> cq = cb.createQuery(InvoiceJobLink.class);
			Root<InvoiceJobLink> invoiceJobLink = cq.from(InvoiceJobLink.class);
			Join<InvoiceJobLink, Invoice> invoiceJoin = invoiceJobLink.join(InvoiceJobLink_.invoice);
			cq.where(cb.equal(invoiceJoin.get(Invoice_.id), invoiceid));
			return cq;
		});
	}

	@Override
	public List<InvoiceJobLink> getLinksForJobNo(String jobno) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceJobLink> cq = cb.createQuery(InvoiceJobLink.class);
			Root<InvoiceJobLink> invoiceJobLink = cq.from(InvoiceJobLink.class);
			cq.where(cb.equal(invoiceJobLink.get(InvoiceJobLink_.jobno), jobno));
			return cq;
		});
	}

	@Override
	public List<InvoiceJobLinkDTO> getLinksByIds(Collection<Integer> invoiceIds) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceJobLinkDTO> cq = cb.createQuery(InvoiceJobLinkDTO.class);
			Root<InvoiceJobLink> root = cq.from(InvoiceJobLink.class);
			Join<InvoiceJobLink, Invoice> invoice = root.join(InvoiceJobLink_.invoice);
			Join<InvoiceJobLink, Job> job = root.join(InvoiceJobLink_.job, JoinType.LEFT);
			cq.where(invoice.get(Invoice_.id).in(invoiceIds));
			cq.select(cb.construct(InvoiceJobLinkDTO.class, invoice.get(Invoice_.id), job.get(Job_.jobid),
					job.get(Job_.jobno)));
			return cq;
		});
	}
}