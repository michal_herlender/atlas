package org.trescal.cwms.core.pricing.invoice.dto;

import lombok.Value;

@Value
public class InvoiceItemPOInvoicingDTO {

     Integer itemId;
     Integer itemNo;
     Integer jobItemNo;
     Integer jobExpenseItemNo;

     String plantNo;
     String serialNo;
     String description;
     String instrumentModelName;
     String jobNoForJobItem;
     String jobNoForJobExpenseItem;
     String jobServiceModelName;

     Boolean selected;


}
