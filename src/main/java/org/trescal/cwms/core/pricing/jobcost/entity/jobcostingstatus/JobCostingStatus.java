package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;

import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.system.entity.status.Status;

@Entity
@DiscriminatorValue("jobcosting")
public class JobCostingStatus extends Status
{
	private List<JobCosting> jobCostings;

	@OneToMany(mappedBy = "status", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<JobCosting> getJobCostings()
	{
		return this.jobCostings;
	}

	public void setJobCostings(List<JobCosting> jobCostings)
	{
		this.jobCostings = jobCostings;
	}
}
