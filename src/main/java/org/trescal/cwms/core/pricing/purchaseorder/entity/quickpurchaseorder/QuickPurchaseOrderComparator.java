package org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder;

import java.util.Comparator;

public class QuickPurchaseOrderComparator implements Comparator<QuickPurchaseOrder>
{
	@Override
	public int compare(QuickPurchaseOrder o1, QuickPurchaseOrder o2)
	{
		if (o1.getCreatedOn().equals(o2.getCreatedOn()))
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
		else
		{
			return o1.getCreatedOn().compareTo(o2.getCreatedOn());
		}
	}
}
