package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum PurchaseOrderItemReceiptStatus
{
	NOT_RECEIVED("poitemreceiptstatus.notreceived"), 
	PART_RECEIVED("poitemreceiptstatus.partreceived"),
	COMPLETE("poitemreceiptstatus.complete");
	
	private String messageCode;
	private MessageSource messageSource;
	
	private PurchaseOrderItemReceiptStatus(String messageCode) {
		this.messageCode = messageCode;
	}
	
	@Component
	public static class PurchaseOrderItemReceiptStatusInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
			for (PurchaseOrderItemReceiptStatus status: EnumSet.allOf(PurchaseOrderItemReceiptStatus.class)) {
				status.setMessageSource(messages);
			}
		}
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessageCode() {
		return messageCode;
	}
	
	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		return this.messageSource.getMessage(messageCode, null, this.toString(), locale);
	}
	
	/*
	 * Needed to expose as bean property for JSP tags etc...
	 */
	public String getName() {
		return this.name();
	}
}
