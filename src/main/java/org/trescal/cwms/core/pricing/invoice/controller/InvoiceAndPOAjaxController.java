package org.trescal.cwms.core.pricing.invoice.controller;

import io.vavr.control.Either;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemInputDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPOInvoicingDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoicePoDto;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.db.InvoicePOService;

import java.util.List;

@RestController
@RequestMapping("/invoicepo")
public class InvoiceAndPOAjaxController {
    final InvoicePOService invoicePOService;

    final InvoiceItemService invoiceItemService;

    public InvoiceAndPOAjaxController(InvoicePOService invoicePOService, InvoiceItemService invoiceItemService) {
        this.invoicePOService = invoicePOService;
        this.invoiceItemService = invoiceItemService;
    }

    @PutMapping("/createinvoicepo.json")
    protected Either<String, InvoicePoDto> createInvoicePO(@RequestParam() int invoiceId,
                                                           @RequestParam() String poNo) {
        return invoicePOService.ajaxCreateInvoicePO(invoiceId, poNo);
    }

    @PutMapping("/deleteinvoicepo.json")
    public Either<String, Integer> removeInvoicePO(@RequestParam() int poId) {
        return invoicePOService.deleteInvoicePO(poId);
    }

    @GetMapping("/getinvoiceitems.json")
    public List<InvoiceItemPOInvoicingDTO> getInvoiceItems(@RequestParam() int invoiceId, int poId) {
        return invoiceItemService.getInvoiceItemsForInvoice(invoiceId, poId);
    }

    @PostMapping("/updateinvoiceitemstopo.json")
    public Either<String, Boolean> updateInvoiceItems(@RequestBody InvoiceItemInputDTO input) {
        return invoicePOService.linkItemsToPO(input.getInvoiceId(), input.getPoId(), input.getItemIds());
    }

}
