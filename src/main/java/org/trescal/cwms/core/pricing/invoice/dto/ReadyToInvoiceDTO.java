package org.trescal.cwms.core.pricing.invoice.dto;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;

import lombok.Data;

@Data
public class ReadyToInvoiceDTO {

	private Integer companyId;
	private String companyName;
	private CompanyRole companyRole;
	private Integer jobId;
	private String jobno;
	private Long jobItemsToInvoice;
	private Long jobExpenseItemsToInvoice;
	private Boolean completeJob;
	private Long jicompletedCountJobComplete;
	private Long jicompletedCountJobnoComplete;
	private Long jecompletedCountJobComplete;
	private Long jecompletedCountJobnoComplete;
	private Long jicompletedTotal;
	private Long jecompletedTotal;

	public ReadyToInvoiceDTO(Integer jobId, String jobno, Boolean completeJob, Integer companyId, String companyName,
			CompanyRole companyRole, Long jobItemsToInvoice, Long jobExpenseItemsToInvoice) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyRole = companyRole;
		this.jobId = jobId;
		this.jobno = jobno;
		this.completeJob = completeJob;
		this.jobItemsToInvoice = jobItemsToInvoice;
		this.jobExpenseItemsToInvoice = jobExpenseItemsToInvoice;
	}

	public ReadyToInvoiceDTO(String companyName, CompanyRole companyRole, Long jicompletedCountJobComplete,
			Long jicompletedCountJobnoComplete, Long jecompletedCountJobComplete, Long jecompletedCountJobnoComplete,
			Long jicompletedTotal, Long jecompletedTotal) {
		super();
		this.companyName = companyName;
		this.companyRole = companyRole;
		this.jicompletedCountJobComplete = jicompletedCountJobComplete;
		this.jicompletedCountJobnoComplete = jicompletedCountJobnoComplete;
		this.jecompletedCountJobComplete = jecompletedCountJobComplete;
		this.jecompletedCountJobnoComplete = jecompletedCountJobnoComplete;
		this.jicompletedTotal = jicompletedTotal;
		this.jecompletedTotal = jecompletedTotal;
	}
}