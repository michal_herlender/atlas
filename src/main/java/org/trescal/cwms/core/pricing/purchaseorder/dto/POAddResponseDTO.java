package org.trescal.cwms.core.pricing.purchaseorder.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class POAddResponseDTO {
    private final Integer poId;
    private final String poNo;
    private final String poCompanyName;
    private final String poContactName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private final LocalDate poFormattedRegDate;
    private final String poCreatedByName;
    private final String poCurrencyERSymbol;
    private final List<Map<String, String>> items;
    private final String jobItemState;

    public POAddResponseDTO(PurchaseOrder po, JobItem jobItem) {
        this.poId = po.getId();
        this.poNo = po.getPono();
        this.poCompanyName = po.getContact().getSub().getComp().getConame();
        this.poContactName = po.getContact().getName();
        this.poFormattedRegDate = po.getRegdate();
        this.poCreatedByName = po.getCreatedBy().getName();
        this.poCurrencyERSymbol = po.getCurrency().getCurrencyERSymbol();
        this.items = new ArrayList<>();
        for (PurchaseOrderItem item: po.getItems()) {
            if(item.getLinkedJobItems().stream().anyMatch(i -> i.getJi().equals(jobItem))) {
                Map<String,String> dtoItem = new HashMap<>();
                dtoItem.put("itemno", item.getItemno().toString());
                dtoItem.put("itemcost", new DecimalFormat("#0.##").format(item.getFinalCost()));
                items.add(dtoItem);
            }
        }
        this.jobItemState = jobItem.getState().getDescription();
    }

}
