package org.trescal.cwms.core.pricing.entity.costs.thirdparty;

/**
 * Interface defining functionality that applies to entities extending the
 * {@link ThirdPartyCostSupport} interface.
 * 
 * @author Richard
 */
public interface ThirdPartyCostSupportService
{
	/**
	 * Cleans up data in a {@link ThirdPartyCostSupport} entity.
	 * 
	 * @param cost the {@link ThirdPartyCostSupport}.
	 * @return {@link ThirdPartyCostSupport}.
	 */
	public ThirdPartyCostSupport cleanThirdPartyCosts(ThirdPartyCostSupport cost);

	/**
	 * Cleans up any third party costs and calculates the total value of the
	 * third party costs. Looks up and applies any necessary markups.
	 * 
	 * @param cost the {@link ThirdPartyCostSupport} entity.
	 * @return the {@link ThirdPartyCostSupport} entity.
	 */
	public ThirdPartyCostSupport recalculateThirdPartyCosts(ThirdPartyCostSupport cost);
}
