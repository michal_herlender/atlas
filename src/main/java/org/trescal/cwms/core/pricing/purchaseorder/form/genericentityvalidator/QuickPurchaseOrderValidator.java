package org.trescal.cwms.core.pricing.purchaseorder.form.genericentityvalidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder;
import org.trescal.cwms.core.validation.AbstractEntityValidator;
import org.trescal.cwms.core.validation.BeanValidator;

@Component("QuickPurchaseOrderValidator")
public class QuickPurchaseOrderValidator extends AbstractEntityValidator
{
	@Autowired
	private BeanValidator beanValidator;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(QuickPurchaseOrder.class);
	}

	@Override
	public void validate(Object target, org.springframework.validation.Errors errors)
	{
		QuickPurchaseOrder order = (QuickPurchaseOrder) target;
		if (errors == null) errors = new org.springframework.validation.BindException(order, "order");
		beanValidator.validate(order, errors);
		this.errors = (BindException) errors;
	}
}