package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed;

@Repository("JobCostingViewedDao")
public class JobCostingViewedDaoImpl extends BaseDaoImpl<JobCostingViewed, Integer> implements JobCostingViewedDao {
	
	@Override
	protected Class<JobCostingViewed> getEntity() {
		return JobCostingViewed.class;
	}
}