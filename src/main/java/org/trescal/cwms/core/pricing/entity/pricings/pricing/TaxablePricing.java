package org.trescal.cwms.core.pricing.entity.pricings.pricing;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.TaxablePricingItem;
import org.trescal.cwms.core.pricing.entity.tax.PricingTax;

import lombok.Setter;

@MappedSuperclass
@Setter
public abstract class TaxablePricing<OrgLevel extends OrganisationLevel, ItemType extends TaxablePricingItem, TaxType extends PricingTax<?>>
		extends Pricing<OrgLevel> {

	private List<TaxType> taxes;

	@Transient
	public abstract Set<ItemType> getItems();

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "pricing", orphanRemoval = true)
	public List<TaxType> getTaxes() {
		return taxes;
	};

	@Transient
	public BigDecimal getTotalTax() {
		return this.getTaxes().stream().map(TaxType::getTax).reduce(BigDecimal.ZERO, BigDecimal::add);
	}
}