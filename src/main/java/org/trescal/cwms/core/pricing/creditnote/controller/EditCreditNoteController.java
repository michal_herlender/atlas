package org.trescal.cwms.core.pricing.creditnote.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateFormatter;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.form.EditCreditNoteForm;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculationSystem;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;

@Controller
@IntranetController
@RequestMapping(value = "/editcreditnote.htm")
public class EditCreditNoteController {

	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Autowired
	private VatRateFormatter vatRateFormatter;
	@Autowired
	private VatRateService vatRateService;
	@Autowired
	private CreditNoteService creditNoteService;
	@Autowired
	private TaxCalculator taxCalculator;

	public static final String COMMAND_NAME = "command";

	@ModelAttribute(COMMAND_NAME)
	public EditCreditNoteForm formBackingObject(@RequestParam(name = "id", required = true) int id) {
		EditCreditNoteForm form = new EditCreditNoteForm();
		CreditNote creditNote = this.creditNoteService.get(id);
		form.setVatCode(creditNote.getVatRateEntity().getVatCode());
		return form;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showView(Locale locale, Model model, @RequestParam(name = "id", required = true) int id) {
		CreditNote creditNote = this.creditNoteService.get(id);
		List<VatRate> vatRates = this.vatRateService.getAllEagerWithCountry();

		model.addAttribute("creditNote", creditNote);
		model.addAttribute("vatRates", vatRateFormatter.getOptionGroupMap(vatRates, locale));

		return "trescal/core/pricing/creditnote/editcreditnote";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String performUpdate(Locale locale, Model model, @RequestParam(name = "id", required = true) int id,
			@Valid @ModelAttribute(COMMAND_NAME) EditCreditNoteForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return showView(locale, model, id);
		}
		CreditNote creditNote = this.creditNoteService.get(id);
		creditNote.setTotalCost(creditNote.getItems().stream().map(CreditNoteItem::getFinalCost).reduce(BigDecimal.ZERO,
				BigDecimal::add));
		VatRate vatRate = this.vatRateService.get(form.getVatCode());
		creditNote.setVatRateEntity(vatRate);
		creditNote.setVatRate(vatRate.getRate());
		TaxCalculationSystem taxCalculationSystem = avalaraAware ? TaxCalculationSystem.AVATAX
				: TaxCalculationSystem.DEFAULT;
		taxCalculator.calculateTax(creditNote, taxCalculationSystem);
		// Recalculate the final cost, as the vat may have changed
		CostCalculator.updatePricingFinalCost(creditNote, creditNote.getItems());
		this.creditNoteService.merge(creditNote);
		model.asMap().clear();
		return "redirect:/viewcreditnote.htm?id=" + id;
	}
}