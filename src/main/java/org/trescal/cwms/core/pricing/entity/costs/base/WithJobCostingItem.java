package org.trescal.cwms.core.pricing.entity.costs.base;

import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

public interface WithJobCostingItem {
    JobCostingItem getJobCostingItem();
}
