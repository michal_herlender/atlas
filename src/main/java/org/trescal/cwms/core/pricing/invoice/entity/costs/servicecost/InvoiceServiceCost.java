package org.trescal.cwms.core.pricing.invoice.entity.costs.servicecost;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.pricing.entity.costs.base.ServiceCost;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;

@Entity
@DiscriminatorValue("invoice")
public class InvoiceServiceCost extends ServiceCost {
	
	private InvoiceItem invoiceItem;
	private NominalCode nominal;
	
	@OneToOne(mappedBy="serviceCost")
	public InvoiceItem getInvoiceItem() {
		return invoiceItem;
	}
	
	public void setInvoiceItem(InvoiceItem invoiceItem) {
		this.invoiceItem = invoiceItem;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="nominal", foreignKey=@ForeignKey(name="FK_costs_service_nominalcode"))
	public NominalCode getNominal() {
		return nominal;
	}
	
	public void setNominal(NominalCode nominal) {
		this.nominal = nominal;
	}
}