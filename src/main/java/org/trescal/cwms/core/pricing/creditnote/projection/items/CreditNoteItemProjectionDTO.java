package org.trescal.cwms.core.pricing.creditnote.projection.items;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import lombok.Getter;

@Getter
public class CreditNoteItemProjectionDTO {
	private Integer itemId;
	private Integer itemNumber;
	private String description;
	private Integer quantity;
	private BigDecimal finalCost;
	private Boolean taxable;
	private BigDecimal taxAmount;
	// From join with Nominal Code
	private Integer nominalCodeId;
	private CostType nominalCostType;

	public CreditNoteItemProjectionDTO(Integer itemId, Integer itemNumber, String description, Integer quantity,
			BigDecimal finalCost, Boolean taxable, BigDecimal taxAmount, Integer nominalCodeId,
			CostType nominalCostType) {
		super();
		this.itemId = itemId;
		this.itemNumber = itemNumber;
		this.description = description;
		this.quantity = quantity;
		this.finalCost = finalCost;
		this.taxable = taxable;
		this.taxAmount = taxAmount;
		this.nominalCodeId = nominalCodeId;
		this.nominalCostType = nominalCostType;
	}
}
