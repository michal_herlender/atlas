package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem;

import java.util.Comparator;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

/**
 * {@link PurchaseOrderJobItem} comparator that sorts in order of
 * {@link PurchaseOrder} id and then by {@link PurchaseOrderItem} number. This
 * gives all purchase order items linked to any job item.
 * 
 * @author Stuarth
 */
public class DistinctPurchaseOrderJobItemComparator implements Comparator<PurchaseOrderJobItem>
{
	@Override
	public int compare(PurchaseOrderJobItem o1, PurchaseOrderJobItem o2)
	{
		int id1 = o1.getPoitem().getOrder().getId();
		int id2 = o2.getPoitem().getOrder().getId();

		if (id1 != id2)
		{
			return ((Integer) o1.getPoitem().getOrder().getId()).compareTo(o2.getPoitem().getOrder().getId());
		}
		else
		{
			return (o1.getPoitem().getItemno().compareTo(o2.getPoitem().getItemno()));
		}
	}
}