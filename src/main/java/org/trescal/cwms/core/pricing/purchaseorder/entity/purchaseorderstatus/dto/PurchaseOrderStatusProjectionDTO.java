package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchaseOrderStatusProjectionDTO {

	Integer statusId;
	String name;
	String description;
	String nameTranslation;
	String descTranslation;
	Boolean defaultStatus;
	Boolean requireAttention;
	Integer nextStatusId;

	
	public PurchaseOrderStatusProjectionDTO(Integer statusId, String name, String description, String nameTranslation,
			String descTranslation, Boolean defaultStatus, Boolean requireAttention, Integer nextStatusId) {
		super();
		this.statusId = statusId;
		this.name = name;
		this.description = description;
		this.nameTranslation = nameTranslation;
		this.descTranslation = descTranslation;
		this.defaultStatus = defaultStatus;
		this.requireAttention = requireAttention;
		this.nextStatusId = nextStatusId;
	}

}
