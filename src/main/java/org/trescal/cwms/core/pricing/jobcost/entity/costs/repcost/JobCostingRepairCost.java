/**
 * 
 */
package org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.WithCostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingItem;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedRepCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

/**
 * @author Richard
 */
@Entity
@DiscriminatorValue("jobcosting")
public class JobCostingRepairCost extends TPSupportedRepCost implements WithJobCostingItem, WithCostSource<JobCostingLinkedRepairCost>
{
	private JobCostingItem jobCostingItem;

	private JobCostingLinkedRepairCost linkedCost;
	private CostSource costSrc;

	@Enumerated(EnumType.STRING)
	@Column(name = "costsource")
	public CostSource getCostSrc()
	{
		return this.costSrc;
	}

	@OneToOne(mappedBy = "repairCost")
	public JobCostingItem getJobCostingItem()
	{
		return this.jobCostingItem;
	}

	@OneToOne(mappedBy = "repairCost", cascade = CascadeType.ALL)
	public JobCostingLinkedRepairCost getLinkedCost()
	{
		return this.linkedCost;
	}

	public void setCostSrc(CostSource costSrc)
	{
		this.costSrc = costSrc;
	}

	public void setJobCostingItem(JobCostingItem jobCostingItem)
	{
		this.jobCostingItem = jobCostingItem;
	}

	public void setLinkedCost(JobCostingLinkedRepairCost linkedCost)
	{
		this.linkedCost = linkedCost;
	}
}
