package org.trescal.cwms.core.pricing.invoice.entity.invoicenote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;

public interface InvoiceNoteDao extends BaseDao<InvoiceNote, Integer> {

}
