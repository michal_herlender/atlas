package org.trescal.cwms.core.pricing.invoice.entity.invoiceaction;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.system.entity.baseaction.BaseAction;

@Entity
@Table(name = "invoiceactionlist")
public class InvoiceAction extends BaseAction
{
	private Invoice invoice;
	private InvoiceStatus activity;
	private InvoiceStatus outcomeStatus;

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "activityid", unique = false, nullable = false)
	public InvoiceStatus getActivity()
	{
		return this.activity;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "invoiceid", unique = false, nullable = false)
	public Invoice getInvoice()
	{
		return this.invoice;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", unique = false)
	public InvoiceStatus getOutcomeStatus()
	{
		return this.outcomeStatus;
	}

	public void setActivity(InvoiceStatus activity)
	{
		this.activity = activity;
	}

	public void setInvoice(Invoice invoice)
	{
		this.invoice = invoice;
	}

	public void setOutcomeStatus(InvoiceStatus outcomeStatus)
	{
		this.outcomeStatus = outcomeStatus;
	}
}
