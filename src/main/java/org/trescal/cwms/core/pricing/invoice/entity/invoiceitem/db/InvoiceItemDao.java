package org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPOInvoicingDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPoLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.projection.invoiceitem.InvoiceItemProjectionDTO;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputInvoiceItem;

import java.util.Collection;
import java.util.List;

public interface InvoiceItemDao extends BaseDao<InvoiceItem, Integer> {

    List<InvoiceItemPoLinkDTO> getPoLinks(Integer invoiceId, Integer jobId);

    List<InvoiceItemPoLinkDTO> getPoLinks(Collection<Integer> invoiceIds);

    List<InvoiceItem> getAllEagerInvoiceItemsOnInvoice(int invoiceid, Integer jobid);

    Integer findMaxItemNoOnInvoice(Integer invoiceId);

    List<InvoiceItem> getAllByInvoiceAndJob(Integer invoiceId, Integer jobId);

    List<PriceLookupOutputInvoiceItem> findInvoiceItems(Integer plantid, Integer servicetypeid, Integer coid);

    List<InvoiceItemProjectionDTO> getInvoiceItemProjectionDTOs(Integer invoiceId);

    List<InvoiceItemPOInvoicingDTO> getInvoiceItemsForInvoice(Integer invoiceId, int poId);

    List<InvoiceItem> findByInvoiceIdAndNotByPOIdAndSelectedIds(Integer invoiceId, Integer poId, List<Integer> itemIds);

    List<InvoicePOItem> findItemsByInvoiceIdAndInInvPoAndNotSelected(Integer invoiceId, Integer poId, List<Integer> itemIds);

}