package org.trescal.cwms.core.pricing.entity.costs.dto;

import lombok.*;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithCostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithQuotationCalibrationCost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CostJobItemDto {
    public static CostJobItemDto fromCost(Cost cost){
        return CostToCostJobItemDtoTransformer.transform(cost);
    }

    private Integer costId;
    private Boolean active;
    private CostType costType;
    private BigDecimal discountValue;
    private BigDecimal finalCost;
    private BigDecimal discountRate;
    private BigDecimal totalCost;
    private CostSource costSource;
    private String costSourceMessageCode;
    private Integer itemId;
    private Integer itemNo;
    private Integer elementId;
    private String elementLabel;
    private Boolean issued;
    private Boolean expired;
    private LocalDate issueDate;
    private String currencyCode;


    public static CostJobItemDto fromJobCostingCalibrationCost(JobCostingCalibrationCost cost) {
        val jobCosting = cost.getJobCostingItem().getJobCosting();
        val jobCostingItem = cost.getJobCostingItem();
        return CostJobItemDto.builder()
            .costId(cost.getCostid())
            .active(cost.isActive()).costType(cost.getCostType())
            .discountRate(cost.getDiscountRate()).discountValue(cost.getDiscountValue())
            .finalCost(cost.getFinalCost()).totalCost(cost.getTotalCost())
            .elementId(jobCosting.getId()).elementLabel(jobCosting.getIdentifier())
            .itemNo(jobCostingItem.getItemno()).itemId(jobCostingItem.getId())
            .issueDate(jobCosting.getIssuedate())
            .currencyCode(jobCosting.getCurrency().getCurrencyCode())
            .build();
    }
}


class CostToCostJobItemDtoTransformer {

    public static CostJobItemDto transform(Cost cost){
        val builder = CostJobItemDto.builder()
            .costId(cost.getCostid())
            .active(cost.isActive()).costType(cost.getCostType())
            .discountRate(cost.getDiscountRate()).discountValue(cost.getDiscountValue())
            .finalCost(cost.getFinalCost()).totalCost(cost.getTotalCost());

        processCostSource(cost, builder);
        return builder
            .build();
    }

    private static void processCostSource(Cost cost, CostJobItemDto.CostJobItemDtoBuilder builder) {
        if(cost instanceof WithCostSource<?>){
            val costWihtSource = (WithCostSource<?>) cost;
            builder.costSource(costWihtSource.getCostSrc());
            switch (costWihtSource.getCostSrc()){
                case QUOTATION:
                    if(costWihtSource.getLinkedCost() instanceof WithQuotationCalibrationCost){
                        val withCalCost = (WithQuotationCalibrationCost) costWihtSource.getLinkedCost();
                        val qi = withCalCost.getQuotationCalibrationCost().getQuoteItem();
                        val quotaion = qi.getQuotation();
                        builder.elementId(quotaion.getId()).elementLabel(quotaion.getQno()+" ver "+quotaion.getVer())
                            .issued(quotaion.isIssued()).expired(quotaion.isExpired())
                            .itemId(qi.getId()).itemNo(qi.getItemno())
                            .costSourceMessageCode("jicontractreview.quote");
                    }
                    break;
                case JOB_COSTING:
                case INSTRUMENT:
                    if(costWihtSource.getLinkedCost() instanceof WithJobCostingCalibrationCost){
                        val withJobCost = (WithJobCostingCalibrationCost) costWihtSource.getLinkedCost();
                        val item = withJobCost.getJobCostingCalibrationCost().getJobCostingItem();
                        val element = item.getJobCosting();
                        builder.elementLabel(element.getIdentifier()).elementId(element.getId())
                            .itemId(item.getId()).itemNo(item.getItemno());
                    }
                    builder.costSourceMessageCode(costWihtSource.getCostSrc()==CostSource.JOB_COSTING? "jicontractreview.jobcostingmod":"jicontractreview.jobcostingint");
                default:
                    break;
            }
        }
    }

}