package org.trescal.cwms.core.pricing.creditnote.controller;

import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteItemRequestDTO;
import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteItemUpdateResult;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItemComparator;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.db.CreditNoteItemService;
import org.trescal.cwms.core.pricing.creditnote.form.CreditNoteItemForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import lombok.extern.slf4j.Slf4j;

@Controller
@JsonController
@Slf4j
public class CreditNoteItemController {

    private CreditNoteItemService creditNoteItemService;
    private CreditNoteService creditNoteService;

    public CreditNoteItemController(CreditNoteItemService creditNoteItemService, CreditNoteService creditNoteService) {
        this.creditNoteItemService = creditNoteItemService;
        this.creditNoteService = creditNoteService;
    }

    @RequestMapping(value = "updatecreditnoteitem.json", method = RequestMethod.POST, consumes = {
            Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
    @ResponseBody
    public RestResultDTO<CreditNoteItemUpdateResult> updateCreditNoteItem(@RequestBody CreditNoteItemForm form) {
        CreditNoteItem item;
        if (form.getId() == null) {
            log.debug("Add new credit note item for credit note " + form.getCreditNoteId());
            item = creditNoteItemService.create(form.getCreditNoteId(), form.getDescription(), form.getNominalId(),
                    form.getAmount(), form.getSubdivId());
        } else {
            log.info("Update credit note item " + form.getId());
            item = creditNoteItemService.update(form.getId(), form.getDescription(), form.getNominalId(),
                    form.getAmount(), form.getSubdivId());
        }
        if (item.getCreditNote().getItems() == null)
            item.getCreditNote().setItems(new TreeSet<CreditNoteItem>(new CreditNoteItemComparator()));
        item.getCreditNote().getItems().add(item);
        creditNoteService.recalculateAndMerge(item.getCreditNote());
        CreditNoteItemUpdateResult updateResult = CreditNoteItemUpdateResult.builder()
                .itemId(item.getId())
                .itemNo(item.getItemno())
                .taxes(item.getCreditNote().getTaxes())
                .grossTotal(item.getCreditNote().getTotalCost())
                .vatValue(item.getCreditNote().getVatValue())
                .netTotal(item.getCreditNote().getFinalCost())
                .currencyCode(item.getCreditNote().getCurrency().getCurrencyCode())
                .build();
        RestResultDTO<CreditNoteItemUpdateResult> result = new RestResultDTO<CreditNoteItemUpdateResult>(true, "");
        result.addResult(updateResult);
        return result;
    }

    @RequestMapping(value = "deletecreditnoteitem.json", method = RequestMethod.POST, consumes = {
            Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
    @ResponseBody
    public CreditNoteItemUpdateResult deleteCreditNoteItem(@RequestBody CreditNoteItemRequestDTO creditNoteItemRequestDTO) {
        CreditNote creditNote = creditNoteItemService.remove(creditNoteItemRequestDTO.getId());
        return CreditNoteItemUpdateResult.builder()
                .grossTotal(creditNote.getTotalCost())
                .taxes(creditNote.getTaxes())
                .vatValue(creditNote.getVatValue())
                .netTotal(creditNote.getFinalCost())
                .currencyCode(creditNote.getCurrency().getCurrencyCode())
                .build();
    }
}