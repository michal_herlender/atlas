package org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.db;

import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost;

public interface JobCostingInspectionCostDao extends JobCostingCostDaoSupport<JobCostingInspectionCost> {}