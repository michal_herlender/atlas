package org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;

import lombok.Setter;

/**
 * Abstract class that provides additional functionality and properties to
 * entities that represent {@link PricingItem}s that have associated third party
 * costs.
 * 
 * @author Richard
 */
@MappedSuperclass
@Setter
public abstract class ThirdPartyPricingItem extends PricingItem {

	// none of these fields are required any more - can all be removed once cost
	// level tp carriage is implemented
	private BigDecimal tpCarriageIn;
	private BigDecimal tpCarriageInMarkupValue;
	private BigDecimal tpCarriageMarkupRate;
	private ThirdCostMarkupSource tpCarriageMarkupSrc;
	private BigDecimal tpCarriageOut;
	private BigDecimal tpCarriageOutMarkupValue;
	private BigDecimal tpCarriageTotal;

	public ThirdPartyPricingItem() {
	}

	public ThirdPartyPricingItem(ThirdPartyPricingItem item) {
		super(item);
		this.tpCarriageIn = item.tpCarriageIn;
		this.tpCarriageInMarkupValue = item.tpCarriageInMarkupValue;
		this.tpCarriageMarkupRate = item.tpCarriageMarkupRate;
		this.tpCarriageMarkupSrc = item.tpCarriageMarkupSrc;
		this.tpCarriageOut = item.tpCarriageOut;
		this.tpCarriageOutMarkupValue = item.tpCarriageOutMarkupValue;
		this.tpCarriageTotal = item.tpCarriageTotal;
	}

	@NotNull
	@Column(name = "tpcarin", nullable = false, precision = 10, scale = 2)
	public BigDecimal getTpCarriageIn() {
		return this.tpCarriageIn;
	}

	@NotNull
	@Column(name = "tpcarinmarkupvalue", nullable = false, precision = 10, scale = 2)
	public BigDecimal getTpCarriageInMarkupValue() {
		return this.tpCarriageInMarkupValue;
	}

	@NotNull
	@Column(name = "tpcarmarkuprate", nullable = false, precision = 10, scale = 2)
	public BigDecimal getTpCarriageMarkupRate() {
		return this.tpCarriageMarkupRate;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tpcarmarkupsrc", nullable = false)
	public ThirdCostMarkupSource getTpCarriageMarkupSrc() {
		return this.tpCarriageMarkupSrc;
	}

	@NotNull
	@Column(name = "tpcarout", nullable = false, precision = 10, scale = 2)
	public BigDecimal getTpCarriageOut() {
		return this.tpCarriageOut;
	}

	@NotNull
	@Column(name = "tpcaroutmarkupvalue", nullable = false, precision = 10, scale = 2)
	public BigDecimal getTpCarriageOutMarkupValue() {
		return this.tpCarriageOutMarkupValue;
	}

	@NotNull
	@Column(name = "tpcartotal", nullable = false, precision = 10, scale = 2)
	public BigDecimal getTpCarriageTotal() {
		return this.tpCarriageTotal;
	}
}