package org.trescal.cwms.core.pricing.invoice.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoice;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoiceItem;

@Component
public class CreateInvoiceValidator implements Validator
{
	public static String MESSAGE_CODE_DIFFERENT_COUNTRY = "error.invoice.create.differentcountry";
	public static String DEFAULT_MESSAGE_DIFFERENT_COUNTRY = "All invoice items must have the same delivery country";
	
	public static String MESSAGE_CODE_DEFAULT_CONTACT_NULL = "error.rest.subdiv.nodefaultcontact";
	public static String DEFAULT_MESSAGE_DEFAULT_CONTACT_NULL = "The subdivision does not have a default contact";
	
	@Autowired
	private PNotInvoicedValidator validatorNotInvoiced;
	@Autowired
	private SubdivService subdivService;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(CreateInvoiceForm.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		CreateInvoiceForm form = (CreateInvoiceForm) target;

		if(subdivService.get(form.getAllocatedSubdivId()).getDefaultContact() == null) {
			errors.reject(MESSAGE_CODE_DEFAULT_CONTACT_NULL, DEFAULT_MESSAGE_DEFAULT_CONTACT_NULL);
		}
		/* 
		 * Removed former check for "at least one item" because we need to 
		 * be able to mark all job items as not invoiced - GB 2016-07-28
		 * Also removed quick invoice.  GB 2016-08-15
		 */
		Country sameCountry = null;
		if (form.getProposedInvoice() != null)
		{
			int invoiceIndex = 0;
			outer: for (PInvoice p : form.getProposedInvoice())
			{
				int itemIndex = 0;
				for (PInvoiceItem item : p.getItems())
				{
					// for all checked items
					if (item.isInclude())
					{
						if (item.getJitem() != null && item.getJitem().getDeliveryItems() != null && item.getJitem().getDeliveryItems().size() > 0) {
							DeliveryItem di = item.getJitem().getDeliveryItems().iterator().next();
							if (di != null && di.getDelivery() != null && di.getDelivery().isClientDelivery()) {
								if (sameCountry == null) {
									sameCountry = di.getDelivery().getAddress().getCountry();
								}
								else {
									// free hand address?
									if (di.getDelivery().getAddress() != null && !sameCountry.equals(di.getDelivery().getAddress().getCountry())) {
										errors.reject(MESSAGE_CODE_DIFFERENT_COUNTRY, DEFAULT_MESSAGE_DIFFERENT_COUNTRY);
										break outer;
									}
								}
							}
						}
					}
					// For items that are not checked, validate not invoiced reason vs comments
					else {
						errors.pushNestedPath("proposedInvoice["+invoiceIndex+"].items["+itemIndex+"].notInvoiced");
						validatorNotInvoiced.validate(item.getNotInvoiced(), errors);
						errors.popNestedPath();
					}
					itemIndex++;
				}
				invoiceIndex++;
			}
		}
	}
}
