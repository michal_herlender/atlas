package org.trescal.cwms.core.pricing.invoice.entity.invoice.db;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.invoice.dto.CompanyReadyToBeInvoiced;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.dto.ReadyToInvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.form.InvoiceHomeForm;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface InvoiceDao extends BaseDao<Invoice, Integer> {

	List<ReadyToInvoiceDTO> getReadyToInvoice(Integer subdivId, Integer companyId);

	Invoice findByInvoiceNo(String invoiceNo, Integer orgId);

	Invoice findPendingInternalInvoice(Company drawer, Company recipient);

	List<CompanyReadyToBeInvoiced> getAllCompaniesWithJobsReadyToInvoice(Company allocatedCompany);
	
	List<InvoiceProjectionDTO> getProjectionDtos(Collection<Integer> invoiceIds);

	List<InvoiceProjectionDTO> getProjectionDtos(Integer allocatedCompanyId, Integer issuedBySubdivId, Integer invoiceStatusId, AccountStatus accountStatus); 
	
	PagedResultSet<InvoiceDTO> searchSortedInvoiceDTO(InvoiceHomeForm form, String username, PagedResultSet<InvoiceDTO> rs);

	List<Invoice> getInvoicesOfTypeOnAfterDate(Company company, Company allocatedCompany, InvoiceType type,
											   LocalDate startDate);

	List<InvoiceDTO> getRecentInvoiceKeyValues(Company company, Company allocatedCompany, InvoiceType type,
                                               LocalDate fromDate);
	
	List<Integer> getJobItemsIdsFromInvoice(Invoice invoice);
	
	List<Integer> getJobsIdsFromInvoice(Invoice invoice);
}