package org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceTypeKeyValue;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;

@Service
public class InvoiceTypeServiceImpl extends BaseServiceImpl<InvoiceType, Integer> implements InvoiceTypeService {

	@Autowired
	private InvoiceTypeDao invoiceTypeDao;

	@Override
	protected InvoiceTypeDao getBaseDao() {
		return this.invoiceTypeDao;
	}

	@Override
	public InvoiceType findInvoiceTypeByName(String name) {
		return invoiceTypeDao.findInvoiceTypeByName(name);
	}

	@Override
	public List<InvoiceTypeKeyValue> getAllTranslated(Locale locale) {
		return invoiceTypeDao.getAllTranslated(locale);
	}
}