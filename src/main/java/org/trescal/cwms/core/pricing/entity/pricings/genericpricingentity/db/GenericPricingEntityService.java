package org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.db;

import io.vavr.control.Either;
import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencySupport;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import java.math.BigDecimal;

/**
 * Collection of generic functions for working with classes that extend
 * {@link GenericPricingEntity}.
 * 
 * @author richard
 */
public interface GenericPricingEntityService
{
	/**
	 * Return's the entity of type {@link MultiCurrencySupport} of the given
	 * {@link Class} type and with the given id.
	 * 
	 * @param clazz {@link Class} type of the {@link MultiCurrencySupport}.
	 * @param id the id.
	 * @return {@link MultiCurrencySupport} instance or null if not found.
	 */
	MultiCurrencySupport findMultiCurrencyEntity(Class<? extends MultiCurrencySupport> clazz, int id);

	Either<String, BigDecimal> convertValueToBaseCurrency(Integer entitiId, BigDecimal value, SupportedCurrency baseCurrency, Class<? extends MultiCurrencySupport> pricingClassName);

	/**
	 * Looks up the {@link GenericPricingEntity} child class represented by the
	 * clazz and id and uses the exchange rate to calculate the base value of
	 * given amount.
	 * 
	 * @param clazzName the {@link Class} name of the
	 *            {@link GenericPricingEntity}.
	 * @param id the id of the {@link GenericPricingEntity}.
	 * @param value the value to convert to the base currency.
	 * @return formatted string value or null if none found.
	 */
	String getBaseCurrencyValue(int id, double value);

	/**
	 * Checks if the given clazz implements the given interface.
	 * 
	 * @param clazz the {@link Class} to test.
	 * @return true if the clazz implements the interface MultiCurrencySupport.
	 */
	boolean supportsMultiCurrency(Class<?> clazz);
}