package org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.db;

import java.util.List;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;

public interface CreditNoteStatusService {

	void deleteCreditNoteStatus(CreditNoteStatus creditnotestatus);

	List<CreditNoteStatus> findAllCreditNoteStatusRequiringAttention();

	CreditNoteStatus findCreditNoteStatus(int id);

	CreditNoteStatus findOnInsertActivity();

	List<CreditNoteStatus> getAllCreditNoteStatuss();

	void insertCreditNoteStatus(CreditNoteStatus creditnotestatus);
}