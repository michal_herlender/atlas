package org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem;

public interface ThirdPartyPricingItemDao extends BaseDao<ThirdPartyPricingItem, Integer> {}