package org.trescal.cwms.core.pricing.invoice.entity.invoicenote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;

@Repository
public class InvoiceNoteDaoImpl extends BaseDaoImpl<InvoiceNote, Integer> implements InvoiceNoteDao {

	@Override
	protected Class<InvoiceNote> getEntity() {
		return InvoiceNote.class;
	}


}
