package org.trescal.cwms.core.pricing.purchaseorder.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderHomeWrapper;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db.PurchaseOrderStatusService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.db.QuickPurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderHomeForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Form displaying the 'home' page for {@link PurchaseOrder}s. Includes search
 * functionality, lists of uncompleted {@link QuickPurchaseOrder} and lists of
 * {@link PurchaseOrder}s at certain status's.
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV,
		Constants.SESSION_ATTRIBUTE_USERNAME })
public class PurchaseOrderHomeController {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private PurchaseOrderService poService;
	@Autowired
	private PurchaseOrderStatusService poStatusService;
	@Autowired
	private QuickPurchaseOrderService quickPOService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private UserService userServ;
	@Autowired
	private LimitCompanyService limitCompServ;

	@ModelAttribute("command")
	protected PurchaseOrderHomeForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
			throws Exception {
		PurchaseOrderHomeForm form = new PurchaseOrderHomeForm();
		form.setOrgid(companyDto.getKey());
		return form;
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@RequestMapping(value = "/oldpurchaseorderhome.htm", method = RequestMethod.POST)
	protected String onSubmit(@ModelAttribute("command") PurchaseOrderHomeForm form) throws Exception {
		// perform the search
		PagedResultSet<PurchaseOrder> rs = this.poService.searchSortedPurchaseOrder(form,
				new PagedResultSet<PurchaseOrder>(
						form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(),
						form.getPageNo() == 0 ? 1 : form.getPageNo()));
		form.setRs(rs);
		form.setOrders((Collection<PurchaseOrder>) rs.getResults());
		return "trescal/core/pricing/purchaseorder/purchaseorderresults";
	}

	@RequestMapping(value = "/oldpurchaseorderhome.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto, Locale locale)
			throws Exception {
		Company allocatedCompany = this.companyService.get(companyDto.getKey());
		Subdiv subdiv = subdivServ.get(subdivDto.getKey());
		User user = userServ.get(userName);
	    Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		Map<String, Object> map = new HashMap<>();
		map.put("stringtools", new StringTools());
		List<PurchaseOrderHomeWrapper<?>> lst = getInterestingItems(allocatedCompany, locale, subdiv, user.getCon());
		map.put("interestingItems", lst);

		if (authServ.hasRight(user.getCon(), Permission.PURCHASE_ORDER_STATUS_PRE_ISSUE, subdiv.getSubdivid())) {
			PurchaseOrderStatus poStatus = null;
			for (PurchaseOrderStatus postatuss : poStatusService.findAllPurchaseOrderStatusRequiringAttention()) {
				if (postatuss.getName().equals("awaiting pre-issue"))
					poStatus = postatuss;
			}

			if (poStatus != null) {
				map.put("posUpperLimit", limitCompServ.getPosUnderLimit(this.poService.getAllPurchaseOrdersAtStatus(poStatus, allocatedCompany),
						this.limitCompServ.getPoUserConfigLimit( auth, allocatedCompany),allocatedCompany));
			}
		}
		return new ModelAndView("trescal/core/pricing/purchaseorder/purchaseorderhome", map);
	}

	protected List<PurchaseOrderHomeWrapper<?>> getInterestingItems(Company allocatedCompany, Locale locale,
			Subdiv subdiv, Contact con) {
		List<PurchaseOrderHomeWrapper<?>> result = new ArrayList<>();
		result.add(getQuickPurchaseOrderWrapper(allocatedCompany, locale));
		List<PurchaseOrderStatus> lst = new ArrayList<PurchaseOrderStatus>();
		List<PurchaseOrderStatus> lstTemp = this.poStatusService.findAllPurchaseOrderStatusRequiringAttention();
		PurchaseOrderStatus poStatutTemp = lstTemp.get(0);
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		while (lst.size() != lstTemp.size() && poStatutTemp != null) {
			if (poStatutTemp != null && poStatutTemp.isRequiresAttention()) {
				if (poStatusService.isAllowChangeStatus(poStatutTemp, auth))
					lst.add(poStatutTemp);
			}
			poStatutTemp = poStatutTemp.getNext();
		}

		for (PurchaseOrderStatus status : lst) {
			String name = StringTools
					.toTitleCase(translationService.getCorrectTranslation(status.getNametranslations(), locale));
			String description = translationService.getCorrectTranslation(status.getDescriptiontranslations(), locale);

			PurchaseOrderHomeWrapper<PurchaseOrder> wrapper = new PurchaseOrderHomeWrapper<>();
			wrapper.setId(StringTools.toLowerNoSpaceCase(status.getName()));
			wrapper.setName(name);
			wrapper.setDescription(description);
			wrapper.setContents(this.poService.getAllPurchaseOrdersAtStatus(status, allocatedCompany));
			wrapper.setContentsPurchaseOrder(true);
			result.add(wrapper);
		}

		result.add(getPendingSyncWrapper(allocatedCompany, locale));
		result.add(getBrokeringErrorsWrapper(allocatedCompany, locale));

		return result;
	}

	protected PurchaseOrderHomeWrapper<QuickPurchaseOrder> getQuickPurchaseOrderWrapper(Company allocatedCompany,
			Locale locale) {
		String title = this.messageSource.getMessage("purchaseorder.quickpo", null, "Quick PO", locale);
		String description = this.messageSource.getMessage("purchaseorder.outstandingquickpo", null,
				"Outstanding Quick PO", locale);

		PurchaseOrderHomeWrapper<QuickPurchaseOrder> wrapper = new PurchaseOrderHomeWrapper<>();
		wrapper.setId("qpo");
		wrapper.setName(title);
		wrapper.setDescription(description);
		wrapper.setContents(this.quickPOService.findAllUnconsolidatedQuickPurchaseOrders(allocatedCompany));
		wrapper.setContentsQuickPurchaseOrder(true);
		return wrapper;
	}

	protected PurchaseOrderHomeWrapper<PurchaseOrder> getPendingSyncWrapper(Company allocatedCompany, Locale locale) {
		String title = this.messageSource.getMessage("purchaseorder.pendingsync", null, "Pending Sync", locale);

		PurchaseOrderHomeWrapper<PurchaseOrder> wrapper = new PurchaseOrderHomeWrapper<>();
		wrapper.setId("pendingsync");
		wrapper.setName(title);
		wrapper.setDescription(title);
		wrapper.setContents(this.poService.getByAccountStatus(allocatedCompany, AccountStatus.P));
		wrapper.setContentsPurchaseOrder(true);
		return wrapper;
	}

	protected PurchaseOrderHomeWrapper<PurchaseOrder> getBrokeringErrorsWrapper(Company allocatedCompany,
			Locale locale) {
		String title = this.messageSource.getMessage("purchaseorder.brokeringerrors", null, "Brokering Errors", locale);
		PurchaseOrderHomeWrapper<PurchaseOrder> wrapper = new PurchaseOrderHomeWrapper<>();
		wrapper.setId("brokeringerrors");
		wrapper.setName(title);
		wrapper.setDescription(title);
		wrapper.setContents(this.poService.getByAccountStatus(allocatedCompany, AccountStatus.E));
		wrapper.setContentsPurchaseOrder(true);
		return wrapper;
	}
}