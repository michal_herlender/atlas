package org.trescal.cwms.core.pricing.invoice.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobSortKey;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemInvoiceDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoice;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db.InvoiceTypeService;
import org.trescal.cwms.core.pricing.invoice.form.CreateInvoiceForm;
import org.trescal.cwms.core.pricing.invoice.form.CreateInvoiceValidator;
import org.trescal.cwms.core.pricing.invoice.service.InvoiceGenerator;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemProgressDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.MonthlyInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.POInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SeparateCosts;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@IntranetController
@RequestMapping("createinvoice.htm")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY,
        Constants.SESSION_ATTRIBUTE_USERNAME})
public class CreateInvoiceController {

    public static final String FORM_NAME = "createinvoiceform";
    public static final String PAGE = "trescal/core/pricing/invoice/createinvoice";

    private final AddressSettingsForAllocatedCompanyService addressSettingsService;
    private final CompanySettingsForAllocatedCompanyService companySettingsService;
    private final CompanyService companyService;
    private final DeliveryService deliveryService;
    private final InstructionService instructionService;
    private final InvoiceGenerator invoiceGenerator;
    private final InvoiceService invoiceService;
    private final InvoiceTypeService invoiceTypeService;
    private final JobExpenseItemService expenseItemService;
    private final JobItemService jobitemService;
    private final PurchaseOrderItemService purchaseOrderItemService;
    private final SubdivService subdivService;
    private final TranslationService translationService;
    private final UserService userService;

    private final CreateInvoiceValidator validator;
    private final MessageSource messageSource;

    private final MonthlyInvoice monthlyInvoice;
    private final POInvoice poInvoice;
    private final SeparateCosts separateCosts;

    public CreateInvoiceController(AddressSettingsForAllocatedCompanyService addressSettingsService, CompanySettingsForAllocatedCompanyService companySettingsService, CompanyService companyService, DeliveryService deliveryService, InstructionService instructionService, InvoiceGenerator invoiceGenerator, InvoiceService invoiceService, InvoiceTypeService invoiceTypeService, JobExpenseItemService expenseItemService, JobItemService jobitemService, PurchaseOrderItemService purchaseOrderItemService, SubdivService subdivService, UserService userService, CreateInvoiceValidator validator, MonthlyInvoice monthlyInvoice, POInvoice poInvoice, SeparateCosts separateCosts, MessageSource messageSource, TranslationService translationService) {
        this.addressSettingsService = addressSettingsService;
        this.companySettingsService = companySettingsService;
        this.companyService = companyService;
        this.deliveryService = deliveryService;
        this.instructionService = instructionService;
        this.invoiceGenerator = invoiceGenerator;
        this.invoiceService = invoiceService;
        this.invoiceTypeService = invoiceTypeService;
        this.expenseItemService = expenseItemService;
        this.jobitemService = jobitemService;
        this.purchaseOrderItemService = purchaseOrderItemService;
        this.subdivService = subdivService;
        this.userService = userService;
        this.validator = validator;
        this.monthlyInvoice = monthlyInvoice;
        this.poInvoice = poInvoice;
        this.separateCosts = separateCosts;
        this.messageSource = messageSource;
        this.translationService = translationService;
    }

    /**
     * @param jobids          - indicates to perform invoicing for specified job(s)
     *                        only
     * @param coIdForJobItems - indicates to perform job item invoicing for
     *                        specified company id
     * @param type            - if left blank, system will pick invoice type.
     *                        Optionally 'p' for pro forma or 'v' for valves.
     * @param companyDto      - provided by session
     */
    @ModelAttribute(FORM_NAME)
    public CreateInvoiceForm createCreateInvoiceForm(
            @RequestParam(value = "jobid", required = false) List<Integer> jobids,
            @RequestParam(value = "coidforjobitems", required = false) Integer coIdForJobItems,
            @RequestParam(value = "type", required = false, defaultValue = "") String type,
            @RequestParam(value = "companyWide", required = false, defaultValue = "false") Boolean companyWide,
            @RequestParam(value = "complete", required = false, defaultValue = "ALL") String completeJob,
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
        CreateInvoiceForm createInvoiceForm = new CreateInvoiceForm();
        Company clientCompany = null;
        // prepare a set of proposed costs for the jobs
        if (coIdForJobItems != null) {
            clientCompany = companyService.get(coIdForJobItems);
            Integer allocatedSubdivId = companyWide ? null : subdivDto.getKey();
            List<JobItemInvoiceDto> jobitems1 = this.jobitemService
                    .findAllCompleteJobItemsWithOnlyProformaInvoicesProjection(companyDto.getKey(), allocatedSubdivId,
                            clientCompany, JobSortKey.JOB, completeJob);
            List<JobItemInvoiceDto> jobitems2 = this.jobitemService.findAllCompleteJobItemsWithoutInvoicesProjection(
                    companyDto.getKey(), allocatedSubdivId, clientCompany, JobSortKey.JOB, completeJob);
            List<JobExpenseItem> expenseItems = this.expenseItemService.getInvoiceable(clientCompany, companyDto.getKey(),
                    allocatedSubdivId, completeJob);
            List<Integer> jobitemids = Stream.concat(jobitems1.stream(), jobitems2.stream())
                    .map(JobItemInvoiceDto::getJobItemId).collect(Collectors.toList());
            List<PInvoice> pinvoices = invoiceGenerator.prepareProspectiveInvoicesForJobItems(jobitemids, expenseItems,
                    type);
            createInvoiceForm.setProposedInvoice(pinvoices);
            createInvoiceForm.setCountItemsAlreadyInvoiced(invoiceGenerator.countAlreadyInvoicedItems(pinvoices));
            createInvoiceForm.setApproveGoodsOnAllPOs(pinvoices.stream()
                    .anyMatch(pinv -> pinv.getItems().stream().anyMatch(item -> !item.isAllPOgoodsApproved())));
        } else if (jobids.size() > 0) {
            List<PInvoice> pinvoices = invoiceGenerator.prepareProspectiveInvoices(jobids, type);
            createInvoiceForm.setProposedInvoice(pinvoices);
            createInvoiceForm.setCountItemsAlreadyInvoiced(invoiceGenerator.countAlreadyInvoicedItems(pinvoices));
            createInvoiceForm.setApproveGoodsOnAllPOs(pinvoices.stream()
                    .anyMatch(pinv -> pinv.getItems().stream().anyMatch(item -> !item.isAllPOgoodsApproved())));
            clientCompany = pinvoices.stream().findAny().map(PInvoice::getCompany).orElse(null);
        }
        if (clientCompany != null) createInvoiceForm.setCoid(clientCompany.getCoid());
        createInvoiceForm.setJobids(jobids);
        createInvoiceForm.setCompanyWide(companyWide);
        createInvoiceForm.setAllocatedSubdivId(subdivDto.getKey());
        return createInvoiceForm;
    }

    @InitBinder(FORM_NAME)
    protected void initBinder(WebDataBinder binder) {
        MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
        binder.registerCustomEditor(Date.class, editor);
        binder.addValidators(validator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String displayForm(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                              @ModelAttribute(FORM_NAME) CreateInvoiceForm createInvoiceForm, Model model, Locale locale) {

        Company company = this.companyService.get(createInvoiceForm.getCoid());
        Company allocatedCompany = this.subdivService.get(subdivDto.getKey()).getComp();
        // data(sorted) used in filters
        Set<String> setJobnos = new TreeSet<>();
        Set<String> setJobStatus = new TreeSet<>();
        Set<String> setJobClientRefs = new TreeSet<>();
        Set<String> setJobItemClientRefs = new TreeSet<>();
        Set<String> setExpenseItemClientRefs = new TreeSet<>();
        Set<String> setJobPOs = new TreeSet<>();
        Set<String> setJobBPOs = new TreeSet<>();
        Map<String, Integer> mapJobSubdivs = new HashMap<>();

        createInvoiceForm.getProposedInvoice().stream().flatMap(pinv -> pinv.getItems().stream())
                .map(PInvoiceItem::getJitem).forEach(ji -> {
                    setJobBPOs.addAll(ji.getItemPOs().stream().map(JobItemPO::getBpo).filter(Objects::nonNull)
                            .map(BPO::getPoNumber).collect(Collectors.toList()));
                    setJobPOs.addAll(ji.getItemPOs().stream().map(JobItemPO::getPo).filter(Objects::nonNull)
                            .map(PO::getPoNumber).collect(Collectors.toList()));
                });
        createInvoiceForm.getProposedInvoice().stream().flatMap(pinv -> pinv.getJobs().values().stream()).forEach(j -> {
            setJobnos.add(j.getJobno());
            setJobStatus.add(translationService.getCorrectTranslation(j.getJs().getNametranslations(), locale));
            if (!StringUtils.isEmpty(j.getClientRef())) {
                setJobClientRefs.add(j.getClientRef());
            }
            mapJobSubdivs.put(j.getCon().getSub().getSubname(), j.getCon().getSub().getSubdivid());
        });

        // sort the keys (subname)
        TreeMap<String, Integer> setJobSubdivs = new TreeMap<>(mapJobSubdivs);

        // get clientRef from jobItem
        createInvoiceForm.getProposedInvoice().stream().flatMap(pinv -> pinv.getItems().stream()).forEach(piitem -> {
            if (!StringUtils.isEmpty(piitem.getJitem().getClientRef())) {
                setJobItemClientRefs.add(piitem.getJitem().getClientRef());
            }
        });

        createInvoiceForm.getProposedInvoice().forEach(pinv -> pinv.getExpenseItems().forEach(expi -> {
            // add client ref from expense item
            if (!StringUtils.isEmpty(expi.getExpenseItem().getClientRef())) {
                setExpenseItemClientRefs.add(expi.getExpenseItem().getClientRef());
            }
            // add bpo and po from expense item
            expi.getExpenseItem().getItemPOs().forEach(item -> {
                if (item.getBpo() != null) {
                    setJobBPOs.add(item.getBpo().getPoNumber());
                }
                if (item.getPo() != null) {
                    setJobPOs.add(item.getPo().getPoNumber());
                }
            });
        }));

        model.addAttribute("company", company);
        model.addAttribute("monthlyinvoicesystemdefault",
                monthlyInvoice.getValueByScope(Scope.COMPANY, company, allocatedCompany));
        model.addAttribute("poinvoicesystemdefault",
                poInvoice.getValueByScope(Scope.COMPANY, company, allocatedCompany));
        model.addAttribute("separatecostsinvoicesystemdefault",
                separateCosts.getValueByScope(Scope.COMPANY, company, allocatedCompany));
        model.addAttribute("notInvoicedReasons", NotInvoicedReason.values());
        model.addAttribute("invoiceTypes", invoiceTypeService.getAllTranslated(locale));
        model.addAttribute("periodicInvoices", invoiceService.getPeriodicInvoiceDTO(company, allocatedCompany, 1));
        model.addAttribute("instructions",
                createInvoiceForm.getProposedInvoice().stream()
                        .flatMap(inv -> instructionService.findAllByJob(
                                inv.getJobs().values().stream().map(Job::getJobid).collect(Collectors.toList()),
                                inv.getItems().stream().map(pii -> pii.getJitem().getJobItemId())
                                        .collect(Collectors.toList()),
                                subdivDto.getKey(), InstructionType.INVOICE, InstructionType.DISCOUNT).stream())
                        .distinct().collect(Collectors.toList()));
        model.addAttribute("listJobSubdivs", setJobSubdivs);
        model.addAttribute("listJobNos", setJobnos);
        model.addAttribute("listJobStatus", setJobStatus);
        model.addAttribute("listJobClientRefs", setJobClientRefs);
        model.addAttribute("listJobItemClientRefs", setJobItemClientRefs);
        model.addAttribute("listExpenseItemClientRefs", setExpenseItemClientRefs);
        model.addAttribute("listJobPOs", setJobPOs);
        model.addAttribute("listJobBPOs", setJobBPOs);
        createInvoiceForm.getProposedInvoice().forEach(i -> i.getItems().forEach(ii -> System.out.println("JI " + ii.getJitem().getItemNo() + ", include " + ii.isInclude())));
        return PAGE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String onSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                           @Validated @ModelAttribute(FORM_NAME) CreateInvoiceForm createInvoiceForm, BindingResult result,
                           Model model, Locale locale, @RequestParam(value = "jobid", required = false, defaultValue = "0") int jobID,
                           @RequestParam(value = "type", required = false, defaultValue = "") String type,
                           RedirectAttributes redirectAttributes) throws Exception {
        List<Invoice> invs;
        Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
        if (!checkVatRate(allocatedSubdiv.getComp(), createInvoiceForm))
            result.addError(new ObjectError("CreateInvoiceForm", "All Items must have the same vat Rate!"));
        if (result.hasErrors())
            return displayForm(subdivDto, createInvoiceForm, model, locale);
        else {
            Contact currentContact = this.userService.get(username).getCon();
            this.invoiceGenerator.updateNotInvoicedItems(createInvoiceForm.getProposedInvoice(), currentContact);
            invoiceGenerator.updateNotInvoicedExpenseItems(createInvoiceForm.getProposedInvoice(), currentContact);
            if (createInvoiceForm.isApproveGoodsOnAllPOs())
                createInvoiceForm.getProposedInvoice()
                        .forEach(pInv -> approveGoodsOnAllPOs(pInv, currentContact, locale));
            invs = this.invoiceGenerator.createInvoicesFromPInvoiceList(createInvoiceForm.getProposedInvoice(),
                    currentContact, allocatedSubdiv);
            model.addAttribute("invs", invs);
            if (invs != null && invs.size() == 1) {
                redirectAttributes.addAttribute("id", invs.get(0).getId());
                return "redirect:/viewinvoice.htm";
            }
            return "trescal/core/pricing/invoice/createinvoiceresults";
        }
    }

    private void approveGoodsOnAllPOs(PInvoice pInvoice, Contact currentContact, Locale locale) {
        pInvoice.getItems().stream().filter(PInvoiceItem::isInclude)
                .flatMap(pInvItem -> pInvItem.getJitem().getPurchaseOrderItems().stream())
                .map(PurchaseOrderJobItem::getPoitem)
                .filter(poItem -> poItem.getOrder().isIssued() && !poItem.isGoodsApproved() && !poItem.isCancelled())
                .forEach(poItem -> {
                    PurchaseOrderItemProgressDTO dto = new PurchaseOrderItemProgressDTO();
                    dto.setAccountsApproved(poItem.isAccountsApproved());
                    dto.setCancelled(false);
                    dto.setGoodsApproved(true);
                    dto.setProgressDescription(messageSource.getMessage("createinvoice.autoGoodsApproved", null,
                            "Automatically set on invoicing process", locale));
                    dto.setReceiptStatus(PurchaseOrderItemReceiptStatus.COMPLETE);
                    purchaseOrderItemService.updatePurchaseOrderItemProgress(dto, poItem, currentContact);
                    purchaseOrderItemService.saveOrUpdatePurchaseOrderItem(poItem, currentContact);
                });
    }

    private Boolean checkVatRate(Company allocatedCompany, CreateInvoiceForm createInvoiceForm) {
        if (createInvoiceForm != null && createInvoiceForm.getProposedInvoice() != null
                && createInvoiceForm.getProposedInvoice().size() > 0) {
            for (PInvoice pi : createInvoiceForm.getProposedInvoice()) {
                VatRate vat = null;
                CompanySettingsForAllocatedCompany compSettings = this.companySettingsService
                        .getByCompany(pi.getCompany(), allocatedCompany);
                for (PInvoiceItem pii : pi.getItems()) {
                    if (pii.isInclude()) {
                        if (vat == null) {
                            vat = vatrateget(allocatedCompany, pii.getJitem(), pi.getAddress(), compSettings);
                        } else {
                            VatRate itemRate = vatrateget(allocatedCompany, pii.getJitem(), pi.getAddress(),
                                    compSettings);
                            if (!vat.getVatCode().equals(itemRate.getVatCode()))
                                return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private VatRate vatrateget(Company allocatedCompany, JobItem ji, Address i,
                               CompanySettingsForAllocatedCompany compSettings) {
        VatRate vatRate = null;
        // set vat rate
        // (1) from delivery of invoice items
        if (ji != null) {
            // Sorted in descending order by delivery date (in potential case of multiple
            // client deliveries)
            List<? extends Delivery> deliveries = this.deliveryService.findByJobItem(ji.getJobItemId(),
                    DeliveryType.CLIENT);
            if (!deliveries.isEmpty()) {
                Delivery delivery = deliveries.get(0);
                AddressSettingsForAllocatedCompany addressSettings = this.addressSettingsService
                        .getByCompany(delivery.getAddress(), allocatedCompany);
                if (addressSettings != null) {
                    vatRate = addressSettings.getVatRate();
                }
                if (vatRate == null) {
                    vatRate = compSettings.getVatrate();
                }
            }
        }

        // (2) from invoice address
        if (vatRate == null) {
            AddressSettingsForAllocatedCompany addressSettings = this.addressSettingsService.getByCompany(i,
                    allocatedCompany);
            if (addressSettings != null)
                vatRate = addressSettings.getVatRate();
        }
        // (3) from company settings
        if (vatRate == null) {
            vatRate = compSettings.getVatrate();
        }
        return vatRate;
    }
}