package org.trescal.cwms.core.pricing.invoice.dto;

import java.util.Comparator;

public class ReadyToInvoiceDTOComparator implements Comparator<ReadyToInvoiceDTO>{
	
	public int compare(ReadyToInvoiceDTO dto0, ReadyToInvoiceDTO dto1) {
		String name0 = dto0.getCompanyName();
		String name1 = dto1.getCompanyName();
		int result = name0.compareTo(name1);

		return result;
	}
}
