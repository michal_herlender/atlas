package org.trescal.cwms.core.pricing.invoice.form;

import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.dto.JobItemForProspectiveInvoiceDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class NewCreateInvoiceForm {

	private Integer invoiceTypeId;
	private Integer addressId;
	private List<JobItemForProspectiveInvoiceDTO> jobItems;
	private List<Integer> ids;

	public NewCreateInvoiceForm() {
		jobItems = new ArrayList<JobItemForProspectiveInvoiceDTO>();
		ids = new ArrayList<>();
	}
	
}