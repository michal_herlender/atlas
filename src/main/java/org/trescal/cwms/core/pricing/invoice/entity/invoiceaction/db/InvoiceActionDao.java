package org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;

public interface InvoiceActionDao extends BaseDao<InvoiceAction, Integer> {}