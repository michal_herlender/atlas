package org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.db;

import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;

public interface JobCostingRepairCostDao extends JobCostingCostDaoSupport<JobCostingRepairCost> {}