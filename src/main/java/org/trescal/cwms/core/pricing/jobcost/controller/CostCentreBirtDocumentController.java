package org.trescal.cwms.core.pricing.jobcost.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.CostCentreFileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Controller @IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST})
public class CostCentreBirtDocumentController {
	@Autowired
	private DocumentService documentService;
	@Autowired
	private JobCostingService jobCostingService;

	@RequestMapping(value="/costcentrebirtdocument.htm")
	private String handleRequest(Locale locale,
			@RequestParam(value="id", required=false, defaultValue="0") Integer id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception {
		JobCosting costing = this.jobCostingService.findJobCosting(id);
		FileNamingService fnService = new CostCentreFileNamingService(costing);
		Document doc = documentService.createBirtDocument(id, BaseDocumentType.COST_CENTRE_PO_REQUEST, locale, Component.JOB_COSTING, fnService);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		
		return "redirect:viewjobcosting.htm?id=" + id + "&loadtab=costingfiles-tab";
	}
}
