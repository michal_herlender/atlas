package org.trescal.cwms.core.pricing.invoice.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.ExpenseItemNotInvoicedService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.JobItemNotInvoicedService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoiceExpenseItem;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoiceItem;
import org.trescal.cwms.core.pricing.invoice.dto.PeriodicLinkingForm;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.form.PeriodicLinkingValidator;
import org.trescal.cwms.core.pricing.invoice.service.InvoiceGenerator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.MonthlyInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.POInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SeparateCosts;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV,
	Constants.SESSION_ATTRIBUTE_USERNAME})
public class PeriodicLinkingController {

	public static final String FORM_NAME = "form";
	private static final String REQUEST_URL = "/periodiclinking.htm";
	public static final String VIEW_NAME = "trescal/core/pricing/invoice/periodiclinking";
	public static final String RETURN_VIEW_NAME = "redirect:viewinvoice.htm";

	@Autowired
	private CompanyService companyService;
	// System Defaults
	@Autowired
	private MonthlyInvoice monthlyInvoice;
	@Autowired
	private POInvoice poInvoice;
	@Autowired
	private SeparateCosts separateCosts;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private InvoiceGenerator invoiceGenerator;
	@Autowired
	private JobItemNotInvoicedService jobItemNotInvoicedService;
	@Autowired
	private ExpenseItemNotInvoicedService expenseItemNotInvoicedService;
	@Autowired
	private UserService userService;
	@Autowired
	private PeriodicLinkingValidator validator;
	@Autowired
	private InstructionService instructionService;


	@ModelAttribute(FORM_NAME)
	public PeriodicLinkingForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		PeriodicLinkingForm form = new PeriodicLinkingForm();
		form.setAllocatedSubdivId(subdivDto.getKey());
		return form;
	}
	
	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.GET)
	public String referenceData(Model model,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                @RequestParam(name = "coid") Integer coid,
                                @RequestParam(name = "companyWide", required = false, defaultValue = "false") Boolean companyWide,
                                @RequestParam(name = "complete", required = false, defaultValue = "ALL") String completeJob,
                                @RequestParam(name = "periodicInvoiceId", required = false) Integer periodicInvoiceId, Locale locale) {
		Company allocatedCompany = this.companyService.get(companyDto.getKey());
		Integer allocatedSubdivId = companyWide ? null : subdivDto.getKey();
		/* for company instructions */
		Company company = companyService.get(coid);
		model.addAttribute("company", company);
		model.addAttribute("monthlyinvoicesystemdefault",
			monthlyInvoice.getValueByScope(Scope.COMPANY, company, allocatedCompany));
		model.addAttribute("poinvoicesystemdefault",
			poInvoice.getValueByScope(Scope.COMPANY, company, allocatedCompany));
		model.addAttribute("separatecostsinvoicesystemdefault",
			separateCosts.getValueByScope(Scope.COMPANY, company, allocatedCompany));

		// get periodic invoice from last 2 years
		List<Invoice> periodicInvoices = this.invoiceService.getRecentPeriodicInvoices(company, allocatedCompany,
			LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()), 2);
		model.addAttribute("periodicInvoices", invoiceService.getPeriodicInvociesOptions(periodicInvoices, locale));
		model.addAttribute("periodicInvoiceId", periodicInvoiceId);

		// get proposed invoice items
		List<PInvoiceItem> pInvoiceItems = invoiceGenerator.prepareProposedInvoiceItems(company, companyDto.getKey(),
			allocatedSubdivId, completeJob);
		List<PInvoiceExpenseItem> pInvoiceExpenseItems = invoiceGenerator.prepareProposedInvoiceExpenseItems(company,
			companyDto.getKey(), allocatedSubdivId, completeJob);

		// data(sorted) used in filters
		Map<String, Integer> mapJobSubdivs = new HashMap<>();
		Set<String> setJobnos = new TreeSet<>();
		Set<String> setJobTypes = new TreeSet<>();
		Set<String> setJobClientRefs = new TreeSet<>();
		Set<String> setJobItemClientRefs = new TreeSet<>();
		Set<String> setExpenseItemClientRefs = new TreeSet<>();
		Set<String> setJobPOs = new TreeSet<>();
		Set<String> setJobBPOs = new TreeSet<>();

		pInvoiceItems.forEach(pitem -> {

            mapJobSubdivs.put(pitem.getJitem().getJob().getCon().getSub().getSubname(),
                pitem.getJitem().getJob().getCon().getSub().getSubdivid());
            setJobnos.add(pitem.getJitem().getJob().getJobno());
            setJobTypes.add(pitem.getJitem().getJob().getType().getName());

            if (!StringUtils.isEmpty(pitem.getJitem().getJob().getClientRef())) {
                setJobClientRefs.add(pitem.getJitem().getJob().getClientRef());
            }
            if (!StringUtils.isEmpty(pitem.getJitem().getClientRef())) {
                setJobItemClientRefs.add(pitem.getJitem().getClientRef());
            }

           pitem.getJitem().getItemPOs().forEach(po -> {
        	   if (po.getBpo() != null) {
                  setJobBPOs.add(po.getBpo().getPoNumber());
               }
               if (po.getPo() != null) {
                   setJobPOs.add(po.getPo().getPoNumber());
               }
           });
           // if we have proposed invoice expense items (job service) we
           // will add BPO from Job
           if (!pInvoiceExpenseItems.isEmpty() && pitem.getJitem().getJob().getBpo() != null) {
                setJobBPOs.add(pitem.getJitem().getJob().getBpo().getPoNumber());
           }
        });

        pInvoiceExpenseItems.forEach(pitem -> {
            mapJobSubdivs.put(pitem.getExpenseItem().getJob().getCon().getSub().getSubname(),
                pitem.getExpenseItem().getJob().getCon().getSub().getSubdivid());
            setJobnos.add(pitem.getExpenseItem().getJob().getJobno());
            setJobTypes.add(pitem.getExpenseItem().getJob().getType().getName());

            // add client ref from expense item
            if (!StringUtils.isEmpty(pitem.getExpenseItem().getClientRef())) {
                setExpenseItemClientRefs.add(pitem.getExpenseItem().getClientRef());
            }

            // add bpo and po from expense item
            pitem.getExpenseItem().getItemPOs().forEach(item -> {
                if (item.getBpo() != null) {
                    setJobBPOs.add(item.getBpo().getPoNumber());
                }
                if (item.getPo() != null) {
                    setJobPOs.add(item.getPo().getPoNumber());
                }
            });
        });
		model.addAttribute("instructions",pInvoiceItems.stream()
				.flatMap(pInvoiceItem -> instructionService.findAllByJob(
						Collections.singleton(pInvoiceItem.getJitem().getJob().getJobid()),
						Collections.singleton(pInvoiceItem.getJitem().getJobItemId()),
						subdivDto.getKey(), InstructionType.INVOICE, InstructionType.DISCOUNT
				).stream())
				.distinct()
				.collect(Collectors.toList()));

		// sort the keys (subname)
		TreeMap<String, Integer> setJobSubdivs = new TreeMap<>(mapJobSubdivs);

		model.addAttribute("proposedInvoiceItems", pInvoiceItems);
		model.addAttribute("proposedInvoiceExpenseItems", pInvoiceExpenseItems);
		model.addAttribute("listJobSubdivs", setJobSubdivs);
		model.addAttribute("listJobNos", setJobnos);
		model.addAttribute("listJobTypes", setJobTypes);
		model.addAttribute("listJobClientRefs", setJobClientRefs);
		model.addAttribute("listJobItemClientRefs", setJobItemClientRefs);
		model.addAttribute("listExpenseItemClientRefs", setExpenseItemClientRefs);
		model.addAttribute("listJobPOs", setJobPOs);
		model.addAttribute("listJobBPOs", setJobBPOs);

		return VIEW_NAME;
	}

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.POST)
	public String onSubmit(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                           @RequestParam(name = "coid") Integer coid,
                           @RequestParam(name = "companyWide", required = false, defaultValue = "false") Boolean companyWide,
                           @RequestParam(name = "complete", required = false, defaultValue = "ALL") String completeJob,
                           @Valid @ModelAttribute(FORM_NAME) PeriodicLinkingForm form, BindingResult result,
                           @RequestParam(name = "periodicInvoiceId", required = false) Integer periodicInvoiceId, Locale locale) {

		if (result.hasErrors()) {
			return referenceData(model, companyDto, subdivDto, coid, companyWide, completeJob,periodicInvoiceId, locale);
		} else {
			Contact contact = this.userService.get(username).getCon();
			this.jobItemNotInvoicedService.createNotInvoicedItems(form.getInvoiceId(), form.getItems(),
					NotInvoicedReason.CONTRACT, "", contact);
			this.expenseItemNotInvoicedService.createNotInvoicedItems(form.getInvoiceId(), form.getExpenseItems(),
					NotInvoicedReason.CONTRACT, "", contact);
		}
		return RETURN_VIEW_NAME + "?id=" + form.getInvoiceId();
	}
}