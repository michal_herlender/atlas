package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db;

import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.dto.ClientCompanyDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemProgressDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressActionComparator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.system.Constants;

@Service("PurchaseOrderItemService")
public class PurchaseOrderItemServiceImpl implements PurchaseOrderItemService {

	@Autowired
	private ContactService contactServ;
	@Autowired
	private PurchaseOrderService poServ;
	@Autowired
	private PurchaseOrderItemDao purchaseOrderItemDao;
	@Autowired
	private UserService userService;

	@Override
	public PurchaseOrder ajaxDeletePurchaseOrderItem(int id, HttpSession session) {
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact currentContact = this.userService.getEagerLoad(username).getCon();
		return this.deletePurchaseOrderItem(this.findPurchaseOrderItem(id), currentContact);
	}

	public PurchaseOrder deletePurchaseOrderItem(PurchaseOrderItem purchaseorderitem, Contact currentContact) {
		PurchaseOrder order = purchaseorderitem.getOrder();
		order.getItems().remove(purchaseorderitem);
		this.purchaseOrderItemDao.remove(purchaseorderitem);
		purchaseorderitem = null;
		// update the total on the purchaseoder
		return this.poServ.updatePurchaseOrderCosts(order, currentContact);
	}

	public List<PurchaseOrderItemDTO> findAllFromPurchaseOrder(Integer poId) {
		return purchaseOrderItemDao.findAllFromPurchaseOrder(poId);
	}

	public PurchaseOrderItem findPurchaseOrderItem(int id) {
		return this.purchaseOrderItemDao.find(id);
	}

	public List<PurchaseOrderItem> getAllPurchaseOrderItems() {
		return this.purchaseOrderItemDao.findAll();
	}

	@Override
	public List<PurchaseOrderItem> getAllSubcontracting(Company subcontractor, Date begin, Date end,
			PurchaseOrderItemReceiptStatus receiptStatus, Boolean invoiced) {
		return purchaseOrderItemDao.getAllSubcontracting(subcontractor, begin, end, receiptStatus, invoiced);
	}

	@Override
	public int getNextItemNo(PurchaseOrder order) {
		return order.getItems().stream().mapToInt(PurchaseOrderItem::getItemno).max().orElse(0) + 1;
	}

	public void insertPurchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
		// loop through job items firing off hook for each
		if (purchaseOrderItem.getLinkedJobItems() != null) {
			for (PurchaseOrderJobItem poji : purchaseOrderItem.getLinkedJobItems()) {
				this.poServ.linkJobItemToPO(poji);
			}
		}

		this.prepareNewPurchaseOrderItem(purchaseOrderItem);

		this.purchaseOrderItemDao.persist(purchaseOrderItem);
	}

	@Override
	public void linkJobItem(PurchaseOrderItem poItem, JobItem jobItem) {
		// no persist or merge necessary, because we handle with managed objects
		poItem.getLinkedJobItems().add(new PurchaseOrderJobItem(jobItem, poItem));
		PurchaseOrder order = poItem.getOrder();
		if (order.getJob() == null)
			order.setJob(jobItem.getJob());
		if (order.getJobno() == null)
			order.setJobno(jobItem.getJob().getJobno());
	}

	@Override
	public PurchaseOrderItem merge(PurchaseOrderItem item) {
		return this.purchaseOrderItemDao.merge(item);
	}

	@Override
	public void prepareNewPurchaseOrderItem(PurchaseOrderItem item) {
		item.setCancelled(false);
		item.setReceiptStatus(PurchaseOrderItemReceiptStatus.NOT_RECEIVED);
		item.setAccountsApproved(false);
		item.setGoodsApproved(false);

		// everything below here can probably be removed / tabbed out
		item.setReceived(false);
	}

	@Override
	public void saveOrUpdatePurchaseOrderItem(PurchaseOrderItem purchaseorderitem, Contact currentContact) {
		purchaseorderitem = this.purchaseOrderItemDao.merge(purchaseorderitem);
		// Formerly done via AOP
		this.poServ.validateAndCompletePurchaseOrder(purchaseorderitem.getOrder(), currentContact);
	}

	@Override
	public void updatePurchaseOrderItem(PurchaseOrderItem purchaseOrderItem, Contact currentContact) {
		// loop through job items firing off hook for each
		if (purchaseOrderItem.getLinkedJobItems() != null) {
			for (PurchaseOrderJobItem poji : purchaseOrderItem.getLinkedJobItems()) {
				this.poServ.linkJobItemToPO(poji);
			}
		}

		purchaseOrderItem = this.purchaseOrderItemDao.merge(purchaseOrderItem);
		// Formerly done via AOP
		this.poServ.validateAndCompletePurchaseOrder(purchaseOrderItem.getOrder(), currentContact);

	}

	@Override
	public void updatePurchaseOrderItemProgress(PurchaseOrderItemProgressDTO progress, PurchaseOrderItem item,
			Contact currentContact) {
		PurchaseOrderItemProgressAction action = new PurchaseOrderItemProgressAction();

		if (progress.getReceiptStatus() != item.getReceiptStatus()) {
			action.setReceiptStatus(progress.getReceiptStatus());
			item.setReceiptStatus(progress.getReceiptStatus());
		}

		if (progress.isGoodsApproved() != item.isGoodsApproved()) {
			action.setGoodsApproved(progress.isGoodsApproved());
			item.setGoodsApproved(progress.isGoodsApproved());
		}

		if (progress.isAccountsApproved() != item.isAccountsApproved()) {
			// add an additional check to make sure the user is accounts
			// approved before making any accounts changes

			if (this.contactServ.contactBelongsToDepartmentOrIsAdmin(currentContact, DepartmentType.ACCOUNTS,
					item.getOrder().getOrganisation())) {
				action.setAccountsApproved(progress.isAccountsApproved());
				item.setAccountsApproved(progress.isAccountsApproved());
			}
		}
		// 2019-10-19 fixed bug where cancelled action was not being set on cancelled
		// items
		if (progress.isCancelled() != item.isCancelled()) {
			action.setCancelled(progress.isCancelled());
			item.setCancelled(progress.isCancelled());
		}

		// if something has been changed then populate the extra fields on the
		// progress action and add it to the po item
		if ((action.getAccountsApproved() != null) || (action.getCancelled() != null)
				|| (action.getGoodsApproved() != null) || (action.getReceiptStatus() != null)) {
			action.setDate(new Date());
			action.setContact(currentContact);
			action.setDescription(progress.getProgressDescription());
			action.setItem(item);

			if (item.getProgressActions() == null) {
				item.setProgressActions(new TreeSet<>(new PurchaseOrderItemProgressActionComparator()));
			}
			item.getProgressActions().add(action);
		}
	}
	
	@Override
	public List<ClientCompanyDTO> getClientCompanyDTOFromPO(Integer poId){
		return this.purchaseOrderItemDao.getClientCompanyDTOFromPO(poId);
	}
}