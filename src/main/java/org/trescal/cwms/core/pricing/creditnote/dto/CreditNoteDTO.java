package org.trescal.cwms.core.pricing.creditnote.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class CreditNoteDTO {

	private Integer id;
	private String creditNoteNo;
	private Integer coid;
	private String coname;
	private Boolean companyActive;
	private Boolean companyOnStop;
	private String invno;
	private LocalDate regDate;
	private String currencyERSymbol;
	private BigDecimal totalCost;

	public CreditNoteDTO(Integer id, String creditNoteNo, Integer coid, String coname, Boolean companyActive,
						 Boolean companyOnStop, String invno, LocalDate regDate, String currencyERSymbol, BigDecimal totalCost) {
		super();
		this.id = id;
		this.creditNoteNo = creditNoteNo;
		this.coid = coid;
		this.coname = coname;
		this.companyActive = companyActive;
		this.companyOnStop = companyOnStop;
		this.invno = invno;
		this.regDate = regDate;
		this.currencyERSymbol = currencyERSymbol;
		this.totalCost = totalCost;
	}
	

}
