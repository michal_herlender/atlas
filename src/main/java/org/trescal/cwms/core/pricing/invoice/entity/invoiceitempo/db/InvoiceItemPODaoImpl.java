package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;

@Repository("InvoiceItemPODao")
public class InvoiceItemPODaoImpl extends BaseDaoImpl<InvoiceItemPO, Integer> implements InvoiceItemPODao {

	@Override
	protected Class<InvoiceItemPO> getEntity() {
		return InvoiceItemPO.class;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<InvoiceItem> getAllInvoiceItemsLinkedToPO(String type, Integer poId, Integer jobid) {
		Criteria criteria = getSession().createCriteria(InvoiceItem.class);
		Criteria invItemCrit = criteria.createCriteria("invItemPOs");
		if (jobid != null)
			criteria.createCriteria("jobItem").createCriteria("job").add(Restrictions.idEq(jobid));
		if (type.equalsIgnoreCase("invpo"))
			invItemCrit.createCriteria("invPO").add(Restrictions.idEq(poId));
		else if (type.equalsIgnoreCase("jobpo"))
			invItemCrit.createCriteria("jobPO").add(Restrictions.idEq(poId));
		else if (type.equalsIgnoreCase("jobbpo"))
			invItemCrit.createCriteria("bpo").add(Restrictions.idEq(poId));
		return criteria.list();
	}*/
}