package org.trescal.cwms.core.pricing.jobcost.entity.costs;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;

public abstract class JobCostingCostDaoSupportImpl<C extends Cost> extends BaseDaoImpl<C, Integer> implements JobCostingCostDaoSupport<C> {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<C> findMatchingCosts(Class<C> clazz, Integer plantid, Integer coid, int modelid, Integer caltypeid, Integer resultsYearFilter, Integer page, Integer resPerPage) {
		Criteria crit = getSession().createCriteria(clazz);
		Criteria costItemCrit = crit.createCriteria("jobCostingItem");
		Criteria costingCrit = costItemCrit.createCriteria("jobCosting");
		costingCrit.setFetchMode("currency", FetchMode.JOIN);
		// exclude site costings as items here will only have 0/default costs
		costingCrit.add(Restrictions.ne("type", PricingType.SITE));
		Criteria jItemCrit = costItemCrit.createCriteria("jobItem");
		Criteria instItemCrit = jItemCrit.createCriteria("inst");
		// active costs only
		crit.add(Restrictions.eq("active", true));
		Criteria compCrit = costingCrit.createCriteria("contact").createCriteria("sub").createCriteria("comp");
		if (coid != null)
			// company restriction
			compCrit.add(Restrictions.idEq(coid));
		// add eager loading for company & other details
		costingCrit.setFetchMode("contact", FetchMode.JOIN);
		costingCrit.setFetchMode("contact.sub", FetchMode.JOIN);
		costingCrit.setFetchMode("contact.sub.comp", FetchMode.JOIN);
		costingCrit.createCriteria("createdBy");
		costingCrit.setFetchMode("createdBy", FetchMode.JOIN);
		costingCrit.createCriteria("currency");
		costingCrit.setFetchMode("currency", FetchMode.JOIN);
		if (plantid != null)
		{
			instItemCrit.add(Restrictions.idEq(plantid));
		}
		// model restriction
		instItemCrit.createCriteria("model").add(Restrictions.idEq(modelid));
		// caltype restriction
		if (caltypeid != null)
		{
			jItemCrit.createCriteria("calType").add(Restrictions.idEq(caltypeid));
		}
		// results year filter (injected property)
		if (resultsYearFilter != null)
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.add(Calendar.YEAR, 0 - resultsYearFilter);
			costingCrit.add(Restrictions.gt("regdate", DateTools.dateToLocalDate(cal.getTime())));
		}
		// ordering
		costingCrit.addOrder(Order.desc("id"));
		crit.addOrder(Order.desc("costid"));
		// apply paging if requested
		if (page != null)
		{
			resPerPage = resPerPage == null ? Constants.RESULTS_PER_PAGE : resPerPage;

			crit.setFirstResult((resPerPage * page) - resPerPage);
			crit.setMaxResults(resPerPage);
		}
		return crit.list();
	}
}