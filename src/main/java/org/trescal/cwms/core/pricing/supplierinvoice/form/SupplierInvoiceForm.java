package org.trescal.cwms.core.pricing.supplierinvoice.form;

import lombok.Data;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class SupplierInvoiceForm {

    @Size(min = 1, max = 100)
    private String invoiceNumber;
    @NotNull(message = "{error.value.notselected}")
    private LocalDate invoiceDate;
    private LocalDate paymentDate;
    private Integer paymentModeId;
    private BigDecimal totalWithoutTax;
    private BigDecimal totalWithTax;
    private String currencyCode;
    private PaymentTerm paymentTerm;
}
