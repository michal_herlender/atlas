package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.invoice.dto.CompanyReadyToBeInvoiced;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.InvoiceSource;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db.InvoiceTypeService;
import org.trescal.cwms.core.pricing.invoice.form.PrepareMonthlyInvoiceForm;
import org.trescal.cwms.core.pricing.invoice.form.PrepareMonthlyInvoiceValidator;
import org.trescal.cwms.core.pricing.invoice.service.InvoiceGenerator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.MonthlyInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.POInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SeparateCosts;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@RequestMapping("/preparemonthlyinvoice.htm")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_USERNAME})
public class PrepareMonthlyInvoiceController
{
	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private SupportedCurrencyService currencyService;
	@Autowired
	private InvoiceGenerator invoiceGenerator;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private InvoiceTypeService invoiceTypeService;
	@Autowired
	private JobService jobService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	// Validator
	@Autowired
	private PrepareMonthlyInvoiceValidator validator;
	// System Defaults
	@Autowired
	private MonthlyInvoice monthlyInvoice;
	@Autowired
	private POInvoice poInvoice;
	@Autowired
	private SeparateCosts separateCosts;
	
	public static final String FORM_NAME ="preparemonthlyinvoiceform";
	
	
	@ModelAttribute(FORM_NAME)
	public PrepareMonthlyInvoiceForm createPrepareMonthlyInvoiceForm(
			@RequestParam(value = "coid", required = false) Integer coid, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		PrepareMonthlyInvoiceForm form = new PrepareMonthlyInvoiceForm();
		form.setAllocatedSubdivId(subdivDto.getKey());
		if ( coid != null) {
			Company comp = companyService.get(coid);
			form.setCompany(comp);
			// find all jobs that are ready to be invoiced
			form.setJobs(this.jobService.findAllJobsReadyToInvoice(coid));
			
			form.setCurrencyId(((comp != null) && (comp.getCurrency() != null)) ? comp.getCurrency().getCurrencyId() : this.currencyService.getDefaultCurrency().getCurrencyId());
		}
		return form;
	}
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}
	
	@ModelAttribute("invoicetypes")
	public List<InvoiceType> populateInvoiceTypes() {
		return invoiceTypeService.getAll();
	}
	
	@ModelAttribute("invoicesource")
	public Object populateInvoiceSource() {
		return InvoiceSource.values();
	}
	
	@ModelAttribute("currencies")
	public Object populateCurrencies() {
		return currencyService.getAllSupportedCurrencys();
	}
	
	@ModelAttribute("readytoinv")
	public List<CompanyReadyToBeInvoiced> populateReadyToInv(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Company allocatedCompany = this.companyService.get(companyDto.getKey());
		return invoiceService.getAllCompaniesWithJobsReadyToInvoice(allocatedCompany);
	}
	
	@ModelAttribute("datetools")
	public Object populateDateTools() {
		return new DateTools();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	protected String displayForm(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(FORM_NAME) PrepareMonthlyInvoiceForm prepareMonthlyInvoiceForm,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "jobid", required = false) Integer jobid,
			Model model) throws Exception
	{
		Company allocatedCompany = this.subdivService.get(subdivDto.getKey()).getComp();
		if (prepareMonthlyInvoiceForm.getCompany() != null) {
			Company company = prepareMonthlyInvoiceForm.getCompany();
			// get all invoice addresses for this company
			model.addAttribute("addresses", this.addressService.getAllCompanyAddresses(company.getId(),AddressType.INVOICE, true));
			// load any monthly invoice settings that may apply to this job
			model.addAttribute("monthlyinvoicesystemdefault", monthlyInvoice.getValueByScope(Scope.COMPANY, company, allocatedCompany));
			model.addAttribute("poinvoicesystemdefault", poInvoice.getValueByScope(Scope.COMPANY, company, allocatedCompany));
			model.addAttribute("separatecostsinvoicesystemdefault", separateCosts.getValueByScope(Scope.COMPANY, company, allocatedCompany));
		}
		Contact contact = this.userService.get(username).getCon();
		model.addAttribute("userContact", contact);
		model.addAttribute("currencies", this.currencyService.getKeyValueListAll());
		prepareMonthlyInvoiceForm.setJobid(jobid);
		prepareMonthlyInvoiceForm.setJobids(new ArrayList<Integer>());
		return "trescal/core/pricing/invoice/preparemonthlyinvoice";
	}

	@RequestMapping(method = RequestMethod.POST)
	protected String submitForm(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value = "coid", required = false) Integer coid,
			@Validated @ModelAttribute(FORM_NAME) PrepareMonthlyInvoiceForm prepareMonthlyInvoiceForm,
			BindingResult result,
			Model model) throws Exception {
		if (result.hasErrors()) {
			// Job ID, if any, should already be bound into form so can be passed as null
			return displayForm(username, prepareMonthlyInvoiceForm, subdivDto, null, model);
		} else {
			Contact currentContact = this.userService.get(username).getCon();
			if (prepareMonthlyInvoiceForm.getSource() == InvoiceSource.NONSYSTEM) {
				// this is a non-system invoice, create it and forward onto the
				// view invoice page
				Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
				Invoice invoice = this.invoiceGenerator.createInvoiceForJobs(
						coid,
						prepareMonthlyInvoiceForm.getJobNos(),
						prepareMonthlyInvoiceForm.getAddressid(),
						prepareMonthlyInvoiceForm.getTypeid(),
						prepareMonthlyInvoiceForm.getCurrencyId(),
						currentContact,
						allocatedSubdiv);
				//Empties the model so we don't get unwanted variables in url
				model.asMap().clear();
				return "redirect:/viewinvoice.htm?id=" + invoice.getId();
			} else {
				// this is a system invoice, send the user onto the preview
				// invoice
				// page
				
				String jobids = "?";
				for (Integer jobid : prepareMonthlyInvoiceForm.getJobids()) {
					jobids = "?".equals(jobids) ? jobids = jobids + "jobid="
							+ jobid : jobids + "&jobid=" + jobid;
				}
				//Empties the model so we don't get unwanted variables in url
				model.asMap().clear();
				return "redirect:/createinvoice.htm" + jobids;
			}
		}
	}
}