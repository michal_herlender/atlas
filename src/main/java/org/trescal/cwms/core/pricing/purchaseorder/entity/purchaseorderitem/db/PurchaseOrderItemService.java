package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.company.dto.ClientCompanyDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemProgressDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;

public interface PurchaseOrderItemService {
	
	PurchaseOrder ajaxDeletePurchaseOrderItem(int id, HttpSession session);

	PurchaseOrder deletePurchaseOrderItem(PurchaseOrderItem purchaseorderitem, Contact currentContact);

	List<PurchaseOrderItemDTO> findAllFromPurchaseOrder(Integer poId);

	PurchaseOrderItem findPurchaseOrderItem(int id);

	List<PurchaseOrderItem> getAllPurchaseOrderItems();

	List<PurchaseOrderItem> getAllSubcontracting(Company subcontractor, Date begin, Date end,
			PurchaseOrderItemReceiptStatus receiptStatus, Boolean invoiced);

	/**
	 * Returns 1 if this {@link PurchaseOrder} has no items, else calculates the
	 * next item number.
	 * 
	 * @param orderid
	 *            the id the the {@link PurchaseOrder}.
	 * @return
	 */
	int getNextItemNo(PurchaseOrder order);

	void insertPurchaseOrderItem(PurchaseOrderItem purchaseorderitem);

	void linkJobItem(PurchaseOrderItem poItem, JobItem jobItem);

	PurchaseOrderItem merge(PurchaseOrderItem item);

	/**
	 * Sets the status of and creates a new insert action for a new
	 * {@link PurchaseOrderItem}. An additional test is done to see if the purchase
	 * order has already been issued, if so the function attempts to find an
	 * 'onissue' status and activity and additionally log these as well. It is up to
	 * the callee to persist.
	 * 
	 * @param item
	 *            the item to prepare.
	 */
	void prepareNewPurchaseOrderItem(PurchaseOrderItem item);

	void saveOrUpdatePurchaseOrderItem(PurchaseOrderItem purchaseorderitem, Contact currentContact);

	void updatePurchaseOrderItem(PurchaseOrderItem purchaseorderitem, Contact currentContact);

	/**
	 * Handles updating a {@link PurchaseOrderItem} with changed progress data.
	 * 
	 * @param progress
	 *            progress data for this item.
	 * @param item
	 *            the {@link PurchaseOrderItem}.
	 */
	void updatePurchaseOrderItemProgress(PurchaseOrderItemProgressDTO progress, PurchaseOrderItem item,
			Contact currentContact);
	
	List<ClientCompanyDTO> getClientCompanyDTOFromPO(Integer poId);
}