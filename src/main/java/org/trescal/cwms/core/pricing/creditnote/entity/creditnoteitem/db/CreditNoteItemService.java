package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.db;

import java.math.BigDecimal;
import java.util.List;

import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.projection.items.CreditNoteItemProjectionDTO;

public interface CreditNoteItemService extends BaseService<CreditNoteItem, Integer> {

	CreditNoteItem buildNewCreditNoteItem(CreditNote creditNote, String desc, NominalCode nominalCode,
			BigDecimal amount, Subdiv businessSubdiv);

	CreditNoteItem create(Integer creditNoteId, String description, Integer nominalId, BigDecimal amount,
			Integer businessSubdivId);

	CreditNoteItem update(Integer itemId, String description, Integer nominalId, BigDecimal amount,
			Integer businessSubdivId);

	CreditNote remove(int id);

	CreditNote remove(CreditNoteItem creditNoteItem);
	
	List<CreditNoteItemProjectionDTO> getCreditNoteItemProjectionDTOs(Integer creditNoteId);
}