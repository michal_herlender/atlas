package org.trescal.cwms.core.pricing.invoice.service;

import org.apache.commons.lang3.StringUtils;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoice;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoiceItem;
import org.trescal.cwms.core.pricing.invoice.dto.PSiteInvoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCostingComparator;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

import java.util.*;
import java.util.stream.Collectors;

public class SiteInvoiceGenerator {
    private InvoiceAddressService invoiceAddressService;

    public PInvoice createPInvoiceForSiteCostings(List<JobCosting> allJobCostings, InvoiceType type) {
        // get a list of all the most recent site costings for the jobs that
        // these items belong to
        // ArrayList<JobCosting> siteCostings = new ArrayList<JobCosting>();
        List<JobCosting> siteCostings = getMostRecentSiteCostings(allJobCostings);

        if (siteCostings.size() > 0) {
            // then create an invoice for all of the items on each of these
            // costings - NOTE these may or may not include the items passed
            // into this method.
            PSiteInvoice inv = new PSiteInvoice(siteCostings.get(0).getContact().getSub().getComp(), 1, type, PricingType.SITE);
            for (JobCosting costing : siteCostings) {
				inv.getJobs().put(costing.getJob().getJobid(), costing.getJob());

				for (JobCostingItem jci : costing.getItems())
				{
					PInvoiceItem item = new PInvoiceItem(jci.getJobItem(), inv);
					item.setCosted(false);
					inv.getItems().add(item);
				}
				// @TODO this isn't the best way to do this, but will work for
				// now
				inv.setCurrency(costing.getCurrency());
				inv.setTotalCost(inv.getTotalCost().add(costing.getTotalCost()));
				if (!StringUtils.isEmpty(costing.getSiteCostingNote())
						&& !inv.getSiteCostingNotes().contains(costing.getSiteCostingNote()))
				{
					inv.getSiteCostingNotes().add(costing.getSiteCostingNote());
				}
			}
			this.invoiceAddressService.setAddress(inv);
			return inv;
		}
		return null;
	}
	
	/**
	 * Extracts a list of the most recent site costings (if any) per job using a previously queried list
	 */
	public List<JobCosting> getMostRecentSiteCostings(List<JobCosting> allJobCostings) {
        Map<Integer, SortedSet<JobCosting>> resultMap = new TreeMap<>();
        for (JobCosting jc : allJobCostings) {
            if (jc.getType().equals(PricingType.SITE)) {
                Integer jobid = jc.getJob().getJobid();
                if (!resultMap.containsKey(jobid)) {
                    resultMap.put(jobid, new TreeSet<>(new JobCostingComparator()));
                }
                resultMap.get(jobid).add(jc);
            }
        }
        // Each entry in map will have at least one costing; return the newest one
        return resultMap.values().stream().map(SortedSet::first).collect(Collectors.toList());
    }

//	public Set<Job> getDistinctJobsFromJobitems(List<JobItem> jobitems)
//	{
//		Set<Job> jobs = new HashSet<Job>();
//		for (JobItem jitem : jobitems)
//		{
//			jobs.add(jitem.getJob());
//		}
//		return jobs;
//	}

//	public JobCosting getMostRecentSiteCosting(Job job)
//	{
//		if (job != null)
//		{
//			if ((job.getJobCostings() != null)
//					&& (job.getJobCostings().size() > 0))
//			{
//				for (JobCosting costing : job.getJobCostings())
//				{
//					if (costing.getType().equals(PricingType.SITE))
//					{
//						return costing;
//					}
//				}
//			}
//		}
//		return null;
//	}
//
//	public Set<JobCosting> getMostRecentSiteCostings(Set<Job> jobs)
//	{
//		Set<JobCosting> siteCostings = new HashSet<JobCosting>();
//		if (jobs != null)
//		{
//			for (Job job : jobs)
//			{
//				JobCosting sc = this.getMostRecentSiteCosting(job);
//				if (sc != null)
//				{
//					siteCostings.add(sc);
//				}
//			}
//		}
//		return siteCostings;
//	}

	public String getSiteInvoiceNotesAsString(PSiteInvoice psinvoice)
	{
		StringBuilder sb = new StringBuilder();
		if (psinvoice != null)
		{
			for (String note : psinvoice.getSiteCostingNotes())
			{
				sb.append(note).append(" ");
			}
		}
		return sb.toString();
	}

	public void setInvoiceAddressService(InvoiceAddressService invoiceAddressService)
	{
		this.invoiceAddressService = invoiceAddressService;
	}
}
