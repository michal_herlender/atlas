package org.trescal.cwms.core.pricing.purchaseorder.controller;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.poorigin.POOriginBlank;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.enums.PurchaseOrderTaxableOption;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote.PurchaseOrderNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.db.QuickPurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderForm;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrencyFormatter;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class CreatePurchaseOrderController {
    @Value("#{props['default.currency']}")
    private String defaultCurrencyCode;
    @Autowired
    private AddressService addressServ;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private ContactService contactServ;
    @Autowired
    private JobService jobService;
    @Autowired
    private MessageSource messages;
    @Autowired
    private PurchaseOrderService poServ;
    @Autowired
    private QuickPurchaseOrderService quickPOServ;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private SupportedCurrencyFormatter currencyFormatter;
    @Autowired
    private SupportedCurrencyService currencyService;
    @Autowired
    private UserService userService;
    @Autowired
    private PurchaseOrderFormValidator validator;
    @Autowired
    private NumerationService numerationService;
    @Autowired
    private NoteService noteService;

    public static final String FORM_NAME = "command";
    public static final String VIEW_NAME = "trescal/core/pricing/purchaseorder/createpurchaseorder";

    @InitBinder(FORM_NAME)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @ModelAttribute(FORM_NAME)
    protected PurchaseOrderForm formBackingObject(
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
            @RequestParam(value = "quickpoid", required = false, defaultValue = "0") int quickpoid) throws Exception {

        Subdiv businessSubdiv = this.subdivService.get(subdivDto.getKey());
        PurchaseOrderForm form = PurchaseOrderForm.builder()
                .orgid(businessSubdiv.getComp().getCoid())
                .returnAddressId(businessSubdiv.getDefaultAddress() == null ? null : businessSubdiv.getDefaultAddress().getAddrid())
                .currencyCode(businessSubdiv.getComp().getCurrency() == null ? this.defaultCurrencyCode : businessSubdiv.getComp().getCurrency().getCurrencyCode())
                .clientref("")
                .jobno(quickpoid == 0 ? "" : this.quickPOServ.get(quickpoid).getJobno())
                .taxableOption(PurchaseOrderTaxableOption.NOT_APPLICABLE)
                .build();
        return form;
    }

    @RequestMapping(value = "/createpurchaseorder.htm", method = RequestMethod.POST)
    protected String onSubmit(Locale locale, Model model,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                              @RequestParam(value = "quickpoid", required = false, defaultValue = "0") int quickpoid,
                              @Validated @ModelAttribute(FORM_NAME) PurchaseOrderForm form, BindingResult result) throws Exception {
        if (result.hasErrors()) {
            return referenceData(locale, model, quickpoid, companyDto);
        }
        Contact currentContact = this.userService.get(userName).getCon();
        Subdiv subdiv = subdivService.get(subdivDto.getKey());
        // create a new PO
        val clazz = PurchaseOrder.class;
        PurchaseOrder order = new PurchaseOrder();
        order.setTotalCost(new BigDecimal("0.00"));
        order.setFinalCost(new BigDecimal("0.00"));
        order.setVatRate(new BigDecimal("0.00"));
        order.setVatValue(new BigDecimal("0.00"));
        order.setPoOrigin(new POOriginBlank());
        order.setDuration(1);
        order.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        order.setIssued(false);
        order.setOrganisation(subdiv.getComp());
        // set address
        order.setAddress(this.addressServ.get(form.getAddrid()));
        order.setReturnToAddress(this.addressServ.get(form.getReturnAddressId()));
        // set contact
        order.setContact(this.contactServ.get(form.getPersonid()));
        // either set the currency and rate from the company default or from the
        // other selected currency
        if (form.getCurrencyCode().equals(Constants.COMPANY_DEFAULT_CURRENCY)) {
            order.setCurrency(order.getContact().getSub().getComp().getCurrency());
            order.setRate(order.getContact().getSub().getComp().getRate());
        } else {
            order.setCurrency(this.currencyService.findCurrencyByCode(form.getCurrencyCode()));
            order.setRate(order.getCurrency().getDefaultRate());
        }
        // set current user
        order.setCreatedBy(currentContact);
        order.setNotes(noteService.initalizeNotes(order, PurchaseOrderNote.class, locale));
        order.setBusinessContact(form.getBusinessPersonid() == null ? null : this.contactServ.get(form.getBusinessPersonid()));
        order.setTaxable(form.getTaxableOption());
        // set the order number from the quick purchase order
        QuickPurchaseOrder qpo = null;
        if (quickpoid != 0) {
            qpo = this.quickPOServ.get(quickpoid);
            order.setPono(qpo.getPono());
        } else {
            // add a pono to get past validation
            String pono = numerationService.generateNumber(NumerationType.PURCHASE_ORDER, subdiv.getComp(),
                    subdiv);
            order.setPono(pono);
        }
        // update the job number
        Job job = jobService.getJobByExactJobNo(form.getJobno());
        if (job != null) order.setJob(job);
        order.setJobno(form.getJobno().trim());
        order.setClientref(form.getClientref());
        // set the status & new pono and persist
        order = this.poServ.prepareAndInsertPurchaseOrder(order, currentContact, subdiv);
        // save the quick po
        if (qpo != null) {
            qpo.setConsolidated(true);
            this.quickPOServ.merge(qpo);
        }
        return "redirect:viewpurchaseorder.htm?id=" + order.getId();
    }

    @RequestMapping(value = "/createpurchaseorder.htm", method = RequestMethod.GET)
    protected String referenceData(Locale locale, Model model,
                                   @RequestParam(value = "quickpoid", required = false, defaultValue = "0") int quickpoid,
                                   @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) throws Exception {

        String noneMessage = this.messages.getMessage("company.nonespecified", null, "None Specified", locale);
        KeyValueIntegerString noneDto = new KeyValueIntegerString(0, noneMessage);
        ContactKeyValue noneContactDto = new ContactKeyValue(0, noneMessage);
        Company allocatedCompany = companyService.get(companyDto.getKey());

        if (quickpoid != 0) model.addAttribute("quickPO", this.quickPOServ.get(quickpoid));
        model.addAttribute("currencyList", this.currencyFormatter.getCompanyCurrencyDTOs(companyDto.getKey(), null, locale));
        // add a list of business company addresses to set as the return to
        model.addAttribute("businessaddresslist", this.addressServ.getAddressDtoList(companyDto.getKey(), null, AddressType.POFROM, noneDto));
        // add a list of business contacts
        model.addAttribute("businesscontacts", this.contactServ.getContactDtoList(companyDto.getKey(), null, true, noneContactDto));
        // specify, whether taxable option is enabled for business company
        model.addAttribute("useTaxableOption",
                allocatedCompany.getBusinessSettings() != null && allocatedCompany.getBusinessSettings().getUseTaxableOption());
        final Map<String, String> taxableOptions = Arrays.stream(PurchaseOrderTaxableOption.values()).collect(Collectors.toMap(PurchaseOrderTaxableOption::name, PurchaseOrderTaxableOption::getMessage));
        model.addAttribute("taxableOptions", taxableOptions.entrySet());
        return VIEW_NAME;
    }
}