package org.trescal.cwms.core.pricing.creditnote.dto;

import lombok.Data;

@Data
public class CreditNoteItemRequestDTO {

    private Integer id;
}