package org.trescal.cwms.core.pricing.creditnote.form;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditCreditNoteForm {
	@NotNull(message="{error.value.notselected}")
	private String vatCode;
}
