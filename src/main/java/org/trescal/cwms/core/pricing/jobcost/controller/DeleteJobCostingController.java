package org.trescal.cwms.core.pricing.jobcost.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;

@Controller @IntranetController
public class DeleteJobCostingController
{
	private static final Logger logger = LoggerFactory.getLogger(DeleteJobCostingController.class);
	
	@Autowired
	private JobCostingService jobCostServ;

	@RequestMapping(value="/deletejobcosting.htm")
	protected RedirectView handleRequestInternal(@RequestParam(value="id", required=false, defaultValue="0") Integer id) throws Exception
	{
		JobCosting costing = this.jobCostServ.findJobCosting(id);
		if ((id == 0) || (costing == null)) {
			logger.error("Couldn't find costing with id " + id + " to delete.");
			throw new Exception("Costing could not be found to delete");
		}	
		int jobid = costing.getJob().getJobid();
		this.jobCostServ.delete(costing);
		return new RedirectView("/viewjob.htm?jobid=" + jobid, true);
	}
}