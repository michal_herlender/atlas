package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.system.entity.baseaction.BaseAction;

@Entity
@Table(name = "creditnoteaction")
public class CreditNoteAction extends BaseAction
{
	private CreditNoteStatus activity;
	private CreditNote creditNote;
	private CreditNoteStatus outcomeStatus;

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "activityid", unique = false, nullable = false)
	public CreditNoteStatus getActivity()
	{
		return this.activity;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "creditnoteid", unique = false, nullable = false)
	public CreditNote getCreditNote()
	{
		return this.creditNote;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", unique = false)
	public CreditNoteStatus getOutcomeStatus()
	{
		return this.outcomeStatus;
	}

	public void setActivity(CreditNoteStatus activity)
	{
		this.activity = activity;
	}

	public void setCreditNote(CreditNote creditNote)
	{
		this.creditNote = creditNote;
	}

	public void setOutcomeStatus(CreditNoteStatus outcomeStatus)
	{
		this.outcomeStatus = outcomeStatus;
	}
}
