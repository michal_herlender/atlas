package org.trescal.cwms.core.pricing.invoice.entity.invoicepo.db;

import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.invoice.dto.InvoicePoDto;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db.InvoiceItemPOService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db.InvoicePOItemDao;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

import java.util.ArrayList;
import java.util.List;

@Service("InvoicePOService")
public class InvoicePOServiceImpl extends BaseServiceImpl<InvoicePO, Integer> implements InvoicePOService {

	@Autowired
	private InvoiceItemPOService iipoServ;
	@Autowired
	private InvoicePODao invoicePODao;
	@Autowired
	private InvoiceService invServ;
	@Autowired
	private InvoiceItemService invoiceItemService;
	@Autowired
	private InvoiceItemPOService invoiceItemPOService;
	@Autowired
	private InvoicePOItemDao invoicePOItemDao;

	@Override
	protected BaseDao<InvoicePO, Integer> getBaseDao() {
		return invoicePODao;
	}

	public ResultWrapper ajaxAddInvoicePO(int invoiceId, String poNumber) {
		Invoice inv = this.invServ.findInvoice(invoiceId);
		if (inv != null) {
			InvoicePO invPO = new InvoicePO();
			invPO.setInvoice(inv);
			invPO.setOrganisation(inv.getOrganisation());
			invPO.setPoNumber(poNumber);
			this.save(invPO);
			// for each invoice item
			for (InvoiceItem ii : inv.getItems()) {
				// if the item doesn't have a job item link
				if (ii.getJobItem() == null) {
					// link the new invoice po to the item
					InvoiceItemPO iipo = new InvoiceItemPO();
					iipo.setInvItem(ii);
					iipo.setInvPO(invPO);
					this.iipoServ.insertInvoiceItemPO(iipo);
				}
			}
			return new ResultWrapper(true, null, invPO, null);
		} else {
			return new ResultWrapper(false, "Could not find invoice");
		}
	}

	public ResultWrapper ajaxDeleteInvoicePO(int invPoId) {
		InvoicePO invPO = this.get(invPoId);
		if (invPO != null) {
			List<InvoiceItemPO> toRemove = new ArrayList<>();
			toRemove.addAll(invPO.getInvItemPOs());
			for (InvoiceItemPO iipo : toRemove) {
				this.iipoServ.deleteInvoiceItemPO(iipo);
			}
			invPO.getInvItemPOs().removeAll(toRemove);
			this.delete(invPO);
			return new ResultWrapper(true, null, null, null);
		} else {
			return new ResultWrapper(false, "Could not find invoice PO");
		}
	}

	@Override
	public Either<String, InvoicePoDto> ajaxCreateInvoicePO(int invoiceId, String poNo) {
		Invoice inv = this.invServ.findInvoice(invoiceId);
		if (inv != null) {
			InvoicePO invPO = new InvoicePO();
			invPO.setInvoice(inv);
			invPO.setOrganisation(inv.getOrganisation());
			invPO.setPoNumber(poNo);
			this.save(invPO);
			// for each invoice item
			for (InvoiceItem ii : inv.getItems()) {
				// if the item doesn't have a job item link
				if (ii.getJobItem() == null) {
					// link the new invoice po to the item
					InvoiceItemPO iipo = new InvoiceItemPO();
					iipo.setInvItem(ii);
					iipo.setInvPO(invPO);
					this.iipoServ.insertInvoiceItemPO(iipo);
				}
			}
			return Either.right(InvoicePoDto.of(invPO.getPoId(), invPO.getPoNumber()));
		} else {
			return Either.left("The invoice id is not correct.");
		}
	}

	@Override
	public Either<String, Integer> deleteInvoicePO(int poId) {
		InvoicePO invPO = this.get(poId);
		if (invPO != null) {
			List<InvoiceItemPO> toRemove = new ArrayList<>();
			toRemove.addAll(invPO.getInvItemPOs());
			for (InvoiceItemPO iipo : toRemove) {
				this.iipoServ.deleteInvoiceItemPO(iipo);
			}
			/*	invoicePOItemDao.deleteAll(invPO.getNewInvItemPOs());*/
			invPO.getInvItemPOs().removeAll(toRemove);
			this.delete(invPO);
			return Either.right(invPO.getPoId());
		} else {
			return Either.left("The Invoice Po cannot be found for selected id.");
		}
	}

	@Override
	public Either<String, Boolean> linkItemsToPO(Integer invoiceId, Integer poId, List<Integer> itemIds) {

		InvoicePO invoicePO = get(poId);
		if (invoicePO == null) {
			return Either.left("The invoicePO id is not correct.");
		}
		List<InvoicePOItem> invoicePOItems =
				invoiceItemService.getItemsByInvoiceIdAndNotInInvPoAndSelectedIds(invoiceId, invoicePO, itemIds);

		List<InvoicePOItem> itemsToBeUnlinked = invoiceItemService.getItemsByInvoiceIdAndInInvPoAndNotSelected(invoiceId, poId, itemIds);

		invoicePOItemDao.saveAll(invoicePOItems);
		invoicePOItemDao.deleteAll(itemsToBeUnlinked);

		return Either.right(true);
	}
}