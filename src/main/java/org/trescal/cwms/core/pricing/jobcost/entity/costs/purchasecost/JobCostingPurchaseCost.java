package org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.WithCostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingItem;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Entity
@DiscriminatorValue("jobcosting")
public class JobCostingPurchaseCost extends TPSupportedPurchaseCost implements WithJobCostingItem, WithCostSource<JobCostingLinkedPurchaseCost>
{
	private JobCostingItem jobCostingItem;

	private JobCostingLinkedPurchaseCost linkedCost;
	private CostSource costSrc;

	@Enumerated(EnumType.STRING)
	@Column(name = "costsource")
	public CostSource getCostSrc()
	{
		return this.costSrc;
	}

	@OneToOne(mappedBy = "purchaseCost")
	public JobCostingItem getJobCostingItem()
	{
		return this.jobCostingItem;
	}

	@OneToOne(mappedBy = "purchaseCost", cascade = CascadeType.ALL)
	public JobCostingLinkedPurchaseCost getLinkedCost()
	{
		return this.linkedCost;
	}

	public void setCostSrc(CostSource costSrc)
	{
		this.costSrc = costSrc;
	}

	public void setJobCostingItem(JobCostingItem jobCostingItem)
	{
		this.jobCostingItem = jobCostingItem;
	}

	public void setLinkedCost(JobCostingLinkedPurchaseCost linkedCost)
	{
		this.linkedCost = linkedCost;
	}
}
