package org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.InvoiceDelivery;

public interface InvoiceDeliveryDao extends BaseDao<InvoiceDelivery, Integer> {}