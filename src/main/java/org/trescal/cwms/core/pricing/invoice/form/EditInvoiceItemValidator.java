package org.trescal.cwms.core.pricing.invoice.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditInvoiceItemValidator extends AbstractBeanValidator {
	public static final String ACTIVE_MESSAGE_CODE = "error.invoiceitem.none.active";
	public static final String ACTIVE_DEFAULT_MESSAGE = "At least one cost must be active when using split costs";
	public static final String DISCOUNT_MESSAGE_CODE = "error.invoiceitem.doublediscount";
	public static final String DISCOUNT_DEFAULT_MESSAGE = "The overall discount must be 0 when using split costs";
	public static final String JOBITEM_MESSAGE_CODE = "error.invoiceitem.mandatory.no.jobitem";
	public static final String JOBITEM_DEFAULT_MESSAGE = "Mandatory field when no jobitem is linked";
	public static final String NOMINALCODE_REQUIRED_CODE = "error.invoiceitem.mandatory.nominalcode";
	public static final String NOMINALCODE_REQUIRED_MESSAGE = "A nominal code must be selected";

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(EditInvoiceItemForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		EditInvoiceItemForm form = (EditInvoiceItemForm) target;
		int countActive = 0;
		if (form.getItem().getAdjustmentCost() != null && form.getItem().getAdjustmentCost().isActive()) {
			countActive++;
			errors.pushNestedPath("item.adjustmentCost");
			super.validate(form.getItem().getAdjustmentCost(), errors);
			errors.popNestedPath();
		}
		if (form.getItem().getCalibrationCost() != null && form.getItem().getCalibrationCost().isActive()) {
			countActive++;
			errors.pushNestedPath("item.calibrationCost");
			super.validate(form.getItem().getCalibrationCost(), errors);
			errors.popNestedPath();
		}
		if (form.getItem().getPurchaseCost() != null && form.getItem().getPurchaseCost().isActive()) {
			countActive++;
			errors.pushNestedPath("item.purchaseCost");
			super.validate(form.getItem().getPurchaseCost(), errors);
			errors.popNestedPath();
		}
		if (form.getItem().getRepairCost() != null && form.getItem().getRepairCost().isActive()) {
			countActive++;
			errors.pushNestedPath("item.repairCost");
			super.validate(form.getItem().getRepairCost(), errors);
			errors.popNestedPath();
		}
		if (form.getItem().getServiceCost() != null && form.getItem().getServiceCost().isActive()) {
			countActive++;
			errors.pushNestedPath("item.serviceCost");
			super.validate(form.getItem().getServiceCost(), errors);
			errors.popNestedPath();
		}
		if (form.getItem().isBreakUpCosts()) {
			if (countActive == 0) {
				errors.reject(ACTIVE_MESSAGE_CODE, null, ACTIVE_DEFAULT_MESSAGE);
			}
			if (form.getItem().getGeneralDiscountRate().doubleValue() != 0) {
				errors.rejectValue("item.generalDiscountRate", DISCOUNT_MESSAGE_CODE, null, DISCOUNT_DEFAULT_MESSAGE);
			}
		} else if (form.getSingleNominalId() == null)
			errors.rejectValue("singleNominalId", NOMINALCODE_REQUIRED_CODE, null, NOMINALCODE_REQUIRED_MESSAGE);
		if ((form.getJobItemId() == null || form.getJobItemId() == 0)
				&& (form.getExpenseItemId() == null || form.getExpenseItemId() == 0)) {
			if ((form.getItem().getDescription() == null) || form.getItem().getDescription().trim().equals("")) {
				errors.rejectValue("item.description", JOBITEM_MESSAGE_CODE, null, JOBITEM_DEFAULT_MESSAGE);
			}
			if ((form.getBusinessSubdivId() == null) || (form.getBusinessSubdivId() == 0)) {
				errors.rejectValue("businessSubdivId", JOBITEM_MESSAGE_CODE, null, JOBITEM_DEFAULT_MESSAGE);
			}
		}
	}
}