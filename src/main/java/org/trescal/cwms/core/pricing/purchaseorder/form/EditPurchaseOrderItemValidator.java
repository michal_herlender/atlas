package org.trescal.cwms.core.pricing.purchaseorder.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditPurchaseOrderItemValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(EditPurchaseOrderItemForm.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		EditPurchaseOrderItemForm form = (EditPurchaseOrderItemForm) target;
		// description can only be null or blank if the item is explicitly
		// linked to a jobitem
		if ((form.getJobItemId() == null || form.getJobItemId() == 0) && 
			((form.getDescription() == null) || form.getDescription().isEmpty()))
		{
			errors.rejectValue("description", "error.purchaseorderitem.description.notempty", null, "Purchase order item description cannot be blank");
		}
	}
}