package org.trescal.cwms.core.pricing;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

/**
 * Enum defining different possible types of @link Issue Method for Pricing
 * 
 */
public enum PricingIssueMethod
{
	EMAIL("pricingissuemethod.email"), 								
	ADVESO("pricingissuemethod.adveso");
	
	private String messageCode;
	private ReloadableResourceBundleMessageSource messages;
	
	@Component
	public static class PricingTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (PricingIssueMethod pt : EnumSet.allOf(PricingIssueMethod.class))
               pt.setMessageSource(messages);
        }
	}
	
	private PricingIssueMethod(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getMessageCode() {
		return this.messageCode;
	}
	
	public String getLocalizedName() {
		return messages == null ? name() : messages.getMessage(messageCode, null, LocaleContextHolder.getLocale());
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
}