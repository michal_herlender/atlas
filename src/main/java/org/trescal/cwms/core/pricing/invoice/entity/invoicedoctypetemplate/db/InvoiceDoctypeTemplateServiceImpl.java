package org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.db;

import java.util.List;

import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.InvoiceDoctypeTemplate;

public class InvoiceDoctypeTemplateServiceImpl implements InvoiceDoctypeTemplateService
{
	InvoiceDoctypeTemplateDao invoiceDoctypeTemplateDao;

	public void deleteInvoiceTemplate(InvoiceDoctypeTemplate invoicetemplate)
	{
		this.invoiceDoctypeTemplateDao.remove(invoicetemplate);
	}

	@Override
	public InvoiceDoctypeTemplate findByCompanyType(int coid, int typeid, String doctype)
	{
		return this.invoiceDoctypeTemplateDao.findByCompanyType(coid, typeid, doctype);
	}

	@Override
	public InvoiceDoctypeTemplate findByInvoice(Invoice invoice, String doctype)
	{
		InvoiceDoctypeTemplate itemp = null;

		// try and find a company specific type first
		itemp = this.findByCompanyType(invoice.getComp().getCoid(), invoice.getType().getId(), doctype);

		// if none found, look for a type specific
		if (itemp == null)
		{
			itemp = this.findByType(invoice.getType().getId(), doctype);
		}
		return itemp;
	}

	@Override
	public InvoiceDoctypeTemplate findByType(int typeid, String doctype)
	{
		return this.invoiceDoctypeTemplateDao.findByType(typeid, doctype);
	}

	public InvoiceDoctypeTemplate findInvoiceTemplate(int id)
	{
		return this.invoiceDoctypeTemplateDao.find(id);
	}

	public List<InvoiceDoctypeTemplate> getAllInvoiceTemplates()
	{
		return this.invoiceDoctypeTemplateDao.findAll();
	}

	public void insertInvoiceTemplate(InvoiceDoctypeTemplate InvoiceTemplate)
	{
		this.invoiceDoctypeTemplateDao.persist(InvoiceTemplate);
	}

	public void saveOrUpdateInvoiceTemplate(InvoiceDoctypeTemplate invoicetemplate)
	{
		this.invoiceDoctypeTemplateDao.saveOrUpdate(invoicetemplate);
	}

	public void setInvoiceTemplateDao(InvoiceDoctypeTemplateDao invoiceDoctypeTemplateDao)
	{
		this.invoiceDoctypeTemplateDao = invoiceDoctypeTemplateDao;
	}

	public void updateInvoiceTemplate(InvoiceDoctypeTemplate InvoiceTemplate)
	{
		this.invoiceDoctypeTemplateDao.update(InvoiceTemplate);
	}
}