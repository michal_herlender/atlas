package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo;

import java.util.Comparator;

/**
 * No need to sort items on @OneToMany, therefore no need for this comparator, 
 * discussing its removal
 * (and as written doesn't deal with new items saved via JPA cascade...)
 *
 */
@Deprecated
public class InvoiceItemPOComparator implements Comparator<InvoiceItemPO>
{

	@Override
	public int compare(InvoiceItemPO iipo1, InvoiceItemPO iipo2)
	{
		return ((Integer) iipo1.getId()).compareTo(iipo2.getId());
	}

}
