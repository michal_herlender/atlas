package org.trescal.cwms.core.pricing.entity.costs;

import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

/**
 * Enumerated Type that defines the different potential sources of a
 * {@link Cost} for a {@link JobCosting}, {@link ContractReview} or
 * {@link Invoice} {@link Cost}. DERIVED refers to the system calculating to
 * {@link Cost} from a number of different inputs - for example calculating a
 * repair cost from labour time and hourly rates.
 * 
 * @author richard
 */
public enum CostSource
{
	DERIVED, CONTRACT_REVIEW, JOB, QUOTATION, JOB_COSTING, INSTRUMENT, MODEL, MANUAL, PURCHASE_ORDER, INVOICE;
}