package org.trescal.cwms.core.pricing.purchaseorder.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchaseOrderHomeWrapper<Entity> {
	
	private String id;							// Lower Case English language name for html use (no spaces)
	private String name;							// Localized title for tab (a couple words)
	private String description;						// Localized description of contents (one sentence)
	private List<Entity> contents;
	private List<SupplierPOProjectionDTO> dtos;
	private boolean contentsPurchaseOrder;
	private boolean contentsQuickPurchaseOrder;
	
}
