package org.trescal.cwms.core.pricing.variableprice.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class VariablePrice {
	private BigDecimal unitPrice;
	private VariablePriceUnit unit;
	@Column(name="unitprice", nullable=false)
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	@Column(name="unit", nullable=false)
	public VariablePriceUnit getUnit() {
		return unit;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	public void setUnit(VariablePriceUnit unit) {
		this.unit = unit;
	}
}
