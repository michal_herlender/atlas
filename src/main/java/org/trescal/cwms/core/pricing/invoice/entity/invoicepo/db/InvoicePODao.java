package org.trescal.cwms.core.pricing.invoice.entity.invoicepo.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

public interface InvoicePODao extends BaseDao<InvoicePO, Integer> {}