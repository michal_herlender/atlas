package org.trescal.cwms.core.pricing.invoice.entity.invoicenote;

import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.ParentEntity;

import javax.persistence.*;

@Entity
@Table(name = "invoicenote")
public class InvoiceNote extends Note implements ParentEntity<Invoice> {
    private Invoice invoice;

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "invoiceid", unique = false, nullable = false, insertable = true, updatable = true)
    public Invoice getInvoice() {
        return invoice;
    }

    @Override
    @Transient
    public NoteType getNoteType() {
        return NoteType.INVOICENOTE;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    @Override
    public void setEntity(Invoice invoice) {
        setInvoice(invoice);
    }

    @Override
    @Transient
    public Invoice getEntity() {
        return getInvoice();
    }
}