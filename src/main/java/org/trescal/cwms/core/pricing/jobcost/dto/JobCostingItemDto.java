package org.trescal.cwms.core.pricing.jobcost.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class JobCostingItemDto {

    private Integer id;
    private String serviceTypeDisplayColor;
    private Integer jobItemTurn;
    private String serviceTypeDisplayColorFastTrack;
    private Integer jobItemId;
    private Integer jobItemNo;
    private String jobItemDefaultPage;
    private Integer instrumentId;
    private String instrumentSerialNo;
    private String instrumentPlantNo;
    private Boolean instrumentCalibrationStandard;
    private Boolean instrumentScrapped;
    private Integer instrumentModelId;
    private SubfamilyTypology typology;
    private ModelMfrType modelMfrType;
    private String instrumentMfrName;
    private Boolean genericMfr;
    private String modelName;
    private String instrumentModelName;
    private String serviceTypeShortNameTranslation;
    private String jobCostingType;
    private Boolean jobCostingItemPartOfBaseUnit;
    private Integer jobCostingItemQuantity;
    private BigDecimal generalDiscountValue;
    private BigDecimal generalDiscountRate;
    private BigDecimal finalCost;
    private Boolean showInspectionCost;
    private Long publicActiveNoteCount;
    private Long privateActiveNoteCount;
    private List<JobCostingItemCostDto> costs;
    private String customerDescription;

    public JobCostingItemDto(Integer id, String serviceTypeDisplayColor, Integer jobItemTurn,
                             String serviceTypeDisplayColorFastTrack, Integer jobItemId, Integer jobItemNo, Long contractReviewJICount,
                             Integer instrumentId, String instrumentSerialNo, String instrumentPlantNo,
                             Boolean instrumentCalibrationStandard, Boolean instrumentScrapped, Integer instrumentModelId,
                             Integer typologyId, ModelMfrType modelMfrType, String instrumentMfrName, Boolean genericMfr,
                             String modelName, String instrumentModelName, String serviceTypeShortNameTranslation,
                             PricingType jobCostingType, Boolean jobCostingItemPartOfBaseUnit, Integer jobCostingItemQuantity,
                             BigDecimal generalDiscountValue, BigDecimal generalDiscountRate, BigDecimal finalCost,
                             Boolean jobCostingItemCalibrated, Long publicActiveNoteCount, Long privateActiveNoteCount,

                             Boolean adjActive, CostType adjCostType, CostSource adjCostSrc, BigDecimal adjDiscountValue,
                             BigDecimal adjDiscountRate, BigDecimal adjTotalCost, BigDecimal adjFinalCost,

                             Boolean calActive, CostType calCostType, CostSource calCostSrc, BigDecimal calDiscountValue,
                             BigDecimal calDiscountRate, BigDecimal calTotalCost, BigDecimal calFinalCost, Integer calQuotationId,
                             String calQuotationNo, Integer calQuotationVer, Boolean calQuotationIssued, LocalDate calQuotationReqdate,
                             Integer calQuotationDuration, LocalDate calQuotationExpdate, LocalDate calQuotationIssuedate,
                             Integer calQuotationItemId, Integer calQuotationItemNo,

                             Boolean insActive, CostType insCostType, BigDecimal insDiscountValue, BigDecimal insDiscountRate,
                             BigDecimal insTotalCost, BigDecimal insFinalCost,

                             Boolean repActive, CostType repCostType, CostSource repCostSrc, BigDecimal repDiscountValue,
                             BigDecimal repDiscountRate, BigDecimal repTotalCost, BigDecimal repFinalCost,

                             Boolean purActive, CostType purCostType, CostSource purCostSrc, BigDecimal purDiscountValue,
                             BigDecimal purDiscountRate, BigDecimal purTotalCost, BigDecimal purFinalCost, String customerDescription) {
        super();
        this.id = id;
        this.serviceTypeDisplayColor = serviceTypeDisplayColor;
        this.jobItemTurn = jobItemTurn;
        this.serviceTypeDisplayColorFastTrack = serviceTypeDisplayColorFastTrack;
        this.jobItemId = jobItemId;
        this.jobItemNo = jobItemNo;
        if (contractReviewJICount > 0)
            this.jobItemDefaultPage = "jiactions.htm";
        else
            this.jobItemDefaultPage = "jicontractreview.htm";
        this.instrumentId = instrumentId;
        this.instrumentSerialNo = instrumentSerialNo;
        this.instrumentPlantNo = instrumentPlantNo;
        this.instrumentCalibrationStandard = instrumentCalibrationStandard;
        this.instrumentScrapped = instrumentScrapped;
        this.instrumentModelId = instrumentModelId;
        this.typology = SubfamilyTypology.convertFromInteger(typologyId);
        this.modelMfrType = modelMfrType;
        this.instrumentMfrName = instrumentMfrName;
        this.genericMfr = genericMfr;
        this.modelName = modelName;
        this.instrumentModelName = instrumentModelName;
        this.serviceTypeShortNameTranslation = serviceTypeShortNameTranslation;
        this.jobCostingType = jobCostingType.name();
        this.jobCostingItemPartOfBaseUnit = jobCostingItemPartOfBaseUnit;
        this.jobCostingItemQuantity = jobCostingItemQuantity;
        this.generalDiscountValue = generalDiscountValue;
        this.generalDiscountRate = generalDiscountRate;
        this.finalCost = finalCost;
        this.showInspectionCost = !jobCostingItemCalibrated || ((adjActive != null) && adjActive) || ((repActive != null) && repActive)
            || ((purActive != null) && purActive);
        this.publicActiveNoteCount = publicActiveNoteCount;
        this.privateActiveNoteCount = privateActiveNoteCount;
        this.costs = new ArrayList<>();
        if (adjActive != null)
            this.costs.add(new JobCostingItemCostDto(adjActive, adjCostType, adjCostSrc, adjDiscountValue,
                adjDiscountRate, adjTotalCost, adjFinalCost));
        if (calActive != null)
            this.costs.add(new JobCostingItemCostDto(calActive, calCostType, calCostSrc, calDiscountValue,
                calDiscountRate, calTotalCost, calFinalCost, calQuotationId, calQuotationNo, calQuotationVer,
                calQuotationIssued, calQuotationReqdate, calQuotationDuration, calQuotationExpdate,
                calQuotationIssuedate, calQuotationItemId, calQuotationItemNo));
        if (insActive != null)
            this.costs.add(new JobCostingItemCostDto(insActive, insCostType, insDiscountValue, insDiscountRate,
                insTotalCost, insFinalCost));
        if (repActive != null)
            this.costs.add(new JobCostingItemCostDto(repActive, repCostType, repCostSrc, repDiscountValue,
                repDiscountRate, repTotalCost, repFinalCost));
        if (purActive != null)
            this.costs.add(new JobCostingItemCostDto(purActive, purCostType, purCostSrc, purDiscountValue,
                purDiscountRate, purTotalCost, purFinalCost));
        this.customerDescription = customerDescription;
    }
}
