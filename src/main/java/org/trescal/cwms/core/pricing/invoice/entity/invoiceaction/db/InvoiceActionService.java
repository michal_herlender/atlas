package org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.db;

import java.util.List;

import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;

public interface InvoiceActionService
{
	InvoiceAction findInvoiceAction(int id);
	void insertInvoiceAction(InvoiceAction invoiceaction);
	void updateInvoiceAction(InvoiceAction invoiceaction);
	List<InvoiceAction> getAllInvoiceActions();
	void deleteInvoiceAction(InvoiceAction invoiceaction);
	void saveOrUpdateInvoiceAction(InvoiceAction invoiceaction);
}