package org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingLinkedCost;

@Entity
@Table(name = "jobcostinglinkedrepaircost")
public class JobCostingLinkedRepairCost extends JobCostingLinkedCost
{
	private JobCostingRepairCost repairCost;

	private ContractReviewRepairCost contractReviewRepairCost;
	private JobCostingRepairCost jobCostingRepairCost;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "contractrevrepcostid")
	public ContractReviewRepairCost getContractReviewRepairCost()
	{
		return this.contractReviewRepairCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostrepcostid")
	public JobCostingRepairCost getJobCostingRepairCost()
	{
		return this.jobCostingRepairCost;
	}

	@OneToOne(fetch=FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
	@JoinColumn(name = "costid", nullable = false, unique = true)
	public JobCostingRepairCost getRepairCost()
	{
		return this.repairCost;
	}

	public void setContractReviewRepairCost(ContractReviewRepairCost contractReviewRepairCost)
	{
		this.contractReviewRepairCost = contractReviewRepairCost;
	}

	public void setJobCostingRepairCost(JobCostingRepairCost jobCostingRepairCost)
	{
		this.jobCostingRepairCost = jobCostingRepairCost;
	}

	public void setRepairCost(JobCostingRepairCost repairCost)
	{
		this.repairCost = repairCost;
	}
}
