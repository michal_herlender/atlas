package org.trescal.cwms.core.pricing.purchaseorder.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class POAddRequestDTO {

    private Integer jobid;
    private Integer jobitemid;
    private Integer personid;
    private Integer addressid;
    private Integer returnToAddressid;
    private List<Integer> jobitemids;
    private List<String> descs;
    private List<String> prices;
    private List<String> discounts;
    private List<Integer> nominalids;
    private List<Integer> costtypeids;
    private String currencyCode;
    private String deliverydate;
    private String supplierref;
    private List<String> qtys;

}
