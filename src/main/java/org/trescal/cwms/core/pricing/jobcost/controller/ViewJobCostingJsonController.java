package org.trescal.cwms.core.pricing.jobcost.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.tools.filebrowser.FileWrapper;

@Controller 
@IntranetController
public class ViewJobCostingJsonController
{
	@Autowired
	private FileBrowserService fileBrowserServ;
	
	@RequestMapping(value="/jobcostingfiles.json", method=RequestMethod.GET)
	public @ResponseBody ResultWrapper getJobCostingFiles(@RequestParam(value="id", required=true) Integer id,
			@RequestParam(value="jobcostingversion", required=true) Integer jcversion)
	{
		FileBrowserWrapper rw = (FileBrowserWrapper) fileBrowserServ
				.getFilesForComponentRoot(Component.JOB_COSTING, id, null);
		List<FileWrapper> files = rw.getFiles();
		List<FileWrapper> result = new ArrayList<FileWrapper>();
		for (FileWrapper file : files) {
			if(!file.isDirectory() && file.getFileName().contains("vers "+jcversion))
			result.add(file);
		}
		return new ResultWrapper(true, null, result, null);
	}
}