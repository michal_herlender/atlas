package org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupportImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost;

@Repository("JobCostingInspectionCostDao")
public class JobCostingInspectionCostDaoImpl extends JobCostingCostDaoSupportImpl<JobCostingInspectionCost> implements JobCostingInspectionCostDao {
	
	@Override
	protected Class<JobCostingInspectionCost> getEntity() {
		return JobCostingInspectionCost.class;
	}
}