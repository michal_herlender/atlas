package org.trescal.cwms.core.pricing.jobcost.form;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.pricing.PricingType;

import lombok.Setter;

@Setter
public class CreateJobCostingForm {

	private Integer jobid;
	private Integer jobCostingTypeId;
	private List<Integer> additionalPersonids;
	private List<Integer> jobItemIds;
	private List<Integer> expenseItemIds;
	private Integer personid;
	private PricingType clientCosting;
	private String clientref;

	public CreateJobCostingForm() {
		super();
		additionalPersonids = new ArrayList<Integer>();
		jobItemIds = new ArrayList<Integer>();
		expenseItemIds = new ArrayList<Integer>();
	}

	@NotNull
	public Integer getJobid() {
		return jobid;
	}

	@NotNull(message = "{error.value.notselected}")
	@Min(value = 1, message = "{error.value.notselected}")
	public Integer getJobCostingTypeId() {
		return jobCostingTypeId;
	}

	public List<Integer> getAdditionalPersonids() {
		return additionalPersonids;
	}

	public List<Integer> getJobItemIds() {
		return jobItemIds;
	}

	public List<Integer> getExpenseItemIds() {
		return expenseItemIds;
	}

	@NotNull(message = "{error.value.notselected}")
	@Min(value = 1, message = "{error.value.notselected}")
	public Integer getPersonid() {
		return personid;
	}

	@NotNull(message = "{error.value.notselected}")
	public PricingType getClientCosting() {
		return clientCosting;
	}

	// Optional field
	@Length(max = 100)
	public String getClientref() {
		return clientref;
	}
}