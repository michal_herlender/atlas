package org.trescal.cwms.core.pricing.invoice.view;

public class InvoiceXlsxDefinitions {
	// Message codes and default text for invoice export  
	
	// Sheet names
	public static String CODE_INVOICE_ITEMS = "invoice.invoiceitems";
	public static String TEXT_INVOICE_ITEMS = "Invoice Items";
	
	public static String CODE_PERIODIC_LINKING = "invoiceexport.periodiclinking";
	public static String TEXT_PERIODIC_LINKING = "Periodic Linking";
	
	// Summary labels
	public static String CODE_EXPENSE_ITEMS = "invoiceexport.expenseitems";
	public static String TEXT_EXPENSE_ITEMS = "Expense Items";
	
	public static String CODE_INVOICE_NUMBER = "invoiceexport.invoicenumber";
	public static String TEXT_INVOICE_NUMBER = "Invoice Number";
	
	public static String CODE_INVOICE_TYPE = "invoiceexport.invoicetype";
	public static String TEXT_INVOICE_TYPE = "Invoice Type";
	
	public static String CODE_INVOICE_STATUS = "invoiceexport.invoicestatus";
	public static String TEXT_INVOICE_STATUS = "Invoice Status";
	
	public static String CODE_SUBTOTAL_JOB_ITEMS = "invoiceexport.subtotaljobItems";
	public static String TEXT_SUBTOTAL_JOB_ITEMS = "Subtotal - Job Items";
	
	public static String CODE_SUBTOTAL_EXPENSE_ITEMS = "invoiceexport.subtotalexpenseitems";
	public static String TEXT_SUBTOTAL_EXPENSE_ITEMS = "Subtotal - Expense Items";
	
	public static String CODE_SUBTOTAL_OTHER_ITEMS = "invoiceexport.subtotalotheritems";
	public static String TEXT_SUBTOTAL_OTHER_ITEMS = "Subtotal - Other Items";
	
	public static String CODE_LINKED_JOB_ITEM_VALUE = "invoiceexport.linkedjobitemvalue";
	public static String TEXT_LINKED_JOB_ITEM_VALUE = "Value of linked job items";
	
	public static String CODE_LINKED_EXPENSE_ITEM_VALUE = "invoiceexport.linkedexpenseitemvalue";
	public static String TEXT_LINKED_EXPENSE_ITEM_VALUE = "Value of linked expense items";
	
	public static String CODE_TOTAL_INVOICE_AMOUNT = "invoiceexport.totalinvoiceamount";
	public static String TEXT_TOTAL_INVOICE_AMOUNT = "Total Invoice Amount";
	
	// Detail labels
	
	public static String CODE_JOB_ITEMS = "invoiceexport.jobitems";
	public static String TEXT_JOB_ITEMS = "Job Items";
	
	public static String CODE_OTHER_ITEMS = "invoiceexport.otheritems";
	public static String TEXT_OTHER_ITEMS = "Other Items";
	
	public static String CODE_INVOICE_ITEM_NO = "invoiceexport.invoiceitemno";
	public static String TEXT_INVOICE_ITEM_NO = "Invoice Item No";
	
	public static String CODE_SEQUENCE_NO = "invoiceexport.sequenceno";
	public static String TEXT_SEQUENCE_NO = "Sequence No";
	
	public static String CODE_CLIENT_COMPANY = "invoiceexport.clientcompany";
	public static String TEXT_CLIENT_COMPANY = "Client Company";
	
	public static String CODE_ON_BEHALF_OF = "invoiceexport.onbehalfof";
	public static String TEXT_ON_BEHALF_OF = "On behalf of";
	
	public static String CODE_CLIENT_SUBDIVISION = "invoiceexport.clientsubdivision";
	public static String TEXT_CLIENT_SUBDIVISION = "Client Subdivision";
	
	public static String CODE_JOB_NUMBER = "invoiceexport.jobnumber";
	public static String TEXT_JOB_NUMBER = "Job Number";
	
	public static String CODE_JOB_ITEM_NO = "invoiceexport.jobitemno";
	public static String TEXT_JOB_ITEM_NO = "Job Item No";
	
	public static String CODE_JOB_SERVICE_NO = "invoiceexport.jobserviceno";
	public static String TEXT_JOB_SERVICE_NO = "Job Service No";
	
	public static String CODE_JOB_CONTACT = "invoiceexport.jobcontact";
	public static String TEXT_JOB_CONTACT = "Job Contact";
	
	public static String CODE_INSTRUMENT_MODEL_NAME = "invoiceexport.instrumentmodelname";
	public static String TEXT_INSTRUMENT_MODEL_NAME = "Instrument Model Name";

	public static String CODE_ITEM_DESCRIPTION = "invoiceexport.itemdescription";
	public static String TEXT_ITEM_DESCRIPTION = "Item Description";
	
	public static String CODE_CLIENT_DESCRIPTION = "invoiceexport.clientdescription";
	public static String TEXT_CLIENT_DESCRIPTION = "Client Description";
	
	public static String CODE_TRESCAL_ID = "invoiceexport.trescalid";
	public static String TEXT_TRESCAL_ID = "Trescal ID";
	
	public static String CODE_CLIENT_ID = "invoiceexport.clientid";
	public static String TEXT_CLIENT_ID = "Client ID";
	
	public static String CODE_SERIAL_NUMBER = "invoiceexport.serialnumber";
	public static String TEXT_SERIAL_NUMBER = "Serial Number";
	
	public static String CODE_SERVICE_TYPE = "invoiceexport.servicetype";
	public static String TEXT_SERVICE_TYPE = "Service Type";
	
	public static String CODE_DELIVERY_DATE = "invoiceexport.deliverydate";
	public static String TEXT_DELIVERY_DATE = "Delivery Date";
	
	public static String CODE_DELIVERY_NUMBER = "invoiceexport.deliverynumber";
	public static String TEXT_DELIVERY_NUMBER = "Delivery Number";
	
	public static String CODE_JOB_CLIENT_REF = "invoiceexport.jobclientref";
	public static String TEXT_JOB_CLIENT_REF = "Job Client Ref";
	
	public static String CODE_JOB_ITEM_CLIENT_REF = "invoiceexport.jobitemclientref";
	public static String TEXT_JOB_ITEM_CLIENT_REF = "Job Item Client Ref";
	
	public static String CODE_CLIENT_PURCHASE_ORDER = "invoiceexport.clientpurchaseorder";
	public static String TEXT_CLIENT_PURCHASE_ORDER = "Client PO";
	
	public static String CODE_PRICE_SOURCE = "invoiceexport.pricesource";
	public static String TEXT_PRICE_SOURCE = "Price Source";
	
	public static String CODE_VERSION = "invoiceexport.version";
	public static String TEXT_VERSION = "Version";
	
	public static String CODE_AMOUNT = "invoiceexport.amount";
	public static String TEXT_AMOUNT = "Amount";
	
	public static String CODE_UNIT_PRICE = "invoiceexport.unitprice";
	public static String TEXT_UNIT_PRICE = "Unit Price";
	
	public static String CODE_QUANTITY = "invoiceexport.quantity";
	public static String TEXT_QUANTITY = "Quantity";
	
	public static String CODE_FINAL_PRICE = "invoiceexport.finalprice";
	public static String TEXT_FINAL_PRICE = "Final Price";
	
	public static String CODE_CURRENCY = "invoiceexport.currency";
	public static String TEXT_CURRENCY = "Currency";
	
	public static String CODE_COMMENT = "comment";
	public static String TEXT_COMMENT = "Comment";
	
	public static String CODE_SINGLE_PRICE = "singleprice";
	public static String TEXT_SINGLE_PRICE = "Single Price";
}