package org.trescal.cwms.core.pricing.entity.costs.base.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.entity.costs.base.RepCost;

@Repository("RepCostDao")
public class RepCostDaoImpl extends BaseDaoImpl<RepCost, Integer> implements RepCostDao {
	
	@Override
	protected Class<RepCost> getEntity() {
		return RepCost.class;
	}
}