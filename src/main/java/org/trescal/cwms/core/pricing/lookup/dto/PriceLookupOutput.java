package org.trescal.cwms.core.pricing.lookup.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter @Setter
public abstract class PriceLookupOutput {
	// For correlation with inputs
	protected Integer serviceTypeId;
	protected Integer instrumentId;
	protected Integer instrumentModelId;
	protected Integer salesCategoryId;
	protected Integer supportedCurrencyId;

	// outputs for quotation results
	protected Integer quotationItemId;
	protected Integer quotationCalCostId;
	protected BigDecimal quotationCalCostDiscountRate;
	protected BigDecimal quotationCalCostTotalCost;
	protected Integer quotationId;
	protected Boolean quotationAccepted;
	protected LocalDate quotationExpiryDate;
	protected Boolean quotationIssued;
	protected LocalDate quotationIssuedDate;
	
	// outputs for invoice results
	protected Integer invoiceItemId;
	protected Integer invoiceId;
	protected String invoiceNo;
	protected Integer itemno;
	protected LocalDate issueDate;
	protected BigDecimal generalDiscountRate;
	protected BigDecimal finalPriceForInvoiceItem;
	protected String currencyInvoice;

	// Updated by post processing (preferredQuotation for quotation queries only) 
	protected PriceLookupQueryType queryType;
	protected Boolean preferredQuotation;

	// output for catalog price results
	protected Integer catalogPriceId;
	protected BigDecimal catalogPrice;

	// TODO when we have work program / capability on quotation item, add here and use for job item population?
	
	public PriceLookupOutput() {
		// default false, updated after query
		this.preferredQuotation = false;
	}
}
