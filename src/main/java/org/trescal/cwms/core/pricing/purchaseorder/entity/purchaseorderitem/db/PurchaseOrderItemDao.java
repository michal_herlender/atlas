package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.ClientCompanyDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;

public interface PurchaseOrderItemDao extends BaseDao<PurchaseOrderItem, Integer> {

	List<PurchaseOrderItemDTO> findAllFromPurchaseOrder(Integer poId);

	List<PurchaseOrderItem> getAllSubcontracting(Company subcontractor, Date begin, Date end,
			PurchaseOrderItemReceiptStatus receiptStatus, Boolean invoiced);
	
	List<ClientCompanyDTO> getClientCompanyDTOFromPO(Integer poId);
}