package org.trescal.cwms.core.pricing.entity.costs;

import java.math.BigDecimal;
import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

/**
 * DTO object used to marshall lookups for different cost sources.
 * 
 * @author Richard
 */
public class CostLookUp
{
	/**
	 * List of all {@link CostSource} types to look up.
	 */
	List<CostSource> costSources;

	/**
	 * List of all matching {@link QuotationCalibrationCost} from
	 * {@link Quotation} already linked to the {@link Job}.
	 */
	List<QuotationCalibrationCost> linkedQuotationCosts;
	/**
	 * List of all matching {@link QuotationCalibrationCost} from
	 * {@link Quotation} not already linked to the {@link Job}.
	 */
	List<QuotationCalibrationCost> unLinkedQuotationCosts;

	/**
	 * The matching (if found) {@link CatalogPrice} value in the required currency for the the {@link InstrumentModel}.
	 */
	List<BigDecimal> catalogPrices;
	
	/**
	 * List of all matching {@link JobCostingCalibrationCost} that match the
	 * {@link InstrumentModel}.
	 */
	List<JobCostingCalibrationCost> jobCostingCosts;

	/**
	 * List of all matching {@link JobCostingCalibrationCost} that match the
	 * {@link Instrument}.
	 */
	List<JobCostingCalibrationCost> plantJobCostingCosts;
	
	/**
	 * Translated Service Type name related to exact catalog price for job item.
	 * 
	 */
	String translatedServiceTypeName;
	
	/**
	 * Translated full model name
	 * 
	 */
	String translatedFullModelName;
	
	/**
	 * Currency of entity we are doing lookup for
	 */
	SupportedCurrency currency;

	String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public SupportedCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(SupportedCurrency currency) {
		this.currency = currency;
	}

	public String getTranslatedServiceTypeName() {
		return translatedServiceTypeName;
	}

	public void setTranslatedServiceTypeName(String translatedServiceTypeName) {
		this.translatedServiceTypeName = translatedServiceTypeName;
	}

	public List<CostSource> getCostSources()
	{
		return this.costSources;
	}

	public List<JobCostingCalibrationCost> getJobCostingCosts()
	{
		return this.jobCostingCosts;
	}

	public List<QuotationCalibrationCost> getLinkedQuotationCosts()
	{
		return this.linkedQuotationCosts;
	}

	public List<JobCostingCalibrationCost> getPlantJobCostingCosts()
	{
		return this.plantJobCostingCosts;
	}

	public List<QuotationCalibrationCost> getUnLinkedQuotationCosts()
	{
		return this.unLinkedQuotationCosts;
	}

	public void setCostSources(List<CostSource> costSources)
	{
		this.costSources = costSources;
	}

	public void setJobCostingCosts(List<JobCostingCalibrationCost> jobCostingCosts)
	{
		this.jobCostingCosts = jobCostingCosts;
	}

	public void setLinkedQuotationCosts(List<QuotationCalibrationCost> linkedQuotationCosts)
	{
		this.linkedQuotationCosts = linkedQuotationCosts;
	}

	public void setPlantJobCostingCosts(List<JobCostingCalibrationCost> plantJobCostingCosts)
	{
		this.plantJobCostingCosts = plantJobCostingCosts;
	}

	public void setUnLinkedQuotationCosts(List<QuotationCalibrationCost> unLinkedQuotationCosts)
	{
		this.unLinkedQuotationCosts = unLinkedQuotationCosts;
	}

	public List<BigDecimal> getCatalogPrices() {
		return catalogPrices;
	}

	public void setCatalogPrices(List<BigDecimal> catalogPrices) {
		this.catalogPrices = catalogPrices;
	}

	public String getTranslatedFullModelName() {
		return translatedFullModelName;
	}

	public void setTranslatedFullModelName(String translatedFullModelName) {
		this.translatedFullModelName = translatedFullModelName;
	}

}
