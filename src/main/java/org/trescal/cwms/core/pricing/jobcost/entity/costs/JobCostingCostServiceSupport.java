package org.trescal.cwms.core.pricing.jobcost.entity.costs;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.ThirdPartyCostSupport;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.ThirdPartyCostSupportService;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.markups.markuprange.db.MarkupRangeService;

public abstract class JobCostingCostServiceSupport<Entity extends Cost> implements JobCostingCostService<Entity>
{
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	protected MarkupRangeService markupRangeServ;
	@Autowired
	protected ThirdPartyCostSupportService thirdCostService;

	// injected properties

	/**
     * If true then if the current cost has a value of 0.00 then it will be set
     * to deactivated.
     * 2021-06-29 This property is initialized per cost type in concrete subclasses
     */
    public abstract boolean getDeactivateCostOnZeroValue();

	/**
	 * If true then system will copy the most recent previous
	 * {@link JobCostingItem} costs for this item rather than calculating an
	 * entirely new set of costs.
	 */
    @Value("${cwms.config.costing.costs.copymostrecentcosting}")
	protected boolean copyMostRecentPreviousCosting;

    @Value("${cwms.config.quotation.resultsyearfilter}")
	protected Integer resultsYearFilter;
    @Value("${cwms.config.costs.costings.roundupnearest50}")
	protected boolean roundUpFinalCosts;

	@Override
	public ThirdPartyCostSupport configureTPCosts(ThirdPartyCostSupport costSupport)
	{
		costSupport.setThirdCostSrc(ThirdCostSource.MANUAL);
		costSupport.setThirdCostTotal(new BigDecimal("0.00"));
		costSupport.setThirdManualPrice(new BigDecimal("0.00"));
		costSupport.setThirdMarkupRate(new BigDecimal("0.00"));
		costSupport.setThirdMarkupSrc(ThirdCostMarkupSource.MANUAL);
		costSupport.setThirdMarkupValue(new BigDecimal("0.00"));
		return costSupport;
	}

	public abstract Cost createCostFromCopy(JobCostingItem ji, JobCostingItem old);

	/**
	 * Creates a new {@link Cost} for the given {@link JobCostingItem}. Checks
	 * if job costing lookups are enabled and if so looks up the most suitable
	 * cost for the item based on the given {@link CostSource} hierarchy of the
	 * system default hierarchy.
	 * 
	 * @param ji the {@link JobItem}.
	 * @return a {@link Cost}.
	 */
	public Cost loadCost(JobCostingItem ji, JobCostingItem mostRecent)
	{
		Cost cost = null;

		// if mostRecent is an actual costing and copyMostRecent is set as true
		// then copy the costs of a previous costing over to this cost,
		// otherwise try and create the costs from scratch using whichever
		// costsource has been defined.
		if ((mostRecent != null) && this.copyMostRecentPreviousCosting)
		{
			this.logger.info("copyMostRecentPreviousCosting is set to true, looking up costs from previous costing ["
					+ mostRecent.getJobCosting().getId() + "].");
			cost = this.createCostFromCopy(ji, mostRecent);
		}

		// if no cost could be found from the mostRecent or if previous cost
		// copying is disabled then search for a cost using the lookup hierarchy
		if ((cost == null) || !this.copyMostRecentPreviousCosting)
		{
			this.logger.info("No other costs found or copyMostRecentPreviousCosting is not set to true, looking up costs.");

			if (this.getLookupCosts())
			{
				if ((getCostSourceHierarchy() != null)
						&& !getCostSourceHierarchy().trim().equals(""))
				{
					this.logger.info("Overridden cost lookup pattern defined, beginning search");

					// split the specified cost hierarchy string into separate
					// entries
					String[] sourceHr = getCostSourceHierarchy().split(",");

					this.logger.info("Using cost lookup pattern: " + getCostSourceHierarchy());

					// convert the given list into a hierarchy list
					for (String sourceStr : sourceHr)
					{
						try
						{
							CostSource source = CostSource.valueOf(sourceStr.trim());
							cost = this.resolveCostLookup(source, ji);
							if (cost != null)
							{
								break;
							}
						}
						catch (IllegalArgumentException iae)
						{
							// spit out the error
							iae.printStackTrace();
						}
					}
				}
				else
				{
					this.logger.info("No specific cost lookup pattern defined, using defaults.");
					// use the default cost hierarchy
					for (CostSource source : CostSource.values())
					{
						cost = this.resolveCostLookup(source, ji);
						if (cost != null)
						{
							break;
                        }
                    }
                }
            } else {
                this.logger.info("looking up costs disabled, setting 0 costs.");
                cost = this.resolveCostLookup(CostSource.MANUAL, ji);
            }
        }
        if (this.getDeactivateCostOnZeroValue()) {
            // if cost is zero and deactivateCostOnZeroValue is true then
            // deactivate the cost
            if ((cost.getFinalCost() == null)
                || (cost.getFinalCost().doubleValue() == 0)) {
                cost.setActive(false);
			}
		}

		return cost;
	}

	public abstract Cost resolveCostLookup(CostSource source, JobCostingItem ji);

	/**
	 * Defines the lookup precedence hierarchy for looking up calibration costs
	 * for {@link JobItem}.  Was defined as "for unit testing only" an Antech code.  -GB 
	 */
	public abstract String getCostSourceHierarchy();
	/**
	 * If true system will attempt to lookup sales costs when adding a jobitem,
	 * if false then model defaults are used.
	 */
	public abstract boolean getLookupCosts();

}
