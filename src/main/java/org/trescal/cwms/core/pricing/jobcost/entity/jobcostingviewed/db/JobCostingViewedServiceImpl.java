package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.db;

import java.util.List;

import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed;

public class JobCostingViewedServiceImpl implements JobCostingViewedService
{
	private JobCostingViewedDao jobCostingViewedDao;

	public void deleteJobCostingViewed(JobCostingViewed jobcostingviewed)
	{
		this.jobCostingViewedDao.remove(jobcostingviewed);
	}

	public JobCostingViewed findJobCostingViewed(int id)
	{
		return this.jobCostingViewedDao.find(id);
	}

	public List<JobCostingViewed> getAllJobCostingVieweds()
	{
		return this.jobCostingViewedDao.findAll();
	}

	public void insertJobCostingViewed(JobCostingViewed JobCostingViewed)
	{
		this.jobCostingViewedDao.persist(JobCostingViewed);
	}

	public void setJobCostingViewedDao(JobCostingViewedDao jobCostingViewedDao)
	{
		this.jobCostingViewedDao = jobCostingViewedDao;
	}

	public void updateJobCostingViewed(JobCostingViewed JobCostingViewed)
	{
		this.jobCostingViewedDao.update(JobCostingViewed);
	}
}