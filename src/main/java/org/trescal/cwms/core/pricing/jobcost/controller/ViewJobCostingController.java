package org.trescal.cwms.core.pricing.jobcost.controller;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.JobCostingClientDecision;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingExpenseItemDto;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContactEqualityComparator;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.db.AdditionalCostContactService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.db.JobCostingExpenseItemService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingtype.JobCostingType;
import org.trescal.cwms.core.pricing.jobcost.form.JobCostingForm;
import org.trescal.cwms.core.pricing.jobcost.form.ViewJobCostingValidator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;

/**
 * Controller used to display a {@link JobCosting}. No form action implemented
 * just yet but basic onSubmit() method implemented for future extension.
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.HTTPSESS_NEW_FILE_LIST, Constants.HTTPSESS_PRICING_CLASS_NAME,
		Constants.SESSION_ATTRIBUTE_USERNAME })
public class ViewJobCostingController {
	
	private static final Logger logger = LoggerFactory.getLogger(ViewJobCostingController.class);

	@Value("#{props['cwms.config.jobitem.fast_track_turn']}")
	private int fastTrackTurn;
	@Value("#{props['cwms.config.costs.costings.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Autowired
	private AdditionalCostContactService addCostContactServ;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private InstructionLinkService instructionLinkServ;
	@Autowired
	private JobCostingService jobCostServ;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private UserService userService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyServ;
	@Autowired
	private ViewJobCostingValidator validator;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private JobCostingItemService jobCostingItemService;
	@Autowired
	private JobCostingExpenseItemService jobCostingExpenseItemService;
	@Autowired
	private StatusService statusService;
	@Autowired
	private TaxCalculator taxCalculator;

	public static final String FORM_NAME = "command";

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.dtf_ISO8601, true);
		binder.registerCustomEditor(LocalDateTime.class,
				new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
		binder.registerCustomEditor(Date.class, editor);
	}

	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}

	@ModelAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME)
	public Object defineJobCostingClass() {
		return JobCosting.class;
	}

	protected TreeSet<AdditionalCostContact> cleanAdditionalContactList(JobCosting costing, List<Integer> personids) {
		Map<Integer, AdditionalCostContact> adds = new HashMap<>();
		Set<AdditionalCostContact> existing = costing.getAdditionalContacts();
		for (AdditionalCostContact cons : existing) {
			if (personids.contains(cons.getContact().getPersonid())) {
				adds.put(cons.getContact().getPersonid(), cons);
			} else {
				this.addCostContactServ.deleteAdditionalCostContact(cons);
			}
		}
		
		// add any new contacts
		if (personids != null) {
			for (Integer personid : personids) {
				if ((personid != null) && !adds.containsKey(personid)) {
					AdditionalCostContact con = new AdditionalCostContact();
					con.setContact(this.contactServ.get(personid));
					con.setCosting(costing);
					adds.put(con.getContact().getPersonid(), con);
				}
			}
		}
		// convert into treeset and return
		TreeSet<AdditionalCostContact> finalList = new TreeSet<>(new AdditionalCostContactEqualityComparator());
		finalList.addAll(adds.values());
		return finalList;
	}

	@ModelAttribute("command")
	protected JobCostingForm formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value = "id", required = false, defaultValue = "0") Integer id) throws Exception {
		Contact currentContact = this.userService.get(username).getCon();
		JobCosting costing = this.jobCostServ.findJobCosting(id);
		costing.setSentEmails(this.emailServ.getAllComponentEmails(Component.JOB_COSTING, id));
		costing.getJcViewed().setLastViewedOn(new Date());
		costing.getJcViewed().setLastViewedBy(currentContact);
		
		costing.getJob().setRelatedInstructionLinks(this.instructionLinkServ.getAllForJob(costing.getJob()));
		if (id == 0) {
			logger.error("Job Costing with id " + id + " could not be found");
			throw new Exception("Could not find job costing");
		}
		JobCostingForm form = new JobCostingForm();
		form.setJobCosting(costing);
		form.setPersonid(costing.getContact().getPersonid());
		form.setJobCostingTypeId(costing.getCostingType().getId());
		form.setDiscountRate(new BigDecimal("0.00"));
		form.setJobCostingStatusId(costing.getStatus().getStatusid());
		if (costing.getLastClientApproval() != null) {
			form.setClientApprovalOn(
					DateTools.dateToLocalDateTime(costing.getLastClientApproval().getClientApprovalOn()));
			form.setClientApprovalComment(costing.getLastClientApproval().getClientApprovalComment());
		}
		// form.setTotalCost(costing.getTotalCost());
		return form;
	}

	@RequestMapping(value = "/viewjobcosting.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model, @Validated @ModelAttribute(FORM_NAME) JobCostingForm form,
			BindingResult bindingResult, @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model, newFiles, username, form, locale);
		}
		Contact currentContact = this.userService.get(username).getCon();
		JobCosting costing = form.getJobCosting();
		if (costing.getContact().getPersonid() != form.getPersonid()) {
			costing.setContact(this.contactServ.get(form.getPersonid()));
		}
		if (costing.getCostingType().getId() != form.getJobCostingTypeId()) {
			costing.setCostingType(JobCostingType.getJobCostingTypeById(form.getJobCostingTypeId()));
		}
		// user is applying the discount
		if (form.isApplyDiscountToCalibration()) {
			// only applying discounts to cal costs
			for (JobCostingItem item : costing.getItems()) {
				item.getCalibrationCost().setDiscountRate(form.getDiscountRate());
				item.setCalibrationCost((JobCostingCalibrationCost) CostCalculator
						.updateSingleCost(item.getCalibrationCost(), this.roundUpFinalCosts));
				CostCalculator.updateTPItemCostWithRunningTotal(item);
				CostCalculator.updateItemCost(item);
			}
		} else if (form.isApplyDiscount()) {
			// iterate over the items on the costing, set the discount rate and
			// update the item totals
			for (JobCostingItem item : costing.getItems()) {
				item.setGeneralDiscountRate(form.getDiscountRate());
				CostCalculator.updateItemCost(item);
			}
		}
		// site costings have a manually set total cost, all other costings have
		// their total costs calculated from the sum of their item totals
		if (costing.getType().equals(PricingType.SITE)) {
			final HashSet<PricingItem> allItems = new HashSet<>();
			allItems.addAll(costing.getItems());
			allItems.addAll(costing.getExpenseItems());
			CostCalculator.updatePricingTotalCost(costing, allItems);
			taxCalculator.calculateAndSetVATaxAndFinalCost(costing);
		} else {
			// carriage cost might have been updated, so recalculate the costs
			Set<PricingItem> pricingItems = new HashSet<>();
			pricingItems.addAll(costing.getItems());
			pricingItems.addAll(costing.getExpenseItems());
			CostCalculator.updatePricingTotalCost(costing, pricingItems);
			taxCalculator.calculateAndSetVATaxAndFinalCost(costing);
		}
		// clean up additional cost contacts
		costing.setAdditionalContacts(this.cleanAdditionalContactList(costing, form.getAdditionalPersonids()));
		// set status if updated
		if (form.getJobCostingStatusId() != null
				&& !form.getJobCostingStatusId().equals(costing.getStatus().getStatusid()))
			costing.setStatus((JobCostingStatus) this.statusService.get(form.getJobCostingStatusId()));

		this.jobCostServ.saveOrUpdate(costing, currentContact);
		model.asMap().clear();
		return "redirect:viewjobcosting.htm?id=" + costing.getId();
	}

	@RequestMapping(value = "/viewjobcosting.htm", method = RequestMethod.POST, params = "setclientdecision")
	protected String onSetClientResponse(Model model, @Validated @ModelAttribute(FORM_NAME) JobCostingForm form,
			BindingResult bindingResult, @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model, newFiles, username, form, locale);
		}
		Contact currentContact = this.userService.get(username).getCon();
		JobCosting costing = form.getJobCosting();
		this.jobCostServ.setJobCostingClientResponse(costing, form, currentContact);

		return "redirect:viewjobcosting.htm?id=" + costing.getId();
	}

	@RequestMapping(value = "/viewjobcosting.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(FORM_NAME) JobCostingForm form, Locale locale) throws Exception {

		model.addAttribute("jobcosttypelist", Arrays.asList(JobCostingType.values()));
		// get files in the root directory
		JobCosting jobCosting = form.getJobCosting();
		Job job = jobCosting.getJob();
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.JOB_COSTING,
				jobCosting.getId(), newFiles);

		// get all other costing for this job
		model.addAttribute("othercostings", job.getJobCostings());
		model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.JOB_COSTING));
		model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.addAttribute(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
		// get a list of Contacts belonging to this contact's division
		model.addAttribute("conlist",
				this.contactServ.getAllActiveSubdivContacts(jobCosting.getContact().getSub().getSubdivid()));
		model.addAttribute("defaultCurrency", this.supportedCurrencyServ.getDefaultCurrency());
		model.addAttribute("privateOnlyNotes", NoteType.JOBCOSTINGITEMNOTE.isPrivateOnly());
		// add datetools
		model.addAttribute(Constants.FAST_TRACK_TURN, this.fastTrackTurn);
		model.addAttribute("requiresIssue", this.jobCostServ.costingRequiresIssue(jobCosting));
		User user = this.userService.get(username);
		model.addAttribute("userInstructionTypes", user.getCon().getUserPreferences().getUserInstructionTypes());
		
		List<InstructionLink<?>> instLinks =  this.instructionLinkServ.getAllForJob(job).stream()
				.filter(instructionLink -> (instructionLink.getInstruction().getInstructiontype().equals(InstructionType.COSTING) ||
						instructionLink.getInstruction().getInstructiontype().equals(InstructionType.DISCOUNT)))
				.collect(Collectors.toList()); 		
		model.addAttribute("instLinks", instLinks);

		boolean islinkToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
				job.getCon().getSub().getSubdivid(),
				user.getCon().getSub().getComp().getCoid(), new Date());
		model.addAttribute("isLinkedToAdveso", islinkToAdveso);

		List<JobCostingItemDto> jobCostingItemsDto = jobCostingItemService
				.getJobCostingItemsDtoByJobCostingId(jobCosting.getId(), locale);

		model.addAttribute("JobCostingItemsDto", jobCostingItemsDto);
		model.addAttribute("discounts", this.jobCostServ.getJobCostingTotalDiscount(jobCostingItemsDto));

		List<JobCostingExpenseItemDto> jobCostingExpenseItemsDto = jobCostingExpenseItemService
				.getJobCostingExpenseItemsDtoByJobCostingId(jobCosting.getId(), locale);

		model.addAttribute("jobCostingExpenseItemsDto", jobCostingExpenseItemsDto);
		model.addAttribute("clientApprovals", JobCostingClientDecision.values());
		model.addAttribute("showVAT", !avalaraAware);
		return "trescal/core/pricing/jobcost/viewjobcosting";
	}

}