package org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity;

import org.hibernate.annotations.Type;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencySupport;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * SuperClass that is extended by all entities that perform a function of either
 * receiving or sending out sets of Costs of some type to or from a client.
 *
 * @author richard
 */
@MappedSuperclass
public abstract class GenericPricingEntity<OrgLevel extends OrganisationLevel> extends Allocated<OrgLevel> implements MultiCurrencySupport {
	private String clientref;
	private Contact createdBy;
	// define multi-currency support here
    private SupportedCurrency currency;
    private Integer duration;
    /**
     * Hibernate is not great at calculating date difference, so it's just
     * easier to store an expiry date as well as a duration.
     */
    private LocalDate expiryDate;
    private int id;
    private Contact issueby;
    private boolean issued;
    private LocalDate issuedate;
    private BigDecimal rate;
    private LocalDate regdate;
    private LocalDate reqdate;

    @Column(name = "clientref", length = 100)
    public String getClientref() {
        return this.clientref;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createdby", nullable = false)
	public Contact getCreatedBy() {
		return this.createdBy;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currencyid", nullable = false)
	public SupportedCurrency getCurrency()
	{
		return this.currency;
	}

	@NotNull
	@Min(1)
	@Column(name = "duration", nullable = false)
	public Integer getDuration() {
		return this.duration;
	}

	@Column(name = "expirydate", columnDefinition = "date")
	public LocalDate getExpiryDate() {
		return this.expiryDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
    public int getId() {
        return this.id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "issueby")
    public Contact getIssueby() {
        return this.issueby;
    }

    @Column(name = "issuedate", columnDefinition = "date")
    public LocalDate getIssuedate() {
        return this.issuedate;
    }

    @NotNull
    @Column(name = "rate", nullable = false, precision = 10, scale = 2)
    public BigDecimal getRate() {
        return this.rate;
    }

    @NotNull
    @Column(name = "regdate", nullable = false, columnDefinition = "date")
    public LocalDate getRegdate() {
        return this.regdate;
	}

	@Column(name = "reqdate", columnDefinition = "date")
	public LocalDate getReqdate() {
		return this.reqdate;
	}

	@Column(name = "issued", nullable = false, columnDefinition = "bit")
	public boolean isIssued() {
		return this.issued;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}

	public void setClientref(String clientref)
	{
		this.clientref = clientref;
	}

	public void setCreatedBy(Contact createdBy)
	{
		this.createdBy = createdBy;
	}

	public void setCurrency(SupportedCurrency currency) {
		this.currency = currency;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	/**
	 * Convenience transient method that returns true if the {@link Quotation}
	 * has been issued and has not expired, false otherwise.
	 */
	@Transient
	public boolean isValid() {
		if (this.expiryDate != null) {
			return !this.expiryDate.isBefore(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		} else {
			return false;
		}
	}

	public void setId(int id) {
		this.id = id;
    }

    public void setIssueby(Contact issueby) {
        this.issueby = issueby;
    }

    public void setIssued(boolean issued) {
        this.issued = issued;
    }

    public void setIssuedate(LocalDate issuedate) {
        this.issuedate = issuedate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public void setRegdate(LocalDate regdate) {
        this.regdate = regdate;
    }

    public void setReqdate(LocalDate reqdate) {
        this.reqdate = reqdate;
	}
}
