package org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

@Entity
@Table(name = "invoicejoblink")
public class InvoiceJobLink extends Auditable
{
	private int id;
	private Invoice invoice;
	private Job job;
	private String jobno;
	private boolean systemJob;

	public InvoiceJobLink()
	{
	}

	public InvoiceJobLink(InvoiceJobLink jl)
	{
		this.id = jl.id;
		this.invoice = jl.invoice;
		this.job = jl.job;
		this.jobno = jl.jobno;
		this.systemJob = jl.systemJob;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "invoiceid", nullable = false)
	public Invoice getInvoice()
	{
		return this.invoice;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", nullable = true)
	public Job getJob()
	{
		return this.job;
	}

	@Length(max = 30)
	@Column(name = "jobno", nullable=true, length = 30)
	public String getJobno()
	{
		return this.jobno;
	}

	@NotNull
	@Column(name = "systemjob", nullable = false, columnDefinition="tinyint")
	public boolean isSystemJob()
	{
		return this.systemJob;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInvoice(Invoice invoice)
	{
		this.invoice = invoice;
	}

	public void setJob(Job job)
	{
		this.job = job;
	}

	public void setJobno(String jobno)
	{
		this.jobno = jobno;
	}

	public void setSystemJob(boolean systemJob)
	{
		this.systemJob = systemJob;
	}
}
