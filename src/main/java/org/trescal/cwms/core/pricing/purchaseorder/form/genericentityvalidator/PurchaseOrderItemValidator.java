package org.trescal.cwms.core.pricing.purchaseorder.form.genericentityvalidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.validation.AbstractEntityValidator;
import org.trescal.cwms.core.validation.BeanValidator;

@Component("PurchaseOrderItemGEValidator")
public class PurchaseOrderItemValidator extends AbstractEntityValidator
{
	@Autowired
	private BeanValidator beanValidator;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(PurchaseOrderItem.class);
	}
	
	@Override
	public void validate(Object target, org.springframework.validation.Errors errors)
	{
		PurchaseOrderItem item = (PurchaseOrderItem) target;
		if (errors == null)	errors = new org.springframework.validation.BindException(item, "item");
		beanValidator.validate(item, errors);
		// description can only be null or blank if the item is explicitly
		// linked to a jobitem
		if (((item.getLinkedJobItems() == null) && ((item.getDescription() == null) || "".equals(item.getDescription()))))
		{
			errors.rejectValue(this.getFullPropertyPath("description"), "error.purchaseorderitem.description.notempty", null, "Purchase order item description cannot be blank");
		}
		this.errors = (BindException) errors;
	}
}