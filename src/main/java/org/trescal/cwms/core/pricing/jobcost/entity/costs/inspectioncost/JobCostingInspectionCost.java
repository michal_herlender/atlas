package org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.InspectionCost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Entity
@DiscriminatorValue("jobcosting")
public class JobCostingInspectionCost extends InspectionCost implements WithJobCostingItem
{
	private JobCostingItem jobCostingItem;

	@OneToOne(mappedBy = "inspectionCost")
	public JobCostingItem getJobCostingItem()
	{
		return this.jobCostingItem;
	}

	public void setJobCostingItem(JobCostingItem jobCostingItem)
	{
		this.jobCostingItem = jobCostingItem;
	}
	
	@Transient
	public CostSource getCostSrc() {
		return CostSource.MANUAL;
	}
}
