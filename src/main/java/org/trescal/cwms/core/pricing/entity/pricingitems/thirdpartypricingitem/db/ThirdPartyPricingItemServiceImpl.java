package org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.db;

import java.math.BigDecimal;
import java.util.List;

import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem;
import org.trescal.cwms.core.system.entity.markups.markuprange.db.MarkupRangeService;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;
import org.trescal.cwms.core.tools.MathTools;

public class ThirdPartyPricingItemServiceImpl implements ThirdPartyPricingItemService
{
	MarkupRangeService markupRangeServ;
	ThirdPartyPricingItemDao thirdPartyPricingItemDao;

	public void deleteThirdPartyPricingItem(ThirdPartyPricingItem thirdpartypricingitem)
	{
		this.thirdPartyPricingItemDao.remove(thirdpartypricingitem);
	}

	public ThirdPartyPricingItem findThirdPartyPricingItem(int id)
	{
		return this.thirdPartyPricingItemDao.find(id);
	}

	public List<ThirdPartyPricingItem> getAllThirdPartyPricingItems()
	{
		return this.thirdPartyPricingItemDao.findAll();
	}

	public void insertThirdPartyPricingItem(ThirdPartyPricingItem ThirdPartyPricingItem)
	{
		this.thirdPartyPricingItemDao.persist(ThirdPartyPricingItem);
	}

	public void saveOrUpdateThirdPartyPricingItem(ThirdPartyPricingItem thirdpartypricingitem)
	{
		this.thirdPartyPricingItemDao.saveOrUpdate(thirdpartypricingitem);
	}

	public void setMarkupRangeServ(MarkupRangeService markupRangeServ)
	{
		this.markupRangeServ = markupRangeServ;
	}

	@Override
	public ThirdPartyPricingItem setThirdPartyDefaultValues(ThirdPartyPricingItem item)
	{
		item.setTpCarriageIn(new BigDecimal("0.00"));
		item.setTpCarriageInMarkupValue(new BigDecimal("0.00"));
		item.setTpCarriageOut(new BigDecimal("0.00"));
		item.setTpCarriageOutMarkupValue(new BigDecimal("0.00"));
		item.setTpCarriageTotal(new BigDecimal("0.00"));
		item.setTpCarriageMarkupRate(new BigDecimal("0.00"));
		item.setTpCarriageMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
		return item;
	}

	public void setThirdPartyPricingItemDao(ThirdPartyPricingItemDao thirdPartyPricingItemDao)
	{
		this.thirdPartyPricingItemDao = thirdPartyPricingItemDao;
	}

	@Override
	public ThirdPartyPricingItem updateThirdPartyCarriageCosts(ThirdPartyPricingItem item)
	{
		// calculate the markup for carriage in and carriage out
		if (item.getTpCarriageMarkupSrc() == ThirdCostMarkupSource.SYSTEM_DEFAULT)
		{
			// calculate the markup rate to use for this cost
			// find the markup rate

			BigDecimal tpCarInMarkup = this.markupRangeServ.calculateMarkUpRangeValue(item.getTpCarriageIn(), MarkupType.CARRIAGE).getValue();
			// if marked up price is less than the original price (and this can
			// happen due to crazy business rules for carriage markups) then
			// assume this markup should be ADDED to the original, rather than
			// replacing it
			if ((item.getTpCarriageIn() != null)
					&& (item.getTpCarriageIn().compareTo(tpCarInMarkup) > 0))
			{
				item.setTpCarriageInMarkupValue(item.getTpCarriageIn().add(tpCarInMarkup));
			}
			else
			{
				item.setTpCarriageInMarkupValue(tpCarInMarkup);
			}

			BigDecimal tpCarOutMarkup = this.markupRangeServ.calculateMarkUpRangeValue(item.getTpCarriageOut(), MarkupType.CARRIAGE).getValue();
			// see comment on tp carriage in above...
			if ((item.getTpCarriageOut() != null)
					&& (item.getTpCarriageOut().compareTo(tpCarOutMarkup) > 0))
			{
				item.setTpCarriageOutMarkupValue(item.getTpCarriageOut().add(tpCarOutMarkup));
			}
			else
			{
				item.setTpCarriageOutMarkupValue(tpCarOutMarkup);
			}
		}
		else
		{
			item.setTpCarriageInMarkupValue(((item.getTpCarriageMarkupRate().divide(new BigDecimal(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN).multiply(item.getTpCarriageIn()))).add(item.getTpCarriageIn(), MathTools.mc).setScale(MathTools.SCALE_FIN));
			item.setTpCarriageOutMarkupValue(((item.getTpCarriageMarkupRate().divide(new BigDecimal(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN).multiply(item.getTpCarriageOut()))).add(item.getTpCarriageOut(), MathTools.mc).setScale(MathTools.SCALE_FIN));
		}

		// update the totals
		item.setTpCarriageTotal(item.getTpCarriageInMarkupValue().add(item.getTpCarriageOutMarkupValue()).setScale(MathTools.SCALE_FIN));
		return item;
	}

	public void updateThirdPartyPricingItem(ThirdPartyPricingItem ThirdPartyPricingItem)
	{
		this.thirdPartyPricingItemDao.update(ThirdPartyPricingItem);
	}
}