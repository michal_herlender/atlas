package org.trescal.cwms.core.pricing.purchaseorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.businesscompanysettings.BusinessCompanySettings;
import org.trescal.cwms.core.company.entity.businesscompanysettings.InterbranchSalesMode;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.purchaseorder.dto.AjaxPurchaseOrderForm;
import org.trescal.cwms.core.pricing.purchaseorder.dto.POAddRequestDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.POAddResponseDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ThirdPartyDiscount;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.util.Locale;

@RestController
@RequestMapping(path = "/purchaseorderapi.json")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class PurchaseOrderAPIController {

    @Autowired
    private PurchaseOrderService purchaseOrderService;
    @Autowired
    private UserService userService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private ThirdPartyDiscount thirdPartyDiscount;

    @PostMapping
    public ResponseEntity<?> createPO(@RequestBody POAddRequestDTO requestDTO,
                                     @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                     @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto){

        AjaxPurchaseOrderForm form = new AjaxPurchaseOrderForm();
        form.setJobid(requestDTO.getJobid());
        form.setPersonid(requestDTO.getPersonid());
        form.setAddressid(requestDTO.getAddressid());
        form.setReturnToAddressid(requestDTO.getReturnToAddressid());
        form.setJobitemids(requestDTO.getJobitemids());
        form.setDescs(requestDTO.getDescs());
        form.setPrices(requestDTO.getPrices());
        form.setDiscounts(requestDTO.getDiscounts());
        form.setNominalids(requestDTO.getNominalids());
        form.setCosttypeids(requestDTO.getCosttypeids());
        form.setQtys(requestDTO.getQtys());
        form.setCurrencyCode(requestDTO.getCurrencyCode());
        form.setDeliverydate(requestDTO.getDeliverydate());
        form.setSupplierref(requestDTO.getSupplierref());

        ResultWrapper result = this.purchaseOrderService.insertAjaxPurchaseOrder(form, userService.get(username), subdivService.get(subdivDto.getKey()));
        if(result.isSuccess()){
            PurchaseOrder order = (PurchaseOrder) result.getResults();
            POAddResponseDTO dto = new POAddResponseDTO(order,jobItemService.findJobItem(requestDTO.getJobitemid()));
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(result.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(path = "/updatedesc/{jobItemId}")
    public String updatePOItemDesc(@PathVariable("jobItemId") Integer jobItemId){
        Locale locale = LocaleContextHolder.getLocale();
        if(jobItemId != null) {
            String shortName = translationService.getCorrectTranslation(this.jobItemService.findJobItem(jobItemId).getCalType().getServiceType().getShortnameTranslation(), locale);
            String longName = translationService.getCorrectTranslation(this.jobItemService.findJobItem(jobItemId).getCalType().getServiceType().getLongnameTranslation(), locale);
            return shortName + " - " + longName;
        }else{
            return null;
        }
    }
    
    /**
     * Returns the discount needed for purchase order creation, by default no discount 
     */
    @RequestMapping(path="/sourcediscount/{personid}")
    public String sourceDiscount(@PathVariable("personid") Integer personid,
    		@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
    	String result = "0.00";
    	if (personid != null) {
        	Contact contact = this.contactService.get(personid);
        	if (contact != null) {
        		if (contact.getSub().getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
        			// Obtain discount from intercompany rate on business settings
        			BusinessCompanySettings settings = contact.getSub().getComp().getBusinessSettings();
        			if ((settings != null) &&
        				InterbranchSalesMode.SALES_CATALOG.equals(settings.getInterbranchSalesMode()) &&
        				settings.getInterbranchDiscountRate() != null) {
        				BigDecimal discount = settings.getInterbranchDiscountRate();
        				result = NumberTools.displayBigDecimalTo2DecimalPlaces(discount);
        			}
        		}
        		else if (contact.getSub().getComp().getCompanyRole().equals(CompanyRole.SUPPLIER)) {
        			// Obtain discount from ThirdPartyDiscount system default
        			Company allocatedCompany = this.subdivService.get(subdivDto.getKey()).getComp();
        			KeyValue<Scope, String> value = thirdPartyDiscount.getValueHierarchical(Scope.CONTACT, contact, allocatedCompany);
        			result = value.getValue();
        		}
        	}
    	}
    	return result;
    }
    /**
     * Returns the nomianl id, default is PO009, intercompany is PO010
     * (see assembla ticket #1221 for details)
     */
    @RequestMapping(path="/sourcenominal/{personid}")
    public Integer sourceNominal(@PathVariable("personid") Integer personid,
    		@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
    	Integer result = 87;
    	if (personid != null) {
        	Contact contact = this.contactService.get(personid);
        	if (contact != null) {
        		if (contact.getSub().getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
        			result = 88;
        		}
        	}
    	}
    	return result;
    }
}
