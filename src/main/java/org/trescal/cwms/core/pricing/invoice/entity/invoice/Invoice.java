package org.trescal.cwms.core.pricing.invoice.entity.invoice;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNoteComparator;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.TaxablePricing;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.InvoiceDelivery;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItemComparator;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLinkComparator;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetax.InvoiceTax;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.system.entity.baseaction.ActionComparator;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name = "invoice")
// overrides the column definition for contact inherited from Pricing - allows
// fk to be nullable (invoices don't have a contact)
@AssociationOverride(name = "contact", joinColumns = @JoinColumn(name = "personid"))
@Setter
public class Invoice extends TaxablePricing<Company, InvoiceItem, InvoiceTax> implements ComponentEntity, NoteAwareEntity {

	private AccountStatus accountsStatus;
	private Set<InvoiceAction> actions;
	private Address address;
	private Company comp;
	private Set<CreditNote> creditNotes;
	private Set<InvoiceDelivery> deliveries;
	private File directory;
	private int financialPeriod;
	private String invno;
	private LocalDate invoiceDate;
	// child entities
	private Set<InvoiceItem> items;
	private Set<InvoiceJobLink> jobLinks;
	private Set<InvoiceNote> notes;
	private Set<InvoicePO> pos;
	private PricingType pricingType;
	private List<Email> sentEmails;
	private String siteCostingNote;
	private InvoiceStatus status;
	private InvoiceType type;
	private PaymentMode paymentMode;
	private VatRate vatRateEntity;
	private Contact businessContact;
	private List<PurchaseOrderItem> internalPOItems;
	private LocalDate duedate;
	private PaymentTerm paymentTerm;
	private List<JobItemNotInvoiced> periodicJobItemLinks;

	@Enumerated(EnumType.STRING)
	@Column(name = "accountstatus", length = 1)
	public AccountStatus getAccountsStatus() {
		return this.accountsStatus;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "paymentterms", length = 10)
	public PaymentTerm getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(PaymentTerm paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "invoice")
	@SortComparator(ActionComparator.class)
	public Set<InvoiceAction> getActions() {
		return this.actions;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addressid", nullable = false)
	public Address getAddress() {
		return this.address;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", nullable = false)
	public Company getComp() {
		return this.comp;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "invoice")
	@SortComparator(CreditNoteComparator.class)
	public Set<CreditNote> getCreditNotes() {
		return this.creditNotes;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		// if there is a at least one job linked to the invoice
		return this.getJobLinks().stream().findFirst().map(InvoiceJobLink::getJob).map(Job::getCon).orElseGet(() ->
		// I know that this nesting looks wried but JDK8 does not have the "or" operator
		// for the Optionals
		Optional.ofNullable(this.comp).map(Company::getDefaultSubdiv).map(Subdiv::getDefaultContact)
				.orElse(this.contact));
	}

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "invoice")
	public Set<InvoiceDelivery> getDeliveries() {
		return this.deliveries;
	}

	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Column(name = "period", nullable = false)
	public int getFinancialPeriod() {
		return this.financialPeriod;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return this.invno;
	}

	@NotNull
	@Length(max = 30)
	@Column(name = "invno", length = 30, nullable = false)
	public String getInvno() {
		return this.invno;
	}

	@NotNull
	@Column(name = "invoicedate", nullable = false, columnDefinition = "date")
	public LocalDate getInvoiceDate() {
		return this.invoiceDate;
	}

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "invoice", orphanRemoval = true)
	@SortComparator(InvoiceItemComparator.class)
	public Set<InvoiceItem> getItems() {
		return this.items;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "invoice")
	@SortComparator(InvoiceJobLinkComparator.class)
	public Set<InvoiceJobLink> getJobLinks() {

		return this.jobLinks != null ? this.jobLinks : new TreeSet<>(new InvoiceJobLinkComparator());
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "invoice")
	@SortComparator(NoteComparator.class)
	public Set<InvoiceNote> getNotes() {
		return this.notes;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "invoice")
	public Set<InvoicePO> getPos() {
		return this.pos;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "pricingtype", nullable = false)
	public PricingType getPricingType() {
		return this.pricingType;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@Length(max = 1000)
	@Column(name = "sitecostingnote", length = 1000)
	public String getSiteCostingNote() {
		return this.siteCostingNote;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = false)
	public InvoiceStatus getStatus() {
		return this.status;
	}

	// @NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "typeid")
	public InvoiceType getType() {
		return this.type;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return true;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "paymentmodeid")
	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	/*
	 * Required for brokering of invoice to accounting interface and deduction of
	 * general business posting group.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vatcode", columnDefinition = "varchar(1)")
	public VatRate getVatRateEntity() {
		return vatRateEntity;
	}

	/*
	 * Tracks the Trescal business contact that should be listed on the invoice May
	 * differ from the createdBy contact Not using the contact field in Pricing as
	 * convention is (when set) that it is the client contact.
	 */
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "businesscontactid", nullable = false)
	public Contact getBusinessContact() {
		return this.businessContact;
	}

	@OneToMany(mappedBy = "internalInvoice", fetch = FetchType.LAZY)
	public List<PurchaseOrderItem> getInternalPOItems() {
		return internalPOItems;
	}

	@OneToMany(mappedBy = "periodicInvoice", fetch = FetchType.LAZY)
	public List<JobItemNotInvoiced> getPeriodicJobItemLinks() {
		return periodicJobItemLinks;
	}

	@Column(name = "duedate", nullable=true, columnDefinition="date")
	public LocalDate getDuedate() {
		return duedate;
	}

	@Transient
	public boolean isCustomerInvoice() {
		return this.type.getName().equals("Calibration");
	}

	@Transient
	public boolean isInternalInvoice() {
		return this.type.getName().equals("Internal");
	}
}