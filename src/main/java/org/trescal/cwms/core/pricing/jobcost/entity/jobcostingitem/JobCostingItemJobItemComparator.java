package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem;

import java.util.Comparator;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

/**
 * Sorts {@link JobCostingItem}s by their most recent {@link JobCosting}. Only
 * intended for use for a single {@link JobItem} to sort it's
 * {@link JobCostingItem}s.
 * 
 * @author Richard
 */
public class JobCostingItemJobItemComparator implements Comparator<JobCostingItem>
{
	@Override
	public int compare(JobCostingItem o1, JobCostingItem o2)
	{
		return ((Integer) o2.getJobCosting().getId()).compareTo(o1.getJobCosting().getId());
	}
}
