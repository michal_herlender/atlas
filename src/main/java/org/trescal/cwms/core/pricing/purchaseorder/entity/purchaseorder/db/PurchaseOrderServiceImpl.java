package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.company.dto.ClientCompanyDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.poorigin.*;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.purchaseorder.dto.*;
import org.trescal.cwms.core.pricing.purchaseorder.entity.enums.PurchaseOrderTaxableOption;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.PurchaseOrderAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.db.PurchaseOrderActionService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemComparator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemDao;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemnote.PurchaseOrderItemNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItemComparator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote.PurchaseOrderNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote.PurchaseOrderNote_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db.PurchaseOrderStatusService;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderHomeForm;
import org.trescal.cwms.core.pricing.purchaseorder.form.genericentityvalidator.PurchaseOrderItemValidator;
import org.trescal.cwms.core.pricing.purchaseorder.form.genericentityvalidator.PurchaseOrderValidator;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoiceComparator;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice_;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.baseaction.ActionComparator;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.basestatus.db.BaseStatusService;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.deletedcomponent.db.DeletedComponentService;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.*;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_RaisePurchaseOrder;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service("PurchaseOrderService")
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

	@Autowired
	private AddressService addServ;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private CostTypeService costTypeServ;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private DeletedComponentService delCompServ;
	@Autowired
	private HookInterceptor_RaisePurchaseOrder interceptor_raisePurchaseOrder;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private NominalCodeService nominalCodeServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private PurchaseOrderItemService poItemServ;
	@Autowired
	private PurchaseOrderItemDao poItemDao;
	@Autowired
	private PurchaseOrderActionService purchaseOrderActionServ;
	@Autowired
	private PurchaseOrderDao purchaseOrderDao;
	@Autowired
	private PurchaseOrderItemValidator purchaseOrderItemValidator;
	@Autowired
	private PurchaseOrderStatusService purchaseOrderStatusServ;
	@Autowired
	private PurchaseOrderValidator purchaseOrderValidator;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private UserService userService;
	@Autowired
	private BaseStatusService statusServ;
	@Autowired
	private BeanValidator validator;
	@Autowired
	private NoteService noteService;
	@Autowired
	private PurchaseOrderItemService poItemService;
	@Autowired
	private LimitCompanyService permissionLimitService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private CompanyListService companyListService;
	@Autowired
	private BPOService bPOService;

	@Override
	public PurchaseOrder createPurchaseOrderFromTPQuotation(TPQuotation tpQuotation, Map<Integer, Boolean> selectionMap,
			Map<Integer, Integer> nominalMap, Contact currentContact, Locale locale, Locale defaultLocale,
			Subdiv allocatedSubdiv) {
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setBusinessContact(tpQuotation.getDefaultContact());
		/* Clone Pricing properties */
		purchaseOrder.setContact(tpQuotation.getContact());
		purchaseOrder.setCreatedBy(currentContact);
		purchaseOrder.setCurrency(tpQuotation.getCurrency());
		purchaseOrder.setDuration(tpQuotation.getDuration());
		purchaseOrder.setRate(tpQuotation.getRate());
		purchaseOrder.setVatRate(tpQuotation.getVatRate());
		purchaseOrder.setOrganisation(tpQuotation.getOrganisation());
		if (tpQuotation.getFromRequest() != null && tpQuotation.getFromRequest().getRequestFromJob() != null)
			purchaseOrder.setJob(tpQuotation.getFromRequest().getRequestFromJob());
		purchaseOrder.setTaxable(PurchaseOrderTaxableOption.NOT_APPLICABLE);
		/* Set other properties */
		purchaseOrder.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		purchaseOrder.setAddress(tpQuotation.getContact().getDefAddress());
		purchaseOrder.setClientref(tpQuotation.getTpqno());
		String poNumber = numerationService.generateNumber(NumerationType.PURCHASE_ORDER, tpQuotation.getOrganisation(),
			null);
		purchaseOrder.setPono(poNumber);
		purchaseOrder.setPoOrigin(new POOriginTPQuote(tpQuotation));
		purchaseOrder.setReturnToAddress(currentContact.getDefAddress());
		purchaseOrder.setStatus((PurchaseOrderStatus) this.statusServ.findDefaultStatus(PurchaseOrderStatus.class));
		/* Generate Purchase Order Items */
		purchaseOrder.setItems(new TreeSet<>(new PurchaseOrderItemComparator()));
		int itemCounter = 0;
		for (TPQuotationItem tpQuotationItem : tpQuotation.getItems()) {
			if (selectionMap.get(tpQuotationItem.getId())) {
				BigDecimal generalDiscountFactor = BigDecimal.valueOf(1)
					.subtract(tpQuotationItem.getGeneralDiscountRate().divide(BigDecimal.valueOf(100), BigDecimal.ROUND_HALF_UP));
				for (Cost cost : tpQuotationItem.getCosts()) {
					if (cost.getFinalCost().compareTo(new BigDecimal(0)) > 0) {
						PurchaseOrderItem poItem = new PurchaseOrderItem();
						poItem.setNominal(nominalCodeServ.get(nominalMap.get(tpQuotationItem.getId())));
						poItem.setTotalCost(cost.getTotalCost());
						BigDecimal itemDiscountFactor = BigDecimal.valueOf(1)
							.subtract(cost.getDiscountRate().divide(BigDecimal.valueOf(100), BigDecimal.ROUND_HALF_UP));
						BigDecimal discountRate = BigDecimal.valueOf(1)
							.subtract(itemDiscountFactor.multiply(generalDiscountFactor))
							.multiply(BigDecimal.valueOf(100));
						poItem.setGeneralDiscountRate(discountRate.setScale(2, RoundingMode.HALF_UP));
						BigDecimal discountValue = discountRate.multiply(cost.getTotalCost())
							.divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP);
						poItem.setFinalCost(cost.getTotalCost().subtract(discountValue));
						poItem.setGeneralDiscountValue(discountValue.setScale(2, RoundingMode.HALF_UP));
						poItem.setCostType(cost.getCostType());
						poItem.setQuantity(tpQuotationItem.getQuantity());
						poItem.setDescription(InstModelTools.modelNameViaTranslations(tpQuotationItem.getModel(),
							locale, defaultLocale));
						poItem.setItemno(++itemCounter);
						poItem.setReceiptStatus(PurchaseOrderItemReceiptStatus.NOT_RECEIVED);
						poItem.setOrder(purchaseOrder);
						poItem.setLinkedJobItems(tpQuotationItem.getLinkedTo().stream()
							.map(link -> new PurchaseOrderJobItem(link.getJobItem(), poItem))
							.collect(Collectors.toCollection(() -> new TreeSet<>(
								new PurchaseOrderJobItemComparator()))));
						poItem.setNotes(tpQuotationItem.getNotes().stream().filter(Note::isActive)
							.map(tpqNote -> new PurchaseOrderItemNote(poItem, tpqNote)).collect(Collectors
								.toCollection(() -> new TreeSet<>(new NoteComparator()))));
						if (!poItem.getLinkedJobItems().isEmpty()) {
							Subdiv jobSubdiv = poItem.getLinkedJobItems().first().getJi().getJob().getOrganisation();
							// Use the job's subdiv only if the job belongs to
							// same company as the PO
							if (jobSubdiv.getComp().getCoid() == purchaseOrder.getOrganisation().getCoid()) {
								poItem.setBusinessSubdiv(jobSubdiv);
							}
						}
						purchaseOrder.getItems().add(poItem);
					}
				}
			}
		}
		purchaseOrder.setFinalCost(purchaseOrder.getItems().stream().reduce(BigDecimal.ZERO,
				(bd, poi) -> bd.add(poi.getFinalCost()), BigDecimal::add));
		purchaseOrder.setTotalCost(purchaseOrder.getItems().stream().reduce(BigDecimal.ZERO,
                (bd, poi) -> bd.add(poi.getTotalCost()), BigDecimal::add));
        purchaseOrder.setNotes(noteService.initalizeNotes(purchaseOrder, PurchaseOrderNote.class, locale));
        this.insertPurchaseOrder(purchaseOrder);
		return purchaseOrder;
	}

	@Override
	public ResultWrapper deleteOrder(int orderid, String reason, HttpSession session) {
		// validate invoice exists
		PurchaseOrder order = this.find(orderid);
		if (order == null) {
			return new ResultWrapper(false, this.messages.getMessage("error.purchaseorder.notfound", null,
					"Purchase Order could not be found", null));
		} else {
			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact currentContact = this.userService.getEagerLoad(username).getCon();

			// create deletion object
			DeletedComponent dinv = new DeletedComponent();
			dinv.setComponent(Component.PURCHASEORDER);
			dinv.setContact(currentContact);
			dinv.setDate(new Date());
			dinv.setRefNo(order.getPono());
			dinv.setReason(reason);

			// validate deletion object
			BindException errors = new BindException(dinv, "dinv");
			this.validator.validate(dinv, errors);

			// if validation passes then delete the order
			if (!errors.hasErrors()) {
				// delete the order
				this.deletePurchaseOrder(order);

				// persist the deletedorder
				this.delCompServ.insertDeletedComponent(dinv);
			}

			// set deletedorder into the results
			return new ResultWrapper(errors, dinv);
		}
	}

	/*
	 * Only called internally by deleteOrder, exposed for testing
	 */
	@Override
	public void deletePurchaseOrder(PurchaseOrder purchaseorder) {
		if ((purchaseorder.getDirectory() != null) && purchaseorder.getDirectory().exists()) {
			FileTools.deleteFiles(purchaseorder.getDirectory());
		}

		this.purchaseOrderDao.remove(purchaseorder);
	}

	@Override
	public List<PurchaseOrderDTO> findAllByJob(Job job) {
		return purchaseOrderDao.findAllByJob(job);
	}

	@Override
	public List<PurchaseOrderDTO> findMostRecent(Company allocatedCompany, Integer maxResults) {
		return purchaseOrderDao.findMostRecent(allocatedCompany, maxResults);
	}

	@Override
	public List<PurchaseOrder> findAllPOsRequiringAttention() {
		return this.purchaseOrderDao.findAllPOsRequiringAttention();
	}

	@Override
	public PurchaseOrder find(int id) {
		PurchaseOrder order = this.purchaseOrderDao.find(id);
		if (order != null && order.getDirectory() == null) {
			order.setDirectory(
					this.compDirServ.getDirectory(this.scServ.findComponent(Component.PURCHASEORDER), order.getPono()));
		}
		return order;
	}

	@Override
	public List<PurchaseOrder> findByJob(Job job) {
		return this.purchaseOrderDao.findAll(job);
	}

	@Override
	public List<PurchaseOrder> getAll() {
		return this.purchaseOrderDao.findAll();
	}

	@Override
	public List<PurchaseOrder> getAllPurchaseOrdersAtStatus(PurchaseOrderStatus status, Company allocatedCompany) {
		return this.purchaseOrderDao.getAllPurchaseOrdersAtStatus(status, allocatedCompany);
	}

	@Override
	public List<PurchaseOrder> getByAccountStatus(Company businessCompany, AccountStatus accStatus) {
		return this.purchaseOrderDao.getByAccountStatus(businessCompany, accStatus);
	}

	public PurchaseOrder getPOByExactPONo(String poNo) {
		return this.purchaseOrderDao.getPOByExactPONo(poNo);
	}

	/**
	 * @deprecated bad performance, wrong place PO != PurchaseOrder
	 */
	public Map<Integer, List<InvoicedJobPONumberDTO>> getPONumbersFromInvoicedJobs(Invoice i) {
		// create new list of invoiced job po number dtos
		Map<Integer, List<InvoicedJobPONumberDTO>> invJobPoNums = new HashMap<>();
		// check each job on invoice
		for (InvoiceJobLink jl : i.getJobLinks()) {
			if (jl.getJob() != null) {
				// create new invoiced job po number dto
				InvoicedJobPONumberDTO invjobpono = new InvoicedJobPONumberDTO();
				invjobpono.setJob(jl.getJob());
				// check bpo null?
				if (jl.getJob().getBpoLinks() != null && !jl.getJob().getBpoLinks().isEmpty()) {
					invjobpono.setBpos(new ArrayList<>());
					jl.getJob().getBpoLinks().forEach(link -> {
						if (!i.getStatus().isOnIssue() && i.getStatus().getNext() != null || i.getIssuedate().compareTo(DateTools.dateToLocalDate(link.getCreatedOn())) > 0)
							invjobpono.getBpos().add(link.getBpo());
					});
				}
				// job has purchase order numbers?
				if (jl.getJob().getPOs() != null) {
					if (jl.getJob().getPOs().size() > 0) {
						invjobpono.setPos(jl.getJob().getPOs());
					}
				}
				// add this dto to list
				if (!invJobPoNums.containsKey(jl.getId())) {
					invJobPoNums.put(jl.getId(), new ArrayList<>());
				}

				invJobPoNums.get(jl.getId()).add(invjobpono);
			}
		}
		return invJobPoNums;
	}

	@Override
	public ResultWrapper insertAjaxPurchaseOrder(AjaxPurchaseOrderForm form, User user, Subdiv subdiv) {
		// do some validation on the given fields
		// validate job
		Job job = form.getJobid() == null ? null : this.jobServ.get(form.getJobid());
		if ((job == null) || (form.getJobid() == null)) {
			// reject no job
			return new ResultWrapper(false,
					this.messages.getMessage("error.purchaseorder.create.job", null, "A job must be specified", null));
		} else {
			Address address = form.getAddressid() == null ? null : this.addServ.get(form.getAddressid());
			if ((address == null) || (form.getAddressid() == null)) {
				// reject no address
				return new ResultWrapper(false, this.messages.getMessage("error.purchaseorder.create.address", null,
						"An address must be specified", null));
			} else {
				Contact con = form.getPersonid() == null ? null : this.conServ.findEagerContact(form.getPersonid());
				if ((con == null) || (form.getPersonid() == null)) {
					// reject no contact
					return new ResultWrapper(false, this.messages.getMessage("error.purchaseorder.create.contact", null,
							"A contact must be specified", null));
				} else {
					// check if the supplier isn’t in the list of 'validated' customers
					Company clientCompany = job.getCon().getSub().getComp(); 
					Company supplierCompany = conServ.get(form.getPersonid()).getSub().getComp();
					CompanySettingsForAllocatedCompany settings = companySettingsService.getByCompany(clientCompany, subdiv.getComp());
					if(settings != null && settings.getRequireSupplierCompanyList() && !companyListService
						.supplierInCompanyListUsedClient(clientCompany.getCoid(), supplierCompany.getCoid(), subdiv.getComp())){
						return new ResultWrapper(false,
								this.messages.getMessage("error.purchaseorder.supplier.outside", null,
										"The supplier isn’t in the list of validated customers", null));
					}
					// don't allow a null delivery date
					else if ((form.getDeliverydate() == null) || (form.getDeliverydate().trim().equals(""))) {
						return new ResultWrapper(false,
								this.messages.getMessage("error.purchaseorderitem.deliverydate.notempty", null,
										"A delivery date must be specified", null));
					} else {
                        Contact currentContact = user.getCon();
                        PurchaseOrder order = new PurchaseOrder();
                        order.setOrganisation(subdiv.getComp());
                        order.setAddress(address);
                        order.setContact(con);
                        order.setJob(job);
                        order.setPoOrigin(new POOriginJob(job));
                        order.setCreatedBy(currentContact);
                        order.setNotes(noteService.initalizeNotes(order, PurchaseOrderNote.class, LocaleContextHolder.getLocale()));
                        order.setJobno(job.getJobno());
                        order.setDuration(1);
                        order.setClientref(form.getSupplierref());
                        order.setFinalCost(new BigDecimal("0.00"));
                        order.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
                        order.setReturnToAddress(this.addServ.get(form.getReturnToAddressid()));
                        order.setBusinessContact(currentContact);
						order.setTaxable(PurchaseOrderTaxableOption.NOT_APPLICABLE);
                        order.setStatus(
                                (PurchaseOrderStatus) this.statusServ.findDefaultStatus(PurchaseOrderStatus.class));
                        order.setItems(new TreeSet<>(new PurchaseOrderItemComparator()));

						// either set the currency and rate from the company
						// default
						// or from the other selected currency
						if (form.getCurrencyCode().equals(Constants.COMPANY_DEFAULT_CURRENCY)) {
							order.setCurrency(this.currencyServ.findCurrencyByCode(
								order.getContact().getSub().getComp().getCurrency().getCurrencyCode()));
							order.setRate(order.getContact().getSub().getComp().getRate());
						} else {
							order.setCurrency(this.currencyServ.findCurrencyByCode(form.getCurrencyCode()));
							order.setRate(order.getCurrency().getDefaultRate());
						}

						// set a string to pass validation
						order.setPono("om");

						// create a new validator to validate this Mfr before
						// trying the insert
						this.purchaseOrderValidator.validate(order, null);

						if (this.purchaseOrderValidator.getErrors().hasErrors()) {
							return new ResultWrapper(this.purchaseOrderValidator.getErrors(), order);
						} else {
							// holder object to contain any item errors
							BindException errors = new BindException(order, "item");

							// set up the items
							for (int i = 0; i < form.getDescs().size(); i++) {
								PurchaseOrderItem item = new PurchaseOrderItem();
								item.setCostType(form.getCosttypeids().get(i) == null ? null
										: CostType.valueOf(form.getCosttypeids().get(i)));
								item.setDescription(form.getDescs().get(i));
								item.setItemno(i + 1);
								item.setNominal(form.getNominalids().get(i) == null ? null
										: this.nominalCodeServ.get(form.getNominalids().get(i)));
								item.setOrder(order);
								item.setBusinessSubdiv(job.getOrganisation());

								item.setQuantity(NumberTools.isAnInteger(form.getQtys().get(i))
										? Integer.parseInt(form.getQtys().get(i))
										: 1);
								item.setTotalCost(new BigDecimal(
										NumberTools.isADouble(form.getPrices().get(i)) ? form.getPrices().get(i)
												: "0.00")); // set
								item.setGeneralDiscountRate(new BigDecimal(
										NumberTools.isADouble(form.getDiscounts().get(i)) ? form.getDiscounts().get(i)
												: "0.00"));

								this.poItemServ.prepareNewPurchaseOrderItem(item);

								try {
									if (form.getDeliverydate() != null) {
										if (!form.getDeliverydate().trim().isEmpty()) {
											item.setDeliveryDate(DateTools.dateToLocalDate(DateTools.df.parse(form.getDeliverydate())));
										}
									}
								} catch (Exception ignored) {
								}

								// add jobitems
								if (form.getJobitemids().get(i) != null) {
									item.setLinkedJobItems(
										new TreeSet<>(new PurchaseOrderJobItemComparator()));
									PurchaseOrderJobItem ji = new PurchaseOrderJobItem();
									ji.setPoitem(item);
									ji.setJi(this.jiServ.findJobItem(form.getJobitemids().get(i)));
									item.getLinkedJobItems().add(ji);
								}

								// calc totals
								CostCalculator.updateItemCost(item);

								// validate
								this.purchaseOrderItemValidator.validate(item, null);
								errors.addAllErrors(this.purchaseOrderItemValidator.getErrors());

								order.getItems().add(item);
							}

							if (errors.hasErrors()) {
								return new ResultWrapper(errors, null);
							} else {
								// update costs
								CostCalculator.updatePricingFinalCost(order, order.getItems());
								// set the order number, look up the new id if
								// it has not already been set
								String poNumber = numerationService.generateNumber(NumerationType.PURCHASE_ORDER,
										subdiv.getComp(), subdiv);
								order.setPono(poNumber);
								// persist
								order = this.mergePurchaseOrder(order);
								// loop through items firing off hook for each
								if (order.getItems() != null) {
									for (PurchaseOrderItem poi : order.getItems()) {
										Set<PurchaseOrderJobItem> pojis = poi.getLinkedJobItems();
										if (pojis != null) {
											for (PurchaseOrderJobItem poji : pojis) {
												this.linkJobItemToPO(poji);
											}
										}
									}
								}

								// Add any on behalf of details as po item note
								// (with html line separators)
								for (PurchaseOrderItem poItem : Objects.requireNonNull(order.getItems())) {
									StringBuilder noteContent = new StringBuilder();
									for (PurchaseOrderJobItem poJobItem : poItem.getLinkedJobItems()) {
										if (poJobItem.getJi().getOnBehalf() != null) {
											noteContent.append(poJobItem.getJi().getOnBehalf().getCompany().getConame());
											noteContent.append("<br/>");
											// On-behalf of address is an
											// optional field, so may be null
											if (poJobItem.getJi().getOnBehalf().getAddress() != null) {
												noteContent.append(poJobItem.getJi().getOnBehalf().getAddress().getAddr1());
												noteContent.append("<br/>");
												if (!poJobItem.getJi().getOnBehalf().getAddress().getAddr2()
													.isEmpty()) {
													noteContent.append(poJobItem.getJi().getOnBehalf().getAddress()
														.getAddr2());
													noteContent.append("<br/>");
												}
												if (!poJobItem.getJi().getOnBehalf().getAddress().getAddr3()
													.isEmpty()) {
													noteContent.append(poJobItem.getJi().getOnBehalf().getAddress()
														.getAddr3());
													noteContent.append("<br/>");
												}
												if (!poJobItem.getJi().getOnBehalf().getAddress().getCounty()
														.isEmpty()) {
													noteContent.append(poJobItem.getJi().getOnBehalf().getAddress()
														.getCounty());
													noteContent.append("<br/>");
												}
												if (!poJobItem.getJi().getOnBehalf().getAddress().getPostcode()
													.isEmpty()) {
													noteContent.append(poJobItem.getJi().getOnBehalf().getAddress()
														.getPostcode());
													noteContent.append("<br/>");
												}
												noteContent.append(poJobItem.getJi().getOnBehalf().getAddress().getCountry()
													.getLocalizedName());
												noteContent.append("<br/>");
											}
										}
									}
									if (noteContent.length() > 0) {
										noteService.insertNoteType(
											messages.getMessage("docs.onbehalfof", null, "Certificate in name of",
												LocaleContextHolder.getLocale()),
											noteContent.toString(), true, currentContact.getId(),
											NoteType.PURCHASEORDERITEMNOTE, poItem.getId(), false, null);
									}
								}
								return new ResultWrapper(true, "", order, null);
							}
						}
					}
				}
			}
		}
	}

	public void insertPurchaseOrder(PurchaseOrder PurchaseOrder) {
		this.purchaseOrderDao.persist(PurchaseOrder);
	}

	public void linkJobItemToPO(PurchaseOrderJobItem poji) {
		this.purchaseOrderDao.linkJobItemToPO(poji);
	}

	public PurchaseOrder mergePurchaseOrder(PurchaseOrder purchaseorder) {
		return purchaseOrderDao.merge(purchaseorder);
	}

	@Override
	public AjaxPurchaseOrderForm prepareAjaxPurchaseOrderForm(int jobitemid, HttpServletRequest request) {
		// Use the current logged in subdiv for creating the PO (addresses), not
		// the job or globals.
		@SuppressWarnings("unchecked")
		KeyValue<Integer, String> subdivDto = (KeyValue<Integer, String>) request.getSession()
				.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		Locale locale = LocaleContextHolder.getLocale();

		AjaxPurchaseOrderForm form = new AjaxPurchaseOrderForm();
		// validate jobitem
		JobItem ji = this.jiServ.findEagerJobItem(jobitemid);
		StateGroup group = ji.getState().getGroupLinks() != null
				&& ji.getState().getGroupLinks().stream().anyMatch(g -> g.getGroup().equals(StateGroup.AWAITING_TP_PO))
			? StateGroup.AWAITING_TP_PO
			: ji.getState().getGroupLinks().stream().anyMatch(
			g -> g.getGroup().equals(StateGroup.AWAITINGINTERNALPO)) ? StateGroup.AWAITINGINTERNALPO
			: null;
		List<AjaxPurchaseOrderItemDTO> poItems = this.purchaseOrderDao.preparePurchaseOrderItem(ji.getJob().getJobid(),
			group, locale);
		form.setFromJobItem(ji);
		form.setFromJobItems(poItems);

		// load other jobitems
		form.setJobitems(new ArrayList<>(ji.getJob().getItems()));
		form.setContractReviewCosts(jiServ.getJobItemContractReviewCosts(ji));

		if (poItems != null && poItems.size() > 0) {
			SortedMap<AjaxPurchaseOrderItemDTO, List<KeyValue<Integer, String>>> myMap = new TreeMap<>(
				Comparator.comparing(AjaxPurchaseOrderItemDTO::getJobItemNo));
			poItems.forEach(i -> form.getJobitems().stream().filter(jit -> i.getJobItemId().equals(jit.getJobItemId()))
				.findAny().ifPresent(existJi -> myMap.put(i, jiServ.getJobItemContractReviewCosts(existJi))));

			form.setJiContractReviewCosts(myMap);
		}

		// load business addresses
		form.setReturnAddress(this.addServ.getAllActiveSubdivAddresses(allocatedSubdiv, AddressType.POFROM));
		// add all client addresses
		form.getReturnAddress().addAll(
				this.addServ.getAllCompanyAddresses(ji.getJob().getCon().getSub().getComp().getCoid(), null, true));
		// load nominals
		form.setNominalcodes(this.nominalCodeServ
				.getNominalCodes(Arrays.asList(Ledgers.PURCHASE_LEDGER, Ledgers.CAPITAL, Ledgers.UTILITY)));
		// load cost types
		form.setCosttypes(this.costTypeServ.getAllActiveCostTypeDtos());
		// load currencies
		form.setCurrencies(this.currencyServ.getAllSupportedCurrenciesWithExRates());
		// add default currency
		form.setDefaultCurrency(allocatedSubdiv.getComp().getCurrency());
		return form;
	}

	@Override
	public PurchaseOrder prepareAndInsertPurchaseOrder(PurchaseOrder purchaseorder, Contact currentContact,
			Subdiv subdiv) {
		// look up the new id if it has not already been set
		// look up the status
		purchaseorder.setStatus((PurchaseOrderStatus) this.statusServ.findDefaultStatus(PurchaseOrderStatus.class));
		// persist the purchaseorder
		purchaseorder = this.merge(purchaseorder, currentContact);
		// add a new purchaseorderactivity for creating the purchase order
		PurchaseOrderAction action = new PurchaseOrderAction();
		action.setContact(purchaseorder.getCreatedBy());
		action.setDate(new Date());
		action.setOutcomeStatus(purchaseorder.getStatus());
		action.setOrder(purchaseorder);
		action.setActivity(this.purchaseOrderStatusServ.findOnInsertPurchaseOrderStatus(ActionType.ACTION));
		this.purchaseOrderActionServ.saveOrUpdatePurchaseOrderAction(action);
		purchaseorder.setActions(new TreeSet<>(new ActionComparator()));
		purchaseorder.getActions().add(action);
		return purchaseorder;
	}

	@Override
	public ResultWrapper resetAccountStatus(int poid, HttpSession session) {
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact currentContact = this.userService.getEagerLoad(username).getCon();

		PurchaseOrder order = this.find(poid);

		if (order == null) {
			return new ResultWrapper(false,
					this.messages.getMessage("error.order.notfound", null, "Purchase Order could not be found", null));
		} else {
			order.setAccountsStatus(AccountStatus.P);
			order = this.merge(order, currentContact);
			return new ResultWrapper(true, "", order, null);
		}
	}

	@Override
	public PurchaseOrder merge(PurchaseOrder purchaseorder, Contact currentContact) {
		purchaseorder = this.purchaseOrderDao.merge(purchaseorder);
		// Formerly done via AOP, updated to local call 2015-11-18 GB
		this.validateAndCompletePurchaseOrder(purchaseorder, currentContact);
		return purchaseorder;
	}

	@Override
	public PagedResultSet<PurchaseOrder> searchSortedPurchaseOrder(PurchaseOrderHomeForm form,
			PagedResultSet<PurchaseOrder> rs) {
		return this.purchaseOrderDao.searchSortedPurchaseOrder(form, rs);
	}

	@Override
	public void updatePurchaseOrder(PurchaseOrder purchaseOrder, Contact currentContact) {
		purchaseOrder = this.purchaseOrderDao.merge(purchaseOrder);
		// Formerly done via AOP, updated to local call 2015-11-18 GB
		this.validateAndCompletePurchaseOrder(purchaseOrder, currentContact);
	}

	@Override
	public PurchaseOrder updatePurchaseOrderCosts(PurchaseOrder order, Contact currentContact) {
		CostCalculator.updatePricingFinalCost(order, order.getItems());
		return this.merge(order, currentContact);
	}

	@Override
	public boolean changeAccountStatus(String purchaseOrderNumber, Integer busId, AccountStatus accountStatus) {
		try {

			PurchaseOrder purchaseOrder = findByNumber(purchaseOrderNumber, busId);
			purchaseOrder.setAccountsStatus(accountStatus);
			purchaseOrderDao.merge(purchaseOrder);
			return true;
		} catch (Exception e) {

			return false;
		}
	}

	@Override
	public Boolean updatePurchaseOrderStatus(PurchaseOrder order, Contact currentContact, Subdiv currentSubdiv) {
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		// get current status
		PurchaseOrderStatus currentStatus = this.purchaseOrderStatusServ
				.findPurchaseOrderStatus(order.getStatus().getStatusid());

		// get next activity and next status
		PurchaseOrderStatus nextActivity = currentStatus.getNext();
		PurchaseOrderStatus nextStatus = nextActivity.getNext();

		if (nextActivity.getName().equals("pre-issued") && !permissionLimitService.getPosUnderLimit(order,
				permissionLimitService.getPoUserConfigLimit(auth, currentSubdiv.getComp()), currentSubdiv.getComp())
				|| !purchaseOrderStatusServ.isAllowChangeStatus(currentStatus, auth)) {
			return false;
		}

		// set new status and save
		order.setStatus(nextStatus);

		// if being issued, set fields, and indicate to linked job items that
		// purchase order raised
		if (nextActivity.isOnIssue()) {
			order.setIssued(true);
			order.setIssueby(currentContact);
			order.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			for (PurchaseOrderItem item : order.getItems()) {
				for (PurchaseOrderJobItem poji : item.getLinkedJobItems()) {
					this.interceptor_raisePurchaseOrder.recordAction(poji);
				}
			}
		}

		// insert a new action record
		// add a new purchaseorderactivity for creating the purchase order
		PurchaseOrderAction action = new PurchaseOrderAction();
		action.setContact(currentContact);
		action.setDate(new Date());
		action.setOutcomeStatus(nextStatus);
		action.setOrder(order);
		action.setActivity(nextActivity);
		this.purchaseOrderActionServ.saveOrUpdatePurchaseOrderAction(action);

		if (order.getActions() == null) {
			order.setActions(new HashSet<>());
		}
		order.getActions().add(action);
		return true;
	}

	@Override
	public void validateAndCompletePurchaseOrder(PurchaseOrder order, Contact currentContact) {
		// we need to make sure that the order is not already complete /
		// cancelled
		// and not already brokered
		if (!order.getStatus().isOnComplete() && !order.getStatus().isOnCancel()
				&& (order.getAccountsStatus() == null)) {
			// make sure an order has at least one item before we change any
			// status's
			if ((order.getItems() != null) && (order.getItems().size() > 0)) {
				int countItemsCancelled = 0;
				int countItemsApprovedOrCancelled = 0;
				for (PurchaseOrderItem item : order.getItems()) {
					if (item.isCancelled())
						countItemsCancelled++;
					if (item.isCancelled() || item.isAccountsApproved())
						countItemsApprovedOrCancelled++;
				}

				PurchaseOrderStatus newActivity = null;
				AccountStatus accountStatus = null;
				// if (a) all items are cancelled or (b) all items are completed
				// or cancelled we can
				// update the order status
				if ((order.getItems().size() == countItemsCancelled)) {
					newActivity = this.purchaseOrderStatusServ.findOnCancelPurchaseOrderStatus(ActionType.ACTION);
					// No brokering of cancelled purchase orders
					accountStatus = null;
				} else if (order.getItems().size() == countItemsApprovedOrCancelled) {
					newActivity = this.purchaseOrderStatusServ.findOnCompletePurchaseOrderStatus(ActionType.ACTION);
					// update the accounts status to enable brokering
					accountStatus = AccountStatus.P;
				} 

				if (newActivity != null) {
					// update the status and add a new action for the
					// PurchaseOrder
					PurchaseOrderAction action = new PurchaseOrderAction();
					action.setActivity(newActivity);
					action.setContact(currentContact);
					action.setDate(new Date());
					action.setOrder(order);
					action.setOutcomeStatus(action.getActivity().getNext());
					if (order.getActions() == null) {
						order.setActions(new TreeSet<>(new ActionComparator()));
					}
					order.getActions().add(action);
					order.setStatus(action.getOutcomeStatus());
					order.setAccountsStatus(accountStatus);
				}
			}
		}
	}

	@Override
	public Integer copyPo(Integer oldPoId, Boolean cancelNotReceivedGoods, Contact currentContact, Integer subdivId) {

		// load old PO
		PurchaseOrder oldPo = this.purchaseOrderDao.find(oldPoId);

		// new PO
		PurchaseOrder po = new PurchaseOrder();

		/* copy properties except id */
		BeanUtils.copyProperties(oldPo, po, PurchaseOrder_.id.getName());

		/* update fields */
		po.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		po.setIssued(false);
		po.setIssuedate(null);
		po.setCreatedBy(currentContact);
		po.setStatus((PurchaseOrderStatus) this.statusServ.findDefaultStatus(PurchaseOrderStatus.class));
		po.setAccountsStatus(null);        // Empty accounts sync status for newly created PO
		Subdiv subdiv = subdivService.get(subdivId);
		String poNumber = numerationService.generateNumber(NumerationType.PURCHASE_ORDER, subdiv.getComp(), subdiv);
		po.setPono(poNumber);

		POOrigin<PurchaseOrder> origin = cancelNotReceivedGoods ? new POOriginCancelCopy(oldPo) : new POOriginSimpleCopy(oldPo);
		po.setPoOrigin(origin);

		/* actions */
		// delete old actions
		po.setActions(new TreeSet<>(new ActionComparator()));
		// add a new purchase order activity for creating the purchase order
		PurchaseOrderAction action = new PurchaseOrderAction();
		action.setContact(po.getCreatedBy());
		action.setDate(new Date());
		action.setOutcomeStatus(po.getStatus());
		action.setActivity(this.purchaseOrderStatusServ.findOnInsertPurchaseOrderStatus(ActionType.ACTION));
		action.setOrder(po);

		/* copy order notes */
		Set<PurchaseOrderNote> notes = oldPo.getNotes().stream().map(oldNote -> {
			PurchaseOrderNote note = new PurchaseOrderNote();
			BeanUtils.copyProperties(oldNote, note, PurchaseOrderNote_.noteid.getName());
			note.setOrder(po);
			return note;
		}).collect(Collectors.toCollection(() -> new TreeSet<>(new NoteComparator())));
		po.setNotes(notes);

		/* copy supplier invoices */
		Set<SupplierInvoice> supplierInvoices = oldPo.getSupplierInvoices().stream().map(oldSI -> {
			SupplierInvoice si = new SupplierInvoice();
			BeanUtils.copyProperties(oldSI, si, SupplierInvoice_.id.getName());
			si.setPurchaseOrder(po);
			return si;
		}).collect(Collectors.toCollection(() -> new TreeSet<>(new SupplierInvoiceComparator())));
		po.setSupplierInvoices(supplierInvoices);

		/* copy order items */
		Set<PurchaseOrderItem> items = oldPo.getItems().stream().map(oldPoi -> {
			// create new item
			PurchaseOrderItem poi = new PurchaseOrderItem();
			BeanUtils.copyProperties(oldPoi, poi, PurchaseOrderItem_.id.getName(), PurchaseOrderItem_.order.getName(),
				PurchaseOrderItem_.progressActions.getName(), PurchaseOrderItem_.goodsApproved.getName(),
				PurchaseOrderItem_.accountsApproved.getName(), PurchaseOrderItem_.supplierInvoice.getName());

			/* update fields */
			poi.setOrder(po);

			/* copy notes */
			SortedSet<PurchaseOrderItemNote> itemNotes = poi.getNotes().stream().map(n -> new PurchaseOrderItemNote(poi, n)).collect(Collectors.toCollection(() -> new TreeSet<>(new NoteComparator())));
			poi.setNotes(itemNotes);

			/* copy LinkedJobitems */
			SortedSet<PurchaseOrderJobItem> linkedji = poi.getLinkedJobItems().stream().map(lji -> new PurchaseOrderJobItem(lji.getJi(), poi)).collect(Collectors
				.toCollection(() -> new TreeSet<>(new PurchaseOrderJobItemComparator())));
			poi.setLinkedJobItems(linkedji);

			return poi;
		}).collect(Collectors.toCollection(() -> new TreeSet<>(new PurchaseOrderItemComparator())));
		po.setItems(items);

		// if to cancel not received goods, remove canceled or completed from
		// the new order
		if (cancelNotReceivedGoods) {
			po.getItems().removeIf(poi -> !(poi.getReceiptStatus() == PurchaseOrderItemReceiptStatus.NOT_RECEIVED));
		}

		// cancel not yet received good if cancelNotReceivedGoods=true on old PO
		for (PurchaseOrderItem poi : oldPo.getItems()) {
			if (poi.getReceiptStatus() == PurchaseOrderItemReceiptStatus.NOT_RECEIVED && cancelNotReceivedGoods) {
				poi.setCancelled(true);
				this.poItemDao.persist(poi);

				// create progress action
				PurchaseOrderItemProgressAction progressAction = new PurchaseOrderItemProgressAction();
				progressAction.setAccountsApproved(poi.isAccountsApproved());
				progressAction.setCancelled(true);
				progressAction.setContact(currentContact);
				progressAction.setDate(new Date());
				progressAction.setGoodsApproved(poi.isGoodsApproved());
				progressAction.setItem(poi);
				progressAction.setReceiptStatus(poi.getReceiptStatus());
				poi.getProgressActions().add(progressAction);
				poItemService.updatePurchaseOrderItem(poi, currentContact);
			}
		}
		validateAndCompletePurchaseOrder(oldPo, currentContact);

		// save po
		this.purchaseOrderDao.persist(po);
		return po.getId();
	}

	@Override
	public PurchaseOrder findByNumber(String poNo, Integer busId) {

		return purchaseOrderDao.findByNumber(poNo, busId);
	}

	@Override
	public List<AjaxPurchaseOrderItemDTO> preparePurchaseOrderItem(int jobid, StateGroup type, Locale locale) {
		return purchaseOrderDao.preparePurchaseOrderItem(jobid, type, locale);
	}

	@Override
	public List<SupplierPOProjectionDTO> getByAccountStatusUsingProjection(Integer businessCompanyId, Integer subdivId,
			AccountStatus accStatus) {
		return this.purchaseOrderDao.getByAccountStatusUsingProjection(businessCompanyId, subdivId, accStatus);
	}

	@Override
	public List<SupplierPOProjectionDTO> getAllPurchaseOrdersAtStatusUsingProjection(Integer statusId,
			Integer allocatedCompanyId, Integer subdivId) {
		return this.purchaseOrderDao.getAllPurchaseOrdersAtStatusUsingProjection(statusId, allocatedCompanyId,
				subdivId);
	}

	@Override
	public List<PurchaseOrder> getAllPurchaseOrdersAtStatus(Integer statusId, Integer allocatedCompanyId) {
		return this.purchaseOrderDao.getAllPurchaseOrdersAtStatus(statusId, allocatedCompanyId);
	}

	@Override
	public List<Integer> getListOfItemNo(PurchaseOrder order, Company allocatedCompany) {
		// list of itemNo of job items with a 'requireSupplierCompanyList' setting of true
		List<Integer> requireSupplierCompanyListTrue = new ArrayList<>();
		// list of itemNo of job items with a 'requireSupplierCompanyList' setting of false
		List<Integer> requireSupplierCompanyListFalse = new ArrayList<>();
		List<ClientCompanyDTO> listClientCompany = poItemService.getClientCompanyDTOFromPO(order.getId());
		listClientCompany.forEach(dto -> {
			// check that the supplier company set on the purchase order is a 'member' of 
			// the approved list for the 'usage' company as obtained through the job contact
			Boolean onApprovedList = companyListService.supplierInCompanyListUsedClient(dto.getClientCompanyId(), order.getContact().getSub().getComp().getCoid(), order.getOrganisation());
			if (!dto.getRequireSupplierCompanyList()) {
				requireSupplierCompanyListFalse.add(dto.getItemNo());
			} else if (!onApprovedList) {
				requireSupplierCompanyListTrue.add(dto.getItemNo());
			}
		});
		
		// if there are no job items with a 'requireSupplierCompanyList' setting 
		// of true then no message is required.
		if(requireSupplierCompanyListFalse.size() == listClientCompany.size()) {
			return new ArrayList<>();
		}
		 
		// if one or more job items do not have the supplier company on an approved list, 
		// we should display a 'warning' 
		else if ( !requireSupplierCompanyListTrue.isEmpty() && requireSupplierCompanyListFalse.isEmpty()){
			return requireSupplierCompanyListTrue;
		}

		// if one or more job items which have a "requireSupplierCompanyList" setting of true 
		// and the supplier company on an approved list, we should display a info message
		else if (requireSupplierCompanyListTrue.isEmpty() && requireSupplierCompanyListFalse.isEmpty()) {
			requireSupplierCompanyListTrue.add(0);
			return requireSupplierCompanyListTrue;
		}

		return new ArrayList<>();
	}

	@Override
	public List<Integer> getJobItemsIdsFromPurchaseOrder(PurchaseOrder purchaseOrder) {
		return this.purchaseOrderDao.getJobItemsIdsFromPurchaseOrder(purchaseOrder);
	}

	@Override
	public List<PurchaseOrderDTO> getDtosByJobid(Integer jobId, Locale locale) {
		return this.purchaseOrderDao.getDtosByJobid(jobId, locale);
	}

	@Override
	public Long countPoPerJob(Integer jobId, Locale locale) {
		return this.purchaseOrderDao.countPoPerJob(jobId, locale);
	}
	
	@Override
	public List<InvoiceItem> getInvoiceItemsWithPos(Integer invoiceid){
		List<Integer> bpos = this.bPOService.getLinkedToJobsOnInvoice(invoiceid);
		return this.purchaseOrderDao.getInvoiceItemsWithPos(invoiceid, bpos);
	}
}