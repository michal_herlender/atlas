package org.trescal.cwms.core.pricing.creditnote.entity.creditnote;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.CreditNoteAction;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItemComparator;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.CreditNoteNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotetax.CreditNoteTax;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.TaxablePricing;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.baseaction.ActionComparator;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "creditnote")
// overrides the column definition for contact inherited from Pricing - allows
// fk to be nullable (credit notes don't have a contact)
@AssociationOverrides({
	@AssociationOverride(name = "contact", joinColumns = @JoinColumn(name = "personid"))})
@Setter
public class CreditNote extends TaxablePricing<Company, CreditNoteItem, CreditNoteTax>
		implements NoteAwareEntity, ComponentEntity {

	private AccountStatus accountsStatus;
	private Set<CreditNoteAction> actions;
	private String creditNoteNo;
	private File directory;
	private Invoice invoice;
	private Set<CreditNoteItem> items;
	private Set<CreditNoteNote> notes;
	private List<Email> sentEmails;
	private CreditNoteStatus status;
	private VatRate vatRateEntity;

	/**
	 * Standard constructor Initialize all collections to prevent null checks later
	 */
	public CreditNote() {
		super();
		this.setItems(new HashSet<>());
		this.setNotes(new HashSet<>());
		this.setTaxes(new ArrayList<>());
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "accountstatus", length = 1)
	public AccountStatus getAccountsStatus() {
		return this.accountsStatus;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "creditNote")
	@SortComparator(ActionComparator.class)
	public Set<CreditNoteAction> getActions() {
		return this.actions;
	}

	@NotNull
	@Length(max = 30)
	@Column(name = "creditnoteno", length = 30, nullable = false)
	public String getCreditNoteNo() {
		return this.creditNoteNo;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		/*
		 * Some logic added here so that when an e-mail is created for an credit note it
		 * will add in the contact for job by default if present
		 */

		if (this.invoice != null) {
			return this.invoice.getDefaultContact();
		}

		return this.contact;
	}

	@Override
	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return this.creditNoteNo;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invid", nullable = false)
	public Invoice getInvoice() {
		return this.invoice;
	}

	@Override
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "creditNote", orphanRemoval = true)
	@SortComparator(CreditNoteItemComparator.class)
	public Set<CreditNoteItem> getItems() {
		return items;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "creditNote")
	@SortComparator(NoteComparator.class)
	public Set<CreditNoteNote> getNotes() {
		return this.notes;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		return null;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = false)
	public CreditNoteStatus getStatus() {
		return this.status;
	}

	/*
	 * Required for brokering of invoice to accounting interface and deduction of
	 * general business posting group.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vatcode", columnDefinition = "varchar(1)")
	public VatRate getVatRateEntity() {
		return vatRateEntity;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return true;
	}
}