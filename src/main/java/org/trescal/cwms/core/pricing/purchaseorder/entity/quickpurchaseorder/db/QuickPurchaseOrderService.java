package org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.db;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.purchaseorder.dto.SupplierPOProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder;

public interface QuickPurchaseOrderService extends BaseService<QuickPurchaseOrder, Integer>
{
	/**
	 * Deletes the {@link QuickPurchaseOrder} identified by the given id.
	 * 
	 * @param qpoid the id of the {@link QuickPurchaseOrder}.
	 * @param reason the reason for deleting the {@link QuickPurchaseOrder}.
	 * @return {@link ResultWrapper} indicating success.
	 */
	ResultWrapper ajaxDeleteQuickPurchaseOrder(int qpoid, String reason, HttpSession session);
	
	/**
	 * Inserts a {@link QuickPurchaseOrder} using the description provided.
	 * Validates the description, issues the {@link PurchaseOrder} number and
	 * returns the new {@link QuickPurchaseOrder} as the results of the
	 * {@link ResultWrapper}.
	 * 
	 * @param description the {@link QuickPurchaseOrder} descriptop.
	 * @param jobno number of the {@link Job} which this quick po can be linked
	 *        to
	 * @return {@link ResultWrapper} indicating success.
	 */
	ResultWrapper createQuickPurchaseOrder(String description, String jobno, HttpSession session);
	
	/**
	 * Returns a list of all {@link QuickPurchaseOrder} entities that are
	 * unconsolidated sorted by creation date asc.
	 * 
	 * @return {@link List} of {@link QuickPurchaseOrder}.
	 */
	List<QuickPurchaseOrder> findAllUnconsolidatedQuickPurchaseOrders(Company allocatedCompany);
	
	
	List<SupplierPOProjectionDTO> findAllUnconsolidatedQuickPurchaseOrdersUsingProjection(Integer allocatedCompanyId,Integer SubdivId);
}