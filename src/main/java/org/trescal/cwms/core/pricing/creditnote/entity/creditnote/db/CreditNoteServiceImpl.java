package org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteDTO;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.CreditNoteAction;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItemComparator;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.db.CreditNoteItemService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.CreditNoteNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.db.CreditNoteStatusService;
import org.trescal.cwms.core.pricing.creditnote.form.CreditNoteHomeForm;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.NominalInvoiceCostDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.tax.TaxCalculationSystem;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.baseaction.ActionComparator;
import org.trescal.cwms.core.system.entity.basestatus.db.BaseStatusService;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.deletedcomponent.db.DeletedComponentService;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Service("CreditNoteService")
public class CreditNoteServiceImpl extends BaseServiceImpl<CreditNote, Integer> implements CreditNoteService {

    @Value("${cwms.config.avalara.aware}")
    private Boolean avalaraAware;
    @Autowired
    private BaseStatusService statusServ;
    @Autowired
    private BeanValidator validator;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private CreditNoteDao creditNoteDao;
	@Autowired
	private CreditNoteItemService cniServ;
    @Autowired
	private CreditNoteStatusService cnsServ;
	@Autowired
	private DeletedComponentService delCompServ;
	@Autowired
	private InvoiceService invServ;
	@Autowired
	private NominalCodeService nominalServ;
    @Autowired
    private NumerationService numerationService;
    @Autowired
    private SystemComponentService scServ;
    @Autowired
    private UserService userService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private TaxCalculator taxCalculator;
    @Autowired
    private NoteService noteService;

    @Override
    protected BaseDao<CreditNote, Integer> getBaseDao() {
        return creditNoteDao;
    }

    @Override
    public CreditNote createCreditNote(Invoice inv, boolean creditWholeInvoice, Contact currentCon,
                                       Subdiv allocatedSubdiv) {
        Locale locale = LocaleContextHolder.getLocale();
		CreditNote cn = new CreditNote();
		cn.setTotalCost(new BigDecimal("0.00"));
		cn.setFinalCost(new BigDecimal("0.00"));
		cn.setVatRateEntity(inv.getVatRateEntity());
		cn.setVatRate(inv.getVatRate());
		cn.setVatValue(new BigDecimal("0.00"));
		cn.setDuration(1);
		cn.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		cn.setIssued(false);
		cn.setInvoice(inv);
		cn.setCurrency(inv.getCurrency());
		cn.setRate(inv.getCurrency().getDefaultRate());
		cn.setCreatedBy(currentCon);
		String cnNo = numerationService.generateNumber(NumerationType.CREDIT_NOTE, allocatedSubdiv.getComp(),
			allocatedSubdiv);
		cn.setCreditNoteNo(cnNo);
		cn.setOrganisation(allocatedSubdiv.getComp());
        cn.setStatus((CreditNoteStatus) this.statusServ.findDefaultStatus(CreditNoteStatus.class));
        cn.setNotes(noteService.initalizeNotes(cn, CreditNoteNote.class, locale));
		CreditNoteAction action = new CreditNoteAction();
		action.setContact(currentCon);
		action.setDate(new Date());
		action.setOutcomeStatus(cn.getStatus());
		action.setActivity(this.cnsServ.findOnInsertActivity());
		action.setCreditNote(cn);
		Set<CreditNoteAction> actions = new TreeSet<>(new ActionComparator());
		actions.add(action);
		cn.setActions(actions);
		cn.setItems(new TreeSet<>(new CreditNoteItemComparator()));
		// Assumption: only one ship-to address per invoice
		// In another step we should generate a credit note item per nominal code and ship-to address
		Address shipToAddress = inv.getItems().isEmpty() ? inv.getAddress() : inv.getItems().stream().findAny().get().getShipToAddress();
		if (creditWholeInvoice) {
			List<NominalInvoiceCostDTO> nominals = this.invServ.getNominalInvoiceSummary(cn.getInvoice().getId());
			for (NominalInvoiceCostDTO nom : nominals) {
				NominalCode nc = this.nominalServ.findNominalCodeByCode(nom.getNominalCode());
				String desc = "Credit against " + translationService.getCorrectTranslation(nc.getTitleTranslations(),
					LocaleContextHolder.getLocale());
				CreditNoteItem cni = this.cniServ.buildNewCreditNoteItem(cn, desc, nc, nom.getCost(), allocatedSubdiv);
				cni.setShipToAddress(shipToAddress);
				cn.getItems().add(cni);
			}
			cn.setTotalCost(
				cn.getItems().stream().map(CreditNoteItem::getFinalCost).reduce(BigDecimal.ZERO, BigDecimal::add));
			TaxCalculationSystem taxCalculationSystem = avalaraAware ? TaxCalculationSystem.AVATAX
					: TaxCalculationSystem.DEFAULT;
			taxCalculator.calculateTax(cn, taxCalculationSystem);
			// update the cost of the credit note
			CostCalculator.updatePricingWithCalculatedTotalCost(cn);
		}
		this.save(cn);
		return cn;
	}

	@Override
	public boolean changeAccountStatus(String creditNoteNumber, Integer companyId, AccountStatus accountStatus) {
		try {
			CreditNote creditNote = findByNumber(creditNoteNumber, companyId);
			creditNote.setAccountsStatus(AccountStatus.S);
			this.merge(creditNote);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
	
	@Override
	public List<KeyValueIntegerString> getNumbersPerInvoices(Collection<Integer> invoiceIds) {
		return creditNoteDao.getNumbersPerInvoices(invoiceIds);
	}

	@Override
	public ResultWrapper deleteCreditNote(int crid, String reason, HttpSession session) {
		// validate credit note exists
		CreditNote cn = this.get(crid);
		// credit note null?
		if (cn == null)
			return new ResultWrapper(false, "Invoice could not be found", null, null);
		else {
			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact contact = this.userService.getEagerLoad(username).getCon();
			// create deletion object
			DeletedComponent dinv = new DeletedComponent();
			dinv.setComponent(Component.CREDIT_NOTE);
			dinv.setContact(contact);
			dinv.setDate(new Date());
			dinv.setRefNo(cn.getCreditNoteNo());
			dinv.setReason(reason);
			// validate deletion object
			BindException errors = new BindException(dinv, "dinv");
			this.validator.validate(dinv, errors);
			// if validation passes then delete the invoice
			if (!errors.hasErrors()) {
				// delete the credit note
				this.delete(cn);
				// persist the deleted credit note
				this.delCompServ.insertDeletedComponent(dinv);
			}
			// set deletedinvoice into the results
			return new ResultWrapper(errors, dinv);
		}
	}

	public List<CreditNote> findAllCreditNotesByAccountStatus(Company businessCompany, AccountStatus accStatus) {
		return this.creditNoteDao.findAllCreditNotesByAccountStatus(businessCompany, accStatus);
	}

	@Override
	public CreditNote get(Integer id) {
		CreditNote creditNote = this.creditNoteDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (creditNote != null && creditNote.getDirectory() == null) {
			creditNote.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.CREDIT_NOTE),
					creditNote.getCreditNoteNo()));
		}
		return creditNote;
	}

	public List<CreditNote> getAllAtStatus(Company businessCompany, CreditNoteStatus status) {
		return this.creditNoteDao.getAllAtStatus(businessCompany, status);
	}

	public PagedResultSet<CreditNoteDTO> searchCreditNotes(CreditNoteHomeForm form, PagedResultSet<CreditNoteDTO> rs) {
		return this.creditNoteDao.searchCreditNotes(form, rs);
	}

	@Override
	public CreditNote findByNumber(String cnNo, Integer orgid) {
		// TODO Auto-generated method stub
		return creditNoteDao.findByNumber(cnNo, orgid);
	}

	@Override
	public CreditNote recalculateAndMerge(CreditNote creditNote) {
		creditNote.setTotalCost(creditNote.getItems().stream().map(CreditNoteItem::getFinalCost).reduce(BigDecimal.ZERO,
				BigDecimal::add));
		TaxCalculationSystem taxCalculationSystem = avalaraAware ? TaxCalculationSystem.AVATAX
				: TaxCalculationSystem.DEFAULT;
		taxCalculator.calculateTax(creditNote, taxCalculationSystem);
		CostCalculator.updatePricingWithCalculatedTotalCost(creditNote);
		return this.merge(creditNote);
	}
}