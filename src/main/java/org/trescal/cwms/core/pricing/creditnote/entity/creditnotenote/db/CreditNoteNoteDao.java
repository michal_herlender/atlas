package org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.CreditNoteNote;

public interface CreditNoteNoteDao extends BaseDao<CreditNoteNote, Integer> {

}
