package org.trescal.cwms.core.pricing.entity.costs.base;

import java.math.BigDecimal;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@Entity
@Table(name="costs_service")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "costtype", discriminatorType = DiscriminatorType.STRING)
public class ServiceCost extends Cost {
	
	public ServiceCost() {
		super();
	}
	
	public ServiceCost(BigDecimal cost) {
		this.setActive(true);
		this.setTotalCost(cost);
		this.setDiscountRate(BigDecimal.valueOf(0.00));
	}
	
	@Override
	@Transient
	public CostType getCostType() {
		return CostType.SERVICE;
	}
	
	@Override
	@Transient
	public Cost getLinkedCostSrc() {
		return null;
	}
}