package org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.db;

import java.util.List;

import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

public interface ThirdPartyPricingItemService
{
	void deleteThirdPartyPricingItem(ThirdPartyPricingItem thirdpartypricingitem);

	ThirdPartyPricingItem findThirdPartyPricingItem(int id);

	List<ThirdPartyPricingItem> getAllThirdPartyPricingItems();

	void insertThirdPartyPricingItem(ThirdPartyPricingItem thirdpartypricingitem);

	void saveOrUpdateThirdPartyPricingItem(ThirdPartyPricingItem thirdpartypricingitem);

	/**
	 * Handles setting the default values for third party carriage information.
	 * 
	 * @param item the {@link ThirdPartyPricingItem}.
	 * @return the {@link ThirdPartyPricingItem}.
	 */
	ThirdPartyPricingItem setThirdPartyDefaultValues(ThirdPartyPricingItem item);

	/**
	 * Handles updating the third party carriage costs associated with this
	 * {@link ThirdPartyPricingItem}. Does <i>not</i> update the costs of the
	 * {@link JobCostingItem} as a whole, it is assumed another layer of
	 * functionality will take of this. See {@link CostCalculator}.
	 * 
	 * @param thirdpartypricingitem the {@link ThirdPartyPricingItem} whose
	 *        carriage costs to update.
	 */
	ThirdPartyPricingItem updateThirdPartyCarriageCosts(ThirdPartyPricingItem thirdpartypricingitem);

	void updateThirdPartyPricingItem(ThirdPartyPricingItem thirdpartypricingitem);
}