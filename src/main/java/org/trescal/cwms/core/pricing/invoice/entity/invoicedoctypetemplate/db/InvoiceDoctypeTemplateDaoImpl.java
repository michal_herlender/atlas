package org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.InvoiceDoctypeTemplate;

@Repository("InvoiceDoctypeTemplateDao")
public class InvoiceDoctypeTemplateDaoImpl extends BaseDaoImpl<InvoiceDoctypeTemplate, Integer> implements InvoiceDoctypeTemplateDao {
	
	@Override
	protected Class<InvoiceDoctypeTemplate> getEntity() {
		return InvoiceDoctypeTemplate.class;
	}
	
	@Override
	public InvoiceDoctypeTemplate findByCompanyType(int coid, int typeid, String doctype) {
		Criteria criteria = getSession().createCriteria(InvoiceDoctypeTemplate.class);
		criteria.createCriteria("type").add(Restrictions.idEq(typeid));
		criteria.createCriteria("company").add(Restrictions.idEq(coid));
		criteria.createCriteria("componentDoctype").createCriteria("doctype").add(Restrictions.eq("name", doctype));
		Object obj = criteria.uniqueResult();
		return obj == null ? null : (InvoiceDoctypeTemplate) obj;
	}

	@Override
	public InvoiceDoctypeTemplate findByType(int typeid, String doctype) {
		Criteria criteria = getSession().createCriteria(InvoiceDoctypeTemplate.class);
		criteria.createCriteria("type").add(Restrictions.idEq(typeid));
		criteria.add(Restrictions.isNull("company"));
		criteria.createCriteria("componentDoctype").createCriteria("doctype").add(Restrictions.eq("name", doctype));
		Object obj = criteria.uniqueResult();
		return obj == null ? null : (InvoiceDoctypeTemplate) obj;
	}
}