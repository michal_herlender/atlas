package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.db;

import java.util.List;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.PurchaseOrderAction;

public class PurchaseOrderActionServiceImpl implements PurchaseOrderActionService
{
	PurchaseOrderActionDao purchaseOrderActionDao;

	public PurchaseOrderAction findPurchaseOrderAction(int id)
	{
		return purchaseOrderActionDao.find(id);
	}

	public void insertPurchaseOrderAction(PurchaseOrderAction PurchaseOrderAction)
	{
		purchaseOrderActionDao.persist(PurchaseOrderAction);
	}

	public void updatePurchaseOrderAction(PurchaseOrderAction PurchaseOrderAction)
	{
		purchaseOrderActionDao.update(PurchaseOrderAction);
	}

	public List<PurchaseOrderAction> getAllPurchaseOrderActions()
	{
		return purchaseOrderActionDao.findAll();
	}

	public void setPurchaseOrderActionDao(PurchaseOrderActionDao purchaseOrderActionDao)
	{
		this.purchaseOrderActionDao = purchaseOrderActionDao;
	}

	public void deletePurchaseOrderAction(PurchaseOrderAction purchaseorderaction)
	{
		this.purchaseOrderActionDao.remove(purchaseorderaction);
	}

	public void saveOrUpdatePurchaseOrderAction(PurchaseOrderAction purchaseorderaction)
	{
		this.purchaseOrderActionDao.saveOrUpdate(purchaseorderaction);
	}
}