package org.trescal.cwms.core.pricing.jobcost.form;

import org.trescal.cwms.core.pricing.jobcost.controller.EditJobCostingItemController;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost.TPQuotationAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost.TPQuotationRepairCost;

/**
 * Formbacking object for {@link EditJobCostingItemController}.
 * 
 * @author Richard
 */
public class EditJobCostingItemForm
{
	/**
	 * The id of a linked {@link TPQuotationAdjustmentCost}.
	 */
	private Integer adjustmentCostId;

	/**
	 * The id of a linked {@link TPQuotationCalibrationCost}.
	 */
	private Integer calibrationCostId;

	private JobCostingItem item;
	private String openTabId;
	/**
	 * The id of a linked {@link TPQuotationPurchaseCost}.
	 */
	private Integer purchaseCostId;
	/**
	 * The id of a linked {@link TPQuotationRepairCost}.
	 */
	private Integer repairCostId;

	public Integer getAdjustmentCostId()
	{
		return this.adjustmentCostId;
	}

	public Integer getCalibrationCostId()
	{
		return this.calibrationCostId;
	}

	public JobCostingItem getItem()
	{
		return this.item;
	}

	public String getOpenTabId()
	{
		return this.openTabId;
	}

	public Integer getPurchaseCostId()
	{
		return this.purchaseCostId;
	}

	public Integer getRepairCostId()
	{
		return this.repairCostId;
	}

	public void setAdjustmentCostId(Integer adjustmentCostId)
	{
		this.adjustmentCostId = adjustmentCostId;
	}

	public void setCalibrationCostId(Integer calibrationCostId)
	{
		this.calibrationCostId = calibrationCostId;
	}

	public void setItem(JobCostingItem item)
	{
		this.item = item;
	}

	public void setOpenTabId(String openTabId)
	{
		this.openTabId = openTabId;
	}

	public void setPurchaseCostId(Integer purchaseCostId)
	{
		this.purchaseCostId = purchaseCostId;
	}

	public void setRepairCostId(Integer repairCostId)
	{
		this.repairCostId = repairCostId;
	}
}
