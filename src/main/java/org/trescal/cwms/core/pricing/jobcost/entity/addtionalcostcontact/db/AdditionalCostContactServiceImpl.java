package org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.db;

import java.util.List;

import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;

public class AdditionalCostContactServiceImpl implements AdditionalCostContactService
{
	AdditionalCostContactDao additionalCostContactDao;

	public AdditionalCostContact findAdditionalCostContact(int id)
	{
		return additionalCostContactDao.find(id);
	}

	public void insertAdditionalCostContact(AdditionalCostContact AdditionalCostContact)
	{
		additionalCostContactDao.persist(AdditionalCostContact);
	}

	public void updateAdditionalCostContact(AdditionalCostContact AdditionalCostContact)
	{
		additionalCostContactDao.update(AdditionalCostContact);
	}

	public List<AdditionalCostContact> getAllAdditionalCostContacts()
	{
		return additionalCostContactDao.findAll();
	}

	public void setAdditionalCostContactDao(AdditionalCostContactDao additionalCostContactDao)
	{
		this.additionalCostContactDao = additionalCostContactDao;
	}

	public void deleteAdditionalCostContact(AdditionalCostContact additionalcostcontact)
	{
		this.additionalCostContactDao.remove(additionalcostcontact);
	}

	public void saveOrUpdateAdditionalCostContact(AdditionalCostContact additionalcostcontact)
	{
		this.additionalCostContactDao.saveOrUpdate(additionalcostcontact);
	}
}