package org.trescal.cwms.core.pricing.invoice.entity.invoicepo.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

@Repository("InvoicePODao")
public class InvoicePODaoImpl extends BaseDaoImpl<InvoicePO, Integer> implements InvoicePODao {
	
	@Override
	protected Class<InvoicePO> getEntity() {
		return InvoicePO.class;
	}
}