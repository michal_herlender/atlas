package org.trescal.cwms.core.pricing.invoice.dto;

import lombok.Value;

import java.math.BigDecimal;

@Value(staticConstructor = "of")
public class SupportedCurrencyDto {
    String currencyCode;
    BigDecimal defaultRate;
}
