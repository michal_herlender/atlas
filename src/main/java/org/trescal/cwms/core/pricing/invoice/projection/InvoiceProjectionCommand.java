package org.trescal.cwms.core.pricing.invoice.projection;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;

import lombok.Getter;

@Getter
public class InvoiceProjectionCommand {
	// Mandatory values in constructor (optionally used by some data loading, mandatory for simplicity)
	private Locale locale;
	private Integer allocatedCompanyId;
	// Initialized via options (default false)
	private Boolean loadNotInvoicedJobItems;
	private Boolean loadNotInvoicedJobItemDeliveries;
	private Boolean loadNotInvoicedExpenseItems;
	private Boolean loadNotInvoicedExpenseItemClientPOs;
	private Boolean calcNotInvoicedJobItemEstimates;
	private Boolean loadCompany;
	private Boolean loadJobLinks;
	
	// Optionally initialized via constructor (if present, load job items)
	private JobItemProjectionCommand notInvoicedJobItemCommand;
	private JobProjectionCommand notInvoicedJobCommand;
	
	public class INV_LOAD_NOT_INVOICED_JOB_ITEMS {};				// Load linked job items (for periodic invoices)
	public class INV_LOAD_NOT_INVOICED_JOB_ITEM_DELIVERIES {};		// Load linked job item delivery info (for periodic invoices)
	public class INV_LOAD_NOT_INVOICED_EXPENSE_ITEMS {};			// Load linked expense items (for periodic invoices) 
	public class INV_LOAD_NOT_INVOICED_EXPENSE_ITEM_CLIENT_POS {};	// Load client POs/BPOs for linked expense items (for periodic invoices)
	public class INV_CALC_NOT_INVOICED_JOB_ITEM_ESTIMATES {};		// Load estimated prices into linked job items (for periodic invoices)
	public class INV_LOAD_COMPANY {};								// Load company being invoiced
	public class INV_LOAD_JOB_LINKS {};								// Load job link data
	
	public InvoiceProjectionCommand(Locale locale, Integer allocatedCompanyId, JobItemProjectionCommand notInvoicedJobItemCommand, 
			JobProjectionCommand notInvoicedJobCommand, Class<?>... options) {
		if (locale == null) throw new IllegalArgumentException("locale must not be null");
		if (allocatedCompanyId == null) throw new IllegalArgumentException("allocatedCompanyId must not be null");
		this.locale = locale;
		this.allocatedCompanyId = allocatedCompanyId;
		List<Class<?>> args = Arrays.asList(options);
		
		this.loadNotInvoicedJobItems = args.contains(INV_LOAD_NOT_INVOICED_JOB_ITEMS.class);
		this.loadNotInvoicedJobItemDeliveries = args.contains(INV_LOAD_NOT_INVOICED_JOB_ITEM_DELIVERIES.class);
		this.loadNotInvoicedExpenseItems = args.contains(INV_LOAD_NOT_INVOICED_EXPENSE_ITEMS.class);
		this.loadNotInvoicedExpenseItemClientPOs = args.contains(INV_LOAD_NOT_INVOICED_EXPENSE_ITEM_CLIENT_POS.class);
		this.calcNotInvoicedJobItemEstimates = args.contains(INV_CALC_NOT_INVOICED_JOB_ITEM_ESTIMATES.class);
		this.loadCompany = args.contains(INV_LOAD_COMPANY.class);
		this.loadJobLinks = args.contains(INV_LOAD_JOB_LINKS.class);

		this.notInvoicedJobItemCommand = notInvoicedJobItemCommand;
		this.notInvoicedJobCommand = notInvoicedJobCommand;
		
		// Logical tests
		if (this.loadNotInvoicedJobItems && (this.notInvoicedJobItemCommand == null))
			throw new IllegalArgumentException("Job Item Command must not be null if loading not-invoiced job items");
		if (this.loadNotInvoicedJobItemDeliveries && !this.loadNotInvoicedJobItems)
			throw new IllegalArgumentException("Must load not-invoiced job items if loading their delivery details");
		if (this.loadNotInvoicedExpenseItemClientPOs && !this.loadNotInvoicedExpenseItems)
			throw new IllegalArgumentException("Must load not-invoiced expense items if loading their client PO/BPO details");
		
		if ((this.notInvoicedJobCommand != null) && (this.notInvoicedJobItemCommand == null))
			throw new IllegalArgumentException("Job Command must not be null if a Job Item Command is provided");
		if (this.calcNotInvoicedJobItemEstimates) {
			// Check data required for loading job item estimates (consider that job pricing is optional)
			if (!loadNotInvoicedJobItems)
				throw new IllegalArgumentException("Must load job items if calculating job item estimates");
			if (this.notInvoicedJobCommand == null)
				throw new IllegalArgumentException("Must load jobs (for currency) if calculating job item estimates");
			if (!this.notInvoicedJobCommand.getLoadCurrency())
				throw new IllegalArgumentException("Must load job currency if calculating job item estimates");
			if (!this.notInvoicedJobItemCommand.getLoadContractReviewPrice())
				throw new IllegalArgumentException("Must load job item contract review price if calculating job item estimates");
			
		}
	}
}