package org.trescal.cwms.core.pricing.invoice.dto;

import lombok.Value;

@Value(staticConstructor = "of")
public class InvoicePoDto {

    Integer poId;
    String poNumber;

}
