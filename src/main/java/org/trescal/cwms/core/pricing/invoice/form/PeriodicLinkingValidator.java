package org.trescal.cwms.core.pricing.invoice.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.pricing.invoice.dto.PeriodicLinkingForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class PeriodicLinkingValidator extends AbstractBeanValidator {

	@Autowired
	private SubdivService subdivService;
	
	public static String MESSAGE_CODE_DEFAULT_CONTACT_NULL = "error.rest.subdiv.nodefaultcontact";
	public static String DEFAULT_MESSAGE_DEFAULT_CONTACT_NULL = "The subdivision does not have a default contact";
	@Override
	public boolean supports(Class<?> clazz) {
		return PeriodicLinkingForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		
		PeriodicLinkingForm plf = (PeriodicLinkingForm) target;
		if ((plf.getItems() == null || plf.getItems().isEmpty())
				&& (plf.getExpenseItems() == null || plf.getExpenseItems().isEmpty())){
			errors.reject("error.value.atleastoneselected");
		} else if(subdivService.get(plf.getAllocatedSubdivId()).getDefaultContact() == null) {
				//Prevent invoice creation by business subdiv with no default contact
				errors.reject(MESSAGE_CODE_DEFAULT_CONTACT_NULL, DEFAULT_MESSAGE_DEFAULT_CONTACT_NULL);
		}
	}
}