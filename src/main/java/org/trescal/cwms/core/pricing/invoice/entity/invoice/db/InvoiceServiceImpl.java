package org.trescal.cwms.core.pricing.invoice.entity.invoice.db;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.avalara.AvalaraConnector;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.filenamingservice.InvoiceNamingService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.ExpenseItemNotInvoicedService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numberformat.db.NumberFormatService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.pricing.invoice.dto.CompanyReadyToBeInvoiced;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.dto.ReadyToInvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.dto.ReadyToInvoiceDTOComparator;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.NominalInvoiceCostDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.InvoiceDoctypeTemplate;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.db.InvoiceDoctypeTemplateService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItemComparator;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db.InvoiceItemPOService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceTypeNames;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db.InvoiceTypeService;
import org.trescal.cwms.core.pricing.invoice.form.InvoiceHomeForm;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.entity.basestatus.db.BaseStatusService;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.deletedcomponent.db.DeletedComponentService;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.template.Template;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.AccountsTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.spring.model.KeyValue;

import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.RuleBasedNumberFormat;

import lombok.val;

@Service("InvoiceService")
public class InvoiceServiceImpl implements InvoiceService {
	private static final Logger logger = LoggerFactory.getLogger(InvoiceServiceImpl.class);

	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Autowired
	private AddressService addressService;
	@Autowired
	private AvalaraConnector avalaraConnector;
	@Autowired
	private BaseStatusService statusServ;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private DeletedComponentService delCompServ;
	@Autowired
	private DocumentService docServ;
	@Autowired
	private InvoiceItemPOService iipoServ;
	@Autowired
	private InvoiceDao invoiceDao;
	@Autowired
	private InvoiceTypeService invoiceTypeService;
	@Autowired
	private InvoiceDoctypeTemplateService invTempServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private NominalCodeService nominalCodeService;
	@Autowired
	private PurchaseOrderItemService purchaseOrderItemService;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private TaxCalculator taxCalculator;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private UserService userService;
	@Autowired
	private BeanValidator validator;
	@Autowired
	private NumberFormatService numberFormatService;
	@Autowired
	private NumerationService numerationService;
	@Value("#{props['cwms.users.automatedusername']}")
	private String autoUserName;
	@Value("#{props['cwms.config.costs.invoices.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	@Autowired
	private ExpenseItemNotInvoicedService einiService;

	/*
	 * method to sort a map by value (value in this case is company name)
	 */
	static LinkedHashMap<Integer, ReadyToInvoiceDTO> sortHashMapByValues(
			HashMap<Integer, ReadyToInvoiceDTO> passedMap) {
		List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
		List<ReadyToInvoiceDTO> mapValues = new ArrayList<>(passedMap.values());
		mapValues.sort(new ReadyToInvoiceDTOComparator());
		Collections.sort(mapKeys);

		LinkedHashMap<Integer, ReadyToInvoiceDTO> sortedMap = new LinkedHashMap<>();

		for (ReadyToInvoiceDTO val : mapValues) {
			Iterator<Integer> keyIt = mapKeys.iterator();
			while (keyIt.hasNext()) {
				Integer key = keyIt.next();
				String comp1 = passedMap.get(key).getCompanyName();
				String comp2 = val.getCompanyName();

				if (comp1.equals(comp2)) {
					keyIt.remove();
					sortedMap.put(key, val);
					break;
				}
			}
		}
		return sortedMap;
	}

	public void addCostToNominalSummary(NominalCode code, BigDecimal cost, Map<String, NominalInvoiceCostDTO> codes) {
		if (codes.containsKey(code.getCode())) {
			NominalInvoiceCostDTO dto = codes.get(code.getCode());
			dto.setCost(dto.getCost().add(cost));
			codes.put(dto.getNominalCode(), dto);
		} else {
			codes.put(code.getCode(), new NominalInvoiceCostDTO(code.getCode(), translationService
					.getCorrectTranslation(code.getTitleTranslations(), LocaleContextHolder.getLocale()), cost));
		}

	}

	@Override
	public Map<Integer, ReadyToInvoiceDTO> getReadyToInvoice(Integer subdivId, Integer companyId) {

		// list of job items and job expenses to invoice grouped by job
		List<ReadyToInvoiceDTO> list = invoiceDao.getReadyToInvoice(subdivId, companyId);
		// group the list by company
		Map<Integer, List<ReadyToInvoiceDTO>> mapGroupedByConame = list.stream()
				.collect(Collectors.groupingBy(ReadyToInvoiceDTO::getCompanyId));

		HashMap<Integer, ReadyToInvoiceDTO> map = new HashMap<>();
		// count the job items (complete/incomplete job) and job expenses
		// (complete/incomplete job)
		// to invoice for each company
		mapGroupedByConame.forEach((key, value) -> {
			Long jiCountJobCompleted = 0L;
			Long jeCountJobCompleted = 0L;
			Long jiCountJobNoCompleted = 0L;
			Long jeCountJobNoCompleted = 0L;
			for (ReadyToInvoiceDTO dto : value) {
				if (dto.getCompleteJob()) {
					jiCountJobCompleted = jiCountJobCompleted + dto.getJobItemsToInvoice();
					jeCountJobCompleted = jeCountJobCompleted + dto.getJobExpenseItemsToInvoice();
				} else {
					jiCountJobNoCompleted = jiCountJobNoCompleted + dto.getJobItemsToInvoice();
					jeCountJobNoCompleted = jeCountJobNoCompleted + dto.getJobExpenseItemsToInvoice();
				}
			}
			ReadyToInvoiceDTO dto = new ReadyToInvoiceDTO(value.get(0).getCompanyName(), value.get(0).getCompanyRole(),
					jiCountJobCompleted, jiCountJobNoCompleted, jeCountJobCompleted, jeCountJobNoCompleted,
					jiCountJobCompleted + jiCountJobNoCompleted, jeCountJobNoCompleted + jeCountJobCompleted);
			map.put(key, dto);
		});

		// return result sorted by companyName
		return sortHashMapByValues(map);
	}

	public void delete(Invoice invoice) {
		if (avalaraAware) {
			avalaraConnector.voidTransaction(invoice);
		}
		this.invoiceDao.remove(invoice);
	}

	@Override
	public ResultWrapper deleteInvoice(int invoiceid, String reason, String username) {
		// validate invoice exists
		Invoice inv = this.findInvoice(invoiceid);
		Locale locale = LocaleContextHolder.getLocale();
		if (inv == null) {
			return new ResultWrapper(false,
					this.messages.getMessage("error.invoice.notfound", null, "Invoice could not be found", locale));
		} else if (reason.isEmpty()) {
			return new ResultWrapper(false, "The reason may not be empty");
		} else {
			List<Integer> invoiceIds = new ArrayList<>();
			invoiceIds.add(invoiceid);
			List<JobExpenseItemNotInvoicedDTO> einiDtos = einiService.getProjectionDTOs(invoiceIds, locale);
			if (CollectionUtils.isNotEmpty(inv.getPeriodicJobItemLinks()) || CollectionUtils.isNotEmpty(einiDtos)) {
				return new ResultWrapper(false, this.messages.getMessage("error.invoice.errors.jobitemlink", null,
						"Periodic invoice can't be deleted because it has linked job items !", locale));
			} else {
				Contact userContact = this.userService.getEagerLoad(username).getCon();
				// create deletion object
				DeletedComponent dinv = new DeletedComponent();
				dinv.setComponent(Component.INVOICE);
				dinv.setContact(userContact);
				dinv.setDate(new Date());
				dinv.setRefNo(inv.getInvno());
				dinv.setReason(reason);

				BindException errors = new BindException(dinv, "dinv");
				// validate deletion object
				this.validator.validate(dinv, errors);

				// if validation passes then delete the invoice
				if (!errors.hasErrors()) {
					Contact autoUser = userService.get(autoUserName).getCon();
					for (PurchaseOrderItem poItem : inv.getInternalPOItems()) {
						poItem.setInternalInvoice(null);
						purchaseOrderItemService.updatePurchaseOrderItem(poItem, autoUser);
					}
					// delete the invoice
					this.delete(inv);

					// persist the deletedinvoice
					this.delCompServ.insertDeletedComponent(dinv);
					return new ResultWrapper(true, null);
				} else {
					// set deletedinvoice into the results
					return new ResultWrapper(false, errors.getMessage());
				}

			}
		}
	}

	public Invoice findInvoice(int id) {
		Invoice invoice = this.invoiceDao.find(id);
		if (invoice != null && invoice.getDirectory() == null) {
			invoice.setDirectory(
					this.compDirServ.getDirectory(this.scServ.findComponent(Component.INVOICE), invoice.getInvno()));
		}
		return invoice;
	}

	@Override
	public Invoice findByInvoiceNo(String invoiceNo, Integer orgId) {
		return invoiceDao.findByInvoiceNo(invoiceNo, orgId);
	}

	public List<Invoice> createInternalInvoices(Company allocatedCompany, Date begin, Date end) {
		List<PurchaseOrderItem> poItems = purchaseOrderItemService.getAllSubcontracting(allocatedCompany, begin, end,
				PurchaseOrderItemReceiptStatus.COMPLETE, false);
		logger.info("Purchase Order Items: " + poItems.size());
		Function<PurchaseOrderItem, Company> receptorFunction = poItem -> poItem.getOrder().getOrganisation();
		Map<Company, List<PurchaseOrderItem>> groupedPOItemsByReceptorCompany = poItems.stream()
				.collect(Collectors.groupingBy(receptorFunction));
		Contact autoUser = userService.get(autoUserName).getCon();
		List<Ledgers> ledgers = Collections.singletonList(Ledgers.SALES_LEDGER);
		List<Invoice> invoices = new ArrayList<>();
		for (Company company : groupedPOItemsByReceptorCompany.keySet()) {
			Function<PurchaseOrderItem, Subdiv> supplierFunction = poItem -> poItem.getOrder().getContact().getSub();
			Map<Subdiv, List<PurchaseOrderItem>> groupedPOItemsBySupplierSubdiv = groupedPOItemsByReceptorCompany
					.get(company).stream().collect(Collectors.groupingBy(supplierFunction));
			for (Subdiv subdiv : groupedPOItemsBySupplierSubdiv.keySet()) {
				Function<PurchaseOrderItem, SupportedCurrency> currencyFunction = poItem -> poItem.getOrder()
						.getCurrency();
				Map<SupportedCurrency, List<PurchaseOrderItem>> groupedPOItemsByCurrency = groupedPOItemsBySupplierSubdiv
						.get(subdiv).stream().collect(Collectors.groupingBy(currencyFunction));
				for (SupportedCurrency currency : groupedPOItemsByCurrency.keySet()) {
					Invoice invoice = generateInternalInvoice(company, allocatedCompany, currency);
					List<PurchaseOrderItem> filteredPOItems = groupedPOItemsByCurrency.get(currency);
					for (PurchaseOrderItem poItem : filteredPOItems) {
						invoice.setPaymentTerm(PaymentTerm.PNM25);
						for (PurchaseOrderJobItem linkedJobItem : poItem.getLinkedJobItems()) {
							InvoiceItem invoiceItem = new InvoiceItem();
							invoiceItem.setJobItem(linkedJobItem.getJi());
							invoiceItem.setShipToAddress(linkedJobItem.getJi().getBookedInAddr());
							invoiceItem.setInvoice(invoice);
							invoiceItem.setItemno(
									invoice.getItems().stream().mapToInt(InvoiceItem::getItemno).max().orElse(0) + 1);
							invoiceItem.setBusinessSubdiv(subdiv);
							invoice.getItems().add(invoiceItem);
							CostType costType = poItem.getCostType() == null ? CostType.CALIBRATION
									: poItem.getCostType();
							invoiceItem.setTotalCost(poItem.getTotalCost());
							invoiceItem.setFinalCost(poItem.getFinalCost());
							invoiceItem.setGeneralDiscountRate(poItem.getGeneralDiscountRate());
							invoiceItem.setGeneralDiscountValue(poItem.getGeneralDiscountValue());
							invoiceItem.setQuantity(poItem.getQuantity());
							invoiceItem.setNominal(nominalCodeService.findBestMatchingNominalCode(ledgers,
									costType.getTypeid(), linkedJobItem.getJi()));
						}
						taxCalculator.calculateTax(invoice);
						CostCalculator.updatePricingFinalCost(invoice, invoice.getItems());
					}
					insertInvoice(invoice);
					filteredPOItems.forEach(poi -> {
						poi.setInternalInvoice(invoice);
						purchaseOrderItemService.updatePurchaseOrderItem(poi, autoUser);
					});
					invoices.add(invoice);
				}
			}
		}
		return invoices;
	}

	@Override
	public List<CompanyReadyToBeInvoiced> getAllCompaniesWithJobsReadyToInvoice(Company allocatedCompany) {
		return this.invoiceDao.getAllCompaniesWithJobsReadyToInvoice(allocatedCompany);
	}

	@Override
	public List<InvoiceDTO> getPeriodicInvoiceDTO(Company company, Company allocatedCompany, Integer recentYears) {
		InvoiceType type = invoiceTypeService.findInvoiceTypeByName(InvoiceTypeNames.PERIODIC);
		val past = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).minusYears(recentYears).withMonth(1)
				.withDayOfMonth(1);
		return invoiceDao.getRecentInvoiceKeyValues(company, allocatedCompany, type, past);
	}

	public List<InvoiceProjectionDTO> getProjectionDtos(Collection<Integer> invoiceIds) {
		List<InvoiceProjectionDTO> result = Collections.emptyList();
		if (invoiceIds != null && !invoiceIds.isEmpty()) {
			result = this.invoiceDao.getProjectionDtos(invoiceIds);
		}
		return result;
	}

	public List<InvoiceProjectionDTO> getProjectionDtos(Integer allocatedCompanyId, Integer issuedBySubdivId,
			Integer invoiceStatusId, AccountStatus accountStatus) {
		return this.invoiceDao.getProjectionDtos(allocatedCompanyId, issuedBySubdivId, invoiceStatusId, accountStatus);
	}

	@Override
	public Document generateInvoiceDocument(Invoice invoice, String docType, List<String> sessionNewFileList,
			Contact contact, int subdivId, Boolean printDiscount, Boolean printUnitaryPrice, Boolean printQuantity,
			Boolean showAppendix, Boolean summarized, String description, Locale locale) {
		Map<String, Object> model = new HashMap<>();
		model.put("invoice", invoice);
		model.put("invoiceId", invoice.getId());
		model.put("subdivId", subdivId);
		model.put("userId", contact.getId());
		model.put("createdBy", invoice.getBusinessContact().getId());
		model.put("ReportLocale", locale.toString());
		model.put("printDiscount", !summarized && printDiscount);
		model.put("printUnitaryPrice", !summarized && printUnitaryPrice);
		model.put("printQuantity", !summarized && printQuantity);
		model.put("summarized", summarized);
		model.put("description", description);
		model.put("showAppendix", !summarized && showAppendix);

		// Gets the parts for the final cost in letters
		NumberFormat formatter = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);
		BigDecimal totalCost = invoice.getFinalCost();
		String leftFinalCostInLetters = formatter.format(invoice.getFinalCost().intValue());
		String rightFinalCostInLetters = formatter
				.format(totalCost.remainder(BigDecimal.ONE).movePointRight(totalCost.scale()).abs().toBigInteger());

		model.put("totalCost", totalCost);
		model.put("leftFinalCostInLetters", leftFinalCostInLetters);
		model.put("rightFinalCostInLetters", rightFinalCostInLetters);

		// SL : Display the cost in letters only for Marroco ! Dsicussed with
		// Christophe and visibility is based on the currency MAD
		model.put("displayTotalCostInLetter", invoice.getCurrency().getCurrencyCode().equals("MAD"));

		// SL : Display facsimile background only if invoice is not approved
		model.put("facsimile", !(invoice.getStatus().isOnIssue() || invoice.getStatus().getNext() == null));

		// Bank Account
		Company allocatedCompany = invoice.getOrganisation();

		// Get the parts for the header table
		Pair<String, String> splitNumber = numberFormatService.splitNumber(invoice.getInvno(), NumerationType.INVOICE,
				invoice.getOrganisation(), null);
		model.put("invNoLeftPart", splitNumber.getLeft());
		model.put("invNoRightPart", splitNumber.getRight());

		// Add payment terms translation from enum, in the locale of the
		// document being
		// generated
		String paymentTerm = invoice.getPaymentTerm() != null ? invoice.getPaymentTerm().getMessageForLocale(locale)
				: "N/A";
		model.put("paymentTermsTranslation", paymentTerm);

		// try and find a custom template to use for this invoice - company
		// or invoice type specific
		InvoiceDoctypeTemplate itemp = this.invTempServ.findByInvoice(invoice, docType);
		// a null template is fine, the document creation service will just
		// default to the general invoice html template
		Template template = null;
		if (itemp != null)
			template = itemp.getTemplate();
		// render the document

		return this.docServ.createBirtSQLDocument(model, Component.INVOICE, docType, template,
				new InvoiceNamingService(invoice), invoice.getDirectory().getAbsolutePath(), sessionNewFileList,
				allocatedCompany);
	}

	public List<KeyValue<Integer, String>> getPeriodicInvociesOptions(List<Invoice> invoices, Locale locale) {

		return Stream
				.of(Stream.of(new KeyValue<>(0, messages.getMessage("companyedit.na", null, locale))),
						invoices.stream()
								.map(in -> new KeyValue<>(in.getId(),
										in.getInvno() + " " + in.getInvoiceDate().format(DateTimeFormatter.ISO_DATE))))
				.flatMap(Function.identity()).collect(Collectors.toList());
	}

	private Invoice generateInternalInvoice(Company recipient, Company subcontractor, SupportedCurrency currency) {
		logger.info("Generate new internal invoice for " + subcontractor.getConame() + " to " + recipient.getConame());
		Invoice invoice = new Invoice();
		invoice.setType(invoiceTypeService.findInvoiceTypeByName(InvoiceTypeNames.INTERNAL));
		invoice.setOrganisation(subcontractor);
		invoice.setBusinessContact(subcontractor.getDefaultBusinessContact());
		invoice.setPricingType(PricingType.CLIENT);
		invoice.setVatValue(new BigDecimal("0.00"));
		invoice.setTotalCost(new BigDecimal("0.00"));
		invoice.setFinalCost(new BigDecimal("0.00"));
		Address address = addressService.getAllCompanyAddresses(recipient.getCoid(), AddressType.INVOICE, true).stream()
				.findAny().orElse(subcontractor.getDefaultSubdiv().getDefaultAddress());
		invoice.setAddress(address);
		invoice.setComp(recipient);
		Contact autoUser = userService.get(autoUserName).getCon();
		invoice.setCreatedBy(autoUser);
		invoice.setInvoiceDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		invoice.setInvno(numerationService.generateNumber(NumerationType.INVOICE, subcontractor, null));
		invoice.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		invoice.setCurrency(currency);
		invoice.setRate(currency.getDefaultRate());
		invoice.setDuration(1);
		invoice.setStatus(this.statusServ.findStatusByName("Internal invoice", InvoiceStatus.class));
		invoice.setItems(new TreeSet<>(new InvoiceItemComparator()));
		CompanySettingsForAllocatedCompany companySettings = companySettingsService.getByCompany(recipient,
				subcontractor);
		VatRate vatRate = companySettings == null ? null : companySettings.getVatrate();
		if (vatRate == null)
			// Missing configuration of VAT rates -> impossible to create an invoice
			throw new RuntimeException(
					"Please, configure VAT rates for purchasing business company " + recipient.getConame() + "!");
		else {
			invoice.setVatRateEntity(companySettings.getVatrate());
			invoice.setVatRate(companySettings.getVatrate().getRate());
		}
		return invoice;
	}

	private Invoice findPendingInternalInvoice(Company drawer, Company recipient) {
		return invoiceDao.findPendingInternalInvoice(drawer, recipient);
	}

	public Invoice getInternalInvoice(Subdiv jobOwner, Pricing<Company> pricingEntity) {
		Subdiv subcontractor = pricingEntity.getContact().getSub();
		Invoice invoice = findPendingInternalInvoice(subcontractor.getComp(), jobOwner.getComp());
		if (invoice == null)
			invoice = generateInternalInvoice(jobOwner, subcontractor, pricingEntity);
		return invoice;
	}

	private void getNominalFromInvoiceItem(Map<String, NominalInvoiceCostDTO> codes, InvoiceItem item,
			BigDecimal value) {
		if (item.getNominal() != null) {
			BigDecimal nominalCost = value != null ? value : item.getFinalCost();
			if (codes.containsKey(item.getNominal().getCode())) {
				NominalInvoiceCostDTO dto = codes.get(item.getNominal().getCode());
				dto.setCost(dto.getCost().add(nominalCost));
				codes.put(dto.getNominalCode(), dto);
			} else {
				codes.put(item.getNominal().getCode(),
						new NominalInvoiceCostDTO(item.getNominal().getCode(),
								translationService.getCorrectTranslation(item.getNominal().getTitleTranslations(),
										LocaleContextHolder.getLocale()),
								nominalCost));
			}
		}
	}

	private Invoice generateInternalInvoice(Subdiv jobOwner, Subdiv subcontractor, Pricing<Company> pricingEntity) {
		Invoice invoice = generateInternalInvoice(jobOwner.getComp(), subcontractor.getComp(),
				pricingEntity.getCurrency());
		invoice.setRate(pricingEntity.getRate());
		return invoice;
	}

	@Override
	public List<Invoice> getRecentPeriodicInvoices(Company company, Company allocatedCompany, LocalDate date,
			Integer previousYears) {

		if (previousYears == null)
			throw new IllegalArgumentException("previousYears must be specified");

		val startDate = date.minusYears(1).withMonth(1).withDayOfMonth(1);

		InvoiceType type = this.invoiceTypeService.findInvoiceTypeByName(InvoiceTypeNames.PERIODIC);

		return invoiceDao.getInvoicesOfTypeOnAfterDate(company, allocatedCompany, type, startDate);
	}

	@Override
	public BigDecimal getTotalNominalCost(List<NominalInvoiceCostDTO> nominal) {
		BigDecimal total = new BigDecimal("0.00");
		for (NominalInvoiceCostDTO dto : nominal) {
			total = total.add(dto.getCost());
		}
		return total;
	}

	@Override
	public List<NominalInvoiceCostDTO> getNominalInvoiceSummary(int invoiceid) {
		Map<String, NominalInvoiceCostDTO> codes = new LinkedHashMap<>();
		Invoice invoice = this.findInvoice(invoiceid);

		if (invoice.getPricingType().equals(PricingType.SITE)) {
			// slightly strange use case, if the invoice is a site invoice then
			// we use the nominal from item one to assign the total cost of the
			// invoice to
			if (invoice.getItems().size() > 0) {
				this.getNominalFromInvoiceItem(codes, new ArrayList<>(invoice.getItems()).get(0),
						invoice.getTotalCost());
			}
		} else {
			for (InvoiceItem item : invoice.getItems()) {
				if (item.isBreakUpCosts()) {
					if ((item.getAdjustmentCost() != null) && (item.getAdjustmentCost().getNominal() != null)
							&& item.getAdjustmentCost().isActive()) {
						this.addCostToNominalSummary(item.getAdjustmentCost().getNominal(),
								item.getAdjustmentCost().getFinalCost(), codes);
					}
					if ((item.getCalibrationCost() != null) && (item.getCalibrationCost().getNominal() != null)
							&& item.getCalibrationCost().isActive()) {
						this.addCostToNominalSummary(item.getCalibrationCost().getNominal(),
								item.getCalibrationCost().getFinalCost(), codes);
					}
					if ((item.getRepairCost() != null) && (item.getRepairCost().getNominal() != null)
							&& item.getRepairCost().isActive()) {
						this.addCostToNominalSummary(item.getRepairCost().getNominal(),
								item.getRepairCost().getFinalCost(), codes);
					}
					if ((item.getPurchaseCost() != null) && (item.getPurchaseCost().getNominal() != null)
							&& item.getPurchaseCost().isActive()) {
						this.addCostToNominalSummary(item.getPurchaseCost().getNominal(),
								item.getPurchaseCost().getFinalCost(), codes);
					}
					if ((item.getServiceCost() != null) && (item.getServiceCost().getNominal() != null)
							&& item.getServiceCost().isActive()) {
						this.addCostToNominalSummary(item.getServiceCost().getNominal(),
								item.getServiceCost().getFinalCost(), codes);
					}
				} else {
					this.getNominalFromInvoiceItem(codes, item, null);
				}
			}
		}
		List<NominalInvoiceCostDTO> dtos = new ArrayList<>();
		for (String key : codes.keySet()) {
			dtos.add(codes.get(key));
		}
		return dtos;
	}

	@Override
	public Invoice merge(Invoice invoice) {
		return this.invoiceDao.merge(invoice);
	}

	@Override
	public ResultWrapper resetInvoiceAccountStatus(int invoiceid) {
		Invoice invoice = this.findInvoice(invoiceid);

		if (invoice == null) {
			return new ResultWrapper(false,
					this.messages.getMessage("error.invoice.notfound", null, "Invoice could not be found", null));
		} else {
			invoice.setAccountsStatus(AccountStatus.P);
			invoice = this.merge(invoice);
			return new ResultWrapper(true, "", invoice, null);
		}
	}

	@Override
	public PagedResultSet<InvoiceDTO> searchSortedInvoiceDTO(InvoiceHomeForm form, String username,
			PagedResultSet<InvoiceDTO> rs) {
		return this.invoiceDao.searchSortedInvoiceDTO(form, username, rs);
	}

	public void insertInvoice(Invoice invoice) {
		// always set the default financial period for a new invoice
		invoice.setFinancialPeriod(AccountsTools.getFinancialPeriod(invoice.getInvoiceDate()).getPeriod());

		this.invoiceDao.persist(invoice);
		invoice.setDirectory(
				this.compDirServ.getDirectory(this.scServ.findComponent(Component.INVOICE), invoice.getInvno()));

		// if the invoice is added with items already on it (persisted via
		// cascade rules) then add the links to POs
		if (invoice.getItems() != null) {
			for (InvoiceItem ii : invoice.getItems()) {
				this.iipoServ.linkSamePOsAsJobItemOrExpenseItem(ii);
			}
		}
	}

	@Override
	public void updateSummaryDescription(int invoiceId, String description) {
		Invoice invoice = this.findInvoice(invoiceId);
		invoice.setSiteCostingNote(description);
	}

	@Override
	public void updateInvoiceCosts(Invoice invoice) {
		CostCalculator.updatePricingFinalCost(invoice, invoice.getItems());
		taxCalculator.calculateTax(invoice);
		CostCalculator.updatePricingFinalCost(invoice, invoice.getItems());
		this.merge(invoice);
	}

	@Override
	public List<Integer> getJobItemsIdsFromInvoice(Invoice invoice) {
		return invoiceDao.getJobItemsIdsFromInvoice(invoice);
	}

	@Override
	public List<Integer> getJobsIdsFromInvoice(Invoice invoice) {
		return invoiceDao.getJobsIdsFromInvoice(invoice);
	}

	@Override
	public void renumberItems(Invoice invoice) {
		Integer counter = 1;
		for(InvoiceItem item: invoice.getItems()) {
			item.setItemno(counter++);
		}
	}
}