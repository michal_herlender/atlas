package org.trescal.cwms.core.pricing.entity.costs.base;

import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;

public interface WithQuotationCalibrationCost {
    QuotationCalibrationCost getQuotationCalibrationCost();
}
