package org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus_;

@Repository("CreditNoteStatusDao")
public class CreditNoteStatusDaoImpl extends BaseDaoImpl<CreditNoteStatus, Integer> implements CreditNoteStatusDao {
	
	@Override
	protected Class<CreditNoteStatus> getEntity() {
		return CreditNoteStatus.class;
	}

	
	@Override
	public List<CreditNoteStatus> findAllCreditNoteStatusRequiringAttention() {
		return getResultList(cb ->{
			CriteriaQuery<CreditNoteStatus> cq = cb.createQuery(CreditNoteStatus.class);
			Root<CreditNoteStatus> root = cq.from(CreditNoteStatus.class);
			cq.where(cb.isTrue(root.get(CreditNoteStatus_.requiresAttention)));
			return cq;
		});
	}
	
	@Override
	public CreditNoteStatus findOnInsertActivity() {
		return getFirstResult(cb ->{
			CriteriaQuery<CreditNoteStatus> cq = cb.createQuery(CreditNoteStatus.class);
			Root<CreditNoteStatus> root = cq.from(CreditNoteStatus.class);
			cq.where(cb.isTrue(root.get(CreditNoteStatus_.onInsert)));
			return cq;
		}).orElse(null);
	}
}