package org.trescal.cwms.core.pricing.invoice.dto;

import org.trescal.cwms.core.company.entity.company.Company;

public class CompanyReadyToBeInvoiced
{
	private Long completeJobs;
	private Company company;

	public CompanyReadyToBeInvoiced(Company company, Long completeJobs)
	{
		this.completeJobs = completeJobs;
		this.company = company;
	}

	public Company getCompany()
	{
		return this.company;
	}

	public Long getCompleteJobs()
	{
		return this.completeJobs;
	}

	public void setCompany(Company company)
	{
		this.company = company;
	}

	public void setCompleteJobs(Long completeJobs)
	{
		this.completeJobs = completeJobs;
	}

}
