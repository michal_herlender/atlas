package org.trescal.cwms.core.pricing.creditnote.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;

@Getter
@Setter
public class CreditNoteHomeForm {
	private Integer orgId;
	private Integer compId;
	private String creditNoteNo;
	private LocalDate date;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	private String invNo;
	private int pageNo;
	private int resultsPerPage;
	private PagedResultSet<CreditNoteDTO> rs;

}
