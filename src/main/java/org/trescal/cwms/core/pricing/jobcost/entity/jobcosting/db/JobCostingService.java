package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.TotalDiscountCalculator;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.dto.JobCostingDTO;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.form.JobCostingForm;
import org.trescal.cwms.core.tools.filebrowser.FileWrapper;

public interface JobCostingService extends BaseService<JobCosting, Integer>
{
	ResultWrapper ajaxIssueJobCosting(int jobCostingId, HttpSession session);
	
	boolean costingRequiresIssue(JobCosting costing);
	
	/**
	 * Loads a jobcosting and it's associated items eagerly - intended for ajax
	 * calls.
	 * 
	 * @param id the {@link JobCosting} id.
	 * @return {@link ResultWrapper} with the {@link JobCosting} set as the
	 *         result.
	 */
	ResultWrapper findAjaxEagerJobCosting(int id);
	
	/**
	 * Gets the current highest version number that of any existing
	 * {@link JobCosting}s for the given {@link Job} or 0 if none exist.
	 * 
	 * @param jobid the id of the {@link Job}.
	 * @return the number.
	 */
	int findCurrentMaxVersion(int jobid);
	
	/**
	 * Loads a jobcosting, it's associated costing items and their model and
	 * instrumetmodel infromation eagerly.
	 * 
	 * @param id the id of the {@link JobCosting}.
	 * @return the {@link JobCosting}.
	 */
	JobCosting findEagerJobCosting(int id);
	
	JobCosting findJobCosting(int id);
	
	boolean getAvalaraAware();
	
	// Intentionally exposed for integration testing of documents
	void setAvalaraAware(boolean avalaraAware);
	
	/**
	 * Calculates and returns a {@link JobCosting} total discount.
	 * 
	 * @param items the {@link JobCostingItem}s beloging to the
	 *        {@link JobCosting}.
	 * @return {@link TotalDiscountCalculator} object containing the derived
	 *         discount total.
	 */
	public TotalDiscountCalculator getJobCostingTotalDiscount(Set<JobCostingItem> items);
	
	public TotalDiscountCalculator getJobCostingTotalDiscount(List<JobCostingItemDto> items);
	
	/**
	 * Issues job costing; may throw OpenActivityExeption (propagated from interceptor) if an item is active 
	 */
	void publishJobCosting(JobCosting jobcosting, Contact currentContact);
	
	void publishJobCosting(JobCosting jobcosting, String fileName, Contact currentContact);
	
	/**
	 * Recalculate the totals of the given {@link JobCosting} and saves the
	 * {@link JobCosting} back to the database.
	 * 
	 * @param jobcosting the {@link JobCosting}.
	 */
	void recalculateAndPersistJobCostingTotals(JobCosting jobcosting, Contact currentContact);
	
	/**
	 * Required to ensure that currentContact can be known
	 */
	Integer save(JobCosting jc, Contact currentContact);
	void saveOrUpdate(JobCosting jc, Contact currentContact);
	
	List<JobCosting> getJobCostingListByJobitemId(Integer jobitemid);
	
	List<JobCosting> getEagerJobCostingListForJobIds(Collection<Integer> jobids);

	ResultWrapper ajaxIssueJobCostingToAdveso(JobCosting jobCosting, String fileName, String userName);
	
	JobCosting getJobCosting(Integer jobItemId, Integer version);
	
	List<JobCostingDTO> getJobCostingsByJobId(Integer jobId, Locale locale);
	
	void setJobCostingClientResponse(JobCosting jobCosting, JobCostingForm jcForm, Contact currentContact);
	
	FileWrapper getLastFileRevision(JobCosting jobCosting);
	
}