package org.trescal.cwms.core.pricing.invoice.dto;

import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class InvoiceHomeWrapper<Entity> {
	private String id;							// Lower Case English language name for html use (no spaces)
	private String name;						// Localized title for tab (a couple words)
	private String description;					// Localized description of contents (one sentence)
	private Collection<Entity> contents;
	private int quantity;						// Number of "items" to display to user; may differ from contents.size() for summaries 
	private boolean contentsInvoice;			// Indicates that wrapper contains Invoices
	private boolean contentsInvoiceProjection;	// Indicates that wrapper contains Invoice Projection DTOs
	private boolean contentsSummary;			// Indicates that wrapper contains summary of potential invoices	
	
}
