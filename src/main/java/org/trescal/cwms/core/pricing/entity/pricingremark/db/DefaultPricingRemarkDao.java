package org.trescal.cwms.core.pricing.entity.pricingremark.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.entity.pricingremark.DefaultPricingRemark;

public interface DefaultPricingRemarkDao extends BaseDao<DefaultPricingRemark, Integer> {
	/**
	 * Returns all default remarks for the business company,
	 * optionally restricting by locale
	 * @param businessCompanyId (mandatory)
	 * @param locale (optional)
	 * @return
	 */
	List<DefaultPricingRemark> getRemarksForOrganisation(Integer businessCompanyId, Locale locale);
}
