package org.trescal.cwms.core.pricing.invoice.projection.invoiceitem;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import lombok.Getter;

@Getter
public class InvoiceItemProjectionDTO {

	private Integer itemId;
	private Integer itemNumber;
	private String description;
	private Integer quantity;
	private BigDecimal finalCost;
	private Boolean taxable;
	private BigDecimal taxAmount;
	// From join with Nominal Code
	private Integer nominalCodeId;
	private CostType nominalCostType;
	// From join with Job Item
	private Integer jobItemId;
	// From join with Expense Item
	private Integer expenseItemId;
	private Integer expenseItemNumber;
	private Integer expenseItemJobId;
	private Integer expenseItemModelId;
	private String expenseItemClientRef;

	// Constructor used with JPA
	public InvoiceItemProjectionDTO(Integer itemId, Integer itemNumber, String description, Integer quantity, BigDecimal finalCost, 
			Boolean taxable, BigDecimal taxAmount, Integer nominalCodeId, CostType nominalCostType, Integer jobItemId, Integer expenseItemId, Integer expenseItemNumber,
			Integer expenseItemJobId, Integer expenseItemModelId, String expenseItemClientRef) {
		super();
		this.itemId = itemId;
		this.itemNumber = itemNumber;
		this.description = description;
		this.quantity = quantity;
		this.finalCost = finalCost;
		this.taxable = taxable;
		this.taxAmount = taxAmount;
		this.nominalCodeId = nominalCodeId;
		this.nominalCostType = nominalCostType;
		this.jobItemId = jobItemId;
		this.expenseItemId = expenseItemId;
		this.expenseItemNumber = expenseItemNumber;
		this.expenseItemJobId = expenseItemJobId;
		this.expenseItemModelId = expenseItemModelId;
		this.expenseItemClientRef = expenseItemClientRef;
	}
}