package org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPOInvoicingDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPoLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db.InvoiceItemPOService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.projection.invoiceitem.InvoiceItemProjectionDTO;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputInvoiceItem;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service("InvoiceItemService")
public class InvoiceItemServiceImpl extends BaseServiceImpl<InvoiceItem, Integer> implements InvoiceItemService {

	private InvoiceItemPOService invoiceItemPOService;
	private InvoiceItemDao invoiceItemDao;
	private InvoiceService invoiceService;

	public InvoiceItemServiceImpl(InvoiceItemPOService invoiceItemPOService, InvoiceItemDao invoiceItemDao, InvoiceService invoiceService) {
		this.invoiceItemPOService = invoiceItemPOService;
		this.invoiceItemDao = invoiceItemDao;
		this.invoiceService = invoiceService;
	}

	@Override
	protected BaseDao<InvoiceItem, Integer> getBaseDao() {
		return invoiceItemDao;
	}

	@Override
	public List<InvoiceItemPoLinkDTO> getPoLinks(Integer invoiceId, Integer jobId) {
		return invoiceItemDao.getPoLinks(invoiceId, jobId);
	}

	@Override
	public List<InvoiceItemPoLinkDTO> getPoLinks(Collection<Integer> invoiceIds) {
		return invoiceItemDao.getPoLinks(invoiceIds);
	}

	@Override
	public Invoice removeItemAndRecalculate(Integer id) {
        InvoiceItem invoiceitem = this.get(id);
        Invoice invoice = invoiceitem.getInvoice();
        invoice.getItems().remove(invoiceitem);
        this.invoiceItemDao.remove(invoiceitem);
        this.invoiceService.updateInvoiceCosts(invoice);
		this.invoiceService.renumberItems(invoice);
        return invoice;
    }

	public InvoiceItem findInvoiceItem(int id) {
		return this.invoiceItemDao.find(id);
	}

	public ResultWrapper getAllInvoiceItemsOnInvoiceDWR(int invoiceid, Integer jobid) {
		// make sure invoice exists
		Invoice i = this.invoiceService.findInvoice(invoiceid);
		// check invoice not null?
		if (i != null) {
			// get list of invoice items
			List<InvoiceItem> iiList = this.invoiceItemDao.getAllEagerInvoiceItemsOnInvoice(i.getId(), jobid);
			// return successful result wrapper
			return new ResultWrapper(true, "",  iiList, null);
		} else {
			return new ResultWrapper(false, "The invoice could not be found", null, null);
		}
	}

	public int getNextItemNo(int invoiceid) {
		int result = invoiceItemDao.findMaxItemNoOnInvoice(invoiceid);
		return ++result;
	}

	@Override
	public void save(InvoiceItem invoiceItem) {
		// Link JobItem POs to InvoiceItem (JPA cascade saves new entities)
		this.invoiceItemPOService.linkSamePOsAsJobItemOrExpenseItem(invoiceItem);
		this.invoiceItemDao.persist(invoiceItem);
	}
	
	@Override
	public List<PriceLookupOutputInvoiceItem> findInvoiceItems(Integer plantid, Integer servicetypeid, Integer coid){
		return this.invoiceItemDao.findInvoiceItems(plantid, servicetypeid, coid);
	}

	@Override
	public List<InvoiceItem> getAllByInvoiceAndJob(Integer invoiceId, Integer jobId) {
		return this.invoiceItemDao.getAllByInvoiceAndJob(invoiceId, jobId);
	}

	@Override
	public List<InvoiceItemProjectionDTO> getInvoiceItemProjectionDTOs(Integer invoiceId) {
		if (invoiceId == null) throw new IllegalArgumentException("invoiceId must not be null");
        return this.invoiceItemDao.getInvoiceItemProjectionDTOs(invoiceId);
    }

    @Override
    public List<InvoiceItemPOInvoicingDTO> getInvoiceItemsForInvoice(Integer invoiceId, int poId) {
        if (invoiceId == null) {
            throw new IllegalArgumentException("Invoice id is not found.");
        }
        return this.invoiceItemDao.getInvoiceItemsForInvoice(invoiceId, poId);
    }

    @Override
    public List<InvoicePOItem> getItemsByInvoiceIdAndNotInInvPoAndSelectedIds(Integer invoiceId, InvoicePO po, List<Integer> itemIds) {
        return invoiceItemDao.findByInvoiceIdAndNotByPOIdAndSelectedIds(invoiceId, po.getPoId(), itemIds).stream()
                .map(ii -> InvoicePOItem.builder().invItem(ii).invPO(po).build())
                .collect(Collectors.toList());
    }

    @Override
    public List<InvoicePOItem> getItemsByInvoiceIdAndInInvPoAndNotSelected(Integer invoiceId, Integer poId, List<Integer> itemIds) {
        return invoiceItemDao.findItemsByInvoiceIdAndInInvPoAndNotSelected(invoiceId, poId, itemIds);
    }
}