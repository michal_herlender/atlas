package org.trescal.cwms.core.pricing.invoice.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.pricing.PricingType;

@Component
public class ViewInvoiceValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(InvoiceForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		InvoiceForm form = (InvoiceForm) target;
		if (form.getInvoiceDate() == null)
			errors.rejectValue("invoiceDate", "error.invoice.invoicedate.blank", null,
					"Invoice date cannot be left blank");
		if (form.getAddrid() == null)
			errors.rejectValue("addrid", "error.invoice.addrid.blank", null, "Invoice address cannot be left blank");
		// site invoice specific
		if (form.getPricingType().equals(PricingType.SITE)) {
			if ((form.getTotalCost() == null) || (form.getTotalCost() < 0.0))
				errors.rejectValue("totalCost", "error.invoice.edit.totalcost", null,
						"A valid total cost must be specified");
			if ((form.getSiteCostingNote() != null) && (form.getSiteCostingNote().length() > 1000))
				errors.rejectValue("siteCostingNote", null, "Site invoice notes should be less than 100 chars");
		}
		// Further VAT rate validation not done - need to be able to override for
		// special situations
		if (form.getVatCode() == null)
			errors.rejectValue("vatCode", "error.vatrate.invalid");
	}
}