package org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;

public interface InvoiceStatusDao extends BaseDao<InvoiceStatus, Integer> {
	
	List<InvoiceStatus> findAllInvoiceStatusRequiringAttention();
	
	InvoiceStatus findOnInsertActivity();
}