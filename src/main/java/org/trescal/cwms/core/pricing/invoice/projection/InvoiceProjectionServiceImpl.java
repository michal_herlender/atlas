package org.trescal.cwms.core.pricing.invoice.projection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionResult;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.service.DeliveryItemProjectionService;
import org.trescal.cwms.core.deliverynote.projection.service.DeliveryProjectionService;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionResult;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.db.JobExpenseItemPOServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.ExpenseItemNotInvoicedService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.JobItemNotInvoicedService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemNotInvoicedDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionTools;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionResult;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionService;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceJobLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.db.InvoiceJobLinkService;

@Component
public class InvoiceProjectionServiceImpl implements InvoiceProjectionService {
	@Autowired
	private DeliveryProjectionService dpService;
	@Autowired
	private DeliveryItemProjectionService dipService;
	@Autowired
	private ExpenseItemNotInvoicedService einiService;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private JobExpenseItemPOServiceImpl eipoService;
	@Autowired
	private JobItemNotInvoicedService jiniService;
	@Autowired
	private JobItemProjectionService jipService;
	@Autowired
	private JobProjectionService jpService;
	@Autowired
	private InvoiceJobLinkService invoiceJobLinkService;
	@Autowired
	private CompanyService companyService;
	
	@Override
	public List<InvoiceProjectionDTO> getInvoiceProjectionDTOs(Collection<Integer> invoiceIds, InvoiceProjectionCommand command) {
		List<InvoiceProjectionDTO> invoiceDtos = Collections.emptyList();
		if (!invoiceIds.isEmpty()) {
			invoiceDtos = this.invoiceService.getProjectionDtos(invoiceIds);
			getCompleteReferences(invoiceDtos, command);
		}
		
		return invoiceDtos;
	}

	@Override
	public List<InvoiceProjectionDTO> getInvoiceProjectionDTOs(Integer allocatedCompanyId, Integer issuedBySubdivId, Integer invoiceStatusId, AccountStatus accountStatus, InvoiceProjectionCommand command) {
		List<InvoiceProjectionDTO> invoiceDtos = this.invoiceService.getProjectionDtos(allocatedCompanyId, issuedBySubdivId, invoiceStatusId, accountStatus);
		getCompleteReferences(invoiceDtos, command);
		return invoiceDtos;
	}
	
	@Override
	public void getCompleteReferences(List<InvoiceProjectionDTO> invoiceDtos, InvoiceProjectionCommand command) {
		if (!invoiceDtos.isEmpty()) {
			if (command.getLoadNotInvoicedExpenseItems() || 
					command.getLoadNotInvoicedJobItems() || 
					command.getLoadJobLinks()) {
				Map<Integer, InvoiceProjectionDTO> mapByInvoiceId = invoiceDtos.stream()
						.collect(Collectors.toMap(InvoiceProjectionDTO::getId, dto -> dto)); 
				
				if (command.getLoadNotInvoicedExpenseItems())
					loadNotInvoicedExpenseItems(mapByInvoiceId, command);
				if (command.getLoadNotInvoicedJobItems())
					loadNotInvoicedJobItems(mapByInvoiceId, command);
				if (command.getLoadJobLinks())
					loadJobLinks(mapByInvoiceId);
			}
			if (command.getLoadCompany())
				loadCompany(invoiceDtos);
		}
	}

	private void loadNotInvoicedExpenseItems(Map<Integer, InvoiceProjectionDTO> mapByInvoiceId, InvoiceProjectionCommand invoiceCommand) {
		JobProjectionCommand jobCommand = invoiceCommand.getNotInvoicedJobCommand();
		
		Set<Integer> invoiceIds = mapByInvoiceId.keySet();
		List<JobExpenseItemNotInvoicedDTO> einiDtos = einiService.getProjectionDTOs(invoiceIds, invoiceCommand.getLocale());
		Map<Integer, JobExpenseItemProjectionDTO> mapExpenseItemById = new HashMap<>();
		for (JobExpenseItemNotInvoicedDTO dto : einiDtos) {
			InvoiceProjectionDTO invoiceDto = mapByInvoiceId.get(dto.getPeriodicInvoiceId());
			dto.setInvoice(invoiceDto);
			if (invoiceDto.getPeriodicExpenseItems() == null)
				invoiceDto.setPeriodicExpenseItems(new ArrayList<>());
			invoiceDto.getPeriodicExpenseItems().add(dto);
			mapExpenseItemById.put(dto.getExpenseItem().getId(), dto.getExpenseItem());
		}
		
		// Load client PO detail for not invoiced items?
		if (invoiceCommand.getLoadNotInvoicedExpenseItemClientPOs()) {
			List<ClientPurchaseOrderProjectionDTO> eipoDtos = this.eipoService.getProjectionDTOs(invoiceIds);
			for (ClientPurchaseOrderProjectionDTO eipoDto : eipoDtos) {
				JobExpenseItemProjectionDTO eiDto = mapExpenseItemById.get(eipoDto.getItemId());
				if (eiDto.getExpenseItemPos() == null)
					eiDto.setExpenseItemPos(new ArrayList<>());
				eiDto.getExpenseItemPos().add(eipoDto);
			}
		}
		
		// Load job detail for not invoiced items?
		if (jobCommand != null) {
			MultiValuedMap<Integer, JobExpenseItemNotInvoicedDTO> mapByJobId = new HashSetValuedHashMap<>();
			einiDtos.forEach(eini -> mapByJobId.put(eini.getExpenseItem().getJobId(), eini));
			Set<Integer> jobIds = mapByJobId.keySet();
			JobProjectionResult jobResult = jpService.getDTOsByJobIds(jobIds, jobCommand);
			for (JobProjectionDTO jobDto : jobResult.getJobDtos()) {
				mapByJobId.get(jobDto.getJobid()).forEach(eini -> eini.getExpenseItem().setJob(jobDto));
			}
		}
		
	}

	private void loadNotInvoicedJobItems(Map<Integer, InvoiceProjectionDTO> mapByInvoiceId, InvoiceProjectionCommand invoiceCommand) {
		
		JobItemProjectionCommand jobItemCommand = invoiceCommand.getNotInvoicedJobItemCommand();
		JobProjectionCommand jobCommand = invoiceCommand.getNotInvoicedJobCommand();
		
		Set<Integer> invoiceIds = mapByInvoiceId.keySet();
		List<JobItemNotInvoicedDTO> jiniDtos = jiniService.getProjectionDTOs(invoiceIds);
		for (JobItemNotInvoicedDTO jiniDto : jiniDtos) {
			InvoiceProjectionDTO invoiceDto = mapByInvoiceId.get(jiniDto.getPeriodicInvoiceId());
			jiniDto.setInvoice(invoiceDto);
			if (invoiceDto.getPeriodicJobItems() == null)
				invoiceDto.setPeriodicJobItems(new ArrayList<>());
			invoiceDto.getPeriodicJobItems().add(jiniDto);
		}
		
		// Load job item detail for not invoiced items?
		if (jobItemCommand != null) {
			// Unique constraint on job-item-not-invoiced, so at max one DTO per job item
			Map<Integer, JobItemNotInvoicedDTO> mapByJobItemId = jiniDtos.stream()
				.collect(Collectors.toMap(JobItemNotInvoicedDTO::getJobItemId, dto -> dto));
			Set<Integer> jobItemIds = mapByJobItemId.keySet();
			
			JobItemProjectionResult jipResult = this.jipService.getProjectionDTOsForJobItemIds(jobItemIds, jobItemCommand);
			List<JobItemProjectionDTO> jiDtos = jipResult.getJiDtos();
			for (JobItemProjectionDTO jiDto : jiDtos) {
				JobItemNotInvoicedDTO jiniDto = mapByJobItemId.get(jiDto.getJobItemId());
				jiniDto.setJobItem(jiDto);
			}
			
			// Check if to also load jobs into job items
			if (jobCommand != null) {
				jpService.getDTOsByJobItems(jiDtos, jobCommand);
			}
			// Check if to also load delivery detail (items and deliveries) into job items
			if (invoiceCommand.getLoadNotInvoicedJobItemDeliveries()) {
				DeliveryItemProjectionResult deliveryResult = dipService.getDTOsByJobItems(jiDtos);
				List<DeliveryItemDTO> diDtos = deliveryResult.getDeliveryItems();
				DeliveryProjectionCommand dpCommand = new DeliveryProjectionCommand(null);
				dpService.getDtosByDeliveryItems(diDtos, dpCommand);
			}
			
			// Check if estimated prices to be calculated on all job items
			if (invoiceCommand.getCalcNotInvoicedJobItemEstimates()) {
				JobItemProjectionTools.setEstimatedPrices(jiDtos);
			}
		}
	}
	
	private void loadJobLinks(Map<Integer, InvoiceProjectionDTO> mapByInvoiceId) {
		Collection<Integer> invoiceIds = mapByInvoiceId.keySet();
		// Possibility of > 2000 invoices in snapshot; if so just load first 2000 (IN clause restriction).
		if (invoiceIds.size() > 2000) {
			Integer[] array = invoiceIds.toArray(new Integer[0]);
			invoiceIds = Arrays.asList(array).subList(0, 2000);
		}
		
		List<InvoiceJobLinkDTO> linkDtos = this.invoiceJobLinkService.getLinksByIds(invoiceIds);
		for (InvoiceJobLinkDTO linkDto : linkDtos) {
			InvoiceProjectionDTO invoiceDto = mapByInvoiceId.get(linkDto.getInvoiceid());
			if (invoiceDto.getInvoiceJobLinks() == null)
				invoiceDto.setInvoiceJobLinks(new ArrayList<>());
			invoiceDto.getInvoiceJobLinks().add(linkDto);
		}
	}
	
	private void loadCompany(List<InvoiceProjectionDTO> invoiceDtos) {
		Integer allocatedCompanyId = invoiceDtos.get(0).getAllocatedCompanyId();
		
		Map<Integer, List<InvoiceProjectionDTO>> mapByCompanyId = 
				invoiceDtos.stream().collect(Collectors.groupingBy(InvoiceProjectionDTO::getCompanyId));
		Set<Integer> companyIds = mapByCompanyId.keySet();
		List<CompanyProjectionDTO> companyDtos = this.companyService.getDTOList(companyIds, allocatedCompanyId);
		for (CompanyProjectionDTO companyDto : companyDtos) {
			List<InvoiceProjectionDTO> invoiceDtosForCompany = mapByCompanyId.get(companyDto.getCoid());
			for (InvoiceProjectionDTO invoiceDto : invoiceDtosForCompany) {
				invoiceDto.setCompany(companyDto);
			}
		}
	}
}
