package org.trescal.cwms.core.pricing.purchaseorder.form;

import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemProgressDTO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Setter
public class EditPurchaseOrderItemForm
{
	private Integer orderId;
	private Integer itemId;
	private PurchaseOrderItemProgressDTO progress;
	private Integer costTypeId;
	private Integer nominalId;
	private Integer businessSubdivId;
	private boolean newPoItem;
	private Integer jobItemId;
	private boolean saveComments;
	private String description;
	private LocalDate deliveryDate;
	private Integer quantity;
	private BigDecimal totalCost;
	private BigDecimal generalDiscountRate;

	public static final String ERROR_CODE_VALUE_NOT_SELECTED = "{error.value.notselected}";

	public Integer getCostTypeId() {
		return this.costTypeId;
	}

	public Integer getJobItemId()
	{
		return this.jobItemId;
	}

	public Integer getNominalId()
	{
		return this.nominalId;
	}

	public PurchaseOrderItemProgressDTO getProgress()
	{
		return this.progress;
	}

	@Min(value=1, message=ERROR_CODE_VALUE_NOT_SELECTED)
	public Integer getBusinessSubdivId() {
		return businessSubdivId;
	}

	public boolean isNewPoItem()
	{
		return this.newPoItem;
	}

	public boolean isSaveComments()
	{
		return this.saveComments;
	}

	@NotNull
	@Length(max = 1000)
	public String getDescription() {
		return description;
	}

	@NotNull
	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	@NotNull
	@Min(value = 1)
	public Integer getQuantity() {
		return quantity;
	}

	@NotNull
	public BigDecimal getTotalCost() {
		return totalCost;
	}

	@NotNull
	public BigDecimal getGeneralDiscountRate() {
		return generalDiscountRate;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public Integer getItemId() {
		return itemId;
	}

}
