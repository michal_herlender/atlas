package org.trescal.cwms.core.pricing.lookup.dto;

import java.util.Comparator;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides comparator where the "best match" is given the highest value
 * 
 * Comparator is intended to compare just the "output" fields, and
 * is presumed to be normally used in comparing outputs for the same "input" criteria
 *
 */
public class PriceLookupOutputComparator implements Comparator<PriceLookupOutput> {

	private Logger logger = LoggerFactory.getLogger(PriceLookupOutputComparator.class);
	
	@Override
	public int compare(PriceLookupOutput o1, PriceLookupOutput o2) {
		if (o1.getQueryType() == null) 
			logger.error("o1 query type was null!");
		if (o2.getQueryType() == null)
			logger.error("o2 query type was null!");
		int result = o1.getPreferredQuotation().compareTo(o2.getPreferredQuotation());
		if (result == 0) {
			result = o1.getQueryType().compareTo(o2.getQueryType());
			if (result == 0) {
				switch (o1.getQueryType()) {
				case QUOTATION_ITEM_INSTRUMENT:
				case QUOTATION_ITEM_INSTRUMENT_MODEL:
				case QUOTATION_ITEM_SALES_CATEGORY_MODEL:
					// Null safe comparison; null issued date (if it happens) is less preferred
					result = ObjectUtils.compare(o1.getQuotationIssuedDate(), o2.getQuotationIssuedDate());
					if (result == 0) {
						result = o1.getQuotationItemId().compareTo(o2.getQuotationItemId());
					}
					break;
					
				case CATALOG_PRICE_INSTRUMENT_MODEL:
				case CATALOG_PRICE_SALES_CATEGORY_MODEL:
					result = o1.getCatalogPriceId().compareTo(o2.getCatalogPriceId());
					break;

				default:
					logger.error("Unexpected query type "+o1.getQueryType());
					break;
				}
			}
		}
		return result;
	}

}
