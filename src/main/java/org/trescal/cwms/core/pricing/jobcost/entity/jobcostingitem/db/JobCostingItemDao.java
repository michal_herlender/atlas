package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.projection.JobCostingItemProjectionDTO;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobCostingItemDTO;

public interface JobCostingItemDao extends BaseDao<JobCostingItem, Integer> {

	void delete(Collection<JobCostingItem> items);

	JobCostingItem findMostRecentCostingForJobitem(int jobitemid);

	List<AdvesoJobCostingItemDTO> findIssuedJobCostingItemByJobItemID(int jobitemid);
	
	List<JobCostingItemDto> getJobCotingItemsDtoByJobCostingId(Integer jobCostingId, Locale locale);
	
	List<JobCostingItemProjectionDTO> getProjectionDTOsForJobItems(Collection<Integer> jobItemIds);

}