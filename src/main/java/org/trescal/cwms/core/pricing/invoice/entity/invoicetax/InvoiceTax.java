package org.trescal.cwms.core.pricing.invoice.entity.invoicetax;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.trescal.cwms.core.pricing.entity.tax.PricingTax;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

@Entity
@Table(name = "invoicetax")
@AssociationOverrides({
		@AssociationOverride(name = "pricing", joinColumns = @JoinColumn(name = "invoiceid"), foreignKey = @ForeignKey(name = "FK_invoicetax_invoice")),
		@AssociationOverride(name = "vatRate", joinColumns = @JoinColumn(name = "vatrateid"), foreignKey = @ForeignKey(name = "FK_invoicetax_vatrate")) })
public class InvoiceTax extends PricingTax<Invoice> {
}