package org.trescal.cwms.core.pricing.entity.pricingremark.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.admin.form.DefaultPricingRemarksForm;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.pricing.entity.pricingremark.DefaultPricingRemark;

public interface DefaultPricingRemarkService extends BaseService<DefaultPricingRemark, Integer> {
	/**
	 * Returns all default remarks for the business company,
	 * optionally restricting by locale
	 * @param businessCompanyId (mandatory)
	 * @param locale (optional)
	 * @return
	 */
	List<DefaultPricingRemark> getRemarksForOrganisation(Integer businessCompanyId, Locale locale);
	
	void perFormEdit(DefaultPricingRemarksForm form);
}
