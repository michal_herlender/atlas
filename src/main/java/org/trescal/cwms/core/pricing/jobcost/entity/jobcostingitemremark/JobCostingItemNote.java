package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "jobcostingitemnote")
public class JobCostingItemNote extends Note implements WithJobCostingItem
{
	private JobCostingItem jobCostingItem;
	
	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostingitemid")
	public JobCostingItem getJobCostingItem() {
		return this.jobCostingItem;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.JOBCOSTINGITEMNOTE;
	}
	
	public void setJobCostingItem(JobCostingItem jobCostingItem) {
		this.jobCostingItem = jobCostingItem;
	}
}