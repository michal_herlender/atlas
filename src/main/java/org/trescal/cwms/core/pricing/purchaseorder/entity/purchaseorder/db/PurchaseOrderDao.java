package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.purchaseorder.dto.AjaxPurchaseOrderItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.SupplierPOProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderHomeForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

public interface PurchaseOrderDao extends BaseDao<PurchaseOrder, Integer>
{
	List<PurchaseOrderDTO> findAllByJob(Job job);
	
	PurchaseOrder findByNumber(String poNo, Integer orgId);
	
	List<PurchaseOrderDTO> findMostRecent(Company allocatedCompany, Integer maxResults);
	
	List<PurchaseOrder> findAllPOsRequiringAttention();
	
	List<PurchaseOrder> findAll(Job job);
	
	List<PurchaseOrder> getAllPurchaseOrdersAtStatus(PurchaseOrderStatus status, Company allocatedCompany);
	
	List<PurchaseOrder> getByAccountStatus(Company businessCompany, AccountStatus accStatus);
	
	PurchaseOrder getPOByExactPONo(String poNo);
	
	void linkJobItemToPO(PurchaseOrderJobItem poji);
	
	PagedResultSet<PurchaseOrder> searchSortedPurchaseOrder(PurchaseOrderHomeForm form, PagedResultSet<PurchaseOrder> rs);
	
	List<AjaxPurchaseOrderItemDTO> preparePurchaseOrderItem(int jobid, StateGroup type, Locale locale);
	
	List<SupplierPOProjectionDTO> getByAccountStatusUsingProjection(Integer businessCompanyId,Integer subdivId, AccountStatus accStatus);
	
	List<SupplierPOProjectionDTO> getAllPurchaseOrdersAtStatusUsingProjection(Integer statusId, Integer alloactedCompanyId,Integer subdivId);
	
	List<PurchaseOrder> getAllPurchaseOrdersAtStatus(Integer statusId, Integer allocatedCompanyId);
	
	List<Integer> getJobItemsIdsFromPurchaseOrder(PurchaseOrder purchaseOrder);
	
	List<PurchaseOrderDTO> getDtosByJobid(Integer jobId,Locale locale);
	
	Long countPoPerJob(Integer jobId,Locale locale);
	
	List<InvoiceItem> getInvoiceItemsWithPos(Integer invoiceid, List<Integer> bpos);
	
}