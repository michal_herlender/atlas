package org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.db;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewLinkedAdjustmentCost;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostServiceSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingLinkedAdjustmentCost;
import org.springframework.beans.factory.annotation.Value;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.tools.MathTools;

@Service("JobCostingAdjustmentCostServiceImpl")
public class JobCostingAdjustmentCostServiceImpl extends JobCostingCostServiceSupport<JobCostingAdjustmentCost>
    implements JobCostingAdjustmentCostService {
    @Autowired
    private JobCostingAdjustmentCostDao JobCostingAdjustmentCostDao;
    @Value("${cwms.config.costing.costs.lookuphierarchy.adjustment}")
    private String costSourceHierarchy;
    @Value("${cwms.config.costing.costs.lookupcosts.adjustment}")
    private boolean lookupCosts;
    @Value("${cwms.config.costing.costs.deactivateonzero.adjustment}")
    protected boolean deactivateCostOnZeroValue;
    @Autowired
    private FaultReportService faultReportService;

    @Override
    public boolean getLookupCosts() {
        return lookupCosts;
    }

    @Override
    public boolean getDeactivateCostOnZeroValue() {
        return deactivateCostOnZeroValue;
    }

    @Override
    public String getCostSourceHierarchy() {
        return costSourceHierarchy;
    }

    @Override
    public void setCostSourceHierarchy(String costSourceHierarchy) {
        this.costSourceHierarchy = costSourceHierarchy;
    }

	@Override
	public Cost createCostFromCopy(JobCostingItem ji, JobCostingItem old) {
		JobCostingAdjustmentCost newCost = null;

		if ((old != null) && (old.getAdjustmentCost() != null)) {
			newCost = new JobCostingAdjustmentCost();

			JobCostingAdjustmentCost oldCost = old.getAdjustmentCost();
			BeanUtils.copyProperties(oldCost, newCost);

			newCost.setCostid(null);
			newCost.setJobCostingItem(ji);
			newCost.setAutoSet(true);

			if (oldCost.getLinkedCost() != null) {
				JobCostingLinkedAdjustmentCost oldLinkedCost = oldCost.getLinkedCost();
				JobCostingLinkedAdjustmentCost newLinkedCost = new JobCostingLinkedAdjustmentCost();
				BeanUtils.copyProperties(oldLinkedCost, newLinkedCost);
				newLinkedCost.setId(0);
				newLinkedCost.setAdjustmentCost(newCost);
				newCost.setLinkedCost(newLinkedCost);
			}
		}
		return newCost;
	}

	public void deleteJobCostingAdjustmentCost(JobCostingAdjustmentCost jobcostingadjustmentcost) {
		this.JobCostingAdjustmentCostDao.remove(jobcostingadjustmentcost);
	}

	public JobCostingAdjustmentCost findJobCostingAdjustmentCost(int id) {
		return this.JobCostingAdjustmentCostDao.find(id);
	}

	@Override
	public List<JobCostingAdjustmentCost> findMatchingCosts(Integer plantid, Integer coid, int modelid,
			Integer caltypeid, Integer page, Integer resPerPage) {
		return this.JobCostingAdjustmentCostDao.findMatchingCosts(JobCostingAdjustmentCost.class, plantid, coid,
				modelid, caltypeid, this.resultsYearFilter, page, resPerPage);
	}

	public List<JobCostingAdjustmentCost> getAllJobCostingAdjustmentCosts() {
		return this.JobCostingAdjustmentCostDao.findAll();
	}

	public void insertJobCostingAdjustmentCost(JobCostingAdjustmentCost JobCostingAdjustmentCost) {
		this.JobCostingAdjustmentCostDao.persist(JobCostingAdjustmentCost);
	}

	@Override
	public Cost resolveCostLookup(CostSource source, JobCostingItem jci) {
		JobCostingAdjustmentCost cost = null;

		boolean costFound = false;

		this.logger.info("loading job costing adjustment costs from " + source);
		switch (source) {
		case CONTRACT_REVIEW:
			this.logger.info("Attempting to set costs using " + source + " strategy");

			JobItem ji = jci.getJobItem();
			if (ji.getAdjustmentCost() != null) {
				ContractReviewAdjustmentCost adjCost = ji.getAdjustmentCost();
				cost = new JobCostingAdjustmentCost();

				cost.setCostSrc(adjCost.getCostSrc());

				// work out the hourly rate to use for this item
				BigDecimal rate = ji.getJob().getOrganisation().getComp().getBusinessSettings().getHourlyRate();
				cost.setHourlyRate(rate == null ? new BigDecimal("0.00") : rate);
				this.logger.info("Hourly rate set as " + cost.getHourlyRate());
				cost.setLabourTime(0);
				cost.setLabourCostTotal(new BigDecimal(0.00));

				// set empty third party costs
				this.configureTPCosts(cost);

				// use the costs set at contract review
				cost.setDiscountRate(adjCost.getDiscountRate());
				cost.setDiscountValue(adjCost.getDiscountValue());
				cost.setHouseCost(adjCost.getTotalCost());
				cost.setTotalCost(adjCost.getTotalCost());
				cost.setFinalCost(adjCost.getFinalCost());
				costFound = true;

				// if a linked cost is being used, link the contract review
				// linked cost to the jobcosting linked cost
				if (adjCost.getLinkedCost() != null) {
					ContractReviewLinkedAdjustmentCost cr = adjCost.getLinkedCost();

					JobCostingLinkedAdjustmentCost lc = new JobCostingLinkedAdjustmentCost();
					lc.setAdjustmentCost(cost);
					if (adjCost.getCostSrc() == CostSource.JOB_COSTING) {
						lc.setJobCostingAdjustmentCost(cr.getJobCostingAdjustmentCost());
					}
				}
			} else {
				this.logger.info("Failed setting costs using " + source + " strategy, no costs found");
			}
			break;
		case JOB:
			this.logger.info(source + " costs are not yet implemented");
			break;
		case JOB_COSTING:
			this.logger.info(source + " costs are not yet implemented");
			break;
		case QUOTATION:
			this.logger.info(source + " costs are not supported");
			break;
		case INSTRUMENT:
			this.logger.info(source + " costs are not supported");
			break;
		case MODEL:
			this.logger.info(source + " costs are not supported");
			break;
		case DERIVED:
			this.logger.info("Attempting to set costs using " + source + " strategy");

			ji = jci.getJobItem();
			FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);
			
			// this function looks for a fault report for the jobitem and
			// calculates a adjustment cost based on this. If no fault
			// report
			// is found then a 'no cost' is found and a manual cost will be
			// loaded instead.
			if (fr != null) {
				// try and calculate repair costs using a fault report entry
				cost = new JobCostingAdjustmentCost();
				cost.setDiscountRate(new BigDecimal(0.00));
				cost.setDiscountValue(new BigDecimal(0.00));

				// set the labour time
				this.logger.info("Setting adjustment time from fault report [" + fr.getEstAdjustTime()
						+ "] minutes");
				cost.setLabourTime(
						fr.getEstAdjustTime() == null ? 0 : fr.getEstAdjustTime());

				BigDecimal rate = ji.getJob().getOrganisation().getComp().getBusinessSettings().getHourlyRate();
				cost.setHourlyRate(rate == null ? new BigDecimal("0.00") : rate);
				this.logger.info("Hourly rate set as " + cost.getHourlyRate());

				// calculate labour cost (labour time * hourly rate)
				BigDecimal labTimeMins = new BigDecimal(cost.getLabourTime()).divide(new BigDecimal("60"),
						MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
				BigDecimal labCost = cost.getHourlyRate().multiply(labTimeMins);
				cost.setLabourCostTotal(labCost.setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));

				// set empty third party costs
				this.configureTPCosts(cost);

				cost.setHouseCost(cost.getLabourCostTotal());
				cost.setTotalCost(cost.getLabourCostTotal());
				cost.setFinalCost(cost.getTotalCost());
				cost.setCostSrc(source);
				costFound = true;
			} else {
				this.logger.info("No fault report found for jobitem, returning no cost from " + source);
				costFound = false;
			}
			break;
		case MANUAL:
			cost = new JobCostingAdjustmentCost();

			// work out the default hourly rate to use for this item
			ji = jci.getJobItem();
			BigDecimal rate = ji.getJob().getOrganisation().getComp().getBusinessSettings().getHourlyRate();
			cost.setHourlyRate(rate == null ? new BigDecimal("0.00") : rate);
			this.logger.info("Hourly rate set as " + cost.getHourlyRate());
			cost.setLabourCostTotal(new BigDecimal(0.00));
			cost.setLabourTime(0);

			// set empty third party costs
			this.configureTPCosts(cost);

			cost.setDiscountRate(new BigDecimal(0.00));
			cost.setDiscountValue(new BigDecimal(0.00));
			cost.setHouseCost(new BigDecimal(0.00));
			cost.setFinalCost(new BigDecimal(0.00));
			cost.setTotalCost(new BigDecimal(0.00));
			cost.setCostSrc(CostSource.MANUAL);
			costFound = true;
			break;
		default:
			break;
		}
		if (costFound) {
			cost.setActive(true);
			cost.setAutoSet(true);
			cost.setJobCostingItem(jci);
			cost.setCostType(CostType.ADJUSTMENT);
		}
		return cost;
	}

	public void saveOrUpdateJobCostingAdjustmentCost(JobCostingAdjustmentCost jobcostingadjustmentcost) {
		this.JobCostingAdjustmentCostDao.saveOrUpdate(jobcostingadjustmentcost);
	}

	public void setJobCostingAdjustmentCostDao(JobCostingAdjustmentCostDao JobCostingAdjustmentCostDao) {
		this.JobCostingAdjustmentCostDao = JobCostingAdjustmentCostDao;
	}

	@Override
	public JobCostingAdjustmentCost updateCosts(JobCostingAdjustmentCost cost) {
		if (cost.getLabourTime() == null)
			cost.setLabourTime(0);
		if (cost.getHourlyRate() == null)
			cost.setHourlyRate(new BigDecimal("0.00"));
		if (cost.getThirdCostTotal() == null)
			cost.setThirdCostTotal(new BigDecimal("0.00"));
		if (cost.getFixedPrice() == null)
			cost.setFixedPrice(new BigDecimal("0.00"));

		// calculate labour cost (labour time * hourly rate) - we assume that
		// the labour cost + hourly rate have already been set on the cost
		BigDecimal labTimeMins = new BigDecimal(cost.getLabourTime()).divide(new BigDecimal("60"),
				MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
		BigDecimal labCost = cost.getHourlyRate().multiply(labTimeMins).add(cost.getFixedPrice());
		cost.setLabourCostTotal(labCost.setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));

		cost.setHouseCost(cost.getLabourCostTotal());

		// calculate TP costs
		cost = (JobCostingAdjustmentCost) this.thirdCostService.recalculateThirdPartyCosts(cost);

		cost.setTotalCost(cost.getHouseCost().add(cost.getThirdCostTotal()));

		// delegate to CostCalculator to apply any discounts and set the final
		// cost
		CostCalculator.updateSingleCost(cost, this.roundUpFinalCosts);

		return cost;
	}

	public void updateJobCostingAdjustmentCost(JobCostingAdjustmentCost JobCostingAdjustmentCost) {
		this.JobCostingAdjustmentCostDao.update(JobCostingAdjustmentCost);
	}
}