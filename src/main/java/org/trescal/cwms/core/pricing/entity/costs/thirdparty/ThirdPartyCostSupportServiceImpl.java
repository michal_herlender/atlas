package org.trescal.cwms.core.pricing.entity.costs.thirdparty;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeAction;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeWrapper;
import org.trescal.cwms.core.system.entity.markups.markuprange.db.MarkupRangeService;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;
import org.trescal.cwms.core.tools.MathTools;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;

public class ThirdPartyCostSupportServiceImpl implements ThirdPartyCostSupportService
{
	private MarkupRangeService markupRangeServ;

	@Override
	public ThirdPartyCostSupport cleanThirdPartyCosts(ThirdPartyCostSupport cost)
	{
		if (cost.getThirdCostSrc() == null)
		{
			cost.setThirdCostSrc(ThirdCostSource.MANUAL);
		}
		if (cost.getThirdManualPrice() == null)
		{
			cost.setThirdManualPrice(new BigDecimal("0.00"));
		}
		if (cost.getThirdMarkupValue() == null)
		{
			cost.setThirdMarkupValue(new BigDecimal("0.00"));
		}
		if (cost.getThirdMarkupRate() == null)
		{
			cost.setThirdMarkupRate(new BigDecimal("0.00"));
		}
		return cost;
	}

	@Override
	public ThirdPartyCostSupport recalculateThirdPartyCosts(ThirdPartyCostSupport c)
	{
		// clean up any null values
		c = this.cleanThirdPartyCosts(c);

		// do the third party calculations
		BigDecimal thirdPriceTotal = ((c.getThirdCostSrc() == ThirdCostSource.MANUAL) || (c.getLinkedCostSrc() == null)) ? c.getThirdManualPrice() : c.getLinkedCostSrc().getFinalCost();

		/* START JAMIE EDIT */
		if ((c.getLinkedCostSrc() != null)
				&& (c.getLinkedCostSrc() instanceof TPQuotationCalibrationCost)
				&& (c instanceof QuotationCalibrationCost))
		{
			TPQuotationCalibrationCost tpqcc = (TPQuotationCalibrationCost) c.getLinkedCostSrc();
			QuotationCalibrationCost qcc = (QuotationCalibrationCost) c;

			if (tpqcc.getTpQuoteItem().getTpquotation().getCurrency().getCurrencyId() != qcc.getQuoteItem().getQuotation().getCurrency().getCurrencyId())
			{
				thirdPriceTotal = qcc.getCalibrationFinalCostInConvertedCurrency();
			}
		}
		else if ((c.getLinkedCostSrc() != null)
				&& (c.getLinkedCostSrc() instanceof TPQuotationPurchaseCost)
				&& (c instanceof QuotationPurchaseCost))
		{
			TPQuotationPurchaseCost tpqpc = (TPQuotationPurchaseCost) c.getLinkedCostSrc();
			QuotationPurchaseCost qpc = (QuotationPurchaseCost) c;

			if (tpqpc.getTpQuoteItem().getTpquotation().getCurrency().getCurrencyId() != qpc.getQuoteItem().getQuotation().getCurrency().getCurrencyId())
			{
				thirdPriceTotal = qpc.getPurchaseFinalCostInConvertedCurrency();
			}
		}
		/* END JAMIE EDIT */

		if (c.getThirdMarkupSrc() == ThirdCostMarkupSource.MANUAL)
		{
			BigDecimal thirdMarkupValue = ((c.getThirdMarkupRate().divide(new BigDecimal(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN).multiply(thirdPriceTotal)));
			c.setThirdMarkupValue(thirdMarkupValue);
		}
		else
		{
			// calculate the markup rate to use for this cost
			// find the markup rate
			MarkupRangeWrapper muWrapper = this.markupRangeServ.calculateMarkUpRangeValue(thirdPriceTotal, MarkupType.SUPPLIER);
			c.setThirdMarkupValue(muWrapper.getValue());

			// only set the markuprate if the markup range action is a
			// percentage and not a fixed value
			if (muWrapper.getRange() != null)
			{
				if (muWrapper.getRange().getAction() == MarkupRangeAction.PERCENTAGE)
				{
					c.setThirdMarkupRate(new BigDecimal(muWrapper.getRange().getActionValue()));
				}
			}
		}

		// add the carriage costs
		this.updateTPCarriage(c);

		c.setThirdCostTotal(((thirdPriceTotal.add(c.getThirdMarkupValue())).add(c.getTpCarriageTotal())).setScale(2, 4));
		return c;
	}

	public void setMarkupRangeServ(MarkupRangeService markupRangeServ)
	{
		this.markupRangeServ = markupRangeServ;
	}

	public void updateTPCarriage(ThirdPartyCostSupport c)
	{
		if (c.getTpCarriageMarkupSrc() == ThirdCostMarkupSource.SYSTEM_DEFAULT)
		{
			// calculate the markup rate to use for this cost
			// find the markup rate

			BigDecimal tpCarInMarkup = this.markupRangeServ.calculateMarkUpRangeValue(c.getTpCarriageIn(), MarkupType.CARRIAGE).getValue();
			// if marked up price is less than the original price (and this can
			// happen due to crazy business rules for carriage markups) then
			// assume this markup should be ADDED to the original, rather than
			// replacing it
			if ((c.getTpCarriageIn() != null)
					&& (c.getTpCarriageIn().compareTo(tpCarInMarkup) > 0))
			{
				c.setTpCarriageInMarkupValue(c.getTpCarriageIn().add(tpCarInMarkup));
			}
			else
			{
				c.setTpCarriageInMarkupValue(tpCarInMarkup);
			}

			BigDecimal tpCarOutMarkup = this.markupRangeServ.calculateMarkUpRangeValue(c.getTpCarriageOut(), MarkupType.CARRIAGE).getValue();
			// see comment on tp carriage in above...
			if ((c.getTpCarriageOut() != null)
					&& (c.getTpCarriageOut().compareTo(tpCarOutMarkup) > 0))
			{
				c.setTpCarriageOutMarkupValue(c.getTpCarriageOut().add(tpCarOutMarkup));
			}
			else
			{
				c.setTpCarriageOutMarkupValue(tpCarOutMarkup);
			}
		}
		else
		{
			c.setTpCarriageInMarkupValue(((c.getTpCarriageMarkupRate().divide(new BigDecimal(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN).multiply(c.getTpCarriageIn()))).add(c.getTpCarriageIn(), MathTools.mc).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
			c.setTpCarriageOutMarkupValue(((c.getTpCarriageMarkupRate().divide(new BigDecimal(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN).multiply(c.getTpCarriageOut()))).add(c.getTpCarriageOut(), MathTools.mc).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
		}

		// update the totals
		c.setTpCarriageTotal(c.getTpCarriageInMarkupValue().add(c.getTpCarriageOutMarkupValue(), MathTools.mc).setScale(MathTools.SCALE_FIN));
	}
}
