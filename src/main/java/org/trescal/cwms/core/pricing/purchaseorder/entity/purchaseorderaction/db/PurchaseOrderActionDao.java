package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.PurchaseOrderAction;

public interface PurchaseOrderActionDao extends BaseDao<PurchaseOrderAction, Integer> {}