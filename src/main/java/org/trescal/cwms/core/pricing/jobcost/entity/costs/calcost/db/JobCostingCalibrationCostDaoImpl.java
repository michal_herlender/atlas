package org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.pricing.jobcost.dto.ModelJobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupportImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Repository("JobCostingCalibrationCostDao")
public class JobCostingCalibrationCostDaoImpl extends JobCostingCostDaoSupportImpl<JobCostingCalibrationCost> implements JobCostingCalibrationCostDao {
	
	public static final Logger logger = LoggerFactory.getLogger(JobCostingCalibrationCostDaoImpl.class); 
	
	@Override
	protected Class<JobCostingCalibrationCost> getEntity() {
		return JobCostingCalibrationCost.class;
	}
	
	@Override
	public JobCostingCalibrationCost findEagerJobCostingCalibrationCost(int id) {
		Criteria crit = getSession().createCriteria(JobCostingCalibrationCost.class);
		crit.add(Restrictions.idEq(id));
		crit.setFetchMode("jobCostingItem", FetchMode.JOIN);
		crit.setFetchMode("jobCostingItem.jobCosting", FetchMode.JOIN);
		crit.setFetchMode("jobCostingItem.jobCosting.currency", FetchMode.JOIN);

		return (JobCostingCalibrationCost) crit.uniqueResult();
	}
	
	@Override
	public List<ModelJobCostingCalibrationCost> findMostRecentCostForModels(Collection<Integer> modelIds, CalibrationType calType, Company company) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
		Root<JobCostingCalibrationCost> calCost = cq.from(JobCostingCalibrationCost.class);
		calCost.fetch(JobCostingCalibrationCost_.jobCostingItem);
		calCost.fetch(JobCostingCalibrationCost_.linkedCost, JoinType.LEFT);
		Join<JobCostingCalibrationCost, JobCostingItem> costingItem = calCost.join(JobCostingCalibrationCost_.jobCostingItem);
		Join<JobCostingItem, JobItem> jobItem = costingItem.join(JobCostingItem_.jobItem);
		Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
		Path<InstrumentModel> model = instrument.get(Instrument_.model);
		Subquery<Integer> sq = cq.subquery(Integer.class);
		Root<JobCostingCalibrationCost> sqCalCost = sq.from(JobCostingCalibrationCost.class);
		Join<JobCostingCalibrationCost, JobCostingItem> sqCostingItem = sqCalCost.join(JobCostingCalibrationCost_.jobCostingItem);
		Join<JobCostingItem, JobItem> sqJobItem = sqCostingItem.join(JobCostingItem_.jobItem);
		Join<JobItem, Instrument> sqInstrument = sqJobItem.join(JobItem_.inst);
		Path<InstrumentModel> sqModel = sqInstrument.get(Instrument_.model);
		Predicate sqClauses = cb.conjunction();
		sqClauses.getExpressions().add(cb.equal(sqInstrument.get(Instrument_.comp), company));
		sqClauses.getExpressions().add(cb.equal(sqJobItem.get(JobItem_.calType), calType));
		sqClauses.getExpressions().add(cb.isTrue(sqCalCost.get(JobCostingCalibrationCost_.active)));
		sqClauses.getExpressions().add(sqModel.get(InstrumentModel_.modelid).in(modelIds));
		sq.where(sqClauses);
		sq.groupBy(sqInstrument.get(Instrument_.model), sqJobItem.get(JobItem_.calType));
		sq.select(cb.max(sqCalCost.get(JobCostingCalibrationCost_.costid)));
		cq.where(calCost.get(JobCostingCalibrationCost_.costid).in(sq));
		cq.multiselect(model.get(InstrumentModel_.modelid), calCost);
		TypedQuery<Tuple> query = getEntityManager().createQuery(cq);
		return query.getResultList().stream().map(t -> (new ModelJobCostingCalibrationCost((Integer) t.get(0), (JobCostingCalibrationCost) t.get(1)))).collect(Collectors.toList());
	}
}