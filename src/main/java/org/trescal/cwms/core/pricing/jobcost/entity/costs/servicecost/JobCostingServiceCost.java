package org.trescal.cwms.core.pricing.jobcost.entity.costs.servicecost;

import java.math.BigDecimal;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.pricing.entity.costs.base.ServiceCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;

@Entity
@DiscriminatorValue("jobcosting")
public class JobCostingServiceCost extends ServiceCost {
	
	private JobCostingExpenseItem expenseItem;
	
	public JobCostingServiceCost() {
		super();
	}
	
	public JobCostingServiceCost(BigDecimal cost) {
		super(cost);
	}
	
	@OneToOne(mappedBy="serviceCost")
	public JobCostingExpenseItem getExpenseItem() {
		return expenseItem;
	}
	
	public void setExpenseItem(JobCostingExpenseItem expenseItem) {
		this.expenseItem = expenseItem;
	}
}