package org.trescal.cwms.core.pricing.invoice.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.Data;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

@Data
@JsonSerialize(converter = PInvoiceItem.PInvoiceItemDtoConverter.class)
public class PInvoiceItem {

	private JobItem jitem;
	private CostSource source;
	private Quotation quotation; // Reference to the Quotation if used as source, optionally present
	private JobCosting jobCosting; // Reference to the JobCosting if used as source, optionally present
	private PInvoice pinvoice;
	private Cost cost;

	@Deprecated
	private PInvoiceItemCost calCost;
	@Deprecated
	private PInvoiceItemCost repCost;
	@Deprecated
	private PInvoiceItemCost purCost;
	@Deprecated
	private PInvoiceItemCost adjCost;
	@Deprecated
	private int calCostId;
	@Deprecated
	private int repCostId;
	@Deprecated
	private int purCostId;
	@Deprecated
	private int adjCostId;
	@Deprecated
	private boolean costed;

	private boolean alreadyInvoiced; // Whether to flag warning that job item has already been invoiced
	private boolean allPOgoodsApproved;

	// Form inputs below
	private boolean include; // Whether invoice item should be included on this invoice
	private PNotInvoiced notInvoiced; // Transfer object for not-invoiced information

	/*
	 * Simple constructor for unit testing
	 */
	public PInvoiceItem() {
		this.include = true;
		this.alreadyInvoiced = false;
		this.allPOgoodsApproved = true;
	}

	public PInvoiceItem(JobItem ji) {
		this.jitem = ji;
		this.alreadyInvoiced = false;
		this.notInvoiced = new PNotInvoiced(this.jitem.getNotInvoiced());
		if (this.jitem.getNotInvoiced() != null) {
			// Exclude item from invoicing if already set to "not invoice"
			this.include = false;
		} else {
			// Exclude item from invoicing if it has already been invoiced on any
			// non-proforma invoices
			// (other than Internal Invoice Type)
			this.include = true;
			for (InvoiceItem invoiceItem : this.jitem.getInvoiceItems()) {
				InvoiceType type = invoiceItem.getInvoice().getType();
				if (type.isAddToAccount() && !type.getName().equals("Internal")) {
					this.include = false;
					this.alreadyInvoiced = true;
					break;
				}
			}
		}
		// All issued purchase orders for this job item either have flag 'goods
		// approved' or are cancelled.
		this.allPOgoodsApproved = ji.getPurchaseOrderItems().stream()
				.allMatch(poi -> !poi.getPoitem().getOrder().isIssued() || poi.getPoitem().isGoodsApproved()
						|| poi.getPoitem().isCancelled());
	}

	/*
	 * Application calls use this constructor
	 */
	public PInvoiceItem(JobItem ji, PInvoice pinvoice) {
		this(ji);
		this.pinvoice = pinvoice;
	}

	@Override
	public String toString() {
		return "JobItem " + jitem.getItemNo() + " job " + jitem.getJob().getJobno()
			+ (this.costed ? " cost found" : " cost NOT found");
	}

	static class PInvoiceItemDtoConverter extends StdConverter<PInvoiceItem, PInvoiceItemDto> {
		@Override
		public PInvoiceItemDto convert(PInvoiceItem value) {
			return PInvoiceItemDto.from(value);
		}

	}
}