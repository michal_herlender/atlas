package org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink;

public class InvoiceJobLinkAjaxDto
{
	private Integer id;
	private Integer invoiceId;
	private String jobNo;

	public InvoiceJobLinkAjaxDto()
	{
	}

	public InvoiceJobLinkAjaxDto(Integer id, Integer invoiceId, String jobNo)
	{
		super();
		this.id = id;
		this.invoiceId = invoiceId;
		this.jobNo = jobNo;
	}

	public Integer getId()
	{
		return this.id;
	}

	public Integer getInvoiceId()
	{
		return this.invoiceId;
	}

	public String getJobNo()
	{
		return this.jobNo;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setInvoiceId(Integer invoiceId)
	{
		this.invoiceId = invoiceId;
	}

	public void setJobNo(String jobNo)
	{
		this.jobNo = jobNo;
	}

}
