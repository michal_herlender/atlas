package org.trescal.cwms.core.pricing.supplierinvoice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentmode.db.PaymentModeService;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.db.SupplierInvoiceService;
import org.trescal.cwms.core.pricing.supplierinvoice.form.CreateSupplierInvoiceForm;
import org.trescal.cwms.core.pricing.supplierinvoice.form.SupplierInvoiceForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.DateTools;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class CreateSupplierInvoiceController {
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private PaymentModeService paymentModeService;
	@Autowired
	private PurchaseOrderService purchaseOrderService;
	@Autowired
	private SupplierInvoiceService supplierInvoiceService; 
	@Autowired
	private UserService userService;
	@Autowired
	private SupportedCurrencyService currencyServ;
	
	public static final String REQUEST_PARAM_PO = "orderid";
	public static final String FORM_NAME = "form";
	public static final String VIEW_NAME = "trescal/core/pricing/supplierinvoice/createsupplierinvoice";
	public static final String REDIRECT_URL = "viewpurchaseorder.htm?loadtab=accounts-tab&id=";
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@ModelAttribute(FORM_NAME)
	public SupplierInvoiceForm formBackingObject(@RequestParam(name = REQUEST_PARAM_PO) int orderid) {
		PurchaseOrder purchaseOrder = this.purchaseOrderService.find(orderid);
		Company supplierCompany = purchaseOrder.getContact().getSub().getComp();
		CompanySettingsForAllocatedCompany settings = this.companySettingsService.getByCompany(supplierCompany, purchaseOrder.getOrganisation());

		CreateSupplierInvoiceForm form = new CreateSupplierInvoiceForm();
		form.setLinkAllItems(true);
		form.setPaymentTerm(settings.getPaymentterm());
		form.setPaymentModeId(settings.getPaymentMode() == null ? null : settings.getPaymentMode().getId());
		form.setCurrencyCode(purchaseOrder.getCurrency().getCurrencyCode());

		return form;
	}

	@RequestMapping(value = "addsupplierinvoice.htm", method = RequestMethod.POST)
	private ModelAndView onSubmit(Locale locale,
								  @RequestParam(name = REQUEST_PARAM_PO) int orderid,
								  @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
								  @Valid @ModelAttribute(FORM_NAME) CreateSupplierInvoiceForm form,
								  BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(locale, orderid);
		}
		Contact currentContact = this.userService.get(username).getCon();
		PurchaseOrder purchaseOrder = this.purchaseOrderService.find(orderid);

		SupplierInvoice supplierInvoice = new SupplierInvoice();
		supplierInvoice.setInvoiceDate(form.getInvoiceDate());
		supplierInvoice.setInvoiceNumber(form.getInvoiceNumber());
		supplierInvoice.setOrganisation(purchaseOrder.getOrganisation());
		supplierInvoice.setPaymentDate(form.getPaymentDate());
		supplierInvoice.setTotalWithoutTax(form.getTotalWithoutTax());
		supplierInvoice.setTotalWithTax(form.getTotalWithTax());
        supplierInvoice.setCurrency(this.currencyServ.findCurrencyByCode(form.getCurrencyCode()));
        
        
		
		if (form.getPaymentTerm() != null) {
			
			supplierInvoice.setPaymentTerm(form.getPaymentTerm());
	}
		
		if (form.getPaymentModeId() != null) {
			PaymentMode paymentMode = this.paymentModeService.get(form.getPaymentModeId());
			supplierInvoice.setPaymentMode(paymentMode);
		}
		supplierInvoice.setPurchaseOrder(purchaseOrder);
		this.supplierInvoiceService.save(supplierInvoice);
		if (form.getLinkAllItems()) {
			for (PurchaseOrderItem item : purchaseOrder.getItems()) {
				item.setSupplierInvoice(supplierInvoice);
			}
		}
		purchaseOrder = this.purchaseOrderService.merge(purchaseOrder, currentContact);
		return new ModelAndView(new RedirectView(REDIRECT_URL + purchaseOrder.getId(), true));
	}

	@RequestMapping(value = "addsupplierinvoice.htm", method = RequestMethod.GET)
	private ModelAndView referenceData(Locale locale,
									   @RequestParam(name = REQUEST_PARAM_PO) int orderid) {
		Map<String, Object> map = new HashMap<>();
		map.put("order", this.purchaseOrderService.find(orderid));
		map.put("paymentterms", Arrays.stream(PaymentTerm.values()).collect(Collectors.toMap(PaymentTerm::name, PaymentTerm::getMessage)));
		map.put("paymentModes", paymentModeService.getDTOList(locale, true));
		map.put("currencyList", this.currencyServ.getAllSupportedCurrencys());
		return new ModelAndView(VIEW_NAME, map);

	}
}