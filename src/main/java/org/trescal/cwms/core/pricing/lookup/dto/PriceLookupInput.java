package org.trescal.cwms.core.pricing.lookup.dto;

import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

public class PriceLookupInput {
	// Lookup data
	private Integer serviceTypeId;					// Mandatory
	private Integer instrumentId;					// One of instrument, model, or sales category id must be specified
	private Integer modelId;
	private Integer salesCategoryId;
	// Connected outputs
	private SortedSet<PriceLookupOutput> outputs;	// Outputs of price lookup, best first (lazilly initialized)
	
	/**
	 * Should be created via factory method for checks of logical consistency
	 */
	protected PriceLookupInput(Integer serviceTypeId) {
		if (serviceTypeId == null) throw new IllegalArgumentException("serviceTypeId may not be null");
		this.serviceTypeId = serviceTypeId;
	}
	
	public PriceLookupInput() {
		super();
	}



	/**
	 * Convenience method
	 * @return whether the lookup has outputs
	 */
	public boolean hasOutputs() {
		return outputs != null && !outputs.isEmpty();
	}

	public Integer getInstrumentId() {
		return instrumentId;
	}

	public Integer getModelId() {
		return modelId;
	}

	public Integer getSalesCategoryId() {
		return salesCategoryId;
	}

	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}

	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}

	public void setSalesCategoryId(Integer salesCategoryId) {
		this.salesCategoryId = salesCategoryId;
	}

	public SortedSet<PriceLookupOutput> getOutputs() {
		if (outputs == null)
			return Collections.emptySortedSet();
		else
			return Collections.unmodifiableSortedSet(outputs);
	}

	public void addOutput(PriceLookupOutput output) {
		if (this.outputs == null) {
			this.outputs = new TreeSet<>(new PriceLookupOutputComparator());
		}
		this.outputs.add(output);
	}

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}
}