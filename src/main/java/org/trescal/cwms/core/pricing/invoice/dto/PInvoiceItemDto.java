package org.trescal.cwms.core.pricing.invoice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.trescal.cwms.core.jobs.job.dto.JobTypeDto;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.TranslationUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@Builder
class PInvoiceItemDto {
    private Boolean include;
    private PNotInvoiced notInvoiced;
    @JsonSerialize(converter = JobTypeDto.JobTypeJsonConverter.class)
    private JobType jobType;
    private String plantNo;
    private Integer jobId;
    private Integer subdivId;
    private String jobNo;
    private Integer jobItemId;
    private Integer jobItemNo;
    private Integer jobItemCount;
    private Integer invoiceItemsCount;
    private String jobStatusName;
    private List<String> poNumbers;
    private List<String> bpoNumbers;
    private CostType costType;
    private BigDecimal finalCost;
    private CostSource costSource;
    private Boolean allPOGoodsApproved;

    private String businessSubDivName;
    private Integer businessSubDivId;

    private Integer quotationId;
    private Integer quotationVer;
    private String quotationNo;
    private Integer jobCostingId;
    private List<PInvoiceDto.PInvoiceItemDeliveryItemDto> deliveryItems;
    private Date lastActionDateStamp;

    private String jobClientRef;
    private String jobItemClientRef;
    private LocalDate date;


    public String getId() {
        return costType != null ? jobItemId + "_" + costType.getName() : jobItemId.toString();
    }


    public static PInvoiceItemDto from(PInvoiceItem value) {
        return PInvoiceItemDto.builder()
            .include(value.isInclude())
            .notInvoiced(value.getNotInvoiced())
            .jobType(value.getJitem().getJob().getType())
            .jobId(value.getJitem().getJob().getJobid())
            .subdivId(value.getJitem().getJob().getCon().getSub().getSubdivid())
            .businessSubDivName(value.getJitem().getJob().getOrganisation().getSubname())
            .businessSubDivId(value.getJitem().getJob().getOrganisation().getSubdivid())
            .jobItemId(value.getJitem().getJobItemId())
            .jobItemNo(value.getJitem().getItemNo())
            .jobItemCount(value.getJitem().getJob().getItems().size())
            .jobNo(value.getJitem().getJob().getJobno())
            .plantNo(value.getJitem().getInst().getPlantno())
            .costType(value.getCost() != null ? value.getCost().getCostType() : null)
            .finalCost(value.getCost() != null ? value.getCost().getFinalCost() : null)
            .allPOGoodsApproved(value.isAllPOgoodsApproved())
            .jobCostingId(value.getJobCosting() != null ? value.getJobCosting().getId() : null)
            .quotationId(value.getQuotation() != null ? value.getQuotation().getId() : null)
            .quotationVer(value.getQuotation() != null ? value.getQuotation().getVer() :
                value.getJobCosting() != null ?
                    value.getJobCosting().getVer() : 0)
            .quotationNo(value.getQuotation() != null ? value.getQuotation().getQno() :
                value.getJobCosting() != null ?
                    value.getJobCosting().getQno() : null)
            .invoiceItemsCount(value.getJitem().getInvoiceItems().size())
            .jobStatusName(TranslationUtils.getBestTranslation(value.getJitem().getJob().getJs().getNametranslations()).orElse(""))
            .bpoNumbers(value.getJitem().getItemPOs().stream()
                .map(JobItemPO::getBpo).filter(Objects::nonNull).map(BPO::getPoNumber).collect(Collectors.toList())
            )
            .poNumbers(value.getJitem().getItemPOs().stream()
                .map(JobItemPO::getPo).filter(Objects::nonNull).map(PO::getPoNumber).collect(Collectors.toList())
            )
            .deliveryItems(value.getJitem().getDeliveryItems().stream().map(PInvoiceDto.PInvoiceItemDeliveryItemDto::fromDeliveryItem)
                .collect(Collectors.toList()))
            .jobClientRef(value.getJitem().getJob().getClientRef())
            .jobItemClientRef(value.getJitem().getClientRef())
            .date(DateTools.dateToLocalDate(value.getJitem().getDateComplete()))
            .lastActionDateStamp(getLastActionStamp(value.getJitem().getActions()))
            .costSource(value.getSource())
            .build();
    }

    private static Date getLastActionStamp(Collection<JobItemAction> actions) {
        if (actions == null || actions.isEmpty()) return null;
        return actions.stream().reduce((a, b) -> b).map(JobItemAction::getEndStamp).orElse(null);
    }
}
