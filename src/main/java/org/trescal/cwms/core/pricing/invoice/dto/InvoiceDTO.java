package org.trescal.cwms.core.pricing.invoice.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Getter
@Setter
public class InvoiceDTO {

	private Integer id;
	private String invno;
	private String invoiceType;
	private LocalDate invoiceDate;
	private LocalDate regDate;
	private LocalDate issuedate;
	private BigDecimal totalCost;
	private List<InvoiceJobLinkDTO> invoiceJobLinksDto;
	private Integer subdivid;
	private String subname;
	private String currencyERSymbol;
	private String currencyCode;
	private Integer coid;
	private String coname;
	private Boolean companyActive;
	private Boolean companyOnStop;
	private Boolean subdivActive;
	
	private List<InvoiceItemPoLinkDTO> invoiceItemPoLinkDTO;
	private List<String> creditNoteNumbers;
	


	public InvoiceDTO(Integer id, String invno, LocalDate invoiceDate) {
		super();
		this.id = id;
		this.invno = invno;
		this.invoiceDate = invoiceDate;
	}

	public InvoiceDTO(Integer id, String invno, String invoiceType, LocalDate invoiceDate, LocalDate regDate, LocalDate issuedate, BigDecimal totalCost, Integer coid,
					  String coname, Boolean companyActive, Boolean companyOnStop, Integer subdivid, String subname,
					  Boolean subdivActive, String currencyERSymbol, String currencyCode) {
		super();
		this.id = id;
		this.invno = invno;
		this.invoiceType = invoiceType;
		this.invoiceDate = invoiceDate;
		this.regDate = regDate;
		this.issuedate = issuedate;
		this.totalCost = totalCost;
		this.subdivid = subdivid;
		this.subname = subname;
		this.subdivActive = subdivActive;
		this.currencyERSymbol = currencyERSymbol;
		this.currencyCode = currencyCode;
		this.coid = coid;
		this.coname = coname;
		this.companyActive = companyActive;
		this.companyOnStop = companyOnStop;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof InvoiceDTO) {
			InvoiceDTO otherDto = (InvoiceDTO) other;
			return this.getId().equals(otherDto.getId());
		} else
			return false;
	}

	@Override
	public int hashCode() {
		return id;
	}

}