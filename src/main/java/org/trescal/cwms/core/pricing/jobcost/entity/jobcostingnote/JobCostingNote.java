package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "jobcostingnote")
public class JobCostingNote extends Note
{
	private JobCosting jobcosting;
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostingid", unique = false, nullable = false, insertable = true, updatable = true)
	public JobCosting getJobcosting() {
		return this.jobcosting;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.JOBCOSTINGNOTE;
	}
	
	public void setJobcosting(JobCosting jobcosting) {
		this.jobcosting = jobcosting;
	}
}