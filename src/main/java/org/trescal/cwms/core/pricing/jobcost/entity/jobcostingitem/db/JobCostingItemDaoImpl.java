package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingLinkedCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingLinkedCalibrationCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark.JobCostingItemNote;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark.JobCostingItemNote_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus_;
import org.trescal.cwms.core.pricing.jobcost.projection.JobCostingItemProjectionDTO;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost_;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobCostingItemDTO;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Repository("JobCostingItemDao")
public class JobCostingItemDaoImpl extends BaseDaoImpl<JobCostingItem, Integer> implements JobCostingItemDao {
	@Override
	protected Class<JobCostingItem> getEntity() {
		return JobCostingItem.class;
	}

	public void delete(Collection<JobCostingItem> items) {
		for (JobCostingItem item : items)
			remove(item);
	}

	public JobCostingItem get(int id) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(JobCostingItem.class);
			cq.where(cb.equal(cq.from(JobCostingItem.class), id));
			return cq;
		});
	}

	@Override
	public JobCostingItem findMostRecentCostingForJobitem(int jobitemid) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(JobCostingItem.class);
			val jci = cq.from(JobCostingItem.class);
			cq.where(cb.equal(jci.get(JobCostingItem_.jobItem), jobitemid));
			cq.orderBy(cb.desc(jci.join(JobCostingItem_.jobCosting).get(JobCosting_.regdate)),
				cb.desc(jci.get(JobCostingItem_.id))
			);
			return cq;
		}).orElse(null);
	}

	@Override
	public List<AdvesoJobCostingItemDTO> findIssuedJobCostingItemByJobItemID(int jobitemid) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AdvesoJobCostingItemDTO> cq = cb.createQuery(AdvesoJobCostingItemDTO.class);
		Root<JobItem> jobItem = cq.from(JobItem.class);
		Join<JobItem, JobCostingItem> jobCostingItemJoin = jobItem.join(JobItem_.jobCostingItems);
		Join<JobCostingItem, JobCosting> jobCostingJoin = jobCostingItemJoin.join(JobCostingItem_.jobCosting);
		Join<JobCosting, JobCostingStatus> jobCostingStatusJoin = jobCostingJoin.join(JobCosting_.status);

		Join<JobCostingItem, JobCostingAdjustmentCost> jobCostingAdjustJoin = jobCostingItemJoin
				.join(JobCostingItem_.adjustmentCost, JoinType.LEFT);
		Join<JobCostingItem, JobCostingCalibrationCost> jobCostingCalJoin = jobCostingItemJoin
				.join(JobCostingItem_.calibrationCost, JoinType.LEFT);
		Join<JobCostingItem, JobCostingInspectionCost> jobCostingInspJoin = jobCostingItemJoin
				.join(JobCostingItem_.inspectionCost, JoinType.LEFT);
		Join<JobCostingItem, JobCostingPurchaseCost> jobCostingPurchJoin = jobCostingItemJoin
				.join(JobCostingItem_.purchaseCost, JoinType.LEFT);
		Join<JobCostingItem, JobCostingRepairCost> jobCostingRepairJoin = jobCostingItemJoin
				.join(JobCostingItem_.repairCost, JoinType.LEFT);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobitemid));
		clauses.getExpressions().add(cb.isTrue(jobCostingStatusJoin.get(JobCostingStatus_.issued)));

		cq.select(cb.construct(AdvesoJobCostingItemDTO.class, jobCostingItemJoin.get(JobCostingItem_.id),
				jobCostingAdjustJoin.get(JobCostingAdjustmentCost_.totalCost),
				jobCostingCalJoin.get(JobCostingCalibrationCost_.totalCost),
				jobCostingInspJoin.get(JobCostingInspectionCost_.totalCost),
				jobCostingPurchJoin.get(JobCostingPurchaseCost_.totalCost),
				jobCostingRepairJoin.get(JobCostingRepairCost_.totalCost)));

		cq.where(clauses);

		TypedQuery<AdvesoJobCostingItemDTO> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<JobCostingItemDto> getJobCotingItemsDtoByJobCostingId(Integer jobCostingId, Locale locale) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<JobCostingItemDto> cq = cb.createQuery(JobCostingItemDto.class);
		Root<JobCosting> jobCostingRoot = cq.from(JobCosting.class);
		Join<JobCosting, JobCostingItem> jobCostingItemJoin = jobCostingRoot.join(JobCosting_.items, JoinType.LEFT);
		Join<JobCostingItem, JobItem> jobItemJoin = jobCostingItemJoin.join(JobCostingItem_.jobItem, JoinType.LEFT);
		Join<JobItem, CalibrationType> calibrationTypeJoin = jobItemJoin.join(JobItem_.calType, JoinType.LEFT);
		Join<CalibrationType, ServiceType> serviceTypeJoin = calibrationTypeJoin.join(CalibrationType_.serviceType,
				JoinType.LEFT);
		Join<ServiceType, Translation> shortnameTranslationJoin = serviceTypeJoin
				.join(ServiceType_.shortnameTranslation, JoinType.LEFT);

		Join<JobItem, Instrument> instrumentJoin = jobItemJoin.join(JobItem_.inst, JoinType.LEFT);
		Join<Instrument, InstrumentModel> instrumentModelJoin = instrumentJoin.join(Instrument_.model, JoinType.LEFT);
		Join<InstrumentModel, Translation> instrumentModelNameTranslationJoin = instrumentModelJoin
				.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
		Join<InstrumentModel, Description> descriptionJoin = instrumentModelJoin.join(InstrumentModel_.description,
				JoinType.LEFT);
		Join<Instrument, Mfr> mfrJoin = instrumentJoin.join(Instrument_.mfr, JoinType.LEFT);

		Join<JobCostingItem, JobCostingAdjustmentCost> jobCostingAdjustmentCostJoin = jobCostingItemJoin
				.join(JobCostingItem_.adjustmentCost, JoinType.LEFT);

		Join<JobCostingItem, JobCostingCalibrationCost> jobCostingCalibrationCostJoin = jobCostingItemJoin
				.join(JobCostingItem_.calibrationCost, JoinType.LEFT);
		Join<JobCostingCalibrationCost, JobCostingLinkedCalibrationCost> jobCostingLinkedCalibrationCostJoin = jobCostingCalibrationCostJoin
				.join(JobCostingCalibrationCost_.linkedCost, JoinType.LEFT);
		Join<JobCostingLinkedCalibrationCost, QuotationCalibrationCost> quotationCalibrationCostJoin = jobCostingLinkedCalibrationCostJoin
				.join(JobCostingLinkedCalibrationCost_.quotationCalibrationCost, JoinType.LEFT);
		Join<QuotationCalibrationCost, Quotationitem> quotationitemJoin = quotationCalibrationCostJoin
				.join(QuotationCalibrationCost_.quoteItem, JoinType.LEFT);
		Join<Quotationitem, Quotation> quotationJoin = quotationitemJoin.join(Quotationitem_.quotation, JoinType.LEFT);

		Join<JobCostingItem, JobCostingInspectionCost> jobCostingInspectionCostJoin = jobCostingItemJoin
			.join(JobCostingItem_.inspectionCost, JoinType.LEFT);
		Join<JobCostingItem, JobCostingRepairCost> jobCostingRepairCostJoin = jobCostingItemJoin
			.join(JobCostingItem_.repairCost, JoinType.LEFT);
		Join<JobCostingItem, JobCostingPurchaseCost> jobCostingPurchaseCostJoin = jobCostingItemJoin
			.join(JobCostingItem_.purchaseCost, JoinType.LEFT);

		Subquery<Long> privateNotesCountSq = cq.subquery(Long.class);
		Root<JobCostingItemNote> privateNotesCountSqRoot = privateNotesCountSq.from(JobCostingItemNote.class);
		Join<JobCostingItemNote, JobCostingItem> jobCostingItemPrivateNoteJoin = privateNotesCountSqRoot
			.join(JobCostingItemNote_.jobCostingItem);
		privateNotesCountSq.select(cb.count(privateNotesCountSqRoot));
		privateNotesCountSq.where(cb.equal(privateNotesCountSqRoot.get(JobCostingItemNote_.active), 1),
			cb.equal(privateNotesCountSqRoot.get(JobCostingItemNote_.publish), 0),
			cb.equal(jobCostingItemPrivateNoteJoin.get(JobCostingItem_.id),
				jobCostingItemJoin.get(JobCostingItem_.id)));

		Subquery<Long> publicNotesCountSq = cq.subquery(Long.class);
		Root<JobCostingItemNote> publicNotesCountSqRoot = publicNotesCountSq.from(JobCostingItemNote.class);
		Join<JobCostingItemNote, JobCostingItem> jobCostingItemPublicNoteJoin = publicNotesCountSqRoot
			.join(JobCostingItemNote_.jobCostingItem);
		publicNotesCountSq.select(cb.count(publicNotesCountSqRoot));
		publicNotesCountSq.where(cb.equal(publicNotesCountSqRoot.get(JobCostingItemNote_.active), 1),
			cb.equal(publicNotesCountSqRoot.get(JobCostingItemNote_.publish), 1),
			cb.equal(jobCostingItemPublicNoteJoin.get(JobCostingItem_.id),
				jobCostingItemJoin.get(JobCostingItem_.id)));

		Subquery<Long> crItemsCountSq = cq.subquery(Long.class);
		Root<ContractReviewItem> crItemsCountSqRoot = crItemsCountSq.from(ContractReviewItem.class);
		Join<ContractReviewItem, JobItem> jobItemCRJoin = crItemsCountSqRoot
			.join(ContractReviewItem_.jobitem);
		crItemsCountSq.select(cb.count(crItemsCountSqRoot));
		crItemsCountSq.where(cb.equal(jobItemCRJoin.get(JobItem_.jobItemId),
			jobItemJoin.get(JobItem_.jobItemId)));

		//noinspection RedundantTypeArguments (explicit type arguments speedup compilation and analysis time)
		cq.select(cb.<JobCostingItemDto>construct(JobCostingItemDto.class, jobCostingItemJoin.get(JobCostingItem_.id),
			serviceTypeJoin.get(ServiceType_.displayColour), jobItemJoin.get(JobItem_.turn),
			serviceTypeJoin.get(ServiceType_.displayColourFastTrack),
			jobItemJoin.get(JobItem_.jobItemId), jobItemJoin.get(JobItem_.itemNo),
			crItemsCountSq.getSelection(), instrumentJoin.get(Instrument_.plantid),
			instrumentJoin.get(Instrument_.serialno), instrumentJoin.get(Instrument_.plantno),
			instrumentJoin.get(Instrument_.calibrationStandard),
			instrumentJoin.get(Instrument_.scrapped),
			instrumentModelJoin.get(InstrumentModel_.modelid),
			descriptionJoin.get(Description_.typology),
			instrumentModelJoin.get(InstrumentModel_.modelMfrType), mfrJoin.get(Mfr_.name),
			mfrJoin.get(Mfr_.genericMfr),
			instrumentModelNameTranslationJoin.get(Translation_.translation),
			instrumentJoin.get(Instrument_.modelname),
			shortnameTranslationJoin.get(Translation_.translation),
			jobCostingRoot.get(JobCosting_.type),
			jobCostingItemJoin.get(JobCostingItem_.partOfBaseUnit),
			jobCostingItemJoin.get(JobCostingItem_.quantity),
			jobCostingItemJoin.get(JobCostingItem_.generalDiscountValue),
			jobCostingItemJoin.get(JobCostingItem_.generalDiscountRate),
			jobCostingItemJoin.get(JobCostingItem_.finalCost),
			jobCostingItemJoin.get(JobCostingItem_.calibrated), publicNotesCountSq.getSelection(),
			privateNotesCountSq.getSelection(),

			jobCostingAdjustmentCostJoin.get(JobCostingAdjustmentCost_.active),
			jobCostingAdjustmentCostJoin.get(JobCostingAdjustmentCost_.costType),
			jobCostingAdjustmentCostJoin.get(JobCostingAdjustmentCost_.costSrc),
			jobCostingAdjustmentCostJoin.get(JobCostingAdjustmentCost_.discountValue),
			jobCostingAdjustmentCostJoin.get(JobCostingAdjustmentCost_.discountRate),
			jobCostingAdjustmentCostJoin.get(JobCostingAdjustmentCost_.totalCost),
			jobCostingAdjustmentCostJoin.get(JobCostingAdjustmentCost_.finalCost),

			jobCostingCalibrationCostJoin.get(JobCostingCalibrationCost_.active),
			jobCostingCalibrationCostJoin.get(JobCostingCalibrationCost_.costType),
			jobCostingCalibrationCostJoin.get(JobCostingCalibrationCost_.costSrc),
			jobCostingCalibrationCostJoin.get(JobCostingCalibrationCost_.discountValue),
			jobCostingCalibrationCostJoin.get(JobCostingCalibrationCost_.discountRate),
			jobCostingCalibrationCostJoin.get(JobCostingCalibrationCost_.totalCost),
			jobCostingCalibrationCostJoin.get(JobCostingCalibrationCost_.finalCost),
			quotationJoin.get(Quotation_.id), quotationJoin.get(Quotation_.qno),
			quotationJoin.get(Quotation_.ver), quotationJoin.get(Quotation_.issued),
			quotationJoin.get(Quotation_.reqdate), quotationJoin.get(Quotation_.duration),
			quotationJoin.get(Quotation_.expiryDate), quotationJoin.get(Quotation_.issuedate),
			quotationitemJoin.get(Quotationitem_.id),
			quotationitemJoin.get(Quotationitem_.itemno),

			jobCostingInspectionCostJoin.get(JobCostingInspectionCost_.active),
			jobCostingInspectionCostJoin.get(JobCostingInspectionCost_.costType),
			jobCostingInspectionCostJoin.get(JobCostingInspectionCost_.discountValue),
			jobCostingInspectionCostJoin.get(JobCostingInspectionCost_.discountRate),
			jobCostingInspectionCostJoin.get(JobCostingInspectionCost_.totalCost),
			jobCostingInspectionCostJoin.get(JobCostingInspectionCost_.finalCost),

			jobCostingRepairCostJoin.get(JobCostingRepairCost_.active),
			jobCostingRepairCostJoin.get(JobCostingRepairCost_.costType),
			jobCostingRepairCostJoin.get(JobCostingRepairCost_.costSrc),
			jobCostingRepairCostJoin.get(JobCostingRepairCost_.discountValue),
			jobCostingRepairCostJoin.get(JobCostingRepairCost_.discountRate),
			jobCostingRepairCostJoin.get(JobCostingRepairCost_.totalCost),
			jobCostingRepairCostJoin.get(JobCostingRepairCost_.finalCost),

			jobCostingPurchaseCostJoin.get(JobCostingPurchaseCost_.active),
			jobCostingPurchaseCostJoin.get(JobCostingPurchaseCost_.costType),
			jobCostingPurchaseCostJoin.get(JobCostingPurchaseCost_.costSrc),
			jobCostingPurchaseCostJoin.get(JobCostingPurchaseCost_.discountValue),
			jobCostingPurchaseCostJoin.get(JobCostingPurchaseCost_.discountRate),
			jobCostingPurchaseCostJoin.get(JobCostingPurchaseCost_.totalCost),
			jobCostingPurchaseCostJoin.get(JobCostingPurchaseCost_.finalCost),
			instrumentJoin.get(Instrument_.customerDescription)
		));

		cq.where(cb.equal(jobCostingRoot.get(JobCosting_.id), jobCostingId),
			cb.equal(shortnameTranslationJoin.get(Translation_.locale), locale),
			cb.equal(instrumentModelNameTranslationJoin.get(Translation_.locale), locale));

		cq.orderBy(cb.asc(jobItemJoin.get(JobItem_.itemNo)));

		TypedQuery<JobCostingItemDto> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}
	
	@Override
	public List<JobCostingItemProjectionDTO> getProjectionDTOsForJobItems(Collection<Integer> jobItemIds) {
		if (jobItemIds == null || jobItemIds.isEmpty())
			throw new IllegalArgumentException("At least one jobItemId must be provided");
		return getResultList(cb -> {
			CriteriaQuery<JobCostingItemProjectionDTO> cq = cb.createQuery(JobCostingItemProjectionDTO.class);
			Root<JobCostingItem> root = cq.from(JobCostingItem.class);
			Join<JobCostingItem, JobItem> jobItem = root.join(JobCostingItem_.jobItem, JoinType.INNER);
			Join<JobCostingItem, JobCosting> jobCosting = root.join(JobCostingItem_.jobCosting, JoinType.INNER);
			
			cq.where(jobItem.get(JobItem_.jobItemId).in(jobItemIds));
			cq.select(cb.construct(JobCostingItemProjectionDTO.class, 
					root.get(JobCostingItem_.id),
					root.get(JobCostingItem_.itemno),
					root.get(JobCostingItem_.totalCost),
					root.get(JobCostingItem_.finalCost),
					jobItem.get(JobItem_.jobItemId), 
					jobCosting.get(JobCosting_.id),
					jobCosting.get(JobCosting_.ver)));
			return cq;
		});
	}
}