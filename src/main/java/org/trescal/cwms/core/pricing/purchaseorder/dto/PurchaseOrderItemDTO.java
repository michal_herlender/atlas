package org.trescal.cwms.core.pricing.purchaseorder.dto;

import java.math.BigDecimal;

public class PurchaseOrderItemDTO {

	private Integer itemId;
	private Integer itemNo;
	private String description;
	private String currencyERSymbol;
	private BigDecimal generalDiscountRate;
	private BigDecimal finalCost;
	private Integer jobItemId;
	private Integer jobItemNo;
	private String jobNo;
	
	public PurchaseOrderItemDTO() {}
	
	public PurchaseOrderItemDTO(Integer itemId, Integer itemNo, String description, String currencyERSymbol,
			BigDecimal generalDiscountRate, BigDecimal finalCost, Integer jobItemId, Integer jobItemNo, String jobNo) {
		this.itemId = itemId;
		this.itemNo = itemNo;
		this.description = description;
		this.currencyERSymbol = currencyERSymbol;
		this.generalDiscountRate = generalDiscountRate;
		this.finalCost = finalCost;
		this.jobItemId = jobItemId;
		this.jobItemNo = jobItemNo;
		this.jobNo = jobNo;
	}
	
	public Integer getItemId() {
		return itemId;
	}
	
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	
	public Integer getItemNo() {
		return itemNo;
	}
	
	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCurrencyERSymbol() {
		return currencyERSymbol;
	}
	
	public void setCurrencyERSymbol(String currencyERSymbol) {
		this.currencyERSymbol = currencyERSymbol;
	}
	
	public BigDecimal getGeneralDiscountRate() {
		return generalDiscountRate;
	}
	
	public void setGeneralDiscountRate(BigDecimal generalDiscountRate) {
		this.generalDiscountRate = generalDiscountRate;
	}
	
	public BigDecimal getFinalCost() {
		return finalCost;
	}
	
	public void setFinalCost(BigDecimal finalCost) {
		this.finalCost = finalCost;
	}
	
	public Integer getJobItemId() {
		return jobItemId;
	}
	
	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}
	
	public Integer getJobItemNo() {
		return jobItemNo;
	}
	
	public void setJobItemNo(Integer jobItemNo) {
		this.jobItemNo = jobItemNo;
	}
	
	public String getJobNo() {
		return jobNo;
	}
	
	public void setJobNo(String jobNo) {
		this.jobNo = jobNo;
	}
}