package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Entity
@Table(name = "jobcostingclientapproval")
public class JobCostingClientApproval {
	private int id;
	private JobCosting jobCosting;
	private Boolean clientApproval;
	private Date clientApprovalOn;
	private String clientApprovalComment;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostingid", nullable = false)
	public JobCosting getJobCosting() {
		return this.jobCosting;
	}

	@Column(name = "clientapproval", nullable = false)
	public Boolean getClientApproval() {
		return clientApproval;
	}

	@Column(name = "clientapprovalon", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getClientApprovalOn() {
		return clientApprovalOn;
	}

	@Column(name = "clientapprovalcomment", nullable = true)
	public String getClientApprovalComment() {
		return clientApprovalComment;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setJobCosting(JobCosting jobCosting) {
		this.jobCosting = jobCosting;
	}

	public void setClientApproval(Boolean clientApproval) {
		this.clientApproval = clientApproval;
	}

	public void setClientApprovalOn(Date clientApprovalOn) {
		this.clientApprovalOn = clientApprovalOn;
	}

	public void setClientApprovalComment(String clientApprovalComment) {
		this.clientApprovalComment = clientApprovalComment;
	}

}