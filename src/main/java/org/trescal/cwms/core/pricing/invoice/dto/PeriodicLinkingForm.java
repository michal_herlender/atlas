package org.trescal.cwms.core.pricing.invoice.dto;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PeriodicLinkingForm {

	@NotNull
	@Min(value = 1, message = "{error.value.notselected}")
	private Integer invoiceId;
	private Integer allocatedSubdivId;
	private List<Integer> items; // Changed to job item id rather than storing whole PInvoiceItem
	private List<Integer> expenseItems;

}