package org.trescal.cwms.core.pricing.entity.enums;

/**
 * Enum which identifies the source of a parts markup rate. Two options are
 * MANUAL - i.e. set by a user and SYSTEM_DERIVED meaning the system will
 * consult it's own lookup table to perform calculate the markup rate.
 * 
 * @author Richard
 */
public enum PartsMarkupSource
{
	MANUAL("Manually set"), SYSTEM_DERIVED("System calculated");

	private String name;

	PartsMarkupSource(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
