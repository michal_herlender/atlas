package org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.InvoiceDelivery;

@Service("InvoiceDeliveryService")
public class InvoiceDeliveryServiceImpl extends BaseServiceImpl<InvoiceDelivery, Integer>
		implements InvoiceDeliveryService {

	@Autowired
	private InvoiceDeliveryDao invDelDao;
	@Autowired
	private InvoiceService invoiceService;

	@Override
	protected BaseDao<InvoiceDelivery, Integer> getBaseDao() {
		return invDelDao;
	}

	public ResultWrapper ajaxAddInvoiceDelivery(int invId, String delNo) {
		Invoice inv = this.invoiceService.findInvoice(invId);

		if (inv != null) {
			if ((delNo != null) && !delNo.trim().isEmpty() && (delNo.length() < 15)) {
				InvoiceDelivery invDel = new InvoiceDelivery();
				invDel.setInvoice(inv);
				invDel.setDeliveryNo(delNo);
				save(invDel);
				return new ResultWrapper(true, null, invDel, null);
			} else {
				return new ResultWrapper(false, "The delivery number must be between 1 and 15 characters long");
			}
		} else {
			return new ResultWrapper(false, "Invoice cannot be found");
		}
	}

	public ResultWrapper ajaxDeleteInvoiceDelivery(int invDelId) {
		InvoiceDelivery invDel = invDelDao.find(invDelId);
		if (invDel != null) {
			invDelDao.remove(invDel);
			return new ResultWrapper(true, null);
		} else {
			return new ResultWrapper(false, "Could not find invoice delivery");
		}
	}
}