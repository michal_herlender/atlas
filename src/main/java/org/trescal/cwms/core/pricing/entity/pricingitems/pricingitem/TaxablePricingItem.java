package org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.address.Address;

import lombok.Setter;

@MappedSuperclass
@Setter
public abstract class TaxablePricingItem extends PricingItem {

	private boolean taxable;
	private BigDecimal taxAmount;
	private Address shipToAddress;

	@NotNull
	@Column(name = "taxable", nullable = false, columnDefinition = "tinyint")
	public boolean isTaxable() {
		return taxable;
	}

	@Column(name = "taxamount")
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	@ManyToOne
	@JoinColumn(name = "shiptoaddressid")
	public Address getShipToAddress() {
		return shipToAddress;
	}
}