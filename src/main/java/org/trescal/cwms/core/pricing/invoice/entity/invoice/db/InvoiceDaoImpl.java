package org.trescal.cwms.core.pricing.invoice.entity.invoice.db;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus_;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.User_;
import org.trescal.cwms.core.pricing.invoice.dto.CompanyReadyToBeInvoiced;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.dto.ReadyToInvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType_;
import org.trescal.cwms.core.pricing.invoice.form.InvoiceHomeForm;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;
import org.trescal.cwms.core.userright.entity.userrole.UserRole_;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;

import lombok.val;

@Repository("InvoiceDao")
public class InvoiceDaoImpl extends BaseDaoImpl<Invoice, Integer> implements InvoiceDao {

	@Override
	protected Class<Invoice> getEntity() {
		return Invoice.class;
	}

	@Override
	public List<ReadyToInvoiceDTO> getReadyToInvoice(Integer subdivId, Integer companyId){
		List<ReadyToInvoiceDTO> invoiceableList = getResultList(cb -> {
			CriteriaQuery<ReadyToInvoiceDTO> cq = cb.createQuery(ReadyToInvoiceDTO.class);
			Root<Job> job = cq.from(Job.class);
			Join<Job, Contact> con = job.join(Job_.con);
			Join<Contact, Subdiv> sub = con.join(Contact_.sub);
			Join<Subdiv, Company> comp = sub.join(Subdiv_.comp);
			Join<Job, JobStatus> js = job.join(Job_.js);
			
			// job items
			Subquery<Long> jobItemsToInvoice = cq.subquery(Long.class);
			Root<JobItem> jobItem = jobItemsToInvoice.from(JobItem.class);
			Join<JobItem, Job> itemsJob = jobItem.join(JobItem_.job);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
			Join<JobItem, JobItemNotInvoiced> itemNotInvoiced = jobItem.join(JobItem_.notInvoiced, JoinType.LEFT);
			Subquery<Long> itemsInvoices = jobItemsToInvoice.subquery(Long.class);
			Root<InvoiceItem> itemsInvoiceItem = itemsInvoices.from(InvoiceItem.class);
			Join<InvoiceItem, Invoice> itemsInvoice = itemsInvoiceItem.join(InvoiceItem_.invoice);
			Join<Invoice, InvoiceType> itemsInvoiceType = itemsInvoice.join(Invoice_.type);
			Predicate itemsInvoicesClauses = cb.conjunction();
			itemsInvoicesClauses.getExpressions().add(cb.notEqual(itemsInvoiceType.get(InvoiceType_.name), "Internal"));
			itemsInvoicesClauses.getExpressions().add(cb.isTrue(itemsInvoiceType.get(InvoiceType_.addToAccount)));
			itemsInvoicesClauses.getExpressions().add(cb.equal(itemsInvoiceItem.get(InvoiceItem_.jobItem), jobItem));
			itemsInvoices.where(itemsInvoicesClauses);
			itemsInvoices.select(cb.count(itemsInvoiceItem));
			Predicate jobItemClauses = cb.conjunction();
			jobItemClauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.job), job));
			if (subdivId == null) {
				Join<Job, Subdiv> itemAllocatedSubdiv = itemsJob.join(Job_.organisation.getName());
				jobItemClauses.getExpressions().add(cb.equal(itemAllocatedSubdiv.get(Subdiv_.comp), companyId));
			} else {
				jobItemClauses.getExpressions().add(cb.equal(itemsJob.get(Job_.organisation), subdivId));
			}
			jobItemClauses.getExpressions().add(cb.equal(itemsInvoices.getSelection(), cb.literal(0L)));
			jobItemClauses.getExpressions().add(cb.isNull(itemNotInvoiced));
			jobItemClauses.getExpressions().add(cb.isFalse(itemState.get(ItemState_.active)));
			jobItemsToInvoice.where(jobItemClauses);
			jobItemsToInvoice.select(cb.count(jobItem));

			// job expenses 
			Subquery<Long> jobExpensesToInvoice = cq.subquery(Long.class);
			Root<JobExpenseItem> expenseItem = jobExpensesToInvoice.from(JobExpenseItem.class);
			Join<JobExpenseItem, Job> expenseJob = expenseItem.join(JobExpenseItem_.job);
			Join<JobExpenseItem, JobExpenseItemNotInvoiced> expenseNotInvoiced = expenseItem
				.join(JobExpenseItem_.notInvoiced, JoinType.LEFT);
			Subquery<Long> expenseInvoices = jobExpensesToInvoice.subquery(Long.class);
			Root<InvoiceItem> expenseInvoiceItem = expenseInvoices.from(InvoiceItem.class);
			Join<InvoiceItem, Invoice> expenseInvoice = expenseInvoiceItem.join(InvoiceItem_.invoice);
			Join<Invoice, InvoiceType> expenseInvoiceType = expenseInvoice.join(Invoice_.type);
			Predicate expenseInvoicesClauses = cb.conjunction();
			expenseInvoicesClauses.getExpressions()
				.add(cb.notEqual(expenseInvoiceType.get(InvoiceType_.name), "Internal"));
			expenseInvoicesClauses.getExpressions().add(cb.isTrue(expenseInvoiceType.get(InvoiceType_.addToAccount)));
			expenseInvoicesClauses.getExpressions()
				.add(cb.equal(expenseInvoiceItem.get(InvoiceItem_.expenseItem), expenseItem));
			expenseInvoices.where(expenseInvoicesClauses);
			expenseInvoices.select(cb.count(expenseInvoiceItem));
			Predicate expenseItemClauses = cb.conjunction();
			expenseItemClauses.getExpressions().add(cb.equal(expenseItem.get(JobExpenseItem_.job), job));
			if (subdivId == null) {
				Join<Job, Subdiv> expenseAllocatedSubdiv = expenseJob.join(Job_.organisation.getName());
				expenseItemClauses.getExpressions().add(cb.equal(expenseAllocatedSubdiv.get(Subdiv_.comp), companyId));
			} else
				expenseItemClauses.getExpressions().add(cb.equal(expenseJob.get(Job_.organisation), subdivId));
			expenseItemClauses.getExpressions().add(cb.equal(expenseInvoices.getSelection(), cb.literal(0L)));
			expenseItemClauses.getExpressions().add(cb.isTrue(expenseItem.get(JobExpenseItem_.invoiceable)));
			expenseItemClauses.getExpressions().add(cb.isNull(expenseNotInvoiced));
			jobExpensesToInvoice.where(expenseItemClauses);
			jobExpensesToInvoice.select(cb.count(expenseItem));

			cq.select(cb.construct(ReadyToInvoiceDTO.class, 
					job.get(Job_.jobid),
					job.get(Job_.jobno),
					js.get(JobStatus_.complete),
					comp.get(Company_.coid),
					comp.get(Company_.coname),
					comp.get(Company_.companyRole),
					jobItemsToInvoice.getSelection(),
					jobExpensesToInvoice.getSelection()
					));
			cq.groupBy(job.get(Job_.jobid), job.get(Job_.jobno),
					job.get(Job_.js).get(JobStatus_.complete),
					comp.get(Company_.coid),
					comp.get(Company_.coname),
					comp.get(Company_.companyRole));
			return cq;
		});
		/*
		 * For some reason the execution plan in production environment for the
		 * sql statement above is worse, if we filter on the server directly (1s
		 * vs 1min). It's not the same with my sql server development edition
		 * 2017, where it works better to filter on the sql server directly.
		 */
		return invoiceableList.stream().filter(job -> job.getJobItemsToInvoice() + job.getJobExpenseItemsToInvoice() > 0)
				.collect(Collectors.toList());
	}

	@Override
	public Invoice findByInvoiceNo(String invoiceNo, Integer orgId) {
		return getFirstResult(cb -> {
			CriteriaQuery<Invoice> cq = cb.createQuery(Invoice.class);
			Root<Invoice> invoice = cq.from(Invoice.class);
			Join<Invoice, Company> company = invoice.join(Invoice_.ORGANISATION, JoinType.INNER);
			cq.where(cb.and(cb.equal(invoice.get(Invoice_.invno), invoiceNo)),
					cb.equal(company.get(Company_.coid), orgId));
			return cq;
		}).orElse(null);
	}

	@Override
	public Invoice findPendingInternalInvoice(Company drawer, Company recipient) {
		return getSingleResult(cb -> {
			CriteriaQuery<Invoice> cq = cb.createQuery(Invoice.class);
			Root<Invoice> root = cq.from(Invoice.class);
			Join<Invoice, InvoiceStatus> status = root.join(Invoice_.status);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Invoice_.organisation), drawer));
			clauses.getExpressions().add(cb.equal(root.get(Invoice_.comp), recipient));
			clauses.getExpressions().add(cb.equal(status.get(InvoiceStatus_.name), "Internal invoice"));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(Invoice_.invno)));
			return cq;
		});
	}

	@Override
	public List<CompanyReadyToBeInvoiced> getAllCompaniesWithJobsReadyToInvoice(Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<CompanyReadyToBeInvoiced> cq = cb.createQuery(CompanyReadyToBeInvoiced.class);
			Root<Job> root = cq.from(Job.class);
			Join<Job, Subdiv> organisation = root.join(Job_.ORGANISATION, JoinType.INNER);
			Join<Job, Contact> contact = root.join(Job_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub, JoinType.INNER);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.INNER);
			Join<Job, JobStatus> status = root.join(Job_.js, JoinType.INNER);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(organisation.get(Subdiv_.comp), allocatedCompany));
			clauses.getExpressions().add(cb.isTrue(status.get(JobStatus_.readyToInvoice)));
			cq.where(clauses);
			cq.groupBy(company, company.get(Company_.coname));
			cq.orderBy(cb.asc(company.get(Company_.coname)));
			cq.select(cb.construct(CompanyReadyToBeInvoiced.class, company, cb.count(root)));
			return cq;
		});
	}
	
	@Override
	public List<InvoiceProjectionDTO> getProjectionDtos(Collection<Integer> invoiceIds) {
		if (invoiceIds == null || invoiceIds.isEmpty())
			throw new IllegalArgumentException("At least one invoiceId must be specified");
		return getResultList(cb -> {
			CriteriaQuery<InvoiceProjectionDTO> cq = cb.createQuery(InvoiceProjectionDTO.class);
			Root<Invoice> root = cq.from(Invoice.class);
			Join<Invoice, Company> allocatedCompany = root.join(Invoice_.ORGANISATION, JoinType.INNER);
			cq.where(root.get(Invoice_.id).in(invoiceIds));
			cq.select(getSelection(cb, root, allocatedCompany));
			
			return cq;
		});
	}
	
	private Selection<InvoiceProjectionDTO> getSelection(CriteriaBuilder cb, Root<Invoice> root, Join<Invoice, Company> allocatedCompany) {
		Join<Invoice, Company> company = root.join(Invoice_.comp, JoinType.INNER);
		
		return cb.construct(InvoiceProjectionDTO.class, 
				root.get(Invoice_.id),
				allocatedCompany.get(Company_.coid),
				company.get(Company_.coid),
				root.get(Invoice_.invno),
				root.get(Invoice_.invoiceDate));
	}

	/**
	 * Searches Invoice and returns a list of InvoiceProjectionDTOs matching 
	 * @param allocatedCompanyId : mandatory Integer id of allocated business company
	 * @param issuedBySubdivId : optional Integer id of business subdivision of invoice 
	 *          from business contact (if not a business-company wide search is performed)
	 * @param invoiceStatusId : id of status to query against
	 * @param accountStatus : account status to query
	 * 
	 * At least one of invoiceStatusId or accountStatus must be provided.
	 * 
	 * @return
	 */
	public List<InvoiceProjectionDTO> getProjectionDtos(Integer allocatedCompanyId, Integer issuedBySubdivId, 
			Integer invoiceStatusId, AccountStatus accountStatus) {
		if (allocatedCompanyId == null) 
			throw new IllegalArgumentException("allocatedCompanyId must be provided but was null");
		if ((invoiceStatusId == null) && (accountStatus == null))
			throw new IllegalArgumentException("At least one of invoiceStatusId and accountStatus must be provided but both were null");
		return getResultList(cb -> {
			CriteriaQuery<InvoiceProjectionDTO> cq = cb.createQuery(InvoiceProjectionDTO.class);
			Root<Invoice> root = cq.from(Invoice.class);
			Join<Invoice, Company> allocatedCompany = root.join(Invoice_.ORGANISATION, JoinType.INNER);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(allocatedCompany.get(Company_.coid), allocatedCompanyId));
			if (issuedBySubdivId != null) {
				Join<Invoice, Contact> issuedByContact = root.join(Invoice_.businessContact, JoinType.INNER); 
				Join<Contact, Subdiv> issuedBySubdiv = issuedByContact.join(Contact_.sub, JoinType.INNER);   
				clauses.getExpressions().add(cb.equal(issuedBySubdiv.get(Subdiv_.subdivid), issuedBySubdivId));
			}
			if (invoiceStatusId != null) {
				Join<Invoice, InvoiceStatus> status = root.join(Invoice_.status, JoinType.INNER);
				clauses.getExpressions().add(cb.equal(status.get(InvoiceStatus_.statusid), invoiceStatusId));
			}
			if (accountStatus != null) {
				clauses.getExpressions().add(cb.equal(root.get(Invoice_.accountsStatus), accountStatus));
			}
			
			cq.where(clauses);
			cq.select(getSelection(cb, root, allocatedCompany));
			
			return cq;
		});
	}
	
	
	@Override
	public PagedResultSet<InvoiceDTO> searchSortedInvoiceDTO(InvoiceHomeForm form, String username, PagedResultSet<InvoiceDTO> rs) {
		completePagedResultSet(rs, InvoiceDTO.class, cb -> cq -> {
			Root<Invoice> root = cq.from(Invoice.class);
			Join<Invoice, InvoiceType> type = root.join(Invoice_.type);
			Join<Invoice, Company> companyJoin = root.join(Invoice_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> settings = companyJoin
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation),
					root.get(Invoice_.organisation)));
			Join<Invoice, Address> addrJoin = root.join(Invoice_.address);
			Join<Address, Subdiv> subdivJoin = addrJoin.join(Address_.sub, JoinType.LEFT);
			Join<Invoice, SupportedCurrency> currencyJoin = root.join(Invoice_.currency);

			Subquery<Integer> sq_userrole = cq.subquery(Integer.class);
			Root<UserRole> root_sq = sq_userrole.from(UserRole.class);
			Join<UserRole, User> sq_user = root_sq.join(UserRole_.user);
			Join<UserRole, Subdiv> sq_subdiv = root_sq.join(UserRole_.organisation.getName());
			sq_userrole.where(cb.equal(sq_user.get(User_.username), username));
			sq_userrole.distinct(true);
			
			Predicate clauses = cb.conjunction();
			Predicate orClauses = cb.disjunction();
			
			if (form.getOrgid() != null && form.getOrgid() != 0) {
				Join<Invoice, Company> organisation = root.join(Invoice_.organisation.getName());
				clauses.getExpressions().add(cb.equal(organisation.get(Company_.coid), form.getOrgid()));
			}
			Boolean coIdCrit = form.getCoid() != null && form.getCoid() != 0;
			Boolean coNameCrit = form.getConame() != null && !form.getConame().trim().equals("");
			if (coIdCrit || coNameCrit) {
				if (coIdCrit)
					clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), form.getCoid()));
				else
					clauses.getExpressions()
							.add(cb.equal(cb.locate(companyJoin.get(Company_.coname), form.getConame()), 1));
			}
			
			if (form.getJobno() != null && !form.getJobno().trim().equals("")) {
				Join<Invoice, InvoiceJobLink> jobLinks = root.join(Invoice_.jobLinks);
				clauses.getExpressions()
						.add(cb.greaterThan(cb.locate(jobLinks.get(InvoiceJobLink_.jobno), form.getJobno()), 0));
			}
			
			if (form.getInvno() != null && !form.getInvno().trim().equals(""))
				clauses.getExpressions().add(cb.greaterThan(cb.locate(root.get(Invoice_.invno), form.getInvno()), 0));
			
			if(form.getClientPO() != null && !form.getClientPO().trim().equals("")) {
				Join<Invoice, InvoicePO> invoicePO = root.join(Invoice_.pos, JoinType.LEFT);
				Join<Invoice, InvoiceItem> items = root.join(Invoice_.items, JoinType.LEFT);
				Join<InvoiceItem, InvoiceItemPO> itemsPO = items.join(InvoiceItem_.invItemPOs, JoinType.LEFT);
				Join<InvoiceItemPO, PO> pos = itemsPO.join(InvoiceItemPO_.jobPO, JoinType.LEFT);
				Join<InvoiceItemPO, BPO> bpos = itemsPO.join(InvoiceItemPO_.BPO, JoinType.LEFT);
				// PO managed at Invoice Item PO Level
				orClauses.getExpressions().add(cb.equal(pos.get(PO_.poNumber), form.getClientPO()));
				// BPO managed at Invoice Item BPO Level
				orClauses.getExpressions().add(cb.equal(bpos.get(BPO_.poNumber), form.getClientPO()));
				// Invoice PO managed in the View Invoice screen
				orClauses.getExpressions().add(cb.equal(invoicePO.get(InvoicePO_.poNumber), form.getClientPO()));
				clauses.getExpressions().add(orClauses);
			}
			
			if(form.getIssuedate() != null) {
				clauses.getExpressions().add(cb.between(root.get(Invoice_.issuedate),
					form.getIssuedate(), form.getIssuedate().plusDays(1)));
			}
			
			if (form.getDate() != null) {
				clauses.getExpressions().add(cb.between(root.get(Invoice_.invoiceDate),
					form.getDate(), form.getDate().plusDays(1)));
			}
			if (form.getDateFrom() != null)
				if (form.getDateTo() != null) {
					clauses.getExpressions().add(cb.between(root.get(Invoice_.invoiceDate),
						form.getDateFrom(), form.getDateTo().plusDays(1)));
				} else
					clauses.getExpressions().add(cb.greaterThanOrEqualTo(root.get(Invoice_.invoiceDate),
						form.getDateFrom()));
			if (form.getTypeid() != null) {
				clauses.getExpressions().add(cb.equal(type.get(InvoiceType_.id), form.getTypeid()));
			}
			Join<Invoice, Contact> bcContact = root.join(Invoice_.businessContact);
			Join<Contact, Subdiv> bcSubdiv = bcContact.join(Contact_.sub);
			if (form.getSubdivId() != null && form.getSubdivId() != 0) {
				clauses.getExpressions().add(cb.equal(bcSubdiv.get(Subdiv_.subdivid), form.getSubdivId()));
			} else {
				clauses.getExpressions().add(bcSubdiv.in(sq_userrole.select(sq_subdiv.get(Subdiv_.subdivid))));
			}

			cq.where(clauses);
			CompoundSelection<InvoiceDTO> selection = cb.construct(InvoiceDTO.class, root.get(Invoice_.id), root.get(Invoice_.invno),
				type.get(InvoiceType_.name), root.get(Invoice_.invoiceDate), root.get(Invoice_.regdate), root.get(Invoice_.issuedate), root.get(Invoice_.totalCost),
				companyJoin.get(Company_.coid), companyJoin.get(Company_.coname), settings.get(CompanySettingsForAllocatedCompany_.active),
				settings.get(CompanySettingsForAllocatedCompany_.onStop), subdivJoin.get(Subdiv_.subdivid),
				subdivJoin.get(Subdiv_.subname), subdivJoin.get(Subdiv_.active), currencyJoin.get(SupportedCurrency_.currencyERSymbol),
				currencyJoin.get(SupportedCurrency_.currencyCode));

			List<Order> orders = new ArrayList<>();
			orders.add(cb.desc(root.get(Invoice_.regdate)));
			orders.add(cb.desc(root.get(Invoice_.invno)));
			cq.distinct(true);
			return Triple.of(root.get(Invoice_.id), selection, orders);
		});
		return rs;
	}

	/*
	 * Finds periodic invoices from the start of specified date forwards Sorted
	 * in reverse date order
	 */
	@Override
	public List<Invoice> getInvoicesOfTypeOnAfterDate(Company company, Company allocatedCompany, InvoiceType type,
													  LocalDate startDate) {
		return getResultList(cb -> {
			CriteriaQuery<Invoice> cq = cb.createQuery(Invoice.class);
			Root<Invoice> root = cq.from(Invoice.class);
			Predicate clauses = cb.and(
				cb.equal(root.get(Invoice_.comp), company),
				cb.equal(root.get(Invoice_.organisation), allocatedCompany),
				cb.equal(root.get(Invoice_.type), type),
				cb.greaterThanOrEqualTo(root.get(Invoice_.regdate), startDate));
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(Invoice_.invno)));
			return cq;
		});
	}

	@Override
	public List<InvoiceDTO> getRecentInvoiceKeyValues(Company company, Company allocatedCompany, InvoiceType type,
													  LocalDate fromDate) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceDTO> cq = cb.createQuery(InvoiceDTO.class);
			Root<Invoice> invoice = cq.from(Invoice.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(invoice.get(Invoice_.comp), company));
			clauses.getExpressions().add(cb.equal(invoice.get(Invoice_.organisation), allocatedCompany));
			clauses.getExpressions().add(cb.equal(invoice.get(Invoice_.type), type));
			clauses.getExpressions()
				.add(cb.greaterThanOrEqualTo(invoice.get(Invoice_.regdate), fromDate));
			cq.where(clauses);
			cq.orderBy(cb.desc(invoice.get(Invoice_.invno)));
			cq.select(cb.construct(InvoiceDTO.class, invoice.get(Invoice_.id), invoice.get(Invoice_.invno),
					invoice.get(Invoice_.invoiceDate)));
			return cq;
		});
	}
	
	@Override
	public List<Integer> getJobItemsIdsFromInvoice(Invoice invoice){
		return getResultList(cb -> {
			val cq = cb.createQuery(Integer.class);
			val inv = cq.from(Invoice.class);
			val invoiceItem = inv.join(Invoice_.items);
			val ji = invoiceItem.join(InvoiceItem_.jobItem, JoinType.LEFT);
			cq.where(cb.equal(inv, invoice));
			cq.distinct(true);
			cq.select(ji.get(JobItem_.jobItemId));
			return cq;
		});	
	}
	
	@Override
	public List<Integer> getJobsIdsFromInvoice(Invoice invoice){
		return getResultList(cb -> {
			val cq = cb.createQuery(Integer.class);
			val inv = cq.from(Invoice.class);
			val jobLink = inv.join(Invoice_.jobLinks);
			val job = jobLink.join(InvoiceJobLink_.job, JoinType.LEFT);
			cq.where(cb.equal(inv, invoice));
			cq.distinct(true);
			cq.select(job.get(Job_.jobid));
			return cq;
		});	
	}
}