package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.dto.PurchaseOrderStatusProjectionDTO;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository("PurchaseOrderStatusDao")
public class PurchaseOrderStatusDaoImpl extends BaseDaoImpl<PurchaseOrderStatus, Integer>
		implements PurchaseOrderStatusDao {

	@Override
	protected Class<PurchaseOrderStatus> getEntity() {
		return PurchaseOrderStatus.class;
	}

	@Override
	public List<PurchaseOrderStatus> findAllPurchaseOrderStatusRequiringAttention() {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrderStatus> cq = cb.createQuery(PurchaseOrderStatus.class);
			Root<PurchaseOrderStatus> poStatus = cq.from(PurchaseOrderStatus.class);
			cq.where(cb.isTrue(poStatus.get(PurchaseOrderStatus_.requiresAttention)));
			cq.orderBy(cb.asc(poStatus.get(PurchaseOrderStatus_.statusid)));
			return cq;
		});
	}

	private PurchaseOrderStatus findByPropertyAndActionType(SingularAttribute<PurchaseOrderStatus, Boolean> property,
			ActionType type) {
		return getSingleResult(cb -> {
			CriteriaQuery<PurchaseOrderStatus> cq = cb.createQuery(PurchaseOrderStatus.class);
			Root<PurchaseOrderStatus> poStatus = cq.from(PurchaseOrderStatus.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(poStatus.get(property)));
			clauses.getExpressions().add(cb.equal(poStatus.get(PurchaseOrderStatus_.type), type));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public PurchaseOrderStatus findOnCancelPurchaseOrderStatus(ActionType type) {
		return findByPropertyAndActionType(PurchaseOrderStatus_.onCancel, type);
	}

	@Override
	public PurchaseOrderStatus findOnCompletePurchaseOrderStatus(ActionType type) {
		return findByPropertyAndActionType(PurchaseOrderStatus_.onComplete, type);
	}

	@Override
	public PurchaseOrderStatus findOnInsertPurchaseOrderStatus(ActionType type) {
		return findByPropertyAndActionType(PurchaseOrderStatus_.onInsert, type);
	}

	@Override
	public Integer findPurchaseOrderStatusIdRequiringAttentionByName(String name) {
		return getSingleResult(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<PurchaseOrderStatus> poStatus = cq.from(PurchaseOrderStatus.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(poStatus.get(PurchaseOrderStatus_.requiresAttention)));
			clauses.getExpressions().add(cb.equal(poStatus.get(PurchaseOrderStatus_.name), name));
			cq.where(clauses);
			cq.select(poStatus.get(PurchaseOrderStatus_.statusid));
			return cq;
		});
	}

	@Override
	public List<PurchaseOrderStatusProjectionDTO> findAllPurchaseOrderStatusRequiringAttentionProjection(
			Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrderStatusProjectionDTO> cq = cb.createQuery(PurchaseOrderStatusProjectionDTO.class);
			Root<PurchaseOrderStatus> poStatus = cq.from(PurchaseOrderStatus.class);
			Join<PurchaseOrderStatus, Translation> nameTranslation = poStatus
					.join(PurchaseOrderStatus_.nametranslations,JoinType.LEFT);
			nameTranslation.on(cb.equal(nameTranslation.get(Translation_.locale), locale));
			Join<PurchaseOrderStatus, Translation> descTranslation = poStatus
					.join(PurchaseOrderStatus_.descriptiontranslations,JoinType.LEFT);
			descTranslation.on(cb.equal(descTranslation.get(Translation_.locale), locale));
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(poStatus.get(PurchaseOrderStatus_.requiresAttention)));
			cq.orderBy(cb.asc(poStatus.get(PurchaseOrderStatus_.statusorder)));
			cq.where(clauses);
			cq.select(cb.construct(PurchaseOrderStatusProjectionDTO.class, poStatus.get(PurchaseOrderStatus_.statusid),
					poStatus.get(PurchaseOrderStatus_.name), poStatus.get(PurchaseOrderStatus_.description),
					nameTranslation.get(Translation_.translation), descTranslation.get(Translation_.translation),
					poStatus.get(PurchaseOrderStatus_.defaultStatus),
					poStatus.get(PurchaseOrderStatus_.requiresAttention),
					poStatus.get(PurchaseOrderStatus_.next).get(PurchaseOrderStatus_.statusid)));
			return cq;
		});
	}
}