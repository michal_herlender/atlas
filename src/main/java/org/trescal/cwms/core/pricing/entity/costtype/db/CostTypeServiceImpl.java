package org.trescal.cwms.core.pricing.entity.costtype.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.spring.model.KeyValue;

@Service("CostTypeService")
public class CostTypeServiceImpl implements CostTypeService
{
	@Override
	public List<CostType> getAllActiveCostTypes()
	{
		List<CostType> result = new ArrayList<CostType>();
		result.add(CostType.CALIBRATION);
		result.add(CostType.ADJUSTMENT);
		result.add(CostType.REPAIR);
		result.add(CostType.PURCHASE);
		result.add(CostType.INSPECTION);
		return result;
	}
	@Override
	public List<CostType> getRemainingCostTypes(Collection<CostType> excludedTypes) {
		List<CostType> result = getAllActiveCostTypes();
		result.removeAll(excludedTypes);
		return result;
	}
	/*
	 * Called by DWR
	 */
	@Override
	public List<KeyValue<Integer, String>> getAllActiveCostTypeDtos() {
		return this.getAllActiveCostTypes().stream().map(ct -> new KeyValue<Integer, String>(ct.getTypeid(), ct.getMessage())).collect(Collectors.toList());
	}
	@Override
	public List<KeyValue<Integer, String>> getRemainingCostTypeDtos(Collection<CostType> excludedCostTypes) {
		return this.getRemainingCostTypes(excludedCostTypes).stream().map(ct -> new KeyValue<Integer, String>(ct.getTypeid(), ct.getMessage())).collect(Collectors.toList());
	}
}