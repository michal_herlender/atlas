package org.trescal.cwms.core.pricing.purchaseorder.dto;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * Convenience dto used to submit data concerning {@link PurchaseOrderItem}
 * updates (cancelled, received etc).
 * 
 * @author Richard
 */
@Getter @Setter
public class PurchaseOrderItemProgressDTO
{
	private boolean accountsApproved;
	private boolean cancelled;
	private boolean goodsApproved;
	private int itemid;
	String progressDescription;
	private PurchaseOrderItemReceiptStatus receiptStatus;
	private boolean received;

	public PurchaseOrderItemProgressDTO()
	{
	}

	public PurchaseOrderItemProgressDTO(int itemid, boolean received, boolean cancelled)
	{
		super();
		this.itemid = itemid;
		this.received = received;
		this.cancelled = cancelled;
	}

	public PurchaseOrderItemProgressDTO(int itemid, boolean cancelled, PurchaseOrderItemReceiptStatus receiptStatus, boolean accountsApproved, boolean goodsApproved)
	{
		super();
		this.itemid = itemid;
		this.cancelled = cancelled;
		this.receiptStatus = receiptStatus;
		this.accountsApproved = accountsApproved;
		this.goodsApproved = goodsApproved;
	}

}
