package org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.db;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewLinkedPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostServiceSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingLinkedPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Service("JobCostingPurchaseCostServiceImpl")
public class JobCostingPurchaseCostServiceImpl extends JobCostingCostServiceSupport<JobCostingPurchaseCost> implements JobCostingPurchaseCostService {
    @Autowired
    private JobCostingPurchaseCostDao JobCostingPurchaseCostDao;
    @Value("${cwms.config.costing.costs.lookuphierarchy.purchase}")
    private String costSourceHierarchy;
    @Value("${cwms.config.costing.costs.lookupcosts.purchase}")
    private boolean lookupCosts;
    @Value("${cwms.config.costing.costs.deactivateonzero.purchase}")
    protected boolean deactivateCostOnZeroValue;

    @Override
    public boolean getLookupCosts() {
        return lookupCosts;
    }

    @Override
    public boolean getDeactivateCostOnZeroValue() {
        return deactivateCostOnZeroValue;
    }

    @Override
    public String getCostSourceHierarchy() {
        return costSourceHierarchy;
    }

    @Override
    public void setCostSourceHierarchy(String costSourceHierarchy) {
        this.costSourceHierarchy = costSourceHierarchy;
    }

	@Override
	public Cost createCostFromCopy(JobCostingItem ji, JobCostingItem old)
	{
		JobCostingPurchaseCost newCost = null;

		if ((old != null) && (old.getPurchaseCost() != null))
		{
			newCost = new JobCostingPurchaseCost();

			JobCostingPurchaseCost oldCost = old.getPurchaseCost();
			BeanUtils.copyProperties(oldCost, newCost);

			newCost.setCostid(null);
			newCost.setJobCostingItem(ji);
			newCost.setAutoSet(true);

			if (oldCost.getLinkedCost() != null)
			{
				JobCostingLinkedPurchaseCost oldLinkedCost = oldCost.getLinkedCost();
				JobCostingLinkedPurchaseCost newLinkedCost = new JobCostingLinkedPurchaseCost();
				BeanUtils.copyProperties(oldLinkedCost, newLinkedCost);
				newLinkedCost.setId(0);
				newLinkedCost.setPurchaseCost(newCost);
				newCost.setLinkedCost(newLinkedCost);
			}
		}
		return newCost;
	}

	public void deleteJobCostingPurchaseCost(JobCostingPurchaseCost jobcostingpurchasecost)
	{
		this.JobCostingPurchaseCostDao.remove(jobcostingpurchasecost);
	}

	public JobCostingPurchaseCost findJobCostingPurchaseCost(int id)
	{
		return this.JobCostingPurchaseCostDao.find(id);
	}

	@Override
	public List<JobCostingPurchaseCost> findMatchingCosts(Integer plantid, Integer coid, int modelid, Integer caltypeid, Integer page, Integer resPerPage)
	{
		return this.JobCostingPurchaseCostDao.findMatchingCosts(JobCostingPurchaseCost.class, plantid, coid, modelid, caltypeid, this.resultsYearFilter, page, resPerPage);
	}

	public List<JobCostingPurchaseCost> getAllJobCostingPurchaseCosts()
	{
		return this.JobCostingPurchaseCostDao.findAll();
	}

	public void insertJobCostingPurchaseCost(JobCostingPurchaseCost JobCostingPurchaseCost)
	{
		this.JobCostingPurchaseCostDao.persist(JobCostingPurchaseCost);
	}

	@Override
	public Cost resolveCostLookup(CostSource source, JobCostingItem jci)
	{
		JobCostingPurchaseCost cost = null;

		boolean costFound = false;

		this.logger.info("loading job costing purchase costs from " + source);
		switch (source)
		{
			case CONTRACT_REVIEW:
				this.logger.info("Attempting to set costs using " + source
						+ " strategy");

				JobItem ji = jci.getJobItem();
				if (ji.getPurchaseCost() != null)
				{
					ContractReviewPurchaseCost purCost = ji.getPurchaseCost();
					cost = new JobCostingPurchaseCost();

					cost.setCostSrc(purCost.getCostSrc());

					// use the costs set at contract review
					cost.setDiscountRate(purCost.getDiscountRate());
					cost.setDiscountValue(purCost.getDiscountValue());
					cost.setTotalCost(purCost.getTotalCost());
					cost.setFinalCost(purCost.getFinalCost());

					// set empty third party costs
					this.configureTPCosts(cost);

					costFound = true;

					// if a linked cost is being used, link the contract review
					// linked cost to the jobcosting linked cost
					if (ji.getPurchaseCost().getLinkedCost() != null)
					{
						ContractReviewLinkedPurchaseCost cr = ji.getPurchaseCost().getLinkedCost();

						JobCostingLinkedPurchaseCost lc = new JobCostingLinkedPurchaseCost();
						lc.setPurchaseCost(cost);
						if (purCost.getCostSrc() == CostSource.JOB_COSTING)
						{
							lc.setJobCostingPurchaseCost(cr.getJobCostingPurchaseCost());
						}
						else if (purCost.getCostSrc() == CostSource.QUOTATION)
						{
							lc.setQuotationPurchaseCost(cr.getQuotationPurchaseCost());
						}
					}
				}
				else
				{
					this.logger.info("Failed setting costs using " + source
							+ " strategy, no costs found");
				}
				break;
			case JOB:
				this.logger.info(source + " costs are not yet implemented");
				break;
			case JOB_COSTING:
				this.logger.info(source + " costs are not yet implemented");
				break;
			case QUOTATION:
				this.logger.info(source + " costs are not supported");
				break;
			case INSTRUMENT:
				this.logger.info(source + " costs are not supported");
				break;
			case MODEL:
				this.logger.info(source + " costs are not supported");
				break;
			case MANUAL:
				cost = new JobCostingPurchaseCost();
				cost.setDiscountRate(new BigDecimal(0.00));
				cost.setDiscountValue(new BigDecimal(0.00));
				cost.setHouseCost(new BigDecimal(0.00));
				cost.setFinalCost(new BigDecimal(0.00));
				cost.setTotalCost(new BigDecimal(0.00));
				cost.setCostSrc(CostSource.MANUAL);

				// set empty third party costs
				this.configureTPCosts(cost);

				costFound = true;
				break;
		case DERIVED:
			break;
		default:
			break;
		}

		if (costFound)
		{
			cost.setActive(true);
			cost.setAutoSet(true);
			cost.setJobCostingItem(jci);
			cost.setCostType(CostType.PURCHASE);
		}

		return cost;
	}

	public void saveOrUpdateJobCostingPurchaseCost(JobCostingPurchaseCost jobcostingpurchasecost)
	{
		this.JobCostingPurchaseCostDao.saveOrUpdate(jobcostingpurchasecost);
	}

	@Override
	public JobCostingPurchaseCost updateCosts(JobCostingPurchaseCost cost)
	{
		if (cost.getHouseCost() == null)
		{
			cost.setHouseCost(new BigDecimal("0.00"));
		}
		if (cost.getThirdCostTotal() == null)
		{
			cost.setThirdCostTotal(new BigDecimal("0.00"));
		}

		// calculate TP costs
		cost = (JobCostingPurchaseCost) this.thirdCostService.recalculateThirdPartyCosts(cost);

		cost.setTotalCost(cost.getHouseCost().add(cost.getThirdCostTotal()));

		// delegate to CostCalculator to apply any discounts and set the final
		// cost
		CostCalculator.updateSingleCost(cost, this.roundUpFinalCosts);
		return cost;
	}

	public void updateJobCostingPurchaseCost(JobCostingPurchaseCost JobCostingPurchaseCost)
	{
		this.JobCostingPurchaseCostDao.update(JobCostingPurchaseCost);
	}

}