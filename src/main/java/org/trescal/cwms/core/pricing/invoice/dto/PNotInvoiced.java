package org.trescal.cwms.core.pricing.invoice.dto;

import javax.validation.constraints.Size;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.AbstractJobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;

/**
 * Transfer object for "Not Invoiced" information for a job item Intended for
 * use in both Invoicing and Job screens (common functionality and validation)
 * 
 * @author Galen
 *
 */
public class PNotInvoiced {

	private boolean selected; // Whether invoice item should be excluded from all future invoicing
	private NotInvoicedReason reason; // If notInvoiced, reason that invoice is being excluded
	private String comments; // If notInvoiced, remarks regarding exclusion
	private int periodicInvoiceId; // The periodic invoice ID for reconciliation, when choosing Contract as a
									// non-invoiced reason

	public PNotInvoiced(AbstractJobItemNotInvoiced jini) {
		if (jini != null) {
			this.selected = true;
			this.reason = jini.getReason();
			this.comments = jini.getComments();
			if (jini.getPeriodicInvoice() != null) {
				this.periodicInvoiceId = jini.getPeriodicInvoice().getId();
			} else {
				this.periodicInvoiceId = 0;
			}
		} else {
			this.selected = false;
			this.reason = NotInvoicedReason.UNDEFINED;
			this.comments = "";
			this.periodicInvoiceId = 0;
		}
	}

	public boolean isSelected() {
		return selected;
	}

	public NotInvoicedReason getReason() {
		return reason;
	}

	@Size(min = 0, max = 100)
	public String getComments() {
		return comments;
	}

	public int getPeriodicInvoiceId() {
		return periodicInvoiceId;
	}

	public void setSelected(boolean notInvoiced) {
		this.selected = notInvoiced;
	}

	public void setReason(NotInvoicedReason notInvoicedReason) {
		this.reason = notInvoicedReason;
	}

	public void setComments(String notInvoicedComments) {
		this.comments = notInvoicedComments;
	}

	public void setPeriodicInvoiceId(int periodicInvoiceId) {
		this.periodicInvoiceId = periodicInvoiceId;
	}
}
