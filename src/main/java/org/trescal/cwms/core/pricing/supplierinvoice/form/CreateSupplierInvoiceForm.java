package org.trescal.cwms.core.pricing.supplierinvoice.form;

import javax.validation.constraints.NotNull;

public class CreateSupplierInvoiceForm extends SupplierInvoiceForm {
	
	@NotNull(message="{error.value.notselected}")
	private Boolean linkAllItems;
	
	public Boolean getLinkAllItems() {
		return linkAllItems;
	}
	public void setLinkAllItems(Boolean linkAllItems) {
		this.linkAllItems = linkAllItems;
	}
}
