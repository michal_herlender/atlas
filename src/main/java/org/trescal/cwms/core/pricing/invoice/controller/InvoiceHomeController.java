package org.trescal.cwms.core.pricing.invoice.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceHomeWrapper;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPoLinkDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceJobLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.db.InvoiceJobLinkService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.db.InvoiceStatusService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db.InvoiceTypeService;
import org.trescal.cwms.core.pricing.invoice.form.InvoiceHomeForm;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand.INV_LOAD_COMPANY;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand.INV_LOAD_JOB_LINKS;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionService;
import org.trescal.cwms.core.pricing.invoice.view.InvoiceSearchXlsxView;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Controller @IntranetController
@RequestMapping("/invoicehome.htm")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class InvoiceHomeController {
	
	protected final static Logger logger = LoggerFactory.getLogger(InvoiceHomeController.class);
	
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private InvoiceProjectionService invoiceProjectionService;
	@Autowired
	private InvoiceStatusService invoiceStatusService;
	@Autowired
	private InvoiceTypeService invoiceTypeService;
	@Autowired
	private CreditNoteService creditNoteService;
	@Autowired
	InvoiceItemService invoiceItemService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private TranslationService translationService; 
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private InvoiceJobLinkService invoiceJobLinkService;
	@Autowired
	private InvoiceSearchXlsxView invoiceSearchXlsxView;
	@Value("${cwms.config.web.invoicesearch.size.restrictexport}")
	private long sizeRestrictExport;
	
	public static final String VIEW_NAME = "trescal/core/pricing/invoice/invoicehome";
	public static final String RESULTS_NAME = "trescal/core/pricing/invoice/invoiceresults";
	public static final String FORM_NAME = "invoicehomeform";

	@ModelAttribute(FORM_NAME)
	public InvoiceHomeForm createInvoiceHomeForm(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		InvoiceHomeForm form = new InvoiceHomeForm();
		form.setOrgid(companyDto.getKey());
		form.setResultsPerPage(Constants.RESULTS_PER_PAGE);
		form.setPageNo(1);
		return form;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@RequestMapping(method = RequestMethod.GET)
	public String referenceData(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value="companyWide", required=false, defaultValue="false") Boolean companyWide,
			Locale locale)
	{
        model.addAttribute("activSubdivs", this.subdivService.getAllActiveCompanySubdivsForUser(this.userService.get(username), companyDto.getKey()));
        model.addAttribute("invtypes", invoiceTypeService.getAll());
        model.addAttribute("defaultSubdivId", subdivDto.getKey());
        model.addAttribute("interestingItems", getInterestingItems(companyDto.getKey(), subdivDto, companyWide, locale));
        model.addAttribute("companyWide", companyWide);
        return VIEW_NAME;
    }
	
	@RequestMapping(method = RequestMethod.POST, params="!export")
	public String onSubmit(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value="companyWide", required=false, defaultValue="false") Boolean companyWide,
			@Validated @ModelAttribute(FORM_NAME) InvoiceHomeForm invoiceHomeForm,
			BindingResult result, Locale locale) throws Exception {
		if (result.hasErrors()) {
            return referenceData(model, companyDto, subdivDto, username, companyWide, locale);
        }
		else {
            // perform the search
            PagedResultSet<InvoiceDTO> rs = this.invoiceService.searchSortedInvoiceDTO(invoiceHomeForm, username, new PagedResultSet<InvoiceDTO>(invoiceHomeForm.getResultsPerPage(), invoiceHomeForm.getPageNo()));
            invoiceHomeForm.setResultsTotal(rs.getResultsCount());
            invoiceHomeForm.setRs(rs);
            completeInvoiceResultData(rs.getResults());
            invoiceHomeForm.setInvoices(rs.getResults());
            model.addAttribute(FORM_NAME, invoiceHomeForm);
            return RESULTS_NAME;
        }
	}
	
	@RequestMapping(method = RequestMethod.POST, params="export")
	public ModelAndView onExport(@Validated @ModelAttribute(FORM_NAME) InvoiceHomeForm invoiceHomeForm,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		if (invoiceHomeForm.getResultsTotal() >= sizeRestrictExport) {
			throw new RuntimeException("This search has " + invoiceHomeForm.getResultsTotal() + " invoice results and due to temporary performance restrictions, searches with " + sizeRestrictExport + " invoice results or more cannot currently be exported.");
		}
		else {
            // perform the search
            PagedResultSet<InvoiceDTO> rs = this.invoiceService.searchSortedInvoiceDTO(invoiceHomeForm, username, new PagedResultSet<InvoiceDTO>(invoiceHomeForm.getResultsTotal(), 0));
            invoiceHomeForm.setRs(rs);
            completeInvoiceResultData(rs.getResults());
            Collection<InvoiceDTO> invoices = rs.getResults();
            return new ModelAndView(invoiceSearchXlsxView, "invoices", invoices);
        }
	}

    /**
     * Moved from Service layer.
     */
    private List<InvoiceHomeWrapper<InvoiceProjectionDTO>> getInterestingItems(
        Integer allocatedCompanyId, KeyValue<Integer, String> allocatedSubdivDto, boolean companyWide, Locale locale) {
        Integer allocatedSubdivId = companyWide ? null : allocatedSubdivDto.getKey();

        List<InvoiceHomeWrapper<InvoiceProjectionDTO>> res = new ArrayList<>();

        for (InvoiceStatus status : this.invoiceStatusService.findAllInvoiceStatusRequiringAttention()) {
            res.add(getInvoiceProjectionStatusWrapper(allocatedCompanyId, allocatedSubdivId, status, locale));
        }
        res.add(getPendingSyncWrapper(allocatedCompanyId, allocatedSubdivId, locale));
        res.add(getErrorSyncWrapper(allocatedCompanyId, allocatedSubdivId, locale));
        return res;
    }

	private InvoiceHomeWrapper<InvoiceProjectionDTO> getPendingSyncWrapper(Integer allocatedCompanyId, Integer allocatedSubdivId, Locale locale) {
		String wrapperId = "pendingsync";
		// Name used for tab title, description for body
		String codeName = "invoice.pendingsync";
		String defaultName = "Pending synchronisation";
		String codeDescription = "invoice.pendingsynchronisationwithaccountingsoftware";
		String defaultDescription = "Pending synchronisation with accounting software";
		return getProjectionAccountStatusWrapper(allocatedCompanyId, allocatedSubdivId, AccountStatus.P, locale, wrapperId, 
				codeName, defaultName, codeDescription, defaultDescription); 
	}
	private InvoiceHomeWrapper<InvoiceProjectionDTO> getErrorSyncWrapper(Integer allocatedCompanyId, Integer allocatedSubdivId, Locale locale) {
		String wrapperId = "brokeringerrors";
		// Name used for tab title, description for body
		String codeName = "invoice.errorssynchronising";
		String defaultName = "Errors synchronising";
		String codeDescription = "invoice.errorssynchronisingwithaccountingsoftware";
		String defaultDescription = "Errors synchronising with accounting software";
		return getProjectionAccountStatusWrapper(allocatedCompanyId, allocatedSubdivId, AccountStatus.E, locale, wrapperId, 
				codeName, defaultName, codeDescription, defaultDescription); 
	}
	private InvoiceHomeWrapper<InvoiceProjectionDTO> getProjectionAccountStatusWrapper(Integer allocatedCompanyId, Integer allocatedSubdivId, 
			AccountStatus accountStatus, Locale locale, String wrapperId, String codeName, String defaultName, 
			String codeDescription, String defaultDescription) {
		InvoiceProjectionCommand command = new InvoiceProjectionCommand(locale, allocatedCompanyId, null, null, 
				INV_LOAD_COMPANY.class,
				INV_LOAD_JOB_LINKS.class);
		
		InvoiceHomeWrapper<InvoiceProjectionDTO> wrapper = new InvoiceHomeWrapper<>();
		wrapper.setId(wrapperId);
		wrapper.setName(this.messages.getMessage(codeName, null, defaultName, locale));
		wrapper.setDescription(this.messages.getMessage(codeDescription, null, defaultDescription, locale));
		wrapper.setContents(this.invoiceProjectionService.getInvoiceProjectionDTOs(allocatedCompanyId, allocatedSubdivId, null, accountStatus, command));
		wrapper.setContentsInvoiceProjection(true);
		wrapper.setQuantity(wrapper.getContents().size());
		return wrapper;
	}
	
	private InvoiceHomeWrapper<InvoiceProjectionDTO> getInvoiceProjectionStatusWrapper(Integer allocatedCompanyId, Integer allocatedSubdivId, InvoiceStatus status, Locale locale) {
		InvoiceProjectionCommand command = new InvoiceProjectionCommand(locale, allocatedCompanyId, null, null, 
				INV_LOAD_COMPANY.class,
				INV_LOAD_JOB_LINKS.class);

		String nameTranslation = StringTools.wordToTitleCase(translationService.getCorrectTranslation(status.getNametranslations(), locale));
		String descriptionTranslation = translationService.getCorrectTranslation(status.getDescriptiontranslations(), locale);

		InvoiceHomeWrapper<InvoiceProjectionDTO> wrapper = new InvoiceHomeWrapper<>();
		wrapper.setId(StringTools.toLowerNoSpaceCase(status.getName()));
		wrapper.setName(nameTranslation);
		wrapper.setDescription(descriptionTranslation);
		wrapper.setContents(this.invoiceProjectionService.getInvoiceProjectionDTOs(allocatedCompanyId, allocatedSubdivId, status.getStatusid(), null, command));
		wrapper.setContentsInvoiceProjection(true);
		wrapper.setQuantity(wrapper.getContents().size());
		return wrapper;
	}

	private void completeInvoiceResultData(Collection<InvoiceDTO> invoiceResult) {
		if (!invoiceResult.isEmpty()) {
			List<Integer> invoiceIds = new ArrayList<>();
			invoiceResult.stream().forEach(inv -> invoiceIds.add(inv.getId()));

			Map<Integer, List<InvoiceJobLinkDTO>> invoiceJobLinks = invoiceJobLinkService.getLinksByIds(invoiceIds)
					.stream().collect(Collectors.groupingBy(InvoiceJobLinkDTO::getInvoiceid));

			Map<Integer, List<InvoiceItemPoLinkDTO>> invoicePoLinks = invoiceItemService.getPoLinks(invoiceIds).stream()
					.collect(Collectors.groupingBy(InvoiceItemPoLinkDTO::getInvoiceId));
			
			List<KeyValueIntegerString> creditNoteNumbersPerInvoiceIds = creditNoteService.getNumbersPerInvoices(invoiceIds);
			
			Map<Integer, List<KeyValueIntegerString>> creditNoteNumberMap = creditNoteNumbersPerInvoiceIds.stream().collect(Collectors.groupingBy(KeyValueIntegerString::getKey));
			

			for (InvoiceDTO invoiceDto : invoiceResult) {
				List<InvoiceJobLinkDTO> jobLinks = invoiceJobLinks.get(invoiceDto.getId());
				invoiceDto.setInvoiceJobLinksDto(jobLinks);
				
				List<InvoiceItemPoLinkDTO> invoiceItemPoLinks = invoicePoLinks.get(invoiceDto.getId());
				invoiceDto.setInvoiceItemPoLinkDTO(invoiceItemPoLinks);
				
				if (creditNoteNumberMap.get(invoiceDto.getId()) != null )
				{
					invoiceDto.setCreditNoteNumbers(creditNoteNumberMap.get(invoiceDto.getId()).stream().map(KeyValueIntegerString::getValue).collect(Collectors.toList()));
				}
				
			}
		}
	}

}