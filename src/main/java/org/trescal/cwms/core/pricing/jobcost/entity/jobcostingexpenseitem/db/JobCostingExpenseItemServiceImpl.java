package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingExpenseItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.form.JobCostingExpenseItemForm;

@Service
public class JobCostingExpenseItemServiceImpl extends BaseServiceImpl<JobCostingExpenseItem, Integer>
		implements JobCostingExpenseItemService {

	@Autowired
	private JobCostingExpenseItemDao jobCostingExpenseItemDao;

	@Override
	protected BaseDao<JobCostingExpenseItem, Integer> getBaseDao() {
		return jobCostingExpenseItemDao;
	}

	@Override
	public JobCostingExpenseItemForm getFormObject(Integer id) {
		Locale locale = LocaleContextHolder.getLocale();
		return jobCostingExpenseItemDao.getFormObject(id, locale);
	}

	@Override
	public List<JobCostingExpenseItemDto> getJobCostingExpenseItemsDtoByJobCostingId(Integer jobCostingId,
			Locale locale) {
		return this.jobCostingExpenseItemDao.getJobCostingExpenseItemsDtoByJobCostingId(jobCostingId, locale);
	}
}