package org.trescal.cwms.core.pricing.purchaseorder.form;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

@Component
public class PurchaseOrderFormAccountsValidator implements Validator {

	public static String MESSAGE_CODE_REQUIRES_GOODS_APPROVAL = "error.accountsapproval.requiresgoodsapproval";
	public static String MESSAGE_REQUIRES_GOODS_APPROVAL = "Item {0} requires either goods approval or cancellation to perform accounts approval";
	
	public static String MESSAGE_CODE_REQUIRES_INVOICE = "error.accountsapproval.requiresinvoice";
	public static String MESSAGE_REQUIRES_INVOICE = "Item {0} has goods approval so requires an invoice to be linked to perform accounts approval";
	
	public static String MESSAGE_CODE_REQUIRES_NO_INVOICE = "error.accountsapproval.requiresnoinvoice";
	public static String MESSAGE_REQUIRES_NO_INVOICE = "Item {0} is cancelled so must have no invoice linked to perform accounts approval";
	
	@Autowired
	private PurchaseOrderService purchaseOrderService; 
	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(PurchaseOrderForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PurchaseOrderForm form = (PurchaseOrderForm) target;
		PurchaseOrder order = this.purchaseOrderService.find(form.getPoid());
		List<PurchaseOrderItem> itemList = new ArrayList<>(order.getItems());
		for (int i = 0; i < order.getItems().size(); i++) {
			PurchaseOrderItem item = itemList.get(i);
			if (form.getAccountsApproval().get(i)) {
				if (!item.isGoodsApproved() && !item.isCancelled()) {
					errors.reject(MESSAGE_CODE_REQUIRES_GOODS_APPROVAL, new Object[] {item.getItemno()}, MESSAGE_REQUIRES_GOODS_APPROVAL);
				}
				if (item.isGoodsApproved() && (form.getSupplierInvoiceIds().get(i) == 0)) {
					errors.reject(MESSAGE_CODE_REQUIRES_INVOICE, new Object[] {item.getItemno()}, MESSAGE_REQUIRES_INVOICE);
				}
				if (item.isCancelled() && (form.getSupplierInvoiceIds().get(i) != 0)) {
					errors.reject(MESSAGE_CODE_REQUIRES_NO_INVOICE, new Object[] {item.getItemno()}, MESSAGE_REQUIRES_NO_INVOICE);
				}
			}
		}
	}
}
