package org.trescal.cwms.core.pricing.jobcost.dto;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;

public class ModelJobCostingCalibrationCost
{
	private JobCostingCalibrationCost calibrationCost;
	private int costid;
	private InstrumentModel instrumentModel;
	private JobItem ji;
	private int modelid;
	
	public ModelJobCostingCalibrationCost(Integer modelId, JobCostingCalibrationCost calibrationCost) {
		this.modelid = modelId;
		this.calibrationCost = calibrationCost;
		this.costid = calibrationCost.getCostid();
	}
	
	public ModelJobCostingCalibrationCost(int costid, int modelid, JobCostingCalibrationCost calibrationCost, InstrumentModel instrumentModel, JobItem ji)
	{
		this.costid = costid;
		this.modelid = modelid;
		this.instrumentModel = instrumentModel;
		this.calibrationCost = calibrationCost;
		this.ji = ji;
	}

	public ModelJobCostingCalibrationCost(JobCostingCalibrationCost jccc)
	{
		this.calibrationCost = jccc;
		this.costid = jccc.getCostid();
		this.ji = jccc.getJobCostingItem().getJobItem();
		this.instrumentModel = this.ji.getInst().getModel();
		this.modelid = this.instrumentModel.getModelid();
	}

	public JobCostingCalibrationCost getCalibrationCost()
	{
		return this.calibrationCost;
	}

	public int getCostid()
	{
		return this.costid;
	}

	public InstrumentModel getInstrumentModel()
	{
		return this.instrumentModel;
	}

	public JobItem getJi()
	{
		return this.ji;
	}

	public int getModelid()
	{
		return this.modelid;
	}

	public void setCalibrationCost(JobCostingCalibrationCost calibrationCost)
	{
		this.calibrationCost = calibrationCost;
	}

	public void setCostid(int costid)
	{
		this.costid = costid;
	}

	public void setInstrumentModel(InstrumentModel instrumentModel)
	{
		this.instrumentModel = instrumentModel;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}

	public void setModelid(int modelid)
	{
		this.modelid = modelid;
	}

}
