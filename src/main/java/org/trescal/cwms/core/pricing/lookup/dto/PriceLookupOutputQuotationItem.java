package org.trescal.cwms.core.pricing.lookup.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class PriceLookupOutputQuotationItem extends PriceLookupOutput {
	// For quotation item projection
    public PriceLookupOutputQuotationItem(Integer serviceTypeId, Integer instrumentId, Integer instrumentModelId,
                                          Integer salesCategoryId, Integer quotationItemId, Integer quotationCalCostId,
                                          BigDecimal quotationCalCostDiscountRate, BigDecimal quotationCalCostTotalCost, Integer quotationId,
                                          Boolean quotationAccepted, LocalDate quotationExpiryDate, Boolean quotationIssued,
                                          LocalDate quotationIssuedDate, Integer supportedCurrencyId) {
        super();
        // Inputs
        this.serviceTypeId = serviceTypeId;
        this.instrumentId = instrumentId;
        this.instrumentModelId = instrumentModelId;
        this.salesCategoryId = salesCategoryId;
        // Outputs (remainder null for quotation item result)
        this.quotationItemId = quotationItemId;
        this.quotationCalCostId = quotationCalCostId;
        this.quotationCalCostDiscountRate = quotationCalCostDiscountRate;
		this.quotationCalCostTotalCost = quotationCalCostTotalCost;
		this.quotationId = quotationId;
		this.quotationAccepted = quotationAccepted;
		this.quotationExpiryDate = quotationExpiryDate;
		this.quotationIssued = quotationIssued;
		this.quotationIssuedDate = quotationIssuedDate;
		this.supportedCurrencyId = supportedCurrencyId;
	}

}
