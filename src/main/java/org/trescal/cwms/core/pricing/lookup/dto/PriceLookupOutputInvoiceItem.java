package org.trescal.cwms.core.pricing.lookup.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class PriceLookupOutputInvoiceItem extends PriceLookupOutput {

	public PriceLookupOutputInvoiceItem(Integer invoiceItemId, Integer invoiceId, 
			String invoiceNo, Integer itemno, LocalDate issueDate, 
			BigDecimal generalDiscountRate, String currencyInvoice, BigDecimal finalPriceForInvoiceItem) {
		super();
		this.invoiceItemId = invoiceItemId;
		this.invoiceId = invoiceId;
		this.invoiceNo = invoiceNo;
		this.itemno = itemno;
		this.issueDate = issueDate;
		this.generalDiscountRate = generalDiscountRate;
		this.currencyInvoice = currencyInvoice;
		this.finalPriceForInvoiceItem = finalPriceForInvoiceItem;
	}
	
}
