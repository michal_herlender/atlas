package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;

import java.util.List;

public interface InvoiceItemPOService {

	ResultWrapper ajaxAddPOLink(int invItemId, String type, int poId);

	/**
	 * this method links or un-links a purchase order to multiple invoice items
	 * 
	 * @param type
	 *            the type of invoice po (i.e. 'jobpo', 'jobbpo', 'invpo')
	 * @param invItemIds
	 *            string array of invoice item ids to be updated
	 * @param poId
	 *            id of the purchase order to link items to
	 * @param jobid
	 *            the id of a {@link Job} to retrict results when fetching
	 *            invoice items to be updated (optional)
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper assignInvoiceItemsToPO(String type, int[] invItemIds, int poId, Integer jobid,Integer invId);

	void deleteInvoiceItemPO(InvoiceItemPO iipo);

	InvoiceItemPO findInvoiceItemPO(int id);

	List<InvoiceItemPO> getAllInvoiceItemPOs();

	void insertInvoiceItemPO(InvoiceItemPO iipo);

	/**
	 * Persists links for the InvoiceItem to any of the attached JobItem or
	 * ExpenseItem's linked POs or BPOs, if the InvoiceItem has reference to
	 * either one.
	 *
	 * To be used when first inserting an InvoiceItem and the user could then
	 * remove/add POs as required
	 *
	 * @param item
	 *            the {@link InvoiceItem}
	 */
	void linkSamePOsAsJobItemOrExpenseItem(InvoiceItem item);

	void updateInvoicePOItem(InvoicePOItem iipo);

	void saveAll(List<InvoicePOItem> invoicePoItems);

}
