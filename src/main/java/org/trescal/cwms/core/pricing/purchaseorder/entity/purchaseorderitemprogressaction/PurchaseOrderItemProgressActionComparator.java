package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction;

import java.util.Comparator;

public class PurchaseOrderItemProgressActionComparator implements Comparator<PurchaseOrderItemProgressAction>
{
	@Override
	public int compare(PurchaseOrderItemProgressAction o1, PurchaseOrderItemProgressAction o2)
	{
		if ((o1.getDate() != null) && (o2.getDate() != null)
				&& !o1.getDate().equals(o2.getDate()))
		{
			return o2.getDate().compareTo(o1.getDate());
		}
		if ((o1.getId() == 0) && (o2.getId() == 0))
		{
			// un persisted items, no id set
			return 1;
		}
		else
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}

	}
}
