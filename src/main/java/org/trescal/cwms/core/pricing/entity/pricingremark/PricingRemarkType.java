package org.trescal.cwms.core.pricing.entity.pricingremark;

public enum PricingRemarkType {
	LABOR,
	PARTS,
	SUBCONTRACTING,
	INSPECTION,
	PURCHASE
}
