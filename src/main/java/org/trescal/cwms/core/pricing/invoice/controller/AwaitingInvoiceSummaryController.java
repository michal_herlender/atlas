package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobSortKey;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemInvoiceDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceHomeWrapper;
import org.trescal.cwms.core.pricing.invoice.dto.SummaryReadyToInvoice;
import org.trescal.cwms.core.pricing.invoice.dto.SummaryReadyToInvoicePopulator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * This controller splits off the "summary ready to invoice" information into a new screen / controller
 * to prevent heavy and repeated data loading of the invoice home
 */
@Controller
@IntranetController
@RequestMapping("/awaitinginvoicesummary.htm")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class AwaitingInvoiceSummaryController {

    @Autowired
    private MessageSource messages;
    @Autowired
    private JobItemService jobItemService;

    public static final String VIEW_NAME = "trescal/core/pricing/invoice/awaitinginvoicesummary";

    @RequestMapping(method = RequestMethod.GET)
    public String referenceData(Model model,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                @RequestParam(value = "companyWide", required = false, defaultValue = "false") Boolean companyWide,
                                Locale locale) {
        Integer allocatedCompanyId = companyDto.getKey();
        model.addAttribute("defaultSubdivId", subdivDto.getKey());
        model.addAttribute("companyWide", companyWide);
        model.addAttribute("wrapper", getSummaryReadyToInvoice(allocatedCompanyId, subdivDto, companyWide, locale));
        return VIEW_NAME;
    }

    private InvoiceHomeWrapper<SummaryReadyToInvoice> getSummaryReadyToInvoice(Integer allocatedCompanyId,
                                                                               KeyValue<Integer, String> allocatedSubdivDto, boolean companyWide, Locale locale) {
        Integer allocatedSubdivId = companyWide ? null : allocatedSubdivDto.getKey();
        // Use "blank" as allocated subdiv, when doing company wide expense search
        String allocatedSubdivName = companyWide ? "" : allocatedSubdivDto.getValue();

        List<JobItemInvoiceDto> dtos = this.jobItemService.findAllCompleteJobItemsWithoutInvoicesProjection(allocatedCompanyId, allocatedSubdivId, null, JobSortKey.COMPANY, "ALL");
        List<JobItemInvoiceDto> proformaDtos = this.jobItemService.findAllCompleteJobItemsWithOnlyProformaInvoicesProjection(allocatedCompanyId, allocatedSubdivId, null, JobSortKey.COMPANY, "ALL");
        // Leave off expense items for now - discuss, we need to add subdiv and job completion date
        //List<InvoiceableItemsByCompanyDTO> invoiceableJobServices = expenseItemService.getInvoiceable(allocatedCompanyId, allocatedSubdivId);
        SummaryReadyToInvoicePopulator populator = new SummaryReadyToInvoicePopulator(allocatedSubdivName);
        populator.addJobItemInvoiceDtos(dtos);
        populator.addProformaDtos(proformaDtos);
        //populator.addExpenseItems(invoiceableJobServices);

        InvoiceHomeWrapper<SummaryReadyToInvoice> wrapper = new InvoiceHomeWrapper<>();
        wrapper.setId("summaryreadytoinvoice");
        wrapper.setName(this.messages.getMessage("invoice.itemstoinvoice", null, "Items To Invoice", locale));
        wrapper.setDescription(this.messages.getMessage("invoice.awaitinginvoicesummary", null, "Awaiting Invoice Summary", locale));
        wrapper.setContents(populator.getSummaryDtos());
        wrapper.setContentsSummary(true);
        wrapper.setQuantity(populator.getQuantity());
        return wrapper;
    }

}
