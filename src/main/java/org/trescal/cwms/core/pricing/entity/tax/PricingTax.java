package org.trescal.cwms.core.pricing.entity.tax;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Setter;

/**
 * Class to store a tax value for a pricing entity. Associations to pricing and
 * vatRate should be overridden to specify column name and foreign key.
 * 
 * @param <P> pricing entity
 */
@MappedSuperclass
@Setter
public abstract class PricingTax<P extends Pricing<?>> {

	private Integer id;
	private P pricing;
	private VatRate vatRate;
	private String description;
	private BigDecimal amount;
	private BigDecimal rate;
	private BigDecimal tax;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@NotNull
	@ManyToOne(optional = false)
	@JsonIgnore
	public P getPricing() {
		return pricing;
	}

	@Column(name = "description", length = 100)
	public String getDescription() {
		return description;
	}

	@ManyToOne
	public VatRate getVatRate() {
		return vatRate;
	}

	@NotNull
	@Column(name = "amount", precision = 10, scale = 2, nullable = false)
	public BigDecimal getAmount() {
		return amount;
	}

	@Column(name = "rate", precision = 8, scale = 6, nullable = true)
	public BigDecimal getRate() {
		return rate;
	}

	@NotNull
	@Column(name = "tax", precision = 10, scale = 2, nullable = false)
	public BigDecimal getTax() {
		return tax;
	}

	@Transient
	public BigDecimal getAmountWithTax() {
		return getAmount().add(tax);
	}
}