package org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

@Entity
@Table(name = "invoicedelivery")
public class InvoiceDelivery
{
	private String deliveryNo;
	private int id;
	private Invoice invoice;

	@NotNull
	@Length(min = 1, max = 15)
	@Column(name = "deliveryno")
	public String getDeliveryNo()
	{
		return this.deliveryNo;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invid")
	public Invoice getInvoice()
	{
		return this.invoice;
	}

	public void setDeliveryNo(String deliveryNo)
	{
		this.deliveryNo = deliveryNo;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInvoice(Invoice invoice)
	{
		this.invoice = invoice;
	}
}
