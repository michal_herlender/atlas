package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.springframework.security.core.GrantedAuthority;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.dto.PurchaseOrderStatusProjectionDTO;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.userright.enums.Permission;

public interface PurchaseOrderStatusService {

	/**
	 * Returns a {@link List} of {@link PurchaseOrderStatus} that are flagged as
	 * requiring attention.
	 * 
	 * @return {@link List} of matching {@link PurchaseOrderStatus}.
	 */
	List<PurchaseOrderStatus> findAllPurchaseOrderStatusRequiringAttention();

	/**
	 * Returns a {@link PurchaseOrderStatus} of the given {@link ActionType} for
	 * when a {@link PurchaseOrder} is completed.
	 * 
	 * @param type the {@link ActionType}.
	 * @return the {@link PurchaseOrderStatus}.
	 */
	PurchaseOrderStatus findOnCompletePurchaseOrderStatus(ActionType type);

	/**
	 * Returns a {@link PurchaseOrderStatus} of the given {@link ActionType} which
	 * should be inserted when a new {@link PurchaseOrder} is inserted.
	 * 
	 * @param type the {@link ActionType}.
	 * @return a {@link PurchaseOrderStatus}.
	 */
	PurchaseOrderStatus findOnInsertPurchaseOrderStatus(ActionType type);

	PurchaseOrderStatus findOnCancelPurchaseOrderStatus(ActionType type);

	PurchaseOrderStatus findPurchaseOrderStatus(int id);

	Permission getPermissionNeeded(PurchaseOrderStatus poStatutTemp);

	Boolean isAllowChangeStatus(PurchaseOrderStatus poStatutTemp, Collection<? extends GrantedAuthority> auth);
	
	Integer findPurchaseOrderStatusIdRequiringAttentionByName(String name);
	
	List<PurchaseOrderStatusProjectionDTO> findAllPurchaseOrderStatusRequiringAttentionProjection(Locale locale);
	
	Boolean isAllowChangeStatus(PurchaseOrderStatusProjectionDTO poStatutTemp, Collection<? extends GrantedAuthority> auth);
	
	Permission getPermissionNeeded(PurchaseOrderStatusProjectionDTO poStatutTemp);
}