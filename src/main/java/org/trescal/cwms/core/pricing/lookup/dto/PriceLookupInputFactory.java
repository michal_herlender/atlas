package org.trescal.cwms.core.pricing.lookup.dto;

import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;

/**
 * Factory method to create a List<PriceLookupInput> for pricing
 * Manages sequential index of PriceLookupInput by default
 * Ensures at least one of instrument, model, sales category id are specified for all
 * 
 * Factory method returns PriceLookupInput for use by caller if desired  
 * 
 * Created 2019-04-20 GB
 */
public class PriceLookupInputFactory {
	private PriceLookupRequirements reqs;
	
	public PriceLookupInputFactory(PriceLookupRequirements reqs) {
		this.reqs = reqs;
	}
	
	public PriceLookupInput addInstrument(Integer serviceTypeId, Integer instrumentId) {
		if (instrumentId == null) throw new IllegalArgumentException("instrumentId must not be null");
		PriceLookupInput input = new PriceLookupInput(serviceTypeId);
		input.setInstrumentId(instrumentId);
		this.reqs.addInput(input);
		return input;
	}
	public PriceLookupInput addInstrumentModel(Integer serviceTypeId, Integer modelId) {
		if (modelId == null) throw new IllegalArgumentException("modelId must not be null");
		PriceLookupInput input = new PriceLookupInput(serviceTypeId);
		input.setModelId(modelId);
		this.reqs.addInput(input);
		return input;
	}
	public PriceLookupInput addSalesCategory(Integer serviceTypeId, Integer salesCategoryId) {
		if (salesCategoryId == null) throw new IllegalArgumentException("salesCategoryId must not be null");
		PriceLookupInput input = new PriceLookupInput(serviceTypeId);
		input.setSalesCategoryId(salesCategoryId);
		this.reqs.addInput(input);
		return input;
	}
}
