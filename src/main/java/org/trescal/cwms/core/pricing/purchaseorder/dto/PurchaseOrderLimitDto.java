package org.trescal.cwms.core.pricing.purchaseorder.dto;


import org.trescal.cwms.core.userright.dto.PoLimitConfig;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PurchaseOrderLimitDto {

	private PoLimitConfig poLimitConfig;	
	private String currency;
	
	
	public PurchaseOrderLimitDto(PoLimitConfig poLimitConfig, String currency) {
		this.setPoLimitConfig(poLimitConfig);
		this.setCurrency(currency);
	}

}