package org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.purchaseorder.dto.SupplierPOProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder;

public interface QuickPurchaseOrderDao extends BaseDao<QuickPurchaseOrder, Integer>
{
	List<QuickPurchaseOrder> findAllUnconsolidatedQuickPurchaseOrders(Company allocatedCompany);
	
	List<SupplierPOProjectionDTO> findAllUnconsolidatedQuickPurchaseOrdersUsingProjection(Integer allocatedCompanyId,Integer SubdivId);
}