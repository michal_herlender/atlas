package org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;

@Repository("InvoiceActionDao")
public class InvoiceActionDaoImpl extends BaseDaoImpl<InvoiceAction, Integer> implements InvoiceActionDao {
	
	@Override
	protected Class<InvoiceAction> getEntity() {
		return InvoiceAction.class;
	}
}