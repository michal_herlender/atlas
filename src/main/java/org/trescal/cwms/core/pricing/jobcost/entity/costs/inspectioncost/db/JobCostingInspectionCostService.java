package org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.db;

import java.math.BigDecimal;
import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRange;

public interface JobCostingInspectionCostService extends JobCostingCostService<JobCostingInspectionCost>
{
	/**
	 * this method calculates the inspection cost of an item dependant on the
	 * calibration cost entered by the user
	 * 
	 * @param calcost calibration cost supplied for item
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper calculateAjaxInspectionCost(BigDecimal calcost);

	JobCostingInspectionCost calculateInspectionCost(JobCostingInspectionCost jobCostingInspectionCost);

	void deleteJobCostingInspectionCost(JobCostingInspectionCost jobcostinginspectioncost);

	JobCostingInspectionCost findJobCostingInspectionCost(int id);

	List<JobCostingInspectionCost> getAllJobCostingInspectionCosts();

	void insertJobCostingInspectionCost(JobCostingInspectionCost jobcostinginspectioncost);

	/**
	 * Loads the a new {@link JobCostingInspectionCost} for the given
	 * {@link JobCostingItem}. Tests if the {@link JobCostingItem} has any
	 * {@link JobCostingCalibrationCost}s and if so uses these to calculate the
	 * cost based on the {@link MarkupRange} system.
	 * 
	 * @param jci the {@link JobCostingItem}.
	 * @return a {@link JobCostingInspectionCost}.
	 */
	Cost loadCost(JobCostingItem jci, JobCostingItem mostRecent);

	void saveOrUpdateJobCostingInspectionCost(JobCostingInspectionCost jobcostinginspectioncost);

	void updateJobCostingInspectionCost(JobCostingInspectionCost jobcostinginspectioncost);
}