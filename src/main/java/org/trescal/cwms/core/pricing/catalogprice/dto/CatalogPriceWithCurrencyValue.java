package org.trescal.cwms.core.pricing.catalogprice.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import lombok.Getter;

@Getter
public class CatalogPriceWithCurrencyValue {

	private CatalogPriceDTO catalogPrice;
	private BigDecimal currencyPrice;
	private BigDecimal currencyUnitPrice;
	private SupportedCurrency currency;
	private Integer coid;
	

	public CatalogPriceWithCurrencyValue(CatalogPriceDTO catalogPrice, BigDecimal currencyPrice,
			BigDecimal currencyUnitPrice, SupportedCurrency currency, Integer coid) {
		super();
		this.catalogPrice = catalogPrice;
		this.currencyPrice = currencyPrice;
		this.currencyUnitPrice = currencyUnitPrice;
		this.currency = currency;
		this.coid = coid;
	}

}
