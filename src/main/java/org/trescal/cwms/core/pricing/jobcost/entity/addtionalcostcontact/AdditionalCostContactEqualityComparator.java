package org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact;

import java.util.Comparator;

/**
 * Used to filter out possible duplicate entries, not suited for sorting
 * purposes.
 * 
 * @author Richard
 */
public class AdditionalCostContactEqualityComparator implements Comparator<AdditionalCostContact>
{
	@Override
	public int compare(AdditionalCostContact o1, AdditionalCostContact o2)
	{
		if ((o1.getContact().getPersonid() == o2.getContact().getPersonid())
				&& (o1.getCosting().getId() == o2.getCosting().getId()))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}
