package org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;

public interface AdditionalCostContactDao extends BaseDao<AdditionalCostContact, Integer> {}