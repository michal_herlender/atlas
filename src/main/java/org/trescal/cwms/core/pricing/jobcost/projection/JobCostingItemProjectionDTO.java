package org.trescal.cwms.core.pricing.jobcost.projection;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * Intended (at first) to contain just the total value of job costing items
 * to allow for generic use where we just need a total amount.
 * 
 * In the future, for use on detailed job costing work, this class could use
 * JobItemPriceProjectionDto as a composite field to contain the detailed 
 * costs and we could then have an alternate constructor with the detailed fields.
 * 
 * Also consider moving jobCostingVersion to a JobCostingProjectionDTO if created,
 * but the job costing reference number is always the same as the job number.
 * 
 */
@Getter @Setter
public class JobCostingItemProjectionDTO extends PricingItemProjectionDTO {

	private Integer jobItemId;
	private Integer jobCostingId;
	private Integer jobCostingVersion;
	
	/**
	 * Used in JPA query
	 */
	public JobCostingItemProjectionDTO(Integer pricingItemId, Integer itemno, BigDecimal totalCost, 
			BigDecimal finalCost, Integer jobItemId, Integer jobCostingId, Integer jobCostingVersion) {
		super(pricingItemId, itemno, totalCost, finalCost);		
				
		this.jobItemId = jobItemId;
		this.jobCostingId = jobCostingId;
		this.jobCostingVersion = jobCostingVersion;
	}

}
