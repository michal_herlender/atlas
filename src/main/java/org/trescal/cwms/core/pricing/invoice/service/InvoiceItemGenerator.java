package org.trescal.cwms.core.pricing.invoice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoice;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoiceItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Service
public class InvoiceItemGenerator {

	public List<PInvoiceItem> createPInvoiceItemsFromWithContractReviewPrices(PInvoice pInvoice, JobItem jobItem) {
		List<PInvoiceItem> items = new ArrayList<>();
		boolean isCosted = false;
		if (jobItem.getCalibrationCost() != null && jobItem.getCalibrationCost().isActive()) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setSource(CostSource.CONTRACT_REVIEW);
			item.setCost(jobItem.getCalibrationCost());
			if (jobItem.getCalibrationCost().getLinkedCost() != null
					&& jobItem.getCalibrationCost().getLinkedCost().getQuotationCalibrationCost() != null) {
				item.setQuotation(jobItem.getCalibrationCost().getLinkedCost().getQuotationCalibrationCost()
						.getQuoteItem().getQuotation());
			}
			item.setInclude(true);
			items.add(item);
			isCosted = true;
		}
		if (jobItem.getRepairCost() != null && jobItem.getRepairCost().isActive()) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setSource(CostSource.CONTRACT_REVIEW);
			item.setCost(jobItem.getRepairCost());
			item.setInclude(true);
			items.add(item);
			isCosted = true;
		}
		if (jobItem.getAdjustmentCost() != null && jobItem.getAdjustmentCost().isActive()) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setSource(CostSource.CONTRACT_REVIEW);
			item.setCost(jobItem.getAdjustmentCost());
			item.setInclude(true);
			items.add(item);
			isCosted = true;
		}
		if (jobItem.getPurchaseCost() != null && jobItem.getPurchaseCost().isActive()) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setSource(CostSource.CONTRACT_REVIEW);
			item.setCost(jobItem.getPurchaseCost());
			item.setInclude(true);
			items.add(item);
			isCosted = true;
		}
		// No contract review costs -> create one invoice item for this job item
		if (!isCosted) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setInclude(false);
			items.add(item);
		}
		return items;
	}

	public List<PInvoiceItem> createPInvoiceItem(PInvoice pInvoice, JobItem jobItem, JobCostingItem jobCostingItem) {
		List<PInvoiceItem> items = new ArrayList<>();
		if (jobCostingItem.getCalibrationCost() != null && jobCostingItem.getCalibrationCost().isActive()) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setJobCosting(jobCostingItem.getJobCosting());
			item.setSource(CostSource.JOB_COSTING);
			item.setCost(jobCostingItem.getCalibrationCost());
			items.add(item);
		}
		if (jobCostingItem.getAdjustmentCost() != null && jobCostingItem.getAdjustmentCost().isActive()) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setJobCosting(jobCostingItem.getJobCosting());
			item.setSource(CostSource.JOB_COSTING);
			item.setCost(jobCostingItem.getAdjustmentCost());
			items.add(item);
		}
		if (jobCostingItem.getRepairCost() != null && jobCostingItem.getRepairCost().isActive()) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setJobCosting(jobCostingItem.getJobCosting());
			item.setSource(CostSource.JOB_COSTING);
			item.setCost(jobCostingItem.getRepairCost());
			items.add(item);
		}
		if (jobCostingItem.getPurchaseCost() != null && jobCostingItem.getPurchaseCost().isActive()) {
			PInvoiceItem item = new PInvoiceItem(jobItem, pInvoice);
			item.setJobCosting(jobCostingItem.getJobCosting());
			item.setSource(CostSource.JOB_COSTING);
			item.setCost(jobCostingItem.getPurchaseCost());
			items.add(item);
		}
		return items;
	}
}