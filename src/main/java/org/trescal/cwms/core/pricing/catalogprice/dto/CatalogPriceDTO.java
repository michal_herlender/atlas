package org.trescal.cwms.core.pricing.catalogprice.dto;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.variableprice.entity.VariablePriceUnit;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.translation.Translation;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CatalogPriceDTO {
	
	private int id;
	private Integer coid;
	private String coname;
	private String costType;
	private String serviceType;
	private String fixedPrice;
	private String variablePrice;
	private String variablePriceUnit;
	private BigDecimal unitPrice;
	private String per;
    private String source;
    private String modelTypeName;
    private Integer modelId;
    private String modelName;
    private BigDecimal price;
    private SupportedCurrency supportedCurrency;
    private String comments;
    private Boolean isSalesCategory;
	
    
	public CatalogPriceDTO(CatalogPrice price, SupportedCurrency currency, Locale locale, Locale defaultLocale) {
		this.id = price.getId();
		this.costType = price.getCostType().getMessage();
		Set<Translation> translations = price.getServiceType().getLongnameTranslation();
		Translation translation = translations.stream().filter(t -> t.getLocale().equals(locale)).findAny().orElse(null);
		if(translation == null) translation = translations.stream().filter(t -> t.getLocale().equals(defaultLocale)).findAny().orElse(null);
		if(translation != null) serviceType = translation.getTranslation();
		this.fixedPrice = price.getFixedPrice() + "" + currency.getCurrencyCode();
		if (price.getVariablePrice() != null) {
			this.variablePrice = price.getVariablePrice().getUnitPrice() + "" + currency.getCurrencyCode();
			this.per = price.getVariablePrice().getUnit().getName();
		}
		else {
			this.variablePrice = "";
			this.per = "";
		}
		
		this.comments = price.getComments();
	}


	public CatalogPriceDTO() {
		super();
	}


	public CatalogPriceDTO(int id, Integer coid, String coname, String serviceType, VariablePriceUnit variablePriceUnit, BigDecimal unitPrice, 
			CostType costType, BigDecimal price, SupportedCurrency supportedCurrency, String comments, Boolean isSalesCategory) {
		super();
		this.id = id;
		this.coid = coid;
		this.coname = coname;
		this.serviceType = serviceType;
		this.variablePriceUnit = variablePriceUnit != null ? variablePriceUnit.getName() : "";
		this.unitPrice = unitPrice;
		this.costType = costType.getMessageCode();
		this.price = price;
		this.supportedCurrency = supportedCurrency;
		this.comments = comments;
		this.isSalesCategory = isSalesCategory;
	}
	
}