package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.PricingIssueMethod;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.JobCostingClientApproval;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItemComparator;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingnote.JobCostingNote;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingtype.JobCostingType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

import lombok.Setter;

@Entity
@Table(name = "jobcosting", uniqueConstraints = { @UniqueConstraint(columnNames = { "jobid", "ver" }) })
@Setter
public class JobCosting extends Pricing<Company> implements ComponentEntity, NoteAwareEntity {

	private Set<AdditionalCostContact> additionalContacts;
	private JobCostingType costingType;
	private File directory;
	// child entities
	private Set<JobCostingItem> items;
	private List<JobCostingExpenseItem> expenseItems;
	private JobCostingViewed jcViewed;
	private Job job;
	private Contact lastUpdateBy;
	private Date lastUpdateOn;
	private Set<JobCostingNote> notes;
	// interface / abstract fields
	private List<Email> sentEmails;
	private String siteCostingNote;
	private JobCostingStatus status;
	private PricingType type;
	private PricingIssueMethod issueMethod;
	private int ver;
	private Set<JobCostingClientApproval> clientApprovals;
	private ActionOutcome clientApprovalActionOutcome;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "costing")
	public Set<AdditionalCostContact> getAdditionalContacts() {
		return this.additionalContacts;
	}

	@NotNull
	@Column(name = "costingtypeid", unique = false, nullable = false)
	public int getCostingTypeId() {
		if (this.costingType != null)
			return this.costingType.getId();

		return 0;
	}

	@Transient
	public JobCostingType getCostingType() {
		return this.costingType;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		return this.contact;
	}

	@Override
	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return this.job.getJobno() + Constants.VERSION_TEXT + this.ver;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "jobCosting", orphanRemoval = true)
	@SortComparator(JobCostingItemComparator.class)
	public Set<JobCostingItem> getItems() {
		return this.items;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "jobCosting", orphanRemoval = true)
	public List<JobCostingExpenseItem> getExpenseItems() {
		return expenseItems;
	}

	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "jobCosting")
	public JobCostingViewed getJcViewed() {
		return this.jcViewed;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", unique = false, nullable = false)
	public Job getJob() {
		return this.job;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "lastupdateby", unique = false, nullable = false)
	public Contact getLastUpdateBy() {
		return this.lastUpdateBy;
	}

	@NotNull
	@Column(name = "lastupdateon", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastUpdateOn() {
		return this.lastUpdateOn;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "jobcosting")
	@SortComparator(NoteComparator.class)
	public Set<JobCostingNote> getNotes() {
		return this.notes;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "jobCosting", orphanRemoval = true)
	@OrderBy("id desc")
	public Set<JobCostingClientApproval> getClientApprovals() {
		return clientApprovals;
	}

	@Transient
	public JobCostingClientApproval getLastClientApproval() {
		return this.clientApprovals != null && this.clientApprovals.iterator().hasNext()
				? this.clientApprovals.iterator().next()
				: null;
	}

	public void setClientApprovals(Set<JobCostingClientApproval> clientApprovals) {
		this.clientApprovals = clientApprovals;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		return this.getJob();
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	/**
	 * old redundant database field
	 */
	@Transient
	public String getQno() {
		return getJob().getJobno();
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@Length(max = 1000)
	@Column(name = "sitecostingnote", length = 1000, nullable = true)
	public String getSiteCostingNote() {
		return this.siteCostingNote;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = false, foreignKey = @ForeignKey(name = "FK_jobcosting_statusid"))
	public JobCostingStatus getStatus() {
		return this.status;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "clientcosting", nullable = false)
	public PricingType getType() {
		return this.type;
	}

	@Min(1)
	@Column(name = "ver", nullable = false)
	@Type(type = "int")
	public int getVer() {
		return this.ver;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "issuemethod", nullable = true)
	public PricingIssueMethod getIssueMethod() {
		return issueMethod;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	public void setCostingTypeId(Integer costingType) {
		if (costingType != null)
			this.costingType = JobCostingType.getJobCostingTypeById(costingType);
		else
			this.costingType = null;
	}

	@Transient
	public ActionOutcome getClientApprovalActionOutcome() {
		return clientApprovalActionOutcome;
	}
}