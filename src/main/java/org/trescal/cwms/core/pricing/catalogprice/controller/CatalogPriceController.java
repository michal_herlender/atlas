package org.trescal.cwms.core.pricing.catalogprice.controller;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.misc.tool.DurationMinuteEditor;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPriceValidator;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.variableprice.entity.VariablePriceUnit;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
@RequestMapping("editmodelprice.htm")
public class CatalogPriceController {
	
    @Autowired
    private CatalogPriceService priceService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private SupportedCurrencyService currencyServ;
    @Autowired
    private InstrumentModelService instrumentModelService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private CatalogPriceValidator validator;
    
    // Use the Company currency if set otherwise use the system default.
    @ModelAttribute("businessCurrency")
    public SupportedCurrency initialiseCurrency(HttpServletRequest request) {
        SupportedCurrency currency = new SupportedCurrency();
        @SuppressWarnings("unchecked")
		Company businessCompany = companyService.get(((KeyValue<Integer,String>) request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_COMPANY)).getKey());
        if(businessCompany.getCurrency() != null){
           currency = businessCompany.getCurrency();
        } else {
            currency = this.currencyServ.getDefaultCurrency();
        }
        return currency;
    }
    
    @ModelAttribute("units")
    public VariablePriceUnit[] initialiseUnits(){
        return VariablePriceUnit.values();
    }
    
    @ModelAttribute("servicetypes")
    public List<ServiceType> initialiseCalTypes(
    		@RequestParam(name="modelid", required=false) Integer modelId){
    	if(modelId == null) {
    		// in this case the price should already exists and hence has a service type -> no selection anymore
    		return new ArrayList<ServiceType>();
    	}
    	else {
    		InstrumentModel model = instrumentModelService.get(modelId);
    		DomainType domainType = model.getDescription().getFamily().getDomain().getDomainType();
    		return serviceTypeService.getAllByDomainType(domainType);
    	}
    }
    
    /**
     * Either the id or modelId must be not null.
     * @param id Identifier of catalog price
     * @param modelId Identifier of instrument model
     * @param serviceTypeId Identifier of service type
     * @param companyDto Identifier and name of allocated company given by session
     * @return catalog price of id or new initialized catalog price
     */
    @ModelAttribute("price")
    public CatalogPrice initializePrice(
    		@RequestParam(value="id", required=false) Integer id,
    		@RequestParam(value="modelid", required=false) Integer modelId,
    		@RequestParam(value="servicetypeid", required = false) Integer serviceTypeId,
    		@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto){
    	if(id == null || id == 0) {
            CatalogPrice price = new CatalogPrice();
            InstrumentModel model = instrumentModelService.findInstrumentModel(modelId);
            price.setInstrumentModel(model);
            DomainType domainType;
            if(serviceTypeId != null) {
            	ServiceType serviceType = serviceTypeService.get(serviceTypeId);
            	price.setServiceType(serviceType);
            	domainType = serviceType.getDomainType();
            }
            else domainType = model.getDescription().getFamily().getDomain().getDomainType();
        	switch (domainType) {
			case INSTRUMENTMODEL:
				price.setCostType(CostType.CALIBRATION);
				break;
			case SERVICE: default:
				price.setCostType(CostType.SERVICE);
				break;
            }
            price.setOrganisation(companyService.get(companyDto.getKey()));
            return price;
        }
    	else return priceService.get(id);
    }
    
    @InitBinder("price")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
        binder.registerCustomEditor(Duration.class, new DurationMinuteEditor());
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String displayForm(){
        return "trescal/core/pricing/catalogprice/editmodelprice";
    }
    
    @RequestMapping(params= "save", method = RequestMethod.POST)
    public String processForm(
    		@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
    		@Validated @ModelAttribute("price") CatalogPrice price, 
    		@RequestParam(value="modelid", required=false) Integer modelId,
    		@RequestParam(value="replace", required=false, defaultValue = "false") Boolean replace,
    		BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return "/trescal/core/pricing/catalogprice/editmodelprice";
        else {
	        price = priceService.merge(price);
	       return "redirect:instrumentmodel.htm?modelid=" + price.getInstrumentModel().getIdentifier();
        }
    }
    
    @RequestMapping(params = "delete", method = RequestMethod.POST)
    public String deletePrice(@ModelAttribute("price") CatalogPrice price){
        String modelId = price.getInstrumentModel().getIdentifier();
        priceService.delete(price);
        return "redirect:instrumentmodel.htm?modelid=" + modelId;
    }
    
    @RequestMapping(params = "return", method = RequestMethod.POST)
    public String doNothing(@ModelAttribute("price") CatalogPrice price){
    	String modelId = price.getInstrumentModel().getIdentifier();
        return "redirect:instrumentmodel.htm?modelid=" + modelId;
    }
}