package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO_;
import org.trescal.cwms.core.pricing.purchaseorder.dto.AjaxPurchaseOrderItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.SupplierPOProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus_;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderHomeForm;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;

import lombok.val;

@Repository("PurchaseOrderDao")
public class PurchaseOrderDaoImpl extends BaseDaoImpl<PurchaseOrder, Integer> implements PurchaseOrderDao {
	@Override
	protected Class<PurchaseOrder> getEntity() {
		return PurchaseOrder.class;
	}

	private CriteriaQuery<PurchaseOrderDTO> getPurchaseOrderDTOQuery(CriteriaBuilder cb,
			Function<Root<PurchaseOrder>, Predicate> predicate, Function<Root<PurchaseOrder>, List<Order>> orders) {
		CriteriaQuery<PurchaseOrderDTO> cq = cb.createQuery(PurchaseOrderDTO.class);
		Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
		Join<PurchaseOrder, Contact> contact = root.join(PurchaseOrder_.contact, JoinType.LEFT);
		Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub, JoinType.LEFT);
		Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.LEFT);
		Join<PurchaseOrder, SupportedCurrency> currency = root.join(PurchaseOrder_.currency, JoinType.LEFT);
		if (predicate != null)
			cq.where(predicate.apply(root));
		if (orders != null)
			cq.orderBy(orders.apply(root));
		cq.select(cb.construct(PurchaseOrderDTO.class, root.get(PurchaseOrder_.id), root.get(PurchaseOrder_.pono),
				company.get(Company_.coname), root.get(PurchaseOrder_.regdate),
				currency.get(SupportedCurrency_.currencyERSymbol), root.get(PurchaseOrder_.finalCost)));
		return cq;
	}

	@Override
	public List<PurchaseOrderDTO> findAllByJob(Job job) {
		return getResultList(
				cb -> getPurchaseOrderDTOQuery(cb, root -> cb.equal(root.get(PurchaseOrder_.job), job), null));
	}

	@Override
	public List<PurchaseOrderDTO> findMostRecent(Company allocatedCompany, Integer maxResults) {
		return getResultList(cb -> getPurchaseOrderDTOQuery(cb,
				root -> cb.equal(root.get(PurchaseOrder_.organisation), allocatedCompany),
				root -> Arrays.asList(cb.desc(root.get(PurchaseOrder_.regdate)), cb.desc(root.get(PurchaseOrder_.id)))),
				0, maxResults);
	}

	@Override
	public List<PurchaseOrder> getByAccountStatus(Company businessCompany, AccountStatus accStatus) {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrder> cq = cb.createQuery(PurchaseOrder.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.organisation), businessCompany));
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.accountsStatus), accStatus));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(PurchaseOrder_.pono)));
			return cq;
		});
	}

	@Override
	public List<PurchaseOrder> findAllPOsRequiringAttention() {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrder> cq = cb.createQuery(PurchaseOrder.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Join<PurchaseOrder, PurchaseOrderStatus> status = root.join(PurchaseOrder_.status);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(status.get(PurchaseOrderStatus_.requiresAttention), true));
			clauses.getExpressions().add(cb.equal(status.get(PurchaseOrderStatus_.type), ActionType.STATUS));
			cq.where(clauses);
			List<Order> orders = new ArrayList<>();
			orders.add(cb.asc(root.get(PurchaseOrder_.regdate)));
			orders.add(cb.asc(root.get(PurchaseOrder_.id)));
			cq.orderBy(orders);
			return cq;
		});
	}

	/**
	 * Finds distinct purchase orders that are both directly linked to the job,
	 * as well as (purpose is to include purchase orders with no direct link to
	 * job, but that are still linked via PO Item + Job Item)
	 * 
	 * Sort is by registration date, and then by id
	 */
	@Override
	public List<PurchaseOrder> findAll(Job job) {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrder> cq = cb.createQuery(PurchaseOrder.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Join<PurchaseOrder, PurchaseOrderItem> poitem = root.join(PurchaseOrder_.items, JoinType.LEFT);
			Join<PurchaseOrderItem, PurchaseOrderJobItem> pojobitem = poitem.join(PurchaseOrderItem_.linkedJobItems,
				JoinType.LEFT);
			Join<PurchaseOrderJobItem, JobItem> jobitem = pojobitem.join(PurchaseOrderJobItem_.ji, JoinType.LEFT);

			Predicate clauses = cb.disjunction();
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.job), job));
			clauses.getExpressions().add(cb.equal(jobitem.get(JobItem_.job), job));

			cq.where(clauses);
			cq.distinct(true);

			List<Order> orders = new ArrayList<>();
			orders.add(cb.asc(root.get(PurchaseOrder_.regdate)));
			orders.add(cb.asc(root.get(PurchaseOrder_.id)));
			cq.orderBy(orders);
			return cq;
		});
	}

	@Override
	public List<PurchaseOrder> getAllPurchaseOrdersAtStatus(PurchaseOrderStatus status, Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrder> cq = cb.createQuery(PurchaseOrder.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.status), status));
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.organisation), allocatedCompany));
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(PurchaseOrder_.regdate)));
			return cq;
		});
	}

	@Override
	public PurchaseOrder findByNumber(String poNo, Integer orgId) {
		return getSingleResult(cb -> {
			CriteriaQuery<PurchaseOrder> cq = cb.createQuery(PurchaseOrder.class);
			Root<PurchaseOrder> purchaseOrder = cq.from(PurchaseOrder.class);
			Join<PurchaseOrder, Company> company = purchaseOrder.join("organisation");
			cq.where(cb.and(cb.equal(purchaseOrder.get(PurchaseOrder_.pono), poNo),
					cb.equal(company.get(Company_.coid), orgId)));
			return cq;
		});

	}

	@Override
	public PurchaseOrder getPOByExactPONo(String poNo) {
		return getSingleResult(cb -> {
			CriteriaQuery<PurchaseOrder> cq = cb.createQuery(PurchaseOrder.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			cq.where(cb.equal(root.get(PurchaseOrder_.pono), poNo));
			return cq;
		});
	}

	@Override
	public void linkJobItemToPO(PurchaseOrderJobItem poji) {
		// HOOK IS FIRED ON THIS METHOD CALL
	}

	@Override
	public PagedResultSet<PurchaseOrder> searchSortedPurchaseOrder(PurchaseOrderHomeForm form,
			PagedResultSet<PurchaseOrder> rs) {
		completePagedResultSet(rs, PurchaseOrder.class, cb -> cq -> {
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Predicate clauses = cb.conjunction();
			if (form.getOrgid() != null && form.getOrgid() != 0) {
				Join<PurchaseOrder, Company> organisation = root.join(PurchaseOrder_.organisation.getName());
				clauses.getExpressions().add(cb.equal(organisation.get(Company_.coid), form.getOrgid()));
			}
			Boolean coIdCrit = form.getCoid() != null && form.getCoid() != 0;
			Boolean coNameCrit = form.getConame() != null && !form.getConame().trim().equals("");
			Boolean personCrit = form.getPersonid() != null && form.getPersonid() != 0;
			Boolean contactCrit = form.getContact() != null && !form.getContact().trim().equals("");
			if (coIdCrit || coNameCrit || personCrit || contactCrit) {
				Join<PurchaseOrder, Contact> contact = root.join(PurchaseOrder_.contact);
				if (coIdCrit || coNameCrit) {
					Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
					Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
					if (coIdCrit)
						clauses.getExpressions().add(cb.equal(company.get(Company_.coid), form.getCoid()));
					else
						clauses.getExpressions()
								.add(cb.equal(cb.locate(company.get(Company_.coname), form.getConame()), 1));
				}
				if (personCrit)
					clauses.getExpressions().add(cb.equal(contact.get(Contact_.personid), form.getPersonid()));
				else if (contactCrit)
					clauses.getExpressions()
							.add(cb.equal(cb.locate(contact.get(Contact_.firstName), form.getContact()), 1));
			}
			if (form.getAddressid() != null && form.getAddressid() != 0) {
				Join<PurchaseOrder, Address> address = root.join(PurchaseOrder_.address);
				clauses.getExpressions().add(cb.equal(address.get(Address_.addrid), form.getAddressid()));
			}
			if (form.getJobno() != null && !form.getJobno().trim().equals("")) {
				Join<PurchaseOrder, Job> job = root.join(PurchaseOrder_.job);
				clauses.getExpressions().add(cb.greaterThan(cb.locate(job.get(Job_.jobno), form.getJobno()), 0));
			}
			if (form.getPono() != null && !form.getPono().trim().equals(""))
				clauses.getExpressions()
						.add(cb.greaterThan(cb.locate(root.get(PurchaseOrder_.pono), form.getPono()), 0));
			if (form.getDate() != null) {
				clauses.getExpressions().add(cb.between(root.get(PurchaseOrder_.regdate),
					form.getDate(), form.getDate().plusDays(1)));
			}
			if (form.getDateFrom() != null) {
				if (form.getDateTo() != null) {
					clauses.getExpressions().add(cb.between(root.get(PurchaseOrder_.regdate),
						form.getDateFrom(), form.getDateTo().plusDays(1)));
				} else
					clauses.getExpressions().add(cb.greaterThanOrEqualTo(root.get(PurchaseOrder_.regdate),
						form.getDateFrom()));
			}
			cq.where(clauses);
			List<Order> orders = new ArrayList<>();
			orders.add(cb.desc(root.get(PurchaseOrder_.regdate)));
			orders.add(cb.desc(root.get(PurchaseOrder_.id)));
			return Triple.of(root.get(PurchaseOrder_.id), null, orders);
		});
		return rs;
	}

	public List<AjaxPurchaseOrderItemDTO> preparePurchaseOrderItem(int jobid, StateGroup type, Locale locale) {
		if (type != null) {
			return getResultList(cb -> {
				CriteriaQuery<AjaxPurchaseOrderItemDTO> cq = cb.createQuery(AjaxPurchaseOrderItemDTO.class);

				Root<Job> rootJob = cq.from(Job.class);
				Join<Job, JobItem> joinJobItem = rootJob.join(Job_.items.getName());

				Join<JobItem, CalibrationType> joinCalType = joinJobItem.join(JobItem_.calType.getName());
				Join<CalibrationType, ServiceType> joinServiceType = joinCalType
						.join(CalibrationType_.serviceType.getName());
				Join<ServiceType, Translation> joinShortname = joinServiceType
						.join(ServiceType_.shortnameTranslation.getName());
				Join<ServiceType, Translation> joinLongname = joinServiceType
						.join(ServiceType_.longnameTranslation.getName());

				Join<JobItem, ItemState> joinItemState = joinJobItem.join(JobItem_.state.getName());
				Join<ItemState, StateGroupLink> joinStateGroupLink = joinItemState
						.join(ItemState_.groupLinks.getName());

				cq.distinct(true);
				cq.select(cb.construct(AjaxPurchaseOrderItemDTO.class,
						joinShortname.get(Translation_.translation.getName()),
						joinLongname.get(Translation_.translation.getName()),
						joinJobItem.get(JobItem_.jobItemId.getName()), joinJobItem.get(JobItem_.itemNo.getName())));

				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(rootJob.get(Job_.jobid.getName()), jobid));

				clauses.getExpressions().add(cb.equal(joinStateGroupLink.get(StateGroupLink_.groupId), type.getId()));

				clauses.getExpressions().add(cb.equal(joinShortname.get(Translation_.locale.getName()), locale));
				clauses.getExpressions().add(cb.equal(joinLongname.get(Translation_.locale.getName()), locale));

				cq.where(clauses);
				cq.orderBy(cb.asc(joinJobItem.get(JobItem_.itemNo.getName())));

				return cq;
			});
		}
		return new ArrayList<>();
	}

	@Override
	public List<SupplierPOProjectionDTO> getByAccountStatusUsingProjection(Integer businessCompanyId, Integer subdivId,
			AccountStatus accStatus) {
		return getResultList(cb -> {
			CriteriaQuery<SupplierPOProjectionDTO> cq = cb.createQuery(SupplierPOProjectionDTO.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.organisation), businessCompanyId));
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.accountsStatus), accStatus));
			if (subdivId != null) {
				Join<PurchaseOrder, Contact> bcCon = root.join(PurchaseOrder_.createdBy);
				Join<Contact, Subdiv> bcSubdiv = bcCon.join(Contact_.sub);
				clauses.getExpressions().add(cb.equal(bcSubdiv.get(Subdiv_.subdivid), subdivId));
			}
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(PurchaseOrder_.pono)));

			cq.select(cb.construct(SupplierPOProjectionDTO.class, root.get(PurchaseOrder_.id),
					root.get(PurchaseOrder_.pono),
					root.get(PurchaseOrder_.contact).get(Contact_.sub).get(Subdiv_.comp).get(Company_.coname),
					root.get(PurchaseOrder_.contact).get(Contact_.firstName),
					root.get(PurchaseOrder_.contact).get(Contact_.lastName), root.get(PurchaseOrder_.regdate)));
			return cq;
		});
	}

	@Override
	public List<SupplierPOProjectionDTO> getAllPurchaseOrdersAtStatusUsingProjection(Integer statusId,
			Integer alloactedCompanyId, Integer subdivId) {
		return getResultList(cb -> {
			CriteriaQuery<SupplierPOProjectionDTO> cq = cb.createQuery(SupplierPOProjectionDTO.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions()
					.add(cb.equal(root.get(PurchaseOrder_.status).get(PurchaseOrderStatus_.statusid), statusId));
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.organisation), alloactedCompanyId));

			if (subdivId != null) {
				Join<PurchaseOrder, Contact> bcCon = root.join(PurchaseOrder_.createdBy);
				Join<Contact, Subdiv> bcSubdiv = bcCon.join(Contact_.sub);
				clauses.getExpressions().add(cb.equal(bcSubdiv.get(Subdiv_.subdivid), subdivId));
			}

			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(PurchaseOrder_.regdate)));

			cq.select(cb.construct(SupplierPOProjectionDTO.class, root.get(PurchaseOrder_.id),
					root.get(PurchaseOrder_.pono),
					root.get(PurchaseOrder_.contact).get(Contact_.sub).get(Subdiv_.comp).get(Company_.coname),
					root.get(PurchaseOrder_.contact).get(Contact_.firstName),
					root.get(PurchaseOrder_.contact).get(Contact_.lastName), root.get(PurchaseOrder_.regdate)));
			return cq;
		});
	}

	@Override
	public List<PurchaseOrder> getAllPurchaseOrdersAtStatus(Integer statusId, Integer allocatedCompanyId) {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrder> cq = cb.createQuery(PurchaseOrder.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.status), statusId));
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.organisation), allocatedCompanyId));
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(PurchaseOrder_.regdate)));
			return cq;
		});
	}

	@Override
	public List<Integer> getJobItemsIdsFromPurchaseOrder(PurchaseOrder purchaseOrder) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Integer.class);
			val po = cq.from(PurchaseOrder.class);
			val poItem = po.join(PurchaseOrder_.items);
			val poJobItem = poItem.join(PurchaseOrderItem_.linkedJobItems);
			val ji = poJobItem.join(PurchaseOrderJobItem_.ji);
			cq.where(cb.equal(po, purchaseOrder));
			cq.distinct(true);
			cq.select(ji.get(JobItem_.jobItemId));
			return cq;
		});
	}

	@Override
	public List<PurchaseOrderDTO> getDtosByJobid(Integer jobId, Locale locale) {
		return getResultList(cb -> {

			CriteriaQuery<PurchaseOrderDTO> cq = cb.createQuery(PurchaseOrderDTO.class);

			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);

			Join<PurchaseOrder, PurchaseOrderStatus> status = root.join(PurchaseOrder_.status);
			Join<PurchaseOrderStatus, Translation> statusTranslation = status
					.join(PurchaseOrderStatus_.descriptiontranslations);

			Join<PurchaseOrder, Contact> contact = root.join(PurchaseOrder_.contact, JoinType.LEFT);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub, JoinType.LEFT);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.LEFT);

			Predicate clauses = cb.conjunction();

			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.job).get(Job_.JOBID), jobId));
			clauses.getExpressions().add(cb.equal(statusTranslation.get(Translation_.locale), locale));

			cq.where(clauses);

			cq.select(cb.construct(PurchaseOrderDTO.class, root.get(PurchaseOrder_.id), root.get(PurchaseOrder_.pono),
					company.get(Company_.coname), company.get(Company_.coid), 
					subdiv.get(Subdiv_.subname), subdiv.get(Subdiv_.subdivid),
					root.get(PurchaseOrder_.regdate),
					root.get(PurchaseOrder_.createdBy).get(Contact_.firstName),
					root.get(PurchaseOrder_.createdBy).get(Contact_.lastName),
					statusTranslation.get(Translation_.translation)));
			return cq;
		});
	}

	@Override
	public Long countPoPerJob(Integer jobId, Locale locale) {
		return getFirstResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<PurchaseOrder> root = cq.from(PurchaseOrder.class);
			Join<PurchaseOrder, PurchaseOrderStatus> status = root.join(PurchaseOrder_.status);
			Join<PurchaseOrderStatus, Translation> statusTranslation = status
					.join(PurchaseOrderStatus_.descriptiontranslations);

			Predicate clauses = cb.conjunction();

			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrder_.job).get(Job_.JOBID), jobId));
			clauses.getExpressions().add(cb.equal(statusTranslation.get(Translation_.locale), locale));

			cq.where(clauses);
			cq.select(cb.count(root));
			return cq;
		}).orElse(0L);
	}
	
	@Override
	public List<InvoiceItem> getInvoiceItemsWithPos(Integer invoiceid, List<Integer> bpos) {
		return getResultList(cb ->{
			CriteriaQuery<InvoiceItem> cq = cb.createQuery(InvoiceItem.class);
			Root<InvoiceItemPO> root = cq.from(InvoiceItemPO.class);
			Join<InvoiceItemPO, InvoiceItem> invoiceItem = root.join(InvoiceItemPO_.invItem);
			Join<InvoiceItem, Invoice> invoice = invoiceItem.join(InvoiceItem_.invoice);
			Join<InvoiceItem, JobItem> jobItem = invoiceItem.join(InvoiceItem_.jobItem);
			Join<JobItem, JobItemPO> jobItemPO = jobItem.join(JobItem_.ITEM_POS);
			Join<JobItemPO, BPO> bpo = jobItemPO.join(JobItemPO_.bpo);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(invoice.get(Invoice_.id), invoiceid));
			clauses.getExpressions().add(bpo.get(BPO_.poId).in(bpos));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(invoiceItem);
			return cq;
		});
	}	
	
}