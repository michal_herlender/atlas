package org.trescal.cwms.core.pricing.purchaseorder.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.pricing.purchaseorder.dto.AjaxPurchaseOrderForm;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.system.Constants;

@Controller
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY})
public class CreatePurchaseOrderDialogContentController {

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @ModelAttribute("purchaseOrderForm")
    protected AjaxPurchaseOrderForm getPurchaseOrderForm(HttpServletRequest request,
                    @RequestParam(value="jobItemId", required=false, defaultValue="0") Integer jobitemid){
        return this.purchaseOrderService.prepareAjaxPurchaseOrderForm(jobitemid, request);
    }

    @RequestMapping(value="purchaseorderdialogcontent.htm")
    protected String makeHtml(){
        return "trescal/core/jobs/jobitem/fragments/createPurchaseOrderDialog";
    }

}
