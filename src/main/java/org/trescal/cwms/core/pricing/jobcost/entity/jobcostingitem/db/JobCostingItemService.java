package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.annotation.Cost;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.dto.DetailedJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.projection.JobCostingItemProjectionDTO;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobCostingItemDTO;

public interface JobCostingItemService extends BaseService<JobCostingItem, Integer> {
	/**
	 * Delete the {@link JobCostingItem} identified by the given id.
	 * 
	 * @param id the id of the {@link JobCostingItem}
	 * @return {@link ResultWrapper} indicating sucess of operation and with the
	 *         results object set to the current {@link JobCosting}.
	 */
	ResultWrapper ajaxDeleteJobCostingItem(int id, HttpSession session);

	/**
	 * Creates a new {@link JobCostingItem} for the given {@link JobItem}. This
	 * function does <i>not</i> persist the {@link JobCostingItem} to the database,
	 * it is up to the caller to perform this.
	 * 
	 * @param costing   the {@link JobCosting} to add the {@link JobItem} to.
	 * @param jobitemid the ids of the {@link JobItem}s to add.
	 * @return
	 */
	List<JobCostingItem> createCostingItem(JobCosting costing, List<Integer> jobitemids);

	/**
	 * Deletes the given {@link JobCostingItem} and makes a call to update a
	 * {@link JobCosting} price total and returns the {@link JobCosting}.
	 * 
	 * @param jobcostingitem the item to delete.
	 */
	JobCosting delete(JobCostingItem jobcostingitem, Contact currentContact);

	/**
	 * Deletes the collection of items
	 * 
	 * @param items
	 */
	void delete(Collection<JobCostingItem> items);

	/**
	 * Finds the most recent {@link JobCostingItem} for the given {@link JobItem},
	 * returns only those {@link JobCostingItem}s that belong to {@link JobCosting}s
	 * from the same {@link Job} as the given {@link JobItem}.
	 * 
	 * @param jobitemid the {@link JobItem} id.
	 * @return most recent {@link JobCostingItem} or null if none found.
	 */
	JobCostingItem findMostRecentCostingForJobitem(int jobitemid);

	/**
	 * See DetailedJobItem getDetailedJobItemAndAlternativeCosts
	 * 
	 * @param jobcostingitemid the id of the {@link JobCostingItem}.
	 * @return {@link ResultWrapper}.
	 */
	ResultWrapper getAjaxDetailedJobItemAndAlternativeCosts(int jobcostingitemid);

	/**
	 * Returns a {@link DetailedJobItem} wrapper containing eagerly fetched
	 * information about a {@link JobItem} and any costs that relate to it.
	 * 
	 * @param jcItem the {@link JobCostingItem}.
	 * @return {@link DetailedJobItem}.
	 */
	DetailedJobItem getDetailedJobItemAndAlternativeCosts(JobCostingItem jcItem);

	/**
	 * Function iterates over a {@link JobCostingItem}'s costs and calculates the
	 * new values of each {@link Cost} on the item and then aggregates this into a
	 * new total cost for the item.
	 * 
	 * @param item {@link JobCostingItem} item.
	 * @return {@link JobCostingItem}.
	 */
	JobCostingItem updateJobCostingItemCosts(JobCostingItem item);

	List<AdvesoJobCostingItemDTO> findIssuedJobCostingItemByJobItemID(int jobitemid);
	
	List<JobCostingItemDto> getJobCostingItemsDtoByJobCostingId(Integer jobCostingId, Locale locale);
	
	List<JobCostingItemProjectionDTO> getProjectionDTOsForJobItems(Collection<Integer> jobItemIds);
}