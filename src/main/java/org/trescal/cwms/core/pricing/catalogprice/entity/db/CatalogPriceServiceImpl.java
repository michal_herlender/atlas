package org.trescal.cwms.core.pricing.catalogprice.entity.db;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.MultiValuedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceDTO;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceValueAndCommentDTO;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputCatalogPrice;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupQueryType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.MultiCurrencyUtils;

@Service("catalogPriceService")
public class CatalogPriceServiceImpl extends BaseServiceImpl<CatalogPrice, Integer> implements CatalogPriceService {
	
	@Autowired
	private CatalogPriceDao catalogPriceDao;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private CompanyService CompanyService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyService;
	
	@Override
	protected BaseDao<CatalogPrice, Integer> getBaseDao() {
		return catalogPriceDao;
	}
	
	@Override
	public CatalogPriceValueAndCommentDTO ajaxFind(Integer modelId, Integer calibrationTypeId, Integer companyId){
		InstrumentModel instrumentModel = instrumentModelService.findInstrumentModel(modelId);
		ServiceType serviceType = calibrationTypeService.find(calibrationTypeId).getServiceType();
		Company company = CompanyService.get(companyId);
		CatalogPriceValueAndCommentDTO dto;
		CatalogPrice price = this.findSingle(instrumentModel, serviceType, null, company);
		if (price == null) {dto = new CatalogPriceValueAndCommentDTO(new BigDecimal(0),"");}
		else if (price.getComments() == null) {dto = new CatalogPriceValueAndCommentDTO(price.getFixedPrice(),"");}
		else {dto = new CatalogPriceValueAndCommentDTO(price.getFixedPrice(),price.getComments());}
		return dto;
	}
	
	@Override
	public CatalogPriceValueAndCommentDTO ajaxGetPriceInGivenCurrency(Integer modelId, Integer calibrationTypeId, Integer companyId, String currencyCode){
		// Get price in business company's currency
		CatalogPriceValueAndCommentDTO price = this.ajaxFind(modelId, calibrationTypeId, companyId);
		// Get business company's currency
		SupportedCurrency businessCurrency = CompanyService.get(companyId).getCurrency();
		// Get required currency
		SupportedCurrency requiredCurrency = supportedCurrencyService.findCurrencyByCode(currencyCode);
		//update value
		price.setValue(MultiCurrencyUtils.convertCurrencyValue(price.getValue(), businessCurrency, requiredCurrency));
		return price;
	}
	
	@Override
	public CatalogPrice findSingle(InstrumentModel model, ServiceType serviceType, CostType costType, Company businessCompany) {
		return findSingle(model, serviceType, costType, businessCompany, false);
	}

	private CatalogPrice findSingle(InstrumentModel model, ServiceType serviceType, CostType costType, Company businessCompany, boolean eager) {
		List<CatalogPrice> results = catalogPriceDao.find(model, serviceType, costType, businessCompany, eager);
		if (results.isEmpty()) return null;
		return results.get(0);
	}
	
	@Override
	public CatalogPrice findCalCatalogPrice(JobItem jobItem) {
		return findCalCatalogPrice(jobItem, false);
	}

	@Override
	public CatalogPrice findCalCatalogPriceEager(JobItem jobItem) {
		return findCalCatalogPrice(jobItem, true);
	}

	private CatalogPrice findCalCatalogPrice(JobItem jobItem, boolean eager) {
		InstrumentModel model = jobItem.getInst().getModel();
		ServiceType serviceType = jobItem.getCalType().getServiceType();
		CostType costType = CostType.CALIBRATION;
		Company company = jobItem.getJob().getOrganisation().getComp();
		return findSingle(model, serviceType, costType, company, eager);
	}
	
	@Override
	public List<CatalogPrice> find(InstrumentModel model, ServiceType serviceType, CostType costType, Company businessCompany) {
		return catalogPriceDao.find(model, serviceType, costType, businessCompany, false);
	}
	
	@Override
	public List<CatalogPrice> find(int modelId, int serviceTypeId, CostType costType, int businessCompanyId, boolean eager) {
		return catalogPriceDao.find(modelId, serviceTypeId, costType, businessCompanyId, eager);
	}
	
	@Override
	public List<CatalogPrice> findAll(ServiceType serviceType, List<InstrumentModel> models, Company businessCompany) {
		return catalogPriceDao.findAll(serviceType, models, businessCompany);
	}
	
	@Override
	public List<PriceLookupOutputCatalogPrice> findCatalogPrices(PriceLookupRequirements reqs, PriceLookupQueryType queryType, MultiValuedMap<Integer, Integer> serviceTypeMap) {
		return catalogPriceDao.findCatalogPrices(reqs, queryType, serviceTypeMap);
	}
	
	@Override
	public Boolean exists(InstrumentModel instrumentModel, CostType costType, ServiceType serviceType, Company businessCompany) {
		return !catalogPriceDao.find(instrumentModel, serviceType, costType, businessCompany, false).isEmpty();
	}
	
	@Override
	public List<CatalogPriceDTO> getCatalogPricesFromSalesCategory(Integer salesCategoryId, Locale locale){
		return this.catalogPriceDao.getCatalogPricesFromSalesCategory(salesCategoryId, locale);
	}
	
	@Override
	public List<CatalogPriceDTO> findPrices(Integer instrumentModelId, Locale locale){
		return this.catalogPriceDao.findPrices(instrumentModelId, locale);
	}
	
}
