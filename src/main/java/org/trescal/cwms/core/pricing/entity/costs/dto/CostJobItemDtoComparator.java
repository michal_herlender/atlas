package org.trescal.cwms.core.pricing.entity.costs.dto;

import java.util.Comparator;

/**
 * Orders CostJobItemDtoComparator by:
 * (a) CostType natural order and then by 
 * (b) costId (just in case there are many for one cost type)  
 * @author galen
 *
 */
public class CostJobItemDtoComparator implements Comparator<CostJobItemDto> {
	
	@Override
	public int compare(CostJobItemDto o1, CostJobItemDto o2) {
		
		int result = 0;
		if (o1.getCostType()!= null && o2.getCostType() != null)
			result = o1.getCostType().compareTo(o2.getCostType());
		if (result == 0) {
			result = o1.getCostId().compareTo(o2.getCostId());
		}
		return result;
	}
}
