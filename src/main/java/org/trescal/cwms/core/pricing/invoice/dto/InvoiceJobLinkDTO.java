package org.trescal.cwms.core.pricing.invoice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class InvoiceJobLinkDTO {
	
	private Integer invoiceid;
	private Integer jobid;
	private String jobno;
	
	public InvoiceJobLinkDTO(Integer invoiceid, Integer jobid, String jobno) {
		super();
		this.invoiceid = invoiceid;
		this.jobid = jobid;
		this.jobno = jobno;
	}
		
}
