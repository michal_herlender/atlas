package org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.InvoiceDoctypeTemplate;

public interface InvoiceDoctypeTemplateDao extends BaseDao<InvoiceDoctypeTemplate, Integer> {
	
	InvoiceDoctypeTemplate findByCompanyType(int coid, int typeid, String doctype);
	
	InvoiceDoctypeTemplate findByType(int typeid, String doctype);
}