package org.trescal.cwms.core.pricing.entity.costs.base.db;

import java.util.List;

import org.trescal.cwms.core.pricing.entity.costs.base.RepCost;

public class RepCostServiceImpl implements RepCostService
{
	RepCostDao RepCostDao;

	public RepCost findRepCost(int id)
	{
		return RepCostDao.find(id);
	}

	public void insertRepCost(RepCost RepCost)
	{
		RepCostDao.persist(RepCost);
	}

	public void updateRepCost(RepCost RepCost)
	{
		RepCostDao.update(RepCost);
	}

	public List<RepCost> getAllRepCosts()
	{
		return RepCostDao.findAll();
	}

	public void setRepCostDao(RepCostDao RepCostDao)
	{
		this.RepCostDao = RepCostDao;
	}

	public void deleteRepCost(RepCost repcost)
	{
		this.RepCostDao.remove(repcost);
	}

	public void saveOrUpdateRepCost(RepCost repcost)
	{
		this.RepCostDao.saveOrUpdate(repcost);
	}
}