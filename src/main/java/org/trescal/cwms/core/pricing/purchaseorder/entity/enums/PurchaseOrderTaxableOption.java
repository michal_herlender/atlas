package org.trescal.cwms.core.pricing.purchaseorder.entity.enums;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum PurchaseOrderTaxableOption {

    NOT_APPLICABLE("notapplicable"),
    YES("yes"),
    NO("no");

    private String messageCode;
    private MessageSource messageSource;

    PurchaseOrderTaxableOption(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return messageSource.getMessage(messageCode, null, LocaleContextHolder.getLocale());
    }

    private void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Component
    public static class PurchaseOrderTaxableMessageSourceInjector {

        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            EnumSet.allOf(PurchaseOrderTaxableOption.class).forEach(option -> option.setMessageSource(messageSource));
        }
    }
}