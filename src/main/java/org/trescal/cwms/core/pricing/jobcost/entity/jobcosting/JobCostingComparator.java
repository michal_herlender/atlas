package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting;

import java.util.Comparator;

/**
 * Basic built in comparator which sorts quotations by registration date (desc)
 * 
 * @author Richard
 */
public class JobCostingComparator implements Comparator<JobCosting>
{
	public int compare(JobCosting c1, JobCosting c2)
	{
		if (c2.getQno().equals(c1.getQno()))
		{
			Integer v1 = c1.getVer();
			Integer v2 = c2.getVer();

			return v2.compareTo(v1);
		}
		else
		{
			return c2.getQno().compareTo(c1.getQno());
		}
	}
}