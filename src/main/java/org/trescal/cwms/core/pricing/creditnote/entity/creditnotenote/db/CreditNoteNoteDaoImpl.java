package org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.CreditNoteNote;

@Repository
public class CreditNoteNoteDaoImpl extends BaseDaoImpl<CreditNoteNote, Integer> implements CreditNoteNoteDao {

	@Override
	protected Class<CreditNoteNote> getEntity() {
		return CreditNoteNote.class;
	}


}
