package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceTypeKeyValue;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db.InvoiceTypeService;
import org.trescal.cwms.core.pricing.invoice.form.CopyInvoiceForm;
import org.trescal.cwms.core.pricing.invoice.service.InvoiceCopier;
import org.trescal.cwms.core.system.Constants;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
@RequestMapping(value = "/copyinvoice.htm")
public class CopyInvoiceController {

	@Autowired
	private InvoiceCopier invoiceCopier;
	@Autowired
	private InvoiceTypeService invoiceTypeService;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private UserService userService;
	@Autowired
	private AddressService addressService;

	@ModelAttribute("form")
	public CopyInvoiceForm formBackingObject(@RequestParam(value = "id", required = true) int sourceInvoiceId) {
		Invoice sourceInvoice = this.invoiceService.findInvoice(sourceInvoiceId);
		CopyInvoiceForm form = new CopyInvoiceForm();
		form.setInvoiceAddressId(sourceInvoice.getAddress().getAddrid());
		form.setInvoiceTypeId(sourceInvoice.getType().getId());
		form.setSourceInvoiceId(sourceInvoiceId);
		return form;
	}

	@ModelAttribute("invoiceTypes")
	public List<InvoiceTypeKeyValue> getInvoiceTypes(Locale locale) {
		return this.invoiceTypeService.getAllTranslated(locale);
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showView(@RequestParam(value = "id", required = true) int sourceInvoiceId, Model model) {
		Invoice sourceInvoice = this.invoiceService.findInvoice(sourceInvoiceId);
		model.addAttribute("invoiceAddresses", this.addressService
				.getAllCompanyAddresses(sourceInvoice.getComp().getCoid(), AddressType.INVOICE, true));
		model.addAttribute("invoice", sourceInvoice);
		return "trescal/core/pricing/invoice/copyinvoice";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String onSubmit(@ModelAttribute("form") CopyInvoiceForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Contact contact = this.userService.get(username).getCon();
		Invoice sourceInvoice = this.invoiceService.findInvoice(form.getSourceInvoiceId());
		InvoiceType invoiceType = this.invoiceTypeService.get(form.getInvoiceTypeId());
		Address invoiceAddress = this.addressService.get(form.getInvoiceAddressId());
		Invoice newInvoice = this.invoiceCopier.copy(sourceInvoice, invoiceType, contact,invoiceAddress);
		this.invoiceService.insertInvoice(newInvoice);

		return "redirect:viewinvoice.htm?id=" + newInvoice.getId();
	}
}
