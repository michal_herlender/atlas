package org.trescal.cwms.core.pricing.entity.costs;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CostSourceSupportService
{
	protected final static Log logger = LogFactory.getLog(CostSourceSupportService.class);

	/**
	 * Resolves a comma separated list of cost source names into a list of
	 * {@link CostSource} enum references.
	 * 
	 * @param costSrc comma separated list of cost source names.
	 * @return list of {@link CostSource}.
	 */
	public static List<CostSource> resolveCostSourceList(String costSrc)
	{
		List<CostSource> costSources = new ArrayList<CostSource>();
		String[] sourceHr = costSrc.split(",");
		
		for (String sourceStr : sourceHr)
		{
			try
			{
				CostSource src = CostSource.valueOf(sourceStr.trim());
				costSources.add(src);
				logger.debug("Resolved cost source " + src);
			}
			catch (IllegalArgumentException iae)
			{
				// spit out the error
				logger.error("Could not resolve cost source '" + sourceStr
						+ "'");
				iae.printStackTrace();
			}
		}
		logger.debug("Resolved " + costSources.size() + " cost sources");
		return costSources;
	}
}
