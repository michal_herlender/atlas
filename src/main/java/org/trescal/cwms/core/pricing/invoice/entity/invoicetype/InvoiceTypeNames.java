package org.trescal.cwms.core.pricing.invoice.entity.invoicetype;

public class InvoiceTypeNames {
	public static final String CALIBRATION = "Calibration";	// ID 1
	public static final String VALVES = "Valves";			// Note, legacy Antech type only (deleted id 2 in database)
	public static final String SALES = "Sales";				// ID 3
	public static final String HIRE = "Hire";				// ID 4
	public static final String PRO_FORMA = "Pro-Forma";		// ID 5
	public static final String PERIODIC = "Periodic";		// ID 6 (new Periodic invoice type added 2016-07-26)
	public static final String INTERNAL = "Internal";		// ID 6 (new Periodic invoice type added 2016-07-26)
}
