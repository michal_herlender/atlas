package org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.db;

import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

public interface JobCostingPurchaseCostService extends JobCostingCostService<JobCostingPurchaseCost>
{
	void deleteJobCostingPurchaseCost(JobCostingPurchaseCost jobcostingpurchasecost);

	JobCostingPurchaseCost findJobCostingPurchaseCost(int id);

	List<JobCostingPurchaseCost> getAllJobCostingPurchaseCosts();

	void insertJobCostingPurchaseCost(JobCostingPurchaseCost jobcostingpurchasecost);

	/**
	 * Creates a new {@link Cost} for the given {@link JobCostingItem}. Checks
	 * if cost lookups are enabled and if so looks up the most suitable cost for
	 * the item based on the given {@link CostSource} hierarchy of the system
	 * default hierarchy. Additionally tests if the deactivateOnZero property
	 * has been set to true and if so ensures that the resulting cost is
	 * deactivated if it's calculated value is 0.
	 * 
	 * @param ji the {@link JobItem}.
	 * @param mostRecent the most recent other {@link JobCostingItem} for the
	 *        current {@link JobItem}.
	 * @return a {@link Cost}.
	 */
	Cost loadCost(JobCostingItem jci, JobCostingItem mostRecent);

	void saveOrUpdateJobCostingPurchaseCost(JobCostingPurchaseCost jobcostingpurchasecost);

	void updateJobCostingPurchaseCost(JobCostingPurchaseCost jobcostingpurchasecost);
}