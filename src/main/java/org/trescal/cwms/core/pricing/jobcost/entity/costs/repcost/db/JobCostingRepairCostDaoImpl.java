package org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupportImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;

@Repository("JobCostingRepairCostDao")
public class JobCostingRepairCostDaoImpl extends JobCostingCostDaoSupportImpl<JobCostingRepairCost> implements JobCostingRepairCostDao {
	
	@Override
	protected Class<JobCostingRepairCost> getEntity() {
		return JobCostingRepairCost.class;
	}
}