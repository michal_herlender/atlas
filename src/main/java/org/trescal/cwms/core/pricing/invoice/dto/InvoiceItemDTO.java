package org.trescal.cwms.core.pricing.invoice.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class InvoiceItemDTO {
    private Integer id;
    private Integer itemNo;
    private Integer jobItemId;
    private Integer jobItemNo;
    private Integer expenseItemId;
    private Integer expenseItemNo;
    private Integer jobId;
    private String jobNo;
    private Integer deliveryId;
    private String deliveryNo;
    private Integer instrumentId;
    private String instrumentName;
    private String plantNo;
    private String serialNo;
    private String description;
    private String serviceType;
    private String subdivisionCode;
    private BigDecimal totalPrice;
    private BigDecimal discount;
    private BigDecimal finalPrice;
    private String nominalCode;
}