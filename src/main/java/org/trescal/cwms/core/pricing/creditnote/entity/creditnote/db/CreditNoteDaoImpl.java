package org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteDTO;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote_;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.creditnote.form.CreditNoteHomeForm;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository("CreditNoteDao")
public class CreditNoteDaoImpl extends BaseDaoImpl<CreditNote, Integer> implements CreditNoteDao
{
	@Autowired
	private CompanyService companyService;
	@Override
	protected Class<CreditNote> getEntity() {
		return CreditNote.class;
	}
	
	@Override
	public List<KeyValueIntegerString> getNumbersPerInvoices(Collection<Integer> invoiceIds) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<CreditNote> creditNote = cq.from(CreditNote.class);
			cq.where(creditNote.get(CreditNote_.invoice).in(invoiceIds));
			cq.distinct(true);
			cq.select(cb.construct(KeyValueIntegerString.class, creditNote.get(CreditNote_.invoice).get(Invoice_.id), creditNote.get(CreditNote_.creditNoteNo)));
			
			return cq;
		});
	}

	
	
	public List<CreditNote> findAllCreditNotesByAccountStatus(Company businessCompany, AccountStatus accStatus) {
		return getResultList(cb -> {
			CriteriaQuery<CreditNote> cq = cb.createQuery(CreditNote.class);
			Root<CreditNote> creditNote = cq.from(CreditNote.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(creditNote.get(CreditNote_.organisation), businessCompany));
			clauses.getExpressions().add(cb.equal(creditNote.get(CreditNote_.accountsStatus), accStatus));
			cq.where(clauses);
			cq.orderBy(cb.asc(creditNote.get(CreditNote_.creditNoteNo)));
			return cq;
		});
	}

	public List<CreditNote> getAllAtStatus(Company businessCompany, CreditNoteStatus status)
	{
		return getResultList(cb -> {
			CriteriaQuery<CreditNote> cq = cb.createQuery(CreditNote.class);
			Root<CreditNote> creditNote = cq.from(CreditNote.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(creditNote.get(CreditNote_.organisation), businessCompany));
			clauses.getExpressions().add(cb.equal(creditNote.get(CreditNote_.status), status));
			cq.where(clauses);
			cq.orderBy(cb.asc(creditNote.get(CreditNote_.creditNoteNo)));
			return cq;
		});
	}
	
	public PagedResultSet<CreditNoteDTO> searchCreditNotes(CreditNoteHomeForm form, PagedResultSet<CreditNoteDTO> rs)
	{
		super.completePagedResultSet(rs, CreditNoteDTO.class, cb-> cq ->{
			Root<CreditNote> creditNote = cq.from(CreditNote.class);
			Join<CreditNote, Invoice> invoiceJoin = creditNote.join(CreditNote_.invoice);
			Join<Invoice, Company> companyJoin = invoiceJoin.join(Invoice_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> settings = companyJoin
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation),
					creditNote.get(CreditNote_.organisation)));
			Join<CreditNote, SupportedCurrency> supportedCurrencyJoin = creditNote.join(CreditNote_.currency);
			
			Predicate clauses = cb.conjunction();
			if ((form.getOrgId() != null) && (form.getOrgId() != 0)) {
				clauses.getExpressions().add(cb.equal(invoiceJoin.get(Invoice_.organisation), 
						companyService.get(form.getOrgId())));
			}
			
			if ((form.getCompId() != null) && (form.getCompId() != 0)){
				clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), form.getCompId()));
			}
			
			if ((form.getInvNo() != null) && !form.getInvNo().trim().isEmpty()){
				clauses.getExpressions().add(ilike(cb, invoiceJoin.get(Invoice_.invno), form.getInvNo(), MatchMode.ANYWHERE));
			}
			
			if ((form.getCreditNoteNo() != null) && !form.getCreditNoteNo().trim().isEmpty()){
				clauses.getExpressions().add(ilike(cb, creditNote.get(CreditNote_.creditNoteNo), form.getCreditNoteNo(), MatchMode.ANYWHERE));
			}
			
			if (form.getDate() != null) {
				clauses.getExpressions().add(cb.between(creditNote.get(CreditNote_.regdate), form.getDate(),
					form.getDate().plusDays(1)));
			}
			
			if (form.getDateFrom() != null){
				if (form.getDateTo() != null) {
					clauses.getExpressions().add(cb.between(creditNote.get(CreditNote_.regdate), form.getDateFrom(),
						form.getDateTo().plusDays(1)));
				}
				else {
					clauses.getExpressions().add(cb.greaterThanOrEqualTo(creditNote.get(CreditNote_.regdate), form.getDateFrom()));
				}
			}
			
			List<Order> order = new ArrayList<>();
			cq.where(clauses);
			
			CompoundSelection<CreditNoteDTO> selection = cb.construct(CreditNoteDTO.class, creditNote.get(CreditNote_.id),
					creditNote.get(CreditNote_.creditNoteNo), companyJoin.get(Company_.coid),
					companyJoin.get(Company_.coname), settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop), invoiceJoin.get(Invoice_.invno),
					creditNote.get(CreditNote_.regdate), supportedCurrencyJoin.get(SupportedCurrency_.currencyERSymbol), 
					creditNote.get(CreditNote_.totalCost));

			order.add(cb.desc(creditNote.get(CreditNote_.regdate)));
			
			return Triple.of(creditNote.get(CreditNote_.id), selection, order);
			});
		
		return rs;
	}

	@Override
	public CreditNote findByNumber(String cnNo, Integer orgid) {
		return getFirstResult(cb -> { 
			CriteriaQuery<CreditNote> cq = cb.createQuery(CreditNote.class);
			Root<CreditNote> creditNote = cq.from(CreditNote.class);
			Join<CreditNote, Company> company = creditNote.join("organisation");
			cq.where(cb.and(cb.equal(creditNote.get(CreditNote_.creditNoteNo),cnNo),cb.equal(company.get(Company_.coid), orgid)));
			return cq;
		}).orElse(null);
	}



	
}