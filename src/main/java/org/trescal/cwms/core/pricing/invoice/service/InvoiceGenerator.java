package org.trescal.cwms.core.pricing.invoice.service;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobSortKey;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemInvoiceDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.ExpenseItemNotInvoicedService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.JobItemNotInvoicedService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.base.ServiceCost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.invoice.dto.*;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItemComparator;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLinkComparator;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.db.InvoiceStatusService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceTypeNames;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db.InvoiceTypeService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.servicecost.JobCostingServiceCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItemReverseComparator;
import org.trescal.cwms.core.pricing.tax.TaxCalculationSystem;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.entity.baseaction.ActionComparator;
import org.trescal.cwms.core.system.entity.basestatus.db.BaseStatusService;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.StringTools;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service("InvoiceGenerator")
public class InvoiceGenerator {

	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Value("#{props['cwms.config.costs.invoices.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	@Autowired
	private AddressService addrServ;
	@Autowired
	private BaseStatusService statusServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private AddressSettingsForAllocatedCompanyService addressSettingsService;
	@Autowired
	private InvoiceAddressService invoiceAddressService;
	@Autowired
	private InvoiceItemGenerator invoiceItemGenerator;
    @Autowired
	private InvoiceService invoiceService;
	@Autowired
	private InvoiceStatusService invoiceStatusServ;
	@Autowired
	private InvoiceTypeService invTypeServ;
	@Autowired
	private JobCostingService jobCostingService;
	@Autowired
	private JobExpenseItemService expenseItemService;
	@Autowired
	private JobItemService jobitemService;
	@Autowired
	private JobItemNotInvoicedService jobItemNotInvoicedService;
	@Autowired
	private ExpenseItemNotInvoicedService expenseItemNotInvoicedService;
	@Autowired
	private JobService jobServ;
	private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private NominalCodeService nomCodeServ;
    @Autowired
    private NumerationService numerationService;
    @Autowired
    private SiteInvoiceGenerator siteInvoiceGenerator;
    @Autowired
    private SupportedCurrencyService currencyServ;
    @Autowired
    private TaxCalculator taxCalculator;
    @Autowired
    private NoteService noteService;

    /**
     * Creates a new 'on insert' activity for an invoice and sets this into the
     * invoices action list.
     */
    protected void addOnInsertActivity(Invoice invoice, Contact contact) {
        InvoiceAction action = new InvoiceAction();
        action.setContact(contact);
        action.setDate(new Date());
		action.setOutcomeStatus(invoice.getStatus());
		action.setActivity(this.invoiceStatusServ.findOnInsertActivity());
		action.setInvoice(invoice);
		TreeSet<InvoiceAction> actions = new TreeSet<>(new ActionComparator());
		actions.add(action);
		invoice.setActions(actions);
	}

	/**
	 * Creates a single {@link Invoice} the given company and jobs (potentially
	 * <i>not</i> {@link Job} s), using the supplied list of job numbers, addressid
	 * and invoicetype. Attempts to match to system {@link Job}s first. Does not add
	 * items from existing jobs to the invoice.
	 * 
	 * @param jobNos      list of job numbers.
	 * @param addressid   the {@link Address} id.
	 * @param invoiceType the {@link InvoiceType} id.
	 * @return list of {@link Invoice} entities created as a result of this task.
	 */
	public Invoice createInvoiceForJobs(Integer coid, List<String> jobNos, int addressid, Integer invoiceType,
			Integer currencyId, Contact currentContact, Subdiv allocatedSubdiv) {
		// first create the invoice
		Company comp = this.compServ.get(coid);
		Address address = this.addrServ.get(addressid);
		InvoiceType type = invoiceType == null ? null : this.invTypeServ.get(invoiceType);

		// create the invoice
		Invoice invoice = new Invoice();

		// set the invno
		String invno = numerationService.generateNumber(NumerationType.INVOICE, allocatedSubdiv.getComp(),
				allocatedSubdiv);
		invoice.setInvno(invno);

		// set the status of the invoice
		invoice.setStatus((InvoiceStatus) this.statusServ.findDefaultStatus(InvoiceStatus.class));
		invoice.setType(type);

		// set a new action of the invoice being created
		this.addOnInsertActivity(invoice, currentContact);

		// if the address was changed on the form then the pinvoice addressid
		// would have updated, but not the Address on the pinvoice
		invoice.setAddress(address);
		// If the currentContact belongs to the allocated subdiv, use it as
		// business contact, otherwise use the allocated subdiv's default
		// contact.
		if (currentContact.getSub().getId().intValue() == allocatedSubdiv.getId().intValue()) {
			invoice.setBusinessContact(currentContact);
		} else {
			invoice.setBusinessContact(allocatedSubdiv.getDefaultContact());
		}
		invoice.setComp(comp);
		invoice.setCreatedBy(currentContact);

		// just set the system default currency
		invoice.setCurrency((currencyId == null) ? this.currencyServ.getDefaultCurrency()
				: this.currencyServ.findSupportedCurrency(currencyId));
		invoice.setRate(invoice.getCurrency().getDefaultRate());
		invoice.setDuration(1);
		invoice.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		invoice.setInvoiceDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));

		// Invoice vat rate determined from current allocated subdivision (not
		// necessarily current contact's home company!)
		Company allocatedCompany = allocatedSubdiv.getComp();
		CompanySettingsForAllocatedCompany compSettings = this.companySettingsService.getByCompany(comp,
				allocatedCompany);
		invoice.setOrganisation(allocatedCompany);

		invoice.setVatValue(new BigDecimal("0.00"));
		invoice.setTotalCost(new BigDecimal("0.00"));
		invoice.setFinalCost(new BigDecimal("0.00"));

		// set this manually for now
		invoice.setPricingType(PricingType.CLIENT);

		// PaymentTerms and PaymentMode
		// set by default for now
		if (compSettings != null && compSettings.getPaymentterm() != null && compSettings.getPaymentterm() != null) {
			invoice.setPaymentTerm(compSettings.getPaymentterm());
			// invoice.setPaymentTerm(compSettings.getPaymentterm());
		} else {
			// default from business Company
			invoice.setPaymentTerm(
					this.companySettingsService.getByCompany(allocatedCompany, allocatedCompany).getPaymentterm());
		}
		if (compSettings != null && compSettings.getPaymentMode() != null) {
			invoice.setPaymentMode(compSettings.getPaymentMode());
		} else {
			// default from business Company
			invoice.setPaymentMode(
					this.companySettingsService.getByCompany(allocatedCompany, allocatedCompany).getPaymentMode());
		}

		// Add default notes
		invoice.setNotes(noteService.initalizeNotes(invoice, InvoiceNote.class, LocaleContextHolder.getLocale()));
		invoice.setItems(new TreeSet<>(new InvoiceItemComparator()));

		// add joblinks
		Set<InvoiceJobLink> jobLinks = jobNos != null ?

				jobNos.stream().filter(StringUtils::isNoneEmpty)
						.map(jobServ::getJobByExactJobNo).filter(Objects::nonNull)
						.map(job -> {
							InvoiceJobLink link = new InvoiceJobLink();
							link.setInvoice(invoice);
							link.setJob(job);
							link.setJobno(job.getJobno());
							link.setSystemJob(true);
							return link;
						}).collect(Collectors.toCollection(() -> new TreeSet<>(new InvoiceJobLinkComparator()))) : Collections.emptySet();

		invoice.setJobLinks(jobLinks);

		// Vat Rate from Adress of deliveries
		VatRate vatRate = null;
		AddressSettingsForAllocatedCompany addressSettings = null;
		// set vat rate
		// (1) from delivery of invoice items
		if (invoice.getJobLinks() != null && invoice.getJobLinks().size() > 0) {
			InvoiceJobLink ji = invoice.getJobLinks().iterator().next();
			if (ji != null && ji.getJob() != null && ji.getJob().getDeliveries() != null
					&& ji.getJob().getDeliveries().size() > 0) {
				JobDelivery jd = ji.getJob().getDeliveries().iterator().next();
				if (jd != null && jd.isClientDelivery()) {
					addressSettings = this.addressSettingsService.getByCompany(jd.getAddress(), allocatedCompany);
				}
			}
		}
		if (addressSettings != null) {
			vatRate = addressSettings.getVatRate();
		}
		// (2) from invoice address
		if (vatRate == null) {
			addressSettings = this.addressSettingsService.getByCompany(invoice.getAddress(), allocatedCompany);
			if (addressSettings != null)
				vatRate = addressSettings.getVatRate();
		}
		// (3) from company settings
		if (vatRate == null) {
			if (compSettings != null) {
				vatRate = compSettings.getVatrate();
			} else {
				// default from business Company
				vatRate = this.companySettingsService.getByCompany(allocatedCompany, allocatedCompany).getVatrate();
			}
		}

		invoice.setVatRate(new BigDecimal("0.00"));

		if (vatRate != null) {
			if (compSettings != null) {
				// Finally, the rate is only used IF the company is taxable -
				// non-taxable gets 0 rate
				if (compSettings.isTaxable()) {
					invoice.setVatRate(vatRate.getRate());
				}
			} else {
				if (this.companySettingsService.getByCompany(allocatedCompany, allocatedCompany).isTaxable()) {
					invoice.setVatRate(vatRate.getRate());
				}
			}
		}

		invoice.setVatRateEntity(vatRate);

		// perist the invoice
		this.invoiceService.insertInvoice(invoice);

		return invoice;
	}

	/**
	 * Takes the preprepared {@link PInvoice} and persists into a real
	 * {@link Invoice} entity.
	 * 
	 * @param pinvoice the {@link PInvoice} to translate.
	 * @return the new {@link Invoice}.
	 */
	public Invoice createInvoiceFromPInvoice(PInvoice pinvoice, Contact currentContact, Subdiv allocatedSubdiv) {
		// make that that the invoice has some items and that at least one of
		// those items has been set to 'include', otherwise we don't create an
		// invoice (because we don't want invoices with no items) and instead
		// return null
		if (!this.validatePInvoiceCanBeCreated(pinvoice)) {
			this.logger.info("Blocked creation of invoice with no items / no 'included' items for jobs "
					+ pinvoice.getJobs().keySet());
			return null;
		} else {
			// create the invoice
			Invoice invoice = new Invoice();
			Company company = pinvoice.getCompany();
			invoice.setOrganisation(allocatedSubdiv.getComp());

			// set the invno
			String invno = numerationService.generateNumber(NumerationType.INVOICE, allocatedSubdiv.getComp(),
					allocatedSubdiv);
			invoice.setInvno(invno);

			// set the status of the invoice
			invoice.setStatus((InvoiceStatus) this.statusServ.findDefaultStatus(InvoiceStatus.class));

			// add an 'oninsert' activity
			this.addOnInsertActivity(invoice, currentContact);

			// set the other basic fields
			invoice.setType(
					pinvoice.getTypeId() == 0 ? this.invTypeServ.findInvoiceTypeByName(InvoiceTypeNames.CALIBRATION)
							: this.invTypeServ.get(pinvoice.getTypeId()));

			// if the address was changed on the form then the pinvoice
			// addressid
			// would have updated, but not the Address on the pinvoice
			invoice.setAddress(pinvoice.getAddress().getAddrid() == pinvoice.getAddressId() ? pinvoice.getAddress()
					: this.addrServ.get(pinvoice.getAddressId()));
			invoice.setComp(company);
			invoice.setCreatedBy(currentContact);
			// If the currentContact belongs to the allocated subdiv, use it as
			// business contact, otherwise use the allocated subdiv's default
			// contact.
			if (currentContact.getSub().getId().intValue() == allocatedSubdiv.getId().intValue()) {
				invoice.setBusinessContact(currentContact);
			} else {
				invoice.setBusinessContact(allocatedSubdiv.getDefaultContact());
			}
			invoice.setDuration(1);
			invoice.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			invoice.setInvoiceDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));

			// if there is no currency/rate set on the pinvoice, use the system
			// defaults
			SupportedCurrency curr = (pinvoice.getCurrency() != null) ? pinvoice.getCurrency()
					: this.currencyServ.getDefaultCurrency();
			invoice.setCurrency(curr);
			BigDecimal rate = (pinvoice.getRate() != null) ? pinvoice.getRate() : new BigDecimal("1.00");
			invoice.setRate(rate);

			// We must use the current working company - not necessarily the
			// current contact's home subdiv!
			Company allocatedCompany = allocatedSubdiv.getComp();
			CompanySettingsForAllocatedCompany compSettings = this.companySettingsService.getByCompany(company,
					allocatedCompany);

			invoice.setVatValue(new BigDecimal("0.00"));
			invoice.setFinalCost(new BigDecimal("0.00"));

			// Add default notes
            invoice.setNotes(noteService.initalizeNotes(invoice, InvoiceNote.class, LocaleContextHolder.getLocale()));
            invoice.setItems(new TreeSet<>(new InvoiceItemComparator()));

			// set this manually for now
			invoice.setPricingType(pinvoice.getPricingType());

			// PaymentTerms and PaymentMode
			// set by default for now
			if (compSettings.getPaymentterm() != null && compSettings.getPaymentterm() != null) {
				invoice.setPaymentTerm(compSettings.getPaymentterm());
			} else {
				// default from business Company
				invoice.setPaymentTerm(
						this.companySettingsService.getByCompany(allocatedCompany, allocatedCompany).getPaymentterm());
			}
			if (compSettings.getPaymentMode() != null) {
				invoice.setPaymentMode(compSettings.getPaymentMode());
			} else {
				// default from business Company
				invoice.setPaymentMode(
						this.companySettingsService.getByCompany(allocatedCompany, allocatedCompany).getPaymentMode());
			}

			List<Ledgers> ledgers = Collections.singletonList(Ledgers.SALES_LEDGER);
			Map<Integer, Job> invoicedJobs = new TreeMap<>();

			// add items
			int itemno = 1;
			for (PInvoiceItem pitem : pinvoice.getItems()) {
				if (pitem.isInclude()) {
					InvoiceItem item = new InvoiceItem();
					Cost cost = pitem.getCost();
					if (cost == null) {
						item.setTotalCost(BigDecimal.valueOf(0, 2));
						item.setFinalCost(BigDecimal.valueOf(0, 2));
						item.setGeneralDiscountRate(BigDecimal.valueOf(0, 2));
						item.setGeneralDiscountValue(BigDecimal.valueOf(0, 2));
					} else {
						item.setTotalCost(cost.getTotalCost());
						item.setFinalCost(cost.getFinalCost());
						item.setGeneralDiscountRate(cost.getDiscountRate());
						item.setGeneralDiscountValue(cost.getDiscountValue());
					}
					item.setInvoice(invoice);
					item.setItemno(itemno);
					item.setJobItem(pitem.getJitem());
					List<? extends Delivery> deliveries = this.deliveryService
							.findByJobItem(pitem.getJitem().getJobItemId(), DeliveryType.CLIENT);
					Address shipToAddress = deliveries.isEmpty() ? pitem.getJitem().getJob().getReturnTo()
							: deliveries.get(0).getAddress();
					item.setShipToAddress(shipToAddress);
					item.setQuantity(1);
					item.setBusinessSubdiv(pitem.getJitem().getJob().getOrganisation());
					invoice.getItems().add(item);
					invoicedJobs.put(pitem.getJitem().getJob().getJobid(), pitem.getJitem().getJob());
					if (invoice.getPricingType().equals(PricingType.SITE))
						// this is a bit funky, but basically we default the
						// site cost nominal to be a nominal for calibration
						// costs for this department/proc combo
						item.setNominal(this.nomCodeServ.findBestMatchingNominalCode(ledgers,
								CostType.CALIBRATION.getTypeid(), item.getJobItem()));
					else if (pitem.getCost() != null && pitem.getCost().getCostType() != null)
						item.setNominal(this.nomCodeServ.findBestMatchingNominalCode(ledgers,
								pitem.getCost().getCostType().getTypeid(), item.getJobItem()));
					itemno++;
				}
			}
			for (PInvoiceExpenseItem invExpItem : pinvoice.getExpenseItems())
				if (invExpItem.isInclude()) {
					InvoiceItem item = new InvoiceItem();
					Cost cost = invExpItem.getServiceCost().getCost();
					item.setTotalCost(cost.getTotalCost());
					item.setFinalCost(cost.getFinalCost());
					item.setGeneralDiscountRate(cost.getDiscountRate());
					item.setGeneralDiscountValue(cost.getDiscountValue());
					item.setDescription(invExpItem.getExpenseItem().getComment());
					item.setInvoice(invoice);
					item.setItemno(itemno);
					item.setExpenseItem(invExpItem.getExpenseItem());
					Address shipToAddress = invExpItem.getExpenseItem().getJob().getReturnTo();
					item.setShipToAddress(shipToAddress);
					item.setQuantity(1);
					item.setBusinessSubdiv(invExpItem.getExpenseItem().getJob().getOrganisation());
					invoice.getItems().add(item);
					invoicedJobs.put(invExpItem.getExpenseItem().getJob().getJobid(),
							invExpItem.getExpenseItem().getJob());
					// add cost items
					NominalCode nominal = nomCodeServ.findBestMatchingNominalCode(Ledgers.SALES_LEDGER,
							CostType.SERVICE, invExpItem.getExpenseItem().getModel().getDescription().getId());
					item.setNominal(nominal);
					itemno++;
				}
			// do some extra site invoice processing
			if (pinvoice instanceof PSiteInvoice) {
                PSiteInvoice spinvoice = (PSiteInvoice) pinvoice;
                invoice.setTotalCost(spinvoice.getTotalCost());
                String siteCostNotes = StringTools
                    .trimToLength(this.siteInvoiceGenerator.getSiteInvoiceNotesAsString(spinvoice), 1000);
                invoice.setSiteCostingNote(siteCostNotes);
                taxCalculator.calculateTax(invoice);
            } else {
                invoice.setTotalCost(invoice.getItems().stream().map(InvoiceItem::getFinalCost).reduce(BigDecimal.ZERO,
                    BigDecimal::add));
                logger.debug("Invoice total amount: " + invoice.getTotalCost());
                taxCalculator.calculateTax(invoice);
                logger.debug("Invoice Tax: " + invoice.getVatValue());
            }
            CostCalculator.updatePricingWithCalculatedTotalCost(invoice);
            TaxCalculationSystem taxCalculationSystem = avalaraAware ? TaxCalculationSystem.AVATAX
                : TaxCalculationSystem.DEFAULT;
            // set Vat from invoice address when Vat Is retrieved via Avalara
            if (taxCalculationSystem.equals(TaxCalculationSystem.AVATAX)) {
                AddressSettingsForAllocatedCompany addressSettings = this.addressSettingsService
                    .getByCompany(invoice.getAddress(), invoice.getOrganisation());
                if (addressSettings != null && addressSettings.getVatRate() != null) {
                    VatRate vatRate = addressSettings.getVatRate();
                    invoice.setVatRateEntity(vatRate);
                    invoice.setVatRate(vatRate.getRate());
                }
            }
            // add joblinks - only for job items that were actually invoiced
			TreeSet<InvoiceJobLink> jobLinks = new TreeSet<>(new InvoiceJobLinkComparator());
			for (Integer key : invoicedJobs.keySet()) {
				InvoiceJobLink link = new InvoiceJobLink();
				link.setInvoice(invoice);
				link.setJob(invoicedJobs.get(key));
				link.setJobno(link.getJob().getJobno());
				link.setSystemJob(true);
				jobLinks.add(link);
			}
			invoice.setJobLinks(jobLinks);
			// perist the invoice
			this.invoiceService.insertInvoice(invoice);
			return invoice;
		}
	}

	/**
	 * Takes the prepared list of {@link PInvoice} objects and persists them into
	 * real {@link Invoice} entities.
	 * 
	 * @param pinvoices         list of {@link PInvoice}.
	 * @return list of {@link Invoice}.
	 */
	public List<Invoice> createInvoicesFromPInvoiceList(List<PInvoice> pinvoices, Contact currentContact,
			Subdiv allocatedSubdiv) {
		List<Invoice> invoices = new ArrayList<>();
		for (PInvoice pinv : pinvoices) {
			Invoice inv = this.createInvoiceFromPInvoice(pinv, currentContact, allocatedSubdiv);
			if (inv != null) {
				invoices.add(inv);
			}
		}
		return invoices;
	}

	/**
	 * Checks the given {@link LinkedHashMap} for a {@link PInvoice} for the given
	 * {@link Company}, if none exists then a new one is created. This is an
	 * internal method exposed only for test reasons.
	 * 
	 * @param co                   the {@link Company}.
	 * @param proposedCompInvoices list of existing {@link PInvoice}.
	 * @return the {@link PInvoice}.
	 */
	public PInvoice getPinvoiceOrCreateIfNotPresent(Company co, Collection<Job> jobs,
			LinkedHashMap<Integer, PInvoice> proposedCompInvoices, InvoiceType type) {
		PInvoice inv = proposedCompInvoices.get(co.getCoid());
		if (inv == null) {
			inv = new PInvoice(co, type, null);
			inv.setJobs(jobs.stream().collect(Collectors.toMap(Job::getJobid, job -> job)));
			this.invoiceAddressService.setAddress(inv);
		} else {
			// make sure that the jobs we're invoicing are logged
			for (Job job : jobs)
				if (!inv.getJobs().containsKey(job.getJobid()))
					inv.getJobs().put(job.getJobid(), job);
		}
		if (inv.getCurrency() == null) {
			val jobOpt = jobs.stream().findAny();
			if (jobOpt.isPresent()) {
				inv.setCurrency(jobOpt.get().getCurrency());
				inv.setCurrencyId(jobOpt.get().getCurrency().getCurrencyId());
				inv.setRate(jobOpt.get().getRate());
			}
		}
		return inv;
	}

	/**
	 * Updates any job items for the Proposed invoices that have been indicated as
	 * not being invoiced and stores their "not invoiced reason" and any associated
	 * comments Updates the looked-up job item back into the PInvoiceItem as updates
	 * may be made
	 * 
	 */
	public void updateNotInvoicedItems(List<PInvoice> pInvoices, Contact contact) {
		pInvoices.stream().flatMap(p -> p.getItems().stream()).forEach(pInvoiceItem -> {
			JobItem jobItem = jobitemService.findJobItem(pInvoiceItem.getJitem().getJobItemId());
			PNotInvoiced pNotInvoiced = pInvoiceItem.getNotInvoiced();
			if (pNotInvoiced.isSelected()) {
				jobItemNotInvoicedService.recordNotInvoiced(jobItem, pNotInvoiced.getReason(),
						pNotInvoiced.getComments(), pNotInvoiced.getPeriodicInvoiceId(), contact);
			} else if (jobItem.getNotInvoiced() != null) {
				jobItemNotInvoicedService.delete(jobItem.getNotInvoiced());
				jobItem.setNotInvoiced(null);
			}
			pInvoiceItem.setJitem(jobItem);
		});
	}

	public void updateNotInvoicedExpenseItems(List<PInvoice> pInvoices, Contact contact) {
		pInvoices.stream().flatMap(i -> i.getExpenseItems().stream()).forEach(ei -> {
			val expenseItem = expenseItemService.get(ei.getExpenseItem().getId());
			val pNotInvoiced = ei.getNotInvoiced();
			if (pNotInvoiced.isSelected())
				expenseItemNotInvoicedService.recordNotInvoiced(expenseItem, pNotInvoiced.getReason(),
						pNotInvoiced.getComments(), pNotInvoiced.getPeriodicInvoiceId(), contact);
			else if (expenseItem.getNotInvoiced() != null) {
				expenseItemNotInvoicedService.delete(expenseItem.getNotInvoiced());
				expenseItem.setNotInvoiced(null);
			}
			ei.setExpenseItem(expenseItem);
		});
	}

	public void updateNotInvoicedItems(int invoiceId, List<PInvoiceItem> items, Contact contact) {
		for (PInvoiceItem pInvoiceItem : items) {
			JobItem jobItem = jobitemService.findJobItem(pInvoiceItem.getJitem().getJobItemId());
			PNotInvoiced pNotInvoiced = pInvoiceItem.getNotInvoiced();
			pNotInvoiced.setPeriodicInvoiceId(invoiceId);
			pNotInvoiced.setSelected(pInvoiceItem.isInclude());
			if (pInvoiceItem.isInclude()) {
				jobItemNotInvoicedService.recordNotInvoiced(jobItem, NotInvoicedReason.CONTRACT, "",
						pNotInvoiced.getPeriodicInvoiceId(), contact);
			} else {
				if (jobItem.getNotInvoiced() != null) {
					jobItemNotInvoicedService.delete(jobItem.getNotInvoiced());
					jobItem.setNotInvoiced(null);
				}
			}
			pInvoiceItem.setJitem(jobItem);
		}
	}

	public int countAlreadyInvoicedItems(List<PInvoice> pinvoices) {
		int count = 0;
		for (PInvoice pinvoice : pinvoices) {
			for (PInvoiceItem item : pinvoice.getItems()) {
				if (item.isAlreadyInvoiced())
					count++;
			}
		}
		return count;
	}

	public List<PInvoice> prepareProspectiveInvoices(List<Integer> jobids, String itype) {
		// load all the jobs requested to be invoice
		List<Job> jobs = this.jobServ.getJobsById(jobids);
		ArrayList<Integer> jobItemIds = new ArrayList<>();
		for (Job job : jobs)
			for (JobItem ji : job.getItems())
				jobItemIds.add(ji.getJobItemId());
		List<JobExpenseItem> expenseItems = jobs.stream().flatMap(job -> job.getExpenseItems().stream())
				.collect(Collectors.toList());
		// special case - we want to allow creation on an empty pro-forma
		// invoice from a job with no items
		if ((jobItemIds.size() == 0) && "p".equals(itype)) {
			PInvoice inv = this.getPinvoiceOrCreateIfNotPresent(jobs.get(0).getCon().getSub().getComp(), jobs,
					new LinkedHashMap<>(), this.invTypeServ.findInvoiceTypeByName(InvoiceTypeNames.PRO_FORMA));
			ArrayList<PInvoice> pi = new ArrayList<>();
			pi.add(inv);
			return pi;
		} else
			return this.prepareProspectiveInvoicesForJobItems(jobItemIds, expenseItems, itype);
	}

	public Map<Integer, SortedSet<JobCostingItem>> getJobCostingItemMap(List<JobCosting> jobCostings) {
		Map<Integer, SortedSet<JobCostingItem>> resultMap = new HashMap<>();
		for (JobCosting jc : jobCostings) {
			// Site job costings treated separately
			if (!jc.getType().equals(PricingType.SITE)) {
				for (JobCostingItem jcItem : jc.getItems()) {
					Integer jobitemId = jcItem.getJobItem().getJobItemId();
					if (!resultMap.containsKey(jobitemId)) {
						// Comparator will compare by job costing ID when
						// different, so caller will
						// ultimately want the FIRST item
						resultMap.put(jobitemId, new TreeSet<>(new JobCostingItemReverseComparator()));
					}
					resultMap.get(jobitemId).add(jcItem);
				}
			}
		}

		return resultMap;
	}

	/**
	 * Base method that all methods that create invoices eventually get piped
	 * through.
	 * 
	 */
	public List<PInvoice> prepareProspectiveInvoicesForJobItems(List<Integer> jobitemids,
			List<JobExpenseItem> expenseItems, String invType) {
		InvoiceType type = null;
		if ((invType != null) && invType.equalsIgnoreCase("p"))
			type = this.invTypeServ.findInvoiceTypeByName(InvoiceTypeNames.PRO_FORMA);
		else if ((invType != null) && invType.equalsIgnoreCase("v"))
			type = this.invTypeServ.findInvoiceTypeByName(InvoiceTypeNames.VALVES);
		List<PInvoice> pinvoices = new ArrayList<>();
		LinkedHashMap<Integer, PInvoice> proposedCompInvoices = new LinkedHashMap<>();
		if ((jobitemids != null) && (jobitemids.size() > 0)) {
			List<JobItem> items = this.jobitemService.getJobItemsByIds(jobitemids);
			Set<Integer> jobids = items.stream().map(item -> item.getJob().getJobid()).collect(Collectors.toSet());
			List<JobCosting> jobCostings = this.jobCostingService.getEagerJobCostingListForJobIds(jobids);
			Map<Integer, SortedSet<JobCostingItem>> jobItemIdToJobCostingItems = getJobCostingItemMap(jobCostings);
			// creates any necessary site invoices
			PInvoice siteInv = this.siteInvoiceGenerator.createPInvoiceForSiteCostings(jobCostings, type);
			if (siteInv != null) {
				this.logger.info("Creating one site pinvoice");
				pinvoices.add(siteInv);
			}
			if (items.size() > 0) {
				for (JobItem ji : items) {
					boolean jobCostInvoiced = false;
					List<Job> singleJobList = Collections.singletonList(ji.getJob());
					// Don't invoice site costing items from here.
					SortedSet<JobCostingItem> jcItems = jobItemIdToJobCostingItems.getOrDefault(ji.getJobItemId(),
							Collections.emptySortedSet());
					// Get the job costing item with the highest job costing ID
					// as the newest one
					// (reverse comparator)
					JobCostingItem jcItem = !jcItems.isEmpty() ? jcItems.first() : null;
					if (jcItem != null) {
						PInvoice inv = this.getPinvoiceOrCreateIfNotPresent(ji.getJob().getCon().getSub().getComp(),
								singleJobList, proposedCompInvoices, type);
						List<PInvoiceItem> pInvoiceItems = invoiceItemGenerator.createPInvoiceItem(inv, ji, jcItem);
						inv.getItems().addAll(pInvoiceItems);
						proposedCompInvoices.put(inv.getCoid(), inv);
						jobCostInvoiced = true;
					}
					// }
					// we only try and create invoice items from contract review
					// items if we don't have any site invoices
					if (!jobCostInvoiced && (siteInv == null)) {
						// now check there is an invoice for this
						// company and if not create one
						PInvoice inv = this.getPinvoiceOrCreateIfNotPresent(ji.getJob().getCon().getSub().getComp(),
								singleJobList, proposedCompInvoices, type);
						// use contract review cost
						logger.debug("No costings found for jobitem");
						List<PInvoiceItem> pInvoiceItems = this.invoiceItemGenerator
								.createPInvoiceItemsFromWithContractReviewPrices(inv, ji);
						inv.getItems().addAll(pInvoiceItems);
						proposedCompInvoices.put(inv.getCoid(), inv);
					}
				}
				for (Integer key : proposedCompInvoices.keySet()) {
					pinvoices.add(proposedCompInvoices.get(key));
				}
			}
		}
		Map<Company, List<JobExpenseItem>> expenseItemsByCompany = (expenseItems != null ? expenseItems
				: Collections.<JobExpenseItem>emptyList()).stream()
						.collect(Collectors.groupingBy(expItem -> expItem.getJob().getCon().getSub().getComp()));
		for (Company company : expenseItemsByCompany.keySet()) {
			Map<Job, List<JobExpenseItem>> expenseItemsByJob = expenseItemsByCompany.get(company).stream()
					.collect(Collectors.groupingBy(JobExpenseItem::getJob));
			PInvoice pInvoice = getPinvoiceOrCreateIfNotPresent(company, expenseItemsByJob.keySet(),
					proposedCompInvoices, type);
			if (!proposedCompInvoices.containsKey(pInvoice.getCoid()))
				proposedCompInvoices.put(pInvoice.getCoid(), pInvoice);
			for (Job job : expenseItemsByJob.keySet())
				for (JobExpenseItem expenseItem : expenseItemsByJob.get(job)) {
					PInvoiceExpenseItem invoiceItem = new PInvoiceExpenseItem(expenseItem, pInvoice);
					PInvoiceItemCost cost = new PInvoiceItemCost();
					cost.setCosttype(CostType.SERVICE);
					invoiceItem.setServiceCost(cost);
					if (expenseItem.getCostingItems() != null && expenseItem.getCostingItems().size() > 0) {
						/* Set cost by latest job costing */
						val costingItemOpt = expenseItem.getCostingItems().stream()
								.max(JobCostingExpenseItem::compareTo);
						costingItemOpt.ifPresent(costingItem -> {
							invoiceItem.setJobCosting(costingItem.getJobCosting());
							invoiceItem.setSource(CostSource.JOB_COSTING);
							cost.setCost(costingItem.getServiceCost());
							invoiceItem.setServiceCostId(costingItem.getServiceCost().getCostid());
							invoiceItem.setCosted(true);
						});
					} else {
						/* No job costing -> take price from expense item */
						invoiceItem.setSource(CostSource.MANUAL);
						ServiceCost serviceCost = new JobCostingServiceCost();
						serviceCost.setCostType(CostType.SERVICE);
						serviceCost.setTotalCost(expenseItem.getTotalPrice());
						serviceCost.setDiscountRate(BigDecimal.valueOf(0, 2));
						CostCalculator.updateSingleCost(serviceCost, roundUpFinalCosts);
						cost.setCost(serviceCost);
						invoiceItem.setCosted(true);
					}
					pInvoice.getExpenseItems().add(invoiceItem);
				}
			pInvoice.getExpenseItems().sort(
					(invItem1, invItem2) -> invItem1.getExpenseItem().getJob() == invItem2.getExpenseItem().getJob()
							? invItem1.getExpenseItem().getId() - invItem2.getExpenseItem().getId()
							: invItem1.getExpenseItem().getJob().getJobid()
									- invItem2.getExpenseItem().getJob().getJobid());
			if (!pinvoices.contains(pInvoice))
				pinvoices.add(pInvoice);
		}
		this.removeAllPInvoicesWithNoItems(pinvoices);
		return pinvoices;
	}

	private void removeAllPInvoicesWithNoItems(Collection<PInvoice> pinvoices) {
		List<PInvoice> toRemove = new ArrayList<>();
		for (PInvoice pinvoice : pinvoices) {
			// allow empty pro-forma invoices to be created from jobs
			if (pinvoice.getItems().isEmpty() && pinvoice.getExpenseItems().isEmpty() && pinvoice.getType() != null
					&& pinvoice.getType().getName().equalsIgnoreCase(InvoiceTypeNames.PRO_FORMA)) {
				toRemove.add(pinvoice);
			}
		}
		pinvoices.removeAll(toRemove);
	}

	/**
	 * Checks that this {@link PInvoice} has at least one {@link PInvoiceItem} and
	 * that at least one of it's item has the 'include' property set to true. Blocks
	 * creation of empty {@link Invoice}s.
	 * 
	 */
	public boolean validatePInvoiceCanBeCreated(PInvoice pinvoice) {
		if (pinvoice == null || pinvoice.getItems() == null)
			return false;
		else if (pinvoice.getItems().isEmpty()
				&& (pinvoice.getExpenseItems() == null || pinvoice.getExpenseItems().isEmpty()))
			// allow creation of empty pro-forma invoices
			return pinvoice.getType().getName().equalsIgnoreCase(InvoiceTypeNames.PRO_FORMA);
		else
			return pinvoice.getItems().stream().anyMatch(PInvoiceItem::isInclude)
					|| pinvoice.getExpenseItems().stream().anyMatch(PInvoiceExpenseItem::isInclude);
	}

	/**
	 * Used by periodic linking only
	 * 
	 */
	public List<PInvoiceItem> prepareProposedInvoiceItems(Company company, Integer allocatedCompanyId,
			Integer allocatedSubdivId, String completeJob) {
		List<PInvoiceItem> items = new ArrayList<>();

		List<JobItemInvoiceDto> jobitems1 = this.jobitemService
				.findAllCompleteJobItemsWithOnlyProformaInvoicesProjection(allocatedCompanyId, allocatedSubdivId,
						company, JobSortKey.JOB, completeJob);
		List<JobItemInvoiceDto> jobitems2 = this.jobitemService.findAllCompleteJobItemsWithoutInvoicesProjection(
				allocatedCompanyId, allocatedSubdivId, company, JobSortKey.JOB, completeJob);
		// For now, because expense items aren't linkable, this is separate from
		// invoice
		// generation (but reuses same code)
		List<JobExpenseItem> expenseItems = Collections.emptyList();
		String emptyType = "";
		List<Integer> jobitemids = new ArrayList<>();
		jobitems1.forEach(dto -> jobitemids.add(dto.getJobItemId()));
		jobitems2.forEach(dto -> jobitemids.add(dto.getJobItemId()));
		List<PInvoice> pinvoices = this.prepareProspectiveInvoicesForJobItems(jobitemids, expenseItems, emptyType);
		pinvoices.forEach(pinvoice -> items.addAll(pinvoice.getItems()));

		/*
		 * items.addAll(jobitems1.stream().map(jidto -> { JobItem ji =
		 * jobitemService.findJobItem(jidto.getJobItemId()); return new
		 * PInvoiceItem(ji); }).collect(Collectors.toList()));
		 * items.addAll(jobitems2.stream().map(jidto -> { JobItem ji =
		 * jobitemService.findJobItem(jidto.getJobItemId()); return new
		 * PInvoiceItem(ji); }).collect(Collectors.toList())); // costing for
		 * (PInvoiceItem piitem : items) {
		 * invoiceItemGenerator.loadProposedItemCostings(piitem); }
		 */
		return items;
	}

	public List<PInvoiceExpenseItem> prepareProposedInvoiceExpenseItems(Company company, Integer allocatedCompanyId,
			Integer allocatedSubdivId, String completeJob) {
		List<JobExpenseItem> expenseItems = this.expenseItemService.getInvoiceable(company, allocatedCompanyId,
				allocatedSubdivId, completeJob);
		return expenseItems.stream()
				.filter(e -> e.getInvoiceItems().stream().noneMatch(i -> i.getInvoice().getType().isAddToAccount()))
				.map(PInvoiceExpenseItem::new).collect(Collectors.toList());
	}
}