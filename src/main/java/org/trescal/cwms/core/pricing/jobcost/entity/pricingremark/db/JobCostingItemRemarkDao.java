package org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.JobCostingItemRemark;

public interface JobCostingItemRemarkDao extends BaseDao<JobCostingItemRemark, Integer> {
	List<JobCostingItemRemark> getRemarksForItem(Integer jobCostingItemId);
	List<JobCostingItemRemark> getRemarksForJobCosting(Integer jobCostingId);
}
