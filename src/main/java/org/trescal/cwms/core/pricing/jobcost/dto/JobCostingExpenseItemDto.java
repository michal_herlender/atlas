package org.trescal.cwms.core.pricing.jobcost.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

@Data
public class JobCostingExpenseItemDto {

	private Integer id;
	private Integer itemNo;
	private String modelNameTranslations;
	private String comment;
	private LocalDate date;
	private String serviceTypeShortnameTranslation;
	private BigDecimal singlePrice;
	private Integer quantity;
	private BigDecimal totalPrice;
	private Integer costingExpenseItemId;

	public JobCostingExpenseItemDto() {
	}

	public JobCostingExpenseItemDto(Integer id, Integer itemNo, String modelNameTranslations, String comment,
			LocalDate date, String serviceTypeShortnameTranslation, BigDecimal singlePrice, Integer quantity,
			Integer costingExpenseItemId) {
		super();
		this.id = id;
		this.itemNo = itemNo;
		this.modelNameTranslations = modelNameTranslations;
		this.comment = comment;
		this.date = date;
		this.serviceTypeShortnameTranslation = serviceTypeShortnameTranslation;
		this.singlePrice = singlePrice;
		this.quantity = quantity;
		this.totalPrice = singlePrice.multiply(BigDecimal.valueOf(quantity));
		this.costingExpenseItemId = costingExpenseItemId;
	}

}
