package org.trescal.cwms.core.pricing.invoice.controller;

import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.form.CreateInternalInvoiceValidator;
import org.trescal.cwms.core.pricing.invoice.form.CreateInternalInvoicesForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class CreateInternalInvoicesController {

	private static final Logger logger = LoggerFactory.getLogger(CreateInternalInvoicesController.class);
	private static final String FORM_NAME = "dates";

	@Autowired
	private CompanyService companyService;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private CreateInternalInvoiceValidator validator;

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}

	@ModelAttribute(FORM_NAME)
	public CreateInternalInvoicesForm dates() {
		CreateInternalInvoicesForm form = new CreateInternalInvoicesForm();
		val now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		form.setBegin(now);
		form.setEnd(now);
		return form;
	}

	@RequestMapping(value = "/createinternalinvoices.htm", method = RequestMethod.GET)
	public String onRequest() {
		return "trescal/core/pricing/invoice/createinternalinvoices";
	}

	@RequestMapping(value = "/createinternalinvoices.htm", method = RequestMethod.POST)
	public ModelAndView createInternalInvoices(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@Validated @ModelAttribute(FORM_NAME) CreateInternalInvoicesForm dates, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ModelAndView(onRequest());
		else {
			Company company = companyService.get(companyDto.getKey());
			List<Invoice> invoices = invoiceService.createInternalInvoices(company,
					Date.from(dates.getBegin().atStartOfDay(ZoneOffset.UTC).toInstant()),
					Date.from(dates.getEnd().plusDays(1).atStartOfDay(ZoneOffset.UTC).toInstant()));
			logger.info("Created internal invoices (" + invoices.size() + ")");
			invoices.forEach(invoice -> logger.info(invoice.getInvno()));
			return new ModelAndView(new RedirectView("invoicehome.htm"));
		}
	}
}