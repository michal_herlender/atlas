package org.trescal.cwms.core.pricing.invoice.projection;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.account.entity.AccountStatus;

public interface InvoiceProjectionService {
	/** 
	 * Gets InvoiceProjectionDTOs for the selected IDs and calls get getCompleteReferences
	 */
	List<InvoiceProjectionDTO> getInvoiceProjectionDTOs(Collection<Integer> invoiceIds, InvoiceProjectionCommand command);
	
	List<InvoiceProjectionDTO> getInvoiceProjectionDTOs(Integer allocatedCompanyId, Integer issuedBySubdivId, Integer invoiceStatusId, AccountStatus accountStatus, InvoiceProjectionCommand command);
	
	/** 
	 * Completes the references to other objects for an existing list of results, based on the command provided
	 */
	void getCompleteReferences(List<InvoiceProjectionDTO> invoiceDtos, InvoiceProjectionCommand command);
}
