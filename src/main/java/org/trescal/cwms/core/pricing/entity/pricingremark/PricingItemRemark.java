package org.trescal.cwms.core.pricing.entity.pricingremark;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;

/**
 * Record to track remarks for a specific type of pricing item (e.g. JobCostingItem) 
 * with further differentiation as to the type of remark (costRemarkType) to support
 * detailed cost type comments
 * 
 * @author galen
 *
 * @param <Entity> 
 */
@MappedSuperclass
public abstract class PricingItemRemark<Entity extends PricingItem> extends PricingRemark {
	private Entity pricingItem;

	@NotNull
	@JoinColumn(name="pricingitemid", nullable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Entity getPricingItem() {
		return pricingItem;
	}

	public void setPricingItem(Entity pricingItem) {
		this.pricingItem = pricingItem;
	}
}
