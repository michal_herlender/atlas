package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem_;
import org.trescal.cwms.core.pricing.purchaseorder.projection.PurchaseOrderItemProjectionDTO;

@Repository("PurchaseOrderJobItemDao")
public class PurchaseOrderJobItemDaoImpl extends BaseDaoImpl<PurchaseOrderJobItem, Integer> implements PurchaseOrderJobItemDao {
	
	@Override
	protected Class<PurchaseOrderJobItem> getEntity() {
		return PurchaseOrderJobItem.class;
	}
	
	// Integer pricingItemId, Integer itemno, BigDecimal totalCost, BigDecimal finalCost, Integer jobItemId, 
	// Integer purchaseOrderId, String purchaseOrderNumber
	
	@Override
	public List<PurchaseOrderItemProjectionDTO> getProjectionDTOsForJobItems(Collection<Integer> jobItemIds) {
		if (jobItemIds == null || jobItemIds.isEmpty())
			throw new IllegalArgumentException("At least one jobItemId must be provided");
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrderItemProjectionDTO> cq = cb.createQuery(PurchaseOrderItemProjectionDTO.class);
			Root<PurchaseOrderJobItem> root = cq.from(PurchaseOrderJobItem.class);
			Join<PurchaseOrderJobItem, JobItem> jobItem = root.join(PurchaseOrderJobItem_.ji, JoinType.INNER);
			Join<PurchaseOrderJobItem, PurchaseOrderItem> poItem = root.join(PurchaseOrderJobItem_.poitem, JoinType.INNER);
			Join<PurchaseOrderItem, PurchaseOrder> po = poItem.join(PurchaseOrderItem_.order, JoinType.INNER);
			
			cq.where(jobItem.get(JobItem_.jobItemId).in(jobItemIds));
			cq.select(cb.construct(PurchaseOrderItemProjectionDTO.class, 
					poItem.get(PurchaseOrderItem_.id),
					poItem.get(PurchaseOrderItem_.itemno),
					poItem.get(PurchaseOrderItem_.totalCost),
					poItem.get(PurchaseOrderItem_.finalCost),
					jobItem.get(JobItem_.jobItemId), 
					po.get(PurchaseOrder_.id),
					po.get(PurchaseOrder_.pono),
					po.get(PurchaseOrder_.reqdate)));
			return cq;
		});
	}	
}