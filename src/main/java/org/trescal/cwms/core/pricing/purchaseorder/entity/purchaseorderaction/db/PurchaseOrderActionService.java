package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.db;

import java.util.List;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.PurchaseOrderAction;

public interface PurchaseOrderActionService
{
	PurchaseOrderAction findPurchaseOrderAction(int id);
	void insertPurchaseOrderAction(PurchaseOrderAction purchaseorderaction);
	void updatePurchaseOrderAction(PurchaseOrderAction purchaseorderaction);
	List<PurchaseOrderAction> getAllPurchaseOrderActions();
	void deletePurchaseOrderAction(PurchaseOrderAction purchaseorderaction);
	void saveOrUpdatePurchaseOrderAction(PurchaseOrderAction purchaseorderaction);
}