package org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingLinkedCost;

@Entity
@Table(name = "jobcostinglinkedadjustmentcost")
public class JobCostingLinkedAdjustmentCost extends JobCostingLinkedCost
{
	private JobCostingAdjustmentCost adjustmentCost;
	private ContractReviewAdjustmentCost contractReviewAdjustmentCost;
	private JobCostingAdjustmentCost jobCostingAdjustmentCost;

	@OneToOne(fetch=FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
	@JoinColumn(name = "costid", nullable = false, unique = true)
	public JobCostingAdjustmentCost getAdjustmentCost()
	{
		return this.adjustmentCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "contractrevcostid")
	public ContractReviewAdjustmentCost getContractReviewAdjustmentCost()
	{
		return this.contractReviewAdjustmentCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostadjcostid")
	public JobCostingAdjustmentCost getJobCostingAdjustmentCost()
	{
		return this.jobCostingAdjustmentCost;
	}

	public void setAdjustmentCost(JobCostingAdjustmentCost adjustmentCost)
	{
		this.adjustmentCost = adjustmentCost;
	}

	public void setContractReviewAdjustmentCost(ContractReviewAdjustmentCost contractReviewAdjustmentCost)
	{
		this.contractReviewAdjustmentCost = contractReviewAdjustmentCost;
	}

	public void setJobCostingAdjustmentCost(JobCostingAdjustmentCost jobCostingAdjustmentCost)
	{
		this.jobCostingAdjustmentCost = jobCostingAdjustmentCost;
	}
}
