package org.trescal.cwms.core.pricing.jobcost.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class JobCostingItemCostDto {

	private Boolean active;
	private String costType;
	private String costTypeMessageCode;
	private String costTypeName;
    private String costSrc;
    private BigDecimal discountValue;
    private BigDecimal discountRate;
    private BigDecimal totalCost;
    private BigDecimal finalCost;
    private Integer quotationId;
    private String quotationNo;
    private Integer quotationVer;
    private Boolean quotationIssued;
    private Boolean quotationExpired;
    private LocalDate quotationIssuedate;
    private Integer quotationItemId;
    private Integer quotationItemNo;

    public JobCostingItemCostDto(Boolean active, CostType costType, CostSource costSrc,
                                 BigDecimal discountValue, BigDecimal discountRate, BigDecimal totalCost, BigDecimal finalCost,
                                 Integer quotationId, String quotationNo, Integer quotationVer, Boolean quotationIssued,
                                 LocalDate quotationReqdate, Integer quotationDuration, LocalDate quotationExpdate,
                                 LocalDate quotationIssuedate,
                                 Integer quotationItemId,
                                 Integer quotationItemNo) {
		super();
		this.active = active;
		this.costType = costType.getDefaultName();
		this.costTypeMessageCode = costType.getMessageCode();
		this.costTypeName = costType.getName();
		this.costSrc = costSrc.name();
		this.discountValue = discountValue;
		this.discountRate = discountRate;
		this.totalCost = totalCost;
		this.finalCost = finalCost;
		this.quotationId = quotationId;
		this.quotationNo = quotationNo;
		this.quotationVer = quotationVer;
		this.quotationIssued = quotationIssued;
		if(quotationIssued != null)
			this.quotationExpired = isExpired(quotationIssued, quotationReqdate, quotationDuration, quotationExpdate);
		this.quotationIssuedate = quotationIssuedate;
		this.quotationItemId = quotationItemId;
		this.quotationItemNo = quotationItemNo;
	}
	
	public JobCostingItemCostDto(Boolean active, CostType costType,
			CostSource costSrc, BigDecimal discountValue, BigDecimal discountRate, BigDecimal totalCost, BigDecimal finalCost) {
		super();
		this.active = active;
		this.costType = costType.getDefaultName();
		this.costTypeMessageCode = costType.getMessageCode();
		this.costTypeName = costType.getName();
		this.costSrc = costSrc.name();
		this.discountValue = discountValue;
		this.discountRate = discountRate;
		this.totalCost = totalCost;
		this.finalCost = finalCost;
	}
	
	public JobCostingItemCostDto(Boolean active, CostType costType,
			BigDecimal discountValue, BigDecimal discountRate, BigDecimal totalCost, BigDecimal finalCost) {
		super();
		this.active = active;
        this.costType = costType.getDefaultName();
        this.costTypeMessageCode = costType.getMessageCode();
        this.costTypeName = costType.getName();
        this.discountValue = discountValue;
        this.discountRate = discountRate;
        this.totalCost = totalCost;
        this.finalCost = finalCost;
    }


    private boolean isExpired(Boolean isIssued, LocalDate reqDate, Integer duration, LocalDate expiryDate) {
        if (!isIssued) {
            // is expiry date is before current date
            return reqDate.plusDays(duration).isBefore(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        } else {
            // is expiry date before current date
            boolean isValid = false;
            if (expiryDate != null)
                isValid = !expiryDate.isBefore(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));

            return !isValid;
		}
	}
}
