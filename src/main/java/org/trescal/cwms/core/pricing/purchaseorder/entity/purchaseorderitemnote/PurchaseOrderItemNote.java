package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "purchaseorderitemnote")
public class PurchaseOrderItemNote extends Note
{
	private PurchaseOrderItem item;
	
	public PurchaseOrderItemNote() {}
	
	public PurchaseOrderItemNote(PurchaseOrderItem item, Note note)  {
		this.item = item;
		this.setNote(note.getNote());
		this.setLabel(note.getLabel());
		this.setPublish(note.getPublish());
		this.setActive(note.isActive());
		this.setSetBy(note.getSetBy());
		this.setSetOn(note.getSetOn());
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "id", unique = false, nullable = false, insertable = true, updatable = true)
	public PurchaseOrderItem getItem() {
		return item;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.PURCHASEORDERITEMNOTE;
	}
	
	public void setItem(PurchaseOrderItem item) {
		this.item = item;
	}
}