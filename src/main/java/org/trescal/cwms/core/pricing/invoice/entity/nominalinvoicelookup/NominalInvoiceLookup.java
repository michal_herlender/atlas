package org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;

/*
 * Convenience lookup entity used to map nominal codes onto things like carriage
 * or vat where the nominalType field is a string represetnation of that type
 * element being matched i.e. 'carriage'.
 */
@Entity
@Table(name = "nominalinvoicelookup", uniqueConstraints = { @UniqueConstraint(columnNames = { "nominaltype", "nominalid" }) })
public class NominalInvoiceLookup
{
	public static enum NominalType
	{
		CARRIAGE("carriage");

		String type;

		private NominalType(String type)
		{
			this.type = type;
		}

		public String getType()
		{
			return this.type;
		}

		public void setType(String type)
		{
			this.type = type;
		}
	}

	private int id;
	private String nominalType;
	private NominalCode nominalCode;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "nominalid", unique = true, nullable = false)
	public NominalCode getNominalCode()
	{
		return this.nominalCode;
	}

	@NotEmpty
	@Length(max = 10)
	@Column(name = "nominaltype", length = 10, unique = true, nullable = false)
	public String getNominalType()
	{
		return this.nominalType;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setNominalCode(NominalCode nominalCode)
	{
		this.nominalCode = nominalCode;
	}

	public void setNominalType(String nominalType)
	{
		this.nominalType = nominalType;
	}
}
