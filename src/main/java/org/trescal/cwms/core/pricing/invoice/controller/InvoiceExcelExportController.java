package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.invoice.view.InvoiceXlsxStreamingView;

@Controller @IntranetController
public class InvoiceExcelExportController {
	@Autowired
	private InvoiceXlsxStreamingView xlsxView;

	@RequestMapping(path = "/invoiceexport.xlsx")
	public ModelAndView performExport(@RequestParam(name = "invoiceId", required=true) Integer invoiceId) {
		Map<String,Object> xlsxModel = new HashMap<>();
		xlsxModel.put("invoiceId", invoiceId);
		return new ModelAndView(xlsxView, xlsxModel);
	}
}
