package org.trescal.cwms.core.pricing.purchaseorder.projection;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.trescal.cwms.core.pricing.jobcost.projection.PricingItemProjectionDTO;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class PurchaseOrderItemProjectionDTO extends PricingItemProjectionDTO {

	private Integer jobItemId;
	private Integer purchaseOrderId;
	private PurchaseOrderProjectionDTO purchaseOrder;
	private LocalDate reqDate;
	
	// Explicit constructor for JPA usage
	public PurchaseOrderItemProjectionDTO(Integer pricingItemId, Integer itemno, BigDecimal totalCost, BigDecimal finalCost, Integer jobItemId, 
			Integer purchaseOrderId, String purchaseOrderNumber, LocalDate reqDate) {
		super(pricingItemId, itemno, totalCost, finalCost);
		this.jobItemId = jobItemId;
		this.purchaseOrderId = purchaseOrderId;
		this.purchaseOrder = new PurchaseOrderProjectionDTO(purchaseOrderId, purchaseOrderNumber);
		this.reqDate = reqDate;
		
	}

}
