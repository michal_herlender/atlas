package org.trescal.cwms.core.pricing.jobcost.entity.pricingremark;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.pricing.entity.pricingremark.PricingItemRemark;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Entity
@Table(name="jobcostingitemremark")
public class JobCostingItemRemark extends PricingItemRemark<JobCostingItem> {

}
