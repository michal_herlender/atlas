package org.trescal.cwms.core.pricing.invoice.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.pricing.invoice.dto.PNotInvoiced;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class PNotInvoicedValidator extends AbstractBeanValidator {

	public static String MESSAGE_CODE_COMMENT_REQUIRED = "error.notinvoiced.commentrequired";
	public static String MESSAGE_CODE_UNDEFINED = "error.notinvoiced.undefined";
	public static String MESSAGE_CODE_PERIODIC_INVOICE_REQUIRED = "error.notinvoiced.periodicinvoice.required";
	public static String MESSAGE_CODE_PERIODIC_INVOICE_NOT_ALLOWED = "error.notinvoiced.periodicinvoice.notallowed";
	
	public static String DEFAULT_MESSAGE_COMMENT_REQUIRED = "Comment entries are required for items with 'Other' as a 'not invoiced reason'";
	public static String DEFAULT_MESSAGE_UNDEFINED = "The 'not invoiced reason' of 'Undefined' is not allowed";
	public static String DEFAULT_MESSAGE_PERIODIC_INVOICE_REQUIRED = "A periodic invoice is required for items with 'Contract' as a 'not invoiced reason'";
	public static String DEFAULT_MESSAGE_PERIODIC_INVOICE_NOT_ALLOWED = "A periodic invoice is allowed only for items with 'Contract' as a 'not invoiced reason'";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return PNotInvoiced.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		PNotInvoiced notInvoiced = (PNotInvoiced) object;
		super.validate(notInvoiced, errors);
		if (notInvoiced.isSelected()) {
			
			// Validate periodic invoice selection
			if (notInvoiced.getReason().equals(NotInvoicedReason.CONTRACT)) {
				if (notInvoiced.getPeriodicInvoiceId() == 0) {
					errors.reject(MESSAGE_CODE_PERIODIC_INVOICE_REQUIRED, DEFAULT_MESSAGE_PERIODIC_INVOICE_REQUIRED);
				}
			}
			else {
				if (notInvoiced.getPeriodicInvoiceId() != 0) {
					errors.reject(MESSAGE_CODE_PERIODIC_INVOICE_NOT_ALLOWED, DEFAULT_MESSAGE_PERIODIC_INVOICE_NOT_ALLOWED);
				}
			}
			
			// Validate other requirements
			if (notInvoiced.getReason().equals(NotInvoicedReason.UNDEFINED)) {
				errors.reject(MESSAGE_CODE_UNDEFINED, DEFAULT_MESSAGE_UNDEFINED);
			}
			else if (notInvoiced.getReason().equals(NotInvoicedReason.OTHER)) {
				if (notInvoiced.getComments().trim().length() == 0) {
					errors.reject(MESSAGE_CODE_COMMENT_REQUIRED, DEFAULT_MESSAGE_COMMENT_REQUIRED);
				}
			}
		}

	}

}
