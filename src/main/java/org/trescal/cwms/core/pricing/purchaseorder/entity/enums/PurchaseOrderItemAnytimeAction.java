package org.trescal.cwms.core.pricing.purchaseorder.entity.enums;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

/**
 * Enum defines the basic actions that can be applied to a
 * {@link PurchaseOrderItem} at any time during it's lifecycle.
 * 
 * @author Richard
 */
public enum PurchaseOrderItemAnytimeAction
{
	RECEIVED, UN_RECEIVED, CANCELLED, UN_CANCELLED;
}
