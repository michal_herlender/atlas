package org.trescal.cwms.core.pricing.invoice.entity.invoice;

import java.math.BigDecimal;

public class NominalInvoiceCostDTO
{
	private String nominalCode;
	private String nominalTitle;
	private BigDecimal cost;

	public NominalInvoiceCostDTO(String nominalCode, String nominalTitle, BigDecimal cost)
	{
		super();
		this.nominalCode = nominalCode;
		this.nominalTitle = nominalTitle;
		this.cost = cost;
	}

	public BigDecimal getCost()
	{
		return cost;
	}

	public String getNominalCode()
	{
		return nominalCode;
	}

	public String getNominalTitle()
	{
		return nominalTitle;
	}

	public void setCost(BigDecimal cost)
	{
		this.cost = cost;
	}

	public void setNominalCode(String nominalCode)
	{
		this.nominalCode = nominalCode;
	}

	public void setNominalTitle(String nominalTitle)
	{
		this.nominalTitle = nominalTitle;
	}

}
