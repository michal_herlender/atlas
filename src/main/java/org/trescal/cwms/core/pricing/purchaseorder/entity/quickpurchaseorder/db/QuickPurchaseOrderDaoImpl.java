package org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.pricing.purchaseorder.dto.SupplierPOProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder_;

@Repository("QuickPurchaseOrderDao")
public class QuickPurchaseOrderDaoImpl extends BaseDaoImpl<QuickPurchaseOrder, Integer>
		implements QuickPurchaseOrderDao {
	@Override
	protected Class<QuickPurchaseOrder> getEntity() {
		return QuickPurchaseOrder.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuickPurchaseOrder> findAllUnconsolidatedQuickPurchaseOrders(Company allocatedCompany) {
		Criteria crit = getSession().createCriteria(QuickPurchaseOrder.class);
		crit.add(Restrictions.eq("organisation", allocatedCompany));
		crit.add(Restrictions.eq("consolidated", false));
		crit.addOrder(Order.asc("createdOn"));
		crit.addOrder(Order.asc("id"));
		return crit.list();
	}

	@Override
	public List<SupplierPOProjectionDTO> findAllUnconsolidatedQuickPurchaseOrdersUsingProjection(
			Integer allocatedCompanyId, Integer SubdivId) {
		return getResultList(cb -> {

			CriteriaQuery<SupplierPOProjectionDTO> cq = cb.createQuery(SupplierPOProjectionDTO.class);
			Root<QuickPurchaseOrder> root = cq.from(QuickPurchaseOrder.class);

			Predicate clauses = cb.conjunction();

			clauses.getExpressions().add(cb.equal(root.get(QuickPurchaseOrder_.organisation), allocatedCompanyId));
			clauses.getExpressions().add(cb.equal(root.get(QuickPurchaseOrder_.consolidated), false));
			if (SubdivId != null) {
				Join<QuickPurchaseOrder, Contact> bcCon = root.join(QuickPurchaseOrder_.createdBy);
				Join<Contact, Subdiv> bcSubdiv = bcCon.join(Contact_.sub);
				clauses.getExpressions().add(cb.equal(bcSubdiv.get(Subdiv_.subdivid), SubdivId));
			}

			cq.orderBy(cb.asc(root.get(QuickPurchaseOrder_.createdOn)), cb.asc(root.get(QuickPurchaseOrder_.id)));

			cq.where(clauses);

			cq.select(cb.construct(SupplierPOProjectionDTO.class, root.get(QuickPurchaseOrder_.id),
					root.get(QuickPurchaseOrder_.pono), root.get(QuickPurchaseOrder_.description),
					root.get(QuickPurchaseOrder_.jobno),
					root.get(QuickPurchaseOrder_.createdBy).get(Contact_.firstName),
					root.get(QuickPurchaseOrder_.createdBy).get(Contact_.lastName),
					root.get(QuickPurchaseOrder_.createdOn)));
			return cq;
		});

	}
}