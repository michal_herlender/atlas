package org.trescal.cwms.core.pricing.invoice.dto;

import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

public class PInvoiceItemCost
{
	private CostType costtype;
	private Cost cost;

	public Cost getCost()
	{
		return this.cost;
	}

	public CostType getCosttype()
	{
		return this.costtype;
	}

	public void setCost(Cost cost)
	{
		this.cost = cost;
	}

	public void setCosttype(CostType costtype)
	{
		this.costtype = costtype;
	}

}
