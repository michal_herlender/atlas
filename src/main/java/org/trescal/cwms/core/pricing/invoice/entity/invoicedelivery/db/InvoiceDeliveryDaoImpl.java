package org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.InvoiceDelivery;

@Repository("InvoiceDeliveryDao")
public class InvoiceDeliveryDaoImpl extends BaseDaoImpl<InvoiceDelivery, Integer> implements InvoiceDeliveryDao {
	
	@Override
	protected Class<InvoiceDelivery> getEntity() {
		return InvoiceDelivery.class;
	}
}