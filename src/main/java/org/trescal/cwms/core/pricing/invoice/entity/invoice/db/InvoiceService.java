package org.trescal.cwms.core.pricing.invoice.entity.invoice.db;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.pricing.invoice.dto.CompanyReadyToBeInvoiced;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.dto.ReadyToInvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.NominalInvoiceCostDTO;
import org.trescal.cwms.core.pricing.invoice.form.InvoiceHomeForm;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

public interface InvoiceService {

	Map<Integer, ReadyToInvoiceDTO> getReadyToInvoice(Integer subdivId, Integer companyId);

	List<Invoice> createInternalInvoices(Company allocatedCompany, Date begin, Date end);

	void delete(Invoice invoice);

	/**
	 * Deletes the {@link Invoice} identified by the given id, logs the deletion in
	 * a {@link DeletedComponent}. Validates the invoice exists and that the
	 * {@link DeletedComponent} has been completed properly.
	 * 
	 * @param invoiceid the id of the {@link Invoice} to delete.
	 * @return {@link ResultWrapper} indicating success of the operation.
	 */
	ResultWrapper deleteInvoice(int invoiceid, String reason, String username);

	Invoice findInvoice(int id);

	Invoice findByInvoiceNo(String invoiceNo, Integer orgId);

	/**
	 * Creates an {@link Invoice} document for the invoice with the given id.
	 * 
	 * @param subdivId TODO
	 *
	 * @return {@link Document} object.
	 */
	Document generateInvoiceDocument(Invoice invoice, String docType, List<String> sessionNewFileList, Contact contact,
			int subdivId, Boolean printDiscount, Boolean printUnitaryPrice, Boolean printQuantity, Boolean showAppendix,
			Boolean summarized, String description, Locale locale);

	/**
	 * Gets a list of all {@link Company}s that have one or more {@link Job}s ready
	 * to invoice and a count of those jobs.
	 * 
	 * @return list matching {@link Company}.
	 */
	List<CompanyReadyToBeInvoiced> getAllCompaniesWithJobsReadyToInvoice(Company allocatedCompany);

	List<InvoiceDTO> getPeriodicInvoiceDTO(Company company, Company allocatedCompany, Integer recentYears);

	List<InvoiceProjectionDTO> getProjectionDtos(Collection<Integer> invoiceIds);
	
	/**
	 * Searches Invoice and returns a list of InvoiceProjectionDTOs matching 
	 * @param allocatedCompanyId : mandatory Integer id of allocated business company
	 * @param issuedBySubdivId : optional Integer id of subdivision issuing invoice (if not a business-company wide search is performed)
	 * @param invoiceStatusId : id of status to query against
	 * @param accountStatus : account status to query
	 * 
	 * At least one of invoiceStatusId or accountStatus must be provided.
	 * 
	 * @return
	 */
	List<InvoiceProjectionDTO> getProjectionDtos(Integer allocatedCompanyId, Integer issuedBySubdivId, Integer invoiceStatusId, AccountStatus accountStatus); 

	/**
	 * Converts invoices into a formatted list of DTOs with invoice number and date
	 *
	 * @param invoices - a List of invoices
	 * @param locale   - Locale to use for "N/A" formatting
	 * @return a list of KeyValue DTOs
	 */
	List<KeyValue<Integer, String>> getPeriodicInvociesOptions(List<Invoice> invoices, Locale locale);

	Invoice getInternalInvoice(Subdiv jobOwner, Pricing<Company> pricingEntity);

	/**
	 * Returns a summary of the nominal invoice costs for an invoice - total cost
	 * per nominal, per invoice.
	 * 
	 * @param invoiceid the id of the invoice
	 * @return List of wrapper objects, one per nominal code.
	 */
	List<NominalInvoiceCostDTO> getNominalInvoiceSummary(int invoiceid);

	/**
	 * Takes the given set of nominal costs and returns the total value.
	 * 
	 * @param nominal list of {@link NominalInvoiceCostDTO} costs associated with an
	 *                invoice.
	 * @return the total value.
	 */
	BigDecimal getTotalNominalCost(List<NominalInvoiceCostDTO> nominal);

	/**
	 * Returns all 'Periodic' invoices since start of previous year
	 *
	 * @param previousYears : number of years prior to current date (e.g. 0 =
	 */
	List<Invoice> getRecentPeriodicInvoices(Company company, Company allocatedCompany, LocalDate date,
											Integer previousYears);

	void insertInvoice(Invoice invoice);

	Invoice merge(Invoice invoice);

	/**
	 * Resets the {@link Invoice} accountStatus to pending {@link AccountStatus} .P.
	 * 
	 * @param invoiceid the id of the invoice to reset.
	 * @return {@link ResultWrapper} indicating the success of the operation.
	 */
	ResultWrapper resetInvoiceAccountStatus(int invoiceid);

	/**
	 * Performs a search for {@link Invoice} using the criteria set in the
	 * {@link InvoiceHomeForm}, results sorted by inv date desc, invno num desc.
	 * 
	 * @param form {@link InvoiceHomeForm}.
	 * @param rs {@link PagedResultSet}.
	 * @return {@link PagedResultSet}.
	 */
	PagedResultSet<InvoiceDTO> searchSortedInvoiceDTO(InvoiceHomeForm form, String username, PagedResultSet<InvoiceDTO> rs);

	/**
	 * Updates and saves the final cost of the given {@link Invoice}.
	 * 
	 * @param invoice the {@link Invoice}.
	 */
	void updateInvoiceCosts(Invoice invoice);

	void updateSummaryDescription(int invoiceId, String description);

	/**
	 * return the List of ids of job items present in the invoice
	 * 
	 * @param invoice the {@link Invoice}.
	 */
	List<Integer> getJobItemsIdsFromInvoice(Invoice invoice);
	
	/**
	 * return the List of ids of jobs present in the invoice
	 * 
	 * @param invoice the {@link Invoice}.
	 */
	List<Integer> getJobsIdsFromInvoice(Invoice invoice);

	/**
	 * renumber all items of the invoice
	 * @param invoice the {@link Invoice}.
	 */
	void renumberItems(Invoice invoice);
}