package org.trescal.cwms.core.pricing.purchaseorder.dto;

public class AjaxPurchaseOrderItemDTO {
	private String shortnameTranslation;
	private String longnameTranslation;
	private Integer jobItemId;
	private Integer jobItemNo;
	
	public String getShortnameTranslation() {
		return shortnameTranslation;
	}

	public void setShortnameTranslation(String shortnameTranslation) {
		this.shortnameTranslation = shortnameTranslation;
	}

	public Integer getJobItemId() {
		return jobItemId;
	}

	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}

	public String getLongnameTranslation() {
		return longnameTranslation;
	}

	public void setLongnameTranslation(String longnameTranslation) {
		this.longnameTranslation = longnameTranslation;
	}

	public Integer getJobItemNo() {
		return jobItemNo;
	}

	public void setJobItemNo(Integer jobItemNo) {
		this.jobItemNo = jobItemNo;
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof AjaxPurchaseOrderItemDTO
				&& ((AjaxPurchaseOrderItemDTO) obj).getJobItemId().equals(this.jobItemId));
	}

	public AjaxPurchaseOrderItemDTO(String shortnameTranslation, String longnameTranslation, Integer jobItemId, Integer itemNo) {
		super();
		this.shortnameTranslation = shortnameTranslation;
		this.longnameTranslation = longnameTranslation;
		this.jobItemId = jobItemId;
		this.jobItemNo = itemNo;
	}
}
