package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem;

import java.util.Comparator;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

/**
 * Default {@link Comparator} implementation that sorts {@link JobCostingItem}
 * entities by their {@link JobItem} (id) or if this is null then by their own
 * ids.
 * 
 * @author richard
 */
public class JobCostingItemComparator implements Comparator<JobCostingItem>
{
	@Override
	public int compare(JobCostingItem o1, JobCostingItem o2)
	{
		if (o1.getId() == null && o2.getId() == null)
		{
			// force a positive return for non-persisted hibernate entities
			// see
			// http://antyardb03:888/cwms2/wiki/development/specifications/hibernatecollections
			return 1;
		}
		else if (o1.getId() == o2.getId())
		{
			return 0;
		}
		else
		{
			if ((o1.getJobItem() != null) && (o2.getJobItem() != null))
			{
				return ((Integer) o1.getJobItem().getJobItemId()).compareTo(o2.getJobItem().getJobItemId());
			}
			else
			{
				return ((Integer) o1.getId()).compareTo(o2.getId());
			}
		}
	}
}
