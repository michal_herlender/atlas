package org.trescal.cwms.core.pricing.supplierinvoice.entity;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "supplierinvoice")
public class SupplierInvoice extends Allocated<Company> {
    private int id;
    private LocalDate invoiceDate;
    private String invoiceNumber;
    private LocalDate paymentDate;
    private PaymentTerm paymentTerm;
    private PurchaseOrder purchaseOrder;
    private PaymentMode paymentMode;
    private BigDecimal totalWithoutTax;
    private BigDecimal totalWithTax;
    private SupportedCurrency currency;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Type(type = "int")
    public int getId() {
        return id;
    }


    @Enumerated(EnumType.STRING)
    @Column(name = "paymentterms", length = 10)
    public PaymentTerm getPaymentTerm() {
        return paymentTerm;
    }


    public void setPaymentTerm(PaymentTerm paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NotNull
    @Column(name = "invoicedate", nullable = false, columnDefinition = "date")
    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @NotNull
    @Length(min = 1, max = 100)
    @Column(name = "invoicenumber", length = 100, nullable = false)
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @NotNull
    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "purchaseorderid", nullable = false)
    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    @Column(name = "paymentdate", columnDefinition = "date")
    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }

    // TODO add not nullable constraint and @Null in future upgrade
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paymentmodeid")
    public PaymentMode getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(PaymentMode paymentMode) {
        this.paymentMode = paymentMode;
    }

    @Column(name = "totalnet", precision = 10, scale = 2)
    public BigDecimal getTotalWithoutTax() {
        return totalWithoutTax;
    }

    public void setTotalWithoutTax(BigDecimal totalWithoutTax) {
        this.totalWithoutTax = totalWithoutTax;
    }

    @Column(name = "totalgross", precision = 10, scale = 2)
    public BigDecimal getTotalWithTax() {
        return totalWithTax;
    }

    public void setTotalWithTax(BigDecimal totalWithTax) {
        this.totalWithTax = totalWithTax;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currencyid", foreignKey = @ForeignKey(name = "FK_supplierinvoice_currency"))
    public SupportedCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(SupportedCurrency currency) {
        this.currency = currency;
    }
}
