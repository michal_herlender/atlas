package org.trescal.cwms.core.pricing.creditnote.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteDTO;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.db.CreditNoteStatusService;
import org.trescal.cwms.core.pricing.creditnote.form.CreditNoteHomeForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@RequestMapping("/creditnotehome.htm")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class CreditNoteHomeController
{
	@Autowired
	private CreditNoteService cnServ;
	@Autowired
	private CreditNoteStatusService cnsServ;
	@Autowired
	private CompanyService companyService; 
	@Autowired
	private MessageSource messages;
	@Autowired
	private TranslationService translationService;
	
	public static final String FORM_NAME = "creditnotehomeform";
	public static final String VIEW_NAME = "trescal/core/pricing/creditnote/creditnotehome";
	public static final String REDIRECT_NAME = "trescal/core/pricing/creditnote/creditnoteresults";
	
	@ModelAttribute(FORM_NAME)
	public CreditNoteHomeForm createCreditNoteHomeForm(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		CreditNoteHomeForm form = new CreditNoteHomeForm();
		form.setOrgId(companyDto.getKey());
		return form;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	
	public LinkedHashMap<String, List<CreditNote>> prepareHomeLists(int businessCompanyId, Locale locale)
	{
		Company businessCompany = this.companyService.get(businessCompanyId);
		LinkedHashMap<String, List<CreditNote>> res = new LinkedHashMap<String, List<CreditNote>>();
		// get a distinct list of credit note statuses to list when showing all
		// credit notes that require attention
		for (CreditNoteStatus status : this.cnsServ.findAllCreditNoteStatusRequiringAttention())
		{
			String translatedStatus = translationService.getCorrectTranslation(status.getNametranslations(), locale); 
			res.put(translatedStatus, this.cnServ.getAllAtStatus(businessCompany, status));

		}
		String pendingTitle = this.messages.getMessage("creditnote.pendingsync", null, "Pending Sync", locale);
		res.put(pendingTitle, this.cnServ.findAllCreditNotesByAccountStatus(businessCompany, AccountStatus.P));
		String errorTitle = this.messages.getMessage("creditnote.brokeringerrors", null, "Brokering Errors", locale);
		res.put(errorTitle, this.cnServ.findAllCreditNotesByAccountStatus(businessCompany, AccountStatus.E));
		return res;
	}	
	
	@RequestMapping( method = RequestMethod.GET)
	public String referenceData(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto) throws Exception
	{
		model.addAttribute("interestingItems", this.prepareHomeLists(companyDto.getKey(), locale));
		return VIEW_NAME;
	}
	
	@RequestMapping( method = RequestMethod.POST )
	protected String onSubmit(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto, 
			@ModelAttribute(FORM_NAME) CreditNoteHomeForm form, BindingResult result) throws Exception
	{
		if(result.hasErrors()) {
			return referenceData(model, locale, companyDto);
		} 
		else {
			// perform the search
			PagedResultSet<CreditNoteDTO> rs = this.cnServ.searchCreditNotes(form, new PagedResultSet<CreditNoteDTO>(form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(), form.getPageNo() == 0 ? 1 : form.getPageNo()));
			form.setRs(rs);
			return REDIRECT_NAME;
		}
	}
}