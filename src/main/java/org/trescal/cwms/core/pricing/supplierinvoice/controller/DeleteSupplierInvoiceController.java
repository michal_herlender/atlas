package org.trescal.cwms.core.pricing.supplierinvoice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.db.SupplierInvoiceService;
import org.trescal.cwms.core.pricing.supplierinvoice.form.DeleteSupplierInvoiceValidator;
import org.trescal.cwms.core.system.Constants;

@Controller @IntranetController
@RequestMapping("deletesupplierinvoice.htm")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class DeleteSupplierInvoiceController {
	@Autowired
	private SupplierInvoiceService supplierInvoiceService;
	@Autowired
	private DeleteSupplierInvoiceValidator validator;
	
	@RequestMapping(method=RequestMethod.GET)
	public String showView(@RequestParam(name="id", required=true) int supplierInvoiceId, Model model) {
		SupplierInvoice supplierInvoice = this.supplierInvoiceService.get(supplierInvoiceId);
		model.addAttribute("supplierInvoice", supplierInvoice);
		return "trescal/core/pricing/supplierinvoice/deletesupplierinvoice";
	}
	
	/*
	 * Binding errors (just used globally) to username as Errors needs to have a binding, but there's no form.
	 */
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(@RequestParam(name="id", required=true) int supplierInvoiceId, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Errors errors, Model model) {
		SupplierInvoice supplierInvoice = this.supplierInvoiceService.get(supplierInvoiceId);
		validator.validate(supplierInvoice, errors);
		if (errors.hasErrors()) {
			return showView(supplierInvoiceId, model);
		}
		int poid = supplierInvoice.getPurchaseOrder().getId();
		this.supplierInvoiceService.delete(supplierInvoice);
		return "redirect:viewpurchaseorder.htm?id="+poid;
	}
}
