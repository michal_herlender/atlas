package org.trescal.cwms.core.pricing.invoice.controller;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.FinancialPeriod;
import org.trescal.cwms.core.avalara.AvalaraConnector;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateFormatter;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.db.JobStatusService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.invoice.dto.InvoicePoDto;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.NominalInvoiceCostDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceTypeNames;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db.InvoiceTypeService;
import org.trescal.cwms.core.pricing.invoice.form.InvoiceForm;
import org.trescal.cwms.core.pricing.invoice.form.ViewInvoiceValidator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressAction;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.*;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.AccountsTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({ Constants.HTTPSESS_NEW_FILE_LIST, Constants.HTTPSESS_PRICING_CLASS_NAME,
		Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY })
public class ViewInvoiceController {

	@Value("#{props['cwms.config.jobitem.fast_track_turn']}")
	private int fastTrackTurn;
	@Value("${cwms.config.thirdpartyaccounts.name}")
	private String accountsSoftware;
	@Value("${cwms.config.thirdpartyaccounts.supported}")
	private Boolean accountsSoftwareSupported;
	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Autowired
	private AddressService addressServ;
	@Autowired
	private AvalaraConnector avalaraConnector;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private EmailService emailServ;
    @Autowired
    private FileBrowserService fileBrowserServ;
    @Autowired
    private InvoiceService invoiceServ;
    @Autowired
    private InvoiceTypeService invTypeServ;

    @Autowired
    private JobStatusService jobStatusService;
    @Autowired
    private PurchaseOrderService poServ;
    @Autowired
    private PurchaseOrderItemService poItemService;
    @Autowired
    private SupportedCurrencyService currencyServ;
    @Autowired
    private SupportedLocaleService supportedLocaleService;
    @Autowired
    private SystemComponentService scService;
	@Autowired
	private TaxCalculator taxCalculator;
	@Autowired
	private UserService userService;
	@Autowired
	private VatRateFormatter vatRateFormatter;
	@Autowired
	private VatRateService vatRateService;
	@Autowired
	private ViewInvoiceValidator validator;
	// System defaults for invoice document generation
	@Autowired
	private PrintDiscount printDiscount;
	@Autowired
	private PrintUnitaryPrice printUnitaryPrice;
    @Autowired
    private PrintQuantity printQuantity;
    @Autowired
    private ShowAppendix showAppendix;
    @Autowired
    private DetailedInvoiceType detailedInvoiceType;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private CompanySettingsForAllocatedCompanyService settingsService;
    @Autowired
    private InstructionService instructionService;
    // Show/hide features under development / testing
    @Value("${cwms.config.invoice.documentoptions}")
    private Boolean showDocumentOptions;
    @Value("${cwms.config.invoice.oldinvoicepofeature}")
    private Boolean showOldInvoicePOFeature;

    public static final String REQUEST_URL = "/viewinvoice.htm";
    public static final String FORM_NAME = "invoiceform";
    public static final String VIEW_NAME = "trescal/core/pricing/invoice/viewinvoice";

	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}

	@ModelAttribute("currencyList")
	private Object populateCurrencyList() {
		return this.currencyServ.getAllSupportedCurrencys();
	}

	@ModelAttribute(FORM_NAME)
	public InvoiceForm createInvoiceForm(@RequestParam(value = "id") int id) throws Exception {
		Invoice invoice = this.invoiceServ.findInvoice(id);
		if (invoice == null)
			throw new Exception("Could not find invoice");

		InvoiceForm form = new InvoiceForm();
		form.setPricingType(invoice.getPricingType());
		form.setInvoiceDate(invoice.getInvoiceDate());
		form.setAddrid(invoice.getAddress().getAddrid());
		form.setSiteCostingNote(invoice.getSiteCostingNote());
		form.setTotalCost(invoice.getTotalCost().doubleValue());
		if (invoice.getVatRateEntity() != null)
			form.setVatCode(invoice.getVatRateEntity().getVatCode());
		form.setBusinessContactId(invoice.getBusinessContact().getId());
		form.setPaymentTerm(invoice.getPaymentTerm());
		return form;
	}

	@ModelAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME)
	public Object defineInvoiceClass() {
		return Invoice.class;
	}

	@ModelAttribute(Constants.REFDATA_SYSTEM_COMPONENT)
	private Object initializeSystemComponent() {
		return this.scService.findComponent(Component.INVOICE);
	}

	@ModelAttribute("defaultCurrency")
	private Object initializeDefaultCurrency() {
		return this.currencyServ.getDefaultCurrency();
	}

	@ModelAttribute("privateOnlyNotes")
	private Boolean initializePrivateOnly() {
		return NoteType.INVOICENOTE.isPrivateOnly();
	}

	@ModelAttribute("invoicetypes")
	protected List<InvoiceType> populateInvoiceTypes() {
		return this.invTypeServ.getAll();
	}

	@ModelAttribute(Constants.FAST_TRACK_TURN)
	private Object initializeFastTrackTurn() {
		return this.fastTrackTurn;
	}

	@ModelAttribute("accountssoftware")
	public String populateAccountsSoftware() {
		return this.accountsSoftware;
	}

	@ModelAttribute("accountssoftwaresupported")
	public Boolean populateAccountSoftwareSupported() {
		return this.accountsSoftwareSupported;
	}

	@ModelAttribute("periods")
	protected List<FinancialPeriod> populatePeriods() {
		return AccountsTools.getFinancialPeriodRange();
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		binder.setValidator(validator);
		val editorInvoiceDate = new CustomDateEditor(new SimpleDateFormat(StdDateFormat.DATE_FORMAT_STR_ISO8601), true);
		binder.registerCustomEditor(Date.class, Invoice_.invoiceDate.getName(), editorInvoiceDate);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.GET, params = "submission=submit")
	protected String updateStatus(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			RedirectAttributes redirectAttributes, @RequestParam(value = "id") int id) throws Exception {
		Invoice invoice = this.invoiceServ.findInvoice(id);
		if (invoice == null)
			throw new Exception("Could not find invoice");

		// Under Discussion
        /*
         * if(invoice.getType().getName().equals(InvoiceTypeNames.CALIBRATION)
         * || invoice.getType().getName().equals(InvoiceTypeNames.PERIODIC)){
         * List<Integer> invoiceItems = invoice.getItems().stream().filter(item
         * -> item.getJobItem() == null && item.getExpenseItem() == null).
         * map(InvoiceItem :: getId).collect(Collectors.toList());
         * if(!invoiceItems.isEmpty()){
         * redirectAttributes.addFlashAttribute("itemsNotLinked", invoiceItems);
         * return "redirect:/viewinvoice.htm?id=" + invoice.getId(); } }
         */

        Contact contact = this.userService.get(username).getCon();
        InvoiceStatus currentStatus = invoice.getStatus();
        InvoiceStatus nextActivity = currentStatus.getNext();

        InvoiceStatus nextStatus = nextActivity.getNext();

        if (avalaraAware && nextActivity.isOnIssue()) {
            boolean verified = avalaraConnector.commitTransaction(invoice);
            if (!verified)
                throw new Exception("Verifying transaction on Avatax fails");
        }
        // set new status and save
        invoice.setStatus(nextStatus);

		if (nextActivity.isOnIssue()) {
            for (InvoiceJobLink jobLink : invoice.getJobLinks()) {
                Job job = jobLink.getJob();
				if ((jobLink.getJob() != null) 
						&& !jobLink.getJob().getJs().isComplete() 
						&& jobLink.getJob().getItems().isEmpty()
                    && jobLink.getJob().getExpenseItems().stream()
                    .allMatch(e -> !e.getInvoiceItems().isEmpty() || e.getNotInvoiced() != null)) {
                    job.setJs(jobStatusService.findCompleteStatus());
                }
            }

            // service
            invoice.getInternalPOItems().stream().filter(poi -> !poi.isAccountsApproved()).forEach(poi -> {
                if (!poi.isAccountsApproved()) {
                    PurchaseOrderItemProgressAction action = new PurchaseOrderItemProgressAction();
                    action.setAccountsApproved(true);
                    action.setCancelled(false);
                    action.setContact(contact);
                    action.setDate(new Date());
                    action.setDescription("Referenced internal invoice approved.");
                    action.setGoodsApproved(poi.isGoodsApproved());
                    action.setItem(poi);
					action.setReceiptStatus(poi.getReceiptStatus());
					poi.getProgressActions().add(action);
					poi.setAccountsApproved(true);
					poItemService.updatePurchaseOrderItem(poi, contact);
				}
			});
			invoice.setIssued(true);
			invoice.setIssueby(contact);
			LocalDate issueDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()); 
			invoice.setIssuedate(issueDate);
			PaymentTerm pt = invoice.getPaymentTerm();
			invoice.setDuedate(pt.computePaymentDate(issueDate));
			// if the invoice type should be brokered
			if (invoice.getType().isAddToAccount()) {
				// release the invoice for brokering into third party
				// accounts software
				invoice.setAccountsStatus(AccountStatus.P);
			} else {
				invoice.setAccountsStatus(AccountStatus.S);
			}
		}
		// insert a new action record
		// add a new Invoice activity for creating the purchase order
		InvoiceAction action = new InvoiceAction();
		action.setContact(contact);
		action.setDate(new Date());
		action.setOutcomeStatus(nextStatus);
		action.setInvoice(invoice);
		action.setActivity(nextActivity);

		if (invoice.getActions() == null) {
			invoice.setActions(new HashSet<>());
		}
		invoice.getActions().add(action);

		this.invoiceServ.merge(invoice);

		return "redirect:/viewinvoice.htm?id=" + invoice.getId();

	}

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale, @RequestParam(value = "id") int id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Invoice invoice = this.invoiceServ.findInvoice(id);
		if (invoice == null)
			throw new Exception("Could not find invoice");
		Map<String, Object> modelMap = model.asMap();
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.INVOICE,
				invoice.getId(), newFiles);

		model.addAttribute("invoice", invoice);

		model.addAttribute("invoicepos", invoice
				.getPos()
				.stream()
				.map(po -> InvoicePoDto.of(po.getPoId(), po.getPoNumber()))
				.collect(Collectors.toSet()));

		model.addAttribute("businessContacts",
				this.contactService.getAllCompanyContacts(invoice.getOrganisation().getCoid()));

		CompanySettingsForAllocatedCompany companySettings = this.companySettingsService.getByCompany(invoice.getComp(),
				invoice.getOrganisation());
		model.addAttribute("companySettings", companySettings);

		model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.addAttribute(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));

		model.addAttribute("invoicedJobPONums", this.poServ.getPONumbersFromInvoicedJobs(invoice));

		model.addAttribute("invoiceAddresses", this.addressServ
				.getAllSubdivAddresses(invoice.getAddress().getSub().getSubdivid(), AddressType.INVOICE, null));

		// get all the current nominal costs for this invoice
		List<NominalInvoiceCostDTO> nominals = this.invoiceServ.getNominalInvoiceSummary(invoice.getId());
		model.addAttribute("nominals", nominals);
		model.addAttribute("itemsnotlinked", modelMap.get("itemsNotLinked"));

		// get the total value of the assigned nominals (for comparison with
		// the total value of the invoice)
		model.addAttribute("nominaltotalvalue", this.invoiceServ.getTotalNominalCost(nominals));
		invoice.setSentEmails(this.emailServ.getAllComponentEmails(Component.INVOICE, id));
		// prepare formatted description of vatrate using actual value from
		// invoice
		model.addAttribute("formattedVatRate",
				this.vatRateFormatter.formatVatRate(invoice.getVatRateEntity(), locale, invoice.getVatRate()));
		List<VatRate> vatRates = this.vatRateService.getAllEagerWithCountry();
		model.addAttribute("vatRates", this.vatRateFormatter.getOptionGroupMap(vatRates, locale));

		int clientCompanyId = invoice.getComp().getCoid();
		int businessCompanyId = invoice.getOrganisation().getCoid();
		// get all invoice items that have pos list not empty
		List<InvoiceItem> invoiceItemsWithPos = this.poServ.getInvoiceItemsWithPos(invoice.getId());
		model.addAttribute("printDiscount",
				this.printDiscount.getValueByScope(Scope.COMPANY, clientCompanyId, businessCompanyId));
		model.addAttribute("printUnitaryPrice",
				this.printUnitaryPrice.getValueByScope(Scope.COMPANY, clientCompanyId, businessCompanyId));
        model.addAttribute("printQuantity",
            this.printQuantity.getValueByScope(Scope.COMPANY, clientCompanyId, businessCompanyId));
        model.addAttribute("showAppendix",
            this.showAppendix.getValueByScope(Scope.COMPANY, clientCompanyId, businessCompanyId));
        model.addAttribute("detailedInvoiceType",
            this.detailedInvoiceType.getValueByScope(Scope.COMPANY, clientCompanyId, businessCompanyId));
        model.addAttribute("isCalibration", invoice.getType().getName().equals(InvoiceTypeNames.CALIBRATION));
        model.addAttribute("languageTags", supportedLocaleService.getSupportedDTOList(locale));
        model.addAttribute("defaultLanguageTag", invoice.getComp().getDocumentLanguage().toLanguageTag());
        // Features in development / testing stage - configured via properties
        model.addAttribute("showOldInvoicePOFeature", showOldInvoicePOFeature);  
        model.addAttribute("showDocumentOptions", showDocumentOptions);
        // set jobitems id
        List<Integer> jobItemIds = this.invoiceServ.getJobItemsIdsFromInvoice(invoice);
        List<Integer> jobIds = this.invoiceServ.getJobsIdsFromInvoice(invoice);
        InstructionType[] instructionType = new InstructionType[]{InstructionType.DISCOUNT, InstructionType.INVOICE};
		Integer instructionsCount = 0;
		if(jobIds.isEmpty() && jobItemIds.isEmpty()){
			// the case when we create the customer invoice and it hasn't link to any job or jobitem
			List<Integer> linkCoid = new ArrayList<Integer>();
			List<Integer> linkSubdiv = new ArrayList<Integer>();
			linkCoid.add(invoice.getComp().getCoid());
			linkSubdiv.add(invoice.getAddress().getSub().getSubdivid());
			instructionsCount = this.instructionService.ajaxInstructions(subdivDto.getKey(), linkCoid, linkSubdiv, null, null, null, null, null,  
					instructionType, null).size();
			model.addAttribute("linkCoid", StringUtils.join(linkCoid, ","));
			model.addAttribute("linkSubdiv", StringUtils.join(linkSubdiv, ","));
		} else {
            model.addAttribute("jobitemids", jobItemIds);
            model.addAttribute("jobids", StringUtils.join(jobIds, ","));
            instructionsCount = this.instructionService.ajaxInstructions(subdivDto.getKey(), null, null, null, null, null, jobIds, jobItemIds,
                    instructionType, null).size();
        }
        model.addAttribute("instructionsCount", instructionsCount);
		model.addAttribute("invoiceItemsWithPos", invoiceItemsWithPos);
		
        return VIEW_NAME;
    }

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale, @RequestParam(value = "id") int id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@Validated @ModelAttribute(FORM_NAME) InvoiceForm invoiceForm,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model, locale, id, newFiles, subdivDto);
		}
		Invoice invoice = this.invoiceServ.findInvoice(id);
		if (invoice == null)
			throw new Exception("Could not find invoice");
		// set address
		if (!invoiceForm.getAddrid().equals(invoice.getAddress().getAddrid())) {
			Address addr = this.addressServ.get(invoiceForm.getAddrid());
			Company newCompany = addr.getSub().getComp();
			Company allocatedCompany = this.subdivService.get(subdivDto.getKey()).getComp();
			CompanySettingsForAllocatedCompany settings = this.settingsService.getByCompany(newCompany,
					allocatedCompany);
			invoice.setAddress(addr);
			invoice.setComp(newCompany);
			// set payment mode
			invoice.setPaymentMode(settings.getPaymentMode());
			// set payment term
			invoice.setPaymentTerm(settings.getPaymentterm());
			if (invoice.getPaymentTerm() == null && invoiceForm.getInvoiceDate() != null) {
				invoice.setPaymentTerm(invoiceForm.getPaymentTerm());
			}
		} else {
			PaymentTerm paymentTerm = (invoiceForm.getPaymentTerm() == null) ? null : invoiceForm.getPaymentTerm();
			invoice.setPaymentTerm(paymentTerm);
		}
		// set business contact
		if (!invoiceForm.getBusinessContactId().equals(invoice.getBusinessContact().getId())) {
			Contact contact = this.contactService.get(invoiceForm.getBusinessContactId());
			invoice.setBusinessContact(contact);
		}
		// set type
		if (invoiceForm.getTypeid() != invoice.getType().getId()) {
			InvoiceType newType = this.invTypeServ.get(invoiceForm.getTypeid());
			// If changing type impacts synchronization status (used for issued
			// invoices, pending synchronization),
			// update the synchronization status accordingly.
			if (invoice.isIssued() && newType.isAddToAccount() && !invoice.getType().isAddToAccount()) {
				invoice.setAccountsStatus(AccountStatus.P);
			} else if (invoice.isIssued() && !newType.isAddToAccount() && invoice.getType().isAddToAccount()) {
				invoice.setAccountsStatus(AccountStatus.S);
			}
			invoice.setType(newType);
		}
		invoice.setInvoiceDate(invoiceForm.getInvoiceDate());
		// always force the period to match the invoice date
		invoice.setFinancialPeriod(AccountsTools.getFinancialPeriod(invoiceForm.getInvoiceDate()).getPeriod());
		// update the VAT rate; cost calculator will recalculate amount
		if (invoiceForm.getVatCode() != null && (invoice.getVatRateEntity() == null
				|| !invoiceForm.getVatCode().equals(invoice.getVatRateEntity().getVatCode()))) {
            VatRate vatRate = this.vatRateService.get(invoiceForm.getVatCode());
            invoice.setVatRateEntity(vatRate);
            invoice.setVatRate(vatRate.getRate());
            taxCalculator.calculateTax(invoice);
        }
		// site invoices have a manually set total cost, all other invoices
		// have their total costs calculated from the sum of their item
		// totals
		if (invoice.getPricingType().equals(PricingType.SITE)) {
			invoice.setTotalCost(BigDecimal.valueOf(invoiceForm.getTotalCost()));
			invoice.setSiteCostingNote(invoiceForm.getSiteCostingNote());
			CostCalculator.updatePricingWithCalculatedTotalCost(invoice);
		} else {
			CostCalculator.updatePricingFinalCost(invoice, invoice.getItems());
		}
		this.invoiceServ.merge(invoice);
		model.asMap().clear();
		return "redirect:viewinvoice.htm?id=" + invoice.getId();
	}
}