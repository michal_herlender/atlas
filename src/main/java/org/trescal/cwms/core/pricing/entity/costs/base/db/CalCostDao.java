package org.trescal.cwms.core.pricing.entity.costs.base.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.entity.costs.base.CalCost;

public interface CalCostDao extends BaseDao<CalCost, Integer> {}