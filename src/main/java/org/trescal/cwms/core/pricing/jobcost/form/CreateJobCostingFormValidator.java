package org.trescal.cwms.core.pricing.jobcost.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CreateJobCostingFormValidator extends AbstractBeanValidator {

	public static final String NOT_SELECTED_CODE = "error.value.notselected";
	public static final String NOT_SELECTED__MESSAGE = "A value must be selected";
	public static final String SINGLE_ITEM_CODE = "jobcost.errorsinglejobitem";
	public static final String SINGLE_ITEM_MESSAGE = "Only one job item may be selected for this job pricing type.";

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(CreateJobCostingForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			CreateJobCostingForm form = (CreateJobCostingForm) target;
			if (form.getJobItemIds().isEmpty() && form.getExpenseItemIds().isEmpty())
				// At least one job item or job service should be selected
				errors.rejectValue("jobItemIds", NOT_SELECTED_CODE, NOT_SELECTED__MESSAGE);
			else if (form.getClientCosting().equals(PricingType.SINGLE_JOB_ITEM) && form.getJobItemIds().size() > 1)
				// Only one job item may be selected
				errors.rejectValue("jobItemIds", SINGLE_ITEM_CODE, SINGLE_ITEM_MESSAGE);
		}
	}
}