package org.trescal.cwms.core.pricing;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemCostDto;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;

/**
 * Object used to calculate the total number of discounts and their accumulated
 * values as applied to one or more {@link PricingItem}(s).
 * GB 2017-02-16 - No longer used for quotations, poor performance for 1000s of quotation items
 * May restrict to just JobCosting and TPQuotation usage...
 * 
 * @author Richard
 */
public class TotalDiscountCalculator
{
	private int discounts;
	private BigDecimal discountValue;

	/**
	 * Instantiates a new {@link TotalDiscountCalculator} and calculates the
	 * total number of discounts and their accumulated values for the given
	 * {@link Set} of {@link PricingItem}(s) and any modular children that might
	 * belong to them.
	 * 
	 * @param pitems the {@link PricingItem}(s) to calculate the discounts for.
	 */
	public TotalDiscountCalculator(Set<? extends PricingItem> pitems)
	{
		this.discounts = 0;
		this.discountValue = new BigDecimal(0.00).setScale(2, 4);

		if (pitems != null)
		{
			for (PricingItem pitem : pitems)
			{
				if (!pitem.isPartOfBaseUnit())
				{
					if (pitem.getGeneralDiscountValue().doubleValue() > 0)
					{
						this.discounts = this.discounts + 1;
						this.discountValue = this.discountValue.add(pitem.getGeneralDiscountValue());
					}

					// loop over the Costs discount values
					for (Cost cost : pitem.getCosts())
					{
						if (cost != null)
						{
							// only include active costs
							if (cost.isActive())
							{
								if (cost.getDiscountValue().doubleValue() > 0)
								{
									this.discounts = this.discounts + 1;
									this.discountValue = this.discountValue.add(cost.getDiscountValue().multiply(new BigDecimal(pitem.getQuantity())));
								}
							}
						}
					}

					if (pitem.getModules() != null)
					{
						for (PricingItem pi : pitem.getModules())
						{
							if (pi.getGeneralDiscountValue().doubleValue() > 0)
							{
								this.discounts = this.discounts + 1;
								this.discountValue = this.discountValue.add(pi.getGeneralDiscountValue());
							}

							// loop over the Costs discount values
							for (Cost cost : pi.getCosts())
							{
								if (cost.isActive())
								{
									if (cost.getDiscountValue().doubleValue() > 0)
									{
										this.discounts = this.discounts + 1;
										this.discountValue = this.discountValue.add(cost.getDiscountValue().multiply(new BigDecimal(pi.getQuantity())));
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public TotalDiscountCalculator(List<JobCostingItemDto> jcitems)
	{
		this.discounts = 0;
		this.discountValue = new BigDecimal(0.00).setScale(2, 4);

		if (jcitems != null)
		{
			for (JobCostingItemDto jcitem : jcitems)
			{
				if (!jcitem.getJobCostingItemPartOfBaseUnit())
				{
					if (jcitem.getGeneralDiscountValue().doubleValue() > 0)
					{
						this.discounts = this.discounts + 1;
						this.discountValue = this.discountValue.add(jcitem.getGeneralDiscountValue());
					}

					// loop over the Costs discount values
					for (JobCostingItemCostDto cost : jcitem.getCosts())
					{
						if (cost != null)
						{
							// only include active costs
							if (cost.getActive())
							{
								if (cost.getDiscountValue().doubleValue() > 0)
								{
									this.discounts = this.discounts + 1;
									this.discountValue = this.discountValue.add(cost.getDiscountValue().multiply(new BigDecimal(jcitem.getJobCostingItemQuantity())));
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Get the discounts for the {@link TotalDiscountCalculator}.
	 * 
	 * @return discounts.
	 */
	public int getDiscounts()
	{
		return this.discounts;
	}

	/**
	 * Get the cumulative value of the discounts for this
	 * {@link TotalDiscountCalculator}.
	 * 
	 * @return discount value.
	 */
	public BigDecimal getDiscountValue()
	{
		return this.discountValue;
	}
}
