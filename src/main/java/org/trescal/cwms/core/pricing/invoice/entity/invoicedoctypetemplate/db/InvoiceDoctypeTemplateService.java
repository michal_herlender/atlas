package org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.db;

import java.util.List;

import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate.InvoiceDoctypeTemplate;

public interface InvoiceDoctypeTemplateService
{
	void deleteInvoiceTemplate(InvoiceDoctypeTemplate invoicetemplate);

	InvoiceDoctypeTemplate findByCompanyType(int coid, int typeid, String doctype);

	InvoiceDoctypeTemplate findByInvoice(Invoice invoice, String doctype);

	InvoiceDoctypeTemplate findByType(int typeid, String doctype);

	InvoiceDoctypeTemplate findInvoiceTemplate(int id);

	List<InvoiceDoctypeTemplate> getAllInvoiceTemplates();

	void insertInvoiceTemplate(InvoiceDoctypeTemplate invoicetemplate);

	void saveOrUpdateInvoiceTemplate(InvoiceDoctypeTemplate invoicetemplate);

	void updateInvoiceTemplate(InvoiceDoctypeTemplate invoicetemplate);
}