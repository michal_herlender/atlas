package org.trescal.cwms.core.pricing.jobcost.entity.costs;

import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.ThirdPartyCostSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface JobCostingCostService<Entity extends Cost>
{
	/**
	 * Takes a {@link Cost} implementing {@link ThirdPartyCostSupport} and sets
	 * up a basic selection of Third party costs (i.e. sets all cost sources to
	 * manual and zeros all associated costs. Intended for use when a
	 * {@link JobCostingItem} is first created and basic costs need to be set.
	 * 
	 * @param costSupport the {@link ThirdPartyCostSupport} entity to set up.
	 * @return {@link ThirdPartyCostSupport}.
	 */
	public ThirdPartyCostSupport configureTPCosts(ThirdPartyCostSupport costSupport);

	public Cost createCostFromCopy(JobCostingItem ji, JobCostingItem old);

	/**
	 * Find all {@link JobCosting} costs that match the given {@link Company} id
	 * (or all if null), the given {@link InstrumentModel} id and the given
	 * {@link CalibrationType} (or all if null) ordered by most recent first.
	 * 
	 * @param plantid an {@link Instrument} barcode, if null then no instrument
	 *        filter applied.
	 * @param coid the {@link Company} id.
	 * @param modelid the {@link InstrumentModel} id.
	 * @param caltypeid the {@link CalibrationType} id.
	 * @param page the current page number, if null or 0 then paging is
	 *        disabled.
	 * @param resPerPage number of results to show per page.
	 * @return list of matching {@link Cost} entities.
	 */
	public List<Entity> findMatchingCosts(Integer plantid, Integer coid, int modelid, Integer caltypeid, Integer page, Integer resPerPage);

	public Cost resolveCostLookup(CostSource source, JobCostingItem ji);

	/**
	 * Recalculates the total cost, discount and final cost for the given
	 * {@link Cost}. Different cost types have different rules on how a cost
	 * total is calculatated, therefore, it is up to each specific cost type's
	 * service to provide an implementation for this function that is specific
	 * to how costs are aggregated for this cost type. Does not persist the
	 * resulting object to the database.
	 * 
	 * @param cost the {@link Cost} to update.
	 * @return the {@link Cost}.
	 */
	public Entity updateCosts(Entity cost);
	
	/**
	 * Defines the lookup precedence hierarchy for looking up calibration costs
	 * for {@link JobItem}.  Setter defined for unit testing purposes. 
	 */
	public String getCostSourceHierarchy();
	
	public void setCostSourceHierarchy(String value);
	
	/**
	 * If true system will attempt to lookup sales costs when adding a jobitem,
	 * if false then model defaults are used.
	 */
	public boolean getLookupCosts();
}
