package org.trescal.cwms.core.pricing.purchaseorder.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemProgressDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderLimitDto;
import org.trescal.cwms.core.pricing.purchaseorder.entity.enums.PurchaseOrderTaxableOption;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemComparator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db.PurchaseOrderStatusService;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderForm;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderFormAccountsValidator;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderFormGoodsValidator;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderFormValidator;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.db.SupplierInvoiceService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.userright.dto.PoLimitConfig;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

/**
 * Controller used to view and edit {@link PurchaseOrder} entities.
 */
@Controller
@IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST, Constants.HTTPSESS_PRICING_CLASS_NAME,
	Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class ViewPurchaseOrderController {
	private static final Logger logger = LoggerFactory.getLogger(ViewPurchaseOrderController.class);

	@Value("#{props['cwms.config.thirdpartyaccounts.name']}")
	private String accountsSoftware;
	@Value("#{props['cwms.config.thirdpartyaccounts.supported']}")
	private Boolean accountsSoftwareSupported;
	@Autowired
	private AddressService addressServ;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private UserService userService;
	@Autowired
	private JobService jobServ;
	@Autowired
	private PurchaseOrderService poService;
	@Autowired
	private PurchaseOrderStatusService poStatusServ;
	@Autowired
	private PurchaseOrderItemService poItemService;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private SupplierInvoiceService supplierInvoiceService;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private PurchaseOrderFormValidator validatorEdit;
	@Autowired
	private PurchaseOrderFormGoodsValidator validatorGoodsReceipt;
	@Autowired
	private PurchaseOrderFormAccountsValidator validatorAccountsReceipt;
	@Autowired
	private LimitCompanyService permissionLimitServ;
	@Autowired
	private InstructionService instructionService;

	public static final String FORM_NAME = "command";
	public static final String VIEW_NAME = "trescal/core/pricing/purchaseorder/viewpurchaseorder";
	public static final String REDIRECT_URL = "/viewpurchaseorder.htm?id=";
	public static final String TAB_GOODS_APPROVAL = "goods-tab";
	public static final String TAB_ACCOUNTS_APPROVAL = "accounts-tab";
	public static final String TAB_EDIT = "edit-tab";

	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}

	@ModelAttribute("limitPo")
	protected PurchaseOrderLimitDto limitPurchaseOrder(@RequestParam(value = "id", required = true) int id,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> compDto) {
		Company allocatedComp = companyService.get(compDto.getKey());
		PurchaseOrder order = this.poService.find(id);
		PoLimitConfig poLimitConfig = permissionLimitServ.getPoUserConfigLimit(
				SecurityContextHolder.getContext().getAuthentication().getAuthorities(), allocatedComp);
		// Its a warning, we give there the sub name where the user can update the status
		return new PurchaseOrderLimitDto(poLimitConfig,
				order.getBusinessContact().getSub().getComp().getCurrency().getCurrencySymbol());
	}

	@ModelAttribute(FORM_NAME)
	protected PurchaseOrderForm formBackingObject(@RequestParam(value = "id", required = true) int id)
			throws Exception {

		PurchaseOrder order = this.poService.find(id);
		if (order == null) {
			logger.error("Purchase Order with id {} could not be found",id);
			throw new Exception("Could not find purchase order");
		} else {
			PurchaseOrderForm form = PurchaseOrderForm.builder()
					.orgid(order.getOrganisation().getId())
					.poid(order.getId())
					.clientref(order.getClientref())
					.jobno(order.getJobno())
					.coid(order.getContact().getSub().getComp().getCoid())
					.subdivid(order.getContact().getSub().getSubdivid())
					.personid(order.getContact().getPersonid())
					.businessPersonid(order.getBusinessContact().getPersonid())
					.addrid(order.getAddress().getAddrid())
					.returnAddressId(order.getReturnToAddress() == null ? null : order.getReturnToAddress().getAddrid())
					.currencyCode(order.getCurrency().getCurrencyCode())
					.taxableOption(order.getTaxable())
					.accountsApproval(new ArrayList<>())
					.goodsApproval(new ArrayList<>())
					.goodsCancelled(new ArrayList<>())
					.goodsComment(new ArrayList<>())
					.goodsItemStatus(new ArrayList<>())
					.supplierInvoiceIds(new ArrayList<>())
					.build();
			for (PurchaseOrderItem item : order.getItems()) {
				form.getAccountsApproval().add(item.isAccountsApproved());
				form.getGoodsApproval().add(item.isGoodsApproved());
				form.getGoodsCancelled().add(item.isCancelled());
				form.getGoodsComment().add("");
				form.getGoodsItemStatus().add(item.getReceiptStatus());
				if (item.getSupplierInvoice() == null)
					form.getSupplierInvoiceIds().add(0);
				else
					form.getSupplierInvoiceIds().add(item.getSupplierInvoice().getId());
			}
			return form;
		}
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	/*
	 * Function allows us to have a link to point directly to this Controller and
	 * force a POST action. In this case including '&submission=submit' as part of
	 * the href will trigger this, otherwise the super() implementation of the
	 * method is called. This implementation is used to allow use of this Controller
	 * to directly update the status and log a new purchaseorderaction.
	 */
	@GetMapping(value = "/viewpurchaseorder.htm", params = "submission=submit")
	public RedirectView performStatusUpdate(@RequestParam(value = "id", required = true) int id,
			@RequestParam(value = "isJump", required = false, defaultValue = "false") boolean isJump,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> compDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		PurchaseOrder order = this.poService.find(id);
		Contact currentContact = this.userService.get(username).getCon();
		Subdiv subdiv = this.subdivServ.get(subdivDto.getKey());
		Company allocatedComp = companyService.get(compDto.getKey());

		if (allocatedComp.getCoid() != order.getOrganisation().getCoid())
			throw new Exception("Has no right to change status in this company");

		// The good method to use but can't be used for now the right are not well set.
		// isUpdate = this.poService.updatePurchaseOrderStatusJump(order,
		// currentContact, subdiv);
		if (!this.poService.updatePurchaseOrderStatus(order, currentContact, subdiv)) {
			throw new Exception("Has no permission limit for purchase order or is under limit");
		}

		return new RedirectView(REDIRECT_URL + order.getId(), true);
	}

	@PostMapping(value = "/viewpurchaseorder.htm", params = "action=edit")
	public ModelAndView performEdit(@RequestParam(value = "id", required = true) int id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) PurchaseOrderForm form, BindingResult bindingResult) throws Exception {
		// Manually validated as validator depends on type of edit performed
		validatorEdit.validate(form, bindingResult);
		if (bindingResult.hasErrors()) {
			return referenceData(id, TAB_EDIT, newFiles, companyDto, subdivDto, username);
		}
		PurchaseOrder order = this.poService.find(id);
		Contact currentContact = this.userService.get(username).getCon();
		Job job = jobServ.getJobByExactJobNo(form.getJobno());
		if (job != null)
			order.setJob(job);
		order.setJobno(form.getJobno().trim());
		order.setClientref(form.getClientref().trim());
		// set address
		if (form.getAddrid() != order.getAddress().getAddrid()) {
			order.setAddress(this.addressServ.get(form.getAddrid()));
		}
		if ((order.getReturnToAddress() == null)
				|| (form.getReturnAddressId() != order.getReturnToAddress().getAddrid())) {
			order.setReturnToAddress(this.addressServ.get(form.getReturnAddressId()));
		}
		// set contact
		if (form.getPersonid() != order.getContact().getPersonid()) {
			order.setContact(this.contactServ.get(form.getPersonid()));
		}
		// set currency
		this.currencyServ.setCurrencyFromForm(order, form.getCurrencyCode(), order.getContact().getSub().getComp());
		order.setBusinessContact(
				form.getBusinessPersonid() == null ? null : this.contactServ.get(form.getBusinessPersonid()));
		// set taxable
		order.setTaxable(form.getTaxableOption());
		this.poService.updatePurchaseOrder(order, currentContact);
		return new ModelAndView(new RedirectView(REDIRECT_URL + order.getId(), true));
	}

	@PostMapping(value = "/viewpurchaseorder.htm", params = "action=goodsReceipt")
	public ModelAndView performGoodsReceipt(@RequestParam(value = "id", required = true) int id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) PurchaseOrderForm form, BindingResult bindingResult) {
		validatorGoodsReceipt.validate(form, bindingResult);
		if (bindingResult.hasErrors()) {
			return referenceData(id, TAB_GOODS_APPROVAL, newFiles, companyDto, subdivDto, username);
		}
		PurchaseOrder order = this.poService.find(id);
		Contact currentContact = this.userService.get(username).getCon();

		int index = 0;
		for (PurchaseOrderItem item : order.getItems()) {
			PurchaseOrderItemProgressDTO dto = new PurchaseOrderItemProgressDTO();
			dto.setAccountsApproved(item.isAccountsApproved());
			dto.setCancelled(form.getGoodsCancelled().get(index));
			dto.setGoodsApproved(form.getGoodsApproval().get(index));
			dto.setProgressDescription(form.getGoodsComment().get(index));

			dto.setReceiptStatus(form.getGoodsItemStatus().get(index));
			this.poItemService.updatePurchaseOrderItemProgress(dto, item, currentContact);
			this.poItemService.saveOrUpdatePurchaseOrderItem(item, currentContact);

			index++;
		}

		this.poService.updatePurchaseOrder(order, currentContact);
		return new ModelAndView(new RedirectView(REDIRECT_URL + order.getId(), true));
	}

	@PostMapping(value = "/viewpurchaseorder.htm", params = "action=accountsApproval")
	public ModelAndView performAccountsApproval(@RequestParam(value = "id", required = true) int id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) PurchaseOrderForm form, BindingResult bindingResult) {
		validatorAccountsReceipt.validate(form, bindingResult);
		if (bindingResult.hasErrors()) {
			return referenceData(id, TAB_ACCOUNTS_APPROVAL, newFiles, companyDto, subdivDto, username);
		}
		PurchaseOrder order = this.poService.find(id);
		Contact currentContact = this.userService.get(username).getCon();

		int index = 0;
		for (PurchaseOrderItem item : order.getItems()) {
			// TODO refactor DTO out of service method once finalized (split
			// accounts/goods) and delete related Javascript
			PurchaseOrderItemProgressDTO dto = new PurchaseOrderItemProgressDTO();
			dto.setAccountsApproved(form.getAccountsApproval().get(index));
			dto.setCancelled(item.isCancelled());
			dto.setGoodsApproved(item.isGoodsApproved());
			dto.setProgressDescription(form.getGoodsComment().get(index));
			dto.setReceiptStatus(item.getReceiptStatus());
			item.setSupplierInvoice(this.supplierInvoiceService.get(form.getSupplierInvoiceIds().get(index)));

			this.poItemService.updatePurchaseOrderItemProgress(dto, item, currentContact);
			this.poItemService.saveOrUpdatePurchaseOrderItem(item, currentContact);

			index++;
		}

		this.poService.updatePurchaseOrder(order, currentContact);
		return new ModelAndView(new RedirectView(REDIRECT_URL + order.getId(), true));
	}

	@ModelAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME)
	protected Class<?> getPricingClass() {
		return PurchaseOrder.class;
	}

	@GetMapping(value = "/viewpurchaseorder.htm")
	public ModelAndView referenceData(@RequestParam(value = "id") int id,
										 @RequestParam(value = "loadtab", required = false) String loadtab,
										 @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		PurchaseOrder order = this.poService.find(id);
		Contact currentContact = userService.get(username).getCon();
		Subdiv subdiv = subdivServ.get(subdivDto.getKey());
		Contact contact = order.getContact();
		Subdiv sub = contact.getSub();
		int coid = sub.getComp().getCoid();

		Company allocatedCompany = companyService.get(companyDto.getKey());
		// the list of itemNo of job items do not have the supplier company on an approved list
		List<Integer> itemNos = this.poService.getListOfItemNo(order, allocatedCompany);
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.PURCHASEORDER, order.getId(), newFiles);
		Map<String, Object> refData = new HashMap<>();
		refData.put("order", order);
		refData.put("itemNos", itemNos);
		refData.put("currencyList", this.currencyServ.getAllSupportedCurrencys());
		refData.put(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.PURCHASEORDER));
		refData.put(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		refData.put(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
		refData.put("accountssoftware", this.accountsSoftware);
		refData.put("accountssoftwaresupported", this.accountsSoftwareSupported);

		PurchaseOrderStatus status = order.getStatus();
		Permission perm = poStatusServ.getPermissionNeeded(status);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		refData.put("isAllowToPerformStatusChange", perm != null && authentication.getAuthorities().contains(perm));
		refData.put("isUnderLimit",
				permissionLimitServ.getPosUnderLimit(order, permissionLimitServ.getPoUserConfigLimit(authentication.getAuthorities(), allocatedCompany),
				allocatedCompany));
		refData.put("isWellAllocated", subdiv.getComp().getCoid() == order.getOrganisation().getCoid());

		// add a list of business company addresses to set as the return to
		// Note, bug from 2016 to 2019 (DEV-1265) resulted in incorrect return to "supplier" addresses, this lets us correct
		String addressText = this.addressServ.getDisplayLine(order.getReturnToAddress());
		KeyValueIntegerString existingAddressDto = new KeyValueIntegerString(order.getReturnToAddress().getAddrid(), addressText);

		refData.put("businessaddresslist", this.addressServ.getAddressDtoList(order.getOrganisation().getCoid(), null, AddressType.POFROM, existingAddressDto));
		refData.put("businesscontacts", this.contactServ.getAllCompanyContacts(companyDto.getKey()));
		refData.put("companySettings", this.companySettingsService.getByCompany(sub.getComp(), allocatedCompany));
		refData.put("defaultCurrency", this.currencyServ.getDefaultCurrency());
		refData.put("deletable", !status.isIssued());
		refData.put("editable", !status.isApproved());
		refData.put("privateOnlyNotes", NoteType.PURCHASEORDERNOTE.isPrivateOnly());
		// add a list of currency options for this company

		refData.put("currencyopts", this.currencyServ.getCompanyCurrencyOptions(coid));
		// check if this user is a member of accounts or an admin

		boolean accountsAuthenticated = this.contactServ.contactBelongsToDepartmentOrIsAdmin(currentContact, DepartmentType.ACCOUNTS, allocatedCompany);
		refData.put("accountsAuthenticated", accountsAuthenticated);

		// build a list of items missing nominals
		Set<PurchaseOrderItem> missingNominals = new TreeSet<>(new PurchaseOrderItemComparator());
		for (PurchaseOrderItem item : order.getItems()) {
			if (item.getNominal() == null) {
				missingNominals.add(item);
			}
		}
		refData.put("missingNominals", missingNominals);
		refData.put("receiptStatuses", PurchaseOrderItemReceiptStatus.values());
		Locale locale = LocaleContextHolder.getLocale();
		refData.put("supplierInvoices", this.supplierInvoiceService.getDTOList(order, locale, true));
		refData.put("useTaxableOption", allocatedCompany.getBusinessSettings() != null && allocatedCompany.getBusinessSettings().getUseTaxableOption());
		final Map<String, String> taxableOptions = Arrays.stream(PurchaseOrderTaxableOption.values()).collect(Collectors.toMap(PurchaseOrderTaxableOption::name, PurchaseOrderTaxableOption::getMessage));
		refData.put("taxableOptions", taxableOptions.entrySet());
		if (loadtab != null) {
			refData.put("loadtab", loadtab);
		}
		// set purchase order jobitems ids for instructions
		List<Integer> jobItemsIdsFromPurchaseOrder = this.poService.getJobItemsIdsFromPurchaseOrder(order);
		refData.put("jobitemIds", jobItemsIdsFromPurchaseOrder);

		InstructionType[] instructionType = new InstructionType[]{InstructionType.CALIBRATION, InstructionType.REPAIRANDADJUSTMENT, InstructionType.PURCHASE};

		Integer jobId = nonNull(order.getJob()) ? order.getJob().getJobid() : null;
		int instructionSize = instructionService.ajaxInstructions(subdivDto.getKey(), singletonList(coid), singletonList(sub.getSubdivid()), singletonList(contact.getPersonid()), null, jobId, null, jobItemsIdsFromPurchaseOrder, instructionType, null).size();
		refData.put("instructionSize", instructionSize);
		return new ModelAndView(VIEW_NAME, refData);
	}

	@GetMapping(value = "/copyPO.json", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	ResultWrapper copyPO(@RequestParam(value = "id") Integer id,
						 @RequestParam(value = "cancel") Boolean cancel,
						 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
						 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Locale locale) {

		ResultWrapper rw = new ResultWrapper();

		/* copy */
		Contact currentContact = this.userService.get(username).getCon();
		Integer newPoId = this.poService.copyPo(id, cancel, currentContact, subdivDto.getKey());

		rw.setSuccess(true);
		rw.setResults("viewpurchaseorder.htm?id=" + newPoId);

		return rw;
	}

}