package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.jobitem.dto.DetailedJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostLookUp;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.CostSourceSupportService;
import org.trescal.cwms.core.pricing.entity.costs.base.db.CalCostService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.enums.PartsMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.db.ThirdPartyPricingItemService;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.db.JobCostingAdjustmentCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db.JobCostingCalibrationCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.db.JobCostingInspectionCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.db.JobCostingPurchaseCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.db.JobCostingRepairCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark.JobCostingItemNote;
import org.trescal.cwms.core.pricing.jobcost.projection.JobCostingItemProjectionDTO;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.tools.MathTools;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.rest.adveso.dto.AdvesoJobCostingItemDTO;

@Service("JobCostingItemService")
public class JobCostingItemServiceImpl extends BaseServiceImpl<JobCostingItem, Integer>
		implements JobCostingItemService {
	@Autowired
	private CalCostService calCostServ;
	@Autowired
	private CalReqService calReqServ;
	@Autowired
	private JobCostingAdjustmentCostService jobCostAdjCostServ;
	@Autowired
	private JobCostingCalibrationCostService jobCostCalCostServ;
	@Autowired
	private JobCostingItemDao jobCostingItemDao;
	@Autowired
	private JobCostingInspectionCostService jobCostInspCostServ;
	@Autowired
	private JobCostingPurchaseCostService jobCostPurCostServ;
	@Autowired
	private JobCostingRepairCostService jobCostRepCostServ;
	@Autowired
	private JobCostingService jobCostServ;
	@Autowired
	private JobItemService jobItemServ;
	@Value("#{props['cwms.config.costs.calibration.lookup.jobcostingitem']}")
	private String lookupSources;
	@Autowired
	private MessageSource messages;
	@Value("#{props['cwms.config.costs.costings.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	@Autowired
	private ThirdPartyPricingItemService thirdPricingItemServ;
	@Autowired
	private UserService userService;
	@Autowired
	private RepairInspectionReportService rirService;

	@Override
	protected BaseDao<JobCostingItem, Integer> getBaseDao() {
		return jobCostingItemDao;
	}

	@Override
	public ResultWrapper ajaxDeleteJobCostingItem(int id, HttpSession session) {
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.getEagerLoad(username).getCon();

		JobCostingItem item = this.get(id);
		if (item == null) {
			Locale locale = LocaleContextHolder.getLocale();
			return new ResultWrapper(false, this.messages.getMessage("error.jobcostingitem.delete.noitem", null,
					"Could not find costing item", locale));
		} else {
			JobCosting costing = this.delete(item, contact);
			item = null;
			return new ResultWrapper(true, "", costing, null);
		}
	}

	@Override
	public List<JobCostingItem> createCostingItem(JobCosting costing, List<Integer> jobitemids) {
		List<JobCostingItem> costItems = new ArrayList<JobCostingItem>();
		int itemno = 1;
		Locale locale = LocaleContextHolder.getLocale();
		// load the list of jobitems
		if ((jobitemids != null) && (jobitemids.size() > 0)) {
			for (JobItem ji : this.jobItemServ.getAllItems(jobitemids)) {
				JobCostingItem item = new JobCostingItem();
				item.setItemno(itemno);
				item.setJobCosting(costing);
				item.setJobItem(ji);
				item.setQuantity(1);
				item.setTotalCost(new BigDecimal("0.00"));
				item.setFinalCost(new BigDecimal("0.00"));
				item.setGeneralDiscountRate(new BigDecimal("0.00"));
				item.setGeneralDiscountValue(new BigDecimal("0.00"));

				item.setTpCarriageIn(new BigDecimal("0.00"));
				item.setTpCarriageOut(new BigDecimal("0.00"));
				item.setTpCarriageMarkupRate(new BigDecimal("0.00"));
				item.setTpCarriageInMarkupValue(new BigDecimal("0.00"));
				item.setTpCarriageOutMarkupValue(new BigDecimal("0.00"));
				item.setTpCarriageMarkupSrc(ThirdCostMarkupSource.MANUAL);
				item.setTpCarriageTotal(new BigDecimal("0.00"));

				// store whether or not the item has been calibrated at this
				// point in time
				item.setCalibrated(this.jobItemServ.itemHasBeenCalibrated(ji));

				// add empty note set
				item.setNotes(new TreeSet<>(new NoteComparator()));

				// Discussing to add reference to failure report (or repair report?) related to
				// DEV-1654
				// FaultReport fr =
				// faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);

				CalReq cr = this.calReqServ.findCalReqsForJobItem(ji);
				if (cr != null) {

					StringBuffer note = new StringBuffer();
					if (cr.getPublicInstructions() != null && !cr.getPublicInstructions().isEmpty()) {
						note.append(cr.getPublicInstructions());
					}

					// set cal req specific info
					if (cr.getPointSet() != null) {
						if (note.length() > 0)
							note.append(", ");
						note.append(
								this.messages.getMessage("jobcostingitem.calibratedat", null, "Calibrated at", locale)
										+ " " + StringTools.convertCalibrationPointSetToString(cr.getPointSet()));
					} else if (cr.getRange() != null) {
						if (note.length() > 0)
							note.append(", ");
						note.append(
								this.messages.getMessage("jobcostingitem.calibratedbetween", null, "Calibrated between",
										locale) + " " + StringTools.convertCalibrationRangeToString(cr.getRange()));
					}
					// set defaults in case not set on cal reqs
					Contact setBy = (cr.getLastModifiedBy() != null) ? cr.getLastModifiedBy() : costing.getCreatedBy();
					Date lastMod = (cr.getLastModified() != null) ? cr.getLastModified() : new Date();

					String notetext = note.toString().trim();
					if (!notetext.isEmpty()) {
						JobCostingItemNote remark = new JobCostingItemNote();
						remark.setActive(true);
						remark.setJobCostingItem(item);
						remark.setLabel(
								this.messages.getMessage("calrequirement", null, "Calibration Requirement", locale));
						remark.setNote(notetext);
						remark.setPublish(true);
						remark.setSetBy(setBy);
						remark.setSetOn(lastMod);
						item.getNotes().add(remark);
					}

					if ((cr.getPrivateInstructions() != null) && !cr.getPrivateInstructions().trim().isEmpty()) {
						JobCostingItemNote pRemark = new JobCostingItemNote();
						pRemark.setActive(true);
						pRemark.setJobCostingItem(item);
						pRemark.setLabel(this.messages.getMessage("notes.privatenotes", null, "Private Notes", locale));
						pRemark.setNote(cr.getPrivateInstructions().trim());
						pRemark.setPublish(false);
						pRemark.setSetBy(setBy);
						pRemark.setSetOn(lastMod);
						item.getNotes().add(pRemark);
					}

				}

				// add the costs for the item
				this.setDefaultCosts(item);

				RepairInspectionReport rir = this.rirService.getLatestValidatedRir(ji.getJobItemId());
				if (rir != null) {
					BigDecimal cost = new BigDecimal(0);
					for (FreeRepairOperation repOp : rir.getFreeRepairOperations()) {
						for (FreeRepairComponent repComp : repOp.getFreeRepairComponents()) {
							cost = cost.add(repComp.getCost());
						}
					}
					JobCostingRepairCost repairCost = (JobCostingRepairCost) item.getRepairCost();
					cost.add(repairCost.getPartsCost());
					repairCost.setPartsCost(cost);
					// No markup, if it's not already defined before
					if(repairCost.getPartsMarkupSource() == null) repairCost.setPartsMarkupSource(PartsMarkupSource.MANUAL);
					if(repairCost.getPartsMarkupRate() == null) repairCost.setPartsMarkupRate(BigDecimal.valueOf(0,2));
					BigDecimal dec = repairCost.getPartsMarkupRate().divide(new BigDecimal("100.00"), MathTools.SCALE_FIN_CALC,
							MathTools.ROUND_MODE_FIN);
					repairCost.setPartsMarkupValue(
							(repairCost.getPartsCost().multiply(dec)).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
					repairCost.setPartsTotal(repairCost.getPartsCost().add(repairCost.getPartsMarkupValue()));
					repairCost.setHouseCost(repairCost.getLabourCostTotal().add(repairCost.getPartsTotal()));
					repairCost.setTotalCost(repairCost.getHouseCost().add(repairCost.getThirdCostTotal()));
					CostCalculator.updateSingleCost(repairCost, this.roundUpFinalCosts);
					item.setRepairCost(repairCost);
				}

				// update the item with the total costs
				CostCalculator.updateTPItemCostWithRunningTotal(item);

				costItems.add(item);
				itemno++;
			}
		}
		return costItems;
	}

	@Override
	public JobCosting delete(JobCostingItem jobcostingitem, Contact currentContact) {
		JobCosting costing = jobcostingitem.getJobCosting();

		// delete the costing item and remove it from the jobcosting
		// jobcotingitem collection
		costing.getItems().remove(jobcostingitem);
		this.jobCostingItemDao.remove(jobcostingitem);
		jobcostingitem = null;

		// call to update the jobcosting totals
		this.jobCostServ.recalculateAndPersistJobCostingTotals(costing, currentContact);

		return costing;
	}

	@Override
	public void delete(Collection<JobCostingItem> items) {
		this.jobCostingItemDao.delete(items);
	}

	@Override
	public JobCostingItem findMostRecentCostingForJobitem(int jobitemid) {
		return this.jobCostingItemDao.findMostRecentCostingForJobitem(jobitemid);
	}

	@Override
	public ResultWrapper getAjaxDetailedJobItemAndAlternativeCosts(int jobcostingitemid) {
		JobCostingItem jcItem = this.get(jobcostingitemid);
		if (jcItem != null) {
			DetailedJobItem dji = this.getDetailedJobItemAndAlternativeCosts(jcItem);
			return new ResultWrapper(true, "", dji, null);
		} else {
			return new ResultWrapper(false, this.messages.getMessage("error.jobcostingitem.delete.noitem", null,
					"could not find job costing item", null));
		}
	}

	@Override
	public DetailedJobItem getDetailedJobItemAndAlternativeCosts(JobCostingItem jcItem) {
		DetailedJobItem dji = this.jobItemServ.getDetailedJobItem(jcItem.getJobItem().getJobItemId());
		JobItem ji = dji.getJobitem();

		List<CostSource> costSources = CostSourceSupportService.resolveCostSourceList(this.lookupSources);
		dji.setCalCosts(this.calCostServ.lookupAlternativeCosts(new CostLookUp(), ji.getInst().getPlantid(),
				ji.getInst().getModel().getModelid(), ji.getCalType().getCalTypeId(),
				jcItem.getJobCosting().getContact().getSub().getComp().getCoid(),
				jcItem.getJobCosting().getJob().getJobid(), costSources,
				jcItem.getJobCosting().getJob().getOrganisation().getComp().getCoid()));

		return dji;
	}

	private void setDefaultCosts(JobCostingItem jci) {
		// find the most recent previous costing item for the current jobitem
		JobCostingItem mostRecent = this.findMostRecentCostingForJobitem(jci.getJobItem().getJobItemId());

		// set the calibration cost
		JobCostingCalibrationCost calCost = (JobCostingCalibrationCost) this.jobCostCalCostServ.loadCost(jci,
				mostRecent);
		CostCalculator.updateSingleCost(calCost, this.roundUpFinalCosts);
		jci.setCalibrationCost(calCost);

		JobCostingPurchaseCost purchaseCost = (JobCostingPurchaseCost) this.jobCostPurCostServ.loadCost(jci,
				mostRecent);
		CostCalculator.updateSingleCost(purchaseCost, this.roundUpFinalCosts);
		jci.setPurchaseCost(purchaseCost);

		JobCostingRepairCost repairCost = (JobCostingRepairCost) this.jobCostRepCostServ.loadCost(jci, mostRecent);
		CostCalculator.updateSingleCost(repairCost, this.roundUpFinalCosts);
		jci.setRepairCost(repairCost);

		JobCostingAdjustmentCost adjustmentCost = (JobCostingAdjustmentCost) this.jobCostAdjCostServ.loadCost(jci,
				mostRecent);
		CostCalculator.updateSingleCost(adjustmentCost, this.roundUpFinalCosts);
		jci.setAdjustmentCost(adjustmentCost);

		JobCostingInspectionCost inspectionCost = (JobCostingInspectionCost) this.jobCostInspCostServ.loadCost(jci,
				mostRecent);
		CostCalculator.updateSingleCost(inspectionCost, this.roundUpFinalCosts);
		jci.setInspectionCost(inspectionCost);
	}

	@Override
	public JobCostingItem updateJobCostingItemCosts(JobCostingItem item) {
		// update each cost using each cost types specific service
		item.setAdjustmentCost(
				(JobCostingAdjustmentCost) this.jobCostAdjCostServ.updateCosts(item.getAdjustmentCost()));
		item.setCalibrationCost(
				(JobCostingCalibrationCost) this.jobCostCalCostServ.updateCosts(item.getCalibrationCost()));
		item.setRepairCost((JobCostingRepairCost) this.jobCostRepCostServ.updateCosts(item.getRepairCost()));
		item.setPurchaseCost((JobCostingPurchaseCost) this.jobCostPurCostServ.updateCosts(item.getPurchaseCost()));

		/*
		 * Stuart commented out this line as inspection costs are now calculated via dwr
		 * when the cal cost is changed
		 */
		// also now update the inspection costs of the item
		// item.setInspectionCost(this.jobCostInspCostServ.calculateInspectionCost(item.getInspectionCost()));

		// delegate to the third party cost service to update the third party
		// carriage costs
		this.thirdPricingItemServ.updateThirdPartyCarriageCosts(item);

		// delegate to the CostCalculator to update the item totals
		CostCalculator.updateTPItemCostWithRunningTotal(item);

		return item;
	}

	@Override
	public List<AdvesoJobCostingItemDTO> findIssuedJobCostingItemByJobItemID(int jobitemid) {
		return jobCostingItemDao.findIssuedJobCostingItemByJobItemID(jobitemid);
	}

	@Override
	public List<JobCostingItemDto> getJobCostingItemsDtoByJobCostingId(Integer jobCostingId, Locale locale) {
		return jobCostingItemDao.getJobCotingItemsDtoByJobCostingId(jobCostingId, locale);
	}

	@Override
	public List<JobCostingItemProjectionDTO> getProjectionDTOsForJobItems(Collection<Integer> jobItemIds) {
		List<JobCostingItemProjectionDTO> result = Collections.emptyList();
		if (jobItemIds != null && !jobItemIds.isEmpty()) {
			result = this.jobCostingItemDao.getProjectionDTOsForJobItems(jobItemIds);
		}
		return result;
	}
}