package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.CreditNoteAction;

@Repository("CreditNoteActionDao")
public class CreditNoteActionDaoImpl extends BaseDaoImpl<CreditNoteAction, Integer> implements CreditNoteActionDao {
	
	@Override
	protected Class<CreditNoteAction> getEntity() {
		return CreditNoteAction.class;
	}
}