package org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceJobLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;

public interface InvoiceJobLinkDao extends BaseDao<InvoiceJobLink, Integer> {
	
	InvoiceJobLink findByJobNo(String jobno);

	List<InvoiceJobLink> findForInvoice(int invoiceid);
	
	List<InvoiceJobLink> getLinksForJobNo(String jobno);
	
	List<InvoiceJobLinkDTO> getLinksByIds(Collection<Integer> invoiceIds);
}