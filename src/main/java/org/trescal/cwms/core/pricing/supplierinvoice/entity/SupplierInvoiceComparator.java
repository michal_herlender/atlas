package org.trescal.cwms.core.pricing.supplierinvoice.entity;

import java.util.Comparator;

public class SupplierInvoiceComparator implements Comparator<SupplierInvoice> {

	@Override
	public int compare(SupplierInvoice invoice0, SupplierInvoice invoice1) {
		// Invoice number is mandatory field for supplier invoice
		int result = invoice0.getInvoiceNumber().compareTo(invoice1.getInvoiceNumber());
		if (result == 0) result = invoice0.getId() - invoice1.getId();
		return result;
	}

}
