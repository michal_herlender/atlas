package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "invoice_po_item", indexes = {
	@Index(columnList = "invoiceitemid", name = "IDX_invoicepoitem_invoiceitemid"),
	@Index(columnList = "invpoid", name = "IDX_invoicepoitem_invpoid")
	})
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoicePOItem {

    @NotNull
    @Id
    @GeneratedValue(generator = "invoice_po_item_id_sequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "invoice_po_item_id_sequence", sequenceName = "invoice_po_item_id_sequence")
    private int id;

    @ManyToOne()
    @JoinColumn(name = "invoiceitemid", foreignKey = @ForeignKey(name = "FK_invoicepoitem_invoiceitem"))
    private InvoiceItem invItem;

    @ManyToOne()
    @JoinColumn(name = "invpoid", foreignKey = @ForeignKey(name = "FK_invoicepoitem_invoicepo"))
    private InvoicePO invPO;
}
