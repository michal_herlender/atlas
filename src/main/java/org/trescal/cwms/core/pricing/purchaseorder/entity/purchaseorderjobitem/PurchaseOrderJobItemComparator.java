package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem;

import java.util.Comparator;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

/**
 * {@link PurchaseOrderJobItem} comparator that sorts in order of
 * {@link JobItem} id.
 * 
 * @author Richard
 */
public class PurchaseOrderJobItemComparator implements Comparator<PurchaseOrderJobItem>
{
	@Override
	public int compare(PurchaseOrderJobItem o1, PurchaseOrderJobItem o2)
	{
		int id1 = o1.getJi().getJobItemId();
		int id2 = o2.getJi().getJobItemId();

		if (id1 != id2)
		{
			return ((Integer) o1.getJi().getJobItemId()).compareTo(o2.getJi().getJobItemId());
		}
		else
		{
			return (o1.getPoitem().getOrder().getRegdate().compareTo(o2.getPoitem().getOrder().getRegdate()));
		}
	}
}
