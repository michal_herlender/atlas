package org.trescal.cwms.core.pricing.jobcost.entity.costs;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;

public interface JobCostingCostDaoSupport<C extends Cost> extends BaseDao<C, Integer> {
	
	List<C> findMatchingCosts(Class<C> clazz, Integer plantid, Integer coid, int modelid, Integer caltypeid, Integer resultsYearFilter, Integer page, Integer resPerPage);
}