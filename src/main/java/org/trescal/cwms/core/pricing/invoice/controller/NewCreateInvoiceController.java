package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.CompanyDTO;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db.InvoiceTypeService;
import org.trescal.cwms.core.pricing.invoice.form.NewCreateInvoiceForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.MonthlyInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.POInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SeparateCosts;
import org.trescal.cwms.core.system.enums.Scope;

@Controller
@IntranetController
@RequestMapping("/newcreateinvoice.htm")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class NewCreateInvoiceController {

	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private InvoiceTypeService invoiceTypeService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private MonthlyInvoice monthlyInvoice;
	@Autowired
	private POInvoice poInvoice;
	@Autowired
	private SeparateCosts separateCosts;

	@ModelAttribute("createinvoiceform")
	public NewCreateInvoiceForm getFormObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyKeyValue,
			@RequestParam(name = "coid", required = true) Integer companyId) {
		NewCreateInvoiceForm form = new NewCreateInvoiceForm();
		form.setJobItems(jobItemService.getForProspectiveInvoice(companyKeyValue.getKey(), companyId));
		return form;
	}

	@GetMapping
	public String onRequest(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyKeyValue,
			@RequestParam(name = "coid", required = true) Integer companyId, Model model, Locale locale) {
		Integer allocatedCompanyId = companyKeyValue.getKey();
		CompanyDTO companyDto = companyService.getForLinkInfo(companyId, allocatedCompanyId);
		model.addAttribute("company", companyDto);
		model.addAttribute("monthlyinvoicesystemdefault",
				monthlyInvoice.getValueByScope(Scope.COMPANY, companyId, allocatedCompanyId));
		model.addAttribute("poinvoicesystemdefault",
				poInvoice.getValueByScope(Scope.COMPANY, companyId, allocatedCompanyId));
		model.addAttribute("separatecostsinvoicesystemdefault",
				separateCosts.getValueByScope(Scope.COMPANY, companyId, allocatedCompanyId));
		model.addAttribute("pricingType", PricingType.CLIENT);
		model.addAttribute("invoiceTypes", invoiceTypeService.getAllTranslated(locale));
		model.addAttribute("addresses", addressService.getForCompany(companyId));
		return "trescal/core/pricing/invoice/newcreateinvoice";
	}
}