package org.trescal.cwms.core.pricing.purchaseorder.form.genericentityvalidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.validation.AbstractEntityValidator;
import org.trescal.cwms.core.validation.BeanValidator;

@Component("PurchaseOrderGEValidator")
public class PurchaseOrderValidator extends AbstractEntityValidator
{
	@Autowired
	private BeanValidator beanValidator;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(PurchaseOrder.class);
	}
	
	@Override
	public void validate(Object target, org.springframework.validation.Errors errors)
	{
		PurchaseOrder order = (PurchaseOrder) target;
		if (errors == null) errors = new org.springframework.validation.BindException(order, "order");
		beanValidator.validate(order, errors);
		this.errors = (BindException) errors;
	}
}