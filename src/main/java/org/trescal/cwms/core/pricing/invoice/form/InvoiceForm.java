package org.trescal.cwms.core.pricing.invoice.form;

import lombok.Data;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.pricing.PricingType;

import java.time.LocalDate;

@Data
public class InvoiceForm {

	private Integer addrid;
	private PricingType pricingType; // Used for validation
	private LocalDate invoiceDate;
	private String siteCostingNote;
	private Double totalCost;
	private Integer typeid;
	private String vatCode;
	private Integer businessContactId;
	private PaymentTerm paymentTerm;
}