/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.enums;

import org.trescal.cwms.core.pricing.entity.costs.base.Cost;

/**
 * Enum identifying different types of third party markup rate sources available
 * for {@link Cost}s with third party costs.
 * 
 * @author Richard
 */
public enum ThirdCostMarkupSource
{
	MANUAL("Manually Set"), SYSTEM_DEFAULT("Default");

	private String name;

	ThirdCostMarkupSource(String name)
	{
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}
}
