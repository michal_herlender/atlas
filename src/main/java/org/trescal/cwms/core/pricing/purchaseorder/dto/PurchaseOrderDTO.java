package org.trescal.cwms.core.pricing.purchaseorder.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchaseOrderDTO {

	private Integer id;
	private String poNo;
	private String companyName;
	private Integer compId;
	private String subName;
	private Integer subdivId;
	private LocalDate regDate;
	private String currencyERSymbol;
	private BigDecimal finalCost;
	private String createdByName;
	private String status;

	public PurchaseOrderDTO() {
	}

	public PurchaseOrderDTO(Integer id, String poNo, String companyName, LocalDate regDate, String currenceERSymbol,
			BigDecimal finalCost) {
		this.id = id;
		this.poNo = poNo;
		this.companyName = companyName;
		this.regDate = regDate;
		this.currencyERSymbol = currenceERSymbol;
		this.finalCost = finalCost;
	}


	public PurchaseOrderDTO(Integer id, String poNo, String companyName, Integer compId, 
			String subName, Integer subdivId, LocalDate regDate,
			String createdByFirstName, String createdByLastName, String status) {
		super();
		this.id = id;
		this.poNo = poNo;
		this.companyName = companyName;
		this.compId = compId;
		this.subName = subName;
		this.subdivId = subdivId;
		this.regDate = regDate;
		this.createdByName = createdByFirstName + " " + createdByLastName;
		this.status = status;
	}

}