package org.trescal.cwms.core.pricing.purchaseorder.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.PurchaseOrderNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Controller @IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class PurchaseOrderBirtDocumentController {
	
	@Autowired
	private DocumentService documentService;
	@Autowired
	private PurchaseOrderService poService;

	@RequestMapping(value="/purchaseorderbirtdocument.htm")
	public String handleRequest(Locale locale,
			@RequestParam(value="id", required=true) Integer orderId,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception {
		PurchaseOrder order = this.poService.find(orderId);
		FileNamingService fnService = new PurchaseOrderNamingService(order);
		Document doc = documentService.createBirtDocument(orderId, BaseDocumentType.PURCHASE_ORDER, locale, Component.PURCHASEORDER, fnService);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		return "redirect:/viewpurchaseorder.htm?id=" + order.getId() + "&loadtab=files-tab";
	}
}
