package org.trescal.cwms.core.pricing.invoice.controller;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.form.EditInvoiceItemForm;
import org.trescal.cwms.core.pricing.invoice.form.EditInvoiceItemValidator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@RequestMapping("/editinvoiceitem.htm")
@SessionAttributes({ Constants.HTTPSESS_PRICING_CLASS_NAME, Constants.SESSION_ATTRIBUTE_USERNAME,
		Constants.SESSION_ATTRIBUTE_SUBDIV })
public class EditInvoiceItemController {

	@Value("${cwms.invoice.allowsplitcosts}")
	private Boolean allowSplitCosts;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private InvoiceItemService invItemServ;
	@Autowired
	private InvoiceService invServ;
	@Autowired
	private JobExpenseItemService expenseItemService;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private NominalCodeService nominalCodeServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TaxCalculator taxCalculator;
	@Autowired
	private UserService userService;
	@Autowired
	private EditInvoiceItemValidator validator;
	@Value("${cwms.config.costs.invoices.roundupnearest50}")
	private boolean roundUpFinalCosts;

	public static final String FORM_NAME = "editinvoiceitemform";
	public static final String VIEW_NAME = "trescal/core/pricing/invoice/editinvoiceitem";

	@ModelAttribute("roundUpFinalCosts")
	public Boolean initialiseRoundUpFinalCosts() {
		return roundUpFinalCosts;
	}

	@ModelAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME)
	public Object defineInvoiceClass() {
		return Invoice.class;
	}

	@ModelAttribute(FORM_NAME)
	public EditInvoiceItemForm initialiseEditInvoiceItemForm(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "itemid", required = false, defaultValue = "0") int itemid,
			@RequestParam(value = "invoiceid", required = false, defaultValue = "0") int invoiceid) throws Exception {

		EditInvoiceItemForm form = new EditInvoiceItemForm();
		InvoiceItem item = this.invItemServ.findInvoiceItem(itemid);
		Invoice invoice = this.invServ.findInvoice(invoiceid);
		if ((item == null) && (invoice == null)) {
			throw new Exception("Unable to find invoice or invoice item");
		} else if ((item == null) && (invoice != null)) {
			Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
			form.setBusinessSubdivId(allocatedSubdiv.getId());
			form.setNewInvoiceItem(true);
			form.setInvoice(invoice);
			// create a new item and set some default values
			item = new InvoiceItem();
			item.setInvoice(invoice); // Needed for validation
			item.setQuantity(1);
			item.setTotalCost(new BigDecimal("0.00"));
			item.setFinalCost(new BigDecimal("0.00"));
			item.setGeneralDiscountRate(new BigDecimal("0.00"));
			item.setGeneralDiscountValue(new BigDecimal("0.00"));
			item.setItemno(this.invItemServ.getNextItemNo(invoice.getId()));
			form.setItem(item);
		} else {
			form.setNewInvoiceItem(false);
			form.setItem(item);
			form.setInvoice(item.getInvoice());
			if (item.getBusinessSubdiv() != null)
				form.setBusinessSubdivId(item.getBusinessSubdiv().getId());
			if (item.getJobItem() != null)
				form.setJobItemId(item.getJobItem().getJobItemId());
			if (item.getExpenseItem() != null)
				form.setExpenseItemId(item.getExpenseItem().getId());
		}
		return form;
	}

	@ModelAttribute("nominalList")
	public List<NominalCode> initialiseNominalList() {
		return this.nominalCodeServ.getNominalCodes(Arrays.asList(Ledgers.SALES_LEDGER));
	}

	@ModelAttribute("contact")
	public Contact getContact(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		return this.userService.get(username).getCon();
	}

	@ModelAttribute("defaultCurrency")
	public SupportedCurrency initialiseDefaultCurrency() {
		return this.currencyServ.getDefaultCurrency();
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
		// binder.setValidator(new EditInvoiceItemValidator());
	}

	@InitBinder(FORM_NAME)
	protected void initValidator(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(FORM_NAME) EditInvoiceItemForm editInvoiceItemForm,
			Locale locale) throws Exception {

		// get a list of all jobnos for this invoice
		List<String> jobnos = new ArrayList<String>();
		for (InvoiceJobLink jl : editInvoiceItemForm.getInvoice().getJobLinks()) {
			if (jl.getJob() != null) {
				jobnos.add(jl.getJob().getJobno());
			}
		}
		// load all jobitems already present in the invoice
		List<Integer> jobItemsAlreadyExistInInvoice = invServ
				.getJobItemsIdsFromInvoice(editInvoiceItemForm.getInvoice());
		Map<String, Object> model = new HashMap<>();
		model.put("businessSubdivs",
				this.subdivService.getAllActiveCompanySubdivs(editInvoiceItemForm.getInvoice().getOrganisation()));
		// load all jobitems for the linked job
		List<JobItemProjectionDTO> jobItems = this.jiServ.getJobItemsFromJobs(jobnos, locale);
		if (editInvoiceItemForm.isNewInvoiceItem()) {
			// Exclude the job items already exist in the invoice when we want to create a
			// new item
			jobItems = jobItems.stream().filter(ji -> !jobItemsAlreadyExistInInvoice.contains(ji.getJobItemId()))
					.collect(Collectors.toList());
		}
		model.put("jobitems", jobItems);
		List<ExpenseItemDTO> expenseItems = expenseItemService
				.getAvailableForInvoice(editInvoiceItemForm.getInvoice().getId());
		model.put("expenseItems", expenseItems);
		// load all invoice items on this invoice
		model.put("itemList", editInvoiceItemForm.getInvoice().getItems());
		model.put("allowSplitCosts", allowSplitCosts);
		return new ModelAndView(VIEW_NAME, model);
	}

	@RequestMapping(method = RequestMethod.POST)
	protected ModelAndView onSubmit(@Validated @ModelAttribute(FORM_NAME) EditInvoiceItemForm editInvoiceItemForm,
			BindingResult bindingResult, Locale locale) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(editInvoiceItemForm, locale);
		}
		InvoiceItem item = editInvoiceItemForm.getItem();
		Invoice invoice = this.invServ.findInvoice(editInvoiceItemForm.getInvoice().getId());
		if (editInvoiceItemForm.isNewInvoiceItem()) {
			// set the itemno
			item.setItemno(this.invItemServ.getNextItemNo(editInvoiceItemForm.getInvoice().getId()));
		}
		item.setInvoice(invoice);
		// update any jobitem links
		if ((editInvoiceItemForm.getJobItemId() != null) && (editInvoiceItemForm.getJobItemId() != 0)) {
			JobItem jobItem = this.jiServ.findJobItem(editInvoiceItemForm.getJobItemId());
			item.setJobItem(jobItem);
			item.setExpenseItem(null);
			item.setBusinessSubdiv(jobItem.getJob().getOrganisation());
			List<? extends Delivery> deliveries = this.deliveryService.findByJobItem(jobItem.getJobItemId(),
					DeliveryType.CLIENT);
			Address shipToAddress = deliveries.isEmpty() ? jobItem.getJob().getReturnTo()
					: deliveries.get(0).getAddress();
			item.setShipToAddress(shipToAddress);

		} else if (editInvoiceItemForm.getExpenseItemId() != null && editInvoiceItemForm.getExpenseItemId() != 0) {
			JobExpenseItem expenseItem = expenseItemService.get(editInvoiceItemForm.getExpenseItemId());
			item.setJobItem(null);
			item.setExpenseItem(expenseItem);
			item.setBusinessSubdiv(expenseItem.getJob().getOrganisation());
			Address shipToAddress = expenseItem.getJob().getReturnTo();
			item.setShipToAddress(shipToAddress);
		} else {
			item.setJobItem(null);
			item.setExpenseItem(null);
			item.setBusinessSubdiv(this.subdivService.get(editInvoiceItemForm.getBusinessSubdivId()));
			item.setShipToAddress(invoice.getAddress());
		}
		// set the nominals of each of the costs - site invoices can have null
		// costs so we need to add a null check in here as well
		if (item.getCalibrationCost() != null) {
			item.getCalibrationCost().setNominal(editInvoiceItemForm.getCalibrationNominalId() == null ? null
					: this.nominalCodeServ.get(editInvoiceItemForm.getCalibrationNominalId()));
		}
		if (item.getRepairCost() != null) {
			item.getRepairCost().setNominal(editInvoiceItemForm.getRepairNominalId() == null ? null
					: this.nominalCodeServ.get(editInvoiceItemForm.getRepairNominalId()));
		}
		if (item.getAdjustmentCost() != null) {
			item.getAdjustmentCost().setNominal(editInvoiceItemForm.getAdjustmentNominalId() == null ? null
					: this.nominalCodeServ.get(editInvoiceItemForm.getAdjustmentNominalId()));
		}
		if (item.getPurchaseCost() != null) {
			item.getPurchaseCost().setNominal(editInvoiceItemForm.getPurchaseNominalId() == null ? null
					: this.nominalCodeServ.get(editInvoiceItemForm.getPurchaseNominalId()));
		}
		if (item.getServiceCost() != null) {
			item.getServiceCost().setNominal(editInvoiceItemForm.getServiceNominalId() == null ? null
					: this.nominalCodeServ.get(editInvoiceItemForm.getServiceNominalId()));
		}
		item.setNominal(editInvoiceItemForm.getSingleNominalId() == null ? null
				: this.nominalCodeServ.get(editInvoiceItemForm.getSingleNominalId()));
		// update the costs of the item
		if (item.isBreakUpCosts()) {
			for (Cost cost : item.getCosts()) {
				CostCalculator.updateSingleCost(cost, this.roundUpFinalCosts);
			}
			CostCalculator.updateItemCostWithRunningTotal(item);
		} else {
			CostCalculator.updateItemCost(item);
		}
		if (editInvoiceItemForm.isNewInvoiceItem()) {
			this.invItemServ.save(item);
		} else {
			item = this.invItemServ.merge(item);
		}
		if (editInvoiceItemForm.isNewInvoiceItem()) {
			if (invoice.getItems() == null) {
				invoice.setItems(new TreeSet<InvoiceItem>());
			}
			invoice.getItems().add(item);
		}
		// update the cost of the invoice - only if this is not a site invoice
		if (!invoice.getPricingType().equals(PricingType.SITE)) {
            invoice.setTotalCost(invoice.getItems().stream().map(InvoiceItem::getFinalCost).reduce(BigDecimal.ZERO,
                BigDecimal::add));
            taxCalculator.calculateTax(invoice);
            CostCalculator.updatePricingWithCalculatedTotalCost(invoice);
        }
		this.invServ.merge(invoice);
		return new ModelAndView(new RedirectView("/viewinvoice.htm?id=" + invoice.getId(), true));
	}
}