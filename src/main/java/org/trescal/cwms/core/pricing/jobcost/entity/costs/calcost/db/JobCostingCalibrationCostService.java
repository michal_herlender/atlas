package org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.jobcost.dto.ModelJobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface JobCostingCalibrationCostService extends JobCostingCostService<JobCostingCalibrationCost>
{
	void deleteJobCostingCalibrationCost(JobCostingCalibrationCost jobcostingcalibrationcost);

	JobCostingCalibrationCost findEagerJobCostingCalibrationCost(int id);

	JobCostingCalibrationCost findJobCostingCalibrationCost(int id);
	
	List<ModelJobCostingCalibrationCost> findMostRecentCostForModels(Collection<Integer> modelIds, CalibrationType calType, Company company);
	
	List<JobCostingCalibrationCost> getAllJobCostingCalibrationCosts();

	void insertJobCostingCalibrationCost(JobCostingCalibrationCost jobcostingcalibrationcost);

	/**
	 * Creates a new {@link Cost} for the given {@link JobCostingItem}. Checks
	 * if cost lookups are enabled and if so looks up the most suitable cost for
	 * the item based on the given {@link CostSource} hierarchy of the system
	 * default hierarchy. Additionally tests if the deactivateOnZero property
	 * has been set to true and if so ensures that the resulting cost is
	 * deactivated if it's calculated value is 0.
	 * 
	 * @param ji the {@link JobItem}.
	 * @param mostRecent the most recent other {@link JobCostingItem} for the
	 *        current {@link JobItem}.
	 * @return a {@link Cost}.
	 */
	Cost loadCost(JobCostingItem jci, JobCostingItem mostRecent);

	void saveOrUpdateJobCostingCalibrationCost(JobCostingCalibrationCost jobcostingcalibrationcost);

	void updateJobCostingCalibrationCost(JobCostingCalibrationCost jobcostingcalibrationcost);
}