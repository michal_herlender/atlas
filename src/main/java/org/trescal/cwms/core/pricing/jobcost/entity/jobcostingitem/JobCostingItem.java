package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem;

import org.hibernate.annotations.SortComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostOrderComparator;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark.JobCostingItemNote;
import org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.JobCostingItemRemark;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;

import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Entity
@Table(name = "jobcostingitem", uniqueConstraints = { @UniqueConstraint(columnNames = { "itemno",
		"costingid" }) }, indexes = @Index(columnList = "jobitemid", name = "IDX_jobcostingitem_jobitemid"))
@Setter
public class JobCostingItem extends ThirdPartyPricingItem implements NoteAwareEntity {

	private Integer id;
	/**
	 * Stores whether or not the item had been calibrated at the time that this
	 * costing was constructed. This is used in the logic determining whether or not
	 * inspection costs should be displayed.
	 */
	private boolean calibrated;
	private JobCostingAdjustmentCost adjustmentCost;
	private JobCostingCalibrationCost calibrationCost;
	private JobCostingInspectionCost inspectionCost;
	private JobCosting jobCosting;
	private JobItem jobItem;
	private Set<JobCostingItemNote> notes;
	private JobCostingPurchaseCost purchaseCost;
	private JobCostingRepairCost repairCost;
	private Set<JobCostingItemRemark> remarks;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public Integer getId() {
		return id;
	}

	@Override
	@Transient
	public SortedSet<Cost> getCosts() {
		SortedSet<Cost> costs = new TreeSet<>(new CostOrderComparator());
		if (adjustmentCost != null)
			costs.add(adjustmentCost);
		if (calibrationCost != null)
			costs.add(calibrationCost);
		if (inspectionCost != null)
			costs.add(inspectionCost);
		if (repairCost != null)
			costs.add(repairCost);
		if (purchaseCost != null)
			costs.add(purchaseCost);
		return costs;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "adjustmentcost_id")
	public JobCostingAdjustmentCost getAdjustmentCost() {
		return this.adjustmentCost;
	}

	@Override
	@Transient
	public PricingItem getBaseUnit() {
		return null;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "calcost_id")
	public JobCostingCalibrationCost getCalibrationCost() {
		return this.calibrationCost;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "inspectioncost_id")
	public JobCostingInspectionCost getInspectionCost() {
		return this.inspectionCost;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "costingid", nullable = false)
	public JobCosting getJobCosting() {
		return this.jobCosting;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getJobItem() {
		return this.jobItem;
	}

	@Override
	@Transient
	public Set<? extends PricingItem> getModules() {
		return null;
	}

	@Override
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "jobCostingItem")
	@SortComparator(NoteComparator.class)
	public Set<JobCostingItemNote> getNotes() {
		return this.notes;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "purchasecost_id")
	public JobCostingPurchaseCost getPurchaseCost() {
		return this.purchaseCost;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "repaircost_id")
	public JobCostingRepairCost getRepairCost() {
		return this.repairCost;
	}

	@Column(name = "calibrated", columnDefinition = "bit")
	// @Type(type = "boolean")
	public boolean isCalibrated() {
		return this.calibrated;
	}

	@OneToMany(mappedBy = "pricingItem", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<JobCostingItemRemark> getRemarks() {
		return remarks;
	}

	/**
	 * Business logic to determine whether inspection costs should be shown for a
	 * particular costing item
	 */
	@Transient
	public boolean isShowInspectionCost() {
		// if the item has NOT yet been calibrated, or if it has and there are
		// any active costs other than calibration, show the inspection cost
		return !this.calibrated || ((this.adjustmentCost != null) && this.adjustmentCost.isActive())
				|| ((this.repairCost != null) && this.repairCost.isActive())
				|| ((this.purchaseCost != null) && this.purchaseCost.isActive());
	}
}