package org.trescal.cwms.core.pricing.invoice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db.InvoiceItemPOService;

@Controller
@JsonController
public class AssignInvoiceItemsToPOController {

	@Autowired
	private InvoiceItemPOService invoiceitemposervice;

	@RequestMapping(value = "/assignitemstopo.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultWrapper assignItemsToPO(@RequestParam(name = "type", required = true) String type,
			@RequestParam(name = "iiarray[]", required = false) int[] iiarray,
			@RequestParam(name = "poid", required = true) Integer poid,
			@RequestParam(name = "jobid", required = false) Integer jobid,
			@RequestParam(name = "invId", required = true) Integer invId) throws Exception {
		return this.invoiceitemposervice.assignInvoiceItemsToPO(type, iiarray, poid, jobid, invId);
	}

}
