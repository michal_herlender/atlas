package org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.JobCostingItemRemark;

@Service
public class JobCostingItemRemarkServiceImpl extends BaseServiceImpl<JobCostingItemRemark, Integer>
		implements JobCostingItemRemarkService {
	
	@Autowired
	private JobCostingItemRemarkDao baseDao;

	@Override
	protected BaseDao<JobCostingItemRemark, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public List<JobCostingItemRemark> getRemarksForItem(Integer jobCostingItemId) {
		return baseDao.getRemarksForItem(jobCostingItemId);
	}
	
	@Override
	public List<JobCostingItemRemark> getRemarksForJobCosting(Integer jobCostingId) {
		return baseDao.getRemarksForJobCosting(jobCostingId);
	}
	
}
