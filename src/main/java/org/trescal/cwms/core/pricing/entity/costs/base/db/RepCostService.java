package org.trescal.cwms.core.pricing.entity.costs.base.db;

import java.util.List;

import org.trescal.cwms.core.pricing.entity.costs.base.RepCost;

public interface RepCostService
{
	RepCost findRepCost(int id);
	void insertRepCost(RepCost repcost);
	void updateRepCost(RepCost repcost);
	List<RepCost> getAllRepCosts();
	void deleteRepCost(RepCost repcost);
	void saveOrUpdateRepCost(RepCost repcost);
}