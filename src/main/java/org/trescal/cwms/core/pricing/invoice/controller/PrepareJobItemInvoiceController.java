package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.invoice.dto.ReadyToInvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class PrepareJobItemInvoiceController {

	@Autowired
	private InvoiceService invoiceService;

	public static final String READY_TO_INVOICE = "readyToInvoice";
	public static final String COUNT_JOB_ITEMS = "countJobItems";
	public static final String COUNT_SERVICE_ITEMS = "countServiceItems";
	public static final String COMPANY_WIDE = "companyWide";

	public static final String REQUEST_URL = "/preparejobiteminvoice.htm";
	public static final String VIEW_NAME = "trescal/core/pricing/invoice/preparejobiteminvoice";

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.GET)
	public String showView(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "companyWide", required = false, defaultValue = "false") Boolean companyWide) {
		Integer allocatedSubdivId = companyWide ? null : subdivDto.getKey();
		Map<Integer, ReadyToInvoiceDTO> readyToInvoice = invoiceService.getReadyToInvoice(allocatedSubdivId, companyDto.getKey());
		model.addAttribute(READY_TO_INVOICE, readyToInvoice);
		model.addAttribute(COUNT_JOB_ITEMS, 
				readyToInvoice.values().stream().mapToLong(ReadyToInvoiceDTO::getJicompletedTotal).sum());
		model.addAttribute(COUNT_SERVICE_ITEMS, 
				readyToInvoice.values().stream().mapToLong(ReadyToInvoiceDTO::getJecompletedTotal).sum());
		model.addAttribute(COMPANY_WIDE, companyWide);
		return VIEW_NAME;
	}

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.POST)
	public String previewInvoice(@RequestParam(name = "coid", required = true) int coid,
			@RequestParam(value = "companyWide", required = false, defaultValue = "false") Boolean companyWide) {
		return "redirect:createinvoice.htm?coidforjobitems=" + coid + "companyWide=" + companyWide;
	}
}