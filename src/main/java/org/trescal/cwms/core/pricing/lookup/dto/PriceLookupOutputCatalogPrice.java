package org.trescal.cwms.core.pricing.lookup.dto;

import java.math.BigDecimal;

public class PriceLookupOutputCatalogPrice extends PriceLookupOutput {
	// For catalog price result projection
	public PriceLookupOutputCatalogPrice(Integer serviceTypeId, Integer instrumentModelId, Integer salesCategoryId, 
			Integer catalogPriceId, BigDecimal catalogPrice, Integer supportedCurrencyId) {
		super();
		// Inputs
		this.serviceTypeId = serviceTypeId;
		this.instrumentModelId = instrumentModelId;
		this.salesCategoryId = salesCategoryId;
		// Outputs (remainder null for catalog price result)
		this.catalogPriceId = catalogPriceId;
		this.catalogPrice = catalogPrice;
		this.supportedCurrencyId = supportedCurrencyId;
	}

}
