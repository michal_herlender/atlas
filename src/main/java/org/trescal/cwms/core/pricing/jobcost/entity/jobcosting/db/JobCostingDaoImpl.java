package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.dto.JobCostingDTO;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository("JobCostingDao")
public class JobCostingDaoImpl extends AllocatedToCompanyDaoImpl<JobCosting, Integer> implements JobCostingDao
{
//	private final Integer JOB_COSTING_ISSUED_TO_CLIENT_ID = 34;
	
	@Override
	protected Class<JobCosting> getEntity() {
		return JobCosting.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public int findCurrentMaxVersion(int jobid)
	{
		Criteria crit = getSession().createCriteria(JobCosting.class);
		crit.createCriteria("job").add(Restrictions.idEq(jobid));
		crit.setProjection(Projections.property("ver"));
		crit.addOrder(Order.desc("ver"));
		List<Integer> list = crit.list();
		return list.size() > 0 ? list.get(0) : 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JobCosting findEagerJobCosting(int id)
	{
		Criteria crit = getSession().createCriteria(JobCosting.class);
		crit.add(Restrictions.idEq(id));

		crit.setFetchMode("contact", FetchMode.JOIN);
		crit.setFetchMode("contact.sub", FetchMode.JOIN);
		crit.setFetchMode("contact.sub.comp", FetchMode.JOIN);
		crit.setFetchMode("currency", FetchMode.JOIN);
		crit.setFetchMode("job", FetchMode.JOIN);
		crit.setFetchMode("status", FetchMode.JOIN);
		crit.setFetchMode("costingType", FetchMode.JOIN);
		crit.setFetchMode("lastUpdateBy", FetchMode.JOIN);

		crit.setFetchMode("items", FetchMode.JOIN);
		crit.setFetchMode("items.jobItem", FetchMode.JOIN);
		crit.setFetchMode("items.jobItem.calType", FetchMode.JOIN);
		crit.setFetchMode("items.jobItem.calType.serviceType", FetchMode.JOIN);
		crit.setFetchMode("items.jobItem.inst", FetchMode.JOIN);
		crit.setFetchMode("items.jobItem.inst.mfr", FetchMode.JOIN);
		crit.setFetchMode("items.jobItem.inst.model", FetchMode.JOIN);
		crit.setFetchMode("items.jobItem.inst.model.mfr", FetchMode.JOIN);
		crit.setFetchMode("items.jobItem.inst.model.description", FetchMode.JOIN);

		List<JobCosting> costs = crit.list();
		return costs.size() > 0 ? costs.get(0) : null;
	}
	
	public void issueJobCosting(JobCosting jobcosting)
	{
		// do not remove - this is here to be used as a pointcut for the hook
		// interceptor that issues the costing
	}

	@Override
	public List<JobCosting> getJobCostingListByJobitemId(Integer jobitemid) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<JobCosting> query = builder.createQuery(JobCosting.class);
        Root<JobCostingItem> jobCostingItemRoot =  query.from(JobCostingItem.class);
        Join<JobCostingItem, JobItem> jobItemJoin = jobCostingItemRoot.join(JobCostingItem_.jobItem);
        Join<JobCostingItem, JobCosting> jobCostingJoin = jobCostingItemRoot.join(JobCostingItem_.jobCosting);
        Join<JobCosting, JobCostingStatus> jobCostingStatusJoin = jobCostingJoin.join(JobCosting_.status);
        	
		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(builder.equal(jobItemJoin.get(JobItem_.jobItemId),jobitemid));
        Predicate orConj = builder.or(builder.equal(jobCostingStatusJoin.get(JobCostingStatus_.issued),true), 
        		builder.or(builder.equal(jobCostingStatusJoin.get(JobCostingStatus_.accepted),true), 
        				builder.equal(jobCostingStatusJoin.get(JobCostingStatus_.rejected),true)));
        conjunction.getExpressions().add(orConj);
        //conjunction.getExpressions().add(builder.equal(itemActivityJoin.get(ItemActivity_.stateid), JOB_COSTING_ISSUED_TO_CLIENT_ID));
		
        query.where(conjunction);

        //query.distinct(true);
        
        query.select(jobCostingJoin);

        return getEntityManager().createQuery(query).getResultList();
	}
	
	@Override
	public List<JobCosting> getEagerJobCostingListForJobIds(Collection<Integer> jobids) {
		List<JobCosting> result = null;
		if (jobids == null || jobids.isEmpty()) {
			result = Collections.emptyList();
		}
		else {
			result = getResultList(cb -> {
				CriteriaQuery<JobCosting> cq = cb.createQuery(JobCosting.class);
				Root<JobCosting> root = cq.from(JobCosting.class);
				// Prevent lazy loading
				root.fetch(JobCosting_.jcViewed, JoinType.LEFT);
				Fetch<JobCosting, JobCostingItem> jitems = root.fetch(JobCosting_.items);
				// No not-null constraint, but all costs always exist for a costing, consider adding constraint 
				Fetch<JobCostingItem, JobCostingCalibrationCost> jcCalCost = jitems.fetch(JobCostingItem_.calibrationCost, JoinType.LEFT);
				Fetch<JobCostingItem, JobCostingRepairCost> jcRepCost = jitems.fetch(JobCostingItem_.repairCost, JoinType.LEFT);
				Fetch<JobCostingItem, JobCostingAdjustmentCost> jcAdjCost = jitems.fetch(JobCostingItem_.adjustmentCost, JoinType.LEFT);
				Fetch<JobCostingItem, JobCostingPurchaseCost> jcPurCost = jitems.fetch(JobCostingItem_.purchaseCost, JoinType.LEFT);
				// Fetch one level deeper into one-to-one
				jcCalCost.fetch(JobCostingCalibrationCost_.linkedCost, JoinType.LEFT);
				jcRepCost.fetch(JobCostingRepairCost_.linkedCost, JoinType.LEFT);
				jcAdjCost.fetch(JobCostingAdjustmentCost_.linkedCost, JoinType.LEFT);
				jcPurCost.fetch(JobCostingPurchaseCost_.linkedCost, JoinType.LEFT);
				
				Join<JobCosting, Job> job =  root.join(JobCosting_.job, JoinType.INNER);
				cq.where(job.get(Job_.jobid).in(jobids));
				
				return cq;
			});
		}
		return result;
	}

	@Override
	public JobCosting getJobCosting(Integer jobItemId, Integer version) {
		return getFirstResult(cb -> {
			CriteriaQuery<JobCosting> cq = cb.createQuery(JobCosting.class);
			Root<JobCosting> jcRoot = cq.from(JobCosting.class);
			Join<JobCosting, JobCostingItem> jciJoin = jcRoot.join(JobCosting_.items.getName());
			Join<JobCostingItem, JobItem> jiJoin = jciJoin.join(JobCostingItem_.jobItem.getName());
			cq.where(cb.and(cb.equal(jiJoin.get(JobItem_.jobItemId), jobItemId), cb.equal(jcRoot.get(JobCosting_.ver), version)));
			return cq;
		}).orElse(null);
	}
	
	@Override
	public List<JobCostingDTO> getJobCostingsByJobId(Integer jobId, Locale locale){
		return getResultList(cb -> {
			CriteriaQuery<JobCostingDTO> cq = cb.createQuery(JobCostingDTO.class);
			Root<JobCosting> jc = cq.from(JobCosting.class);			
			Join<JobCosting, Job> job =  jc.join(JobCosting_.job, JoinType.INNER);
			Join<Job, Contact> jobContact = job.join(Job_.con);
			Join<Contact, Subdiv> clientSubdiv = jobContact.join(Contact_.sub);
			Join<Subdiv, Company> clientCompany = clientSubdiv.join(Subdiv_.comp);
			Join<Company, SupportedCurrency> currency = clientCompany.join(Company_.currency);
			Join<JobCosting, Contact> jobCostingContact = jc.join(JobCosting_.contact);
			Join<JobCosting, Contact> jobCostingCreatedBy = jc.join(JobCosting_.createdBy);
			Join<Contact, Subdiv> subdiv = jobCostingContact.join(Contact_.sub);
			Join<Subdiv, Company> comp = subdiv.join(Subdiv_.comp);
			Join<JobCosting, JobCostingViewed> jobCostingViewed = jc.join(JobCosting_.jcViewed);
			Join<JobCosting, JobCostingStatus> jobCostingStatus = jc.join(JobCosting_.status);
	       
			Join<JobCostingStatus, Translation> jobCostingStatusTranslation = jobCostingStatus.join(JobCostingStatus_.nametranslations,
					javax.persistence.criteria.JoinType.LEFT);
			jobCostingStatusTranslation.on(cb.equal(jobCostingStatusTranslation.get(Translation_.locale), locale));
			 		
			Subquery<Long> itemCountSq = cq.subquery(Long.class);
			Root<JobCostingItem> itemCount = itemCountSq.from(JobCostingItem.class); 
			itemCountSq.where(cb.equal(itemCount.get(JobCostingItem_.jobCosting), jc));
			itemCountSq.select(cb.count(itemCount));
			
			cq.where(cb.equal(job.get(Job_.jobid), jobId));
			
			cq.select(cb.construct(JobCostingDTO.class, jc.get(JobCosting_.id), jobContact,
					jc.get(JobCosting_.type) , jobCostingCreatedBy, jobCostingCreatedBy, jc.get(JobCosting_.regdate),   
					jobCostingViewed.get(JobCostingViewed_.lastViewedBy), jobCostingViewed.get(JobCostingViewed_.lastViewedOn),
					jc.get(JobCosting_.lastUpdateBy), jc.get(JobCosting_.lastUpdateOn), jobCostingStatusTranslation.get(Translation_.translation.getName()),
					itemCountSq.getSelection(), job.get(Job_.jobno), jc.get(JobCosting_.ver), comp.get(Company_.coname), currency.get(SupportedCurrency_.currencyCode),
					jc.get(JobCosting_.costingTypeId)));
			
			return cq;
		});
	}
}