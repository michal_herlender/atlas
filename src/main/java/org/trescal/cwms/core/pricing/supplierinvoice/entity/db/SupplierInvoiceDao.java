package org.trescal.cwms.core.pricing.supplierinvoice.entity.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;

public interface SupplierInvoiceDao extends AllocatedDao<Company, SupplierInvoice, Integer> {

}
