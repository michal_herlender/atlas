package org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "creditnotestatus")
public class CreditNoteStatus extends BaseStatus
{
	private CreditNoteStatus next;

	/**
	 * Flags that this activity is associated with the insert of a
	 * {@link CreditNote}
	 */
	private boolean onInsert;

	/**
	 * Indicates that this activity is issuing the {@link CreditNote}
	 */
	private boolean onIssue;

	private boolean requiresAttention;
	private ActionType type;

	/**
	 * Translation for name 
	 */
	private Set<Translation> nametranslations;

	/**
	 * Translation for description 
	 */
	private Set<Translation> descriptiontranslations;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "nextid")
	public CreditNoteStatus getNext()
	{
		return this.next;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "actiontype", nullable = false)
	public ActionType getType()
	{
		return this.type;
	}

	@NotNull
	@Column(name = "oninsert", nullable = false, columnDefinition="bit")
	public boolean isOnInsert()
	{
		return this.onInsert;
	}

	@NotNull
	@Column(name = "onissue", nullable = false, columnDefinition="bit")
	public boolean isOnIssue()
	{
		return this.onIssue;
	}

	@NotNull
	@Column(name = "requiresattention", nullable = false, columnDefinition="bit")
	public boolean isRequiresAttention()
	{
		return this.requiresAttention;
	}

	public void setNext(CreditNoteStatus next)
	{
		this.next = next;
	}

	public void setOnInsert(boolean onInsert)
	{
		this.onInsert = onInsert;
	}

	public void setOnIssue(boolean onIssue)
	{
		this.onIssue = onIssue;
	}

	public void setRequiresAttention(boolean requiresAttention)
	{
		this.requiresAttention = requiresAttention;
	}

	public void setType(ActionType type)
	{
		this.type = type;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="creditnotestatusnametranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getNametranslations() {
		return nametranslations;
	}

	public void setNametranslations(Set<Translation> nametranslations) {
		this.nametranslations = nametranslations;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="creditnotestatusdescriptiontranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getDescriptiontranslations() {
		return descriptiontranslations;
	}

	public void setDescriptiontranslations(Set<Translation> descriptiontranslations) {
		this.descriptiontranslations = descriptiontranslations;
	}
}
