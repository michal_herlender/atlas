package org.trescal.cwms.core.pricing.jobcost.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.trescal.cwms.core.tools.NumberTools;

public class ModelCostingHistory
{
	private String calTypeName;
	private boolean companyActive;
	private int companyId;
	private String companyName;
	private boolean contactActive;
	private int contactId;
	private String contactName;
	private int costingId;
	private String costingNo;
	private String currency;
	private BigDecimal discount;
	private String displayColour;
	private String displayColourFast;
	private BigDecimal finalCost;
	private int itemId;
	private int itemNo;
	private boolean onStop;
	private Date regDate;
	private int turn;
	
	public ModelCostingHistory() {}
	
	public String getCalTypeName()
	{
		return this.calTypeName;
	}

	public int getCompanyId()
	{
		return this.companyId;
	}

	public String getCompanyName()
	{
		return this.companyName;
	}

	public int getContactId()
	{
		return this.contactId;
	}

	public String getContactName()
	{
		return this.contactName;
	}

	public int getCostingId()
	{
		return this.costingId;
	}

	public String getCostingNo()
	{
		return this.costingNo;
	}

	public String getCurrency()
	{
		return this.currency;
	}

	public BigDecimal getDiscount()
	{
		return this.discount;
	}

	public String getDiscount2dp()
	{
		return NumberTools.displayBigDecimalTo2DecimalPlaces(this.discount);
	}

	public String getDisplayColour()
	{
		return this.displayColour;
	}

	public String getDisplayColourFast()
	{
		return this.displayColourFast;
	}

	public BigDecimal getFinalCost()
	{
		return this.finalCost;
	}

	public String getFinalCost2dp()
	{
		return NumberTools.displayBigDecimalTo2DecimalPlaces(this.finalCost);
	}

	public int getItemId()
	{
		return this.itemId;
	}

	public int getItemNo()
	{
		return this.itemNo;
	}

	public Date getRegDate()
	{
		return this.regDate;
	}

	public int getTurn()
	{
		return this.turn;
	}

	public boolean isCompanyActive()
	{
		return this.companyActive;
	}

	public boolean isContactActive()
	{
		return this.contactActive;
	}

	public boolean isOnStop()
	{
		return this.onStop;
	}

	public void setCalTypeName(String calTypeName)
	{
		this.calTypeName = calTypeName;
	}

	public void setCompanyActive(boolean companyActive)
	{
		this.companyActive = companyActive;
	}

	public void setCompanyId(int companyId)
	{
		this.companyId = companyId;
	}

	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	public void setContactActive(boolean contactActive)
	{
		this.contactActive = contactActive;
	}

	public void setContactId(int contactId)
	{
		this.contactId = contactId;
	}

	public void setContactName(String contactName)
	{
		this.contactName = contactName;
	}

	public void setCostingId(int costingId)
	{
		this.costingId = costingId;
	}

	public void setCostingNo(String costingNo)
	{
		this.costingNo = costingNo;
	}

	public void setCurrency(String currency)
	{
		this.currency = currency;
	}

	public void setDiscount(BigDecimal discount)
	{
		this.discount = discount;
	}

	public void setDisplayColour(String displayColour)
	{
		this.displayColour = displayColour;
	}

	public void setDisplayColourFast(String displayColourFast)
	{
		this.displayColourFast = displayColourFast;
	}

	public void setFinalCost(BigDecimal finalCost)
	{
		this.finalCost = finalCost;
	}

	public void setItemId(int itemId)
	{
		this.itemId = itemId;
	}

	public void setItemNo(int itemNo)
	{
		this.itemNo = itemNo;
	}

	public void setOnStop(boolean onStop)
	{
		this.onStop = onStop;
	}

	public void setRegDate(Date regDate)
	{
		this.regDate = regDate;
	}

	public void setTurn(int turn)
	{
		this.turn = turn;
	}

}