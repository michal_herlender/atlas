package org.trescal.cwms.core.pricing.jobcost.form;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JobCostingExpenseItemForm {

	private Integer id;
	private Integer itemNo;
	private Integer quantity;
	private BigDecimal singlePrice;
	private BigDecimal discountRate;
	private BigDecimal discountValue;
	private BigDecimal finalPrice;
	private Integer expenseItemId;
	private String serviceType;
	private String modelName;
	private String comment;
}