package org.trescal.cwms.core.pricing.invoice.entity.invoicestatus;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "invoicestatus")
public class InvoiceStatus extends BaseStatus
{
	private ActionType type;
	private List<Invoice> invoices;
	private InvoiceStatus next;
	private boolean requiresAttention;

	/**
	 * Flags that this activity is associated with the insert of a
	 * {@link Invoice}.
	 */
	private boolean onInsert;

	/**
	 * Indicates that this activity is issuing the invoice.
	 */
	private boolean onIssue;
	
	/**
	 * Indicates whether the invoice has been issued -
	 * can apply to multiple statuses
	 */
	private boolean issued;

	/**
	 * Indicates whether the invoice has been approved -
	 * can apply to multiple statuses
	 */
	private boolean approved;

	/**
	 * Translation for name 
	 */
	private Set<Translation> nametranslations;

	/**
	 * Translation for description 
	 */
	private Set<Translation> descriptiontranslations;

	@OneToMany(mappedBy = "status", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<Invoice> getInvoices()
	{
		return invoices;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "nextid", nullable = true)
	public InvoiceStatus getNext()
	{
		return next;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "potype", nullable = false)
	public ActionType getType()
	{
		return type;
	}

	@NotNull
	@Column(name = "oninsert", nullable = false, columnDefinition="bit")
	public boolean isOnInsert()
	{
		return onInsert;
	}

	@NotNull
	@Column(name = "onissue", nullable = false, columnDefinition="bit")
	public boolean isOnIssue()
	{
		return onIssue;
	}

	@NotNull
	@Column(name = "requiresattention", nullable = false, columnDefinition="bit")
	public boolean isRequiresAttention()
	{
		return requiresAttention;
	}

	@NotNull
	@Column(name = "approved", nullable=false, columnDefinition="bit")
	public boolean isApproved() {
		return this.approved;
	}

	@NotNull
	@Column(name = "issued", nullable=false, columnDefinition="bit")
	public boolean isIssued()
	{
		return this.issued;
	}

	public void setInvoices(List<Invoice> invoices)
	{
		this.invoices = invoices;
	}

	public void setNext(InvoiceStatus next)
	{
		this.next = next;
	}

	public void setOnInsert(boolean onInsert)
	{
		this.onInsert = onInsert;
	}

	public void setOnIssue(boolean onIssue)
	{
		this.onIssue = onIssue;
	}

	public void setRequiresAttention(boolean requiresAttention)
	{
		this.requiresAttention = requiresAttention;
	}

	public void setType(ActionType type)
	{
		this.type = type;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="invoicestatusnametranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getNametranslations() {
		return nametranslations;
	}

	public void setNametranslations(Set<Translation> nametranslations) {
		this.nametranslations = nametranslations;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="invoicestatusdescriptiontranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getDescriptiontranslations() {
		return descriptiontranslations;
	}

	public void setDescriptiontranslations(Set<Translation> descriptiontranslations) {
		this.descriptiontranslations = descriptiontranslations;
	}

	public void setIssued(boolean issued) {
		this.issued = issued;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}
}
