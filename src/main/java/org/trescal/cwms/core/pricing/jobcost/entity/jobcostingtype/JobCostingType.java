package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingtype;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

/**
 * Entity describing the basic purpose of a {@link JobCosting}.
 * 
 * @author richard
 */

public enum JobCostingType
{
	PRELIMINARY(1, true, "jobcostingtype.preliminary", "Preliminary"), 
	FINAL(2, true, "jobcostingtype.final", "Final"), 
	REVISED(3, true, "jobcostingtype.revised", "Revised"), 
	CONFIRMATION(4, false, "jobcostingtype.confirmation", "Confirmation"), 
	INVOICEONLY(5, false, "jobcostingtype.invoiceonly", "Invoice Only");
	
	private int id;
	/**
	 * Determines if this job costing type should include inspection charges or
	 * not.
	 */
	private boolean includeInspectionCharges;
	private String messageCode;
	private String name;
	private ReloadableResourceBundleMessageSource messages;
	
	private JobCostingType (int id, boolean includeInspectionCharges, String messageCode, String name) {
		this.id = id;
		this.includeInspectionCharges = includeInspectionCharges;
		this.messageCode = messageCode;
		this.name = name;
	}
		
	@Component
	public static class InstructionTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (JobCostingType jct : EnumSet.allOf(JobCostingType.class))
               jct.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	public int getId()
	{
		return this.id;
	}

	public String getType()
	{
		Locale loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name, loc);
		}
		return this.toString();
	}
	
	public static JobCostingType getJobCostingTypeById(int id) {
		switch (id) {
		case 1: return PRELIMINARY;
		case 2: return FINAL;
		case 3: return REVISED;
		case 4: return CONFIRMATION;
		case 5: return INVOICEONLY;
		}
		return null;
	}

	public boolean isIncludeInspectionCharges()
	{
		return this.includeInspectionCharges;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
}
