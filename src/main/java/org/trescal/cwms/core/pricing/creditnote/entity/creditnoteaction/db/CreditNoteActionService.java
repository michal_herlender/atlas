package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.db;

import java.util.List;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.CreditNoteAction;

public interface CreditNoteActionService
{
	CreditNoteAction findCreditNoteAction(int id);
	void insertCreditNoteAction(CreditNoteAction creditnoteaction);
	void updateCreditNoteAction(CreditNoteAction creditnoteaction);
	List<CreditNoteAction> getAllCreditNoteActions();
	void deleteCreditNoteAction(CreditNoteAction creditnoteaction);
}