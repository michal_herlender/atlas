package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.db.InvoicePOService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service("InvoiceItemPOService")
public class InvoiceItemPOServiceImpl implements InvoiceItemPOService {

	@Autowired
	private BPOService bpoServ;
	@Autowired
	private InvoiceItemService invoiceItemService;
	@Autowired
	private InvoiceItemPODao invoiceItemPODao;
	@Autowired
	private InvoicePOService invPOServ;
	@Autowired
	private POService poServ;
	@Autowired
	private InvoicePOItemDao invoicePOItemDao;

	public ResultWrapper ajaxAddPOLink(int invItemId, String type, int poId) {
		InvoiceItemPO iipo = new InvoiceItemPO();
		iipo.setInvItem(this.invoiceItemService.findInvoiceItem(invItemId));

		if (type.equalsIgnoreCase("invoice")) {
			iipo.setInvPO(this.invPOServ.get(poId));
		} else if (type.equalsIgnoreCase("jobpo")) {
			iipo.setJobPO(this.poServ.get(poId));
		} else if (type.equalsIgnoreCase("jobbpo")) {
			iipo.setBpo(this.bpoServ.get(poId));
		}

		this.insertInvoiceItemPO(iipo);

		return new ResultWrapper(true, null, iipo, null);
	}

	/**
	 * Here we take all the items and assign the PO
	 */
	public ResultWrapper assignInvoiceItemsToPO(String type, int[] invItemIds, int poId, Integer jobid,
			Integer invId) {
		try {
			List<Integer> checkedItemIds = new ArrayList<Integer>();
			if(invItemIds != null){
				checkedItemIds = Arrays.stream(invItemIds).boxed().collect(Collectors.toList());
			}
			List<InvoiceItem> items = this.invoiceItemService.getAllByInvoiceAndJob(invId, jobid);
			switch (type) {
			case "invpo":
				InvoicePO invoicePO = this.invPOServ.get(poId);
				for (InvoiceItem invoiceItem : items) {
					if (checkedItemIds.contains(invoiceItem.getId())) {
						if (!invoiceItem.getInvItemPOs().stream().anyMatch(iipo -> invoicePO.equals(iipo.getInvPO()))) {
							InvoiceItemPO iipo = new InvoiceItemPO();
							iipo.setInvItem(invoiceItem);
							iipo.setInvPO(invoicePO);
							this.insertInvoiceItemPO(iipo);
						}
					} else {
						invoiceItem.getInvItemPOs().removeIf(iiPo -> invoicePO.equals(iiPo.getInvPO()));
						invoiceItemService.merge(invoiceItem);
					}
				}
				break;
			case "jobpo":
				PO jobPO = this.poServ.get(poId);
				for (InvoiceItem invoiceItem : items) {
					if (checkedItemIds.contains(invoiceItem.getId())) {
						if (!invoiceItem.getInvItemPOs().stream().anyMatch(iipo -> jobPO.equals(iipo.getJobPO()))) {
							InvoiceItemPO iipo = new InvoiceItemPO();
							iipo.setInvItem(invoiceItem);
							iipo.setJobPO(jobPO);
							this.insertInvoiceItemPO(iipo);
						}
					} else {
						invoiceItem.getInvItemPOs().removeIf(iiPo -> jobPO.equals(iiPo.getJobPO()));
						invoiceItemService.merge(invoiceItem);
					}
				}
				break;
			case "jobbpo":
				BPO bpo = this.bpoServ.get(poId);
				for (InvoiceItem invoiceItem : items) {
					if (checkedItemIds.contains(invoiceItem.getId())) {
						if (!invoiceItem.getInvItemPOs().stream().anyMatch(iipo -> bpo.equals(iipo.getBpo()))) {
							InvoiceItemPO iipo = new InvoiceItemPO();
							iipo.setInvItem(invoiceItem);
							iipo.setBpo(bpo);
							this.insertInvoiceItemPO(iipo);
						}
					} else {
						invoiceItem.getInvItemPOs().removeIf(iiPo -> bpo.equals(iiPo.getBpo()));
						invoiceItemService.merge(invoiceItem);
					}
				}
				break;
			default:
			}
			return new ResultWrapper(true, null, checkedItemIds, null);
		} catch (Exception e) {
			return new ResultWrapper(false, e.getMessage());
		}
	}

	public void deleteInvoiceItemPO(InvoiceItemPO iipo) {
		this.invoiceItemPODao.remove(iipo);
	}

	public InvoiceItemPO findInvoiceItemPO(int id) {
		return this.invoiceItemPODao.find(id);
	}

	public List<InvoiceItemPO> getAllInvoiceItemPOs() {
		return this.invoiceItemPODao.findAll();
	}

	public void insertInvoiceItemPO(InvoiceItemPO InvoiceItemPO) {
		this.invoiceItemPODao.persist(InvoiceItemPO);
	}

	@Override
	public void linkSamePOsAsJobItemOrExpenseItem(InvoiceItem item) {
		boolean hasJobItem = item.getJobItem() != null;
		boolean hasExpenseItem = item.getExpenseItem() != null;

		if (hasJobItem || hasExpenseItem) {
			if (item.getInvItemPOs() == null) {
				item.setInvItemPOs(new HashSet<>());
			}
			if (hasJobItem) {
				for (JobItemPO jipo : item.getJobItem().getItemPOs()) {
					InvoiceItemPO iipo = new InvoiceItemPO();
					iipo.setBpo(jipo.getBpo());
					iipo.setJobPO(jipo.getPo());
					iipo.setInvItem(item);

					item.getInvItemPOs().add(iipo);
				}
			}
			if (hasExpenseItem) {
				for (JobExpenseItemPO eipo : item.getExpenseItem().getItemPOs()) {
					InvoiceItemPO iipo = new InvoiceItemPO();
					iipo.setBpo(eipo.getBpo());
					iipo.setJobPO(eipo.getPo());
					iipo.setInvItem(item);

					item.getInvItemPOs().add(iipo);
				}
			}
		}
	}

	public void updateInvoicePOItem(InvoicePOItem InvoiceItemPO) {
		this.invoicePOItemDao.save(InvoiceItemPO);
	}

	public void saveAll(List<InvoicePOItem> invoicePoItems) {
		invoicePOItemDao.saveAll(invoicePoItems);
	}


}
