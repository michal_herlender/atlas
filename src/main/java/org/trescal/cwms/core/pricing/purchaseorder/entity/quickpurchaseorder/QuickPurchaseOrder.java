package org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "quickpurchaseorder", uniqueConstraints={@UniqueConstraint(columnNames={"pono"})})
public class QuickPurchaseOrder extends Allocated<Company>
{
	private boolean consolidated;
	private Contact createdBy;
	private Date createdOn;
	private String description;
	private int id;
	private String jobno;
	private String pono;

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable = false)
	public Contact getCreatedBy()
	{
		return this.createdBy;
	}

	@NotNull
	@Column(name = "date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedOn()
	{
		return this.createdOn;
	}

	@NotNull
	@Size(min=1, max = 1000)
	@Column(name = "description", nullable = false, length = 1000)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Length(max = 30)
	@Column(name = "jobno", length = 30)
	public String getJobno()
	{
		return this.jobno;
	}

	@Length(max=30)
	@Column(name = "pono", length = 30, nullable = false)
	public String getPono()
	{
		return this.pono;
	}

	@NotNull
	@Column(name = "consolidated", nullable = false, columnDefinition="tinyint")
	public boolean isConsolidated()
	{
		return this.consolidated;
	}

	public void setConsolidated(boolean consolidated)
	{
		this.consolidated = consolidated;
	}

	public void setCreatedBy(Contact createdBy)
	{
		this.createdBy = createdBy;
	}

	public void setCreatedOn(Date createdOn)
	{
		this.createdOn = createdOn;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setJobno(String jobno)
	{
		this.jobno = jobno;
	}

	public void setPono(String pono)
	{
		this.pono = pono;
	}

}