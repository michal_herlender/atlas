package org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;

public interface CreditNoteStatusDao extends BaseDao<CreditNoteStatus, Integer> {
	
	List<CreditNoteStatus> findAllCreditNoteStatusRequiringAttention();
	
	CreditNoteStatus findOnInsertActivity();
}