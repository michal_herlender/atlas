package org.trescal.cwms.core.pricing.catalogprice.entity;

import java.math.BigDecimal;
import java.time.Duration;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.variableprice.entity.VariablePrice;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

import lombok.Setter;

@Entity
@Setter
@Table(name = "catalogprice")
public class CatalogPrice extends Allocated<Company> {

	private int id;
	private InstrumentModel instrumentModel;
	private CostType costType;
	private ServiceType serviceType;
	private BigDecimal fixedPrice;
	private VariablePrice variablePrice;
	private Duration standardTime;
	private String comments;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return id;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid", nullable = false)
	public InstrumentModel getInstrumentModel() {
		return instrumentModel;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "costtypeid", nullable = false)
	public CostType getCostType() {
		return costType;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicetype", nullable = false)
	public ServiceType getServiceType() {
		return serviceType;
	}

	@NotNull(message = "{error.value.notselected}")
	@Column(name = "fixedPrice")
	public BigDecimal getFixedPrice() {
		return fixedPrice;
	}

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "unitPrice", column = @Column(nullable = true)),
			@AttributeOverride(name = "unit", column = @Column(nullable = true)) })
	public VariablePrice getVariablePrice() {
		return variablePrice;
	}

	@Column(name = "standardTime")
	public Duration getStandardTime() {
		return standardTime;
	}

	@Length(max = 2000)
	@Column(name = "comments", nullable = true, length = 2000) // migration expanded to 2000
	public String getComments() {
		return comments;
	}	
	
}