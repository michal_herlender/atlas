package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.birt.invoices.InvoiceDataSet;
import org.trescal.cwms.core.documents.filenamingservice.InvoiceNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.HTTPSESS_NEW_FILE_LIST,
		Constants.SESSION_ATTRIBUTE_SUBDIV })
public class InvoiceDocumentController {

	@Autowired
	private InvoiceService invServ;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/birtinvoicedocument.htm")
	public String handleBirtRequest(Model model,
			@RequestParam(value = "id", required = true) Integer invoiceId,
			@RequestParam(value = "summarized", required = false, defaultValue = "false") Boolean summarized,
			@RequestParam(value = "printDiscount", required = false, defaultValue = "true") Boolean printDiscount,
			@RequestParam(value = "printUnitaryPrice", required = false, defaultValue = "true") Boolean printUnitaryPrice,
			@RequestParam(value = "printQuantity", required = false, defaultValue = "true") Boolean printQuantity,
			@RequestParam(value = "showAppendix", required = false, defaultValue = "true") Boolean showAppendix,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "languageTag", required = true) String languageTag,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) {

		if (description != null) {
			this.invServ.updateSummaryDescription(invoiceId, description);
		}
		
		Invoice invoice = this.invServ.findInvoice(invoiceId);
		InvoiceNamingService fnService = new InvoiceNamingService(invoice);

		Map<String, Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, !invoice.isIssued());
		additionalParameters.put(InvoiceDataSet.PARAMETER_PRINT_DISCOUNT, printDiscount);
		additionalParameters.put(InvoiceDataSet.PARAMETER_PRINT_QUANTITY, printQuantity);
		additionalParameters.put(InvoiceDataSet.PARAMETER_SHOW_APPENDIX, showAppendix);
		additionalParameters.put(InvoiceDataSet.PARAMETER_PRINT_UNITPRICE, printUnitaryPrice);
		additionalParameters.put(InvoiceDataSet.PARAMETER_SUMMARIZED, summarized);

		Locale documentLocale = Locale.forLanguageTag(languageTag);

		Document doc = documentService.createBirtDocument(invoiceId, BaseDocumentType.INVOICE_CALIBRATION,
				documentLocale, Component.INVOICE, fnService, additionalParameters);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		model.asMap().clear();
		return "redirect:viewinvoice.htm?id=" + invoiceId + "&loadtab=files-tab";
	}

	/**
	 * Targeted for removal once new invoice document fully accepted
	 */
	@RequestMapping(value = "/invoicedocument.htm", method = RequestMethod.GET)
	public String createInvoiceDoc(@RequestParam(value = "id", required = true) int id,
			@RequestParam(value = "summarized", required = true) String summarized,
			@RequestParam(value = "printDiscount", required = true) String printDiscount,
			@RequestParam(value = "printUnitaryPrice", required = true) String printUnitaryPrice,
			@RequestParam(value = "printQuantity", required = true) String printQuantity,
			@RequestParam(value = "showAppendix", required = true) String showAppendix,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "languageTag", required = true) String languageTag,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		
		if (description != null) {
			this.invServ.updateSummaryDescription(id, description);
		}

		Invoice invoice = this.invServ.findInvoice(id);

		Contact contact = userService.get(username).getCon();

		Locale documentLocale = Locale.forLanguageTag(languageTag);

		this.invServ.generateInvoiceDocument(invoice, "html", sessionNewFileList, contact, subdivDto.getKey(),
				Boolean.valueOf(printDiscount), Boolean.valueOf(printUnitaryPrice),  Boolean.valueOf(printQuantity), Boolean.valueOf(showAppendix),
				Boolean.valueOf(summarized), description, documentLocale);
		return "redirect:/viewinvoice.htm?id=" + invoice.getId() + "&loadtab=files-tab";
	}
}