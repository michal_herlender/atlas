package org.trescal.cwms.core.pricing.jobcost.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.base.db.AdjCostService;
import org.trescal.cwms.core.pricing.entity.costs.base.db.CalCostService;
import org.trescal.cwms.core.pricing.entity.costs.base.db.PurchaseCostService;
import org.trescal.cwms.core.pricing.entity.costs.base.db.RepCostService;
import org.trescal.cwms.core.pricing.entity.enums.PartsMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemService;
import org.trescal.cwms.core.pricing.jobcost.form.EditJobCostingItemForm;
import org.trescal.cwms.core.pricing.jobcost.form.EditJobCostingItemValidator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost.TPQuotationAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost.TPQuotationRepairCost;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Controller used to edit a {@link JobCostingItem}.
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class EditJobCostingItemController {
	private static final Logger logger = LoggerFactory.getLogger(EditJobCostingItemController.class);

	@Autowired
	private AdjCostService adjCostServ;
	@Autowired
	private CalCostService calCostServ;
	@Autowired
	private JobCostingItemService jobCostItemServ;
	@Autowired
	private JobCostingService jobCostServ;
	@Autowired
	private PurchaseCostService purCostServ;
	@Autowired
	private RepCostService repCostServ;
	@Autowired
	private UserService userService;
	@Autowired
	private EditJobCostingItemValidator validator;
	@Autowired
	private TaxCalculator taxCalculator;
	@Autowired
	private SubdivService subdivService;

	public static final String FORM_NAME = "command";

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME)
	public Object getPricingClass() {
		return JobCosting.class;
	}

	@ModelAttribute(FORM_NAME)
	public EditJobCostingItemForm formBackingObject(
			@RequestParam(value = "id", required = false, defaultValue = "0") Integer id,
			@RequestParam(value = "loadtab", required = false) String openTab) throws Exception {
		JobCostingItem item = this.jobCostItemServ.get(id);
		if ((item == null) || (id == 0)) {
			logger.error("Job Costing item with id " + id + " could not be found");
			throw new Exception("Could not find job costing item");
		}
		EditJobCostingItemForm form = new EditJobCostingItemForm();
		form.setItem(item);
		form.setOpenTabId(openTab);

		return form;
	}

	@RequestMapping(value = "/editjobcostingitem.htm", method = RequestMethod.POST)
	public String onSubmit(Model model, @Validated @ModelAttribute(FORM_NAME) EditJobCostingItemForm form,
			BindingResult bindingResult, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model, subdivDto, form);
		}
		Contact currentContact = this.userService.get(username).getCon();
		JobCostingItem item = form.getItem();
		// test if a tpquotation cost has been set for any of the cost types and
		// if so look it up and set it as the linkedCostSrc or alternately
		// remove any non-linked tpcosts - also remove any linked third costs if
		// the third cost source is set to MANUAL
		if ((form.getAdjustmentCostId() != null)
				&& (item.getAdjustmentCost().getThirdCostSrc() != ThirdCostSource.MANUAL)) {
			item.getAdjustmentCost().setLinkedCostSrc(
					(TPQuotationAdjustmentCost) this.adjCostServ.findAdjCost(form.getAdjustmentCostId()));
		} else {
			item.getAdjustmentCost().setLinkedCostSrc(null);
		}
		if ((form.getCalibrationCostId() != null)
				&& (item.getCalibrationCost().getThirdCostSrc() != ThirdCostSource.MANUAL)) {
			item.getCalibrationCost().setLinkedCostSrc(
					(TPQuotationCalibrationCost) this.calCostServ.findCalCost(form.getCalibrationCostId()));
		} else {
			item.getCalibrationCost().setLinkedCostSrc(null);
		}
		if ((form.getPurchaseCostId() != null)
				&& (item.getPurchaseCost().getThirdCostSrc() != ThirdCostSource.MANUAL)) {
			item.getPurchaseCost().setLinkedCostSrc(
					(TPQuotationPurchaseCost) this.purCostServ.findPurchaseCost(form.getPurchaseCostId()));
		} else {
			item.getPurchaseCost().setLinkedCostSrc(null);
		}
		if ((form.getRepairCostId() != null) && (item.getRepairCost().getThirdCostSrc() != ThirdCostSource.MANUAL)) {
			item.getRepairCost()
					.setLinkedCostSrc((TPQuotationRepairCost) this.repCostServ.findRepCost(form.getRepairCostId()));
		} else {
			item.getRepairCost().setLinkedCostSrc(null);
		}
		this.jobCostItemServ.updateJobCostingItemCosts(item);
		item = this.jobCostItemServ.merge(item);
        JobCosting costing = this.jobCostServ.findJobCosting(item.getJobCosting().getId());
        Set<PricingItem> pricingItems = new HashSet<>();
		pricingItems.addAll(costing.getItems());
		pricingItems.addAll(costing.getExpenseItems());
		CostCalculator.updatePricingTotalCost(costing, pricingItems);
		taxCalculator.calculateAndSetVATaxAndFinalCost(costing);
		this.jobCostServ.saveOrUpdate(costing, currentContact);
		String loadTab = (form.getOpenTabId() != null) ? "&loadtab=" + form.getOpenTabId() : "";
		model.asMap().clear();
		return "redirect:editjobcostingitem.htm?id=" + item.getId() + loadTab;
	}

	@RequestMapping(value = "/editjobcostingitem.htm", method = RequestMethod.GET)
	public String referenceData(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) EditJobCostingItemForm form) throws Exception {

		Subdiv subDiv = subdivService.get(subdivDto.getKey());
		model.addAttribute("defaultCurrency", subDiv.getComp().getCurrency());
		model.addAttribute("partsmarkupsources", PartsMarkupSource.values());
		model.addAttribute("tpcostsrc", ThirdCostSource.values());
		model.addAttribute("tpcostmarkupsrc", ThirdCostMarkupSource.values());
		model.addAttribute("repairePartsPrice", getRepairePartsCost(form));

		return "trescal/core/pricing/jobcost/editjobcostingitem";
	}

	private BigDecimal getRepairePartsCost(EditJobCostingItemForm form) {
		BigDecimal price = BigDecimal.ZERO;

		if (!form.getItem().getJobItem().getRepairInspectionReports().isEmpty()) {
			int lastindex = form.getItem().getJobItem().getRepairInspectionReports().size();

			for (FreeRepairOperation frepOp : form.getItem().getJobItem().getRepairInspectionReports()
					.get(lastindex - 1).getFreeRepairOperations()) {
				if (!frepOp.getFreeRepairComponents().isEmpty())
					for (FreeRepairComponent freRepComp : frepOp.getFreeRepairComponents()) {
						price = price.add(freRepComp.getCost());

					}
			}
		}

		return price;
	}
}