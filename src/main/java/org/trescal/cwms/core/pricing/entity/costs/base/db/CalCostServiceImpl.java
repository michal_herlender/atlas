package org.trescal.cwms.core.pricing.entity.costs.base.db;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostLookUp;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.CalCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db.JobCostingCalibrationCostService;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.db.QuotationCalibrationCostService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.MultiCurrencyUtils;

@Service("CalCostService")
public class CalCostServiceImpl implements CalCostService {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private CalibrationTypeService calTypeService;
    @Autowired
    private CatalogPriceService catalogPriceService;
    @Autowired
    private QuotationCalibrationCostService quotationCostService;
    @Autowired
    private JobCostingCalibrationCostService jobCostingCostService;
    @Autowired
    private CalCostDao CalCostDao;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private InstrumentModelService instrumentModelService;
    @Autowired
    private CompanyService companyService;

    public void deleteCalCost(CalCost calcost) {
        this.CalCostDao.remove(calcost);
    }

    public CalCost findCalCost(int id) {
        return this.CalCostDao.find(id);
    }

    public List<CalCost> getAllCalCosts() {
        return this.CalCostDao.findAll();
    }

    public void insertCalCost(CalCost CalCost) {
        this.CalCostDao.persist(CalCost);
    }

    @Override
    public CostLookUp lookupAlternativeCosts(CostLookUp lookUp, Integer plantid, int modelid, int calTypeId, int coid, Integer jobid, List<CostSource> costSources, int businessCoId) {
        Locale locale = LocaleContextHolder.getLocale();
    	
    	if (lookUp == null) {
            lookUp = new CostLookUp();
        }

        lookUp.setCostSources(costSources);

        final Company targetCompany = companyService.get(coid);
        lookUp.setCurrency(companyService.getCurrency(targetCompany));

        if (modelid > 0) {
            InstrumentModel instrumentModel = instrumentModelService.findInstrumentModel(modelid);
            lookUp.setTranslatedFullModelName(translationService.getCorrectTranslation(instrumentModel.getNameTranslations(), locale));
        }

        // TODO : Ultimately, this whole method should use service type.  For now, serviceType is passed to catalog price
        CalibrationType calType = this.calTypeService.find(calTypeId);
        int serviceTypeId = calType.getServiceType().getServiceTypeId();

        for (CostSource source : costSources) {
            switch (source) {
                case JOB_COSTING:
                    this.logger.debug("loading costs from JOB_COSTING");

                    List<JobCostingCalibrationCost> jobCostingCosts = (List<JobCostingCalibrationCost>) this.jobCostingCostService.findMatchingCosts(null, coid, modelid, calTypeId, 1, Constants.RESULTS_PER_PAGE);
                    lookUp.setJobCostingCosts(jobCostingCosts);
                    this.logger.debug("Found " + jobCostingCosts.size()
                            + " matching linked job costing costs.");

                    break;
                case QUOTATION:
                    this.logger.debug("loading calibration costs from QUOTATION");

                    this.logger.debug("finding calibration costs from linked quotations");

                    if (jobid != null) {
                        List<QuotationCalibrationCost> linkedQuotationCosts = (List<QuotationCalibrationCost>) this.quotationCostService.findMatchingCostOnLinkedQuotation(jobid, modelid, calTypeId);
                        lookUp.setLinkedQuotationCosts(linkedQuotationCosts);
                        this.logger.debug("Found " + linkedQuotationCosts.size()
                                + " matching linked quotation costs.");
                    } else {
                        this.logger.debug("No jobid set so not attempting to lookup linked quotation costs");
                    }

                    List<QuotationCalibrationCost> unLinkedQuotationCosts = (List<QuotationCalibrationCost>) this.quotationCostService.findMatchingCosts(coid, modelid, calTypeId, 1, Constants.RESULTS_PER_PAGE);
                    lookUp.setUnLinkedQuotationCosts(unLinkedQuotationCosts);

                    this.logger.debug("Found " + unLinkedQuotationCosts.size()
                            + " matching non-linked quotation costs.");
                    break;

                case INSTRUMENT:
                    this.logger.debug("loading calibration costs from INSTRUMENT ");

                    if (plantid != null) {
                        List<JobCostingCalibrationCost> instJobCostingCosts = (List<JobCostingCalibrationCost>) this.jobCostingCostService.findMatchingCosts(plantid, coid, modelid, calTypeId, 1, null);
                        lookUp.setPlantJobCostingCosts(instJobCostingCosts);
                        this.logger.debug("Found " + instJobCostingCosts.size()
                                + " matching job costing costs.");
                    } else {
                        this.logger.debug("Found 0 matching job costing costs - null plantid.");
                    }

                    break;
                case MODEL:
                    this.logger.debug("loading calibration costs from MODEL");
                    // Note, we don't narrow to a CostType.CALIBRATION as we may use quotation for CostType.SERVICE lookup, also 
                    List<CatalogPrice> listCatalogPrices = this.catalogPriceService.find(modelid, serviceTypeId, null, businessCoId, true);


                    SupportedCurrency requiredCurrency = lookUp.getCurrency();
                    List<BigDecimal> pricesInCurrency = listCatalogPrices.stream().map(c ->
                        MultiCurrencyUtils.convertCurrencyValue(c.getFixedPrice(), c.getOrganisation().getCurrency(), requiredCurrency)
                    ).collect(Collectors.toList());
                    lookUp.setCatalogPrices(pricesInCurrency);
                    if (listCatalogPrices.size() > 0) {
                        lookUp.setComment(listCatalogPrices.get(0).getComments());
                        Set<Translation> translations = listCatalogPrices.get(0).getServiceType().getLongnameTranslation();
                        lookUp.setTranslatedServiceTypeName(translationService.getCorrectTranslation(translations, locale));
                    }
                    this.logger.debug("Found " + listCatalogPrices.size() + " matching catalog prices.");
                    break;
                case CONTRACT_REVIEW:
                    break;
                case DERIVED:
                    break;
                case JOB:
                    break;
                case MANUAL:
                    break;
                default:
                    break;
            }
        }

        return lookUp;
    }

    public void saveOrUpdateCalCost(CalCost calcost) {
        this.CalCostDao.saveOrUpdate(calcost);
    }

    public void updateCalCost(CalCost CalCost) {
        this.CalCostDao.update(CalCost);
    }
}