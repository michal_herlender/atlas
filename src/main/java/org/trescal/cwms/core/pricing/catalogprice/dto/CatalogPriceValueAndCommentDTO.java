package org.trescal.cwms.core.pricing.catalogprice.dto;

import java.math.BigDecimal;

public class CatalogPriceValueAndCommentDTO {

	private BigDecimal value;
	private String comment;

	public CatalogPriceValueAndCommentDTO(BigDecimal value, String comment) {
		super();
		this.value = value;
		this.comment = comment;
	}
	
	public BigDecimal getValue() {
		return value;
	}
	public String getComment() {
		return comment;
	}

	public void setValue(BigDecimal convertCurrencyValue) {
	}
}
