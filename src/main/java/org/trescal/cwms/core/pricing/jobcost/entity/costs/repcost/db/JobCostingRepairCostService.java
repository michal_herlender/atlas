package org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.db;

import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

public interface JobCostingRepairCostService extends JobCostingCostService<JobCostingRepairCost>
{
	void deleteJobCostingRepairCost(JobCostingRepairCost jobcostingrepaircost);

	JobCostingRepairCost findJobCostingRepairCost(int id);

	List<JobCostingRepairCost> getAllJobCostingRepairCosts();

	void insertJobCostingRepairCost(JobCostingRepairCost jobcostingrepaircost);

	/**
	 * Creates a new {@link Cost} for the given {@link JobCostingItem}. Checks
	 * if cost lookups are enabled and if so looks up the most suitable cost for
	 * the item based on the given {@link CostSource} hierarchy of the system
	 * default hierarchy. Additionally tests if the deactivateOnZero property
	 * has been set to true and if so ensures that the resulting cost is
	 * deactivated if it's calculated value is 0.
	 * 
	 * @param ji the {@link JobItem}.
	 * @param mostRecent the most recent other {@link JobCostingItem} for the
	 *        current {@link JobItem}.
	 * @return a {@link Cost}.
	 */
	Cost loadCost(JobCostingItem jci, JobCostingItem mostRecent);

	void saveOrUpdateJobCostingRepairCost(JobCostingRepairCost jobcostingrepaircost);

	void updateJobCostingRepairCost(JobCostingRepairCost jobcostingrepaircost);
}