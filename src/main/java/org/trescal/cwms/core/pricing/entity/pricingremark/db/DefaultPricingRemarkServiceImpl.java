package org.trescal.cwms.core.pricing.entity.pricingremark.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.admin.dto.DefaultPricingRemarkDto;
import org.trescal.cwms.core.admin.form.DefaultPricingRemarksForm;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.pricingremark.DefaultPricingRemark;

@Service
public class DefaultPricingRemarkServiceImpl extends BaseServiceImpl<DefaultPricingRemark, Integer> 
	implements DefaultPricingRemarkService {

	@Autowired
	private DefaultPricingRemarkDao baseDao;
	@Autowired
	private CompanyService companyService; 
	
	@Override
	protected BaseDao<DefaultPricingRemark, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public void perFormEdit(DefaultPricingRemarksForm form) {
		Company businessCompany = companyService.get(form.getBusinessCompanyId());
		performEditForCostType(form.getAdjustmentRemarks(), CostType.ADJUSTMENT, businessCompany);
		performEditForCostType(form.getCalibrationRemarks(), CostType.CALIBRATION, businessCompany);
		performEditForCostType(form.getRepairRemarks(), CostType.REPAIR, businessCompany);
		performEditForCostType(form.getPurchaseRemarks(), CostType.PURCHASE, businessCompany);
	}
	
	private void performEditForCostType(List<DefaultPricingRemarkDto> remarks, 
			CostType costType, Company businessCompany) {
		for (DefaultPricingRemarkDto dto : remarks) {
			if (dto.getId() > 0 && !dto.getRemark().isEmpty()) {
				// Update existing default remark (if changed)
				DefaultPricingRemark remark = this.get(dto.getId());
				if (!dto.getRemark().equals(remark.getRemark())) {
					remark.setRemark(dto.getRemark());
				}
			}
			else if (dto.getId() > 0 && dto.getRemark().isEmpty()) {
				// Delete existing default remark
				DefaultPricingRemark remark = this.get(dto.getId());
				this.delete(remark);
			}
			else if (dto.getId() == 0 && !dto.getRemark().isEmpty()) {
				// Create new default remark
				DefaultPricingRemark remark = new DefaultPricingRemark();
				remark.setCostType(costType);
				remark.setLocale(dto.getLocale());
				remark.setOrganisation(businessCompany);
				remark.setRemark(dto.getRemark());
				remark.setRemarkType(dto.getRemarkType());
			}
		}
	}

	@Override
	public List<DefaultPricingRemark> getRemarksForOrganisation(Integer businessCompanyId, Locale locale) {
		return this.baseDao.getRemarksForOrganisation(businessCompanyId, locale);
	}
	
}
