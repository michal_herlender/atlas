package org.trescal.cwms.core.pricing.jobcost.projection;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * Meant as a base class for projections of entities based on PricingItem (e.g. JobCostingItem) 
 * 
 * Could add other fields when/if necessary (discounts / quantities) from PricingItem.
 */

@Getter @Setter
public abstract class PricingItemProjectionDTO {
	private Integer pricingItemId;
	private Integer itemno;
	private BigDecimal totalCost;
	private BigDecimal finalCost;

	/**
	 * Constructor for JPA use (via subclasses)
	 */
	public PricingItemProjectionDTO(Integer pricingItemId, Integer itemno, BigDecimal totalCost, BigDecimal finalCost) {
		this.pricingItemId = pricingItemId;
		this.itemno = itemno;
		this.totalCost = totalCost;
		this.finalCost = finalCost;
	}
}