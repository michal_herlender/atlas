package org.trescal.cwms.core.pricing.entity.pricingremark;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@MappedSuperclass
public abstract class PricingRemark {
	private Integer id;
	private PricingRemarkType remarkType;
	private CostType costType;
	private String remark;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@NotNull
	@Column(name="remarktype", nullable=false)
	@Enumerated(EnumType.ORDINAL)
	public PricingRemarkType getRemarkType() {
		return remarkType;
	}

	public CostType getCostType() {
		return costType;
	}
	
	@NotNull
	@Length(max=255)
	@Column(name="remark", nullable=false, columnDefinition="nvarchar(255)")
	public String getRemark() {
		return remark;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setCostType(CostType costType) {
		this.costType = costType;
	}

	public void setRemarkType(PricingRemarkType remarkType) {
		this.remarkType = remarkType;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}	
}
