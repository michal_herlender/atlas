package org.trescal.cwms.core.pricing.invoice.form;

import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.InvoiceSource;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PrepareMonthlyInvoiceForm
{
	Integer addressid;
	Company company;
	Integer currencyId;
	Integer jobid;
	List<Integer> jobids;
	List<String> jobNos;
	List<Job> jobs;
	InvoiceSource source;
	Integer typeid;
	Integer allocatedSubdivId;

}