package org.trescal.cwms.core.pricing.entity.costs.thirdparty;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

/**
 * Interface that all {@link Cost}s that offer third party cost support must
 * implement.
 * 
 * @author Richard
 */
public interface ThirdPartyCostSupport
{
	/**
	 * The in-house charges for this {@link CostType}. Use:
	 * 
	 * @Column(name = "house*costname*cost", unique = false, insertable = true,
	 *              updatable = true, precision = 10, scale = 2)
	 */
	public BigDecimal getHouseCost();

	/**
	 * @return get a {@link TPQuotation} cost that is acting as the cost source
	 *         for this cost.
	 */
	public Cost getLinkedCostSrc();

	/**
	 * @return get the source of the third party cost - one of 'manual' or
	 *         'quotation'.
	 */
	public ThirdCostSource getThirdCostSrc();

	/**
	 * @return the total cost of the third party costs.
	 */
	public BigDecimal getThirdCostTotal();

	/**
	 * @return third party manually set cost.
	 */
	public BigDecimal getThirdManualPrice();

	/**
	 * @return the markup rate being applied to the third party costs. (%).
	 */
	public BigDecimal getThirdMarkupRate();

	/**
	 * @return get the third party markup source.
	 */
	public ThirdCostMarkupSource getThirdMarkupSrc();

	/**
	 * @return get the value of the markup being applied to the third party
	 *         cost.
	 */
	public BigDecimal getThirdMarkupValue();

	public BigDecimal getTpCarriageIn();

	// public void setLinkedCostSrc(Cost cost);

	public BigDecimal getTpCarriageInMarkupValue();

	public BigDecimal getTpCarriageMarkupRate();

	public ThirdCostMarkupSource getTpCarriageMarkupSrc();

	public BigDecimal getTpCarriageOut();

	public BigDecimal getTpCarriageOutMarkupValue();

	public BigDecimal getTpCarriageTotal();

	/**
	 * @param houseCalCost the houseCalCost to set
	 */
	public void setHouseCost(BigDecimal houseCost);

	public void setThirdCostSrc(ThirdCostSource thirdCalPriceSrc);

	public void setThirdCostTotal(BigDecimal thirdCostTotal);

	public void setThirdManualPrice(BigDecimal thirdManualPrice);

	public void setThirdMarkupRate(BigDecimal thirdCalPriceMarkupRate);

	public void setThirdMarkupSrc(ThirdCostMarkupSource thirdCalPriceMarkupSrc);

	public void setThirdMarkupValue(BigDecimal thirdPriceMarkupValue);

	public void setTpCarriageIn(BigDecimal tpCarriageIn);

	public void setTpCarriageInMarkupValue(BigDecimal tpCarriageInMarkupValue);

	public void setTpCarriageMarkupRate(BigDecimal tpCarriageMarkupRate);

	public void setTpCarriageMarkupSrc(ThirdCostMarkupSource tpCarriageMarkupSrc);

	public void setTpCarriageOut(BigDecimal tpCarriageOut);

	public void setTpCarriageOutMarkupValue(BigDecimal tpCarriageOutMarkupValue);

	public void setTpCarriageTotal(BigDecimal tpCarriageTotal);
}
