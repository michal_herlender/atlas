package org.trescal.cwms.core.pricing.supplierinvoice.entity.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentmode.db.PaymentModeService;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.pricing.supplierinvoice.form.SupplierInvoiceForm;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValue;

@Service
public class SupplierInvoiceServiceImpl extends
		BaseServiceImpl<SupplierInvoice, Integer> implements SupplierInvoiceService {
	@Autowired
	private MessageSource messageSource; 
	@Autowired
	private SupplierInvoiceDao supplierInvoiceDao;
	@Autowired
	private PaymentModeService paymentModeService;
	@Autowired
	private SupportedCurrencyService currencyServ;
	
	@Override
	protected BaseDao<SupplierInvoice, Integer> getBaseDao() {
		return this.supplierInvoiceDao;
	}
	
	@Override
	public List<KeyValue<Integer,String>> getDTOList(PurchaseOrder order, Locale locale, boolean prependNoneDto) {
		List <KeyValue<Integer,String>> results = new ArrayList<>();
		if (prependNoneDto) {
			results.add(new KeyValue<Integer, String>(0, messageSource.getMessage("company.none", null, locale)));
		}
		for(SupplierInvoice invoice : order.getSupplierInvoices()) {
			results.add(new KeyValue<Integer, String>(invoice.getId(), invoice.getInvoiceNumber()));
		}
		return results;
	}

	@Override
	public SupplierInvoice editSupplierInvoice(Integer supplierinvoiceid, SupplierInvoiceForm form){
		SupplierInvoice supplierInvoice = this.get(supplierinvoiceid);
		supplierInvoice.setInvoiceDate(form.getInvoiceDate());
		supplierInvoice.setInvoiceNumber(form.getInvoiceNumber());
		supplierInvoice.setPaymentDate(form.getPaymentDate());
		supplierInvoice.setTotalWithTax(form.getTotalWithTax());
		supplierInvoice.setTotalWithoutTax(form.getTotalWithoutTax());
		supplierInvoice.setCurrency(this.currencyServ.findCurrencyByCode(form.getCurrencyCode()));

		PaymentTerm paymentTerm = (form.getPaymentTerm() == null) ? null : form.getPaymentTerm();
		supplierInvoice.setPaymentTerm(paymentTerm);


		PaymentMode paymentMode = (form.getPaymentModeId() == null) ? null : this.paymentModeService.get(form.getPaymentModeId());
		supplierInvoice.setPaymentMode(paymentMode);

		this.save(supplierInvoice);
		
		return supplierInvoice;
	}

}
