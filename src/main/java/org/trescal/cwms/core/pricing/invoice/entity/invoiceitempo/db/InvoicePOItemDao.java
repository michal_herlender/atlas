package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db;

import org.springframework.data.repository.CrudRepository;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;

public interface InvoicePOItemDao extends CrudRepository<InvoicePOItem, Integer> {
}
