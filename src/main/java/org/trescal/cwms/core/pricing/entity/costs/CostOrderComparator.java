/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.costs;

import java.util.Comparator;

import org.trescal.cwms.core.pricing.entity.costs.base.Cost;

/**
 * Custom cost comparator that checks for any custom ordering being applied to
 * both costs and if not defaults to the system default costtype ordering.
 * 2015-10-07 GB: Rewrote this class as it was hiding results with the same cost type. 
 * Also removed checks for null entities (there should be no reason to compare nulls!)
 * If NPE occurs, the underlying reason should be fixed.
 */
public class CostOrderComparator implements Comparator<Cost>
{
	public int compare(Cost c1, Cost c2)
	{
		// First priority: Sort by any cost specific ordering if available
		if ((c1.getCustomOrderBy() != null) && 
			(c2.getCustomOrderBy() != null)) {	
			int customOrderResult = c1.getCustomOrderBy().compareTo(c2.getCustomOrderBy()); 
			if (customOrderResult != 0) {
				return customOrderResult;
			}
		}
		// Second priority: Compare by natural order of cost type if available
		if ((c1.getCostType() != null) &&
			(c2.getCostType() != null)) {
			int naturalOrderResult = c1.getCostType().getNaturalOrder().compareTo(c2.getCostType().getNaturalOrder());
			if (naturalOrderResult != 0) {
				return naturalOrderResult;
			}
		}
		// Third priority: Compare by type of class
		int instanceClassResult = c1.getClass().getName().compareTo(c2.getClass().getName());
		if (instanceClassResult != 0) {
			return instanceClassResult;
		}
		// Fourth priority: Compare by entity ID (note it may not be set yet)
		if ((c1.getCostid() != null) && (c2.getCostid() != null)) {
			return c1.getCostid() - c2.getCostid();
		}
		if ((c1.getCostid() == null) && (c2.getCostid() == null)) return 0;
		if (c1.getCostid() == null) return -1;
		return 1;
	}
}
