package org.trescal.cwms.core.pricing.entity.costs.base.db;

import java.util.List;

import org.trescal.cwms.core.pricing.entity.costs.base.AdjCost;

public interface AdjCostService
{
	AdjCost findAdjCost(int id);
	void insertAdjCost(AdjCost adjcost);
	void updateAdjCost(AdjCost adjcost);
	List<AdjCost> getAllAdjCosts();
	void deleteAdjCost(AdjCost adjcost);
	void saveOrUpdateAdjCost(AdjCost adjcost);
}