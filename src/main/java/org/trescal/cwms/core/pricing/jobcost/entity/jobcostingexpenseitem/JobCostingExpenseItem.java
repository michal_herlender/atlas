package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.servicecost.JobCostingServiceCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

import lombok.Setter;

@Entity
@Table(name = "jobcostingexpenseitem")
@Setter
public class JobCostingExpenseItem extends PricingItem implements Comparable<JobCostingExpenseItem> {

	private Integer id;
	private JobCostingServiceCost serviceCost;
	private JobCosting jobCosting;
	private JobExpenseItem jobExpenseItem;

	public JobCostingExpenseItem() {
		super();
	}

	public JobCostingExpenseItem(JobExpenseItem expenseItem, JobCosting jobCosting, Integer itemNo) {
		this();
		this.jobExpenseItem = expenseItem;
		this.setItemno(itemNo);
		this.setJobCosting(jobCosting);
		this.setQuantity(expenseItem.getQuantity());
		this.setTotalCost(expenseItem.getSinglePrice());
		this.setFinalCost(new BigDecimal("0.00"));
		this.setGeneralDiscountRate(new BigDecimal("0.00"));
		this.setGeneralDiscountValue(new BigDecimal("0.00"));
		this.setServiceCost(new JobCostingServiceCost(expenseItem.getTotalPrice()));
	}

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public Integer getId() {
		return id;
	}

	@Override
	@Transient
	public PricingItem getBaseUnit() {
		return null;
	}

	@Override
	@Transient
	public Set<Cost> getCosts() {
		Set<Cost> result = new HashSet<Cost>();
		result.add(serviceCost);
		return result;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "serviceCost", foreignKey = @ForeignKey(name = "FK_jobcostingexpenseitem_serviceCost"))
	public JobCostingServiceCost getServiceCost() {
		return serviceCost;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobCosting", foreignKey = @ForeignKey(name = "FK_jobcostingexpenseitem_jobCosting"), nullable = false)
	public JobCosting getJobCosting() {
		return jobCosting;
	}

	@Override
	@Transient
	public Set<? extends PricingItem> getModules() {
		return null;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobExpenseItem", foreignKey = @ForeignKey(name = "FK_jobcostingexpenseitem_jobExpenseItem"), nullable = false)
	public JobExpenseItem getJobExpenseItem() {
		return jobExpenseItem;
	}

	@Override
	public int compareTo(JobCostingExpenseItem other) {
		if (this.jobCosting.equals(other.jobCosting))
			return this.getItemno() - other.getItemno();
		else if (this.getJobCosting().getJob().equals(other.getJobCosting().getJob()))
			return this.getJobCosting().getVer() - other.getJobCosting().getVer();
		else
			return this.getJobCosting().getJob().getJobid() - this.getJobCosting().getJob().getJobid();
	}
}