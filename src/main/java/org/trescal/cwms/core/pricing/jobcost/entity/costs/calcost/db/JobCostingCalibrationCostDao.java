package org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.jobcost.dto.ModelJobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface JobCostingCalibrationCostDao extends JobCostingCostDaoSupport<JobCostingCalibrationCost> {
	
	JobCostingCalibrationCost findEagerJobCostingCalibrationCost(int id);
	
	List<ModelJobCostingCalibrationCost> findMostRecentCostForModels(Collection<Integer> modelIds, CalibrationType calType, Company company);
}