package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@RestController
@IntranetController
public class PurchaseOrderJsonController {

	@Autowired
	PurchaseOrderService poService;

	@Autowired
	JobService jobService;

	@RequestMapping(value = "/getPurchaseOrderByJobId.json", method = RequestMethod.GET)
	protected List<PurchaseOrderDTO> getPurchaseOrderByJob(@RequestParam(name = "jobId", required = true) Integer jobId,
														   Locale local) throws Exception {
		return this.poService.getDtosByJobid(jobId, local);
	}


	@GetMapping("/searchPoByJobIdAndNumber.json")
	public List<POSearchResultDto> searchPoByJobIdAndNumber(@RequestParam Integer jobId, @RequestParam String poNumber) {
		return jobService.searchMatchingJobPOs(jobId, poNumber).stream().map(POSearchResultDto::of).collect(Collectors.toList());
	}

	@Data
	@Builder
	@AllArgsConstructor
	private static class POSearchResultDto {
		private Integer poId;
		private String poNumber;
		private Boolean isBPO;

		public static POSearchResultDto of(AbstractPO po) {
			return POSearchResultDto.builder()
				.poId(po.getPoId())
				.poNumber(po.getPoNumber())
				.isBPO(po instanceof BPO)
				.build();
		}
	}
}
