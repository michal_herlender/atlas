package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.db;

import java.util.List;

import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed;

public interface JobCostingViewedService
{
	void deleteJobCostingViewed(JobCostingViewed jobcostingviewed);

	JobCostingViewed findJobCostingViewed(int id);

	List<JobCostingViewed> getAllJobCostingVieweds();

	void insertJobCostingViewed(JobCostingViewed jobcostingviewed);

	void updateJobCostingViewed(JobCostingViewed jobcostingviewed);
}