package org.trescal.cwms.core.pricing.lookup;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInput;

import java.time.LocalDate;
import java.util.*;

/**
 * Command object containing parameters for a pricing lookup
 * <p>
 * Created 2019-04-20 - GB
 */
public class PriceLookupRequirements {
	private final Integer businessCompanyId;
	private final Integer clientCompanyId;
    private List<Integer> includeQuotationIds;        // Optional quotation ids to match against first (can be null/empty)

    private boolean quotationItemSearch;            // Default = true : Whether to include quotation items in search
    private boolean catalogPriceSearch;                // Default = true : Whether to include catalog prices in search
    private boolean firstMatchSearch;                // Default = true : Best match search - otherwise all matches returned

    private Boolean quotationAccepted;                // Default = true  : include 'accepted' quotations only (nullable)
    private Boolean quotationIssued;                // Default = true  : include 'issued' quotations only (nullable)
    // Indexes of pricelookup inputs by
    private final List<PriceLookupInput> inputs;
    private LocalDate quotationRegDateAfter;                // Default = null : include quotations registered after certain date only (nullable)
    private final Map<MultiKey<Integer>, PriceLookupInput> inputsByInstrumentId;
    private final Map<MultiKey<Integer>, PriceLookupInput> inputsByInstrumentModelId;
    private final Map<MultiKey<Integer>, PriceLookupInput> inputsBySalesCategoryId;
    private LocalDate quotationExpiryDateAfter;            // Default = today : include quotations expiring after today only (nullable)

    /**
     * Default constructor for price lookups where defaults are for use on jobs / job items:
     *
     * 	quotationItemSearch = true;
     * 	catalogPriceSearch = based on @param catalogPriceSearch (normally from business settings);
	 * 	firstMatchSearch = true;
	 * 	quotationAccepted = true; 	(confirm this requirement)
	 *  quotationIssued = true;
	 *  quotationExpiryDateAfter = today
	 *  quotationRegDateAfter = null (no registration date) 
	 * 
	 */
	public PriceLookupRequirements(Integer businessCompanyId, Integer clientCompanyId, Boolean catalogPriceSearch) {
		if (businessCompanyId == null) throw new IllegalArgumentException("businessCompanyId may not be null");
		if (clientCompanyId == null) throw new IllegalArgumentException("clientCompanyId may not be null");
		if (catalogPriceSearch == null) throw new IllegalArgumentException("catalogPriceSearch may not be null");

		this.businessCompanyId = businessCompanyId;
		this.clientCompanyId = clientCompanyId;

		this.quotationItemSearch = true;
		this.catalogPriceSearch = catalogPriceSearch;
		this.firstMatchSearch = true;

		this.quotationAccepted = true;
		this.quotationIssued = true;
		this.quotationExpiryDateAfter = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		this.quotationRegDateAfter = null;

		this.includeQuotationIds = Collections.emptyList();
		this.inputs = new ArrayList<>();
		this.inputsByInstrumentId = new HashMap<>();
		this.inputsByInstrumentModelId = new HashMap<>();
		this.inputsBySalesCategoryId = new HashMap<>();
	}
	
	public void addInput(PriceLookupInput input) {
		this.inputs.add(input);
		if (input.getInstrumentId() != null) {
			registerInstrumentId(input);
		}
		if (input.getModelId() != null) {
			registerModelId(input);
		}
		if (input.getSalesCategoryId() != null) {
			registerSalesCategoryId(input);
		}
	}

	public List<PriceLookupInput> getInputs() {
		return inputs;
	}
	
	public void registerInstrumentId(PriceLookupInput input) {
		MultiKey<Integer> key = new MultiKey<>(input.getServiceTypeId(), input.getInstrumentId());
		this.inputsByInstrumentId.put(key, input);
	}
	
	public void registerModelId(PriceLookupInput input) {
		MultiKey<Integer> key = new MultiKey<>(input.getServiceTypeId(), input.getModelId());
		this.inputsByInstrumentModelId.put(key, input);
	}
	
	public void registerSalesCategoryId(PriceLookupInput input) {
		MultiKey<Integer> key = new MultiKey<>(input.getServiceTypeId(), input.getSalesCategoryId());
		this.inputsBySalesCategoryId.put(key, input);
	}
	
	public PriceLookupInput getInputByInstrumentId(Integer serviceTypeId, Integer instrumentId) {
		MultiKey<Integer> key = new MultiKey<>(serviceTypeId, instrumentId);
		return this.inputsByInstrumentId.get(key);
	}
	
	public PriceLookupInput getInputByModelId(Integer serviceTypeId, Integer modelId) {
		MultiKey<Integer> key = new MultiKey<>(serviceTypeId, modelId);
		return this.inputsByInstrumentModelId.get(key);
	}
	
	public PriceLookupInput getInputBySalesCategoryId(Integer serviceTypeId, Integer salesCategoryId) {
		MultiKey<Integer> key = new MultiKey<>(serviceTypeId, salesCategoryId);
		return this.inputsBySalesCategoryId.get(key);
	}
	
	public Collection<PriceLookupInput> getInputsWithInstrumentId() {
		return this.inputsByInstrumentId.values();
	}
	
	public Collection<PriceLookupInput> getInputsWithModelId() {
		return this.inputsByInstrumentId.values();
	}
	
	public Collection<PriceLookupInput> getInputsWithSalesCategoryId() {
		return this.inputsByInstrumentId.values();
	}
	
	/**
	 * convenience method to test whether requirements INCLUDE specific quotation ids
	 */
	public boolean isIncludeQuotationIds() {
		return (includeQuotationIds != null) && !includeQuotationIds.isEmpty();
	}

	public Integer getBusinessCompanyId() {
		return businessCompanyId;
	}

	public Integer getClientCompanyId() {
		return clientCompanyId;
	}

	public List<Integer> getIncludeQuotationIds() {
		return includeQuotationIds;
	}

	public boolean isQuotationItemSearch() {
		return quotationItemSearch;
	}

	public boolean isCatalogPriceSearch() {
		return catalogPriceSearch;
	}

	public boolean isFirstMatchSearch() {
		return firstMatchSearch;
    }

    public Boolean getQuotationAccepted() {
        return quotationAccepted;
    }

    public Boolean getQuotationIssued() {
        return quotationIssued;
    }

    public LocalDate getQuotationRegDateAfter() {
        return quotationRegDateAfter;
    }

    public LocalDate getQuotationExpiryDateAfter() {
        return quotationExpiryDateAfter;
    }

    public void setIncludeQuotationIds(List<Integer> includeQuotationIds) {
        if (includeQuotationIds == null) throw new IllegalArgumentException("includeQuotationIds was null!");
        this.includeQuotationIds = includeQuotationIds;
	}

	public void setQuotationItemSearch(boolean quotationItemSearch) {
		this.quotationItemSearch = quotationItemSearch;
	}

	public void setCatalogPriceSearch(boolean catalogPriceSearch) {
		this.catalogPriceSearch = catalogPriceSearch;
	}

	public void setFirstMatchSearch(boolean firstMatchSearch) {
		this.firstMatchSearch = firstMatchSearch;
	}

	public void setQuotationAccepted(Boolean quotationAccepted) {
		this.quotationAccepted = quotationAccepted;
    }

    public void setQuotationIssued(Boolean quotationIssued) {
        this.quotationIssued = quotationIssued;
    }

    public void setQuotationExpiryDateAfter(LocalDate quotationExpiryDateAfter) {
        this.quotationExpiryDateAfter = quotationExpiryDateAfter;
    }

    public void setQuotationRegDateAfter(LocalDate quotationRegDateAfter) {
        this.quotationRegDateAfter = quotationRegDateAfter;
    }
}
