/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.enums;

/**
 * Enum identifying the source of a set of third party costs.
 * 
 * @author Richard
 */
public enum ThirdCostSource
{
	MANUAL("Manually Set"), THIRD_PARTY_QUOTATION("Third Party Quotation");

	private String name;

	ThirdCostSource(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return this.name;
	}
}
