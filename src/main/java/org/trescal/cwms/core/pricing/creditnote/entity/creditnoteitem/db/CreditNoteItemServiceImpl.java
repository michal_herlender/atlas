package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.db;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.projection.items.CreditNoteItemProjectionDTO;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculationSystem;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;

@Service("CreditNoteItemService")
public class CreditNoteItemServiceImpl extends BaseServiceImpl<CreditNoteItem, Integer>
		implements CreditNoteItemService {

	@Autowired
	private CreditNoteItemDao creditNoteItemDao;
	@Autowired
	private CreditNoteService creditNoteService;
	@Autowired
	private NominalCodeService nominalCodeService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TaxCalculator taxCalculator;

	@Override
	public CreditNoteItem buildNewCreditNoteItem(CreditNote creditNote, String desc, NominalCode nominalCode,
			BigDecimal value, Subdiv businessSubdiv) {
		CreditNoteItem item = new CreditNoteItem();
		item.setQuantity(1);
		item.setTotalCost(value);
		item.setFinalCost(new BigDecimal("0.00"));
		item.setGeneralDiscountRate(new BigDecimal("0.00"));
		item.setGeneralDiscountValue(new BigDecimal("0.00"));
		int nextItemNo = 1;
		for (CreditNoteItem i : creditNote.getItems())
			if (i.getItemno() >= nextItemNo)
				nextItemNo = i.getItemno() + 1;
		item.setItemno(nextItemNo);
		item.setDescription(desc);
		item.setNominal(nominalCode);
		item.setCreditNote(creditNote);
		item.setBusinessSubdiv(businessSubdiv);
		CostCalculator.updateItemCost(item);
		return item;
	}

	@Override
	public CreditNote remove(int id) {
		return this.remove(this.get(id));
	}

	@Override
	public CreditNote remove(CreditNoteItem creditNoteItem) {
		// remove the item from it's invoice's collection of items
		CreditNote creditNote = creditNoteItem.getCreditNote();
		creditNote.getItems().remove(creditNoteItem);
		// update the total on the credit note
		CostCalculator.updatePricingTotalCost(creditNote, creditNote.getItems());
		taxCalculator.calculateTax(creditNote);
		CostCalculator.updatePricingFinalCost(creditNote, creditNote.getItems());
		creditNote = this.creditNoteService.merge(creditNote);
		return creditNote;
	}

	@Override
	protected BaseDao<CreditNoteItem, Integer> getBaseDao() {
		return creditNoteItemDao;
	}

	private void update(CreditNoteItem item, String description, Integer nominalId, BigDecimal amount,
			Integer businessSubdivId) {
		item.setDescription(description);
		if (nominalId != null) {
			NominalCode nominalCode = nominalCodeService.get(nominalId);
			item.setNominal(nominalCode);
		}
		item.setTotalCost(amount);
		if (businessSubdivId != null) {
			Subdiv businessSubdiv = subdivService.get(businessSubdivId);
			item.setBusinessSubdiv(businessSubdiv);
		}
		CostCalculator.updateItemCost(item);
	}

	@Override
	public CreditNoteItem create(Integer creditNoteId, String description, Integer nominalId, BigDecimal amount,
			Integer businessSubdivId) {
		CreditNote creditNote = creditNoteService.get(creditNoteId);
		CreditNoteItem item = new CreditNoteItem();
		item.setQuantity(1);
		item.setFinalCost(new BigDecimal("0.00"));
		item.setGeneralDiscountRate(new BigDecimal("0.00"));
		item.setGeneralDiscountValue(new BigDecimal("0.00"));
		item.setItemno(creditNote.getItems().stream().mapToInt(CreditNoteItem::getItemno).max().orElse(0) + 1);
		item.setCreditNote(creditNote);
		item.setShipToAddress(creditNote.getInvoice().getAddress());
		this.update(item, description, nominalId, amount, businessSubdivId);
		this.save(item);
		return item;
	}

	@Override
	public CreditNoteItem update(Integer itemId, String description, Integer nominalId, BigDecimal amount,
			Integer businessSubdivId) {
		CreditNoteItem item = this.get(itemId);
		this.update(item, description, nominalId, amount, businessSubdivId);
		return this.merge(item);
	}
	
	@Override
	public List<CreditNoteItemProjectionDTO> getCreditNoteItemProjectionDTOs(Integer creditNoteId) {
		if (creditNoteId == null) throw new IllegalArgumentException("creditNoteId must not be null");
		return this.creditNoteItemDao.getCreditNoteItemProjectionDTOs(creditNoteId);
	}
}