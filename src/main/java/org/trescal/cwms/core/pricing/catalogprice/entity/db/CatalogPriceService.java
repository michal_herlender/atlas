package org.trescal.cwms.core.pricing.catalogprice.entity.db;

import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.MultiValuedMap;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceDTO;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceValueAndCommentDTO;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputCatalogPrice;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupQueryType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

public interface CatalogPriceService extends BaseService<CatalogPrice, Integer> {

	/**
	 * Search performed with serviceType
	 */
	CatalogPrice findSingle(InstrumentModel model, ServiceType serviceType, CostType costType, Company businessCompany);
	
	/*
	 * Returns a specific Calibration catalog price (or null if none found) for the job item.
	 */
	CatalogPrice findCalCatalogPrice(JobItem jobItem);

	/*
	 * Returns a specific Calibration catalog price (or null if none found) for the job item.
	 * 
	 * eager = eagerly loads Instrument Model, Service Type and Company
	 */
	CatalogPrice findCalCatalogPriceEager(JobItem jobItem);

	/*
	 * Returns all the catalog prices matching the criteria (supports null parameters)
	 */
	List<CatalogPrice> find(InstrumentModel model, ServiceType serviceType, CostType costType, Company businessCompany);
	
	/*
	 * Returns all the catalog prices matching the criteria (id of 0 is skipped)
	 */
	List<CatalogPrice> find(int modelId, int serviceTypeId, CostType costType, int businessCompanyId, boolean eager);
	
	List<CatalogPrice> findAll(ServiceType serviceType, List<InstrumentModel> models, Company businessCompany);
	
	List<PriceLookupOutputCatalogPrice> findCatalogPrices(PriceLookupRequirements reqs, PriceLookupQueryType queryType, MultiValuedMap<Integer, Integer> serviceTypeMap);
	
	/*
	* Checks for uniqueness
	 */
	Boolean exists(InstrumentModel instrumentModel, CostType costType, ServiceType serviceType, Company businessCompany);
	
	CatalogPriceValueAndCommentDTO ajaxFind(Integer modelId, Integer calibrationTypeId, Integer subdivId);
	
	CatalogPriceValueAndCommentDTO ajaxGetPriceInGivenCurrency(Integer modelId, Integer calibrationTypeId, Integer companyId,String currencyCode);
	
	List<CatalogPriceDTO> getCatalogPricesFromSalesCategory(Integer salesCategoryId, Locale locale);
	
	List<CatalogPriceDTO> findPrices(Integer instrumentModelId, Locale locale);
}