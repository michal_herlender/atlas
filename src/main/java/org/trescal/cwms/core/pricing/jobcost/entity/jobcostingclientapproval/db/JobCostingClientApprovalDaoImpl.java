package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.JobCostingClientApproval;

@Repository("JobCostingClientApprovalDao")
public class JobCostingClientApprovalDaoImpl extends BaseDaoImpl<JobCostingClientApproval, Integer> implements JobCostingClientApprovalDao
{
	
	@Override
	protected Class<JobCostingClientApproval> getEntity() {
		return JobCostingClientApproval.class;
	}
	
}