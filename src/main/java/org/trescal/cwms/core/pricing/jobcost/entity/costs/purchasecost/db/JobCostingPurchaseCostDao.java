package org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.db;

import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;

public interface JobCostingPurchaseCostDao extends JobCostingCostDaoSupport<JobCostingPurchaseCost> {}