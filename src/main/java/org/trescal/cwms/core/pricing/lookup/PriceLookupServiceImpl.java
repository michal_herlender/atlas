package org.trescal.cwms.core.pricing.lookup;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.lookup.dto.*;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;

import javax.persistence.Tuple;
import java.util.Collection;
import java.util.List;

/**
 * Service to aid in efficiently looking up prices for multiple instruments / instrument models
 * at once using a small number of queries.
 *
 * Targeted for use in job item creation, alternative cost overlay, and quotation item creation
 *
 * History
 *
 * 2019-04-30 Galen Beck - Created service and related classes for calibration pricing only, from 
 *   quotation item and catalog price entry sources.  
 *
 */
@Slf4j
@Service
public class PriceLookupServiceImpl implements PriceLookupService {

	@Autowired
	private CatalogPriceService catalogPriceService;
	@Autowired
	private InstrumentModelService instrumentModelService; 
	@Autowired
	private QuotationItemService quotationItemService;
	@Autowired
	private SalesCategoryService salesCategoryService;

	@Override
	public void findPrices(PriceLookupRequirements reqs) {
		if (reqs.isQuotationItemSearch()) {
			// Search instruments
			MultiValuedMap<Integer, Integer> instrumentMap = getInstrumentSearchMap(reqs);
			searchQuotationItemInstruments(reqs, instrumentMap);
			// Search instrument models
			searchRequiredInstrumentModelIds(reqs);			
			MultiValuedMap<Integer, Integer> instrumentModelMap = getInstrumentModelSearchMap(reqs);
			searchQuotationItemModels(reqs, instrumentModelMap);
			// Search sales categories
			searchRequiredSalesCategoryIds(reqs);
			MultiValuedMap<Integer, Integer> salesCategoryMap = getSalesCategorySearchMap(reqs);
			searchQuotationItemSalesCategories(reqs, salesCategoryMap);
		}
		if (reqs.isCatalogPriceSearch()) {
			// Search catalog prices for instrument models
			// If performing just a catalog search, may need to correlate instrument ids to model ids first 
			searchRequiredInstrumentModelIds(reqs);
			MultiValuedMap<Integer, Integer> instrumentModelMap = getInstrumentModelSearchMap(reqs);
			searchCatalogPriceModels(reqs, instrumentModelMap);
			// Search catalog prices for sales categories
			// If performing just a catalog search, may need to correlate model ids to sales category ids first
			searchRequiredSalesCategoryIds(reqs);
			MultiValuedMap<Integer, Integer> salesCategoryMap = getSalesCategorySearchMap(reqs);
			searchCatalogPriceSalesCategories(reqs, salesCategoryMap);
		}
	}
	
	/**
	 * Analyzes search inputs and determines instruments to search for:
	 * - MultiValuedMap of service type ids to Set of instrument ids that need pricing
	 * Excludes inputs that already have search results, if doing a "first match" search
	 */
	private MultiValuedMap<Integer, Integer> getInstrumentSearchMap(PriceLookupRequirements reqs) {
		MultiValuedMap<Integer, Integer> resultMap = new HashSetValuedHashMap<>();
		for (PriceLookupInput input : reqs.getInputs()) {
			if (input.getInstrumentId() != null && (!input.hasOutputs() || !reqs.isFirstMatchSearch())) {
				resultMap.put(input.getServiceTypeId(), input.getInstrumentId());
			}
		}
		return resultMap;
	}
	/**
	 * Analyzes search inputs and determines instrument models to search for:
	 * - MultiValuedMap of service type ids to Set of instrument model ids that need pricing
	 * Excludes inputs that already have search results, if doing a "first match" search
	 * 
	 * @return MultiValuedMap<Integer, Integer> of serviceTypeId and Set of instrumentModelId for each serviceTypeId
	 */
	private MultiValuedMap<Integer, Integer> getInstrumentModelSearchMap(PriceLookupRequirements reqs) {
		MultiValuedMap<Integer, Integer> resultMap = new HashSetValuedHashMap<>();
		for (PriceLookupInput input : reqs.getInputs()) {
			if (input.getModelId() != null && (!input.hasOutputs() || !reqs.isFirstMatchSearch())) {
				resultMap.put(input.getServiceTypeId(), input.getModelId());
			}
		}
		return resultMap;
	}

	/**
	 * Analyzes search inputs and determines sales categories required to search for:
	 * - MultiValuedMap of service type ids to Set of sales category ids 
	 * Excludes inputs that already have search results, if doing a "first match" search
	 * 
	 * @return MultiValuedMap<Integer, Integer> of serviceTypeId and Set of salesCategoryId for each serviceTypeId
	 */
	private MultiValuedMap<Integer, Integer> getSalesCategorySearchMap(PriceLookupRequirements reqs) {
		MultiValuedMap<Integer, Integer> resultMap = new HashSetValuedHashMap<>();
		for (PriceLookupInput input : reqs.getInputs()) {
			if (input.getSalesCategoryId() != null && (!input.hasOutputs() || !reqs.isFirstMatchSearch())) {
				resultMap.put(input.getServiceTypeId(), input.getSalesCategoryId());
			}
		}
		return resultMap;
	}
	
	private void searchCatalogPriceModels(PriceLookupRequirements reqs, MultiValuedMap<Integer, Integer> instrumentModelMap) {
		if (!instrumentModelMap.isEmpty()) {
			List<PriceLookupOutputCatalogPrice> outputs = this.catalogPriceService.findCatalogPrices(reqs,
				PriceLookupQueryType.CATALOG_PRICE_INSTRUMENT_MODEL, instrumentModelMap);
			correlateInstrumentModelOutputs(reqs, outputs);
			log.debug("searchCatalogPriceModels() searched for " + instrumentModelMap.size() + " combinations, found " + outputs.size());
		}
		else {
			log.debug("searchCatalogPriceModels() skipped, no instrument models requiring search");
		}
	}
	
	private void searchCatalogPriceSalesCategories(PriceLookupRequirements reqs, MultiValuedMap<Integer, Integer> salesCategoryMap) {
		if (!salesCategoryMap.isEmpty()) {
			List<PriceLookupOutputCatalogPrice> outputs = this.catalogPriceService.findCatalogPrices(reqs,
				PriceLookupQueryType.CATALOG_PRICE_SALES_CATEGORY_MODEL, salesCategoryMap);
			correlateSalesCategoryOutputs(reqs, outputs);
			log.debug("searchCatalogPriceSalesCategories() searched for " + salesCategoryMap.size() + " combinations, found " + outputs.size());
		}
		else {
			log.debug("searchCatalogPriceSalesCategories() skipped, no sales categories requiring search");
		}
	}

	/**
	 * Performs search for "instrument" quotation items for each service type id and
	 * correlates results to inputs
	 */
	private void searchQuotationItemInstruments(PriceLookupRequirements reqs, 
			MultiValuedMap<Integer, Integer> instrumentMap) {
		if (!instrumentMap.isEmpty()) {
			List<PriceLookupOutputQuotationItem> outputs = this.quotationItemService.findQuotationItems(reqs,
				PriceLookupQueryType.QUOTATION_ITEM_INSTRUMENT, instrumentMap);
			updatePreferredQuotationFlag(reqs, outputs);
			correlateInstrumentOutputs(reqs, outputs);
			log.debug("searchQuotationItemInstruments() searched for " + instrumentMap.size() + " combinations, found " + outputs.size());
		} else {
			log.debug("searchQuotationItemInstruments() skipped, no instruments requiring search");
		}
	}
	
	
	private void searchQuotationItemModels(PriceLookupRequirements reqs, MultiValuedMap<Integer, Integer> instrumentModelMap) {
		if (!instrumentModelMap.isEmpty()) {
			List<PriceLookupOutputQuotationItem> outputs = this.quotationItemService.findQuotationItems(reqs,
				PriceLookupQueryType.QUOTATION_ITEM_INSTRUMENT_MODEL, instrumentModelMap);
			updatePreferredQuotationFlag(reqs, outputs);
			correlateInstrumentModelOutputs(reqs, outputs);
			log.debug("searchQuotationItemModels() searched for " + instrumentModelMap.size() + " combinations, found " + outputs.size());
		} else {
			log.debug("searchQuotationItemModels() skipped, no instrument models requiring search");
		}
	}

	private void searchQuotationItemSalesCategories(PriceLookupRequirements reqs, MultiValuedMap<Integer, Integer> salesCategoryMap) {
		if (!salesCategoryMap.isEmpty()) {
			List<PriceLookupOutputQuotationItem> outputs = this.quotationItemService.findQuotationItems(reqs,
				PriceLookupQueryType.QUOTATION_ITEM_SALES_CATEGORY_MODEL, salesCategoryMap);
			updatePreferredQuotationFlag(reqs, outputs);
			correlateSalesCategoryOutputs(reqs, outputs);
			log.debug("searchQuotationItemSalesCategories() searched for " + salesCategoryMap.size() + " combinations, found " + outputs.size());
		} else {
			log.debug("searchQuotationItemSalesCategories() skipped, no sales categories requiring search");
		}
	}
	
	/**
	 * Queries instrument model ids, for those instruments with unknown model ids, 
	 * where it is also desired to search for further pricing.
	 * 
	 * If only finding best matches, and an instrument already has a match, its model id is not needed.
	 * 
	 * This could be done as a join (to a subquery) in one combined quotation item query for both 
	 * instruments and models, but we won't always need to run it - and we need to correlate back to the 
	 * specific instruments, anyway.
	 */
	private void searchRequiredInstrumentModelIds(PriceLookupRequirements reqs) {
		// Map of instrument ids (needing model ids) to PriceLookupInputs for those instruments
		MultiValuedMap<Integer, PriceLookupInput> instrumentMap = new HashSetValuedHashMap<>();
		for (PriceLookupInput input : reqs.getInputsWithInstrumentId()) {
			if (input.getModelId() == null && !reqs.isFirstMatchSearch() || !input.hasOutputs()) {
				instrumentMap.put(input.getInstrumentId(), input);
			}
		}
		if (!instrumentMap.isEmpty()) {
			List<Tuple> results = this.instrumentModelService.getModelIdsForInstruments(instrumentMap.keySet());
			for (Tuple tuple : results) {
				Integer instrumentId = tuple.get(0, Integer.class);
				Integer modelId = tuple.get(1, Integer.class);
				Collection<PriceLookupInput> inputs = instrumentMap.get(instrumentId);
				for (PriceLookupInput input : inputs) {
					input.setModelId(modelId);
					reqs.registerModelId(input);
				}
			}
			log.debug("searchRequiredInstrumentModelIds() searched for " + instrumentMap.keySet().size() + " distinct instruments, found " + results.size());
		} else {
			log.debug("searchRequiredInstrumentModelIds() skipped, no instruments requiring search");
		}
	}

	/**
	 * Queries sales category ids, for those instruments / instrument models with unknown sales category ids,
	 * if we want to search for further pricing.
	 * 
	 * Instruments needing pricing should already have their instrument model correlated, as 
	 * searchRequiredInstrumentModelIds() will have already been run.
	 * 
	 * However we need to correlate sales category results back to both instrument and instrument model lookups. 
	 * 
	 * If only finding best matches, and an input already has a match, its sales category id is not needed.
	 * 
	 */
	private void searchRequiredSalesCategoryIds(PriceLookupRequirements reqs) {
		// Map of instrument model ids (needing sales category ids) to PriceLookupInputs for those instrument model ids
		MultiValuedMap<Integer, PriceLookupInput> modelMap = new HashSetValuedHashMap<>();
		// Scan all inputs (covers both instrument and instrument model inputs) to determine needed sales categories
		for (PriceLookupInput input : reqs.getInputs()) {
			if (input.getModelId() != null && input.getSalesCategoryId() == null && 
					(!reqs.isFirstMatchSearch() || !input.hasOutputs())) {
				modelMap.put(input.getModelId(), input);
			}
		}
		if (!modelMap.isEmpty()) {
			List<Tuple> results = this.salesCategoryService.getSalesCategoryIdsForModelIds(modelMap.keySet());
			for (Tuple tuple : results) {
				Integer modelId = tuple.get(0, Integer.class);
				Integer salesCategoryId = tuple.get(1, Integer.class);
				Collection<PriceLookupInput> inputs = modelMap.get(modelId);
				for (PriceLookupInput input : inputs) {
					input.setSalesCategoryId(salesCategoryId);
					reqs.registerSalesCategoryId(input);
				}
			}
			log.debug("searchRequiredSalesCategoryIds() searched for " + modelMap.keySet().size() + " distinct instrument models, found " + results.size());
		} else {
			log.debug("searchRequiredSalesCategoryIds() skipped, no sales categoriues requiring search");
		}
	}

	/**
	 * We look up all quotations at once, so we need to flag preferred quotations in results after query
	 * 
	 */
	private void updatePreferredQuotationFlag(PriceLookupRequirements reqs, List<PriceLookupOutputQuotationItem> outputs) {
		if (reqs.isIncludeQuotationIds()) {
			for (PriceLookupOutput output : outputs) {
				if (output.getQuotationId() != null && reqs.getIncludeQuotationIds().contains(output.getQuotationId())) {
					output.setPreferredQuotation(true);
				}
			}
		}
	}
	
	/**
	 * Connects outputs containing instrument id back to their inputs
	 * Note that the inputs/outputs could have the instrument id multiple times, e.g. for different multiple service type ids
	 */
	private void correlateInstrumentOutputs(PriceLookupRequirements reqs, List<? extends PriceLookupOutput> outputs) {
		for (PriceLookupOutput output : outputs) {
			PriceLookupInput input = reqs.getInputByInstrumentId(output.getServiceTypeId(), output.getInstrumentId());
			input.addOutput(output);
		}
	}

	/**
	 * Connects outputs containing instrument model id back to their inputs
	 */
	private void correlateInstrumentModelOutputs(PriceLookupRequirements reqs, List<? extends PriceLookupOutput> outputs) {
		for (PriceLookupOutput output : outputs) {
			PriceLookupInput input = reqs.getInputByModelId(output.getServiceTypeId(), output.getInstrumentModelId());
			input.addOutput(output);
		}
	}
	
	/**
	 * Connects outputs containing sales category id back to their inputs
	 */
	private void correlateSalesCategoryOutputs(PriceLookupRequirements reqs, List<? extends PriceLookupOutput> outputs) {
		for (PriceLookupOutput output : outputs) {
			PriceLookupInput input = reqs.getInputBySalesCategoryId(output.getServiceTypeId(), output.getSalesCategoryId());
			input.addOutput(output);
		}
	}
	

}
