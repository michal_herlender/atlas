package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.PurchaseOrderAction;

@Repository("PurchaseOrderActionDao")
public class PurchaseOrderActionDaoImpl extends BaseDaoImpl<PurchaseOrderAction, Integer> implements PurchaseOrderActionDao {
	
	@Override
	protected Class<PurchaseOrderAction> getEntity() {
		return PurchaseOrderAction.class;
	}
}