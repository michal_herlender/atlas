package org.trescal.cwms.core.pricing.entity.costtype.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.spring.model.KeyValue;

public interface CostTypeService
{
	List<CostType> getAllActiveCostTypes();
	List<CostType> getRemainingCostTypes(Collection<CostType> excludedCostTypes);
	List<KeyValue<Integer, String>> getAllActiveCostTypeDtos();
	List<KeyValue<Integer, String>> getRemainingCostTypeDtos(Collection<CostType> excludedCostTypes);
}