package org.trescal.cwms.core.pricing.purchaseorder.projection;

import lombok.Getter;

import lombok.Setter;

@Getter @Setter
public class PurchaseOrderProjectionDTO {
	private Integer purchaseOrderId;
	private String purchaseOrderNumber;

	public PurchaseOrderProjectionDTO(Integer purchaseOrderId, String purchaseOrderNumber) {
		super();
		this.purchaseOrderId = purchaseOrderId;
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
}
