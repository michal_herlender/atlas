package org.trescal.cwms.core.pricing.purchaseorder.form;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * Generic {@link PurchaseOrder} validator used for adding as well as editing
 * {@link PurchaseOrder} entities.
 * 
 * @author Richard
 */
@Component
public class PurchaseOrderFormValidator extends AbstractBeanValidator {
	private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderFormValidator.class);
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(PurchaseOrderForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		PurchaseOrderForm form = (PurchaseOrderForm) target;
		logger.debug("Validate");
		logger.debug("form.getPersonid() = {}" ,form.getPersonid());
		logger.debug("form.getTaxableOption() = {}",form.getTaxableOption());
		logger.debug("form.getBusinessPersonid() = {}" ,form.getBusinessPersonid());
		super.validate(form, errors);
		/*
		 * Keeping validation code here instead of using JPA as these messages are also used in DWR/ajax calls
		 * and didn't want to have duplicate messages for JPA and message based validation.  GB 2016-05-25
		 */
		if ((form.getPersonid() == null) || (form.getPersonid() == 0)) {
			errors.rejectValue("personid", "error.purchaseorder.create.contact", null, "A contact must be specified");
		}

		if ((form.getBusinessPersonid() == null) || (form.getBusinessPersonid() == 0))
		{
			errors.rejectValue("businessPersonid", "error.purchaseorder.create.contact", null, "A contact must be specified");
		}
		if ((form.getAddrid() == null) || (form.getAddrid() == 0)) 
		{
			errors.rejectValue("addrid", "error.purchaseorder.create.address", null, "An address must be specified");
		}
		if ((form.getReturnAddressId() == null) || (form.getReturnAddressId() == 0)) 
		{
			errors.rejectValue("returnAddressId", "error.purchaseorder.create.address", null, "An address must be specified");
		}
		if ((form.getCurrencyCode() == null)
				|| form.getCurrencyCode().trim().equals(""))
		{
			errors.rejectValue("currencyCode", "error.purchaseorder.create.currency", null, "A currency must be specified");
		}
	}
}