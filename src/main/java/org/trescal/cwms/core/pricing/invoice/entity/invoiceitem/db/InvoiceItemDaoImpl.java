package org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrument.db.DuplicationSearchForCriteriaBuilder;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPOInvoicingDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPoLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO_;
import org.trescal.cwms.core.pricing.invoice.projection.invoiceitem.InvoiceItemProjectionDTO;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputInvoiceItem;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.List;

@Repository("InvoiceItemDao")
public class InvoiceItemDaoImpl extends BaseDaoImpl<InvoiceItem, Integer> implements InvoiceItemDao {

	@Override
	protected Class<InvoiceItem> getEntity() {
		return InvoiceItem.class;
	}

	@Override
	public List<InvoiceItemPoLinkDTO> getPoLinks(Integer invoiceId, Integer jobId) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceItemPoLinkDTO> cq = cb.createQuery(InvoiceItemPoLinkDTO.class);
			Root<InvoiceItem> item = cq.from(InvoiceItem.class);
			Join<InvoiceItem, InvoiceItemPO> poLinks = item.join(InvoiceItem_.invItemPOs, JoinType.LEFT);
			Join<InvoiceItemPO, PO> jobPo = poLinks.join(InvoiceItemPO_.jobPO, JoinType.LEFT);
			Join<InvoiceItemPO, BPO> bpo = poLinks.join(InvoiceItemPO_.bpo, JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(item.get(InvoiceItem_.invoice), invoiceId));
			if (jobId != null) {
				Join<InvoiceItem, JobItem> jobItem = item.join(InvoiceItem_.jobItem);
				clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.job), jobId));
			}
			cq.where(clauses);
			cq.orderBy(cb.asc(item.get(InvoiceItem_.itemno)));
			cq.select(cb.construct(InvoiceItemPoLinkDTO.class, item.get(InvoiceItem_.id), item.get(InvoiceItem_.itemno),
					poLinks.get(InvoiceItemPO_.id), cb.literal(invoiceId), cb.literal(jobId), jobPo.get(PO_.poId),
					jobPo.get(PO_.poNumber), bpo.get(BPO_.poId), bpo.get(BPO_.poNumber)));
			return cq;
		});
	}

	@Override
	public List<InvoiceItemPoLinkDTO> getPoLinks(Collection<Integer> invoiceIds) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceItemPoLinkDTO> cq = cb.createQuery(InvoiceItemPoLinkDTO.class);
			Root<InvoiceItem> item = cq.from(InvoiceItem.class);
			Join<InvoiceItem, InvoiceItemPO> poLinks = item.join(InvoiceItem_.invItemPOs, JoinType.LEFT);
			Join<InvoiceItemPO, PO> jobPo = poLinks.join(InvoiceItemPO_.jobPO, JoinType.LEFT);
			Join<InvoiceItemPO, BPO> bpo = poLinks.join(InvoiceItemPO_.bpo, JoinType.LEFT);
			Join<InvoiceItemPO, InvoicePO> invoicePO = poLinks.join(InvoiceItemPO_.invPO, JoinType.LEFT);

			cq.where(item.get(InvoiceItem_.invoice).in(invoiceIds));
			cq.distinct(true);
			cq.select(cb.construct(InvoiceItemPoLinkDTO.class, item.get(InvoiceItem_.invoice), jobPo.get(PO_.poId),
					jobPo.get(PO_.poNumber), bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), invoicePO.get(InvoicePO_.poId),
					invoicePO.get(InvoicePO_.poNumber)));

			return cq;
		});
	}

	@SuppressWarnings("unchecked")
	public List<InvoiceItem> getAllEagerInvoiceItemsOnInvoice(int invoiceid, Integer jobid) {
		Criteria crit = getSession().createCriteria(InvoiceItem.class);
		crit.createCriteria("invoice").add(Restrictions.idEq(invoiceid));
		if (jobid != null)
			crit.createCriteria("jobItem").createCriteria("job").add(Restrictions.idEq(jobid));
		crit.setFetchMode("invItemPOs", FetchMode.JOIN);
		crit.setFetchMode("invItemPOs.bpo", FetchMode.JOIN);
		crit.setFetchMode("invItemPOs.jobPO", FetchMode.JOIN);
		crit.setFetchMode("invItemPOs.invPO", FetchMode.JOIN);
		crit.setFetchMode("jobItem", FetchMode.JOIN);
		crit.setFetchMode("jobItem.job", FetchMode.JOIN);
		crit.setFetchMode("jobItem.inst", FetchMode.JOIN);
		crit.setFetchMode("jobItem.itemPOs", FetchMode.JOIN);
		crit.setFetchMode("jobItem.itemPOs.po", FetchMode.JOIN);
		crit.setFetchMode("jobItem.itemPOs.bpo", FetchMode.JOIN);
		crit.addOrder(Order.asc("itemno"));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return crit.list();
	}

	@Override
	public Integer findMaxItemNoOnInvoice(Integer invoiceId) {
		return getFirstResult(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<InvoiceItem> invoiceItem = cq.from(InvoiceItem.class);
			cq.where(cb.equal(invoiceItem.get(InvoiceItem_.invoice), invoiceId));
			cq.select(cb.max(invoiceItem.get(InvoiceItem_.itemno)));
			return cq;
		}).orElse(0);
	}

	@Override
	public List<InvoiceItem> getAllByInvoiceAndJob(Integer invoiceId, Integer jobId) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceItem> cq = cb.createQuery(InvoiceItem.class);
			Root<InvoiceItem> item = cq.from(InvoiceItem.class);
			Join<InvoiceItem, Invoice> invoice = item.join(InvoiceItem_.invoice);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(invoice.get(Invoice_.id), invoiceId));
			if (jobId != null) {
				Join<InvoiceItem, JobItem> jobItem = item.join(InvoiceItem_.jobItem, JoinType.INNER);
				Join<JobItem, Job> job = jobItem.join(JobItem_.job);
				clauses.getExpressions().add(cb.equal(job.get(Job_.jobid), jobId));
			}
			cq.where(clauses);
			cq.distinct(true);
			return cq;
		});
	}
	
	@Override
	public List<PriceLookupOutputInvoiceItem> findInvoiceItems(Integer plantid, Integer servicetypeid, Integer coid) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<PriceLookupOutputInvoiceItem> cq = cb.createQuery(PriceLookupOutputInvoiceItem.class);
		Root<InvoiceItem> item = cq.from(InvoiceItem.class);
		Join<InvoiceItem, Invoice> invoice = item.join(InvoiceItem_.invoice);
		Join<Invoice, SupportedCurrency> supportedCurrency = invoice.join(Invoice_.currency);
		Join<InvoiceItem, JobItem> jobitem = item.join(InvoiceItem_.jobItem);
		Join<JobItem, Instrument> instrument = jobitem.join(JobItem_.inst);
		Join<Instrument, ServiceType> servicetype = instrument.join(Instrument_.defaultServiceType, JoinType.LEFT);
		Join<Invoice, Company> company = invoice.join(Invoice_.comp);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.isTrue(invoice.get(Invoice_.issued)));
		clauses.getExpressions().add(cb.equal(company.get(Company_.coid), coid));
		clauses.getExpressions().add(cb.equal(servicetype.get(ServiceType_.serviceTypeId), servicetypeid));
		clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.plantid), plantid));
		cq.where(clauses);
		cq.select(cb.construct(PriceLookupOutputInvoiceItem.class,
				item.get(InvoiceItem_.id), 
				invoice.get(Invoice_.id),
				invoice.get(Invoice_.invno), 
				item.get(InvoiceItem_.itemno),
				invoice.get(Invoice_.issuedate),
				item.get(InvoiceItem_.generalDiscountRate),
				supportedCurrency.get(SupportedCurrency_.currencyERSymbol),
				item.get(InvoiceItem_.finalCost)));
		cq.orderBy(cb.desc(invoice.get(Invoice_.issuedate)));
		TypedQuery<PriceLookupOutputInvoiceItem> query = getEntityManager().createQuery(cq);
		query.setMaxResults(10);
		return query.getResultList();
	}
	
	@Override
	public List<InvoiceItemProjectionDTO> getInvoiceItemProjectionDTOs(Integer invoiceId) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceItemProjectionDTO> cq = cb.createQuery(InvoiceItemProjectionDTO.class);
			Root<InvoiceItem> root = cq.from(InvoiceItem.class);
			Join<InvoiceItem, Invoice> invoice = root.join(InvoiceItem_.invoice, JoinType.INNER);
			Join<InvoiceItem, NominalCode> nominal = root.join(InvoiceItem_.nominal, JoinType.LEFT);
			Join<InvoiceItem, JobItem> jobItem = root.join(InvoiceItem_.jobItem, JoinType.LEFT);
			Join<InvoiceItem, JobExpenseItem> expenseItem = root.join(InvoiceItem_.expenseItem, JoinType.LEFT);
			Join<JobExpenseItem, Job> expenseItemJob = expenseItem.join(JobExpenseItem_.job, JoinType.LEFT);
			Join<JobExpenseItem, InstrumentModel> instrumentModel = expenseItem.join(JobExpenseItem_.model, JoinType.LEFT);

			cq.where(cb.equal(invoice.get(Invoice_.id), invoiceId));
			cq.select(cb.construct(InvoiceItemProjectionDTO.class,
					root.get(InvoiceItem_.id),
					root.get(InvoiceItem_.itemno),
					root.get(InvoiceItem_.description),
					root.get(InvoiceItem_.quantity),
					root.get(InvoiceItem_.finalCost),
					root.get(InvoiceItem_.taxable),
					root.get(InvoiceItem_.taxAmount),
					nominal.get(NominalCode_.id),
					nominal.get(NominalCode_.costType),
					jobItem.get(JobItem_.jobItemId),
					expenseItem.get(JobExpenseItem_.id),
					expenseItem.get(JobExpenseItem_.itemNo),
					expenseItemJob.get(Job_.jobid),
					instrumentModel.get(InstrumentModel_.modelid),
					expenseItem.get(JobExpenseItem_.clientRef)
			));
			cq.orderBy(cb.asc(root.get(InvoiceItem_.id)));

			return cq;
		});
	}

	@Override
	public List<InvoiceItemPOInvoicingDTO> getInvoiceItemsForInvoice(Integer invoiceId, int poId) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceItemPOInvoicingDTO> cq = cb.createQuery(InvoiceItemPOInvoicingDTO.class);
			Root<InvoiceItem> root = cq.from(InvoiceItem.class);
			Join<InvoiceItem, Invoice> invoice = root.join(InvoiceItem_.invoice, JoinType.INNER);
			Join<InvoiceItem, JobItem> jobItem = root.join(InvoiceItem_.jobItem, JoinType.LEFT);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst, JoinType.LEFT);
			Join<InvoiceItem, JobExpenseItem> jobExpenseitem = root.join(InvoiceItem_.expenseItem, JoinType.LEFT);

			Join<InvoiceItem, InvoicePOItem> iiPo = root.join(InvoiceItem_.invPOsItem, JoinType.LEFT);
			iiPo.on(cb.equal(iiPo.get(InvoicePOItem_.invPO), poId));

			cq.where(cb.equal(invoice.get(Invoice_.id), invoiceId));
			cq.select(cb.construct(InvoiceItemPOInvoicingDTO.class,
					root.get(InvoiceItem_.id),
					root.get(InvoiceItem_.itemno),
					jobItem.get(JobItem_.itemNo),
					jobExpenseitem.get(JobExpenseItem_.itemNo),
					instrument.get(Instrument_.plantno),
					instrument.get(Instrument_.serialno),
					root.get(InvoiceItem_.description),
					DuplicationSearchForCriteriaBuilder.generateDescription(instrument, instrument.join(Instrument_.model, JoinType.LEFT)).apply(cb),
					jobItem.join(JobItem_.job, JoinType.LEFT).get(Job_.jobno),
					jobExpenseitem.join(JobExpenseItem_.job, JoinType.LEFT).get(Job_.jobno),
					joinTranslation(cb, jobExpenseitem.join(JobExpenseItem_.model, JoinType.LEFT), InstrumentModel_.nameTranslations, LocaleContextHolder.getLocale()),
					cb.selectCase()
							.when(cb.isNotNull(iiPo), 1)
							.otherwise(0).as(Boolean.class)));

			cq.orderBy(cb.asc(root.get(InvoiceItem_.id)));
			return cq;
		});
	}

	@Override
	public List<InvoiceItem> findByInvoiceIdAndNotByPOIdAndSelectedIds(Integer invoiceId, Integer poId, List<Integer> itemIds) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceItem> cq = cb.createQuery(InvoiceItem.class);
			Root<InvoiceItem> invoiceItem = cq.from(InvoiceItem.class);
			Join<InvoiceItem, InvoicePOItem> invoicePoItem = invoiceItem.join(InvoiceItem_.invPOsItem, JoinType.LEFT);
			invoicePoItem.on(cb.equal(invoicePoItem.get(InvoicePOItem_.invPO), poId));
			cq.where(cb.equal(invoiceItem.get(InvoiceItem_.invoice), invoiceId), cb.isNull(invoicePoItem),
					invoiceItem.in(itemIds)
			);
			return cq;

		});
	}

	@Override
	public List<InvoicePOItem> findItemsByInvoiceIdAndInInvPoAndNotSelected(Integer invoiceId, Integer poId, List<Integer> itemIds) {
		return getResultList(cb -> {
			CriteriaQuery<InvoicePOItem> cq = cb.createQuery(InvoicePOItem.class);
			Root<InvoicePOItem> invoicePOItem = cq.from(InvoicePOItem.class);
			Join<InvoicePOItem, InvoiceItem> invoiceItem = invoicePOItem.join(InvoicePOItem_.invItem);

			cq.where(cb.equal(invoiceItem.get(InvoiceItem_.invoice), invoiceId),
					cb.equal(invoicePOItem.get(InvoicePOItem_.invPO), poId),
					itemIds == null || itemIds.isEmpty() ? cb.and() : cb.not(invoiceItem.in(itemIds))
			);
			return cq;

		});
	}
}