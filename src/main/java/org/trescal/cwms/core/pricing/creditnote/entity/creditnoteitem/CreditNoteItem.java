package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem;

import java.util.Collections;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.TaxablePricingItem;

import lombok.Setter;

@Entity
@Table(name = "creditnoteitem", uniqueConstraints = { @UniqueConstraint(columnNames = { "itemno", "creditnoteid" }) })
@Setter
public class CreditNoteItem extends TaxablePricingItem {

	private Integer id;
	private CreditNote creditNote;
	private String description;
	private NominalCode nominal;
	private Subdiv businessSubdiv;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public Integer getId() {
		return id;
	}

	@Override
	@Transient
	public Set<Cost> getCosts() {
		return Collections.emptySet();
	}

	@Override
	@Transient
	public PricingItem getBaseUnit() {
		return null;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creditnoteid", nullable = false)
	public CreditNote getCreditNote() {
		return this.creditNote;
	}

	@NotNull
	@Length(max = 1000)
	@Column(name = "description", nullable = false, length = 1000)
	public String getDescription() {
		return this.description;
	}

	@Override
	@Transient
	public Set<? extends PricingItem> getModules() {
		return null;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nominalid", nullable = false)
	public NominalCode getNominal() {
		return this.nominal;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orgid", nullable = true)
	public Subdiv getBusinessSubdiv() {
		return businessSubdiv;
	}
}