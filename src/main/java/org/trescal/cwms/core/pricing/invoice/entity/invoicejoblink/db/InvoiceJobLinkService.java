package org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceJobLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLinkAjaxDto;

public interface InvoiceJobLinkService
{
	ResultWrapper ajaxDeleteJobLink(int id);

	ResultWrapper ajaxSaveOrUpdateJobLinks(List<InvoiceJobLinkAjaxDto> invoiceJobLinks);

	void deleteInvoiceJobLink(InvoiceJobLink invoiceJobLink);

	InvoiceJobLink findByJobNo(String jobno);

	List<InvoiceJobLink> findForInvoice(int invoiceid);

	InvoiceJobLink findInvoiceJobLink(int id);

	List<InvoiceJobLink> getLinksForJobNo(String jobno);

	void saveOrUpdateInvoiceJobLink(InvoiceJobLink invoiceJobLink);
	
	List<InvoiceJobLinkDTO> getLinksByIds(Collection<Integer> invoiceIds);
}
