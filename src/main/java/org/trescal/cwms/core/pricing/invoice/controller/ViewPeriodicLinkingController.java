package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand.JOB_LOAD_CONTACT;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand.JOB_LOAD_CURRENCY;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_CLIENT_POs;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_CONTRACT_REVIEW_PRICE;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_COSTING_ITEMS;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_INSTRUMENTS;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_ON_BEHALF;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_SERVICE_TYPES;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand.INV_CALC_NOT_INVOICED_JOB_ITEM_ESTIMATES;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand.INV_LOAD_NOT_INVOICED_EXPENSE_ITEMS;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand.INV_LOAD_NOT_INVOICED_JOB_ITEMS;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand.INV_LOAD_NOT_INVOICED_JOB_ITEM_DELIVERIES;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Controller
@IntranetController
public class ViewPeriodicLinkingController {

	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private InvoiceProjectionService invoiceProjectionService;
	@Autowired
	private SupportedLocaleService localeService;

	public static final String REQUEST_URL = "/viewPeriodicLinking.htm";
	public static final String VIEW_NAME = "trescal/core/pricing/invoice/viewperiodicLinking";

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.GET)
	public String viewPeriodicLinking(@RequestParam(value = "id", required = true) int id, Locale locale, Model model) {

		Invoice invoice = this.invoiceService.findInvoice(id);

		Locale fallbackLocale = this.localeService.getPrimaryLocale();

		JobItemProjectionCommand jobItemCommand = new JobItemProjectionCommand(locale,
				invoice.getOrganisation().getId(), JI_LOAD_CONTRACT_REVIEW_PRICE.class, JI_LOAD_COSTING_ITEMS.class,
				JI_LOAD_INSTRUMENTS.class, JI_LOAD_CLIENT_POs.class, JI_LOAD_ON_BEHALF.class,
				JI_LOAD_SERVICE_TYPES.class);
		JobProjectionCommand jobCommand = new JobProjectionCommand(fallbackLocale, invoice.getOrganisation().getId(),
				JOB_LOAD_CONTACT.class, JOB_LOAD_CURRENCY.class);

		InvoiceProjectionCommand invoiceCommand = new InvoiceProjectionCommand(locale,
				invoice.getOrganisation().getId(), jobItemCommand, jobCommand,
				INV_LOAD_NOT_INVOICED_EXPENSE_ITEMS.class, INV_LOAD_NOT_INVOICED_JOB_ITEMS.class,
				INV_LOAD_NOT_INVOICED_JOB_ITEM_DELIVERIES.class, INV_CALC_NOT_INVOICED_JOB_ITEM_ESTIMATES.class);

		List<InvoiceProjectionDTO> invoiceDtos = this.invoiceProjectionService
				.getInvoiceProjectionDTOs(Collections.singleton(id), invoiceCommand);

		InvoiceProjectionDTO invoiceDto = invoiceDtos.get(0);

		model.addAttribute("dto", invoiceDto);
		model.addAttribute("orgId", invoice.getComp().getCoid());

		return VIEW_NAME;
	}

}
