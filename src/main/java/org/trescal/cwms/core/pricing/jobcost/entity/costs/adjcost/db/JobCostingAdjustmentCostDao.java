package org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.db;

import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;

public interface JobCostingAdjustmentCostDao extends JobCostingCostDaoSupport<JobCostingAdjustmentCost> {}