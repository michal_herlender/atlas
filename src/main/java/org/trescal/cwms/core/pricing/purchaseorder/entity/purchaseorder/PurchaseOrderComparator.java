package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder;

import java.util.Comparator;

/**
 * Basic built in comparator which sorts quotations by registration date (desc)
 * and then by id (in case of migrated POs with exact same dates!)
 * @author Richard
 */
public class PurchaseOrderComparator implements Comparator<PurchaseOrder>
{
	public int compare(PurchaseOrder po1, PurchaseOrder po2)
	{
		int result = po2.getRegdate().compareTo(po1.getRegdate());
		if (result == 0) result = po2.getId() - po1.getId();
		return result;
	}
}
