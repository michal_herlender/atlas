package org.trescal.cwms.core.pricing.entity.costs.base.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.entity.costs.base.CalCost;

@Repository
public class CalCostDaoImpl extends BaseDaoImpl<CalCost, Integer> implements CalCostDao {
	
	@Override
	protected Class<CalCost> getEntity() {
		return CalCost.class;
	}
}