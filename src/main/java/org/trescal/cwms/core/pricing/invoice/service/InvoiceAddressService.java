package org.trescal.cwms.core.pricing.invoice.service;

import java.util.List;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

public class InvoiceAddressService {
	
	private AddressService addressService;

	/**
	 * Trys to find an {@link Invoice} address for the {@link PInvoice}.
	 * 
	 * @param invoice the {@link PInvoice}.
	 * @return the {@link PInvoice} with an address set.
	 */
	public PInvoice setAddress(PInvoice invoice) {
		// get a list of all invoice addresses for this company
		List<Address> addresses = this.addressService.getAllCompanyAddresses(invoice.getCoid(), AddressType.INVOICE, true);
		if (addresses.isEmpty()) {
			addresses = this.addressService.getAllCompanyAddresses(invoice.getCoid(), AddressType.WITHOUT, true);
			// no invoice address for this company - find a delivery address for
			// this company and job
			Job j = invoice.getJobs().get(invoice.getJobs().keySet().iterator().next());
			for (Address a : addresses) {
				if (a.getAddrid() == j.getReturnTo().getAddrid()) {
					invoice.setAddress(a);
					invoice.setAddressId(invoice.getAddress().getAddrid());
					break;
				}
			}
			// we were still unable to match an address, just take any address
			// from the company address list
			if (invoice.getAddress() == null) {
				invoice.setAddress(addresses.get(0));
				invoice.setAddressId(invoice.getAddress().getAddrid());
			}
		} else if (addresses.size() == 1) {
			// found one invoice address - use this
			invoice.setAddress(addresses.get(0));
			invoice.setAddressId(invoice.getAddress().getAddrid());
		} else {
			// multiple invoice addresses found, try and match to the subdiv for
			// this job
			Job j = invoice.getJobs().get(invoice.getJobs().keySet().iterator().next());
			for (Address a : addresses) {
				if (a.getSub().getSubdivid() == j.getCon().getSub().getSubdivid()) {
					invoice.setAddress(a);
					invoice.setAddressId(invoice.getAddress().getAddrid());
					break;
				}
			}
			// we were still unable to match an address, just take any address
			// from the company invoice address list
			if (invoice.getAddress() == null) {
				invoice.setAddress(addresses.get(0));
				invoice.setAddressId(invoice.getAddress().getAddrid());
			}
		}
		invoice.setCompanyAddresses(addresses);
		return invoice;
	}

	public void setAddrServ(AddressService addrServ) {
		this.addressService = addrServ;
	}
}
