package org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.creditnote.dto.CreditNoteDTO;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.creditnote.form.CreditNoteHomeForm;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface CreditNoteService extends BaseService<CreditNote, Integer>
{
	/**
	 * Deletes the {@link CreditNote} identified by the given id, logs the deletion
	 * in a {@link DeletedComponent}. Validates the credit note exists and that the
	 * {@link DeletedComponent} has been completed properly.
	 * 
	 * @param crid the id of the {@link CreditNote} to delete.
	 * @return {@link ResultWrapper} indicating sucess of the operation.
	 */
	List<KeyValueIntegerString> getNumbersPerInvoices(Collection<Integer> invoiceIds);
	
	ResultWrapper deleteCreditNote(int crid, String reason, HttpSession session);
	
	List<CreditNote> findAllCreditNotesByAccountStatus(Company businessCompany,AccountStatus accStatus);
	
	CreditNote findByNumber(String cnNo, Integer orgid); 
	
	List<CreditNote> getAllAtStatus(Company businessCompany, CreditNoteStatus status);
	
	CreditNote createCreditNote(Invoice invoice, boolean creditWoleInvoice, Contact currentContact, Subdiv allocatedSubdiv);
	
	boolean changeAccountStatus(String creditNoteNumber, Integer companyId, AccountStatus accountStatus);
	
	PagedResultSet<CreditNoteDTO> searchCreditNotes(CreditNoteHomeForm form, PagedResultSet<CreditNoteDTO> rs);
	
	CreditNote recalculateAndMerge(CreditNote creditNote);
}