package org.trescal.cwms.core.pricing.supplierinvoice.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.paymentmode.db.PaymentModeService;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.db.SupplierInvoiceService;
import org.trescal.cwms.core.pricing.supplierinvoice.form.SupplierInvoiceForm;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;

@Controller
@IntranetController
public class EditSupplierInvoiceController {
	@Autowired
	private PaymentModeService paymentModeService;
	@Autowired
	private SupplierInvoiceService supplierInvoiceService;
	@Autowired
	private SupportedCurrencyService currencyServ;

	public static final String FORM_NAME = "form";
	public static final String VIEW_NAME = "trescal/core/pricing/supplierinvoice/editsupplierinvoice";
	public static final String REDIRECT_URL = "viewpurchaseorder.htm?loadtab=accounts-tab&id=";
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@ModelAttribute(FORM_NAME)
	public SupplierInvoiceForm formBackingObject(@RequestParam(name = "id") int supplierinvoiceid) {
		SupplierInvoice supplierInvoice = this.supplierInvoiceService.get(supplierinvoiceid);

		SupplierInvoiceForm form = new SupplierInvoiceForm();
		form.setInvoiceDate(supplierInvoice.getInvoiceDate());
		form.setInvoiceNumber(supplierInvoice.getInvoiceNumber());
		form.setPaymentDate(supplierInvoice.getPaymentDate());
		form.setTotalWithoutTax(supplierInvoice.getTotalWithoutTax() == null ? null : supplierInvoice.getTotalWithoutTax());
		form.setTotalWithTax(supplierInvoice.getTotalWithTax() == null ? null : supplierInvoice.getTotalWithTax());
		form.setPaymentTerm(supplierInvoice.getPaymentTerm());
		form.setPaymentModeId(supplierInvoice.getPaymentMode() == null ? null : supplierInvoice.getPaymentMode().getId());
		form.setCurrencyCode(supplierInvoice.getCurrency() == null ? null : supplierInvoice.getCurrency().getCurrencyCode());
		return form;
	}

	@RequestMapping(value = "editsupplierinvoice.htm", method = RequestMethod.POST)
	private ModelAndView onSubmit(Locale locale,
								  @RequestParam(name = "id") int supplierinvoiceid,
								  @Valid @ModelAttribute(FORM_NAME) SupplierInvoiceForm form,
								  BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(locale, supplierinvoiceid);
		}
		// edit the supplier invoice
		SupplierInvoice supplierInvoice = this.supplierInvoiceService.editSupplierInvoice(supplierinvoiceid, form);

		return new ModelAndView(new RedirectView(REDIRECT_URL + supplierInvoice.getPurchaseOrder().getId(), true));
	}

	@RequestMapping(value = "editsupplierinvoice.htm", method = RequestMethod.GET)
	private ModelAndView referenceData(Locale locale,
									   @RequestParam(name = "id") int supplierinvoiceid) {
		SupplierInvoice supplierInvoice = this.supplierInvoiceService.get(supplierinvoiceid);

		Map<String, Object> map = new HashMap<>();
		map.put("order", supplierInvoice.getPurchaseOrder());
		map.put("paymentterms", Arrays.stream(PaymentTerm.values()).collect(Collectors.toMap(PaymentTerm::name, PaymentTerm::getMessage)));
		map.put("paymentModes", paymentModeService.getDTOList(locale, true));
		map.put("supplierInvoice", supplierInvoice);
		map.put("currencyList", this.currencyServ.getAllSupportedCurrencys());

		return new ModelAndView(VIEW_NAME, map);
	}
}