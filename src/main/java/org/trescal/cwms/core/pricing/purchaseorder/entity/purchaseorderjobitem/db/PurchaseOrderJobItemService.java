package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.projection.PurchaseOrderItemProjectionDTO;

public interface PurchaseOrderJobItemService {
	
	void insertPurchaseOrderJobItem(PurchaseOrderJobItem purchaseorderjobitem);

	/**
	 * Removes any existing {@link JobItem} links to the given
	 * {@link PurchaseOrderItem} and links any new ones. Does not persist new
	 * {@link PurchaseOrderJobItem} to db.
	 * 
	 * @param jobitemids list of {@link JobItem} ids.
	 * @param item the {@link PurchaseOrderItem}.
	 */
	void linkJobItemsToPurchaseOrder(List<Integer> jobitemids, PurchaseOrderItem item);
	
	List<PurchaseOrderItemProjectionDTO> getProjectionDTOsForJobItems(Collection<Integer> jobItemIds);
}