package org.trescal.cwms.core.pricing.invoice.dto;

import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InvoiceItemPoLinkDTO {

	private Integer itemId;
	private Integer itemNo;
	private Integer linkId;
	private Integer invoiceId;
	private Integer jobId;
	private Integer poId;
	private String poNo;
	private Integer bpoId;
	private String bpoNo;
	
	private Integer invoicePOId;
	private String invoicePONo;
	
	public InvoiceItemPoLinkDTO(Integer itemId, Integer itemNo, Integer linkId, Integer invoiceId, Integer jobId,
			Integer poId, String poNo, Integer bpoId, String bpoNo) {
		super();
		this.itemId = itemId;
		this.itemNo = itemNo;
		this.linkId = linkId;
		this.invoiceId = invoiceId;
		this.jobId = jobId;
		this.poId = poId;
		this.poNo = poNo;
		this.bpoId = bpoId;
		this.bpoNo = bpoNo;
	}

	public InvoiceItemPoLinkDTO(Invoice invoice, Integer poId,
			String poNo, Integer bpoId, String bpoNo, Integer invoicePOId, String invoicePONo) {
		super();

		this.invoiceId = invoice.getId();
		this.poId = poId;
		this.poNo = poNo;
		this.bpoId = bpoId;
		this.bpoNo = bpoNo;
		this.invoicePOId = invoicePOId;
		this.invoicePONo = invoicePONo;
	}
	
	
	
	
}