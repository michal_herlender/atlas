package org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.WithCostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingItem;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedAdjCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

import javax.persistence.*;

@Entity
@DiscriminatorValue("jobcosting")
public class JobCostingAdjustmentCost extends TPSupportedAdjCost implements WithJobCostingItem, WithCostSource<JobCostingLinkedAdjustmentCost>
{
	private JobCostingItem jobCostingItem;

	private JobCostingLinkedAdjustmentCost linkedCost;
	private CostSource costSrc;

	@Enumerated(EnumType.STRING)
	@Column(name = "costsource")
	public CostSource getCostSrc()
	{
		return this.costSrc;
	}

	@OneToOne(mappedBy = "adjustmentCost")
	public JobCostingItem getJobCostingItem()
	{
		return this.jobCostingItem;
	}

	@OneToOne(mappedBy = "adjustmentCost", cascade = CascadeType.ALL)
	public JobCostingLinkedAdjustmentCost getLinkedCost()
	{
		return this.linkedCost;
	}

	public void setCostSrc(CostSource costSrc)
	{
		this.costSrc = costSrc;
	}

	public void setJobCostingItem(JobCostingItem jobCostingItem)
	{
		this.jobCostingItem = jobCostingItem;
	}

	public void setLinkedCost(JobCostingLinkedAdjustmentCost linkedCost)
	{
		this.linkedCost = linkedCost;
	}
}
