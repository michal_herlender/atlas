package org.trescal.cwms.core.pricing.invoice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.system.Constants;

@Controller
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class DeleteInvoiceController {
	@Autowired
	private InvoiceService invoiceService;

	@RequestMapping(value = "/deleteInvoice.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultWrapper updateQuotationItems(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(name = "invoiceid", required = true) Integer invoiceid,
			@RequestParam(name = "reason", required = false) String reason) throws Exception {

		return this.invoiceService.deleteInvoice(invoiceid, reason, username);
	}
	
}
