package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.CreditNoteAction;

public interface CreditNoteActionDao extends BaseDao<CreditNoteAction, Integer> {}