package org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceTypeKeyValue;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType_;

@Repository
public class InvoiceTypeDaoImpl extends BaseDaoImpl<InvoiceType, Integer> implements InvoiceTypeDao {

	@Override
	protected Class<InvoiceType> getEntity() {
		return InvoiceType.class;
	}

	@Override
	public List<InvoiceTypeKeyValue> getAllTranslated(Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<InvoiceTypeKeyValue> cq = cb.createQuery(InvoiceTypeKeyValue.class);
			Root<InvoiceType> invoiceType = cq.from(InvoiceType.class);
			Expression<String> translation = joinTranslation(cb, invoiceType, InvoiceType_.nametranslation, locale);
			cq.select(cb.construct(InvoiceTypeKeyValue.class, invoiceType.get(InvoiceType_.id), translation));
			return cq;
		});
	}

	@Override
	public InvoiceType findInvoiceTypeByName(String name) {
		return findByUniqueProperty(InvoiceType_.name, name);
	}
}