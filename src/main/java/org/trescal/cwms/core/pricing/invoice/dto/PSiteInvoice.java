package org.trescal.cwms.core.pricing.invoice.dto;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;

public class PSiteInvoice extends PInvoice
{
	private BigDecimal totalCost;
	private ArrayList<String> siteCostingNotes;

	public PSiteInvoice(Company comp, int tempId, InvoiceType type, PricingType pricingType)
	{
		super(comp, type, pricingType);
		this.totalCost = new BigDecimal("0.00");
		this.siteCostingNotes = new ArrayList<String>();
	}

	public ArrayList<String> getSiteCostingNotes()
	{
		return this.siteCostingNotes;
	}

	public BigDecimal getTotalCost()
	{
		return this.totalCost;
	}

	public void setSiteCostingNotes(ArrayList<String> siteCostingNotes)
	{
		this.siteCostingNotes = siteCostingNotes;
	}

	public void setTotalCost(BigDecimal totalCost)
	{
		this.totalCost = totalCost;
	}

}
