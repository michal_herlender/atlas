package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.dto.PurchaseOrderStatusProjectionDTO;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;

public interface PurchaseOrderStatusDao extends BaseDao<PurchaseOrderStatus, Integer> {
	
	List<PurchaseOrderStatus> findAllPurchaseOrderStatusRequiringAttention();
	
	PurchaseOrderStatus findOnCancelPurchaseOrderStatus(ActionType type);
	
	PurchaseOrderStatus findOnCompletePurchaseOrderStatus(ActionType type);
	
	PurchaseOrderStatus findOnInsertPurchaseOrderStatus(ActionType type);
		
	Integer findPurchaseOrderStatusIdRequiringAttentionByName(String name);
	
	List<PurchaseOrderStatusProjectionDTO> findAllPurchaseOrderStatusRequiringAttentionProjection(Locale locale);
	
}