package org.trescal.cwms.core.pricing.invoice.entity.invoiceitem;

import java.util.Comparator;

public class InvoiceItemComparator implements Comparator<InvoiceItem> {

	@Override
	public int compare(InvoiceItem o1, InvoiceItem o2) {
		if (o1.getId() == null && o2.getId() == null)
			return 1;
		else
			return (o1.getItemno()).compareTo(o2.getItemno());
	}
}