package org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink;

import java.util.Comparator;

/**
 * Simple comparator that sorts {@link InvoiceJobLink} entites by jobid, then
 * invoiceid.
 * 
 * @author Richard
 */
public class InvoiceJobLinkComparator implements Comparator<InvoiceJobLink>
{
	@Override
	public int compare(InvoiceJobLink o1, InvoiceJobLink o2)
	{
		if ((o1.getJob() == null) || (o2.getJob() == null))
		{
			return o1.getJobno().compareTo(o2.getJobno());
		}
		if (o1.getJob().getJobid() == o2.getJob().getJobid())
		{
			return ((Integer) o2.getInvoice().getId()).compareTo(o1.getInvoice().getId());
		}
		else
		{
			return ((Integer) o1.getJob().getJobid()).compareTo(o2.getJob().getJobid());
		}
	}
}
