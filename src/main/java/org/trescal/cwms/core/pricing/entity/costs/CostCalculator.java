package org.trescal.cwms.core.pricing.entity.costs;

import java.math.BigDecimal;
import java.util.Set;

import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.tools.MathTools;

public class CostCalculator {
    /**
     * Convenience method for populating an empty {@link Cost} (i.e. zero valued).
     *
     * @param costType the {@link CostType} of the {@link Cost}.
     * @param cost     the {@link Cost}.
     * @return the {@link Cost}.
     */
    public static Cost createEmptyCost(CostType costType, Cost cost) {
        cost.setCostType(costType);
        cost.setTotalCost(new BigDecimal("0.00"));
        cost.setDiscountRate(new BigDecimal("0.00"));
        cost.setDiscountValue(new BigDecimal("0.00"));
        cost.setFinalCost(new BigDecimal("0.00"));
        return cost;
    }

    /**
     * Convenience method for populating a {@link Cost} of any time.
     *
     * @param costType     the type of {@link Cost}.
     * @param totalCost    the total cost of this cost.
     * @param discountRate the discount rate for this cost.
     * @param activeCost   whether or not this cost is active.
     * @param cost         the {@link Cost}.
     * @return the {@link Cost}.
     */
    public static void populateCostType(CostType costType, BigDecimal totalCost, BigDecimal discountRate,
                                        boolean activeCost, Cost cost) {
        cost.setCostType(costType);
        cost.setActive(activeCost);

        cost.setTotalCost(totalCost);
        cost.setDiscountRate(discountRate);

        BigDecimal discValue = totalCost
                .multiply(discountRate.divide(new BigDecimal(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN));
        cost.setFinalCost((totalCost.subtract(discValue)).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
        cost.setDiscountValue(discValue);
    }

    /**
     * See
     * {@link #populateCostType(CostType, BigDecimal, BigDecimal, boolean, Cost)} .
     *
     * @param costType
     * @param totalCost
     * @param discountRate
     * @param activeCost
     * @param cost
     * @return
     */
    public static void populateCostType(CostType costType, double totalCost, double discountRate, boolean activeCost,
                                        Cost cost) {
        populateCostType(costType, new BigDecimal(String.valueOf(totalCost)),
                new BigDecimal(String.valueOf(discountRate)), activeCost, cost);
    }

    /**
     * Method rounds a double value to the next .5. In some cases CWMS business
     * rules dicate that prices from 0 - 0.5 p should round to 0.5p, any costs from
     * 0.51 - 0.99 should round to 1.
     *
     * @param num
     * @return
     */
    public static double roundCWMSCost(double num) {
        double down = Math.floor(num);
        double decimal = num - down;

        if (decimal > 0.00) {
            decimal = decimal + 0.5;
            decimal = Math.ceil(decimal);
            decimal = decimal / 2;
            down = down + decimal;
        }
        return down;
    }

    /**
     * Updates the discount value and final cost of a {@link PricingItem}.
     *
     * @param item the {@link PricingItem} to update, not null.
     * @return the {@link PricingItem}.
     */
    public static PricingItem updateItemCost(PricingItem item) {
        if ((item.getTotalCost().doubleValue() > 0) && (item.getGeneralDiscountRate().doubleValue() > 0)) {
            BigDecimal totalCost = item.getTotalCost().multiply(new BigDecimal(item.getQuantity()));
            BigDecimal discountValue = totalCost.multiply(item.getGeneralDiscountRate().divide(new BigDecimal(100),
                    MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN));
            discountValue = discountValue.setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN);
            item.setFinalCost(totalCost.subtract(discountValue));
            item.setGeneralDiscountValue(discountValue);
        } else {
            BigDecimal totalCost = (item.getQuantity() != null)
                    ? item.getTotalCost().multiply(new BigDecimal(item.getQuantity()))
                    : item.getTotalCost();
            item.setGeneralDiscountValue(new BigDecimal(0));
            item.setFinalCost(BigDecimal.valueOf(totalCost.doubleValue()).setScale(MathTools.SCALE_FIN,
                    MathTools.ROUND_MODE_FIN));
        }
        return item;
    }

    /**
     * Method looks through the {@link Set} of {@link Cost}(s) that belong to the
     * given {@link PricingItem}, adds each active {@link CostType}'s final cost to
     * produce a total figure and sets this as the item's total cost. This figure is
     * then used to calculate the finalcost for the item. <br/>
     * <br/>
     * Warning this function should only be used after a form submission if any
     * changed {@link Cost}s have been manually set - otherwise the {@link Cost} s
     * Set will contain old data. <br/>
     * <br/>
     * Additionally we have hardcoded in here to ignore inspection costs. There may
     * be a need to externalise this and add a includeInTotal boolean property on
     * {@link CostType}.
     *
     * @param item the {@link PricingItem}, not null.
     * @return the {@link PricingItem}.
     */
    public static PricingItem updateItemCostWithRunningTotal(PricingItem item) {
        BigDecimal runningTotal = new BigDecimal(0.00);
        for (Cost cost : item.getCosts()) {
            if (cost.isActive() && !cost.getCostType().equals(CostType.INSPECTION)) {
                runningTotal = runningTotal.add(cost.getFinalCost());
            }
        }
        item.setTotalCost(runningTotal.setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
        return updateItemCost(item);
    }

    /**
     * Updates the finalcost of the given {@link Pricing} entity with the sum of the
     * finalcosts of each of it's {@link PricingItem}s.
     *
     * @param pricing the {@link Pricing} entity to calculate the final cost for,
     *                not null.
     * @param items   a {@link Set} of {@link PricingItem}s that belong to this
     *                entity, not null.
     * @return the {@link Pricing}.
     */
    public static Pricing<?> updatePricingFinalCost(Pricing<?> pricing, Set<? extends PricingItem> items) {
        updatePricingTotalCost(pricing, items);
        return updatePricingWithCalculatedTotalCost(pricing);
    }

    public static Pricing<?> updatePricingTotalCost(Pricing<?> pricing, Set<? extends PricingItem> items) {
        BigDecimal runningTotal = items == null ? new BigDecimal(0.00)
                : items.stream().map(PricingItem::getFinalCost).reduce(BigDecimal.valueOf(0, 2), BigDecimal::add);
        pricing.setTotalCost(runningTotal);
        return pricing;
    }

    /**
     * Updates the Pricing total using the current totalCost i.e. does not use item
     * totals in calculations. Normally used when a total cost has already been set,
     * for example manually on a site job costing.
     *
     * @param pricing
     * @return
     * @deprecated use updatePricingWithCalculatedTotalCost and calculate tax
     * somewhere else
     */
    public static Pricing<?> updatePricingWithCalculatedTotalCostAndTax(Pricing<?> pricing) {
        // append the carriage cost to the total
        pricing.setFinalCost(pricing.getTotalCost());
        // now set the vat value
        if (pricing.getVatRate() != null) {
            BigDecimal vatm = pricing.getVatRate().doubleValue() > 0
                    ? pricing.getVatRate().divide(new BigDecimal("100.00"))
                    : new BigDecimal("0.00");
            pricing.setVatValue(
                    vatm.multiply(pricing.getFinalCost()).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
        } else {
            pricing.setVatValue(new BigDecimal(0.00));
        }
        // update the final cost with the vat value
        pricing.setFinalCost(pricing.getFinalCost().add(pricing.getVatValue()));
        return pricing;
    }

    public static Pricing<?> updatePricingWithCalculatedTotalCost(Pricing<?> pricing) {
        // append the carriage cost to the total
        pricing.setFinalCost(pricing.getTotalCost());
        // update the final cost with the vat value
        if (pricing.getVatValue() != null)
            pricing.setFinalCost(pricing.getFinalCost().add(pricing.getVatValue()));
        return pricing;
    }

    /**
     * Calculates the discount value and final cost of the given {@link Cost}
     * entity.
     *
     * @param cost             the {@link Cost} entity to sum, not null.
     * @param roundUpFinalCost
     * @return the {@link Cost} entity.
     */
    public static Cost updateSingleCost(Cost cost, boolean roundUpFinalCost) {
        if (cost != null) {
            if ((cost.getTotalCost().doubleValue() > 0) && (cost.getDiscountRate().doubleValue() > 0)) {
                // get the discount rate as a decimal (set scale to 4 decimal
                // places)
                BigDecimal up = cost.getDiscountRate().divide(new BigDecimal("100"), MathTools.SCALE_FIN_CALC,
                        MathTools.ROUND_MODE_FIN);

                // now calculate discount value
                BigDecimal discountValue = up.multiply(cost.getTotalCost()).setScale(MathTools.SCALE_FIN,
                        MathTools.ROUND_MODE_FIN);

                // get final cost
                cost.setFinalCost(cost.getTotalCost().subtract(discountValue));
                cost.setDiscountValue(discountValue);
            } else {
                cost.setDiscountValue(BigDecimal.valueOf(0.00));
                cost.setFinalCost(BigDecimal.valueOf(cost.getTotalCost().doubleValue()));
            }

            // round to nearest .50p // @TODO maybe allow round to to be
            // editable?
            if (roundUpFinalCost) {
                cost.setFinalCost(new BigDecimal(roundCWMSCost(cost.getFinalCost().doubleValue()))
                        .setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
            }
        }
        return cost;
    }

    /**
     * Same as updateItemCostWithRunningTotal({@link PricingItem}) but includes
     * support for adding the third party carriage costs into the total of the items
     * value.
     *
     * @param item
     * @return
     */
    public static ThirdPartyPricingItem updateTPItemCostWithRunningTotal(ThirdPartyPricingItem item) {
        item = (ThirdPartyPricingItem) updateItemCostWithRunningTotal(item);
        if (item.getTpCarriageTotal() != null) {
            item.setTotalCost(item.getTotalCost().add(item.getTpCarriageTotal()));
        }
        return (ThirdPartyPricingItem) updateItemCost(item);
    }
}
