package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.purchaseorder.dto.AjaxPurchaseOrderForm;
import org.trescal.cwms.core.pricing.purchaseorder.dto.AjaxPurchaseOrderItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.InvoicedJobPONumberDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.SupplierPOProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.PurchaseOrderAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderHomeForm;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

public interface PurchaseOrderService
{
	PurchaseOrder createPurchaseOrderFromTPQuotation(TPQuotation tpQuotation, Map<Integer, Boolean> selection, Map<Integer, Integer> nominals, Contact currentContact, Locale locale, Locale defaultLocale, Subdiv allocatedSubdiv);
	
	/**
	 * Deletes the {@link PurchaseOrder} identified by the given id and adds an
	 * entry to {@link DeletedComponent} to log this deletion.
	 * 
	 * @param orderid the {@link PurchaseOrder} id.
	 * @param reason the reason for the deletion.
	 * @return {@link ResultWrapper} indicating success of operation.
	 */
	ResultWrapper deleteOrder(int orderid, String reason, HttpSession session);

	void deletePurchaseOrder(PurchaseOrder purchaseorder);
	
	/**
	 * Find all purchase orders directly linked to job
	 * @param job Job
	 * @return List of all purchase orders linked to job
	 */
	List<PurchaseOrderDTO> findAllByJob(Job job);
	
	/**
	 * Find {@value maxResults} most recent purchase orders allocated to given business company
	 * @param allocatedCompany
	 * @param maxResults maximum number of results
	 * @return List of most recent purchase orders
	 */
	List<PurchaseOrderDTO> findMostRecent(Company allocatedCompany, Integer maxResults);
	
	/**
	 * Returns a {@link List} of all {@link PurchaseOrder} entities at a status
	 * that requires attention from an accounts staff member.
	 * 
	 * @return
	 */
	List<PurchaseOrder> findAllPOsRequiringAttention();
	
	PurchaseOrder find(int id);

	/**
	 * Returns a list of all {@link PurchaseOrder} entities linked to the Job or its Job Items.
	 * 
	 * @param job the @link Job job to match.
	 * @return list of matching {@link PurchaseOrder} sorted by regdate and then id.
	 */
	List<PurchaseOrder> findByJob(Job job);

	List<PurchaseOrder> getAll();

	/**
	 * Returns all {@link PurchaseOrder} entites at the given
	 * {@link org.trescal.cwms.core.system.entity.status.Status}.
	 * 
	 * @param statusid the id of the
	 *        {@link org.trescal.cwms.core.system.entity.status.Status}.
	 * @return list of matching {@link PurchaseOrder}.
	 */
	List<PurchaseOrder> getAllPurchaseOrdersAtStatus(PurchaseOrderStatus status, Company allocatedCompany);

	List<PurchaseOrder> getByAccountStatus(Company businessCompany, AccountStatus accStatus);
	/**
	 * Returns the {@link PurchaseOrder} with the given PO No or null if there
	 * are no purchase orders with the given PO No.
	 * 
	 * @param poNo the poNo to match.
	 * @return the {@link Purchase Order}
	 */
	PurchaseOrder getPOByExactPONo(String poNo);

	Map<Integer,List<InvoicedJobPONumberDTO>> getPONumbersFromInvoicedJobs(Invoice i);

	ResultWrapper insertAjaxPurchaseOrder(AjaxPurchaseOrderForm form, User user, Subdiv subdiv);

	void insertPurchaseOrder(PurchaseOrder purchaseorder);

	/**
	 * THIS METHOD IS FIRED FROM ALL VARIOUS WAYS A JOB ITEM CAN BE LINKED TO A
	 * PURCHASE ORDER SO THAT MULTIPLE HOOKS DO NOT HAVE TO BE WRITTEN
	 */
	void linkJobItemToPO(PurchaseOrderJobItem poji);

	PurchaseOrder mergePurchaseOrder(PurchaseOrder purchaseorder);

	/**
	 * Prepares a new {@link AjaxPurchaseOrderForm} ready to create a
	 * {@link PurchaseOrder} from a {@link JobItem} screen.
	 * 
	 * @param jobitemid the id of the {@link JobItem}.
	 * @param type 
	 * @return prepared form.
	 */
	AjaxPurchaseOrderForm prepareAjaxPurchaseOrderForm(int jobitemid, HttpServletRequest request);

	/**
	 * Takes the given {@link PurchaseOrder} entity, looks up its new
	 * {@link PurchaseOrderStatus} and ponum and then persists it to the
	 * database.
	 * 
	 * @param purchaseorder the {@link PurchaseOrder} to persist.
	 * @return 
	 */
	PurchaseOrder prepareAndInsertPurchaseOrder(PurchaseOrder purchaseorder, Contact currentContact, Subdiv subdiv);

	/**
	 * Resets the given {@link PurchaseOrder} to the 'pending' brokering account
	 * status.
	 * 
	 * @param poid the order id.
	 * @return {@link ResultWrapper} indicating success of operation.
	 */
	ResultWrapper resetAccountStatus(int poid, HttpSession session);

	PurchaseOrder merge(PurchaseOrder purchaseorder, Contact currentContact);
	
	/**
	 * Performs a search for {@link PurchaseOrder} using the criteria set in the
	 * {@link PurchaseOrderHomeForm}, results sorted by po date desc, po id
	 * desc.
	 * 
	 * @param form {@link PurchaseOrderHomeForm}.
	 * @param rs a {@link PagedResultSet}.
	 * @return {@link PagedResultSet}.
	 */
	PagedResultSet<PurchaseOrder> searchSortedPurchaseOrder(PurchaseOrderHomeForm form, PagedResultSet<PurchaseOrder> rs);

	void updatePurchaseOrder(PurchaseOrder purchaseorder, Contact currentContact);

	/**
	 * Updates and saves the final cost of the given {@link PurchaseOrder}.
	 * 
	 * @param order the {@link PurchaseOrder}.
	 * @return 
	 */
	PurchaseOrder updatePurchaseOrderCosts(PurchaseOrder order, Contact currentContact);

	/**
	 * Updates the {@link PurchaseOrder}, sets the next available status and
	 * inserts an {@link PurchaseOrderAction} to log this. Also does a special
	 * test to check if the items on the order also require status updates and
	 * if so performs this task. (Currently this is just set to check if the
	 * purchase order is being issued and if so looks for purchase order item
	 * actions for 'issuing' and logs this activity for each item on the order).
	 * Note it is up to the callee to persist these changes.
	 * 
	 * @param order the {@link PurchaseOrder}.
 	 * @param order the {@link Contact}.
	 * @param order the {@link Subdiv}.
	 * @return 

	 */
	Boolean updatePurchaseOrderStatus(PurchaseOrder order, Contact currentContact,Subdiv currentSubdiv);
	
	/**
	 * Checks each item on the order to ensure that they have been approved by
	 * accounts or cancelled. If all match this criteria then the order's status
	 * is updated and the accountstatus changed to pending.
	 * 
	 * @param order
	 */
	void validateAndCompletePurchaseOrder(PurchaseOrder order, Contact currentContact);

	Integer copyPo(Integer oldPoId, Boolean cancelNotReceivedGoods, Contact currentContact, Integer subdivId);
	
	PurchaseOrder findByNumber(String poNo, Integer busId);
	
	boolean changeAccountStatus(String purchaseOrderNumber, Integer busId, AccountStatus accountStatus);
	
	List<AjaxPurchaseOrderItemDTO> preparePurchaseOrderItem(int jobid, StateGroup type, Locale locale);
	
	List<SupplierPOProjectionDTO> getByAccountStatusUsingProjection(Integer businessCompanyId,Integer SubdivId ,AccountStatus accStatus);
	
	List<SupplierPOProjectionDTO> getAllPurchaseOrdersAtStatusUsingProjection(Integer statusId, Integer allocatedCompanyId,Integer subdivId);
	
	List<PurchaseOrder> getAllPurchaseOrdersAtStatus(Integer statusId, Integer allocatedCompanyId);
	
	List<Integer> getListOfItemNo(PurchaseOrder order, Company allocatedCompany);
	
	List<Integer> getJobItemsIdsFromPurchaseOrder(PurchaseOrder purchaseOrder);
	
	List<PurchaseOrderDTO> getDtosByJobid(Integer jobId,Locale locale);
	
	Long countPoPerJob(Integer jobId,Locale locale);
	
	List<InvoiceItem> getInvoiceItemsWithPos(Integer invoiceid);
}