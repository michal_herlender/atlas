package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingtype.JobCostingType;

import java.time.LocalDate;
import java.util.Date;

@Setter
@Getter
public class JobCostingDTO {

	private int jobCostingId;
	private int personid;
	private String type;
	private String pricingType;
	private String createdBy;
	private int costContact;
	private String costContactName;
	private LocalDate createdOn;
	private String lastViewedBy;
	private Date lastViewedOn;
	private Date lastUpdateOn;
	private String lastUpdateBy;
	private String status;
	private Long itemCount;
	private String jobNo;
	private Integer ver;
	private String coname;
	private String currency;
	private String differenceDateCreated;
	private String differenceDateViewed;
	private String differenceDateUpdated;

	public JobCostingDTO(int jobCostingId, Contact contactName, PricingType type, Contact costContact, Contact createdBy,
						 LocalDate createdOn, Contact lastViewedBy, Date lastViewedOn, Contact lastUpdateBy, Date lastUpdateOn,
						 String status, Long itemCount, String jobNo, Integer ver, String coname, String currency, int pricingTypeId) {
		super();
		this.jobCostingId = jobCostingId;
		this.personid = contactName.getPersonid();
		this.type = type.name();
		JobCostingType jct = JobCostingType.getJobCostingTypeById(pricingTypeId);
		this.pricingType = jct != null ? jct.getType() : null;
		this.createdBy = createdBy.getName();
		this.costContact = costContact.getPersonid();
		this.costContactName = costContact.getName();
		this.createdOn = createdOn;
		this.lastViewedBy = lastViewedBy.getName();
		this.lastViewedOn = lastViewedOn;
		this.lastUpdateOn = lastUpdateOn;
		this.lastUpdateBy = lastUpdateBy.getName();
		this.status = status;
		this.itemCount = itemCount;
		this.jobNo = jobNo;
		this.ver = ver;
		this.coname = coname;
		this.currency = currency;
	}

}
