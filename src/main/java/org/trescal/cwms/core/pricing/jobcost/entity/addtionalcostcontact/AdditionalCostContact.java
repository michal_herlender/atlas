package org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Entity
@Table(name = "additionalcostcontact")
public class AdditionalCostContact extends Auditable
{
	private Contact contact;
	private JobCosting costing;
	private int id;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable = false)
	public Contact getContact()
	{
		return this.contact;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "costingid", nullable = false)
	public JobCosting getCosting()
	{
		return this.costing;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setCosting(JobCosting costing)
	{
		this.costing = costing;
	}

	public void setId(int id)
	{
		this.id = id;
	}

}
