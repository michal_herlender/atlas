package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.jobcost.projection.JobCostingItemProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.projection.PurchaseOrderItemProjectionDTO;

@Service("PurchaseOrderJobItemService")
public class PurchaseOrderJobItemServiceImpl implements PurchaseOrderJobItemService {
	
	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private PurchaseOrderJobItemDao purchaseOrderJobItemDao;
	@Autowired
	private PurchaseOrderService purchaseOrderServ;
	
	@Override
	public void insertPurchaseOrderJobItem(PurchaseOrderJobItem PurchaseOrderJobItem)
	{
		// fires hook
		this.purchaseOrderServ.linkJobItemToPO(PurchaseOrderJobItem);

		this.purchaseOrderJobItemDao.persist(PurchaseOrderJobItem);
	}

	@Override
	public void linkJobItemsToPurchaseOrder(List<Integer> jobitemids, PurchaseOrderItem item) {
		List<Integer> keptItemIds = new ArrayList<Integer>();
		// remove any currently linked jobitems
		if(item.getLinkedJobItems() != null && item.getLinkedJobItems().size() > 0)
			for (PurchaseOrderJobItem poji : item.getLinkedJobItems())
				if(jobitemids != null && jobitemids.contains(poji.getJi().getJobItemId()))
					keptItemIds.add(poji.getJi().getJobItemId());
				else
					item.getLinkedJobItems().remove(poji);
		// now link the new jobitems
		if (jobitemids != null) {
			List<JobItem> jobItemsToLink = jobitemids.stream()
					.filter(id -> id != null && !keptItemIds.contains(id))
					.map(jobItemServ::findJobItem)
					.filter(ji -> ji != null)
					.distinct()
					.collect(Collectors.toList());
			for(JobItem ji : jobItemsToLink) {
				PurchaseOrderJobItem poji = new PurchaseOrderJobItem();
				poji.setPoitem(item);
				poji.setJi(ji);
				item.getLinkedJobItems().add(poji);
			}
		}
	}
	
	@Override
	public List<PurchaseOrderItemProjectionDTO> getProjectionDTOsForJobItems(Collection<Integer> jobItemIds) {
		List<PurchaseOrderItemProjectionDTO> result = Collections.emptyList();
		if (jobItemIds != null && !jobItemIds.isEmpty()) {
			result = this.purchaseOrderJobItemDao.getProjectionDTOsForJobItems(jobItemIds);
		}
		return result;
	}
}