package org.trescal.cwms.core.pricing.invoice.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.Collection;

@Getter
@Setter
public class InvoiceHomeForm {
	// search fields
	private Integer orgid; // ID of logged in user's business company
	private String coname;
	private String contact;
	private Integer coid;
	private Integer personid;
	private String jobno;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	private LocalDate date;
	private LocalDate issuedate;
	private String invno;
	private String clientPO;
	
	//Newly field added for search
	public String clientBPO;
	
	private Integer typeid;
	private Integer subdivId;

	// results data
	private PagedResultSet<InvoiceDTO> rs;
	private Collection<InvoiceDTO> invoices;
	private int itemCount;
	private String searchCriteria;
	private int pageNo;
	private int resultsPerPage;
	private int resultsTotal;
	
}