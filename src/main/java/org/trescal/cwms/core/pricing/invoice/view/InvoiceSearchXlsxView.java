package org.trescal.cwms.core.pricing.invoice.view;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPoLinkDTO;
import org.trescal.cwms.core.tools.DateTools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class InvoiceSearchXlsxView extends AbstractXlsxStreamingView {

	@Autowired
	private MessageSource messageSource;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook wb, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		long startTime = System.currentTimeMillis();
		SXSSFWorkbook workbook = (SXSSFWorkbook) wb;

		@SuppressWarnings("unchecked")
		Collection<InvoiceDTO> invoices = (Collection<InvoiceDTO>) model.get("invoices");

		Locale locale = LocaleContextHolder.getLocale();
		// create a new Excel sheet
		Sheet sheet = workbook.createSheet(messageSource.getMessage("invoices", null, "invoices", locale));
		sheet.setDefaultColumnWidth(25);
		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);
		// create style for date cells
		DataFormat dateFormat = wb.createDataFormat();
		CellStyle dateStyle = wb.createCellStyle();
		dateStyle.setDataFormat(dateFormat.getFormat("d/m/yyyy"));
		// create header row
		Row header = sheet.createRow(0);
		int cellNo = 0;
		header.createCell(cellNo)
				.setCellValue(messageSource.getMessage("invoice.invoicenumber", null, "Invoice Number", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo)
				.setCellValue(messageSource.getMessage("invoice.invoicebpopo", null, "Invoice BpoPo", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue(messageSource.getMessage("company", null, "Company", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue(messageSource.getMessage("subdiv", null, "Subdiv", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue(messageSource.getMessage("jobno", null, "Job No", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue(messageSource.getMessage("jobcount", null, "Job Count", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue(messageSource.getMessage("type", null, "Type", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;
		
		header.createCell(cellNo).setCellValue(messageSource.getMessage("creditnote.ref", null, "Credit Note", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo)
				.setCellValue(messageSource.getMessage("invoice.invoicedate", null, "Invoice Date", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue(messageSource.getMessage("value", null, "Value", locale));
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue(messageSource.getMessage("currency", null, "Currency", locale));
		header.getCell(cellNo).setCellStyle(style);

		long startSegmentTime = System.currentTimeMillis(); // To time export in
															// chunks/batches
		logger.debug("Initialized sheet in " + (startSegmentTime - startTime));
		response.setHeader("Content-Disposition", "attachment; filename=\"invoicesearch_export.xlsx\"");
		int rowCount = 1;
		for (InvoiceDTO invoice : invoices) {
			Row row = sheet.createRow(rowCount++);
			cellNo = 0;

			row.createCell(cellNo).setCellValue(invoice.getInvno());
			cellNo++;

			if (invoice.getInvoiceItemPoLinkDTO() != null) {

				List<String> invoicePoNos = invoice.getInvoiceItemPoLinkDTO().stream()
						.filter(po -> po.getInvoicePOId() != null).map(InvoiceItemPoLinkDTO::getInvoicePONo)
						.collect(Collectors.toList());

				List<String> bpoNos = invoice.getInvoiceItemPoLinkDTO().stream().filter(po -> po.getBpoId() != null)
						.map(InvoiceItemPoLinkDTO::getBpoNo).collect(Collectors.toList());

				List<String> poNos = invoice.getInvoiceItemPoLinkDTO().stream().filter(po -> po.getPoId() != null)
						.map(InvoiceItemPoLinkDTO::getPoNo).collect(Collectors.toList());

				String invoicePoNoString = String.join(",", invoicePoNos);
				String bpoNoString = String.join(",", bpoNos);
				String poNosString = String.join(",", poNos);

				String invpoString = "";
				String bpoString = "";
				String ponosString = "";

				CellStyle cs = wb.createCellStyle();
				cs.setWrapText(true);
				boolean firstLine = true;

				if (!invoicePoNoString.isEmpty()) {
					invpoString = "Invoice PO : " + invoicePoNoString;
					firstLine = false;
				}

				if (!bpoNoString.isEmpty()) {
					if (!firstLine) {
						bpoString = "\n";
					} else {
						firstLine = false;
					}
					bpoString += "BPO : " + bpoNoString;
				}

				if (!poNosString.isEmpty()) {
					if (!firstLine) {
						ponosString += "\n";
					} else {
						firstLine = false;
					}
					ponosString += "PO : " + poNosString;

				}

				Cell cellval = row.createCell(cellNo);
				cellval.setCellValue(invpoString + bpoString + ponosString);

				/*
				 * row.createCell(cellNo).setCellValue(invpoString + "\n" +
				 * bpoString + "\n" + ponosString);
				 */

				CellStyle celllinebreak = wb.createCellStyle();
				celllinebreak.setWrapText(true);
				cellval.setCellStyle(celllinebreak);

			} else {
				row.createCell(cellNo).setCellValue("");
				/*	cellNo++;
			/*	row.createCell(cellNo).setCellValue(0);
				cellNo++;*/
			}
			cellNo++;


			row.createCell(cellNo).setCellValue(invoice.getConame());
			cellNo++;

			row.createCell(cellNo).setCellValue(invoice.getSubname());
			cellNo++;
			if (invoice.getInvoiceJobLinksDto() != null) {
				row.createCell(cellNo).setCellValue(invoice.getInvoiceJobLinksDto().get(0).getJobno());
				cellNo++;
				row.createCell(cellNo).setCellValue(invoice.getInvoiceJobLinksDto().size());
				cellNo++;
			} else {
				row.createCell(cellNo).setCellValue("");
				cellNo++;
				row.createCell(cellNo).setCellValue(0);
				cellNo++;
			}

			row.createCell(cellNo).setCellValue(invoice.getInvoiceType());
			cellNo++;

			/*
			 * New column details
			 */
			if (invoice.getCreditNoteNumbers() != null) {
				/*List<String> creditnoteNos = invoice.getCreditNoteNumbers().stream().collect(Collectors.toList());*/
				String creditnote = " ";
				 creditnote =  String.join(",", invoice.getCreditNoteNumbers());
				 row.createCell(cellNo).setCellValue(creditnote);
			}else {
				row.createCell(cellNo).setCellValue("");
			}
			cellNo++;
		/*	List<String> creditnoteNos = invoice.getCreditNoteNumbers().stream().collect(Collectors.toList());
			String creditnote = "";
			 creditnote =  String.join(",", creditnoteNos);*/
			
		/*	CellStyle cn = wb.createCellStyle();
			cn.setWrapText(true);*/
			
	/*		row.createCell(cellNo).setCellValue(creditnote);
			cellNo++;*/

			
			Cell cell = row.createCell(cellNo);
			cell.setCellValue(DateTools.dateFromLocalDate(invoice.getInvoiceDate()));
			cell.setCellStyle(dateStyle);
			cellNo++;
			row.createCell(cellNo).setCellValue(invoice.getTotalCost().doubleValue());
			cellNo++;
			row.createCell(cellNo).setCellValue(invoice.getCurrencyCode());
		}
	}
}
