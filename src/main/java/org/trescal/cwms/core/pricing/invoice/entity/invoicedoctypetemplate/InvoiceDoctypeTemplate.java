package org.trescal.cwms.core.pricing.invoice.entity.invoicedoctypetemplate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.system.entity.componentdoctypetemplate.ComponentDoctypeTemplate;
import org.trescal.cwms.core.system.entity.template.Template;

/**
 * Entity that maps document {@link Template}s onto {@link InvoiceType}s. It is
 * intended that each {@link InvoiceType} should have it's own template. This
 * entity also allows definition of specific invoice templates, for specific
 * companies and for specific types.
 * 
 * @author Richard
 */
@Entity
@Table(name = "invoicedoctypetemplate")
public class InvoiceDoctypeTemplate extends ComponentDoctypeTemplate
{
	private InvoiceType type;
	private Template supplementaryTemplate;
	private String description;
	private Company company;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", nullable = true)
	public Company getCompany()
	{
		return this.company;
	}

	@Length(max = 500)
	@Column(name = "description", length = 500, nullable = true)
	public String getDescription()
	{
		return this.description;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "suptemplateid", nullable = true)
	public Template getSupplementaryTemplate()
	{
		return this.supplementaryTemplate;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "typeid", nullable = false)
	public InvoiceType getType()
	{
		return this.type;
	}

	public void setCompany(Company company)
	{
		this.company = company;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setSupplementaryTemplate(Template supplementaryTemplate)
	{
		this.supplementaryTemplate = supplementaryTemplate;
	}

	public void setType(InvoiceType type)
	{
		this.type = type;
	}
}
