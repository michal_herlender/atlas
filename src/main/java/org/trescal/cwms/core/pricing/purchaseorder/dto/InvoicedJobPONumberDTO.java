package org.trescal.cwms.core.pricing.purchaseorder.dto;

import java.util.List;

import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.po.PO;

import lombok.Data;

@Data
public class InvoicedJobPONumberDTO {

	private Job job;
	private List<BPO> bpos;
	private List<PO> pos;
}