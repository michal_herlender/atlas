package org.trescal.cwms.core.pricing.invoice.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CopyInvoiceForm {
	
	private int sourceInvoiceId;
	private int invoiceTypeId;
	private int invoiceAddressId;
	
}
