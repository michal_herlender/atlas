package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.JobCostingClientApproval;

public interface JobCostingClientApprovalService extends BaseService<JobCostingClientApproval, Integer>
{
	
}