package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.ClientCompanyDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressAction_;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.tools.DateTools;

@Repository
public class PurchaseOrderItemDaoImpl extends BaseDaoImpl<PurchaseOrderItem, Integer> implements PurchaseOrderItemDao {

	@Override
	protected Class<PurchaseOrderItem> getEntity() {
		return PurchaseOrderItem.class;
	}

	@Override
	public List<PurchaseOrderItemDTO> findAllFromPurchaseOrder(Integer poId) {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrderItemDTO> cq = cb.createQuery(PurchaseOrderItemDTO.class);
			Root<PurchaseOrder> order = cq.from(PurchaseOrder.class);
			Join<PurchaseOrder, PurchaseOrderItem> item = order.join(PurchaseOrder_.items,
					javax.persistence.criteria.JoinType.LEFT);
			Join<PurchaseOrderItem, PurchaseOrderJobItem> poJobItem = item.join(PurchaseOrderItem_.linkedJobItems,
					javax.persistence.criteria.JoinType.LEFT);
			Join<PurchaseOrderJobItem, JobItem> jobItem = poJobItem.join(PurchaseOrderJobItem_.ji,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job, javax.persistence.criteria.JoinType.LEFT);
			Join<PurchaseOrder, SupportedCurrency> currency = order.join(PurchaseOrder_.currency,
					javax.persistence.criteria.JoinType.LEFT);
			cq.where(cb.equal(order.get(PurchaseOrder_.id), poId));
			cq.select(cb.construct(PurchaseOrderItemDTO.class, item.get(PurchaseOrderItem_.id),
					item.get(PurchaseOrderItem_.itemno), item.get(PurchaseOrderItem_.description),
					currency.get(SupportedCurrency_.currencyERSymbol), item.get(PurchaseOrderItem_.generalDiscountRate),
					item.get(PurchaseOrderItem_.finalCost), jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), job.get(Job_.jobno)));
			return cq;
		});
	}

	@Override
	public List<PurchaseOrderItem> getAllSubcontracting(Company subcontractor, Date begin, Date end,
			PurchaseOrderItemReceiptStatus receiptStatus, Boolean invoiced) {
		return getResultList(cb -> {
			CriteriaQuery<PurchaseOrderItem> cq = cb.createQuery(PurchaseOrderItem.class);
			Root<PurchaseOrderItem> root = cq.from(PurchaseOrderItem.class);
			Join<PurchaseOrderItem, PurchaseOrderItemProgressAction> progressAction = root
					.join(PurchaseOrderItem_.progressActions, JoinType.INNER);
			Predicate actionClauses = cb.conjunction();
			actionClauses.getExpressions()
					.add(cb.equal(progressAction.get(PurchaseOrderItemProgressAction_.receiptStatus), receiptStatus));
			if (begin != null)
				actionClauses.getExpressions()
						.add(cb.greaterThanOrEqualTo(progressAction.get(PurchaseOrderItemProgressAction_.date), begin));
			if (end != null)
				actionClauses.getExpressions()
						.add(cb.lessThanOrEqualTo(progressAction.get(PurchaseOrderItemProgressAction_.date),
								DateTools.add(end, Calendar.DATE, 1)));
			progressAction.on(actionClauses);
			Join<PurchaseOrderItem, PurchaseOrder> order = root.join(PurchaseOrderItem_.order);
			Join<PurchaseOrder, Contact> contact = order.join(PurchaseOrder_.contact);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			root.fetch(PurchaseOrderItem_.linkedJobItems).fetch(PurchaseOrderJobItem_.ji);
			cq.select(root).distinct(true);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(PurchaseOrderItem_.receiptStatus), receiptStatus));
			if (invoiced != null)
				if (invoiced)
					clauses.getExpressions().add(cb.isNotNull(root.get(PurchaseOrderItem_.internalInvoice)));
				else
					clauses.getExpressions().add(cb.isNull(root.get(PurchaseOrderItem_.internalInvoice)));
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.comp), subcontractor));
			clauses.getExpressions().add(cb.notEqual(order.get(PurchaseOrder_.organisation), subcontractor));
			cq.where(clauses);
			return cq;
		});
	}
	
		@Override
		public List<ClientCompanyDTO> getClientCompanyDTOFromPO(Integer poId){
			return getResultList(cb ->{
				CriteriaQuery<ClientCompanyDTO> cq = cb.createQuery(ClientCompanyDTO.class);
				Root<PurchaseOrder> order = cq.from(PurchaseOrder.class);
				Join<PurchaseOrder, PurchaseOrderItem> item = order.join(PurchaseOrder_.items,
						javax.persistence.criteria.JoinType.LEFT);
				Join<PurchaseOrderItem, PurchaseOrderJobItem> poJobItem = item.join(PurchaseOrderItem_.linkedJobItems,
						javax.persistence.criteria.JoinType.LEFT);
				Join<PurchaseOrderJobItem, JobItem> jobItem = poJobItem.join(PurchaseOrderJobItem_.ji,
						javax.persistence.criteria.JoinType.LEFT);
				Join<JobItem, Job> job = jobItem.join(JobItem_.job, javax.persistence.criteria.JoinType.LEFT);
				Join<Job, Contact> contact = job.join(Job_.con);
				Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
				Join<Subdiv, Company> clientCompany = subdiv.join(Subdiv_.comp);
				Join<Company, CompanySettingsForAllocatedCompany> settings = clientCompany
						.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
				settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation),
						order.get(PurchaseOrder_.organisation)));
				cq.where(cb.equal(order.get(PurchaseOrder_.id), poId));
				cq.select(cb.construct(ClientCompanyDTO.class, clientCompany.get(Company_.coid),
						jobItem.get(JobItem_.itemNo), settings.get(CompanySettingsForAllocatedCompany_.requireSupplierCompanyList)
						));
				cq.distinct(true);
				return cq;
			});
		}
}