package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;

@Entity
@Table(name = "purchaseorderjobitem", uniqueConstraints = { @UniqueConstraint(columnNames = { "jobitemid", "poitemid" }) })
public class PurchaseOrderJobItem extends Auditable
{
	private int id;
	private JobItem ji;
	private PurchaseOrderItem poitem;
	
	public PurchaseOrderJobItem() {};
	
	public PurchaseOrderJobItem(JobItem jobItem, PurchaseOrderItem poItem) {
		this.ji = jobItem;
		this.poitem = poItem;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getJi()
	{
		return this.ji;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "poitemid", nullable = false)
	public PurchaseOrderItem getPoitem()
	{
		return this.poitem;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}

	public void setPoitem(PurchaseOrderItem poitem)
	{
		this.poitem = poitem;
	}
}