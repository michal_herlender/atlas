package org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.db;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewLinkedRepairCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationTypeEnum;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.enums.PartsMarkupSource;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostServiceSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingLinkedRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeAction;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeWrapper;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;
import org.trescal.cwms.core.tools.MathTools;

@Service("JobCostingRepairCostServiceImpl")
public class JobCostingRepairCostServiceImpl extends JobCostingCostServiceSupport<JobCostingRepairCost>
		implements JobCostingRepairCostService {

	@Autowired
    private FaultReportService faultReportService;
    @Autowired
    private RepairInspectionReportService rirService;

    @Autowired
    private JobCostingRepairCostDao JobCostingRepairCostDao;
    @Value("${cwms.config.costing.costs.lookuphierarchy.repair}")
    private String costSourceHierarchy;
    @Value("${cwms.config.costing.costs.lookupcosts.repair}")
    private boolean lookupCosts;
    @Value("${cwms.config.costing.costs.deactivateonzero.repair}")
    protected boolean deactivateCostOnZeroValue;

    @Override
    public boolean getLookupCosts() {
        return lookupCosts;
    }

    @Override
    public boolean getDeactivateCostOnZeroValue() {
        return deactivateCostOnZeroValue;
    }

    @Override
    public String getCostSourceHierarchy() {
        return costSourceHierarchy;
    }

    @Override
    public void setCostSourceHierarchy(String costSourceHierarchy) {
        this.costSourceHierarchy = costSourceHierarchy;
    }

	@Override
	public Cost createCostFromCopy(JobCostingItem ji, JobCostingItem old) {
		JobCostingRepairCost newCost = null;

		if ((old != null) && (old.getRepairCost() != null)) {
			newCost = new JobCostingRepairCost();

			JobCostingRepairCost oldRepCost = old.getRepairCost();
			BeanUtils.copyProperties(oldRepCost, newCost);

			newCost.setCostid(null);
			newCost.setJobCostingItem(ji);
			newCost.setAutoSet(true);

			if (oldRepCost.getLinkedCost() != null) {
				JobCostingLinkedRepairCost oldLinkedCost = oldRepCost.getLinkedCost();
				JobCostingLinkedRepairCost newLinkedCost = new JobCostingLinkedRepairCost();
				BeanUtils.copyProperties(oldLinkedCost, newLinkedCost);
				newLinkedCost.setId(0);
				newLinkedCost.setRepairCost(newCost);
				newCost.setLinkedCost(newLinkedCost);
			}
		}
		return newCost;
	}

	public void deleteJobCostingRepairCost(JobCostingRepairCost jobcostingrepaircost) {
		this.JobCostingRepairCostDao.remove(jobcostingrepaircost);
	}

	public JobCostingRepairCost findJobCostingRepairCost(int id) {
		return this.JobCostingRepairCostDao.find(id);
	}

	@Override
	public List<JobCostingRepairCost> findMatchingCosts(Integer plantid, Integer coid, int modelid, Integer caltypeid,
			Integer page, Integer resPerPage) {
		return this.JobCostingRepairCostDao.findMatchingCosts(JobCostingRepairCost.class, plantid, coid, modelid,
				caltypeid, this.resultsYearFilter, page, resPerPage);
	}

	public List<JobCostingRepairCost> getAllJobCostingRepairCosts() {
		return this.JobCostingRepairCostDao.findAll();
	}

	public void insertJobCostingRepairCost(JobCostingRepairCost JobCostingRepairCost) {
		this.JobCostingRepairCostDao.persist(JobCostingRepairCost);
	}

	@Override
	public Cost resolveCostLookup(CostSource source, JobCostingItem jci) {
		JobCostingRepairCost cost = null;

		this.logger.debug("loading job costing repair costs from " + source);
		switch (source) {
		case CONTRACT_REVIEW:
			this.logger.debug("Attempting to set costs using " + source + " strategy");

			JobItem ji = jci.getJobItem();
			if ((ji.getRepairCost() != null) && ji.getRepairCost().isActive()) {
				ContractReviewRepairCost repCost = ji.getRepairCost();
				cost = new JobCostingRepairCost();

				cost.setCostSrc(repCost.getCostSrc());

				BigDecimal rate = ji.getJob().getOrganisation().getComp().getBusinessSettings().getHourlyRate();
				cost.setHourlyRate(rate == null ? new BigDecimal("0.00") : rate);
				this.logger.debug("Hourly rate set as " + cost.getHourlyRate());
				cost.setLabourCostTotal(new BigDecimal(0.00));
				cost.setLabourTime(0);

				cost.setPartsCost(new BigDecimal(0.00));
				cost.setPartsMarkupRate(new BigDecimal(0.00));
				cost.setPartsMarkupValue(new BigDecimal(0.00));
				cost.setPartsTotal(new BigDecimal(0.00));

				// use the costs set at contract review
				cost.setDiscountRate(repCost.getDiscountRate());
				cost.setDiscountValue(repCost.getDiscountValue());
				cost.setHouseCost(repCost.getTotalCost());
				cost.setTotalCost(repCost.getTotalCost());
				cost.setFinalCost(repCost.getFinalCost());
				cost.setPartsMarkupSource(PartsMarkupSource.SYSTEM_DERIVED);

				// set empty third party costs
				this.configureTPCosts(cost);

				// if a linked cost is being used, link the contract review
				// linked cost to the jobcosting linked cost
				if (ji.getRepairCost().getLinkedCost() != null) {
					ContractReviewLinkedRepairCost cr = ji.getRepairCost().getLinkedCost();

					JobCostingLinkedRepairCost lc = new JobCostingLinkedRepairCost();
					lc.setRepairCost(cost);
					if (repCost.getCostSrc() == CostSource.JOB_COSTING) {
						lc.setJobCostingRepairCost(cr.getJobCostingRepairCost());
					}
				}
			} else {
				this.logger.debug("Failed setting costs using " + source + " strategy, no costs found");
			}

			break;
		case JOB:
			this.logger.debug(source + " costs are not yet implemented");
			break;
		case JOB_COSTING:
			this.logger.debug(source + " costs are not yet implemented");
			break;
		case QUOTATION:
			this.logger.debug(source + " costs are not supported");
			break;
		case INSTRUMENT:
			this.logger.debug(source + " costs are not supported");
			break;
		case MODEL:
			this.logger.debug(source + " costs are not supported");
			break;
		case DERIVED:
			this.logger.debug("Attempting to set costs using " + source + " strategy");
			cost = derivedCostLookup(jci);
			break;
		case MANUAL:
			cost = new JobCostingRepairCost();

			// work out the hourly rate to use for this item
			ji = jci.getJobItem();
			BigDecimal rate = ji.getJob().getOrganisation().getComp().getBusinessSettings().getHourlyRate();
			cost.setHourlyRate(rate == null ? new BigDecimal("0.00") : rate);
			this.logger.debug("Hourly rate set as " + cost.getHourlyRate());
			cost.setLabourCostTotal(new BigDecimal(0.00));
			cost.setLabourTime(0);

			cost.setPartsCost(new BigDecimal(0.00));
			cost.setPartsMarkupSource(PartsMarkupSource.SYSTEM_DERIVED);
			cost.setPartsMarkupRate(new BigDecimal(0.00));
			cost.setPartsMarkupValue(new BigDecimal(0.00));
			cost.setPartsTotal(new BigDecimal(0.00));

			cost.setDiscountRate(new BigDecimal(0.00));
			cost.setDiscountValue(new BigDecimal(0.00));
			cost.setFinalCost(new BigDecimal(0.00));
			cost.setTotalCost(new BigDecimal(0.00));
			cost.setCostSrc(CostSource.MANUAL);

			// set empty third party costs
			this.configureTPCosts(cost);

			break;
		default:
			break;
		}
		if (cost != null) {
			cost.setActive(true);
			cost.setAutoSet(true);
			cost.setJobCostingItem(jci);
			cost.setCostType(CostType.REPAIR);
		}
		return cost;
	}
	
	/**
	 * NEW derived cost lookup that uses repair evaluation / repair inspection
	 * in future, rates from contract used for job item could be used if supported	
	 * @param jci
	 * @return
	 */
	private JobCostingRepairCost derivedCostLookup(JobCostingItem jci) {
		JobCostingRepairCost cost = null;
		RepairInspectionReport rir = this.rirService.getLatestValidatedRir(jci.getJobItem().getJobItemId());
		if (rir != null) {
			this.logger.debug("Setting derived repair cost from latest repair inspection");
			cost = new JobCostingRepairCost();
			cost.setDiscountRate(new BigDecimal(0.00));
			cost.setDiscountValue(new BigDecimal(0.00));
			cost.setHourlyRate(calculateLabourRate(rir));
			cost.setLabourTime(calculateLabourTime(rir));

			// calculate labour cost (labour time * hourly rate)
			BigDecimal labTimeMins = new BigDecimal(cost.getLabourTime()).divide(new BigDecimal("60"),
					MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
			BigDecimal labCost = cost.getHourlyRate().multiply(labTimeMins);
			cost.setLabourCostTotal(labCost.setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));

			cost.setPartsCost(calculatePartsCost(rir));
			// find the markup rate
			MarkupRangeWrapper muWrapper = this.markupRangeServ.calculateMarkUpRangeValue(cost.getPartsCost(),
					MarkupType.SUPPLIER);
			cost.setPartsMarkupValue(muWrapper.getValue());

			// only set the markuprate if the markup range action is a
			// percentage and not a fixed value
			if (muWrapper.getRange() != null) {
				if (muWrapper.getRange().getAction() == MarkupRangeAction.PERCENTAGE) {
					cost.setPartsMarkupRate(new BigDecimal(muWrapper.getRange().getActionValue()));
				}
			}
			
			// set the total value of the repair parts
			cost.setPartsTotal(cost.getPartsCost().add(cost.getPartsMarkupValue()));
			cost.setPartsMarkupSource(PartsMarkupSource.SYSTEM_DERIVED);

			cost.setHouseCost(cost.getLabourCostTotal().add(cost.getPartsTotal()));
			cost.setTotalCost(cost.getHouseCost());
			cost.setFinalCost(cost.getTotalCost());
			cost.setCostSrc(CostSource.DERIVED);

			// set empty third party costs
			this.configureTPCosts(cost);
		}
		else {
			this.logger.debug("No validated repair inspection, therefore no derived cost");
		}
		return cost;
	}
	
	/**
	 * Calculates the labour rate per hour
	 * Currently, the job organisation's hourly rate is used; discuss and confirm
	 */
	private BigDecimal calculateLabourRate(RepairInspectionReport rir) {
		BigDecimal rate = rir.getJi().getJob().getOrganisation().getComp().getBusinessSettings().getHourlyRate();
		if (rate == null) rate = new BigDecimal("0.00");
		this.logger.debug("Hourly rate set as " + rate);
		return rate;
	}
	
	/**
	 * Calculates the labour time in minutes for the internal operations connected to an RIR
	 * @return
	 */
	private int calculateLabourTime(RepairInspectionReport rir) {
		int result = rir.getFreeRepairOperations().stream().
			filter(operation -> operation.getLabourTime() != null).
			filter(operation -> RepairOperationTypeEnum.INTERNAL.equals(operation.getOperationType())).
			mapToInt(FreeRepairOperation::getLabourTime).
			sum();
		this.logger.debug( "Setting repair time from rir [" + result + "] minutes for internal operations");
		return result;
	}

	/**
	 * Calculates the total cost of all components attached to RIR
	  * To discuss / confirm - is the "cost" of a component unitary or total?
	 * @param rir
	 * @return
	 */
	
	private BigDecimal calculatePartsCost(RepairInspectionReport rir) {
		BigDecimal externalResult = rir.getFreeRepairOperations().stream()
				.filter(operation -> (operation.getOperationType().equals(RepairOperationTypeEnum.EXTERNAL) || 
						operation.getOperationType().equals(RepairOperationTypeEnum.EXTERNAL_WITH_CALIBRATION)) && operation.getCost() != null)
				.map(fro -> fro.getCost()).reduce(BigDecimal.ZERO, BigDecimal::add);

		this.logger.debug( "Setting parts costs from rir as [" + externalResult + "]");
		return externalResult;
	}
	
	/**
	 * OLD derived cost lookup that used failure report
	 * @param jci
	 * @return cost, if it can be resolved
	 */
	@Deprecated
	private JobCostingRepairCost oldDerivedCostLookup(JobCostingItem jci) {
		JobCostingRepairCost cost = null;
		
		JobItem ji = jci.getJobItem();
		FaultReport fr = faultReportService.getLastFailureReportByJobitemId(ji.getJobItemId(), true);

		// this function looks for a fault report for the jobitem and
		// calculates a repair cost based on this. If not fault report
		// is found then a 'no cost' is found and a manual cost will be
		// loaded instead.
		if (fr != null) {
			// try and calculate repair costs using a fault report entry
			cost = new JobCostingRepairCost();
			cost.setDiscountRate(new BigDecimal(0.00));
			cost.setDiscountValue(new BigDecimal(0.00));

			// set the labour time
			this.logger.debug(
					"Setting repair time from fault report [" + fr.getEstRepairTime() + "] minutes");
			cost.setLabourTime(
					fr.getEstRepairTime() == null ? 0 : fr.getEstRepairTime());

			BigDecimal rate = ji.getJob().getOrganisation().getComp().getBusinessSettings().getHourlyRate();
			cost.setHourlyRate(rate == null ? new BigDecimal("0.00") : rate);
			this.logger.debug("Hourly rate set as " + cost.getHourlyRate());

			// calculate labour cost (labour time * hourly rate)
			BigDecimal labTimeMins = new BigDecimal(cost.getLabourTime()).divide(new BigDecimal("60"),
					MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
			BigDecimal labCost = cost.getHourlyRate().multiply(labTimeMins);
			cost.setLabourCostTotal(labCost.setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));

			// calculate any parts + markup costs
			cost.setPartsCost(
					fr.getCost() == null ? new BigDecimal("0.00") : fr.getCost());

			// find the markup rateb
			MarkupRangeWrapper muWrapper = this.markupRangeServ.calculateMarkUpRangeValue(cost.getPartsCost(),
					MarkupType.SUPPLIER);
			cost.setPartsMarkupValue(muWrapper.getValue());

			// only set the markuprate if the markup range action is a
			// percentage and not a fixed value
			if (muWrapper.getRange() != null) {
				if (muWrapper.getRange().getAction() == MarkupRangeAction.PERCENTAGE) {
					cost.setPartsMarkupRate(new BigDecimal(muWrapper.getRange().getActionValue()));
				}
			}

			// set the total value of the repair cost
			cost.setPartsTotal(cost.getPartsCost().add(cost.getPartsMarkupValue()));
			cost.setPartsMarkupSource(PartsMarkupSource.SYSTEM_DERIVED);

			cost.setHouseCost(cost.getLabourCostTotal().add(cost.getPartsTotal()));
			cost.setTotalCost(cost.getHouseCost());
			cost.setFinalCost(cost.getTotalCost());
			cost.setCostSrc(CostSource.DERIVED);

			// set empty third party costs
			this.configureTPCosts(cost);
		} else {
			this.logger.debug("No fault report found for jobitem, returning no cost from " + CostSource.DERIVED);
		}
		return cost;
	}

	public void saveOrUpdateJobCostingRepairCost(JobCostingRepairCost jobcostingrepaircost) {
		this.JobCostingRepairCostDao.saveOrUpdate(jobcostingrepaircost);
	}

	public void setJobCostingRepairCostDao(JobCostingRepairCostDao JobCostingRepairCostDao) {
		this.JobCostingRepairCostDao = JobCostingRepairCostDao;
	}

	@Override
	public JobCostingRepairCost updateCosts(JobCostingRepairCost cost) {
		if (cost.getLabourTime() == null) {
			cost.setLabourTime(0);
		}
		if (cost.getHourlyRate() == null) {
			cost.setHourlyRate(new BigDecimal("0.00"));
		}
		if (cost.getPartsCost() == null) {
			cost.setPartsCost(new BigDecimal("0.00"));
		}
		if (cost.getPartsMarkupRate() == null) {
			cost.setPartsMarkupRate(new BigDecimal("0.00"));
		}
		if (cost.getPartsMarkupValue() == null) {
			cost.setPartsMarkupValue(new BigDecimal("0.00"));
		}
		if (cost.getThirdCostTotal() == null) {
			cost.setThirdCostTotal(new BigDecimal("0.00"));
		}

		// calculate labour cost (labour time * hourly rate) - we assume that
		// the labour cost + hourly rate have already been set on the cost
		BigDecimal labTimeMins = new BigDecimal(cost.getLabourTime()).divide(new BigDecimal("60"),
				MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
		BigDecimal labCost = cost.getHourlyRate().multiply(labTimeMins);
		cost.setLabourCostTotal(labCost.setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));

		// check if we need to re-calculate the markup rate - only happens if
		// markup source is system derived
		if (cost.getPartsMarkupSource() == PartsMarkupSource.SYSTEM_DERIVED) {
			MarkupRangeWrapper muWrapper = this.markupRangeServ.calculateMarkUpRangeValue(cost.getPartsCost(),
					MarkupType.SUPPLIER);
			cost.setPartsMarkupValue(muWrapper.getValue());

			// only set the markuprate if the markup range action is a
			// percentage and not a fixed value
			if (muWrapper.getRange() != null) {
				if (muWrapper.getRange().getAction() == MarkupRangeAction.PERCENTAGE) {
					cost.setPartsMarkupRate(new BigDecimal(muWrapper.getRange().getActionValue()));
				} else {
					cost.setPartsMarkupRate(null);
				}
			}
		} else {
			// using a manually entered markup rate - this is always considered
			// a %
			BigDecimal dec = cost.getPartsMarkupRate().divide(new BigDecimal("100.00"), MathTools.SCALE_FIN_CALC,
					MathTools.ROUND_MODE_FIN);
			cost.setPartsMarkupValue(
					(cost.getPartsCost().multiply(dec)).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
		}

		// calculate parts costs
		cost.setPartsTotal(cost.getPartsCost().add(cost.getPartsMarkupValue()));

		cost.setHouseCost(cost.getLabourCostTotal().add(cost.getPartsTotal()));

		// calculate TP costs
		cost = (JobCostingRepairCost) this.thirdCostService.recalculateThirdPartyCosts(cost);

		cost.setTotalCost(cost.getHouseCost().add(cost.getThirdCostTotal()));

		// delegate to CostCalculator to apply any discounts and set the final
		// cost
		CostCalculator.updateSingleCost(cost, this.roundUpFinalCosts);

		return cost;
	}

	public void updateJobCostingRepairCost(JobCostingRepairCost JobCostingRepairCost) {
		this.JobCostingRepairCostDao.update(JobCostingRepairCost);
	}
}