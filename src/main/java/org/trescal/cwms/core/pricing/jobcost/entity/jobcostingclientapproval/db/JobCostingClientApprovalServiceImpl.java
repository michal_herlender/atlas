package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.JobCostingClientApproval;

@Service("JobCostingClientApprovalService")
public class JobCostingClientApprovalServiceImpl extends BaseServiceImpl<JobCostingClientApproval, Integer> implements JobCostingClientApprovalService {

	@Autowired
	private JobCostingClientApprovalDao jobCostingClientApprovalDao;
	
	@Override
	protected BaseDao<JobCostingClientApproval, Integer> getBaseDao() {
		return this.jobCostingClientApprovalDao;
	}
	
}