package org.trescal.cwms.core.pricing.invoice.view;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand.JOB_LOAD_CONTACT;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand.JOB_LOAD_CURRENCY;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemNotInvoicedDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.*;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceTypeNames;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionCommand.*;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.reports.view.XlsxCellStyleHolder;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
public class InvoiceXlsxStreamingView extends AbstractXlsxStreamingView {

	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private InvoiceProjectionService invoiceProjectionService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private SupportedLocaleService localeService;
	@Autowired
	private TranslationService tranServ;
	
	private static boolean autosize = true;
	private static int colIndexLeft = 0;
	private static int colIndexMid = 1;
	private static int colIndexRight = 2;
	
	private static enum SheetType {INVOICE_ITEMS, PERIODIC_LINIKNG};
	private static enum SectionType {JOB_ITEMS, EXPENSE_ITEMS, OTHER_ITEMS};

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Integer invoiceId = (Integer) model.get("invoiceId");
		Invoice invoice = this.invoiceService.findInvoice(invoiceId);

		Locale locale =  LocaleContextHolder.getLocale();
		Locale fallbackLocale = this.localeService.getPrimaryLocale();
		
		Sheet sheet1 = workbook.createSheet(messageSource.getMessage(InvoiceXlsxDefinitions.CODE_INVOICE_ITEMS, null, InvoiceXlsxDefinitions.TEXT_INVOICE_ITEMS, locale));
		String titleJobItems = messageSource.getMessage(InvoiceXlsxDefinitions.CODE_JOB_ITEMS, null, InvoiceXlsxDefinitions.TEXT_JOB_ITEMS, locale);
		String titleExpenseItems = messageSource.getMessage(InvoiceXlsxDefinitions.CODE_EXPENSE_ITEMS, null, InvoiceXlsxDefinitions.TEXT_EXPENSE_ITEMS, locale);
		String titleOtherItems = messageSource.getMessage(InvoiceXlsxDefinitions.CODE_OTHER_ITEMS, null, InvoiceXlsxDefinitions.TEXT_OTHER_ITEMS, locale);
		
		SXSSFWorkbook wb = (SXSSFWorkbook) workbook;
		XlsxCellStyleHolder styleHolder = new XlsxCellStyleHolder(wb);
		XlsxViewDecorator decorator1 = new XlsxViewDecorator(sheet1, wb, autosize, styleHolder);
		
		int rowIndex1 = writeSummary(decorator1, invoice, null, SheetType.INVOICE_ITEMS, locale);
		
		rowIndex1++;
		rowIndex1 = writeHeader(decorator1, locale, titleJobItems, SheetType.INVOICE_ITEMS, SectionType.JOB_ITEMS, rowIndex1);
		rowIndex1 = writeJobItems(decorator1, invoice, locale, fallbackLocale, rowIndex1);
		
		rowIndex1++;
		rowIndex1 = writeHeader(decorator1, locale, titleExpenseItems, SheetType.INVOICE_ITEMS, SectionType.EXPENSE_ITEMS, rowIndex1);
		rowIndex1 = writeExpenseItems(decorator1, invoice, locale, fallbackLocale, rowIndex1);
		
		rowIndex1++;
		rowIndex1 = writeHeader(decorator1, locale, titleOtherItems, SheetType.INVOICE_ITEMS, SectionType.OTHER_ITEMS, rowIndex1);
		rowIndex1 = writeOtherItems(decorator1, invoice, locale, fallbackLocale, rowIndex1);
		decorator1.autoSizeColumns();
		
		if (invoice.getType().getName().equals(InvoiceTypeNames.PERIODIC)) {
			Sheet sheet2 = workbook.createSheet(messageSource.getMessage(InvoiceXlsxDefinitions.CODE_PERIODIC_LINKING, null, InvoiceXlsxDefinitions.TEXT_PERIODIC_LINKING, locale));
			XlsxViewDecorator decorator2 = new XlsxViewDecorator(sheet2, wb, autosize, styleHolder);
			int rowIndex2 = 0;
			
			// Ultimately use invoice projection for entire export; for now just for periodic linking
			Integer allocatedCompanyId = invoice.getOrganisation().getCoid();
			JobItemProjectionCommand jobItemCommand = new JobItemProjectionCommand(locale, allocatedCompanyId, 
					JI_LOAD_CONTRACT_REVIEW_PRICE.class, 
					JI_LOAD_COSTING_ITEMS.class, 
					JI_LOAD_INSTRUMENTS.class, 
					JI_LOAD_CLIENT_POs.class, 
					JI_LOAD_ON_BEHALF.class, 
					JI_LOAD_SERVICE_TYPES.class);			
			JobProjectionCommand jobCommand = new JobProjectionCommand(fallbackLocale, allocatedCompanyId, 
					JOB_LOAD_CONTACT.class,
					JOB_LOAD_CURRENCY.class);
			
			InvoiceProjectionCommand invoiceCommand = new InvoiceProjectionCommand(locale, allocatedCompanyId, 
					jobItemCommand, jobCommand,
					INV_LOAD_NOT_INVOICED_EXPENSE_ITEMS.class,
					INV_LOAD_NOT_INVOICED_EXPENSE_ITEM_CLIENT_POS.class,
					INV_LOAD_NOT_INVOICED_JOB_ITEMS.class,
					INV_LOAD_NOT_INVOICED_JOB_ITEM_DELIVERIES.class,
					INV_CALC_NOT_INVOICED_JOB_ITEM_ESTIMATES.class); 
			List<InvoiceProjectionDTO> invoiceDtos = this.invoiceProjectionService.getInvoiceProjectionDTOs(Collections.singleton(invoiceId), invoiceCommand);
			
			InvoiceProjectionDTO invoiceDto = invoiceDtos.get(0);
			
			rowIndex2 = writeSummary(decorator2, invoice, invoiceDto, SheetType.PERIODIC_LINIKNG, locale);

			rowIndex2++;
			rowIndex2 = writeHeader(decorator2, locale, titleJobItems, SheetType.PERIODIC_LINIKNG, SectionType.JOB_ITEMS, rowIndex2);
			rowIndex2 = writePeriodicJobItems(decorator2, invoiceDto, locale, rowIndex2);
			
			rowIndex2++;
			rowIndex2 = writeHeader(decorator2, locale, titleExpenseItems, SheetType.PERIODIC_LINIKNG, SectionType.EXPENSE_ITEMS, rowIndex2);
			rowIndex2 = writePeriodicExpenseItems(decorator2, invoiceDto, locale, rowIndex2);
			decorator2.autoSizeColumns();
		}
		
		String fileName = invoice.getInvno()+".xlsx";
		response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");		
	}
	
	private int writeSummary(XlsxViewDecorator decorator, Invoice invoice, InvoiceProjectionDTO invoiceDto, SheetType sheetType, Locale locale) {
		int rowIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();
		CellStyle styleMonetary = decorator.getStyleHolder().getMonetaryStyle();
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		
		decorator.createCell(rowIndex, colIndexLeft, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_INVOICE_NUMBER, null, InvoiceXlsxDefinitions.TEXT_INVOICE_NUMBER, locale), 
				styleHeader);
		decorator.createCell(rowIndex, colIndexMid, invoice.getInvno(), styleText);
		rowIndex++;
		
		decorator.createCell(rowIndex, colIndexLeft, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_INVOICE_TYPE, null, InvoiceXlsxDefinitions.TEXT_INVOICE_TYPE, locale), 
				styleHeader);
		decorator.createCell(rowIndex, colIndexMid, tranServ.getCorrectTranslation(invoice.getType().getNametranslation(), locale), styleText);
		rowIndex++;
		
		decorator.createCell(rowIndex, colIndexLeft, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_INVOICE_STATUS, null, InvoiceXlsxDefinitions.TEXT_INVOICE_STATUS, locale), 
				styleHeader);
		decorator.createCell(rowIndex, colIndexMid, tranServ.getCorrectTranslation(invoice.getStatus().getNametranslations(), locale), styleText);
		rowIndex++;
		
		if (sheetType.equals(SheetType.INVOICE_ITEMS)) {
			decorator.createCell(rowIndex, colIndexLeft, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_SUBTOTAL_JOB_ITEMS, null, InvoiceXlsxDefinitions.TEXT_SUBTOTAL_JOB_ITEMS, locale), 
					styleHeader);
			decorator.createCell(rowIndex, colIndexMid, getTotalJobItems(invoice), styleMonetary);
			decorator.createCell(rowIndex, colIndexRight, invoice.getCurrency().getCurrencyCode(), styleText);
			rowIndex++;
			
			decorator.createCell(rowIndex, colIndexLeft, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_SUBTOTAL_EXPENSE_ITEMS, null, InvoiceXlsxDefinitions.TEXT_SUBTOTAL_EXPENSE_ITEMS, locale), 
					styleHeader);
			decorator.createCell(rowIndex, colIndexMid, getTotalExpenseItems(invoice), styleMonetary);
			decorator.createCell(rowIndex, colIndexRight, invoice.getCurrency().getCurrencyCode(), styleText);
			rowIndex++;
			
			decorator.createCell(rowIndex, colIndexLeft, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_SUBTOTAL_OTHER_ITEMS, null, InvoiceXlsxDefinitions.TEXT_SUBTOTAL_OTHER_ITEMS, locale), 
					styleHeader);
			decorator.createCell(rowIndex, colIndexMid, getTotalOtherItems(invoice), styleMonetary);
			decorator.createCell(rowIndex, colIndexRight, invoice.getCurrency().getCurrencyCode(), styleText);
			rowIndex++;
		}
		else if (sheetType.equals(SheetType.PERIODIC_LINIKNG)) {
			decorator.createCell(rowIndex, colIndexLeft, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_LINKED_JOB_ITEM_VALUE, null, InvoiceXlsxDefinitions.TEXT_LINKED_JOB_ITEM_VALUE, locale), 
					styleHeader);
			decorator.createCell(rowIndex, colIndexMid, getTotalLinkedJobItems(invoiceDto), styleMonetary);
			decorator.createCell(rowIndex, colIndexRight, invoice.getCurrency().getCurrencyCode(), styleText);
			rowIndex++;

			decorator.createCell(rowIndex, colIndexLeft, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_LINKED_EXPENSE_ITEM_VALUE, null, InvoiceXlsxDefinitions.TEXT_LINKED_EXPENSE_ITEM_VALUE, locale), 
					styleHeader);
			decorator.createCell(rowIndex, colIndexMid, getTotalLinkedExpenseItems(invoiceDto), styleMonetary);
			decorator.createCell(rowIndex, colIndexRight, invoice.getCurrency().getCurrencyCode(), styleText);
			rowIndex++;
		}
		
		decorator.createCell(rowIndex, colIndexLeft, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_TOTAL_INVOICE_AMOUNT, null, InvoiceXlsxDefinitions.TEXT_TOTAL_INVOICE_AMOUNT, locale), 
				styleHeader);
		decorator.createCell(rowIndex, colIndexMid, invoice.getTotalCost(), styleMonetary);
		decorator.createCell(rowIndex, colIndexRight, invoice.getCurrency().getCurrencyCode(), styleText);
		rowIndex++;
		
		return rowIndex;
	}
	
	public BigDecimal getTotalJobItems(Invoice invoice) {
		BigDecimal result = new BigDecimal("0.00");
		for (InvoiceItem ii : invoice.getItems()) {
			if (ii.getJobItem() != null)
				result = result.add(ii.getFinalCost());
		}
		return result;
	}
	
	public BigDecimal getTotalExpenseItems(Invoice invoice) {
		BigDecimal result = new BigDecimal("0.00");
		for (InvoiceItem ii : invoice.getItems()) {
			if (ii.getExpenseItem() != null)
				result = result.add(ii.getFinalCost());
		}
		return result;
	}
	
	public BigDecimal getTotalOtherItems(Invoice invoice) {
		BigDecimal result = new BigDecimal("0.00");
		for (InvoiceItem ii : invoice.getItems()) {
			if (ii.getExpenseItem() == null && ii.getJobItem() == null)
				result = result.add(ii.getFinalCost());
		}
		return result;
	}
	
	public BigDecimal getTotalLinkedExpenseItems(InvoiceProjectionDTO invoiceDto) {
		BigDecimal result = new BigDecimal("0.00");
		// Note, we assume that the job currency of any linked expense items is same as the invoice currency
		if (invoiceDto.getPeriodicExpenseItems() != null) {
			for (JobExpenseItemNotInvoicedDTO einiDto : invoiceDto.getPeriodicExpenseItems()) {
				result = result.add(einiDto.getExpenseItem().getTotalPrice());
			}
		}
		return result;
	}
	
	public BigDecimal getTotalLinkedJobItems(InvoiceProjectionDTO invoiceDto) {
		BigDecimal result = new BigDecimal("0.00");
		// Note, we assume for this total that the job currency of any linked job items is same as the invoice currency
		if (invoiceDto.getPeriodicJobItems() != null) {
			for (JobItemNotInvoicedDTO jiniDto : invoiceDto.getPeriodicJobItems()) {
				result = result.add(jiniDto.getJobItem().getEstimatedPrice().getPrice());
			}
		}
		return result;
	}
	
	
	private int writeHeader(XlsxViewDecorator decorator, Locale locale, String titleText, SheetType sheetType, SectionType sectionType, int rowIndex) {
		int colIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		
		decorator.createCell(rowIndex, colIndex++, titleText, styleText);
		rowIndex++;
		colIndex = 0;
		
		String firstColCode = InvoiceXlsxDefinitions.CODE_INVOICE_ITEM_NO;
		String firstColText = InvoiceXlsxDefinitions.TEXT_INVOICE_ITEM_NO;
		if (sheetType.equals(SheetType.PERIODIC_LINIKNG)) {
			firstColCode = InvoiceXlsxDefinitions.CODE_SEQUENCE_NO;
			firstColText = InvoiceXlsxDefinitions.TEXT_SEQUENCE_NO;
		}
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(firstColCode, null, firstColText, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_CLIENT_COMPANY, null, InvoiceXlsxDefinitions.TEXT_CLIENT_COMPANY, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_ON_BEHALF_OF, null, InvoiceXlsxDefinitions.TEXT_ON_BEHALF_OF, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_CLIENT_SUBDIVISION, null, InvoiceXlsxDefinitions.TEXT_CLIENT_SUBDIVISION, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_JOB_NUMBER, null, InvoiceXlsxDefinitions.TEXT_JOB_NUMBER, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_JOB_ITEM_NO, null, InvoiceXlsxDefinitions.TEXT_JOB_ITEM_NO, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_JOB_SERVICE_NO, null, InvoiceXlsxDefinitions.TEXT_JOB_SERVICE_NO, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_JOB_CONTACT, null, InvoiceXlsxDefinitions.TEXT_JOB_CONTACT, locale), 
				styleHeader);
		
		if (SectionType.OTHER_ITEMS.equals(sectionType)) {
			decorator.createCell(rowIndex, colIndex++, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_ITEM_DESCRIPTION, null, InvoiceXlsxDefinitions.TEXT_ITEM_DESCRIPTION, locale), 
					styleHeader);
		}
		else {
			decorator.createCell(rowIndex, colIndex++, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_INSTRUMENT_MODEL_NAME, null, InvoiceXlsxDefinitions.TEXT_INSTRUMENT_MODEL_NAME, locale), 
					styleHeader);
		}
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_CLIENT_DESCRIPTION, null, InvoiceXlsxDefinitions.TEXT_CLIENT_DESCRIPTION, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_TRESCAL_ID, null, InvoiceXlsxDefinitions.TEXT_TRESCAL_ID, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_CLIENT_ID, null, InvoiceXlsxDefinitions.TEXT_CLIENT_ID, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_SERIAL_NUMBER, null, InvoiceXlsxDefinitions.TEXT_SERIAL_NUMBER, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_SERVICE_TYPE, null, InvoiceXlsxDefinitions.TEXT_SERVICE_TYPE, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_DELIVERY_DATE, null, InvoiceXlsxDefinitions.TEXT_DELIVERY_DATE, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_DELIVERY_NUMBER, null, InvoiceXlsxDefinitions.TEXT_DELIVERY_NUMBER, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_JOB_CLIENT_REF, null, InvoiceXlsxDefinitions.TEXT_JOB_CLIENT_REF, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_JOB_ITEM_CLIENT_REF, null, InvoiceXlsxDefinitions.TEXT_JOB_ITEM_CLIENT_REF, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_CLIENT_PURCHASE_ORDER, null, InvoiceXlsxDefinitions.TEXT_CLIENT_PURCHASE_ORDER, locale), 
				styleHeader);
		
		if (SheetType.PERIODIC_LINIKNG.equals(sheetType)) {
			if (SectionType.EXPENSE_ITEMS.equals(sectionType)) {
				decorator.createCell(rowIndex, colIndex++, 
						messageSource.getMessage(InvoiceXlsxDefinitions.CODE_UNIT_PRICE, null, InvoiceXlsxDefinitions.TEXT_UNIT_PRICE, locale), 
						styleHeader);

				decorator.createCell(rowIndex, colIndex++, 
						messageSource.getMessage(InvoiceXlsxDefinitions.CODE_QUANTITY, null, InvoiceXlsxDefinitions.TEXT_QUANTITY, locale), 
						styleHeader);
			}
			else {
				decorator.createCell(rowIndex, colIndex++, 
						messageSource.getMessage(InvoiceXlsxDefinitions.CODE_PRICE_SOURCE, null, InvoiceXlsxDefinitions.TEXT_PRICE_SOURCE, locale), 
						styleHeader);

				decorator.createCell(rowIndex, colIndex++, 
						messageSource.getMessage(InvoiceXlsxDefinitions.CODE_VERSION, null, InvoiceXlsxDefinitions.TEXT_VERSION, locale), 
						styleHeader);
			}
		}
		
		if(SheetType.INVOICE_ITEMS.equals(sheetType)) {
			
			decorator.createCell(rowIndex, colIndex++, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_COMMENT, null, InvoiceXlsxDefinitions.TEXT_COMMENT, locale), 
					styleHeader);
			
			decorator.createCell(rowIndex, colIndex++, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_SINGLE_PRICE, null, InvoiceXlsxDefinitions.TEXT_SINGLE_PRICE, locale), 
					styleHeader);
			
			decorator.createCell(rowIndex, colIndex++, 
					messageSource.getMessage(InvoiceXlsxDefinitions.CODE_QUANTITY, null, InvoiceXlsxDefinitions.TEXT_QUANTITY, locale), 
					styleHeader);
			
		}
		
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_FINAL_PRICE, null, InvoiceXlsxDefinitions.TEXT_FINAL_PRICE, locale), 
				styleHeader);
		
		decorator.createCell(rowIndex, colIndex++, 
				messageSource.getMessage(InvoiceXlsxDefinitions.CODE_CURRENCY, null, InvoiceXlsxDefinitions.TEXT_CURRENCY, locale), 
				styleHeader);
		
		rowIndex++;
		return rowIndex;
	}
	
	/**
	 * 
	 * @return rowIndex
	 */
	private int writeJobItems(XlsxViewDecorator decorator, Invoice invoice, Locale locale, Locale fallbackLocale, int rowIndex) {
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleMonetary = decorator.getStyleHolder().getMonetaryStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();
		CellStyle styleDate = decorator.getStyleHolder().getDateStyle();
		
		for (InvoiceItem invoiceItem : invoice.getItems()) {
			JobItem jobItem = invoiceItem.getJobItem();
			
			if (jobItem != null) {
				Job job = jobItem.getJob();
				Instrument instrument = jobItem.getInst();
				int colIndex = 0;
				decorator.createCell(rowIndex, colIndex++, invoiceItem.getItemno(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, job.getCon().getSub().getComp().getConame(), styleText);
				if (jobItem.getOnBehalf() != null) {
					decorator.createCell(rowIndex, colIndex, jobItem.getOnBehalf().getCompany().getConame(), styleText);
				}
				colIndex++;
				decorator.createCell(rowIndex, colIndex++, job.getCon().getSub().getSubname(), styleText);
				decorator.createCell(rowIndex, colIndex++, job.getJobno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getItemNo(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, job.getCon().getName(), styleText);
				
				decorator.createCell(rowIndex, colIndex++, InstModelTools.instrumentModelNameViaTranslations(instrument, false, locale, fallbackLocale), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInst().getCustomerDescription(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInst().getPlantid(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInst().getPlantno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInst().getSerialno(), styleText);
				decorator.createCell(rowIndex, colIndex++, tranServ.getCorrectTranslation(jobItem.getCalType().getServiceType().getLongnameTranslation(),locale), styleText);
				
				// TODO get last client delivery from projection 
				JobDeliveryItem clientDeliveryItem = jobItem.getDeliveryItems().stream()
					.filter(di -> di.getDelivery().getType().equals(DeliveryType.CLIENT))
					.findAny().orElse(null);
				
				if(clientDeliveryItem != null)
				{ 
					decorator.createCell(rowIndex, colIndex++, jobItem.getDateComplete(), styleDate);
					decorator.createCell(rowIndex, colIndex++, clientDeliveryItem.getDelivery().getDeliveryno(), styleText);	
				}
				else {
					colIndex += 2;
				}
				
				if (job.getClientRef() != null && !job.getClientRef().isEmpty()) {
					decorator.createCell(rowIndex, colIndex, job.getClientRef(), styleText);
				}
				colIndex++;
				if (jobItem.getClientRef() != null && !jobItem.getClientRef().isEmpty()) {
					decorator.createCell(rowIndex, colIndex, jobItem.getClientRef(), styleText);
				}
				colIndex++;
				
				String formattedPOs = getFormattedPOs(invoiceItem);
				if (!formattedPOs.isEmpty()) {
					decorator.createCell(rowIndex, colIndex, formattedPOs, styleText);
				}
				colIndex++;
				
				colIndex += 3;
				decorator.createCell(rowIndex, colIndex++, invoiceItem.getFinalCost(), styleMonetary);
				decorator.createCell(rowIndex, colIndex++, invoice.getCurrency().getCurrencyCode(), styleText);
	
				rowIndex++;
			}
		}
		return rowIndex;
	}
		
	/**
	 * 
	 * @return rowIndex
	 */
	private int writeExpenseItems(XlsxViewDecorator decorator, Invoice invoice, Locale locale, Locale fallbackLocale, int rowIndex) {
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleMonetary = decorator.getStyleHolder().getMonetaryStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

		for (InvoiceItem invoiceItem : invoice.getItems()){
			JobExpenseItem jobExpenseItem = invoiceItem.getExpenseItem();
			
			if(jobExpenseItem != null){	
				Job job = jobExpenseItem.getJob();
				int colIndex = 0;
				decorator.createCell(rowIndex, colIndex++, invoiceItem.getItemno(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, job.getCon().getSub().getComp().getConame(), styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText); 
				decorator.createCell(rowIndex, colIndex++, job.getCon().getSub().getSubname(), styleText);
				decorator.createCell(rowIndex, colIndex++, job.getJobno(), styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText); 
				decorator.createCell(rowIndex, colIndex++, jobExpenseItem.getItemNo(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, job.getCon().getName(), styleText);
				decorator.createCell(rowIndex, colIndex++, InstModelTools.modelNameViaTranslations(jobExpenseItem.getModel(), locale, fallbackLocale), styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText); 
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText); 
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText); 
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText); 
				decorator.createCell(rowIndex, colIndex++, 
						tranServ.getCorrectTranslation(jobExpenseItem.getServiceType().getLongnameTranslation(),locale),
						styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText); 
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText); 
				
				if (job.getClientRef() != null) {
					decorator.createCell(rowIndex, colIndex, job.getClientRef(), styleText);
				}
				colIndex++;
				
				decorator.createCell(rowIndex, colIndex++, jobExpenseItem.getClientRef(), styleText); 
				String formattedPOs = getFormattedPOs(invoiceItem);
				if (!formattedPOs.isEmpty()) {
					decorator.createCell(rowIndex, colIndex, formattedPOs, styleText);
				}
				colIndex++;
				decorator.createCell(rowIndex, colIndex++, jobExpenseItem.getComment(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobExpenseItem.getSinglePrice(), styleMonetary);
				decorator.createCell(rowIndex, colIndex++, jobExpenseItem.getQuantity(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, invoiceItem.getFinalCost(), styleMonetary);
				decorator.createCell(rowIndex, colIndex++, invoice.getCurrency().getCurrencyCode(), styleText);
				
				rowIndex++;
			}
		}
		return rowIndex;
	}
	
	/**
	 * Writes details of invoice items which neither based on an expense item nor job item
	 */
	private int writeOtherItems(XlsxViewDecorator decorator, Invoice invoice, Locale locale, Locale fallbackLocale, int rowIndex) {
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleMonetary = decorator.getStyleHolder().getMonetaryStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

		for (InvoiceItem invoiceItem : invoice.getItems()) {
			if ((invoiceItem.getJobItem() == null) && (invoiceItem.getExpenseItem() == null)) {
				int colIndex = 0;
				decorator.createCell(rowIndex, colIndex++, invoiceItem.getItemno(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, invoice.getAddress().getSub().getSubname(), styleText);
				// No ON_BEHALF_OF: 
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, invoice.getComp().getConame(), styleText);
				// No JOB_NUMBER, JOB_ITEM_NO, JOB_SERVICE_NO, JOB_CONTACT:
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				// Use item description for inst model name
				decorator.createCell(rowIndex, colIndex++, invoiceItem.getDescription(), styleText);
				// No CLIENT_DESCRIPTION, TRESCAL_ID, CLIENT_ID:
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, invoiceItem.getSerialno(), styleText);
				// No SERVICE_TYPE, DELIVERY_DATE, DELIVERY_NUMBER, JOB_CLIENT_REF, JOB_ITEM_CLIENT_REF
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				decorator.createCell(rowIndex, colIndex++, "N/A", styleText);
				String formattedPOs = getFormattedPOs(invoiceItem);
				if (!formattedPOs.isEmpty()) {
					decorator.createCell(rowIndex, colIndex, formattedPOs, styleText);
				}
				colIndex++;
				colIndex += 3;
				decorator.createCell(rowIndex, colIndex++, invoiceItem.getFinalCost(), styleMonetary);
				decorator.createCell(rowIndex, colIndex++, invoice.getCurrency().getCurrencyCode(), styleText);
				
				rowIndex++;
			}
		}
		return rowIndex;
	}
	
	private String getFormattedPOs(InvoiceItem invoiceItem) {
		StringBuffer result = new StringBuffer();
		for (InvoicePOItem invoicePOItem : invoiceItem.getInvPOsItem()) {
			if (result.length() > 0) {
				result.append(" ");
			}
			result.append(invoicePOItem.getInvPO().getPoNumber());
		}

		for (InvoiceItemPO iipo : invoiceItem.getInvItemPOs()) {
			if (result.length() > 0) {
				result.append(" ");
			} else if (iipo.getBpo() != null) {
				result.append(iipo.getBpo().getPoNumber());
			} else if (iipo.getJobPO() != null) {
				result.append(iipo.getJobPO().getPoNumber());
			}
		}
		
		return result.toString();
	}

	/**
	 * Uses projection data to write details of linked periodic job items
	 * @param decorator
	 * @param invoiceDto
	 * @param locale
	 * @param rowIndex
	 * @return
	 */
	private int writePeriodicJobItems(XlsxViewDecorator decorator, InvoiceProjectionDTO invoiceDto, Locale locale, int rowIndex) {
		if (invoiceDto.getPeriodicJobItems() != null) {
			CellStyle styleText = decorator.getStyleHolder().getTextStyle();
			CellStyle styleDate = decorator.getStyleHolder().getDateStyle();
			CellStyle styleMonetary = decorator.getStyleHolder().getMonetaryStyle();
			CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

			int linkedItemCount = 1;
			for (JobItemNotInvoicedDTO jiniDto : invoiceDto.getPeriodicJobItems()) {
				int colIndex = 0;
				JobItemProjectionDTO jiDto = jiniDto.getJobItem();
				InstrumentProjectionDTO instDto = jiDto.getInstrument();
				
				decorator.createCell(rowIndex, colIndex++, linkedItemCount++, styleInteger);
				decorator.createCell(rowIndex, colIndex++, jiDto.getJob().getContact().getSubdiv().getCompany().getConame(), styleText);
				if (jiDto.getOnBehalfCompany() != null) {
					decorator.createCell(rowIndex, colIndex, jiDto.getOnBehalfCompany().getConame(), styleText);
				}
				colIndex++;
				decorator.createCell(rowIndex, colIndex++, jiDto.getJob().getContact().getSubdiv().getSubname(), styleText);
				decorator.createCell(rowIndex, colIndex++, jiDto.getJob().getJobno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jiDto.getItemno(), styleInteger);
				colIndex++;	// Blank service number
				decorator.createCell(rowIndex, colIndex++, jiDto.getJob().getContact().getName(), styleText);
				decorator.createCell(rowIndex, colIndex++, instDto.getInstrumentModelNameViaFields(false), styleText);
				decorator.createCell(rowIndex, colIndex++, instDto.getCustomerDescription(), styleText);
				decorator.createCell(rowIndex, colIndex++, instDto.getPlantid(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, instDto.getPlantno(), styleText);
				decorator.createCell(rowIndex, colIndex++, instDto.getSerialno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jiDto.getServiceType().getLongName(), styleText);
				DeliveryItemDTO diDto = getClientDeliveryItem(jiDto);
				if (diDto != null) {
					decorator.createCell(rowIndex, colIndex++, diDto.getDelivery().getDeliveryDate(), styleDate);
					decorator.createCell(rowIndex, colIndex++, diDto.getDelivery().getDeliveryNumber(), styleText);
				}
				else {
					colIndex += 2;
				}
				decorator.createCell(rowIndex, colIndex++, jiDto.getJob().getClientRef(), styleText);
				decorator.createCell(rowIndex, colIndex++, jiDto.getClientRef(), styleText);
				String formattedPOs = getFormattedPOs(jiDto.getJobItemPos());
				if (!formattedPOs.isEmpty())
					decorator.createCell(rowIndex, colIndex, formattedPOs, styleText);
				colIndex++;
				String formattedCostSource = formatCostSource(jiDto.getEstimatedPrice().getCostSource());
				decorator.createCell(rowIndex, colIndex++, formattedCostSource, styleText);
				if (jiDto.getEstimatedPrice().getRevision() != null)
					decorator.createCell(rowIndex, colIndex, jiDto.getEstimatedPrice().getRevision(), styleInteger);
				colIndex++;
				decorator.createCell(rowIndex, colIndex++, jiDto.getEstimatedPrice().getPrice(), styleMonetary);
				decorator.createCell(rowIndex, colIndex++, jiDto.getEstimatedPrice().getCurrency().getValue(), styleText);
				
				rowIndex++;
			}
		}
		return rowIndex;
	}
	
	private String formatCostSource(CostSource costSource) {
		// "IN" = Invoice cost source (not expected for periodic linking)
		// All job items would either have a JP or CR source for periodic linking
		String result = "IN";
		switch (costSource) {
		case JOB_COSTING:
			result = "JP";
			break;
		case CONTRACT_REVIEW:
			result = "CR";
			break;
		default:
			break;
		}
		return result;
	}
	
	private DeliveryItemDTO getClientDeliveryItem(JobItemProjectionDTO jiDto) {
		DeliveryItemDTO result = null;
		if (jiDto.getDeliveryItems() != null) {
			result = jiDto.getDeliveryItems().stream()
					.filter(diDto -> diDto.getDelivery().getType().equals(DeliveryType.CLIENT))
					.findFirst()
					.orElse(null);
		}
		return result;
	}

	private String getFormattedPOs(List<ClientPurchaseOrderProjectionDTO> pos) {
		StringBuffer result = new StringBuffer();
		// Job items without POs will not have information
		if (pos != null) {
			for (ClientPurchaseOrderProjectionDTO po : pos) {
				if (po.getBpoNumber() != null) {
					if (result.length() > 0) result.append(", ");
					result.append(po.getBpoNumber());
				}
				if (po.getPoNumber() != null) {
					if (result.length() > 0) result.append(", ");
					result.append(po.getPoNumber());
				}
			}
		}
		return result.toString();
	}
	
	private int writePeriodicExpenseItems(XlsxViewDecorator decorator, InvoiceProjectionDTO invoiceDto, Locale locale, int rowIndex) {
		if (invoiceDto.getPeriodicExpenseItems() != null) {
			CellStyle styleText = decorator.getStyleHolder().getTextStyle();
			CellStyle styleMonetary = decorator.getStyleHolder().getMonetaryStyle();
			CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

			int linkedItemCount = 1;
			for (JobExpenseItemNotInvoicedDTO einiDto : invoiceDto.getPeriodicExpenseItems()) {
				int colIndex = 0;
				JobProjectionDTO jobDto = einiDto.getExpenseItem().getJob();
				
				decorator.createCell(rowIndex, colIndex++, linkedItemCount++, styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobDto.getContact().getSubdiv().getCompany().getConame(), styleText);
				colIndex++;	// No on-behalf for expense item
				decorator.createCell(rowIndex, colIndex++, jobDto.getContact().getSubdiv().getSubname(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobDto.getJobno(), styleText);
				colIndex++;	// No job item number
				decorator.createCell(rowIndex, colIndex++, einiDto.getExpenseItem().getItemNo(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobDto.getContact().getName(), styleText);
				decorator.createCell(rowIndex, colIndex++, einiDto.getExpenseItem().getInstrumentModelTranslation(), styleText);
				decorator.createCell(rowIndex, colIndex++, einiDto.getExpenseItem().getComment(), styleText);
				colIndex++;	// No Trescal ID
				colIndex++;	// No Client ID
				colIndex++;	// No Serial number
				colIndex++;	// No Service type
				colIndex++;	// No delivery date
				colIndex++;	// No delivery number
				decorator.createCell(rowIndex, colIndex++, jobDto.getClientRef(), styleText);
				decorator.createCell(rowIndex, colIndex++, einiDto.getExpenseItem().getClientRef(), styleText);
				String formattedPOs = getFormattedPOs(einiDto.getExpenseItem().getExpenseItemPos());
				if (!formattedPOs.isEmpty())
					decorator.createCell(rowIndex, colIndex, formattedPOs, styleText);
				colIndex++;	// No PO / BPO connection to expense item currently
				decorator.createCell(rowIndex, colIndex++, einiDto.getExpenseItem().getSinglePrice(), styleMonetary);
				decorator.createCell(rowIndex, colIndex++, einiDto.getExpenseItem().getQuantity(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, einiDto.getExpenseItem().getTotalPrice(), styleMonetary);
				decorator.createCell(rowIndex, colIndex++, jobDto.getCurrency().getValue(), styleText);
				
				rowIndex++;
			}
		}
		return rowIndex;
	}
}