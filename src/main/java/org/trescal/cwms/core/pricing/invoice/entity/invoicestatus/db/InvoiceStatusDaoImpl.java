package org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;

@Repository
public class InvoiceStatusDaoImpl extends BaseDaoImpl<InvoiceStatus, Integer> implements InvoiceStatusDao
{
	@Override
	protected Class<InvoiceStatus> getEntity() {
		return InvoiceStatus.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<InvoiceStatus> findAllInvoiceStatusRequiringAttention() {
		Criteria criteria = getSession().createCriteria(InvoiceStatus.class);
		criteria.add(Restrictions.eq("requiresAttention", true));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public InvoiceStatus findOnInsertActivity() {
		Criteria criteria = getSession().createCriteria(InvoiceStatus.class);
		criteria.add(Restrictions.eq("onInsert", true));
		List<InvoiceStatus> list = criteria.list();
		return list.size() > 0 ? list.get(0) : null;
	}
}