package org.trescal.cwms.core.pricing.creditnote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.form.CreateCreditNoteForm;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class CreateCreditNoteController {

	@Autowired
	private CreditNoteService cnServ;
	@Autowired
	private InvoiceService invServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;

	@ModelAttribute("createcreditnoteform")
	public CreateCreditNoteForm createCreateCreditNoteForm() {
		CreateCreditNoteForm form = new CreateCreditNoteForm();
		form.setCreditWholeInvoice(true);
		return form;
	}

	@RequestMapping(value = "/createcreditnote.htm", method = RequestMethod.GET)
	public String displayForm(Model model, @RequestParam(value = "invid", required = true) int invId) {
		model.addAttribute("invoice", this.invServ.findInvoice(invId));
		return "trescal/core/pricing/creditnote/createcreditnote";
	}

	@RequestMapping(value = "/createcreditnote.htm", method = RequestMethod.POST)
	public String onSubmit(@RequestParam(value = "invid", required = true) int invid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("createcreditnoteform") CreateCreditNoteForm form) {
		Contact currentContact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Invoice invoice = this.invServ.findInvoice(invid);
		CreditNote creditNote = this.cnServ.createCreditNote(invoice, form.getCreditWholeInvoice(), currentContact,
				allocatedSubdiv);
		return "redirect:viewcreditnote.htm?id=" + creditNote.getId();
	}
}