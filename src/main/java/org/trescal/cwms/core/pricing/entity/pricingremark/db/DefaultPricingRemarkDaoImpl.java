package org.trescal.cwms.core.pricing.entity.pricingremark.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.pricing.entity.pricingremark.DefaultPricingRemark;
import org.trescal.cwms.core.pricing.entity.pricingremark.DefaultPricingRemark_;

@Repository
public class DefaultPricingRemarkDaoImpl extends BaseDaoImpl<DefaultPricingRemark, Integer> implements DefaultPricingRemarkDao {

	@Override
	protected Class<DefaultPricingRemark> getEntity() {
		return DefaultPricingRemark.class;
	}
	
	@Override
	public List<DefaultPricingRemark> getRemarksForOrganisation(Integer businessCompanyId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<DefaultPricingRemark> cq = cb.createQuery(DefaultPricingRemark.class);
			Root<DefaultPricingRemark> root = cq.from(DefaultPricingRemark.class);
			Join<DefaultPricingRemark, Company> organisation = root.join(DefaultPricingRemark_.organisation);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(organisation.get(Company_.coid), businessCompanyId));
			if (locale != null) {
				clauses.getExpressions().add(cb.equal(root.get(DefaultPricingRemark_.locale), locale));
			}
			cq.where(clauses);
			return cq;
		});
	}

}
