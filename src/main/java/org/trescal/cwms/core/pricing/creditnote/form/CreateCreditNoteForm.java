package org.trescal.cwms.core.pricing.creditnote.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CreateCreditNoteForm
{
	private Boolean creditWholeInvoice;
}