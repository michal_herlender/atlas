package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.system.entity.baseaction.BaseAction;

@Entity
@Table(name = "purchaseorderactionlist")
public class PurchaseOrderAction extends BaseAction
{
	private PurchaseOrder order;
	private PurchaseOrderStatus activity;
	private PurchaseOrderStatus outcomeStatus;

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "activityid", unique = false, nullable = false)
	public PurchaseOrderStatus getActivity()
	{
		return this.activity;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "orderid", unique = false, nullable = false)
	public PurchaseOrder getOrder()
	{
		return this.order;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", unique = false, nullable = false)
	public PurchaseOrderStatus getOutcomeStatus()
	{
		return this.outcomeStatus;
	}

	public void setActivity(PurchaseOrderStatus activity)
	{
		this.activity = activity;
	}

	public void setOrder(PurchaseOrder order)
	{
		this.order = order;
	}

	public void setOutcomeStatus(PurchaseOrderStatus outcomeStatus)
	{
		this.outcomeStatus = outcomeStatus;
	}
}
