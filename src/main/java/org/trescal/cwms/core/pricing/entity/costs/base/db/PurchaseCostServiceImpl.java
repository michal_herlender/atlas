package org.trescal.cwms.core.pricing.entity.costs.base.db;

import java.util.List;

import org.trescal.cwms.core.pricing.entity.costs.base.PurchaseCost;

public class PurchaseCostServiceImpl implements PurchaseCostService
{
	PurchaseCostDao PurchaseCostDao;

	public PurchaseCost findPurchaseCost(int id)
	{
		return PurchaseCostDao.find(id);
	}

	public void insertPurchaseCost(PurchaseCost PurchaseCost)
	{
		PurchaseCostDao.persist(PurchaseCost);
	}

	public void updatePurchaseCost(PurchaseCost PurchaseCost)
	{
		PurchaseCostDao.update(PurchaseCost);
	}

	public List<PurchaseCost> getAllPurchaseCosts()
	{
		return PurchaseCostDao.findAll();
	}

	public void setPurchaseCostDao(PurchaseCostDao PurchaseCostDao)
	{
		this.PurchaseCostDao = PurchaseCostDao;
	}

	public void deletePurchaseCost(PurchaseCost purchasecost)
	{
		this.PurchaseCostDao.remove(purchasecost);
	}

	public void saveOrUpdatePurchaseCost(PurchaseCost purchasecost)
	{
		this.PurchaseCostDao.saveOrUpdate(purchasecost);
	}
}