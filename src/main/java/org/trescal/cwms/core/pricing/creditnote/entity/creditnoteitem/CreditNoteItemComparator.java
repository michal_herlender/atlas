package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem;

import java.util.Comparator;

public class CreditNoteItemComparator implements Comparator<CreditNoteItem> {

	@Override
	public int compare(CreditNoteItem item1, CreditNoteItem item2) {
		if (item1.getId() == null && item2.getId() == null) {
			return 1;
		} else {
			return (item1.getItemno().compareTo(item2.getItemno()));
		}
	}
}