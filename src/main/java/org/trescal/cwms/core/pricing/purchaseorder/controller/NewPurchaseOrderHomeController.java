package org.trescal.cwms.core.pricing.purchaseorder.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderHomeWrapper;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db.PurchaseOrderStatusService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.dto.PurchaseOrderStatusProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.db.QuickPurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.form.PurchaseOrderHomeForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV,
		Constants.SESSION_ATTRIBUTE_USERNAME })
public class NewPurchaseOrderHomeController {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private PurchaseOrderService poService;
	@Autowired
	private PurchaseOrderStatusService poStatusService;
	@Autowired
	private QuickPurchaseOrderService quickPOService;
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private UserService userServ;
	@Autowired
	private LimitCompanyService limitCompServ;
	
	public static final String FORM_NAME = "command";

	@ModelAttribute(FORM_NAME)
	protected PurchaseOrderHomeForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
			throws Exception {
		PurchaseOrderHomeForm form = new PurchaseOrderHomeForm();
		form.setOrgid(companyDto.getKey());
		return form;
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@RequestMapping(value = "/purchaseorderhome.htm", method = RequestMethod.POST)
	protected String onSubmit(@ModelAttribute(FORM_NAME) PurchaseOrderHomeForm form) throws Exception {
		// perform the search
		PagedResultSet<PurchaseOrder> rs = this.poService.searchSortedPurchaseOrder(form,
				new PagedResultSet<PurchaseOrder>(
						form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(),
						form.getPageNo() == 0 ? 1 : form.getPageNo()));
		form.setRs(rs);
		form.setOrders((Collection<PurchaseOrder>) rs.getResults());
		return "trescal/core/pricing/purchaseorder/purchaseorderresults";
	}

	@RequestMapping(value = "/purchaseorderhome.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Locale locale)
			throws Exception {

		User user = userServ.get(userName);
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();

		Map<String, Object> map = new HashMap<>();
		map.put("stringtools", new StringTools());
		List<PurchaseOrderHomeWrapper<?>> lst = getInterestingItems(companyDto.getKey(), subdivDto.getKey(), locale,
				auth);
		map.put("interestingItems", lst);

		if (authServ.hasRight(user.getCon(), Permission.PURCHASE_ORDER_STATUS_PRE_ISSUE, subdivDto.getKey())) {
			Integer poStatusId = this.poStatusService
					.findPurchaseOrderStatusIdRequiringAttentionByName("awaiting pre-issue");
			if (poStatusId != null) {
				map.put("posUpperLimit", limitCompServ.getPosUnderLimit(
						this.poService.getAllPurchaseOrdersAtStatus(poStatusId, companyDto.getKey()),
						this.limitCompServ.getPoUserConfigLimit(auth, companyDto.getKey()), companyDto.getKey()));
			}
		}
		return new ModelAndView("trescal/core/pricing/purchaseorder/newpurchaseorderhome", map);
	}

	protected List<PurchaseOrderHomeWrapper<?>> getInterestingItems(Integer allocatedCompanyId, Integer subdivId,
			Locale locale, Collection<? extends GrantedAuthority> auth) {

		List<PurchaseOrderHomeWrapper<?>> result = new ArrayList<>();

		result.add(getQuickPurchaseOrderWrapper(allocatedCompanyId, subdivId, locale));
		
		for (PurchaseOrderStatusProjectionDTO status : getPurchaseOrderStatusRequiringAttentionAndAllowChangeStatusProjection(
				auth, locale)) {

			PurchaseOrderHomeWrapper<PurchaseOrder> wrapper = new PurchaseOrderHomeWrapper<>();
			wrapper.setId(StringTools.toLowerNoSpaceCase(status.getName()));
			wrapper.setName(status.getNameTranslation());
			wrapper.setDescription(status.getDescTranslation());
			wrapper.setDtos(this.poService.getAllPurchaseOrdersAtStatusUsingProjection(status.getStatusId(),
					allocatedCompanyId, subdivId));
			wrapper.setContentsPurchaseOrder(true);
			result.add(wrapper);
		}

		result.add(getPendingSyncWrapper(allocatedCompanyId, subdivId, locale));
		result.add(getBrokeringErrorsWrapper(allocatedCompanyId, subdivId, locale));

		return result;
	}

	protected PurchaseOrderHomeWrapper<QuickPurchaseOrder> getQuickPurchaseOrderWrapper(Integer allocatedCompanyId,
			Integer subdivId, Locale locale) {

		String title = this.messageSource.getMessage("purchaseorder.quickpo", null, "Quick PO", locale);
		String description = this.messageSource.getMessage("purchaseorder.outstandingquickpo", null,
				"Outstanding Quick PO", locale);
		PurchaseOrderHomeWrapper<QuickPurchaseOrder> wrapper = new PurchaseOrderHomeWrapper<>();
		wrapper.setId("qpo");
		wrapper.setName(title);
		wrapper.setDescription(description);
		wrapper.setDtos(this.quickPOService.findAllUnconsolidatedQuickPurchaseOrdersUsingProjection(allocatedCompanyId,
				subdivId));
		wrapper.setContentsQuickPurchaseOrder(true);
		return wrapper;
	}

	protected PurchaseOrderHomeWrapper<PurchaseOrder> getPendingSyncWrapper(Integer allocatedCompanyId,
			Integer subdivId, Locale locale) {

		String title = this.messageSource.getMessage("purchaseorder.pendingsync", null, "Pending Sync", locale);
		PurchaseOrderHomeWrapper<PurchaseOrder> wrapper = new PurchaseOrderHomeWrapper<>();
		wrapper.setId("pendingsync");
		wrapper.setName(title);
		wrapper.setDescription(title);
		wrapper.setDtos(
				this.poService.getByAccountStatusUsingProjection(allocatedCompanyId, subdivId, AccountStatus.P));
		wrapper.setContentsPurchaseOrder(true);
		return wrapper;
	}

	protected PurchaseOrderHomeWrapper<PurchaseOrder> getBrokeringErrorsWrapper(Integer allocatedCompanyId,
			Integer subdivId, Locale locale) {

		String title = this.messageSource.getMessage("purchaseorder.brokeringerrors", null, "Brokering Errors", locale);
		PurchaseOrderHomeWrapper<PurchaseOrder> wrapper = new PurchaseOrderHomeWrapper<>();
		wrapper.setId("brokeringerrors");
		wrapper.setName(title);
		wrapper.setDescription(title);
		wrapper.setDtos(
				this.poService.getByAccountStatusUsingProjection(allocatedCompanyId, subdivId, AccountStatus.E));
		wrapper.setContentsPurchaseOrder(true);
		return wrapper;
	}

	protected List<PurchaseOrderStatusProjectionDTO> getPurchaseOrderStatusRequiringAttentionAndAllowChangeStatusProjection(
			Collection<? extends GrantedAuthority> auth, Locale locale) {

		List<PurchaseOrderStatusProjectionDTO> lposP = new ArrayList<PurchaseOrderStatusProjectionDTO>();
		List<PurchaseOrderStatusProjectionDTO> lposPTemp = this.poStatusService
				.findAllPurchaseOrderStatusRequiringAttentionProjection(locale);
		if (!lposPTemp.isEmpty()) {
			for (PurchaseOrderStatusProjectionDTO poStatutTemps : lposPTemp) {
				if (poStatutTemps != null) {
					if (poStatusService.isAllowChangeStatus(poStatutTemps, auth))
						lposP.add(poStatutTemps);
				}
			}
		}
		return lposP;
	}

}
