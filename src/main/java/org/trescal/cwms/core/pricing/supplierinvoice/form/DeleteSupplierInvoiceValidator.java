package org.trescal.cwms.core.pricing.supplierinvoice.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;

/**
 * Verifies whether a SupplierInvoice can be deleted
 */
@Component
public class DeleteSupplierInvoiceValidator implements Validator {

	public static String ERROR_CODE_CONNECTED = "error.supplierinvoice.deletion";
	public static String DEFAULT_MESSAGE_CONNECTED = "This supplier invoice is connected to {0} purchase order items.  The supplier invoice must be disconnected from all purchase order items to allow deletion.";
	
	@Override
	public boolean supports(Class<?> arg0) {
		return SupplierInvoice.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object object, Errors errors) {
		SupplierInvoice supplierInvoice = (SupplierInvoice) object;
		int count = 0;
		for (PurchaseOrderItem item : supplierInvoice.getPurchaseOrder().getItems()) {
			if ((item.getSupplierInvoice() != null) && (item.getSupplierInvoice().getId() == supplierInvoice.getId())) {
				count++;
			}
		}
		if (count > 0) {
			errors.reject(ERROR_CODE_CONNECTED, new Object[] {count}, DEFAULT_MESSAGE_CONNECTED);
		}
	}

}
