package org.trescal.cwms.core.pricing.entity.costtype;

import java.util.Comparator;

public class CostTypeComparator implements Comparator<CostType> {

	@Override
	public int compare(CostType arg0, CostType arg1) {
		return arg0.getNaturalOrder().compareTo(arg1.getNaturalOrder());
	}

}
