package org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.db;

import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencySupport;

public interface GenericPricingEntityDao {
	
	<T extends MultiCurrencySupport> T findMultiCurrencyEntity(Class<T> clazz, int id);
	
	<T extends GenericPricingEntity<?>> T findPricing(Class<T> clazz, int id);
}