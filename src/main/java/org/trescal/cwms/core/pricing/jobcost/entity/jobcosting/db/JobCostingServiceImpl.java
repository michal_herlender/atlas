package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.OpenActivityException;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.JobCostingClientDecision;
import org.trescal.cwms.core.pricing.TotalDiscountCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.dto.JobCostingDTO;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.JobCostingClientApproval;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus;
import org.trescal.cwms.core.pricing.jobcost.form.JobCostingForm;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ApprovedLimit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.TimeTool;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.tools.filebrowser.FileWrapper;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateJobCosting;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_IssueJobCosting;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_JobCostingClientDecision;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service("JobCostingService")
public class JobCostingServiceImpl extends BaseServiceImpl<JobCosting, Integer> implements JobCostingService {
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private HookInterceptor_CreateJobCosting interceptor_createJobCosting;
	@Autowired
	private HookInterceptor_IssueJobCosting interceptor_issueJobCosting;
	@Autowired
	private HookInterceptor_JobCostingClientDecision interceptor_jobCostingClientResponse;
	@Autowired
	private JobCostingDao jobCostingDao;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private UserService userService;
	@Autowired
	private ApprovedLimit approvedLimit;
	@Autowired
	private ActionOutcomeService actionOutcomeService;
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private WorkRequirementService wrService;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private TaxCalculator taxCalculator;
	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware; 
	
	private final Logger logger = LoggerFactory.getLogger(JobCostingServiceImpl.class);

	@Override
	protected BaseDao<JobCosting, Integer> getBaseDao() {
		return jobCostingDao;
	}

	@Override
	public ResultWrapper ajaxIssueJobCosting(int jobCostingId, HttpSession session) {
		try {
			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact currentContact = this.userService.get(username).getCon();
			JobCosting jc = this.findJobCosting(jobCostingId);

			if (jc != null) {
				// May throw an OpenActivityException
				this.publishJobCosting(jc, currentContact);
				return new ResultWrapper(true, null, jc, null);
			} else {
				return new ResultWrapper(false, "Job costing not found");
			}
		} catch (OpenActivityException e) {
			logger.info("OpenActivityException caught");
			logger.info(e.getMessage());
			return new ResultWrapper(false, e.getLocalizedMessage());
		}
	}

	@Override
	public ResultWrapper ajaxIssueJobCostingToAdveso(JobCosting jobCosting, String fileName, String username) {
		try {
			Contact currentContact = this.userService.get(username).getCon();
			if (jobCosting != null) {
				// May throw an OpenActivityException
				this.publishJobCosting(jobCosting, fileName, currentContact);
				return new ResultWrapper(true, null, jobCosting, null);
			} else {
				return new ResultWrapper(false, "Job costing item not found");
			}
		} catch (OpenActivityException e) {
			logger.info("OpenActivityException caught");
			logger.info(e.getMessage());
			return new ResultWrapper(false, e.getLocalizedMessage());
		}
	}

	@Override
	public boolean costingRequiresIssue(JobCosting costing) {
		Contact con = costing.getContact();
		Double limit = approvedLimit.parseValue(
				approvedLimit.getValueHierarchical(Scope.CONTACT, con, costing.getOrganisation()).getValue());
		// if company has a valid approval limit
		if (limit > 0) {
			// cast the approval limit to big decimal
			BigDecimal approvalLimit = new BigDecimal(limit);
			// check each item on the costing
			for (JobCostingItem jci : costing.getItems()) {
				BigDecimal adj = jci.getAdjustmentCost().getFinalCost();
				BigDecimal rep = jci.getRepairCost().getFinalCost();
				BigDecimal pur = jci.getPurchaseCost().getFinalCost();
				// combined total of everything but cal cost
				BigDecimal total = (adj.add(rep).add(pur));
				// if the combined cost is OVER the approval limit
				if (total.compareTo(approvalLimit) > 0) {
					// costs on this job item require approval (greater than
					// limit) so costing requires issuing
					return true;
				}
			}
			// all costs lie within approval limits so no need to issue costing
			return false;
		}
		// company doesn't have approval limit so costing definitely requires
		// issuing for client approval
		else
			return true;
	}

	@Override
	public ResultWrapper findAjaxEagerJobCosting(int id) {
		JobCosting jc = this.findEagerJobCosting(id);
		if (jc != null) {
			return new ResultWrapper(true, "", jc, null);
		} else {
			return new ResultWrapper(false, this.messages.getMessage("error.jobcosting.create.notfound", null,
					"The costing could not be found", null));
		}
	}

	@Override
	public int findCurrentMaxVersion(int jobid) {
		return this.jobCostingDao.findCurrentMaxVersion(jobid);
	}

	@Override
	public JobCosting findEagerJobCosting(int id) {
		return this.jobCostingDao.findEagerJobCosting(id);
	}

	@Override
	public JobCosting findJobCosting(int id) {
		JobCosting jc = this.jobCostingDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (jc != null && jc.getDirectory() == null) {
			jc.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.JOB_COSTING),
				jc.getJob().getJobno())));
		}
		return jc;
	}
	
	@Override
	public boolean getAvalaraAware() {
		return this.avalaraAware;
	}

	// Intentionally exposed for integration testing of documents
	@Override
	public void setAvalaraAware(boolean avalaraAware) {
		this.avalaraAware = avalaraAware;
	}

	@Override
	public TotalDiscountCalculator getJobCostingTotalDiscount(Set<JobCostingItem> items) {
		return new TotalDiscountCalculator(items);
	}
	
	@Override
	public TotalDiscountCalculator getJobCostingTotalDiscount(List<JobCostingItemDto> items) {
		return new TotalDiscountCalculator(items);
	}

	@Override
	@Deprecated
	public void save(JobCosting object) {
		throw new UnsupportedOperationException("Programmer must call save() with currentContact instead");
	}

	@Override
	public Integer save(JobCosting jc, Contact currentContact) {
		jc.setLastUpdateOn(new Date());
		jc.setLastUpdateBy(currentContact);
		this.interceptor_createJobCosting.recordAction(jc);
		jc.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.JOB_COSTING),
			jc.getJob().getJobno())));
		this.jobCostingDao.persist(jc);
		return jc.getId();
	}

	@Override
	public void publishJobCosting(JobCosting jobCosting, Contact currentContact) throws OpenActivityException {
		this.jobCostingDao.issueJobCosting(jobCosting);
		HookInterceptor_IssueJobCosting.Input input = new HookInterceptor_IssueJobCosting.Input(jobCosting, null);
		this.interceptor_issueJobCosting.recordAction(input);

		// change status to 'Issued' if not already
		if (!jobCosting.getStatus().getName().equalsIgnoreCase("issued")) {
			jobCosting.setStatus((JobCostingStatus) this.statusServ.findStatusByName("Issued", JobCostingStatus.class));
			jobCosting.setIssued(true);
			jobCosting.setIssueby(currentContact);
			jobCosting.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			this.saveOrUpdate(jobCosting, currentContact);
		}
	}

	@Override
	public void publishJobCosting(JobCosting jobCosting, String fileName, Contact currentContact)
			throws OpenActivityException {
		this.jobCostingDao.issueJobCosting(jobCosting);
		HookInterceptor_IssueJobCosting.Input input = new HookInterceptor_IssueJobCosting.Input(jobCosting, fileName);
		this.interceptor_issueJobCosting.recordAction(input);

		// change status to 'Issued' if not already
		if (!jobCosting.getStatus().getName().equalsIgnoreCase("issued")) {
			jobCosting.setStatus((JobCostingStatus) this.statusServ.findStatusByName("Issued", JobCostingStatus.class));
			jobCosting.setIssued(true);
			jobCosting.setIssueby(currentContact);
			jobCosting.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			this.saveOrUpdate(jobCosting, currentContact);
		}
	}

	@Override
	public void recalculateAndPersistJobCostingTotals(JobCosting jobcosting, Contact currentContact) {
		Set<PricingItem> items = new HashSet<>();
		items.addAll(jobcosting.getItems());
		items.addAll(jobcosting.getExpenseItems());
		CostCalculator.updatePricingTotalCost(jobcosting, items);
		taxCalculator.calculateAndSetVATaxAndFinalCost(jobcosting);
		this.saveOrUpdate(jobcosting, currentContact);
	}

	@Override
	public void saveOrUpdate(JobCosting jobcosting, Contact currentContact) {
		jobcosting.setLastUpdateOn(new Date());
		jobcosting.setLastUpdateBy(currentContact);
		this.jobCostingDao.merge(jobcosting);
	}

	@Override
	public List<JobCosting> getJobCostingListByJobitemId(Integer jobitemid) {
		return jobCostingDao.getJobCostingListByJobitemId(jobitemid);
	}

	@Override
	public List<JobCosting> getEagerJobCostingListForJobIds(Collection<Integer> jobids) {
		return jobCostingDao.getEagerJobCostingListForJobIds(jobids);
	}

	@Override
	public JobCosting getJobCosting(Integer jobItemId, Integer version) {
		return jobCostingDao.getJobCosting(jobItemId, version);
	}
	
	@Override
	public List<JobCostingDTO> getJobCostingsByJobId(Integer jobId, Locale locale){
		
		List<JobCostingDTO> list = this.jobCostingDao.getJobCostingsByJobId(jobId, locale);
		for (JobCostingDTO jc : list) {
			jc.setDifferenceDateCreated(TimeTool.formatPeriod(jc.getCreatedOn().until(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))));
			jc.setDifferenceDateViewed(TimeTool.calculateDateDifference(jc.getLastViewedOn(), null));
			jc.setDifferenceDateUpdated(TimeTool.calculateDateDifference(jc.getLastUpdateOn(), null));
		}
		
		return list;
	}
	
	@Override
	public void setJobCostingClientResponse(JobCosting jobCosting, JobCostingForm jcForm, Contact currentContact) throws OpenActivityException {
		JobCostingClientApproval decision = new JobCostingClientApproval();
		boolean completeWR = false;
		decision.setJobCosting(jobCosting);
		decision.setClientApprovalOn(DateTools.dateFromLocalDateTime(jcForm.getClientApprovalOn()));
		decision.setClientApprovalComment(jcForm.getClientApprovalComment());
		if (JobCostingClientDecision.APPROVED.equals(jcForm.getClientDecision())) {
			decision.setClientApproval(true);
			jobCosting.setStatus((JobCostingStatus) this.statusServ.findStatusByName("Accepted", JobCostingStatus.class));
		} else {
			decision.setClientApproval(false);
			if (JobCostingClientDecision.REJECTED.equals(jcForm.getClientDecision())) {
				jobCosting.setStatus((JobCostingStatus) this.statusServ.findStatusByName("Rejected", JobCostingStatus.class));
				completeWR = true;
			} else if (JobCostingClientDecision.REJECTED_REDO.equals(jcForm.getClientDecision()))
				jobCosting.setStatus((JobCostingStatus) this.statusServ.findStatusByName("Rejected - Redo", JobCostingStatus.class));

		}
		// if jobcosting rejected complete the current workrequirement
		if (completeWR) {
			for (JobCostingItem jcItems : jobCosting.getItems()) {
				this.wrService.checkCurrentWorkRequirement(jcItems.getJobItem());
			}
		}
		if (jobCosting.getClientApprovals() == null)
			jobCosting.setClientApprovals(new HashSet<>());
		jobCosting.getClientApprovals().add(decision);
		this.jobCostingDao.merge(jobCosting);
		//find and set the Transient client approval action outcome job costing field
		ItemActivity clientResponseActivity = this.itemStateService.findItemActivityByName("Client response received");
		List<ActionOutcome> clientResponseActionOutcomes = null;
		ActionOutcome targetActionOutcome = null;
		if (clientResponseActivity != null)
			clientResponseActionOutcomes = this.actionOutcomeService.getActionOutcomesForActivity(clientResponseActivity);
		if (clientResponseActionOutcomes != null)
			targetActionOutcome = clientResponseActionOutcomes.stream().filter(a -> a.getGenericValue().name().equals(jcForm.getClientDecision().name())).findFirst().orElse(null);
		HookInterceptor_JobCostingClientDecision.Input input = new HookInterceptor_JobCostingClientDecision.Input(jobCosting, targetActionOutcome);
		this.interceptor_jobCostingClientResponse.recordAction(input);
	}

	@Override
	public FileWrapper getLastFileRevision(JobCosting jobCosting) {
		FileBrowserWrapper rw = this.fileBrowserServ
			.getFilesForComponentRoot(Component.JOB_COSTING, jobCosting.getId(), null);

		if (rw.getFiles() != null && rw.getFiles().size() > 0) {
			Comparator<FileWrapper> cmp = (fw1, fw2) -> {
				Date d1;
				Date d2;
				d1 = fw1.getLastModified();
				d2 = fw2.getLastModified();
				return d1.compareTo(d2);
			};

			// get all documents for jobcosting version
			List<FileWrapper> list = rw.getFiles().stream()
				.filter(f -> !f.isDirectory() && (f.getFileName().contains(Constants.FILE_NAMING_VERSION.concat(String.valueOf(jobCosting.getVer())))
					|| f.getFileName().contains(Constants.FILE_NAMING_VERSION.concat(" ").concat(String.valueOf(jobCosting.getVer())))))
				.collect(Collectors.toList());

			if (!CollectionUtils.isEmpty(list))
				return list.size() > 1 ? Collections.max(list, cmp) : list.size() == 1 ? list.get(0) : null;
		}
		return null;
	}
	
}