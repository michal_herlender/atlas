package org.trescal.cwms.core.pricing.invoice.dto;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.trescal.cwms.core.jobs.jobitem.dto.InvoiceableItemsByCompanyDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemInvoiceDto;

import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Factory class that helps assemble a List<SummaryReadyToInvoice> of job items,
 * pro forma, and expense items ready to invoice
 *
 * @author galen
 */
public class SummaryReadyToInvoicePopulator {

	private final Map<ImmutableTriple<String, String, String>, SummaryReadyToInvoice> summaryMap;
	private final DateTimeFormatter yearMonthFormat;
	private int quantity; // Quantity to show to user; quantity of all items ready to invoice
	private final String businessSubdivName; // Used for expense items, as business subdiv not returned from query

	public SummaryReadyToInvoicePopulator(String businessSubdivName) {
		this.summaryMap = new TreeMap<>();
		this.yearMonthFormat = DateTimeFormatter.ofPattern("yyyy MM");
		this.quantity = 0;
		this.businessSubdivName = businessSubdivName;
	}

	public void addJobItemInvoiceDtos(List<JobItemInvoiceDto> dtos) {
		for (JobItemInvoiceDto dto : dtos) {
			addJobItemInvoiceDto(dto);
		}
	}

	public void addProformaDtos(List<JobItemInvoiceDto> dtos) {
		for (JobItemInvoiceDto dto : dtos) {
			addProformaDto(dto);
		}
	}

	public void addExpenseItems(List<InvoiceableItemsByCompanyDTO> dtos) {
		for (InvoiceableItemsByCompanyDTO dto : dtos) {
			addExpenseDto(dto);
		}
	}

	public void addJobItemInvoiceDto(JobItemInvoiceDto dto) {
		SummaryReadyToInvoice summary = getSummary(dto);
		summary.incrementJobItemCount();
		if (dto.getJobComplete()) {
			summary.incrementJobItemCompleteJobCount();
		} else {
			summary.incrementJobItemIncompleteJobCount();
		}
		this.quantity++;
	}

	public void addProformaDto(JobItemInvoiceDto dto) {
		SummaryReadyToInvoice summary = getSummary(dto);
		summary.incrementProformaCount();
		this.quantity++;
	}

	public void addExpenseDto(InvoiceableItemsByCompanyDTO dto) {
		SummaryReadyToInvoice summary = getSummary(dto);
		summary.incrementExpenseItemCount();
		this.quantity++;
	}

	private SummaryReadyToInvoice getSummary(InvoiceableItemsByCompanyDTO dto) {
		String yearMonth = ""; // No completion month for summary
		return getSummary(businessSubdivName, dto.getCompanyId(), dto.getCompanyName(),
			yearMonth, false);
	}

	private SummaryReadyToInvoice getSummary(JobItemInvoiceDto dto) {
		String yearMonth = formatYearMonth(dto);
		return getSummary(dto.getSubdivName(), dto.getCompanyId(), dto.getCompanyName(),
			yearMonth, dto.getJobComplete());
	}

	private SummaryReadyToInvoice getSummary(String businessSubdivName, Integer companyId, String companyName,
			String yearMonth, boolean jobComplete) {
		ImmutableTriple<String, String, String> key = new ImmutableTriple<>(yearMonth, companyName, businessSubdivName);
		SummaryReadyToInvoice summary;
		if (!summaryMap.containsKey(key)) {
			summary = new SummaryReadyToInvoice(businessSubdivName, companyId, companyName, yearMonth, jobComplete);
			summaryMap.put(key, summary);
		} else {
			summary = summaryMap.get(key);
		}
		return summary;
	}

	public Collection<SummaryReadyToInvoice> getSummaryDtos() {
		return summaryMap.values();
	}

	private String formatYearMonth(JobItemInvoiceDto dto) {
		String result = "";
		// Give preference to delivery date, then job date complete
		if (dto.getDeliveryDate() != null) {
			result = dto.getDeliveryDate().format(yearMonthFormat);
		} else if (dto.getJobDateComplete() != null) {
			result = dto.getJobDateComplete().format(yearMonthFormat);
		}
		return result;
	}

	public int getQuantity() {
		return quantity;
	}
}