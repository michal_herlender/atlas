package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem;

import java.util.Comparator;

public class JobCostingItemReverseComparator implements Comparator<JobCostingItem>
{
	@Override
	public int compare(JobCostingItem o1, JobCostingItem o2)
	{
		if (o1.getJobCosting().getId() == o2.getJobCosting().getId())
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
		else
		{
			return ((Integer) o2.getJobCosting().getId()).compareTo(o1.getJobCosting().getId());
		}
	}
}
