package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed;

public interface JobCostingViewedDao extends BaseDao<JobCostingViewed, Integer> {}