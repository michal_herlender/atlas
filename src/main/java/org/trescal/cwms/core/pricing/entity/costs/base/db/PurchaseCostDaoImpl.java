package org.trescal.cwms.core.pricing.entity.costs.base.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.entity.costs.base.PurchaseCost;

@Repository("PurchaseCostDao")
public class PurchaseCostDaoImpl extends BaseDaoImpl<PurchaseCost, Integer> implements PurchaseCostDao {
	
	@Override
	protected Class<PurchaseCost> getEntity() {
		return PurchaseCost.class;
	}
}