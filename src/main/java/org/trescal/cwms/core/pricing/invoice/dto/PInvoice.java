package org.trescal.cwms.core.pricing.invoice.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

@Data
@JsonSerialize(converter = PInvoiceDtoConverter.class)
public class PInvoice {

    private Map<Integer, Job> jobs;
    private List<PInvoiceItem> items;
    private List<PInvoiceExpenseItem> expenseItems;
    private int coid;
    private Company company;
    private int addressId;
    private Address address;
    private List<Address> companyAddresses;
    private SupportedCurrency currency;
    private int currencyId;
    private BigDecimal rate;
    private int typeId;
    private InvoiceType type;
    private PricingType pricingType;

    public PInvoice(Company comp, InvoiceType type, PricingType pricingType) {
        this.jobs = new HashMap<>();
        this.items = new ArrayList<>();
        this.expenseItems = new ArrayList<>();
        this.company = comp;
        this.coid = this.company.getCoid();
        this.typeId = type == null ? 0 : type.getId();
        this.type = type;
        this.pricingType = pricingType == null ? PricingType.CLIENT : pricingType;
    }

	@Override
	public String toString() {
		return "Proposed " + this.pricingType + " invoice for jobs " + this.jobs + " with " + this.items.size()
				+ " items";
	}
}