package org.trescal.cwms.core.pricing.supplierinvoice.entity.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.pricing.supplierinvoice.form.SupplierInvoiceForm;
import org.trescal.cwms.spring.model.KeyValue;

public interface SupplierInvoiceService extends BaseService<SupplierInvoice, Integer> {
	
	List<KeyValue<Integer,String>> getDTOList(PurchaseOrder order, Locale locale, boolean prependNoneDto);
	
	SupplierInvoice editSupplierInvoice(Integer supplierinvoiceid, SupplierInvoiceForm form);
}
