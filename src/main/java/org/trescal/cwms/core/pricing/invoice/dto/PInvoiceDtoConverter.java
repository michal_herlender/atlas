package org.trescal.cwms.core.pricing.invoice.dto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import com.fasterxml.jackson.databind.util.StdConverter;

public class PInvoiceDtoConverter extends StdConverter<PInvoice, PInvoiceDto> {
    @Override
    public PInvoiceDto convert(PInvoice value) {
        return PInvoiceDto.builder()
            .company(PInvoiceDto.CompanyShortDto.of(value.getCompany().getCoid(), value.getCompany().getConame()))
            .address(PInvoiceDto.AddressShortDTO.of(value.getAddressId(), value.getAddress().getSub().getSubname(), value.getAddress().getAddr1(), value.getAddress().getTown(), processAddresType(value.getAddress().getAddressType())))
            .pricingType(value.getPricingType())
            .addressId(value.getAddressId())
            .companyAddresses(value.getCompanyAddresses().stream().map(a ->
                PInvoiceDto.AddressShortDTO.of(a.getAddrid(), a.getSub().getSubname(), a.getAddr1(), a.getTown(), processAddresType(a.getAddressType()))
            ).collect(Collectors.toList()))
            .currency(processCurrency(value.getCurrency()))
            .currencyId(value.getCurrencyId())
            .type(value.getType())
            .rate(value.getRate())
            .items(processItems(value.getItems()))
            .expenseItems(processExpenseItems(value.getExpenseItems()))
            .build();
    }

    private List<PInvoiceExpenseItemDto> processExpenseItems(List<PInvoiceExpenseItem> expenseItems) {
        return expenseItems.stream()
            .map(PInvoiceExpenseItemDto::fromExpenseItem)
            .collect(Collectors.toList());
    }

    private List<PInvoiceItemDto> processItems(List<PInvoiceItem> items) {
        return items.stream()
            .map(PInvoiceItemDto::from)
            .collect(Collectors.toList());
    }

    private List<String> processAddresType(Collection<AddressType> types) {
        return types.stream().map(AddressType::getType).collect(Collectors.toList());
    }

    private SupportedCurrencyDto processCurrency(SupportedCurrency curr) {
        if (curr == null) return null;
        return SupportedCurrencyDto.of(curr.getCurrencyCode(), curr.getDefaultRate());
    }
}
