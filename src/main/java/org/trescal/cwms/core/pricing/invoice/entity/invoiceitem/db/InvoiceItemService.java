package org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPOInvoicingDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPoLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.projection.invoiceitem.InvoiceItemProjectionDTO;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputInvoiceItem;

import java.util.Collection;
import java.util.List;

public interface InvoiceItemService extends BaseService<InvoiceItem, Integer> {

	List<InvoiceItemPoLinkDTO> getPoLinks(Integer invoiceId, Integer jobId);

	List<InvoiceItemPoLinkDTO> getPoLinks(Collection<Integer> invoiceIds);

	InvoiceItem findInvoiceItem(int id);

	/**
	 * Returns a {@link ResultWrapper} containing list of all {@link InvoiceItem}s
	 * belonging to the {@link Invoice} with the given id.
	 * 
	 * @param invoiceid the id of the {@link Invoice}.
	 * @param jobid     the id of a {@link Job} to retrict results (optional)
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper getAllInvoiceItemsOnInvoiceDWR(int invoiceid, Integer jobid);

	/**
	 * Returns 1 if this {@link Invoice} has no items, else calculates the next item
	 * number.
	 * 
	 * @param invoiceid the id the the {@link Invoice}.
	 * @return
	 */
	int getNextItemNo(int invoiceid);

	/**
	 * @param itemId Identifier of the item which should be removed
	 * @return Invoice with recalculated prices
	 */
	Invoice removeItemAndRecalculate(Integer itemId);

	/**
	 * Returns all invoice items for the {@link Invoice} and linked to a job item of the {@link Job}
	 * If jobId is null, then it returns all invoice items for the {@link Invoice}
	 *
	 * @param invoiceId identifier for the {@link Invoice}
     * @param jobId     identifier for the {@link Job}
     * @return
     */
    List<InvoiceItem> getAllByInvoiceAndJob(Integer invoiceId, Integer jobId);

    List<PriceLookupOutputInvoiceItem> findInvoiceItems(Integer plantid, Integer servicetypeid, Integer coid);

    List<InvoiceItemProjectionDTO> getInvoiceItemProjectionDTOs(Integer invoiceId);

    List<InvoiceItemPOInvoicingDTO> getInvoiceItemsForInvoice(Integer invoiceId, int poId);

    List<InvoicePOItem> getItemsByInvoiceIdAndNotInInvPoAndSelectedIds(Integer invoiceId, InvoicePO po, List<Integer> itemIds);

    List<InvoicePOItem> getItemsByInvoiceIdAndInInvPoAndNotSelected(Integer invoiceId, Integer poId, List<Integer> itemIds);
}