package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.poorigin.OriginKind;
import org.trescal.cwms.core.jobs.job.entity.poorigin.POOrigin;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.pricing.purchaseorder.entity.enums.PurchaseOrderTaxableOption;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderaction.PurchaseOrderAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemComparator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote.PurchaseOrderNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoiceComparator;
import org.trescal.cwms.core.system.entity.baseaction.ActionComparator;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "purchaseorder")
@Setter
public class PurchaseOrder extends Pricing<Company> implements NoteAwareEntity, ComponentEntity, OriginKind {

    private AccountStatus accountsStatus;
    private Set<PurchaseOrderAction> actions;
    private Address address;
    /* Contact of business requesting the purchase order */
    private Contact businessContact;
    private File directory;
    // child entities
    private Set<PurchaseOrderItem> items;
    private Job job;
    private String jobno;
    private Set<PurchaseOrderNote> notes;
    private String pono;
    private Address returnToAddress;
    private List<Email> sentEmails;
    private PurchaseOrderStatus status;
    private Set<SupplierInvoice> supplierInvoices;
    private POOrigin<? extends OriginKind> poOrigin;
    private PurchaseOrderTaxableOption taxable;

    @Enumerated(EnumType.STRING)
    @Column(name = "accountstatus", length = 1, nullable = true)
    public AccountStatus getAccountsStatus() {
        return this.accountsStatus;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "order")
    @SortComparator(ActionComparator.class)
    public Set<PurchaseOrderAction> getActions() {
        return this.actions;
    }

    @NotNull
    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "addressid", unique = false, nullable = false)
    public Address getAddress() {
        return this.address;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "buspersonid")
    public Contact getBusinessContact() {
        return this.businessContact;
    }

    @Override
    @Transient
    public Contact getDefaultContact() {
        return this.contact;
    }

    @Transient
    public File getDirectory() {
        return this.directory;
    }

    @Override
    @Transient
    public String getIdentifier() {
        return this.pono;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "order")
    @SortComparator(PurchaseOrderItemComparator.class)
    public Set<PurchaseOrderItem> getItems() {
        return this.items;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jobId", nullable = true, foreignKey = @ForeignKey(name = "FK_purchaseorder_job"))
    public Job getJob() {
        return this.job;
    }

    @Length(max = 30)
    @Column(name = "jobno", length = 30)
    public String getJobno() {
        return this.jobno;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "order")
    @SortComparator(NoteComparator.class)
    public Set<PurchaseOrderNote> getNotes() {
        return this.notes;
    }

    @NotNull
    @Length(max = 30)
    @Column(name = "pono", length = 30, nullable = false)
    public String getPono() {
        return this.pono;
    }

    @NotNull
    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "returnaddressid", unique = false, nullable = false)
    public Address getReturnToAddress() {
        return this.returnToAddress;
    }

    @NotNull
    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "statusid", unique = false, nullable = false)
    public PurchaseOrderStatus getStatus() {
        return this.status;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "purchaseOrder")
    @SortComparator(SupplierInvoiceComparator.class)
    public Set<SupplierInvoice> getSupplierInvoices() {
        return supplierInvoices;
    }

    @Override
    @Transient
    public boolean isAccountsEmail() {
        return true;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "poOriginId")
    public POOrigin<? extends OriginKind> getPoOrigin() {
        return poOrigin;
    }

    @NotNull
    @Enumerated
    @Column(name = "taxable", nullable = false)
    public PurchaseOrderTaxableOption getTaxable() {
        return this.taxable;
    }

    @Override
    @Transient
    public ComponentEntity getParentEntity() {
        return null;
    }

    @Transient
    public List<Email> getSentEmails() {
        return this.sentEmails;
    }

    @Transient
    public Integer getPrivateActiveNoteCount() {
        return NoteCountCalculator.getPrivateActiveNoteCount(this);
    }

    @Transient
    public Integer getPrivateDeactivatedNoteCount() {
        return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
    }

    @Transient
    public Integer getPublicActiveNoteCount() {
        return NoteCountCalculator.getPublicActiveNoteCount(this);
    }

    @Transient
    public Integer getPublicDeactivatedNoteCount() {
        return NoteCountCalculator.getPublicDeactivedNoteCount(this);
    }

    @Override
    public String toString() {
        return "PurchaseOrder [accountsStatus=" + accountsStatus + ", actions=" + actions + ", address=" + address
                + ", businessContact=" + businessContact + ", directory=" + directory + ", items=" + items + ", jobno="
                + jobno + ", notes=" + notes + ", pono=" + pono + ", privateActiveNoteCount="
                + getPrivateActiveNoteCount() + ", privateDeactivatedNoteCount=" + getPrivateDeactivatedNoteCount()
                + ", publicActiveNoteCount=" + getPublicActiveNoteCount() + ", publicDeactivatedNoteCount="
                + getPublicDeactivatedNoteCount() + ", returnToAddress=" + returnToAddress + ", sentEmails="
                + sentEmails + ", status=" + status + "]";
    }
}