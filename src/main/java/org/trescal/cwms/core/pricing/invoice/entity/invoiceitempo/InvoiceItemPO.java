package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

@Entity
@Table(name = "invoiceitempo")
public class InvoiceItemPO extends Auditable {

	private BPO bpo;
	private int id;
	private InvoiceItem invItem;
	private InvoicePO invPO;
	private PO jobPO;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bpoid")
	public BPO getBpo() {
		return this.bpo;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoiceitemid", foreignKey = @ForeignKey(name = "FK_invoiceitempo_invoiceitem"))
	public InvoiceItem getInvItem() {
		return this.invItem;
	}

	/**
	 * @deprecated use the InvoicePOItem entity which manages InvoicePO links
	 * @return
	 */
	@Deprecated
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invpoid")
	public InvoicePO getInvPO() {
		return this.invPO;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobpoid")
	public PO getJobPO() {
		return this.jobPO;
	}

	@Transient
	public String getNumber() {
		if (this.invPO != null) {
			return this.invPO.getPoNumber();
		} else if (this.jobPO != null) {
			return this.jobPO.getPoNumber();
		} else if (this.bpo != null) {
			return this.bpo.getPoNumber();
		} else {
			// handle case of data error - no po connected
			return "";
		}
	}

	@Transient
	public String getType() {
		if (this.invPO != null) {
			return "Invoice";
		} else if (this.jobPO != null) {
			return "Job PO";
		} else if (this.bpo != null) {
			return "Job BPO";
		} else {
			// handle case of data error - no po connected
			return "Undefined";
		}
	}

	public void setBpo(BPO bpo) {
		this.bpo = bpo;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setInvItem(InvoiceItem invItem) {
		this.invItem = invItem;
	}

	/**
	 * @deprecated use the InvoicePOItem entity which manages InvoicePO links
	 * @return
	 */
	@Deprecated
	public void setInvPO(InvoicePO invPO) {
		this.invPO = invPO;
	}

	public void setJobPO(PO jobPO) {
		this.jobPO = jobPO;
	}
}