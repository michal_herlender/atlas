package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.dto.JobCostingDTO;

public interface JobCostingDao extends AllocatedDao<Company, JobCosting, Integer>
{
	int findCurrentMaxVersion(int jobid);

	JobCosting findEagerJobCosting(int id);
	
	void issueJobCosting(JobCosting jobcosting);
	
	List<JobCosting> getJobCostingListByJobitemId(Integer jobitemid);
	
	JobCosting getJobCosting(Integer jobItemId, Integer version);
	
	List<JobCosting> getEagerJobCostingListForJobIds(Collection<Integer> jobids);
	
	List<JobCostingDTO> getJobCostingsByJobId(Integer jobId, Locale locale);
}