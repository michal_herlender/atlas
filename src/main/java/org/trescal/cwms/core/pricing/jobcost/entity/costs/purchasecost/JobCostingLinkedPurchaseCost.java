package org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingLinkedCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;

@Entity
@Table(name = "jobcostinglinkedpurchasecost")
public class JobCostingLinkedPurchaseCost extends JobCostingLinkedCost
{
	private JobCostingPurchaseCost purchaseCost;

	private QuotationPurchaseCost quotationPurchaseCost;
	private ContractReviewPurchaseCost contractReviewPurchaseCost;
	private JobCostingPurchaseCost jobCostingPurchaseCost;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "contractrevpurcostid")
	public ContractReviewPurchaseCost getContractReviewPurchaseCost()
	{
		return this.contractReviewPurchaseCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostpurcostid")
	public JobCostingPurchaseCost getJobCostingPurchaseCost()
	{
		return this.jobCostingPurchaseCost;
	}

	@OneToOne(fetch=FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
	@JoinColumn(name = "costid", nullable = false, unique = true)
	public JobCostingPurchaseCost getPurchaseCost()
	{
		return this.purchaseCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "quotepurcostid")
	public QuotationPurchaseCost getQuotationPurchaseCost()
	{
		return this.quotationPurchaseCost;
	}

	public void setContractReviewPurchaseCost(ContractReviewPurchaseCost contractReviewPurchaseCost)
	{
		this.contractReviewPurchaseCost = contractReviewPurchaseCost;
	}

	public void setJobCostingPurchaseCost(JobCostingPurchaseCost jobCostingPurchaseCost)
	{
		this.jobCostingPurchaseCost = jobCostingPurchaseCost;
	}

	public void setPurchaseCost(JobCostingPurchaseCost purchaseCost)
	{
		this.purchaseCost = purchaseCost;
	}

	public void setQuotationPurchaseCost(QuotationPurchaseCost quotationPurchaseCost)
	{
		this.quotationPurchaseCost = quotationPurchaseCost;
	}
}
