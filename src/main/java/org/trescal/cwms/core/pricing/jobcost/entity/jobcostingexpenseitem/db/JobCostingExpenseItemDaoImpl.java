package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingExpenseItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem_;
import org.trescal.cwms.core.pricing.jobcost.form.JobCostingExpenseItemForm;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository
public class JobCostingExpenseItemDaoImpl extends BaseDaoImpl<JobCostingExpenseItem, Integer>
		implements JobCostingExpenseItemDao {

	@Override
	protected Class<JobCostingExpenseItem> getEntity() {
		return JobCostingExpenseItem.class;
	}

	@Override
	public JobCostingExpenseItemForm getFormObject(Integer id, Locale locale) {
		return getSingleResult(cb -> {
			CriteriaQuery<JobCostingExpenseItemForm> cq = cb.createQuery(JobCostingExpenseItemForm.class);
			Root<JobCostingExpenseItem> costingItem = cq.from(JobCostingExpenseItem.class);
			Join<JobCostingExpenseItem, JobExpenseItem> expenseItem = costingItem
					.join(JobCostingExpenseItem_.jobExpenseItem);
			Join<JobExpenseItem, ServiceType> serviceType = expenseItem.join(JobExpenseItem_.serviceType);
			Expression<String> serviceTypeName = joinTranslation(cb, serviceType, ServiceType_.shortnameTranslation,
					locale);
			Join<JobExpenseItem, InstrumentModel> model = expenseItem.join(JobExpenseItem_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			cq.where(cb.equal(costingItem.get(JobCostingExpenseItem_.id), id));
			cq.select(cb.construct(JobCostingExpenseItemForm.class, costingItem.get(JobCostingExpenseItem_.id),
					costingItem.get(JobCostingExpenseItem_.itemno), costingItem.get(JobCostingExpenseItem_.quantity),
					costingItem.get(JobCostingExpenseItem_.totalCost),
					costingItem.get(JobCostingExpenseItem_.generalDiscountRate),
					costingItem.get(JobCostingExpenseItem_.generalDiscountValue),
					costingItem.get(JobCostingExpenseItem_.finalCost), expenseItem.get(JobExpenseItem_.id),
					serviceTypeName, modelName, expenseItem.get(JobExpenseItem_.comment)));
			return cq;
		});
	}

	@Override
	public List<JobCostingExpenseItemDto> getJobCostingExpenseItemsDtoByJobCostingId(Integer jobCostingId,
			Locale locale) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<JobCostingExpenseItemDto> cq = cb.createQuery(JobCostingExpenseItemDto.class);
		Root<JobCosting> jobCostingRoot = cq.from(JobCosting.class);
		Join<JobCosting, JobCostingExpenseItem> jobCostingExpenseItemJoin = jobCostingRoot
				.join(JobCosting_.expenseItems, JoinType.LEFT);
		Join<JobCostingExpenseItem, JobExpenseItem> jobExpenseItemJoin = jobCostingExpenseItemJoin
				.join(JobCostingExpenseItem_.jobExpenseItem, JoinType.LEFT);
		Join<JobExpenseItem, ServiceType> serviceTypeJoin = jobExpenseItemJoin.join(JobExpenseItem_.serviceType,
				JoinType.LEFT);
		Join<ServiceType, Translation> serviceTypeShortnameTranslationJoin = serviceTypeJoin
				.join(ServiceType_.shortnameTranslation, JoinType.LEFT);
		Join<JobExpenseItem, InstrumentModel> instrumentModelJoin = jobExpenseItemJoin.join(JobExpenseItem_.model,
				JoinType.LEFT);
		Expression<String> instrumentModelName = joinTranslation(cb, instrumentModelJoin,
				InstrumentModel_.nameTranslations, locale);
		cq.select(cb.construct(JobCostingExpenseItemDto.class, jobExpenseItemJoin.get(JobExpenseItem_.id),
				jobExpenseItemJoin.get(JobExpenseItem_.itemNo), instrumentModelName,
				jobExpenseItemJoin.get(JobExpenseItem_.comment), jobExpenseItemJoin.get(JobExpenseItem_.date),
				serviceTypeShortnameTranslationJoin.get(Translation_.translation.getName()),
				jobExpenseItemJoin.get(JobExpenseItem_.singlePrice), jobExpenseItemJoin.get(JobExpenseItem_.quantity),
				jobCostingExpenseItemJoin.get(JobCostingExpenseItem_.id.getName())

		));
		cq.where(cb.equal(jobCostingRoot.get(JobCosting_.id.getName()), jobCostingId),
				cb.equal(serviceTypeShortnameTranslationJoin.get(Translation_.locale.getName()), locale));
		TypedQuery<JobCostingExpenseItemDto> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}
}