package org.trescal.cwms.core.pricing.invoice.entity.invoicepo.db;

import io.vavr.control.Either;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.invoice.dto.InvoicePoDto;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;

import java.util.List;

public interface InvoicePOService extends BaseService<InvoicePO, Integer> {

    ResultWrapper ajaxAddInvoicePO(int invoiceId, String poNumber);

    ResultWrapper ajaxDeleteInvoicePO(int invPoId);

    Either<String, InvoicePoDto> ajaxCreateInvoicePO(int invoiceId, String poNo);

    Either<String, Integer> deleteInvoicePO(int poId);

    Either<String, Boolean> linkItemsToPO(Integer invoiceId, Integer poId, List<Integer> itemIds);

}