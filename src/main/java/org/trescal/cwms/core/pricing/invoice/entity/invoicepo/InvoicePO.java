package org.trescal.cwms.core.pricing.invoice.entity.invoicepo;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "invoicepo")
public class InvoicePO extends AbstractPO {
	private List<InvoiceItemPO> invItemPOs;
	private Invoice invoice;
	private List<InvoicePOItem> newInvItemPOs;

	@OneToMany(mappedBy = "invPO", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	public List<InvoicePOItem> getNewInvItemPOs() {
		return newInvItemPOs;
	}

	public void setNewInvItemPOs(List<InvoicePOItem> newInvItemPOs) {
		this.newInvItemPOs = newInvItemPOs;
	}


	@OneToMany(mappedBy = "invPO", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	public List<InvoiceItemPO> getInvItemPOs() {
		return this.invItemPOs;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invid")
	public Invoice getInvoice() {
		return this.invoice;
	}

	@Override
	@Transient
	public boolean isBPO() {
		return false;
	}

	public void setInvItemPOs(List<InvoiceItemPO> invItemPOs) {
		this.invItemPOs = invItemPOs;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
}
