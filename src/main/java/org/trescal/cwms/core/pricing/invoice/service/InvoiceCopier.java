package org.trescal.cwms.core.pricing.invoice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.adjcost.InvoiceAdjustmentCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.calcost.InvoiceCalibrationCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.purchasecost.InvoicePurchaseCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.repcost.InvoiceRepairCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.servicecost.InvoiceServiceCost;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItemComparator;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLinkComparator;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoicepo.InvoicePO;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.db.InvoiceStatusService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.entity.baseaction.ActionComparator;
import org.trescal.cwms.core.system.entity.note.NoteComparator;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class InvoiceCopier {

	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private InvoiceStatusService invoiceStatusService;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private TaxCalculator taxCalculator;

	/**
	 * Creates a copy of an invoice based on the source.
	 * 
	 * @return invoice (not persisted, user must insert)
	 */
	public Invoice copy(Invoice sourceInvoice, InvoiceType invoiceType, Contact createdBy, Address invoiceAddress) {
		Invoice invoice = new Invoice();
		InvoiceStatus createdActivity = this.invoiceStatusService.findOnInsertActivity();

		invoice.setAccountsStatus(null);
		invoice.setActions(new TreeSet<>(new ActionComparator()));

		if (!invoiceAddress.getAddrid().equals(sourceInvoice.getAddress().getAddrid()))
			invoice.setAddress(invoiceAddress);
		else
			invoice.setAddress(sourceInvoice.getAddress());
		invoice.setBusinessContact(sourceInvoice.getBusinessContact());
		invoice.setClientref(sourceInvoice.getClientref());
		invoice.setComp(sourceInvoice.getComp());
		invoice.setContact(sourceInvoice.getContact());
		invoice.setCreatedBy(createdBy);
		invoice.setCurrency(sourceInvoice.getCurrency());
		// Deliveries not copied
		invoice.setDeliveries(new HashSet<>());
		invoice.setDuration(sourceInvoice.getDuration());
		invoice.setExpiryDate(sourceInvoice.getExpiryDate());
		invoice.setTotalCost(sourceInvoice.getTotalCost());
		// invoice.setFinancialPeriod() is set upon insert
		invoice.setInvno(
				numerationService.generateNumber(NumerationType.INVOICE, sourceInvoice.getOrganisation(), null));
		invoice.setInvoiceDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		invoice.setIssueby(null);
		invoice.setIssued(false);
		invoice.setIssuedate(null);
		invoice.setItems(copyInvoiceItems(sourceInvoice.getItems(), invoice));
		invoice.setJobLinks(copyJobLinks(sourceInvoice.getJobLinks(), invoice));
		invoice.setNotes(copyInvoiceNotes(sourceInvoice.getNotes(), invoice));
		invoice.setOrganisation(sourceInvoice.getOrganisation());
		/*
		 *
		 */
		CompanySettingsForAllocatedCompany settings = this.companySettingsService.getByCompany(sourceInvoice.getComp(),
				sourceInvoice.getOrganisation());

		if (!settings.getPaymentterm().getCode().equals(sourceInvoice.getPaymentTerm().getCode()))
			invoice.setPaymentTerm(settings.getPaymentterm());
		else
			invoice.setPaymentTerm(sourceInvoice.getPaymentTerm());

		if (!settings.getPaymentMode().getCode().equals(sourceInvoice.getPaymentMode().getCode()))
			invoice.setPaymentMode(settings.getPaymentMode());
		else
			invoice.setPaymentMode(sourceInvoice.getPaymentMode());

		invoice.setPos(copyInvoicePOs(sourceInvoice.getPos(), invoice));
		invoice.setPricingType(sourceInvoice.getPricingType());
		// 4 NoteCount values are all transient
		invoice.setRate(sourceInvoice.getRate());
		invoice.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		invoice.setReqdate(sourceInvoice.getReqdate());
		invoice.setSiteCostingNote(sourceInvoice.getSiteCostingNote());
		invoice.setStatus(createdActivity.getNext());
		invoice.setType(invoiceType);
        // Create an InvoiceAction that the invoice was created; save of invoice
        // will cascade to InvoiceAction
        InvoiceAction action = new InvoiceAction();
        action.setActivity(createdActivity);
        action.setContact(createdBy);
        action.setDate(new Date());
        action.setInvoice(invoice);
        action.setOutcomeStatus(createdActivity.getNext());
        invoice.getActions().add(action);
        // We don't copy tax information, but recalculate it
        taxCalculator.calculateTax(invoice);
        CostCalculator.updatePricingWithCalculatedTotalCost(invoice);
        return invoice;
    }

	private Set<InvoiceItem> copyInvoiceItems(Set<InvoiceItem> sourceItems, Invoice invoice) {
		Set<InvoiceItem> results = new TreeSet<>(new InvoiceItemComparator());
		for (InvoiceItem sourceItem : sourceItems) {
			InvoiceItem item = new InvoiceItem();
			item.setAdjustmentCost(copyAdjustmentCost(sourceItem.getAdjustmentCost(), item));
			item.setBreakUpCosts(sourceItem.isBreakUpCosts());
			item.setBusinessSubdiv(sourceItem.getBusinessSubdiv());
			item.setCalibrationCost(copyCalibrationCost(sourceItem.getCalibrationCost(), item));
			item.setDescription(sourceItem.getDescription());
			item.setFinalCost(sourceItem.getFinalCost());
			item.setGeneralDiscountRate(sourceItem.getGeneralDiscountRate());
			item.setGeneralDiscountValue(sourceItem.getGeneralDiscountValue());
			item.setInvoice(invoice);
			item.setItemno(sourceItem.getItemno());
			item.setJobItem(sourceItem.getJobItem());
			item.setExpenseItem(sourceItem.getExpenseItem());
			item.setServiceCost(copyServiceCost(sourceItem.getServiceCost(), item));
			item.setNominal(sourceItem.getNominal());
			item.setPartOfBaseUnit(sourceItem.isPartOfBaseUnit());
			item.setPurchaseCost(copyPurchaseCost(sourceItem.getPurchaseCost(), item));
			item.setQuantity(sourceItem.getQuantity());
			item.setRepairCost(copyRepairCost(sourceItem.getRepairCost(), item));
			item.setSerialno(sourceItem.getSerialno());
			item.setTotalCost(sourceItem.getTotalCost());
			item.setShipToAddress(sourceItem.getShipToAddress());
			results.add(item);
		}
		return results;
	}

	private InvoiceAdjustmentCost copyAdjustmentCost(InvoiceAdjustmentCost source, InvoiceItem item) {
		InvoiceAdjustmentCost result = null;
		if (source != null) {
			result = new InvoiceAdjustmentCost();
			copyCostFields(source, result);
			result.setInvoiceItem(item);
			result.setNominal(source.getNominal());
		}
		return result;
	}

	private InvoiceCalibrationCost copyCalibrationCost(InvoiceCalibrationCost source, InvoiceItem item) {
		InvoiceCalibrationCost result = null;
		if (source != null) {
			result = new InvoiceCalibrationCost();
			copyCostFields(source, result);
			result.setInvoiceItem(item);
			result.setNominal(source.getNominal());
		}
		return result;
	}

	private InvoicePurchaseCost copyPurchaseCost(InvoicePurchaseCost source, InvoiceItem item) {
		InvoicePurchaseCost result = null;
		if (source != null) {
			result = new InvoicePurchaseCost();
			copyCostFields(source, result);
			result.setInvoiceItem(item);
			result.setNominal(source.getNominal());
		}
		return result;
	}

	private InvoiceRepairCost copyRepairCost(InvoiceRepairCost source, InvoiceItem item) {
		InvoiceRepairCost result = null;
		if (source != null) {
			result = new InvoiceRepairCost();
			copyCostFields(source, result);
			result.setInvoiceItem(item);
			result.setNominal(source.getNominal());
		}
		return result;
	}

	private InvoiceServiceCost copyServiceCost(InvoiceServiceCost source, InvoiceItem item) {
		InvoiceServiceCost result = null;
		if (source != null) {
			result = new InvoiceServiceCost();
			copyCostFields(source, result);
			result.setInvoiceItem(item);
			result.setNominal(source.getNominal());
		}
		return result;
	}

	private void copyCostFields(Cost source, Cost result) {
		result.setActive(source.isActive());
		result.setAutoSet(source.isAutoSet());
		result.setCostType(source.getCostType());
		result.setCustomOrderBy(source.getCustomOrderBy());
		result.setDiscountRate(source.getDiscountRate());
		result.setDiscountValue(source.getDiscountValue());
		result.setFinalCost(source.getFinalCost());
		result.setHourlyRate(source.getHourlyRate());
		result.setLabourCostTotal(source.getLabourCostTotal());
		result.setLabourTime(source.getLabourTime());
		result.setTotalCost(source.getTotalCost());
	}

	private Set<InvoiceJobLink> copyJobLinks(Set<InvoiceJobLink> sourceJobLinks, Invoice invoice) {
		Set<InvoiceJobLink> result = new TreeSet<>(new InvoiceJobLinkComparator());
		for (InvoiceJobLink sourceJobLink : sourceJobLinks) {
			InvoiceJobLink invoiceJobLink = new InvoiceJobLink();
			invoiceJobLink.setInvoice(invoice);
			invoiceJobLink.setJob(sourceJobLink.getJob());
			invoiceJobLink.setJobno(sourceJobLink.getJobno());
			invoiceJobLink.setSystemJob(sourceJobLink.isSystemJob());

			result.add(invoiceJobLink);
		}
		return result;

	}

	private Set<InvoiceNote> copyInvoiceNotes(Set<InvoiceNote> sourceInvoiceNotes, Invoice invoice) {
		Set<InvoiceNote> result = new TreeSet<>(new NoteComparator());
		for (InvoiceNote sourceNote : sourceInvoiceNotes) {
			// We only want to copy active invoice notes
			if (sourceNote.isActive()) {
				InvoiceNote invoiceNote = new InvoiceNote();
				invoiceNote.setActive(true);
				invoiceNote.setInvoice(invoice);
				invoiceNote.setLabel(sourceNote.getLabel());
				invoiceNote.setNote(sourceNote.getNote());
				invoiceNote.setPublish(sourceNote.getPublish());
				invoiceNote.setSetBy(sourceNote.getSetBy());
				invoiceNote.setSetOn(sourceNote.getSetOn());

				result.add(invoiceNote);
			}
		}

		return result;

	}

	/*
	 * This also copies the invoiceItemPOs for each invoice item, which must be
	 * copied already
	 */
	private Set<InvoicePO> copyInvoicePOs(Set<InvoicePO> sourcePOs, Invoice invoice) {
		Set<InvoicePO> result = new HashSet<>(); // Unsorted OK
		for (InvoicePO sourcePO : sourcePOs) {
			InvoicePO invoicePO = new InvoicePO();
			invoicePO.setComment(sourcePO.getComment());
			invoicePO.setNewInvItemPOs(copyInvoicePOItems(sourcePO.getNewInvItemPOs(), invoice, invoicePO));
			invoicePO.setInvItemPOs(copyInvoiceItemPOs(sourcePO.getInvItemPOs(), invoice, invoicePO));
			invoicePO.setInvoice(invoice);
			invoicePO.setOrganisation(sourcePO.getOrganisation());
			invoicePO.setPoNumber(sourcePO.getPoNumber());
		}
		return result;
	}

	private List<InvoicePOItem> copyInvoicePOItems(List<InvoicePOItem> newInvItemPOs, Invoice invoice, InvoicePO invoicePO) {
		return newInvItemPOs.stream().map(iip -> InvoicePOItem.builder().invPO(invoicePO)
						.invItem(getMatchingItem(iip.getInvItem(), invoice)).build())
				.collect(Collectors.toList());
	}

	private List<InvoiceItemPO> copyInvoiceItemPOs(List<InvoiceItemPO> sourceInvItemPOs, Invoice invoice,
												   InvoicePO invoicePO) {
		List<InvoiceItemPO> result = new ArrayList<>();
		for (InvoiceItemPO sourceInvoiceItemPO : sourceInvItemPOs) {
			InvoiceItemPO invoiceItemPO = new InvoiceItemPO();
			invoiceItemPO.setBpo(sourceInvoiceItemPO.getBpo());
			invoiceItemPO.setInvItem(getMatchingItem(sourceInvoiceItemPO.getInvItem(), invoice));
			invoiceItemPO.setInvPO(invoicePO);
			invoiceItemPO.setJobPO(sourceInvoiceItemPO.getJobPO());
			result.add(invoiceItemPO);
		}
		return result;
	}

	/*
	 * Finds the corresponding copied item (by item number) in the destination
	 * invoice
	 */
	private InvoiceItem getMatchingItem(InvoiceItem sourceItem, Invoice invoice) {
		for (InvoiceItem item : invoice.getItems()) {
			if (sourceItem.getItemno().equals(item.getItemno()))
				return item;
		}
		throw new RuntimeException("Could not find matching copied InvoiceItem for itemNo " + sourceItem.getItemno());
	}
}
