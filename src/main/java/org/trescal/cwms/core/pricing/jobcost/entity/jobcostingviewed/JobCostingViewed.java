package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

@Entity
@Table(name = "jobcostingviewed")
public class JobCostingViewed
{
	private int id;
	private JobCosting jobCosting;
	private Contact lastViewedBy;
	private Date lastViewedOn;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostingid")
	public JobCosting getJobCosting()
	{
		return this.jobCosting;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "lastviewedby", unique = false, nullable = false)
	public Contact getLastViewedBy()
	{
		return this.lastViewedBy;
	}

	@NotNull
	@Column(name = "lastviewedon", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastViewedOn()
	{
		return this.lastViewedOn;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setJobCosting(JobCosting jobCosting)
	{
		this.jobCosting = jobCosting;
	}

	public void setLastViewedBy(Contact lastViewedBy)
	{
		this.lastViewedBy = lastViewedBy;
	}

	public void setLastViewedOn(Date lastViewedOn)
	{
		this.lastViewedOn = lastViewedOn;
	}

}
