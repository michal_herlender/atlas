package org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;

@Repository("AdditionalCostContactDao")
public class AdditionalCostContactDaoImpl extends BaseDaoImpl<AdditionalCostContact, Integer> implements AdditionalCostContactDao {
	
	@Override
	protected Class<AdditionalCostContact> getEntity() {
		return AdditionalCostContact.class;
	}
}