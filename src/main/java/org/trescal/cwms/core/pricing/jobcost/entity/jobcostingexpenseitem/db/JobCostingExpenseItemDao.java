package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingExpenseItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.form.JobCostingExpenseItemForm;

public interface JobCostingExpenseItemDao extends BaseDao<JobCostingExpenseItem, Integer> {

	JobCostingExpenseItemForm getFormObject(Integer id, Locale locale);

	List<JobCostingExpenseItemDto> getJobCostingExpenseItemsDtoByJobCostingId(Integer jobCostingId, Locale locale);
}