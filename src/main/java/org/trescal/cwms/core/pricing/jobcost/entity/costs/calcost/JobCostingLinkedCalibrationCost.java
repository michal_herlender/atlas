package org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithQuotationCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingLinkedCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;

@Entity
@Table(name = "jobcostinglinkedcalibrationcost")
public class JobCostingLinkedCalibrationCost extends JobCostingLinkedCost implements WithJobCostingCalibrationCost,WithQuotationCalibrationCost
{
	// the job costing cal cost that this linked cost is the source for
	private JobCostingCalibrationCost calibrationCost;
	private QuotationCalibrationCost quotationCalibrationCost;
	private ContractReviewCalibrationCost contractReviewCalibrationCost;
	
	// a job costing calibration cost that is the source of this calibrationCost
	private JobCostingCalibrationCost jobCostingCalibrationCost;

	@OneToOne(fetch=FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
	@JoinColumn(name = "calcostid", nullable = false, unique = true)
	public JobCostingCalibrationCost getCalibrationCost()
	{
		return this.calibrationCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "contractrevcalcostid")
	public ContractReviewCalibrationCost getContractReviewCalibrationCost()
	{
		return this.contractReviewCalibrationCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostcalcostid")
	public JobCostingCalibrationCost getJobCostingCalibrationCost()
	{
		return this.jobCostingCalibrationCost;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "quotecalcostid")
	public QuotationCalibrationCost getQuotationCalibrationCost()
	{
		return this.quotationCalibrationCost;
	}

	public void setCalibrationCost(JobCostingCalibrationCost calibrationCost)
	{
		this.calibrationCost = calibrationCost;
	}

	public void setContractReviewCalibrationCost(ContractReviewCalibrationCost contractReviewCalibrationCost)
	{
		this.contractReviewCalibrationCost = contractReviewCalibrationCost;
	}

	public void setJobCostingCalibrationCost(JobCostingCalibrationCost jobCostingCalibrationCost)
	{
		this.jobCostingCalibrationCost = jobCostingCalibrationCost;
	}
	
	public void setQuotationCalibrationCost(QuotationCalibrationCost quotationCalibrationCost)
	{
		this.quotationCalibrationCost = quotationCalibrationCost;
	}
}