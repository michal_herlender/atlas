package org.trescal.cwms.core.pricing.jobcost.controller;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.db.JobCostingExpenseItemService;
import org.trescal.cwms.core.pricing.jobcost.form.JobCostingExpenseItemForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.EncryptionTools;

@Controller
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class JobCostingController {
	private final static Logger logger = LoggerFactory.getLogger(JobCostingController.class);
	@Autowired
	private JobCostingService jobCostingService;
	@Autowired
	private JobCostingExpenseItemService jobCostingExpenseItemService;
	@Autowired
	private UserService userService;

	@ResponseBody
	@RequestMapping(value = "deletejobcostingexpenseitem.json")
	public Map<String, String> deleteJobCostingExpenseItem(@RequestParam(name = "expenseItemId") Integer expenseItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		JobCostingExpenseItem expenseItem = jobCostingExpenseItemService.get(expenseItemId);
		if (expenseItem != null) {
			JobCosting jobCosting = expenseItem.getJobCosting();
			jobCosting.getExpenseItems().remove(expenseItem);
			Contact currentContact = userService.get(userName).getCon();
			jobCostingService.recalculateAndPersistJobCostingTotals(jobCosting, currentContact);
			Map<String, String> result = new HashMap<String, String>();
			result.put("totalCost", jobCosting.getTotalCost().toString());
			result.put("vatValue", jobCosting.getVatValue().toString());
			result.put("finalCost", jobCosting.getFinalCost().toString());
			return result;
		} else
			throw new RuntimeException();
	}

	@ResponseBody
	@RequestMapping(value = "jobpricingexpenseitem.json", method = RequestMethod.GET)
	public JobCostingExpenseItemForm getJobPricingExpenseItem(@RequestParam(name = "id") Integer id) {
		return jobCostingExpenseItemService.getFormObject(id);
	}

	@ResponseBody
	@RequestMapping(value = "updatejobpricingexpenseitem.json", method = RequestMethod.POST)
	public void updateJobPricingExpenseItem(@RequestBody JobCostingExpenseItemForm form) {

	}

	@ResponseBody
	@RequestMapping(value = "issueCostingToAdveso.json", method = RequestMethod.GET)
	public Map<String, String> issueCostingToAdveso(
			@RequestParam(value = "jobid", required = false, defaultValue = "0") Integer jobid,
			@RequestParam(value = "attach", required = false, defaultValue = "") String encryptedFilePath,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		String fileName = verifyAndReturnFileName(encryptedFilePath, true);
		String[] fileNameTab = fileName.split(" ");
		if (fileNameTab != null && fileNameTab.length > 1) {
			String costingNumber = fileNameTab[1];
			int costingVersion = Integer.valueOf(fileNameTab[3]).intValue();
			List<Integer> jobIds = new ArrayList<Integer>();
			jobIds.add(jobid);
			List<JobCosting> jobsCosting = jobCostingService.getEagerJobCostingListForJobIds(jobIds);
			JobCosting jobCosting = null;
			if (jobsCosting != null)
				jobCosting = jobsCosting.stream()
						.filter(jc -> jc.getIdentifier().startsWith(costingNumber) && jc.getVer() == costingVersion)
						.findFirst().orElse(null);
			if (jobCosting != null)
				jobCostingService.ajaxIssueJobCostingToAdveso(jobCosting, fileName, userName);
		}
		return null;
	}

	private String verifyAndReturnFileName(String encryptedFilePath, boolean decodeFirst) {
		String result = "";
		File file = verifyAndReturnFile(encryptedFilePath, decodeFirst);
		if (file != null) {
			result = file.getName();
		}
		return result;
	}

	private File verifyAndReturnFile(String encryptedFilePath, boolean decodeFirst) {
		File result = null;
		try {
			String decryptionInput = null;
			if (decodeFirst) {
				logger.debug("Decoding : " + encryptedFilePath);
				decryptionInput = URLDecoder.decode(encryptedFilePath, "UTF-8");
			} else {
				decryptionInput = encryptedFilePath;
			}
			logger.debug("Decrypting : " + decryptionInput);
			String filePath = EncryptionTools.decrypt(decryptionInput);
			File file = new File(filePath);
			if (file.exists()) {
				logger.debug("Adding file from path " + filePath);
				result = file;
			} else {
				logger.error("File not found, skipping : " + filePath);
			}
		} catch (Exception e) {
			logger.error("Could not decode and decrypt filepath : " + encryptedFilePath, e);
		}
		return result;
	}
}