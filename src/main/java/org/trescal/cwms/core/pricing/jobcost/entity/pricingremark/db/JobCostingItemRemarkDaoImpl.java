package org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem_;
import org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.JobCostingItemRemark;
import org.trescal.cwms.core.pricing.jobcost.entity.pricingremark.JobCostingItemRemark_;

@Repository
public class JobCostingItemRemarkDaoImpl extends BaseDaoImpl<JobCostingItemRemark, Integer>
		implements JobCostingItemRemarkDao {

	@Override
	protected Class<JobCostingItemRemark> getEntity() {
		return JobCostingItemRemark.class;
	}

	@Override
	public List<JobCostingItemRemark> getRemarksForItem(Integer jobCostingItemId) {
		return getResultList(cb -> {
			CriteriaQuery<JobCostingItemRemark> cq = cb.createQuery(JobCostingItemRemark.class);
			Root<JobCostingItemRemark> root = cq.from(JobCostingItemRemark.class);
			Join<JobCostingItemRemark, JobCostingItem> jobCostingItem = root.join(JobCostingItemRemark_.pricingItem.getName());
			cq.where(cb.equal(jobCostingItem.get(JobCostingItem_.id), jobCostingItemId));
			return cq;
		});
	}

	@Override
	public List<JobCostingItemRemark> getRemarksForJobCosting(Integer jobCostingId) {
		return getResultList(cb -> {
			CriteriaQuery<JobCostingItemRemark> cq = cb.createQuery(JobCostingItemRemark.class);
			Root<JobCostingItemRemark> root = cq.from(JobCostingItemRemark.class);
			Join<JobCostingItemRemark, JobCostingItem> jobCostingItem = root.join(JobCostingItemRemark_.pricingItem.getName());
			Join<JobCostingItem, JobCosting> jobCosting = jobCostingItem.join(JobCostingItem_.jobCosting);
			cq.where(cb.equal(jobCosting.get(JobCosting_.id), jobCostingId));
			return cq;
		});
	}
}
