package org.trescal.cwms.core.pricing.purchaseorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemProgressDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.db.PurchaseOrderJobItemService;
import org.trescal.cwms.core.pricing.purchaseorder.form.EditPurchaseOrderItemForm;
import org.trescal.cwms.core.pricing.purchaseorder.form.EditPurchaseOrderItemValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.db.PresetCommentService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ThirdPartyDiscount;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Form used to add a new {@link PurchaseOrderItem} or edit an existing
 * {@link PurchaseOrderItem}.
 * 
 * @author Richard
 */
@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class EditPurchaseOrderItemController 
{
	@Autowired
	private CompanyService companyService; 
	@Autowired
	private ContactService contactServ;
	@Autowired
	private CostTypeService costTypeServ;
	@Autowired
	private EditPurchaseOrderItemValidator validator;
	@Autowired
	private NominalCodeService nominalCodeServ;
	@Autowired
	private PurchaseOrderItemService poItemServ;
	@Autowired
	private PurchaseOrderJobItemService poJobItemServ;
	@Autowired
	private PurchaseOrderService poServ;
	@Autowired
	private PresetCommentService presetCommentServ;
	@Autowired
	private SubdivService subdivService; 
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private ThirdPartyDiscount thirdPartyDiscount;
	@Autowired
	private UserService userService;
	
	@ModelAttribute("command")
	protected EditPurchaseOrderItemForm formBackingObject(HttpSession session, 
												@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
												@RequestParam(value = "poiid", required = false, defaultValue = "0") int poiid,
												@RequestParam(value = "poid", required = false, defaultValue = "0") int poid) throws Exception
	{

		PurchaseOrderItem item = this.poItemServ.findPurchaseOrderItem(poiid);
		PurchaseOrder order = this.poServ.find(poid);

		EditPurchaseOrderItemForm form = new EditPurchaseOrderItemForm();
		form.setSaveComments(false);

		if ((item == null) && (order == null)) {
			throw new Exception("Unable to find purchase order item");
		} else if (item == null) {
			form.setNewPoItem(true);
			form.setItemId(0);
			form.setOrderId(order.getId());
			form.setBusinessSubdivId(order.getJob() != null ? order.getJob().getOrganisation().getId() : subdivDto.getKey());
			Double discountRate = thirdPartyDiscount.parseValue(thirdPartyDiscount.getValueHierarchical(Scope.CONTACT, order.getContact(), order.getOrganisation()).getValue());
			form.setQuantity(1);
			form.setTotalCost(new BigDecimal("0.00"));
			form.setGeneralDiscountRate(new BigDecimal(discountRate));
		} else
		{
			order = item.getOrder();	// Only the "poiid" is provided for an edit
			form.setNewPoItem(false);
			form.setItemId(item.getId());
			form.setOrderId(order.getId());
			form.setCostTypeId(item.getCostType() != null ? item.getCostType().getTypeid() : null);
			form.setNominalId(item.getNominal() != null ? item.getNominal().getId() : null);
			form.setBusinessSubdivId(item.getBusinessSubdiv() == null ? null : item.getBusinessSubdiv().getId());
			form.setQuantity(item.getQuantity());
			form.setTotalCost(item.getTotalCost());
			form.setDescription(item.getDescription());
			form.setDeliveryDate(item.getDeliveryDate());
			form.setGeneralDiscountRate(item.getGeneralDiscountRate());

			int jobItemId = 0;
			if (!item.getLinkedJobItems().isEmpty()) {
				// Technically (previously?) possible to have multiple jobitems per po item, but not used in database
				// One job item per po item is required due to nominal code lookup
				// So we use the first linked job item (there should be at most one)
				jobItemId = item.getLinkedJobItems().first().getJi().getJobItemId();
			}
			form.setJobItemId(jobItemId);

			// test this
			form.setProgress(new PurchaseOrderItemProgressDTO(item.getId(), item.isCancelled(), item.getReceiptStatus(), item.isAccountsApproved(), item.isGoodsApproved()));
		}

		// set the different note types and preset comment types into the
		// session for use by dwr
		session.setAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME, PurchaseOrder.class);
		session.setAttribute("purchaseorderitemcomment", PresetCommentType.PURCHASE_ORDER_ITEM);
		return form;
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@RequestMapping(value="/editpurchaseorderitem.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("command") EditPurchaseOrderItemForm form, 
			BindingResult result) throws Exception
	{
		validator.validate(form, result);
		if (result.hasErrors()) return referenceData(username, form);
		else {
			Contact currentContact = this.userService.get(username).getCon();
			PurchaseOrder order = this.poServ.find(form.getOrderId());
			PurchaseOrderItem item;
			if (form.isNewPoItem()) {
				item = new PurchaseOrderItem();
				// set the itemno
				item.setItemno(this.poItemServ.getNextItemNo(order));
				item.setOrder(order);
			} else {
				item = this.poItemServ.findPurchaseOrderItem(form.getItemId());
			}
			item.setBusinessSubdiv(form.getBusinessSubdivId() == null ? null : this.subdivService.get(form.getBusinessSubdivId()));
			item.setCostType(form.getCostTypeId() == null ? null : CostType.valueOf(form.getCostTypeId()));
			item.setDeliveryDate(form.getDeliveryDate());
			item.setDescription(form.getDescription());
			item.setGeneralDiscountRate(form.getGeneralDiscountRate());
			item.setNominal(form.getNominalId() == null ? null : this.nominalCodeServ.get(form.getNominalId()));
			item.setQuantity(form.getQuantity());
			item.setTotalCost(form.getTotalCost());
			
			// update any jobitem link (due to nominal selection, only one job item allowed)
			List<Integer> jobItemIds = new ArrayList<>();
			if (form.getJobItemId() != null && form.getJobItemId() != 0)
				jobItemIds.add(form.getJobItemId());
			this.poJobItemServ.linkJobItemsToPurchaseOrder(jobItemIds, item);
			// update the costs of the item
			CostCalculator.updateItemCost(item);
			// this.poItemServ.saveOrUpdatePurchaseOrderItem(item);
			if (form.isNewPoItem())
			{
				this.poItemServ.insertPurchaseOrderItem(item);
			}
			else
			{
				// merge back into the session - session can be lost because of ajax
				// calls on page (this won't cause problems until we try and lazily
				// load properties on the item)
				item = this.poItemServ.merge(item);
	
				// call a separate method to set/update the progress fields
				this.poItemServ.updatePurchaseOrderItemProgress(form.getProgress(), item, currentContact);
	
				// persist all of the changes
				this.poItemServ.updatePurchaseOrderItem(item, currentContact);
			}
			if (form.isNewPoItem())
			{
				if (order.getItems() == null) {
					order.setItems(new HashSet<>());
				}
				order.getItems().add(item);
			}
			// update the cost of the purchase order
			CostCalculator.updatePricingFinalCost(order, order.getItems());
			order = this.poServ.merge(order, currentContact);
			// if user has requested the comment be saved then save it
			if (form.isSaveComments() && item.getDescription().trim().length() > 0) {
				PresetComment comment = new PresetComment();
				comment.setType(PresetCommentType.PURCHASE_ORDER_ITEM);
				comment.setComment(item.getDescription());
				comment.setSeton(new Date());
				comment.setSetby(currentContact);
				Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
				comment.setOrganisation(allocatedSubdiv);
				this.presetCommentServ.save(comment);
			}
			return new ModelAndView(new RedirectView("/viewpurchaseorder.htm?id=" + order.getId(), true));
		}
	}

	@RequestMapping(value = "/editpurchaseorderitem.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
		@ModelAttribute("command") EditPurchaseOrderItemForm form) {
		Map<String, Object> refData = new HashMap<>();
		PurchaseOrder order = this.poServ.find(form.getOrderId());
		refData.put("order", order);

		// set up ledger list to be used on the page
		List<Ledgers> ledgers = Arrays.asList(Ledgers.PURCHASE_LEDGER, Ledgers.CAPITAL, Ledgers.UTILITY);

		// load all jobitems for the linked job
		refData.put("jobitems", order.getJob() != null ? order.getJob().getItems() : Collections.emptySet());

		// load all available cost types
		refData.put("costTypeList", this.costTypeServ.getAllActiveCostTypes());

		// add purchase order items on this purchase order sorted by item number
		refData.put("itemList", order.getItems().stream().sorted(Comparator.comparingInt(PricingItem::getItemno)).collect(Collectors.toList()));
		refData.put("businessSubdivs", this.subdivService.getAllActiveCompanySubdivs(order.getOrganisation()));
		refData.put("defaultCurrency", this.currencyServ.getDefaultCurrency());

		// load list of all available nominals
		refData.put("nominalList", this.nominalCodeServ.getNominalCodes(ledgers));

		if (!form.isNewPoItem()) {
			PurchaseOrderItem item = this.poItemServ.findPurchaseOrderItem(form.getItemId());
			refData.put("item", item);
			// get the primary jobitem for this PO item - if > 1 item then just
			// take the first one
			if (item.getLinkedJobItems().size() > 0)
			{
				JobItem jItem = null;
				for (PurchaseOrderJobItem poji : item.getLinkedJobItems())
				{
					jItem = poji.getJi();
					break;
				}
				if (jItem != null)
				{
					NominalCode nom = this.nominalCodeServ.findBestMatchingNominalCode(ledgers, item.getCostType() != null ? item.getCostType().getTypeid() : null, jItem);

					// try and find what the nominal code for this item should
					refData.put("suggestednominal", nom);
				}
			}
		}

		// add a list of all nominals allowed
		refData.put("ledgers", ledgers);

		// add the list of available purchaseorderreceiptstatus's
		refData.put("receiptstatuslist", PurchaseOrderItemReceiptStatus.values());

		// check if this user is a member of accounts or an admin for the PO's company
		Company allocatedCompany = companyService.get(order.getOrganisation().getId());
		Contact currentContact = this.userService.get(username).getCon();
		refData.put("accountsAuthenticated", this.contactServ.contactBelongsToDepartmentOrIsAdmin(currentContact, DepartmentType.ACCOUNTS, allocatedCompany));

		return new ModelAndView("trescal/core/pricing/purchaseorder/editpurchaseorderitem", refData);
	}
}
