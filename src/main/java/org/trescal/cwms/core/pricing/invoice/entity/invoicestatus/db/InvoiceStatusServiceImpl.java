package org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;

@Service
public class InvoiceStatusServiceImpl extends BaseServiceImpl<InvoiceStatus, Integer> implements InvoiceStatusService {
	
	@Autowired
	private InvoiceStatusDao invoiceStatusDao;
	
	@Override
	protected BaseDao<InvoiceStatus, Integer> getBaseDao() {
		return invoiceStatusDao;
	}
	
	@Override
	public List<InvoiceStatus> findAllInvoiceStatusRequiringAttention() {
		return invoiceStatusDao.findAllInvoiceStatusRequiringAttention();
	}
	
	@Override
	public InvoiceStatus findOnInsertActivity() {
		return invoiceStatusDao.findOnInsertActivity();
	}
}