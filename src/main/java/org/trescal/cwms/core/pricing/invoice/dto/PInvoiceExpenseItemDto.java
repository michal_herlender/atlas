package org.trescal.cwms.core.pricing.invoice.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.jobs.job.dto.JobTypeDto;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.TranslationUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@Builder
public class PInvoiceExpenseItemDto {

    private Integer jobId;
    private Integer expenseItemId;
    private PNotInvoiced notInvoiced;
    private String jobNo;
    private Integer subdivId;
    private Integer expenseItemNo;
    private Boolean include;
    private String jobStatus;
    private List<String> poNumbers;
    private List<String> bpoNumbers;
    private String model;
    private String currencyCode;
    private BigDecimal singlePrice;
    private Integer quantity;
    private BigDecimal totalCost;
    private Integer invoiceItemsCount;
    private Integer expenseItemsCount;
    private String jobServiceClientRef;
    private String comment;
    @JsonSerialize(converter = JobTypeDto.JobTypeJsonConverter.class)
    private JobType jobType;

    private String businessSubDivName;
    private Integer businessSubDivId;

    private String jobClientRef;
    private LocalDate date;

    public static PInvoiceExpenseItemDto fromExpenseItem(PInvoiceExpenseItem item) {
        return PInvoiceExpenseItemDto.builder()
            .expenseItemId(item.getExpenseItem().getId())
            .notInvoiced(item.getNotInvoiced())
            .jobId(item.getExpenseItem().getJob().getJobid())
            .jobNo(item.getExpenseItem().getJob().getJobno())
            .jobType(item.getExpenseItem().getJob().getType())
            .subdivId(item.getExpenseItem().getJob().getCon().getSub().getSubdivid())
            .include(item.isInclude())
            .jobStatus(TranslationUtils.getBestTranslation(item.getExpenseItem().getJob().getJs().getNametranslations()).orElse(""))
            .model(InstModelTools.modelNameViaTranslations(item.getExpenseItem().getModel(), LocaleContextHolder.getLocale(), LocaleContextHolder.getLocale()))
            .currencyCode(item.getJobCosting() != null ? item.getJobCosting().getCurrency().getCurrencyCode() :
                item.getPinvoice() != null ?
                    item.getPinvoice().getCurrency().getCurrencyCode() : null)
            .singlePrice(item.getExpenseItem().getSinglePrice())
            .quantity(item.getExpenseItem().getQuantity())
            .totalCost(item.getExpenseItem().getTotalPrice())
            .invoiceItemsCount(item.getExpenseItem().getInvoiceItems().size())
            .expenseItemsCount(item.getExpenseItem().getJob().getExpenseItems().size())
            .jobServiceClientRef(item.getExpenseItem().getClientRef())
            .comment(item.getExpenseItem().getComment())
            .expenseItemNo(item.getExpenseItem().getItemNo())
            .bpoNumbers(item.getExpenseItem().getItemPOs().stream()
                .map(JobExpenseItemPO::getBpo).filter(Objects::nonNull).map(BPO::getPoNumber).collect(Collectors.toList())
            )
            .poNumbers(item.getExpenseItem().getItemPOs().stream()
                .map(JobExpenseItemPO::getPo).filter(Objects::nonNull).map(PO::getPoNumber).collect(Collectors.toList())
            )
            .jobClientRef(item.getExpenseItem().getJob().getClientRef())
            .date(item.getExpenseItem().getDate())
            .businessSubDivName(item.getExpenseItem().getJob().getOrganisation().getSubname())
            .businessSubDivId(item.getExpenseItem().getJob().getOrganisation().getSubdivid())
            .build();
    }
}
