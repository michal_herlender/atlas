package org.trescal.cwms.core.pricing.jobcost.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * @TODO this is incomplete, only put in for site costing total cost validation
 *       so far
 * @author Richard
 */
@Component
public class ViewJobCostingValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> arg0)
	{
		return JobCostingForm.class.isAssignableFrom(arg0);
	}
	
	@Override
	public void validate(Object arg0, Errors errors)
	{
		JobCostingForm form = (JobCostingForm) arg0;

		if (form.getJobCosting().getType().equals(PricingType.SITE))
		{
			if ((form.getJobCosting().getTotalCost() == null)
					|| (form.getJobCosting().getTotalCost().doubleValue() < 0.0))
			{
				errors.rejectValue("jobCosting.totalCost", "error.jobcosting.edit.totalcost", null, "A valid total cost must be specified");
			}
			errors.pushNestedPath("jobCosting");
			super.validate(form.getJobCosting(), errors, "siteCostingNote");
			errors.popNestedPath();
		}
		
		if(form.isSetclientdecision())
		{
			if(form.getClientDecision() == null)
				errors.rejectValue("clientDecision", "error.jobcosting.edit.clientapprobationtype", null, "The approbation type must be added");
			if(form.getClientApprovalOn() == null)
				errors.rejectValue("clientApprovalOn", "error.jobcosting.edit.clientapprobationdate", null, "The approbation date must be added");
		}
	}
}