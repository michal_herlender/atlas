/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.costs.base;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.tools.MathTools;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;

/**
 * Base class that all {@link Cost} entities derive from.
 * 
 * @author Richard
 */
@MappedSuperclass
public abstract class Cost extends Auditable
{
	/**
	 * Denotes if this Cost is being used/included as part of it's entity's
	 * costs.
	 */
	protected boolean active;

	/**
	 * Defines if this {@link Cost} has been automatically set by the system
	 * (true), or manually by a User.
	 */
	protected boolean autoSet;

	protected Integer costid;

	protected CostType costType;

	protected Integer customOrderBy;

	/**
	 * Percentage discount rate being applied to the totalCost
	 */
	protected BigDecimal discountRate;

	/**
	 * The monetary value of the discountRate being applied to the totalCost
	 */
	protected BigDecimal discountValue;

	/**
	 * The total cost of the costtype following discounts being applied.
	 */
	protected BigDecimal finalCost;

	/**
	 * The hourly rate that can (optionally) be used to calculate this cost.
	 * Nullable.
	 */
	protected BigDecimal hourlyRate;

	/**
	 * The total cost of this costtypes labour - typically as a result of
	 * labourTime * hourlyRate.
	 */
	protected BigDecimal labourCostTotal;

	/**
	 * Time in minutes that was taken to perform the labour associated with this
	 * cost. Nullable.
	 */
	protected Integer labourTime;

	/**
	 * The total Cost of the costtype prior to discount's being applied.
	 */
	protected BigDecimal totalCost;

	@Transient
	public BigDecimal getCalibrationFinalCostInConvertedCurrency()
	{
		BigDecimal finalCostConverted = null;

		if ((this.getLinkedCostSrc() != null)
				&& (this.getLinkedCostSrc() instanceof TPQuotationCalibrationCost)
				&& (this instanceof QuotationCalibrationCost))
		{
			TPQuotationCalibrationCost tpqcc = (TPQuotationCalibrationCost) this.getLinkedCostSrc();
			QuotationCalibrationCost qcc = (QuotationCalibrationCost) this;

			if (tpqcc.getTpQuoteItem().getTpquotation().getCurrency().getCurrencyId() != qcc.getQuoteItem().getQuotation().getCurrency().getCurrencyId())
			{
				// DIFFERENT CURRENCIES!!
				BigDecimal divisor = tpqcc.getTpQuoteItem().getTpquotation().getRate().divide(qcc.getQuoteItem().getQuotation().getRate(), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
				finalCostConverted = this.getLinkedCostSrc().getFinalCost().divide(divisor, MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
			}
		}

		return finalCostConverted;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "costid", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getCostid()
	{
		return this.costid;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "costtypeid", unique = false, nullable = false, insertable = true, updatable = true)
	public CostType getCostType()
	{
		return this.costType;
	}

	@Column(name = "customorderby", nullable = true, unique = false)
	@Type(type = "int")
	public Integer getCustomOrderBy()
	{
		return this.customOrderBy;
	}

	@NotNull
	@Column(name = "discountrate", unique = false, nullable = false, precision = 5, scale = 2)
	public BigDecimal getDiscountRate()
	{
		return this.discountRate;
	}

	@NotNull
	@Column(name = "discountvalue", unique = false, nullable = false, precision = 10, scale = 2)
	public BigDecimal getDiscountValue()
	{
		return this.discountValue;
	}

	@NotNull
	@Column(name = "finalcost", unique = false, nullable = false, precision = 10, scale = 2)
	public BigDecimal getFinalCost()
	{
		return this.finalCost;
	}

	@Column(name = "hourlyrate", precision = 10, scale = 2, nullable = true)
	public BigDecimal getHourlyRate()
	{
		return this.hourlyRate;
	}

	@Column(name = "labourcost", precision = 10, scale = 2, nullable = true)
	public BigDecimal getLabourCostTotal()
	{
		return this.labourCostTotal;
	}

	@Column(name = "labourtime")
	public Integer getLabourTime()
	{
		return this.labourTime;
	}

	@Transient
	public abstract Cost getLinkedCostSrc();

	@Transient
	public BigDecimal getPurchaseFinalCostInConvertedCurrency()
	{
		BigDecimal finalCostConverted = null;

		if ((this.getLinkedCostSrc() != null)
				&& (this.getLinkedCostSrc() instanceof TPQuotationPurchaseCost)
				&& (this instanceof QuotationPurchaseCost))
		{
			TPQuotationPurchaseCost tpqpc = (TPQuotationPurchaseCost) this.getLinkedCostSrc();
			QuotationPurchaseCost qpc = (QuotationPurchaseCost) this;

			if (tpqpc.getTpQuoteItem().getTpquotation().getCurrency().getCurrencyId() != qpc.getQuoteItem().getQuotation().getCurrency().getCurrencyId())
			{
				// DIFFERENT CURRENCIES!!
				BigDecimal divisor = tpqpc.getTpQuoteItem().getTpquotation().getRate().divide(qpc.getQuoteItem().getQuotation().getRate(), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
				finalCostConverted = this.getLinkedCostSrc().getFinalCost().divide(divisor, MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
			}
		}

		return finalCostConverted;
	}

	@NotNull
	@Column(name = "totalcost", unique = false, nullable = false, insertable = true, updatable = true, precision = 10, scale = 2)
	public BigDecimal getTotalCost()
	{
		return this.totalCost;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="tinyint")
	public boolean isActive()
	{
		return this.active;
	}

	@NotNull
	@Column(name = "autoset", nullable = false, columnDefinition="tinyint")
	public boolean isAutoSet()
	{
		return this.autoSet;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setAutoSet(boolean autoSet)
	{
		this.autoSet = autoSet;
	}

	public void setCostid(Integer costid)
	{
		this.costid = costid;
	}

	public void setCostType(CostType costType)
	{
		this.costType = costType;
	}

	public void setCustomOrderBy(Integer customOrderBy)
	{
		this.customOrderBy = customOrderBy;
	}

	public void setDiscountRate(BigDecimal discountRate)
	{
		this.discountRate = discountRate;
	}

	public void setDiscountValue(BigDecimal discountValue)
	{
		this.discountValue = discountValue;
	}

	public void setFinalCost(BigDecimal totalCost)
	{
		this.finalCost = totalCost;
	}

	public void setHourlyRate(BigDecimal hourlyRate)
	{
		this.hourlyRate = hourlyRate;
	}

	public void setLabourCostTotal(BigDecimal labourCostTotal)
	{
		this.labourCostTotal = labourCostTotal;
	}

	public void setLabourTime(Integer labourTime)
	{
		this.labourTime = labourTime;
	}

	public void setTotalCost(BigDecimal totalCost)
	{
		this.totalCost = totalCost;
	}
}