package org.trescal.cwms.core.pricing.invoice.dto;

import lombok.Data;

import java.util.List;

@Data
public class InvoiceItemInputDTO {

     List<Integer> itemIds;
     Integer invoiceId;
     Integer poId;

}
