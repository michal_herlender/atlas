package org.trescal.cwms.core.pricing;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum JobCostingClientDecision
{
	APPROVED("jobcost.clientdecision.approved"), 								
	REJECTED("jobcost.clientdecision.rejected"),
	REJECTED_REDO("jobcost.clientdecision.rejectedredo");
	
	private String messageCode;
	private ReloadableResourceBundleMessageSource messages;
	
	@Component
	public static class JobCostingClientDecisionMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (JobCostingClientDecision pt : EnumSet.allOf(JobCostingClientDecision.class))
               pt.setMessageSource(messages);
        }
	}
	
	private JobCostingClientDecision(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getMessageCode() {
		return this.messageCode;
	}
	
	public String getLocalizedName() {
		return messages == null ? name() : messages.getMessage(messageCode, null, LocaleContextHolder.getLocale());
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
}