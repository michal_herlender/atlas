package org.trescal.cwms.core.pricing.invoice.dto;

public class SummaryReadyToInvoice {
	private String businessSubdivName;
	private int clientCompanyId;
	private String clientCompanyName;
	private String completionYearMonth;		// Formatted in yyyy MM notation e.g. "2019 06" for easy comparison
	private int jobItemCount;
	private int proformaCount;
	private int expenseItemCount;
	private boolean completeJob;
	private int jiWithCompleteJob;
	private int jiWithIncompleteJob; 
	
	public SummaryReadyToInvoice(String businessSubdivName,
			int clientCompanyId, String clientCompanyName, String completionYearMonth, boolean completeJob) {
		this.businessSubdivName = businessSubdivName;
		this.clientCompanyId = clientCompanyId;
		this.clientCompanyName = clientCompanyName;
		this.completionYearMonth = completionYearMonth;		// Formatted in yyyy MM notation e.g. "2019 06" for easy comparison
		this.completeJob = completeJob;
	}
	
	public String getBusinessSubdivName() {
		return businessSubdivName;
	}
	public int getClientCompanyId() {
		return clientCompanyId;
	}
	public String getClientCompanyName() {
		return clientCompanyName;
	}
	public String getCompletionYearMonth() {
		return completionYearMonth;
	}
	public int getJobItemCount() {
		return jobItemCount;
	}
	public int getExpenseItemCount() {
		return expenseItemCount;
	}
	public int getProformaCount() {
		return proformaCount;
	}
	public void setBusinessSubdivName(String businessSubdivName) {
		this.businessSubdivName = businessSubdivName;
	}
	public void setClientCompanyId(int clientCompanyId) {
		this.clientCompanyId = clientCompanyId;
	}
	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}
	public void setCompletionYearMonth(String completionYearMonth) {
		this.completionYearMonth = completionYearMonth;
	}
	public void incrementJobItemCount() {
		this.jobItemCount++;
	}
	public void incrementJobItemCompleteJobCount() {
		this.jiWithCompleteJob++;
	}
	public void incrementJobItemIncompleteJobCount() {
		this.jiWithIncompleteJob++;
	}
	public void incrementExpenseItemCount() {
		this.expenseItemCount++;
	}
	public void incrementProformaCount() {
		this.proformaCount++;
	}
	
	public boolean isCompleteJob() {
		return completeJob;
	}

	public void setCompleteJob(boolean completeJob) {
		this.completeJob = completeJob;
	}

	public int getJiWithCompleteJob() {
		return jiWithCompleteJob;
	}

	public int getJiWithIncompleteJob() {
		return jiWithIncompleteJob;
	}
	
}
