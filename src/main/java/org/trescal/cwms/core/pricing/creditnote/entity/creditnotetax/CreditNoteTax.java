package org.trescal.cwms.core.pricing.creditnote.entity.creditnotetax;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.entity.tax.PricingTax;

@Entity
@Table(name = "creditnotetax")
@AssociationOverrides({
		@AssociationOverride(name = "pricing", joinColumns = @JoinColumn(name = "creditnoteid"), foreignKey = @ForeignKey(name = "FK_creditnotetax_invoice")),
		@AssociationOverride(name = "vatRate", joinColumns = @JoinColumn(name = "vatrateid"), foreignKey = @ForeignKey(name = "FK_creditnotetax_vatrate")) })
public class CreditNoteTax extends PricingTax<CreditNote> {
}