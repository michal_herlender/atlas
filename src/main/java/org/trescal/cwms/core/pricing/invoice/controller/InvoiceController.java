package org.trescal.cwms.core.pricing.invoice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceItemPoLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;

@Controller
@JsonController
public class InvoiceController {

	@Autowired
	private InvoiceItemService invoiceItemService;

	public static final String DELETE_ITEM_URL = "deleteinvoiceitem.json";
	public static final String ITEM_PO_LINKS_URL = "invoiceitempolinks.json";

	@ResponseBody
	@RequestMapping(value = DELETE_ITEM_URL, method = RequestMethod.POST)
	public Map<String, String> deleteInvoiceItem(@RequestBody Integer itemId) throws Exception {
		Invoice invoice = invoiceItemService.removeItemAndRecalculate(itemId);
		Map<String, String> result = new HashMap<String, String>();
		result.put("totalCost", invoice.getTotalCost().toString());
		result.put("vatValue", invoice.getVatValue().toString());
		result.put("finalCost", invoice.getFinalCost().toString());
		return result;
	}

	@ResponseBody
	@RequestMapping(value = ITEM_PO_LINKS_URL, method = RequestMethod.GET)
	public List<InvoiceItemPoLinkDTO> getItemPoLinks(@RequestParam(name = "invoiceid") Integer invoiceId,
			@RequestParam(name = "jobid", required = false) Integer jobId) {
		return null;
	}
}