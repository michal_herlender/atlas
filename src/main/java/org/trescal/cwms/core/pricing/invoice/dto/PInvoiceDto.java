package org.trescal.cwms.core.pricing.invoice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Value;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class PInvoiceDto {

    private CompanyShortDto company;
    private Integer addressId;
    private int coid;
    private int addressid;
    private AddressShortDTO address;
    private SupportedCurrencyDto currency;
    private int currencyId;
    private BigDecimal rate;
    private int typeId;
    private InvoiceType type;
    private PricingType pricingType;


    private List<AddressShortDTO> companyAddresses;
    private List<PInvoiceItemDto> items;
    private List<PInvoiceExpenseItemDto> expenseItems;


    @Value(staticConstructor = "of")
    static class CompanyShortDto {
        Integer coid;
        String coname;
    }


    @Data
    @AllArgsConstructor
    @Builder
    static class PInvoiceItemDeliveryItemDto {

        public static PInvoiceItemDeliveryItemDto fromDeliveryItem(JobDeliveryItem item) {
            return PInvoiceItemDeliveryItemDto.builder()
                .deliveryId(item.getDelivery().getDeliveryid())
                .deliveryNo(item.getDelivery().getDeliveryno())
                .deliveryDate(item.getDelivery().getDeliverydate())
                .isThirdPartyDelivery(item.getDelivery().isThirdPartyDelivery())
                .isClientDelivery(item.getDelivery().isClientDelivery())
                .countryCode(item.getDelivery().getAddress().getCountry().getCountryCode())
                .build();
        }

        private Integer deliveryId;
        private String deliveryNo;
        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDate deliveryDate;
        private String countryCode;
        private Boolean isClientDelivery;
        private Boolean isThirdPartyDelivery;
    }

    @Value(staticConstructor = "of")
    static class AddressShortDTO {
        Integer addressId;
        String subdivName;
        String addressLine;
        String town;
        List<String> addressTypes;
    }
}


