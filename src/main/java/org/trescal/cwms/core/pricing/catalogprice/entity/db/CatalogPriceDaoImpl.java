package org.trescal.cwms.core.pricing.catalogprice.entity.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.MultiValuedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType_;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory_;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceDTO;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice_;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputCatalogPrice;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupQueryType;
import org.trescal.cwms.core.pricing.variableprice.entity.VariablePrice;
import org.trescal.cwms.core.pricing.variableprice.entity.VariablePrice_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
@Repository
public class CatalogPriceDaoImpl extends BaseDaoImpl<CatalogPrice, Integer> implements CatalogPriceDao {

	private static Logger logger = LoggerFactory.getLogger(CatalogPriceDaoImpl.class);
	
	@Override
	protected Class<CatalogPrice> getEntity() {
		return CatalogPrice.class;
	}
	
	@Override
	public List<CatalogPrice> find(InstrumentModel model, ServiceType serviceType, CostType costType, Company company, boolean eager) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<CatalogPrice> cq = cb.createQuery(CatalogPrice.class);
		Root<CatalogPrice> price = cq.from(CatalogPrice.class);
		if (eager) {
			price.fetch(CatalogPrice_.instrumentModel);
			price.fetch(CatalogPrice_.serviceType);
			Fetch<CatalogPrice, Company> fetchCompany = price.fetch(CatalogPrice_.organisation.getName());
			fetchCompany.fetch(Company_.currency);
		}
		
		Predicate clauses = cb.conjunction();
		if (model != null)
			clauses.getExpressions().add(cb.equal(price.get(CatalogPrice_.instrumentModel), model));
		if (serviceType != null)
			clauses.getExpressions().add(cb.equal(price.get(CatalogPrice_.serviceType), serviceType));
		if (company != null)
			clauses.getExpressions().add(cb.equal(price.get(CatalogPrice_.organisation), company));
		if (costType != null)
			clauses.getExpressions().add(cb.equal(price.get(CatalogPrice_.costType), costType));
		cq.where(clauses);
		
		TypedQuery<CatalogPrice> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<CatalogPrice> find(int modelId, int serviceTypeId, CostType costType, int companyId, boolean eager) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<CatalogPrice> cq = cb.createQuery(CatalogPrice.class);
		Root<CatalogPrice> price = cq.from(CatalogPrice.class);
		if (eager) {
			price.fetch(CatalogPrice_.instrumentModel);
			price.fetch(CatalogPrice_.serviceType);
			Fetch<CatalogPrice, Company> fetchCompany = price.fetch(CatalogPrice_.organisation.getName());
			fetchCompany.fetch(Company_.currency);
		}
		
		Predicate clauses = cb.conjunction();
		if (modelId != 0) {
			Join<CatalogPrice,InstrumentModel> model = price.join(CatalogPrice_.instrumentModel);
			clauses.getExpressions().add(cb.equal(model.get(InstrumentModel_.modelid), modelId));
		}
		if (serviceTypeId != 0) {
			Join<CatalogPrice,ServiceType> serviceType = price.join(CatalogPrice_.serviceType);
			clauses.getExpressions().add(cb.equal(serviceType.get(ServiceType_.serviceTypeId), serviceTypeId));
		}
		if (companyId != 0) {
			Join<CatalogPrice, Company> company = price.join(CatalogPrice_.organisation.getName());
			clauses.getExpressions().add(cb.equal(company.get(Company_.coid), companyId));
		}
		if (costType != null) {
			clauses.getExpressions().add(cb.equal(price.get(CatalogPrice_.costType), costType));
		}
		cq.where(clauses);
		
		TypedQuery<CatalogPrice> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}
	
	@Override
	public List<CatalogPrice> findAll(ServiceType serviceType, List<InstrumentModel> models, Company businessCompany) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<CatalogPrice> cq = cb.createQuery(CatalogPrice.class);
		Root<CatalogPrice> catalogPrice = cq.from(CatalogPrice.class);
		cq.select(catalogPrice);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(catalogPrice.get(CatalogPrice_.serviceType), serviceType));
		clauses.getExpressions().add(catalogPrice.get(CatalogPrice_.instrumentModel).in(models));
		clauses.getExpressions().add(cb.equal(catalogPrice.get(CatalogPrice_.organisation), businessCompany));
		cq.where(clauses);
		TypedQuery<CatalogPrice> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}
	
	@Override
	public List<PriceLookupOutputCatalogPrice> findCatalogPrices(PriceLookupRequirements reqs, PriceLookupQueryType queryType, MultiValuedMap<Integer, Integer> serviceTypeMap) {
		List<PriceLookupOutputCatalogPrice> results = null;
		if ((reqs == null) || (queryType == null) || (serviceTypeMap == null)) {
			throw new IllegalArgumentException("One or more method arguments were null");
		}
		else if (serviceTypeMap.isEmpty()) {
			logger.warn("Returning empty quotation results, as input map is null/empty");
			results = Collections.emptyList();
		}
		else {
			results = getResultList(cb -> {
				CriteriaQuery<PriceLookupOutputCatalogPrice> cq = cb.createQuery(PriceLookupOutputCatalogPrice.class);
				Root<CatalogPrice> root = cq.from(CatalogPrice.class);
				Join<CatalogPrice,ServiceType> serviceType = root.join(CatalogPrice_.serviceType);
				Join<CatalogPrice, InstrumentModel> model = root.join(CatalogPrice_.instrumentModel);
				Join<InstrumentModel, SalesCategory> salesCategory = model.join(InstrumentModel_.salesCategory, JoinType.LEFT);
				Join<CatalogPrice, Company> businessCompany = root.join(CatalogPrice_.organisation.getName());
				Join<Company, SupportedCurrency> currency = businessCompany.join(Company_.currency);
				
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(root.get(CatalogPrice_.organisation), reqs.getBusinessCompanyId()));
				clauses.getExpressions().add(cb.equal(root.get(CatalogPrice_.costType), CostType.CALIBRATION));
				
				Predicate itemClauses = cb.disjunction();
				
				switch (queryType) {
				case CATALOG_PRICE_INSTRUMENT_MODEL:
					// Search for catalog prices for specific instrument model id / service type id combinations
					// serviceTypeMap is of serviceTypeId to Set of instrumentModelIds
					for (Integer serviceTypeId : serviceTypeMap.keySet()) {
						Collection<Integer> modelIds = serviceTypeMap.get(serviceTypeId);
						
						Predicate serviceTypeClauses = cb.conjunction();
						serviceTypeClauses.getExpressions().add(cb.equal(root.get(CatalogPrice_.serviceType), serviceTypeId));
						serviceTypeClauses.getExpressions().add(model.get(InstrumentModel_.modelid).in(modelIds));
						itemClauses.getExpressions().add(serviceTypeClauses);
					}
					
					break;

				case CATALOG_PRICE_SALES_CATEGORY_MODEL:
					// Search for catalog prices for specific sales category id / service type id combinations
					// serviceTypeMap is of serviceTypeId to Set of salesCategoryIds

					Join<InstrumentModel, InstrumentModelType> modelType = model.join(InstrumentModel_.modelType);
					clauses.getExpressions().add(cb.isTrue(modelType.get(InstrumentModelType_.salescategory)));
					
					for (Integer serviceTypeId : serviceTypeMap.keySet()) {
						Collection<Integer> salesCategoryIds = serviceTypeMap.get(serviceTypeId);
						
						Predicate serviceTypeClauses = cb.conjunction();
						serviceTypeClauses.getExpressions().add(cb.equal(root.get(CatalogPrice_.serviceType), serviceTypeId));
						serviceTypeClauses.getExpressions().add(salesCategory.get(SalesCategory_.id).in(salesCategoryIds));
						itemClauses.getExpressions().add(serviceTypeClauses);
					}
					
					break;

				default:
					throw new IllegalArgumentException("Unsupported query type "+queryType);
				}
				clauses.getExpressions().add(itemClauses);
				
				cq.where(clauses);
				cq.select(cb.construct(PriceLookupOutputCatalogPrice.class,
						serviceType.get(ServiceType_.serviceTypeId),
						model.get(InstrumentModel_.modelid),
						salesCategory.get(SalesCategory_.id),
						root.get(CatalogPrice_.id),
						root.get(CatalogPrice_.fixedPrice),
						currency.get(SupportedCurrency_.currencyId)
						));
				return cq;
			});
		}
		// Workaround to set query type (setting enum as literal throws NPE in Hibernate 5.2.17)
		// https://hibernate.atlassian.net/browse/HHH-12184
		// https://hibernate.atlassian.net/browse/HHH-13016
		results.forEach(output -> output.setQueryType(queryType));
		return results;
	}
	
	@Override
	public List<CatalogPriceDTO> getCatalogPricesFromSalesCategory(Integer salesCategoryId, Locale locale){
		return getResultList(cb ->{
			CriteriaQuery<CatalogPriceDTO> cq = cb.createQuery(CatalogPriceDTO.class);
			Root<CatalogPrice> root = cq.from(CatalogPrice.class);
			Join<CatalogPrice, InstrumentModel> instrumentModelJoin = root.join(CatalogPrice_.instrumentModel);
			Join<InstrumentModel, SalesCategory> salesCategoryJoin = instrumentModelJoin.join(InstrumentModel_.salesCategory);
			Join<InstrumentModel, InstrumentModelType> instrumentModelTypeJoin = instrumentModelJoin.join(InstrumentModel_.modelType);
			Join<CatalogPrice, VariablePrice> variablePrice = root.join(CatalogPrice_.variablePrice, JoinType.LEFT);
			Join<CatalogPrice,ServiceType> serviceType = root.join(CatalogPrice_.serviceType);
			Join<ServiceType, Translation> joinLongname = serviceType
					.join(ServiceType_.longnameTranslation.getName());
			joinLongname.on(cb.equal(joinLongname.get(Translation_.locale), locale));
			Join<CatalogPrice, Company> businessCompany = root.join(CatalogPrice_.organisation.getName());
			Join<Company, SupportedCurrency> supportedCurrency = businessCompany.join(Company_.currency);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(salesCategoryJoin.get(SalesCategory_.id), salesCategoryId));
			clauses.getExpressions().add(cb.isTrue(instrumentModelTypeJoin.get(InstrumentModelType_.salescategory)));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(CatalogPriceDTO.class, 
					root.get(CatalogPrice_.id),
					businessCompany.get(Company_.coid),
					businessCompany.get(Company_.coname),
					joinLongname.get(Translation_.translation),
					variablePrice.get(VariablePrice_.unit),
					variablePrice.get(VariablePrice_.unitPrice),
					root.get(CatalogPrice_.costType),
					root.get(CatalogPrice_.fixedPrice),
					supportedCurrency,
					root.get(CatalogPrice_.comments),
					instrumentModelTypeJoin.get(InstrumentModelType_.salescategory)
					));
			return cq;
		});
	}
	
	@Override
	public List<CatalogPriceDTO> findPrices(Integer instrumentModelId, Locale locale){
		return getResultList(cb ->{
			CriteriaQuery<CatalogPriceDTO> cq = cb.createQuery(CatalogPriceDTO.class);
			Root<CatalogPrice> root = cq.from(CatalogPrice.class);
			Join<CatalogPrice, InstrumentModel> instrumentModel = root.join(CatalogPrice_.instrumentModel);
			Join<InstrumentModel, InstrumentModelType> instrumentModelType = instrumentModel.join(InstrumentModel_.modelType);
			Join<CatalogPrice, VariablePrice> variablePrice = root.join(CatalogPrice_.variablePrice, JoinType.LEFT);
			Join<CatalogPrice,ServiceType> serviceType = root.join(CatalogPrice_.serviceType);
			Join<ServiceType, Translation> joinLongname = serviceType
					.join(ServiceType_.longnameTranslation.getName());
			joinLongname.on(cb.equal(joinLongname.get(Translation_.locale), locale));
			Join<CatalogPrice, Company> businessCompany = root.join(CatalogPrice_.organisation.getName());
			Join<Company, SupportedCurrency> supportedCurrency = businessCompany.join(Company_.currency);
			cq.where(cb.equal(instrumentModel.get(InstrumentModel_.modelid), instrumentModelId));
			cq.distinct(true);
			cq.select(cb.construct(CatalogPriceDTO.class, 
					root.get(CatalogPrice_.id),
					businessCompany.get(Company_.coid),
					businessCompany.get(Company_.coname),
					joinLongname.get(Translation_.translation),
					variablePrice.get(VariablePrice_.unit),
					variablePrice.get(VariablePrice_.unitPrice),
					root.get(CatalogPrice_.costType),
					root.get(CatalogPrice_.fixedPrice),
					supportedCurrency,
					root.get(CatalogPrice_.comments),
					instrumentModelType.get(InstrumentModelType_.salescategory)
					));
			return cq;
		});
	}
	
}
