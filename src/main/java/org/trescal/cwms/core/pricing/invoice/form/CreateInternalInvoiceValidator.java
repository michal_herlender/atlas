package org.trescal.cwms.core.pricing.invoice.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class CreateInternalInvoiceValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(CreateInternalInvoicesForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CreateInternalInvoicesForm form = (CreateInternalInvoicesForm) target;
		if (form.getBegin().isAfter(form.getEnd())) {
			errors.reject("", "");
		}
	}
}