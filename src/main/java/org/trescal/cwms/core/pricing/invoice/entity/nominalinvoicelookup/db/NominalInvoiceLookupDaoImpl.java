package org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.NominalInvoiceLookup;
import org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.NominalInvoiceLookup_;

@Repository("NominalInvoiceLookupDao")
public class NominalInvoiceLookupDaoImpl extends BaseDaoImpl<NominalInvoiceLookup, Integer>
		implements NominalInvoiceLookupDao {

	@Override
	protected Class<NominalInvoiceLookup> getEntity() {
		return NominalInvoiceLookup.class;
	}

	@Override
	public NominalInvoiceLookup findNominalInvoiceLookup(String nominalType) {
		return getFirstResult(cb -> {
			CriteriaQuery<NominalInvoiceLookup> cq = cb.createQuery(NominalInvoiceLookup.class);
			Root<NominalInvoiceLookup> root = cq.from(NominalInvoiceLookup.class);
			cq.where(cb.equal(root.get(NominalInvoiceLookup_.nominalType), nominalType));
			return cq;
		}).orElse(null);
	}
}