package org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.NominalInvoiceLookup;

public interface NominalInvoiceLookupDao extends BaseDao<NominalInvoiceLookup, Integer> {
	
	NominalInvoiceLookup findNominalInvoiceLookup(String nominalType);
}