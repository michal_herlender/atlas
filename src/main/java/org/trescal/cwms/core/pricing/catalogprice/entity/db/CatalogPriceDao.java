package org.trescal.cwms.core.pricing.catalogprice.entity.db;

import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.MultiValuedMap;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceDTO;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputCatalogPrice;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupQueryType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

public interface CatalogPriceDao extends BaseDao<CatalogPrice, Integer> {

	/*
	 * Returns all catalog prices matching the following criteria:
	 * InstrumentModel model, ServiceType serviceType, CostType costType, Company businessCompany
	 * Any & all parameters may be null, except eager
	 * 
	 * @param eager : true = fetches : Instrument Model, Calibration Type, Service Type, Cost Type, Company and Company.currency
	 */
	List<CatalogPrice> find(InstrumentModel model, ServiceType serviceType, CostType costType, Company company, boolean eager);
	
	/*
	 * Returns all the catalog prices matching the criteria (id of 0 is skipped)
	 * Query is performed using service type (not cal type)
	 * 
	 * @param eager : true = fetches : Instrument Model, Calibration Type, Service Type, Cost Type, Company and Company.currency
	 */
	List<CatalogPrice> find(int modelId, int calTypeId, CostType costType, int companyId, boolean eager);
		
	List<CatalogPrice> findAll(ServiceType serviceType, List<InstrumentModel> models, Company businessCompany);
	
	List<PriceLookupOutputCatalogPrice> findCatalogPrices(PriceLookupRequirements reqs, PriceLookupQueryType queryType, MultiValuedMap<Integer, Integer> serviceTypeMap);
	
	List<CatalogPriceDTO> getCatalogPricesFromSalesCategory(Integer salesCategoryId, Locale locale);
	
	List<CatalogPriceDTO> findPrices(Integer instrumentModelId, Locale locale);
}
