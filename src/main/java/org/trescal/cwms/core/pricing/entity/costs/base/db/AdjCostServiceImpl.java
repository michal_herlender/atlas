package org.trescal.cwms.core.pricing.entity.costs.base.db;

import java.util.List;

import org.trescal.cwms.core.pricing.entity.costs.base.AdjCost;

public class AdjCostServiceImpl implements AdjCostService
{
	AdjCostDao AdjCostDao;

	public AdjCost findAdjCost(int id)
	{
		return AdjCostDao.find(id);
	}

	public void insertAdjCost(AdjCost AdjCost)
	{
		AdjCostDao.persist(AdjCost);
	}

	public void updateAdjCost(AdjCost AdjCost)
	{
		AdjCostDao.update(AdjCost);
	}

	public List<AdjCost> getAllAdjCosts()
	{
		return AdjCostDao.findAll();
	}

	public void setAdjCostDao(AdjCostDao AdjCostDao)
	{
		this.AdjCostDao = AdjCostDao;
	}

	public void deleteAdjCost(AdjCost adjcost)
	{
		this.AdjCostDao.remove(adjcost);
	}

	public void saveOrUpdateAdjCost(AdjCost adjcost)
	{
		this.AdjCostDao.saveOrUpdate(adjcost);
	}
}