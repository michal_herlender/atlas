package org.trescal.cwms.core.pricing.entity.costs.base.db;

import java.util.List;

import org.trescal.cwms.core.pricing.entity.costs.base.PurchaseCost;

/**
 * Provides functions for accessing and updating children of {@link PurchaseCost}.
 * @author Richard
 *
 */
public interface PurchaseCostService
{
	PurchaseCost findPurchaseCost(int id);
	void insertPurchaseCost(PurchaseCost purchasecost);
	void updatePurchaseCost(PurchaseCost purchasecost);
	List<PurchaseCost> getAllPurchaseCosts();
	void deletePurchaseCost(PurchaseCost purchasecost);
	void saveOrUpdatePurchaseCost(PurchaseCost purchasecost);
}