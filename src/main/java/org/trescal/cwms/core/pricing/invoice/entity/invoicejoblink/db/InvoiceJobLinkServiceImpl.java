package org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceJobLinkDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLinkAjaxDto;
import org.trescal.cwms.core.validation.BeanValidator;

public class InvoiceJobLinkServiceImpl implements InvoiceJobLinkService {
	private InvoiceJobLinkDao invoiceJobLinkDao;
	private InvoiceService invoiceService;
	private JobService jobService;
	private MessageSource messages;
	private BeanValidator validator;

	@Override
	public ResultWrapper ajaxDeleteJobLink(int id) {
		InvoiceJobLink jl = this.findInvoiceJobLink(id);
		if (jl == null) {
			return new ResultWrapper(false, this.messages.getMessage("error.invoicejoblink.notfound", null,
					"The invoice job link could not be found", null));
		} else {
			this.deleteInvoiceJobLink(jl);
			return new ResultWrapper(true, null);
		}
	}

	@Override
	public ResultWrapper ajaxSaveOrUpdateJobLinks(List<InvoiceJobLinkAjaxDto> invoiceJobLinks) {
		ArrayList<InvoiceJobLink> jls = new ArrayList<InvoiceJobLink>();
		for (InvoiceJobLinkAjaxDto jla : invoiceJobLinks) {
			InvoiceJobLink jl = null;
			if (jla.getId() == null) {
				jl = new InvoiceJobLink();
				jl.setInvoice(this.invoiceService.findInvoice(jla.getInvoiceId()));
			} else {
				jl = this.invoiceJobLinkDao.find(jla.getId());
			}

			// create copy of the joblink which is not attached to the hibernate
			// session, this way if validation fails and we return out of the
			// method without saving then hibernate won't still try and persist
			// our invalid changes.
			InvoiceJobLink valiCopy = new InvoiceJobLink(jl);

			Job job = this.jobService.findByJobNo(jla.getJobNo());
			valiCopy.setJob(job);
			valiCopy.setSystemJob(job != null);
			valiCopy.setJobno(jla.getJobNo());

			BindException errors = new BindException(jl, "jl");
			// perform validation
			this.validator.validate(valiCopy, errors);

			// if validation passes then save the jl
			if (!errors.hasErrors()) {
				// validation has passed so copy the fields on the temp bean
				// over to the actual entity
				BeanUtils.copyProperties(valiCopy, jl);
				this.saveOrUpdateInvoiceJobLink(jl);
				jls.add(jl);
			} else {
				// fail as soon as we hit an error
				return new ResultWrapper(errors, jl);
			}
		}

		// if we get here there have been no errors
		return new ResultWrapper(true, null, jls, null);
	}

	@Override
	public void deleteInvoiceJobLink(InvoiceJobLink invoiceJobLink) {
		this.invoiceJobLinkDao.remove(invoiceJobLink);
	}

	@Override
	public InvoiceJobLink findByJobNo(String jobno) {
		return this.invoiceJobLinkDao.findByJobNo(jobno);
	}

	@Override
	public List<InvoiceJobLink> findForInvoice(int invoiceid) {
		return this.invoiceJobLinkDao.findForInvoice(invoiceid);
	}

	@Override
	public InvoiceJobLink findInvoiceJobLink(int id) {
		return this.invoiceJobLinkDao.find(id);
	}

	@Override
	public List<InvoiceJobLink> getLinksForJobNo(String jobno) {
		return this.invoiceJobLinkDao.getLinksForJobNo(jobno);
	}

	@Override
	public void saveOrUpdateInvoiceJobLink(InvoiceJobLink invoiceJobLink) {
		this.invoiceJobLinkDao.saveOrUpdate(invoiceJobLink);

	}

	@Override
	public List<InvoiceJobLinkDTO> getLinksByIds(Collection<Integer> invoiceIds) {
		return this.invoiceJobLinkDao.getLinksByIds(invoiceIds);
	}

	public void setInvoiceJobLinkDao(InvoiceJobLinkDao invoiceJobLinkDao) {
		this.invoiceJobLinkDao = invoiceJobLinkDao;
	}

	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public void setMessages(MessageSource messages) {
		this.messages = messages;
	}

	public void setValidator(BeanValidator validator) {
		this.validator = validator;
	}

}
