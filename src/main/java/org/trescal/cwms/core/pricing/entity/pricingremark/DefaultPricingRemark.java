package org.trescal.cwms.core.pricing.entity.pricingremark;

import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.company.Company;

/**
 * Configures default remarks by business company
 * @author galen
 *
 */
@Entity
@Table(name="defaultpricingremark")
public class DefaultPricingRemark extends PricingRemark {
	private Company organisation;
	private Locale locale;

	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orgid", nullable=false, foreignKey=@ForeignKey(name="FK_defaultpricingremark_organisation"))
	public Company getOrganisation() {
		return organisation;
	}
	
	@NotNull
	@Column(name="locale", nullable=false)
	public Locale getLocale() {
		return locale;
	}
	
	public void setOrganisation(Company organisation) {
		this.organisation = organisation;
	}
	
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
