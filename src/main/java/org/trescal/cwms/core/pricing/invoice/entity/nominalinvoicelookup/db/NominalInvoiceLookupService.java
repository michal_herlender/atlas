package org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.db;

import java.util.List;

import org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.NominalInvoiceLookup;

public interface NominalInvoiceLookupService
{
	void deleteNominalInvoiceLookup(NominalInvoiceLookup nominalinvoicelookup);

	NominalInvoiceLookup findNominalInvoiceLookup(int id);

	NominalInvoiceLookup findNominalInvoiceLookup(String nominalType);

	List<NominalInvoiceLookup> getAllNominalInvoiceLookups();

	void insertNominalInvoiceLookup(NominalInvoiceLookup nominalinvoicelookup);

	void saveOrUpdateNominalInvoiceLookup(NominalInvoiceLookup nominalinvoicelookup);

	void updateNominalInvoiceLookup(NominalInvoiceLookup nominalinvoicelookup);
}