package org.trescal.cwms.core.pricing.invoice.form;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.pricing.invoice.dto.PInvoice;

import lombok.Data;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateInvoiceForm {

    private Integer addressid;
    private int coid;
    private Integer currencyId;
    private List<Integer> jobids;
    private List<PInvoice> proposedInvoice;
    private int countItemsAlreadyInvoiced;
    private boolean approveGoodsOnAllPOs;
    private boolean companyWide;
    private Integer allocatedSubdivId;
}