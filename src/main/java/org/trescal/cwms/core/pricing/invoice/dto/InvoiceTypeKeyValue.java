package org.trescal.cwms.core.pricing.invoice.dto;

import org.trescal.cwms.spring.model.KeyValue;

public class InvoiceTypeKeyValue extends KeyValue<Integer, String> {

	public InvoiceTypeKeyValue(Integer key, String value) {
		super(key, value);
	}
}