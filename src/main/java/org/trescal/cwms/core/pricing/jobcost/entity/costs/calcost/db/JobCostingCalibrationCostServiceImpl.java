package org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.dto.ModelJobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostServiceSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingLinkedCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Service("JobCostingCalibrationCostService")
public class JobCostingCalibrationCostServiceImpl extends JobCostingCostServiceSupport<JobCostingCalibrationCost> implements JobCostingCalibrationCostService {
    @Autowired
    private JobCostingCalibrationCostDao jobCostingCalibrationCostDao;
    @Value("${cwms.config.costing.costs.lookuphierarchy.calibration}")
    private String costSourceHierarchy;
    @Value("${cwms.config.costing.costs.lookupcosts.calibration}")
    private boolean lookupCosts;
    @Value("${cwms.config.costing.costs.deactivateonzero.calibration}")
    protected boolean deactivateCostOnZeroValue;

    @Override
    public boolean getLookupCosts() {
        return lookupCosts;
    }

    @Override
    public boolean getDeactivateCostOnZeroValue() {
        return deactivateCostOnZeroValue;
    }

    @Override
    public String getCostSourceHierarchy() {
        return costSourceHierarchy;
    }

    @Override
    public void setCostSourceHierarchy(String costSourceHierarchy) {
        this.costSourceHierarchy = costSourceHierarchy;
    }

	@Override
	public Cost createCostFromCopy(JobCostingItem ji, JobCostingItem old)
	{
		JobCostingCalibrationCost newCost = null;

		if ((old != null) && (old.getCalibrationCost() != null))
		{
			newCost = new JobCostingCalibrationCost();

			JobCostingCalibrationCost oldCost = old.getCalibrationCost();
			BeanUtils.copyProperties(oldCost, newCost);

			newCost.setCostid(null);
			newCost.setJobCostingItem(ji);
			newCost.setAutoSet(true);

			if (oldCost.getLinkedCost() != null)
			{
				JobCostingLinkedCalibrationCost oldLinkedCost = oldCost.getLinkedCost();
				JobCostingLinkedCalibrationCost newLinkedCost = new JobCostingLinkedCalibrationCost();
				BeanUtils.copyProperties(oldLinkedCost, newLinkedCost);
				newLinkedCost.setId(0);
				newLinkedCost.setCalibrationCost(newCost);
				newCost.setLinkedCost(newLinkedCost);
			}
		}
		return newCost;
	}

	public void deleteJobCostingCalibrationCost(JobCostingCalibrationCost jobcostingcalibrationcost)
	{
		this.jobCostingCalibrationCostDao.remove(jobcostingcalibrationcost);
	}

	@Override
	public JobCostingCalibrationCost findEagerJobCostingCalibrationCost(int id)
	{
		return this.jobCostingCalibrationCostDao.findEagerJobCostingCalibrationCost(id);
	}

	public JobCostingCalibrationCost findJobCostingCalibrationCost(int id)
	{
		return this.jobCostingCalibrationCostDao.find(id);
	}

	@Override
	public List<JobCostingCalibrationCost> findMatchingCosts(Integer plantid, Integer coid, int modelid, Integer caltypeid, Integer page, Integer resPerPage)
	{
		return this.jobCostingCalibrationCostDao.findMatchingCosts(JobCostingCalibrationCost.class, plantid, coid, modelid, caltypeid, this.resultsYearFilter, page, resPerPage);
	}
	
	@Override
	public List<ModelJobCostingCalibrationCost> findMostRecentCostForModels(Collection<Integer> modelIds, CalibrationType calType, Company company) {
		return this.jobCostingCalibrationCostDao.findMostRecentCostForModels(modelIds, calType, company);
	}
	
	public List<JobCostingCalibrationCost> getAllJobCostingCalibrationCosts()
	{
		return this.jobCostingCalibrationCostDao.findAll();
	}

	public void insertJobCostingCalibrationCost(JobCostingCalibrationCost JobCostingCalibrationCost)
	{
		this.jobCostingCalibrationCostDao.persist(JobCostingCalibrationCost);
	}

	@Override
	public Cost resolveCostLookup(CostSource source, JobCostingItem jci)
	{
		JobCostingCalibrationCost cost = null;

		boolean costFound = false;

		this.logger.info("loading job costing calibration costs from " + source);
		switch (source)
		{
			case CONTRACT_REVIEW:
				this.logger.info("Attempting to set costs using " + source
						+ " strategy");

				JobItem ji = jci.getJobItem();
				if (ji.getCalibrationCost() != null)
				{
					ContractReviewCalibrationCost calCost = ji.getCalibrationCost();
					this.logger.info("Contract review costs found as costing source. Contract review costs set from "
							+ calCost.getCostSrc()
							+ " strategy from "
							+ calCost.getCostid());

					cost = new JobCostingCalibrationCost();

					cost.setCostSrc(calCost.getCostSrc());

					// use the costs set at contract review
					cost.setHouseCost(calCost.getTotalCost());
					cost.setDiscountRate(calCost.getDiscountRate());
					cost.setDiscountValue(calCost.getDiscountValue());
					cost.setTotalCost(calCost.getTotalCost());
					cost.setFinalCost(calCost.getFinalCost());
					costFound = true;

					// set empty third party costs
					this.configureTPCosts(cost);

					// if a linked cost is being used, link the contract review
					// linked cost to the jobcosting linked cost
					if (ji.getCalibrationCost().getLinkedCost() != null)
					{
						ContractReviewLinkedCalibrationCost cr = ji.getCalibrationCost().getLinkedCost();

						JobCostingLinkedCalibrationCost lc = new JobCostingLinkedCalibrationCost();
						lc.setCalibrationCost(cost);
						if (calCost.getCostSrc() == CostSource.JOB_COSTING)
						{
							// set the house cost - this is where we will need
							// to look at TP stuff as well
							cost.setHouseCost(cr.getJobCostingCalibrationCost().getHouseCost());
							lc.setJobCostingCalibrationCost(cr.getJobCostingCalibrationCost());
						}
						else if (calCost.getCostSrc() == CostSource.QUOTATION)
						{
							// set the house cost - this is where we will need
							// to look at TP stuff as well
							cost.setHouseCost(cr.getQuotationCalibrationCost().getHouseCost());
							lc.setQuotationCalibrationCost(cr.getQuotationCalibrationCost());
						}
						
						cost.setLinkedCost(lc);
					}
				}
				else
				{
					this.logger.info("Failed setting costs using " + source
							+ " strategy, no costs found");
				}

				break;
			case JOB:
				this.logger.info(source + " costs are not yet implemented");
				break;
			case JOB_COSTING:
				this.logger.info(source + " costs are not yet implemented");
				break;
			case QUOTATION:
				this.logger.info(source + " costs are not supported");
				break;
			case INSTRUMENT:
				this.logger.info(source + " costs are not supported");
				break;
			case MODEL:
				this.logger.info(source + " costs are not supported");
				break;
			case MANUAL:
				cost = new JobCostingCalibrationCost();
				cost.setDiscountRate(new BigDecimal(0.00));
				cost.setDiscountValue(new BigDecimal(0.00));
				cost.setHouseCost(new BigDecimal(0.00));
				cost.setFinalCost(new BigDecimal(0.00));
				cost.setTotalCost(new BigDecimal(0.00));
				cost.setCostSrc(CostSource.MANUAL);

				// set empty third party costs
				this.configureTPCosts(cost);

				costFound = true;
				cost.setCostSrc(source);
				break;
		case DERIVED:
			break;
		default:
			break;
		}

		if (costFound)
		{
			this.logger.info("Calibration costs set using " + source
					+ " strategy");
			cost.setJobCostingItem(jci);
			cost.setCostType(CostType.CALIBRATION);
			cost.setActive(true);
			cost.setAutoSet(true);
		}

		return cost;
	}

	public void saveOrUpdateJobCostingCalibrationCost(JobCostingCalibrationCost jobcostingcalibrationcost)
	{
		this.jobCostingCalibrationCostDao.saveOrUpdate(jobcostingcalibrationcost);
	}

	@Override
	public JobCostingCalibrationCost updateCosts(JobCostingCalibrationCost cost)
	{
		if (cost.getHouseCost() == null) cost.setHouseCost(new BigDecimal("0.00"));
		// set carriage in/out from linked cost source
		if (cost.getLinkedCostSrc() != null && cost.getLinkedCostSrc().getTpQuoteItem() != null) {
			cost.setTpCarriageIn(cost.getLinkedCostSrc().getTpQuoteItem().getCarriageIn());
			cost.setTpCarriageOut(cost.getLinkedCostSrc().getTpQuoteItem().getCarriageOut());
		}
		// calculate TP costs
		cost = (JobCostingCalibrationCost) this.thirdCostService.recalculateThirdPartyCosts(cost);

		cost.setTotalCost(cost.getHouseCost().add(cost.getThirdCostTotal()));

		// delegate to CostCalculator to apply any discounts and set the final
		// cost
		CostCalculator.updateSingleCost(cost, this.roundUpFinalCosts);
		return cost;
	}

	public void updateJobCostingCalibrationCost(JobCostingCalibrationCost JobCostingCalibrationCost)
	{
		this.jobCostingCalibrationCostDao.update(JobCostingCalibrationCost);
	}
}