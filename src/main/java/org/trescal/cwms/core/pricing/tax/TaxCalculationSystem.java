package org.trescal.cwms.core.pricing.tax;

public enum TaxCalculationSystem {

	DEFAULT, AVATAX;
}