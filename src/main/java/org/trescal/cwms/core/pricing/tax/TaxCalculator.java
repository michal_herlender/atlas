package org.trescal.cwms.core.pricing.tax;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.avalara.AvalaraConnector;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotetax.CreditNoteTax;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.TaxablePricingItem;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.TaxablePricing;
import org.trescal.cwms.core.pricing.entity.tax.PricingTax;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetax.InvoiceTax;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.tools.MathTools;

import lombok.extern.slf4j.Slf4j;
import net.avalara.avatax.rest.client.TransactionBuilder;
import net.avalara.avatax.rest.client.enums.DocumentType;
import net.avalara.avatax.rest.client.enums.TransactionAddressType;
import net.avalara.avatax.rest.client.models.TransactionLineModel;
import net.avalara.avatax.rest.client.models.TransactionModel;
import net.avalara.avatax.rest.client.models.TransactionSummary;

@Component
@Slf4j
public class TaxCalculator {

    @Value("${cwms.config.avalara.aware}")
    private Boolean avalaraAware;
    @Autowired
    private AddressSettingsForAllocatedCompanyService addressSettingsService;
    @Autowired
    private AvalaraConnector avalaraConnector;
    @Autowired
    private CompanySettingsForAllocatedCompanyService companySettingsService;
    @Autowired
    private DeliveryService deliveryService;
    @Autowired
    private MessageSource messageSource;

	/**
	 * Can be used for all subclasses of {@link Pricing} except invoices and credit notes. The VAT rate must be set on the pricing before.
	 * @param pricing a pricing, for which the VAT rate is already set
	 */
	public void calculateAndSetVATaxAndFinalCost(Pricing<?> pricing) {
		if (pricing.getVatRate() != null) {
			BigDecimal vatFactor = pricing.getVatRate().doubleValue() > 0
					? pricing.getVatRate().divide(BigDecimal.valueOf(100))
					: BigDecimal.valueOf(0,2);
			pricing.setVatValue(vatFactor.multiply(pricing.getTotalCost()).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
		} else {
			pricing.setVatValue(BigDecimal.valueOf(0,2));
		}
		pricing.setFinalCost(pricing.getTotalCost().add(pricing.getVatValue()));
	}

    public boolean calculateTax(Invoice invoice) {
        TaxCalculationSystem taxCalculationSystem = avalaraAware ? TaxCalculationSystem.AVATAX
            : TaxCalculationSystem.DEFAULT;
        switch (taxCalculationSystem) {
            case AVATAX:
                return calculateTaxWithAvalara(invoice);
            case DEFAULT:
            default:
                return calculateTaxWithAtlasSettings(invoice);
        }
    }

	public boolean calculateTax(CreditNote creditNote) {
		TaxCalculationSystem taxCalculationSystem = avalaraAware ? TaxCalculationSystem.AVATAX
				: TaxCalculationSystem.DEFAULT;
		return calculateTax(creditNote, taxCalculationSystem);
	}

    public boolean calculateTax(CreditNote creditNote, TaxCalculationSystem taxCalculationSystem) {
		switch (taxCalculationSystem) {
		case AVATAX:
			return calculateTaxWithAvalara(creditNote);
		case DEFAULT:
		default:
			return calculateTaxWithAtlasSettings(creditNote);
		}
	}

	private Address shipFromAddress(Invoice invoice) {
		if (invoice.getItems() != null) {
			for (InvoiceItem invoiceItem : invoice.getItems())
				if (invoiceItem.getJobItem() != null) {
					List<? extends Delivery> deliveries = this.deliveryService
							.findByJobItem(invoiceItem.getJobItem().getJobItemId(), DeliveryType.CLIENT);
					if (!deliveries.isEmpty())
						return deliveries.get(0).getOrganisation().getDefaultAddress();
					else
						return invoiceItem.getJobItem().getJob().getOrganisation().getDefaultAddress();
				} else if (invoiceItem.getExpenseItem() != null)
					return invoiceItem.getExpenseItem().getJob().getOrganisation().getDefaultAddress();
		}
		return invoice.getBusinessContact().getDefAddress();
	}

	private Address shipToAddress(Invoice invoice) {
		// There should be an unique ship-to-address by invoice
		if (invoice.getItems() != null)
			// Get ship-to-address from latest client delivery of first invoice item linked
			// to a job item
			for (InvoiceItem invoiceItem : invoice.getItems()) {
				if (invoiceItem.getJobItem() != null) {
					// Sorted in descending order by delivery date (in case of multiple)
					List<? extends Delivery> deliveries = this.deliveryService
							.findByJobItem(invoiceItem.getJobItem().getJobItemId(), DeliveryType.CLIENT);
					if (!deliveries.isEmpty())
						return deliveries.get(0).getAddress();
				}
			}
		// If we cannot find a ship-to-address, then we use invoice address
		return invoice.getAddress();
	}

	private <PricingType extends TaxablePricing<?, ItemType, TaxType>, ItemType extends TaxablePricingItem, TaxType extends PricingTax<PricingType>> boolean calculateTaxByVatRate(
			PricingType pricing, Class<TaxType> taxClass) {
		if (pricing.getVatRate() != null) {
			BigDecimal vatm = pricing.getVatRate().doubleValue() > 0
					? pricing.getVatRate().divide(new BigDecimal("100.00"))
					: new BigDecimal("0.00");
			pricing.setVatValue(
					vatm.multiply(pricing.getTotalCost()).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
			pricing.getItems().forEach(item -> {
				item.setTaxable(true);
				item.setTaxAmount(pricing.getVatRate().multiply(item.getFinalCost()).divide(BigDecimal.valueOf(100),
						MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN));
			});
			if (pricing.getTaxes() == null)
				pricing.setTaxes(new ArrayList<>());
			else
				pricing.getTaxes().clear();
			try {
                TaxType pricingTax = taxClass.newInstance();
                Locale locale = LocaleContextHolder.getLocale();
                pricingTax.setDescription(messageSource.getMessage("vat", null, "VAT", locale));
                pricingTax.setAmount(pricing.getTotalCost());
                pricingTax.setRate(pricing.getVatRate().divide(BigDecimal.valueOf(100)));
                pricingTax.setTax(pricing.getVatValue());
                pricingTax.setPricing(pricing);
                pricing.getTaxes().add(pricingTax);
                return true;
            } catch (InstantiationException | IllegalAccessException e) {
				log.error("Cannot instantiate class " + taxClass.getName());
				return false;
			}
		} else {
			pricing.setVatValue(new BigDecimal("0.00"));
			return true;
		}
	}

	public boolean calculateTaxWithAtlasSettings(CreditNote creditNote) {
		return calculateTaxByVatRate(creditNote, CreditNoteTax.class);
	}

	public boolean calculateTaxWithAtlasSettings(Invoice invoice) {
		Address shipToAddress = shipToAddress(invoice);
		Company allocatedCompany = invoice.getOrganisation();
		CompanySettingsForAllocatedCompany companySettings = companySettingsService.getByCompany(invoice.getComp(),
				allocatedCompany);
		if (invoice.getVatRateEntity() == null) {
			if (companySettings.isTaxable()) {
				AddressSettingsForAllocatedCompany addressSettings = this.addressSettingsService
						.getByCompany(shipToAddress, allocatedCompany);
				log.debug("Checking VAT rate via delivery address id " + shipToAddress.getAddrid());
				if (addressSettings != null && addressSettings.getVatRate() != null) {
					log.debug("VAT rate from address settings");
					VatRate vatRate = addressSettings.getVatRate();
					invoice.setVatRateEntity(vatRate);
					invoice.setVatRate(vatRate.getRate());
				} else {
					log.debug("VAT rate from company settings");
					VatRate vatRate = companySettings.getVatrate();
					invoice.setVatRateEntity(vatRate);
					invoice.setVatRate(vatRate.getRate());
				}
			} else {
				log.debug("Setting VAT rate value to zero as company is not taxable");
				invoice.setVatRate(new BigDecimal("0.00"));
			}
		}
		return calculateTaxByVatRate(invoice, InvoiceTax.class);
	}

	private void setShipAddresses(TransactionBuilder tb, Invoice invoice) {
		Address shipFromAddress = shipFromAddress(invoice);
		if (shipFromAddress != null)
			tb.withAddress(TransactionAddressType.ShipFrom, shipFromAddress.getAddr1(), shipFromAddress.getAddr2(),
					shipFromAddress.getAddr3(), shipFromAddress.getTown(), shipFromAddress.getCounty(),
					shipFromAddress.getPostcode(), shipFromAddress.getCountry().getCountryCode());
		Address shipToAddress = shipToAddress(invoice);
		if (shipToAddress != null)
			tb.withAddress(TransactionAddressType.ShipTo, shipToAddress.getAddr1(), shipToAddress.getAddr2(),
					shipToAddress.getAddr3(), shipToAddress.getTown(), shipToAddress.getCounty(),
					shipToAddress.getPostcode(), shipToAddress.getCountry().getCountryCode());
	}

	private <PricingType extends TaxablePricing<?, ItemType, TaxType>, ItemType extends TaxablePricingItem, TaxType extends PricingTax<PricingType>> boolean createTransaction(
			TransactionBuilder tb, PricingType pricing, Class<TaxType> taxClass) {
		try {
			TransactionModel transaction = tb.create();
			log.debug("Tax calculated by Avatax: " + transaction.getTotalTax());
			List<TransactionLineModel> lines = transaction.getLines();
			for (TransactionLineModel line : lines) {
				try {
					pricing.getItems().stream()
							.filter(i -> i.getItemno().equals(Integer.parseInt(line.getLineNumber()))).findAny()
							.ifPresent(item -> {
								item.setTaxable(line.getIsItemTaxable());
								item.setTaxAmount(line.getTax());
							});
				} catch (Exception ex) {
					log.debug("Issues with parseInt due to break-up-costs (deprecated)");
				}
			}
			List<TransactionSummary> summary = transaction.getSummary();
			if (pricing.getTaxes() == null)
				pricing.setTaxes(new ArrayList<>());
			else
				pricing.getTaxes().clear();
			for (TransactionSummary tax : summary) {
				log.debug("Add tax: " + tax.getJurisName());
				TaxType pricingTax = taxClass.newInstance();
				pricingTax.setDescription(tax.getJurisName());
				pricingTax.setAmount(tax.getTaxable());
				pricingTax.setRate(tax.getRate());
				pricingTax.setTax(tax.getTax());
				pricingTax.setPricing(pricing);
				pricing.getTaxes().add(pricingTax);
			}
			pricing.setVatValue(transaction.getTotalTax());
			return true;
		} catch (InstantiationException | IllegalAccessException e) {
			log.error("Cannot instantiate class " + taxClass.getName());
			return false;
		} catch (Exception e) {
			log.error("Cannot create transaction in Avatax");
			log.error(e.getMessage());
			return false;
		}
	}

	public boolean calculateTaxWithAvalara(CreditNote creditNote) {
		log.debug("Calculate tax for a credit note with Avatax");
		Invoice invoice = creditNote.getInvoice();
		TransactionBuilder tb = avalaraConnector.getTransactionBuilder(DocumentType.ReturnInvoice,
				creditNote.getOrganisation().getCoid(), invoice.getComp().getFiscalIdentifier());
		if (tb == null)
			// No Avalara configuration for invoicing business company
			// Use Atlas default
			return calculateTaxWithAtlasSettings(creditNote);
		else {
			String taxCode;
			for (CreditNoteItem item : creditNote.getItems()) {
				taxCode = item.getNominal().getAvalaraTaxCode();
				tb.withLine(item.getItemno().toString(), item.getFinalCost(), BigDecimal.valueOf(item.getQuantity()),
						taxCode);
			}
			setShipAddresses(tb, invoice);
			tb.withDate(dateFromLocalDate(invoice.getInvoiceDate()));
			tb.withCode(creditNote.getCreditNoteNo());
			return createTransaction(tb, creditNote, CreditNoteTax.class);
		}
	}

	public boolean calculateTaxWithAvalara(Invoice invoice) {
		log.debug("Calculate tax for an invoice with Avatax");
		TransactionBuilder tb = avalaraConnector.getTransactionBuilder(DocumentType.SalesInvoice,
				invoice.getOrganisation().getCoid(), invoice.getComp().getFiscalIdentifier());
		if (tb == null)
			// No Avalara configuration for invoicing business company
			// Use Atlas default
			return calculateTaxWithAtlasSettings(invoice);
		else {
			String taxCode;
			for (InvoiceItem item : invoice.getItems()) {
				if (item.isBreakUpCosts()) {
					// TODO: Remove when split costs is disabled
					int costCounter = 1;
					if (item.getAdjustmentCost() != null
							&& item.getAdjustmentCost().getFinalCost().compareTo(BigDecimal.ZERO) != 0) {
                        taxCode = item.getAdjustmentCost().getNominal().getAvalaraTaxCode();
                        tb.withLine(item.getItemno() + "-" + costCounter++,
                            item.getAdjustmentCost().getFinalCost(), BigDecimal.valueOf(item.getQuantity()),
                            taxCode);
                    }
					if (item.getCalibrationCost() != null
							&& item.getCalibrationCost().getFinalCost().compareTo(BigDecimal.ZERO) != 0) {
                        taxCode = item.getCalibrationCost().getNominal().getAvalaraTaxCode();
                        tb.withLine(item.getItemno() + "-" + costCounter++,
                            item.getCalibrationCost().getFinalCost(), BigDecimal.valueOf(item.getQuantity()),
                            taxCode);
                    }
					if (item.getRepairCost() != null
							&& item.getRepairCost().getFinalCost().compareTo(BigDecimal.ZERO) != 0) {
                        taxCode = item.getRepairCost().getNominal().getAvalaraTaxCode();
                        tb.withLine(item.getItemno() + "-" + costCounter++,
                            item.getRepairCost().getFinalCost(), BigDecimal.valueOf(item.getQuantity()), taxCode);
                    }
					if (item.getPurchaseCost() != null
							&& item.getPurchaseCost().getFinalCost().compareTo(BigDecimal.ZERO) != 0) {
                        taxCode = item.getPurchaseCost().getNominal().getAvalaraTaxCode();
                        tb.withLine(item.getItemno() + "-" + costCounter++,
                            item.getPurchaseCost().getFinalCost(), BigDecimal.valueOf(item.getQuantity()), taxCode);
                    }
					if (item.getServiceCost() != null
							&& item.getServiceCost().getFinalCost().compareTo(BigDecimal.ZERO) != 0) {
                        taxCode = item.getServiceCost().getNominal().getAvalaraTaxCode();
                        tb.withLine(item.getItemno() + "-" + costCounter,
                            item.getServiceCost().getFinalCost(), BigDecimal.valueOf(item.getQuantity()), taxCode);
                    }
				} else {
					taxCode = item.getNominal() == null ? null : item.getNominal().getAvalaraTaxCode();
					tb.withLine(item.getItemno().toString(), item.getFinalCost(),
							BigDecimal.valueOf(item.getQuantity()), taxCode);
				}
			}
			setShipAddresses(tb, invoice);
			tb.withDate(dateFromLocalDate(invoice.getInvoiceDate()));
			tb.withCode(invoice.getInvno());
			return createTransaction(tb, invoice, InvoiceTax.class);
		}
	}
}