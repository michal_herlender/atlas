package org.trescal.cwms.core.pricing.invoice.form;

import javax.validation.Valid;

import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditInvoiceItemForm {

	private Invoice invoice;
	@Valid
	private InvoiceItem item;
	private boolean newInvoiceItem;
	private Integer jobItemId;
	private Integer expenseItemId;
	private Integer businessSubdivId;
	private Integer calibrationNominalId;
	private Integer repairNominalId;
	private Integer purchaseNominalId;
	private Integer adjustmentNominalId;
	private Integer serviceNominalId;
	private Integer singleNominalId;
}