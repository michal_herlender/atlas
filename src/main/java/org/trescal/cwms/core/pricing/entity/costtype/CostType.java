package org.trescal.cwms.core.pricing.entity.costtype;

import java.util.Locale;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CostType
{
	SERVICE(0,"Service","costtype.service"), 
	CALIBRATION(1,"Calibration","costtype.calibration"), 
	ADJUSTMENT(2,"Adjustment","costtype.adjustment"), 
	REPAIR(3,"Repair","costtype.repair"), 
	PURCHASE(4,"Purchase","costtype.purchase"), 
	INSPECTION(5,"Inspection","costtype.inspection"), 
	OTHER(6,"Other","costtype.other");
	
	private int typeid;			// Same as ordinal, for clarity and compatibility with the existing CWMS2 database definitions
	private String defaultName; // Renamed from 'name' to prevent overloading enum methods 
	private String messageCode;	// Unique message code used for cost types only
	private int naturalOrder;	// Used by comparator
	private MessageSource messageSource; 
	
	private CostType(int typeid, String defaultName, String messageCode) {
		this.typeid = typeid;
		this.defaultName = defaultName;
		this.messageCode = messageCode;
		this.naturalOrder = typeid;
	}
	
	@Component
	public static class CostTypeMessageInjector {
		@Autowired
		private MessageSource messageSource;
		@PostConstruct
		public void postConstruct() {
			for (CostType costType : CostType.values()) {
				costType.setMessageSource(this.messageSource);
			}
		}
	}
	
	public static CostType valueOf(int typeid) {
		switch (typeid) {
		case 0:
			return SERVICE;
		case 1:
			return CALIBRATION;
		case 2:
			return ADJUSTMENT;
		case 3:
			return REPAIR;
		case 4:
			return PURCHASE;
		case 5:
			return INSPECTION;
		case 6:
			return OTHER;
		default:
			throw new IllegalArgumentException("Undefined type id "+typeid);
		}
	}
	
	/*
	 * For accessing name property via JSTL expressions etc...
	 */
	public String getName() {
		return this.name();
	}
	
	public String getDefaultName()
	{
		return this.defaultName;
	}

	public String getMessageCode()
	{
		return this.messageCode;
	}

	public Integer getNaturalOrder()
	{
		return this.naturalOrder;
	}

	public int getTypeid()
	{
		return this.typeid;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		return getMessageForLocale(locale);
	}
	
	public String getMessageForLocale(Locale locale) {
		return this.messageSource.getMessage(messageCode, null, defaultName, locale);
	}
}