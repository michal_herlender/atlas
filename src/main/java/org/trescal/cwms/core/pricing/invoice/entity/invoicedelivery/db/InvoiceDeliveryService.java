package org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.invoice.entity.invoicedelivery.InvoiceDelivery;

public interface InvoiceDeliveryService extends BaseService<InvoiceDelivery, Integer> {

	ResultWrapper ajaxAddInvoiceDelivery(int invId, String delNo);

	ResultWrapper ajaxDeleteInvoiceDelivery(int invDelId);
}