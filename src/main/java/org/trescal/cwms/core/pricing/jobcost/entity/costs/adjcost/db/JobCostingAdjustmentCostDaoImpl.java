package org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupportImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;

@Repository("JobCostingAdjustmentCostDao")
public class JobCostingAdjustmentCostDaoImpl extends JobCostingCostDaoSupportImpl<JobCostingAdjustmentCost> implements JobCostingAdjustmentCostDao {
	
	@Override
	protected Class<JobCostingAdjustmentCost> getEntity() {
		return JobCostingAdjustmentCost.class;
	}
}