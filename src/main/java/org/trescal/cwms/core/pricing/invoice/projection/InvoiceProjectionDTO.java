package org.trescal.cwms.core.pricing.invoice.projection;

import java.time.LocalDate;
import java.util.List;

import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemNotInvoicedDTO;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceJobLinkDTO;

import lombok.Getter;
import lombok.Setter;

/**
 * Consider making a superclass to standardize common fields
 * if expanded beyond invoice (e.g. JobCosting. Quotation, etc...)  
 *
 */
@Getter @Setter
public class InvoiceProjectionDTO {
	private Integer id;
	private Integer allocatedCompanyId; 
	private Integer companyId; 
	private String invoiceNumber;
	private LocalDate invoiceDate;

	private List<JobItemNotInvoicedDTO> periodicJobItems;
	private List<JobExpenseItemNotInvoicedDTO> periodicExpenseItems;
	private CompanyProjectionDTO company;	// The company being invoiced
	private List<InvoiceJobLinkDTO> invoiceJobLinks;

	public InvoiceProjectionDTO(Integer id, Integer allocatedCompanyId, Integer companyId, String invoiceNumber, LocalDate invoiceDate) {
		super();
		this.id = id;
		this.allocatedCompanyId = allocatedCompanyId;
		this.companyId = companyId;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
	}
}