package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem;

import java.util.Comparator;

public class PurchaseOrderItemComparator implements Comparator<PurchaseOrderItem> {

	@Override
	public int compare(PurchaseOrderItem o1, PurchaseOrderItem o2) {
		if (o1.getId() == null && o2.getId() == null) {
			return 1;
		} else {
			return (o1.getItemno().compareTo(o2.getItemno()));
		}
	}
}
