package org.trescal.cwms.core.pricing.jobcost.entity.jobcosting;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.dto.JobCostingDTO;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemService;
import org.trescal.cwms.core.system.Constants;

@Controller
@JsonController
public class JobCostingJsonController {
	
	@Autowired
	private JobCostingService jobCostingService;
	
	@Autowired
	private JobCostingItemService jobCostingItemService;
	
	
	@RequestMapping(value = "/showJobCostings.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public List<JobCostingDTO> jobCostingList(@RequestParam(name = "jobId", required = true) Integer jobId,
			Locale locale) 
	{
		return jobCostingService.getJobCostingsByJobId(jobId, locale);
	}
	
	@RequestMapping(value = "/showJobCostingItems.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public List<JobCostingItemDto> jobCostingItemList(@RequestParam(name = "jobCostingId", required = true) Integer jobCostingId,
			Locale locale) 
	{
		return jobCostingItemService.getJobCostingItemsDtoByJobCostingId(jobCostingId, locale);
	}

}
