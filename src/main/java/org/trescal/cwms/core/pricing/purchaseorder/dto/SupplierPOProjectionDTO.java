package org.trescal.cwms.core.pricing.purchaseorder.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
public class SupplierPOProjectionDTO {

	private Integer id;
	private String pono;
	private String compName;
	private String contactName;
	private LocalDate regdate;

	private String desc;
	private String jobNo;
	private String createdBy;
	private Date createdOn;

	/* contructor used for PO */
	public SupplierPOProjectionDTO(Integer id, String pono, String compName, String contactFirstname, String contactLastname, LocalDate regdate) {
		super();
		this.id = id;
		this.pono = pono;
		this.compName = compName;
		this.contactName = contactFirstname + " " + contactLastname;
		this.regdate = regdate;
	}

	/* contructor used for Quick PO */
	public SupplierPOProjectionDTO(Integer id, String pono, String desc, String jobNo, String contactFirstname, String contactLastname,
			Date createdOn) {
		super();
		this.id = id;
		this.pono = pono;
		this.desc = desc;
		this.jobNo = jobNo;
		this.createdBy = contactFirstname +" "+ contactLastname;
		this.createdOn = createdOn;
	}

}
