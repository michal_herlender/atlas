/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.costs.base;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;

/**
 * @author Richard
 */
@Entity
@Table(name = "costs_calibration")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "costtype", discriminatorType = DiscriminatorType.STRING)
public abstract class CalCost extends Cost
{
	private CostSource costSrc;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "costsource")
	public CostSource getCostSrc() {
		return this.costSrc;
	}
	
	public void setCostSrc(CostSource costSrc) {
		this.costSrc = costSrc;
	}
	
}
