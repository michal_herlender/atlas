package org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.db;

import java.util.List;

import org.trescal.cwms.core.pricing.invoice.entity.invoiceaction.InvoiceAction;

public class InvoiceActionServiceImpl implements InvoiceActionService
{
	InvoiceActionDao invoiceActionDao;

	public InvoiceAction findInvoiceAction(int id)
	{
		return invoiceActionDao.find(id);
	}

	public void insertInvoiceAction(InvoiceAction InvoiceAction)
	{
		invoiceActionDao.persist(InvoiceAction);
	}

	public void updateInvoiceAction(InvoiceAction InvoiceAction)
	{
		invoiceActionDao.update(InvoiceAction);
	}

	public List<InvoiceAction> getAllInvoiceActions()
	{
		return invoiceActionDao.findAll();
	}

	public void setInvoiceActionDao(InvoiceActionDao invoiceActionDao)
	{
		this.invoiceActionDao = invoiceActionDao;
	}

	public void deleteInvoiceAction(InvoiceAction invoiceaction)
	{
		this.invoiceActionDao.remove(invoiceaction);
	}

	public void saveOrUpdateInvoiceAction(InvoiceAction invoiceaction)
	{
		this.invoiceActionDao.saveOrUpdate(invoiceaction);
	}
}