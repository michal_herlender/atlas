package org.trescal.cwms.core.pricing.entity.costs.base.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.entity.costs.base.AdjCost;

@Repository("AdjCostDao")
public class AdjCostDaoImpl extends BaseDaoImpl<AdjCost, Integer> implements AdjCostDao {
	
	@Override
	protected Class<AdjCost> getEntity() {
		return AdjCost.class;
	}
}