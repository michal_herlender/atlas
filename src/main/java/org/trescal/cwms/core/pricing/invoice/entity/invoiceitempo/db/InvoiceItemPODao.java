package org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;

public interface InvoiceItemPODao extends BaseDao<InvoiceItemPO, Integer> {
}