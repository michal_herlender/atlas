package org.trescal.cwms.core.pricing.purchaseorder.form;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;

/**
 * Validator for use of PurchaseOrderForm in updating Goods Approval status
 * @author Galen
 * For each PurchaseOrderItem: 
 * (a) if it has goods approval the receipt status should be COMPLETE
 * (b) if it is cancelled the receipt status should be NOT_RECEIVED
 * (c) should not be able to have goods approval and cancelled at same time
 */

@Component
public class PurchaseOrderFormGoodsValidator implements Validator {

	@Autowired
	private PurchaseOrderService purchaseOrderService; 
	
	public static final String MESSAGE_CODE_APPROVAL_STATUS_MISMATCH = "error.goodsapproval.approvalstatusmismatch";
	public static final String MESSAGE_APPROVAL_STATUS_MISMATCH = "Item {0} has goods approval so should be in COMPLETE status"; 
	
	public static final String MESSAGE_CODE_CANCELLED_STATUS_MISMATCH = "error.goodsapproval.cancelledstatusmismatch";
	public static final String MESSAGE_CANCELLED_STATUS_MISMATCH = "Item {0} is cancelled so should be in NOT_RECEIVED status";
	
	public static final String MESSAGE_CODE_APPROVAL_AND_CANCELLED = "error.goodsapproval.approvalandcancelled";
	public static final String MESSAGE_APPROVAL_AND_CANCELLED = "Item {0} cannot have goods approval and be cancelled simultaneously";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(PurchaseOrderForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PurchaseOrderForm form = (PurchaseOrderForm) target;
		PurchaseOrder order = this.purchaseOrderService.find(form.getPoid());
		List<PurchaseOrderItem> itemList = new ArrayList<>(order.getItems());
		for (int i = 0; i < order.getItems().size(); i++) {
			PurchaseOrderItem item = itemList.get(i);
			if (form.getGoodsApproval().get(i)) {
				if (form.getGoodsItemStatus().get(i) != PurchaseOrderItemReceiptStatus.COMPLETE) {
					errors.reject(MESSAGE_CODE_APPROVAL_STATUS_MISMATCH, new Object[]{new Integer(item.getItemno())}, MESSAGE_APPROVAL_STATUS_MISMATCH);
				}
				if (form.getGoodsCancelled().get(i)) {
					errors.reject(MESSAGE_CODE_APPROVAL_AND_CANCELLED, new Object[]{new Integer(item.getItemno())}, MESSAGE_APPROVAL_AND_CANCELLED);
				}
			}
			if (form.getGoodsCancelled().get(i) &&
				(form.getGoodsItemStatus().get(i) != PurchaseOrderItemReceiptStatus.NOT_RECEIVED)) {
				errors.reject(MESSAGE_CODE_CANCELLED_STATUS_MISMATCH, new Object[]{new Integer(item.getItemno())}, MESSAGE_CANCELLED_STATUS_MISMATCH);
			}
		}
	}


}
