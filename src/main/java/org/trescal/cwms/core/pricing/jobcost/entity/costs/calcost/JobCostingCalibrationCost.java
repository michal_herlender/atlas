/**
 * 
 */
package org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.pricing.entity.costs.base.WithCostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingItem;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedCalCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

@Entity
@DiscriminatorValue("jobcosting")
public class JobCostingCalibrationCost extends TPSupportedCalCost implements WithJobCostingItem, WithCostSource<JobCostingLinkedCalibrationCost>
{
	private JobCostingItem jobCostingItem;

	private JobCostingLinkedCalibrationCost linkedCost;

	@OneToOne(mappedBy = "calibrationCost")
	public JobCostingItem getJobCostingItem()
	{
		return this.jobCostingItem;
	}

	@OneToOne(mappedBy = "calibrationCost", cascade = CascadeType.ALL)
	public JobCostingLinkedCalibrationCost getLinkedCost()
	{
		return this.linkedCost;
	}

	public void setJobCostingItem(JobCostingItem jobCostingItem)
	{
		this.jobCostingItem = jobCostingItem;
	}

	public void setLinkedCost(JobCostingLinkedCalibrationCost linkedCost)
	{
		this.linkedCost = linkedCost;
	}
}