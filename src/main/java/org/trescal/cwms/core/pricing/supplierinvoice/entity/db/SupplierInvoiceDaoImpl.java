package org.trescal.cwms.core.pricing.supplierinvoice.entity.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;

@Repository
public class SupplierInvoiceDaoImpl extends
		AllocatedToCompanyDaoImpl<SupplierInvoice, Integer> implements SupplierInvoiceDao {

	@Override
	protected Class<SupplierInvoice> getEntity() {
		return SupplierInvoice.class;
	}

}
