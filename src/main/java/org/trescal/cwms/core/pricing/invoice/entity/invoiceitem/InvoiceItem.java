package org.trescal.cwms.core.pricing.invoice.entity.invoiceitem;

import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.audit.Auditable_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostOrderComparator;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.TaxablePricingItem;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.TaxablePricingItem_;
import org.trescal.cwms.core.pricing.invoice.entity.costs.adjcost.InvoiceAdjustmentCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.calcost.InvoiceCalibrationCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.purchasecost.InvoicePurchaseCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.repcost.InvoiceRepairCost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.servicecost.InvoiceServiceCost;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoicePOItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Entity
@Table(name = "invoiceitem", indexes = { @Index(name = "IDX_invoiceitem_invoice", columnList = "invoiceid"),
		@Index(name = "IDX_invoiceitem_jobitem", columnList = "jobitemid"),
		@Index(name = "IDX_invoiceitem_expenseitem", columnList = "expenseitem")})
@AssociationOverrides({
		@AssociationOverride(name = Auditable_.LAST_MODIFIED_BY, joinColumns = @JoinColumn(name = "lastModifiedBy"), foreignKey = @ForeignKey(name = "FK_invoiceitem_lastmodifiedby")),
		@AssociationOverride(name = TaxablePricingItem_.SHIP_TO_ADDRESS, joinColumns = @JoinColumn(name = "shiptoaddressid"), foreignKey = @ForeignKey(name = "FK_invoiceitem_shiptoaddress")) })
@Setter
public class InvoiceItem extends TaxablePricingItem {

	private Integer id;
	@Deprecated
	private InvoiceAdjustmentCost adjustmentCost;
	/**
	 * If true then costs will be displayed separately, otherwise editinvoiceitem
	 * form will display an editable totalcost field.
	 *
	 * @deprecated Should be always false!
	 */
	private boolean breakUpCosts = false;
	@Deprecated
	private InvoiceCalibrationCost calibrationCost;
	private String description;
	private Set<InvoiceItemPO> invItemPOs;


	private Set<InvoicePOItem> invPOsItem;
	private Invoice invoice;
	private JobItem jobItem;
	private JobExpenseItem expenseItem;
	private NominalCode nominal;
	@Deprecated
	private InvoicePurchaseCost purchaseCost;
	@Deprecated
	private InvoiceRepairCost repairCost;
	@Deprecated
	private InvoiceServiceCost serviceCost;
	private String serialno;
	private Subdiv businessSubdiv;

	@Override
	@Id
	@GeneratedValue(generator = "invoiceitem_id_sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "invoiceitem_id_sequence", sequenceName = "invoiceitem_id_sequence")
	public Integer getId() {
		return id;
	}

	@Override
	@Transient
	public SortedSet<Cost> getCosts() {
		SortedSet<Cost> costs = new TreeSet<>(new CostOrderComparator());
		if (adjustmentCost != null)
			costs.add(adjustmentCost);
		if (calibrationCost != null)
			costs.add(calibrationCost);
		if (purchaseCost != null)
			costs.add(purchaseCost);
		if (repairCost != null)
			costs.add(repairCost);
		if (serviceCost != null)
			costs.add(serviceCost);
		return costs;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "adjustmentcost_id")
	public InvoiceAdjustmentCost getAdjustmentCost() {
		return this.adjustmentCost;
	}

	@Override
	@Transient
	public PricingItem getBaseUnit() {
		return null;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "calcost_id")
	public InvoiceCalibrationCost getCalibrationCost() {
		return this.calibrationCost;
	}

	@Length(max = 500)
	@Column(name = "description", length = 500)
	public String getDescription() {
		return this.description;
	}

	@OneToMany(mappedBy = "invItem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<InvoiceItemPO> getInvItemPOs() {
		return this.invItemPOs;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoiceid", nullable = false, foreignKey = @ForeignKey(name = "FK_invoiceitem_invoice"))
	public Invoice getInvoice() {
		return this.invoice;
	}

	@Transient
	public Job getJob() {
		if (jobItem != null)
			return jobItem.getJob();
		else if (expenseItem != null)
			return expenseItem.getJob();
		else
			return null;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", foreignKey = @ForeignKey(name = "FK_invoiceitem_jobitem"))
	public JobItem getJobItem() {
		return this.jobItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "expenseitem", foreignKey = @ForeignKey(name = "FK_invoiceitem_expenseitem"))
	public JobExpenseItem getExpenseItem() {
		return expenseItem;
	}

	@Override
	@Transient
	public Set<? extends PricingItem> getModules() {
		return null;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nominalid", foreignKey = @ForeignKey(name = "FK_invoiceitem_nominalcode"))
	public NominalCode getNominal() {
		return this.nominal;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "purchasecost_id")
	public InvoicePurchaseCost getPurchaseCost() {
		return this.purchaseCost;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "repaircost_id")
	public InvoiceRepairCost getRepairCost() {
		return this.repairCost;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "servicecost", foreignKey = @ForeignKey(name = "FK_invoiceitem_servicecost"))
	public InvoiceServiceCost getServiceCost() {
		return serviceCost;
	}

	@Length(max = 30)
	@Column(name = "serialno", length = 30)
	public String getSerialno() {
		return this.serialno;
	}

	@Column(name = "breakupcosts", nullable = false, columnDefinition = "tinyint")
	public boolean isBreakUpCosts() {
		return this.breakUpCosts;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orgid", foreignKey = @ForeignKey(name = "FK_invoiceitem_organisation"))
	public Subdiv getBusinessSubdiv() {
		return businessSubdiv;
	}

	@OneToMany(mappedBy = "invItem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<InvoicePOItem> getInvPOsItem() {
		return invPOsItem;
	}
}