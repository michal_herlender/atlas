package org.trescal.cwms.core.pricing.invoice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.Converter;
import com.fasterxml.jackson.databind.util.StdConverter;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

import lombok.Data;

@Data
@JsonSerialize(converter = PInvoiceExpenseItem.PInvoiceExpenseItemDtoConverter.class)
public class PInvoiceExpenseItem {

	private JobExpenseItem expenseItem;
	private JobCosting jobCosting;
	private CostSource source;
	private PInvoice pinvoice;
	private PInvoiceItemCost serviceCost;
	private int serviceCostId;
	private boolean alreadyInvoiced; // Whether to flag warning that job item has already been invoiced
	private boolean costed;
	private boolean include; // Whether invoice item should be included on this invoice
	private PNotInvoiced notInvoiced; // Transfer object for not-invoiced information

	public PInvoiceExpenseItem() {
	}

	/*
	 * Application calls use this constructor
	 */
	public PInvoiceExpenseItem(JobExpenseItem expenseItem) {
		this.expenseItem = expenseItem;
		this.setNotInvoiced(new PNotInvoiced(expenseItem.getNotInvoiced()));
		this.setAlreadyInvoiced(false);
		// Exclude item from invoicing if it has already been invoiced on any
		// non-proforma invoices
		// (other than Internal Invoice Type)
		this.setInclude(true);
		for (InvoiceItem invoiceItem : expenseItem.getInvoiceItems()) {
			InvoiceType type = invoiceItem.getInvoice().getType();
			if (type.isAddToAccount() && !type.getName().equals("Internal")) {
				this.setInclude(false);
				this.setAlreadyInvoiced(true);
			}
		}
		// Note, include status may be updated later, if no costs attached.
	}

	public PInvoiceExpenseItem(JobExpenseItem expenseItem, PInvoice pinvoice) {
		this(expenseItem);
		this.pinvoice = pinvoice;
	}

	static class PInvoiceExpenseItemDtoConverter extends StdConverter<PInvoiceExpenseItem, PInvoiceExpenseItemDto> {

		@Override
		public PInvoiceExpenseItemDto convert(PInvoiceExpenseItem value) {
			return PInvoiceExpenseItemDto.fromExpenseItem(value);
		}
	}
}