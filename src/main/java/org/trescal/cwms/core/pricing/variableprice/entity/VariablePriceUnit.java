package org.trescal.cwms.core.pricing.variableprice.entity;

public enum VariablePriceUnit {
	DATA_POINT("Data Point"), HOUR("Hour");
	private String name;
	
	private VariablePriceUnit(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
