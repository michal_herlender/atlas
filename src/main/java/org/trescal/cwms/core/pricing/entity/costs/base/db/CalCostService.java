package org.trescal.cwms.core.pricing.entity.costs.base.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostLookUp;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.CalCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

/**
 * Provides a set of functions for accessing children of {@link CalCost}.
 * 
 * @author Richard
 */
public interface CalCostService
{
	void deleteCalCost(CalCost calcost);

	CalCost findCalCost(int id);

	List<CalCost> getAllCalCosts();

	void insertCalCost(CalCost calcost);

	/**
	 * Populates a ({@link CostLookUp} with sets of alternative costs, it is up
	 * to the callee to create the {@link CostLookUp} object. This is intended
	 * for use on {@link Quotationitem}, {@link JobCostingItem} and
	 * {@link JobItem} (contract review) pages to show past costs for a model
	 * type for a particular company. The list of {@link CostSource} allows us
	 * to inject a different set of alternative costs for each costing area from
	 * the project properties.
	 * 
	 * @param lookUp a {@link CostLookUp} object to hold all of the different
	 *        costs.
	 * @param plantid optional plantid to look up explicit {@link Instrument}
	 *        costs.
	 * @param modelid the id of the {@link InstrumentModel}, not null.
	 * @param calTypeId the {@link CalibrationType} of the costs.
	 * @param coid the coid of the {@link Company} to lookup costs for, not
	 *        null.
	 * @param jobid the id of the current job, nullable.
	 * @param costSources list of {@link CostSource} that should be looked up.
	 * @return a populated {@link CostLookUp}.
	 */
	CostLookUp lookupAlternativeCosts(CostLookUp lookUp, Integer plantid, int modelid, int calTypeId, int coid, Integer jobid, List<CostSource> costSources, int businessCoId);

	void saveOrUpdateCalCost(CalCost calcost);

	void updateCalCost(CalCost calcost);
}