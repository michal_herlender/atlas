package org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.db;

import java.util.List;

import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;

public interface AdditionalCostContactService
{
	AdditionalCostContact findAdditionalCostContact(int id);
	void insertAdditionalCostContact(AdditionalCostContact additionalcostcontact);
	void updateAdditionalCostContact(AdditionalCostContact additionalcostcontact);
	List<AdditionalCostContact> getAllAdditionalCostContacts();
	void deleteAdditionalCostContact(AdditionalCostContact additionalcostcontact);
	void saveOrUpdateAdditionalCostContact(AdditionalCostContact additionalcostcontact);
}