package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.pricing.jobcost.dto.JobCostingExpenseItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.form.JobCostingExpenseItemForm;

public interface JobCostingExpenseItemService extends BaseService<JobCostingExpenseItem, Integer> {

	JobCostingExpenseItemForm getFormObject(Integer id);

	List<JobCostingExpenseItemDto> getJobCostingExpenseItemsDtoByJobCostingId(Integer jobCostingId, Locale locale);
}