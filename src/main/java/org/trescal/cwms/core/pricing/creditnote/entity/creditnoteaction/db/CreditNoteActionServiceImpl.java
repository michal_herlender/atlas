package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.db;

import java.util.List;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.CreditNoteAction;

public class CreditNoteActionServiceImpl implements CreditNoteActionService
{
	private CreditNoteActionDao creditNoteActionDao;

	public CreditNoteAction findCreditNoteAction(int id)
	{
		return creditNoteActionDao.find(id);
	}

	public void insertCreditNoteAction(CreditNoteAction CreditNoteAction)
	{
		creditNoteActionDao.persist(CreditNoteAction);
	}

	public void updateCreditNoteAction(CreditNoteAction CreditNoteAction)
	{
		creditNoteActionDao.update(CreditNoteAction);
	}

	public List<CreditNoteAction> getAllCreditNoteActions()
	{
		return creditNoteActionDao.findAll();
	}

	public void setCreditNoteActionDao(CreditNoteActionDao creditNoteActionDao)
	{
		this.creditNoteActionDao = creditNoteActionDao;
	}

	public void deleteCreditNoteAction(CreditNoteAction creditnoteaction)
	{
		this.creditNoteActionDao.remove(creditnoteaction);
	}
}