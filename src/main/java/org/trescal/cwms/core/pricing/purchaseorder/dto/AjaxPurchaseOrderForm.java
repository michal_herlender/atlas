package org.trescal.cwms.core.pricing.purchaseorder.dto;

import java.util.List;
import java.util.Map;

import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AjaxPurchaseOrderForm
{
	private Integer addressid;
	private List<KeyValue<Integer,String>> contractReviewCosts;
	private Map<AjaxPurchaseOrderItemDTO, List<KeyValue<Integer,String>>> jiContractReviewCosts;
	private List<Integer> costtypeids;
	private List<KeyValue<Integer, String>> costtypes;
	private List<SupportedCurrency> currencies;
	private String currencyCode;

	private SupportedCurrency defaultCurrency;
	private String deliverydate;
	private String supplierref;
	private List<String> descs;
	private List<String> discounts;
	// ref data
	private JobItem fromJobItem;
	private List<AjaxPurchaseOrderItemDTO> fromJobItems;
	private Integer jobid;
	private List<Integer> jobitemids;
	private List<JobItem> jobitems;

	private List<NominalCode> nominalcodes;
	private List<Integer> nominalids;
	private Integer personid;
	private List<String> prices;
	private List<String> qtys;
	private List<Address> returnAddress;
	private Integer returnToAddressid;
	
}
