/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.costs.thirdparty;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.pricing.entity.costs.base.CalCost;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;

/**
 * Abstract class that all Calibration {@link Cost}s should extend that require
 * third party cost support for Calibration costs.
 * 
 * @author Richard
 */
@MappedSuperclass
public abstract class TPSupportedCalCost extends CalCost implements ThirdPartyCostSupport
{
	/**
	 * The in-house charges.
	 */
	private BigDecimal houseCost;

	// third party
	private TPQuotationCalibrationCost linkedCostSrc;
	private ThirdCostSource thirdCostSrc;
	private BigDecimal thirdCostTotal;
	private BigDecimal thirdManualPrice;
	private BigDecimal thirdMarkupRate;
	private ThirdCostMarkupSource thirdMarkupSrc;
	private BigDecimal thirdMarkupValue;

	// carriage (pushed down from thirdpartypricingitem)
	private BigDecimal tpCarriageIn;
	private BigDecimal tpCarriageInMarkupValue;
	private BigDecimal tpCarriageMarkupRate;
	private ThirdCostMarkupSource tpCarriageMarkupSrc;
	private BigDecimal tpCarriageOut;
	private BigDecimal tpCarriageOutMarkupValue;
	private BigDecimal tpCarriageTotal;

	public TPSupportedCalCost() {
		// set some sensible defaults here
		this.tpCarriageIn = new BigDecimal("0.00");
		this.tpCarriageInMarkupValue = new BigDecimal("0.00");
		this.tpCarriageMarkupRate = new BigDecimal("0.00");
		this.tpCarriageMarkupSrc = ThirdCostMarkupSource.SYSTEM_DEFAULT;
		this.tpCarriageOut = new BigDecimal("0.00");
		this.tpCarriageOutMarkupValue = new BigDecimal("0.00");
		this.tpCarriageTotal = new BigDecimal("0.00");
	}
	
	public TPSupportedCalCost(TPSupportedCalCost otherTPCalCost) {
		this.houseCost = otherTPCalCost.getHouseCost();
		this.discountRate = otherTPCalCost.getDiscountRate();
		this.discountValue = otherTPCalCost.getDiscountValue();
		this.finalCost = otherTPCalCost.getFinalCost();
		this.totalCost = otherTPCalCost.getTotalCost();
		this.linkedCostSrc = otherTPCalCost.getLinkedCostSrc();
		this.costType = otherTPCalCost.getCostType();
		this.thirdCostSrc = otherTPCalCost.getThirdCostSrc();
		this.thirdCostTotal = otherTPCalCost.getThirdCostTotal();
		this.thirdManualPrice = otherTPCalCost.getThirdManualPrice();
		this.thirdMarkupRate = otherTPCalCost.getThirdMarkupRate();
		this.thirdMarkupSrc = otherTPCalCost.getThirdMarkupSrc();
		this.thirdMarkupValue = otherTPCalCost.getThirdMarkupValue();
		this.tpCarriageIn = otherTPCalCost.getTpCarriageIn();
		this.tpCarriageInMarkupValue = otherTPCalCost.getTpCarriageInMarkupValue();
		this.tpCarriageMarkupRate = otherTPCalCost.getTpCarriageMarkupRate();
		this.tpCarriageMarkupSrc = otherTPCalCost.getTpCarriageMarkupSrc();
		this.tpCarriageOut = otherTPCalCost.getTpCarriageOut();
		this.tpCarriageOutMarkupValue = otherTPCalCost.getTpCarriageOutMarkupValue();
		this.tpCarriageTotal = otherTPCalCost.getTpCarriageTotal();
	}
	
	@Override
	@Column(name = "housecost", precision = 10, scale = 2, nullable = true)
	public BigDecimal getHouseCost()
	{
		return this.houseCost;
	}

	@Override
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "linkedcalcostid")
	public TPQuotationCalibrationCost getLinkedCostSrc()
	{
		return this.linkedCostSrc;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "tpcostsrc")
	public ThirdCostSource getThirdCostSrc()
	{
		return this.thirdCostSrc;
	}

	@Column(name = "thirdcosttotal", precision = 10, scale = 2, nullable = true)
	public BigDecimal getThirdCostTotal()
	{
		return this.thirdCostTotal;
	}

	@Column(name = "tpmanualprice", precision = 10, scale = 2, nullable = true)
	public BigDecimal getThirdManualPrice()
	{
		return this.thirdManualPrice;
	}

	@Column(name = "tpmarkuprate", precision = 10, scale = 2, nullable = true)
	public BigDecimal getThirdMarkupRate()
	{
		return this.thirdMarkupRate;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "tpmarkupsource")
	public ThirdCostMarkupSource getThirdMarkupSrc()
	{
		return this.thirdMarkupSrc;
	}

	@Column(name = "tpmarkupvalue", precision = 10, scale = 2, nullable = true)
	public BigDecimal getThirdMarkupValue()
	{
		return this.thirdMarkupValue;
	}

	@NotNull
	@Column(name = "tpcarin", precision = 10, scale = 2)
	public BigDecimal getTpCarriageIn()
	{
		return this.tpCarriageIn;
	}

	@NotNull
	@Column(name = "tpcarinmarkupvalue", precision = 10, scale = 2)
	public BigDecimal getTpCarriageInMarkupValue()
	{
		return this.tpCarriageInMarkupValue;
	}

	@NotNull
	@Column(name = "tpcarmarkuprate", precision = 10, scale = 2)
	public BigDecimal getTpCarriageMarkupRate()
	{
		return this.tpCarriageMarkupRate;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tpcarmarkupsrc")
	public ThirdCostMarkupSource getTpCarriageMarkupSrc()
	{
		return this.tpCarriageMarkupSrc;
	}

	@NotNull
	@Column(name = "tpcarout", precision = 10, scale = 2)
	public BigDecimal getTpCarriageOut()
	{
		return this.tpCarriageOut;
	}

	@NotNull
	@Column(name = "tpcaroutmarkupvalue", precision = 10, scale = 2)
	public BigDecimal getTpCarriageOutMarkupValue()
	{
		return this.tpCarriageOutMarkupValue;
	}

	@NotNull
	@Column(name = "tpcartotal", precision = 10, scale = 2)
	public BigDecimal getTpCarriageTotal()
	{
		return this.tpCarriageTotal;
	}

	@Override
	public void setHouseCost(BigDecimal houseCost)
	{
		this.houseCost = houseCost;
	}

	public void setLinkedCostSrc(TPQuotationCalibrationCost cost)
	{
		this.linkedCostSrc = cost;
	}

	public void setThirdCostSrc(ThirdCostSource thirdCalPriceSrc)
	{
		this.thirdCostSrc = thirdCalPriceSrc;
	}

	public void setThirdCostTotal(BigDecimal thirdCostTotal)
	{
		this.thirdCostTotal = thirdCostTotal;
	}

	public void setThirdManualPrice(BigDecimal thirdManualPrice)
	{
		this.thirdManualPrice = thirdManualPrice;
	}

	public void setThirdMarkupRate(BigDecimal thirdCalPriceMarkupRate)
	{
		this.thirdMarkupRate = thirdCalPriceMarkupRate;
	}

	public void setThirdMarkupSrc(ThirdCostMarkupSource thirdCalPriceMarkupSrc)
	{
		this.thirdMarkupSrc = thirdCalPriceMarkupSrc;
	}

	public void setThirdMarkupValue(BigDecimal thirdPriceMarkupValue)
	{
		this.thirdMarkupValue = thirdPriceMarkupValue;
	}

	public void setTpCarriageIn(BigDecimal tpCarriageIn)
	{
		this.tpCarriageIn = tpCarriageIn;
	}

	public void setTpCarriageInMarkupValue(BigDecimal tpCarriageInMarkupValue)
	{
		this.tpCarriageInMarkupValue = tpCarriageInMarkupValue;
	}

	public void setTpCarriageMarkupRate(BigDecimal tpCarriageMarkupRate)
	{
		this.tpCarriageMarkupRate = tpCarriageMarkupRate;
	}

	public void setTpCarriageMarkupSrc(ThirdCostMarkupSource tpCarriageMarkupSrc)
	{
		this.tpCarriageMarkupSrc = tpCarriageMarkupSrc;
	}

	public void setTpCarriageOut(BigDecimal tpCarriageOut)
	{
		this.tpCarriageOut = tpCarriageOut;
	}

	public void setTpCarriageOutMarkupValue(BigDecimal tpCarriageOutMarkupValue)
	{
		this.tpCarriageOutMarkupValue = tpCarriageOutMarkupValue;
	}

	public void setTpCarriageTotal(BigDecimal tpCarriageTotal)
	{
		this.tpCarriageTotal = tpCarriageTotal;
	}
}
