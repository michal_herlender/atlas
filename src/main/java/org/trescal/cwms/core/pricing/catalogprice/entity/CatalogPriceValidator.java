package org.trescal.cwms.core.pricing.catalogprice.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * Validator for CatalogPrice, currently just checks CatalogPrice 
 * for same model, caltype and company isn't a duplicate.
 * SC 23/11/2015.
 * GB 2016-10-10 Connected to AbstractBeanValidator to do 
 * JPA/Hibernate validation on entity
 */
@Component
public class CatalogPriceValidator extends AbstractBeanValidator {

    @Autowired
    public CatalogPriceService catalogPriceService;
    
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(CatalogPrice.class);
    }
    
    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);
        CatalogPrice catalogPrice = (CatalogPrice)target;
        if(catalogPrice.getId() == 0) {
			if(catalogPriceService.exists(catalogPrice.getInstrumentModel(), catalogPrice.getCostType(),
					catalogPrice.getServiceType(),catalogPrice.getOrganisation())) {
				errors.reject("duplicate.catalogPrice","Price already exists");
			}
		}
    }
}