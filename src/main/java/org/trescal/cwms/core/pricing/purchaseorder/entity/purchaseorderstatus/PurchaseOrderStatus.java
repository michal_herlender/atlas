package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "purchaseorderstatus")
public class PurchaseOrderStatus extends BaseStatus
{
	private ActionType type;
	private List<PurchaseOrder> orders;
	private PurchaseOrderStatus next;
	private boolean requiresAttention;

	/**
	 * Flags that this status/activity is associated with the insert of a
	 * {@link PurchaseOrder}.
	 */
	private boolean onInsert;

	/**
	 * Flags that this status/activity is associated with the issure of a
	 * {@link PurchaseOrder}.
	 */
	private boolean onIssue;

	/**
	 * Flags that if this is an action this action is the 'completed' action, or
	 * if a status this is the 'complete' status.
	 */
	private boolean onComplete;

	/**
	 * Flags that if this is an action this action is the 'cancelled' action, or
	 * if a status this is the 'cancelled' status.
	 */
	private boolean onCancel;

	/**
	 * Optional field that specifies the {@link Department} a {@link Contact}
	 * must belong to if they are to be allowed to peform this activity.
	 */
	private DepartmentType deptMembershipRequired;

	/**
	 * Indicates whether the order has been issued -
	 * can apply to multiple statuses
	 */
	private boolean issued;

	/**
	 * Indicates whether the order has been approved -
	 * can apply to multiple statuses
	 */
	private boolean approved;
	
	/**
	 * Translation for name 
	 */
	private Set<Translation> nametranslations;

	/**
	 * Translation for description 
	 */
	private Set<Translation> descriptiontranslations;
	
	private int statusorder;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "depttype", nullable = true)
	public DepartmentType getDeptMembershipRequired()
	{
		return this.deptMembershipRequired;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "nextid", nullable = true)
	public PurchaseOrderStatus getNext()
	{
		return this.next;
	}

	@OneToMany(mappedBy = "status", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<PurchaseOrder> getOrders()
	{
		return this.orders;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "potype", nullable = false)
	public ActionType getType()
	{
		return this.type;
	}

	@NotNull
	@Column(name = "oncancel", nullable=false, columnDefinition="bit")
	public boolean isOnCancel() {
		return onCancel;
	}

	@NotNull
	@Column(name = "approved", nullable=false, columnDefinition="bit")
	public boolean isApproved() {
		return this.approved;
	}

	@NotNull
	@Column(name = "issued", nullable=false, columnDefinition="bit")
	public boolean isIssued()
	{
		return this.issued;
	}

	@NotNull
	@Column(name = "oncomplete", nullable = false, columnDefinition="bit")
	public boolean isOnComplete()
	{
		return this.onComplete;
	}

	@NotNull
	@Column(name = "oninsert", nullable = false, columnDefinition="bit")
	public boolean isOnInsert()
	{
		return this.onInsert;
	}

	@NotNull
	@Column(name = "onissue", nullable = false, columnDefinition="bit")
	public boolean isOnIssue()
	{
		return this.onIssue;
	}

	@NotNull
	@Column(name = "requiresattention", nullable = false, columnDefinition="bit")
	public boolean isRequiresAttention()
	{
		return this.requiresAttention;
	}

	public void setDeptMembershipRequired(DepartmentType deptMembershipRequired)
	{
		this.deptMembershipRequired = deptMembershipRequired;
	}

	public void setIssued(boolean issued)
	{
		this.issued = issued;
	}

	public void setNext(PurchaseOrderStatus next)
	{
		this.next = next;
	}

	public void setOnCancel(boolean onCancel) {
		this.onCancel = onCancel;
	}

	public void setOnComplete(boolean onComplete)
	{
		this.onComplete = onComplete;
	}

	public void setOnInsert(boolean onInsert)
	{
		this.onInsert = onInsert;
	}

	public void setOnIssue(boolean onIssue)
	{
		this.onIssue = onIssue;
	}

	public void setOrders(List<PurchaseOrder> orders)
	{
		this.orders = orders;
	}

	public void setRequiresAttention(boolean requiresAttention)
	{
		this.requiresAttention = requiresAttention;
	}

	public void setType(ActionType type)
	{
		this.type = type;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="purchaseorderstatusnametranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getNametranslations() {
		return nametranslations;
	}

	public void setNametranslations(Set<Translation> nametranslations) {
		this.nametranslations = nametranslations;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="purchaseorderstatusdescriptiontranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getDescriptiontranslations() {
		return descriptiontranslations;
	}

	public void setDescriptiontranslations(Set<Translation> descriptiontranslations) {
		this.descriptiontranslations = descriptiontranslations;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	
	@NotNull
	@Column(name = "statusorder", nullable=false)
	public int getStatusorder() {
		return statusorder;
	}

	public void setStatusorder(int statusorder) {
		this.statusorder = statusorder;
	}
	
	
}
