package org.trescal.cwms.core.pricing.creditnote.form;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CreditNoteItemForm {

	private Integer creditNoteId;
	private Integer id;
	private String description;
	private Integer nominalId;
	private Integer subdivId;
	private BigDecimal amount;
}