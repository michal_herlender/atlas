package org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;

@Service("CreditNoteStatusService")
public class CreditNoteStatusServiceImpl implements CreditNoteStatusService {

	@Autowired
	private CreditNoteStatusDao creditNoteStatusDao;

	public void deleteCreditNoteStatus(CreditNoteStatus creditnotestatus) {
		this.creditNoteStatusDao.remove(creditnotestatus);
	}

	public List<CreditNoteStatus> findAllCreditNoteStatusRequiringAttention() {
		return this.creditNoteStatusDao.findAllCreditNoteStatusRequiringAttention();
	}

	public CreditNoteStatus findCreditNoteStatus(int id) {
		return this.creditNoteStatusDao.find(id);
	}

	public CreditNoteStatus findOnInsertActivity() {
		return this.creditNoteStatusDao.findOnInsertActivity();
	}

	public List<CreditNoteStatus> getAllCreditNoteStatuss() {
		return this.creditNoteStatusDao.findAll();
	}

	public void insertCreditNoteStatus(CreditNoteStatus CreditNoteStatus) {
		this.creditNoteStatusDao.persist(CreditNoteStatus);
	}
}