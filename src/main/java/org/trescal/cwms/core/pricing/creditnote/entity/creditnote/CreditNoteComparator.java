package org.trescal.cwms.core.pricing.creditnote.entity.creditnote;

import java.util.Comparator;

public class CreditNoteComparator implements Comparator<CreditNote>
{
	@Override
	public int compare(CreditNote note1, CreditNote note2)
	{
		if ((note1.getId() == 0) && (note2.getId() == 0))
		{
			return 1;
		}
		else
		{
			return (note1.getCreditNoteNo().compareTo(note2.getCreditNoteNo()));
		}
	}
}
