package org.trescal.cwms.core.pricing.invoice.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.InvoiceSource;

@Component
public class PrepareMonthlyInvoiceValidator implements Validator
{
	@Autowired
	private SubdivService subdivService;
	
	public static String MESSAGE_CODE_DEFAULT_CONTACT_NULL = "error.rest.subdiv.nodefaultcontact";
	public static String DEFAULT_MESSAGE_DEFAULT_CONTACT_NULL = "The subdivision does not have a default contact";
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(PrepareMonthlyInvoiceForm.class);
	}

	@Override
	public void validate(Object command, Errors errors)
	{
		PrepareMonthlyInvoiceForm form = (PrepareMonthlyInvoiceForm) command;

		//Prevent invoice creation by business subdiv with no default contact
		if(subdivService.get(form.getAllocatedSubdivId()).getDefaultContact() == null){
			errors.reject(MESSAGE_CODE_DEFAULT_CONTACT_NULL, DEFAULT_MESSAGE_DEFAULT_CONTACT_NULL);
		}
			
		if (form.getSource() == InvoiceSource.SYSTEM)
		{
			if ((form.getJobids() == null) || (form.getJobids().size() < 1))
			{
				errors.rejectValue("jobids", "error.invoice.monthly.create.nojobs", null, "You must specify at least one job to invoice");
			}
		}
		else
		{
			// make sure company not null
			if (form.getCompany() == null)
			{
				errors.rejectValue("company", "error.invoice.monthly.create.nocompany", null, "You must specify a company to invoice");
			}

			// make sure address not null
			if (form.getAddressid() == null)
			{
				errors.rejectValue("addressid", "error.invoice.monthly.create.noaddress", null, "You must specify an address for your invoice");
			}

			// make sure invoicetype not null
			if (form.getTypeid() == null)
			{
				errors.rejectValue("typeid", "error.invoice.monthly.create.notype", null, "You must specify the type of invoice you require");
			}
		}
	}
}