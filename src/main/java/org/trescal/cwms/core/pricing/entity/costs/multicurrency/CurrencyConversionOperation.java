package org.trescal.cwms.core.pricing.entity.costs.multicurrency;

import java.math.BigDecimal;

import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

/**
 * Wrapper object used to store the results of a currency conversion operation.
 * 
 * @author richard
 */
public class CurrencyConversionOperation
{
	/**
	 * The base currency of this operation i.e. the currency being converted to.
	 */
	private SupportedCurrency baseCurrency;
	/**
	 * The target currency i.e. the currency being converted from.
	 */
	private SupportedCurrency targetCurrency;
	/**
	 * The rate being used to make the conversion.
	 */
	private BigDecimal rate;
	/**
	 * The value of the targetValue after conversion to the base currency.
	 */
	private BigDecimal baseValue;
	/**
	 * The currency value prior to conversion to the base currency.
	 */
	private BigDecimal targetValue;

	/**
	 * Indicates if this conversion was a success.
	 */
	private boolean success;

	/**
	 * Holds any messages relating to this operation - including failure
	 * reasons.
	 */
	private String message;

	/**
	 * Holds a string value of the conversion value, including the currency
	 * symbol and correct decimal formatting.
	 */
	private String stringValue;

	/**
	 * Set's the status of this {@link CurrencyConversionOperation} to having
	 * failed and also sets the reason for the failure.
	 * 
	 * @param message the message outlining the reason for the failure.
	 */
	public void fail(String message)
	{
		this.success = false;
		this.message = message;
	}

	public SupportedCurrency getBaseCurrency()
	{
		return this.baseCurrency;
	}

	public BigDecimal getBaseValue()
	{
		return this.baseValue;
	}

	public String getMessage()
	{
		return this.message;
	}

	public BigDecimal getRate()
	{
		return this.rate;
	}

	public String getStringValue()
	{
		return this.stringValue;
	}

	public SupportedCurrency getTargetCurrency()
	{
		return this.targetCurrency;
	}

	public BigDecimal getTargetValue()
	{
		return this.targetValue;
	}

	public boolean isSuccess()
	{
		return this.success;
	}

	public void setBaseCurrency(SupportedCurrency baseCurrency)
	{
		this.baseCurrency = baseCurrency;
	}

	public void setBaseValue(BigDecimal baseValue)
	{
		this.baseValue = baseValue;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setRate(BigDecimal rate)
	{
		this.rate = rate;
	}

	public void setStringValue(String stringValue)
	{
		this.stringValue = stringValue;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public void setTargetCurrency(SupportedCurrency targetCurrency)
	{
		this.targetCurrency = targetCurrency;
	}

	public void setTargetValue(BigDecimal targetValue)
	{
		this.targetValue = targetValue;
	}

}
