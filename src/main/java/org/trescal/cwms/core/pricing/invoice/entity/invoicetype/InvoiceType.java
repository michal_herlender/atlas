package org.trescal.cwms.core.pricing.invoice.entity.invoicetype;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "invoicetype")
public class InvoiceType {

	private int id;
	private boolean addToAccount;
	private boolean defToSingleCost;
	private boolean includeOnJobSheet;
//	private boolean proForma;
	private Set<Invoice> invoices;
	private String name;
	private Set<Translation> nametranslation;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@OneToMany(mappedBy = "type")
	public Set<Invoice> getInvoices() {
		return this.invoices;
	}

	@Length(max = 200)
	@NotNull
	@Column(name = "name", length = 200, nullable = false)
	public String getName() {
		return this.name;
	}

	@NotNull
	@Column(name = "addtoaccount", nullable = false, columnDefinition = "bit")
	public boolean isAddToAccount() {
		return this.addToAccount;
	}

	@NotNull
	@Column(name = "deftosinglecost", nullable = false, columnDefinition = "bit")
	public boolean isDefToSingleCost() {
		return this.defToSingleCost;
	}

	@NotNull
	@Deprecated
	@Column(name = "includeonjobsheet", nullable = false, columnDefinition = "bit")
	public boolean isIncludeOnJobSheet() {
		return this.includeOnJobSheet;
	}

//	@NotNull
//	@Column(name = "proForma", nullable = false, columnDefinition = "bit")
//	public boolean isProForma() {
//		return proForma;
//	}

	public void setAddToAccount(boolean addToAccount) {
		this.addToAccount = addToAccount;
	}

	public void setDefToSingleCost(boolean defToSingleCost) {
		this.defToSingleCost = defToSingleCost;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setIncludeOnJobSheet(boolean includeOnJobSheet) {
		this.includeOnJobSheet = includeOnJobSheet;
	}

//	public void setProForma(boolean proForma) {
//		this.proForma = proForma;
//	}

	public void setInvoices(Set<Invoice> invoices) {
		this.invoices = invoices;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "invoicetypetranslation", joinColumns = @JoinColumn(name = "id"))
	public Set<Translation> getNametranslation() {
		return nametranslation;
	}

	public void setNametranslation(Set<Translation> nametranslation) {
		this.nametranslation = nametranslation;
	}
}