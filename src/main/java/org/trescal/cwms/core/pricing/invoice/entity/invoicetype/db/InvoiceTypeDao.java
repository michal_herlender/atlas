package org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceTypeKeyValue;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;

public interface InvoiceTypeDao extends BaseDao<InvoiceType, Integer> {

	List<InvoiceTypeKeyValue> getAllTranslated(Locale locale);

	InvoiceType findInvoiceTypeByName(String name);
}