package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.ParentEntity;

import javax.persistence.*;

@Entity
@Table(name = "purchaseordernote")
public class PurchaseOrderNote extends Note implements ParentEntity<PurchaseOrder> {
    private PurchaseOrder order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id", nullable = false)
    public PurchaseOrder getOrder() {
        return order;
    }

    @Override
    @Transient
    public NoteType getNoteType() {
        return NoteType.PURCHASEORDERNOTE;
    }

    public void setOrder(PurchaseOrder order) {
        this.order = order;
    }

    @Override
    public void setEntity(PurchaseOrder purchaseOrder) {
        setOrder(purchaseOrder);
    }

    @Override
    @Transient
    public PurchaseOrder getEntity() {
        return getOrder();
    }
}