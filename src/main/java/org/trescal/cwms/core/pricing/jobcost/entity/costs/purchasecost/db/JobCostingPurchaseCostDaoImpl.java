package org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostDaoSupportImpl;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;

@Repository("JobCostingPurchaseCostDao")
public class JobCostingPurchaseCostDaoImpl extends JobCostingCostDaoSupportImpl<JobCostingPurchaseCost> implements JobCostingPurchaseCostDao {
	
	@Override
	protected Class<JobCostingPurchaseCost> getEntity() {
		return JobCostingPurchaseCost.class;
	}
}