package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem;

import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemnote.PurchaseOrderItemNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressAction;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction.PurchaseOrderItemProgressActionComparator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItemComparator;
import org.trescal.cwms.core.pricing.supplierinvoice.entity.SupplierInvoice;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;

import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Entity
@Table(name = "purchaseorderitem", uniqueConstraints = { @UniqueConstraint(columnNames = { "itemno", "orderid" }) })
@Setter
public class PurchaseOrderItem extends PricingItem implements NoteAwareEntity {

	private Integer id;
	private boolean accountsApproved;
	private boolean cancelled;
	private CostType costType;
	private LocalDate deliveryDate;
	private String description;
	private boolean goodsApproved;
	private SortedSet<PurchaseOrderJobItem> linkedJobItems;
	private NominalCode nominal;
	private SortedSet<PurchaseOrderItemNote> notes;
	private PurchaseOrder order;
	private SortedSet<PurchaseOrderItemProgressAction> progressActions;
	private PurchaseOrderItemReceiptStatus receiptStatus;
	private SupplierInvoice supplierInvoice;
	private Invoice internalInvoice;
	private Subdiv businessSubdiv;

	public PurchaseOrderItem() {
		linkedJobItems = new TreeSet<>(new PurchaseOrderJobItemComparator());
		notes = new TreeSet<>(new NoteComparator());
		progressActions = new TreeSet<>(new PurchaseOrderItemProgressActionComparator());
	}

	/**
	 * Indicates if this item (good or service) has been received by the business.
	 */
	private boolean received;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public Integer getId() {
		return id;
	}

	@Override
	@Transient
	public Set<Cost> getCosts() {
		return Collections.emptySet();
	}

	@Override
	@Transient
	public PricingItem getBaseUnit() {
		return null;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "costtypeid")
	public CostType getCostType() {
		return this.costType;
	}

	@Column(name = "deliverydate", columnDefinition = "date")
	public LocalDate getDeliveryDate() {
		return this.deliveryDate;
	}

	@NotNull
	@Length(max = 1000)
	@Column(name = "description", nullable = false, length = 1000)
	public String getDescription() {
		return this.description;
	}

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "poitem")
	@SortComparator(PurchaseOrderJobItemComparator.class)
	public SortedSet<PurchaseOrderJobItem> getLinkedJobItems() {
		return this.linkedJobItems;
	}

	@Override
	@Transient
	public Set<? extends PricingItem> getModules() {
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "item")
	@SortComparator(NoteComparator.class)
	public SortedSet<PurchaseOrderItemNote> getNotes() {
		return this.notes;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderid", nullable = false)
	public PurchaseOrder getOrder() {
		return this.order;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "item")
	@SortComparator(PurchaseOrderItemProgressActionComparator.class)
	public SortedSet<PurchaseOrderItemProgressAction> getProgressActions() {
		return this.progressActions;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "receiptstatus", nullable = false)
	public PurchaseOrderItemReceiptStatus getReceiptStatus() {
		return this.receiptStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nominalid")
	public NominalCode getNominal() {
		return this.nominal;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supplierinvoiceid")
	public SupplierInvoice getSupplierInvoice() {
		return supplierInvoice;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "internalinvoiceid")
	public Invoice getInternalInvoice() {
		return internalInvoice;
	}

	@Column(name = "accountsapproved", nullable = false, columnDefinition = "tinyint")
	public boolean isAccountsApproved() {
		return this.accountsApproved;
	}

	@Column(name = "cancelled", nullable = false, columnDefinition = "tinyint")
	public boolean isCancelled() {
		return this.cancelled;
	}

	@Column(name = "goodsapproved", nullable = false, columnDefinition = "tinyint")
	public boolean isGoodsApproved() {
		return this.goodsApproved;
	}

	@Column(name = "received", nullable = false, columnDefinition = "tinyint")
	public boolean isReceived() {
		return this.received;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orgid")
	public Subdiv getBusinessSubdiv() {
		return businessSubdiv;
	}
}