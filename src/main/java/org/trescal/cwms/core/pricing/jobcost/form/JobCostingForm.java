package org.trescal.cwms.core.pricing.jobcost.form;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.JobCostingClientDecision;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class JobCostingForm
{
	private List<Integer> additionalPersonids;
	private boolean applyDiscount;
	private boolean applyDiscountToCalibration;
	private PricingType clientCosting;
	private String clientref;
	private BigDecimal discountRate;
	private Job job;
	private JobCosting jobCosting;
	private Integer jobCostingTypeId;
	private List<Integer> jobItemIds;
	private List<Integer> expenseItemIds;
	private Integer personid;
	private JobCostingClientDecision clientDecision;
	private boolean setclientdecision;
	private LocalDateTime clientApprovalOn;
	private String clientApprovalComment;
	private Integer jobCostingStatusId;

}
