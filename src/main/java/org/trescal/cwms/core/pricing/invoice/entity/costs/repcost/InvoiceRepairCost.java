package org.trescal.cwms.core.pricing.invoice.entity.costs.repcost;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.base.RepCost;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;

@Entity
@DiscriminatorValue("invoice")
public class InvoiceRepairCost extends RepCost
{
	private InvoiceItem invoiceItem;

	private NominalCode nominal;

	@OneToOne(mappedBy = "repairCost")
	public InvoiceItem getInvoiceItem()
	{
		return this.invoiceItem;
	}

	@Override
	@Transient
	public Cost getLinkedCostSrc()
	{
		return null;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nominalid", unique = false, nullable = true)
	public NominalCode getNominal()
	{
		return this.nominal;
	}

	public void setInvoiceItem(InvoiceItem invoiceItem)
	{
		this.invoiceItem = invoiceItem;
	}

	public void setNominal(NominalCode nominal)
	{
		this.nominal = nominal;
	}
}
