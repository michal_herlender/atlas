package org.trescal.cwms.core.pricing.jobcost.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember;
import org.trescal.cwms.core.company.entity.mailgroupmember.db.MailGroupMemberService;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;
import org.trescal.cwms.core.company.entity.vatrate.VatRateType;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.PricingIssueMethod;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContactEqualityComparator;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItemComparator;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingstatus.JobCostingStatus;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingtype.JobCostingType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.JobCostingViewed;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingviewed.db.JobCostingViewedService;
import org.trescal.cwms.core.pricing.jobcost.form.CreateJobCostingForm;
import org.trescal.cwms.core.pricing.jobcost.form.CreateJobCostingFormValidator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.PricingTypeSingleJobItem;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller used to create new {@link JobCosting}.
 */
@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY})
public class CreateJobCostingController {
	private static final Logger logger = LoggerFactory.getLogger(CreateJobCostingController.class);

	@Value("#{props['cwms.config.costs.invoices.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	@Autowired
	private AddressSettingsForAllocatedCompanyService addresssettingsService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService settingsService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private InstructionLinkService instructionLinkServ;
	@Autowired
	private JobCostingViewedService jcvServ;
	@Autowired
	private JobCostingItemService jobCostItemServ;
	@Autowired
	private JobCostingService jobCostServ;
	@Autowired
	private JobExpenseItemService jobExpenseItemService;
	@Autowired
	private JobService jobServ;
	@Autowired
	private MailGroupMemberService mailGroupMemberServ;
	@Autowired
	private PricingTypeSingleJobItem pricingTypeSingleJobItem;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private TaxCalculator taxCalculator;
	@Autowired
	private UserService userService;
	@Autowired
	private VatRateService vatRateService;
	@Autowired
	private CreateJobCostingFormValidator validator;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;

	public static final String FORM_NAME = "jobCostingForm";

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	public CreateJobCostingForm formBackingObject(@RequestParam(value = "jobid") Integer jobid)
		throws Exception {
		Job job = this.jobServ.get(jobid);
		if (job == null) {
			logger.error("Job with id " + jobid + " could not be found");
			throw new Exception("Could not find job");
		}
		CreateJobCostingForm form = new CreateJobCostingForm();

		form.setPersonid(job.getCon().getPersonid());
		form.setClientref(job.getClientRef());
		form.setClientCosting(getDefaultPricingType(job));
		form.setAdditionalPersonids(getAdditionalPersonIds(job));
		form.setExpenseItemIds(new ArrayList<>());
		return form;
	}

	@PreAuthorize("@hasJobAuthorityOnConnectedSubdiv.check('JOB_COSTING_CREATE')")
	@RequestMapping(value = "/createjobcosting.htm", method = RequestMethod.POST)
	public String onSubmit(@Validated @ModelAttribute(FORM_NAME) CreateJobCostingForm form, BindingResult bindingResult,
						   Model model,
						   @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
						   @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		if (bindingResult.hasErrors()) {
			return referenceData(form.getJobid(), model, allocatedCompanyDto, username);
		}
		Job job = this.jobServ.get(form.getJobid());
		Contact currentContact = this.userService.get(username).getCon();

		JobCosting jc = new JobCosting();
		jc.setContact(this.contactServ.get(form.getPersonid()));
		jc.setCostingType(JobCostingType.getJobCostingTypeById(form.getJobCostingTypeId()));
		jc.setCreatedBy(currentContact);
		jc.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		jc.setFinalCost(new BigDecimal("0.00"));
		jc.setRate(job.getRate());
		jc.setCurrency(job.getCurrency());
		jc.setIssued(false);
		jc.setJob(job);
		jc.setLastUpdateBy(currentContact);
		jc.setLastUpdateOn(new Date());
		jc.setStatus(this.statusServ.findDefaultStatus(JobCostingStatus.class));
		jc.setType(form.getClientCosting());
		jc.setVer(this.jobCostServ.findCurrentMaxVersion(form.getJobid()) + 1);
		jc.setClientref(form.getClientref());
		// set issue method
		boolean islinkToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(job.getCon().getSub().getSubdivid(),
			currentContact.getSub().getComp().getCoid(), new Date());
		if (islinkToAdveso && form.getClientCosting().equals(PricingType.SINGLE_JOB_ITEM))
			jc.setIssueMethod(PricingIssueMethod.ADVESO);
		else
			jc.setIssueMethod(PricingIssueMethod.EMAIL);
		// other required fields
		jc.setDuration(1);

		Company company = jc.getContact().getSub().getComp();
		Company allocatedCompany = this.companyService.get(allocatedCompanyDto.getKey());
		jc.setOrganisation(allocatedCompany);

		// vat rate from address of job
		CompanySettingsForAllocatedCompany companySettings = this.settingsService.getByCompany(company,
				allocatedCompany);
		if (companySettings.isTaxable()) {
			AddressSettingsForAllocatedCompany addressSettings = this.addresssettingsService
					.getByCompany(jc.getContact().getDefAddress(), allocatedCompany);
			if (addressSettings != null && addressSettings.getVatRate() != null) {
				jc.setVatRate(addressSettings.getVatRate().getRate());
			} else {
				jc.setVatRate((companySettings.getVatrate() != null) ? companySettings.getVatrate().getRate()
						: vatRateService.getForCountryAndType(allocatedCompany.getCountry().getCountryid(),
								VatRateType.COUNTRY_DEFAULT).getRate());
			}
		} else {
			logger.info("Company " + company.getConame() + " non-taxable");
			jc.setVatRate(new BigDecimal("0.00"));
		}
		// now set the items
		List<JobCostingItem> items = this.jobCostItemServ.createCostingItem(jc, form.getJobItemIds());
		TreeSet<JobCostingItem> jItemList = new TreeSet<>(new JobCostingItemComparator());
		jItemList.addAll(items);
		jc.setItems(jItemList);
		int itemNo = jItemList.stream().map(PricingItem::getItemno).max(Integer::compareTo).orElse(0);
		List<JobExpenseItem> expenseItems = form.getExpenseItemIds().stream().map(id -> jobExpenseItemService.get(id))
			.collect(Collectors.toList());
		jc.setExpenseItems(new ArrayList<>());
		for (JobExpenseItem expenseItem : expenseItems) {
			JobCostingExpenseItem newItem = new JobCostingExpenseItem(expenseItem, jc, ++itemNo);
			CostCalculator.updateSingleCost(newItem.getServiceCost(), roundUpFinalCosts);
			CostCalculator.updateItemCost(newItem);
			jc.getExpenseItems().add(newItem);
		}
		Set<PricingItem> allItems = new HashSet<>();
		allItems.addAll(jItemList);
		allItems.addAll(jc.getExpenseItems());
		CostCalculator.updatePricingTotalCost(jc, allItems);
		taxCalculator.calculateAndSetVATaxAndFinalCost(jc);
		jc.setAdditionalContacts(this.prepareAdditionalContacts(form.getAdditionalPersonids(), jc));
		this.jobCostServ.save(jc, currentContact);
		// create new job costing viewed
		JobCostingViewed jcv = new JobCostingViewed();
		jcv.setJobCosting(jc);
		jcv.setLastViewedBy(currentContact);
		jcv.setLastViewedOn(new Date());
		// insert job costing viewed
		this.jcvServ.insertJobCostingViewed(jcv);
		// Remove attributes from model to prevent inclusion as redirect parameters
		model.asMap().clear();
		return "redirect:viewjobcosting.htm?id=" + jc.getId();
	}

	private List<Integer> getAdditionalPersonIds(Job job) {
		// any costing mailgroup members should automatically be added as default
		// contacts
		List<MailGroupMember> mailGroupMembers = this.mailGroupMemberServ
				.getMembersOfGroupForSubdiv(MailGroupType.COSTINGS, job.getCon().getSub().getSubdivid(), true);
		List<Integer> result = new ArrayList<>();
		for (MailGroupMember member : mailGroupMembers) {
			result.add(member.getContact().getId());
		}
		return result;

	}

	private Set<AdditionalCostContact> prepareAdditionalContacts(List<Integer> personids, JobCosting costing) {
		List<Integer> addedContacts = new ArrayList<>();
		TreeSet<AdditionalCostContact> contacts = new TreeSet<>(
			new AdditionalCostContactEqualityComparator());
		if (personids != null) {
			// look for any additional contacts
			for (Integer personid : personids) {
				if (personid != null) {
					if ((personid != costing.getContact().getPersonid()) && !addedContacts.contains(personid)) {
						AdditionalCostContact con = new AdditionalCostContact();
						con.setContact(this.contactServ.get(personid));
						con.setCosting(costing);
						contacts.add(con);
						addedContacts.add(personid);
					}
				}
			}
		}
		return contacts;
	}

	/**
	 * Returns appropriate pricing types for the selected job and client
	 */
	private List<PricingType> getPricingTypeList(Job job) {
		List<PricingType> result = new ArrayList<>();
		if (job.getType() == JobType.SINGLE_PRICE) {
			result.add(PricingType.SITE);
		} else {
			Company allocatedCompany = job.getOrganisation().getComp();
			Contact clientContact = job.getCon();
			Boolean requiresSingleJobItem = this.pricingTypeSingleJobItem.parseValueHierarchical(Scope.CONTACT,
					clientContact, allocatedCompany);
			if (!requiresSingleJobItem) {
				result.add(PricingType.CLIENT);
			}
			result.add(PricingType.SINGLE_JOB_ITEM);
		}
		return result;
	}

	private PricingType getDefaultPricingType(Job job) {
		PricingType result;
		if (job.getType() == JobType.SINGLE_PRICE) {
			result = PricingType.SITE;
		} else {
			Company allocatedCompany = job.getOrganisation().getComp();
			Contact clientContact = job.getCon();
			Boolean requiresSingleJobItem = this.pricingTypeSingleJobItem.parseValueHierarchical(Scope.CONTACT,
				clientContact, allocatedCompany);
			if (requiresSingleJobItem) {
				result = PricingType.SINGLE_JOB_ITEM;
			} else {
				result = PricingType.CLIENT;
			}
		}
		return result;
	}

	@PreAuthorize("hasAuthority('JOB_COSTING_CREATE')")
	@RequestMapping(value = "/createjobcosting.htm", method = RequestMethod.GET)
	public String referenceData(@RequestParam(value = "jobid") Integer jobid, Model model,
								@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
								@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Job job = this.jobServ.get(jobid);
		if (job.getOrganisation().getComp().getCoid() != allocatedCompanyDto.getKey()) {
			throw new RuntimeException(
				"You must change to the business company of the job, in order to create a job costing.");
		}

		model.addAttribute("job", job);
		model.addAttribute("clientcost", getPricingTypeList(job));
		model.addAttribute("contacts",
			this.contactServ.getAllActiveSubdivContacts(job.getCon().getSub().getSubdivid()));
		model.addAttribute("userInstructionTypes",
				this.userService.get(username).getCon().getUserPreferences().getUserInstructionTypes());
		model.addAttribute("jobcosttypelist", Arrays.asList(JobCostingType.values()));
		model.addAttribute("relatedInstructionLinks", this.instructionLinkServ.getAllForJob(job));

		return "trescal/core/pricing/jobcost/createjobcosting";
	}
}