package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.PurchaseOrderStatus;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderstatus.dto.PurchaseOrderStatusProjectionDTO;
import org.trescal.cwms.core.system.entity.basestatus.ActionType;
import org.trescal.cwms.core.userright.enums.Permission;

@Service
public class PurchaseOrderStatusServiceImpl implements PurchaseOrderStatusService {

	@Autowired
	private PurchaseOrderStatusDao purchaseOrderStatusDao;

	@Override
	public PurchaseOrderStatus findOnCancelPurchaseOrderStatus(ActionType type) {
		return this.purchaseOrderStatusDao.findOnCancelPurchaseOrderStatus(type);
	}

	@Override
	public List<PurchaseOrderStatus> findAllPurchaseOrderStatusRequiringAttention() {
		return this.purchaseOrderStatusDao.findAllPurchaseOrderStatusRequiringAttention();
	}

	@Override
	public PurchaseOrderStatus findOnCompletePurchaseOrderStatus(ActionType type) {
		return this.purchaseOrderStatusDao.findOnCompletePurchaseOrderStatus(type);
	}

	@Override
	public PurchaseOrderStatus findOnInsertPurchaseOrderStatus(ActionType type) {
		return this.purchaseOrderStatusDao.findOnInsertPurchaseOrderStatus(type);
	}

	public PurchaseOrderStatus findPurchaseOrderStatus(int id) {
		return this.purchaseOrderStatusDao.find(id);
	}

	public void setPurchaseOrderStatusDao(PurchaseOrderStatusDao purchaseOrderStatusDao) {
		this.purchaseOrderStatusDao = purchaseOrderStatusDao;
	}

	@Override
	public Boolean isAllowChangeStatus(PurchaseOrderStatus poStatutTemp, Collection<? extends GrantedAuthority> auth) {
		return auth.contains(getPermissionNeeded(poStatutTemp));
	}

	@Override
	public Permission getPermissionNeeded(PurchaseOrderStatus poStatutTemp) {
		switch (poStatutTemp.getName()) {
		case "awaiting construction":
			return Permission.PURCHASE_ORDER_STATUS_CONSTRUCT;
		case "awaiting approval":
			return Permission.PURCHASE_ORDER_STATUS_VALIDATE;
		case "awaiting issue":
			return Permission.PURCHASE_ORDER_STATUS_ISSUE;
		case "awaiting pre-approval":
			return Permission.PURCHASE_ORDER_STATUS_PRE_VALIDATE;
		case "awaiting pre-issue":
			return Permission.PURCHASE_ORDER_STATUS_PRE_ISSUE;
		case "issued":
			return Permission.PURCHASE_ORDER_STATUS_ISSUE;
		default:
			return null;
		}
	}

	@Override
	public Integer findPurchaseOrderStatusIdRequiringAttentionByName(String name) {
		return this.purchaseOrderStatusDao.findPurchaseOrderStatusIdRequiringAttentionByName(name);
	}

	@Override
	public List<PurchaseOrderStatusProjectionDTO> findAllPurchaseOrderStatusRequiringAttentionProjection(Locale locale) {
		return this.purchaseOrderStatusDao.findAllPurchaseOrderStatusRequiringAttentionProjection(locale);
	}

	@Override
	public Boolean isAllowChangeStatus(PurchaseOrderStatusProjectionDTO poStatutTemp,
			Collection<? extends GrantedAuthority> auth) {
		if (getPermissionNeeded(poStatutTemp) != null)
			return auth.contains(getPermissionNeeded(poStatutTemp));
		else
			return false;
	}

	@Override
	public Permission getPermissionNeeded(PurchaseOrderStatusProjectionDTO poStatutTemp) {
		switch (poStatutTemp.getName()) {
		case "awaiting construction":
			return Permission.PURCHASE_ORDER_STATUS_CONSTRUCT;
		case "awaiting pre-approval":
			return Permission.PURCHASE_ORDER_STATUS_PRE_VALIDATE;
		case "awaiting approval":
			return Permission.PURCHASE_ORDER_STATUS_VALIDATE;
		case "awaiting issue":
			return Permission.PURCHASE_ORDER_STATUS_ISSUE;
		case "awaiting pre-issue":
			return Permission.PURCHASE_ORDER_STATUS_PRE_ISSUE;
		case "issued":
			return Permission.PURCHASE_ORDER_STATUS_ISSUE;
		default:
			return null;
		}
	}
}