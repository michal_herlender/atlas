package org.trescal.cwms.core.pricing.purchaseorder.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.Collection;

@Getter
@Setter
public class PurchaseOrderHomeForm {
    private Integer addressid;
    private Integer orgid;
    private Integer coid;
    // search fields
    private String coname;
    private String contact;
    private LocalDate date;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private int itemCount;
    private String jobno;
    private Collection<PurchaseOrder> orders;

    private int pageNo;
    private Integer personid;
    private String pono;
    private int resultsPerPage;
    // results data
    private PagedResultSet<PurchaseOrder> rs;
    private String searchCriteria;
}
