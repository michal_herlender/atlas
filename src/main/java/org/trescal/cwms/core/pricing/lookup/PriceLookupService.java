package org.trescal.cwms.core.pricing.lookup;

/**
 * Utility service to lookup pricing from multiple sources (quotation, catalog)
 * for use in multiple places (job items, quotation) with support for efficient
 * lookup of prices for many items at once
 * 
 * Preference / order (with possibility to enable/disable specific parts of search) is to:
 * 
 *  1) Search quotation items
 *     i) matching on instruments
 *     	   a) matching on specific quotations
 *         b) matching on valid quotations
 *     ii) matching on instrument models
 *     	   a) matching on specific quotations
 *         b) matching on valid quotations
 *     iii) matching on sales categories (sales category models, for now)
 *     	   a) matching on specific quotations
 *         b) matching on valid quotations
 *        
 *  2) Search catalog prices
 *        i) matching on instrument models
 *        ii) matching on sales categories (sales category models, for now)
 * 
 * Created 2019-04-20 GB
 */
public interface PriceLookupService {
	public void findPrices(PriceLookupRequirements requirements);
}
