package org.trescal.cwms.core.pricing.creditnote.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotetax.CreditNoteTax;

import lombok.Data;

@Builder
@Getter
public class CreditNoteItemUpdateResult {

	private Integer itemId;
	private Integer itemNo;
	private List<CreditNoteTax> taxes;
	private BigDecimal grossTotal;
	private BigDecimal vatValue;
	private BigDecimal netTotal;
	private String currencyCode;
}