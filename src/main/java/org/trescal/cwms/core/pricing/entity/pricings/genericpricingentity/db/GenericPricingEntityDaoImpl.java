package org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.db;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencySupport;

@Repository("GenericPricingEntityDao")
public class GenericPricingEntityDaoImpl implements GenericPricingEntityDao {
	
	@PersistenceContext(unitName="entityManagerFactory")
	private EntityManager entityManager;
	
	@Override
	public <T extends MultiCurrencySupport> T findMultiCurrencyEntity(Class<T> clazz, int id) {
		return entityManager.find(clazz, id);
	}
	
	@Override
	public <T extends GenericPricingEntity<?>> T findPricing(Class<T> clazz, int id) {
		return entityManager.find(clazz, id);
	}
}