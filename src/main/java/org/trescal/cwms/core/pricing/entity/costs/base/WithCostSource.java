package org.trescal.cwms.core.pricing.entity.costs.base;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;

public interface WithCostSource<A> {
    CostSource getCostSrc();
    A getLinkedCost();
}
