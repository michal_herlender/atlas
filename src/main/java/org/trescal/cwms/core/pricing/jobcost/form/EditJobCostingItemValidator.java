package org.trescal.cwms.core.pricing.jobcost.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditJobCostingItemValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(EditJobCostingItemForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		EditJobCostingItemForm form = (EditJobCostingItemForm) target;

		errors.pushNestedPath("item");
		super.validate(form.getItem(), errors);

		// validate each of the costs individually
		errors.pushNestedPath("adjustmentCost");
		super.validate(form.getItem().getAdjustmentCost(), errors);
		errors.popNestedPath();
		errors.pushNestedPath("calibrationCost");
		super.validate(form.getItem().getCalibrationCost(), errors);
		errors.popNestedPath();
		errors.pushNestedPath("purchaseCost");
		super.validate(form.getItem().getPurchaseCost(), errors);
		errors.popNestedPath();
		errors.pushNestedPath("repairCost");
		super.validate(form.getItem().getRepairCost(), errors);
		errors.popNestedPath();
		errors.popNestedPath();
		
		// Check that at least one cost is specified to prevent empty job costings
		// Note - (this form is not used for single price costs)
		if (!form.getItem().getAdjustmentCost().isActive() &&
			!form.getItem().getCalibrationCost().isActive() &&
			!form.getItem().getRepairCost().isActive() &&
			!form.getItem().getPurchaseCost().isActive()) {
			errors.reject("error.jobcostingitem.noprices", null, "The pricing item must contain at least one type of price");
		}

		// for each cost check if a third party quotation is selected as the
		// third cost source and if so validate that a third party quote for
		// this costtype is actually linked
		JobCostingCalibrationCost cal = form.getItem().getCalibrationCost();
		if ((cal.getThirdCostSrc() == ThirdCostSource.THIRD_PARTY_QUOTATION)
				&& (form.getCalibrationCostId() == null))
		{
			errors.rejectValue("calibrationCostId", "error.jobcostingitem.linkedcost.calibration.missing", "You must link a quotation item");
		}
		JobCostingRepairCost rep = form.getItem().getRepairCost();
		if ((rep.getThirdCostSrc() == ThirdCostSource.THIRD_PARTY_QUOTATION)
				&& (form.getRepairCostId() == null))
		{
			errors.rejectValue("repairCostId", "error.jobcostingitem.linkedcost.repair.missing", "You must link a quotation item");
		}
		JobCostingAdjustmentCost adj = form.getItem().getAdjustmentCost();
		if ((adj.getThirdCostSrc() == ThirdCostSource.THIRD_PARTY_QUOTATION)
				&& (form.getAdjustmentCostId() == null))
		{
			errors.rejectValue("adjustmentCostId", "error.jobcostingitem.linkedcost.adjustment.missing", "You must link a quotation item");
		}
		JobCostingPurchaseCost pur = form.getItem().getPurchaseCost();
		if ((pur.getThirdCostSrc() == ThirdCostSource.THIRD_PARTY_QUOTATION)
				&& (form.getPurchaseCostId() == null))
		{
			errors.rejectValue("purchaseCostId", "error.jobcostingitem.linkedcost.purchase.missing", "You must link a quotation item");
		}

	}
}