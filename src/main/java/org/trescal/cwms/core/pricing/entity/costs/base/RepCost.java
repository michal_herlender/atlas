/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.costs.base;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.trescal.cwms.core.pricing.entity.enums.PartsMarkupSource;

@Entity
@Table(name = "costs_repair")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "costtype", discriminatorType = DiscriminatorType.STRING)
public abstract class RepCost extends Cost
{
	// parts costs defined here because they don't seem applicable to any other
	// cost type

	/**
	 * Total base cost of all parts used in this repair (before markups).
	 */
	private BigDecimal partsCost;

	/**
	 * The markup rate to be used to markup these parts costs. This can be null
	 * if the markup is a fixed value as opposed to a precentage of the cost.
	 */
	private BigDecimal partsMarkupRate;

	/**
	 * Enum reference defining the source of a parts markup.
	 */
	private PartsMarkupSource partsMarkupSource;

	/**
	 * The value of the markup being applied to these parts costs.
	 */
	private BigDecimal partsMarkupValue;

	/**
	 * Total of the parts + markup.
	 */
	private BigDecimal partsTotal;

	@Column(name = "partscost", precision = 10, scale = 2, nullable = true)
	public BigDecimal getPartsCost()
	{
		return this.partsCost;
	}

	@Column(name = "partsmarkuprate", precision = 5, scale = 2)
	public BigDecimal getPartsMarkupRate()
	{
		return this.partsMarkupRate;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "partsmarkupsource")
	public PartsMarkupSource getPartsMarkupSource()
	{
		return this.partsMarkupSource;
	}

	@Column(name = "partsmarkupvalue", precision = 10, scale = 2, nullable = true)
	public BigDecimal getPartsMarkupValue()
	{
		return this.partsMarkupValue;
	}

	@Column(name = "partstotal", precision = 10, scale = 2, nullable = true)
	public BigDecimal getPartsTotal()
	{
		return this.partsTotal;
	}

	public void setPartsCost(BigDecimal partsCost)
	{
		this.partsCost = partsCost;
	}

	public void setPartsMarkupRate(BigDecimal partsMarkupRate)
	{
		this.partsMarkupRate = partsMarkupRate;
	}

	public void setPartsMarkupSource(PartsMarkupSource partsMarkupSource)
	{
		this.partsMarkupSource = partsMarkupSource;
	}

	public void setPartsMarkupValue(BigDecimal partsMarkupValue)
	{
		this.partsMarkupValue = partsMarkupValue;
	}

	public void setPartsTotal(BigDecimal partsTotal)
	{
		this.partsTotal = partsTotal;
	}
}