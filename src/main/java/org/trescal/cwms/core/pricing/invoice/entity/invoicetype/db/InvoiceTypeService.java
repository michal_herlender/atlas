package org.trescal.cwms.core.pricing.invoice.entity.invoicetype.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.pricing.invoice.dto.InvoiceTypeKeyValue;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;

public interface InvoiceTypeService extends BaseService<InvoiceType, Integer> {

	InvoiceType findInvoiceTypeByName(String name);

	List<InvoiceTypeKeyValue> getAllTranslated(Locale locale);
}