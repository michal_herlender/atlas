package org.trescal.cwms.core.pricing.jobcost.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.JobCostingFileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME, Constants.HTTPSESS_NEW_FILE_LIST})
public class JobCostingDocumentController
{
	@Autowired
	private DocumentService documentService; 
	@Autowired
	private JobCostingService jobCostServ;
	
	@RequestMapping(value="/birtjobcostingdocument.htm")
	public String handleBirtRequest(Model model, Locale locale,
			@RequestParam(value="id", required=true) Integer id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) {
		JobCosting costing = this.jobCostServ.findJobCosting(id);
		JobCostingFileNamingService fnService = new JobCostingFileNamingService(costing);
		Document doc = documentService.createBirtDocument(id, BaseDocumentType.JOB_COSTING, locale, Component.JOB_COSTING, fnService);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		// Remove attributes from model to prevent inclusion as redirect parameters
		model.asMap().clear();
		return "redirect:viewjobcosting.htm?id=" + id + "&loadtab=costingfiles-tab";
	}
}