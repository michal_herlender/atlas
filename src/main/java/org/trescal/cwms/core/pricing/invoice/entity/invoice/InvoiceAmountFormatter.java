package org.trescal.cwms.core.pricing.invoice.entity.invoice;

import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.RuleBasedNumberFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;

@Component
public class InvoiceAmountFormatter {
	
	@Autowired
	private MessageSource messages;
	
	private static final String MESSAGE_CODE_AND = "and";
	private static final String DEFAULT_MESSAGE_AND = "and";
	
	public String getTotalAmountInLetters(BigDecimal amount, SupportedCurrency currency, Locale locale) {
		if (amount == null) throw new IllegalArgumentException("Amount must not be null");
		if (currency == null) throw new IllegalArgumentException("Currency must not be null");
		if (locale == null) throw new IllegalArgumentException("Locale must not be null");

		StringBuilder result = new StringBuilder();

		// Gets the final cost in letters
		NumberFormat formatter = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);

		int majorAmount = amount.intValue();
		String leftFinalCostInLetters = formatter.format(majorAmount);
		result.append(leftFinalCostInLetters);
		result.append(" ");
		result.append(majorAmount == 1 ? currency.getMajorSingularName() : currency.getMajorPluralName());
		
		if (currency.getMinorUnit()) {
			result.append(" ");
			result.append(this.messages.getMessage(MESSAGE_CODE_AND, null, DEFAULT_MESSAGE_AND, locale));
			result.append(" ");
			int exponent = currency.getMinorExponent();
			BigInteger remainder = amount.remainder(BigDecimal.ONE).movePointRight(exponent).abs().toBigInteger();
			String rightFinalCostInLetters = formatter.format(remainder);
			result.append(rightFinalCostInLetters);
			result.append(" ");
			result.append(majorAmount == 1 ? currency.getMinorSingularName() : currency.getMinorPluralName());
		}
	
		return result.toString();
	}
}
