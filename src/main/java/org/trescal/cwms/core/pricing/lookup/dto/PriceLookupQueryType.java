package org.trescal.cwms.core.pricing.lookup.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;
import java.util.Locale;

/**
 * From "least preferred" to "most preferred"
 */
public enum PriceLookupQueryType {
	CATALOG_PRICE_SALES_CATEGORY_MODEL(
			"pricelookupquerytype.catalogprice",
			"pricelookupquerytype.salescategorymodel"),
	CATALOG_PRICE_INSTRUMENT_MODEL(
			"pricelookupquerytype.catalogprice",
			"pricelookupquerytype.instrumentmodel"),
	QUOTATION_ITEM_SALES_CATEGORY_MODEL(
			"pricelookupquerytype.quotationitem",
			"pricelookupquerytype.salescategorymodel"),
	QUOTATION_ITEM_INSTRUMENT_MODEL(
			"pricelookupquerytype.quotationitem",
			"pricelookupquerytype.instrumentmodel"),
	QUOTATION_ITEM_INSTRUMENT(
			"pricelookupquerytype.quotationitem",
			"pricelookupquerytype.instrument");
	
	private final String majorMessageCode;
	private final String minorMessageCode;
	private MessageSource messageSource;
	
	PriceLookupQueryType(String majorMessageCode, String minorMessageCode) {
		this.majorMessageCode = majorMessageCode;
		this.minorMessageCode = minorMessageCode;
	}
	
	@Component
	public static class PriceLookupQueryTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
			for (PriceLookupQueryType reason: EnumSet.allOf(PriceLookupQueryType.class)) {
				reason.setMessageSource(messages);
			}
		}
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		String majorMessage = this.messageSource.getMessage(this.majorMessageCode, null, "", locale);
		String minorMessage = this.messageSource.getMessage(this.minorMessageCode, null, "", locale);
		StringBuilder result = new StringBuilder();
		if (majorMessage.isEmpty() || minorMessage.isEmpty()) {
			result.append(this.name());
		}
		else {
			result.append(majorMessage);
			result.append(" - ");
			result.append(minorMessage);
		}
		return result.toString();
	}
	
}