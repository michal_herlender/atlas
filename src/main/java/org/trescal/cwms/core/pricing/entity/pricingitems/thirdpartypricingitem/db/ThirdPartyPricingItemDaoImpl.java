package org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem;

@Repository("ThirdPartyPricingItemDao")
public class ThirdPartyPricingItemDaoImpl extends BaseDaoImpl<ThirdPartyPricingItem, Integer> implements ThirdPartyPricingItemDao {
	
	@Override
	protected Class<ThirdPartyPricingItem> getEntity() {
		return ThirdPartyPricingItem.class;
	}
}