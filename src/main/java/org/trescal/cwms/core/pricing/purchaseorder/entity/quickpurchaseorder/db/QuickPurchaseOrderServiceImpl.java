package org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.db;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.purchaseorder.dto.SupplierPOProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.quickpurchaseorder.QuickPurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.form.genericentityvalidator.QuickPurchaseOrderValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.deletedcomponent.db.DeletedComponentService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.spring.model.KeyValue;

@Service("QuickPurchaseOrderService")
public class QuickPurchaseOrderServiceImpl extends BaseServiceImpl<QuickPurchaseOrder, Integer>
		implements QuickPurchaseOrderService {
	@Autowired
	private BeanValidator dcValidator;
	@Autowired
	private DeletedComponentService delCompServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private QuickPurchaseOrderDao quickPurchaseOrderDao;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private QuickPurchaseOrderValidator validator;

	@Override
	protected BaseDao<QuickPurchaseOrder, Integer> getBaseDao() {
		return quickPurchaseOrderDao;
	}

	@Override
	public ResultWrapper ajaxDeleteQuickPurchaseOrder(int qpoid, String reason, HttpSession session) {
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.getEagerLoad(username).getCon();
		// find quick purchase order
		QuickPurchaseOrder order = this.get(qpoid);
		// order null?
		if (order != null) {
			// create deletion object
			DeletedComponent dinv = new DeletedComponent();
			dinv.setComponent(Component.PURCHASEORDER);
			dinv.setContact(contact);
			dinv.setDate(new Date());
			dinv.setRefNo(order.getPono());
			dinv.setReason(reason);
			// validate deletion object
			BindException errors = new BindException(dinv, "dinv");
			this.dcValidator.validate(dinv, errors);
			// if validation passes then delete the order
			if (!errors.hasErrors()) {
				// delete the quick purchase order
				this.delete(order);
				// persist the deletedorder
				this.delCompServ.insertDeletedComponent(dinv);
			}
			// set deletedorder into the results
			return new ResultWrapper(errors, dinv);
		} else {
			// return un-successful wrapper
			return new ResultWrapper(false, "Quick purchase order could not be found", null, null);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResultWrapper createQuickPurchaseOrder(String description, String jobno, HttpSession session) {
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.getEagerLoad(username).getCon();
		KeyValue<Integer, String> subdivDto = (KeyValue<Integer, String>) session
				.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		QuickPurchaseOrder order = new QuickPurchaseOrder();
		order.setConsolidated(false);
		order.setCreatedOn(new Date());
		order.setDescription(description);
		order.setCreatedBy(contact);
		order.setJobno(jobno);
		order.setOrganisation(allocatedSubdiv.getComp());
		// add a temporary pono to get past validation
		order.setPono("temp");
		// create a new validator to validate this order before trying the
		// insert
		this.validator.validate(order, null);
		// if validation passes then calculate the next id and insert the order
		if (!this.validator.getErrors().hasErrors()) {
			// look up the new id
			String pono = numerationService.generateNumber(NumerationType.PURCHASE_ORDER, allocatedSubdiv.getComp(),
					allocatedSubdiv);
			order.setPono(pono);
			this.save(order);
		}
		return new ResultWrapper(this.validator.getErrors(), order);
	}

	@Override
	public List<QuickPurchaseOrder> findAllUnconsolidatedQuickPurchaseOrders(Company allocatedCompany) {
		return this.quickPurchaseOrderDao.findAllUnconsolidatedQuickPurchaseOrders(allocatedCompany);
	}

	@Override
	public List<SupplierPOProjectionDTO> findAllUnconsolidatedQuickPurchaseOrdersUsingProjection(
			Integer allocatedCompanyId,Integer SubdivId) {
		return this.quickPurchaseOrderDao.findAllUnconsolidatedQuickPurchaseOrdersUsingProjection(allocatedCompanyId,SubdivId);
	}
}