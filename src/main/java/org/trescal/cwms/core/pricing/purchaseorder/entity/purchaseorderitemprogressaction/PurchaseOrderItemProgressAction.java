package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemprogressaction;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;

/**
 * Entity that tracks one or more progress changes to a particular
 * {@link PurchaseOrderItem}.
 * 
 * @author Richard
 */
@Entity
@Table(name = "purchaseorderitemprogressaction")
public class PurchaseOrderItemProgressAction
{
	private int id;
	private Contact contact;
	private Date date;
	private String description;
	private PurchaseOrderItem item;

	private PurchaseOrderItemReceiptStatus receiptStatus;
	private Boolean cancelled;
	private Boolean accountsApproved;
	private Boolean goodsApproved;

	@Column(name = "accountsapproved", nullable = true, columnDefinition="tinyint")
	public Boolean getAccountsApproved()
	{
		return this.accountsApproved;
	}

	@Column(name = "cancelled", nullable = true, columnDefinition="tinyint")
	public Boolean getCancelled()
	{
		return this.cancelled;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", unique = false, nullable = false)
	public Contact getContact()
	{
		return this.contact;
	}

	@NotNull
	@Column(name = "date", nullable = false, columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate()
	{
		return this.date;
	}

	@Length(max = 200)
	@Column(name = "description", length = 200, nullable = true)
	public String getDescription()
	{
		return this.description;
	}

	@Column(name = "goodsapproved", nullable = true, columnDefinition="tinyint")
	public Boolean getGoodsApproved()
	{
		return this.goodsApproved;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "itemid", nullable = false)
	public PurchaseOrderItem getItem()
	{
		return this.item;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "receiptstatus", nullable = true)
	public PurchaseOrderItemReceiptStatus getReceiptStatus()
	{
		return this.receiptStatus;
	}

    public void setAccountsApproved(Boolean accountsApproved)
	{
		this.accountsApproved = accountsApproved;
	}

	public void setCancelled(Boolean cancelled)
	{
		this.cancelled = cancelled;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setGoodsApproved(Boolean goodsApproved)
	{
		this.goodsApproved = goodsApproved;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setItem(PurchaseOrderItem item)
	{
		this.item = item;
	}

	public void setReceiptStatus(PurchaseOrderItemReceiptStatus receiptStatus)
	{
		this.receiptStatus = receiptStatus;
	}

}
