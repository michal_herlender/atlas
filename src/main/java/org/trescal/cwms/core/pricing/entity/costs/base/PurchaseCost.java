/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.costs.base;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "costs_purchase")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "costtype", discriminatorType = DiscriminatorType.STRING)
public abstract class PurchaseCost extends Cost
{

}
