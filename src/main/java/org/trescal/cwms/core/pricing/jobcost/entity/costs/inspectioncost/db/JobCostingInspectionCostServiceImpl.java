package org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.db;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.JobCostingCostServiceSupport;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.inspectioncost.JobCostingInspectionCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.system.entity.markups.markuprange.db.MarkupRangeService;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;
import org.trescal.cwms.core.tools.MathTools;

@Service("JobCostingInspectionCostService")
public class JobCostingInspectionCostServiceImpl extends JobCostingCostServiceSupport<JobCostingInspectionCost> implements JobCostingInspectionCostService {
    @Autowired
    private JobCostingInspectionCostDao jobCostingInspectionCostDao;
    @Autowired
    private MarkupRangeService markupRangeServ;
    @Value("${cwms.config.costing.costs.lookuphierarchy.inspection}")
    private String costSourceHierarchy;
    @Value("${cwms.config.costing.costs.lookupcosts.inspection}")
    private boolean lookupCosts;
    @Value("${cwms.config.costing.costs.deactivateonzero.inspection}")
    protected boolean deactivateCostOnZeroValue;

    @Override
    public boolean getLookupCosts() {
        return lookupCosts;
    }

    @Override
    public boolean getDeactivateCostOnZeroValue() {
        return deactivateCostOnZeroValue;
    }

    @Override
    public String getCostSourceHierarchy() {
        return costSourceHierarchy;
    }

    @Override
    public void setCostSourceHierarchy(String costSourceHierarchy) {
        this.costSourceHierarchy = costSourceHierarchy;
    }

	public ResultWrapper calculateAjaxInspectionCost(BigDecimal calcost)
	{
		BigDecimal inspection = new BigDecimal("0.00");

		// now calculate what the inspection cost should be
		inspection = this.markupRangeServ.calculateMarkUp(calcost, MarkupType.INSPECTION);

		// round up the inspection cost?
		if (this.roundUpFinalCosts)
		{
			inspection = new BigDecimal(CostCalculator.roundCWMSCost(inspection.doubleValue())).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN);
		}

		return new ResultWrapper(true, "", inspection, null);
	}

	@Override
	public JobCostingInspectionCost calculateInspectionCost(JobCostingInspectionCost cost)
	{
		cost.setActive(true);
		cost.setAutoSet(true);

		// don't discount inspection costs
		cost.setDiscountRate(new BigDecimal("0.00"));
		cost.setDiscountValue(new BigDecimal("0.00"));

		BigDecimal inspection = new BigDecimal("0.00");

		// if the calibration cost is not null set this into the inspection cost
		if (cost.getJobCostingItem().getCalibrationCost() != null)
		{
			inspection = cost.getJobCostingItem().getCalibrationCost().getFinalCost();
		}

		// now calculate what the inspection cost should be
		inspection = this.markupRangeServ.calculateMarkUp(inspection, MarkupType.INSPECTION);

		cost.setTotalCost(inspection);
		cost.setFinalCost(inspection);

		return cost;
	}

	@Override
	public Cost createCostFromCopy(JobCostingItem ji, JobCostingItem old)
	{
		JobCostingInspectionCost newCost = null;

		if ((old != null) && (old.getInspectionCost() != null))
		{
			newCost = new JobCostingInspectionCost();

			JobCostingInspectionCost oldCost = old.getInspectionCost();
			BeanUtils.copyProperties(oldCost, newCost);
			newCost.setCostid(null);
			newCost.setJobCostingItem(ji);
			newCost.setAutoSet(true);
		}
		return newCost;
	}

	public void deleteJobCostingInspectionCost(JobCostingInspectionCost jobcostinginspectioncost)
	{
		this.jobCostingInspectionCostDao.remove(jobcostinginspectioncost);
	}

	public JobCostingInspectionCost findJobCostingInspectionCost(int id)
	{
		return this.jobCostingInspectionCostDao.find(id);
	}

	@Override
	public List<JobCostingInspectionCost> findMatchingCosts(Integer plantid, Integer coid, int modelid, Integer caltypeid, Integer page, Integer resPerPage)
	{
		return this.jobCostingInspectionCostDao.findMatchingCosts(JobCostingInspectionCost.class, plantid, coid, modelid, caltypeid, this.resultsYearFilter, page, resPerPage);
	}

	public List<JobCostingInspectionCost> getAllJobCostingInspectionCosts()
	{
		return this.jobCostingInspectionCostDao.findAll();
	}

	public void insertJobCostingInspectionCost(JobCostingInspectionCost JobCostingInspectionCost)
	{
		this.jobCostingInspectionCostDao.persist(JobCostingInspectionCost);
	}

	@Override
	public Cost resolveCostLookup(CostSource source, JobCostingItem ji)
	{
		JobCostingInspectionCost cost = new JobCostingInspectionCost();
		cost.setCostType(CostType.INSPECTION);
		cost.setJobCostingItem(ji);
		return this.calculateInspectionCost(cost);
	}

	public void saveOrUpdateJobCostingInspectionCost(JobCostingInspectionCost jobcostinginspectioncost)
	{
		this.jobCostingInspectionCostDao.saveOrUpdate(jobcostinginspectioncost);
	}

	@Override
	public JobCostingInspectionCost updateCosts(JobCostingInspectionCost cost)
	{
		return cost;
	}

	public void updateJobCostingInspectionCost(JobCostingInspectionCost JobCostingInspectionCost)
	{
		this.jobCostingInspectionCostDao.update(JobCostingInspectionCost);
	}
}