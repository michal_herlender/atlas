package org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.tools.MathTools;

import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Base class from which all Price Item sub-classes inherit.
 */
@MappedSuperclass
@Setter
public abstract class PricingItem extends Auditable {

	private BigDecimal finalCost;
	private BigDecimal generalDiscountRate;
	private BigDecimal generalDiscountValue;
	private Integer itemno;
	/**
	 * Simple flag to determine is this item is part of a base-unit - save's call to
	 * thisModuleBaseUnits.size()
	 */
	private boolean partOfBaseUnit;
	private Integer quantity;
	private BigDecimal totalCost;

	public PricingItem() {
	}

	/**
	 * Copy constructor
	 * 
	 * @param pi an instance or child class of PricingItem
	 */
	public PricingItem(PricingItem pi) {
		this.itemno = pi.getItemno();
		this.quantity = pi.getQuantity();
		this.generalDiscountRate = pi.getGeneralDiscountRate();
		this.generalDiscountValue = pi.getGeneralDiscountValue();
		this.totalCost = pi.getTotalCost();
		this.partOfBaseUnit = pi.isPartOfBaseUnit();
	}

	@Transient
	public abstract PricingItem getBaseUnit();

	@Transient
	public abstract Set<Cost> getCosts();

	@NotNull
	@Column(name = "finalcost", nullable = false, precision = 10, scale = 2)
	public BigDecimal getFinalCost() {
		return this.finalCost;
	}

	@NotNull
	@Column(name = "discountrate", nullable = false, precision = 5, scale = 2)
	public BigDecimal getGeneralDiscountRate() {
		return this.generalDiscountRate;
	}

	@NotNull
	@Column(name = "discountvalue", nullable = false, precision = 10, scale = 2)
	public BigDecimal getGeneralDiscountValue() {
		return this.generalDiscountValue;
	}

	@Transient
	public abstract Integer getId();

	@NotNull
	@Column(name = "itemno", nullable = false)
	@Type(type = "int")
	public Integer getItemno() {
		return this.itemno;
	}

	@Transient
	public abstract Set<? extends PricingItem> getModules();

	@NotNull
	@Min(value = 1)
	@Column(name = "quantity", nullable = false)
	public Integer getQuantity() {
		return this.quantity;
	}

	@NotNull
	@Column(name = "totalcost", nullable = false, precision = 10, scale = 2)
	public BigDecimal getTotalCost() {
		return this.totalCost;
	}

	/**
	 * Returns the cost per unit with any applied discounts
	 * 
	 * @return the unit cost
	 */
	@Transient
	public BigDecimal getUnitCost() {
		if ((this.finalCost != null) && (this.quantity != null) && (this.finalCost.doubleValue() > 0)
				&& (this.quantity > 0)) {
			return (this.finalCost.divide(new BigDecimal(this.quantity), MathTools.SCALE_FIN_CALC,
					MathTools.ROUND_MODE_FIN)).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN);
		} else {
			return new BigDecimal("0.00");
		}
	}

	@Column(name = "partofbaseunit", nullable = false, columnDefinition = "bit")
	public boolean isPartOfBaseUnit() {
		return this.partOfBaseUnit;
	}
}