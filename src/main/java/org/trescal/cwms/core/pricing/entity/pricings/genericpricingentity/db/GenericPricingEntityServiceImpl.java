package org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.db;

import java.math.BigDecimal;

import io.vavr.control.Either;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.dwr.DWRService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencySupport;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.MultiCurrencyUtils;
import org.trescal.cwms.spring.model.KeyValue;

@Service("GenericEntityPricingService")
public class GenericPricingEntityServiceImpl implements GenericPricingEntityService {
    @Autowired
    private GenericPricingEntityDao pricingDao;
    @Autowired
    private DWRService ajaxServ;
    @Autowired
    private MessageSource messages;
    @Autowired
    private CompanyService companyService;

    @Override
    public MultiCurrencySupport findMultiCurrencyEntity(Class<? extends MultiCurrencySupport> clazz, int id) {
        if (clazz != null && this.supportsMultiCurrency(clazz))
            return this.pricingDao.findMultiCurrencyEntity(clazz, id);
        return null;
    }

    @Override
    public Either<String, BigDecimal> convertValueToBaseCurrency(Integer entitiId, BigDecimal value, SupportedCurrency baseCurrency, Class<? extends MultiCurrencySupport> pricingClassName){
            if (pricingClassName == null || !this.supportsMultiCurrency(pricingClassName))
                return Either.left(this.messages.getMessage("error.currency.conversion.general", null, "Unable to get base value", null));

            val pricing = this.findMultiCurrencyEntity(pricingClassName,entitiId);

            if ((pricing == null) || (pricing.getRate() == null))
                return Either.left(this.messages.getMessage("error.currency.conversion.general", null, "Unable to get base value", null));
                return Either.right(MultiCurrencyUtils.convertCurrencyValue(value,pricing.getCurrency(),baseCurrency));
        }

        @Override
        public String getBaseCurrencyValue(int id, double value) {
        try {
            SupportedCurrency baseCurrency = this.getBaseCurrency();
            // try and get a class from the session
            Class<? extends MultiCurrencySupport> clazz = this.getPricingClass();
            
            if (clazz == null) {
                throw new ClassNotFoundException();
            } else if (!this.supportsMultiCurrency(clazz)) {
                throw new Exception();
            } else {
                MultiCurrencySupport pricing = this.findMultiCurrencyEntity(clazz, id);
                if ((pricing == null) || (pricing.getRate() == null)) {
                    return this.messages.getMessage("error.currency.conversion.general", null, "Unable to get base value", null);
                } else {
                    BigDecimal convertedValue = MultiCurrencyUtils.convertCurrencyValue(new BigDecimal(value), pricing.getCurrency(), baseCurrency);

                    return baseCurrency.getCurrencySymbol() + " " + convertedValue;
                }
            }
        } catch (Exception ex) {
            return this.messages.getMessage("error.currency.conversion.general", null, "Unable to get base value", null);
        }
    }

    @SuppressWarnings("unchecked")
    private Class<? extends MultiCurrencySupport> getPricingClass() {
        Class<? extends MultiCurrencySupport> clazz = null;
        Object obj = this.ajaxServ.getDWRSessionAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME, null);
        if (obj != null) clazz = (Class<? extends MultiCurrencySupport>) obj;
        return clazz;
    }

    @SuppressWarnings("unchecked")
    private SupportedCurrency getBaseCurrency() {
        KeyValue<Integer, String> companyKeyValue = (KeyValue<Integer, String>) this.ajaxServ.getDWRSessionAttribute(Constants.SESSION_ATTRIBUTE_COMPANY, null);
        Company usersCompany = companyService.get(companyKeyValue.getKey());
        return usersCompany.getCurrency();
    }

    @Override
    public boolean supportsMultiCurrency(Class<?> clazz) {
        return MultiCurrencySupport.class.isAssignableFrom(clazz);
    }
}
