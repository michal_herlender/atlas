package org.trescal.cwms.core.pricing.creditnote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.avalara.AvalaraConnector;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.vatrate.VatRateFormatter;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteaction.CreditNoteAction;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.CreditNoteStatus;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotestatus.db.CreditNoteStatusService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.baseaction.ActionComparator;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;

import java.time.LocalDate;
import java.util.*;

@Controller
@IntranetController
@RequestMapping("/viewcreditnote.htm")
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ViewCreditNoteController {

	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Autowired
	private AvalaraConnector avalaraConnector;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private CreditNoteService creditNoteService;
	@Autowired
	private CreditNoteStatusService creditNoteStatusService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FileBrowserService fileBrowserService;
	@Autowired
	private NominalCodeService nominalCodeService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private VatRateFormatter vatRateFormatter;
	@Autowired
	private UserService userService;
	@Value("${cwms.config.thirdpartyaccounts.name}")
	private String accountsSoftware;
	@Value("${cwms.config.thirdpartyaccounts.supported}")
	private String accountsSoftwareSupported;

	@RequestMapping(method = RequestMethod.GET, params = "submission=submit")
	protected String updateStatus(@RequestParam(value = "id") int id,
								  @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		CreditNote cn = this.creditNoteService.get(id);
		if (cn == null) {
			throw new Exception("Credit note with ID " + id + " cannot be found.");
		}
		Contact contact = this.userService.get(username).getCon();
		// get current status
		CreditNoteStatus currentStatus = this.creditNoteStatusService
			.findCreditNoteStatus(cn.getStatus().getStatusid());
		// get next activity and next status
		CreditNoteStatus nextActivity = currentStatus.getNext();
		CreditNoteStatus nextStatus = nextActivity.getNext();
		if (avalaraAware && nextActivity.isOnIssue()) {
			boolean verified = avalaraConnector.commitTransaction(cn);
			if (!verified)
				throw new Exception("Verifying transaction on Avatax fails");
		}
		// set new status and save
		cn.setStatus(nextStatus);
		if (nextActivity.isOnIssue()) {
			cn.setIssued(true);
			cn.setIssueby(contact);
			cn.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			// release the credit note for brokering into third party
			// accounts software
			cn.setAccountsStatus(AccountStatus.P);
		}
		// insert a new action record
		CreditNoteAction action = new CreditNoteAction();
		action.setContact(contact);
		action.setDate(new Date());
		action.setOutcomeStatus(nextStatus);
		action.setCreditNote(cn);
		action.setActivity(nextActivity);
		if (cn.getActions() == null) {
			cn.setActions(new TreeSet<>(new ActionComparator()));
		}
		cn.getActions().add(action);
		this.creditNoteService.merge(cn);
		return "redirect:/viewcreditnote.htm?id=" + cn.getId();
	}

	@RequestMapping(method = RequestMethod.GET)
	protected String displayForm(Model model, Locale locale, @RequestParam(value = "id") int id,
								 @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles) throws Exception {
		// retrieve previously sent e-mails from db
		CreditNote cn = this.creditNoteService.get(id);
		if (cn == null) {
			throw new Exception("Credit note with ID " + id + " cannot be found.");
		}
		cn.setSentEmails(this.emailService.getAllComponentEmails(Component.CREDIT_NOTE, cn.getId()));
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserService.getFilesForComponentRoot(Component.CREDIT_NOTE, cn.getId(), newFiles);
		model.addAttribute("creditNote", cn);
		model.addAttribute("privateOnlyNotes", NoteType.CREDITNOTENOTE.isPrivateOnly());
		model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.addAttribute(Constants.REFDATA_FILES_NAME, this.fileBrowserService.getFilesName(fileBrowserWrapper));
		model.addAttribute("accountssoftware", this.accountsSoftware);
		model.addAttribute(Constants.HTTPSESS_NEW_FILE_LIST, new ArrayList<String>());
		model.addAttribute("companySettings",
			this.companySettingsService.getByCompany(cn.getInvoice().getComp(), cn.getOrganisation()));
		model.addAttribute("accountssoftwaresupported", this.accountsSoftwareSupported);
		model.addAttribute("businessSubdivs", this.subdivService.getAllActiveCompanySubdivs(cn.getOrganisation()));
		model.addAttribute("nominals", this.nominalCodeService.getNominalCodes(Collections.singletonList(Ledgers.SALES_LEDGER)));
		// set up file-browsing on this page
		model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.CREDIT_NOTE));
		// prepare formatted description of vatrate using actual value from credit note
		model.addAttribute("formattedVatRate",
			this.vatRateFormatter.formatVatRate(cn.getVatRateEntity(), locale, cn.getVatRate()));

		return "trescal/core/pricing/creditnote/viewcreditnote";
	}
}