package org.trescal.cwms.core.pricing;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

/**
 * Enum defining different possible types of @link Pricings
 * <p>
 * Note, these are strings in database (invoice/job costing), do not rename enums unless confirmed not in use.
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum PricingType {
    CLIENT("pricingtype.normal"),                                // Regular costing with multiple job items
    SINGLE_JOB_ITEM("pricingtype.singlejobitem"),                // Replaces unused "WARRANTY" costing, costing with single job item only
    SITE("pricingtype.singlepriceforjob");                        // Note, a so-called "site" pricing in Antech is really a "Single Price" pricing type.

    private final String messageCode;
    private ReloadableResourceBundleMessageSource messages;

    @Component
    public static class PricingTypeMessageSourceInjector {
        @Autowired
        private ReloadableResourceBundleMessageSource messages;

        @PostConstruct
        public void postConstruct() {
            for (PricingType pt : EnumSet.allOf(PricingType.class))
                pt.setMessageSource(messages);
        }
    }

    PricingType(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessageCode() {
        return this.messageCode;
    }

    public String getLocalizedName() {
        return messages == null ? name() : messages.getMessage(messageCode, null, LocaleContextHolder.getLocale());
    }

    public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
        this.messages = messages;
    }
}