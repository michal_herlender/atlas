package org.trescal.cwms.core.pricing.invoice.form;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class CreateInternalInvoicesForm {

	private LocalDate begin;
	private LocalDate end;

	@DateTimeFormat(iso = ISO.DATE)
	public LocalDate getBegin() {
		return begin;
	}

	public void setBegin(LocalDate begin) {
		this.begin = begin;
	}

	@DateTimeFormat(iso = ISO.DATE)
	public LocalDate getEnd() {
		return end;
	}

	public void setEnd(LocalDate end) {
		this.end = end;
	}
}