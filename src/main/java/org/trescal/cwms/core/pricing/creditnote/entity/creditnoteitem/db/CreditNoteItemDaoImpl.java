package org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote_;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnoteitem.CreditNoteItem_;
import org.trescal.cwms.core.pricing.creditnote.projection.items.CreditNoteItemProjectionDTO;

@Repository("CreditNoteItemDao")
public class CreditNoteItemDaoImpl extends BaseDaoImpl<CreditNoteItem, Integer> implements CreditNoteItemDao {

	@Override
	protected Class<CreditNoteItem> getEntity() {
		return CreditNoteItem.class;
	}
	
	@Override
	public List<CreditNoteItemProjectionDTO> getCreditNoteItemProjectionDTOs(Integer creditNoteId) {
		return getResultList(cb -> {
			CriteriaQuery<CreditNoteItemProjectionDTO> cq = cb.createQuery(CreditNoteItemProjectionDTO.class);
			Root<CreditNoteItem> root = cq.from(CreditNoteItem.class);
			Join<CreditNoteItem, CreditNote> creditNote = root.join(CreditNoteItem_.creditNote, JoinType.INNER);
			Join<CreditNoteItem, NominalCode> nominal = root.join(CreditNoteItem_.nominal, JoinType.LEFT);
			
			cq.where(cb.equal(creditNote.get(CreditNote_.id), creditNoteId));
			cq.select(cb.construct(CreditNoteItemProjectionDTO.class,
				root.get(CreditNoteItem_.id),
				root.get(CreditNoteItem_.itemno),
				root.get(CreditNoteItem_.description),
				root.get(CreditNoteItem_.quantity),
				root.get(CreditNoteItem_.finalCost),
				root.get(CreditNoteItem_.taxable),
				root.get(CreditNoteItem_.taxAmount),
				nominal.get(NominalCode_.id),
				nominal.get(NominalCode_.costType)
			));
			cq.orderBy(cb.asc(root.get(CreditNoteItem_.id)));
			
			return cq;
		});
	}
}