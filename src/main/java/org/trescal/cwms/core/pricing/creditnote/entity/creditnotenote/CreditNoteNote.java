package org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote;

import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.ParentEntity;

import javax.persistence.*;

@Entity
@Table(name = "creditnotenote")
public class CreditNoteNote extends Note implements ParentEntity<CreditNote> {
    private CreditNote creditNote;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jobid")
    public CreditNote getCreditNote() {
        return this.creditNote;
    }

    @Override
    @Transient
    public NoteType getNoteType() {
        return NoteType.CREDITNOTENOTE;
    }

    public void setCreditNote(CreditNote creditNote) {
        this.creditNote = creditNote;
    }

    @Override
    public void setEntity(CreditNote creditNote) {
        setCreditNote(creditNote);
    }

    @Override
    @Transient
    public CreditNote getEntity() {
        return getCreditNote();
    }
}