package org.trescal.cwms.core.pricing.purchaseorder.form;

import java.util.List;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.pricing.purchaseorder.controller.CreatePurchaseOrderController;
import org.trescal.cwms.core.pricing.purchaseorder.controller.ViewPurchaseOrderController;
import org.trescal.cwms.core.pricing.purchaseorder.entity.enums.PurchaseOrderTaxableOption;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItemReceiptStatus;

/**
 * Form backing object used to back viewing/editing as well as creating
 * {@link PurchaseOrder} entities. See {@link ViewPurchaseOrderController} and
 * {@link CreatePurchaseOrderController}.
 */
@Builder
@Data
public class PurchaseOrderForm {

    // Purchase Order ID and Organisation ID are bound specifically for use in validators
    private Integer poid;
    private Integer orgid;
    // Section 1 - Purchase Order Edit
    private Integer businessPersonid;
    @Length(max = 100)
    private String clientref;
    private String currencyCode;
    @Length(max = 30)
    private String jobno;
    private Integer returnAddressId;
    // Used in cascading search plugin
    private Integer coid;
    private Integer personid;
    private Integer addrid;
    private Integer subdivid;
    // Section 2 - Goods Approval
    private List<PurchaseOrderItemReceiptStatus> goodsItemStatus;
    private List<Boolean> goodsApproval;
    private List<Boolean> goodsCancelled;
    // Section 2/3 - Shared between goods and accounts approval
    private List<String> goodsComment;
    // Section 3 - Accounts Approval
    private List<Boolean> accountsApproval;
    private List<Integer> supplierInvoiceIds;
    private PurchaseOrderTaxableOption taxableOption;
}