package org.trescal.cwms.core.pricing.entity.costs.base;

import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;

public interface WithJobCostingCalibrationCost {
    JobCostingCalibrationCost getJobCostingCalibrationCost();
}
