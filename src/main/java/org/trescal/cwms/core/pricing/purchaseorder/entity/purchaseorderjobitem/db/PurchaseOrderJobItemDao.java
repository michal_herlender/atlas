package org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.projection.PurchaseOrderItemProjectionDTO;

public interface PurchaseOrderJobItemDao extends BaseDao<PurchaseOrderJobItem, Integer> {
	
	List<PurchaseOrderItemProjectionDTO> getProjectionDTOsForJobItems(Collection<Integer> jobItemIds);
}