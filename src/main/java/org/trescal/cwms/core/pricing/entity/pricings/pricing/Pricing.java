package org.trescal.cwms.core.pricing.entity.pricings.pricing;

import lombok.Setter;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity;
import org.trescal.cwms.core.tools.MathTools;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * SuperClass that should be extended by all entities that have sets of
 * PricingItems attached to them and that are specifically for one Contact at a
 * client or third party and that explicity require aggregation of the attached
 * costs.
 */
@MappedSuperclass
@Setter
public abstract class Pricing<OrgLevel extends OrganisationLevel> extends GenericPricingEntity<OrgLevel> {

	/**
	 * The Contact that 'owns' this Pricing entity. The Contact who this pricing
	 * entity is created for and we are requesting a service from.
	 */
	protected Contact contact;

	/**
	 * The final cost of this {@link Pricing} which is made up of the totalCost and
	 * then any other costs that can be applied (such as vat or carriage).
	 */
	protected BigDecimal finalCost;

	/**
	 * Aggregate cost of the {@link PricingItem}s on this {@link Pricing}.
	 */
	protected BigDecimal totalCost;

	private BigDecimal vatRate;

	private BigDecimal vatValue;

	/**
	 * Convenience function for calculating the total cost of this {@link Pricing}
	 * in the base currency.
	 */
	@Override
	@Transient
	public BigDecimal getBaseFinalCost() {
		if (this.getRate().doubleValue() == 0.0) {
			return this.finalCost;
		} else {
			return this.finalCost.divide(this.getRate(), MathTools.mc);
		}
	}

	@Transient
	@Override
	public String getBaseFinalCostFormatted() {
		return this.getBaseFinalCost() == null ? null : MathTools.getDecimalFormat().format(this.getBaseFinalCost());
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable = false)
	public Contact getContact() {
		return this.contact;
	}

	@Column(name = "finalcost", nullable = false, precision = 10, scale = 2)
	public BigDecimal getFinalCost() {
		return this.finalCost;
	}

	@Column(name = "totalcost", nullable = false, precision = 10, scale = 2)
	public BigDecimal getTotalCost() {
		return this.totalCost;
	}

	@Column(name = "vatrate", precision = 10, scale = 2)
	public BigDecimal getVatRate() {
		return this.vatRate;
	}

	@Column(name = "vatvalue", precision = 10, scale = 2)
	public BigDecimal getVatValue() {
		return this.vatValue;
	}
}