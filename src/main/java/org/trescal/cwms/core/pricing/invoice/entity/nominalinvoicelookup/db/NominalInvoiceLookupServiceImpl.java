package org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.db;

import java.util.List;

import org.trescal.cwms.core.pricing.invoice.entity.nominalinvoicelookup.NominalInvoiceLookup;

public class NominalInvoiceLookupServiceImpl implements NominalInvoiceLookupService
{
	NominalInvoiceLookupDao nominalInvoiceLookupDao;

	public void deleteNominalInvoiceLookup(NominalInvoiceLookup nominalinvoicelookup)
	{
		this.nominalInvoiceLookupDao.remove(nominalinvoicelookup);
	}

	public NominalInvoiceLookup findNominalInvoiceLookup(int id)
	{
		return this.nominalInvoiceLookupDao.find(id);
	}

	@Override
	public NominalInvoiceLookup findNominalInvoiceLookup(String nominalType)
	{
		return this.nominalInvoiceLookupDao.findNominalInvoiceLookup(nominalType);
	}

	public List<NominalInvoiceLookup> getAllNominalInvoiceLookups()
	{
		return this.nominalInvoiceLookupDao.findAll();
	}

	public void insertNominalInvoiceLookup(NominalInvoiceLookup NominalInvoiceLookup)
	{
		this.nominalInvoiceLookupDao.persist(NominalInvoiceLookup);
	}

	public void saveOrUpdateNominalInvoiceLookup(NominalInvoiceLookup nominalinvoicelookup)
	{
		this.nominalInvoiceLookupDao.saveOrUpdate(nominalinvoicelookup);
	}

	public void setNominalInvoiceLookupDao(NominalInvoiceLookupDao nominalInvoiceLookupDao)
	{
		this.nominalInvoiceLookupDao = nominalInvoiceLookupDao;
	}

	public void updateNominalInvoiceLookup(NominalInvoiceLookup NominalInvoiceLookup)
	{
		this.nominalInvoiceLookupDao.update(NominalInvoiceLookup);
	}
}