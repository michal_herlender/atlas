/**
 * 
 */
package org.trescal.cwms.core.pricing.entity.costs.base;

import java.math.BigDecimal;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "costs_adjustment")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "costtype", discriminatorType = DiscriminatorType.STRING)
public abstract class AdjCost extends Cost {
	
	private BigDecimal fixedPrice;
	
	public BigDecimal getFixedPrice() {
		return fixedPrice;
	}
	
	public void setFixedPrice(BigDecimal fixedPrice) {
		this.fixedPrice = fixedPrice;
	}
}