package org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.db;

import java.util.List;

import org.trescal.cwms.core.pricing.invoice.entity.invoicestatus.InvoiceStatus;

public interface InvoiceStatusService {
	
	void delete(InvoiceStatus invoicestatus);
	
	List<InvoiceStatus> findAllInvoiceStatusRequiringAttention();
	
	InvoiceStatus findOnInsertActivity();
}