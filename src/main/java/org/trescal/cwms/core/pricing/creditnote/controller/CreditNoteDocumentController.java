package org.trescal.cwms.core.pricing.creditnote.controller;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.CreditNoteFileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.template.Template;
import org.trescal.cwms.core.system.entity.template.db.TemplateService;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class CreditNoteDocumentController {
	
	@Autowired
	private BirtEngine birtEngine;
	@Autowired
	private CreditNoteService cnServ;
	@Autowired
	private DocumentService documentService; 
	@Autowired
	private UserService userService;
	@Autowired
	private TemplateService templateService;
	
	/**
	 * Targeted for removal once new credit note document accepted
	 */
	@RequestMapping(value="/creditnotedocs.htm", method=RequestMethod.GET)
	public String createCreditNoteDoc(
			@RequestParam(value="id", required=false, defaultValue="0") Integer cnId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception
	{
		Map<String, Object> model = new HashMap<String, Object>();
		CreditNote cn = this.cnServ.get(cnId);
		Contact userContact = this.userService.get(username).getCon();
		model.put("creditnoteId", cnId.toString());
		model.put("createdBy", userContact.getPersonid());
		/* business subdiv */
		model.put("subdivId", subdivDto.getKey());
		/* current locale */
		model.put("reportLocale", LocaleContextHolder.getLocale().toString());
		Template template = templateService.findByName("Credit note RTF");
		String docPath = (new CreditNoteFileNamingService(cn)).getFileName();
		FileOutputStream fileOutput = new FileOutputStream(docPath + ".pdf");
		birtEngine.generatePDF(template.getTemplatePath(), model, fileOutput, LocaleContextHolder.getLocale());
		sessionNewFileList.add(docPath);
		return "redirect:viewcreditnote.htm?id=" + cn.getId() + "&loadtab=cndocs-tab";
	}
	
	@RequestMapping(value="/birtcreditnote.htm", method=RequestMethod.GET)
	public String handleBirtRequest(Model model,
			@RequestParam(value="id", required=true) Integer creditNoteId,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) {
		
		CreditNote cn = this.cnServ.get(creditNoteId);
		CreditNoteFileNamingService fnService = new CreditNoteFileNamingService(cn);
		Map<String,Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, !cn.isIssued());
		
		Locale documentLocale = LocaleContextHolder.getLocale();
		Document doc = documentService.createBirtDocument(creditNoteId, BaseDocumentType.CREDIT_NOTE, documentLocale, Component.CREDIT_NOTE, fnService, additionalParameters);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		// Prevent session attributes in redirect URL
		model.asMap().clear();
		return "redirect:viewcreditnote.htm?id=" + creditNoteId + "&loadtab=cndocs-tab";
	}
	
}