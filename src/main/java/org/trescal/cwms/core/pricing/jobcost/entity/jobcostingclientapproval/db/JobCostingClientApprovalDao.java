package org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingclientapproval.JobCostingClientApproval;

public interface JobCostingClientApprovalDao extends BaseDao<JobCostingClientApproval, Integer>
{
	
}