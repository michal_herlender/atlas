package org.trescal.cwms.core.pricing.lookup.dto;

/*
 * These are in natural order of "least preferred" to "most preferred"
 */
public enum PriceLookupMatchSource {
	CATALOG_PRICE,
	QUOTATION,
	PREFERRED_QUOTATION;
}
