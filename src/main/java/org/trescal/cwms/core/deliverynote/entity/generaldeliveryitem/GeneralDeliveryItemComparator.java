package org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem;

import java.util.Comparator;

/**
 * Inner comparator class for sorting GeneralDeliveryItems when returning
 * collections of deliveries, each with multiple items
 */
public class GeneralDeliveryItemComparator implements Comparator<GeneralDeliveryItem>
{
	public int compare(GeneralDeliveryItem delItem, GeneralDeliveryItem anotherDelItem)
	{
		Integer id1 = (delItem).getDelitemid();
		Integer id2 = (anotherDelItem).getDelitemid();

		return id1.compareTo(id2);
	}
}