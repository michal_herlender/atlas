package org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.db;

import org.trescal.cwms.core.admin.form.AddTransportOptionForm;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;

public interface TransportOptionScheduleRuleService extends BaseService<TransportOptionScheduleRule, Integer> {

	void updateTransportOptionScheduleRule(TransportOptionScheduleRule transportOptionScheduleRule);

	void insertTransportOptionScheduleRule(TransportOptionScheduleRule transportOptionScheduleRule);
	
	void addTransportOptionScheduleRule(AddTransportOptionForm form, TransportOption transportOption);
}
