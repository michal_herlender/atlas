package org.trescal.cwms.core.deliverynote.entity.generaldelivery;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;

import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItemComparator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

@Entity
@DiscriminatorValue("general")
public class GeneralDelivery extends Delivery
{
	private String clientref;
	private Set<GeneralDeliveryItem> items;
	private String ourref;

	public GeneralDelivery()
	{
		super();
	}

	@Column(name = "clientref", nullable = true, length = 100)
	public String getClientref()
	{
		return this.clientref;
	}

	/**
	 * @return the items
	 */
	@Override
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "delivery", fetch = FetchType.LAZY)
	@SortComparator(GeneralDeliveryItemComparator.class)
	public Set<GeneralDeliveryItem> getItems()
	{
		return this.items;
	}

	@NotNull
	@Column(name = "ourref", nullable = true, length = 100)
	public String getOurref()
	{
		return this.ourref;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity()
	{
		return null;
	}
	
	@Override
	@Transient
	public DeliveryType getType() {
		return DeliveryType.GENERAL;
	}
	
	@Override
	@Transient
	public boolean isGeneralDelivery()
	{
		return true;
	}

	public void setClientref(String clientref)
	{
		this.clientref = clientref;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Set<GeneralDeliveryItem> items)
	{
		this.items = items;
	}

	public void setOurref(String ourref)
	{
		this.ourref = ourref;
	}
}