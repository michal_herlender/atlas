package org.trescal.cwms.core.deliverynote.form;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CourierDespatchSearchForm {
	private int companyid;
	private String consignmentno;
	private Set<CompanyRole> coroles;
	private int courierid;
	private List<Courier> couriers;
	private int couriertypeid;
	private LocalDate date;
	private Integer subdivid;

	// results data
	private PagedResultSet<CourierDespatchDTO> rs;
	private int pageNo;
	private int resultsPerPage;

}