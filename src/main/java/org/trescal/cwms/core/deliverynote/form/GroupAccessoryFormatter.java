package org.trescal.cwms.core.deliverynote.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

import java.util.Locale;
import java.util.Set;

@Component
public class GroupAccessoryFormatter {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private TranslationService translationService; 
	
	public String getAccessoryText(Job job, Locale locale) {
		StringBuilder result = new StringBuilder();
		if (!job.getGroups().isEmpty()) {
			result.append(messageSource.getMessage("createdelnote.groupaccessories", null, "Group Accessories", locale));
			result.append(":\r\n");
			for (JobItemGroup jobItemGroup : job.getGroups()) {
				boolean onFirst = true;
				for (GroupAccessory accessory : jobItemGroup.getAccessories()) {
					if (!onFirst) result.append(", ");
					Set<Translation> accessoryTranslations = accessory.getAccessory().getTranslations();
					String translation = this.translationService.getCorrectTranslation(accessoryTranslations, locale);
					result.append(translation);
					result.append(" x").append(accessory.getQuantity());
					onFirst = false;
				}
				if(jobItemGroup.getAccessoryFreeText() != null) {
					if (!onFirst) result.append(", ");
					result.append(jobItemGroup.getAccessoryFreeText());
				}
				result.append("\r\n");
			}
		}
		return result.toString();
	}
}