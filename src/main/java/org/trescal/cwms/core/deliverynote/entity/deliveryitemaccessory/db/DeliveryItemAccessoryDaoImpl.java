package org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem_;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory_;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemAccessoryDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository("DeliveryItemAccessoryDao")
public class DeliveryItemAccessoryDaoImpl extends BaseDaoImpl<DeliveryItemAccessory, Integer> implements DeliveryItemAccessoryDao
{
	@Override
	protected Class<DeliveryItemAccessory> getEntity() {
		return DeliveryItemAccessory.class;
	}
	
	@Override
	public List<DeliveryItemAccessory> getAll(DeliveryItem deliveryItem) {
		return getResultList(cb ->{
			CriteriaQuery<DeliveryItemAccessory> cq = cb.createQuery(DeliveryItemAccessory.class);
			Root<DeliveryItemAccessory> root = cq.from(DeliveryItemAccessory.class);
			cq.where(cb.equal(root.get(DeliveryItemAccessory_.deliveryItem), deliveryItem));
			return cq;
		});
	}
	
	public List<DeliveryItemAccessoryDTO> getProjectionDTOsForDeliveryId(Integer deliveryId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<DeliveryItemAccessoryDTO> cq = cb.createQuery(DeliveryItemAccessoryDTO.class);
			
			Root<DeliveryItemAccessory> root = cq.from(DeliveryItemAccessory.class);
			Join<DeliveryItemAccessory, Accessory> accessory = root.join(DeliveryItemAccessory_.accessory, JoinType.INNER);
			Join<Accessory, Translation> translations = accessory.join(Accessory_.translations, JoinType.LEFT);
			translations.on(cb.equal(translations.get(Translation_.locale), locale));
			Join<DeliveryItemAccessory, DeliveryItem> deliveryItem = root.join(DeliveryItemAccessory_.deliveryItem, JoinType.INNER);
			Join<DeliveryItem, Delivery> delivery = deliveryItem.join(DeliveryItem_.delivery, JoinType.INNER);
			
			cq.where(cb.equal(delivery.get(Delivery_.deliveryid), deliveryId));
			
			// Integer accessoryId, Integer deliveryItemId, Integer quantity, String description, String descriptionTranslation
			
			cq.select(cb.construct(DeliveryItemAccessoryDTO.class, 
					root.get(DeliveryItemAccessory_.id), deliveryItem.get(DeliveryItem_.delitemid),
					root.get(DeliveryItemAccessory_.qty), accessory.get(Accessory_.desc),
					translations.get(Translation_.translation)));
			return cq;
		});
	}
}