package org.trescal.cwms.core.deliverynote.entity.courierdespatch.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchDTO;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.form.CourierDespatchSearchForm;
import org.trescal.cwms.core.deliverynote.projection.CourierDespatchProjectionDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface CourierDespatchDao extends BaseDao<CourierDespatch, Integer> {
	
	List<Delivery> getDeliveriesForCourierDespatch(int cdid);
	
	List<CourierDespatch> getDespatchesCreatedInLastWeek(Integer allocatedSubdivId);
	
	List<CourierDespatchProjectionDTO> getProjectionDTOs(Collection<Integer> despatchIds);
	
	PagedResultSet<CourierDespatchDTO> queryDespatches(CourierDespatchSearchForm form,
			PagedResultSet<CourierDespatchDTO> rs);
}