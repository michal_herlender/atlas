package org.trescal.cwms.core.deliverynote.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.schedule.dto.ScheduleResultDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.schedule.form.CreateScheduleForm;
import org.trescal.cwms.core.schedule.form.CreateScheduleFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class AssignScheduleController {

	@Autowired
	private DeliveryService delServ;
	@Autowired
	private ScheduleService schServ;
	@Autowired
	private CreateScheduleFormValidator validator;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private TranslationService translationService;

	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	protected CreateScheduleForm formBackingObject(
			@RequestParam(value = "delid", required = false, defaultValue = "0") Integer delId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Delivery delivery = this.delServ.get(delId);
		CreateScheduleForm form = new CreateScheduleForm();
		form.setBusinessSubdivid(subdivDto.getKey());
		if (delivery.getAddress() != null) {
			form.setValidSchedules(this.schServ.getSchedulesForAddress(delivery.getAddress().getAddrid(), false, null));
		}
		form.setBusinessSubdivid(delivery.getOrganisation().getId());
		form.setDelivery(delivery);
		form.setSource(ScheduleSource.COMPANY);
		form.setScheduleTypes(Arrays.asList(ScheduleType.values()));
		form.setScheduleStatuses(Arrays.asList(ScheduleStatus.values()));
		return form;
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/assignschedule.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, @ModelAttribute(FORM_NAME) CreateScheduleForm form, Locale locale,
			@RequestParam(value = "newschedTab", required = false, defaultValue = "false") Boolean newschedTab,
			@RequestParam(value = "scheduleExistsId", required = false) String scheduleExistsId) {

		Integer deliverySubdivId = form.getDelivery().getOrganisation().getId();
		Integer deliveryAddressId = form.getDelivery().getAddress().getAddrid();
		if(!form.getValidSchedules().isEmpty()){
			for(Schedule schedule : form.getValidSchedules()){
				List<ScheduledDelivery> scheduledDeliveryList = this.scheduledDeliveryService.scheduledDeliveryFromScheduleAndOrDelivery(schedule.getScheduleId(), form.getDelivery().getDeliveryid());
				if(!scheduledDeliveryList.isEmpty()){
					form.setAlreadyAssigned(true);
					form.setSchId(schedule.getScheduleId());
					break;
				}
			}
		}
		List<TransportOption> transportOptionsSchedulable = this.transportOptionService.getTransportOptionsSchedulable(deliverySubdivId);
		ScheduleResultDTO dto = new ScheduleResultDTO();
		TransportOption transportOptionOut = transportOptionService
				.getTransportOptionOut(addressService.get(deliveryAddressId), subdivService.get(deliverySubdivId));
		TransportOption transportOptionIn = transportOptionService
				.getTransportOptionIn(addressService.get(deliveryAddressId), subdivService.get(deliverySubdivId));
		if (transportOptionOut != null) {
			dto.setTransportOptionOutId(transportOptionOut.getId());
			dto.setTransportOptionOutName(
					!StringUtils.isEmpty(transportOptionOut.getLocalizedName()) ? transportOptionOut.getLocalizedName()
							: translationService.getCorrectTranslation(
									transportOptionOut.getMethod().getMethodTranslation(), locale));
		}

		if (transportOptionIn != null) {
			dto.setTransportOptionInId(transportOptionIn.getId());
			dto.setTransportOptionInName(
					!StringUtils.isEmpty(transportOptionIn.getLocalizedName()) ? transportOptionIn.getLocalizedName()
							: translationService.getCorrectTranslation(
									transportOptionIn.getMethod().getMethodTranslation(), locale));
		}
		
		model.addAttribute("scheduleDto", dto);
		model.addAttribute("newschedtab", newschedTab);
		model.addAttribute("scheduleExists", scheduleExistsId);
		model.addAttribute("transportOptionsSchedulable", transportOptionsSchedulable);
		if (transportOptionsSchedulable.isEmpty()) {
			model.addAttribute("noActiveDOW", true);
		}
		return "trescal/core/deliverynote/assignschedule";
	}

	@RequestMapping(value = "/assignschedule.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(Model model, Locale locale,
			@RequestParam(value = "assignoption", required = true) String assignoption,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(FORM_NAME) @Validated CreateScheduleForm form, BindingResult bindingResult)
			throws Exception {

		Delivery delivery = form.getDelivery();
		
		// If create new -
		if (assignoption.equals("new")) {
			// validate the create schedule form
			if (bindingResult.hasErrors()) {
				// get the id of schedule already created with the same
				// address, the same transport option and the same date.
				if(bindingResult.getFieldError().getField().equals("schedulesCreated")){
					return new ModelAndView(referenceData(model, form, locale, true, bindingResult.getFieldError().getArguments()[0].toString()));
				}
				return new ModelAndView(referenceData(model, form, locale, true, ""));
			}
			// create new schedule and add delivery to schedule created
			Schedule schedule = this.schServ.createSchedule(form, username);
			this.scheduledDeliveryService.addScheduledDelivery(delivery, schedule);

			return new ModelAndView(
					new RedirectView("viewdelnote.htm?delid=" + delivery.getDeliveryid() + "&loadtab=despatch-tab"));
		}
		// else add delivery note to existing schedule
		else {
			Schedule schedule = this.schServ.get(form.getSchId());
			this.scheduledDeliveryService.addScheduledDelivery(delivery, schedule);
			return new ModelAndView(
					new RedirectView("viewdelnote.htm?delid=" + delivery.getDeliveryid() + "&loadtab=despatch-tab"));
			}
			
		}
}
