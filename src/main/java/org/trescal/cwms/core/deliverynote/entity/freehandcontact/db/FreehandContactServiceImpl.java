package org.trescal.cwms.core.deliverynote.entity.freehandcontact.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;

@Service("FreehandContactService")
public class FreehandContactServiceImpl implements FreehandContactService
{
	@Autowired
	FreehandContactDao freehandContactDao;

	public FreehandContact findFreehandContact(int id)
	{
		return freehandContactDao.find(id);
	}

	public void insertFreehandContact(FreehandContact FreehandContact)
	{
		freehandContactDao.persist(FreehandContact);
	}

	public void updateFreehandContact(FreehandContact FreehandContact)
	{
		freehandContactDao.update(FreehandContact);
	}

	public List<FreehandContact> getAllFreehandContacts()
	{
		return freehandContactDao.findAll();
	}

	public void setFreehandContactDao(FreehandContactDao freehandContactDao)
	{
		this.freehandContactDao = freehandContactDao;
	}

	public void deleteFreehandContact(FreehandContact freehandcontact)
	{
		this.freehandContactDao.remove(freehandcontact);
	}

	public void saveOrUpdateFreehandContact(FreehandContact freehandcontact)
	{
		this.freehandContactDao.saveOrUpdate(freehandcontact);
	}
}