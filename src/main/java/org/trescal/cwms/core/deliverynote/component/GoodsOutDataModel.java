package org.trescal.cwms.core.deliverynote.component;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.company.dto.CompanySettingsDTO;
import org.trescal.cwms.core.deliverynote.dto.DespatchItemDTO;
import org.trescal.cwms.core.deliverynote.dto.DespatchItemDTOComparator;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

/**
 * Type safe class to manage reference data for goods out
 * <p>
 * This is intended to be used for one business subdivision's data where there may be multiple
 * goods out addresses, for client deliveries (e.g. using transport out options)
 *
 * @author Galen 2019-02-15
 */
public class GoodsOutDataModel {
	/**
	 * Sorted set of all source addresses (business addresses for logged in subdiv)
	 */
	private final Set<KeyValue<Integer, String>> sourceAddressSet;
	/**
	 * Sorted set of all active transport options for the business subdiv that the model is generated for
	 */
	private final Set<KeyValueIntegerString> subdivTransportOptionSet;
	/**
	 * Set of all active transport option IDs for the business subdiv that the model is generated for
	 */
	private final Set<Integer> subdivTransportOptionIds;
	/**
	 * Sorted set of transport options in result data (may be different set than subdiv's!)
	 */
	private final Set<KeyValueIntegerString> resultTransportOptionSet;
	/**
	 * Map of state groups to ItemState IDs contained within each group
	 */
	private final Map<StateGroup, Set<Integer>> stateGroupMap;
	/**
	 * Map indicating whether a company is active/inactive and whether on stop
	 */
	private final Map<Integer, CompanySettingsDTO> companySettingsMap;
	/**
	 * Map of delivery IDs to Set of delivery data
	 */
	private final Map<Integer, DeliveryDTO> deliveryMap;
	/**
	 * Map of job item IDs to Set of delivery item data
	 */
	private final Map<Integer, Set<DeliveryItemDTO>> deliveryItemMap;
	/**
	 * Map of all destination address IDs to address DTOs
	 */
	private final Map<Integer, KeyValue<Integer, String>> destAddressLookupMap;
	/**
	 * Map of all source address IDs to Map of:
	 * transport out option IDs to Map of:
	 * destination company KeyValue to Map of:
	 * destination subdiv KeyValue to sorted Set of address KeyValues requiring shipment from each source address
	 */
	private final Map<Integer, Map<Integer, Map<KeyValue<Integer, String>, Map<KeyValue<Integer, String>, Set<KeyValue<Integer, String>>>>>> destAddressMap;
	/**
	 * Map of all source address IDs to Map of:
	 * transport out option IDs to Map of:
	 * destination address IDs to Map of:
	 * Job KeyValue to sorted Set of despatchItemDtos
	 */
	private final Map<Integer, Map<Integer, Map<Integer, Map<KeyValue<Integer, String>, Set<DespatchItemDTO>>>>> despatchItemMap;
	private final LocalDate today;

	private static final Logger logger = LoggerFactory.getLogger(GoodsOutDataModel.class);

	/**
	 * Initializes empty Maps/Sets which are later populated with "add" methods below
	 */
	public GoodsOutDataModel() {
		this.sourceAddressSet = new TreeSet<>();
		this.subdivTransportOptionSet = new TreeSet<>();
		this.subdivTransportOptionIds = new HashSet<>();
		this.resultTransportOptionSet = new TreeSet<>();
		this.stateGroupMap = new HashMap<>();
		this.companySettingsMap = new HashMap<>();
		this.deliveryMap = new HashMap<>();
		this.deliveryItemMap = new HashMap<>();
		this.destAddressLookupMap = new HashMap<>();
		this.destAddressMap = new HashMap<>();
		this.despatchItemMap = new HashMap<>();
		this.today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
	}

	/**
	 * Initializes source addresses (normally for one business subdivision)
	 */
	public void addSourceAddressDtos(List<KeyValue<Integer, String>> sourceAddressDtos) {
		this.sourceAddressSet.addAll(sourceAddressDtos);
	}

	/**
	 * Initializes all transport options active for the business subdivision.
	 * These are intended to appear in a selection menu.
	 * 
	 * Not all may have pending job items to ship.
	 * 
	 * Note there may potentially also be other "incorrect" transport options in the
	 * data model, these are initialized separately so that they can show as errors.
	 */
	public void addSubdivTransportOptions(Collection<KeyValueIntegerString> transportOptionDtos) {
		this.subdivTransportOptionSet.addAll(transportOptionDtos);
		transportOptionDtos.forEach(dto -> this.subdivTransportOptionIds.add(dto.getKey()));
		
	}

	/**
	 * List of all transport options contained in the result data
	 */
	public void addResultTransportOptions(Collection<KeyValueIntegerString> transportOptionDtos) {
		this.resultTransportOptionSet.addAll(transportOptionDtos);
	}
	
	/**
	 * Initializes destination addresses
	 */
	public void addDestAddressDtos(List<KeyValue<Integer, String>> destAddressDtos) {
		destAddressDtos
			.forEach(dto -> this.destAddressLookupMap.put(dto.getKey(), dto));
	}
	
	/**
	 * Initializes map of state groups to Set of ItemState ids mapped to each state group
	 * 
	 * The item state / stategrouplinks should be pre-initialized by query
	 * (may need to do by querying StateGroupLink) 
	 */
	public void addStateGroups(List<StateGroup> stateGroups, List<ItemState> itemStates) {
		stateGroups.forEach(sg -> this.stateGroupMap.put(sg, new HashSet<>()));
		itemStates.
			forEach(itemState -> itemState.getGroupLinks().stream().
				filter(sgl -> stateGroups.contains(sgl.getGroup())).
				forEach(sgl -> this.stateGroupMap.get(sgl.getGroup()).add(sgl.getState().getStateid())));
	}
	
	public void addCompanySettingsData(List<CompanySettingsDTO> companySettingsList) {
		companySettingsList.forEach(dto -> this.companySettingsMap.put(dto.getCompanyId(), dto));
	}
	
	public void addDeliveryItems(List<DeliveryItemDTO> deliveryItems, List<DeliveryDTO> deliveries) {
		deliveries.forEach(dto -> deliveryMap.put(dto.getDeliveryId(), dto));
		for (DeliveryItemDTO itemDto : deliveryItems) {
			if (!deliveryItemMap.containsKey(itemDto.getJobItemId())) {
				deliveryItemMap.put(itemDto.getJobItemId(), new HashSet<>());
			}
			deliveryItemMap.get(itemDto.getJobItemId()).add(itemDto);
		}
	}

	/**
	 * Initializes delivery data maps.
	 * 
	 * The source and destination addresses should have already been initialized, 
	 * with all source and destination addresses contained in the list of dtos
	 * 
	 * This should be called after all other add() initialization code 
	 */
	public void addDespatchItemDtos(List<DespatchItemDTO> despatchItemDtos) {
		for (DespatchItemDTO itemDto : despatchItemDtos) {
			Integer sourceAddressId = itemDto.getSourceAddressId();
			Integer destAddressId = itemDto.getDestAddressId();
			Integer transportOptionId = itemDto.getTransportOutOptionId();
			
			KeyValue<Integer, String> destCompanyDto = 
					new KeyValue<>(itemDto.getDestCompanyId(), itemDto.getDestCompanyName()); 
			KeyValue<Integer, String> destSubdivDto = 
					new KeyValue<>(itemDto.getDestSubdivId(), itemDto.getDestSubdivName()); 
			KeyValue<Integer, String> destAddressDto = 
					getDestAddressDto(destAddressId);
			
			addToAddressDtoMap(sourceAddressId, transportOptionId, destCompanyDto, destSubdivDto, destAddressDto);
			addToDespatchItemMap(sourceAddressId, transportOptionId, destAddressId, itemDto);
		}
	}
	
	/*
	 * Here the maps/sets are "specific" to the source address and 
	 * level of destination hierarchy retrieved
	 */
	private void addToAddressDtoMap(Integer sourceAddressId, Integer transportOptionId, KeyValue<Integer,String> destCompanyDto, 
			KeyValue<Integer,String> destSubdivDto, KeyValue<Integer,String> destAddressDto) {
		
		if (!this.destAddressMap.containsKey(sourceAddressId)) 
			this.destAddressMap.put(sourceAddressId, new HashMap<>());
		Map<Integer, Map<KeyValue<Integer, String>, Map<KeyValue<Integer, String>, Set<KeyValue<Integer, String>>>>> specificTransportOptionDtoMap = 
				this.destAddressMap.get(sourceAddressId);
				
		if (!specificTransportOptionDtoMap.containsKey(transportOptionId))
			specificTransportOptionDtoMap.put(transportOptionId, new TreeMap<>());
		Map<KeyValue<Integer, String>, Map<KeyValue<Integer, String>, Set<KeyValue<Integer, String>>>> specificCompanyDtoMap = 
				specificTransportOptionDtoMap.get(transportOptionId);
		
		if (!specificCompanyDtoMap.containsKey(destCompanyDto))
			specificCompanyDtoMap.put(destCompanyDto, new TreeMap<>());
		Map<KeyValue<Integer, String>, Set<KeyValue<Integer, String>>> specificSubdivDtoMap = 
				specificCompanyDtoMap.get(destCompanyDto);
		
		if (!specificSubdivDtoMap.containsKey(destSubdivDto))
			specificSubdivDtoMap.put(destSubdivDto, new TreeSet<>());
		Set<KeyValue<Integer, String>> specificAddressDtoSet = 
				specificSubdivDtoMap.get(destSubdivDto);

		specificAddressDtoSet.add(destAddressDto);
	}
	
	private void addToDespatchItemMap(Integer sourceAddressId, Integer transportOptionId, Integer destAddressId, DespatchItemDTO itemDto) {
		if (!this.despatchItemMap.containsKey(sourceAddressId))
			this.despatchItemMap.put(sourceAddressId, new HashMap<>());
		Map<Integer, Map<Integer, Map<KeyValue<Integer, String>, Set<DespatchItemDTO>>>> transportOptionMap = 
				this.despatchItemMap.get(sourceAddressId);
		
		if (!transportOptionMap.containsKey(transportOptionId))
			transportOptionMap.put(transportOptionId, new HashMap<>());
		Map<Integer, Map<KeyValue<Integer, String>, Set<DespatchItemDTO>>> destAddressMap = 
				transportOptionMap.get(transportOptionId);
		
		if (!destAddressMap.containsKey(destAddressId))
			destAddressMap.put(destAddressId, new TreeMap<>());
		Map<KeyValue<Integer, String>, Set<DespatchItemDTO>> jobMap =
				destAddressMap.get(destAddressId);
		
		KeyValue<Integer, String> jobDto = new KeyValue<>(itemDto.getJobId(), itemDto.getJobNumber());
		if (!jobMap.containsKey(jobDto))
			jobMap.put(jobDto, new TreeSet<>(new DespatchItemDTOComparator()));
		Set<DespatchItemDTO> itemSet = jobMap.get(jobDto);
		itemSet.add(itemDto);
	}

	public Set<KeyValue<Integer, String>> getSourceAddressSet() {
		return sourceAddressSet;
	}

	public Set<KeyValueIntegerString> getSubdivTransportOptionSet() {
		return subdivTransportOptionSet;
	}

	public KeyValue<Integer, String> getDestAddressDto(Integer addressId) {
		return destAddressLookupMap.get(addressId);
	}
	
	public Integer getItemCount(Integer sourceAddressId) {
		int result = 0;
		for (Integer transportOptionId : this.despatchItemMap.keySet()) {
			result += getItemCount(sourceAddressId, transportOptionId);
		}
		return result;
	}
	
	public Integer getItemCount(Integer sourceAddressId, Integer transportOptionId) {
		int result = 0;
		//
		Map<Integer, Map<Integer, Map<KeyValue<Integer, String>, Set<DespatchItemDTO>>>> transportOptionMap = 
				this.despatchItemMap.get(sourceAddressId);
		if (transportOptionMap != null) {
			Map<Integer, Map<KeyValue<Integer, String>, Set<DespatchItemDTO>>> destAddressMap = 
					transportOptionMap.get(transportOptionId);
			if (destAddressMap != null) {
				// destAddressMap.values() are individually Map<KeyValue, Set<DespatchItemDTO>> for each job
				result += destAddressMap.values().stream().
					mapToInt(jobMap -> jobMap.values().stream().
							mapToInt(Set::size).sum()).sum();
			}
		}
		return result;
	}
	
	public Integer getItemCount(Integer sourceAddressId, Integer transportOptionId, KeyValue<Integer, String> companyDto) {
		int result = 0;
		for (KeyValue<Integer, String> subdivDto : getDestSubdivDtos(sourceAddressId, transportOptionId, companyDto)) {
			result += getItemCount(sourceAddressId, transportOptionId, companyDto, subdivDto);
		}
		return result;
	}
	
	public Integer getItemCount(Integer sourceAddressId, Integer transportOptionId, KeyValue<Integer, String> companyDto, KeyValue<Integer, String> subdivDto) {
		int result = 0;
		for (KeyValue<Integer, String> destAddressDto : getDestAddressDtos(sourceAddressId, transportOptionId, companyDto, subdivDto)) {
			result += getItemCount(sourceAddressId, transportOptionId, destAddressDto.getKey());
		}
		return result;
	}
	
	public Integer getItemCount(Integer sourceAddressId, Integer transportOptionId, Integer destAddressId) {
		int result = 0;
		for (KeyValue<Integer, String> jobDto : getJobDtos(sourceAddressId, transportOptionId, destAddressId)) {
			result += getItemCount(sourceAddressId, transportOptionId, destAddressId, jobDto); 
		}
		return result;
	}
	
	public Integer getItemCount(Integer sourceAddressId, Integer transportOptionId, Integer destAddressId, KeyValue<Integer,String> jobDto) {
		return getDespatchItems(sourceAddressId, transportOptionId, destAddressId, jobDto).size();
	}
	
	private Map<Integer, Map<KeyValue<Integer, String>, Map<KeyValue<Integer, String>, Set<KeyValue<Integer, String>>>>> getTransportOptionMap(
			Integer sourceAddressId) {
		return this.destAddressMap
				.getOrDefault(sourceAddressId, Collections.emptyMap());
	}
	
	private Map<KeyValue<Integer, String>, Map<KeyValue<Integer, String>, Set<KeyValue<Integer, String>>>> getCompanyMap(
			Integer sourceAddressId, Integer transportOptionId) {
		return getTransportOptionMap(sourceAddressId)
				.getOrDefault(transportOptionId, Collections.emptyMap());
	}

	private Map<KeyValue<Integer, String>, Set<KeyValue<Integer, String>>> getSubdivMap(
			Integer sourceAddressId, Integer transportOptionId, KeyValue<Integer,String> companyDto) {
		return getCompanyMap(sourceAddressId, transportOptionId)
				.getOrDefault(companyDto, Collections.emptyMap());
	}
	
	private Map<KeyValue<Integer, String>, Set<DespatchItemDTO>> getJobMap(Integer sourceAddressId, Integer transportOptionId,
																		   Integer destAddressId) {
		Map<Integer, Map<Integer, Map<KeyValue<Integer, String>, Set<DespatchItemDTO>>>> transportOptionMap =
			this.despatchItemMap.getOrDefault(sourceAddressId, Collections.emptyMap());

		Map<Integer, Map<KeyValue<Integer, String>, Set<DespatchItemDTO>>> destAddressMap =
			transportOptionMap.getOrDefault(transportOptionId, Collections.emptyMap());

		return destAddressMap.getOrDefault(destAddressId, Collections.emptyMap());
	}
	
	public Set<KeyValue<Integer, String>> getDestCompanyDtos(Integer sourceAddressId, Integer transportOptionId) {
		return getCompanyMap(sourceAddressId, transportOptionId).keySet();
	}
	
	public Set<KeyValue<Integer, String>> getDestSubdivDtos(Integer sourceAddressId, Integer transportOptionId, 
			KeyValue<Integer,String> companyDto) {
		return getSubdivMap(sourceAddressId, transportOptionId, companyDto)
				.keySet();
	}
	
	public Set<KeyValue<Integer, String>> getDestAddressDtos(Integer sourceAddressId, Integer transportOptionId, 
			KeyValue<Integer,String> companyDto, KeyValue<Integer,String> subdivDto) {
		return getSubdivMap(sourceAddressId, transportOptionId, companyDto)
				.getOrDefault(subdivDto, Collections.emptySet());
	}
	
	public Set<KeyValue<Integer,String>> getJobDtos(Integer sourceAddressId, Integer transportOptionId,
			Integer destAddressId) {
		return getJobMap(sourceAddressId, transportOptionId, destAddressId).keySet();
	}
	
	public Set<DespatchItemDTO> getDespatchItems(Integer sourceAddressId, Integer transportOptionId,
			Integer destAddressId, KeyValue<Integer,String> jobDto) {
		return getJobMap(sourceAddressId, transportOptionId, destAddressId)
				.getOrDefault(jobDto, Collections.emptySet());
	}
	
	public Set<KeyValue<Integer, String>> getDeliveries(Integer jobItemId) {
		Set<DeliveryItemDTO> deliveryItemDtos = this.deliveryItemMap.getOrDefault(jobItemId, Collections.emptySet());
		Set<KeyValue<Integer, String>> result = Collections.emptySet();
		if (!deliveryItemDtos.isEmpty()) {
			// We return in reverse order, so newest delivery shows first
			result = new TreeSet<>(Collections.reverseOrder());
			for (DeliveryItemDTO itemDto : deliveryItemDtos) {
				DeliveryDTO deliveryDto = this.deliveryMap.get(itemDto.getDeliveryId());
				if (deliveryDto == null) {
					logger.error("No deliveryDto for deliveryId " + itemDto.getDeliveryId());
				} else
					result.add(new KeyValue<>(deliveryDto.getDeliveryId(), deliveryDto.getDeliveryNumber()));
			}
		}
		
		return result;		
	}
	
	/**
	 * Returns a set of transport options which are present in the results for a
	 * particular sourceAddress but unexpected, as they aren't active or don't 
	 * belong to the subdivision.  
	 */
	public Set<KeyValue<Integer, String>> getUnexpectedTransportOptions(Integer sourceAddressId) {
		Set<KeyValue<Integer,String>> result = new HashSet<>();
		Set<Integer> resultOptionIds = getTransportOptionMap(sourceAddressId).keySet();
		for (KeyValue<Integer,String> option : resultTransportOptionSet) {
			if (resultOptionIds.contains(option.getKey()) &&
				!this.subdivTransportOptionIds.contains(option.getKey())) {
				result.add(option);
			}
		}

		return result;
	}
	
	/**
	 * Tests whether the specified state id is in the stateGroup
	 * The stateGroup to itemState map should be initialized before calling 
	 */
	public boolean isItemStateInStateGroup(Integer itemStateId, StateGroup stateGroup) {
		if (itemStateId == null) throw new IllegalArgumentException("itemStateId must not be null");
		if (stateGroup == null) throw new IllegalArgumentException("stateGroup must not be null");
		Set<Integer> itemStateIdSet = this.stateGroupMap.get(stateGroup);
		if (itemStateIdSet == null) throw new UnsupportedOperationException("The state group map has not been initialized for stateGroup "+stateGroup);
		return itemStateIdSet.contains(itemStateId);
	}
	public boolean isItemStateInClientDelivery(Integer itemStateId) {
		return isItemStateInStateGroup(itemStateId, StateGroup.CLIENTDELIVERY);
	}
	public boolean isItemStateInClientDespatch(Integer itemStateId) {
		return isItemStateInStateGroup(itemStateId, StateGroup.CLIENTDESPATCH);
	}
	public boolean isItemStateInAwaitingDespatchApproval(Integer itemStateId) {
		return isItemStateInStateGroup(itemStateId, StateGroup.AWAITINGDESPATCHAPPROVAL);
	}
	
	public boolean isItemStateInTPDelivery(Integer itemStateId) {
		return isItemStateInStateGroup(itemStateId, StateGroup.TPDELIVERY);
	}
	
	public boolean isItemStateInTPDespatch(Integer itemStateId) {
		return isItemStateInStateGroup(itemStateId, StateGroup.TPDESPATCH);
	}

	public boolean isCompanyActive(Integer companyId) {
		// No settings for allocated company = company inactive
		CompanySettingsDTO dto = this.companySettingsMap.get(companyId);
		return dto != null && dto.isActive();
	}

	public boolean isCompanyOnStop(Integer companyId) {
		// No settings for allocated company = company on stop
		CompanySettingsDTO dto = this.companySettingsMap.get(companyId);
		return dto == null || dto.isOnStop();
	}
	
	public int compareTodayToDate(LocalDate localDate) {
		if (localDate == null) throw new IllegalArgumentException("localDate was null");
		return today.compareTo(localDate);
	}
}