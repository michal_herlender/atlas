package org.trescal.cwms.core.deliverynote.entity.deliveryitem.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.vdi.projection.PositionProjection;

/**
 * Interface for accessing and manipulating {@link DeliveryItem} entities.
 */
public interface DeliveryItemService extends BaseService<DeliveryItem, Integer> {

	void addItemToDelivery(JobDeliveryItem jobDeliveryItem);

	/**
	 * Deletes the given {@link DeliveryItem} from the database.
	 * 
	 * @param deliveryitem the {@link DeliveryItem} to delete.
	 */
	void deleteDeliveryItem(DeliveryItem deliveryitem);

	/**
	 * Deletes the {@link DeliveryItem} with the given ID from the database.
	 * 
	 * @param id the ID of the {@link DeliveryItem} to delete.
	 */
	void deleteDeliveryItemById(int id);

	/**
	 * Returns the {@link DeliveryItem} entity with the given ID.
	 * 
	 * @param id the {@link DeliveryItem} ID.
	 * @return the {@link DeliveryItem}
	 */
	DeliveryItem findDeliveryItem(int id);

	/**
	 * Returns the {@link DeliveryItem} entity with the given ID.
	 * 
	 * @param id    the {@link DeliveryItem} ID.
	 * @param clazz the {@link Class} of the {@link DeliveryItem}
	 * @return the {@link DeliveryItem}
	 */
	DeliveryItem findDeliveryItem(int id, Class<? extends DeliveryItem> clazz);

	/**
	 * Returns all {@link DeliveryItem}(s) for the {@link GeneralDelivery} with the
	 * given ID.
	 * 
	 * @param delid the {@link Delivery} ID.
	 * @return the {@link List} of {@link DeliveryItem}(s).
	 */
	List<GeneralDeliveryItem> getItemsForGeneralDelivery(int delid);

	/**
	 * Returns all {@link DeliveryItem}(s) for the {@link JobDelivery} with the
	 * given ID.
	 * 
	 * @param delid the {@link Delivery} ID.
	 * @return the {@link List} of {@link DeliveryItem}(s).
	 */
	List<JobDeliveryItem> getItemsForJobDelivery(int delid);

	/**
	 * Insert the given {@link DeliveryItem} into the database.
	 * 
	 * @param deliveryitem the {@link DeliveryItem} to insert.
	 */
	void insertDeliveryItem(DeliveryItem deliveryitem);

	List<PositionProjection> getPositionProjectionForDeliveryNote(Integer deliveryId);
	
	JobDeliveryItem findNotYetDespatchedJobDeliveryItemByInstrument(Integer plantId);
	
	List<DeliveryItemDTO> getDeliveryItemDTOs(Integer deliveryId, Collection<Integer> jobItemIds);
	
	List<DeliveryItem> getItemsForJobDeliveryBydDelNo(String deliveryNo);
}