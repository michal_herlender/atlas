package org.trescal.cwms.core.deliverynote.entity.delivery;

import java.util.Comparator;

public class DeliveryComparator implements Comparator<Delivery>
{
	public int compare(Delivery del1, Delivery del2)
	{
		if ((del1.getDeliverydate() == null)
				|| (del2.getDeliverydate() == null))
		{
			Integer id1 = del1.getDeliveryid();
			Integer id2 = del2.getDeliveryid();

			return id1.compareTo(id2);
		}
		else
		{
			if (del1.getDeliverydate().equals(del2.getDeliverydate()))
			{
				Integer id1 = del1.getDeliveryid();
				Integer id2 = del2.getDeliveryid();

				return id1.compareTo(id2);
			}

			return del1.getDeliverydate().compareTo(del2.getDeliverydate());
		}

	}
}
