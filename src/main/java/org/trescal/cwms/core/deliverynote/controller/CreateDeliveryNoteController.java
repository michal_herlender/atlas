package org.trescal.cwms.core.deliverynote.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliverydestination.DeliveryDestination;
import org.trescal.cwms.core.deliverynote.entity.internaldelivery.InternalDelivery;
import org.trescal.cwms.core.deliverynote.entity.internalreturndelivery.InternalReturnDelivery;
import org.trescal.cwms.core.deliverynote.entity.thirdpartydelivery.ThirdPartyDelivery;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNoteForm;
import org.trescal.cwms.core.deliverynote.form.GroupAccessoryFormatter;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.db.OnBehalfItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.PartDelivery;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.val;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
	Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE, Constants.SESSION_ATTRIBUTE_COMPANY})
public class CreateDeliveryNoteController {
	@Autowired
	private AddressService addServ;
	@Autowired
	private CourierDespatchService cdServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private GroupAccessoryFormatter groupAccessoryFormatter;
	@Autowired
	private InstructionLinkService instructionLinkService;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private PartDelivery partDelivery;
	@Autowired
	private OnBehalfItemService onBehalfItemService;

	public static final String VIEW_NAME = "trescal/core/deliverynote/createdelnote";
	public static final String FORM_NAME = "delnoteform";

	@ModelAttribute(FORM_NAME)
	protected CreateDeliveryNoteForm formBackingObject(
			@RequestParam(value = "destination", required = false, defaultValue = "") DeliveryDestination deliveryDestination,
			@RequestParam(value = "jobid", required = false, defaultValue = "0") Integer jobId,
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobItemId,
			@RequestParam(value = "personid", required = false, defaultValue = "0") Integer personId,
			@RequestParam(value = "onbehalfid", required = false, defaultValue = "0") Integer onBehalfId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE) Locale defaultLocale) throws Exception {
		JobItem jobItem = jobItemId == 0 ? null : jiServ.findJobItem(jobItemId);
		Job job = jobId == 0 ? jobItem == null ? null : jobItem.getJob() : this.jobServ.get(jobId);
		Subdiv subdiv = subdivService.get(subdivDto.getKey());
		if(job != null)
			job.setRelatedInstructionLinks(this.instructionLinkService.getAllForJob(job));
		CreateDeliveryNoteForm form = new CreateDeliveryNoteForm();
		boolean tp = deliveryDestination == DeliveryDestination.THIRDPARTY;
		if (onBehalfId > 0)
			form.setOnBehalf(onBehalfItemService.get(onBehalfId));
		form.setTp(tp);
		form.setJob(job);
		switch (deliveryDestination) {
		case CLIENT:
			form.setDelivery(new ClientDelivery());
			if (form.getOnBehalf() != null) {
				form.setContact(form.getOnBehalf().getAddress().getSub().getDefaultContact());
				form.setContactid(form.getOnBehalf().getAddress().getSub().getDefaultContact().getId());
				form.setContacts(form.getOnBehalf().getAddress().getSub().getContacts());
				form.setAddressid(form.getOnBehalf().getAddress().getAddrid());
			} else {
				form.setContact(form.getJob().getCon());
				form.setContactid(form.getJob().getCon().getId());
				form.setAddressid(form.getJob().getReturnTo().getAddrid());
			}
			break;
		case INTERNAL:
			form.setDelivery(new InternalDelivery());
			if(jobItem!=null) {
				form.setContact(jobItem.getCalAddr().getSub().getDefaultContact());
				form.setContacts(jobItem.getCalAddr().getSub().getContacts());
				form.setAddressid(jobItem.getCalAddr().getAddrid());
			}
			break;
		case INTERNAL_RETURN:
			form.setDelivery(new InternalReturnDelivery());
			if(jobItem!=null) {
				form.setContact(jobItem.getReturnOption().getSub().getDefaultContact());
				form.setContacts(jobItem.getReturnOption().getSub().getContacts());
				if (jobItem.getReturnOption().getSub().getDefaultAddress() != null) {
					form.setAddressid(jobItem.getReturnOption().getSub().getDefaultAddress().getAddrid());
				}
			}
			break;
		case THIRDPARTY:
			form.setDelivery(new ThirdPartyDelivery());
			Contact contact = this.conServ.get(personId);
			form.setContact(contact);
			form.setContactid(personId);
			form.setAddressid(contact != null ? contact.getDefAddress().getAddrid() : 0);
		}
		val now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		form.getDelivery().setDeliverydate(now);
		form.setDeliverydate(now);
		form.setDespatches(this.cdServ.getDespatchesCreatedInLastWeek(subdiv.getSubdivid()));
		form.setAllowsPartDeliveries(true);
		if (job != null)
			form.setDelInstruction(this.groupAccessoryFormatter.getAccessoryText(job, LocaleContextHolder.getLocale()));
		if (form.getContact() != null) {
			KeyValue<Scope, String> partDeliveryValue = partDelivery.getValueHierarchical(Scope.CONTACT,
				form.getContact(), subdiv.getComp());
			form.setAllowsPartDeliveries(partDelivery.parseValue(partDeliveryValue.getValue()));
		}
		return form;
	}

	@ModelAttribute("userInstructionTypes")
	public Set<UserInstructionType> userInstructionTypes(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		return userService.get(username).getCon().getUserPreferences().getUserInstructionTypes();
	}

	@ModelAttribute("showInstructions")
	public boolean showInstructions(@ModelAttribute(FORM_NAME) CreateDeliveryNoteForm form,
			@ModelAttribute("userInstructionTypes") Set<UserInstructionType> userTypes) {
		return userTypes.stream().anyMatch(uit -> uit.getInstructionType() == InstructionType.CARRIAGE)
				&& form.getJob().getRelatedInstructionLinks().stream()
						.anyMatch(link -> link.getInstruction().getInstructiontype() == InstructionType.CARRIAGE)
				|| userTypes.stream().anyMatch(uit -> uit.getInstructionType() == InstructionType.PACKAGING)
						&& form.getJob().getRelatedInstructionLinks().stream().anyMatch(
								link -> link.getInstruction().getInstructiontype() == InstructionType.PACKAGING);
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@RequestMapping(value = "/createdelnote.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(FORM_NAME) CreateDeliveryNoteForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
			throws Exception {

		List<Contact> activesContacts = new ArrayList<>();
		String companyName = "";
		String subdivName = "";
		Integer subdivid = null;
		int defaultContactId = 0;

		if (form.getContact() != null) {
			Subdiv sub = form.getContact().getSub();
			activesContacts = sub.getContacts().stream().filter(Contact::isActive).collect(Collectors.toList());
			companyName = sub.getComp().getConame();
			subdivName = sub.getSubname();
			subdivid = sub.getSubdivid();
		} else if (!form.getContacts().isEmpty()) {
			activesContacts = form.getContacts().stream().filter(Contact::isActive).collect(Collectors.toList());
			val sub = activesContacts.stream().findAny().map(Contact::getSub).orElse(null);
			if (sub != null) {
				companyName = sub.getComp().getConame();
				subdivName = sub.getSubname();
				subdivid = sub.getSubdivid();
			}
		}

		if (form.getDelivery().getType().equals(DeliveryType.CLIENT)) {
			defaultContactId = form.getJob().getDefaultContact().getPersonid();
		} else if ((form.getDelivery().getType().equals(DeliveryType.INTERNAL)
				|| form.getDelivery().getType().equals(DeliveryType.INTERNAL_RETURN)
				|| form.getDelivery().getType().equals(DeliveryType.THIRDPARTY)) && form.getContact() != null) {
			defaultContactId = form.getContact().getPersonid();
		}

		if (subdivid != null) {
			LinkedHashSet<Address> deliveryAddresses = new LinkedHashSet<>();
			// For client delivery only, we always want to ensure the default return to address is available
			// (in case on-behalf of address is the preselected address in the controller)
			if (form.getDelivery().getType().equals(DeliveryType.CLIENT)) {
				deliveryAddresses.add(form.getJob().getReturnTo());
				if(form.getOnBehalf() != null){
					deliveryAddresses.add(form.getOnBehalf().getAddress());
				}
			}
			deliveryAddresses.addAll(this.addServ.getAllSubdivAddresses(subdivid, AddressType.DELIVERY, true));
			model.addAttribute("activeDeliveryAddresses", deliveryAddresses);
		} else {
			// It seems this shouldn't happen, as the subdiv would always be defined...
			model.addAttribute("activeDeliveryAddresses", new ArrayList<Address>());
		}

		List<Address> overriddenDeliveryAddresses = form.getJob().getItems().stream()
				.map(JobItem::getReturnToAddress).filter(Objects::nonNull).distinct()
				.collect(Collectors.toList());
		model.addAttribute("overriddenDeliveryAddresses", overriddenDeliveryAddresses);

		List<Contact> overriddenReturnToContacts = form.getJob().getItems().stream()
						.map(JobItem::getReturnToContact).filter(Objects::nonNull).distinct()
				.collect(Collectors.toList());
		model.addAttribute("overriddenReturnToContacts", overriddenReturnToContacts);

		model.addAttribute("contacts", activesContacts);
		model.addAttribute("defaultContactId", defaultContactId);
		model.addAttribute("coname", companyName);
		model.addAttribute("subdivName", subdivName);
		// add date tools
		model.addAttribute("datetools", new DateTools());
		this.cdServ.prepareCourierDespatchData(subdivDto.getKey(), companyDto.getKey(), model);
		return VIEW_NAME;
	}
}