package org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchTypeDTO;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.dwr.ResultWrapper;

/**
 * Interface for accessing and manipulating {@link CourierDespatchType}
 * entities.
 */
public interface CourierDespatchTypeService extends BaseService<CourierDespatchType, Integer>
{
	/**
	 * Sets the description of the {@link CourierDespatchType} with the given ID
	 * to the given new description.
	 * 
	 * @param cdtid the {@link CourierDespatchType} ID.
	 * @param newDesc the new description.
	 * @return {@link ResultWrapper}.
	 */
	ResultWrapper editType(int cdtid, String newDesc);
	
	List<CourierDespatchTypeDTO> getAllSorted();
	
	/**
	 * Inserts a new {@link CourierDespatchType} with the given string
	 * description for the {@link CourierDTO} with the given ID.
	 * 
	 * @param courierid the {@link CourierDTO} ID.
	 * @param desc the {@link CourierDespatchType} description.
	 * @return {@link ResultWrapper}.
	 */
	ResultWrapper newDespatchTypeForCourier(int courierid, String desc);
	
	void updateDefaultCourierDespatchType(Integer courierid, Integer cdtid);
	
	void deleteCourierDespatchType(Integer cdtid);
	
	Long linkedEntitiesCount(Integer cdtid);
}