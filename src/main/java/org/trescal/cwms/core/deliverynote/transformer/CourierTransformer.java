package org.trescal.cwms.core.deliverynote.transformer;

import java.util.HashSet;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.deliverynote.dto.CourierDTO;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchTypeDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.tools.EntityDtoTransformer;

@Service
public class CourierTransformer implements EntityDtoTransformer<Courier, CourierDTO> {

	@Override
	public CourierDTO transform(Courier entity) {
		CourierDTO dto = new CourierDTO();
		
		dto.setCourierid(entity.getCourierid());
		dto.setDefaultCourier(entity.isDefaultCourier());
		dto.setName(entity.getName());
		dto.setWebTrackURL(entity.getWebTrackURL());
		
		if(CollectionUtils.isNotEmpty(entity.getCdts())) {
			dto.setCdts(new HashSet<>());
			entity.getCdts().stream().forEach(cdt -> {
				dto.getCdts().add(new CourierDespatchTypeDTO(cdt));
			});
		}

		return dto;
	}

	@Override
	public Courier transform(CourierDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

}
