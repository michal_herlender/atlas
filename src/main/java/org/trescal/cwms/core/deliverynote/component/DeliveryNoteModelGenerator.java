package org.trescal.cwms.core.deliverynote.component;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.db.BusinessDocumentSettingsService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionCommand.DI_LOAD_ACCESSORIES;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionCommand.DI_LOAD_NOTES;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionResult;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand.DELIVERY_LOAD_COURIER_DESPATCH;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand.DELIVERY_LOAD_CREATED_BY_CONTACT;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand.DELIVERY_LOAD_DEST_ADDRESS;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand.DELIVERY_LOAD_DEST_CONTACT;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand.DELIVERY_LOAD_NOTES;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand.DELIVERY_LOAD_SOURCE_SUBDIV;
import org.trescal.cwms.core.deliverynote.projection.service.DeliveryItemProjectionService;
import org.trescal.cwms.core.deliverynote.projection.service.DeliveryProjectionService;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.instrument.service.InstrumentProjectionCommand;
import org.trescal.cwms.core.instrument.service.InstrumentProjectionCommand.INST_LOAD_ADDRESSES;
import org.trescal.cwms.core.instrument.service.InstrumentProjectionCommand.INST_LOAD_CONTACTS;
import org.trescal.cwms.core.instrument.service.InstrumentProjectionCommand.INST_LOAD_LOCATIONS;
import org.trescal.cwms.core.instrument.service.InstrumentProjectionService;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand.JOB_LOAD_CURRENCY;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_CERTIFICATES;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_CLIENT_POs;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_CONTRACT_REVIEW_PRICE;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_COSTING_ITEMS;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_INSTRUMENTS;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_ON_BEHALF;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_SERVICE_TYPES;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_SUPPLIER_POS;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionResult;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

import lombok.extern.slf4j.Slf4j;

/**
 * Loads delivery projection data for display of delivery note data via DeliveryNoteModel
 */
@Component @Slf4j
public class DeliveryNoteModelGenerator {
	@Autowired
	private BusinessDocumentSettingsService bdsService;
	@Autowired
	private DeliveryProjectionService dpService; 
	@Autowired
	private DeliveryItemProjectionService dipService;
	@Autowired
	private InstrumentProjectionService ipService;
	@Autowired
	private JobProjectionService jobProjectionService;
	@Autowired
	private InstructionService instructionService;

	public DeliveryNoteModel getModel(Integer deliveryId, Locale locale) {
		Integer allocatedCompanyId = null;	// Looked up via delivery, no need for separate query
		
		DeliveryProjectionCommand dpCommand = new DeliveryProjectionCommand(allocatedCompanyId, 
				DELIVERY_LOAD_CREATED_BY_CONTACT.class, DELIVERY_LOAD_SOURCE_SUBDIV.class, 
				DELIVERY_LOAD_DEST_ADDRESS.class, DELIVERY_LOAD_DEST_CONTACT.class, 
				DELIVERY_LOAD_NOTES.class, DELIVERY_LOAD_COURIER_DESPATCH.class);
		DeliveryDTO deliveryDto = this.dpService.getDeliveryDtosForIds(Collections.singleton(deliveryId), dpCommand).get(0);
		allocatedCompanyId = deliveryDto.getSourceCompanyId();
		Integer allocatedSubdivId = deliveryDto.getSourceSubdivId();
		
		DeliveryItemProjectionCommand dipCommand = new DeliveryItemProjectionCommand(locale, DI_LOAD_ACCESSORIES.class, DI_LOAD_NOTES.class); 
		DeliveryItemProjectionResult dipResult = this.dipService.getDeliveryItemDTOs(deliveryId, dipCommand);
		
		if (log.isDebugEnabled()) log.debug("deliveryItems.size() : "+dipResult.getDeliveryItems().size());
		JobItemProjectionCommand jipCommand = new JobItemProjectionCommand(locale, allocatedCompanyId, 
				JI_LOAD_INSTRUMENTS.class, JI_LOAD_SERVICE_TYPES.class, 
				JI_LOAD_CONTRACT_REVIEW_PRICE.class, JI_LOAD_CERTIFICATES.class,
				JI_LOAD_CLIENT_POs.class, JI_LOAD_COSTING_ITEMS.class,
				JI_LOAD_ON_BEHALF.class, JI_LOAD_SUPPLIER_POS.class);
		JobItemProjectionResult jipResult = this.dipService.loadProjectionJobItems(dipResult.getDeliveryItems(), jipCommand);
		List<JobItemProjectionDTO> jiDtos = jipResult.getJiDtos();
		if (log.isDebugEnabled()) log.debug("jiDtos.size() : "+jipResult.getJiDtos().size());
		
		List<InstrumentProjectionDTO> instDtos = jipResult.getInstruments();
		InstrumentProjectionCommand instCommand = new InstrumentProjectionCommand(locale, allocatedCompanyId,
				INST_LOAD_ADDRESSES.class, INST_LOAD_CONTACTS.class, INST_LOAD_LOCATIONS.class);
		ipService.loadInstrumentProjections(instDtos, instCommand);
		
		JobProjectionCommand jobCommand = new JobProjectionCommand(locale, allocatedCompanyId, 
				JOB_LOAD_CURRENCY.class);
		jobProjectionService.getDTOsByJobItems(jiDtos, jobCommand);
		
		BusinessDocumentSettings settings = this.bdsService.getForCompanyOrDefault(allocatedCompanyId);
		
		DeliveryNoteModel model = new DeliveryNoteModel();
		model.setAccessoryDtos(dipResult.getItemAccessories());
		model.setDeliveryDto(deliveryDto);
		model.setDeliveryItemDtos(dipResult.getDeliveryItems());
		model.setCertificateDtos(jipResult.getCertificates());
		model.setInstructionDtos(getInstructionDtosForDeliveryNote(jiDtos, allocatedSubdivId));
		model.setIncludeOnlySignedCertificates(settings.getDeliveryNoteSignedCertificates());
		return model;
	}
	
	/**
	 * We include all potential delivery related instructions (CARRIAGE, PACKAGING) and let the user filter (if needed) 
	 * to the ones which should appear on the delivery note document (only CARRIAGE instructions are eligible to appear) 
	 * This allows the data model to be used for different purposes (e.g. delivery note JSP view or other reporting).
	 */
	private List<InstructionDTO> getInstructionDtosForDeliveryNote(List<JobItemProjectionDTO> jiDtos, Integer allocatedSubdivId) {
		List<Integer> jobItemIds = jiDtos.stream().map(JobItemProjectionDTO::getJobItemId).collect(Collectors.toList());
		List<InstructionDTO> instDtos = Collections.emptyList();
		if (jobItemIds.size() > 0) 
			instDtos = this.instructionService.findAllByJobItems(jobItemIds, allocatedSubdivId, InstructionType.CARRIAGE, InstructionType.PACKAGING);
		if (log.isDebugEnabled()) 
			log.debug("instDtos.size() : "+instDtos.size()); 
		return instDtos;
	}
}
