package org.trescal.cwms.core.deliverynote.entity.courier;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "courier")
public class Courier extends Allocated<Company>
{
	private Set<CourierDespatchType> cdts;
	private int courierid;
	private boolean defaultCourier;
	private String name;
	private String webTrackURL;

	/**
	 * Data for job Courier Links
	 */
	private List<JobCourierLink> jobCouriers;
	
	/**
	 * @return the cdts
	 */
	@OneToMany(mappedBy = "courier", fetch = FetchType.LAZY, orphanRemoval = true)
	@SortComparator(CourierDespatchType.CourierDespatchTypeComparator.class)
	public Set<CourierDespatchType> getCdts()
	{
		return this.cdts;
	}

	/**
	 * @return the courierid
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "courierid", nullable = false, unique = true)
	@Type(type = "int")
	public int getCourierid()
	{
		return this.courierid;
	}

	/**
	 * @return the name
	 */
	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "name", nullable = false, length = 100)
	public String getName()
	{
		return this.name;
	}

	/**
	 * @return the webTrackURL
	 */
	@Length(max = 1000)
	@Column(name = "webtrackurl", length=1000)
	public String getWebTrackURL()
	{
		return this.webTrackURL;
	}

	@Column(name = "defaultcourier", nullable=false, columnDefinition="bit")
	public boolean isDefaultCourier()
	{
		return this.defaultCourier;
	}

	/**
	 * @param cdts the cdts to set
	 */
	public void setCdts(Set<CourierDespatchType> cdts)
	{
		this.cdts = cdts;
	}

	/**
	 * @param courierid the courierid to set
	 */
	public void setCourierid(int courierid)
	{
		this.courierid = courierid;
	}

	public void setDefaultCourier(boolean defaultCourier)
	{
		this.defaultCourier = defaultCourier;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @param webTrackURL the webTrackURL to set
	 */
	public void setWebTrackURL(String webTrackURL)
	{
		this.webTrackURL = webTrackURL;
	}

	@OneToMany(mappedBy = "courier", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<JobCourierLink> getJobCouriers() {
		return jobCouriers;
	}

	public void setJobCouriers(List<JobCourierLink> jobCouriers) {
		this.jobCouriers = jobCouriers;
	}
	
	
}