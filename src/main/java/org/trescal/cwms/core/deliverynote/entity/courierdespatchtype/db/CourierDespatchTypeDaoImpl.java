package org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchTypeDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier_;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType_;

@Repository("CourierDespatchTypeDao")
public class CourierDespatchTypeDaoImpl extends BaseDaoImpl<CourierDespatchType, Integer> implements CourierDespatchTypeDao
{
	@Override
	protected Class<CourierDespatchType> getEntity() {
		return CourierDespatchType.class;
	}
	
	public List<CourierDespatchTypeDTO> getAllSorted()
	{
		return getResultList(cb ->{
			CriteriaQuery<CourierDespatchTypeDTO> cq = cb.createQuery(CourierDespatchTypeDTO.class);
			Root<CourierDespatchType> root = cq.from(CourierDespatchType.class);
			Join<CourierDespatchType, Courier> courier = root.join(CourierDespatchType_.courier);
			List<javax.persistence.criteria.Order> orders = new ArrayList<>();
			cq.select(cb.construct(CourierDespatchTypeDTO.class, root.get(CourierDespatchType_.cdtid),
					root.get(CourierDespatchType_.description), 
					courier.get(Courier_.courierid),
					courier.get(Courier_.name)));
			cq.distinct(true);
			orders.add(cb.asc(courier.get(Courier_.courierid)));
			orders.add(cb.asc(root.get(CourierDespatchType_.cdtid)));
			cq.orderBy(orders);
			return cq;
		});
	}
}