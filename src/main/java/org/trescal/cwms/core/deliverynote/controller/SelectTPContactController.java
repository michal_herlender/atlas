package org.trescal.cwms.core.deliverynote.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.form.SelectTPContactForm;
import org.trescal.cwms.core.deliverynote.form.SelectTPContactValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;

/**
 * @author Jamie Vinall
 */
@Controller @IntranetController
public class SelectTPContactController
{
	@Autowired
	private SelectTPContactValidator validator;	// Manual validation
	private int jobid;

	@InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
	}
	
	@ModelAttribute("form")
	protected Object formBackingObject(@RequestParam(value="jobid", required=false, defaultValue="0") Integer jobid,
			@RequestParam(value="freehand", required=false, defaultValue="false") Boolean freehand) throws Exception
	{
		this.jobid = jobid;
		SelectTPContactForm form = new SelectTPContactForm();
		form.setText((this.jobid == 0) ? "General" : "Third Party");
		form.setFreehand(freehand);
		return form;
	}
	
	@RequestMapping(value="/selecttpcontact.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/deliverynote/selecttpcontact";
	}
	
	@RequestMapping(value="/selecttpcontact.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request,
			@Validated @ModelAttribute("form") SelectTPContactForm form, BindingResult bindingResult)
	{
		if (bindingResult.hasErrors()) {
			return new ModelAndView("trescal/core/deliverynote/selecttpcontact", "form", form);
		}
		if (this.jobid == 0)
		{
			if (form.isFreehand())
			{
				return new ModelAndView(new RedirectView("creategendelivery.htm?freehand=true"));
			}
			else
			{
				return new ModelAndView(new RedirectView("creategendelivery.htm?personid="
						+ form.getPersonid()));
			}
		}
		else
		{
			if (form.isFreehand())
			{
				return new ModelAndView(new RedirectView("createdelnote.htm?jobid="
						+ this.jobid + "&destination=THIRDPARTY&freehand=true"));
			}
			else
			{
				return new ModelAndView(new RedirectView("createdelnote.htm?jobid="
						+ this.jobid
						+ "&destination=THIRDPARTY&personid="
						+ form.getPersonid()));
			}
		}
	}
}