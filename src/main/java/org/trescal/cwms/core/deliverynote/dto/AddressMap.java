package org.trescal.cwms.core.deliverynote.dto;

public class AddressMap
{
	private int addrid;
	private long jobCount;

	public AddressMap(int addrid, long jobCount)
	{
		this.addrid = addrid;
		this.jobCount = jobCount;
	}

	public int getAddrid()
	{
		return this.addrid;
	}

	public long getJobCount()
	{
		return this.jobCount;
	}

	public void setAddrid(int addrid)
	{
		this.addrid = addrid;
	}

	public void setJobCount(long jobCount)
	{
		this.jobCount = jobCount;
	}
}