package org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;

@Entity
@Table(name = "deliveryitemaccessory")
public class DeliveryItemAccessory
{
	private int id;
	private Accessory accessory;
	private DeliveryItem deliveryItem;
	private int qty;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public int getId() {
		return this.id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accessoryId")
	public Accessory getAccessory() {
		return this.accessory;
	}
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name = "deliveryitemid")
	public DeliveryItem getDeliveryItem() {
		return this.deliveryItem;
	}
	
	@Column(name = "qty")
	public int getQty() {
		return this.qty;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setAccessory(Accessory accessory) {
		this.accessory = accessory;
	}
	
	public void setDeliveryItem(DeliveryItem di) {
		this.deliveryItem = di;
	}
	
	public void setQty(int qty) {
		this.qty = qty;
	}
}