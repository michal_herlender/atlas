package org.trescal.cwms.core.deliverynote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNoteForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
@IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_CONTACT,
	Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class ViewGeneralDeliveryController {
	@Autowired
	private AddressService addServ;
	@Autowired
	private CourierDespatchService cdServ;
	@Autowired
	private CourierDespatchTypeService cdtServ;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private DeliveryService delServ;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private LocationService locServ;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;

	@InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
    }
	
	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}
	
	@ModelAttribute("form")
	protected CreateDeliveryNoteForm formBackingObject(
			@RequestParam(value = "delid", required = false, defaultValue = "0") Integer delid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		CreateDeliveryNoteForm form = new CreateDeliveryNoteForm();
		GeneralDelivery delivery = (GeneralDelivery) this.delServ.get(delid);
		this.delServ.setDirectoryUsingComponent(delivery);
		form.setDelivery(delivery);
		form.getDelivery().setSentEmails(this.emailServ.getAllComponentEmails(Component.GENERAL_DELIVERY, delid));
		form.getDelivery().setDirectory(this.compDirServ.getDirectory(this.scService.findComponent(Component.GENERAL_DELIVERY), delivery.getDeliveryno()));
		form.setDespatches(this.cdServ.getDespatchesCreatedInLastWeek(subdivDto.getKey()));
		return form;
	}

	@RequestMapping(value="/gendelnotesummary.htm", method=RequestMethod.POST)
	protected String onSubmit(Model model,
							  @RequestParam(value = "selectant", required = false, defaultValue = "") String selectant,
							  @RequestParam(value = "selectdespatch", required = false, defaultValue = "0") Integer despatchId,
							  @RequestParam(value = "despatchdate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate despatchDate,
							  @RequestParam(value = "selecttype", required = false, defaultValue = "0") Integer selectType,
							  @RequestParam(value = "consigno", required = false, defaultValue = "") String consignmentNumber,
							  @ModelAttribute(Constants.SESSION_ATTRIBUTE_CONTACT) Contact currentUser,
							  @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
							  @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
							  @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
							  @ModelAttribute("form") @Validated CreateDeliveryNoteForm formObj, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData(model, newFiles, formObj, subdivDto, companyDto);
		}
		Delivery delivery = formObj.getDelivery();
		String tab = "";
		if (formObj.getAction().equals("editdelnote")) {
			if (formObj.getAddressid() != null) {
				delivery.setAddress(this.addServ.get(formObj.getAddressid()));
			}
			if (formObj.getContactid() != null) {
				delivery.setContact(this.conServ.get(formObj.getContactid()));
			}
			delivery.setLocation(this.locServ.get(formObj.getLocid()));
			this.delServ.updateDelivery(delivery);
			tab = "&loadtab=editdelivery-tab";
		}
		else if (formObj.getAction().equals("assigncrdes")) {
			if (selectant.equals("exists")) {
				// add to existing courier despatch
				delivery.setCrdes(this.cdServ.findCourierDespatch(despatchId));
				this.delServ.updateDelivery(delivery);
			}
			else if (selectant.equals("new")) {
				// create new courier despatch and add delivery to it
				Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
				CourierDespatch cd = new CourierDespatch();
				cd.setDespatchDate(despatchDate);
				cd.setCreationDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
				cd.setOrganisation(allocatedSubdiv);
				cd.setCdtype(this.cdtServ.get(selectType));
				cd.setCreatedBy(currentUser);
				cd.setConsignmentno(consignmentNumber);
				this.cdServ.insertCourierDespatch(cd);
				delivery.setCrdes(cd);
				this.delServ.updateDelivery(delivery);
			}
			tab = "&loadtab=despatch-tab";
		}
		model.asMap().clear();
		return "redirect:gendelnotesummary.htm?delid=" + delivery.getDeliveryid() + tab;
	}
	
	@RequestMapping(value="/gendelnotesummary.htm", method=RequestMethod.GET)
	protected String referenceData(Model model,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute("form") CreateDeliveryNoteForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) throws Exception
	{
		int id = form.getDelivery().getDeliveryid();
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.GENERAL_DELIVERY, id, newFiles);
		
		List<ScheduledDelivery> scheduledDeliveryList = scheduledDeliveryService.scheduledDeliveryFromScheduleAndOrDelivery(null, id);
        //  no crdes and no schedule for the delivery 
        if(form.getDelivery().getCrdes() == null && scheduledDeliveryList.isEmpty()){
        	model.addAttribute("nullCrDes", true);
        } else {
        	model.addAttribute("nullCrDes", false);
        	model.addAttribute("scheduledDeliveryList", scheduledDeliveryList);
        }
		model.addAttribute("privateOnlyNotes", NoteType.DELIVERYNOTE.isPrivateOnly());
		model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.GENERAL_DELIVERY));
		model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.addAttribute(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
		this.cdServ.prepareCourierDespatchData(subdivDto.getKey(), companyDto.getKey(), model);	
		return "trescal/core/deliverynote/gendelnotesummary";
	}
}