package org.trescal.cwms.core.deliverynote.dto;

import java.util.Comparator;

/**
 * Comparator for display sorting
 * All fields compared come from not-nullable fields in the DB
 * 
 * @author galen
 *
 */
public class DespatchItemDTOComparator implements Comparator<DespatchItemDTO> {

	@Override
	public int compare(DespatchItemDTO dto0, DespatchItemDTO dto1) {
		
		int result = dto0.getJobNumber().compareTo(dto1.getJobNumber());
		if (result == 0) {
			result = dto0.getJobId().compareTo(dto1.getJobId());
			if (result == 0) {
				result = dto0.getJobItemNumber().compareTo(dto1.getJobItemNumber());
				if (result == 0) {
					result = dto0.getJobItemId().compareTo(dto1.getJobItemId());
				}
			}
		}
		
		return result;
	}

}
