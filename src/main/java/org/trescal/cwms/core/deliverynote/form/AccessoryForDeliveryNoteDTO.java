package org.trescal.cwms.core.deliverynote.form;

import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;

public class AccessoryForDeliveryNoteDTO {
	
	private int id;
	private Accessory accessory;
	private int qty;
	private boolean addToDelivery;
	
	public AccessoryForDeliveryNoteDTO(ItemAccessory itemAccessory) {
		this.id = itemAccessory.getId();
		this.accessory = itemAccessory.getAccessory();
		this.qty = itemAccessory.getQuantity();
		addToDelivery = false;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Accessory getAccessory() {
		return accessory;
	}
	
	public void setAccessory(Accessory accessory) {
		this.accessory = accessory;
	}
	
	public boolean isAddToDelivery() {
		return addToDelivery;
	}
	
	public void setAddToDelivery(boolean addToDelivery) {
		this.addToDelivery = addToDelivery;
	}
	
	public int getQty() {
		return qty;
	}
	
	public void setQty(int qty) {
		this.qty = qty;
	}
}