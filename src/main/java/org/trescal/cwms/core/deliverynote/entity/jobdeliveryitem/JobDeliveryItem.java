package org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;

@Entity
@DiscriminatorValue("job")
public class JobDeliveryItem extends DeliveryItem
{
	private JobItem jobitem;
	private TPRequirement tpRequirement;

	public JobDeliveryItem()
	{
		super();
	}

	/**
	 * @return the jobitem
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJobitem()
	{
		return this.jobitem;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name = "tprequirementid", unique=true)
	public TPRequirement getTpRequirement()
	{
		return this.tpRequirement;
	}

	/**
	 * @param jobitem the jobitem to set
	 */
	public void setJobitem(JobItem jobitem)
	{
		this.jobitem = jobitem;
	}

	public void setTpRequirement(TPRequirement tpRequirement)
	{
		this.tpRequirement = tpRequirement;
	}
}