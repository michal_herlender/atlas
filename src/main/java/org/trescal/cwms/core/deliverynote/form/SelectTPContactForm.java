package org.trescal.cwms.core.deliverynote.form;

public class SelectTPContactForm
{
	private boolean freehand;
	private Integer personid;
	private String text;

	/**
	 * @return the personid
	 */
	public Integer getPersonid()
	{
		return this.personid;
	}

	public String getText()
	{
		return this.text;
	}

	public boolean isFreehand()
	{
		return this.freehand;
	}

	public void setFreehand(boolean freehand)
	{
		this.freehand = freehand;
	}

	/**
	 * @param personid the personid to set
	 */
	public void setPersonid(Integer personid)
	{
		this.personid = personid;
	}

	public void setText(String text)
	{
		this.text = text;
	}
}