package org.trescal.cwms.core.deliverynote.entity.deliveryitem;

import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Set;

@Entity
@Table(name = "deliveryitem")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "deliveryitemtype", discriminatorType = DiscriminatorType.STRING)
public abstract class DeliveryItem extends Auditable implements NoteAwareEntity {
	private Delivery delivery;
	private Set<DeliveryItemAccessory> accessories;
	private String accessoryFreeText;
	private int delitemid;
	private boolean despatched;
	private ZonedDateTime despatchedon;
	private Set<DeliveryItemNote> notes;

	public DeliveryItem() {
	}

	/**
	 * @return the delitemid
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "deliveryitemid", nullable = false, unique = true)
	public int getDelitemid() {
		return this.delitemid;
	}

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "deliveryItem", fetch = FetchType.LAZY)
	public Set<DeliveryItemAccessory> getAccessories() {
		return this.accessories;
	}

	@Column(name = "accessoryFreeText")
	public String getAccessoryFreeText() {
		return accessoryFreeText;
	}

	/**
	 * @return the delivery
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
	@JoinColumn(name = "deliveryid")
	public Delivery getDelivery()
	{
		return this.delivery;
	}

	/**
	 * @return the despatchedon
	 */
	@Column(name = "despatchedon", columnDefinition = "datetime2")
	public ZonedDateTime getDespatchedon() {
		return this.despatchedon;
    }
	
	/**
	 * @return the delitemnotes
	 */
	@Override
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, mappedBy = "deliveryitem", fetch = FetchType.LAZY)
	public Set<DeliveryItemNote> getNotes() {
		return this.notes;
	}
	
	@Transient
	public Integer getPrivateActiveNoteCount()
	{
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount()
	{
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}
	
	/**
	 * @return the despatched
	 */
	@Column(name = "despatched", nullable=false, columnDefinition="bit")
	public boolean isDespatched() {
		return this.despatched;
	}
	
	/**
	 * @param delitemid the delitemid to set
	 */
	public void setDelitemid(int delitemid) {
		this.delitemid = delitemid;
	}
	
	public void setAccessories(Set<DeliveryItemAccessory> accessories) {
		this.accessories = accessories;
	}
	
	public void setAccessoryFreeText(String accessoryFreeText) {
		this.accessoryFreeText = accessoryFreeText;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}
	
	/**
	 * @param despatched the despatched to set
	 */
	public void setDespatched(boolean despatched) {
		this.despatched = despatched;
	}

	/**
	 * @param despatchedon the despatchedon to set
	 */
	public void setDespatchedon(ZonedDateTime despatchedon) {
		this.despatchedon = despatchedon;
	}
	
	/**
	 * @param delitemnotes the delitemnotes to set
	 */
	public void setNotes(Set<DeliveryItemNote> delitemnotes) {
		this.notes = delitemnotes;
	}
}