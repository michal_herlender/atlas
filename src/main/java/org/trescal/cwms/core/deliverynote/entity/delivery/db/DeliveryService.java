package org.trescal.cwms.core.deliverynote.entity.delivery.db;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.SortedSet;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.dto.DespatchItemDTO;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNotePlainForm;
import org.trescal.cwms.core.deliverynote.form.DeliverySearchForm;
import org.trescal.cwms.core.deliverynote.form.EditDeliveryNoteForm;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.rest.customer.dto.RestCustomerDeliveryDto;
import org.trescal.cwms.rest.customer.dto.RestDeliveryDetailDto;

/**
 * Interface for accessing and manipulating {@link Delivery} entities.
 */
public interface DeliveryService extends BaseService<Delivery, Integer> {

	/**
	 * Checks whether or not the {@link Delivery} entity with the given ID contains
	 * any items that have been marked as 'despatched'.
	 * 
	 * @param delid the {@link Delivery} ID.
	 * @return true if the {@link Delivery} contains one or more
	 *         {@link DeliveryItem}(s) that have their 'despatched' attribute set to
	 *         'true'. Otherwise, false.
	 */
	boolean containsDespatchedItems(int delid);

	ClientDelivery createDeliveryForJobItems(List<JobItem> jobItems, Contact currentContact, Subdiv allocatedSubdiv);

	/**
	 * Deletes the given {@link Delivery} (and all cascading entities) from the
	 * database.
	 * 
	 * @param delivery the {@link Delivery} to delete.
	 */
	void deleteDelivery(Delivery delivery);

	ResultWrapper despatchDelivery(String deliveryNo);

	ResultWrapper despatchDeliveryOrItem(String deliveryNoOrBarcode);

	/**
	 * Returns the {@link Delivery} entity with the given ID.
	 * 
	 * @param id the {@link Delivery} ID.
	 * @return the {@link Delivery}
	 */
	Delivery get(int id);

	/**
	 * Returns the {@link Delivery} entity with the given ID.
	 * 
	 * @param id    the {@link Delivery} ID.
	 * @param clazz the {@link Class} of the entity to be found.
	 * @return the {@link Delivery}
	 */
	<DeliveryClass extends Delivery> DeliveryClass get(int id, Class<DeliveryClass> clazz);

	/**
	 * Returns the {@link Delivery} entity with the given 'DeliveryNo' value.
	 * 
	 * @param delno the 'DeliveryNo' attribute value of the {@link Delivery} entity
	 *              to be found.
	 * @return the {@link Delivery}
	 */
	Delivery findDeliveryByDeliveryNo(String delno);

	/**
	 * Returns a list of all deliveries relates to the job, including those linked
	 * to other jobs' delivery notes
	 * 
	 * @param job
	 * @return set of Deliveries
	 */
	SortedSet<JobDelivery> findRelatedToJob(Job job);

	List<? extends Delivery> findByJobItem(int jobItemId, DeliveryType type);
	
	List<DeliveryDTO> getDeliveryDtosForIds(Collection<Integer> deliveryIds);

	/**
	 * Returns a {@link List} of {@link DespatchItemDTO}s containing job items
	 * with the specified state groups
	 * 
	 * @return
	 */
	List<DespatchItemDTO> getTpGoodsOutItems(List<Integer> stateGroup, int allocatedSubdivId, Locale locale);

	List<DespatchItemDTO> getGoodsOutDtos(List<Integer> stateGroups, int allocatedSubdivId, Locale locale);

	/**
	 * Inserts the given {@link Delivery} into the database.
	 * 
	 * @param delivery the {@link Delivery} to insert.
	 */
	void insertDelivery(Delivery delivery);

	/**
	 * Returns all {@link Delivery} results of a complex query customised by the
	 * user on the {@link DeliverySearchForm} object.
	 * 
	 * @param dsf the {@link DeliverySearchForm} containing the query criteria to
	 *            build.
	 * @param rs  the {@link PagedResultSet}
	 * @return the {@link List} of {@link Delivery} entities.
	 */
	PagedResultSet<Delivery> queryDeliveries(DeliverySearchForm dsf, PagedResultSet<Delivery> rs);

	ResultWrapper receiveDelivery(String deliveryNo, int addrId);

	ResultWrapper receiveDeliveryOrItem(String deliveryNoOrBarcode, int addrId, Integer locId);

	/**
	 * Removes the given assigned {@link Delivery} from the {@link Schedule} with
	 * which it is linked.
	 * 
	 * @param delid the ID of the {@link Delivery} to remove.
	 */
	void removeDeliveryFromSchedule(int delid);

	/**
	 * Updates the given {@link Delivery} in the database.
	 *
	 * @param delivery the {@link Delivery} to update.
	 */
	void updateDelivery(Delivery delivery);

	List<ClientDelivery> getClientDeliveryListByJobitemId(Integer jobitemid);

	List<RestCustomerDeliveryDto> findDeliveryDto(Subdiv subdiv, LocalDate dtStart, LocalDate dtEnd, DeliveryType deliveryType);

	List<RestDeliveryDetailDto> findDeliveryDetails(List<Integer> deliveries);

	/**
	 * Sets the transient directory on the Delivery (still used by file browser),
	 * removed from get() 2019-03-21
	 */
	void setDirectoryUsingComponent(Delivery d);

	Optional<OrderProjection> getOrderProjectionForDeliveryNote(Integer deliveryId);

    Long deliveryCountByCourier(Integer courierId);

    Long deliveryCountByCourierDespatchType(Integer cdtId);

    /**
     * edit given Delivery Note
     */
    void editDeliveryNote(Delivery delivery, LocalDate newDeliveryDate, EditDeliveryNoteForm form);

    void updateDestinationReceiptDate(Delivery delivery, ZonedDateTime destinationReceiptDate, Integer businessCompanyId);

    boolean isLastTPDelivery(JobItem ji);

	List<Integer> getdeliveriesIdsByJob(Integer jobId);
	
	boolean isReadyForDestinationReceipt(Delivery delivery);
	
	JobDelivery createDelivery(CreateDeliveryNotePlainForm form, String username, Subdiv allocatedSubdiv);
	
	ResultWrapper scanOutDeliveryOrItem(String deliveryNoOrBarcode);
	
	ResultWrapper scanOuthDelivery (String deliveryNo);
	
	String deliveryExists(String DeliveryNo);
	
	Address getDeliveryAddressByDeliveryNo(String deliveryNo);
	
	ResultWrapper scanInDeliveryOrItem(String deliveryNoOrBarcode, int addrId, Integer locId);
	
	ResultWrapper scanInDelivery(String deliveryNo, int addrId);
		
}