package org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule;

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;

import lombok.Setter;

@Entity
@Setter
@Table(name = "transportoptionschedulerule")
public class TransportOptionScheduleRule {

	private Integer id;
	private TransportOption transportOption;
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime cutoffTime;
	private String dayOfWeek;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transportoptionid", foreignKey = @ForeignKey(name = "FK_transportoptionschedulerule_transportoption"), nullable = false)
	public TransportOption getTransportOption() {
		return transportOption;
	}

	@NotNull
	@Column(name = "cutofftime", nullable = false)
	public LocalTime getCutoffTime() {
		return cutoffTime;
	}

	@NotNull
	@Column(name = "dayofweek", nullable = false)
	public String getDayOfWeek() {
		return dayOfWeek;
	}

}
