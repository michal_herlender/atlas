package org.trescal.cwms.core.deliverynote.projection;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

/**
 * Intended to consolidate the CourierDespatch, Courier, and CourierDespatchType entities into one projection
 * Initially created for delivery note BIRT document, but could also be used to efficiently create a list of 
 * CourierDespatch options for user selections without entity loading.
 * 
 * Other fields (createdById, default booleans) could be easily added at minimal query cost when needed
 */
@Getter @Setter
public class CourierDespatchProjectionDTO {
	// Fields from CourierDespatch
	private Integer courierDespatchId;
	private String consignmentNumber;
	private LocalDate creationDate;
	private LocalDate despatchDate;
	// Fields from CourierDespatchType
	private Integer courierDespatchTypeId;
	private String courierDespatchTypeDescription;
	// Fields from Courier
	private Integer courierId;
	private String courierName;
	
	// Constructor used for JPA creation
	public CourierDespatchProjectionDTO(Integer courierDespatchId, String consignmentNumber, LocalDate creationDate,
			LocalDate despatchDate, Integer courierDespatchTypeId, String courierDespatchTypeDescription, Integer courierId,
			String courierName) {
		super();
		this.courierDespatchId = courierDespatchId;
		this.consignmentNumber = consignmentNumber;
		this.creationDate = creationDate;
		this.despatchDate = despatchDate;
		this.courierDespatchTypeId = courierDespatchTypeId;
		this.courierDespatchTypeDescription = courierDespatchTypeDescription;
		this.courierId = courierId;
		this.courierName = courierName;
	}
}
