package org.trescal.cwms.core.deliverynote.component;

import java.util.Collection;

import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemAccessoryDTO;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateProjectionDTO;
import org.trescal.cwms.core.system.dto.InstructionDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DeliveryNoteModel {
	private DeliveryDTO deliveryDto;
	private Collection<DeliveryItemDTO> deliveryItemDtos;
	private Collection<DeliveryItemAccessoryDTO> accessoryDtos;
	private Collection<CertificateProjectionDTO> certificateDtos;
	private Collection<InstructionDTO> instructionDtos;
	private Boolean includeOnlySignedCertificates; 
}
