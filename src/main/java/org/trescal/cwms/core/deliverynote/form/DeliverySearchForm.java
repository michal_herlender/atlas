package org.trescal.cwms.core.deliverynote.form;

import java.util.Date;

import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DeliverySearchForm
{
	private int allocatedSubdivId;
	private String coid;
	private String consignmentNo;
	private Date deliverydate;
	private DeliveryType deliveryType;
	private String delno;
	private int pageNo;
	private int resultsPerPage = 50;
	private PagedResultSet<Delivery> rs;

}