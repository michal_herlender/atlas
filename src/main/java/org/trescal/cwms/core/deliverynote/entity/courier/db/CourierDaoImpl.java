package org.trescal.cwms.core.deliverynote.entity.courier.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier_;

@Repository("CourierDao")
public class CourierDaoImpl extends BaseDaoImpl<Courier, Integer> implements CourierDao {

	@Override
	protected Class<Courier> getEntity() {
		return Courier.class;
	}

	@Override
	public List<Courier> getCouriersByBusinessCompany(Integer orgid) {
		if (orgid == null) 
			throw new IllegalArgumentException("orgid must be specified!");

		return getResultList(cb -> {
			CriteriaQuery<Courier> cq = cb.createQuery(Courier.class);
			Root<Courier> root = cq.from(Courier.class);
			Join<Courier, Company> companyJoin = root.join(Courier_.organisation.getName(), JoinType.INNER);
			cq.where(cb.equal(companyJoin.get(Company_.coid), orgid));
			cq.orderBy(cb.desc(root.get(Courier_.defaultCourier)), cb.asc(root.get(Courier_.name)));
			
			return cq;
		});
	}
}