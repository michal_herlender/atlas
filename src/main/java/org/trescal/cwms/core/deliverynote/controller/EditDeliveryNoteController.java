package org.trescal.cwms.core.deliverynote.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNoteForm;
import org.trescal.cwms.core.deliverynote.form.EditDeliveryNoteForm;
import org.trescal.cwms.core.deliverynote.form.EditGeneralDeliveryNoteValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_CONTACT, Constants.SESSION_ATTRIBUTE_SUBDIV,
    Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_COMPANY})
public class EditDeliveryNoteController {

    private static final String JFS_VIEW = "viewdelnote.htm";

    @Autowired
	private DeliveryService delServ;
	@Autowired
	private EditGeneralDeliveryNoteValidator validator;
	
	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.dtf_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
		binder.setValidator(validator);
	}
	
	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}
	
	@RequestMapping(value = "/editdelnote.htm", method = RequestMethod.POST)
	protected ModelAndView onEditSubmit(Model model, 
			@RequestParam(value = "delid", required = false, defaultValue = "0") Integer delId,
			@RequestParam(name = "delivery.deliverydate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate newDeliveryDate,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			final RedirectAttributes redirectAttributes,
			@ModelAttribute("form") @Validated EditDeliveryNoteForm form, BindingResult bindingResult)
			throws Exception {
		
		Delivery delivery = this.delServ.get(delId);
	
		if (bindingResult.hasErrors()) {
			CreateDeliveryNoteForm formObj = new CreateDeliveryNoteForm();
			formObj.setDelivery(delivery);

			redirectAttributes.addFlashAttribute("model", model);
			redirectAttributes.addFlashAttribute("newFiles", newFiles);
			redirectAttributes.addFlashAttribute("form", formObj);
			redirectAttributes.addFlashAttribute("model", subdivDto);
			redirectAttributes.addFlashAttribute("model", companyDto);
			redirectAttributes.addAttribute("delid", delivery.getDeliveryid());
			return new ModelAndView(new RedirectView(JFS_VIEW));
		}
		// edit the delivery note 
		// convert newDeliveryDate from DateTimeFormat to Date 
		//(until we decide if we want to convert deliverydate to DateTimeFormat)
		delServ.editDeliveryNote(delivery, newDeliveryDate, form);

		model.asMap().clear();
		redirectAttributes.addAttribute("delid", delivery.getDeliveryid());
		redirectAttributes.addAttribute("loadtab", "editdelivery-tab");
		return new ModelAndView(new RedirectView(JFS_VIEW));
	}


}
