package org.trescal.cwms.core.deliverynote.entity.delivery.db;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilikePredicateList;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.dto.DespatchItemDTO;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery_;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier_;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch_;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType_;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery_;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem_;
import org.trescal.cwms.core.deliverynote.entity.thirdpartydelivery.ThirdPartyDelivery;
import org.trescal.cwms.core.deliverynote.entity.thirdpartydelivery.ThirdPartyDelivery_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.deliverynote.form.DeliverySearchForm;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus_;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.StringJpaUtils;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;
import org.trescal.cwms.rest.customer.dto.RestCustomerDeliveryDto;
import org.trescal.cwms.rest.customer.dto.RestDeliveryDetailFlatDto;

import lombok.val;

@Repository("DeliveryDao")
public class DeliveryDaoImpl extends AllocatedToSubdivDaoImpl<Delivery, Integer> implements DeliveryDao {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;

	@Override
	protected Class<Delivery> getEntity() {
		return Delivery.class;
	}

	public <DeliveryClass extends Delivery> DeliveryClass get(int id, Class<DeliveryClass> clazz) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(clazz);
			val delivery = cq.from(clazz);
			cq.where(cb.equal(delivery, id));
			return cq;
		}).orElse(null);
	}

	@Override
	public Delivery findDeliveryByDeliveryNo(String delno) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Delivery.class);
			val delivery = cq.from(Delivery.class);
			cq.where(cb.equal(delivery.get(Delivery_.deliveryno), delno));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<JobDelivery> findByJob(Job job) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobDelivery.class);
			val delivery = cq.from(JobDelivery.class);
			cq.where(cb.equal(delivery.join(JobDelivery_.items, JoinType.LEFT)
					.join(JobDeliveryItem_.jobitem, JoinType.LEFT).get(JobItem_.job), job));
			return cq;
		});
	}

	@Override
	public List<DespatchItemDTO> getTpGoodsOutItems(List<Integer> stateGroups, int allocatedSubdivId, Locale locale) {
		// if no state groups have been passed
		if ((stateGroups == null) || (stateGroups.size() < 1))
			return null;
		return getResultList(cb ->{
			CriteriaQuery<DespatchItemDTO> cq = cb.createQuery(DespatchItemDTO.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, Job> job = root.join(JobItem_.job, JoinType.INNER);
			Join<JobItem, ItemState> itemState = root.join(JobItem_.state, JoinType.INNER);
			Join<Job, Address> destAddress = job.join(Job_.returnTo, JoinType.INNER);
			Join<JobItem, TransportOption> transportOption = root.join(JobItem_.returnOption, JoinType.INNER);
			Join<ItemState, StateGroupLink> grplinks = itemState.join(ItemState_.groupLinks, JoinType.LEFT);
			Join<JobItem, Address> currentAddress = root.join(JobItem_.currentAddr);
			Join<Address, Subdiv> currentSubdiv = currentAddress.join(Address_.sub);
			Join<Address, Subdiv> destSubdiv = destAddress.join(Address_.sub);
			Join<Subdiv, Company> destCompany = destSubdiv.join(Subdiv_.comp);
			Join<JobItem, Instrument> instrument = root.join(JobItem_.inst);
			Join<Instrument, Mfr> instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model);
			Join<InstrumentModel, Description> subFamily = instrumentModel.join(InstrumentModel_.description);
			Join<InstrumentModel, Translation> instrumentModelTranslation = instrumentModel
					.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			
			Subquery<Long> accessoriesCountSq = cq.subquery(Long.class);
			Root<ItemAccessory> accessoriesCountSqRoot = accessoriesCountSq.from(ItemAccessory.class);
			Join<ItemAccessory, JobItem> jobItem = accessoriesCountSqRoot.join(ItemAccessory_.ji);
			accessoriesCountSq.where(cb.equal(root.get(JobItem_.jobItemId), jobItem.get(JobItem_.jobItemId)));
			accessoriesCountSq.distinct(true);
			accessoriesCountSq.select(cb.count(accessoriesCountSqRoot));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(currentSubdiv.get(Subdiv_.subdivid), allocatedSubdivId));
			clauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));
			clauses.getExpressions().add(cb.equal(instrumentModelTranslation.get(Translation_.locale), locale));
			clauses.getExpressions().add(grplinks.get(StateGroupLink_.groupId).in(stateGroups));
			
			cq.where(clauses);
			
			cq.orderBy(cb.asc(root.get(JobItem_.itemNo)), cb.asc(job.get(Job_.jobno)));
			cq.select(cb.construct(DespatchItemDTO.class, job.get(Job_.jobid), job.get(Job_.jobno),
					root.get(JobItem_.jobItemId), root.get(JobItem_.itemNo), itemState.get(ItemState_.stateid),
					currentAddress.get(Address_.addrid), transportOption.get(TransportOption_.id),
					destAddress.get(Address_.addrid), destSubdiv.get(Subdiv_.subdivid), destSubdiv.get(Subdiv_.subname),
					destCompany.get(Company_.coid), destCompany.get(Company_.coname),
					instrumentModelTranslation.get(Translation_.translation),
					instrumentModel.get(InstrumentModel_.modelMfrType), subFamily.get(Description_.typology),
					instrumentMfr.get(Mfr_.name), instrument.get(Instrument_.modelname), root.get(JobItem_.dueDate),
					accessoriesCountSq.getSelection()));
			return cq;
		});
	}

	@Override
	public List<DeliveryDTO> getDeliveryDtosForIds(Collection<Integer> deliveryIds) {
		if (deliveryIds == null || deliveryIds.isEmpty())
			return Collections.emptyList();
		return getResultList(cb -> {
			CriteriaQuery<DeliveryDTO> cq = cb.createQuery(DeliveryDTO.class);
			Root<Delivery> root = cq.from(Delivery.class);
			// Below are destination / recipient entities
			// (Delivery technically allows freehand contact/address in data
			// model, so left
			// join needed)
			Join<Delivery, Contact> destContact = root.join(Delivery_.contact, JoinType.LEFT);
			Join<Delivery, Address> destAddress = root.join(Delivery_.address, JoinType.LEFT);
			Join<Delivery, CourierDespatch> courierDespatch = root.join(Delivery_.crdes, JoinType.LEFT);
			Join<Delivery, Contact> createdByContact = root.join(Delivery_.createdby, JoinType.LEFT);
			Join<Delivery, Subdiv> sourceSubdiv = root.join(Delivery_.organisation.getName(), JoinType.INNER);
			Join<Subdiv, Company> sourceCompany = sourceSubdiv.join(Subdiv_.comp, JoinType.INNER);

			cq.where(root.get(Delivery_.deliveryid).in(deliveryIds));
			cq.select(cb.construct(DeliveryDTO.class, root.get(Delivery_.deliveryid), root.type(),
					root.get(Delivery_.deliveryno), courierDespatch.get(CourierDespatch_.crdespid),
					createdByContact.get(Contact_.personid), sourceSubdiv.get(Subdiv_.subdivid),
					sourceCompany.get(Company_.coid), destAddress.get(Address_.addrid),
					destContact.get(Contact_.personid), root.get(Delivery_.deliverydate)));

			return cq;
		});
	}

	@Override
	public List<DespatchItemDTO> getGoodsOutDtos(List<Integer> stateGroupIds, int allocatedSubdivId, Locale locale) {
		if (stateGroupIds == null)
			throw new UnsupportedOperationException("stateGroups may not be null");
		if (stateGroupIds.isEmpty())
			throw new UnsupportedOperationException("stateGroups may not be empty");
		return getResultList(cb -> {
			CriteriaQuery<DespatchItemDTO> cq = cb.createQuery(DespatchItemDTO.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, ItemState> itemState = root.join(JobItem_.state);

			Join<JobItem, Job> job = root.join(JobItem_.job);
			Join<JobItem, Address> currentAddress = root.join(JobItem_.currentAddr);
			Join<Address, Subdiv> currentSubdiv = currentAddress.join(Address_.sub);
			Join<JobItem, TransportOption> transportOut = root.join(JobItem_.returnOption, JoinType.LEFT);
			// TODO determine third party / intersubdiv address - this only
			// works for client
			// deliveries!
			Join<Job, Address> destAddress = job.join(Job_.returnTo, JoinType.LEFT);
			Join<Address, Subdiv> destSubdiv = destAddress.join(Address_.sub);
			Join<Subdiv, Company> destCompany = destSubdiv.join(Subdiv_.comp);
			Join<JobItem, Instrument> instrument = root.join(JobItem_.inst);
			Join<Instrument, Mfr> instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model);
			Join<InstrumentModel, Description> subFamily = instrumentModel.join(InstrumentModel_.description);
			Join<InstrumentModel, Translation> instrumentModelTranslation = instrumentModel
					.join(InstrumentModel_.nameTranslations, JoinType.LEFT);

			Subquery<Integer> sq = cq.subquery(Integer.class);
			Root<StateGroupLink> sqRoot = sq.from(StateGroupLink.class);
			Join<StateGroupLink, ItemState> sqItemState = sqRoot.join(StateGroupLink_.state);
			sq.where(sqRoot.get(StateGroupLink_.groupId).in(stateGroupIds));
			sq.distinct(true);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(currentSubdiv.get(Subdiv_.subdivid), allocatedSubdivId));
			clauses.getExpressions().add(cb.equal(instrumentModelTranslation.get(Translation_.locale), locale));
			clauses.getExpressions()
					.add(itemState.get(ItemState_.stateid).in(sq.select(sqItemState.get(ItemState_.stateid))));

			cq.where(clauses);

			cq.select(cb.construct(DespatchItemDTO.class, job.get(Job_.jobid), job.get(Job_.jobno),
					root.get(JobItem_.jobItemId), root.get(JobItem_.itemNo), itemState.get(ItemState_.stateid),
					currentAddress.get(Address_.addrid), transportOut.get(TransportOption_.id),
					destAddress.get(Address_.addrid), destSubdiv.get(Subdiv_.subdivid), destSubdiv.get(Subdiv_.subname),
					destCompany.get(Company_.coid), destCompany.get(Company_.coname),
					instrumentModelTranslation.get(Translation_.translation),
					instrumentModel.get(InstrumentModel_.modelMfrType), subFamily.get(Description_.typology),
					instrumentMfr.get(Mfr_.name), instrument.get(Instrument_.modelname), root.get(JobItem_.dueDate)));

			return cq;
		});
	}

	@Override
	public List<? extends Delivery> findByJobItem(int jobItemId, DeliveryType type) {
		if (!JobDelivery.class.isAssignableFrom(type.getDeliveryClass()))
			throw new IllegalArgumentException();
		// JAVA's type system issue as any can see this cast is checked in the
		// line above
		@SuppressWarnings("unchecked")
		val clazz = (Class<? extends JobDelivery>) type.getDeliveryClass();
		// Intentionally returns all deliveries for job item, not restricted by
		// allocated subdiv
		return getResultList(cb -> {
			val cq = cb.createQuery(type.getDeliveryClass());
			val delivery = cq.from(clazz);
			cq.where(cb.equal(delivery.join(JobDelivery_.items).get(JobDeliveryItem_.jobitem), jobItemId));
			cq.orderBy(cb.desc(delivery.get(Delivery_.deliverydate)));
			return cq;
		});
	}

	private Stream<Predicate> datePredicate(CriteriaBuilder cb, Path<LocalDate> path, LocalDate date) {
		if (date != null)
			return Stream.of(cb.equal(path, date));
		return Stream.empty();
	}

	@Override
	public PagedResultSet<Delivery> queryDeliveries(DeliverySearchForm dsf, PagedResultSet<Delivery> rs) {
		completePagedResultSet(rs, Delivery.class, cb -> cq -> {
			val root = cq.from(dsf.getDeliveryType().getDeliveryClass());
			val subdiv = subdivService.get(dsf.getAllocatedSubdivId());
			// Java's poor type system
			@SuppressWarnings("unchecked")
			val delivery = (Root<Delivery>) root;
			val predicates = Stream
					.of(Stream.of(cb.equal(delivery.get(Delivery_.organisation), subdiv)),
							dsf.getDeliverydate() != null ? datePredicate(cb, delivery.get(Delivery_.deliverydate),
									dsf.getDeliverydate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
									: Stream.<Predicate>empty(),
							StringUtils.isNotBlank(dsf.getDelno())
									? ilikePredicateList(cb, delivery.get(Delivery_.deliveryno), dsf.getDelno(),
											MatchMode.ANYWHERE).stream()
									: Stream.<Predicate>empty(),
							StringUtils.isNotBlank(dsf.getConsignmentNo()) ? ilikePredicateList(cb,
									delivery.join(Delivery_.crdes).get(CourierDespatch_.consignmentno),
									dsf.getConsignmentNo(), MatchMode.ANYWHERE).stream() : Stream.<Predicate>empty(),
							StringUtils.isNotBlank(dsf.getCoid())
									? Stream.of(cb.equal(delivery.join(Delivery_.contact).join(Contact_.sub)
											.get(Subdiv_.comp).get(Company_.coid), dsf.getCoid()))
									: Stream.<Predicate>empty()

					).flatMap(Function.identity()).toArray(Predicate[]::new);
			cq.where(cb.and(predicates));
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(delivery.get(Delivery_.deliverydate)));
			return Triple.of(delivery, delivery, order);
		});
		return rs;
	}

	@Override
	public void removeDeliveryFromSchedule(int delid) {
		Delivery delivery = this.find(delid);
		List<ScheduledDelivery> scheduledDeliveryList = scheduledDeliveryService
				.scheduledDeliveryFromScheduleAndOrDelivery(null, delivery.getDeliveryid());
		for (ScheduledDelivery scheduledDelivery : scheduledDeliveryList) {
			scheduledDeliveryService.delete(scheduledDelivery);
		}
	}

	@Override
	public List<ClientDelivery> getClientDeliveryListByJobitemId(Integer jobitemid) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ClientDelivery> query = builder.createQuery(ClientDelivery.class);
		Root<ClientDelivery> clientDeliveryRoot = query.from(ClientDelivery.class);
		Join<ClientDelivery, Job> jobJoin = clientDeliveryRoot.join(ClientDelivery_.job);
		Join<Job, JobItem> jobItemJoin = jobJoin.join(Job_.items);

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(builder.equal(jobItemJoin.get(JobItem_.jobItemId), jobitemid));

		query.where(conjunction);

		query.select(clientDeliveryRoot);

		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public List<RestCustomerDeliveryDto> findDeliveryDto(Subdiv subdiv, LocalDate dtStart, LocalDate dtEnd,
			DeliveryType deliveryType) {
		return getResultList(cb -> {
			CriteriaQuery<RestCustomerDeliveryDto> cq = cb.createQuery(RestCustomerDeliveryDto.class);
			Root<JobDelivery> deliveryRoot = cq.from(JobDelivery.class);
			Join<JobDelivery, Contact> contact = deliveryRoot.join(JobDelivery_.contact);
			deliveryRoot.join(JobDelivery_.items, JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			if (dtEnd != null && dtStart != null)
				clauses.getExpressions().add(cb.between(deliveryRoot.get(Delivery_.creationdate), dtStart, dtEnd));
			clauses.getExpressions().add(cb.equal(contact.get(Contact_.sub), subdiv));
			if (deliveryType != DeliveryType.UNDEFINED)
				clauses.getExpressions().add(cb.equal(deliveryRoot.type(), deliveryType.getDeliveryClass()));
			cq.where(clauses);
			cq.groupBy(deliveryRoot.get(JobDelivery_.deliveryid));
			return cq.select(cb.construct(RestCustomerDeliveryDto.class, deliveryRoot.get(JobDelivery_.deliveryid),
					cb.greatest(deliveryRoot.get(JobDelivery_.deliverydate)),
					cb.greatest(deliveryRoot.get(JobDelivery_.deliveryno)),
					cb.count(deliveryRoot.get(Delivery_.deliveryid))));
		});
	}

	@Override
	public List<RestDeliveryDetailFlatDto> findDeliveryDetailFlat(List<Integer> deliveriesId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<RestDeliveryDetailFlatDto> cq = cb.createQuery(RestDeliveryDetailFlatDto.class);
			Root<JobDelivery> deliveryRoot = cq.from(JobDelivery.class);
			Join<JobDelivery, JobDeliveryItem> jobDelItem = deliveryRoot.join(JobDelivery_.items, JoinType.LEFT);
			Join<JobDeliveryItem, JobItem> jobItems = jobDelItem.join(JobDeliveryItem_.jobitem);
			Join<JobItem, Job> jobs = jobItems.join(JobItem_.job);
			Join<JobItem, Instrument> instruments = jobItems.join(JobItem_.inst);
			Join<Instrument, Mfr> instrumentMfr = instruments.join(Instrument_.mfr, JoinType.LEFT);
			Join<Instrument, Mfr> instMfr = instruments.join(Instrument_.mfr, JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = instruments.join(Instrument_.model);
			Join<InstrumentModel, Mfr> modelMfr = model.join(InstrumentModel_.mfr);
			Join<InstrumentModel, Translation> modelname = model.join(InstrumentModel_.nameTranslations);
			modelname.on(cb.equal(modelname.get(Translation_.locale), locale));
			Join<InstrumentModel, Description> subFamily = model.join(InstrumentModel_.description);
			Join<Description, Translation> subFamilyName = subFamily.join(Description_.translations, JoinType.LEFT);
			subFamilyName.on(cb.equal(subFamilyName.get(Translation_.locale), locale));
			Join<Description, InstrumentModelFamily> family = subFamily.join(Description_.family);
			Join<InstrumentModelFamily, Translation> familyName = family.join(InstrumentModelFamily_.translation,
					JoinType.LEFT);
			familyName.on(cb.equal(familyName.get(Translation_.locale), locale));
			Join<JobItem, CalLink> calLinks = jobItems.join(JobItem_.calLinks, JoinType.LEFT);
			Join<JobItem, CertLink> certLinks = jobItems.join(JobItem_.certLinks, JoinType.LEFT);
			Join<CalLink, Calibration> calibrations = calLinks.join(CalLink_.cal, JoinType.LEFT);
			Join<Calibration, CalibrationType> calibrationTypes = calibrations.join(Calibration_.calType,
					JoinType.LEFT);
			Join<Calibration, CalibrationStatus> calibrationStatus = calibrations.join(Calibration_.status,
					JoinType.LEFT);
			Join<CalibrationType, ServiceType> serviceType = calibrationTypes.join(CalibrationType_.serviceType,
					JoinType.LEFT);
			Join<ServiceType, Translation> serviceTypeTranslation = serviceType.join(ServiceType_.longnameTranslation,
					JoinType.LEFT);
			serviceTypeTranslation.on(cb.equal(serviceTypeTranslation.get(Translation_.locale), locale));
			Join<CertLink, Certificate> certifcates = certLinks.join(CertLink_.cert, JoinType.LEFT);
			Path<Calibration> certP = certifcates.get(Certificate_.cal);
			Subquery<Integer> sq = cq.subquery(Integer.class);
			Root<JobDelivery> sqJobDelRoot = sq.from(JobDelivery.class);
			Expression<Integer> deliveriesIdQeury = sqJobDelRoot.get(JobDelivery_.deliveryid);
			Predicate clausesSub = cb.conjunction();
			clausesSub.getExpressions().add(deliveriesIdQeury.in(deliveriesId));
			sq.where(clausesSub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(deliveryRoot.in(sq.select(sqJobDelRoot.get(JobDelivery_.deliveryid))));
			cq.where(clauses);
			return cq.select(cb.construct(RestDeliveryDetailFlatDto.class, deliveryRoot.get(Delivery_.deliveryid),
					deliveryRoot.get(Delivery_.deliveryno), jobItems.get(JobItem_.jobItemId),
					jobItems.get(JobItem_.itemNo), jobs.get(Job_.jobno), instruments.get(Instrument_.plantid),
					instruments.get(Instrument_.plantno), instruments.get(Instrument_.serialno),
					instruments.get(Instrument_.customerDescription),
					instruments.get(Instrument_.customerSpecification), modelname.get(Translation_.translation),
					cb.coalesce(modelMfr.get(Mfr_.name), instMfr.get(Mfr_.name)), model.get(InstrumentModel_.model),
					instrumentMfr.get(Mfr_.name), instruments.get(Instrument_.modelname),
					familyName.get(Translation_.translation), subFamilyName.get(Translation_.translation),
					instruments.get(Instrument_.nextCalDueDate), instruments.get(Instrument_.calFrequency),
					instruments.get(Instrument_.calFrequencyUnit), calibrations.get(Calibration_.id),
					calibrations.get(Calibration_.calClass), calibrations.get(Calibration_.startTime),
					calibrations.get(Calibration_.completeTime), calibrationStatus.get(CalibrationStatus_.name),
					serviceTypeTranslation.get(Translation_.translation), certifcates.get(Certificate_.certid),
					certifcates.get(Certificate_.certno), certifcates.get(Certificate_.certDate),
					certifcates.get(Certificate_.calDate), certP.get(Calibration_.id),
					certifcates.get(Certificate_.calibrationVerificationStatus),
					deliveryRoot.get(Delivery_.lastModified), jobItems.get(JobItem_.lastModified),
					jobs.get(Job_.lastModified), calibrations.get(Calibration_.lastModified),
					certifcates.get(Certificate_.lastModified), instruments.get(Instrument_.lastModified)));
		});
	}
	/*
	 * private String manufacturer; private String model; private String
	 * instrumentMfr; private String instrumentMfrModel; private String family;
	 * private String subFamily;
	 */

	@Override
	public List<JobDelivery> findDeliveries(List<Integer> deliveriesId) {
		return getResultList(cb -> {
			CriteriaQuery<JobDelivery> cq = cb.createQuery(JobDelivery.class);
			Root<JobDelivery> deliveryRoot = cq.from(JobDelivery.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(deliveryRoot.in(deliveriesId));
			cq.where(clauses);
			return cq.distinct(true);
		});
	}

	@Override
	public Optional<OrderProjection> getOrderProjectionForDeliveryNote(Integer deliveryId) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(OrderProjection.class);
			val delivery = cq.from(JobDelivery.class);
			val job = delivery.join(JobDelivery_.job);
			val contact = delivery.join(JobDelivery_.contact);
			val contactCompany = contact.join(Contact_.sub).join(Subdiv_.comp);
			val contactCompanyAddress = contactCompany.join(Company_.legalAddress);
			val deliveryOrganisation = delivery.<JobDelivery, Subdiv>join(Delivery_.organisation.getName());
			val deliveryOrganisationCompany = deliveryOrganisation.join(Subdiv_.comp);
			val deliveryOrganisationAddress = deliveryOrganisationCompany.join(Company_.legalAddress);
			cq.where(cb.equal(delivery, deliveryId));
			cq.select(cb.construct(OrderProjection.class, delivery.get(JobDelivery_.deliveryid),
					delivery.get(JobDelivery_.deliveryno), contactCompany.get(Company_.coid),
					contactCompany.get(Company_.coname), contactCompanyAddress.get(Address_.addr1),
					contactCompanyAddress.get(Address_.addr2), contactCompanyAddress.get(Address_.addr3),
					contactCompanyAddress.get(Address_.town),
					contactCompanyAddress.join(Address_.country).get(Country_.countryCode),
					StringJpaUtils.trimAndConcatWithWhitespace(contact.get(Contact_.firstName),
							contact.get(Contact_.lastName)).apply(cb),
					contactCompanyAddress.get(Address_.postcode), contact.get(Contact_.telephone),
					contact.get(Contact_.fax), contact.get(Contact_.email), delivery.get(JobDelivery_.deliverydate),
					deliveryOrganisation.get(Subdiv_.subname), deliveryOrganisationCompany.get(Company_.coname),
					deliveryOrganisationAddress.get(Address_.addr1), deliveryOrganisationAddress.get(Address_.addr2),
					deliveryOrganisationAddress.get(Address_.addr3), deliveryOrganisationAddress.get(Address_.town),
					deliveryOrganisationAddress.join(Address_.country).get(Country_.countryCode),
					deliveryOrganisationAddress.get(Address_.postcode), job.get(Job_.jobno)));
			return cq;
		});
	}

	@Override
	public Long deliveryCountByCourier(Integer courierId) {
		return getFirstResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Delivery> root = cq.from(Delivery.class);
			Join<Delivery, CourierDespatch> cdJoin = root.join(Delivery_.crdes);
			Join<CourierDespatch, CourierDespatchType> cdtJoin = cdJoin.join(CourierDespatch_.cdtype);
			Join<CourierDespatchType, Courier> courierJoin = cdtJoin.join(CourierDespatchType_.courier);
			cq.where(cb.equal(courierJoin.get(Courier_.courierid), courierId));
			cq.select(cb.count(root));
			return cq;
		}).orElse(0L);
	}

	@Override
	public Long deliveryCountByCourierDespatchType(Integer cdtId) {
		return getFirstResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Delivery> root = cq.from(Delivery.class);
			Join<Delivery, CourierDespatch> cdJoin = root.join(Delivery_.crdes);
			Join<CourierDespatch, CourierDespatchType> cdtJoin = cdJoin.join(CourierDespatch_.cdtype);
			cq.where(cb.equal(cdtJoin.get(CourierDespatchType_.cdtid), cdtId));
			cq.select(cb.count(root));
			return cq;
		}).orElse(0L);
	}

	@Override
	public boolean isLastTPDelivery(JobItem ji) {
		ThirdPartyDelivery tpd = getFirstResult(cb -> {
			val cq = cb.createQuery(ThirdPartyDelivery.class);
			val delivery = cq.from(ThirdPartyDelivery.class);
			val deliveryItems = delivery.join(ThirdPartyDelivery_.items);

			val sq = cq.subquery(Integer.class);
			val sqRoot = sq.from(JobDelivery.class);
			val sqItems = sqRoot.join(JobDelivery_.items);
			sq.where(cb.equal(sqItems.get(JobDeliveryItem_.jobitem), ji));
			sq.select(cb.max(sqRoot.get(JobDelivery_.deliveryid)));

			cq.where(cb.and(cb.equal(deliveryItems.get(JobDeliveryItem_.jobitem), ji),
					cb.and(cb.isNotNull(sq.getSelection()),
							delivery.get(ThirdPartyDelivery_.deliveryid).in(sq.getSelection()))));
			return cq;
		}).orElse(null);
		return tpd != null;
	}

	@Override
	public List<Integer> getdeliveriesIdsByJob(Integer jobId) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Integer.class);
			val delivery = cq.from(JobDelivery.class);
			cq.where(cb.equal(delivery.join(JobDelivery_.items, JoinType.LEFT)
					.join(JobDeliveryItem_.jobitem, JoinType.LEFT).get(JobItem_.job).get(Job_.jobid), jobId));
			cq.select(delivery.get(JobDelivery_.DELIVERYID));
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public String deliveryExists(String DeliveryNo) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(String.class);
			val delivery = cq.from(Delivery.class);
			cq.where(cb.equal(delivery.get(Delivery_.deliveryno), DeliveryNo));
			return cq.select(delivery.get(Delivery_.DELIVERYNO));
		}).orElse(null);
	}

	@Override
	public Address getDeliveryAddressByDeliveryNo(String deliveryNo) {
		return getSingleResult(cb->{
			CriteriaQuery<Address> cq = cb.createQuery(Address.class);
			Root<Delivery> root = cq.from(Delivery.class);
			cq.where(cb.equal(root.get(Delivery_.deliveryno), deliveryNo));
			return cq.select(root.get(Delivery_.address));
			
		});
	}

}