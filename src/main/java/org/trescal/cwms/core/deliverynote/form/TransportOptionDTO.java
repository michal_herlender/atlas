package org.trescal.cwms.core.deliverynote.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TransportOptionDTO {
	private Integer optionId;
	private String countryCode;
	private String companyCode;
	private String subdivCode;
	private Integer subdivId;
	private Boolean active;
	private Integer dayOfWeek;
	private String method;
	private String methodTranslation;
	private String localizedName;

	public TransportOptionDTO(Integer optionId, String countryCode, String companyCode, String subdivCode, Integer subdivId, 
			Boolean active, Integer dayOfWeek, String method, String methodTranslation, String localizedName) {
		super();
		this.optionId = optionId;
		this.countryCode = countryCode;
		this.companyCode = companyCode;
		this.subdivCode = subdivCode;		
		this.subdivId = subdivId;
		this.active = active;
		this.dayOfWeek = dayOfWeek;
		this.method = method;
		this.methodTranslation = methodTranslation;
		this.localizedName = localizedName;
	}

}
