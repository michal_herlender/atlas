package org.trescal.cwms.core.deliverynote.form;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;

import lombok.Data;

@Data
public class CreateDeliveryNoteForm {
    private String action;
    private Integer addressid;
    private boolean allowsPartDeliveries;
    private String clientref;
    private Contact contact;
    private Integer contactid;
    private int crdesid;
    private String delInstruction;
    private Delivery delivery;
    private LocalDate deliverydate;
    private LocalDateTime destinationReceiptDate;
    private List<CourierDespatch> despatches;
    private boolean hasRemainingJobItems;
    private Job job;
    private int locid;
    private LocalDate newDeliveryDate;
    private String ourref;
    private boolean tp;
    private Integer addrid;
    private OnBehalfItem onBehalf;
    private Set<Contact> contacts;

    private LocalDateTime currentDate = LocalDateTime.now(LocaleContextHolder.getTimeZone().toZoneId());

    /**
     * @return the delivery
     */
    @Valid
    public Delivery getDelivery() {
        return this.delivery;
    }

}