package org.trescal.cwms.core.deliverynote.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class SelectTPContactValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(SelectTPContactForm.class);
	}
	
	@Override
	public void validate(Object obj, Errors errors)
	{
		super.validate(obj, errors);
		SelectTPContactForm form = (SelectTPContactForm) obj;
		if (((form.getPersonid() == null) || (form.getPersonid() == 0)) && !form.isFreehand())
		{
			errors.rejectValue("personid", "error.form.personid", null, "A contact must be chosen to continue creating this delivery note.");
		}
	}
}