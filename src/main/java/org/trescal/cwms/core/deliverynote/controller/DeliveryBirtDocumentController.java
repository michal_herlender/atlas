package org.trescal.cwms.core.deliverynote.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.DeliveryFileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Controller @IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class DeliveryBirtDocumentController {
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private DocumentService documentService; 

	/**
	 * Handler to generate NEW (July 2020) format of delivery note
	 * Currently, just the client delivery note is supported, 
	 * so the request should only be made for the client delivery note
	 */
	@RequestMapping(value="/birtnewdelnote.htm", method=RequestMethod.GET)
	public String handleNewRequest(Locale locale, Model model,
			@RequestParam(value="delid", required=true) Integer delId,
			@RequestParam(name="showCertificates", required=false, defaultValue="true") Boolean showCertificates,
			@RequestParam(name="showDateReceived", required=false, defaultValue="true") Boolean showDateReceived,
			@RequestParam(name="showPurchaseOrder", required=false, defaultValue="true") Boolean showPurchaseOrder,
			@RequestParam(name="showTotalPrice", required=false, defaultValue="true") Boolean showTotalPrice,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception {
		Delivery delivery = deliveryService.get(delId);
		this.deliveryService.setDirectoryUsingComponent(delivery);
		FileNamingService fnService = new DeliveryFileNamingService(delivery, delivery.getDirectory().getAbsolutePath());
		if (!delivery.getType().equals(DeliveryType.CLIENT))
			throw new UnsupportedOperationException("Only the client delivery note is currently supported");
		BaseDocumentType bdType = BaseDocumentType.DELIVERY_PROJECTION;
		Component component = getComponent(delivery);
		Map<String,Object> additionalParameters = new HashMap<>();
		additionalParameters.put("showCertificates", showCertificates);
		additionalParameters.put("showDateReceived", showDateReceived);
		additionalParameters.put("showPurchaseOrder", showPurchaseOrder);
		additionalParameters.put("showTotalPrice", showTotalPrice);
		
		Document doc = documentService.createBirtDocument(delId, bdType, locale, component, fnService, additionalParameters);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		model.asMap().clear();
		
		if (delivery instanceof GeneralDelivery)
			return "redirect:gendelnotesummary.htm?delid=" + delId + "&loadtab=files-tab";
		else
			return "redirect:viewdelnote.htm?delid=" + delId + "&loadtab=files-tab";
	}
	
	/**
	 * Handler for the OLD format of delivery note
	 * Probably, we will have to keep this for a while.
	 */
	@RequestMapping(value="/birtdelnote.htm", method=RequestMethod.GET)
	public String handleOldRequest(Locale locale,
			@RequestParam(value="delid", required=true) Integer delId,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception {
		Delivery delivery = deliveryService.get(delId);
		this.deliveryService.setDirectoryUsingComponent(delivery);
		FileNamingService fnService = new DeliveryFileNamingService(delivery, delivery.getDirectory().getAbsolutePath());
		BaseDocumentType bdType = getBaseDocumentType(delivery);
		Component component = getComponent(delivery);
		Document doc = documentService.createBirtDocument(delId, bdType, locale, component, fnService);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		
		if (delivery instanceof GeneralDelivery)
			return "redirect:gendelnotesummary.htm?delid=" + delId + "&loadtab=files-tab";
		else
			return "redirect:viewdelnote.htm?delid=" + delId + "&loadtab=files-tab";
	}

	private BaseDocumentType getBaseDocumentType(Delivery delivery) {
		if (delivery.isGeneralDelivery()) 
			return BaseDocumentType.GENERAL_DELIVERY;
		else if (delivery.isThirdPartyDelivery())
			return BaseDocumentType.THIRD_PARTY_DELIVERY;
		else if (delivery.isClientDelivery()) 
			return BaseDocumentType.CLIENT_DELIVERY;
		
		else
			return BaseDocumentType.INTERNAL_DELIVERY;
	}
	
	private Component getComponent(Delivery delivery) {
		if (delivery.isGeneralDelivery())
			return Component.GENERAL_DELIVERY;
		else
			return Component.DELIVERY;
	}
}
