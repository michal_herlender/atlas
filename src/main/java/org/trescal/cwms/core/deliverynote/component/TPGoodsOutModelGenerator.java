package org.trescal.cwms.core.deliverynote.component;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.dto.CompanySettingsDTO;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.deliverynote.dto.DespatchItemDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionResult;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.service.DeliveryItemProjectionService;
import org.trescal.cwms.core.deliverynote.projection.service.DeliveryProjectionService;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Component
public class TPGoodsOutModelGenerator {
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private DeliveryProjectionService deliveryProjectionService;
	@Autowired
	private DeliveryItemProjectionService deliveryItemProjectionService;
	@Autowired
	private ItemStateService itemStateService; 
	@Autowired
	private TransportOptionService transOptServ;

	public GoodsOutDataModel getTPGoodsOutDataModel(List<StateGroup> stateGroups, int allocatedSubdivId, 
			int allocatedCompanyId, Locale locale) {
		
		List<Integer> stateGroupIds = stateGroups.stream().map(StateGroup::getId).collect(Collectors.toList());
		
		List<DespatchItemDTO> despatchItems = this.deliveryService.getTpGoodsOutItems(stateGroupIds, allocatedSubdivId, locale);
		
		Set<Integer> sourceAddressIds = despatchItems.stream()
				.filter(dto -> dto.getSourceAddressId() != null)
				.map(DespatchItemDTO::getSourceAddressId).collect(Collectors.toSet()); 
		Set<Integer> destAddressIds = despatchItems.stream()
				.filter(dto -> dto.getDestAddressId() != null)
				.map(DespatchItemDTO::getDestAddressId).collect(Collectors.toSet());
		Set<Integer> companyIds = despatchItems.stream()
				.map(DespatchItemDTO::getDestCompanyId).collect(Collectors.toSet());
						
		List<KeyValue<Integer, String>> sourceAddressDtos = this.addressService.getAddressesKeyValue(sourceAddressIds);
		List<KeyValue<Integer, String>> destAddressDtos = this.addressService.getAddressesKeyValue(destAddressIds);
		Set<KeyValueIntegerString> subdivTransportOptions = this.transOptServ.getTransportOptionDtosForSubdivs(false, true, locale, Collections.singleton(allocatedSubdivId));
		List<ItemState> itemStates = this.itemStateService.getAllForStateGroups(stateGroupIds);
		List<CompanySettingsDTO> companySettings = this.companySettingsService.getDtosForAllocatedCompany(companyIds, allocatedCompanyId);
		
		GoodsOutDataModel result = new GoodsOutDataModel();
		result.addSourceAddressDtos(sourceAddressDtos);
		result.addDestAddressDtos(destAddressDtos);
		result.addSubdivTransportOptions(subdivTransportOptions);
		result.addStateGroups(stateGroups, itemStates);
		result.addCompanySettingsData(companySettings);
		result.addDespatchItemDtos(despatchItems);
		
		// we need to get deliveries only for the tp despatch case
		Set<Integer> jobItemIds = despatchItems.stream().filter(dto -> result.isItemStateInTPDespatch(dto.getItemStateId()))
				.map(DespatchItemDTO::getJobItemId).collect(Collectors.toSet());
		if(!jobItemIds.isEmpty()){
			DeliveryItemProjectionCommand dipCommand = new DeliveryItemProjectionCommand(locale);
			DeliveryItemProjectionResult dipResult = this.deliveryItemProjectionService.getDeliveryItemDTOs(jobItemIds, dipCommand);
			List<DeliveryItemDTO> deliveryItems = dipResult.getDeliveryItems();
			Set<Integer> deliveryIds = deliveryItems.stream()
					.map(DeliveryItemDTO::getDeliveryId).collect(Collectors.toSet());
			DeliveryProjectionCommand deliveryCommand = new DeliveryProjectionCommand(allocatedCompanyId); 
			List<DeliveryDTO> deliveries = this.deliveryProjectionService.getDeliveryDtosForIds(deliveryIds, deliveryCommand);
			// get only deliveries with Third Party type
			deliveries.removeIf(dto -> !dto.getType().equals(DeliveryType.THIRDPARTY));
			result.addDeliveryItems(deliveryItems, deliveries);
		}
		
		return result;
	}

}
