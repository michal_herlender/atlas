package org.trescal.cwms.core.deliverynote.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliverydestination.DeliveryDestination;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.db.DeliveryNoteService;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNotePlainForm;
import org.trescal.cwms.core.deliverynote.form.DeliveryNoteValidator;
import org.trescal.cwms.core.deliverynote.form.JobItemForDeliveryNoteDTO;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.PartDelivery;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.extern.slf4j.Slf4j;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
@Slf4j
public class DeliveryNoteController {

	@Autowired
	private DeliveryNoteService deliveryNoteService;
	@Autowired
	private InstructionService instructionService;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContactService conService;
	@Autowired
	private DeliveryService delServ;
	@Autowired
	private DeliveryNoteValidator validator;
	@Autowired
	private PartDelivery partDelivery;
	@Autowired
	private TransportOptionService transportOptionService;

	@RequestMapping(value = "/deliveryNoteInstructions.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public List<InstructionDTO> getDeliveryNoteInstructions(
			@RequestParam(name = "jobItemIds[]", required = false) List<Integer> jobItemIds,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto) {
		if (jobItemIds != null) {
			return instructionService.findAllByJobItems(jobItemIds, subdivDto.getKey(), InstructionType.CARRIAGE,
					InstructionType.PACKAGING);
		} else
			return new ArrayList<>();
	}

	@RequestMapping(value = "/deliveryNoteItemsOnJob.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public List<JobItemForDeliveryNoteDTO> getDeliveryNoteItemsOnJob(@RequestParam(value = "jobId") Integer jobId,
			@RequestParam(value = "destination") DeliveryDestination deliveryDestination,
			@RequestParam(value = "jobItemId", required = false) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto) {
		return deliveryNoteService.getJobItemsForDeliveryNote(jobId, jobItemId, deliveryDestination,
				subdivDto.getKey());
	}

	@RequestMapping(value = "/createDeliveryNote.json", method = { RequestMethod.GET,
			RequestMethod.POST }, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RestResultDTO<?> createDeliveryNote(@Validated @RequestBody CreateDeliveryNotePlainForm form,
			BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) throws Exception {
		log.info("Result has errors: " + bindingResult.hasErrors());
		form.getItems().forEach(i -> log.info("" + i));
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		form.setAllowsPartDeliveries(true);
		if (!ObjectUtils.isEmpty(form.getContactid())) {
			KeyValue<Scope, String> partDeliveryValue = partDelivery.getValueHierarchical(Scope.CONTACT,
					conService.get(form.getContactid()), allocatedSubdiv.getComp());
			form.setAllowsPartDeliveries(partDelivery.parseValue(partDeliveryValue.getValue()));
		}
		validator.validate(form, bindingResult);
		if (!bindingResult.hasErrors()) {
			// create Delivery Note
			JobDelivery delivery = this.delServ.createDelivery(form, username, allocatedSubdiv);
			// create schedule if all items have the same scheduled transport option
			try {
				this.transportOptionService.automaticallyAddDeliveryNoteToTransportSchedule(subdivDto.getKey(), delivery,
						username, form.getItems());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new RestResultDTO<>(true, "viewdelnote.htm?delid=" + delivery.getDeliveryid());
		} else
			return errorConverter.createResultDTO(bindingResult, locale);
	}
}