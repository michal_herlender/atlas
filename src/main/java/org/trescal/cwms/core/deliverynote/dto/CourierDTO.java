package org.trescal.cwms.core.deliverynote.dto;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class CourierDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer courierid;
	private Boolean defaultCourier;
	private String name;
	private String webTrackURL;
	private Set<CourierDespatchTypeDTO> cdts;
	
}