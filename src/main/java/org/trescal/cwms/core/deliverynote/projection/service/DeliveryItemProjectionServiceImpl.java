package org.trescal.cwms.core.deliverynote.projection.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemDao;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.db.DeliveryItemAccessoryService;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemAccessoryDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionResult;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.db.TPRequirementService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionResult;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionService;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;
import org.trescal.cwms.core.system.projection.note.service.NoteProjectionService;

import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class DeliveryItemProjectionServiceImpl implements DeliveryItemProjectionService {

	@Autowired
	private DeliveryItemAccessoryService deliveryItemAccessoryService;
	@Autowired
	private DeliveryItemDao deliveryItemDao;
	@Autowired
	private JobItemProjectionService jobItemProjectionService;
	@Autowired
	private TPRequirementService tpRequirementService;
	@Autowired
	private NoteProjectionService noteProjectionService;
	
	@Override
	public DeliveryItemProjectionResult getDeliveryItemDTOs(Collection<Integer> jobItemIds, DeliveryItemProjectionCommand command) {
		DeliveryItemProjectionResult result = new DeliveryItemProjectionResult();
		List<DeliveryItemDTO> deliveryItems = Collections.emptyList();
		if (!jobItemIds.isEmpty()) {
			deliveryItems = this.deliveryItemDao.getDeliveryItemDTOs(null, jobItemIds);
			if (command.getLoadTpRequirements()) {
				List<TPRequirementDTO> tpRequirements = loadProjectionTpRequirements(deliveryItems);
				result.setTpRequirements(tpRequirements);
			}
			if (command.getLoadAccessories()) {
				throw new UnsupportedOperationException("Accessory loading not implemented for job items");
			}
			if (command.getLoadNotes()) {
				throw new UnsupportedOperationException("Note loading not implemented for job items");
			}
		}
		result.setDeliveryItems(deliveryItems);
		return result;
	}

	@Override
	public DeliveryItemProjectionResult getDeliveryItemDTOs(Integer deliveryId, DeliveryItemProjectionCommand command) {
		DeliveryItemProjectionResult result = new DeliveryItemProjectionResult();
		List<DeliveryItemDTO> deliveryItems = this.deliveryItemDao.getDeliveryItemDTOs(deliveryId, null);
		if (command.getLoadTpRequirements()) {
			List<TPRequirementDTO> tpRequirements =loadProjectionTpRequirements(deliveryItems);
			result.setTpRequirements(tpRequirements);
		}
		if (command.getLoadAccessories()) {
			List<DeliveryItemAccessoryDTO> accessories = loadProjectionAccessoryDTOs(deliveryId, deliveryItems, command.getLocale());
			result.setItemAccessories(accessories);
		}
		if (command.getLoadNotes()) 
			loadNotes(deliveryId, deliveryItems);
		result.setDeliveryItems(deliveryItems);
		return result;
	}
	
	@Override
	public DeliveryItemProjectionResult getDTOsByJobItems(Collection<JobItemProjectionDTO> jiDtos) {
		DeliveryItemProjectionResult result = new DeliveryItemProjectionResult();
		List<DeliveryItemDTO> deliveryItems = Collections.emptyList();
		if (!jiDtos.isEmpty()) {
			Map<Integer, JobItemProjectionDTO> mapByJobItemId = jiDtos.stream()
					.collect(Collectors.toMap(JobItemProjectionDTO::getJobItemId, i -> i));
			Set<Integer> jobItemIds = mapByJobItemId.keySet();
			
			deliveryItems = this.deliveryItemDao.getDeliveryItemDTOs(null, jobItemIds);
			for (DeliveryItemDTO diDto : deliveryItems) {
				JobItemProjectionDTO jiDto = mapByJobItemId.get(diDto.getJobItemId());
				diDto.setJobItem(jiDto);
				if (jiDto.getDeliveryItems() == null)
					jiDto.setDeliveryItems(new ArrayList<>());
				jiDto.getDeliveryItems().add(diDto);
			}
		}
		
		result.setDeliveryItems(deliveryItems);
		return result;
	}

	@Override
	public JobItemProjectionResult loadProjectionJobItems(List<DeliveryItemDTO> deliveryItems, JobItemProjectionCommand command) {
		Set<Integer> jobItemIds = deliveryItems.stream().map(dto -> dto.getJobItemId()).collect(Collectors.toSet());

		if (log.isDebugEnabled()) log.debug("jobItemIds.size() : "+jobItemIds.size());
		JobItemProjectionResult jipResult = this.jobItemProjectionService.getProjectionDTOsForJobItemIds(jobItemIds, command);
		
		List<JobItemProjectionDTO> jiDtos = jipResult.getJiDtos(); 
		if (log.isDebugEnabled()) log.debug("jiDtos.size() : "+jiDtos.size());
		
		Map<Integer, JobItemProjectionDTO> jobItemMap = jiDtos.stream()
				.collect(Collectors.toMap(jiDto -> jiDto.getJobItemId(), jiDto -> jiDto));
		deliveryItems.forEach(dItemDto -> dItemDto.setJobItem(jobItemMap.get(dItemDto.getJobItemId())));
		return jipResult;
	}

	private List<TPRequirementDTO> loadProjectionTpRequirements(List<DeliveryItemDTO> deliveryItems) {
		Set<Integer> tpRequirementIds = deliveryItems.stream().map(diDto -> diDto.getTpRequirementId())
				.collect(Collectors.toSet());
		List<TPRequirementDTO> tpRequirements = this.tpRequirementService
				.getProjectionDTOsForIds(tpRequirementIds);
		Map<Integer, TPRequirementDTO> tpRequirementMap = tpRequirements.stream()
				.collect(Collectors.toMap(tpDto -> tpDto.getId(), tpDto -> tpDto));
		deliveryItems.forEach(
				dItemDto -> dItemDto.setTpRequirement(tpRequirementMap.get(dItemDto.getTpRequirementId())));
		return tpRequirements;
	}

	@Override
	public List<DeliveryItemAccessoryDTO> loadProjectionAccessoryDTOs(Integer deliveryId, List<DeliveryItemDTO> deliveryItems, Locale locale) {
		List<DeliveryItemAccessoryDTO> accessoryDtos = this.deliveryItemAccessoryService.getProjectionDTOsForDeliveryId(deliveryId, locale);
		Map<Integer, DeliveryItemDTO> mapDeliveryItemsById = deliveryItems.stream()
				.collect(Collectors.toMap(diDto -> diDto.getDeliveryItemId(), diDto -> diDto));
		for (DeliveryItemAccessoryDTO accessoryDto : accessoryDtos) {
			DeliveryItemDTO diDto = mapDeliveryItemsById.get(accessoryDto.getDeliveryItemId());
			accessoryDto.setDeliveryItem(diDto);
			if (diDto.getAccessories() == null)
				diDto.setAccessories(new ArrayList<>());
			diDto.getAccessories().add(accessoryDto);
		}
		
		return accessoryDtos;
	}
	
	private void loadNotes(Integer deliveryId, List<DeliveryItemDTO> deliveryItems) {
		List<NoteProjectionDTO> notes = this.noteProjectionService.getDeliveryItemNotes(deliveryId);
		if (log.isDebugEnabled()) log.debug("notes.size() : "+notes.size());
		Map<Integer, DeliveryItemDTO> mapDeliveryItemsById = deliveryItems.stream()
				.collect(Collectors.toMap(diDto -> diDto.getDeliveryItemId(), diDto -> diDto));
		for (NoteProjectionDTO noteDto : notes) {
			DeliveryItemDTO diDto = mapDeliveryItemsById.get(noteDto.getEntityId());
			if (diDto.getNotes() == null) {
				diDto.setNotes(new ArrayList<>());
			}
			diDto.getNotes().add(noteDto);
		}
		
	}
	
}
