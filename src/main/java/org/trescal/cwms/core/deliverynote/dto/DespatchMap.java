package org.trescal.cwms.core.deliverynote.dto;

import java.util.Map;

public class DespatchMap<A,B>
{
	private int itemCount;
	private Map<A,B> map;

	public DespatchMap(int itemCount, Map<A,B> map)
	{
		this.itemCount = itemCount;
		this.map = map;
	}
	
	public int getItemCount()
	{
		return this.itemCount;
	}
	
	public Map<A,B> getMap()
	{
		return this.map;
	}
	
	public void incrementItemCount()
	{
		this.itemCount++;
	}

	public void setItemCount(int itemCount)
	{
		this.itemCount = itemCount;
	}

	public void setMap(Map<A,B> map)
	{
		this.map = map;
	}
}