package org.trescal.cwms.core.deliverynote.entity.courierdespatch.db;

import java.util.Collection;
import java.util.List;

import org.springframework.ui.Model;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchDTO;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.form.CourierDespatchSearchForm;
import org.trescal.cwms.core.deliverynote.projection.CourierDespatchProjectionDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

/**
 * Interface for accessing and manipulating {@link CourierDespatch} entities.
 * 
 * @author jamiev
 */
public interface CourierDespatchService
{
	/**
	 * Adds the given {@link Delivery} to a new {@link CourierDespatch}.
	 * 
	 * @param delivery the {@link Delivery} to add to a courier despatch
	 * @param cdt the {@link CourierDespatchType} to use
	 * @param consignmentNo the consignment number (if known)
	 * @return the new {@link CourierDespatch}
	 */
	CourierDespatch addCourierDespatchForDelivery(Delivery delivery, CourierDespatchType cdt, String consignmentNo, Contact creator, Subdiv allocatedSubdiv);

	void deleteCourierDespatch(CourierDespatch cd);

	/**
	 * Returns the {@link CourierDespatch} entity with the given ID.
	 * 
	 * @param id the {@link CourierDespatch} ID.
	 * @return the {@link CourierDespatch}
	 */
	CourierDespatch findCourierDespatch(int id);

	/**
	 * Returns all {@link CourierDespatch}(es) stored in the database.
	 * 
	 * @return the {@link List} of {@link CourierDespatch}(es).
	 */
	List<CourierDespatch> getAllCourierDespatches();

	/**
	 * Returns all {@link Delivery} entities corresponding to the
	 * {@link CourierDespatch} entity with the given ID.
	 * 
	 * @param cdid the ID of the {@link CourierDespatch} to list
	 *        {@link Delivery} entities for.
	 * @return the {@link List} of {@link Delivery} entities.
	 */
	List<Delivery> getDeliveriesForCourierDespatch(int cdid);

	/**
	 * Returns all {@link CourierDespatch}(es) stored in the database that were
	 * created within the last seven days.
	 * 
	 * @return the {@link List} of {@link CourierDespatch}(es).
	 */
	List<CourierDespatch> getDespatchesCreatedInLastWeek(Integer allocatedSubdivId);
	
	List<CourierDespatchProjectionDTO> getProjectionDTOs(Collection<Integer> despatchIds);

	/**
	 * Inserts the given {@link CourierDespatch} into the database.
	 * 
	 * @param courierdespatch the {@link CourierDespatch} to insert.
	 */
	void insertCourierDespatch(CourierDespatch courierdespatch);

	/**
	 * Returns all {@link CourierDespatch} results of a complex query customised
	 * by the user on the {@link CourierDespatchSearchForm}.
	 * 
	 * @param form the {@link CourierDespatchSearchForm} containing the query
	 *        criteria to build.
	 * @return the {@link List} of {@link CourierDespatchDTO}(es).
	 */
	 PagedResultSet<CourierDespatchDTO> queryDespatches(CourierDespatchSearchForm form, PagedResultSet<CourierDespatchDTO> rs);

	/**
	 * Updates the given {@link CourierDespatch} in the database.
	 * 
	 * @param courierdespatch the {@link CourierDespatch} to update.
	 */
	void updateCourierDespatch(CourierDespatch courierdespatch);
	
	void prepareCourierDespatchData(Integer subdivId, Integer companyId, Model model);
}