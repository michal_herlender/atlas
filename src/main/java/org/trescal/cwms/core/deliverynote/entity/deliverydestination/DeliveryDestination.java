package org.trescal.cwms.core.deliverynote.entity.deliverydestination;

public enum DeliveryDestination {
	CLIENT,
	THIRDPARTY,
	INTERNAL,
	INTERNAL_RETURN;
}