package org.trescal.cwms.core.deliverynote.entity.freehandcontact.db;

import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;

public interface FreehandContactService
{
	FreehandContact findFreehandContact(int id);
	void insertFreehandContact(FreehandContact freehandcontact);
	void updateFreehandContact(FreehandContact freehandcontact);
	List<FreehandContact> getAllFreehandContacts();
	void deleteFreehandContact(FreehandContact freehandcontact);
	void saveOrUpdateFreehandContact(FreehandContact freehandcontact);
}