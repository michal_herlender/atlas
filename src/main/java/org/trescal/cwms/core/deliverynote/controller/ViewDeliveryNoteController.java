package org.trescal.cwms.core.deliverynote.controller;

import static java.util.Collections.singletonList;
import static org.trescal.cwms.core.tools.DateTools.localDateTimeFromZonedDateTime;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNoteForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ClientDeliveryCertificates;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ClientDeliveryDateReceived;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ClientDeliveryPurchaseOrder;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ClientDeliveryTotalPrice;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.val;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_CONTACT, Constants.SESSION_ATTRIBUTE_SUBDIV,
    Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_COMPANY})
public class ViewDeliveryNoteController {
    @Autowired
    private CourierDespatchService cdServ;
    @Autowired
    private CourierDespatchTypeService cdtServ;
    @Autowired
    private ComponentDirectoryService compDirServ;
    @Autowired
    private DeliveryService delServ;
    @Autowired
    private EmailService emailServ;
    @Autowired
    private FileBrowserService fileBrowserServ;
    @Autowired
    private JobItemService jobItemServ;
    @Autowired
    private SystemComponentService scService;
    @Autowired
    private SubdivService subdivService;
    @Value("${cwms.config.delivery.birt.newdocument}")
    private boolean showNewDeliveryDocument;
    // System defaults for configuration of new client delivery note generation
    @Autowired
    private ClientDeliveryCertificates clientDeliveryCertificates;
    @Autowired
    private ClientDeliveryDateReceived clientDeliveryDateReceived;
    @Autowired
    private ClientDeliveryPurchaseOrder clientDeliveryPurchaseOrder;
    @Autowired
    private ClientDeliveryTotalPrice clientDeliveryTotalPrice;
    @Autowired
    private ScheduledDeliveryService scheduledDeliveryService;
    @Autowired
    private InstructionService instructionService;
    @Autowired
    private Clock clock;

    @InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
        MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
        binder.registerCustomEditor(Date.class, editor);
        binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
        binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
    }

    @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
    public List<String> newFiles() {
        return new ArrayList<>();
    }

    @ModelAttribute("form")
    protected CreateDeliveryNoteForm formBackingObject(
            @RequestParam(value = "delid", required = false, defaultValue = "0") Integer delId,
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
        JobDelivery delivery = this.delServ.get(delId, JobDelivery.class);
        this.delServ.setDirectoryUsingComponent(delivery);
        CreateDeliveryNoteForm form = new CreateDeliveryNoteForm();
        // third party delivery note?
        form.setTp(delivery.isThirdPartyDelivery());
        form.setDelivery(delivery);
        form.getDelivery().setSentEmails(this.emailServ.getAllComponentEmails(Component.DELIVERY, delId));
        if (delivery.getContact() != null)
            form.setContactid(delivery.getContact().getId());

        // the job file link hasn't been set as Job is being lazily loaded
        // from Delivery
        delivery.getJob().setDirectory(this.compDirServ.getDirectory(this.scService.findComponent(Component.JOB),
                delivery.getJob().getJobno()));

        form.setHasRemainingJobItems(this.jobItemServ.getRemainingJobItemsForDelivery(delivery).size() > 0);

        form.setDespatches(this.cdServ.getDespatchesCreatedInLastWeek(subdivDto.getKey()));
        if (delivery.getDestinationReceiptDate() != null)
            form.setDestinationReceiptDate(
                localDateTimeFromZonedDateTime(delivery.getDestinationReceiptDate()).truncatedTo(ChronoUnit.MINUTES));
        else
            form.setDestinationReceiptDate(LocalDateTime.now(clock.withZone(LocaleContextHolder.getTimeZone().toZoneId())).truncatedTo(ChronoUnit.MINUTES)
            );
        return form;
    }

    @RequestMapping(value = "/viewdelnote.htm", method = RequestMethod.POST)
    protected String onSubmit(Model model,
                              @RequestParam(value = "selectant", required = false, defaultValue = "") String selectant,
                              @RequestParam(value = "selectdespatch", required = false, defaultValue = "0") Integer despatchId,
                              @RequestParam(value = "despatchdate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate despatchDate,
                              @RequestParam(value = "selecttype", required = false, defaultValue = "0") Integer selectType,
                              @RequestParam(value = "consigno", required = false, defaultValue = "") String consignmentNumber,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_CONTACT) Contact currentUser,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                              @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
                              @ModelAttribute("form") @Validated CreateDeliveryNoteForm formObj, BindingResult bindingResult, RedirectAttributes redirectAttributes)
            throws Exception {
        if (bindingResult.hasErrors()) {
            return referenceData(model, newFiles, formObj, subdivDto, companyDto);
        }
        int deliveryid = formObj.getDelivery().getDeliveryid();
        Delivery delivery = this.delServ.get(deliveryid);
        if (formObj.getAction().equals("assigncrdes")) {
            if (selectant.equals("exists")) {
                // add to existing courier despatch
                delivery.setCrdes(this.cdServ.findCourierDespatch(despatchId));
                this.delServ.updateDelivery(delivery);
            } else if (selectant.equals("new")) {
                // create new courier despatch and add delivery to it
                Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
                CourierDespatch cd = new CourierDespatch();
                cd.setDespatchDate(despatchDate);
                cd.setCreationDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
                cd.setOrganisation(allocatedSubdiv);
                cd.setCdtype(this.cdtServ.get(selectType));
                cd.setCreatedBy(currentUser);
                cd.setConsignmentno(consignmentNumber);
                this.cdServ.insertCourierDespatch(cd);
                delivery.setCrdes(cd);
                this.delServ.updateDelivery(delivery);
            }
            redirectAttributes.addAttribute("loadtab", "despatch-tab");
        }
        model.asMap().clear();
        redirectAttributes.addAttribute("delid", delivery.getDeliveryid());
        return "redirect:viewdelnote.htm";
    }

    @RequestMapping(value = "/viewdelnote.htm", method = RequestMethod.GET)
    protected String referenceData(Model model, @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
                                   @ModelAttribute("form") CreateDeliveryNoteForm form,
                                   @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                   @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
            throws Exception {

        Delivery formDelivery = form.getDelivery();
        int id = formDelivery.getDeliveryid();
        
        // get files in the root directory
        FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.DELIVERY, id, newFiles);
        List<ScheduledDelivery> scheduledDeliveryList = scheduledDeliveryService.scheduledDeliveryFromScheduleAndOrDelivery(null, id);
        //  no crdes and no schedule for the delivery 
        if(formDelivery.getCrdes() == null && scheduledDeliveryList.isEmpty()){
        	model.addAttribute("nullCrDes", true);
        } else {
        	model.addAttribute("nullCrDes", false);
        	model.addAttribute("scheduledDeliveryList", scheduledDeliveryList);
        }
        
        model.addAttribute("showNewDeliveryDocument",
                showNewDeliveryDocument && formDelivery.getType().equals(DeliveryType.CLIENT));
        if (formDelivery.getType().equals(DeliveryType.CLIENT)) {
            Delivery delivery = this.delServ.get(id);
            Subdiv clientSubdiv = delivery.getAddress().getSub();

            model.addAttribute("sdCertificates",
                    this.clientDeliveryCertificates.parseValueHierarchical(Scope.SUBDIV, clientSubdiv, null));
            model.addAttribute("sdDateReceived",
                    this.clientDeliveryDateReceived.parseValueHierarchical(Scope.SUBDIV, clientSubdiv, null));
            model.addAttribute("sdPurchaseOrder",
                this.clientDeliveryPurchaseOrder.parseValueHierarchical(Scope.SUBDIV, clientSubdiv, null));
            model.addAttribute("sdTotalPrice",
                this.clientDeliveryTotalPrice.parseValueHierarchical(Scope.SUBDIV, clientSubdiv, null));
        }
        model.addAttribute("privateOnlyNotes", NoteType.DELIVERYNOTE.isPrivateOnly());
        model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.DELIVERY));
        model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
        model.addAttribute(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
        this.cdServ.prepareCourierDespatchData(subdivDto.getKey(), companyDto.getKey(), model);
        if (formDelivery instanceof JobDelivery) {
            val delivery = (JobDelivery) formDelivery;
            delivery.getItems().stream().min(Comparator.comparing(i -> i.getJobitem().getItemNo()))
                    .map(DeliveryItem::getDespatchedon).map(DateTools::localDateTimeFromZonedDateTime)
                    .map(d -> d.truncatedTo(ChronoUnit.MINUTES))
                    .ifPresent(d -> model.addAttribute("minDestinationReceiptDate", d));
        }
        model.addAttribute("maxDestinationReceiptDate",
                LocalDateTime.now(clock.withZone(LocaleContextHolder.getTimeZone().toZoneId()))
                        .truncatedTo(ChronoUnit.MINUTES));
        model.addAttribute("isReadyForDestinationReceipt", this.delServ.isReadyForDestinationReceipt(formDelivery));

        val itemIds = (formDelivery.getItems() != null) ?
                formDelivery.getItems().stream().filter(e -> e instanceof JobDeliveryItem)
                        .map(i -> (JobDeliveryItem) i).map(JobDeliveryItem::getJobitem).map(JobItem::getJobItemId)
                        .collect(Collectors.toList()) : Collections.<Integer>emptyList();
        model.addAttribute("jobItemIds", itemIds);

        int instructionCount = 0;
        if (formDelivery.getContact() != null) {
            InstructionType[] instructionType = new InstructionType[]{InstructionType.PACKAGING, InstructionType.CARRIAGE};
            Contact contact = formDelivery.getContact();

            Integer jobId = (formDelivery instanceof JobDelivery) && formDelivery.getType().equals(DeliveryType.THIRDPARTY) ?
                    ((JobDelivery) formDelivery).getJob().getJobid() : null;
            Subdiv sub = contact.getSub();

            List<InstructionDTO> instructionDTOS = this.instructionService.ajaxInstructions(subdivDto.getKey(), singletonList(sub.getComp().getCoid()), singletonList(sub.getSubdivid()), singletonList(contact.getPersonid()), null, jobId, null, itemIds, instructionType, formDelivery.getType());
            instructionCount = instructionDTOS.size();
        }
        model.addAttribute("instructionsNumbers", instructionCount);
        return "trescal/core/deliverynote/delnotesummary";
    }
}