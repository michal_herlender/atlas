package org.trescal.cwms.core.deliverynote.entity.transportmethod.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;

public interface TransportMethodDao extends BaseDao<TransportMethod, Integer> {}