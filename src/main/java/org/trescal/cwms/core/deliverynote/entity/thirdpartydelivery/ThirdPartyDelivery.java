package org.trescal.cwms.core.deliverynote.entity.thirdpartydelivery;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;

@Entity
@DiscriminatorValue("thirdparty")
public class ThirdPartyDelivery extends JobDelivery
{
	@Override
	@Transient
	public DeliveryType getType() {
		return DeliveryType.THIRDPARTY;
	}
}