package org.trescal.cwms.core.deliverynote.entity.transportmethod.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;

@Repository("TransportMethodDao")
public class TransportMethodDaoImpl extends BaseDaoImpl<TransportMethod, Integer> implements TransportMethodDao {
	
	@Override
	protected Class<TransportMethod> getEntity() {
		return TransportMethod.class;
	}
}