package org.trescal.cwms.core.deliverynote.entity.delivery;

import java.util.EnumSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.entity.internaldelivery.InternalDelivery;
import org.trescal.cwms.core.deliverynote.entity.internalreturndelivery.InternalReturnDelivery;
import org.trescal.cwms.core.deliverynote.entity.thirdpartydelivery.ThirdPartyDelivery;

public enum DeliveryType {
	// Undefined is a generic value - used for "Any" search
	UNDEFINED("notapplicable", Delivery.class),	
	CLIENT("client", ClientDelivery.class),
	THIRDPARTY("thirdparty", ThirdPartyDelivery.class),
	GENERAL("general", GeneralDelivery.class),
	INTERNAL("internal", InternalDelivery.class),
	INTERNAL_RETURN( "internal.return", InternalReturnDelivery.class);
	
	private String messageCode;
	private Class<? extends Delivery> deliveryClass;
	private MessageSource messageSource;
	
	@Component
	public static class DeliveryTypeMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;
		
		@PostConstruct
        public void postConstruct() {
            for (DeliveryType dt : EnumSet.allOf(DeliveryType.class))
               dt.setMessageSource(messageSource);
        }
	}
	
	public static Set<DeliveryType> getActiveDeliveryTypes() {
		return EnumSet.complementOf(EnumSet.of(UNDEFINED));
	}
	
	public Class<? extends Delivery> getDeliveryClass() {
		return deliveryClass;
	}
	
	private DeliveryType(String messageCode, Class<? extends Delivery> deliveryClass) {
		this.messageCode = messageCode;
		this.deliveryClass = deliveryClass; 
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getDescription() {
		return this.messageSource.getMessage(messageCode, null, this.name(), LocaleContextHolder.getLocale());
	}
}
