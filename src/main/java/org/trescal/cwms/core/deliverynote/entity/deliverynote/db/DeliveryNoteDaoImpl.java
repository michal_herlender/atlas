package org.trescal.cwms.core.deliverynote.entity.deliverynote.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.Location_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionDao;
import org.trescal.cwms.core.deliverynote.form.JobItemForDeliveryNoteDTO;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement_;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup_;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem_;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Locale;

@Repository
public class DeliveryNoteDaoImpl extends BaseDaoImpl<DeliveryNote, Integer> implements DeliveryNoteDao {

	@Autowired
	private TransportOptionDao transportOptionDao;

	@Override
	protected Class<DeliveryNote> getEntity() {
		return DeliveryNote.class;
	}

	/**
	 * Intended to return a list of job items from current job for DN
	 *
	 * @param jobId:     the id of the current job
	 * @param stateGroup : determined basing on DN type
	 * @param subdivId   : the id of business subdivision
	 */
	@Override
	public List<JobItemForDeliveryNoteDTO> getJobItemsFromCurrentJobForDeliveryNote(Integer jobId,
			StateGroup stateGroup, Integer subdivId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemForDeliveryNoteDTO> cq = cb.createQuery(JobItemForDeliveryNoteDTO.class);
			Root<Job> job = cq.from(Job.class);
			Join<Job, JobItem> jobItem = job.join(Job_.items);
			Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr, JoinType.LEFT);
			Join<Address, Subdiv> currentSubdiv = currentAddress.join(Address_.sub);
			Join<JobItem, Address> jobitemReturnToAddress = jobItem.join(JobItem_.returnToAddress, JoinType.LEFT);
			Join<JobItem, Contact> jobitemReturnToContact = jobItem.join(JobItem_.returnToContact, JoinType.LEFT);
			Join<Job, Address> jobReturnToAddress = job.join(Job_.returnTo);
			Join<Job, Contact> con = job.join(Job_.con);
			Join<Contact, Subdiv> subdiv = con.join(Contact_.sub);
			Join<Subdiv, Company> comp = subdiv.join(Subdiv_.comp);
			Join<Company, Country> country = comp.join(Company_.country, JoinType.LEFT);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Join<Instrument, Contact> instrumentContact = instrument.join(Instrument_.con, JoinType.LEFT);
			Join<Instrument, Address> instrumentAddress = instrument.join(Instrument_.add, JoinType.LEFT);
			Join<Instrument, Location> instrumentLocation = instrument.join(Instrument_.loc, JoinType.LEFT);
			Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model);
			Expression<String> modelName = joinTranslation(cb, instrumentModel, InstrumentModel_.nameTranslations,
					locale);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
			Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks, JoinType.LEFT);
			stateGroupLink.on(cb.equal(stateGroupLink.get(StateGroupLink_.groupId), stateGroup.getId()));
			Expression<Boolean> readyForDelivery = cb.selectCase()
					.when(cb.and(cb.isNotNull(stateGroupLink), cb.equal(currentAddress.get(Address_.sub), subdivId))
							.as(Boolean.class), cb.literal(true))
					.otherwise(cb.literal(false)).as(Boolean.class);

			Join<JobItem, JobItemWorkRequirement> nextWorkRequirement = jobItem.join(JobItem_.nextWorkReq,
					JoinType.LEFT);
			Join<JobItemWorkRequirement, WorkRequirement> workRequirement = nextWorkRequirement
					.join(JobItemWorkRequirement_.workRequirement, JoinType.LEFT);
			Join<WorkRequirement, Address> addressWorkRequirement = workRequirement.join(WorkRequirement_.address,
					JoinType.LEFT);
			Join<Address, Subdiv> nextSubdivFromWR = addressWorkRequirement.join(Address_.sub, JoinType.LEFT);

			Join<JobItem, TransportOption> returnOption = jobItem.join(JobItem_.returnOption);
			Join<TransportOption, Subdiv> returnOptionSub = returnOption.join(TransportOption_.sub);

			Join<JobItem, OnBehalfItem> onBehalfItem = jobItem.join(JobItem_.onBehalf, JoinType.LEFT);
			Join<OnBehalfItem, Company> onBehalfCompany = onBehalfItem.join(OnBehalfItem_.company, JoinType.LEFT);
			Join<OnBehalfItem, Address> onBehalfAddress = onBehalfItem.join(OnBehalfItem_.address, JoinType.LEFT);
			Join<Address, Subdiv> onBehalfSubdiv = onBehalfAddress.join(Address_.sub, JoinType.LEFT);
			Expression<String> onBehalfInfo = cb.concat(
					cb.concat(onBehalfCompany.get(Company_.coname), cb.literal(" ")),
					onBehalfAddress.get(Address_.addr1));

			Expression<Integer> onbehalfId = onBehalfItem.get(OnBehalfItem_.id);
			Join<JobItem, JobItemGroup> group = jobItem.join(JobItem_.group, JoinType.LEFT);
			Expression<String> transportOptionName = transportOptionDao.getCompleteTransportOption(cb,
					jobItem.join(JobItem_.returnOption), locale);

			cq.where(cb.equal(job.get(Job_.jobid), jobId));
			cq.orderBy(cb.asc(jobItem.get(JobItem_.itemNo)));
			cq.select(cb.construct(JobItemForDeliveryNoteDTO.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), job.get(Job_.jobid), job.get(Job_.jobno),
					instrument.get(Instrument_.plantid), modelName, instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.plantno), currentSubdiv.get(Subdiv_.subdivid),
					jobItem.get(JobItem_.accessoryFreeText), readyForDelivery, onBehalfInfo, onbehalfId,
					group.get(JobItemGroup_.id), transportOptionName, jobItem.get(JobItem_.clientRef),
					job.get(Job_.clientRef), instrumentContact.get(Contact_.firstName),
					instrumentContact.get(Contact_.lastName), instrumentAddress.get(Address_.addr1),
					instrumentLocation.get(Location_.location), country.get(Country_.country),
					subdiv.get(Subdiv_.subname), onBehalfSubdiv.get(Subdiv_.subname),
					workRequirement.get(WorkRequirement_.id), workRequirement.get(WorkRequirement_.status),
					returnOptionSub.get(Subdiv_.subdivid), nextSubdivFromWR.get(Subdiv_.subdivid),
					cb.concat(jobitemReturnToAddress.get(Address_.addr1),
							cb.concat(" ", jobitemReturnToAddress.get(Address_.addr2))),
					jobitemReturnToAddress.get(Address_.addrid),
					cb.concat(jobitemReturnToContact.get(Contact_.firstName),
							cb.concat(" ", jobitemReturnToContact.get(Contact_.lastName))),
					jobitemReturnToContact.get(Contact_.personid),
					cb.concat(jobReturnToAddress.get(Address_.addr1),
							cb.concat(" ", jobReturnToAddress.get(Address_.addr2))),
					jobReturnToAddress.get(Address_.addrid),
					cb.concat(con.get(Contact_.firstName),
							cb.concat(" ", con.get(Contact_.lastName))),
					con.get(Contact_.personid)

			));
			return cq;
		});
	}

	/**
	 * Intended to return a list of job items from other jobs for DN
	 *
	 * @param stateGroup : determined basing on DN type
	 */
	@Override
	public List<JobItemForDeliveryNoteDTO> getJobItemsFromOtherJobsForDeliveryNote(Subdiv subdiv, StateGroup stateGroup,
			Job currentJob, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemForDeliveryNoteDTO> cq = cb.createQuery(JobItemForDeliveryNoteDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, Address> jobReturnToAddress = job.join(Job_.returnTo);
			Join<JobItem, Address> jobitemReturnToAddress = jobItem.join(JobItem_.returnToAddress, JoinType.LEFT);
			Join<JobItem, Contact> jobitemReturnToContact = jobItem.join(JobItem_.returnToContact, JoinType.LEFT);
			Join<Job, Contact> con = job.join(Job_.con);
			Join<Contact, Subdiv> sub = con.join(Contact_.sub);
			Join<Subdiv, Company> comp = sub.join(Subdiv_.comp);
			Join<Company, Country> country = comp.join(Company_.country, JoinType.LEFT);

			// used in Internal DN case, to determine next subdivision of job
			// item
			Join<JobItem, JobItemWorkRequirement> nextWorkRequirement = jobItem.join(JobItem_.nextWorkReq,
					JoinType.LEFT);
			Join<JobItemWorkRequirement, WorkRequirement> workRequirement = nextWorkRequirement
					.join(JobItemWorkRequirement_.workRequirement, JoinType.LEFT);
			Join<WorkRequirement, Address> addressWorkRequirement = workRequirement.join(WorkRequirement_.address,
					JoinType.LEFT);
			Join<Address, Subdiv> nextSubdivFromWR = addressWorkRequirement.join(Address_.sub, JoinType.LEFT);

			Join<JobItem, TransportOption> returnOption = jobItem.join(JobItem_.returnOption);
			Join<TransportOption, Subdiv> returnOptionSub = returnOption.join(TransportOption_.sub);

			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Join<Instrument, Contact> instrumentContact = instrument.join(Instrument_.con, JoinType.LEFT);
			Join<Instrument, Address> instrumentAddress = instrument.join(Instrument_.add, JoinType.LEFT);
			Join<Instrument, Location> instrumentLocation = instrument.join(Instrument_.loc, JoinType.LEFT);
			Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model);
			Expression<String> modelName = joinTranslation(cb, instrumentModel, InstrumentModel_.nameTranslations,
					locale);
			Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr, JoinType.LEFT);
			Join<Address, Subdiv> currentSubdiv = currentAddress.join(Address_.sub);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
			Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks, JoinType.LEFT);
			stateGroupLink.on(cb.equal(stateGroupLink.get(StateGroupLink_.groupId), stateGroup.getId()));
			Expression<Boolean> readyForDelivery = cb.selectCase()
					.when(cb.and(cb.isNotNull(stateGroupLink),
							cb.equal(currentAddress.get(Address_.sub), subdiv.getSubdivid())).as(Boolean.class),
							cb.literal(true))
					.otherwise(cb.literal(false)).as(Boolean.class);

			Join<JobItem, OnBehalfItem> onBehalfItem = jobItem.join(JobItem_.onBehalf, JoinType.LEFT);
			Join<OnBehalfItem, Company> onBehalfCompany = onBehalfItem.join(OnBehalfItem_.company, JoinType.LEFT);
			Join<OnBehalfItem, Address> onBehalfAddress = onBehalfItem.join(OnBehalfItem_.address, JoinType.LEFT);
			Join<Address, Subdiv> onBehalfSubdiv = onBehalfAddress.join(Address_.sub, JoinType.LEFT);
			Expression<String> onBehalfInfo = cb.concat(
					cb.concat(onBehalfCompany.get(Company_.coname), cb.literal(" ")),
					onBehalfAddress.get(Address_.addr1));

			Expression<Integer> onbehalfId = onBehalfItem.get(OnBehalfItem_.id);
			Join<JobItem, JobItemGroup> group = jobItem.join(JobItem_.group, JoinType.LEFT);
			Expression<String> transportOptionName = transportOptionDao.getCompleteTransportOption(cb,
					jobItem.join(JobItem_.returnOption), locale);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.notEqual(job, currentJob));

			if (stateGroup.equals(StateGroup.CLIENTDELIVERY)) {
				clauses.getExpressions()
						.add(cb.equal(sub.get(Subdiv_.subdivid), currentJob.getCon().getSub().getSubdivid()));
			}
			clauses.getExpressions().add(cb.equal(stateGroupLink.get(StateGroupLink_.groupId), stateGroup.getId()));
			clauses.getExpressions().add(cb.equal(currentAddress.get(Address_.sub), subdiv));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(JobItemForDeliveryNoteDTO.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), job.get(Job_.jobid), job.get(Job_.jobno),
					instrument.get(Instrument_.plantid), modelName, instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.plantno), currentSubdiv.get(Subdiv_.subdivid),
					jobItem.get(JobItem_.accessoryFreeText), readyForDelivery, onBehalfInfo, onbehalfId,
					group.get(JobItemGroup_.id), transportOptionName, jobItem.get(JobItem_.clientRef),
					job.get(Job_.clientRef), instrumentContact.get(Contact_.firstName),
					instrumentContact.get(Contact_.lastName), instrumentAddress.get(Address_.addr1),
					instrumentLocation.get(Location_.location), country.get(Country_.country), sub.get(Subdiv_.subname),
					onBehalfSubdiv.get(Subdiv_.subname), workRequirement.get(WorkRequirement_.id),
					workRequirement.get(WorkRequirement_.status), returnOptionSub.get(Subdiv_.subdivid),
					nextSubdivFromWR.get(Subdiv_.subdivid),
					cb.concat(jobitemReturnToAddress.get(Address_.addr1),
							cb.concat(" ", jobitemReturnToAddress.get(Address_.addr2))),
					jobitemReturnToAddress.get(Address_.addrid),
					cb.concat(jobitemReturnToContact.get(Contact_.firstName),
							cb.concat(" ", jobitemReturnToContact.get(Contact_.lastName))),
					jobitemReturnToContact.get(Contact_.personid),
					cb.concat(jobReturnToAddress.get(Address_.addr1),
							cb.concat(" ", jobReturnToAddress.get(Address_.addr2))),
					jobReturnToAddress.get(Address_.addrid),
					cb.concat(con.get(Contact_.firstName),
							cb.concat(" ", con.get(Contact_.lastName))),
					con.get(Contact_.personid)
					));
			return cq;
		});
	}
	
	@Override
	public Boolean itemSelectedIsNotReadyForDelivery(StateGroup stateGroup, List<Integer> itemIds){
		JobItemForDeliveryNoteDTO dto = getFirstResult(cb->{
			CriteriaQuery<JobItemForDeliveryNoteDTO> cq = cb.createQuery(JobItemForDeliveryNoteDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
			Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks, JoinType.LEFT);
			stateGroupLink.on(cb.equal(stateGroupLink.get(StateGroupLink_.groupId), stateGroup.getId()));
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isNull(stateGroupLink));
			clauses.getExpressions().add(jobItem.get(JobItem_.jobItemId).in(itemIds));
			cq.where(clauses);
			cq.select(cb.construct(JobItemForDeliveryNoteDTO.class,
				jobItem.get(JobItem_.jobItemId),
				job.get(Job_.jobno)));
			return cq;
		}).orElse(null);

		return dto != null;
	}
}