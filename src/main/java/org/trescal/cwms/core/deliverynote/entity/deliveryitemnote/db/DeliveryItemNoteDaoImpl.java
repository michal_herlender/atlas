package org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;

@Repository("DeliveryItemNoteDao")
public class DeliveryItemNoteDaoImpl extends BaseDaoImpl<DeliveryItemNote, Integer> implements DeliveryItemNoteDao {
	
	@Override
	protected Class<DeliveryItemNote> getEntity() {
		return DeliveryItemNote.class;
	}
}