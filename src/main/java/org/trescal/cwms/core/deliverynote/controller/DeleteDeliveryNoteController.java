package org.trescal.cwms.core.deliverynote.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class DeleteDeliveryNoteController
{
	@Autowired
	private DeliveryService delServ;
	@RequestMapping(value="/deletedelnote.htm", method=RequestMethod.GET)
	public ModelAndView handleRequestInternal(HttpServletRequest req, HttpServletResponse res) throws Exception
	{
		int id = ServletRequestUtils.getIntParameter(req, "delid");
		Delivery delivery = this.delServ.get(id);

		// delivery of type job?
		if (delivery instanceof JobDelivery) {
			// cast as job delivery
			JobDelivery jobdel = (JobDelivery) delivery;
			// remove all links to third party requirements first
			for (JobDeliveryItem di : jobdel.getItems())
				// remove tp requirement link
				di.setTpRequirement(null);
		}
		// now delete delivery
		this.delServ.deleteDelivery(delivery);

		if (delivery instanceof GeneralDelivery)
		{
			return new ModelAndView(new RedirectView("home.htm"));
		}
		else
		{
			JobDelivery jobDelivery = (JobDelivery) delivery;
			return new ModelAndView(new RedirectView("/viewjob.htm?jobid="
					+ jobDelivery.getJob().getJobid(), true));
		}
	}
}