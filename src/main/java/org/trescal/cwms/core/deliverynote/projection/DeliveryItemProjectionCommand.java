package org.trescal.cwms.core.deliverynote.projection;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides indication of which data to load
 *
 */
@Getter @Setter
public class DeliveryItemProjectionCommand {
	// Mandatory values set in constructor
	private Locale locale;
	// Optional values set via options (default false)
	private Boolean loadTpRequirements;
	private Boolean loadAccessories;
	private Boolean loadNotes;
	
	public static interface DI_LOAD_TP_REQUIREMENTS {};
	public static interface DI_LOAD_ACCESSORIES {};
	public static interface DI_LOAD_NOTES {};
	
	public DeliveryItemProjectionCommand(Locale locale,  
			Class<?>... options) {
		if (locale == null) throw new IllegalArgumentException("locale must not be null");
		this.locale = locale;
		List<Class<?>> args = Arrays.asList(options);
		this.loadTpRequirements = args.contains(DI_LOAD_TP_REQUIREMENTS.class);
		this.loadAccessories = args.contains(DI_LOAD_ACCESSORIES.class);
		this.loadNotes = args.contains(DI_LOAD_NOTES.class);
	}
}
