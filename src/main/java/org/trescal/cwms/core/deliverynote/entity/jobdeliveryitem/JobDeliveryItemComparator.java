package org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem;

import java.util.Comparator;

/**
 * Inner comparator class for sorting JobDeliveryItems when returning
 * collections of deliveries, each with multiple items
 */
public class JobDeliveryItemComparator implements Comparator<JobDeliveryItem>
{
	public int compare(JobDeliveryItem delItem, JobDeliveryItem anotherDelItem) {
		int result = delItem.getJobitem().getJob().getJobno().compareTo(anotherDelItem.getJobitem().getJob().getJobno());
		if (result == 0) result = delItem.getJobitem().getItemNo() - anotherDelItem.getJobitem().getItemNo();
		if (result == 0) result = delItem.getDelitemid() - anotherDelItem.getDelitemid();
		return result;
	}
}