package org.trescal.cwms.core.deliverynote.entity.transportoption.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.form.TransportOptionDTO;

public interface TransportOptionDao extends BaseDao<TransportOption, Integer> {

	Expression<String> getCompleteTransportOption(CriteriaBuilder cb, From<?, TransportOption> path, Locale locale);

	List<TransportOptionDTO> getTransportOptionDtos(Boolean active, Locale locale,
			Collection<Integer> transportOptionIds, Collection<Integer> subdivIds);

	List<TransportOption> getAllDaySpecificTransportOptions(int fromSubdivId);

	List<TransportOption> getTransportOptionsFromSubdiv(int fromSubdivId);

	TransportOption getEager(int id);
	
	List<TransportOption> transportOptionsFromSubdivWithActiveMethod(int fromSubdivId);
	
	List<TransportOption> getTransportOptionsSchedulable(Integer fromSubdivId);
	
	List<TransportOption> transportOptionByLocalizedName(Integer subdivid, String localizedName, Integer methodId);
	
	Integer getTransportOptionByJobItemId(Integer jobitemId);
	
	List<Integer> itemsWithTransportOptionSchedulable(List<Integer> jobitemIds);
}