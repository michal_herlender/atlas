package org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem;

import java.util.Comparator;

/**
 * Comparator sorts {@link JobDeliveryItem} by ids - most recent first
 * 
 * @author Richard
 */
public class JobDeliveryItemAgeComparator implements Comparator<JobDeliveryItem>
{
	@Override
	public int compare(JobDeliveryItem o1, JobDeliveryItem o2)
	{
		return ((Integer) o1.getDelitemid()).compareTo(o2.getDelitemid());
	}
}
