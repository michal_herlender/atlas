package org.trescal.cwms.core.deliverynote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.form.DeliverySearchForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV})
public class DeliverySearchController
{
	@Autowired
	private DeliveryService delServ;
	
	@Autowired
	private MessageSource messageSource;
	
	public static final String CODE_ANY = "cdsearch.any";
	public static final String COMMAND_NAME = "command";

	@ModelAttribute(COMMAND_NAME)
	public Object formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto) throws Exception
	{
		DeliverySearchForm form = new DeliverySearchForm();
		form.setDeliveryType(DeliveryType.UNDEFINED);
		form.setAllocatedSubdivId(subdivDto.getKey());
		return form;
	}

	@InitBinder
	public void initBinder(ServletRequestDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	private KeyValue<DeliveryType,String> getAnyDto() {
		Locale locale = LocaleContextHolder.getLocale();
		String text = messageSource.getMessage(CODE_ANY, null, locale);
		return new KeyValue<>(DeliveryType.UNDEFINED, text);
	}
	
	private List<KeyValue<DeliveryType,String>> getDeliveryTypeDtos() {

		List<KeyValue<DeliveryType, String>> result = new ArrayList<>();
		result.add(getAnyDto());
		DeliveryType.getActiveDeliveryTypes().forEach(dt -> result.add(new KeyValue<>(dt, dt.getDescription())));
		return result;
	}

	@RequestMapping(value="/delsearch.htm", method=RequestMethod.GET)
	public String referenceData(Model model) {
		model.addAttribute("deliveryTypes", getDeliveryTypeDtos());
		model.addAttribute("companyRoles", EnumSet.complementOf(EnumSet.of(CompanyRole.NOTHING)));
		return "trescal/core/deliverynote/delsearch";
	}

	@RequestMapping(value = "/delsearch.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(@ModelAttribute(COMMAND_NAME) DeliverySearchForm dsf) throws Exception {
		PagedResultSet<Delivery> rs = this.delServ.queryDeliveries(dsf, new PagedResultSet<>(dsf.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : dsf.getResultsPerPage(), dsf.getPageNo() == 0 ? 1 : dsf.getPageNo()));
		dsf.setRs(rs);

		return new ModelAndView("trescal/core/deliverynote/delsearchresults", "form", dsf);
	}
}