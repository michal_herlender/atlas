package org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchTypeDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;

@Service("CourierDespatchTypeService")
public class CourierDespatchTypeServiceImpl extends BaseServiceImpl<CourierDespatchType, Integer> implements CourierDespatchTypeService
{
	@Autowired
	private CourierDespatchTypeDao courierDespatchTypeDao;
	@Autowired
	private CourierService courierServ;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private HireService hireService;
	
	@Override
	protected BaseDao<CourierDespatchType, Integer> getBaseDao() {
		return courierDespatchTypeDao;
	}
	
	public ResultWrapper editType(int cdtid, String newDesc)
	{
		// first get type
		CourierDespatchType cdt = this.get(cdtid);
		// type null?
		if (cdt != null)
		{
			// set description
			cdt.setDescription(newDesc);
			// update courier despatch type
			this.update(cdt);
			// return successful wrapper
			return new ResultWrapper(true, "", new CourierDespatchTypeDTO(cdt), null);
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "The courier despatch type could not be found", null, null);
		}
	}
	
	public List<CourierDespatchTypeDTO> getAllSorted()
	{
		return this.courierDespatchTypeDao.getAllSorted();
	}
	
	public ResultWrapper newDespatchTypeForCourier(int courierid, String desc)
	{
		// create new courier despatch type object
		CourierDespatchType cdt = new CourierDespatchType();
		cdt.setDescription(desc);
		cdt.setCourier(this.courierServ.get(courierid));
		// insert courier despatch type
		this.save(cdt);
		// return successful wrapper
		return new ResultWrapper(true, "", new CourierDespatchTypeDTO(cdt), null);
	}
	
	@Override
	public void updateDefaultCourierDespatchType(Integer courierid, Integer cdtid) {
		Courier courier = this.courierServ.get(courierid);
		courier.getCdts().stream().forEach(cd -> {
			if(cd.getCdtid() == cdtid) {
				cd.setDefaultForCourier(true);
				this.courierDespatchTypeDao.merge(cd);
			}
			else if(cd.isDefaultForCourier()) {
				cd.setDefaultForCourier(false);
				this.courierDespatchTypeDao.merge(cd);
			}
		});
	}

	@Override
	public void deleteCourierDespatchType(Integer cdtid) {
		CourierDespatchType cdt = this.get(cdtid);
		if(cdt != null)
			this.courierDespatchTypeDao.remove(cdt);
	}

	@Override
	public Long linkedEntitiesCount(Integer cdtid) {
		return this.deliveryService.deliveryCountByCourierDespatchType(cdtid)+this.hireService.hireCountByCourierDespatchType(cdtid);
	}
}