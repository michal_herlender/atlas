package org.trescal.cwms.core.deliverynote.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.LocationDTO;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.db.DeliveryNoteService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNoteForm;
import org.trescal.cwms.core.deliverynote.form.CreateGeneralDeliveryNoteValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class CreateGeneralDeliveryController {
	@Autowired
	private AddressService addServ;
	@Autowired
	private CourierDespatchService cdServ;
	@Autowired
	private CourierDespatchTypeService cdtServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private DeliveryNoteService delNoteServ;
	@Autowired
	private DeliveryService delServ;
	@Autowired
	private LocationService locServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private CreateGeneralDeliveryNoteValidator validator;

	@InitBinder("delnoteform")
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@ModelAttribute("delnoteform")
	protected CreateDeliveryNoteForm formBackingObject(
			@RequestParam(value="personid", required=true, defaultValue="0") Integer personId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = this.conServ.get(personId);
		
		CreateDeliveryNoteForm form = new CreateDeliveryNoteForm();
		form.setDelivery(new GeneralDelivery());
		form.setDeliverydate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		form.setDespatches(this.cdServ.getDespatchesCreatedInLastWeek(subdivDto.getKey()));
		form.setContact(contact);
		form.setContactid(personId);
		form.setAddressid(contact.getDefAddress().getAddrid());
		return form;
	}

	@RequestMapping(value = "/creategendelivery.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model,
							  @RequestParam(value = "selectant", required = false, defaultValue = "") String selectant,
							  @RequestParam(value = "selectdespatch", required = false, defaultValue = "0") Integer selectDespatch,
							  @RequestParam(value = "despatchdate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate despatchDate,
							  @RequestParam(value = "selecttype", required = false, defaultValue = "0") Integer selectType,
							  @RequestParam(value = "consigno", required = false, defaultValue = "") String consigNo,
							  @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
							  @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
							  @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
							  @ModelAttribute("delnoteform") CreateDeliveryNoteForm formObj, BindingResult bindingResult) throws Exception
	{
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		GeneralDelivery delivery = (GeneralDelivery) formObj.getDelivery();
		delivery.setOrganisation(allocatedSubdiv);
		delivery.setOurref(formObj.getOurref().trim());
		validator.validate(formObj, bindingResult);
		if (bindingResult.hasErrors()) {
			return referenceData(model, formObj, subdivDto, companyDto);
		}
		Contact currentUser = userService.get(username).getCon();
		// Get the next delno
		String gendelno = numerationService.generateNumber(NumerationType.GENERAL_DELIVERY, allocatedSubdiv.getComp(),
			allocatedSubdiv);
		// Get the current user for createdby fields
		delivery.setDeliveryno(gendelno);
		delivery.setCreatedby(currentUser);
		delivery.setCreationdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		delivery.setDeliverydate(formObj.getDeliverydate());
		if (formObj.getAddressid() != null)
			delivery.setAddress(this.addServ.get(formObj.getAddressid()));
		if (formObj.getContactid() != null)
			delivery.setContact(this.conServ.get(formObj.getContactid()));
		delivery.setLocation(this.locServ.get(formObj.getLocid()));
		// set client reference
		if (!formObj.getClientref().trim().equals(""))
			delivery.setClientref(formObj.getClientref().trim());
		if (selectant.equals("exists")) {
			// add to existing courier despatch
			delivery.setCrdes(this.cdServ.findCourierDespatch(selectDespatch));
		} else if (selectant.equals("new")) {
			// create new courier despatch and add delivery to it
			CourierDespatch cd = new CourierDespatch();
			cd.setDespatchDate(despatchDate);
			cd.setCreationDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			cd.setOrganisation(allocatedSubdiv);
			cd.setCdtype(this.cdtServ.get(selectType));
			cd.setCreatedBy(currentUser);
			cd.setConsignmentno(consigNo);
			this.cdServ.insertCourierDespatch(cd);
			delivery.setCrdes(cd);
		}
		// Create the delivery object itself
		this.delServ.insertDelivery(delivery);
		// Create a Delivery Note object (IF there is text in the delivery note
		// field)
		if (!formObj.getDelInstruction().trim().equals("")) {
			DeliveryNote delNote = new DeliveryNote();
			delNote.setNote(formObj.getDelInstruction());
			delNote.setDelivery(delivery);
			delNote.setSetBy(currentUser);
			delNote.setSetOn(new Date());
			delNote.setActive(true);
			delNote.setPublish(true);
			this.delNoteServ.save(delNote);
		}
		return "redirect:gendelnotesummary.htm?delid=" + delivery.getDeliveryid();
	}

	@RequestMapping(value = "/creategendelivery.htm", method = RequestMethod.GET)
	protected String referenceData(Model refData,
			@ModelAttribute("delnoteform") CreateDeliveryNoteForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) throws Exception
	{
		Integer subdivid = (form.getContact() == null) ? null : form.getContact().getSub().getSubdivid();
		List<ContactKeyValue> clientContacts = (subdivid == null) ? new ArrayList<>()
				: this.conServ.getContactDtoList(null, subdivid, true);
		List<LocationDTO> clientLocations = (form.getContact() == null) ? new ArrayList<>()
				: this.locationService.getAddressLocations(
						form.getContact().getDefAddress().getAddrid(), true);

		refData.addAttribute("activeDeliveryAddresses", (subdivid == null) ? new ArrayList<Address>()
				: this.addServ.getAllSubdivAddresses(subdivid, AddressType.DELIVERY, true));
		refData.addAttribute("clientContacts", clientContacts);
		refData.addAttribute("clientLocations", clientLocations);
		this.cdServ.prepareCourierDespatchData(subdivid, companyDto.getKey(), refData);
		return "trescal/core/deliverynote/creategendelnote";
	}

}