package org.trescal.cwms.core.deliverynote.entity.transportoption.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.Case;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOptionComparator;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;
import org.trescal.cwms.core.deliverynote.form.TransportOptionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository("TransportOptionDao")
public class TransportOptionDaoImpl extends BaseDaoImpl<TransportOption, Integer> implements TransportOptionDao {

	@Override
	protected Class<TransportOption> getEntity() {
		return TransportOption.class;
	}

	@Override
	public Expression<String> getCompleteTransportOption(CriteriaBuilder cb, From<?, TransportOption> path,
			Locale locale) {
		Join<TransportOption, TransportMethod> method = path.join(TransportOption_.method);
		Expression<String> methodName = joinTranslation(cb, method, TransportMethod_.methodTranslation, locale);
		Join<TransportOption, Subdiv> subdiv = path.join(TransportOption_.sub, JoinType.LEFT);
		Case<String> param = cb.selectCase();
		param.when(cb.isNull(subdiv),
				cb.concat(cb.concat(" (", path.get(TransportOption_.dayOfWeek).as(String.class)), " )"));
		param.otherwise(cb.concat(" - ", subdiv.get(Subdiv_.subdivCode)));
		return cb.concat(methodName, param);
	}

	@Override
	public List<TransportOptionDTO> getTransportOptionDtos(Boolean active, Locale locale,
			Collection<Integer> transportOptionIds, Collection<Integer> subdivIds) {
		return getResultList(cb -> {
			CriteriaQuery<TransportOptionDTO> cq = cb.createQuery(TransportOptionDTO.class);
			Root<TransportOption> root = cq.from(TransportOption.class);
			Join<TransportOption, TransportMethod> method = root.join(TransportOption_.method);
			Join<TransportOption, Subdiv> subdiv = root.join(TransportOption_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Join<Company, Country> country = company.join(Company_.country);
			Join<TransportMethod, Translation> translation = method.join(TransportMethod_.methodTranslation,
					JoinType.LEFT);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(translation.get(Translation_.locale), locale));
			if (active != null) {
				clauses.getExpressions().add(cb.equal(root.get(TransportOption_.active), active));
			}
			if (transportOptionIds != null && !transportOptionIds.isEmpty()) {
				clauses.getExpressions().add(root.get(TransportOption_.id).in(transportOptionIds));
			}
			if (subdivIds != null && !subdivIds.isEmpty()) {
				clauses.getExpressions().add(subdiv.get(Subdiv_.subdivid).in(subdivIds));
			}
			cq.where(clauses);

			cq.select(cb.construct(TransportOptionDTO.class, root.get(TransportOption_.id),
					country.get(Country_.countryCode), company.get(Company_.companyCode),
					subdiv.get(Subdiv_.subdivCode), subdiv.get(Subdiv_.subdivid), root.get(TransportOption_.active),
					root.get(TransportOption_.dayOfWeek), method.get(TransportMethod_.method),
					translation.get(Translation_.translation), root.get(TransportOption_.localizedName)));
			cq.distinct(true);

			return cq;
		});
	}

	@Override
	public List<TransportOption> getAllDaySpecificTransportOptions(int fromSubdivId) {
		List<TransportOption> transOpts = getResultList(cb -> {
			CriteriaQuery<TransportOption> cq = cb.createQuery(TransportOption.class);
			Root<TransportOption> root = cq.from(TransportOption.class);
			Join<TransportOption, Subdiv> subdivJoin = root.join(TransportOption_.sub);
			Join<TransportOption, TransportMethod> transportMethodJoin = root.join(TransportOption_.method);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(TransportOption_.active)));
			clauses.getExpressions().add(cb.equal(subdivJoin.get(Subdiv_.subdivid), fromSubdivId));
			clauses.getExpressions().add(cb.isTrue(transportMethodJoin.get(TransportMethod_.optionPerDayOfWeek)));
			cq.where(clauses);
			return cq;
		});

		Collections.sort(transOpts, new TransportOptionComparator());
		return transOpts;
	}

	@Override
	public List<TransportOption> getTransportOptionsFromSubdiv(int fromSubdivId) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<TransportOption> cq = cb.createQuery(TransportOption.class);
		Root<TransportOption> root = cq.from(TransportOption.class);
		Join<TransportOption, Subdiv> subdiv = root.join(TransportOption_.sub);

		Fetch<TransportOption, TransportMethod> method = root.fetch(TransportOption_.method, JoinType.LEFT);
		method.fetch(TransportMethod_.methodTranslation);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), fromSubdivId));
		clauses.getExpressions().add(cb.isTrue(root.get(TransportOption_.active)));

		cq.where(clauses);
		cq.distinct(true);

		List<TransportOption> resultList = getEntityManager().createQuery(cq).getResultList();
		Collections.sort(resultList, new TransportOptionComparator());
		return resultList;
	}

	/**
	 * Returns the transport option with joined information - primarily used for
	 * DWR
	 */
	@Override
	public TransportOption getEager(int id) {
		return getFirstResult(cb -> {
			CriteriaQuery<TransportOption> cq = cb.createQuery(TransportOption.class);
			Root<TransportOption> root = cq.from(TransportOption.class);
			cq.where(cb.equal(root.get(TransportOption_.id), id));
			root.fetch("method");
			return cq;
		}).orElse(null);
	}

	@Override
	public List<TransportOption> transportOptionsFromSubdivWithActiveMethod(int fromSubdivId) {
		List<TransportOption> resultList = getResultList(cb -> {
			CriteriaQuery<TransportOption> cq = cb.createQuery(TransportOption.class);
			Root<TransportOption> root = cq.from(TransportOption.class);
			Join<TransportOption, TransportMethod> method = root.join(TransportOption_.method);
			Join<TransportOption, Subdiv> subdiv = root.join(TransportOption_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), fromSubdivId));
			clauses.getExpressions().add(cb.isTrue(method.get(TransportMethod_.active)));
			cq.where(clauses);
			return cq;
		});
		Collections.sort(resultList, new TransportOptionComparator());
		return resultList;
	}

	@Override
	public List<TransportOption> getTransportOptionsSchedulable(Integer fromSubdivId) {
		return getResultList(cb -> {
			CriteriaQuery<TransportOption> cq = cb.createQuery(TransportOption.class);
			Root<TransportOption> root = cq.from(TransportOption.class);
			Join<TransportOption, TransportMethod> method = root.join(TransportOption_.method);
			Join<TransportOption, Subdiv> subdiv = root.join(TransportOption_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), fromSubdivId));
			clauses.getExpressions().add(cb.isTrue(method.get(TransportMethod_.active)));
			clauses.getExpressions().add(cb.isTrue(method.get(TransportMethod_.optionPerDayOfWeek)));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public List<TransportOption> transportOptionByLocalizedName(Integer subdivid, String localizedName,
			Integer methodId) {
		return getResultList(cb -> {
			CriteriaQuery<TransportOption> cq = cb.createQuery(TransportOption.class);
			Root<TransportOption> root = cq.from(TransportOption.class);
			Join<TransportOption, Subdiv> subdivJoin = root.join(TransportOption_.sub);
			Join<TransportOption, TransportMethod> methodJoin = root.join(TransportOption_.method);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.like(root.get(TransportOption_.localizedName), localizedName));
			clauses.getExpressions().add(cb.equal(methodJoin.get(TransportMethod_.id), methodId));
			clauses.getExpressions().add(cb.equal(subdivJoin.get(Subdiv_.subdivid), subdivid));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public Integer getTransportOptionByJobItemId(Integer jobitemId) {
		return getFirstResult(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<JobItem> root = cq.from(JobItem.class);
			cq.where(cb.equal(root.get(JobItem_.jobItemId), jobitemId));
			cq.select(root.get(JobItem_.returnOption).get(TransportOption_.id));
			return cq;
		}).orElse(null);
	}
	
	@Override
	public List<Integer> itemsWithTransportOptionSchedulable(List<Integer> jobitemIds){
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, TransportOption> trOption = root.join(JobItem_.returnOption);
			Join<TransportOption, TransportMethod> trMethod = trOption.join(TransportOption_.method);
			Join<TransportOption, TransportOptionScheduleRule> rules = trOption.join(TransportOption_.scheduleRules);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(root.in(jobitemIds));
			clauses.getExpressions().add(cb.isTrue(trMethod.get(TransportMethod_.optionPerDayOfWeek)));
			clauses.getExpressions().add(cb.isNotNull(rules));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(root.get(JobItem_.jobItemId));
			return cq;
		});
	}
}