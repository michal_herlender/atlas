package org.trescal.cwms.core.deliverynote.form;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AddDeliveryItemValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(AddDeliveryItemForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		AddDeliveryItemForm command = (AddDeliveryItemForm) target;
		/**
		 * Business logic
		 */
		// make sure all items being sent that have accessories have their
		// accessories checked off too
		boolean oneOrMore = false;
		for (Integer itemId : command.getItems().keySet())
		{
			// if item is to be added
			if (command.getItems().get(itemId) != null)
			{
				// if accessories are not checked
				if (command.getAccessories().get(itemId) == null)
				{
					oneOrMore = true;
				}
			}
		}
		if (oneOrMore)
		{
			errors.rejectValue("accessories", null, "Items with accessories must have their accessories sent with the item.");
		}
		// if company doesn't allow part deliveries - all items should be
		// checked
		boolean allChecked = true;
		if (!command.isAllowsPartDeliveries())
		{
			for (Integer itemId : command.getItems().keySet())
			{
				if (command.getItems().get(itemId) == null)
				{
					allChecked = false;
				}
			}
		}
		if (!allChecked)
		{
			errors.rejectValue("items", "error.deliverynote.notsupportpartdel", "This job does not support part deliveries. Make sure all items are added.");
		}
		// if one item is in a group, all group members must be checked too
		List<JobItemGroup> groupsToAdd = new ArrayList<JobItemGroup>();
		List<Integer> groupIdsToAdd = new ArrayList<Integer>();
		for (JobItem ji : command.getItemsToAdd()) {
			if (ji.getGroup() != null) 
				if (!groupIdsToAdd.contains(ji.getGroup().getId())) {
					groupsToAdd.add(ji.getGroup());
					groupIdsToAdd.add(ji.getGroup().getId());
				} 
		}
		for (JobItemGroup group : groupsToAdd) {
			boolean allGroupItems = true;
			for (JobItem ji : group.getItems())
				if (command.getItems().get(ji.getJobItemId()) == null)
					allGroupItems = false;
			if (!allGroupItems)
				errors.rejectValue("items", "error.deliverynote.groupingitems", new Object[]{group.getId()},
						"One or more items from Group " + group.getId() + " have been selected, but some have not.");
		}
	}
}