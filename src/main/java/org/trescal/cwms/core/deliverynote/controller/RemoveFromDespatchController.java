package org.trescal.cwms.core.deliverynote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.exception.controller.IntranetController;

import javax.servlet.http.HttpServletRequest;

@Controller
@IntranetController
public class RemoveFromDespatchController {
	@Autowired
	private DeliveryService delServ;

	@RequestMapping(value = "/removefromdespatch.htm")
	protected ModelAndView handleRequestInternal(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		int delid = ServletRequestUtils.getIntParameter(request, "delid", 0);
		Delivery delivery = this.delServ.get(delid);
		delivery.setCrdes(null);
		delServ.removeDeliveryFromSchedule(delid);

		this.delServ.updateDelivery(delivery);

		redirectAttributes.addAttribute("delid", delid)
			.addAttribute("loadtab", "despatch-tab");

		return new ModelAndView(new RedirectView("viewdelnote.htm"));
	}
}