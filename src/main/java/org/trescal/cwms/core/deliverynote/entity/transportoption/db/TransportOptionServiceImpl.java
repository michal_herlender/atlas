package org.trescal.cwms.core.deliverynote.entity.transportoption.db;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.admin.dto.EditTransportOptionDTO;
import org.trescal.cwms.core.admin.form.AddTransportOptionForm;
import org.trescal.cwms.core.admin.form.TransportOptionForm;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.db.TransportMethodService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.db.TransportOptionScheduleRuleService;
import org.trescal.cwms.core.deliverynote.form.TransportOptionDTO;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.schedule.form.CreateScheduleForm;
import org.trescal.cwms.core.system.entity.closeddate.ClosedDate;
import org.trescal.cwms.core.system.entity.closeddate.db.ClosedDateService;
import org.trescal.cwms.core.tools.transitcalendar.ClientTransit;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.val;

@Service("TransportOptionService")
public class TransportOptionServiceImpl extends BaseServiceImpl<TransportOption, Integer>
		implements TransportOptionService {
	@Autowired
	private ClosedDateService closedDateServ;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private TransportMethodService transportMethodService;
	@Autowired
	private TransportOptionDao transportOptionDao;
	@Autowired
	private AddressSettingsForAllocatedSubdivService addressSettingsService;
	@Autowired
	private UserService userService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TransportOptionScheduleRuleService transportOptionScheduleRuleService;
	@Autowired
	private ScheduleService scheduleService;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;

	/**
	 * Supplied map indicates which transport options should be active, and
	 * which should be inactive / not exist Any remaining active methods should
	 * be created Note, this is only for non-day-of-week options at this time
	 */
	@Override
	public void editTransportOptions(TransportOptionForm form) {

		for (EditTransportOptionDTO dto : form.getOptions()) {
			List<String> dayList = new ArrayList<>();
			TransportOption transportOption = this.get(dto.getTransportOptionId());
			transportOption.setActive(dto.getActive());
			transportOption.setLocalizedName(dto.getLocalizedName());
			transportOption.setExternalRef(dto.getExternalRef());
			List<TransportOptionScheduleRule> newScheduleRuleList = new ArrayList<>();
			if (transportOption.getMethod().isOptionPerDayOfWeek()) {
				// list of schedule rules with dayOfweek not null
				List<TransportOptionScheduleRule> transportOptionScheduleRuleList = new ArrayList<>(
						dto.getScheduleRule());
				// get days already exist in data base for a transport option
				transportOptionScheduleRuleList.forEach(sr -> dayList.add(sr.getDayOfWeek()));

				// delete unselected Transport Option Schedule Rule already
				// exist in the database
				transportOption.getScheduleRules().forEach(rule -> {
					if (!dayList.contains(rule.getDayOfWeek())) {
						this.transportOptionScheduleRuleService.delete(rule);
					}
				});

				transportOptionScheduleRuleList.forEach(sr -> {
					// update Transport Option Schedule Rule
					if (sr.getId() != null) {
						TransportOptionScheduleRule tr = transportOptionScheduleRuleService.get(sr.getId());
						if (tr!= null && !tr.getCutoffTime().equals(sr.getCutoffTime())) {
							tr.setCutoffTime(sr.getCutoffTime());
							this.transportOptionScheduleRuleService.updateTransportOptionScheduleRule(tr);
						}	
					} else if (sr.getDayOfWeek() != null) {
						// create new Transport Option Schedule Rule
						TransportOptionScheduleRule scheduleRule = new TransportOptionScheduleRule();
						scheduleRule.setTransportOption(transportOption);
						scheduleRule.setDayOfWeek(sr.getDayOfWeek());
						scheduleRule.setCutoffTime(sr.getCutoffTime());
						newScheduleRuleList.add(scheduleRule);
						this.transportOptionScheduleRuleService.insertTransportOptionScheduleRule(scheduleRule);
					}
				});
			}
			if (!newScheduleRuleList.isEmpty()) {
				// add all new Schedule Rules to the transport option
				transportOption.setScheduleRules(newScheduleRuleList);
			}
			this.save(transportOption);
		}
	}

	@Override
	public SortedSet<KeyValueIntegerString> getTransportOptionDtosForSubdivs(Boolean extendedFormat, Boolean active,
			Locale locale, Collection<Integer> subdivIds) {
		SortedSet<KeyValueIntegerString> resultSet = new TreeSet<>();
		if ((subdivIds != null) && !subdivIds.isEmpty()) {
			List<TransportOptionDTO> projectionList = this.transportOptionDao.getTransportOptionDtos(active, locale,
					null, subdivIds);
			projectionList.forEach(dto -> resultSet.add(formatTransportOption(dto, extendedFormat)));
		}

		return resultSet;
	}

	@Override
	public SortedSet<KeyValueIntegerString> getTransportOptionDtosForIds(Boolean extendedFormat,
			Collection<Integer> ids, Locale locale) {
		SortedSet<KeyValueIntegerString> resultSet = new TreeSet<>();
		if ((ids != null) && !ids.isEmpty()) {
			List<TransportOptionDTO> projectionList = this.transportOptionDao.getTransportOptionDtos(null, locale, ids,
					null);
			projectionList.forEach(dto -> resultSet.add(formatTransportOption(dto, extendedFormat)));
		}
		return resultSet;
	}

	/**
	 * @param extendedFormat
	 *            - whether to prepend the country code, company code, and
	 *            subdiv code to the transport option
	 */

	public KeyValueIntegerString formatTransportOption(TransportOptionDTO dto, Boolean extendedFormat) {
		StringBuilder text = new StringBuilder();
		if (extendedFormat != null && extendedFormat) {
			text.append(dto.getCountryCode());
			text.append(" - ");
			text.append(dto.getCompanyCode());
			text.append(" - ");
			text.append(dto.getSubdivCode());
			text.append(" - ");
		}

		if (!StringUtils.isEmpty(dto.getLocalizedName())) {
			text.append(dto.getLocalizedName());
		} else if (dto.getMethodTranslation() != null && !dto.getMethodTranslation().isEmpty()) {
			text.append(dto.getMethodTranslation());
		} else {
			text.append(dto.getMethod());
		}
		// TODO multilanguage day of week options - not currently used
		return new KeyValueIntegerString(dto.getOptionId(), text.toString());
	}

	@Override
	public List<TransportOption> getAllDaySpecificTransportOptions(int fromSubdivId) {
		return this.transportOptionDao.getAllDaySpecificTransportOptions(fromSubdivId);
	}

	@Override
	public List<TransportOption> getTransportOptionsFromSubdiv(int fromSubdivId) {
		return this.transportOptionDao.getTransportOptionsFromSubdiv(fromSubdivId);
	}

	private boolean isClosedDateForCompany(LocalDate date, List<ClosedDate> closedDates) {
		return closedDates.stream().map(ClosedDate::getClosedDate).noneMatch(date::isEqual);
	}

	@Override
	public List<ClientTransit> listClientTransits(int fromSubdivId, int weeks) {
		Subdiv subdiv = this.subServ.get(fromSubdivId);
		List<TransportOption> options = this.getTransportOptionsFromSubdiv(fromSubdivId);
		List<ClosedDate> closedDates = this.closedDateServ.getFutureClosedDatesForCompany(subdiv.getComp().getCoid());

		val today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		val end = today.plusWeeks(weeks);
		val daysBetween = today.until(end, ChronoUnit.DAYS);

		return options.stream()
				.flatMap(to -> to.getDayOfWeek() == null ? IntStream.rangeClosed(0, (int) daysBetween)
						.mapToObj(today::plusDays)
						.filter(d -> d.getDayOfWeek() != DayOfWeek.SUNDAY && d.getDayOfWeek() != DayOfWeek.SATURDAY)
						.filter(d -> this.isClosedDateForCompany(d, closedDates)).map(d -> {
							val ct = new ClientTransit();
							ct.setClientTransit(to);
							ct.setTransitDate(d);
							return ct;
						})
						: IntStream.rangeClosed(0, weeks)
								.mapToObj(today.with(DayOfWeek.of(to.getDayOfWeek()))::plusWeeks)
								.filter(d -> isClosedDateForCompany(d, closedDates)).map(d -> {
									val ct = new ClientTransit();
									ct.setClientTransit(to);
									ct.setTransitDate(d);
									return ct;
								}))
				.collect(Collectors.toList());
	}

	/**
	 * Returns the Transport In option for the specified business subdiv, or
	 * null if none found
	 */
	@Override
	public TransportOption getTransportOptionIn(Address address, Subdiv allocatedSubdiv) {
		AddressSettingsForAllocatedSubdiv settings = this.addressSettingsService.getBySubdiv(address, allocatedSubdiv);
		TransportOption transportOption = null;
		if (settings != null) {
			transportOption = settings.getTransportIn();
		}
		return transportOption;
	}

	/**
	 * Returns the Transport Out option for the specified business subdiv, or
	 * null if none found
	 */
	@Override
	public TransportOption getTransportOptionOut(Address address, Subdiv allocatedSubdiv) {
		AddressSettingsForAllocatedSubdiv settings = this.addressSettingsService.getBySubdiv(address, allocatedSubdiv);
		TransportOption transportOption = null;
		if (settings != null) {
			transportOption = settings.getTransportOut();
		}
		return transportOption;
	}

	/**
	 * Returns the transport option with joined information - primarily used for
	 * DWR
	 */
	@Override
	public TransportOption getEager(int id) {
		return transportOptionDao.getEager(id);
	}

	@Override
	protected BaseDao<TransportOption, Integer> getBaseDao() {
		return this.transportOptionDao;
	}

	@Override
	public List<TransportOption> transportOptionsFromSubdivWithActiveMethod(int fromSubdivId) {
		return this.transportOptionDao.transportOptionsFromSubdivWithActiveMethod(fromSubdivId);
	}

	@Override
	public List<TransportOption> getTransportOptionsSchedulable(Integer fromSubdivId) {
		return this.transportOptionDao.getTransportOptionsSchedulable(fromSubdivId);
	}

	@Override
	public List<TransportOption> transportOptionByLocalizedName(Integer subdivid, String localizedName,
			Integer methodId) {
		return this.transportOptionDao.transportOptionByLocalizedName(subdivid, localizedName, methodId);
	}

	@Override
	public Boolean checkLocalizedNameUniqueness(Integer subdivid, String localizedName, Integer methodId) {
		List<TransportOption> options = this.transportOptionByLocalizedName(subdivid, localizedName, methodId);
		return options.isEmpty();
	}

	@Override
	public TransportOption addTransportOption(AddTransportOptionForm form, String username) {
		TransportOption newTransportOption = new TransportOption();
		newTransportOption.setSub(subdivService.get(form.getSubdivid()));
		newTransportOption.setActive(form.getActive());
		newTransportOption.setMethod(transportMethodService.findTransportMethod(form.getMethodId()));
		newTransportOption.setLastModified(new Timestamp(System.currentTimeMillis()));
		newTransportOption.setLastModifiedBy(userService.get(username).getCon());
		newTransportOption.setLocalizedName(form.getLocalizedName());
		newTransportOption.setExternalRef(form.getExternalRef());
		this.transportOptionDao.persist(newTransportOption);
		return newTransportOption;
	}

	@Override
	public LocalDate getNextAvailable(Integer transportOptionId, LocalDate scheduleDate) {
		TransportOption transportOption = this.get(transportOptionId);
		if (transportOption != null) {
			List<TransportOptionScheduleRule> scheduleRules = transportOption.getScheduleRules().stream()
					.sorted(Comparator.comparing(TransportOptionScheduleRule::getDayOfWeek))
					.collect(Collectors.toList());
			if (!scheduleRules.isEmpty()) {
				LocalDate localDate;
				LocalTime time;
				// get next available date comparing with the today
				if (scheduleDate == null) {
					// the date of today
					localDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
					// the current time
					time = LocalTime.now();
				} else {
					localDate = scheduleDate;
					time = LocalTime.MAX;
				}

				TransportOptionScheduleRule scheduleRuleFound = scheduleRules.stream()
						.filter(schr -> localDate.getDayOfWeek() == DayOfWeek.valueOf(schr.getDayOfWeek())
								&& schr.getCutoffTime().isAfter(time))
						.findFirst().orElse(null);
				// the schedule date will be the today (we are not past the
				// cutoff time)
				if (scheduleRuleFound != null) {
					return localDate;
				} else {
					// the schedule date will be the next available day of the
					// current week if exist
					scheduleRuleFound = scheduleRules.stream().filter(schr -> localDate.getDayOfWeek()
							.getValue() < DayOfWeek.valueOf(schr.getDayOfWeek()).getValue()).findFirst().orElse(null);
					if (scheduleRuleFound != null) {
						int day = DayOfWeek.valueOf(scheduleRuleFound.getDayOfWeek()).getValue()
								- localDate.getDayOfWeek().getValue();
						return localDate.plusDays(day);
					} else {
						// the schedule date will be the next available day of
						// the next week
						scheduleRuleFound = scheduleRules.stream().filter(schr -> localDate.getDayOfWeek()
								.getValue() > DayOfWeek.valueOf(schr.getDayOfWeek()).getValue()).findFirst()
								.orElse(null);
						if (scheduleRuleFound != null) {
							int day = (7 - localDate.getDayOfWeek().getValue())
									+ (DayOfWeek.valueOf(scheduleRuleFound.getDayOfWeek()).getValue());
							return localDate.plusDays(day);
						} else {
							// the schedule date = today + 7
							scheduleRuleFound = scheduleRules.stream().filter(schr -> localDate.getDayOfWeek()
									.getValue() == DayOfWeek.valueOf(schr.getDayOfWeek()).getValue()).findFirst()
									.orElse(null);
							if (scheduleRuleFound != null) {
								return localDate.plusDays(7);
							} else
								return null;
						}

					}
				}
			}
			return null;
		}
		return null;
	}

	@Override
	public List<LocalDate> availableDateForNWeeks(TransportOption transportOption, int numWeeks) {

        List<TransportOptionScheduleRule> scheduleRules = transportOption.getScheduleRules().stream()
            .sorted(Comparator.comparing(TransportOptionScheduleRule::getDayOfWeek)).collect(Collectors.toList());
        List<LocalDate> dates = new ArrayList<>();
        // the date of today
        LocalDate localDate = LocalDate.now();
        // the current time
        LocalTime time = LocalTime.now();

        if (!scheduleRules.isEmpty()) {
            for (TransportOptionScheduleRule scheduleRule : scheduleRules) {
                if (localDate.getDayOfWeek().getValue() == DayOfWeek
                    .valueOf(scheduleRule.getDayOfWeek()).getValue() && scheduleRule.getCutoffTime().isAfter(time)) {
                    java.sql.Date.valueOf(localDate);
                    dates.add(localDate);
                    for (int i = 0; i < numWeeks - 1; i++) {
                        dates.add(localDate.plusDays(7 * (i + 1)));
                    }

                } else if (localDate.getDayOfWeek().getValue() == DayOfWeek.valueOf(scheduleRule.getDayOfWeek()).getValue()) {
                    for (int i = 0; i < numWeeks; i++) {
                        dates.add(localDate.plusDays(7 * (i + 1)));
                    }
                } else if (localDate.getDayOfWeek()
                    .getValue() < DayOfWeek.valueOf(scheduleRule.getDayOfWeek()).getValue()) {
                    int day = DayOfWeek.valueOf(scheduleRule.getDayOfWeek()).getValue()
                        - localDate.getDayOfWeek().getValue();
                    LocalDate nextAvailableDate = localDate.plusDays(day);
                    dates.add(nextAvailableDate);
                    for (int i = 0; i < numWeeks - 1; i++) {
                        dates.add(nextAvailableDate.plusDays(7 * (i + 1)));
                    }
                } else {
                    int day = (7 - localDate.getDayOfWeek().getValue())
                        + (DayOfWeek.valueOf(scheduleRule.getDayOfWeek()).getValue());
                    LocalDate nextAvailableDate = localDate.plusDays(day);
                    dates.add(nextAvailableDate);
                    for (int i = 0; i < numWeeks - 1; i++) {
                        dates.add(nextAvailableDate.plusDays(7 * (i + 1)));
                    }
                }
            }
            return dates;
        }
        return new ArrayList<>();
    }

	@Override
	public Integer getTransportOptionByJobItemId(Integer jobitemId) {
		return this.transportOptionDao.getTransportOptionByJobItemId(jobitemId);
	}
	
	@Override
	public Integer transportOptionSchedulableFromJobItemIds(List<Integer> jobitemIds) {
		// check if all job items have a Transport Option Scheduled
		if(!jobitemIds.isEmpty() &&
				this.transportOptionDao.itemsWithTransportOptionSchedulable(jobitemIds).size() == jobitemIds.size()) {
			Boolean hasSametransportOption = true;
			Integer transportOptionId = this.getTransportOptionByJobItemId(jobitemIds.get(0));
			// check if all job items have the same Transport Option
			for(Integer itemId : jobitemIds) {
				Integer id = this.getTransportOptionByJobItemId(itemId);
				if(!transportOptionId.equals(id)) {
					hasSametransportOption = false;
					break;
				}
			}
			if(hasSametransportOption) {
				return transportOptionId;
			} else {
				return null;
			}
		}
		return null;
	}
	
	@Override
	public void automaticallyAddDeliveryNoteToTransportSchedule(Integer businessSubdivId, Delivery delivery, 
			String username, List<Integer> items) {
		Integer transportOptionId = this.transportOptionSchedulableFromJobItemIds(items);
		if(transportOptionId != null && delivery.getAddress() != null && delivery.getContact() != null) {
			LocalDate nextAvailableDate = this.getNextAvailable(transportOptionId, null);
			Schedule availableSchedule = this.scheduleService.scheduleFromTransportOptionAndDateAndSubdiv
			(transportOptionId, nextAvailableDate, delivery.getAddress().getSub().getId());
			if(availableSchedule != null) {
				this.scheduledDeliveryService.addScheduledDelivery(delivery, availableSchedule);
			} else if(nextAvailableDate != null){
				CreateScheduleForm scheduleForm = new CreateScheduleForm();
				scheduleForm.setTransportOptionId(transportOptionId);
				scheduleForm.setBusinessSubdivid(businessSubdivId);
				scheduleForm.setManualEntryDate(true);
				scheduleForm.setScheduleDate(nextAvailableDate);
				scheduleForm.setAddrid(delivery.getAddress().getAddrid());
				scheduleForm.setPersonid(delivery.getContact().getPersonid());
				scheduleForm.setStatus(ScheduleStatus.AGREED);
				scheduleForm.setType(ScheduleType.DELIVERY);
				scheduleForm.setSource(ScheduleSource.COMPANY);
				Schedule scheduleCreated = this.scheduleService.createSchedule(scheduleForm, username);
				this.scheduledDeliveryService.addScheduledDelivery(delivery, scheduleCreated);
			}
		}
	}
	
}