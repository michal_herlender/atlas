package org.trescal.cwms.core.deliverynote.projection;

import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DeliveryItemAccessoryDTO {
	private Integer accessoryId;
	private Integer deliveryItemId;
	private Integer quantity;
	private String description;					// Fallback
	private String descriptionTranslation;		// Actual translation
	// Optional references, opulated after creation
	private DeliveryItemDTO deliveryItem;
	
	public DeliveryItemAccessoryDTO(Integer accessoryId, Integer deliveryItemId, Integer quantity, String description, String descriptionTranslation) {
		this.accessoryId = accessoryId;
		this.deliveryItemId = deliveryItemId;
		this.quantity = quantity;
		this.description = description;
		this.descriptionTranslation = descriptionTranslation;
	}
}
