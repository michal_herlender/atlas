package org.trescal.cwms.core.deliverynote.entity.delivery.db;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.dto.DespatchItemDTO;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.form.DeliverySearchForm;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.rest.customer.dto.RestCustomerDeliveryDto;
import org.trescal.cwms.rest.customer.dto.RestDeliveryDetailFlatDto;

public interface DeliveryDao extends BaseDao<Delivery, Integer> {

	<DeliveryClass extends Delivery> DeliveryClass get(int id, Class<DeliveryClass> clazz);

	Delivery findDeliveryByDeliveryNo(String delno);

	List<JobDelivery> findByJob(Job job);

	List<DeliveryDTO> getDeliveryDtosForIds(Collection<Integer> deliveryIds);

	List<DespatchItemDTO> getTpGoodsOutItems(List<Integer> stateGroups, int allocatedSubdivId, Locale locale);

	List<DespatchItemDTO> getGoodsOutDtos(List<Integer> stateGroups, int allocatedSubdivId, Locale locale);

	List<? extends Delivery> findByJobItem(int jobItemId, DeliveryType type);

	PagedResultSet<Delivery> queryDeliveries(DeliverySearchForm dsf, PagedResultSet<Delivery> rs);

	List<RestCustomerDeliveryDto> findDeliveryDto(Subdiv subdiv, LocalDate dtStart, LocalDate dtEnd, DeliveryType deliveryType);

	List<RestDeliveryDetailFlatDto> findDeliveryDetailFlat(List<Integer> deliveriesId, Locale locale);

	void removeDeliveryFromSchedule(int delid);

	List<ClientDelivery> getClientDeliveryListByJobitemId(Integer jobitemid);

	List<JobDelivery> findDeliveries(List<Integer> deliveriesId);

	Optional<OrderProjection> getOrderProjectionForDeliveryNote(Integer deliveryId);

	Long deliveryCountByCourier(Integer courierId);

	Long deliveryCountByCourierDespatchType(Integer cdtId);

	boolean isLastTPDelivery(JobItem ji);
	
	List<Integer> getdeliveriesIdsByJob(Integer jobId);
	
	String deliveryExists(String DeliveryNo);
	
	Address getDeliveryAddressByDeliveryNo(String deliveryNo);
	
}