package org.trescal.cwms.core.deliverynote.entity.freehandcontact.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;

@Repository("FreehandContactDao")
public class FreehandContactDaoImpl extends BaseDaoImpl<FreehandContact, Integer> implements FreehandContactDao {
	
	@Override
	protected Class<FreehandContact> getEntity() {
		return FreehandContact.class;
	}
}