package org.trescal.cwms.core.deliverynote.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.HashMap;

@Controller
@IntranetController
@SessionAttributes(value = {Constants.SESSION_ATTRIBUTE_COMPANY})
public class ViewCourierDespatchController {
	@Autowired
	private CourierService courierServ;
	@Autowired
	private CourierDespatchService crDesServ;
	@Autowired
	private CourierDespatchTypeService courierDespatchTypeService;

	@ModelAttribute("crdes")
	protected CourierDespatchDTO formBackingObject(
		@RequestParam(name = "cdid") Integer cdid) throws Exception {
		return new CourierDespatchDTO(crDesServ.findCourierDespatch(cdid));
	}
	
	@RequestMapping(value="/viewcrdes.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
			@ModelAttribute("crdes") @Validated CourierDespatchDTO courierDespatchDTO, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData(courierDespatchDTO, companyDto);
		else {
			CourierDespatch courierDespatch = crDesServ.findCourierDespatch(courierDespatchDTO.getId());
			courierDespatch.setCdtype(courierDespatchTypeService.get(courierDespatchDTO.getCdTypeId()));
			courierDespatch.setConsignmentno(courierDespatchDTO.getConsignmentno());
			courierDespatch.setDespatchDate(courierDespatchDTO.getDespatchDate());
			this.crDesServ.updateCourierDespatch(courierDespatch);
			return new ModelAndView(new RedirectView("viewcrdes.htm?cdid=" + courierDespatch.getCrdespid()));
		}
	}

	@RequestMapping(value = "/viewcrdes.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("crdes") CourierDespatchDTO courierDespatchDTO, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {

		HashMap<String, Object> map = new HashMap<>();
		map.put("couriers", this.courierServ.getCouriersByBusinessCompany(companyDto.getKey()));
		if(courierDespatchDTO.getCdType() != null) map.put("typesforcurrentcourier", this.courierServ.getAllDespatchTypes(courierDespatchDTO.getCdType().getCourier().getCourierid()));
		return new ModelAndView("trescal/core/deliverynote/viewcrdes", map);
	}
}